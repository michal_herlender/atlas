package org.trescal.cwms.core.deliverynote.entity.deliverynote.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.DeliveryNote;
import org.trescal.cwms.core.deliverynote.form.JobItemForDeliveryNoteDTO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;

public interface DeliveryNoteDao extends BaseDao<DeliveryNote, Integer> {

	List<JobItemForDeliveryNoteDTO> getJobItemsFromCurrentJobForDeliveryNote(Integer jobId, StateGroup stateGroup, Integer subdivId,
			Locale locale);
	List<JobItemForDeliveryNoteDTO> getJobItemsFromOtherJobsForDeliveryNote(Subdiv subdiv, StateGroup stateGroup,
			Job currentJob, Locale locale);
	
	Boolean itemSelectedIsNotReadyForDelivery(StateGroup stateGroup, List<Integer> itemIds);
}