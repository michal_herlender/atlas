package org.trescal.cwms.core.deliverynote.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.deliverynote.dto.CourierDespatchDTO;
import org.trescal.cwms.core.deliverynote.entity.courier.db.CourierService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.db.CourierDespatchService;
import org.trescal.cwms.core.deliverynote.form.CourierDespatchSearchForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(value = { Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class CourierDespatchSearchController
{
	@Autowired
	private CourierDespatchService cdServ;
	@Autowired
	private CourierService courierServ;
	
	public static final String FORM_NAME = "form";
	
	@ModelAttribute(FORM_NAME)
	protected CourierDespatchSearchForm formBackingObject(@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto) throws Exception {
		CourierDespatchSearchForm form = new CourierDespatchSearchForm();
		form.setSubdivid(subdivDto.getKey());
		form.setCouriers(this.courierServ.getCouriersByBusinessCompany(companyDto.getKey()));
		form.setCoroles(EnumSet.complementOf(EnumSet.of(CompanyRole.NOTHING)));
		return form;
	}
	
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}
	
	@RequestMapping(value="/cdsearch.htm", method=RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/deliverynote/cdsearch";
	}
	
	@RequestMapping(value="/cdsearch.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(
			@RequestParam(name="coid", required=false, defaultValue="0") Integer coId,
			@ModelAttribute(FORM_NAME) CourierDespatchSearchForm form) throws Exception {
		form.setCompanyid(coId);
		HashMap<String, Object> map = new HashMap<String, Object>();
		PagedResultSet<CourierDespatchDTO> resultSet = cdServ.queryDespatches(form,
				new PagedResultSet<CourierDespatchDTO>(form.getResultsPerPage(), form.getPageNo()));
		form.setRs(resultSet);
		map.put("companyid", form.getCompanyid());
		return new ModelAndView("trescal/core/deliverynote/cdsearchresults", "map", map);
	}
}