package org.trescal.cwms.core.deliverynote.form;

import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EditDeliveryNoteForm {

	private Integer delid;
	private DeliveryType deltype;
	private Integer addressid;
	private Integer contactid;
	private Integer locid;
	private String address;
	private String company;
	private String contact;
	private String telephone;
	private String subdiv;

}
