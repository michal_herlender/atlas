package org.trescal.cwms.core.deliverynote.entity.courierdespatch.db;

import lombok.val;
import org.apache.commons.lang3.tuple.Triple;
import org.hibernate.criterion.MatchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.dto.CourierDespatchDTO;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier_;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch_;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType_;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery_;
import org.trescal.cwms.core.deliverynote.form.CourierDespatchSearchForm;
import org.trescal.cwms.core.deliverynote.projection.CourierDespatchProjectionDTO;
import org.trescal.cwms.core.tools.PagedResultSet;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.trescal.cwms.core.tools.StringJpaUtils.ilike;


@Repository("CourierDespatchDao")
public class CourierDespatchDaoImpl extends BaseDaoImpl<CourierDespatch, Integer> implements CourierDespatchDao {
	
	@Autowired
	private SubdivService subService;
	@Override
	protected Class<CourierDespatch> getEntity() {
		return CourierDespatch.class;
	}
	
	@Override
	public List<CourierDespatch> findAll() {
		return getResultList(cb ->{
			CriteriaQuery<CourierDespatch> cq = cb.createQuery(CourierDespatch.class);
			Root<CourierDespatch> root = cq.from(CourierDespatch.class);
			cq.orderBy(cb.asc(root.get(CourierDespatch_.despatchDate)));
			return cq;
		});
	}
	
	public List<Delivery> getDeliveriesForCourierDespatch(int cdid) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Delivery.class);
			val del = cq.from(Delivery.class);
			cq.where(cb.equal(del.get(Delivery_.crdes), cdid));
			return cq;
		});
	}
	
	public List<CourierDespatch> getDespatchesCreatedInLastWeek(Integer allocatedSubdivId) {
		return getResultList(cb -> {
			CriteriaQuery<CourierDespatch> cq = cb.createQuery(CourierDespatch.class);
			Root<CourierDespatch> root = cq.from(CourierDespatch.class);
			Join<CourierDespatch, Subdiv> subdivJoin = root.join(CourierDespatch_.organisation.getName());
			root.join(CourierDespatch_.cdtype);

			LocalDate now = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
			LocalDate sixDaysAgo = now.minusDays(6);

			Predicate clauses = cb.and(
				cb.equal(subdivJoin.get(Subdiv_.subdivid), allocatedSubdivId),
				cb.between(root.get(CourierDespatch_.creationDate), sixDaysAgo, now));

			cq.where(clauses);
			cq.orderBy(cb.asc(root.get(CourierDespatch_.despatchDate)));
			cq.select(root);

			return cq;
		});
	}
	
	@Override
	public PagedResultSet<CourierDespatchDTO> queryDespatches(CourierDespatchSearchForm form, 
			PagedResultSet<CourierDespatchDTO> rs) {
		
		Subdiv loggedInSubdiv = this.subService.get(form.getSubdivid());
		completePagedResultSet(rs, CourierDespatchDTO.class, cb -> cq -> {
			Root<CourierDespatch> root = cq.from(CourierDespatch.class);
			Join<CourierDespatch, CourierDespatchType>  cdtype = root.join(CourierDespatch_.cdtype);
			Join<CourierDespatchType, Courier> courier = cdtype.join(CourierDespatchType_.courier);
			
			Subquery<Long> deliveriesCountSq = cq.subquery(Long.class);
			Root<Delivery> deliveriesCountSqRoot = deliveriesCountSq.from(Delivery.class);
			Join<Delivery, CourierDespatch> cdJoin = deliveriesCountSqRoot.join(Delivery_.crdes);
			deliveriesCountSq.select(cb.count(deliveriesCountSqRoot));
			deliveriesCountSq.where(cb.equal(cdJoin.get(CourierDespatch_.crdespid),
					root.get(CourierDespatch_.crdespid)));
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(CourierDespatch_.organisation), loggedInSubdiv));
			
			// courier restriction
			if (form.getCourierid() != 0) {
				clauses.getExpressions().add(cb.equal(courier.get(Courier_.courierid), form.getCourierid()));
			}
			// courier despatch type restriction
			if (form.getCouriertypeid() != 0) {
				clauses.getExpressions().add(cb.equal(cdtype.get(CourierDespatchType_.cdtid), form.getCouriertypeid()));
			}
			
			// consignmentno restriction
			if ((form.getConsignmentno() != null) && (form.getConsignmentno().trim().length() > 0)) {
				clauses.getExpressions().add(ilike(cb, root.get(CourierDespatch_.consignmentno) , form.getConsignmentno(), MatchMode.ANYWHERE));	
			}
			// date restriction
			if (form.getDate() != null) {
				clauses.getExpressions().add(cb.equal(root.get(CourierDespatch_.despatchDate), form.getDate()));
			}
			
			// company restriction
			if (form.getCompanyid() != 0) {
				Join<CourierDespatch, Delivery> deliveries = root.join(CourierDespatch_.deliveries);
				Join<Delivery, Contact> contact = deliveries.join(Delivery_.contact);
				Join<Contact, Subdiv> sub = contact.join(Contact_.sub);
				Join<Subdiv, Company> comp = sub.join(Subdiv_.comp);
				clauses.getExpressions().add(cb.equal(comp.get(Company_.coid), form.getCompanyid()));
			}
			// restrict to only despatches that have deliveries
			clauses.getExpressions().add(cb.isNotEmpty(root.get(CourierDespatch_.deliveries)));
			cq.where(clauses);
			cq.distinct(true);
			
			CompoundSelection<CourierDespatchDTO> selection = cb.construct(CourierDespatchDTO.class, root.get(CourierDespatch_.crdespid),
					root.get(CourierDespatch_.consignmentno), cdtype.get(CourierDespatchType_.description), courier.get(Courier_.name),
					deliveriesCountSq.getSelection(), root.get(CourierDespatch_.despatchDate));
			
			List<javax.persistence.criteria.Order> order = new ArrayList<>();
			order.add(cb.desc(root.get(CourierDespatch_.despatchDate))); 
			order.add(cb.asc(cdtype.get(CourierDespatchType_.description)));
				
			return Triple.of(root, selection, order);	
		});
		return rs;
	}

	@Override
	public List<CourierDespatchProjectionDTO> getProjectionDTOs(Collection<Integer> despatchIds) {
		if (despatchIds == null || despatchIds.isEmpty())
			throw new IllegalArgumentException("Must supply at least one id");
		return getResultList(cb -> {
			CriteriaQuery<CourierDespatchProjectionDTO> cq = cb.createQuery(CourierDespatchProjectionDTO.class);
			Root<CourierDespatch> root = cq.from(CourierDespatch.class);
			Join<CourierDespatch, CourierDespatchType> cdtype = root.join(CourierDespatch_.cdtype, JoinType.INNER);
			Join<CourierDespatchType, Courier> courier = cdtype.join(CourierDespatchType_.courier, JoinType.INNER);
			
			cq.select(cb.construct(CourierDespatchProjectionDTO.class, 
					root.get(CourierDespatch_.crdespid),
					root.get(CourierDespatch_.consignmentno),
					root.get(CourierDespatch_.creationDate),
					root.get(CourierDespatch_.despatchDate),
					cdtype.get(CourierDespatchType_.cdtid),
					cdtype.get(CourierDespatchType_.description),
					courier.get(Courier_.courierid),
					courier.get(Courier_.name)
				));
			
			return cq;
		});
	}
}