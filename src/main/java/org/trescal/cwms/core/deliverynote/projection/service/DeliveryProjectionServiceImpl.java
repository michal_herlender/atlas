package org.trescal.cwms.core.deliverynote.projection.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.projection.AddressProjectionDTO;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.company.projection.SubdivProjectionDTO;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.db.CourierDespatchService;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.projection.CourierDespatchProjectionDTO;
import org.trescal.cwms.core.deliverynote.projection.DeliveryProjectionCommand;
import org.trescal.cwms.core.system.projection.note.NoteProjectionDTO;
import org.trescal.cwms.core.system.projection.note.service.NoteProjectionService;

import lombok.extern.slf4j.Slf4j;

@Service @Slf4j
public class DeliveryProjectionServiceImpl implements DeliveryProjectionService {
	@Autowired
	private DeliveryService deliveryService;
	@Autowired
	private CourierDespatchService courierDespatchService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private NoteProjectionService noteProjectionService;
	
	@Override
	public List<DeliveryDTO> getDeliveryDtosForIds(Collection<Integer> deliveryIds, DeliveryProjectionCommand dpCommand) {
		List<DeliveryDTO> deliveryDTOs = Collections.emptyList();
		if (!deliveryIds.isEmpty()) {
			deliveryDTOs = this.deliveryService.getDeliveryDtosForIds(deliveryIds);
			
			Integer allocatedCompanyId = dpCommand.getAllocatedCompanyId();
			if (allocatedCompanyId == null) {
				// Intended for use where we are retrieving a delivery, and use the owning company's information.
				if (deliveryDTOs.isEmpty()) throw new RuntimeException("No results found for specified delivery ids!");
				DeliveryDTO firstDto = deliveryDTOs.get(0); 
				allocatedCompanyId = firstDto.getSourceCompanyId();
			}
			if (dpCommand.getLoadSourceSubdiv())
				loadProjectionSourceSubdivs(deliveryDTOs, allocatedCompanyId);
			if (dpCommand.getLoadDestAddress())
				loadProjectionDestAddresses(deliveryDTOs, allocatedCompanyId);
			if (dpCommand.getLoadDestContact())
				loadProjectionDestContacts(deliveryDTOs, allocatedCompanyId);
			if (dpCommand.getLoadCreatedByContact())
				loadProjectionCreatedByContacts(deliveryDTOs, allocatedCompanyId);
			if (dpCommand.getLoadCourierDespatch())
				loadProjectionCourierDespatch(deliveryDTOs);
			
			if (dpCommand.getLoadNotes()) {
				Map<Integer, DeliveryDTO> mapDeliveryById = deliveryDTOs.stream()
						.collect(Collectors.toMap(dto -> dto.getDeliveryId(), dto -> dto));

				loadNotes(mapDeliveryById);
			}
			
		}

		return deliveryDTOs;
	}

	@Override
	public List<DeliveryDTO> getDtosByDeliveryItems(Collection<DeliveryItemDTO> diDtos, DeliveryProjectionCommand dpCommand) {
		MultiValuedMap<Integer, DeliveryItemDTO> mapItemsByDeliveryId = new HashSetValuedHashMap<>();
		diDtos.forEach(diDto -> mapItemsByDeliveryId.put(diDto.getDeliveryId(), diDto));
		Set<Integer> deliveryIds = mapItemsByDeliveryId.keySet();
		List<DeliveryDTO> deliveryDtos = getDeliveryDtosForIds(deliveryIds, dpCommand);
		for (DeliveryDTO deliveryDto : deliveryDtos) {
			mapItemsByDeliveryId.get(deliveryDto.getDeliveryId()).forEach(diDto -> diDto.setDelivery(deliveryDto));
		}
		return deliveryDtos;
	}

	private void loadProjectionSourceSubdivs(List<DeliveryDTO> deliveryDTOs, Integer allocatedCompanyId) {
		MultiValuedMap<Integer, DeliveryDTO> mapSourceSubdivIds = new HashSetValuedHashMap<>();
		deliveryDTOs.forEach(deliveryDto -> mapSourceSubdivIds.put(deliveryDto.getSourceSubdivId(), deliveryDto));
		
		Set<Integer> sourceSubdivIds = mapSourceSubdivIds.keySet();
		List<SubdivProjectionDTO> subdivDtos = this.subdivService
				.getSubdivProjectionDTOs(sourceSubdivIds, allocatedCompanyId);
		for (SubdivProjectionDTO subdivDto : subdivDtos) {
			mapSourceSubdivIds.get(subdivDto.getSubdivid())
				.forEach(deliveryDto -> deliveryDto.setSourceSubdiv(subdivDto));
		}
	}

	private void loadProjectionDestAddresses(List<DeliveryDTO> deliveryDTOs, Integer allocatedCompanyId) {
		MultiValuedMap<Integer, DeliveryDTO> mapDestAddressIds = new HashSetValuedHashMap<>();
		deliveryDTOs.forEach(deliveryDto -> mapDestAddressIds.put(deliveryDto.getDestAddressId(), deliveryDto));
		
		Set<Integer> destAddressIds = mapDestAddressIds.keySet();
		if (log.isDebugEnabled()) log.debug("destAddressIds : "+destAddressIds.toString());
		List<AddressProjectionDTO> addressDtos = this.addressService
				.getAddressProjectionDTOs(destAddressIds, allocatedCompanyId);
		
		for(AddressProjectionDTO addressDto : addressDtos) {
			mapDestAddressIds.get(addressDto.getAddressId())
				.forEach(deliveryDto -> deliveryDto.setDestAddress(addressDto));
		}
	}

	private void loadProjectionDestContacts(List<DeliveryDTO> deliveryDTOs, Integer allocatedCompanyId) {
		MultiValuedMap<Integer, DeliveryDTO> mapDestContactIds = new HashSetValuedHashMap<>();
		deliveryDTOs.forEach(deliveryDto -> mapDestContactIds.put(deliveryDto.getDestContactId(), deliveryDto));
		
		Set<Integer> destContactIds = mapDestContactIds.keySet();
		if (log.isDebugEnabled()) log.debug("destContactIds : "+destContactIds.toString());
		List<ContactProjectionDTO> contactDtos = this.contactService
				.getContactProjectionDTOs(destContactIds, allocatedCompanyId);
		if (log.isDebugEnabled()) log.debug("contactDtos.size() : "+contactDtos.size());
		
		for (ContactProjectionDTO contactDto : contactDtos) {
			mapDestContactIds.get(contactDto.getPersonid())
			.forEach(deliveryDto -> deliveryDto.setDestContact(contactDto));
		}
	}
	
	private void loadProjectionCreatedByContacts(List<DeliveryDTO> deliveryDTOs, Integer allocatedCompanyId) {
		MultiValuedMap<Integer, DeliveryDTO> mapContactIds = new HashSetValuedHashMap<>();
		deliveryDTOs.forEach(deliveryDto -> mapContactIds.put(deliveryDto.getCreatedByContactId(), deliveryDto));
		
		Set<Integer> contactIds = mapContactIds.keySet();
		if (log.isDebugEnabled()) log.debug("createdByContactIds : "+contactIds.toString());
		List<ContactProjectionDTO> contactDtos = this.contactService
				.getContactProjectionDTOs(contactIds, allocatedCompanyId);
		if (log.isDebugEnabled()) log.debug("contactDtos.size() : "+contactDtos.size());
		
		for (ContactProjectionDTO contactDto : contactDtos) {
			mapContactIds.get(contactDto.getPersonid())
			.forEach(deliveryDto -> deliveryDto.setCreatedByContact(contactDto));
		}
	}
	
	private void loadNotes(Map<Integer, DeliveryDTO> mapDeliveryById) {
		Collection<Integer> deliveryIds = mapDeliveryById.keySet();
		List<NoteProjectionDTO> notes = this.noteProjectionService.getDeliveryNotes(deliveryIds);
		if (log.isDebugEnabled()) log.debug("notes.size() : "+notes.size());
		for (NoteProjectionDTO noteDto : notes) {
			DeliveryDTO deliveryDto = mapDeliveryById.get(noteDto.getEntityId());
			if (deliveryDto.getNotes() == null) {
				deliveryDto.setNotes(new ArrayList<>());
			}
			deliveryDto.getNotes().add(noteDto);
		}
	}

	private void loadProjectionCourierDespatch(List<DeliveryDTO> deliveryDTOs) {
		MultiValuedMap<Integer, DeliveryDTO> mapCourierDespatchIds = new HashSetValuedHashMap<>();
		deliveryDTOs.stream()
			.filter(dto -> dto.getCourierDespatchId() != null)
			.forEach(dto -> mapCourierDespatchIds.put(dto.getCourierDespatchId(), dto));
		Collection<Integer> despatchIds = mapCourierDespatchIds.keySet();
		List<CourierDespatchProjectionDTO> despatchDtos = 
				this.courierDespatchService.getProjectionDTOs(despatchIds);
		for (CourierDespatchProjectionDTO despatchDto : despatchDtos) {
			for (DeliveryDTO deliveryDto : mapCourierDespatchIds.get(despatchDto.getCourierDespatchId())) {
				deliveryDto.setCourierDespatch(despatchDto);
			}
		}
	}

}
