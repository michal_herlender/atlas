package org.trescal.cwms.core.deliverynote.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.db.DeliveryItemService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory.DeliveryItemAccessory;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory.db.DeliveryItemAccessoryService;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.deliverynote.form.AddDeliveryItemForm;
import org.trescal.cwms.core.deliverynote.form.AddDeliveryItemValidator;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.PartDelivery;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class AddDeliveryItemController
{
	@Autowired
	private DeliveryItemService delItemServ;
	@Autowired
	private DeliveryService delServ;
	@Autowired
	private JobItemService jobItemServ;
	@Autowired
	private AddDeliveryItemValidator validator; // Manually validated due to onBind
	@Autowired
	private PartDelivery partDelivery;
	@Autowired
	private DeliveryItemAccessoryService delItemAccServ;

	@ModelAttribute("form")
	protected AddDeliveryItemForm formBackingObject(
			@RequestParam(name="delid", required=true) Integer delId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception
	{
		AddDeliveryItemForm form = new AddDeliveryItemForm();
		JobDelivery delivery = this.delServ.get(delId, JobDelivery.class);
		form.setRemainingJobItems(this.jobItemServ.getRemainingJobItemsForDelivery(delivery));
		HashMap<Integer, String> items = new HashMap<Integer, String>();
		HashMap<Integer, String> accessories = new HashMap<Integer, String>();
		List<Integer> ready = new ArrayList<Integer>();
		for (JobItem ji : form.getRemainingJobItems()) {
			items.put(ji.getJobItemId(), null);
			accessories.put(ji.getJobItemId(), null);
			if (delivery.isClientDelivery()) {
				if (this.jobItemServ.isReadyForClientDelivery(ji, subdivDto.getKey()))
					ready.add(ji.getJobItemId());
			}
			else if (delivery.isThirdPartyDelivery()) {
				if (this.jobItemServ.isReadyForTPDelivery(ji, subdivDto.getKey()))
					ready.add(ji.getJobItemId());
			}
			else if (delivery.isInternalDelivery()) {
				if (this.jobItemServ.isReadyForInternalDelivery(ji, subdivDto.getKey()))
					ready.add(ji.getJobItemId());
			}
			else if (delivery.isInternalReturnDelivery()){
				if (this.jobItemServ.isReadyForInternalReturnDelivery(ji, subdivDto.getKey()))
					ready.add(ji.getJobItemId());
			}
		}
		form.setItems(items);
		form.setAccessories(accessories);
		form.setReady(ready);
		form.setJob(delivery.getJob());
		form.setDelivery(delivery);
		form.setAllowsPartDeliveries(true);
		if (form.getJob().getCon() != null) {
			Company businessCompanyOfJob = form.getJob().getOrganisation().getComp();
			KeyValue<Scope, String> partDeliveryValue = partDelivery.getValueHierarchical(Scope.CONTACT, form.getJob().getCon(), businessCompanyOfJob);
			form.setAllowsPartDeliveries(partDelivery.parseValue(partDeliveryValue.getValue()));
		}
		return form;
	}
	
	protected void onBind(AddDeliveryItemForm form)
	{
		List<Integer> ids = new ArrayList<Integer>();
		for (Integer id : form.getItems().keySet())
		{
			if (form.getItems().get(id) != null)
			{
				ids.add(id);
			}
		}

		form.setItemsToAdd(this.jobItemServ.getAllItems(ids));
	}
	
	@RequestMapping(value="/additems.htm", method=RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/deliverynote/additems";
	}

	@RequestMapping(value="/additems.htm", method=RequestMethod.POST)
	public ModelAndView onSubmit(@ModelAttribute("form") AddDeliveryItemForm form, BindingResult bindingResult) throws Exception
	{
		onBind(form);
		validator.validate(form, bindingResult);
		if (bindingResult.hasErrors()) {
			return new ModelAndView("trescal/core/deliverynote/additems", "form", form);
		}
		JobDelivery d = (JobDelivery) form.getDelivery();

		for (JobItem ji : form.getRemainingJobItems())
		{
			if (form.getItems().get(ji.getJobItemId()) != null)
			{
				JobDeliveryItem deliveryitem = new JobDeliveryItem();
				deliveryitem.setDelivery(d);
				// the job item is reloaded here so that if the
				// create-delivery-note hook is active, there will not be a
				// session error when finding the job item
				deliveryitem.setJobitem(this.jobItemServ.findJobItem(ji.getJobItemId()));

				if (d.isThirdPartyDelivery())
				{
					if (deliveryitem.getJobitem().getTpRequirements() != null)
					{
						if (deliveryitem.getJobitem().getTpRequirements().size() > 0)
						{
							
							TPRequirement tpReq = deliveryitem.getJobitem().getTpRequirements().iterator().next();
							deliveryitem.setTpRequirement(tpReq);
							tpReq.setDeliveryItem(deliveryitem);
						}
					}
				}
				
				for (ItemAccessory itemAccessory : ji.getAccessories()){
					DeliveryItemAccessory deliveryItemAccessory = new DeliveryItemAccessory();
					deliveryItemAccessory.setAccessory(itemAccessory.getAccessory());
					deliveryItemAccessory.setQty(itemAccessory.getQuantity());
					deliveryItemAccessory.setDeliveryItem(deliveryitem);
					// insert delivery item accessory
					delItemAccServ.save(deliveryItemAccessory);
				}
				if(!StringUtils.isEmpty(ji.getAccessoryFreeText()))
					deliveryitem.setAccessoryFreeText(ji.getAccessoryFreeText());
				
				this.delItemServ.addItemToDelivery(deliveryitem);
			}
		}

		return new ModelAndView(new RedirectView("viewdelnote.htm?delid="
				+ form.getDelivery().getDeliveryid()));
	}
}