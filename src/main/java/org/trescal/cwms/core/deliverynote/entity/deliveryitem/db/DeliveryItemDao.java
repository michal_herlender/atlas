package org.trescal.cwms.core.deliverynote.entity.deliveryitem.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.entity.generaldeliveryitem.GeneralDeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.vdi.projection.PositionProjection;

public interface DeliveryItemDao extends BaseDao<DeliveryItem, Integer> {
	
	void addItemToDelivery(JobDeliveryItem jobDeliveryItem);
	
	DeliveryItem findDeliveryItem(int id, Class<? extends DeliveryItem> clazz);

	/**
	 * Returns a projection of delivery items; at least one of deliveryId or jobItemIds must be specified
	 * In case both provided, query must satisfy both results (AND) 
	 * @param deliveryId (id of the delivery being queried - can be null or empty if querying jobItemId) 
	 * @param jobItemIds (one or more job items ids being queried - can be null or empty if querying deliveryId)
	 * @return
	 */
	List<DeliveryItemDTO> getDeliveryItemDTOs(Integer deliveryId, Collection<Integer> jobItemIds);
	
	List<GeneralDeliveryItem> getItemsForGeneralDelivery(int delid);
	
	List<JobDeliveryItem> getItemsForJobDelivery(int delid);

	List<PositionProjection> getPositionProjectionForDeliveryNote(Integer deliveryId);
	
	JobDeliveryItem findNotYetDespatchedJobDeliveryItemByInstrument(Integer plantId);
	
	List<DeliveryItem> getItemsForJobDeliveryBydDelNo(String deliveryNo);
	
	
}