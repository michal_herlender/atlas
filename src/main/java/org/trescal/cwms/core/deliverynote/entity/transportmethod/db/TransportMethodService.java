package org.trescal.cwms.core.deliverynote.entity.transportmethod.db;

import java.util.List;

import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod;

public interface TransportMethodService
{
	TransportMethod findTransportMethod(int id);
	void insertTransportMethod(TransportMethod transportmethod);
	void updateTransportMethod(TransportMethod transportmethod);
	List<TransportMethod> getAllTransportMethods();
	void deleteTransportMethod(TransportMethod transportmethod);
	void saveOrUpdateTransportMethod(TransportMethod transportmethod);
}