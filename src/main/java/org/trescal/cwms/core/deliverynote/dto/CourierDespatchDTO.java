package org.trescal.cwms.core.deliverynote.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.tools.DateTools;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CourierDespatchDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer cdTypeId;
	private CourierDespatchType cdType;
	private Integer courierId;
	@Size(max = 100)
	private String consignmentno;
    private Contact createdBy;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate creationDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate despatchDate;
    private List<Delivery> deliveries;
	private String cdTypeDescription;
	private String courierName;
	private Long deliveriesCount;

	public CourierDespatchDTO(CourierDespatch courierDespatch) {
		this.id = courierDespatch.getCrdespid();
		if(courierDespatch.getCdtype() != null){
			this.cdTypeId = courierDespatch.getCdtype().getCdtid();
			this.cdType = courierDespatch.getCdtype();
			this.courierId = cdType.getCourier().getCourierid();
			this.cdTypeDescription = cdType.getDescription();
		}
		this.consignmentno = courierDespatch.getConsignmentno();
		this.createdBy = courierDespatch.getCreatedBy();
		this.creationDate = courierDespatch.getCreationDate();
		this.setDespatchDate(courierDespatch.getDespatchDate());
		this.deliveries = courierDespatch.getDeliveries();
	}

    //constructor used for delivery creation UI
    public CourierDespatchDTO(Integer id, String consignmentno, String cdTypeDescription, LocalDate despatchDate) {
        this.id = id;
        this.consignmentno = consignmentno;
        this.despatchDate = despatchDate;
        this.cdTypeDescription = cdTypeDescription;
    }


    //constructor used for Courier Despatch Search
    public CourierDespatchDTO(Integer id, String consignmentno, String cdTypeDescription, String courierName,
                              Long deliveriesCount, LocalDate despatchDate) {
        super();
        this.id = id;
        this.consignmentno = consignmentno;
        this.cdTypeDescription = cdTypeDescription;
        this.courierName = courierName;
        this.deliveriesCount = deliveriesCount;
        this.despatchDate = despatchDate;
    }

	public String getFormatedDespatchDate() {
		return this.despatchDate == null ? "" : DateTools.df.format(this.despatchDate);
	}

}