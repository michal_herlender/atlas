package org.trescal.cwms.core.deliverynote.entity.delivery;

import java.time.LocalDate;
import java.util.EnumSet;
import java.util.List;

import org.trescal.cwms.core.company.projection.AddressProjectionDTO;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.company.projection.SubdivProjectionDTO;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.projection.CourierDespatchProjectionDTO;
import org.trescal.cwms.core.system.projection.note.NoteProjectionDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeliveryDTO {

	private Integer deliveryId;
	private Class<? extends Delivery> deliveryClass;
	private String deliveryNumber;
	private Integer courierDespatchId;
	private Integer createdByContactId;
	private Integer sourceSubdivId;
	private Integer sourceCompanyId;
	private Integer destAddressId;
	private Integer destContactId;
	private LocalDate deliveryDate;
	// Optional references
	private CourierDespatchProjectionDTO courierDespatch;
	private ContactProjectionDTO createdByContact;
	private SubdivProjectionDTO sourceSubdiv;
	private ContactProjectionDTO destContact;
	private AddressProjectionDTO destAddress;
	private List<DeliveryItemDTO> deliveryItems;
	private List<NoteProjectionDTO> notes;

	// Constructor for query creation
	public DeliveryDTO(Integer deliveryId, Class<? extends Delivery> deliveryClass, String deliveryNumber,
			Integer courierDespatchId, Integer createdByContactId, Integer sourceSubdivId, Integer sourceCompanyId,
			Integer destAddressId, Integer destContactId, LocalDate deliveryDate) {
		super();
		this.deliveryId = deliveryId;
		this.deliveryClass = deliveryClass;
		this.deliveryNumber = deliveryNumber;
		this.courierDespatchId = courierDespatchId;
		this.createdByContactId = createdByContactId;
		this.sourceSubdivId = sourceSubdivId;
		this.sourceCompanyId = sourceCompanyId;
		this.destAddressId = destAddressId;
		this.destContactId = destContactId;
		this.deliveryDate = deliveryDate;
	}

	public DeliveryType getType() {
		DeliveryType result = EnumSet.allOf(DeliveryType.class).stream()
				.filter(dt -> dt.getDeliveryClass().equals(deliveryClass)).findAny().orElse(DeliveryType.UNDEFINED);
		return result;
	}

}
