package org.trescal.cwms.core.deliverynote.entity.clientdelivery;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;

@Entity
@DiscriminatorValue("client")
public class ClientDelivery extends JobDelivery
{
	@Override
	@Transient
	public DeliveryType getType() {
		return DeliveryType.CLIENT;
	}
}