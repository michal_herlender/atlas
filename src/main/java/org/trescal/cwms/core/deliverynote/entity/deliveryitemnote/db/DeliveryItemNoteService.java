package org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.db;

import java.util.List;

import org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.DeliveryItemNote;

/**
 * Interface for accessing and manipulating {@link DeliveryItemNote} entities.
 * 
 * @author jamiev
 */
public interface DeliveryItemNoteService
{
	/**
	 * Deletes the given {@link DeliveryItemNote} from the database.
	 * 
	 * @param deliveryitemnote the {@link DeliveryItemNote} to delete.
	 */
	void deleteDeliveryItemNote(DeliveryItemNote deliveryitemnote);

	/**
	 * Deletes the {@link DeliveryItemNote} with the given ID from the database.
	 * 
	 * @param id the {@link DeliveryItemNote} ID.
	 */
	void deleteDeliveryItemNoteById(int id);

	/**
	 * Returns the {@link DeliveryItemNote} with the given ID.
	 * 
	 * @param id the {@link DeliveryItemNote} ID.
	 * @return the {@link DeliveryItemNote}
	 */
	DeliveryItemNote findDeliveryItemNote(int id);

	/**
	 * Returns all {@link DeliveryItemNote}(s) stored in the database.
	 * 
	 * @return the {@link List} of {@link DeliveryItemNote}(s).
	 */
	List<DeliveryItemNote> getAllDeliveryItemNotes();

	/**
	 * Inserts the given {@link DeliveryItemNote} into the database.
	 * 
	 * @param deliveryitemnote the {@link DeliveryItemNote} to insert.
	 */
	void insertDeliveryItemNote(DeliveryItemNote deliveryitemnote);

	/**
	 * Inserts the given {@link DeliveryItemNote} if it is not currently stored
	 * in the database, and otherwise updates the existing
	 * {@link DeliveryItemNote}.
	 * 
	 * @param deliveryitemnote the {@link DeliveryItemNote} to insert or update.
	 */
	void saveOrUpdateDeliveryItemNote(DeliveryItemNote deliveryitemnote);

	/**
	 * Updates the given {@link DeliveryItemNote} in the database.
	 * 
	 * @param deliveryitemnote the {@link DeliveryItemNote} to update.
	 */
	void updateDeliveryItemNote(DeliveryItemNote deliveryitemnote);
}