package org.trescal.cwms.core.deliverynote.form;

import java.util.Date;

import org.trescal.cwms.core.company.entity.contact.Contact;

import lombok.Data;

@Data
public class TPInstructionDTO {
	
	private Integer tpInstructionId;
	private Integer jobItemId;
	private String instruction;
	private String contactName;
    private boolean isShownOnTPDelNote;
    private boolean active;
    private Date lastModified;
	public TPInstructionDTO(Integer tpInstructionId, Integer jobItemId, String instruction, Contact contactName, Date lastModified, boolean isShownOnTPDelNote,
			boolean active) {
		super();
		this.tpInstructionId = tpInstructionId;
		this.jobItemId=jobItemId;
		this.instruction = instruction;
		this.contactName = contactName.getName();
		this.lastModified = lastModified;
		this.isShownOnTPDelNote = isShownOnTPDelNote;
		this.active = active;
	}
    
    

}
