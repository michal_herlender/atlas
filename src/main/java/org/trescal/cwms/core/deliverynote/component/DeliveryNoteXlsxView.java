package org.trescal.cwms.core.deliverynote.component;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemAccessoryDTO;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.dto.InstrumentProjectionDTO;
import org.trescal.cwms.core.jobs.certificate.dto.CertificateProjectionDTO;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.db.JobItemPOService;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemPriceProjectionDto;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.PriceProjectionDto;
import org.trescal.cwms.core.pricing.purchaseorder.projection.PurchaseOrderItemProjectionDTO;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.reports.view.XlsxCellStyleHolder;
import org.trescal.cwms.core.tools.reports.view.XlsxViewDecorator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

@Component
public class DeliveryNoteXlsxView extends AbstractXlsxStreamingView {
	
	private static final boolean automaticAutosize = true;
	
	@Autowired
    private JobItemPOService jiPOService;

	@Override
	protected void buildExcelDocument(Map<String, Object> modelMap, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		DeliveryNoteModel model = (DeliveryNoteModel) modelMap.get("model");
		SXSSFWorkbook sxxfWorkbook = (SXSSFWorkbook) workbook;  
		XlsxCellStyleHolder styleHolder = new XlsxCellStyleHolder(workbook);

		Sheet sheetDelivery = sxxfWorkbook.createSheet("Delivery");
		Sheet sheetDeliveryItems = sxxfWorkbook.createSheet("Delivery Items");
		Sheet sheetAccessories = sxxfWorkbook.createSheet("Accessories");
		Sheet sheetCertificates = sxxfWorkbook.createSheet("Certificates");
		
		XlsxViewDecorator decoratorDelivery = new XlsxViewDecorator(sheetDelivery, sxxfWorkbook, automaticAutosize, styleHolder);
		XlsxViewDecorator decoratorDeliveryItems = new XlsxViewDecorator(sheetDeliveryItems, sxxfWorkbook, automaticAutosize, styleHolder);
		XlsxViewDecorator decoratorAccessories = new XlsxViewDecorator(sheetAccessories, sxxfWorkbook, automaticAutosize, styleHolder);  
		XlsxViewDecorator decoratorCertificates = new XlsxViewDecorator(sheetCertificates, sxxfWorkbook, automaticAutosize, styleHolder);  

		createLabelsDelivery(decoratorDelivery);
		createLabelsDeliveryItems(decoratorDeliveryItems);
		createLabelsAccessories(decoratorAccessories);
		createLabelsCertificates(decoratorCertificates);
		
		populateDelivery(decoratorDelivery, model);
		populateDeliveryItems(decoratorDeliveryItems, model.getDeliveryItemDtos());
		populateAccessories(decoratorAccessories, model.getDeliveryItemDtos());
		populateCertificates(decoratorCertificates, model.getDeliveryItemDtos(), model.getIncludeOnlySignedCertificates());
		
		decoratorDelivery.autoSizeColumns();
		decoratorDeliveryItems.autoSizeColumns();
		decoratorAccessories.autoSizeColumns();
		decoratorCertificates.autoSizeColumns();
	}
	
	private void createLabelsDelivery(XlsxViewDecorator decorator) {
		// All labels for now in separate rows
		int colIndex = 0;
		int rowIndex = 0;
		CellStyle styleHeader = decorator.getStyleHolder().getHeaderStyle();

		// TODO translations once design / layout of export is finalized 
		decorator.createCell(rowIndex++, colIndex, "Delivery Number", styleHeader);
		decorator.createCell(rowIndex++, colIndex, "Delivery Date", styleHeader);
		decorator.createCell(rowIndex, colIndex, "Item Quantity", styleHeader);


	}
	private void populateDelivery(XlsxViewDecorator decorator, DeliveryNoteModel model) {
		DeliveryDTO deliveryDto = model.getDeliveryDto();
		int itemQuantity = model.getDeliveryItemDtos().size();

		CellStyle styleText = decorator.getStyleHolder().getTextStyle();
		CellStyle styleDate = decorator.getStyleHolder().getDateStyle();
		CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();

		// Delivery information for single entity - basic for now - in column 1
		// TODO potentially replicate the general layout of the first page of the printed delivery note
		int colIndex = 1;
		int rowIndex = 0;
		decorator.createCell(rowIndex++, colIndex, deliveryDto.getDeliveryNumber(), styleText);
		decorator.createCell(rowIndex++, colIndex, deliveryDto.getDeliveryDate(), styleDate);
		decorator.createCell(rowIndex, colIndex, itemQuantity, styleInteger);
	}

	private void createLabelsDeliveryItems(XlsxViewDecorator decorator) {
		int colIndex = 0;
		int rowIndex = 0;
		CellStyle styleHeader = decorator.getStyleHolder().getHeaderStyle();
		
		decorator.createCell(rowIndex, colIndex++, "Delivery Item", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Job Client Ref", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Job Item Client Ref", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Job Item Number", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Date In", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Service Type", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Price", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Currency", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Client PO/BPO", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Trescal PO", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Trescal ID", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Client ID", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Serial Number", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Instrument Model Name", styleHeader);
		decorator.createCell(rowIndex, colIndex, "Customer Description", styleHeader);
	}
	
	private void populateDeliveryItems(XlsxViewDecorator decorator, Collection<DeliveryItemDTO> deliveryItems) {
		CellStyle styleText = decorator.getStyleHolder().getTextStyle();
		CellStyle styleDate = decorator.getStyleHolder().getDateStyle();
		CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();
		CellStyle styleCurrency = decorator.getStyleHolder().getMonetaryStyle();
		
		int colIndex = 0;
		int rowIndex = 1;
		int deliveryItemCount = 0;

		for (DeliveryItemDTO diDto : deliveryItems) {
			JobItemProjectionDTO jiDto = diDto.getJobItem();
			JobItemPriceProjectionDto priceDto = jiDto.getPriceContractReview();
			JobProjectionDTO jobDto = diDto.getJobItem().getJob();
			InstrumentProjectionDTO instDto = diDto.getJobItem().getInstrument();
			deliveryItemCount++;

			decorator.createCell(rowIndex, colIndex++, deliveryItemCount, styleInteger);
			decorator.createCell(rowIndex, colIndex++, jobDto.getClientRef(), styleText);
			decorator.createCell(rowIndex, colIndex++, jiDto.getClientRef(), styleText);
			decorator.createCell(rowIndex, colIndex++, formatJobItemNumber(jiDto), styleText);
			decorator.createCell(rowIndex, colIndex++, DateTools.dateFromZonedDateTime(jiDto.getDateIn()), styleDate);
			decorator.createCell(rowIndex, colIndex++, jiDto.getServiceType().getShortName(), styleText);
			decorator.createCell(rowIndex, colIndex++, formatPrice(priceDto), styleCurrency);
			decorator.createCell(rowIndex, colIndex++, jobDto.getCurrency().getValue(), styleText);
			
			if (jobDto.getBpoNumber() != null){
                decorator.createCell(rowIndex, colIndex++, jobDto.getBpoNumber(), styleText);
            } else {
                String pos = jiPOService.getAllPOsByJobItem(jiDto.getJobItemId());
                if(pos != null){
                    decorator.createCell(rowIndex, colIndex++, pos, styleText);
                }
            }
			decorator.createCell(rowIndex, colIndex++, formatSupplierPOs(jiDto.getSupplierPos()), styleText);
			decorator.createCell(rowIndex, colIndex++, instDto.getPlantid(), styleInteger);
			decorator.createCell(rowIndex, colIndex++, instDto.getPlantno(), styleText);
			decorator.createCell(rowIndex, colIndex++, instDto.getSerialno(), styleText);
			decorator.createCell(rowIndex, colIndex++, instDto.getInstrumentModelNameViaFields(true), styleText);
			decorator.createCell(rowIndex, colIndex, instDto.getCustomerDescription(), styleText);

			// TODO consider having accessories / certificates here in multi-line text (in addition to separate sheet?)
			// or accessory count / certificate count?

			rowIndex++;
			colIndex = 0;
		}
		
	}
	
	private BigDecimal formatPrice(JobItemPriceProjectionDto jiPriceDto) {
		// TODO consider job costing price first, revert to contract review otherwise.
		BigDecimal total = new BigDecimal("0.00"); 
		for (PriceProjectionDto priceDto : jiPriceDto.getPrices()) {
			total = total.add(priceDto.getFinalCost());
		}
		return total;
	}
	
	private String formatJobItemNumber(JobItemProjectionDTO jiDto) {
		return jiDto.getJob().getJobno() +
			"." +
			jiDto.getItemno().toString();
	}

	private void createLabelsAccessories(XlsxViewDecorator decorator) {
		int colIndex = 0;
		int rowIndex = 0;
		CellStyle styleHeader = decorator.getStyleHolder().getHeaderStyle();

		decorator.createCell(rowIndex, colIndex++, "Delivery Item", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Job Item Number", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Quantity", styleHeader);
		decorator.createCell(rowIndex, colIndex, "Accessory Description", styleHeader);

	}
	private void populateAccessories(XlsxViewDecorator decorator, Collection<DeliveryItemDTO> deliveryItems) {
		// TODO Add Job Item - Accessory Free Text!
		
		CellStyle styleText = decorator.getStyleHolder().getTextStyle();
		CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();
		int colIndex = 0;
		int rowIndex = 1;
		int deliveryItemCount = 0;
		
		for (DeliveryItemDTO diDto : deliveryItems) {
			JobItemProjectionDTO jiDto = diDto.getJobItem();
			Collection<DeliveryItemAccessoryDTO> accessories = diDto.getAccessories();
			deliveryItemCount++;
			
			if (diDto.getAccessoryFreeText() != null && !diDto.getAccessoryFreeText().isEmpty()) {
				decorator.createCell(rowIndex, colIndex++, deliveryItemCount, styleText);
				decorator.createCell(rowIndex, colIndex++, formatJobItemNumber(jiDto), styleText);
				colIndex++;        // Blank for quantity
				decorator.createCell(rowIndex, colIndex, diDto.getAccessoryFreeText(), styleText);

				rowIndex++;
				colIndex = 0;

			}
			if (accessories != null) {
				for (DeliveryItemAccessoryDTO accessoryDto : accessories) {
					decorator.createCell(rowIndex, colIndex++, deliveryItemCount, styleText);
					decorator.createCell(rowIndex, colIndex++, formatJobItemNumber(jiDto), styleText);
					decorator.createCell(rowIndex, colIndex++, accessoryDto.getQuantity(), styleInteger);
					decorator.createCell(rowIndex, colIndex, formatAccessory(accessoryDto), styleText);

					rowIndex++;
					colIndex = 0;
				}
			}
		}
		
	}
	
	private String formatAccessory(DeliveryItemAccessoryDTO accessoryDto) {
		String result;
		if (accessoryDto.getDescriptionTranslation() != null)
			result = accessoryDto.getDescriptionTranslation();
		else
			result = accessoryDto.getDescription();
		return result;
	}

	private void createLabelsCertificates(XlsxViewDecorator decorator) {
		int colIndex = 0;
		int rowIndex = 0;
		CellStyle styleHeader = decorator.getStyleHolder().getHeaderStyle();
		
		decorator.createCell(rowIndex, colIndex++, "Delivery Item", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Job Item Number", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Trescal ID", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Client ID", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Serial Number", styleHeader);

		decorator.createCell(rowIndex, colIndex++, "Certificate Number", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Calibration Date", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Certificate Date", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Verification Status", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Adjustment", styleHeader);

		decorator.createCell(rowIndex, colIndex++, "Optimization", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Repair", styleHeader);
		decorator.createCell(rowIndex, colIndex, "Restriction", styleHeader);
	}
	private void populateCertificates(XlsxViewDecorator decorator, Collection<DeliveryItemDTO> deliveryItems, boolean includeOnlySignedCertificates) {
		CellStyle styleText = decorator.getStyleHolder().getTextStyle();
		CellStyle styleDate = decorator.getStyleHolder().getDateStyle();
		CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();
		int colIndex = 0;
		int rowIndex = 1;
		int deliveryItemCount = 0;
		
		EnumSet<CertStatusEnum> includeStatus;
		if (includeOnlySignedCertificates) {
			includeStatus = EnumSet.of(CertStatusEnum.SIGNED);
		}
		else {
			includeStatus = EnumSet.of(CertStatusEnum.SIGNED, CertStatusEnum.SIGNING_NOT_SUPPORTED);
		}		
		
		for (DeliveryItemDTO diDto : deliveryItems) {
			JobItemProjectionDTO jiDto = diDto.getJobItem();
			deliveryItemCount++;
			
			if (jiDto.getCertificates() != null) {
				for (CertificateProjectionDTO certDto : jiDto.getCertificates()) {
					if (certDto.getCertStatus() != null && includeStatus.contains(certDto.getCertStatus())) {
						decorator.createCell(rowIndex, colIndex++, deliveryItemCount, styleInteger);
						decorator.createCell(rowIndex, colIndex++, formatJobItemNumber(certDto.getJobItemDto()), styleText);
						decorator.createCell(rowIndex, colIndex++, certDto.getJobItemDto().getInstrument().getPlantid(), styleText);
						decorator.createCell(rowIndex, colIndex++, certDto.getJobItemDto().getInstrument().getPlantno(), styleText);
						decorator.createCell(rowIndex, colIndex++, certDto.getJobItemDto().getInstrument().getSerialno(), styleText);

						decorator.createCell(rowIndex, colIndex++, formatCertificateNumber(certDto), styleText);
						decorator.createCell(rowIndex, colIndex++, certDto.getCalDate(), styleDate);
						decorator.createCell(rowIndex, colIndex++, certDto.getCertDate(), styleDate);
						decorator.createCell(rowIndex, colIndex++, formatVerificationStatus(certDto), styleText);
						decorator.createCell(rowIndex, colIndex++, formatBoolean(certDto.getAdjustment()), styleInteger);

						decorator.createCell(rowIndex, colIndex++, formatBoolean(certDto.getOptimization()), styleInteger);
						decorator.createCell(rowIndex, colIndex++, formatBoolean(certDto.getRepair()), styleInteger);
						decorator.createCell(rowIndex, colIndex, formatBoolean(certDto.getRestriction()), styleInteger);

						rowIndex++;
						colIndex = 0;
					}
				}
			}
		}
		
	}
	
	// Formats nullable boolean to 1 if not null and true
	private Integer formatBoolean(Boolean value) {
		Integer result = null;
		if ((value != null) && value) {
			result = 1;
		}
		return result;
	}
	
	private String formatVerificationStatus(CertificateProjectionDTO certDto) {
		CalibrationVerificationStatus status = certDto.getCalibrationVerificationStatus();
		String result = null;
		if (status != null) {
			result = status.getName();
		}
		return result;
	}
	
	private String formatCertificateNumber(CertificateProjectionDTO certDto) {
		if (certDto.getThirdCertNo() != null && !certDto.getThirdCertNo().isEmpty()) {
			return certDto.getThirdCertNo();
		}
		else {
			return certDto.getCertno();
		}
	}
	
	private String formatSupplierPOs(List<PurchaseOrderItemProjectionDTO> poItems) {
		String formattedResult = null;
		if (poItems != null) {
			StringBuffer result = new StringBuffer();
			for (PurchaseOrderItemProjectionDTO poiDto : poItems) {
				if (result.length() > 0)
					result.append(", ");
				result.append(poiDto.getPurchaseOrder().getPurchaseOrderNumber());
				result.append(".");
				result.append(poiDto.getItemno());
			}
			formattedResult = result.toString(); 
		}
		return formattedResult;
	}
}