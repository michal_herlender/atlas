package org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.DeliveryItemNote;

@Service("DeliveryItemNoteService")
public class DeliveryItemNoteServiceImpl implements DeliveryItemNoteService
{
	@Autowired
	DeliveryItemNoteDao deliveryItemNoteDao;

	public void deleteDeliveryItemNote(DeliveryItemNote deliveryitemnote)
	{
		this.deliveryItemNoteDao.remove(deliveryitemnote);
	}

	public void deleteDeliveryItemNoteById(int id)
	{
		this.deliveryItemNoteDao.remove(this.deliveryItemNoteDao.find(id));
	}

	public DeliveryItemNote findDeliveryItemNote(int id)
	{
		return this.deliveryItemNoteDao.find(id);
	}

	public List<DeliveryItemNote> getAllDeliveryItemNotes()
	{
		return this.deliveryItemNoteDao.findAll();
	}

	public void insertDeliveryItemNote(DeliveryItemNote DeliveryItemNote)
	{
		this.deliveryItemNoteDao.persist(DeliveryItemNote);
	}

	public void saveOrUpdateDeliveryItemNote(DeliveryItemNote deliveryitemnote)
	{
		this.deliveryItemNoteDao.saveOrUpdate(deliveryitemnote);
	}

	public void setDeliveryItemNoteDao(DeliveryItemNoteDao deliveryItemNoteDao)
	{
		this.deliveryItemNoteDao = deliveryItemNoteDao;
	}

	public void updateDeliveryItemNote(DeliveryItemNote DeliveryItemNote)
	{
		this.deliveryItemNoteDao.update(DeliveryItemNote);
	}
}