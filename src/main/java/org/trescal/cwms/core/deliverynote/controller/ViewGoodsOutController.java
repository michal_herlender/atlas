package org.trescal.cwms.core.deliverynote.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.deliverynote.component.ClientGoodsOutModelGenerator;
import org.trescal.cwms.core.deliverynote.component.GoodsOutDataModel;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY})
public class ViewGoodsOutController {
    @Autowired
    private ClientGoodsOutModelGenerator generator;
    @Autowired
    private ScheduleService schServ;

    @RequestMapping(value = "/viewgoodsout.htm", method = RequestMethod.GET)
    public String referenceData(Model model, Locale locale,
                                @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
        List<StateGroup> stateGroups = new ArrayList<>();
        stateGroups.add(StateGroup.CLIENTDELIVERY);
        stateGroups.add(StateGroup.CLIENTDESPATCH);
        stateGroups.add(StateGroup.AWAITINGDESPATCHAPPROVAL);

        GoodsOutDataModel dataModel = this.generator.getNewGoodsOutDataModel(stateGroups, subdivDto.getKey(), companyDto.getKey(), locale);

        model.addAttribute("dataModel", dataModel);
        // TODO replace schedule lists with counts
        model.addAttribute("futureSchedules", this.schServ.getSchedulesForDate(null, true, null, null, subdivDto.getKey(), companyDto.getKey()).size());
        model.addAttribute("todaySchedules", this.schServ.getSchedulesForDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()), false, null, null, subdivDto.getKey(), companyDto.getKey()).size());
        model.addAttribute("stateGroups", stateGroups);

        return "trescal/core/deliverynote/viewnewgoodsout";
    }
}