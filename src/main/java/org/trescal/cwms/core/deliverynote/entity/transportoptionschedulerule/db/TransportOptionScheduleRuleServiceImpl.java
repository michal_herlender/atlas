package org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.db;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.admin.form.AddTransportOptionForm;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.TransportOptionScheduleRule;

@Service("TransportOptionScheduleRuleService")
public class TransportOptionScheduleRuleServiceImpl extends BaseServiceImpl<TransportOptionScheduleRule, Integer>
		implements TransportOptionScheduleRuleService {

	@Autowired
	TransportOptionScheduleRuleDao transportOptionScheduleRuleDao;

	@Override
	protected BaseDao<TransportOptionScheduleRule, Integer> getBaseDao() {
		return this.transportOptionScheduleRuleDao;
	}
	
	@Override
	public void updateTransportOptionScheduleRule(TransportOptionScheduleRule transportOptionScheduleRule){
		this.transportOptionScheduleRuleDao.update(transportOptionScheduleRule);
	}

	@Override
	public void insertTransportOptionScheduleRule(TransportOptionScheduleRule transportOptionScheduleRule){
		this.transportOptionScheduleRuleDao.persist(transportOptionScheduleRule);
	}
	
	@Override
	public void addTransportOptionScheduleRule(AddTransportOptionForm form, TransportOption transportOption) {
		for (TransportOptionScheduleRule toScheduleRule : form.getScheduleRule().stream()
				.filter(sr -> sr.getDayOfWeek() != null).collect(Collectors.toList())) {
			TransportOptionScheduleRule newTransportOptionScheduleRule = new TransportOptionScheduleRule();
			newTransportOptionScheduleRule.setTransportOption(transportOption);
			newTransportOptionScheduleRule.setDayOfWeek(toScheduleRule.getDayOfWeek().toString());
			newTransportOptionScheduleRule.setCutoffTime(toScheduleRule.getCutoffTime());
			this.insertTransportOptionScheduleRule(newTransportOptionScheduleRule);
		}
	}
	
}
