package org.trescal.cwms.core.deliverynote.entity.courierdespatch;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "courierdespatch")
public class CourierDespatch extends Allocated<Subdiv> {
	private CourierDespatchType cdtype;
	private String consignmentno;
	private int crdespid;
	private Contact createdBy;
	private LocalDate creationDate;
	private List<Delivery> deliveries;
	private LocalDate despatchDate;

	/**
	 * @return the cdtype
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cdtypeid")
	public CourierDespatchType getCdtype() {
		return this.cdtype;
	}

	/**
	 * @return the consignmentno
	 */
	@Length(max = 100)
	@Column(name = "consignmentno")
	public String getConsignmentno()
	{
		return this.consignmentno;
	}

	/**
	 * @return the crdespid
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "courierdespatchid", nullable = false, unique = true)
	@Type(type = "int")
	public int getCrdespid()
	{
		return this.crdespid;
	}

	/**
	 * @return the createdBy
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "createdbyid")
	public Contact getCreatedBy()
	{
		return this.createdBy;
	}

	/**
     * @return the creationDate
     */
    @Column(name = "creationdate", columnDefinition = "date")
    public LocalDate getCreationDate() {
        return this.creationDate;
    }

	/**
	 * @return the deliveries
	 */
	@OneToMany(mappedBy = "crdes", fetch = FetchType.LAZY)
	public List<Delivery> getDeliveries()
	{
		return this.deliveries;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate(LocalDate creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @param cdtype the cdtype to set
	 */
	public void setCdtype(CourierDespatchType cdtype)
	{
		this.cdtype = cdtype;
	}

	/**
	 * @param consignmentno the consignmentno to set
	 */
	public void setConsignmentno(String consignmentno)
	{
		this.consignmentno = consignmentno;
	}

	/**
	 * @param crdespid the crdespid to set
	 */
	public void setCrdespid(int crdespid)
	{
		this.crdespid = crdespid;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Contact createdBy)
	{
		this.createdBy = createdBy;
    }

    /**
     * @return the despatchDate
     */
    @Column(name = "despatchdate", columnDefinition = "date")
    public LocalDate getDespatchDate() {
        return this.despatchDate;
	}

	/**
	 * @param deliveries the deliveries to set
	 */
	public void setDeliveries(List<Delivery> deliveries)
	{
		this.deliveries = deliveries;
	}

	/**
	 * @param despatchDate the despatchDate to set
	 */
	public void setDespatchDate(LocalDate despatchDate) {
		this.despatchDate = despatchDate;
	}
}