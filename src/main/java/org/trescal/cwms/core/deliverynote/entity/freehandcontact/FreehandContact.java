package org.trescal.cwms.core.deliverynote.entity.freehandcontact;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;

@Entity
@Table(name = "freehandcontact")
public class FreehandContact extends Auditable
{
	private String address;
	private String company;
	private String contact;
	private int id;
	private String subdiv;
	private String telephone;

	@Length(max = 300)
	@Column(name = "address", length = 300)
	public String getAddress()
	{
		return this.address;
	}

	@Length(max = 100)
	@Column(name = "company", length = 100)
	public String getCompany()
	{
		return this.company;
	}

	@Length(max = 100)
	@Column(name = "contact", length = 100)
	public String getContact()
	{
		return this.contact;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Length(max = 100)
	@Column(name = "subdiv", length = 100)
	public String getSubdiv()
	{
		return this.subdiv;
	}

	@Length(max = 30)
	@Column(name = "telephone", length = 30)
	public String getTelephone()
	{
		return this.telephone;
	}

	@Transient
	public boolean isEmpty()
	{
		if (((this.address == null) || this.address.trim().isEmpty())
				&& ((this.company == null) || this.company.trim().isEmpty())
				&& ((this.contact == null) || this.contact.trim().isEmpty())
				&& ((this.subdiv == null) || (this.subdiv.trim().isEmpty() && ((this.telephone == null) || this.telephone.trim().isEmpty()))))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public void setAddress(String address)
	{
		this.address = address;
	}

	public void setCompany(String company)
	{
		this.company = company;
	}

	public void setContact(String contact)
	{
		this.contact = contact;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setSubdiv(String subdiv)
	{
		this.subdiv = subdiv;
	}

	public void setTelephone(String telephone)
	{
		this.telephone = telephone;
	}
}
