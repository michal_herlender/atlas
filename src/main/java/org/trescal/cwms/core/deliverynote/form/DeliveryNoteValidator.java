package org.trescal.cwms.core.deliverynote.form;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.SortedSet;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.db.DeliveryNoteService;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.db.JobItemGroupService;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class DeliveryNoteValidator extends AbstractBeanValidator {

	private static final Logger logger = LoggerFactory.getLogger(DeliveryNoteValidator.class);
	@Autowired
	private JobItemService jiService;
	@Autowired
	private ContactService conService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private JobItemGroupService jiGroupeService;
	@Autowired
	private DeliveryNoteService deliveryNoteService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(CreateDeliveryNotePlainForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		logger.debug("DeliveryNoteValidator called");
		super.validate(target, errors);

		CreateDeliveryNotePlainForm form = (CreateDeliveryNotePlainForm) target;

		Integer contactId = form.getContactid();
		Contact contact = conService.get(contactId);

		if (form.getAddressid() == null) {
			errors.reject("error.createdeliverynoteform.delivery.freehandContact.address", "The delivery must have an address");
		}

		if (form.getDeliveryType().equals(DeliveryType.CLIENT)) {
			// if company doesn't allow part deliveries - all items should be
			// checked
			if (!form.isAllowsPartDeliveries() && ObjectUtils.isEmpty(form.getItems())) {
				errors.reject("error.deliverynote.notsupportpartdel", "This job does not support part deliveries. Make sure all items are added");
			}

			List<Integer> selectedGroupIds = this.jiGroupeService.getGroupIdsByJobitemIds(form.getItems());

			logger.debug("selectedGroupIds.size() : {}" , selectedGroupIds.size());

			for (Integer groupId : selectedGroupIds) {
				List<Integer> jitemids = jiGroupeService.getJobitemsByGroupId(groupId);
				int size = 0;
				for (Integer jitemId : jitemids) {
					if (form.getItems().contains(jitemId)) {
						size++;
					}
				}
				if (size != jitemids.size()) {
					errors.rejectValue("", "", new Object[]{groupId}, "One or more items from Group " + groupId + " have been selected, but some have not.");
				}
				logger.debug("rejecting group : {}" , groupId);
			}

			Integer addrId = null;
			if (form.getAddressid() != null) {
				addrId = form.getAddressid();
			}
			else if (form.getAddrid() != null) {
				addrId = form.getAddrid();
			}

			if (addrId != null) {
				// compare custom return to addresses with delivery address
				final Integer finalAddrId = addrId;
				List<String> nonCompatibleItems = new ArrayList<>();
				jiService.getAllItems(form.getItems()).stream()
						.filter(ji -> ji.getReturnToAddress() != null && !ji.getReturnToAddress().getAddrid().equals(finalAddrId))
						.forEach(ji -> nonCompatibleItems.add(ji.getItemNo() + ""));
	
				if (!nonCompatibleItems.isEmpty()) {
					errors.reject("error.deliverynote.createdelnot.incomatibleaddresses",
							new String[] { String.join(", ", nonCompatibleItems) },
							"The return address of the following items ({0}) are not compatible with the main delivery address.");
				}
			}
		}

		// if third party item, check for existing delivery item using latest
		// third party requirement (duplicates not allowed)
		if (form.getDeliveryType().equals(DeliveryType.THIRDPARTY)) {
			for (Integer jiId : form.getItems()) {
				JobItem jobItem = this.jiService.findJobItem(jiId);
				SortedSet<TPRequirement> tpRequirements = jobItem.getTpRequirements();
				if (!tpRequirements.isEmpty()) {
					// TPRequirementComparator orders newest-to-oldest, so
					// newest is first in set
					TPRequirement tpReq = tpRequirements.first();
					JobDeliveryItem deliveryItem = tpReq.getDeliveryItem();
					if (deliveryItem != null) {
						String[] params = new String[] { jobItem.getJob().getJobno(), String.valueOf(jobItem.getItemNo()), deliveryItem.getDelivery().getDeliveryno() };
						String defaultMessage = "Job item {0}.{1} must be deleted from existing delivery note {2} before a new delivery note can be created.";
						errors.rejectValue("", "error.deliverynote.createdelnot.itemalreadyexist", params, defaultMessage);
					}
				}
			}
		}

		// check if one of items selected is not ready for the delivery
		if (deliveryNoteService.itemSelectedIsNotReadyForDelivery(form.getDeliveryType(), form.getItems())) {
			errors.reject("error.deliverynote.createdelnot.itemnotready", "One of the selected items already exists in a DN, please refresh the page!");
		}

		boolean accessoriesChecked = true;
		for (Integer jiId : form.getItems()) {
			JobItem jobItem = jiService.findJobItem(jiId);
			for (ItemAccessory itemAccssery : jobItem.getAccessories()) {
				if (!form.getAccessoryItems().contains(itemAccssery.getId())) {
					accessoriesChecked = false;
					break;
				}
			}
			if (accessoriesChecked && !StringUtils.isEmpty(jobItem.getAccessoryFreeText()) && !form.getAccessoryFreeText().contains(jiId)) {
				accessoriesChecked = false;
			}
		}

		if (!accessoriesChecked) {
			errors.reject("error.deliverynote.createdelnot.missaccs", "Items with accessories must have their accessories sent with the item");
		}

		// check if the contact belongs to the subdivision of the main address
		if (contactId != null && form.getAddressid() != null) {
			Address mainAddress = addressService.get(form.getAddressid());

			if (mainAddress.getSub().getSubdivid() != Objects.requireNonNull(contact).getSub().getSubdivid()) {
				errors.reject("error.deliverynote.createdelnot.contactdoesntbelongtosubdivofaddress", new String[] {}, "The main contact doesn't belong to the subdivision of the main address");
			}

		}

	}
}
