package org.trescal.cwms.core.deliverynote.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.deliverynote.component.DeliveryNoteModel;
import org.trescal.cwms.core.deliverynote.component.DeliveryNoteModelGenerator;
import org.trescal.cwms.core.deliverynote.component.DeliveryNoteXlsxView;

@Controller
public class DeliveryNoteExcelController {
	
	@Autowired
	private DeliveryNoteModelGenerator modelGenerator;
	@Autowired
	private DeliveryNoteXlsxView xlsxView;
	
	/**
	 * Creates an Excel export (xlsx format) of all the data on a delivery note
	 * Download first for testing/acceptance, then could store file in Delivery 
	 */
	@RequestMapping(path="deliverynoteexcel.htm", method=RequestMethod.GET)
	public ModelAndView performExport(Locale locale,
			@RequestParam(name="deliveryid", required=true) Integer deliveryId) {
		
		DeliveryNoteModel model = this.modelGenerator.getModel(deliveryId, locale);
		Map<String,Object> modelMap = new HashMap<>();
		modelMap.put("model", model);
		
		return new ModelAndView(xlsxView, modelMap); 
	}
}
