package org.trescal.cwms.core.deliverynote.entity.courierdespatchtype;

import java.util.Comparator;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;

@Entity
@Table(name = "courierdespatchtype")
public class CourierDespatchType extends Auditable
{
	/**
	 * Inner comparator class for sorting CourierDespatchTypes
	 */
	public static class CourierDespatchTypeComparator implements Comparator<CourierDespatchType>
	{
		public int compare(CourierDespatchType cdt, CourierDespatchType anotherCdt)
		{
			Integer id1 = (cdt).getCdtid();
			Integer id2 = (anotherCdt).getCdtid();
			return id1.compareTo(id2);
		}
	}
	
	private int cdtid;
	private Courier courier;
	private boolean defaultForCourier;
	private String description;
	private Set<CourierDespatch> despatches;
	
	/**
	 * @return the cdtid
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cdtid", nullable = false, unique = true)
	@Type(type = "int")
	public int getCdtid()
	{
		return this.cdtid;
	}

	/**
	 * @return the courier
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "courierid")
	public Courier getCourier()
	{
		return this.courier;
	}
	
	/**
	 * @return the description
	 */
	@NotNull
	@Column(name = "description", nullable = false, length=255)
	public String getDescription()
	{
		return this.description;
	}

	/**
	 * @return the despatches
	 */
	@OneToMany(mappedBy = "cdtype", fetch = FetchType.LAZY)
	public Set<CourierDespatch> getDespatches()
	{
		return this.despatches;
	}

	@NotNull
	@Column(name = "defaultforcourier", nullable=false, columnDefinition="bit")
	public boolean isDefaultForCourier()
	{
		return this.defaultForCourier;
	}

	/**
	 * @param cdtid the cdtid to set
	 */
	public void setCdtid(int cdtid)
	{
		this.cdtid = cdtid;
	}

	/**
	 * @param courier the courier to set
	 */
	public void setCourier(Courier courier)
	{
		this.courier = courier;
	}

	public void setDefaultForCourier(boolean defaultForCourier)
	{
		this.defaultForCourier = defaultForCourier;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * @param despatches the despatches to set
	 */
	public void setDespatches(Set<CourierDespatch> despatches)
	{
		this.despatches = despatches;
	}
}