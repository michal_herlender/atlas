package org.trescal.cwms.core.deliverynote.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.deliverynote.dto.CourierDespatchTypeDTO;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courier.db.CourierService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.db.CourierDespatchTypeService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @JsonController
@SessionAttributes(value = { Constants.SESSION_ATTRIBUTE_COMPANY })
public class CourierController {
	
	private static final Logger logger = LoggerFactory.getLogger(CourierController.class);
	
	@Autowired
	private CourierService courierService;
	@Autowired
	private CourierDespatchTypeService courierDespatchTypeService;
	
	@RequestMapping(value="courierdespatchtypes.json", method=RequestMethod.GET)
	@ResponseBody
	public List<CourierDespatchTypeDTO> getCourierDespatchTypes(
			@RequestParam(name="courierId", required=true) Integer courierId) {
		logger.debug("courierdespatchtypes.json called");
		return courierService.get(courierId).getCdts().stream().map(cdt -> new CourierDespatchTypeDTO(cdt)).collect(Collectors.toList());
	}
	
	@RequestMapping(value="couriers.json", method=RequestMethod.GET)
	@ResponseBody
	public List<Courier> getCouriers(@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		return courierService.getCouriersByBusinessCompany(companyDto.getKey());
	}
	
	@RequestMapping(value="deletecourierdespatchtype.json")
	@ResponseBody
	public void deleteCourierDespatchType(
			@RequestParam(name="id", required=true) Integer id) {
		CourierDespatchType type = courierDespatchTypeService.get(id);
		courierDespatchTypeService.delete(type);
	}
}