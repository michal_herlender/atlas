package org.trescal.cwms.core.deliverynote.dto;

import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CourierDespatchTypeDTO {
	
	private Integer id;
	private String name;
	private Boolean defaultForCourier;
	private Integer courierid;
	private String courierName;
	
	public CourierDespatchTypeDTO(CourierDespatchType type) {
		this.id = type.getCdtid();
		this.name = type.getDescription();
		this.defaultForCourier = type.isDefaultForCourier();
	}

	public CourierDespatchTypeDTO(Integer id, String name, Integer courierid, String courierName) {
		super();
		this.id = id;
		this.name = name;
		this.courierid = courierid;
		this.courierName = courierName;
	}

}