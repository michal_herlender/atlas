package org.trescal.cwms.core.deliverynote.entity.courier.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;

public interface CourierDao extends BaseDao<Courier, Integer> {
	public List<Courier> getCouriersByBusinessCompany(Integer orgid);
}