package org.trescal.cwms.core.deliverynote.entity.deliveryitem.db;

import lombok.val;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery_;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem_;
import org.trescal.cwms.core.deliverynote.entity.generaldeliveryitem.GeneralDeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.generaldeliveryitem.GeneralDeliveryItem_;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration_;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess_;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink_;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate_;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement_;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnote.JobItemNote_;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement_;
import org.trescal.cwms.core.procedure.entity.procedure.Capability_;
import org.trescal.cwms.core.vdi.projection.PositionProjection;

import javax.persistence.criteria.*;
import java.util.Collection;
import java.util.List;

import static org.trescal.cwms.core.tools.StringJpaUtils.trimAndConcatWithWhitespace;

@Repository("DeliveryItemDao")
public class DeliveryItemDaoImpl extends BaseDaoImpl<DeliveryItem, Integer> implements DeliveryItemDao {

	@Override
	protected Class<DeliveryItem> getEntity() {
		return DeliveryItem.class;
	}

	@Override
	public void addItemToDelivery(JobDeliveryItem jobDeliveryItem) {
		this.persist(jobDeliveryItem);
	}

	@Override
	public DeliveryItem findDeliveryItem(int id, Class<? extends DeliveryItem> clazz) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(clazz);
			val root = cq.from(clazz);
			cq.where(cb.equal(root, id));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<DeliveryItemDTO> getDeliveryItemDTOs(Integer deliveryId, Collection<Integer> jobItemIds) {
		if (deliveryId == null && (jobItemIds == null || jobItemIds.isEmpty())) {
			throw new IllegalArgumentException("At least one of deliveryId or a non-empty jobItemIds must be provided");
		}

		return getResultList(cb -> {
			CriteriaQuery<DeliveryItemDTO> cq = cb.createQuery(DeliveryItemDTO.class);
			Root<JobDeliveryItem> root = cq.from(JobDeliveryItem.class);
			Join<JobDeliveryItem, JobItem> jobItem = root.join(JobDeliveryItem_.jobitem, JoinType.INNER);
			Join<JobDeliveryItem, Delivery> delivery = root.join(JobDeliveryItem_.delivery, JoinType.INNER);
			// Optional tp requirement
			Join<JobDeliveryItem, TPRequirement> tpRequirement = root.join(JobDeliveryItem_.tpRequirement,
					JoinType.LEFT);

			Predicate clauses = cb.conjunction();
			if (deliveryId != null)
				clauses.getExpressions().add(cb.equal(delivery.get(Delivery_.deliveryid), deliveryId));
			if (jobItemIds != null && !jobItemIds.isEmpty())
				clauses.getExpressions().add(jobItem.get(JobItem_.jobItemId).in(jobItemIds));

			cq.where(clauses);
			cq.select(cb.construct(DeliveryItemDTO.class, root.get(JobDeliveryItem_.delitemid),
					delivery.get(Delivery_.deliveryid), jobItem.get(JobItem_.jobItemId),
					tpRequirement.get(TPRequirement_.id), root.get(JobDeliveryItem_.accessoryFreeText)));

			return cq;
		});
	}

	@Override
	public List<GeneralDeliveryItem> getItemsForGeneralDelivery(int deliveryId) {
		return getResultList(cb -> {
			val cq = cb.createQuery(GeneralDeliveryItem.class);
			val general = cq.from(GeneralDeliveryItem.class);
			cq.where(cb.equal(general.get(GeneralDeliveryItem_.delivery), deliveryId));
			return cq;
		});
	}

	@Override
	public List<JobDeliveryItem> getItemsForJobDelivery(int deliveryId) {
		return getResultList(cb -> {
			val cq = cb.createQuery(JobDeliveryItem.class);
			val jdi = cq.from(JobDeliveryItem.class);
			cq.where(cb.equal(jdi.get(JobDeliveryItem_.delivery), deliveryId));
			val ji = jdi.fetch(JobDeliveryItem_.jobitem, JoinType.LEFT);
			val inst = ji.fetch(JobItem_.inst, JoinType.LEFT);
			inst.fetch(Instrument_.mfr, JoinType.LEFT);
			val model = inst.fetch(Instrument_.model, JoinType.LEFT);
			model.fetch(InstrumentModel_.mfr, JoinType.LEFT);
			model.fetch(InstrumentModel_.description, JoinType.LEFT);
			return cq;
		});
	}

	@Override
	public List<PositionProjection> getPositionProjectionForDeliveryNote(Integer deliveryId) {
		return getResultList(cb -> {
            val cq = cb.createQuery(PositionProjection.class);
            val deliveryItem = cq.from(JobDeliveryItem.class);
            val delivery = deliveryItem.join(JobDeliveryItem_.delivery);
            cq.where(cb.equal(deliveryItem.get(JobDeliveryItem_.delivery), deliveryId));
            val jobItem = deliveryItem.join(JobDeliveryItem_.jobitem);
            val inst = jobItem.join(JobItem_.inst);
            val lastCal = inst.join(Instrument_.lastCal, JoinType.LEFT);
            val mfr = inst.join(Instrument_.mfr, JoinType.LEFT);
            val model = inst.join(Instrument_.model);
            val modelName = joinTranslation(cb, model, InstrumentModel_.nameTranslations,
                LocaleContextHolder.getLocale());
            val proc = jobItem.join(JobItem_.workRequirements, JoinType.LEFT)
                .join(JobItemWorkRequirement_.workRequirement, JoinType.LEFT)
                .join(WorkRequirement_.capability, JoinType.LEFT);
            val cal = jobItem.join(JobItem_.calLinks, JoinType.LEFT).join(CalLink_.cal, JoinType.LEFT);
            val cert = cal.join(Calibration_.certs, JoinType.LEFT);
            val registeredBy = cert.join(Certificate_.registeredBy, JoinType.LEFT);
            cq.select(cb.construct(PositionProjection.class, delivery.get(Delivery_.deliveryid),
                inst.get(Instrument_.plantno), proc.get(Capability_.id), proc.get(Capability_.reqType),
                trimAndConcatWithWhitespace(proc.get(Capability_.name), proc.get(Capability_.description)).apply(cb),
                inst.get(Instrument_.plantid), inst.get(Instrument_.formerBarCode), inst.get(Instrument_.serialno),
                inst.get(Instrument_.modelname), cb.coalesce(inst.get(Instrument_.customerDescription), modelName),
                inst.get(Instrument_.calFrequency), inst.get(Instrument_.calFrequencyUnit),
                lastCal.get(Calibration_.calDate), inst.get(Instrument_.nextCalDueDate), mfr.get(Mfr_.name),
                delivery.get(Delivery_.deliverydate), cal.get(Calibration_.id), cert.get(Certificate_.certid),
                cert.get(Certificate_.certDate), cert.get(Certificate_.certno), cert.get(Certificate_.remarks),
                cal.get(Calibration_.calDate),
                cal.join(Calibration_.calProcess, JoinType.LEFT).get(CalibrationProcess_.description),
                jobItem.join(JobItem_.notes, JoinType.LEFT).get(JobItemNote_.note),
                trimAndConcatWithWhitespace(registeredBy.get(Contact_.firstName),
                    registeredBy.get(Contact_.lastName)).apply(cb),
                cert.get(Certificate_.calibrationVerificationStatus),
                cb.coalesce().value(cert.get(Certificate_.restriction)).value(false)));
			return cq;
		});
	}

	@Override
	public JobDeliveryItem findNotYetDespatchedJobDeliveryItemByInstrument(Integer plantId) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(JobDeliveryItem.class);
			val jdi = cq.from(JobDeliveryItem.class);
			val jobItemJoin = jdi.join(JobDeliveryItem_.jobitem);
			val instrumentJoin = jobItemJoin.join(JobItem_.inst);
			cq.where(cb.and(cb.isFalse(jdi.get(JobDeliveryItem_.despatched)),
					cb.equal(instrumentJoin.get(Instrument_.plantid), plantId)));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<DeliveryItem> getItemsForJobDeliveryBydDelNo(String deliveryNo) {
		return getResultList(cb -> {
			val cq = cb.createQuery(DeliveryItem.class);
			val di = cq.from(DeliveryItem.class);
			cq.where(cb.equal(di.get(DeliveryItem_.delivery).get(Delivery_.deliveryno), deliveryNo));
			return cq;
		});
	}

	

}