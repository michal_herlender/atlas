package org.trescal.cwms.core.deliverynote.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.db.DeliveryItemService;
import org.trescal.cwms.core.deliverynote.entity.generaldelivery.GeneralDelivery;
import org.trescal.cwms.core.deliverynote.entity.generaldeliveryitem.GeneralDeliveryItem;
import org.trescal.cwms.core.deliverynote.form.AddDeliveryItemForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;

@Controller @IntranetController
public class AddGeneralDeliveryItemController
{
	@Autowired
	private DeliveryItemService delItemServ;
	@Autowired
	private DeliveryService delServ;
	
	public static final String FORM_NAME = "form";

	@ModelAttribute(FORM_NAME)
	protected Object formBackingObject(HttpServletRequest req) throws Exception
	{
		AddDeliveryItemForm form = new AddDeliveryItemForm();
		GeneralDelivery delivery = (GeneralDelivery) this.delServ.get(ServletRequestUtils.getIntParameter(req, "delid"), GeneralDelivery.class);
		form.setDelivery(delivery);
		// set the different note types and preset comment types into the
		// session for use by dwr
		HttpSession sess = req.getSession();
		sess.setAttribute("generaldeliveryitemcomment", PresetCommentType.GENERAL_DELIVERY_ITEM);
		return form;
	}

	@RequestMapping(value="/addgenitems.htm", method=RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/deliverynote/addgenitems";
	}
	
	@RequestMapping(value="/addgenitems.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest req, @ModelAttribute(FORM_NAME) AddDeliveryItemForm form) throws Exception
	{
		GeneralDelivery d = (GeneralDelivery) form.getDelivery();

		int i = 0;
		GeneralDeliveryItem genItem;
		while (ServletRequestUtils.getStringParameter(req, "itemlist" + i) != null)
		{
			genItem = new GeneralDeliveryItem();
			genItem.setDelivery(d);
			genItem.setItemDescription(ServletRequestUtils.getStringParameter(req, "itemlist"
					+ i));

			this.delItemServ.insertDeliveryItem(genItem);

			i++;
		}

		return new ModelAndView(new RedirectView("gendelnotesummary.htm?delid="
				+ form.getDelivery().getDeliveryid()));
	}
}