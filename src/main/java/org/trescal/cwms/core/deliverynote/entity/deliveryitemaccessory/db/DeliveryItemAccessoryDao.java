package org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory.DeliveryItemAccessory;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemAccessoryDTO;

public interface DeliveryItemAccessoryDao extends BaseDao<DeliveryItemAccessory, Integer> {
	
	List<DeliveryItemAccessory> getAll(DeliveryItem deliveryItem);
	
	List<DeliveryItemAccessoryDTO> getProjectionDTOsForDeliveryId(Integer deliveryId, Locale locale);
}