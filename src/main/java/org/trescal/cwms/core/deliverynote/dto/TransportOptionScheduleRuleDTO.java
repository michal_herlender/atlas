package org.trescal.cwms.core.deliverynote.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.DayOfWeek;
import java.time.LocalDate;

@Getter
@Setter
public class TransportOptionScheduleRuleDTO {

    private Integer transportOptionId;
    private String transportOptionName;
    private DayOfWeek dayOfWeek;
    private LocalDate date;

}
