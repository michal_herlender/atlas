package org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.TransportOptionScheduleRule;

public interface TransportOptionScheduleRuleDao extends BaseDao<TransportOptionScheduleRule, Integer> {
	
}
