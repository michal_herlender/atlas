package org.trescal.cwms.core.deliverynote.entity.deliverynote.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.deliverydestination.DeliveryDestination;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.DeliveryNote;
import org.trescal.cwms.core.deliverynote.form.JobItemForDeliveryNoteDTO;

/**
 * Interface for accessing and manipulating {@link DeliveryNote} entities.
 */
public interface DeliveryNoteService extends BaseService<DeliveryNote, Integer> {

	List<JobItemForDeliveryNoteDTO> getJobItemsForDeliveryNote(Integer jobId, Integer jobItemId,
			DeliveryDestination destination, Integer subdivId);
	
	Boolean itemSelectedIsNotReadyForDelivery(DeliveryType deliveryType, List<Integer> itemIds);
}