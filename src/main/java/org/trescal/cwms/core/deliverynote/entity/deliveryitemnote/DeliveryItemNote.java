package org.trescal.cwms.core.deliverynote.entity.deliveryitemnote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;

@Entity
@Table(name = "deliveryitemnote")
public class DeliveryItemNote extends Note
{
	private DeliveryItem deliveryitem;
	
	/**
	 * @return the deliveryitem
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deliveryitemid")
	public DeliveryItem getDeliveryitem() {
		return deliveryitem;
	}
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.DELIVERYITEMNOTE;
	}
	
	/**
	 * @param deliveryitem the deliveryitem to set
	 */
	public void setDeliveryitem(DeliveryItem deliveryitem) {
		this.deliveryitem = deliveryitem;
	}
}