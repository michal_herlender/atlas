package org.trescal.cwms.core.deliverynote.entity.courier.db;

import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.dwr.ResultWrapper;

/**
 * Interface for accessing and manipulating {@link Courier} entities.
 * 
 * @author jamiev
 */
public interface CourierService
{
	/**
	 * Updates a {@link Courier} entity's details with those given as
	 * parameters.
	 * 
	 * @param courierid the ID of the {@link Courier} to edit.
	 * @param name the new value of the name field.
	 * @param URL the new value of the webTrackURL field.
	 * @return {@link ResultWrapper}.
	 */
	ResultWrapper editCourier(int courierid, String name, String URL);

	/**
	 * Returns the {@link Courier} entity with the given ID.
	 * 
	 * @param id the {@link Courier} ID.
	 * @return the {@link Courier}
	 */
	Courier get(int id);

	/**
	 * Returns all {@link Courier}(s) stored in the database.
	 * 
	 * @return the {@link List} of {@link Courier}(s).
	 */
	List<Courier> getAllCouriers();

	/**
	 * Returns all {@link CourierDespatchType}(s) corresponding to the
	 * {@link Courier} entity with the given ID.
	 * 
	 * @param courierid the ID of the {@link Courier} to list
	 *        {@link CourierDespatchType}(s) for.
	 * @return the {@link Set} of {@link CourierDespatchType}(s).
	 */
	Set<CourierDespatchType> getAllDespatchTypes(int courierid);

	/**
	 * Inserts the given {@link Courier} into the database.
	 * 
	 * @param courier the {@link Courier} to insert.
	 */
	void insertCourier(Courier courier);

	/**
	 * Updates the given {@link Courier} in the database.
	 * 
	 * @param courier the {@link Courier} to update.
	 */
	void updateCourier(Courier courier);
	
	List<Courier> getCouriersByBusinessCompany(Integer orgid);
	
	void updateDefaultCompanyCourier(Integer courierid, Integer coid);
	
	void deleteCourier(Integer courierid);
	
	Long linkedEntitiesCount(Integer courierId);
	
	int addNewCourier(String courierName, String webTrackUrl, Integer businessCoid);
}