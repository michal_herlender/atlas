package org.trescal.cwms.core.deliverynote.entity.transportmethod.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod;

@Service("TransportMethodService")
public class TransportMethodServiceImpl implements TransportMethodService
{
	@Autowired
	TransportMethodDao transportMethodDao;

	public TransportMethod findTransportMethod(int id)
	{
		return transportMethodDao.find(id);
	}

	public void insertTransportMethod(TransportMethod TransportMethod)
	{
		transportMethodDao.persist(TransportMethod);
	}

	public void updateTransportMethod(TransportMethod TransportMethod)
	{
		transportMethodDao.update(TransportMethod);
	}

	public List<TransportMethod> getAllTransportMethods()
	{
		return transportMethodDao.findAll();
	}

	public void setTransportMethodDao(TransportMethodDao transportMethodDao)
	{
		this.transportMethodDao = transportMethodDao;
	}

	public void deleteTransportMethod(TransportMethod transportmethod)
	{
		this.transportMethodDao.remove(transportmethod);
	}

	public void saveOrUpdateTransportMethod(TransportMethod transportmethod)
	{
		this.transportMethodDao.saveOrUpdate(transportmethod);
	}
}