package org.trescal.cwms.core.deliverynote.entity.deliverynote.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.deliverydestination.DeliveryDestination;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.DeliveryNote;
import org.trescal.cwms.core.deliverynote.form.JobItemForDeliveryNoteDTO;
import org.trescal.cwms.core.deliverynote.form.TPInstructionDTO;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.dto.ItemAccessoryDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.db.ItemAccessoryService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.db.JobItemPOService;
import org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction.db.TPInstructionService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DeliveryNoteServiceImpl extends BaseServiceImpl<DeliveryNote, Integer> implements DeliveryNoteService {

	@Autowired
	private DeliveryNoteDao deliveryNoteDao;
	@Autowired
	private ItemAccessoryService itemAccessoryService;
	@Autowired
	private TPInstructionService tpInstructionService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private JobService jobService;
	@Autowired
	private JobItemPOService jiPOService;

	@Override
	protected BaseDao<DeliveryNote, Integer> getBaseDao() {
		return deliveryNoteDao;
	}

	@Override
	public List<JobItemForDeliveryNoteDTO> getJobItemsForDeliveryNote(Integer jobId, Integer jobItemId,
			DeliveryDestination destination, Integer businessSubdivId) {
		StateGroup stateGroup;
		switch (destination) {
		case CLIENT:
			stateGroup = StateGroup.CLIENTDELIVERY;
			break;
		case INTERNAL:
			stateGroup = StateGroup.INTERNALDELIVERY;
			break;
		case INTERNAL_RETURN :
			stateGroup = StateGroup.INTERNAL_RETURN_DELIVERY;
			break;
		case THIRDPARTY:
			stateGroup = StateGroup.TPDELIVERY;
			break;
		default:
			throw new IllegalArgumentException("Invalid destination specified");
		}
		Locale locale = LocaleContextHolder.getLocale();

		List<JobItemForDeliveryNoteDTO> jobItemsForDeliveryNote = this.deliveryNoteDao
			.getJobItemsFromCurrentJobForDeliveryNote(jobId, stateGroup, businessSubdivId, locale).stream()
			.filter(ji -> ji.getSubdivId().equals(businessSubdivId)).collect(Collectors.toList());

		Map<Integer, List<ItemAccessoryDTO>> accessories = this.itemAccessoryService.getAllByJob(jobId, locale).stream()
			.collect(Collectors.groupingBy(ItemAccessoryDTO::getJobItemId));

		Map<Integer, List<TPInstructionDTO>> tpInstructions = this.tpInstructionService.getAllByJob(jobId).stream()
			.collect(Collectors.groupingBy(TPInstructionDTO::getJobItemId));

		// set PO/BPO for each job item if exist
		jobItemsForDeliveryNote
			.forEach(ji -> ji.setJobItemPOs(jiPOService.getAllPOsByJobItem(ji.getJobItemId())));

		if (destination.equals(DeliveryDestination.CLIENT)) {
			List<JobItemForDeliveryNoteDTO> itemsFromAnotherJobs = this.deliveryNoteDao
				.getJobItemsFromOtherJobsForDeliveryNote(subdivService.get(businessSubdivId), stateGroup,
					jobService.get(jobId), locale);
			itemsFromAnotherJobs.forEach(ji -> {
				jobItemsForDeliveryNote.add(ji);
				List<ItemAccessoryDTO> itemAccessoryOtherJob = this.itemAccessoryService
					.getAllByJob(ji.getJobId(), locale).stream()
						.filter(itemAccessory -> itemAccessory.getJobItemId() == ji.getJobItemId())
						.collect(Collectors.toList());
				accessories.put(ji.getJobItemId(), itemAccessoryOtherJob);
			});
		} else if (destination.equals(DeliveryDestination.INTERNAL)) {
			Integer nextSubdivId = jiServ.findJobItem(jobItemId).getNextSubdiv().getSubdivid();
			jobItemsForDeliveryNote.removeIf(ji -> !nextSubdivId.equals(ji.getNextSubdivId()));
			List<JobItemForDeliveryNoteDTO> itemsFromAnotherJobs = this.deliveryNoteDao
					.getJobItemsFromOtherJobsForDeliveryNote(subdivService.get(businessSubdivId), stateGroup,
							jobService.get(jobId), locale)
					.stream().filter(ji -> nextSubdivId.equals(ji.getNextSubdivId())).collect(Collectors.toList());
			itemsFromAnotherJobs.forEach(ji -> {
				jobItemsForDeliveryNote.add(ji);
				List<ItemAccessoryDTO> itemAccessoryOtherJob = this.itemAccessoryService
						.getAllByJob(ji.getJobId(), locale).stream()
						.filter(itemAccessory -> itemAccessory.getJobItemId() == ji.getJobItemId())
						.collect(Collectors.toList());
				accessories.put(ji.getJobItemId(), itemAccessoryOtherJob);
			});
		} else if (destination.equals(DeliveryDestination.INTERNAL_RETURN)){
			Integer nextSubdivId = jiServ.findJobItem(jobItemId).getReturnOption().getSub().getSubdivid();
			jobItemsForDeliveryNote.removeIf(ji -> !nextSubdivId.equals(ji.getNextSubdivIdFromReturnOption()));
			List<JobItemForDeliveryNoteDTO> itemsFromAnotherJobs = this.deliveryNoteDao
				.getJobItemsFromOtherJobsForDeliveryNote(subdivService.get(businessSubdivId), stateGroup,
					jobService.get(jobId), locale)
				.stream().filter(ji -> nextSubdivId.equals(ji.getNextSubdivIdFromReturnOption())).collect(Collectors.toList());
			itemsFromAnotherJobs.forEach(ji -> {
				jobItemsForDeliveryNote.add(ji);
				List<ItemAccessoryDTO> itemAccessoryOtherJob = this.itemAccessoryService
					.getAllByJob(ji.getJobId(), locale).stream().filter(itemAccessory -> itemAccessory.getJobItemId() == ji.getJobItemId())
					.collect(Collectors.toList());
				accessories.put(ji.getJobItemId(), itemAccessoryOtherJob);
			});
		} else {
			List<JobItemForDeliveryNoteDTO> itemsFromAnotherJob = this.deliveryNoteDao
				.getJobItemsFromOtherJobsForDeliveryNote(subdivService.get(businessSubdivId), stateGroup,
					jobService.get(jobId), locale);
			itemsFromAnotherJob.forEach(ji -> {
				jobItemsForDeliveryNote.add(ji);
				List<ItemAccessoryDTO> itemAccessoryOtherJob = this.itemAccessoryService
					.getAllByJob(ji.getJobId(), locale).stream()
					.filter(jobitem -> jobitem.getJobItemId() == ji.getJobItemId()).collect(Collectors.toList());
				List<TPInstructionDTO> tpInstructionOtherJob = this.tpInstructionService.getAllByJob(ji.getJobId())
					.stream().filter(jobitem -> jobitem.getJobItemId() == ji.getJobItemId())
						.collect(Collectors.toList());
				accessories.put(ji.getJobItemId(), itemAccessoryOtherJob);
				tpInstructions.put(ji.getJobItemId(), tpInstructionOtherJob);
			});
		}
		jobItemsForDeliveryNote.forEach(ji -> {
			ji.setItemAccessories(accessories.get(ji.getJobItemId()));
			ji.setTpInstructions(tpInstructions.get(ji.getJobItemId()));
			// use the index to have a default sort order where the
			// items for the selected job are always at the top of the table,
			// with the job items secondarily sorted (within each job) by item
			// number
			if (ji.getJobId() == jobId) {
				ji.setIndex(0);
			} else {
				ji.setIndex(ji.getItemNo());
			}
		});
		return jobItemsForDeliveryNote;
	}
	
	@Override
	public Boolean itemSelectedIsNotReadyForDelivery(DeliveryType deliveryType, List<Integer> itemIds){
		StateGroup stateGroup;
		switch (deliveryType) {
		case CLIENT:
			stateGroup = StateGroup.CLIENTDELIVERY;
			break;
		case INTERNAL:
			stateGroup = StateGroup.INTERNALDELIVERY;
			break;
		case INTERNAL_RETURN :
			stateGroup = StateGroup.INTERNAL_RETURN_DELIVERY;
			break;
		case THIRDPARTY:
			stateGroup = StateGroup.TPDELIVERY;
			break;
		default:
			throw new IllegalArgumentException("Invalid destination specified");
		}
		return this.deliveryNoteDao.itemSelectedIsNotReadyForDelivery(stateGroup, itemIds);
	}
}