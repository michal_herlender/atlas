package org.trescal.cwms.core.deliverynote.projection;

import java.util.List;

import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirementDTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DeliveryItemProjectionResult {
	private List<DeliveryItemDTO> deliveryItems;
	// Optional. loaded if requested
	private List<TPRequirementDTO> tpRequirements;
	private List<DeliveryItemAccessoryDTO> itemAccessories;
}
