package org.trescal.cwms.core.deliverynote.projection.service;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.projection.DeliveryProjectionCommand;

public interface DeliveryProjectionService {
	List<DeliveryDTO> getDeliveryDtosForIds(Collection<Integer> deliveryIds, DeliveryProjectionCommand dpCommand);
	
	List<DeliveryDTO> getDtosByDeliveryItems(Collection<DeliveryItemDTO> diDtos, DeliveryProjectionCommand dpCommand);
}
