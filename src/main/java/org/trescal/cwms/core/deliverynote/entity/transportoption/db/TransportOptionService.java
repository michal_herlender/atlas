package org.trescal.cwms.core.deliverynote.entity.transportoption.db;

import org.trescal.cwms.core.admin.form.AddTransportOptionForm;
import org.trescal.cwms.core.admin.form.TransportOptionForm;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.tools.transitcalendar.ClientTransit;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import java.time.LocalDate;
import java.util.*;

public interface TransportOptionService extends BaseService<TransportOption, Integer> {
    void editTransportOptions(TransportOptionForm form);

    SortedSet<KeyValueIntegerString> getTransportOptionDtosForSubdivs(Boolean extendedFormat, Boolean active, Locale locale, Collection<Integer> subdivIds);

    SortedSet<KeyValueIntegerString> getTransportOptionDtosForIds(Boolean extendedFormat, Collection<Integer> ids, Locale locale);

    /**
     * Returns a {@link List} of {@link TransportOption}s that have a specific
     * day of the week. i.e. all round robins.
     *
     * @return the {@link List} of {@link TransportOption}s
     */
    List<TransportOption> getAllDaySpecificTransportOptions(int fromSubdivId);

    List<TransportOption> getTransportOptionsFromSubdiv(int fromSubdivId);

    List<ClientTransit> listClientTransits(int fromSubdivId, int weeks);

    TransportOption getTransportOptionIn(Address address, Subdiv allocatedSubdiv);
	
	TransportOption getTransportOptionOut(Address address, Subdiv allocatedSubdiv);
	
	TransportOption getEager(int id);
	
	List<TransportOption> transportOptionsFromSubdivWithActiveMethod(int fromSubdivId);
	
	List<TransportOption> getTransportOptionsSchedulable(Integer fromSubdivId);
	
	/**
	 * return false if the localized name is duplicated
	 */
	Boolean checkLocalizedNameUniqueness(Integer subdivid, String localizedName, Integer methodId);
	
	/**
	 * Returns a {@link TransportOption} that have 
	 * created using a AddTransportOptionForm  
	 * 
	 * @return the {@link TransportOption}
     */
    TransportOption addTransportOption(AddTransportOptionForm form, String username);

    /**
     * Returns a {@link List} of {@link TransportOption}s that have a specific
     * localized name, a specific transport method
     *
     * @return the {@link List} of {@link TransportOption}s
     */
    List<TransportOption> transportOptionByLocalizedName(Integer subdivid, String localizedName, Integer methodId);

    /**
     * Returns the next available date for a transport option
     * if scheduleDate is null, we get next available date
     * basing on the date of today
     *
     * @return
     */
    LocalDate getNextAvailable(Integer transportOptionId, LocalDate scheduleDate);

    /**
     * Returns a {@link List} of {@link Date}s that available for the
     * n next weeks
     *
     * @return
     */

    List<LocalDate> availableDateForNWeeks(TransportOption transportOption, int weeksNumber);
    
    Integer getTransportOptionByJobItemId(Integer jobitemId);
    
    Integer transportOptionSchedulableFromJobItemIds(List<Integer> jobitemIds);
    
    void automaticallyAddDeliveryNoteToTransportSchedule(Integer businessSubdivId, Delivery delivery, 
    		String username, List<Integer> items);
	
	
}