package org.trescal.cwms.core.deliverynote.entity.transportoption;

import java.util.Comparator;

/**
 * Inner comparator class for sorting {@link TransportOption} when returning
 * collections
 * 
 * @author jamiev
 */
public class TransportOptionComparator implements Comparator<TransportOption>
{
	public int compare(TransportOption opt1, TransportOption opt2)
	{
		// opt1 = Client courier
		// opt2 = Client
		if ((opt1.getSub() == null) && (opt2.getSub() == null))
		{
			return opt1.getMethod().getMethodText().compareTo(opt2.getMethod().getMethodText());
		}
		// opt1 = Client courier
		// opt2 = Yarmouth - Round Robin (Mon)
		else if ((opt1.getSub() == null) && (opt2.getSub() != null))
		{
			return -1;
		}
		// opt1 = Yarmouth - Round Robin (Mon)
		// opt2 = Client courier
		else if ((opt1.getSub() != null) && (opt2.getSub() == null))
		{
			return 1;
		}
		else
		{
			// opt1 = Yarmouth - Round Robin (Mon)
			// opt2 = Kettering - Round Robin (Mon)
			if (opt1.getSub().getSubdivid() != opt2.getSub().getSubdivid())
			{
				// opt1 = Yarmouth - Round Robin (Mon)
				// opt2 = Kettering - Round Robin (Mon)
				if (!opt1.getSub().getSubname().equals(opt2.getSub().getSubname()))
				{
					return opt1.getSub().getSubname().compareTo(opt2.getSub().getSubname());
				}
				// opt1 = Un-named Subdivision - Round Robin (Mon)
				// opt2 = Un-named Subdivision - Round Robin (Mon)
				else
				{
					Integer one = opt1.getSub().getSubdivid();
					Integer two = opt2.getSub().getSubdivid();
					return one.compareTo(two);
				}
			}
			// opt1 = Yarmouth - Round Robin (Mon)
			// opt2 = Yarmouth - Round Robin (Tue)
			else
			{
				// opt1 = Yarmouth - Round Robin (Mon)
				// opt2 = Yarmouth - Round Robin (Tue)
				if ((opt1.getDayOfWeek() != null)
						&& (opt2.getDayOfWeek() != null))
				{
					return opt1.getDayOfWeek().compareTo(opt2.getDayOfWeek());
				}
				// opt1 = Yarmouth - Local
				// opt2 = Yarmouth - Round Robin (Tue)
				else if ((opt1.getDayOfWeek() == null)
						&& (opt2.getDayOfWeek() != null))
				{
					return 1;
				}
				// opt1 = Yarmouth - Round Robin (Mon)
				// opt2 = Yarmouth - Local
				else if ((opt1.getDayOfWeek() != null)
						&& (opt2.getDayOfWeek() == null))
				{
					return -1;
				}
				// opt1 = Yarmouth - Local
				// opt2 = Yarmouth - Client
				else
				{
					// opt1 = Yarmouth - Local
					// opt2 = Yarmouth - Client
					if (!opt1.getMethod().getMethodText().equals(opt2.getMethod().getMethodText()))
					{
						return opt1.getMethod().getMethodText().compareTo(opt2.getMethod().getMethodText());
					}
					// opt1 = Yarmouth - Local
					// opt2 = Yarmouth - Local
					else
					{
						// FALLBACK IN CASE ANY DUPLICATIONS SNUCK INTO THE DB
						return ((Integer) opt1.getId()).compareTo(opt2.getId());
					}
				}
			}
		}
	}
}