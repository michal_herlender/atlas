package org.trescal.cwms.core.deliverynote.entity.deliverynote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;

@Entity
@Table(name = "deliverynote")
public class DeliveryNote extends Note implements Comparable<DeliveryNote>
{
	private Delivery delivery;
	
	/**
	 * @return the delivery
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deliveryid")
	public Delivery getDelivery() {
		return delivery;
	}
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.DELIVERYNOTE;
	}
	
	/**
	 * @param delivery the delivery to set
	 */
	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}
	
	@Override
	public int compareTo(DeliveryNote other) {
		return getNoteid() - other.getNoteid();
	}
}