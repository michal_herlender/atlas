package org.trescal.cwms.core.deliverynote.form;

import java.util.HashMap;
import java.util.List;

import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

import lombok.Data;

@Data
public class AddDeliveryItemForm
{
	private HashMap<Integer, String> accessories;
	private boolean allowsPartDeliveries;
	private Delivery delivery;
	private boolean generalDelivery;
	private HashMap<Integer, String> items;
	private List<JobItem> itemsToAdd;
	private Job job;
	private List<Integer> ready;
	private List<JobItem> remainingJobItems;
	
}