package org.trescal.cwms.core.deliverynote.entity.generaldeliveryitem;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;

@Entity
@DiscriminatorValue("general")
public class GeneralDeliveryItem extends DeliveryItem
{
	private String itemDescription;

	public GeneralDeliveryItem()
	{
		super();
	}

	/**
	 * @return the itemDescription
	 */
	@Length(max = 1000)
	@Column(name = "itemdescription")
	public String getItemDescription()
	{
		return this.itemDescription;
	}

	/**
	 * @param itemDescription the itemDescription to set
	 */
	public void setItemDescription(String itemDescription)
	{
		this.itemDescription = itemDescription;
	}
}