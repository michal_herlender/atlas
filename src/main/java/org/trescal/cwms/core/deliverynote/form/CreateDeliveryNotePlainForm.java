package org.trescal.cwms.core.deliverynote.form;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;

import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Data
public class CreateDeliveryNotePlainForm {

	private Integer contactid;
	private Integer addressid;
	@Size(min = 1)
	private List<Integer> items;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate deliverydate;
	private Integer despatchId;
	private Integer coid;
	private String coname;
	private Integer subdivid;
	private DeliveryType deliveryType;
	private Integer jobId;
	private Integer personid;
	private Integer addrid;
	private int locid;
	private boolean tp;
	private List<Integer> accessoryItems;
	private String selectant;
	private LocalDate despatchDate;
	private Integer selectType;
	private String consignmentno;
	private Integer selectdespatch;
	private List<Integer> accessoryFreeText;
	private boolean allowsPartDeliveries;
	private String delInstruction;
}