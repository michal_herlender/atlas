package org.trescal.cwms.core.deliverynote.controller;

import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.form.CreateDeliveryNoteForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateTimeEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY})
public class EditReceiptDateController {
    @Autowired
    private EditReceiptDateFormValidator validator;
    @Autowired
    private DeliveryService delServ;

    @InitBinder("form")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
        binder.registerCustomEditor(LocalDateTime.class, new CustomLocalDateTimeEditor(DateTimeFormatter.ISO_DATE_TIME));
    }

    @ModelAttribute("form")
    protected EditReceiptDateForm formBackingObject(
        @RequestParam(value = "delid", required = false, defaultValue = "0") Integer delId
    ) {
        Delivery delivery = this.delServ.get(delId);

        return EditReceiptDateForm.builder().delivery(delivery).build();
    }

    @PostMapping("editreceiptdate.htm")
    String onFormSubmit(Model model, RedirectAttributes redirectAttributes,
                        @Valid @ModelAttribute("form") EditReceiptDateForm form,
                        @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
                        BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            CreateDeliveryNoteForm formObj = new CreateDeliveryNoteForm();

            redirectAttributes.addFlashAttribute("model", model);
            redirectAttributes.addFlashAttribute("form", formObj);
            redirectAttributes.addFlashAttribute("model", companyDto);
            redirectAttributes.addAttribute("delid", form.getDelivery().getDeliveryid());
            return "redirect:viewdelnote.htm";
        }
        delServ.updateDestinationReceiptDate(form.delivery, DateTools.localDateTimeToZonedDateTime(form.getDestinationReceiptDate()), companyDto.getKey());
        redirectAttributes.addAttribute("delid", form.getDelivery().getDeliveryid());
        redirectAttributes.addAttribute("loadtab", "editdestinationreceiptdate-tab");
        return "redirect:viewdelnote.htm";
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    static class EditReceiptDateForm {
        Delivery delivery;
        LocalDateTime destinationReceiptDate;
    }

}

@Component
class EditReceiptDateFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(EditReceiptDateController.EditReceiptDateForm.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        val receiptDate = ((EditReceiptDateController.EditReceiptDateForm) target).getDestinationReceiptDate();
        if (receiptDate == null)
            errors.rejectValue("form.destinationreceiptdate", "error.delivery.destinationreceiptdate", "Please enter both a date AND a time of receipt date");
    }
}
