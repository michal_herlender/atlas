package org.trescal.cwms.core.deliverynote.entity.transportoption;

import lombok.Setter;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod;
import org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.TransportOptionScheduleRule;
import org.trescal.cwms.core.tools.DateTools;

import javax.persistence.*;
import java.util.List;

@Entity
@Setter
@Table(name = "transportoption")
public class TransportOption extends Auditable {
    private boolean active;
    private Integer dayOfWeek;
    private int id;
    private String localizedName;
    private String externalRef;
    private TransportMethod method;
    private Subdiv sub;
    private List<TransportOptionScheduleRule> scheduleRules;

    @Transient
    public String getCompleteTransportOption() {
        return this.toString();
    }

    @Column(name = "dayofweek")
    public Integer getDayOfWeek() {
        return this.dayOfWeek;
    }

    @Transient
    public String getDayText() {
        String text = null;

        if (this.dayOfWeek != null) {
            text = DateTools.getDayInShortWords(this.dayOfWeek);
        }

        return text;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    @Column(name = "localizedname")
    public String getLocalizedName() {
        return localizedName;
    }

    @Column(name = "externalref", length = 10)
    public String getExternalRef() {
        return externalRef;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transportmethodid")
    public TransportMethod getMethod() {
        return this.method;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subdivid")
    public Subdiv getSub() {
        return this.sub;
    }

    @Column(name = "active", nullable = false, columnDefinition = "tinyint")
    public boolean isActive() {
        return this.active;
	}

	@OneToMany(mappedBy = "transportOption", fetch = FetchType.LAZY)
	public List<TransportOptionScheduleRule> getScheduleRules() {
		return this.scheduleRules;
	}

	@Transient
	public String toShortString() {
		String text = this.method.getMethodText();

		if (this.getDayText() != null) {
			text = text.concat(" (" + this.getDayText() + ")");
		}

		return text;
	}

	@Transient
	@Override
	public String toString() {
		String text = this.method.getMethodText();
		if (this.sub != null) {
			text = text.concat(" - " + this.sub.getSubdivCode());
		}
		if (this.getDayText() != null) {
			text = text.concat(" (" + this.getDayText() + ")");
		}
		return text;
	}
}