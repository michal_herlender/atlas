package org.trescal.cwms.core.deliverynote.entity.transportmethod;

import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.system.entity.translation.Translation;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Locale;
import java.util.Set;

@Entity
@Setter
@Table(name = "transportmethod")
public class TransportMethod extends Auditable
{
	private int id;
	private String method;
	private boolean optionPerDayOfWeek;
	private Set<Translation> methodTranslation;
	private Boolean active;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@Length(min = 1, max = 50)
	@Column(name = "method", nullable = false, length = 50)
	@Deprecated
	public String getMethod()
	{
		return this.method;
	}
	
	/**
	 * Translated method text (using LocaleContextHolder)
	 * @return String
	 */
	@Transient
	public String getMethodText()
	{
		Locale loc = LocaleContextHolder.getLocale();
		for (Translation t: methodTranslation) {
			if (t.getLocale().getLanguage().equals(loc.getLanguage())) return t.getTranslation(); }
		for (Translation t: methodTranslation) {
			if (t.getLocale().getLanguage().equals("en")) return t.getTranslation(); }
		return getMethod();
	}

	@Column(name = "optionperday", nullable = false, columnDefinition="tinyint")
	public boolean isOptionPerDayOfWeek()
	{
		return this.optionPerDayOfWeek;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="transportmethodtranslation", joinColumns=@JoinColumn(name="id"))
	public Set<Translation> getMethodTranslation() {
		return methodTranslation;
	}

	@NotNull
	@Column(name = "active", nullable = false, columnDefinition="tinyint")
	public Boolean getActive() {
		return active;
	}
	
}