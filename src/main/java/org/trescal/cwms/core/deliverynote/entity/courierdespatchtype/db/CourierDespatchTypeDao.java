package org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.deliverynote.dto.CourierDespatchTypeDTO;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;

public interface CourierDespatchTypeDao extends BaseDao<CourierDespatchType, Integer> {
	
	List<CourierDespatchTypeDTO> getAllSorted();
}