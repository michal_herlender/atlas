package org.trescal.cwms.core.deliverynote.entity.delivery;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.SortNatural;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.clientdelivery.ClientDelivery;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.DeliveryNote;
import org.trescal.cwms.core.deliverynote.entity.freehandcontact.FreehandContact;
import org.trescal.cwms.core.deliverynote.entity.internaldelivery.InternalDelivery;
import org.trescal.cwms.core.deliverynote.entity.internalreturndelivery.InternalReturnDelivery;
import org.trescal.cwms.core.deliverynote.entity.thirdpartydelivery.ThirdPartyDelivery;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

import javax.persistence.*;
import java.io.File;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "delivery")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "deliverytype", discriminatorType = DiscriminatorType.STRING)
public abstract class Delivery extends Allocated<Subdiv> implements NoteAwareEntity, ComponentEntity {
    private Address address;
    private Contact contact;
    private CourierDespatch crdes;
    private Contact createdby;
    private LocalDate creationdate;
    private LocalDate deliverydate;
    private int deliveryid;
	private String deliveryno;
	private File directory;
	private FreehandContact freehandContact;
	private Location location;
	private Set<DeliveryNote> notes;
	private Schedule schedule;
	private List<Email> sentEmails;
	private boolean signatureCaptured;
	private Date signedOn;
	private String signeeName;
	private ZonedDateTime destinationReceiptDate;

	/**
	 * @return the address
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "addressid")
	public Address getAddress() {
		return this.address;
	}

	/**
	 * @return the contact
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contactid")
	public Contact getContact()
	{
		return this.contact;
	}

	/**
	 * @return the crdes
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "courierdespatchid")
	public CourierDespatch getCrdes()
	{
		return this.crdes;
	}

	/**
	 * @return the createdby
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "createdbyid")
	public Contact getCreatedby()
	{
		return this.createdby;
	}

    /**
     * @return the creationdate
     */
    @Column(name = "creationdate", columnDefinition = "date")
    public LocalDate getCreationdate() {
        return this.creationdate;
    }

	@Override
	@Transient
	public Contact getDefaultContact()
	{
		return this.contact;
	}

    /**
     * @return the deliverydate
     */
    @Column(name = "deliverydate", columnDefinition = "date")
    public LocalDate getDeliverydate() {
        return this.deliverydate;
    }

	/**
	 * @return the deliveryid
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "deliveryid", nullable = false, unique = true)
	@Type(type = "int")
	public int getDeliveryid()
	{
		return this.deliveryid;
	}

	/**
	 * @return the deliveryno
	 */
	@Length(max = 30)
	@Column(name = "deliveryno", length = 30)
	public String getDeliveryno()
	{
		return this.deliveryno;
	}

	@Override
	@Transient
	public File getDirectory()
	{
		return this.directory;
	}

	/**
	 * @deprecated no longer supported on new/edited delivery notes, for display only where already set
	 */
	@Deprecated
	@Cascade(CascadeType.ALL)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "freehandid")
	public FreehandContact getFreehandContact()
	{
		return this.freehandContact;
	}

	@Override
	@Transient
	public String getIdentifier()
	{
		return this.deliveryno;
	}

	@Transient
	public abstract Set<? extends DeliveryItem> getItems();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "locationid")
	public Location getLocation()
	{
		return this.location;
	}

	/**
	 * @return the delnotes
	 */
	@Override
	@Cascade(CascadeType.ALL)
	@OneToMany(mappedBy = "delivery", fetch = FetchType.LAZY)
	@SortNatural
	public Set<DeliveryNote> getNotes()
	{
		return this.notes;
	}

	@Override
	@Transient
	public abstract ComponentEntity getParentEntity();

	@Transient
	public Integer getPrivateActiveNoteCount()
	{
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount()
	{
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount()
	{
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount()
	{
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}

	/**
	 * @return the schedule
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scheduleid")
	public Schedule getSchedule()
	{
		return this.schedule;
	}

	@Override
	@Transient
	public List<Email> getSentEmails()
	{
		return this.sentEmails;
	}

	@Column(name = "signedon", columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSignedOn()
	{
		return this.signedOn;
	}

	@Length(max = 60)
	@Column(name = "signeename", length = 60)
	public String getSigneeName()
	{
		return this.signeeName;
	}

	@Transient
	public abstract DeliveryType getType();
	
	@Transient 
	public Boolean isThirdPartyDelivery()
	{
		return (this instanceof ThirdPartyDelivery);
	}
	
	@Transient 
	public Boolean isClientDelivery()
	{
		return (this instanceof ClientDelivery);
	}
	
	@Transient 
	public Boolean isInternalDelivery()
	{
		return (this instanceof InternalDelivery);
	}
	
	@Transient 
	public Boolean isInternalReturnDelivery()
	{
		return (this instanceof InternalReturnDelivery);
	}
	
	@Override
	@Transient
	public boolean isAccountsEmail()
	{
		return false;
	}

	@Transient
	public abstract boolean isGeneralDelivery();

	@Column(name = "sigcaptured", columnDefinition="bit")
	public boolean isSignatureCaptured()
	{
		return this.signatureCaptured;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(Address address)
	{
		this.address = address;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	/**
	 * @param crdes the crdes to set
	 */
	public void setCrdes(CourierDespatch crdes)
	{
		this.crdes = crdes;
	}

	/**
	 * @param createdby the createdby to set
	 */
	public void setCreatedby(Contact createdby)
	{
		this.createdby = createdby;
    }

    /**
     * @param creationdate the creationdate to set
     */
    public void setCreationdate(LocalDate creationdate) {
        this.creationdate = creationdate;
	}

	/**
	 * @param deliverydate the deliverydate to set
	 */
	public void setDeliverydate(LocalDate deliverydate)
	{
		this.deliverydate = deliverydate;
	}

	/**
	 * @param deliveryid the deliveryid to set
	 */
	public void setDeliveryid(int deliveryid)
	{
		this.deliveryid = deliveryid;
	}

	/**
	 * @param deliveryno the deliveryno to set
	 */
	public void setDeliveryno(String deliveryno)
	{
		this.deliveryno = deliveryno;
	}

	@Override
	public void setDirectory(File file)
	{
		this.directory = file;
	}

	/**
	 * @deprecated no longer supported on new/edited delivery notes, for display only where already set
	 */
	@Deprecated
	public void setFreehandContact(FreehandContact freehandContact)
	{
		this.freehandContact = freehandContact;
	}

	public void setLocation(Location location)
	{
		this.location = location;
	}

	/**
	 * @param delnotes the delnotes to set
	 */
	public void setNotes(Set<DeliveryNote> delnotes)
	{
		this.notes = delnotes;
	}

	/**
	 * @param schedule the schedule to set
	 */
	public void setSchedule(Schedule schedule)
	{
		this.schedule = schedule;
	}

	@Override
	public void setSentEmails(List<Email> emails)
	{
		this.sentEmails = emails;
	}

	public void setSignatureCaptured(boolean signatureCaptured)
	{
		this.signatureCaptured = signatureCaptured;
	}

	public void setSignedOn(Date signedOn)
	{
		this.signedOn = signedOn;
	}

	public void setSigneeName(String signeeName) {
		this.signeeName = signeeName;
	}

	@Column(name = "destinationReceiptDate", columnDefinition = "datetime2")
	@DateTimeFormat(iso = ISO.DATE_TIME)
	public ZonedDateTime getDestinationReceiptDate() {
		return destinationReceiptDate;
	}

	public void setDestinationReceiptDate(ZonedDateTime destinationReceiptDate) {
		this.destinationReceiptDate = destinationReceiptDate;
	}
	
	
}