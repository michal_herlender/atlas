package org.trescal.cwms.core.deliverynote.entity.courierdespatch.db;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.dto.CourierDTO;
import org.trescal.cwms.core.deliverynote.dto.CourierDespatchDTO;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courier.db.CourierService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.form.CourierDespatchSearchForm;
import org.trescal.cwms.core.deliverynote.projection.CourierDespatchProjectionDTO;
import org.trescal.cwms.core.deliverynote.transformer.CourierTransformer;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service("CourierDespatchService")
public class CourierDespatchServiceImpl implements CourierDespatchService
{
	@Autowired
	CourierDespatchDao courierDespatchDao;
	@Autowired
	private CourierService courierServ;
	@Autowired
	private CourierTransformer courierTransformer;
	
	public CourierDespatch addCourierDespatchForDelivery(Delivery delivery, CourierDespatchType cdt, String consignmentNo, Contact creator, Subdiv allocatedSubdiv) {
		CourierDespatch cd = new CourierDespatch();
		cd.setCreatedBy(creator);
		cd.setCreationDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		cd.setOrganisation(allocatedSubdiv);
		cd.setDespatchDate(delivery.getDeliverydate());
		cd.setCdtype(cdt);
		cd.setConsignmentno(consignmentNo);

		cd.setDeliveries(new ArrayList<>());
		cd.getDeliveries().add(delivery);
		this.insertCourierDespatch(cd);
		delivery.setCrdes(cd);

		return cd;
	}

	public void deleteCourierDespatch(CourierDespatch cd)
	{
		this.courierDespatchDao.remove(cd);
	}

	public CourierDespatch findCourierDespatch(int id)
	{
		return this.courierDespatchDao.find(id);
	}

	public List<CourierDespatch> getAllCourierDespatches()
	{
		return this.courierDespatchDao.findAll();
	}

	public List<Delivery> getDeliveriesForCourierDespatch(int cdid)
	{
		return this.courierDespatchDao.getDeliveriesForCourierDespatch(cdid);
	}

	public List<CourierDespatch> getDespatchesCreatedInLastWeek(Integer allocatedSubdivId)
	{
		return this.courierDespatchDao.getDespatchesCreatedInLastWeek(allocatedSubdivId);
	}
	
	public List<CourierDespatchProjectionDTO> getProjectionDTOs(Collection<Integer> despatchIds) {
		List<CourierDespatchProjectionDTO> result = Collections.emptyList();
		if (despatchIds != null && !despatchIds.isEmpty()) {
			result = this.courierDespatchDao.getProjectionDTOs(despatchIds);
		}
		return result;
	}

	public void insertCourierDespatch(CourierDespatch CourierDespatch)
	{
		this.courierDespatchDao.persist(CourierDespatch);
	}

	public PagedResultSet<CourierDespatchDTO> queryDespatches(CourierDespatchSearchForm form, 
			PagedResultSet<CourierDespatchDTO> rs)
	{
		return this.courierDespatchDao.queryDespatches(form, rs);
	}

	public void updateCourierDespatch(CourierDespatch CourierDespatch)
	{
		this.courierDespatchDao.merge(CourierDespatch);
	}

	@Override
	public void prepareCourierDespatchData(Integer subdivId, Integer companyId, Model model) {
		List<CourierDespatch> existingCDs = this.getDespatchesCreatedInLastWeek(subdivId);
		List<CourierDespatchDTO> existingCDsDto = new ArrayList<>();
		existingCDs.forEach(cd -> existingCDsDto.add(new CourierDespatchDTO(cd.getCrdespid(), cd.getConsignmentno(),cd.getCdtype().getDescription(), cd.getDespatchDate())));
		String existingCDsJson = new Gson().toJson(existingCDsDto);
		model.addAttribute("existingCDs", existingCDsJson);

		// If there are no couriers defined for business company, courier selections should logically be empty!
		List<Courier> couriers = this.courierServ.getCouriersByBusinessCompany(companyId);
			
		List<CourierDTO> couriersDto = new ArrayList<>();
		couriers.forEach(c -> couriersDto.add(courierTransformer.transform(c)));
		
		String couriersDtoJson = new Gson().toJson(couriersDto);
		model.addAttribute("couriers", couriersDtoJson);	
	}
	
}