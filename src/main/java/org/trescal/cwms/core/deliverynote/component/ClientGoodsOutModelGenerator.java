package org.trescal.cwms.core.deliverynote.component;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.dto.CompanySettingsDTO;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.deliverynote.dto.DespatchItemDTO;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemProjectionCommand;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemProjectionResult;
import org.trescal.cwms.core.deliverynote.projection.DeliveryProjectionCommand;
import org.trescal.cwms.core.deliverynote.projection.service.DeliveryItemProjectionService;
import org.trescal.cwms.core.deliverynote.projection.service.DeliveryProjectionService;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Component
public class ClientGoodsOutModelGenerator {
	@Autowired
	private AddressService addressService;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private DeliveryService deliveryService;
	@Autowired
	private DeliveryProjectionService deliveryProjectionService;
	@Autowired
	private DeliveryItemProjectionService deliveryItemProjectionService;
	@Autowired
	private ItemStateService itemStateService; 
	@Autowired
	private TransportOptionService transOptServ;

	public static Logger logger = LoggerFactory.getLogger(ClientGoodsOutModelGenerator.class);

	/*
	 * Replacement for original code using projections
	 * ItemState / StateGroup mapping uses single query
	 */
	public GoodsOutDataModel getNewGoodsOutDataModel(List<StateGroup> stateGroups, int allocatedSubdivId, int allocatedCompanyId, Locale locale) {
		
		List<Integer> stateGroupIds = stateGroups.stream().map(StateGroup::getId).collect(Collectors.toList());
		List<DespatchItemDTO> despatchItemDtos = this.deliveryService.getGoodsOutDtos(stateGroupIds, allocatedSubdivId, locale);
		
		// TODO identify null sourceAddressId / destAddressID / transportOptionId as error
		Set<Integer> sourceAddressIds = despatchItemDtos.stream()
				.filter(dto -> dto.getSourceAddressId() != null)
				.map(DespatchItemDTO::getSourceAddressId).collect(Collectors.toSet()); 
		Set<Integer> destAddressIds = despatchItemDtos.stream()
				.filter(dto -> dto.getDestAddressId() != null)
				.map(DespatchItemDTO::getDestAddressId).collect(Collectors.toSet());
		Set<Integer> transportOptionIds = despatchItemDtos.stream()
				.filter(dto -> dto.getTransportOutOptionId() != null)
				.map(DespatchItemDTO::getTransportOutOptionId).collect(Collectors.toSet());
		Set<Integer> companyIds = despatchItemDtos.stream()
				.map(DespatchItemDTO::getDestCompanyId).collect(Collectors.toSet());
		Set<Integer> jobItemIds = despatchItemDtos.stream()
				.map(DespatchItemDTO::getJobItemId).collect(Collectors.toSet());
		
		List<KeyValue<Integer, String>> sourceAddressDtos = this.addressService.getAddressesKeyValue(sourceAddressIds);
		List<KeyValue<Integer, String>> destAddressDtos = this.addressService.getAddressesKeyValue(destAddressIds);
		Set<KeyValueIntegerString> resultTransportOptions = this.transOptServ.getTransportOptionDtosForIds(true, transportOptionIds, locale);
		Set<KeyValueIntegerString> subdivTransportOptions = this.transOptServ.getTransportOptionDtosForSubdivs(false, true, locale, Collections.singleton(allocatedSubdivId));
		List<ItemState> itemStates = this.itemStateService.getAllForStateGroups(stateGroupIds);
		List<CompanySettingsDTO> companySettings = this.companySettingsService.getDtosForAllocatedCompany(companyIds, allocatedCompanyId);
		logger.info("Getting delivery items details for "+jobItemIds.size()+" job item ids");
		DeliveryItemProjectionCommand dipCommand = new DeliveryItemProjectionCommand(locale);	// No options needed
		DeliveryItemProjectionResult dipResult = this.deliveryItemProjectionService.getDeliveryItemDTOs(jobItemIds, dipCommand);
		List<DeliveryItemDTO> deliveryItems = dipResult.getDeliveryItems();
		
		// TODO now that we could have "projection graph" in DTO, could refactor out some of above items 
		Set<Integer> deliveryIds = deliveryItems.stream()
				.map(DeliveryItemDTO::getDeliveryId).collect(Collectors.toSet());
		logger.info("Getting delivery details for "+deliveryIds.size()+" delivery ids");
		DeliveryProjectionCommand deliveryCommand = new DeliveryProjectionCommand(allocatedCompanyId); 
		List<DeliveryDTO> deliveries = this.deliveryProjectionService.getDeliveryDtosForIds(deliveryIds, deliveryCommand);
		logger.info("Delivery details for "+deliveries.size()+" deliveries");
		
		GoodsOutDataModel result = new GoodsOutDataModel();
		result.addSourceAddressDtos(sourceAddressDtos);
		result.addDestAddressDtos(destAddressDtos);
		result.addResultTransportOptions(resultTransportOptions);
		result.addSubdivTransportOptions(subdivTransportOptions);
		result.addStateGroups(stateGroups, itemStates);
		result.addCompanySettingsData(companySettings);
		result.addDeliveryItems(deliveryItems, deliveries);
		result.addDespatchItemDtos(despatchItemDtos);
		
		return result;
	}
}
