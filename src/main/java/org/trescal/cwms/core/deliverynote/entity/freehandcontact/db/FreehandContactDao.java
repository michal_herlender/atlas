package org.trescal.cwms.core.deliverynote.entity.freehandcontact.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.deliverynote.entity.freehandcontact.FreehandContact;

public interface FreehandContactDao extends BaseDao<FreehandContact, Integer> {}