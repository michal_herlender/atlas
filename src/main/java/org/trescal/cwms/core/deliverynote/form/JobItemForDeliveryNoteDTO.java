package org.trescal.cwms.core.deliverynote.form;

import java.util.List;
import java.util.Map;

import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.dto.ItemAccessoryDTO;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class JobItemForDeliveryNoteDTO {

	private int jobItemId;
	private int itemNo;
	private int jobId;
	private int index;
	private String jobNo;
	private Integer plantId;
	private String instrumentModelName;
	private String instrumentContact;
	private String instrumentAddress;
	private String instrumentLocation;
	private String instrumentSerialNo;
	private String instrumentPlantNo;
	private Integer subdivId;
	@JsonIgnore
	private Map<Integer, AccessoryForDeliveryNoteDTO> accessories;
	private String accessoryFreeText;
	private Boolean addAccessoryFreeText;
	private List<TPInstructionDTO> tpInstructions;
	private boolean addToDeliveryNote;
	private boolean readyForDelivery;
	private String jobItemPOs = "";
	private String onBehalfOfName;
	private Integer onBehalfOfId;
	private Integer groupId;
	private String transportOut;
	private String jobItemClientRef;
	private String jobClientRef;
	private List<ItemAccessoryDTO> itemAccessories;
	private Integer nextSubdivId;
	private String jobitemReturnToAddress;
	private Integer jobitemReturnToAddressId;
	private String jobitemReturnToContact;
	private Integer jobitemReturnToContactId;
	private String jobReturnToAddress;
	private Integer jobReturnToAddressId;
	private String jobReturnToContact;
	private Integer jobReturnToContactId;
	private Integer nextSubdivIdFromReturnOption;

	public JobItemForDeliveryNoteDTO(int jobItemId, int itemNo, int jobId, String jobNo, Integer plantId,
			String instrumentModelName, String instrumentSerialNo, String instrumentPlantNo, Integer subdivId,
			String accessoryFreetext, boolean readyForDelivery, String onBehalfOfName, Integer onBehalfOfId,
			Integer groupId, String transportOut, String jobItemClientRef, String jobClientRef,
			String instContactFirstName, String instContactLastName, String instAddress, String instrumentLocation,
			String country, String subnameFromJob, String onBehalfSubname, Integer workRequirementId,
			WorkrequirementStatus workRequirementStatus, Integer nextSubdivIdFromReturnOption,
			Integer nextSubdivIdFromWR, String jobitemReturnToAddress, Integer jobitemReturnToAddressId,
			String jobitemReturnToContact, Integer jobitemReturnToContactId, String jobReturnToAddress,
			Integer jobReturnToAddressId, String jobReturnToContact, Integer jobReturnToContactId) {
		super();
		this.jobItemId = jobItemId;
		this.itemNo = itemNo;
		this.jobId = jobId;
		this.jobNo = jobNo;
		this.plantId = plantId;
		this.instrumentModelName = instrumentModelName;
		this.instrumentSerialNo = instrumentSerialNo;
		this.instrumentPlantNo = instrumentPlantNo;
		this.subdivId = subdivId;
		this.accessoryFreeText = accessoryFreetext;
		this.readyForDelivery = readyForDelivery;
		this.onBehalfOfName = onBehalfOfName;
		this.onBehalfOfId = onBehalfOfId;
		this.groupId = groupId;
		this.transportOut = transportOut;
		this.jobItemClientRef = jobItemClientRef;
		this.jobClientRef = jobClientRef;
		this.instrumentContact = instContactFirstName + " " + instContactLastName;
		this.instrumentAddress = instAddress;
		this.jobitemReturnToAddress = jobitemReturnToAddress;
		this.jobReturnToAddress = jobReturnToAddress;
		this.jobitemReturnToAddressId = jobitemReturnToAddressId;
		this.jobReturnToAddressId = jobReturnToAddressId;
		this.jobitemReturnToContact = jobitemReturnToContact;
		this.jobReturnToContact = jobReturnToContact;
		this.jobitemReturnToContactId = jobitemReturnToContactId;
		this.jobReturnToContactId = jobReturnToContactId;
		this.nextSubdivIdFromReturnOption = nextSubdivIdFromReturnOption;

		// next subdivision is determined from the address of the next WR if the WR is
		// not null and not complete if not it determined from the return option
		if (workRequirementId == null || workRequirementStatus.equals(WorkrequirementStatus.COMPLETE)
				|| workRequirementStatus.equals(WorkrequirementStatus.CANCELLED)) {
			this.nextSubdivId = nextSubdivIdFromReturnOption;
		} else
			this.nextSubdivId = nextSubdivIdFromWR;

		// for Germany, we put "Client Department" information in the
		// Location field
		if (country != null && country.equals("Germany")) {
			if (onBehalfSubname != null) {
				this.instrumentLocation = onBehalfSubname;
			} else
				this.instrumentLocation = subnameFromJob;
		} else
			this.instrumentLocation = instrumentLocation;
	}

	public JobItemForDeliveryNoteDTO(int itemNo, String jobNo) {
		super();
		this.itemNo = itemNo;
		this.jobNo = jobNo;
	}
	
	
}