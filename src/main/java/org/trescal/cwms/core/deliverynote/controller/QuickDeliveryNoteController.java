package org.trescal.cwms.core.deliverynote.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.form.QuickDeliveryNoteForm;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@RequestMapping("quickdeliverynote.htm")
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class QuickDeliveryNoteController {
	
	private static final Logger logger = LoggerFactory.getLogger(QuickDeliveryNoteController.class);
	
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private JobItemService jobItemService;
	
	@ModelAttribute("form")
	private QuickDeliveryNoteForm getForm() {
		return new QuickDeliveryNoteForm();
	}
	
	@GetMapping
	public String onRequest() {
		return "trescal/core/deliverynote/quickdeliverynote";
	}
	
	@PostMapping
	public ModelAndView onSubmit(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto,
			@Validated @ModelAttribute("form") QuickDeliveryNoteForm form, BindingResult bindingResult) {
		logger.info("Barcode: " + form.getBarcode());
		if(bindingResult.hasErrors()) return new ModelAndView(onRequest());
		else {
			Instrument instrument = instrumentService.get(form.getBarcode());
			if(instrument == null) {
				bindingResult.rejectValue("barcode", "", "Barcode doesn't exist");
				return new ModelAndView(onRequest());
			}
			else {
				JobItem jobItem = instrument.getJobItems().stream().filter(ji -> !ji.getJob().getJs().isComplete()).findAny().orElse(null);
				if(jobItem == null) {
					bindingResult.rejectValue("barcode", "", "Instrument isn't on an active job");
					return new ModelAndView(onRequest());
				}
				else {
					if(jobItemService.isReadyForClientDelivery(jobItem, subdivDto.getKey()))
						return new ModelAndView(new RedirectView("createdelnote.htm?destination=CLIENT&jobitemid=" + jobItem.getJobItemId()));
					else
						return new ModelAndView(onRequest());
				}
			}
		}
	}
}