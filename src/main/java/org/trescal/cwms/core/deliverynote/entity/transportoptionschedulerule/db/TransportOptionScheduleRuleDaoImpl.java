package org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.TransportOptionScheduleRule;

@Repository("TransportOptionScheduleRuleDao")
public class TransportOptionScheduleRuleDaoImpl extends BaseDaoImpl<TransportOptionScheduleRule, Integer> implements TransportOptionScheduleRuleDao{

	@Override
	protected Class<TransportOptionScheduleRule> getEntity() {
		return TransportOptionScheduleRule.class;
	}
}
