package org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.DeliveryItemNote;

public interface DeliveryItemNoteDao extends BaseDao<DeliveryItemNote, Integer> {}