package org.trescal.cwms.core.deliverynote.entity.delivery.db;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.dto.DespatchItemDTO;
import org.trescal.cwms.core.deliverynote.entity.clientdelivery.ClientDelivery;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.db.CourierDespatchService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.db.CourierDespatchTypeService;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryComparator;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.db.DeliveryItemService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory.DeliveryItemAccessory;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory.db.DeliveryItemAccessoryService;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.DeliveryNote;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.db.DeliveryNoteService;
import org.trescal.cwms.core.deliverynote.entity.generaldelivery.GeneralDelivery;
import org.trescal.cwms.core.deliverynote.entity.internaldelivery.InternalDelivery;
import org.trescal.cwms.core.deliverynote.entity.internalreturndelivery.InternalReturnDelivery;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItemComparator;
import org.trescal.cwms.core.deliverynote.entity.thirdpartydelivery.ThirdPartyDelivery;
import org.trescal.cwms.core.deliverynote.form.CreateDeliveryNotePlainForm;
import org.trescal.cwms.core.deliverynote.form.DeliverySearchForm;
import org.trescal.cwms.core.deliverynote.form.EditDeliveryNoteForm;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.db.NotificationSystemFieldChangeModelService;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirementComparator;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.ScheduledDelivery;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.db.ScheduledDeliveryService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.ClientDeliveryDestinationReceiptDate;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.vdi.projection.OrderProjection;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_CreateDeliveryNote;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_DestinationReceiptConfirmed;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.rest.customer.dto.RestCalibrationDto;
import org.trescal.cwms.rest.customer.dto.RestCertificateDto;
import org.trescal.cwms.rest.customer.dto.RestCustomerDeliveryDto;
import org.trescal.cwms.rest.customer.dto.RestDeliveryDetailDto;
import org.trescal.cwms.rest.customer.dto.RestDeliveryDetailFlatDto;
import org.trescal.cwms.rest.customer.dto.RestJobItemDeliveryDto;

import lombok.val;

@Service("DeliveryService")
public class DeliveryServiceImpl extends BaseServiceImpl<Delivery, Integer> implements DeliveryService {

	@Value("#{props['cwms.config.delivery.allow_delivery_scanning']}")
	private boolean allowDeliveryScanning;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private DeliveryItemService delItemServ;
	@Autowired
	private DeliveryDao deliveryDao;
	@Autowired
	private HookInterceptor_CreateDeliveryNote interceptor_createDeliveryNote;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private SystemComponentService scServ;
	@Autowired
	private AddressService addServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private LocationService locServ;
	@Autowired
	private HookInterceptor_DestinationReceiptConfirmed interceptor_DestinationReceiptConfirmed;
	@Autowired
	private ScheduledDeliveryService scheduledDeliveryService;
	@Autowired
	private LinkToAdveso linkToAdvesoSettings;
	@Autowired
	private AddressService addressService;
	@Autowired
	private NotificationSystemFieldChangeModelService notificationSystemFieldChangeModelService;
	@Autowired
	private ClientDeliveryDestinationReceiptDate clientDeliveryDestinationReceiptDate;
	@Autowired
	private UserService userService;
	@Autowired
	private CourierDespatchService cdServ;
	@Autowired
	private CourierDespatchTypeService cdtServ;
	@Autowired
	private DeliveryItemAccessoryService delItemAccServ;
	@Autowired
	private DeliveryNoteService delNoteServ;
	@Autowired
	private JobService jobService;
	@Autowired
	private JobItemService jobitemService;

	public static final Logger logger = LoggerFactory.getLogger(DeliveryServiceImpl.class);

	public final static String ERROR_CODE_DELIVERY_NOT_FOUND = "error.delivery.notfound";
	public final static String ERROR_CODE_DELIVERY_WRONG_TYPE = "error.delivery.wrongtype";
	public final static String ERROR_CODE_DELIVERY_NOT_IN_TRANSIT = "error.delivery.notintransit";
	public final static String ERROR_CODE_BARCODE_NOT_RECOGNISED = "error.barcode.notrecognised";
	public final static String ERROR_CODE_DELIVERY_NOT_AWAITING_DESPATCH = "error.delivery.notawaitingdespatch";

	@Override
	protected BaseDao<Delivery, Integer> getBaseDao() {
		return deliveryDao;
	}

	@Override
	public boolean containsDespatchedItems(int delid) {

		Delivery delivery = this.get(delid);
		return delivery.getItems().stream().anyMatch(DeliveryItem::isDespatched);
	}

	@Override
	public ClientDelivery createDeliveryForJobItems(List<JobItem> jobItems, Contact currentContact, Subdiv allocatedSubdiv) {

		JobItem item = jobItems.iterator().next();
		Job job = item.getJob();
		// Get the next transit date
		val delDate = this.jiServ.calculateNextTransitDate(item);

		// Get the next delno
		String delno = numerationService.generateNumber(NumerationType.DELIVERY_NOTE, allocatedSubdiv.getComp(),
				allocatedSubdiv);

		ClientDelivery delivery = new ClientDelivery();
		delivery.setAddress(job.getReturnTo());
		delivery.setLocation(job.getReturnToLoc());
		delivery.setContact(job.getCon());
		delivery.setCreatedby(currentContact);
		delivery.setCreationdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		delivery.setDeliverydate(delDate);
		delivery.setDeliveryno(delno);
		delivery.setJob(job);
		delivery.setOrganisation(allocatedSubdiv);

		delivery.setItems(new TreeSet<>(new JobDeliveryItemComparator()));
		for (JobItem ji : jobItems) {
			for (StateGroupLink sgl : ji.getState().getGroupLinks()) {
				if (sgl.getGroup().equals(StateGroup.CLIENTDELIVERY)) {
					JobDeliveryItem jdi = new JobDeliveryItem();
					jdi.setDelivery(delivery);
					jdi.setDespatched(false);
					jdi.setJobitem(ji);
					delivery.getItems().add(jdi);
				}
			}
		}

		this.insertDelivery(delivery);

		return delivery;
	}

	@Override
	public void deleteDelivery(Delivery delivery) {
		List<ScheduledDelivery> scheduledDeliveryList = this.scheduledDeliveryService
				.scheduledDeliveryFromScheduleAndOrDelivery(null, delivery.getDeliveryid());
		for (ScheduledDelivery scheduledDelivery : scheduledDeliveryList) {
			this.scheduledDeliveryService.delete(scheduledDelivery);
		}
		this.deliveryDao.remove(delivery);
	}

	@Override
	public ResultWrapper despatchDelivery(String deliveryNo) {
		// find the delivery with the given number
		Delivery delivery = this.findDeliveryByDeliveryNo(deliveryNo);

		// if there is no delivery with this number
		if (delivery == null) {
			String message = this.messageSource.getMessage(ERROR_CODE_DELIVERY_NOT_FOUND, null,
					"Delivery number not found", LocaleContextHolder.getLocale());
			return new ResultWrapper(false, message);
		}

		// if the delivery is not a job delivery
		if (!(delivery instanceof JobDelivery)) {
			String message = this.messageSource.getMessage(ERROR_CODE_DELIVERY_WRONG_TYPE, null,
					"Delivery is not of the correct type", LocaleContextHolder.getLocale());
			return new ResultWrapper(false, message);
		}

		// else, cast the delivery
		JobDelivery jobDelivery = (JobDelivery) delivery;

		// for each job item on the delivery
		for (JobDeliveryItem jdi : jobDelivery.getItems()) {
			// check if the item is ready for despatch
			if (!this.jiServ.itemCanBeDespatched(jdi.getJobitem())) {
				String message = this.messageSource.getMessage(ERROR_CODE_DELIVERY_NOT_AWAITING_DESPATCH, null,
						"One or more job items on this delivery note are not ready for despatch",
						LocaleContextHolder.getLocale());
				return new ResultWrapper(false, message);
			}
		}

		// if all is good, despatch all items
		for (JobDeliveryItem jdi : jobDelivery.getItems()) {
			ResultWrapper rw = this.jiServ.despatchItem(jdi.getJobitem().getInst().getPlantid());
			if (rw.isSuccess()) {
				jdi.setDespatched(true);
				jdi.setDespatchedon(ZonedDateTime.now(LocaleContextHolder.getTimeZone().toZoneId()));
				this.delItemServ.merge(jdi);
			}
		}

		// return successful result
		return new ResultWrapper(true, null, jobDelivery, null);
	}

	@Override
	public ResultWrapper despatchDeliveryOrItem(String deliveryNoOrBarcode) {
		if (NumberTools.isAnInteger(deliveryNoOrBarcode)) {
			int plantId = Integer.parseInt(deliveryNoOrBarcode);
			ResultWrapper rw = this.jiServ.despatchItem(plantId);
			if (rw.isSuccess()) {
				JobDeliveryItem jdi = this.delItemServ.findNotYetDespatchedJobDeliveryItemByInstrument(plantId);
				if (jdi != null) {
					jdi.setDespatched(true);
					jdi.setDespatchedon(ZonedDateTime.now(LocaleContextHolder.getTimeZone().toZoneId()));
					this.delItemServ.merge(jdi);
				}
			}
			return rw;
		} else if (this.allowDeliveryScanning) {
			return this.despatchDelivery(deliveryNoOrBarcode);
		} else {
			String message = this.messageSource.getMessage(ERROR_CODE_BARCODE_NOT_RECOGNISED, null,
					"Barcode not recognised as an instrument barcode", LocaleContextHolder.getLocale());
			return new ResultWrapper(false, message);
		}
	}

	@Override
	public Delivery get(int id) {
		return this.deliveryDao.find(id);
	}

	@Override
	public <DeliveryClass extends Delivery> DeliveryClass get(int id, Class<DeliveryClass> clazz) {
		return this.deliveryDao.get(id, clazz);
	}

	@Override
	public Delivery findDeliveryByDeliveryNo(String delno) {
		return this.deliveryDao.findDeliveryByDeliveryNo(delno);
	}

	@Override
	public SortedSet<JobDelivery> findRelatedToJob(Job job) {
		SortedSet<JobDelivery> results = new TreeSet<>(new DeliveryComparator());
		// Add JobDeliveries with JobDeliveryItem->JobItem->this Job
		// This ensures that all related multi-Job delivery notes are linked and
		// not
		// just
		// those with this Job as the primary job
		results.addAll(this.deliveryDao.findByJob(job));
		// Job may also have JobDeliveries with no items yet, so these are added
		// too.
		results.addAll(job.getDeliveries());

		return results;
	}

	@Override
	public List<DespatchItemDTO> getTpGoodsOutItems(List<Integer> stateGroups, int allocatedSubdivId, Locale locale) {
		return this.deliveryDao.getTpGoodsOutItems(stateGroups, allocatedSubdivId, locale);
	}

	@Override
	public List<DespatchItemDTO> getGoodsOutDtos(List<Integer> stateGroups, int allocatedSubdivId, Locale locale) {
		return this.deliveryDao.getGoodsOutDtos(stateGroups, allocatedSubdivId, locale);
	}

	@Override
	public void insertDelivery(Delivery delivery) {
		this.deliveryDao.persist(delivery);
		this.interceptor_createDeliveryNote.recordAction(delivery);
	}

	@Override
	public List<? extends Delivery> findByJobItem(int jobItemId, DeliveryType type) {
		return this.deliveryDao.findByJobItem(jobItemId, type);
	}

	@Override
	public List<DeliveryDTO> getDeliveryDtosForIds(Collection<Integer> deliveryIds) {
		List<DeliveryDTO> result = Collections.emptyList();
		if (deliveryIds != null && !deliveryIds.isEmpty()) {
			result = this.deliveryDao.getDeliveryDtosForIds(deliveryIds);
		}
		return result;
	}

	@Override
	public PagedResultSet<Delivery> queryDeliveries(DeliverySearchForm dsf, PagedResultSet<Delivery> rs) {
		return this.deliveryDao.queryDeliveries(dsf, rs);
	}

	@Override
	public ResultWrapper receiveDelivery(String deliveryNo, int addrId) {
		// find the delivery with the given number
		Delivery delivery = this.findDeliveryByDeliveryNo(deliveryNo);

		// if there is no delivery with this number
		if (delivery == null) {
			String message = this.messageSource.getMessage(ERROR_CODE_DELIVERY_NOT_FOUND, null,
					"Delivery number not found", LocaleContextHolder.getLocale());
			return new ResultWrapper(false, message);
		}

		// if the delivery is not a third party delivery
		if (!delivery.isThirdPartyDelivery() && !delivery.isClientDelivery() && !delivery.isInternalDelivery()
				&& !delivery.isInternalReturnDelivery()) {
			String message = this.messageSource.getMessage(ERROR_CODE_DELIVERY_WRONG_TYPE, null,
					"Delivery is not of the correct type", LocaleContextHolder.getLocale());
			return new ResultWrapper(false, message);
		}

		// else, cast the delivery
		JobDelivery jobDelivery = (JobDelivery) delivery;

		// for each job item on the delivery
		for (JobDeliveryItem jdi : jobDelivery.getItems()) {
			// check if the item is ready to be received
			if (!this.jiServ.itemCanBeReceived(jdi.getJobitem())) {
				String message = this.messageSource.getMessage(ERROR_CODE_DELIVERY_NOT_IN_TRANSIT, null,
						"One or more job items on this delivery note are not in transit",
						LocaleContextHolder.getLocale());
				return new ResultWrapper(false, message);
			}
		}

		// if all is good, receive all items
		for (JobDeliveryItem jdi : jobDelivery.getItems()) {
			this.jiServ.receiveItem(jdi.getJobitem().getInst().getPlantid(), addrId, null);
		}

		// return successful result
		return new ResultWrapper(true, null, jobDelivery, null);
	}

	@Override
	public ResultWrapper receiveDeliveryOrItem(String deliveryNoOrBarcode, int addrId, Integer locId) {
		if (NumberTools.isAnInteger(deliveryNoOrBarcode)) {
			return this.jiServ.receiveItem(Integer.parseInt(deliveryNoOrBarcode), addrId, locId);
		} else {
			if (this.allowDeliveryScanning) {
				return this.receiveDelivery(deliveryNoOrBarcode, addrId);
			} else {
				String message = this.messageSource.getMessage(ERROR_CODE_BARCODE_NOT_RECOGNISED, null,
						"Barcode not recognised as an instrument barcode", LocaleContextHolder.getLocale());
				return new ResultWrapper(false, message);
			}
		}
	}

	@Override
	public void removeDeliveryFromSchedule(int delid) {
		this.deliveryDao.removeDeliveryFromSchedule(delid);
	}

	@Override
	public void setDirectoryUsingComponent(Delivery d) {
		// 2018-12-12 GB : Prevent directory lookup on repeated call.
		if (d != null && d.getDirectory() == null) {
			if (d instanceof GeneralDelivery) {
				GeneralDelivery gd = (GeneralDelivery) d;
				gd.setDirectory((this.compDirServ.getDirectory(this.scServ.findComponent(Component.GENERAL_DELIVERY),
						gd.getDeliveryno())));
			} else {
				JobDelivery jd = (JobDelivery) d;
				jd.setDirectory((this.compDirServ.getDirectory(this.scServ.findComponent(Component.DELIVERY),
						jd.getJob().getJobno())));
			}
		}
	}

	@Override
	public Optional<OrderProjection> getOrderProjectionForDeliveryNote(Integer deliveryId) {
		return deliveryDao.getOrderProjectionForDeliveryNote(deliveryId);
	}

	@Override
	public void updateDelivery(Delivery Delivery) {
		this.deliveryDao.merge(Delivery);
	}

	@Override
	public List<ClientDelivery> getClientDeliveryListByJobitemId(Integer jobitemid) {
		return deliveryDao.getClientDeliveryListByJobitemId(jobitemid);
	}

	@Override
	public List<RestCustomerDeliveryDto> findDeliveryDto(Subdiv subdiv, LocalDate dtStart, LocalDate dtEnd,
			DeliveryType deliveryType) {
		return deliveryDao.findDeliveryDto(subdiv, dtStart, dtEnd, deliveryType);
	}

	@Override
	public List<RestDeliveryDetailDto> findDeliveryDetails(List<Integer> deliveries) {
		List<RestDeliveryDetailFlatDto> queryResults = findDeliveryDetailsFlat(deliveries);
		List<RestDeliveryDetailDto> restResults = new ArrayList<>();
		Map<Integer, List<RestDeliveryDetailFlatDto>> deliveryMap = queryResults.stream()
				.collect(Collectors.groupingBy(RestDeliveryDetailFlatDto::getDeliveryId));
		for (Integer deliveryId : deliveryMap.keySet()) {
			RestDeliveryDetailDto deliveryDetail = new RestDeliveryDetailDto(deliveryId,
					deliveryMap.get(deliveryId).get(0).getDeliveryNo());
			Map<Integer, List<RestDeliveryDetailFlatDto>> jobItemMap = deliveryMap.get(deliveryId).stream()
					.collect(Collectors.groupingBy(RestDeliveryDetailFlatDto::getJobItemId));
			for (Integer jobItemId : jobItemMap.keySet()) {
				RestDeliveryDetailFlatDto reference = jobItemMap.get(jobItemId).get(0);
				String mfr = reference.getManufacturer().replace('/', ' ').trim().isEmpty()
						? reference.getInstrumentMfr() : reference.getManufacturer();
				String model = reference.getModel().replace('/', ' ').trim().isEmpty()
						? reference.getInstrumentMfrModel() : reference.getModel();
				RestJobItemDeliveryDto jobItem = new RestJobItemDeliveryDto(jobItemId, reference.getJobItemNo(),
						reference.getJobNo(), reference.getPlantId(), reference.getPlantNo(), reference.getSerialNo(),
						reference.getCustomerDescription(), reference.getCustomerSpecification(),
						reference.getInstrModelName(), reference.getFamily(), reference.getSubFamily(), mfr, model,
						reference.getNextCalDate(), reference.getCalFrequency(), reference.getCalFrequencyUnit());
				Map<Integer, List<RestDeliveryDetailFlatDto>> certificateMap = jobItemMap.get(jobItemId).stream()
						.filter(i -> i.getCertifId() != null)
						.collect(Collectors.groupingBy(RestDeliveryDetailFlatDto::getCertifId));
				for (Integer certificateId : certificateMap.keySet()) {
					RestDeliveryDetailFlatDto certificateRef = certificateMap.get(certificateId).get(0);
					jobItem.getCertifs()
							.add(new RestCertificateDto(certificateId, certificateRef.getCertifNo(),
									certificateRef.getCertDate(), certificateRef.getCalDate(),
									certificateRef.getCertifCalId(), certificateRef.getCertificateCalibrationStatus()));
				}
				Map<Integer, List<RestDeliveryDetailFlatDto>> calibrationMap = jobItemMap.get(jobItemId).stream()
						.filter(i -> i.getCalId() != null)
						.collect(Collectors.groupingBy(RestDeliveryDetailFlatDto::getCalId));
				for (Integer calibrationId : calibrationMap.keySet()) {
					RestDeliveryDetailFlatDto calibrationRef = calibrationMap.get(calibrationId).get(0);
					jobItem.getCalibs()
							.add(new RestCalibrationDto(calibrationId, calibrationRef.getCalClass().name(),
									calibrationRef.getStartTime(), calibrationRef.getCompletionTime(),
									calibrationRef.getCalStatus(), calibrationRef.getServiceDescription()));
				}
				deliveryDetail.getJobitems().add(jobItem);
			}
			restResults.add(deliveryDetail);
		}
		return restResults;
	}

	@Override
	public void editDeliveryNote(Delivery delivery, LocalDate newDeliveryDate, EditDeliveryNoteForm form) {

		delivery.setDeliverydate(newDeliveryDate);
		// set Address
		if (form.getAddressid() != null) {
			delivery.setAddress(this.addServ.get(form.getAddressid()));
		}
		// set Contact
		if (form.getContactid() != null) {
			delivery.setContact(this.conServ.get(form.getContactid()));
		}
		// set telephone for FreehandContact
		// set Location
		if (form.getLocid() != null) {
			delivery.setLocation(this.locServ.get(form.getLocid()));
		}
	}

	@Override
	public void updateDestinationReceiptDate(Delivery delivery, ZonedDateTime destinationReceiptDate,
			Integer businessCompanyId) {

		if (delivery.isClientDelivery() || delivery.isThirdPartyDelivery()) {
			for (DeliveryItem item : delivery.getItems()) {
				JobDeliveryItem jdItem = (JobDeliveryItem) item;
				if (jdItem != null) {
					// add jobitem notification on destination receipt date
					if (jdItem.getJobitem().getClientReceiptDate() != null) {
						boolean isLinkedToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
								jdItem.getJobitem().getJob().getCon().getSub().getSubdivid(), businessCompanyId,
								new Date());
						String newDateValue = destinationReceiptDate != null ? DateTools.dtf_activities
								.format(new Date(destinationReceiptDate.toInstant().toEpochMilli())) : null;
						String oldDateValue = jdItem.getJobitem().getClientReceiptDate() != null
								? DateTools.dtf_activities.format(
										new Date(jdItem.getJobitem().getClientReceiptDate().toInstant().toEpochMilli()))
								: null;
						if (isLinkedToAdveso && !StringUtils.equals(oldDateValue, newDateValue)) {
							notificationSystemFieldChangeModelService.saveAndPublishNotificationFieldChange(
									jdItem.getJobitem(), JobItem_.clientReceiptDate.getName(),
									NotificationSystemFieldTypeEnum.Date, newDateValue, oldDateValue);
						}
					}
					jdItem.getJobitem().setClientReceiptDate(destinationReceiptDate);

					// call "destination-receipt-confirmed" hook
					if (destinationReceiptDate != null) {
						interceptor_DestinationReceiptConfirmed.recordAction(jdItem);
					}
				}
			}
		}
		delivery.setDestinationReceiptDate(destinationReceiptDate);

		this.updateDelivery(delivery);
	}

	public List<RestDeliveryDetailFlatDto> findDeliveryDetailsFlat(List<Integer> deliveries) {
		Locale locale = LocaleContextHolder.getLocale();
		return deliveryDao.findDeliveryDetailFlat(deliveries, locale);
	}

	@Override
	public Long deliveryCountByCourier(Integer courierId) {
		return this.deliveryDao.deliveryCountByCourier(courierId);
	}

	@Override
	public Long deliveryCountByCourierDespatchType(Integer cdtId) {
		return this.deliveryDao.deliveryCountByCourierDespatchType(cdtId);
	}

	@Override
	public boolean isLastTPDelivery(JobItem ji) {
		return this.deliveryDao.isLastTPDelivery(ji);
	}

	@Override
	public List<Integer> getdeliveriesIdsByJob(Integer jobId) {
		return this.deliveryDao.getdeliveriesIdsByJob(jobId);
	}

	@Override
	public boolean isReadyForDestinationReceipt(Delivery delivery) {
		// display the <Destination Receipt Date> TAB if DestinationReceiptDate
		// is not
		// null to update the field value
		// if necessary
		boolean clientDeliveryDestinationReceiptDateIsNotNull = delivery.getDestinationReceiptDate() != null;
		// display the <Destination Receipt Date> TAB only if :
		// 1) the delivery is CLIENT or TP delivery
		// 2) the clientDestinationReceiptDate setting is set to TRUE
		// 3) all the items are on the status 'Awaiting destination receipt
		// date'
		boolean isClientOrTpDelivery = delivery.isClientDelivery() || delivery.isThirdPartyDelivery();
		boolean isJobDelivery = delivery instanceof JobDelivery;
		Contact clientContact = isJobDelivery ? delivery.getItems().stream().findFirst()
			.map(i -> ((JobDeliveryItem) i).getJobitem().getJob().getCon()).orElse(null) : null;
		boolean clientDeliveryDestinationReceiptDateRequired = clientContact != null
			&& clientDeliveryDestinationReceiptDate.parseValueHierarchical(Scope.SUBDIV, clientContact.getSub(),
			clientContact.getSub().getComp());
		boolean allItemsAreOnTheCorrectStatus = isJobDelivery && delivery.getItems().stream()
				.allMatch(i -> jiServ.stateHasGroupOfKeyName(((JobDeliveryItem) i).getJobitem().getState(),
						StateGroup.AWAITING_CLIENT_RECEIPT_CONFIRMATION));
		return clientDeliveryDestinationReceiptDateIsNotNull || (isClientOrTpDelivery
				&& clientDeliveryDestinationReceiptDateRequired && allItemsAreOnTheCorrectStatus);
	}

	@Override
	public JobDelivery createDelivery(CreateDeliveryNotePlainForm form, String username, Subdiv allocatedSubdiv) {

        JobDelivery delivery = new ClientDelivery();
        Job job = jobService.get(form.getJobId());
        String deliveryNo = numerationService.generateNumber(NumerationType.DELIVERY_NOTE, allocatedSubdiv.getComp(),
            allocatedSubdiv);
        if (form.getDeliveryType().equals(DeliveryType.THIRDPARTY)) {
            delivery = new ThirdPartyDelivery();
            form.setTp(true);
		} else if (form.getDeliveryType().equals(DeliveryType.INTERNAL)) {
			delivery = new InternalDelivery();
		} else if (form.getDeliveryType().equals(DeliveryType.INTERNAL_RETURN)) {
			delivery = new InternalReturnDelivery();
		}

		// Get the current user for createdby fields
		Contact currentUser = userService.get(username).getCon();
		delivery.setOrganisation(allocatedSubdiv);
		delivery.setDeliveryno(deliveryNo);
		delivery.setCreatedby(currentUser);
		delivery.setJob(job);
		delivery.setCreationdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		delivery.setDeliverydate(form.getDeliverydate());

		if (form.getCoid() != null && form.getCoid() > 0) {
			// coid is set so the cascading selector plugin has been used to
			// select a contact and address from a different company.
			if (form.getPersonid() != null)
				delivery.setContact(this.conServ.get(form.getPersonid()));
			if (form.getAddrid() != null)
				delivery.setAddress(this.addressService.get(form.getAddrid()));
		} else {
			// Company not changing get address and contact from normal
			// selectors
			if (form.getAddressid() != null)
				delivery.setAddress(this.addressService.get(form.getAddressid()));
			if (form.getContactid() != null)
				delivery.setContact(this.conServ.get(form.getContactid()));
		}
		delivery.setLocation(this.locServ.get(form.getLocid()));
		// add to existing courier despatch
		if (delivery.getItems() == null)
			delivery.setItems(new TreeSet<>(new JobDeliveryItemComparator()));
		Map<Integer, JobItem> jobItemMap = new HashMap<>();
		// Put all items (current job and foreign) together
		form.getItems().forEach(jiId -> jobItemMap.put(jiId, jiServ.findJobItem(jiId)));

		for (Integer itemId : form.getItems()) {
			JobItem ji = jobItemMap.get(itemId);
			JobDeliveryItem deliveryitem = new JobDeliveryItem();
			deliveryitem.setDelivery(delivery);
			deliveryitem.setJobitem(ji);
			if (form.isTp() && ji.getTpRequirements() != null && ji.getTpRequirements().size() > 0) {
				TreeSet<TPRequirement> reqs = new TreeSet<>(new TPRequirementComparator());
				reqs.addAll(ji.getTpRequirements());
				TPRequirement tpReq = reqs.first(); // Ordered
													// newest-to-oldest
				deliveryitem.setTpRequirement(tpReq);
				tpReq.setDeliveryItem(deliveryitem);
			}
			delivery.getItems().add(deliveryitem);
		}
		if (form.getSelectant().equals("exists"))
			// add to existing courier despatch
			delivery.setCrdes(this.cdServ.findCourierDespatch(form.getSelectdespatch()));
		else if (form.getSelectant().equals("new")) {
			// create new courier despatch and add delivery to it
			CourierDespatch cd = new CourierDespatch();
			cd.setDespatchDate(form.getDespatchDate());
			cd.setCreationDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			cd.setOrganisation(allocatedSubdiv);
			cd.setCdtype(this.cdtServ.get(form.getSelectType()));
			cd.setConsignmentno(form.getConsignmentno());
			cd.setCreatedBy(currentUser);
			this.cdServ.insertCourierDespatch(cd);
			delivery.setCrdes(cd);
		}

		// Create the delivery object itself
		this.insertDelivery(delivery);
		for (JobDeliveryItem di : delivery.getItems()) {
			// get job item
			JobItem ji = di.getJobitem();
			// job item has accessories?
			if (form.getAccessoryItems().size() > 0) {
				if (ji.getAccessories() != null && ji.getAccessories().size() > 0)
					// check all job item accessories
					for (ItemAccessory itemAccessory : ji.getAccessories())
						// this accessory selected for delivery note?
						if (form.getAccessoryItems().contains(itemAccessory.getId())) {
							// create delivery item accessory for any
							// checked
							DeliveryItemAccessory deliveryItemAccessory = new DeliveryItemAccessory();
							deliveryItemAccessory.setAccessory(itemAccessory.getAccessory());
							deliveryItemAccessory.setQty(itemAccessory.getQuantity());
							deliveryItemAccessory.setDeliveryItem(di);
							// insert delivery item accessory
							delItemAccServ.save(deliveryItemAccessory);
						}
			}
			di.setAccessoryFreeText(ji.getAccessoryFreeText());
		}
		this.updateDelivery(delivery);
		// Create a Delivery Note object (IF there is text
		// in the delivery note field)
		if (!form.getDelInstruction().trim().equals("")) {
			DeliveryNote delNote = new DeliveryNote();
			delNote.setNote(form.getDelInstruction());
			delNote.setDelivery(delivery);
			delNote.setSetBy(currentUser);
			delNote.setSetOn(new Date());
			delNote.setActive(true);
			delNote.setPublish(true);
			this.delNoteServ.save(delNote);
		}

		return delivery;
	}

	@Override
	public ResultWrapper scanOutDeliveryOrItem(String deliveryNoOrBarcode) {
		if (NumberTools.isAnInteger(deliveryNoOrBarcode)) {
			int plantId = Integer.parseInt(deliveryNoOrBarcode);
			ResultWrapper rw = this.jiServ.despatchItem(plantId);
			if (rw.isSuccess()) {
				JobDeliveryItem jdi = this.delItemServ.findNotYetDespatchedJobDeliveryItemByInstrument(plantId);
				if (jdi != null) {
					jdi.setDespatched(true);
					jdi.setDespatchedon(ZonedDateTime.now(LocaleContextHolder.getTimeZone().toZoneId()));
					this.delItemServ.merge(jdi);
				}
			}
			return rw;
		} else if (this.allowDeliveryScanning) {

			return this.scanOuthDelivery(deliveryNoOrBarcode);

		} else {
			String message = this.messageSource.getMessage(ERROR_CODE_BARCODE_NOT_RECOGNISED, null,
					"Barcode not recognised as an instrument barcode", LocaleContextHolder.getLocale());
			return new ResultWrapper(false, message);
		}
	}

	@Override
	public ResultWrapper scanOuthDelivery(String deliveryno) {
		// find the delivery with the given number
		if (this.deliveryExists(deliveryno) == null) {
			String message = this.messageSource.getMessage(ERROR_CODE_DELIVERY_NOT_FOUND, null,
					"Delivery number not found", LocaleContextHolder.getLocale());
			return new ResultWrapper(false, message);
		}

		 List<JobItem> jis = this.jobitemService.getJobItemByDeliveryId(deliveryno);

		

		for (JobItem ji : jis) {
			if (!this.jiServ.itemCanBeDespatched(ji)) {
				String message = this.messageSource.getMessage(ERROR_CODE_DELIVERY_NOT_AWAITING_DESPATCH, null,
						"One or more job items on this delivery note are not ready for despatch",
						LocaleContextHolder.getLocale());
				return new ResultWrapper(false, message);
			}
		}

		Address addr = this.getDeliveryAddressByDeliveryNo(deliveryno);
		// if all is good, despatch all items

		for (JobItem ji : jis) {
			this.jiServ.scanOutItem(ji, addr);
		}
		
		List<DeliveryItem> jdis = this.delItemServ.getItemsForJobDeliveryBydDelNo(deliveryno);
		
		for (DeliveryItem jdi : jdis) {
			jdi.setDespatched(true);
			jdi.setDespatchedon(ZonedDateTime.now(LocaleContextHolder.getTimeZone().toZoneId()));
			this.delItemServ.merge(jdi);
		}

		// return successful result
		return new ResultWrapper(true, null, null, null);

	}

	@Override
	public String deliveryExists(String DeliveryNo) {
		return this.deliveryDao.deliveryExists(DeliveryNo);
	}

	@Override
	public Address getDeliveryAddressByDeliveryNo(String deliveryNo) {
		return this.deliveryDao.getDeliveryAddressByDeliveryNo(deliveryNo);
	}

	@Override
	public ResultWrapper scanInDeliveryOrItem(String deliveryNoOrBarcode, int addrId, Integer locId) {
		if (NumberTools.isAnInteger(deliveryNoOrBarcode)) {
			return this.jiServ.scanInItem(Integer.parseInt(deliveryNoOrBarcode), addrId, locId);
		} else {
			if (this.allowDeliveryScanning) {
				return this.scanInDelivery(deliveryNoOrBarcode, addrId);
			} else {
				String message = this.messageSource.getMessage(ERROR_CODE_BARCODE_NOT_RECOGNISED, null,
						"Barcode not recognised as an instrument barcode", LocaleContextHolder.getLocale());
				return new ResultWrapper(false, message);
			}
		}
	}

	@Override
	public ResultWrapper scanInDelivery(String deliveryNo, int addrId) {
		
		if (this.deliveryExists(deliveryNo) == null) {
			String message = this.messageSource.getMessage(ERROR_CODE_DELIVERY_NOT_FOUND, null,
					"Delivery number not found", LocaleContextHolder.getLocale());
			return new ResultWrapper(false, message);
		}
		
		
		Delivery delivery = this.findDeliveryByDeliveryNo(deliveryNo);
		// if the delivery is not a third party delivery
		if (!delivery.isThirdPartyDelivery() && !delivery.isClientDelivery() && !delivery.isInternalDelivery()
				&& !delivery.isInternalReturnDelivery()) {
			String message = this.messageSource.getMessage(ERROR_CODE_DELIVERY_WRONG_TYPE, null,
					"Delivery is not of the correct type", LocaleContextHolder.getLocale());
			return new ResultWrapper(false, message);
		}

		
		
		List<JobItem> jis = this.jobitemService.getJobItemByDeliveryId(deliveryNo);
		
		for (JobItem ji : jis) {
			// check if the item is ready to be received
			if (!this.jiServ.itemCanBeReceived(ji)) {
				String message = this.messageSource.getMessage(ERROR_CODE_DELIVERY_NOT_IN_TRANSIT, null,
						"One or more job items on this delivery note are not in transit",
						LocaleContextHolder.getLocale());
				return new ResultWrapper(false, message);
			}
		}

		// if all is good, receive all items
		for (JobItem ji : jis) {
			this.jiServ.scanInDeliveryItems(ji, addrId, null);
		}
		
		return new ResultWrapper(true, null, null, null);
	}

}