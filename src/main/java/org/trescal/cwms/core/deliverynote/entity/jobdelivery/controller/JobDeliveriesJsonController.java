package org.trescal.cwms.core.deliverynote.entity.jobdelivery.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.db.DeliveryItemService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;

@Controller
@IntranetController
public class JobDeliveriesJsonController {

	@Autowired
	private DeliveryService delService;
	@Autowired
	private ContactService contService;
	@Autowired
	private DeliveryItemService delItemService;
	@Autowired
	private JobItemService jobitemservice;

	@RequestMapping(value = "/getDeliveriesByJob.json", method = RequestMethod.GET)
	@ResponseBody
	protected List<DeliveryDTO> getDeliveriesByJob(@RequestParam(name = "jobId", required = false) Integer jobId)
			throws Exception {
		List<DeliveryDTO> dtos = this.delService.getDeliveryDtosForIds(this.delService.getdeliveriesIdsByJob(jobId));
		for (DeliveryDTO dto : dtos) {
			dto.setDestContact(this.contService.getContactProjectionDTO(dto.getDestContactId()));
		}
		return dtos;
	}

	@RequestMapping(value = "/getJobDeliveryItems.json", method = RequestMethod.GET)
	@ResponseBody
	protected List<DeliveryItemDTO> getJobDeliveryItems(
			@RequestParam(name = "deliveryId", required = false) Integer deliveryId, Locale locale) throws Exception {
		List<DeliveryItemDTO> dtos = this.delItemService.getDeliveryItemDTOs(deliveryId, null);
		for (DeliveryItemDTO dto : dtos) {
			dto.setJobItem(this.jobitemservice.getJobItemProjectionDTO(dto.getJobItemId(), locale));
		}
		return dtos;
	}

}
