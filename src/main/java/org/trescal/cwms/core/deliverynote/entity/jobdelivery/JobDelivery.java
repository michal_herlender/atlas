package org.trescal.cwms.core.deliverynote.entity.jobdelivery;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.SortComparator;

import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItemComparator;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

@Entity
public abstract class JobDelivery extends Delivery
{
	private Job job;
	private Set<JobDeliveryItem> items;

	/**
	 * @return the items
	 */
	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "delivery", fetch = FetchType.LAZY)
	@SortComparator(JobDeliveryItemComparator.class)
	@Override
	public Set<JobDeliveryItem> getItems()
	{
		return this.items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(Set<JobDeliveryItem> items)
	{
		this.items = items;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobid")
	public Job getJob()
	{
		return this.job;
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity()
	{
		return this.job;
	}

	@Override
	@Transient
	public boolean isGeneralDelivery()
	{
		return false;
	}

	public void setJob(Job job)
	{
		this.job = job;
	}
}
