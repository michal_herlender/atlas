package org.trescal.cwms.core.deliverynote.entity.deliveryitem;

import java.util.List;

import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryDTO;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemAccessoryDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirementDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.system.projection.note.NoteProjectionDTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DeliveryItemDTO {
	private Integer deliveryItemId;
	private Integer deliveryId;
	private String deliveryNumber;
	private Integer jobItemId;
	private Integer itemNo;
	private String jobno;
	private Integer tpRequirementId;
	private String accessoryFreeText;
	// Optional references populated after creation
	private DeliveryDTO delivery;
	private JobItemProjectionDTO jobItem;
	private TPRequirementDTO tpRequirement; 
	private List<DeliveryItemAccessoryDTO> accessories;
	private List<NoteProjectionDTO> notes;
	
	public DeliveryItemDTO(Integer deliveryItemId, Integer deliveryId, Integer jobItemId, Integer tpRequirementId,
		String accessoryFreeText) {
		this.deliveryItemId = deliveryItemId;
		this.deliveryId = deliveryId;
		this.jobItemId = jobItemId;
		this.tpRequirementId = tpRequirementId;
		this.accessoryFreeText = accessoryFreeText;
	}

	public DeliveryItemDTO(Integer deliveryItemId, String deliveryNumber, String jobno, Integer itemNo) {
		super();
		this.deliveryItemId = deliveryItemId;
		this.deliveryNumber = deliveryNumber;
		this.jobno = jobno;
		this.itemNo = itemNo;
	}
	
}