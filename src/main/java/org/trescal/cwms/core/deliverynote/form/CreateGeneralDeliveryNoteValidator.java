package org.trescal.cwms.core.deliverynote.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class CreateGeneralDeliveryNoteValidator extends AbstractBeanValidator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(CreateDeliveryNoteForm.class);
	}
	
	@Override
	public void validate(Object target, Errors errors)
	{
		super.validate(target, errors);
		CreateDeliveryNoteForm command = (CreateDeliveryNoteForm) target;
		if (command.getAddressid() == null)
		{
			errors.rejectValue("addrid", "error.createdeliverynoteform.addrid", null, "The delivery must have an address");
		}

		if (command.getContactid() == null)
		{
			errors.rejectValue("contactid", "error.createdeliverynoteform.contactid", null, "The delivery must have a contact");
		}
	}
}