package org.trescal.cwms.core.deliverynote.entity.courier.db;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;

@Service("CourierService")
public class CourierServiceImpl implements CourierService
{
	@Autowired
	private CourierDao courierDao;
	@Autowired
	private DeliveryService deliveryService;
	@Autowired
	private HireService hireService;
	@Autowired
	private CompanyService compnayService;
	
	public ResultWrapper editCourier(int courierid, String name, String URL)
	{
		// first find courier
		Courier c = this.get(courierid);
		// courier null?
		if (c != null)
		{
			// set new courier name
			c.setName(name);
			// url provided?
			if ((URL != null) && (URL.trim().length() > 0)
					&& !URL.equals("&nbsp;"))
			{
				c.setWebTrackURL(URL);
			}
			// update the courier
			this.updateCourier(c);
			// return successful wrapper
			return new ResultWrapper(true, "", c, null);
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "The courier could not be found", null, null);
		}
	}

	public Courier get(int id)
	{
		return this.courierDao.find(id);
	}

	public List<Courier> getAllCouriers()
	{
		return this.courierDao.findAll();
	}
	
	public Set<CourierDespatchType> getAllDespatchTypes(int courierid)
	{
		return this.get(courierid).getCdts();
	}
	
	public void insertCourier(Courier Courier)
	{
		this.courierDao.persist(Courier);
	}

	public void setCourierDao(CourierDao courierDao)
	{
		this.courierDao = courierDao;
	}

	public void updateCourier(Courier Courier)
	{
		this.courierDao.update(Courier);
	}

	@Override
	public List<Courier> getCouriersByBusinessCompany(Integer orgid) {
		return courierDao.getCouriersByBusinessCompany(orgid);
	}

	@Override
	public void updateDefaultCompanyCourier(Integer courierid, Integer coid) {
		List<Courier> companyCouriers = this.courierDao.getCouriersByBusinessCompany(coid);
		
		for (Courier courier : companyCouriers) {
			if(courier.getCourierid() == courierid) {
				courier.setDefaultCourier(true);
			}
			else if(courier.isDefaultCourier()) {
				courier.setDefaultCourier(false);
			}
		}	
	}

	@Override
	public void deleteCourier(Integer courierid) {
		Courier courier = this.get(courierid);
		if(courier != null)
			this.courierDao.remove(courier);
	}

	@Override
	public Long linkedEntitiesCount(Integer courierId) {
		return this.deliveryService.deliveryCountByCourier(courierId)+this.hireService.hireCountByCourier(courierId);
	}

	@Override
	public int addNewCourier(String courierName, String webTrackUrl, Integer businessCoid) {
		Courier courier = new Courier();
		courier.setDefaultCourier(false);
		courier.setName(courierName);
		courier.setWebTrackURL(webTrackUrl);
		Company company = this.compnayService.get(businessCoid);
		courier.setOrganisation(company);
		this.insertCourier(courier);
		return courier.getCourierid();
	}
	
	
}