package org.trescal.cwms.core.deliverynote.dto;

import java.time.LocalDate;

import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.description.SubfamilyTypology;

import lombok.Getter;

@Getter
public class DespatchItemDTO {
	private Integer jobId;
	private String jobNumber;
	private Integer jobItemId;
	private Integer jobItemNumber;
	private Integer itemStateId;
	private Integer sourceAddressId;
	private Integer transportOutOptionId;
	// All references below are to the destination unless otherwise indicated
	private Integer destAddressId;
	private Integer destSubdivId; 
	private String destSubdivName; 
	private Integer destCompanyId; 
	private String destCompanyName;
	// Instrument Model information
	private String instrumentModelName; 
	private ModelMfrType modelMfrType;
	private Integer subfamilyTypologyId;
	// Instrument information
	private String brand;
	private String modelName;
	// Job Item information
	private LocalDate dueDate;
	private Long accessoriesCount;
	
	public DespatchItemDTO(Integer jobId, String jobNumber, Integer jobItemId, Integer jobItemNumber, Integer itemStateId,
			Integer sourceAddressId, Integer transportOutOptionId, Integer destAddressId, Integer destSubdivId, String destSubdivName, 
			Integer destCompanyId, String destCompanyName, String instrumentModelName, ModelMfrType modelMfrType, Integer subfamilyTypologyId, 
			String brand, String modelName, LocalDate dueDate) {
		this.jobId = jobId;
		this.jobNumber = jobNumber;
		this.jobItemId = jobItemId;
		this.jobItemNumber = jobItemNumber;
		this.itemStateId = itemStateId;
		this.sourceAddressId = sourceAddressId;
		this.transportOutOptionId = transportOutOptionId;
		// All references below are to the destination unless otherwise indicated
		this.destAddressId = destAddressId;
		this.destSubdivId = destSubdivId; 
		this.destSubdivName = destSubdivName; 
		this.destCompanyId = destCompanyId; 
		this.destCompanyName = destCompanyName;
		// Instrument Model information
		this.instrumentModelName = instrumentModelName;
		this.modelMfrType = modelMfrType;
		this.subfamilyTypologyId = subfamilyTypologyId;
		// Instrument information
		this.brand = brand;
		this.modelName = modelName;
		// Job Item information
		this.dueDate = dueDate;
	}
	
	// constructor with the accessories count (used for tp goods out case)
	public DespatchItemDTO(Integer jobId, String jobNumber, Integer jobItemId, Integer jobItemNumber, Integer itemStateId,
			Integer sourceAddressId, Integer transportOutOptionId, Integer destAddressId, Integer destSubdivId, String destSubdivName, 
			Integer destCompanyId, String destCompanyName, String instrumentModelName, ModelMfrType modelMfrType, Integer subfamilyTypologyId, 
			String brand, String modelName, LocalDate dueDate, Long accessoriesCount) {
		this.jobId = jobId;
		this.jobNumber = jobNumber;
		this.jobItemId = jobItemId;
		this.jobItemNumber = jobItemNumber;
		this.itemStateId = itemStateId;
		this.sourceAddressId = sourceAddressId;
		this.transportOutOptionId = transportOutOptionId;
		// All references below are to the destination unless otherwise indicated
		this.destAddressId = destAddressId;
		this.destSubdivId = destSubdivId; 
		this.destSubdivName = destSubdivName; 
		this.destCompanyId = destCompanyId; 
		this.destCompanyName = destCompanyName;
		// Instrument Model information
		this.instrumentModelName = instrumentModelName;
		this.modelMfrType = modelMfrType;
		this.subfamilyTypologyId = subfamilyTypologyId;
		// Instrument information
		this.brand = brand;
		this.modelName = modelName;
		// Job Item information
		this.dueDate = dueDate;
		this.accessoriesCount = accessoriesCount;
	}

	public SubfamilyTypology getSubfamilyTypology() {
		return SubfamilyTypology.convertFromInteger(subfamilyTypologyId);
	}

}
