package org.trescal.cwms.core.deliverynote.entity.internalreturndelivery;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;

@Entity
@DiscriminatorValue("internalreturn")
public class InternalReturnDelivery extends JobDelivery {

	@Override
	@Transient
	public DeliveryType getType() {
		return DeliveryType.INTERNAL_RETURN;
	}
}
