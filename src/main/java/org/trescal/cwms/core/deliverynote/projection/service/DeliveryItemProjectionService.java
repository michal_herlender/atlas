package org.trescal.cwms.core.deliverynote.projection.service;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemAccessoryDTO;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemProjectionCommand;
import org.trescal.cwms.core.deliverynote.projection.DeliveryItemProjectionResult;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionResult;

public interface DeliveryItemProjectionService {

	/**
	 * Returns a projection list of delivery items
	 * 
	 * @param Collection<Integer> jobItemIds to load related delivery items for
	 * @param DeliveryItemProjectionCommand command providing options for data load
	 * @return DeliveryItemProjectionResult representing the results found 
	 */
	DeliveryItemProjectionResult getDeliveryItemDTOs(Collection<Integer> jobItemIds, DeliveryItemProjectionCommand command);

	/**
	 * Returns a projection list of all delivery items on the specified delivery note
	 * 
	 * @param Integer deliveryId representing the delivery to load related delivery items for
	 * @param DeliveryItemProjectionCommand command providing options for data loading
	 * @return DeliveryItemProjectionResult representing the results found 
	 */
	DeliveryItemProjectionResult getDeliveryItemDTOs(Integer deliveryId, DeliveryItemProjectionCommand command);

	/**
	 * Loads delivery item projection data for specific job item projections 
	 * The delivery items are loaded into each job item DTO
	 * No options needed yet for data loading so not supported
	 * 
	 * @param Collection<JobItemProjectionDTO> job item dtos to load delivery items for 
	 * @return DeliveryItemProjectionResult representing the results found 
	 */
	DeliveryItemProjectionResult getDTOsByJobItems(Collection<JobItemProjectionDTO> jiDtos);
	
	/**
	 * Loads job item projection data for delivery items
	 * 
	 * @param List<DeliveryItemDTO> deliveryItems containing jobItemIds for which JobItemProjectionDTOs should be loaded
	 * @param JobItemProjectionCommand command providing options for data loading
	 * 
	 * @return JobItemProjectionResult containing results (and other information) though DTOs are also correlated with each DeliveryItemDTO
	 */
	JobItemProjectionResult loadProjectionJobItems(List<DeliveryItemDTO> deliveryItems, JobItemProjectionCommand command);
	
	/**
	 * Returns a projection list of all accessories on the specified delivery note, loading into specified delivery item DTOs
	 */
	List<DeliveryItemAccessoryDTO> loadProjectionAccessoryDTOs(Integer deliveryId, List<DeliveryItemDTO> deliveryItems, Locale locale); 
}
