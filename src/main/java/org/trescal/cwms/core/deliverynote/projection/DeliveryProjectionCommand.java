package org.trescal.cwms.core.deliverynote.projection;

import java.util.Arrays;
import java.util.List;

import lombok.Getter;

@Getter
public class DeliveryProjectionCommand {
	private Integer allocatedCompanyId;
	
	private Boolean loadCreatedByContact;
	private Boolean loadCourierDespatch;
	private Boolean loadSourceSubdiv;
	private Boolean loadDestAddress;
	private Boolean loadDestContact; 
	private Boolean loadNotes;
	
	public static interface DELIVERY_LOAD_CREATED_BY_CONTACT {};
	public static interface DELIVERY_LOAD_COURIER_DESPATCH {};
	public static interface DELIVERY_LOAD_SOURCE_SUBDIV {};
	public static interface DELIVERY_LOAD_DEST_ADDRESS {};
	public static interface DELIVERY_LOAD_DEST_CONTACT {};
	public static interface DELIVERY_LOAD_NOTES {};

	/**
	 * 
	 * @param allocatedCompanyId if null, will use the allocatedCompanyId from the first Delivery result
	 * @param options
	 */
	public DeliveryProjectionCommand(Integer allocatedCompanyId, 
			Class<?>... options) {
		this.allocatedCompanyId = allocatedCompanyId;
		List<Class<?>> args = Arrays.asList(options);
		this.loadSourceSubdiv = args.contains(DELIVERY_LOAD_SOURCE_SUBDIV.class);
		this.loadCreatedByContact = args.contains(DELIVERY_LOAD_CREATED_BY_CONTACT.class);
		this.loadCourierDespatch = args.contains(DELIVERY_LOAD_COURIER_DESPATCH.class);
		this.loadDestAddress = args.contains(DELIVERY_LOAD_DEST_ADDRESS.class);
		this.loadDestContact = args.contains(DELIVERY_LOAD_DEST_CONTACT.class);
		this.loadNotes = args.contains(DELIVERY_LOAD_NOTES.class);
	}
	
}
