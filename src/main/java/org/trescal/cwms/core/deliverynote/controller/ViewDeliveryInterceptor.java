package org.trescal.cwms.core.deliverynote.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.generaldelivery.GeneralDelivery;

@Component
public class ViewDeliveryInterceptor implements HandlerInterceptor
{
	@Autowired
	private DeliveryService delServ;

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		int delid = ServletRequestUtils.getIntParameter(request, "delid", 0);
		String tab = ServletRequestUtils.getStringParameter(request, "loadtab", "deliveryitems-tab");
		Delivery delivery = this.delServ.get(delid);

		if (delivery instanceof GeneralDelivery) {
			String location = "gendelnotesummary.htm?delid=" + delid + "&loadtab=" + tab;
			response.sendRedirect(location);
			return false;
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {}
}