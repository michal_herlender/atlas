package org.trescal.cwms.core.deliverynote.entity.deliveryitem.db;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.deliverynote.entity.generaldeliveryitem.GeneralDeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.vdi.projection.PositionProjection;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_AddDeliveryItem;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_DeleteDeliveryItem;

import java.util.Collection;
import java.util.List;

@Service("DeliveryItemService")
@Slf4j
public class DeliveryItemServiceImpl extends BaseServiceImpl<DeliveryItem, Integer> implements DeliveryItemService {

	@Autowired
	private DeliveryItemDao deliveryItemDao;
	@Autowired
	private HookInterceptor_AddDeliveryItem interceptor;
	@Autowired
	private HookInterceptor_DeleteDeliveryItem interceptor_DeleteDeliveryItem;

	@Override
	protected BaseDao<DeliveryItem, Integer> getBaseDao() {
		return deliveryItemDao;
	}

	@Override
	public void addItemToDelivery(JobDeliveryItem jobDeliveryItem) {
		this.deliveryItemDao.addItemToDelivery(jobDeliveryItem);
		this.interceptor.recordAction(jobDeliveryItem);
	}

	@Override
	public void deleteDeliveryItem(DeliveryItem deliveryitem) {
		this.deliveryItemDao.remove(deliveryitem);
	}

	@Override
	public void deleteDeliveryItemById(int id) {
		DeliveryItem deliveryItem = this.findDeliveryItem(id);
		// If them item has a third party requirement associated, clear it
		// before deleting (managed by delivery item side)
		if (deliveryItem.getDelivery().getType().equals(DeliveryType.THIRDPARTY)) {
			JobDeliveryItem jobDeliveryItem = (JobDeliveryItem) deliveryItem;
			if (jobDeliveryItem.getTpRequirement() != null) {
				log.debug("Clearing requirement " + jobDeliveryItem.getTpRequirement().getId());
				jobDeliveryItem.setTpRequirement(null);
			} else {
				log.debug("No requirement on TP delivery item " + deliveryItem.getDelitemid());
			}
			this.interceptor_DeleteDeliveryItem.recordAction(jobDeliveryItem);
		} else {
			log.debug("No requirement on non-TP delivery item " + deliveryItem.getDelitemid());
		}
		this.deliveryItemDao.remove(deliveryItem);
		if (!deliveryItem.getDelivery().getType().equals(DeliveryType.THIRDPARTY)) {
			JobDeliveryItem jobDeliveryItem = (JobDeliveryItem) deliveryItem;
			this.interceptor_DeleteDeliveryItem.recordAction(jobDeliveryItem);
		}
	}

	@Override
	public DeliveryItem findDeliveryItem(int id) {
		return this.deliveryItemDao.find(id);
	}

	@Override
	public DeliveryItem findDeliveryItem(int id, Class<? extends DeliveryItem> clazz) {
		return this.deliveryItemDao.findDeliveryItem(id, clazz);
	}

	@Override
	public List<GeneralDeliveryItem> getItemsForGeneralDelivery(int delid) {
		return this.deliveryItemDao.getItemsForGeneralDelivery(delid);
	}

	@Override
	public List<JobDeliveryItem> getItemsForJobDelivery(int delid) {
		return this.deliveryItemDao.getItemsForJobDelivery(delid);
	}

	@Override
	public void insertDeliveryItem(DeliveryItem DeliveryItem) {
		this.deliveryItemDao.persist(DeliveryItem);
	}

    @Override
    public List<PositionProjection> getPositionProjectionForDeliveryNote(Integer deliveryId) {
        return deliveryItemDao.getPositionProjectionForDeliveryNote(deliveryId);
    }

	@Override
	public JobDeliveryItem findNotYetDespatchedJobDeliveryItemByInstrument(Integer plantId) {
		return this.deliveryItemDao.findNotYetDespatchedJobDeliveryItemByInstrument(plantId);
	}

	@Override
	public List<DeliveryItemDTO> getDeliveryItemDTOs(Integer deliveryId, Collection<Integer> jobItemIds) {
		return this.deliveryItemDao.getDeliveryItemDTOs(deliveryId, jobItemIds);
	}

	@Override
	public List<DeliveryItem> getItemsForJobDeliveryBydDelNo(String deliveryNo) {
		return this.deliveryItemDao.getItemsForJobDeliveryBydDelNo(deliveryNo);
	}
	
}