package org.trescal.cwms.core.hire.form;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.hire.dto.HireBasketItemDto;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor @NoArgsConstructor
public class HireForm
{
	private Integer addrid;
	private List<Integer> basketCalTypeIds;
	private List<String> basketHireCosts;
	private List<Integer> basketIds;
	private List<String> basketUKASCosts;
	private List<Integer> chItemCalTypeIds;
	private List<String> chItemDescs;
	private List<String> chItemHireCosts;
	private List<String> chItemPlants;
	private List<String> chItemSerials;
	private List<String> chItemUKASCosts;
	private Integer coid;
	private String currencyCode;
	private String freeAddr1;
	private String freeAddr2;
	private String freeCompany;
	private String freeCounty;
	private boolean freehandDelivery;

	private String freeName;
	private String freePostcode;
	private String freeTown;
	private Hire hire;
	private Boolean itemsReadyForHire;
	private Integer personid;
	private Integer subdivid;

	private List<HireBasketItemDto> items ;

	@NotNull
	public Integer getPersonid()
	{
		return this.personid;
	}


	public List<Integer> getHireInstPlantIds(){
		if(this.hire==null || this.hire.getItems() == null) return Collections.emptyList();
		return this.hire.getItems().stream().map(HireItem::getHireInst).filter(Objects::nonNull)
				.map(HireInstrument::getInst).filter(Objects::nonNull)
				.map(Instrument::getPlantid).collect(Collectors.toList());
	}

}
