package org.trescal.cwms.core.hire.entity.hiresuspenditem;

import java.util.Comparator;

/**
 * @author stuarth
 */
public class HireSuspendItemComparator implements Comparator<HireSuspendItem>
{
	public int compare(HireSuspendItem hsi1, HireSuspendItem hsi2)
	{
		if ((hsi1.getEndDate() != null) && (hsi2.getEndDate() != null)) {
			if (hsi1.getEndDate().equals(hsi2.getEndDate())) {
				return Integer.compare(hsi2.getId(), hsi1.getId());
			} else {
				return hsi2.getEndDate().compareTo(hsi1.getEndDate());
			}
		} else if ((hsi1.getEndDate() != null) && (hsi2.getEndDate() == null)) {
			return 1;
		} else if ((hsi1.getEndDate() == null) && (hsi2.getEndDate() != null)) {
			return -1;
		} else {
			return Integer.compare(hsi1.getId(), hsi2.getId());
		}
	}
}