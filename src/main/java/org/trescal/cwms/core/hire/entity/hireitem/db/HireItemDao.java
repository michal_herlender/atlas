package org.trescal.cwms.core.hire.entity.hireitem.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.hire.dto.HireSearchResultItem;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;

public interface HireItemDao extends BaseDao<HireItem, Integer> {
	
	HireItem findHireItemDWR(int id);
	
	List<HireItem> getAllHireItemsOnHire(int hireid);
	
	List<HireItem> getAllItemsCurrentlyOnHire();
	
	List<HireItem> getAllOffHiredItemsOnHire(int hireid);

    List<HireSearchResultItem> findByHireId(Collection<Integer> ids);
}