package org.trescal.cwms.core.hire.entity.hireitemaccessory.db;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.dto.HireAccessoryDto;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.hire.entity.hireaccessory.db.HireAccessoryService;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.hire.entity.hireitem.db.HireItemService;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessory;

import java.util.*;

public class HireItemAccessoryServiceImpl implements HireItemAccessoryService
{
	private HireAccessoryService haccessServ;
	private HireItemAccessoryDao hireItemAccessoryDao;
	private HireService hireServ;
	private HireItemService hitemServ;

	public ResultWrapper assignAccessoriesToHireItem(Integer[] accessoryIds, int hireItemId)
	{
		// get hire item to edit accessories for
		HireItem hi = (HireItem) this.hitemServ.findHireItemDWR(hireItemId).getResults();
		// hire item found?
		if (hi != null)
		{
			try
			{
				List<Integer> accessoryIdList = new ArrayList<Integer>();
				accessoryIdList.addAll(Arrays.asList(accessoryIds));
				Map<Integer, HireItemAccessory> alreadyOnHireItem = new HashMap<Integer, HireItemAccessory>();

				// populate map of hire accessories already on the hire item
				for (HireItemAccessory hia : hi.getAccessories())
				{
					alreadyOnHireItem.put(hia.getHireAccessory().getHireAccessoryId(), hia);
				}

				// for each of the checked hire accessories
				for (int i : accessoryIds)
				{
					// if the accessory isn't already on the item
					if (!alreadyOnHireItem.containsKey(i))
					{
						// assign it hire item
						HireItemAccessory hia = new HireItemAccessory();
						hia.setHireItem(hi);
						hia.setHireAccessory(this.haccessServ.findEagerHireAccessory(i));
						hia.setReturned(false);

						this.insertHireItemAccessory(hia);
						hi.getAccessories().add(hia);
					}
				}

				// for each of the hire accessories on the hire item
				for (Integer i : alreadyOnHireItem.keySet())
				{
					// if the hire accessory has been unchecked
					if (!accessoryIdList.contains(i))
					{
						// remove it from hire item
						hi.getAccessories().remove(alreadyOnHireItem.get(i));
						this.deleteHireItemAccessory(alreadyOnHireItem.get(i));
					}
				}

				return new ResultWrapper(true, null, hi, null);
			}
			catch (Exception e)
			{
				return new ResultWrapper(false, e.getMessage());
			}
		}
		else
		{
			return new ResultWrapper(false, "Hire item could not be found", null, null);
		}
	}

	public void deleteHireItemAccessory(HireItemAccessory hia)
	{
		this.hireItemAccessoryDao.remove(hia);
	}

	public HireItemAccessory findHireItemAccessory(int id)
	{
		return this.hireItemAccessoryDao.find(id);
	}

	public List<HireItemAccessory> getAllHireItemAccessorys()
	{
		return this.hireItemAccessoryDao.findAll();
	}

	public void insertHireItemAccessory(HireItemAccessory hia)
	{
		this.hireItemAccessoryDao.persist(hia);
	}

	public void saveOrUpdateHireItemAccessory(HireItemAccessory hia)
	{
		this.hireItemAccessoryDao.saveOrUpdate(hia);
	}

	public void setHaccessServ(HireAccessoryService haccessServ)
	{
		this.haccessServ = haccessServ;
	}

	public void setHireItemAccessoryDao(HireItemAccessoryDao hireItemAccessoryDao)
	{
		this.hireItemAccessoryDao = hireItemAccessoryDao;
	}

	public void setHireServ(HireService hireServ)
	{
		this.hireServ = hireServ;
	}

	public void setHitemServ(HireItemService hitemServ)
	{
		this.hitemServ = hitemServ;
	}

	public void updateHireItemAccessory(HireItemAccessory hia)
	{
		this.hireItemAccessoryDao.update(hia);
	}

	public ResultWrapper validateHireItemAccessories(int hireId) {
		// new list of hire item accessories
		List<HireItemAccessory> hitemAccs = new ArrayList<>();
		// find hire
		Hire hire = this.hireServ.get(hireId);
		// hire null?
		if (hire != null) {
			// check all hire items
			for (HireItem hi : hire.getItems()) {
				// check all hire item accessories on item
				for (HireItemAccessory hia : hi.getAccessories())
				{
					// hire accessory active and available?
					if (!hia.getHireAccessory().isActive()
							|| !hia.getHireAccessory().getStatus().getDefaultStatus())
					{
						// add hire item accessory to list
						hitemAccs.add(hia);
					}
				}
			}
			// any items failed validation?
			if (hitemAccs.size() > 0) {
				// return un-successful wrapper
				return new ResultWrapper(false, "", hitemAccs
					.stream().map(HireAccessoryDto::fromHireAccessory), null);
			}
			else
			{
				// return successful wrapper
				return new ResultWrapper(true, "", null, null);
			}
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "Hire could not be found", null, null);
		}
	}
}