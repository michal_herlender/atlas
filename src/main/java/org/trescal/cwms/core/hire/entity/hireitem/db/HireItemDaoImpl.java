package org.trescal.cwms.core.hire.entity.hireitem.db;

import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.hire.dto.HireSearchResultItem;
import org.trescal.cwms.core.hire.entity.hire.Hire_;
import org.trescal.cwms.core.hire.entity.hireaccessory.HireAccessory_;
import org.trescal.cwms.core.hire.entity.hirecrossitem.HireCrossItem_;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem_;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessory_;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.tools.StringJpaUtils;

import lombok.val;

@Repository("HireItemDao")
public class HireItemDaoImpl extends BaseDaoImpl<HireItem, Integer> implements HireItemDao {
	
	@Override
	protected Class<HireItem> getEntity() {
		return HireItem.class;
	}
	
	public HireItem findHireItemDWR(int id) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(HireItem.class);
			val item = cq.from(HireItem.class);
			item.fetch(HireItem_.hireInst, JoinType.LEFT)
					.fetch(HireInstrument_.inst,JoinType.LEFT);
			item.fetch(HireItem_.accessories,JoinType.LEFT)
					.fetch(HireItemAccessory_.hireAccessory,JoinType.LEFT)
					.fetch(HireAccessory_.status,JoinType.LEFT);
			cq.where(cb.equal(item.get(HireItem_.id),id));
			cq.select(item);
			return  cq;
		}).orElse(null);
	}
	
	public List<HireItem> getAllHireItemsOnHire(int hireid) {
	    return getResultList(cb-> {
	    	val cq = cb.createQuery(HireItem.class);
	    	val item = cq.from(HireItem.class);
	    	cq.where(cb.equal(item.get(HireItem_.hire),hireid));
	    	cq.orderBy(cb.asc(item.get(HireItem_.itemno)));
	    	return cq;
		});
	}
	
	public List<HireItem> getAllItemsCurrentlyOnHire() {
	    return getResultList(cb -> {
	    	val cq = cb.createQuery(HireItem.class);
	    	val item  = cq.from(HireItem.class);
	    	cq.where(cb.and(
	    			cb.isFalse(item.join(HireItem_.hire).get(Hire_.enquiry)),
					cb.isFalse(item.get(HireItem_.offHire))
			));
	    	return cq;
		});
	}
	
	public List<HireItem> getAllOffHiredItemsOnHire(int hireid) {
	    return getResultList(cb -> {
	    	val cq = cb.createQuery(HireItem.class);
	    	val item = cq.from(HireItem.class);
	    	cq.where(cb.and(
	    			cb.equal(item.get(HireItem_.hire),hireid),
					cb.isTrue(item.get(HireItem_.offHire))
			));
	    	cq.orderBy(cb.asc(item.get(HireItem_.itemno)));
	    	return cq;
		});
	}


	@Override
	public List<HireSearchResultItem> findByHireId(Collection<Integer> ids){
		return getResultList(cb -> {
			val cq = cb.createQuery(HireSearchResultItem.class);
			val hireItem = cq.from(HireItem.class);
			val instrument = hireItem
					.join(HireItem_.hireInst, JoinType.LEFT)
					.join(HireInstrument_.inst,JoinType.LEFT);
			val cross = hireItem.join(HireItem_.hireCrossItem,JoinType.LEFT);
			val instModel = instrument.join(Instrument_.model,JoinType.LEFT);
			val modeDescription = instModel.join(InstrumentModel_.description,JoinType.LEFT);
			val mfr = instrument.join(Instrument_.mfr,JoinType.LEFT);
			cq.where(hireItem.get(HireItem_.hire).in(ids));
			cq.select(cb.construct(HireSearchResultItem.class,
					hireItem.get(HireItem_.hire).get(Hire_.id),
					hireItem.get(HireItem_.itemno),
					cb.coalesce(getFullInstrumentModelName(cb,instModel,modeDescription,mfr,instrument.get(Instrument_.modelname)),cross.get(HireCrossItem_.description)),
					cb.coalesce(instrument.get(Instrument_.serialno),cross.get(HireCrossItem_.serialno)),
					cb.coalesce(instrument.get(Instrument_.plantno),cross.get(HireCrossItem_.plantno)),
					joinTranslation(cb,hireItem.join(HireItem_.calType,JoinType.LEFT).join(CalibrationType_.serviceType,JoinType.LEFT),
							ServiceType_.shortnameTranslation, LocaleContextHolder.getLocale()),
					hireItem.get(HireItem_.offHire),
					hireItem.get(HireItem_.suspended)
					));
			return cq;
		});
	}

	private Expression<String> getFullInstrumentModelName(CriteriaBuilder cb, Join<Instrument, InstrumentModel> model, Join<InstrumentModel, Description> modelDescription,
														  Join<Instrument, Mfr> instrumentMfr, Path<String> instrumentModelName)  {
		val modelNameTranslation  = joinTranslation(cb,model, InstrumentModel_.nameTranslations, LocaleContextHolder.getLocale());
		val typology = modelDescription.get(Description_.typology);
		val convertedTypology =  cb.<String>selectCase().when(cb.equal(typology,cb.literal(1)),cb.literal("(BM)"))
				.when(cb.equal(typology,cb.literal("2")),cb.literal("(SFC)"))
				.otherwise("");
		val genericPredicate = cb.and(cb.equal(model.get(InstrumentModel_.modelMfrType), ModelMfrType.MFR_GENERIC)
				,cb.isNotNull(instrumentMfr.get(Mfr_.name)),cb.isFalse(instrumentMfr.get(Mfr_.genericMfr)));
		val generic = cb.<String>selectCase().when(genericPredicate,instrumentMfr.get(Mfr_.name)).otherwise("");
		val modelName= model.get(InstrumentModel_.model);
		val modelNamePredicate = cb.and(cb.or(cb.isNull(modelName),cb.equal(modelName,"/")),cb.isNotNull(instrumentModelName),cb.notEqual(instrumentModelName,"/"));
		val generatedModelName = cb.<String>selectCase().when(modelNamePredicate,instrumentModelName).otherwise("");
		return StringJpaUtils.trimAndConcatWithWhitespace(modelNameTranslation,convertedTypology)
				.append(generic)
				.append(generatedModelName).apply(cb);
	}
}