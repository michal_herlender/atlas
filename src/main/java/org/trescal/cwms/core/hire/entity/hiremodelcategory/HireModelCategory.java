package org.trescal.cwms.core.hire.entity.hiremodelcategory;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.hire.entity.hirecategory.HireCategory;
import org.trescal.cwms.core.hire.entity.hiremodelincategory.HireModelInCategory;

@Entity
@Table(name = "hiremodelcategory")
public class HireModelCategory
{
	private HireCategory hireCategory;
	private Set<HireModelInCategory> hireModelInCategories;
	private int id;
	private int modelcode;
	private String plantcode;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "categoryid")
	public HireCategory getHireCategory()
	{
		return this.hireCategory;
	}

	/**
	 * @return the hire model categories
	 */
	@OneToMany(mappedBy = "hireModelCategory", cascade = CascadeType.ALL)
	public Set<HireModelInCategory> getHireModelInCategories()
	{
		return this.hireModelInCategories;
	}

	@Transient
	public String getHireModelReferenceNo()
	{
		return this.hireCategory.getHireSection().getSectioncode() + "."
				+ this.hireCategory.getCategorycode() + "."
				+ this.getModelcode();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Column(name = "modelcode", length = 4)
	public int getModelcode()
	{
		return this.modelcode;
	}

	@NotNull
	@Length(min = 1, max = 3)
	@Column(name = "plantcode", length = 3, unique = false)
	public String getPlantcode()
	{
		return this.plantcode;
	}

	public void setHireCategory(HireCategory hireCategory)
	{
		this.hireCategory = hireCategory;
	}

	public void setHireModelInCategories(Set<HireModelInCategory> hireModelInCategories)
	{
		this.hireModelInCategories = hireModelInCategories;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setModelcode(int modelcode)
	{
		this.modelcode = modelcode;
	}

	public void setPlantcode(String plantcode)
	{
		this.plantcode = plantcode;
	}

}
