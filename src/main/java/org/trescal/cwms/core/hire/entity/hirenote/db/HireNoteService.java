package org.trescal.cwms.core.hire.entity.hirenote.db;

import java.util.List;

import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hirenote.HireNote;

/**
 * Interface for accessing and manipulating {@link HireNote} entities.
 * 
 * @author stuart
 */
public interface HireNoteService
{
	/**
	 * Deletes the given {@link HireNote} from the database.
	 * 
	 * @param hirenote the {@link HireNote} to delete.
	 */
	void deleteHireNote(HireNote hirenote);

	/**
	 * Deletes the {@link HireNote} with the given ID from the database.
	 * 
	 * @param id the ID of the {@link HireNote} to delete.
	 */
	void deleteHireNoteById(int id);

	/**
	 * Returns the {@link HireNote} with the given ID.
	 * 
	 * @param id the {@link HireNote} ID.
	 * @return the {@link HireNote}
	 */
	HireNote findHireNote(int id);

	/**
	 * Returns all hire notes for the {@link Hire} with the given ID that are
	 * currently active.
	 * 
	 * @param hireid the ID of the {@link Hire} to get notes for.
	 * @return the {@link List} of {@link HireNote} entities.
	 */
	List<HireNote> getActiveHireNotesForHire(int hireid);

	/**
	 * Returns all {@link HireNote} entities stored in the database.
	 * 
	 * @return the {@link List} of {@link HireNote} entities.
	 */
	List<HireNote> getAllHireNotes();

	/**
	 * Inserts the given {@link HireNote} into the database.
	 * 
	 * @param hirenote the {@link HireNote} to insert.
	 */
	void insertHireNote(HireNote hirenote);

	/**
	 * Inserts the given {@link HireNote} if it is not currently stored in the
	 * database, and otherwise updates the existing {@link HireNote}.
	 * 
	 * @param hirenote the {@link HireNote} to insert or update.
	 */
	void saveOrUpdateHireNote(HireNote hirenote);

	/**
	 * Updates the given {@link HireNote} in the database.
	 * 
	 * @param hirenote the {@link HireNote} to update.
	 */
	void updateHireNote(HireNote hirenote);
}