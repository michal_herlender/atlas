package org.trescal.cwms.core.hire.entity.hiresuspenditem.db;

import io.vavr.control.Either;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.entity.hiresuspenditem.HireSuspendItem;

import java.time.LocalDate;
import java.util.List;

public interface HireSuspendItemService {
    Either<String, Boolean> completeHireItemSuspension(int id, LocalDate enddate);

    void deleteHireSuspendItem(HireSuspendItem hsi);

    HireSuspendItem findHireSuspendItem(int id);

    List<HireSuspendItem> getAllHireSuspendItems();

    void insertHireSuspendItem(HireSuspendItem hsi);

    void saveOrUpdateHireSuspendItem(HireSuspendItem hsi);

    ResultWrapper suspendAllHireItems(String startdate, String enddate);

    Either<String, Boolean> suspendHireItem(int id, LocalDate startdate);

    void updateHireSuspendItem(HireSuspendItem hsi);
}