package org.trescal.cwms.core.hire.entity.hiremodelcategory.db;

import java.util.List;

import org.trescal.cwms.core.hire.entity.hiremodelcategory.HireModelCategory;

public interface HireModelCategoryService
{
	void deleteHireModelCategory(HireModelCategory hmc);

	HireModelCategory findHireModelCategory(int id);

	List<HireModelCategory> getAllHireModelCategorys();

	List<String> getDistinctPlantCodeList();

	int getNextModelCodeForCategory(int categoryid);

	void insertHireModelCategory(HireModelCategory hmc);

	void saveOrUpdateHireModelCategory(HireModelCategory hmc);

	void updateHireModelCategory(HireModelCategory hmc);
}