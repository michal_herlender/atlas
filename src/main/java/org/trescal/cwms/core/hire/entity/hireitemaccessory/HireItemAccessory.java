package org.trescal.cwms.core.hire.entity.hireitemaccessory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.hire.entity.hireaccessory.HireAccessory;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;

@Entity
@Table(name = "hireitemaccessory")
@NoArgsConstructor
public class HireItemAccessory extends Auditable
{
	public HireItemAccessory(HireAccessory hireAccessory, HireItem hireItem, Boolean returned){
		super();
		this.hireAccessory = hireAccessory;
		this.hireItem = hireItem;
		this.returned = returned;
	}

	private HireAccessory hireAccessory;
	private HireItem hireItem;
	private int id;
	private boolean returned;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hireaccessoryid")
	public HireAccessory getHireAccessory()
	{
		return this.hireAccessory;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hireitemid")
	public HireItem getHireItem()
	{
		return this.hireItem;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@Column(name = "returned", nullable=false, columnDefinition="bit")
	public boolean isReturned()
	{
		return this.returned;
	}

	public void setHireAccessory(HireAccessory hireAccessory)
	{
		this.hireAccessory = hireAccessory;
	}

	public void setHireItem(HireItem hireItem)
	{
		this.hireItem = hireItem;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setReturned(boolean returned)
	{
		this.returned = returned;
	}

}
