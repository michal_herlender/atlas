package org.trescal.cwms.core.hire.dto;

import lombok.Value;

@Value(staticConstructor = "of")
public class HireShortDto {
    Integer id;
    String hireNo;

}
