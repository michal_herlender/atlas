package org.trescal.cwms.core.hire.entity.hiresection.db;

import java.util.List;

import org.trescal.cwms.core.hire.entity.hiresection.HireSection;

public interface HireSectionService
{
	void deleteHireSection(HireSection hs);

	HireSection findHireSection(int id);

	List<HireSection> getAllHireSections();

	void insertHireSection(HireSection hs);

	void saveOrUpdateHireSection(HireSection hs);

	void updateHireSection(HireSection hs);
}