package org.trescal.cwms.core.hire.entity.hiresuspenditem;

import org.hibernate.annotations.Type;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name = "hiresuspenditem")
public class HireSuspendItem extends Auditable {
	private LocalDate endDate;
	private HireItem hireItem;
	private int id;
	private LocalDate startDate;

	@Transient
	public long getDaysSuspended() {
		// suspension days
		long suspensiondays = 0;
		// check for item suspensions
		if ((this.getStartDate() != null) && (this.getEndDate() != null)) {
            // accumulate suspension days
            suspensiondays = ChronoUnit.DAYS.between(startDate, endDate);
        } else if ((this.getStartDate() != null) && (this.getEndDate() == null)) {
            // accumulate suspension days
            suspensiondays = ChronoUnit.DAYS.between(startDate, LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
        }
        // return number of days suspended
        return suspensiondays;
    }

    @Column(name = "enddate", columnDefinition = "date")
    public LocalDate getEndDate() {
        return this.endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hireitemid")
	public HireItem getHireItem() {
		return this.hireItem;
	}

	public void setHireItem(HireItem hireItem) {
		this.hireItem = hireItem;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "startdate", columnDefinition = "date")
    public LocalDate getStartDate() {
        return this.startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

}
