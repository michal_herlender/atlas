package org.trescal.cwms.core.hire.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.filenamingservice.FileNamingService;
import org.trescal.cwms.core.documents.filenamingservice.HireContractFileNamingService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Controller @IntranetController
@SessionAttributes({Constants.HTTPSESS_NEW_FILE_LIST})
public class HireContractBirtDocumentController {
	@Autowired
	private DocumentService documentService; 
	@Autowired
	private HireService hireService;
	
	@RequestMapping(value="hirecontractbirtdocument.htm")
	public String handleRequest(Locale locale,
			@RequestParam(value="id", required=false, defaultValue="0") Integer hireId,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList) throws Exception {
		Hire hire = this.hireService.get(hireId);
		FileNamingService fnService = new HireContractFileNamingService(hire);
		Document doc = documentService.createBirtDocument(hireId, BaseDocumentType.HIRE_CONTRACT, locale, Component.HIRE, fnService);
		documentService.addFileNameToSession(doc, sessionNewFileList);
		return "redirect:viewhire.htm?id=" + hireId + "&loadtab=files-tab";
	}
}
