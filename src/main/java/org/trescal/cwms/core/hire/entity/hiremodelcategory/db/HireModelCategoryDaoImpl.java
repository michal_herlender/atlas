package org.trescal.cwms.core.hire.entity.hiremodelcategory.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.hire.entity.hiremodelcategory.HireModelCategory;

@Repository("HireModelCategoryDao")
public class HireModelCategoryDaoImpl extends BaseDaoImpl<HireModelCategory, Integer> implements HireModelCategoryDao {
	
	@Override
	protected Class<HireModelCategory> getEntity() {
		return HireModelCategory.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getDistinctPlantCodeList() {
		Criteria crit = getSession().createCriteria(HireModelCategory.class);
		crit.setProjection(Projections.distinct(Projections.projectionList().add(Projections.property("plantcode"))));
		crit.addOrder(Order.asc("plantcode"));
		return crit.list();
	}
	
	public int getNextModelCodeForCategory(int categoryid) {
		Criteria criteria = getSession().createCriteria(HireModelCategory.class);
		Criteria categoryCrit = criteria.createCriteria("hireCategory");
		categoryCrit.add(Restrictions.idEq(categoryid));
		@SuppressWarnings("unchecked")
		List<HireModelCategory> hmcList = criteria.list();
		int highest = 0;
		if (hmcList.size() > 0)
			for (HireModelCategory hmc : hmcList)
				if (hmc.getModelcode() > highest)
					highest = hmc.getModelcode();
		return (highest + 1);
	}
}