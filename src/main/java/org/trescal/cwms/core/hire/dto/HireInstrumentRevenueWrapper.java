package org.trescal.cwms.core.hire.dto;

import java.math.BigDecimal;

import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;

public class HireInstrumentRevenueWrapper
{
	private Long daysOnHire;
	private HireInstrument hi;
	private BigDecimal revenue;
	private Long timesHired;

	public HireInstrumentRevenueWrapper(Long daysOnHire, Long timesHired, HireInstrument hi, BigDecimal revenue)
	{
		this.daysOnHire = daysOnHire;
		this.timesHired = timesHired;
		this.hi = hi;
		this.revenue = revenue;
	}

	public Long getDaysOnHire()
	{
		return this.daysOnHire;
	}

	public HireInstrument getHi()
	{
		return this.hi;
	}

	public BigDecimal getRevenue()
	{
		return this.revenue;
	}

	public Long getTimesHired()
	{
		return this.timesHired;
	}

	public void setDaysOnHire(Long daysOnHire)
	{
		this.daysOnHire = daysOnHire;
	}

	public void setHi(HireInstrument hi)
	{
		this.hi = hi;
	}

	public void setRevenue(BigDecimal revenue)
	{
		this.revenue = revenue;
	}

	public void setTimesHired(Long timesHired)
	{
		this.timesHired = timesHired;
	}

}
