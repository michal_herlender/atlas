package org.trescal.cwms.core.hire.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.hire.entity.hirestatus.HireStatus;
import org.trescal.cwms.core.hire.form.HireHomeForm;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.TranslationUtils;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Displays form to search for {@link HireItem} entities and displays their
 * results.
 * 
 * @author stuart
 */
@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV,Constants.SESSION_ATTRIBUTE_USERNAME})
public class HireHomeController
{
	@Autowired
	private ContactService conServ;
	@Autowired
	private HireService hireServ;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;

	@ModelAttribute("form")
	protected HireHomeForm formBackingObject() throws Exception
	{
		// create new hire form
		HireHomeForm form = new HireHomeForm();
		return form;
	}
	
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception
	{
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
	}
	
	
	@RequestMapping(value="/hirehome.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute("form") HireHomeForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception
	{
		Subdiv subdiv = this.subdivService.get(subdivDto.getKey());
		// get list of outstanding enquiries requiring attention
		form.setEnquiries(this.hireServ.getAllHireEnquiries(subdiv));
		// get list of current hire contracts
		form.setContracts(this.hireServ.getAllActiveHireContracts(subdiv));
		// get list of current hire awaiting accounts varification
		form.setAwaitingAccountsVarification(this.hireServ.getEnquiriesAwaitingAccountsVarification(subdiv));
		// get list of hires at awaiting invoice status
		form.setAwaitingInvoice(this.hireServ.getAllActiveHireContractsAwaitingInvoice(subdiv));
		// load all hire status
		form.setStatusList(this.statusServ.getAllStatuss(HireStatus.class).stream()
				.map(st -> new KeyValue<>(st.getStatusid(), TranslationUtils.getBestTranslation(st.getNametranslations())
						.orElse(""))
				).collect(Collectors.toList())
		);
		// create map
		Map<String, Object> model = new HashMap<String, Object>();
		// user has verification permission
		boolean canVerify = false;
		Contact contact = this.userService.get(username).getCon();
		// member of accounts or admin
		if (this.conServ.contactBelongsToDepartmentOrIsAdmin(contact, DepartmentType.ACCOUNTS, subdiv.getComp()))
		{
			canVerify = true;
		}
		model.put("canVerifyHire", canVerify);
		return new ModelAndView("trescal/core/hire/hirehome", model);
	}
}