package org.trescal.cwms.core.hire.entity.hireaccessory;

import java.util.Comparator;

/**
 * comparator class for sorting Hire Accessories by item desciption ascending
 * 
 * @author stuarth
 */
public class HireAccessoryComparator implements Comparator<HireAccessory>
{
	public int compare(HireAccessory hireAcc1, HireAccessory hireAcc2)
	{
		if (((hireAcc1.getItem() != null) && !hireAcc1.getItem().isEmpty())
				&& ((hireAcc2.getItem() != null) && !hireAcc2.getItem().isEmpty()))
		{
			if (hireAcc1.getItem().equalsIgnoreCase(hireAcc2.getItem()))
			{
				Integer id1 = hireAcc1.getHireAccessoryId();
				Integer id2 = hireAcc2.getHireAccessoryId();

				return id2.compareTo(id1);
			}
			else
			{
				return hireAcc1.getItem().compareTo(hireAcc2.getItem());
			}
		}
		else
		{
			Integer id1 = hireAcc1.getHireAccessoryId();
			Integer id2 = hireAcc2.getHireAccessoryId();

			return id2.compareTo(id1);
		}
	}
}