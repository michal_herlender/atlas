package org.trescal.cwms.core.hire.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.deliverynote.entity.freehandcontact.FreehandContact;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

/**
 * Generic {@link Hire} validator used for adding as well as editing
 * {@link Hire} entities.
 * 
 * @author Stuart
 */
@Component
public class HireValidator extends AbstractBeanValidator
{	
	public boolean supports(Class<?> clazz)
	{
		return clazz.isAssignableFrom(HireForm.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		HireForm form = (HireForm) target;

		FreehandContact fc = form.getHire().getFreehandContact();

		// make sure a hire date is input
		if (form.getHire().getHireDate() == null)
		{
			errors.rejectValue("hire.hireDate", "", null, "Must specify a hire date");
		}

		if (form.getAddrid() == null)
		{
			errors.rejectValue("addrid", "error.form.addrid", null, "A system address must be specified for the hire");
		}
		if (form.getPersonid() == null)
		{
			errors.rejectValue("personid", "error.form.personid", null, "A system contact must be specified for the hire");
		}
		// freehand contact null?
		if (fc != null)
		{
			if (fc.getContact().length() > 100)
			{
				errors.rejectValue("hire.freehandContact.contact", "", null, "The alternate delivery contact max length is 100 characters");
			}
			if (fc.getCompany().length() > 100)
			{
				errors.rejectValue("hire.freehandContact.company", "", null, "The alternate delivery company max length is 100 characters");
			}
			// validate optional delivery address length
			if (fc.getAddress().length() > 300)
			{
				errors.rejectValue("addr1", "", null, "The alternate delivery address max length is 300 characters");
			}
		}

		if ((form.getCurrencyCode() == null)
				|| form.getCurrencyCode().trim().equals(""))
		{
			errors.rejectValue("currencyCode", "", null, "A currency must be specified");
		}

		// Perform JPA validations
		super.validate(form, errors);
	}
}