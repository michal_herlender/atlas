package org.trescal.cwms.core.hire.entity.hirecategory;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.hire.entity.hiremodelcategory.HireModelCategory;
import org.trescal.cwms.core.hire.entity.hiresection.HireSection;

@Entity
@Table(name = "hirecategory")
public class HireCategory
{
	private int categorycode;
	private String categoryname;
	private Set<HireModelCategory> hireModelCategories;
	private HireSection hireSection;
	private int id;

	@Length(min = 0, max = 4)
	@Column(name = "categorycode", length = 4)
	public int getCategorycode()
	{
		return this.categorycode;
	}

	@Length(min = 0, max = 100)
	@Column(name = "categoryname", length = 100, nullable = false)
	public String getCategoryname()
	{
		return this.categoryname;
	}

	@OneToMany(mappedBy = "hireCategory", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<HireModelCategory> getHireModelCategories()
	{
		return this.hireModelCategories;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sectionid")
	public HireSection getHireSection()
	{
		return this.hireSection;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	public void setCategorycode(int categorycode)
	{
		this.categorycode = categorycode;
	}

	public void setCategoryname(String categoryname)
	{
		this.categoryname = categoryname;
	}

	public void setHireModelCategories(Set<HireModelCategory> hireModelCategories)
	{
		this.hireModelCategories = hireModelCategories;
	}

	public void setHireSection(HireSection hireSection)
	{
		this.hireSection = hireSection;
	}

	public void setId(int id)
	{
		this.id = id;
	}

}
