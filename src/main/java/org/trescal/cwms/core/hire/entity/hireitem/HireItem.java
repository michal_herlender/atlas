package org.trescal.cwms.core.hire.entity.hireitem;

import org.hibernate.annotations.SortComparator;
import org.hibernate.validator.constraints.Length;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hirecrossitem.HireCrossItem;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessory;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessoryComparator;
import org.trescal.cwms.core.hire.entity.hirereplacedinstrument.HireReplacedInstrument;
import org.trescal.cwms.core.hire.entity.hiresuspenditem.HireSuspendItem;
import org.trescal.cwms.core.hire.entity.hiresuspenditem.HireSuspendItemComparator;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "hireitem")
@Setter
public class HireItem extends PricingItem {

	private Integer id;
	private Set<HireItemAccessory> accessories;
	private CalibrationType calType;
	private Hire hire;
	private BigDecimal hireCost;
	private HireCrossItem hireCrossItem;
	private HireInstrument hireInst;
	private boolean offHire;
	private String offHireCondition;
	private LocalDate offHireDate;
	private Integer offHireDays;
	private Set<HireReplacedInstrument> replacedInsts;
	private boolean suspended;
	private Set<HireSuspendItem> suspenditems;
	private BigDecimal ukasCalCost;

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	public Integer getId() {
		return id;
	}

	@Override
	@Transient
	public Set<Cost> getCosts() {
		return Collections.emptySet();
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "hireItem")
	@SortComparator(HireItemAccessoryComparator.class)
	public Set<HireItemAccessory> getAccessories() {
		return this.accessories == null ? new TreeSet<>(new HireItemAccessoryComparator()) : this.accessories;
	}

	@Override
	@Transient
	public PricingItem getBaseUnit() {
		return null;
	}

	@ManyToOne
	@JoinColumn(name = "caltypeid")
	public CalibrationType getCalType() {
		return this.calType;
	}

	@Transient
	public long getDaysOnHire() {
		// get hire to find out hire date
		Hire hire = this.getHire();
		// get hire date and current date
		LocalDate startdate = hire.getHireDate();
		LocalDate currentdate = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		// initialise number of days
		long days = 0;
		// check hire date is before today?
		if (startdate.isBefore(currentdate)) {
			// calculate number of days on hire
			days = ChronoUnit.DAYS.between(startdate, currentdate) + 1;
		}
		// return number of days on hire (minus suspension days)
		return (days - this.getDaysSuspended());
	}

	@Transient
	public int getDaysSuspended() {
		// suspension days
		int suspensiondays = 0;
		// check for item suspensions
		if ((this.getSuspenditems() != null) && (this.getSuspenditems().size() > 0)) {
			// check all hire suspend items
			for (HireSuspendItem hsi : this.getSuspenditems()) {
				// accumulate suspension days
				suspensiondays += hsi.getDaysSuspended();
			}
		}
		// return number of days suspended
		return suspensiondays;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hireid")
	public Hire getHire() {
		return this.hire;
	}

	@Column(name = "hirecost", precision = 10, scale = 2)
	public BigDecimal getHireCost() {
		return this.hireCost;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "itemid")
	public HireCrossItem getHireCrossItem() {
		return this.hireCrossItem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hireinstid")
	public HireInstrument getHireInst() {
		return this.hireInst;
	}

	@Override
	@Transient
	public Set<? extends PricingItem> getModules() {
		return null;
	}

	@Length(max = 100)
	@Column(name = "offhirecondition", length = 100)
	public String getOffHireCondition() {
		return this.offHireCondition;
	}

	public void setOffHireCondition(String offHireCondition) {
		this.offHireCondition = offHireCondition;
	}

	@Column(name = "offhiredate", columnDefinition = "date")
	public LocalDate getOffHireDate() {
		return this.offHireDate;
	}

	@OneToMany(mappedBy = "hireItem", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<HireReplacedInstrument> getReplacedInsts() {
		return this.replacedInsts;
	}

	@OneToMany(mappedBy = "hireItem", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(HireSuspendItemComparator.class)
	public Set<HireSuspendItem> getSuspenditems() {
		return this.suspenditems;
	}

	@Column(name = "offhiredays")
	public Integer getOffHireDays() {
		return this.offHireDays;
	}

	@Column(name = "suspended", nullable = false, columnDefinition = "bit")
	public boolean isSuspended() {
		return this.suspended;
	}

	@Column(name = "offhire", nullable = false, columnDefinition = "bit")
	public boolean isOffHire() {
		return this.offHire;
	}

	@Column(name = "ukascalcost", precision = 10, scale = 2)
	public BigDecimal getUkasCalCost() {
		return this.ukasCalCost;
	}
}