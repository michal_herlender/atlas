package org.trescal.cwms.core.hire.entity.hireaccessory.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.dto.HireAccessoriesDTO;
import org.trescal.cwms.core.hire.entity.hireaccessory.HireAccessory;
import org.trescal.cwms.core.hire.entity.hireaccessorystatus.HireAccessoryStatus;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;
import org.trescal.cwms.core.instrument.entity.hireinstrument.db.HireInstrumentService;
import org.trescal.cwms.core.system.entity.status.db.StatusService;

@Service("HireAccessoryService")
public class HireAccessoryServiceImpl extends BaseServiceImpl<HireAccessory, Integer> implements HireAccessoryService
{
	@Autowired
	private HireInstrumentService hinstServ;
	@Autowired
	private HireAccessoryDao hireAccessoryDao;
	@Autowired
	private StatusService statusServ;
	
	@Override
	protected BaseDao<HireAccessory, Integer> getBaseDao() {
		return hireAccessoryDao;
	}
	
	public HireAccessory findEagerHireAccessory(int id)
	{
		return this.hireAccessoryDao.findEagerHireAccessory(id);
	}
	
	public ResultWrapper getAccessoriesForHireInstrumentAndModel(int hireInstId)
	{
		// find hire instrument
		HireInstrument hinst = this.hinstServ.findHireInstrument(hireInstId);
		// hire instrument found?
		if (hinst != null)
		{
			// create new accessories dto
			HireAccessoriesDTO hadto = new HireAccessoriesDTO();
			// get list of active hire accessories for instrument and add to dto
			hadto.setActiveHireInstAccessories(this.getAllAccessoriesForHireInstrument(hinst, true));
			// get list of de-activated hire accessories for instrument and add
			// to dto
			hadto.setDeactivatedHireInstAccessories(this.getAllAccessoriesForHireInstrument(hinst, false));
			// get list of distinct accessories for model and add to dto
			hadto.setDistinctModelAccessories(this.hireAccessoryDao.getDistinctAccessoriesForHireModel(hinst));
			// return successful result
			return new ResultWrapper(true, "", hadto, null);
		}
		else
		{
			return new ResultWrapper(false, "Hire instrument could not be found", null, null);
		}
	}

	public List<HireAccessory> getAllAccessoriesForHireInstrument(HireInstrument hi, boolean active)
	{
		return this.hireAccessoryDao.getAccessoriesForHireInstrument(hi, active, false);
	}
	
	public ResultWrapper getAvailableAccessoriesForHireInstrument(int hireInstId)
	{
		// find hire instrument
		HireInstrument hinst = this.hinstServ.findHireInstrument(hireInstId);
		// hire instrument found?
		if (hinst != null)
		{
			// get list of hire accessories for instrument
			List<HireAccessory> haccess = this.hireAccessoryDao.getAccessoriesForHireInstrument(hinst, true, true);
			// return successful result
			return new ResultWrapper(true, "", haccess, null);
		}
		else
		{
			return new ResultWrapper(false, "Hire instrument could not be found", null, null);
		}
	}
	
	public ResultWrapper updateHireAccessoryDescription(int hireAccId, String newDesc)
	{
		// check new description provided?
		if ((newDesc != null) && (newDesc.length() > 0))
		{
			// find hire accessory
			HireAccessory ha = this.get(hireAccId);
			// hire accessory null?
			if (ha != null)
			{
				// update description
				ha.setItem(newDesc);
				// update hire accessory
				this.update(ha);
				// return successful wrapper
				return new ResultWrapper(true, "", ha, null);
			}
			else
			{
				// return un-successful wrapper
				return new ResultWrapper(false, "Hire accessory could not be found", null, null);
			}
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "New hire accessory description not supplied", null, null);
		}
	}

	public ResultWrapper updateHireAccessoryStatus(int hireAccId, int hireAccStatusId)
	{
		// find hire accessory
		HireAccessory ha = this.get(hireAccId);
		// hire accessory null?
		if (ha != null)
		{
			// find new status
			HireAccessoryStatus has = (HireAccessoryStatus) this.statusServ.findStatus(hireAccStatusId, HireAccessoryStatus.class);
			// hire accessory status null?
			if (has != null)
			{
				// set new status
				ha.setStatus(has);
				// update hire accessory
				this.update(ha);
				// return successful wrapper
				return new ResultWrapper(true, "", ha, null);
			}
			else
			{
				// return un-successful wrapper
				return new ResultWrapper(false, "Hire accessory status could not be found", null, null);
			}
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "Hire accessory could not be found", null, null);
		}
	}

	public ResultWrapper updateInstrumentAccessories(int hireInstId, Integer[] existActiveAccIds, Integer[] existInActiveAccIds, String[] modelAccStrings, boolean newAcc, String newAccValue)
	{
		// get hire instrument to edit accessories for
		HireInstrument hi = this.hinstServ.findHireInstrumentDWR(hireInstId);
		// hire instrument found?
		if (hi != null)
		{
			// add new accessory?
			if (newAcc && newAccValue.isEmpty())
			{
				// return un-successful wrapper
				return new ResultWrapper(false, "The new hire accessory description is blank", null, null);
			}
			try
			{
				// de-activate hire accessories
				for (Integer i : existActiveAccIds)
				{
					// find hire accessory
					HireAccessory ha = this.get(i);
					// update active field
					ha.setActive(false);
					// update hire accessory
					this.update(ha);
				}
				// activate hire accessories
				for (Integer i : existInActiveAccIds)
				{
					// find hire accessory
					HireAccessory ha = this.get(i);
					// update active field
					ha.setActive(true);
					// update hire accessory
					this.update(ha);
				}
				// add any model accessories selected
				for (String s : modelAccStrings)
				{
					// first create new hire accessory
					HireAccessory ha = new HireAccessory();
					ha.setHireInst(hi);
					ha.setItem(s);
					ha.setStatus((HireAccessoryStatus) this.statusServ.findDefaultStatus(HireAccessoryStatus.class));
					ha.setActive(true);
					// insert new hire accessory
					this.save(ha);
					// add to hire instrument
					hi.getHireInstAccessories().add(ha);
				}
				// add new accessory?
				if (newAcc)
				{
					// first create new hire accessory
					HireAccessory ha = new HireAccessory();
					ha.setHireInst(hi);
					ha.setItem(newAccValue);
					ha.setStatus((HireAccessoryStatus) this.statusServ.findDefaultStatus(HireAccessoryStatus.class));
					ha.setActive(true);
					// insert new hire accessory
					this.save(ha);
					// add to hire instrument
					hi.getHireInstAccessories().add(ha);
				}
				// update hire instrument
				this.hinstServ.updateHireInstrument(hi);
				// return successful result
				return new ResultWrapper(true, null, hi, null);
			}
			catch (Exception e)
			{
				return new ResultWrapper(false, e.getMessage(), null, null);
			}
		}
		else
		{
			return new ResultWrapper(false, "Hire instrument could not be found", null, null);
		}
	}
}