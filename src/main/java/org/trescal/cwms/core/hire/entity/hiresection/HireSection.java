package org.trescal.cwms.core.hire.entity.hiresection;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.hire.entity.hirecategory.HireCategory;

@Entity
@Table(name = "hiresection")
public class HireSection
{
	private Set<HireCategory> hireCategories;
	private int id;
	private int sectioncode;
	private String sectionname;

	@OneToMany(mappedBy = "hireSection", fetch = FetchType.LAZY)
	public Set<HireCategory> getHireCategories()
	{
		return this.hireCategories;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Length(min = 0, max = 4)
	@Column(name = "sectioncode", length = 4)
	public int getSectioncode()
	{
		return this.sectioncode;
	}

	@Length(min = 0, max = 100)
	@Column(name = "sectionname", length = 100, nullable = false)
	public String getSectionname()
	{
		return this.sectionname;
	}

	public void setHireCategories(Set<HireCategory> hireCategories)
	{
		this.hireCategories = hireCategories;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setSectioncode(int sectioncode)
	{
		this.sectioncode = sectioncode;
	}

	public void setSectionname(String sectionname)
	{
		this.sectionname = sectionname;
	}

}
