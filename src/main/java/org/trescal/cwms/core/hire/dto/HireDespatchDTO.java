package org.trescal.cwms.core.hire.dto;

import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;

import java.time.LocalDate;

public class HireDespatchDTO {
	private String carriageCost;
	private String carriageType;
	private String cdConsigNo;
	private Integer cdCourierId;
	private Integer cdCourierTypeId;
	private LocalDate cdDesDate;
	private Integer cdId;
	private Integer schedAddId;
	private Integer schedConId;
	private String schedDate;
	private Integer schedFromId;
	private ScheduleStatus schedStatus;
	private ScheduleType schedType;
	private Integer scheduleId;

	public HireDespatchDTO() {
		super();
	}

	public HireDespatchDTO(String carriageCost, String carriageType, Integer scheduleId, Integer schedFromId,
						   ScheduleType schedType, Integer schedConId, Integer schedAddId, ScheduleStatus schedStatus,
						   String schedDate, Integer cdId, String cdConsigNo, LocalDate cdDesDate, Integer cdCourierId,
						   Integer cdCourierTypeId) {
		this.carriageCost = carriageCost;
		this.carriageType = carriageType;
		this.scheduleId = scheduleId;
		this.schedFromId = schedFromId;
		this.schedType = schedType;
		this.schedConId = schedConId;
		this.schedAddId = schedAddId;
		this.schedStatus = schedStatus;
		this.schedDate = schedDate;
		this.cdId = cdId;
		this.cdConsigNo = cdConsigNo;
		this.cdDesDate = cdDesDate;
		this.cdCourierId = cdCourierId;
		this.cdCourierTypeId = cdCourierTypeId;
	}

	public String getCarriageCost() {
		return this.carriageCost;
	}

	public String getCarriageType() {
		return this.carriageType;
	}

	public String getCdConsigNo() {
		return this.cdConsigNo;
	}

	public Integer getCdCourierId() {
		return this.cdCourierId;
	}

	public Integer getCdCourierTypeId() {
		return this.cdCourierTypeId;
	}

	public LocalDate getCdDesDate() {
		return this.cdDesDate;
	}

	public Integer getCdId() {
		return this.cdId;
	}

	public Integer getSchedAddId() {
		return this.schedAddId;
	}

	public Integer getSchedConId() {
		return this.schedConId;
	}

	public String getSchedDate() {
		return this.schedDate;
	}

	public Integer getSchedFromId() {
		return this.schedFromId;
	}

	public ScheduleStatus getSchedStatus() {
		return this.schedStatus;
	}

	public ScheduleType getSchedType() {
		return this.schedType;
	}

	public Integer getScheduleId() {
		return this.scheduleId;
	}

	public void setCarriageCost(String carriageCost) {
		this.carriageCost = carriageCost;
	}

	public void setCarriageType(String carriageType) {
		this.carriageType = carriageType;
	}

	public void setCdConsigNo(String cdConsigNo) {
		this.cdConsigNo = cdConsigNo;
	}

	public void setCdCourierId(Integer cdCourierId) {
		this.cdCourierId = cdCourierId;
	}

	public void setCdCourierTypeId(Integer cdCourierTypeId) {
		this.cdCourierTypeId = cdCourierTypeId;
	}

	public void setCdDesDate(LocalDate cdDesDate) {
		this.cdDesDate = cdDesDate;
	}

	public void setCdId(Integer cdId) {
		this.cdId = cdId;
	}

	public void setSchedAddId(Integer schedAddId) {
		this.schedAddId = schedAddId;
	}

	public void setSchedConId(Integer schedConId) {
		this.schedConId = schedConId;
	}

	public void setSchedDate(String schedDate) {
		this.schedDate = schedDate;
	}

	public void setSchedFromId(Integer schedFromId) {
		this.schedFromId = schedFromId;
	}

	public void setSchedStatus(ScheduleStatus schedStatus) {
		this.schedStatus = schedStatus;
	}

	public void setSchedType(ScheduleType schedType) {
		this.schedType = schedType;
	}

	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}
}