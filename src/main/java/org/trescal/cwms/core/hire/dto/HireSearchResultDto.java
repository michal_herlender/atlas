package org.trescal.cwms.core.hire.dto;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HireSearchResultDto {
    private Integer hireId;
    private String hireNumber;
    private Integer companyId;
    private String companyName;
    private Integer contactId;
    private String contactName;
    private String poNumber;
    private String clientRef;
    private LocalDate hireDate;
    private String hireStatus;

    private List<HireSearchResultItem> items;

    public HireSearchResultDto(Integer hireId, String hireNumber, Integer companyId, String companyName, Integer contactId, String contactName, String poNumber, String clientRef, LocalDate hireDate, String hireStatus){
        this(hireId,hireNumber,companyId,companyName,contactId,contactName,poNumber,clientRef,hireDate,hireStatus,Collections.emptyList());
    }


   /*public static List<HireSearchResultDto> fromProjectionList(Collection<HireSearchResultProjection> projections){
        return new ArrayList<>(projections.stream()
                .collect(Collectors.toMap(HireSearchResultProjection::getHireId,
                        HireSearchResultDto::fromProjection,
                        (hire1, hire2) -> {
                            hire1.setItems(Stream.concat(hire1.getItems().stream(), hire2.getItems().stream())
                                    .collect(Collectors.toList())
                            );
                            return hire1;
                        }
                )).values());
    }



    private static HireSearchResultDto fromProjection(HireSearchResultProjection projection) {
        return  HireSearchResultDto.builder()
                .hireId(projection.getHireId())
                .hireNumber(projection.getHireNumber())
                .companyId(projection.getCompanyId())
                .contactId(projection.getContactId())
                .companyName(projection.getCompanyName())
                .contactName(projection.getContactName())
                .hireDate(projection.getHireDate())
                .poNumber(projection.getPoNumber())
                .clientRef(projection.getClientRef())
                .hireStatus(projection.getHireStatus())
                .items(HireSearchResultItem.fromProjection(projection)
                        .map(Collections::singletonList).orElse(Collections.emptyList()))
                .build();
    }*/
}

