package org.trescal.cwms.core.hire.entity.hirenote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;

@Entity
@Table(name = "hirenote")
public class HireNote extends Note
{
	public final static boolean privateOnly = false;
	
	private Hire hire;
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "hireid", unique = false, nullable = false, insertable = true, updatable = true)
	public Hire getHire() {
		return this.hire;
	}
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.HIRENOTE;
	}
	
	/**
	 * @param hire the hire to set
	 */
	public void setHire(Hire hire) {
		this.hire = hire;
	}
}