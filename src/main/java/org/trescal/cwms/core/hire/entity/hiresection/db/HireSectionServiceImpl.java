package org.trescal.cwms.core.hire.entity.hiresection.db;

import java.util.List;

import org.trescal.cwms.core.hire.entity.hiresection.HireSection;

public class HireSectionServiceImpl implements HireSectionService
{
	private HireSectionDao hireSectionDao;

	public void deleteHireSection(HireSection hs)
	{
		this.hireSectionDao.remove(hs);
	}

	public HireSection findHireSection(int id)
	{
		return this.hireSectionDao.find(id);
	}

	public List<HireSection> getAllHireSections()
	{
		return this.hireSectionDao.findAll();
	}

	public void insertHireSection(HireSection hs)
	{
		this.hireSectionDao.persist(hs);
	}

	public void saveOrUpdateHireSection(HireSection hs)
	{
		this.hireSectionDao.saveOrUpdate(hs);
	}

	public void setHireSectionDao(HireSectionDao hireSectionDao)
	{
		this.hireSectionDao = hireSectionDao;
	}

	public void updateHireSection(HireSection hs)
	{
		this.hireSectionDao.update(hs);
	}

}