package org.trescal.cwms.core.hire.entity.hireaccessory.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.entity.hireaccessory.HireAccessory;
import org.trescal.cwms.core.hire.entity.hireaccessorystatus.HireAccessoryStatus;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;

public interface HireAccessoryService extends BaseService<HireAccessory, Integer>
{
	HireAccessory findEagerHireAccessory(int id);
	
	ResultWrapper getAccessoriesForHireInstrumentAndModel(int hireInstId);
	
	/**
	 * this method gets a list of all active/in-active {@link HireAccessory}
	 * available for a hire instrument
	 * 
	 * @param hi {@link HireInstrument} object
	 * @param active do we want active or in-active accessories
	 * @return {@link List} of {@link HireAccessory}
	 */
	List<HireAccessory> getAllAccessoriesForHireInstrument(HireInstrument hi, boolean active);
	
	/**
	 * this method gets a list of all {@link HireAccessory} that are active and
	 * have a status of available
	 * 
	 * @param hireInstId id of {@link HireInstrument}
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper getAvailableAccessoriesForHireInstrument(int hireInstId);
	
	/**
	 * this method updates the text description of a {@link HireAccessory}
	 * 
	 * @param hireAccId id of the {@link HireAccessory}
	 * @param newDesc new string description of the {@link HireAccessory}
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper updateHireAccessoryDescription(int hireAccId, String newDesc);
	
	/**
	 * this method updates a {@link HireAccessory} status
	 * 
	 * @param hireAccId id of the {@link HireAccessory}
	 * @param hireAccStatusId id of the new {@link HireAccessoryStatus}
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper updateHireAccessoryStatus(int hireAccId, int hireAccStatusId);
	
	/**
	 * update {@link HireAccessory} for a {@link HireInstrument}
	 * 
	 * @param hireInstId id of the {@link HireInstrument}
	 * @param existActiveAccIds array of active {@link HireAccessory} ids
	 * @param existInActiveAccIds array of in-active {@link HireAccessory} ids
	 * @param modelAccStrings array of model string {@link HireAccessory} values
	 * @param newAcc do we just want a new freehand hire accessory?
	 * @param newAccValue new freehand {@link HireAccessory} value
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper updateInstrumentAccessories(int hireInstId, Integer[] existActiveAccIds, Integer[] existInActiveAccIds, String[] modelAccStrings, boolean newAcc, String newAccValue);
}