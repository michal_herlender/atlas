package org.trescal.cwms.core.hire.entity.hiremodelcategory.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.hire.entity.hiremodelcategory.HireModelCategory;

public interface HireModelCategoryDao extends BaseDao<HireModelCategory, Integer> {
	
	List<String> getDistinctPlantCodeList();
	
	int getNextModelCodeForCategory(int categoryid);
}