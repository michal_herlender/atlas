package org.trescal.cwms.core.hire.entity.hirenote.db;

import java.util.List;

import org.trescal.cwms.core.hire.entity.hirenote.HireNote;

public class HireNoteServiceImpl implements HireNoteService
{
	HireNoteDao hireNoteDao;

	public void deleteHireNote(HireNote HireNote)
	{
		this.hireNoteDao.remove(HireNote);
	}

	public void deleteHireNoteById(int id)
	{
		this.hireNoteDao.remove(this.hireNoteDao.find(id));
	}

	public HireNote findHireNote(int id)
	{
		return this.hireNoteDao.find(id);
	}

	public List<HireNote> getActiveHireNotesForHire(int jobid)
	{
		return this.hireNoteDao.getActiveHireNotesForHire(jobid);
	}

	public List<HireNote> getAllHireNotes()
	{
		return this.hireNoteDao.findAll();
	}

	public void insertHireNote(HireNote HireNote)
	{
		this.hireNoteDao.persist(HireNote);
	}

	public void saveOrUpdateHireNote(HireNote HireNote)
	{
		this.hireNoteDao.saveOrUpdate(HireNote);
	}

	public void setHireNoteDao(HireNoteDao HireNoteDao)
	{
		this.hireNoteDao = HireNoteDao;
	}

	public void updateHireNote(HireNote HireNote)
	{
		this.hireNoteDao.update(HireNote);
	}
}