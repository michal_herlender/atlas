package org.trescal.cwms.core.hire.entity.hirecrossitem.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.hire.entity.hirecrossitem.HireCrossItem;

public interface HireCrossItemDao extends BaseDao<HireCrossItem, Integer> {}