package org.trescal.cwms.core.hire.entity.hireitemaccessory.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessory;

public interface HireItemAccessoryDao extends BaseDao<HireItemAccessory, Integer> {}