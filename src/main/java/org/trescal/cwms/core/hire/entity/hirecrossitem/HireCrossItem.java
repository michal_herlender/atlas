package org.trescal.cwms.core.hire.entity.hirecrossitem;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;

@Entity
@Table(name = "hirecrossitem")
public class HireCrossItem extends Auditable
{
	private String description;
	private int itemid;
	private String plantno;
	private String serialno;

	@Length(max = 200, min = 0)
	@Column(name = "description", length = 200, nullable = false)
	public String getDescription()
	{
		return this.description;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "itemid", nullable = false, unique = true)
	@Type(type = "int")
	public int getItemid()
	{
		return this.itemid;
	}

	@Length(max = 50, min = 0)
	@Column(name = "plantno", length = 50, nullable = true)
	public String getPlantno()
	{
		return this.plantno;
	}

	@Length(max = 50, min = 0)
	@Column(name = "serialno", length = 50, nullable = true)
	public String getSerialno()
	{
		return this.serialno;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setItemid(int itemid)
	{
		this.itemid = itemid;
	}

	public void setPlantno(String plantno)
	{
		this.plantno = plantno;
	}

	public void setSerialno(String serialno)
	{
		this.serialno = serialno;
	}

}
