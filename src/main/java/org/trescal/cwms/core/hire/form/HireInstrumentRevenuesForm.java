package org.trescal.cwms.core.hire.form;

import java.util.List;

import org.trescal.cwms.core.hire.dto.HireInstrumentRevenueWrapper;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;

public class HireInstrumentRevenuesForm
{
	private String barcode;
	private Integer descid;
	private String descname;
	private Boolean export;
	private HireInstrument hi;
	private Integer mfrid;
	private String mfrname;
	private String modelname;
	private String plantno;
	private List<HireInstrumentRevenueWrapper> revWrappers;
	private String serialno;
	private String spreadsheetPath;

	public String getBarcode()
	{
		return this.barcode;
	}

	public Integer getDescid()
	{
		return this.descid;
	}

	public String getDescname()
	{
		return this.descname;
	}

	public Boolean getExport() {
		return export;
	}

	public HireInstrument getHi()
	{
		return this.hi;
	}

	public Integer getMfrid()
	{
		return this.mfrid;
	}

	public String getMfrname()
	{
		return this.mfrname;
	}

	public String getModelname()
	{
		return this.modelname;
	}

	public String getPlantno()
	{
		return this.plantno;
	}

	public List<HireInstrumentRevenueWrapper> getRevWrappers()
	{
		return this.revWrappers;
	}

	public String getSerialno()
	{
		return this.serialno;
	}

	public String getSpreadsheetPath()
	{
		return this.spreadsheetPath;
	}

	public void setBarcode(String barcode)
	{
		this.barcode = barcode;
	}

	public void setDescid(Integer descid)
	{
		this.descid = descid;
	}

	public void setDescname(String descname)
	{
		this.descname = descname;
	}

	public void setExport(Boolean export) {
		this.export = export;
	}

	public void setHi(HireInstrument hi)
	{
		this.hi = hi;
	}

	public void setMfrid(Integer mfrid)
	{
		this.mfrid = mfrid;
	}

	public void setMfrname(String mfrname)
	{
		this.mfrname = mfrname;
	}

	public void setModelname(String modelname)
	{
		this.modelname = modelname;
	}

	public void setPlantno(String plantno)
	{
		this.plantno = plantno;
	}

	public void setRevWrappers(List<HireInstrumentRevenueWrapper> revWrappers)
	{
		this.revWrappers = revWrappers;
	}

	public void setSerialno(String serialno)
	{
		this.serialno = serialno;
	}

	public void setSpreadsheetPath(String spreadsheetPath)
	{
		this.spreadsheetPath = spreadsheetPath;
	}

}
