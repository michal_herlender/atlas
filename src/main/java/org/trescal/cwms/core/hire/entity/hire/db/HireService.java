package org.trescal.cwms.core.hire.entity.hire.db;

import io.vavr.control.Either;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.dto.HireDespatchDTO;
import org.trescal.cwms.core.hire.dto.HireSearchResultDto;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.form.HireHomeForm;
import org.trescal.cwms.core.tools.PagedResultSet;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Locale;

public interface HireService extends BaseService<Hire, Integer> {
	/**
	 * this method updates the accounts verified value of a {@link Hire}
	 *
	 * @param hireId id of the hire to be verified
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper accountsVerifyHire(int hireId, HttpSession session);

	/**
	 * this method sets the {@link Hire} to a status of 'Complete'
	 * 
	 * @param hireId id of the hire to be completed
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper completeHireContract(int hireId);

    /**
     * converts a hire enquiry to a hire contract
     *
     * @param hireId      id of hire to convert
     * @param clientpo    client purchase order number
     * @param specialInst any special instructions for hire
     * @return {@link ResultWrapper}
     */
    Either<String, Boolean> convertEnquiryToContract(int hireId, String clientpo, String specialInst, Integer allocatedSubdivId);
	
	List<Hire> findHiresForCourierDespatch(int cdId);

	ResultWrapper generateOffHireBirtDocument(int hireid, Locale locale);
	
	List<Hire> getAllActiveHireContracts(Subdiv allocatedSubdiv);

	/**
	 * this method returns a list of active {@link Hire} contracts which are at
	 * the status of 'Awaiting Invoice'.
	 * 
	 * @return {@link List} {@link Hire}
	 */
	List<Hire> getAllActiveHireContractsAwaitingInvoice(Subdiv allocatedSubdiv);

	List<Hire> getAllHireEnquiries(Subdiv allocatedSubdiv);

	List<Hire> getAllHires();

	/**
	 * retrieves a list of hires which are awaiting accounts varification due to
	 * company on stop
	 * 
	 * @return {@link List} of {@link Hire}
	 */
	List<Hire> getEnquiriesAwaitingAccountsVarification(Subdiv allocatedSubdiv);

	/**
	 * Returns the {@link Hire} with the given hire No or null if there are no
	 * hires with the given hire No.
	 * 
	 * @param hireNo the hire No to match.
	 * @return the {@link Hire}
	 */
	Hire getHireByExactHireNo(String hireNo);

	void insertHire(Hire hire);

	void prepareAndInsertHire(Hire hire);

	/**
	 * this method removes the courier depatch or schedule from the hire and
	 * tries to determine the best action to perform on these whether it be
	 * deleting or leaving active.
	 * 
	 * @param hireId id of {@link Hire}
	 * @param cdId id of {@link CourierDespatch} if applicable, else 0
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper removeHireDespatchType(int hireId, int cdId);

	void saveOrUpdateHire(Hire hire);

	/**
	 * this method sends a confirmation email for hire enquiries to the hire
	 * contact
	 * 
	 * @param hireid id of the hire to send confirmation
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper sendEnquiryEmailConfirmation(int hireid, HttpSession session);

	void updateHire(Hire hire);

	ResultWrapper updateHireDespatch(Integer hireId, Hire hire, HireDespatchDTO hddto, Integer allocatedSubdivId, HttpSession session);

	/**
	 * sets the hire contract or enquiry to 'Rejected'
	 * 
	 * @param hireId id of hire contract/enquiry to reject
	 * @param reason why has hire been rejected
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper voidHire(int hireId, String reason, HttpSession session);
	
	Long hireCountByCourier(Integer courierId);
	
	Long hireCountByCourierDespatchType(Integer cdtId);

	PagedResultSet<HireSearchResultDto> queryHire(HireHomeForm form, PagedResultSet<HireSearchResultDto> prs, Subdiv allocatedSubdiv);
}