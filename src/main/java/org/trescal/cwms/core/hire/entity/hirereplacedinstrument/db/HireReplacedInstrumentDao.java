package org.trescal.cwms.core.hire.entity.hirereplacedinstrument.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.hire.entity.hirereplacedinstrument.HireReplacedInstrument;

public interface HireReplacedInstrumentDao extends BaseDao<HireReplacedInstrument, Integer> {}