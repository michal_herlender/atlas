package org.trescal.cwms.core.hire.entity.hiresuspenditem.db;

import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.hire.entity.hireitem.db.HireItemService;
import org.trescal.cwms.core.hire.entity.hiresuspenditem.HireSuspendItem;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

@Service("HireSuspendItemService")
public class HireSuspendItemServiceImpl implements HireSuspendItemService {
    @Autowired
    private HireSuspendItemDao hireSuspendItemDao;
    @Autowired
    private HireItemService hitemServ;

    public Either<String, Boolean> completeHireItemSuspension(int id, LocalDate enddate) {
        // find the hire item
        HireItem hi = this.hitemServ.findHireItem(id);
        // hire item found?
        if (hi != null) {
            // hire suspend item object
            HireSuspendItem hsi = null;
            // get the current hire suspend item which needs completing
            for (HireSuspendItem hsitem : hi.getSuspenditems()) {
                // does this hsi need completing?
                if (hsitem.getEndDate() == null) {
                    hsi = hsitem;
                }
            }
            // found the hire suspend item?
            if (hsi != null) {
                // set values
                hsi.setEndDate(enddate);
                // update hire suspend item
                this.updateHireSuspendItem(hsi);
                // set hire item as not suspended
                hi.setSuspended(false);
                // update hire item
                this.hitemServ.updateHireItem(hi);
                // return successful wrapper
                return Either.right(false);
            } else {
                // return un-successful wrapper
                return Either.left("Hire item suspension could not be completed");
            }
        } else {
            // return un-successful wrapper
            return Either.left("Hire item could not be found");
        }
    }

    public void deleteHireSuspendItem(HireSuspendItem hi) {
        this.hireSuspendItemDao.remove(hi);
    }

    public HireSuspendItem findHireSuspendItem(int id) {
        return this.hireSuspendItemDao.find(id);
    }

    public List<HireSuspendItem> getAllHireSuspendItems() {
        return this.hireSuspendItemDao.findAll();
    }

    public void insertHireSuspendItem(HireSuspendItem hi) {
        this.hireSuspendItemDao.persist(hi);
    }

    public void saveOrUpdateHireSuspendItem(HireSuspendItem hi) {
        this.hireSuspendItemDao.persist(hi);
    }

    public void setHitemServ(HireItemService hitemServ) {
        this.hitemServ = hitemServ;
    }

    public ResultWrapper suspendAllHireItems(String startdate, String enddate) {
        // get all items currently on hire and on active hire contracts
        List<HireItem> allItemsOnHire = this.hitemServ.getAllItemsCurrentlyOnHire();
        // create list of hire items to return if they fail
        List<HireItem> failedItems = new ArrayList<>();
        // hire items found?
        if (allItemsOnHire.size() > 0) {
            // create date variable
            LocalDate today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
            LocalDate sdate;
            LocalDate edate;
            try {
                // format dates
                sdate = LocalDate.parse(startdate, DateTimeFormatter.ISO_DATE);
                edate = LocalDate.parse(enddate, DateTimeFormatter.ISO_DATE);
            } catch (DateTimeParseException e) {
                // return un-successful wrapper
                return new ResultWrapper(false, e.getMessage(), null, null);
            }
            // first check formatted dates not null
            if ((sdate != null) && (edate != null)) {
                // check end date before today so suspension is taking place in
                // the
                // past
                if (edate.isBefore(today)) {
                    // start date is before end date
                    if (sdate.isBefore(edate)) {
                        // suspend items
                        for (HireItem hi : allItemsOnHire) {
                            // start date is after hire date
                            if (sdate.isAfter(hi.getHire().getHireDate())) {
                                // create new hire suspend item
                                HireSuspendItem hsi = new HireSuspendItem();
                                // set values
                                hsi.setHireItem(hi);
                                hsi.setStartDate(sdate);
                                hsi.setEndDate(edate);
                                // insert hire suspend item
                                this.insertHireSuspendItem(hsi);
                                // add to hire item
                                hi.getSuspenditems().add(hsi);
                                // update hire item
                                this.hitemServ.updateHireItem(hi);
                            } else {
                                // add to failed item list
                                failedItems.add((HireItem) this.hitemServ.findHireItemDWR(hi.getId()).getResults());
                            }
                        }
                        // return wrapper
                        return new ResultWrapper(true, "", failedItems, null);
                    } else {
                        // return un-successful wrapper
                        return new ResultWrapper(false, "You have selected an end date which is before the start date", null, null);
                    }
                } else {
                    // return un-successful wrapper
                    return new ResultWrapper(false, "Suspension should be performed as a historical action (i.e. end date should be before today)", null, null);
                }
            } else {
                // return un-successful wrapper
                return new ResultWrapper(false, "There is a problem with one of the dates you selected", null, null);
            }
        } else {
            // return un-successful wrapper
            return new ResultWrapper(false, "No hire items could be found for suspension", null, null);
        }
    }

    public Either<String, Boolean> suspendHireItem(int id, LocalDate startdate) {
        // find the hire item
        HireItem hi = this.hitemServ.findHireItem(id);
        // hire item found?
        if (hi != null) {
            // create date variable
            // check formatted start date not null and after hire date
            if ((startdate != null) && (startdate.isAfter(hi.getHire().getHireDate()))) {
                // create new hire suspend item
                HireSuspendItem hsi = new HireSuspendItem();
                // set values
                hsi.setHireItem(hi);
                hsi.setStartDate(startdate);
                // insert hire suspend item
                this.insertHireSuspendItem(hsi);
                // add to hire item
                hi.getSuspenditems().add(hsi);
                // set hire item as suspended
                hi.setSuspended(true);
                // update hire item
                this.hitemServ.updateHireItem(hi);
                // return successful wrapper
                return Either.right(true);
            } else {
                // return un-successful wrapper
                return Either.left("The date selected for item suspension is before hire has commenced");
            }
        } else {
            // return un-successful wrapper
            return Either.left("Hire item could not be found");
        }
    }

    public void updateHireSuspendItem(HireSuspendItem hi) {
        this.hireSuspendItemDao.merge(hi);
    }

}