package org.trescal.cwms.core.hire.entity.hireitemaccessory;

import java.util.Comparator;

/**
 * comparator class for sorting hire item accessories by hire accessory item
 * desciption ascending
 * 
 * @author stuarth
 */
public class HireItemAccessoryComparator implements Comparator<HireItemAccessory>
{
	public int compare(HireItemAccessory hiAcc1, HireItemAccessory hiAcc2)
	{
		if (((hiAcc1.getHireAccessory().getItem() != null) && !hiAcc1.getHireAccessory().getItem().isEmpty())
				&& ((hiAcc2.getHireAccessory().getItem() != null) && !hiAcc2.getHireAccessory().getItem().isEmpty()))
		{
			return hiAcc1.getHireAccessory().getItem().compareTo(hiAcc2.getHireAccessory().getItem());
		}
		else
		{
			Integer id1 = hiAcc1.getHireAccessory().getHireAccessoryId();
			Integer id2 = hiAcc2.getHireAccessory().getHireAccessoryId();

			return id2.compareTo(id1);
		}
	}
}