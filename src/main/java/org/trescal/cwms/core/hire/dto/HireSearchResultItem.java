package org.trescal.cwms.core.hire.dto;

import lombok.Value;

@Value
public class  HireSearchResultItem {

    Integer hireId;
    Integer itemNumber;
    String instrument;
    String serialNumber;
    String plantNumber;
    String calType;
    Boolean offHire;
    Boolean suspended;
}
