package org.trescal.cwms.core.hire.entity.hireitem.db;

import io.vavr.control.Either;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.dto.HireSearchResultItem;
import org.trescal.cwms.core.hire.dto.OffHireAccessoryDTO;
import org.trescal.cwms.core.hire.dto.ViewHireItemDto;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.hire.entity.hireaccessorystatus.HireAccessoryStatus;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessory;
import org.trescal.cwms.core.hire.entity.hirereplacedinstrument.HireReplacedInstrument;
import org.trescal.cwms.core.hire.entity.hirereplacedinstrument.db.HireReplacedInstrumentService;
import org.trescal.cwms.core.hire.entity.hirestatus.HireStatus;
import org.trescal.cwms.core.hire.entity.hiresuspenditem.HireSuspendItem;
import org.trescal.cwms.core.hire.entity.hiresuspenditem.db.HireSuspendItemService;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;
import org.trescal.cwms.core.instrument.entity.hireinstrument.db.HireInstrumentService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.tools.NumberTools;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

@Service("HireItemService")
public class HireItemServiceImpl implements HireItemService {
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private HireInstrumentService hireInstServ;
	@Autowired
	private HireItemDao hireItemDao;
	@Autowired
	private HireReplacedInstrumentService hireReplacedInstServ;
	@Value("#{props['cwms.config.hire.min_hire_days']}")
	private Integer minHireDays;
	@Autowired
	private HireService hireServ;
	@Autowired
	private HireSuspendItemService hireSuspendItemServ;
	@Autowired
	private UserService userService;
	@Autowired
	private StatusService statusServ;

	@Override
	public ResultWrapper ajaxDeleteHireItems(int[] delIds) {
		// item ids to delete?
		if (delIds.length > 0) {
			// create new list of hire items
			List<HireItem> hitems = new ArrayList<>();
			// find and add each hire item for ids passed
			for (Integer i : delIds) {
				// find hire item
				HireItem hi = this.findHireItem(i);
				// hi null?
				if (hi != null) {
					// add hire item to list
					hitems.add(hi);
				}
			}
			// check all hire items found
			if (hitems.size() == delIds.length)
			{
				// delete all items found
				for (HireItem hi : hitems)
				{
					// delete hire item
					this.deleteHireItem(hi);
				}
				// return successful wrapper
				return new ResultWrapper(true, "", null, null);
			}
			else
			{
				// return un-successful wrapper
				return new ResultWrapper(false, "All hire items could not be found", null, null);
			}
		} else {
			// return un-successful wrapper
			return new ResultWrapper(false, "No hire items supplied", null, null);
		}
	}

	@Override
	public Either<String, Boolean> ajaxOffHireItem(int offhireId, String offHireCond, LocalDate offHireDate, List<OffHireAccessoryDTO> offHireAccs, boolean genOffHireDoc) {
		Hire hire;
		// find hire item
		HireItem hi = this.findHireItem(offhireId);
		if (hi == null)
			return Either.left("Hire item could not be found");
		// hire item found
		// get hire to find out hire date
		hire = hi.getHire();
		// get hire date and offhire date of item
		LocalDate hiredate = hire.getHireDate();
		// check formatted off hire date not null and after hire
		// date
		if (offHireDate.isBefore(hiredate))
			// return un-successful wrapper
			return Either.left("The off hire date selected is before hire has commenced");

		// check if item is suspended?
		if (hi.isSuspended()) {
			// hire suspend item object
			HireSuspendItem hsi = null;
			// get the current hire suspend item which
			// needs completing
			for (HireSuspendItem hsitem : hi.getSuspenditems()) {
				// does this hsi need completing?
				if (hsitem.getEndDate() == null) {
					hsi = hsitem;
				}
			}
			// found the hire suspend item?
			if (hsi != null) {
				// set end date
				hsi.setEndDate(offHireDate);
				// update hire suspend item
				this.hireSuspendItemServ.updateHireSuspendItem(hsi);
				// set hire item as not suspended
				hi.setSuspended(false);
			}
		}
		// update all hire item accessories
		for (HireItemAccessory hia : hi.getAccessories()) {
			// check for matching off hire accessory dto
			for (OffHireAccessoryDTO offHireAccDto : offHireAccs) {
				// match?
				if (hia.getId() == offHireAccDto.getHiaccid()) {
					// set returned value
					hia.setReturned(offHireAccDto.getReturned());
					// if status id passed not null
					if (offHireAccDto.getStatusid() != null) {
						// set new status of hire accessory if
						// applicable
						hia.getHireAccessory().setStatus(this.statusServ.findStatus(offHireAccDto.getStatusid(), HireAccessoryStatus.class));
						// set to in-active
						hia.getHireAccessory().setActive(false);
					}
				}
			}
		}
		// set all off hire values for items
		hi.setOffHire(true);
		hi.setOffHireCondition(offHireCond);
		hi.setOffHireDate(offHireDate);
		// get off hire date of item
		LocalDate offhiredate = hi.getOffHireDate();
		// calculate number of days on hire
		int days = (int) (hiredate.until(offhiredate, ChronoUnit.DAYS) + 1 - hi.getDaysSuspended());
		// make sure minimum days is three
		days = (days < this.minHireDays) ? this.minHireDays : days;
		// set number of days on hire
		hi.setOffHireDays(days);
		// now update to big decimal for mutliplication
		BigDecimal bdDays = new BigDecimal(days);
		// use number of days on hire to calculate total
		// cost for item
		BigDecimal totalHire = hi.getHireCost().multiply(bdDays);
		// now add ukas cal cost
		hi.setTotalCost(hi.getUkasCalCost().add(totalHire));
		hi.setFinalCost(hi.getTotalCost());
		// update hire item
		this.updateHireItem(hi);

		// update total costs of hire
		CostCalculator.updatePricingFinalCost(hire, hire.getItems());
		// check all items off hired?
		if (hire.getItems().size() == this.getAllOffHiredItemsOnHire(hire.getId()).size()) {
			// set status to complete
			hire.setStatus((HireStatus) this.statusServ.findRequiresAttentionStatus(HireStatus.class));
		}
		// update hire
		this.hireServ.updateHire(hire);
		// generate the off hire document?
		if (genOffHireDoc) {
			// try to generate off hire document
			Locale locale = LocaleContextHolder.getLocale();
			ResultWrapper rwrap = this.hireServ.generateOffHireBirtDocument(hire.getId(), locale);
			//ResultWrapper rwrap = this.hireServ.generateOffHireDocument(hire.getId(), false, session);
			// generation successful?
			return !rwrap.isSuccess() ?
				Either.left(rwrap.getMessage()) :
				Either.right(true);
		} else {
			// return successful wrapper
			return Either.right(true);
		}
	}

	@Override
	public void deleteHireItem(HireItem hi) {
		this.hireItemDao.remove(hi);
	}

	@Override
	public HireItem findHireItem(int id) {
		return this.hireItemDao.find(id);
	}

	@Override
	public ResultWrapper findHireItemDWR(int id)
	{
		// get hire item
		HireItem hi = this.hireItemDao.findHireItemDWR(id);
		// check hire item found?
		if (hi != null)
		{
			// return successful wrapper
			return new ResultWrapper(true, "", hi, null);
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "Hire item not found for id "+id, null, null);
		}
	}

	@Override
	public List<HireItem> getAllHireItems()
	{
		return this.hireItemDao.findAll();
	}

	@Override
	public List<HireItem> getAllHireItemsOnHire(int hireid)
	{
		return this.hireItemDao.getAllHireItemsOnHire(hireid);
	}

	@Override
	public List<HireItem> getAllItemsCurrentlyOnHire()
	{
		return this.hireItemDao.getAllItemsCurrentlyOnHire();
	}

	@Override
	public List<HireItem> getAllOffHiredItemsOnHire(int hireid)
	{
		return this.hireItemDao.getAllOffHiredItemsOnHire(hireid);
	}

	@Override
	public int getNextItemNo(int hireid)
	{
		List<HireItem> items = this.getAllHireItemsOnHire(hireid);
		int itemno = 1;
		if (items.size() > 0)
		{
			HireItem item = items.get(items.size() - 1);
			itemno = item.getItemno() + 1;
		}

		return itemno;
	}

	@Override
	public boolean hireItemsReadyForHire(Hire hire) {
		// indicates whether all items are off hired
		// start checking items
		return hire.getItems()
			.stream()
			.filter(hi -> hi.getHireInst() != null && hi.getHireInst().getHireItems().size() > 0)
			.noneMatch(hitem -> !hitem.getHire().isEnquiry() && !hitem.isOffHire());
	}

	@Override
	public void insertHireItem(HireItem hi)
	{
		this.hireItemDao.persist(hi);
	}

	@Override
	public Either<String, Boolean> replaceHireItemInstrument(int hireItemId, int newPlantId, String reason) {
		// check reason supplied for replacement of instrument
		if ((reason != null) && (!reason.equals(""))) {
			// first find hire item
			HireItem hitem = this.findHireItem(hireItemId);
			// hire item null?
			if (hitem != null) {
				// find new hire instrument
				HireInstrument hi = (HireInstrument) this.hireInstServ.findReplacementHireInstrumentDWR(newPlantId).getResults();
				// hire instrument null?
				if (hi != null) {
					// get current contact
					String username = SecurityContextHolder.getContext().getAuthentication().getName();
					Contact contact = this.userService.getEagerLoad(username).getCon();
					// create new replaced instrument
					HireReplacedInstrument hri = new HireReplacedInstrument();
					// set values
					hri.setHireItem(hitem);
					hri.setHireInst(hitem.getHireInst());
					hri.setReason(reason);
					hri.setReplacedBy(contact);
					hri.setReplacedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
					// insert hire replaced instrument
					this.hireReplacedInstServ.insertHireReplacedInstrument(hri);
					// update hire item with new hire instrument
					hitem.setHireInst(hi);
					// update hire item
					this.updateHireItem(hitem);
					// return successful wrapper
					return Either.right(true);
				}
				else {
					// return un-successful wrapper
					return Either.left("New hire instrument could not be found");
				}
			}
			else {
				// return un-successful wrapper
				return Either.left("Hire item could not be found");
			}
		}
		else {
			// return un-successful wrapper
			return Either.left("A reason has not been supplied for the replacement of this instrument");
		}
	}

	@Override
	public Either<String, Boolean> resetOffHireDate(int hireItemId) {
		// find hire item
		HireItem hi = this.findHireItem(hireItemId);
		// hire item null?
		if (hi != null) {
			// reset off hire values
			hi.setOffHire(false);
			hi.setOffHireCondition(null);
			hi.setOffHireDate(null);
			hi.setOffHireDays(null);
			// now add ukas cal cost
			hi.setTotalCost(new BigDecimal("0.00"));
			hi.setFinalCost(new BigDecimal("0.00"));
			// update hire item
			this.updateHireItem(hi);
			// get hire
			Hire hire = hi.getHire();
			// update total costs of hire
			CostCalculator.updatePricingFinalCost(hire, hire.getItems());
			// hire status not 'Hire Contract'?
			if (!hire.getStatus().getAccepted())
			{
				// set status to 'Hire Contract'
				hire.setStatus((HireStatus) this.statusServ.findAcceptedStatus(HireStatus.class));
			}
			// update hire
			this.hireServ.updateHire(hire);
			// try to generate off hire document
			//ResultWrapper rwrap = this.hireServ.generateOffHireDocument(hire.getId(), false, session);
			Locale locale = LocaleContextHolder.getLocale();
			ResultWrapper rwrap = this.hireServ.generateOffHireBirtDocument(hire.getId(), locale);
			// generation successful?
			if (!rwrap.isSuccess()) {
				// return unsuccessful wrapper
				return Either.left(rwrap.getMessage());
			}
			else {
				// return successful wrapper
				return Either.right(true);
			}
		}
		else {
			// return un-successful wrapper
			return Either.left("Hire item could not be found");
		}
	}

	@Override
	public void saveOrUpdateHireItem(HireItem hi) {
		this.hireItemDao.persist(hi);
	}

	@Override
	public void updateHireItem(HireItem hi) {
		this.hireItemDao.merge(hi);
	}


	@Override
	public Either<String, ViewHireItemDto> updateHireItemValues(Integer hireItemId, Integer calTypeId, BigDecimal hireCost, BigDecimal ukasCalCost) {
		val hi = findHireItem(hireItemId);
		if (hi == null)
			return Either.left("Hire item could not be found");
		hi.setCalType(calTypeServ.find(calTypeId));
		hi.setHireCost(hireCost);
		hi.setUkasCalCost(ukasCalCost);
		return Either.right(ViewHireItemDto.fromHireItem(hi));
	}

	@Override
	public ResultWrapper updateHireItemValues(int hireItemId, String calTypeId, String hireCost, String ukasCalCost) {
		// first find hire item
		HireItem hi = this.findHireItem(hireItemId);
		// hire item null?
		if (hi != null) {
			// next check caltype id is an integer?
			if (NumberTools.isAnInteger(calTypeId))
			{
				// next check hire cost and ukas cal cost is a double
				if (NumberTools.isADouble(hireCost)
						&& NumberTools.isADouble(ukasCalCost))
				{
					// update the calibration type of item
					hi.setCalType(this.calTypeServ.find(Integer.parseInt(calTypeId)));
					// update hire cost]
					hi.setHireCost(new BigDecimal(NumberTools.isADouble(hireCost) ? hireCost : "0.00"));
					// update ukas cal cost
					hi.setUkasCalCost(new BigDecimal(NumberTools.isADouble(ukasCalCost) ? ukasCalCost : "0.00"));
					// update hire item
					this.updateHireItem(hi);
					// return successful wrapper
					return new ResultWrapper(true, "", hi, null);
				}
				else
				{
					// return un-successful wrapper
					return new ResultWrapper(false, "Hire cost or ukas cal cost supplied is not formatted correctly for insertion", null, null);
				}
			}
			else
			{
				// return un-successful wrapper
				return new ResultWrapper(false, "Calibration type id supplied is not an integer", null, null);
			}
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "Hire item could not be found", null, null);
		}
	}

	@Override
	public List<HireSearchResultItem> findItemsByHireIds(Collection<Integer> ids){
		return hireItemDao.findByHireId(ids);
	}

}