package org.trescal.cwms.core.hire.entity.hirecategory.db;

import java.util.List;

import org.trescal.cwms.core.hire.entity.hirecategory.HireCategory;

public class HireCategoryServiceImpl implements HireCategoryService
{
	private HireCategoryDao hireCategoryDao;

	public void deleteHireCategory(HireCategory hc)
	{
		this.hireCategoryDao.remove(hc);
	}

	public HireCategory findHireCategory(int id)
	{
		return this.hireCategoryDao.find(id);
	}

	public List<HireCategory> getAllHireCategories()
	{
		return this.hireCategoryDao.findAll();
	}

	public List<HireCategory> getCategoriesForSection(int sectionId)
	{
		return this.hireCategoryDao.getCategoriesForSection(sectionId);
	}

	public void insertHireCategory(HireCategory hc)
	{
		this.hireCategoryDao.persist(hc);
	}

	public void saveOrUpdateHireCategory(HireCategory hc)
	{
		this.hireCategoryDao.saveOrUpdate(hc);
	}

	public void setHireCategoryDao(HireCategoryDao hireCategoryDao)
	{
		this.hireCategoryDao = hireCategoryDao;
	}

	public void updateHireCategory(HireCategory hc)
	{
		this.hireCategoryDao.update(hc);
	}

}