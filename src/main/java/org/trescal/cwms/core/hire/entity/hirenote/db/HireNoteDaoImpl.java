package org.trescal.cwms.core.hire.entity.hirenote.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.Hire_;
import org.trescal.cwms.core.hire.entity.hirenote.HireNote;
import org.trescal.cwms.core.hire.entity.hirenote.HireNote_;

@Repository("HireNoteDao")
public class HireNoteDaoImpl extends BaseDaoImpl<HireNote, Integer> implements HireNoteDao {
	
	@Override
	protected Class<HireNote> getEntity() {
		return HireNote.class;
	}

	@Override
	public List<HireNote> getActiveHireNotesForHire(int hireid) {
		return getResultList(cb ->{
			CriteriaQuery<HireNote> cq = cb.createQuery(HireNote.class);
			Root<HireNote> root = cq.from(HireNote.class);
			Join<HireNote, Hire> hire = root.join(HireNote_.hire);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(hire.get(Hire_.id), hireid));
			clauses.getExpressions().add(cb.isTrue(root.get(HireNote_.active)));
			cq.where(clauses);
			return cq;
		});
	}
}