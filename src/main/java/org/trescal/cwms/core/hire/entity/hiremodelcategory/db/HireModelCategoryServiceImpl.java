package org.trescal.cwms.core.hire.entity.hiremodelcategory.db;

import java.util.List;

import org.trescal.cwms.core.hire.entity.hiremodelcategory.HireModelCategory;

public class HireModelCategoryServiceImpl implements HireModelCategoryService
{
	HireModelCategoryDao hireModelCategoryDao;

	public void deleteHireModelCategory(HireModelCategory hmc)
	{
		this.hireModelCategoryDao.remove(hmc);
	}

	public HireModelCategory findHireModelCategory(int id)
	{
		return this.hireModelCategoryDao.find(id);
	}

	public List<HireModelCategory> getAllHireModelCategorys()
	{
		return this.hireModelCategoryDao.findAll();
	}

	public List<String> getDistinctPlantCodeList()
	{
		return this.hireModelCategoryDao.getDistinctPlantCodeList();
	}

	public int getNextModelCodeForCategory(int categoryid)
	{
		return this.hireModelCategoryDao.getNextModelCodeForCategory(categoryid);
	}

	public void insertHireModelCategory(HireModelCategory hmc)
	{
		this.hireModelCategoryDao.persist(hmc);
	}

	public void saveOrUpdateHireModelCategory(HireModelCategory hmc)
	{
		this.hireModelCategoryDao.saveOrUpdate(hmc);
	}

	public void setHireModelCategoryDao(HireModelCategoryDao hireModelCategoryDao)
	{
		this.hireModelCategoryDao = hireModelCategoryDao;
	}

	public void updateHireModelCategory(HireModelCategory hmc)
	{
		this.hireModelCategoryDao.update(hmc);
	}

}