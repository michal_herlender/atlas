package org.trescal.cwms.core.hire.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.hire.form.HireForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.servicetype.dto.ServiceTypeSimpleDto;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class SourceHireInstrumentsController
{
	@Value("#{props['cwms.config.hire.defaultukascal']}")
	private String defaultUkasCal;
	@Value("#{props['cwms.config.hire.min_hire_days']}")
	private Integer minHireDays;
	@Autowired
	private CalibrationTypeService calTypeServ;	
	@Autowired
	private HireService hireServ;
	@Autowired
	private SupportedCurrencyService currencyServ;
	
	@ModelAttribute("form")
	protected HireForm formBackingObject(@RequestParam(value = "id", required = false, defaultValue = "0") Integer id) throws Exception
	{
		HireForm form = new HireForm();
		if (id != 0) form.setHire(this.hireServ.get(id));
		return form;
	}



	@RequestMapping(value="/sourcehireinstruments.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception
	{
		Map<String, Object> refData = new HashMap<String, Object>();
		// get list of calibration types
		refData.put("caltypes", this.calTypeServ.getCalTypes()
				.stream().map(CalibrationType::getServiceType)
				.map(ServiceTypeSimpleDto::fromServiceType).collect(Collectors.toList())
		);
		// get the default currency
		refData.put("defaultCurrency", this.currencyServ.getDefaultCurrency());
		// add default ukas cost to refdata
		refData.put("defaultUkasCal", this.defaultUkasCal);
		return new ModelAndView("trescal/core/hire/sourcehireinstruments", refData);
	}
}