package org.trescal.cwms.core.hire.entity.hiresuspenditem.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.hire.entity.hiresuspenditem.HireSuspendItem;

public interface HireSuspendItemDao extends BaseDao<HireSuspendItem, Integer> {}