package org.trescal.cwms.core.hire.entity.hirestatus;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.system.entity.status.Status;

import javax.persistence.*;
import java.util.Set;

/**
 * A {@link Status} for a {@link Hire}.
 *
 * @author stuart
 */
@Entity
@DiscriminatorValue("hire")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class HireStatus extends Status
{
	private Set<Hire> hires;

	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "status")
	@JsonIgnore
	public Set<Hire> getHires()
	{
		return this.hires;
	}

	public void setHires(Set<Hire> hires)
	{
		this.hires = hires;
	}

}
