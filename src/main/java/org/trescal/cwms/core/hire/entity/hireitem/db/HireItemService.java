package org.trescal.cwms.core.hire.entity.hireitem.db;

import io.vavr.control.Either;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.dto.HireSearchResultItem;
import org.trescal.cwms.core.hire.dto.OffHireAccessoryDTO;
import org.trescal.cwms.core.hire.dto.ViewHireItemDto;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

public interface HireItemService {
    /**
     * this method deletes all hire items with a matching id to that passed in
     * the array
     *
     * @param delIds array of hire item ids
     * @return {@link ResultWrapper}
     */
    ResultWrapper ajaxDeleteHireItems(int[] delIds);

    Either<String, Boolean> ajaxOffHireItem(int offhireId, String offHireCond, LocalDate offHireDate, List<OffHireAccessoryDTO> offHireAccs, boolean genOffHireDoc);

    void deleteHireItem(HireItem hi);

    HireItem findHireItem(int id);

    ResultWrapper findHireItemDWR(int id);

    List<HireItem> getAllHireItems();

    /**
	 * this method returns a {@link List} of all items on a hire regardless of
	 * their state
	 * 
	 * @param hireid id of the {@link HireItem}
	 * @return {@link List} of {@link HireItem}
	 */
	List<HireItem> getAllHireItemsOnHire(int hireid);

	/**
	 * this method returns a {@link List} of all items currently on hire and not
	 * on enquiries
	 * 
	 * @return {@link List} of {@link HireItem}
	 */
	List<HireItem> getAllItemsCurrentlyOnHire();

	/**
	 * this method returns a {@link List} of all items on a hire that have been
	 * off hired
	 * 
	 * @param hireid id of the {@link Hire}
	 * @return {@link List} of {@link HireItem}
	 */
	List<HireItem> getAllOffHiredItemsOnHire(int hireid);

	/**
	 * this method gets the next item number for a {@link HireItem}
	 * 
	 * @param hireid id of the {@link Hire} to get item no for
	 * @return {@link Integer}
	 */
	int getNextItemNo(int hireid);

	/**
	 * this method checks all items on a hire are ready for hire (i.e. not
	 * already on hire because of a previous hire contract)
	 * 
	 * @param hire {@link Hire} object
	 * @return {@link Boolean}
	 */
	boolean hireItemsReadyForHire(Hire hire);

	void insertHireItem(HireItem hi);

    /**
     * this method replaces the {@link Instrument} on a {@link HireItem} because
     * of damage/out of calibration etc..
     *
     * @param hireItemId id of the {@link HireItem}
     * @param newPlantId id of the new {@link Instrument}
     * @param reason     the reason for the swap
     * @return {@link ResultWrapper}
     */
    Either<String, Boolean> replaceHireItemInstrument(int hireItemId, int newPlantId, String reason);

    /**
     * this method resets the off hire properties of a hire item and
     * re-generates the off hire document to give a proper representation of
     * what has happened
     *
     * @param hireItemId id of the {@link HireItem}
     * @return {@link ResultWrapper}
     */
    Either<String, Boolean> resetOffHireDate(int hireItemId);

    void saveOrUpdateHireItem(HireItem hi);

    void updateHireItem(HireItem hi);

    Either<String, ViewHireItemDto> updateHireItemValues(Integer hireItemId, Integer calTypeId, BigDecimal hireCost, BigDecimal ukasCalCost);

    /**
     * this method updates a few of the properties on the hire item so long as
     * it is just an enquiry or the hire is on-going (i.e. still a contract)
     *
     * @param hireItemId  id of the {@link HireItem}
     * @param calTypeId   id of the chosen {@link CalibrationType}
     * @param hireCost    cost to hire the item
     * @param ukasCalCost cost to calibrate the item to ukas
     * @return {@link ResultWrapper}
     */
    ResultWrapper updateHireItemValues(int hireItemId, String calTypeId, String hireCost, String ukasCalCost);

    List<HireSearchResultItem> findItemsByHireIds(Collection<Integer> ids);
}