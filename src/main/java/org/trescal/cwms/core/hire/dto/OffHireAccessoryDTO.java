package org.trescal.cwms.core.hire.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OffHireAccessoryDTO {
    private Integer hiaccid;
    private Boolean returned;
    private Integer statusid;

}
