package org.trescal.cwms.core.hire.controller;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.hire.dto.HireBasketItemDto;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hireaccessory.db.HireAccessoryService;
import org.trescal.cwms.core.hire.entity.hirecrossitem.HireCrossItem;
import org.trescal.cwms.core.hire.entity.hirecrossitem.db.HireCrossItemService;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.hire.entity.hireitem.db.HireItemService;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessory;
import org.trescal.cwms.core.hire.form.HireForm;
import org.trescal.cwms.core.instrument.entity.hireinstrument.db.HireInstrumentService;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Component
public class HireProcessFormUtil {

    @Autowired
    private HireInstrumentService hireInstrumentService;
    @Autowired
    private CalibrationTypeService calTypeServ;
    @Autowired
    private HireAccessoryService hireAccServ;
    @Autowired
    private HireCrossItemService hireCrossItemServ;
    @Autowired
    private HireItemService hitemServ;

    private HireItem processHireItem(HireBasketItemDto item, AtomicInteger itemNo) {
        return item.getPlantId() != null && item.getPlantId() > 0 ? createHireItem(item, itemNo) :
                createCrossHireItem(item, itemNo);
    }

    private HireItem createCrossHireItem(HireBasketItemDto item, AtomicInteger itemNo) {
        val hi = new HireItem();
        val hci = new HireCrossItem();
        hci.setDescription(item.getDescription());
        hci.setSerialno(item.getSerialNo());
        hci.setPlantno(item.getPlantNo());
        hireCrossItemServ.save(hci);
        hi.setHireCrossItem(hci);
        hi.setItemno(itemNo.getAndIncrement());
        fillHireItemFromBasketItem(item, hi);
        return hi;
    }

    private HireItem createHireItem(HireBasketItemDto item, AtomicInteger itemNo) {
        val hi = new HireItem();
        hi.setItemno(itemNo.getAndIncrement());
        val maybeHireInstrument = hireInstrumentService.getHireInstrumentByPlantId(item.getPlantId());
        if (!maybeHireInstrument.isPresent())
            throw new IllegalArgumentException(String.format("Instrument %d is not a hire instrument!", item.getPlantId()));
        maybeHireInstrument.ifPresent(hi::setHireInst);
        fillHireItemFromBasketItem(item, hi);
        val accessories = maybeHireInstrument.map(hinst -> hireAccServ.getAllAccessoriesForHireInstrument(hinst, true)).orElseGet(Collections::emptyList);
        val hireItemAccessories = accessories.stream().map(ac ->
                new HireItemAccessory(ac, hi, false)
        ).collect(Collectors.toList());
        val accessoriesSet = hi.getAccessories();
        accessoriesSet.addAll(hireItemAccessories);
        hi.setAccessories(accessoriesSet);
        return hi;
    }

    private void fillHireItemFromBasketItem(HireBasketItemDto item, HireItem hi) {
        val zero = new BigDecimal("0.00");
        hi.setCalType(calTypeServ.get(item.getCalTypeId()));
        hi.setHireCost(item.getHireCost());
        hi.setOffHire(false);
        hi.setUkasCalCost(item.getUkas());
        hi.setQuantity(1);
        hi.setGeneralDiscountValue(zero);
        hi.setGeneralDiscountRate(zero);
        hi.setTotalCost(zero);
        hi.setFinalCost(zero);
    }

    public Hire processItems(HireForm form ) {
        val hire = form.getHire()!=null?form.getHire():new Hire();
        if(form.getItems() == null || form.getItems().isEmpty()) return hire;
        final AtomicInteger itemNo = form.getHire()!=null? new AtomicInteger(this.hitemServ.getNextItemNo(hire.getId())):new AtomicInteger(1);
        val hireItems = form.getItems().stream().map(item -> this.processHireItem(item, itemNo)).peek(it -> it.setHire(hire)).collect(Collectors.toList());
        val items = hire.getItems();
        items.addAll(hireItems);
        hire.setItems(items);
        return hire;
    }
}
