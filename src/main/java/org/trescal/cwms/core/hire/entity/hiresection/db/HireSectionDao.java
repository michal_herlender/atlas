package org.trescal.cwms.core.hire.entity.hiresection.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.hire.entity.hiresection.HireSection;

public interface HireSectionDao extends BaseDao<HireSection, Integer> {}