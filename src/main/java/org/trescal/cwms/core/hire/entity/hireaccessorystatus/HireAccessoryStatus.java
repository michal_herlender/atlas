package org.trescal.cwms.core.hire.entity.hireaccessorystatus;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.trescal.cwms.core.hire.entity.hireaccessory.HireAccessory;
import org.trescal.cwms.core.system.entity.status.Status;

/**
 * A {@link Status} for a {@link HireAccessory}.
 * 
 * @author stuart
 */
@Entity
@DiscriminatorValue("hireaccessory")
public class HireAccessoryStatus extends Status
{
	private Set<HireAccessory> hireAccessories;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "status")
	public Set<HireAccessory> getHireAccessories()
	{
		return this.hireAccessories;
	}

	public void setHireAccessories(Set<HireAccessory> hireAccessories)
	{
		this.hireAccessories = hireAccessories;
	}

}
