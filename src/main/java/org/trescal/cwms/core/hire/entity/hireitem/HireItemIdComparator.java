package org.trescal.cwms.core.hire.entity.hireitem;

import java.util.Comparator;

/**
 * comparator class for sorting Hire Items by hire date decending
 * 
 * @author stuarth
 */
public class HireItemIdComparator implements Comparator<HireItem>
{
	public int compare(HireItem hItem1, HireItem hItem2)
	{
		Integer hi1Id = hItem1.getId();
		Integer hi2Id = hItem2.getId();

		return hi2Id.compareTo(hi1Id);
	}
}