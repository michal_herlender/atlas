package org.trescal.cwms.core.hire.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HireInstrumentDto {
    private Integer plantId;
    private String plantNo;
    private String serialNo;
    private String fullInstrumentModelName;
    private String hireCost;
    private Boolean scrapped;
    private Boolean offHire;

}
