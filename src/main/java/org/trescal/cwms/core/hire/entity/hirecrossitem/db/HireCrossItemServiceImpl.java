package org.trescal.cwms.core.hire.entity.hirecrossitem.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.hire.entity.hirecrossitem.HireCrossItem;

@Service("HireCrossItemService")
public class HireCrossItemServiceImpl extends BaseServiceImpl<HireCrossItem, Integer> implements HireCrossItemService
{
	@Autowired
	private HireCrossItemDao hireCrossItemDao;
	
	@Override
	protected BaseDao<HireCrossItem, Integer> getBaseDao() {
		return hireCrossItemDao;
	}
}