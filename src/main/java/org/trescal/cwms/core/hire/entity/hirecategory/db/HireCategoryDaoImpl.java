package org.trescal.cwms.core.hire.entity.hirecategory.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.hire.entity.hirecategory.HireCategory;
import org.trescal.cwms.core.hire.entity.hirecategory.HireCategory_;
import org.trescal.cwms.core.hire.entity.hiresection.HireSection;
import org.trescal.cwms.core.hire.entity.hiresection.HireSection_;

@Repository("HireCategoryDao")
public class HireCategoryDaoImpl extends BaseDaoImpl<HireCategory, Integer> implements HireCategoryDao {
	
	@Override
	protected Class<HireCategory> getEntity() {
		return HireCategory.class;
	}

	@Override
	public List<HireCategory> getCategoriesForSection(int sectionId) {
		return getResultList(cb ->{
			CriteriaQuery<HireCategory> cq = cb.createQuery(HireCategory.class);
			Root<HireCategory> root = cq.from(HireCategory.class);
			Join<HireCategory, HireSection> hireSection = root.join(HireCategory_.hireSection);
			cq.where(cb.equal(hireSection.get(HireSection_.id), sectionId));
			cq.orderBy(cb.asc(root.get(HireCategory_.categorycode)));
			return cq;
		});
	}
}