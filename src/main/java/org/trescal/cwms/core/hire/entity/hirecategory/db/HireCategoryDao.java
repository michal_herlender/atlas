package org.trescal.cwms.core.hire.entity.hirecategory.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.hire.entity.hirecategory.HireCategory;

public interface HireCategoryDao extends BaseDao<HireCategory, Integer> {
	
	List<HireCategory> getCategoriesForSection(int sectionId);
}