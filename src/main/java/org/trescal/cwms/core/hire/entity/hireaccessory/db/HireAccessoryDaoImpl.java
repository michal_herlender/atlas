package org.trescal.cwms.core.hire.entity.hireaccessory.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.hire.entity.hireaccessory.HireAccessory;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;

@Repository("HireAccessoryDao")
public class HireAccessoryDaoImpl extends BaseDaoImpl<HireAccessory, Integer> implements HireAccessoryDao {
	
	@Override
	protected Class<HireAccessory> getEntity() {
		return HireAccessory.class;
	}
	
	public HireAccessory findEagerHireAccessory(int id) {
		Criteria criteria = getSession().createCriteria(HireAccessory.class);
		criteria.add(Restrictions.idEq(id));
		criteria.setFetchMode("status", FetchMode.JOIN);
		return (HireAccessory) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<HireAccessory> getAccessoriesForHireInstrument(HireInstrument hireInst, boolean active, boolean available) {
		Criteria criteria = getSession().createCriteria(HireAccessory.class);
		Criteria hireInstCrit = criteria.createCriteria("hireInst");
		hireInstCrit.add(Restrictions.idEq(hireInst.getId()));
		// only available status?
		if (available) {
			Criteria statusCrit = criteria.createCriteria("status");
			statusCrit.add(Restrictions.eq("name", "Available"));
		}
		else criteria.setFetchMode("status", FetchMode.JOIN);
		criteria.add(Restrictions.eq("active", active));
		criteria.addOrder(Order.asc("item"));
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<HireAccessory> getDistinctAccessoriesForHireModel(HireInstrument hireInst) {
		Criteria criteria = getSession().createCriteria(HireAccessory.class);
		Criteria hireInstCrit = criteria.createCriteria("hireInst");
		Criteria instCrit = hireInstCrit.createCriteria("inst");
		Criteria modelCrit = instCrit.createCriteria("model");
		modelCrit.add(Restrictions.idEq(hireInst.getInst().getModel().getModelid()));
		criteria.setProjection(Projections.distinct(Projections.projectionList().add(Projections.property("item"))));
		criteria.addOrder(Order.asc("item"));
		return criteria.list();
	}
}