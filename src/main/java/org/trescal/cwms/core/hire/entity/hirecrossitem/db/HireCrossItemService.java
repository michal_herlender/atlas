package org.trescal.cwms.core.hire.entity.hirecrossitem.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.hire.entity.hirecrossitem.HireCrossItem;

public interface HireCrossItemService extends BaseService<HireCrossItem, Integer> {}