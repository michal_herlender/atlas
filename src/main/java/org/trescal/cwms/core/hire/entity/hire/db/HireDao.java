package org.trescal.cwms.core.hire.entity.hire.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.hire.dto.HireSearchResultDto;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hirestatus.HireStatus;
import org.trescal.cwms.core.hire.form.HireHomeForm;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface HireDao extends AllocatedDao<Subdiv, Hire, Integer>
{
	List<Hire> findHiresForCourierDespatch(int cdId);

	List<Hire> getAllActiveHireContractsForStatus(HireStatus status, Subdiv allocatedSubdiv);

	List<Hire> getAllHireEnquiriesForStatus(HireStatus status, Boolean accountsVarified, Subdiv allocatedSubdiv);

	List<Hire> getAllHires();

	Hire getHireByExactHireNo(String hireNo);

	void insertHire(Hire hire);

    void saveOrUpdateHire(Hire hire);

	void updateHire(Hire hire);

    PagedResultSet<HireSearchResultDto> queryHire(HireHomeForm form, PagedResultSet<HireSearchResultDto> prs, Subdiv allocatedSubdiv);
	
	Long hireCountByCourier(Integer courierId);
	
	Long hireCountByCourierDespatchType(Integer cdtId);
}