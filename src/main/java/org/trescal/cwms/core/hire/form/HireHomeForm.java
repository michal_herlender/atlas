package org.trescal.cwms.core.hire.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HireHomeForm {
	private List<Hire> awaitingAccountsVarification;
	private List<Hire> awaitingInvoice;
	private String clientRef;
	private String coid;
	private List<Hire> contracts;
	private String desc;
	private Integer descid;
	private List<Hire> enquiries;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate hireDate1;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate hireDate2;
	private boolean hireDateBetween;

	private Integer hireInstId;
	private String hireNo;
	private Collection<Hire> hires;
	private String mfr;
	private Integer mfrid;
	private String model;

	private int pageNo;
	private String personid;
	private String plantid;
	private String plantno;
	private String purOrder;
	private int resultsPerPage;
	// results data
	private PagedResultSet<Hire> rs;
	private String searchCriteria;
	private String serialno;
	private String statusid;
	private List<KeyValue<Integer,String>> statusList;

}
