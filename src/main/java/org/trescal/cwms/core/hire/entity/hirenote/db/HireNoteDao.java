package org.trescal.cwms.core.hire.entity.hirenote.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.hire.entity.hirenote.HireNote;

public interface HireNoteDao extends BaseDao<HireNote, Integer> {
	
	List<HireNote> getActiveHireNotesForHire(int jobid);
}