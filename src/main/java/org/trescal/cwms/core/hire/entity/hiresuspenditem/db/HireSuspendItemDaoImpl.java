package org.trescal.cwms.core.hire.entity.hiresuspenditem.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.hire.entity.hiresuspenditem.HireSuspendItem;

@Repository("HireSuspendItemDao")
public class HireSuspendItemDaoImpl extends BaseDaoImpl<HireSuspendItem, Integer> implements HireSuspendItemDao {
	
	@Override
	protected Class<HireSuspendItem> getEntity() {
		return HireSuspendItem.class;
	}
}