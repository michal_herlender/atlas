package org.trescal.cwms.core.hire.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessory;

@Data
@AllArgsConstructor
@Builder
public class HireAccessoryDto {
    Integer itemno;
    String item;
    Boolean active;
    String status;

    public static HireAccessoryDto fromHireAccessory(HireItemAccessory hia) {
        return HireAccessoryDto.builder()
            .item(hia.getHireAccessory().getItem())
            .itemno(hia.getHireItem().getItemno())
            .active(hia.getHireAccessory().isActive())
            .status(hia.getHireAccessory().getStatus().getName())
            .build();
    }
}
