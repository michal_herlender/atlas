package org.trescal.cwms.core.hire.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.db.CourierDespatchService;
import org.trescal.cwms.core.deliverynote.entity.freehandcontact.FreehandContact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.hire.dto.ViewHireItemDto;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.hire.entity.hireaccessorystatus.HireAccessoryStatus;
import org.trescal.cwms.core.hire.entity.hireitem.db.HireItemService;
import org.trescal.cwms.core.hire.entity.hirenote.HireNote;
import org.trescal.cwms.core.hire.form.HireForm;
import org.trescal.cwms.core.hire.form.HireValidator;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.db.InvoiceJobLinkService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.instructionlink.db.InstructionLinkService;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller used to view and edit {@link Hire} entities.
 */
@Controller @IntranetController
@SessionAttributes({Constants.HTTPSESS_NEW_FILE_LIST, Constants.SESSION_ATTRIBUTE_SUBDIV, 
	Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_USERNAME})
public class ViewHireController 
{
	private static final Logger logger = LoggerFactory.getLogger(ViewHireController.class);
	
	@Autowired
	private BusinessDetailsService bdServ;
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private CourierDespatchService cdServ;
	@Autowired
	private SupportedCurrencyService currencyServ;
	@Value("#{props['cwms.config.hire.defaultukascal']}")
	public String defaultUkasCal;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private HireService hireServ;
	@Autowired
	private HireItemService hiServ;
	@Autowired
	private InstructionLinkService instructionLinkService;
	@Autowired
	private InvoiceJobLinkService invJobLinkServ;
	@Value("#{props['cwms.config.hire.min_hire_days']}")
	private int minHireDays;
	@Autowired
	private SystemComponentService scServ;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService; 
	@Autowired
	private HireValidator validator;
	
	public static final String FORM_NAME = "form";
	
	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
		return new ArrayList<String>();
	}
	
	@ModelAttribute(FORM_NAME)
	protected HireForm formBackingObject(
			@RequestParam(value="id", required=false, defaultValue="0") Integer id) throws Exception
	{
		Hire hire = this.hireServ.get(id);
		if ((id == 0) || (hire == null)) {
			logger.error("Hire with id " + id + " could not be found");
			throw new Exception("Could not find hire");
		}
		else {
			// create new hire form
			HireForm form = new HireForm();
			// add sent emails
			hire.setSentEmails(this.emailServ.getAllComponentEmails(Component.HIRE, id));
			// add any related instructions
			hire.setRelatedInstructions(this.instructionLinkService.getAllForHire(hire));
			// set freehand contact on form
			form.setFreehandDelivery(false);
			// freehand contact?
			if (hire.getFreehandContact() != null) {
				form.setFreehandDelivery(true);
			}
			// set hire onto form
			form.setHire(hire);
			// set items ready for hire onto form
			form.setItemsReadyForHire(this.hiServ.hireItemsReadyForHire(hire));
			return form;
		}
	}
	
	@InitBinder(FORM_NAME)
    public void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
	
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception
	{
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}
	
	private void onBind(HireForm form) throws Exception
	{
		// create new address string
		String addrString = "";
		// check freehand delivery checked?
		if (form.isFreehandDelivery()) {
			// create new freehand contact
			FreehandContact fc = new FreehandContact();
			// freehand contact already on hire?
			if (form.getHire().getFreehandContact() != null) {
				fc = form.getHire().getFreehandContact();
			}
			// delivery address supplied?
			if ((form.getFreeAddr1().length() > 0)
					|| (form.getFreeAddr2().length() > 0)
					|| (form.getFreeTown().length() > 0)
					|| (form.getFreeCounty().length() > 0)
					|| (form.getFreePostcode().length() > 0))
			{
				// add address elements to string delimitted by comma's
				addrString = addrString.concat(form.getFreeAddr1() + ","
						+ form.getFreeAddr2() + "," + form.getFreeTown() + ","
						+ form.getFreeCounty() + "," + form.getFreePostcode());
			}
			// set onto freehand contact
			fc.setContact(form.getFreeName());
			fc.setCompany(form.getFreeCompany());
			fc.setAddress(addrString);
			// add freehand contact to form
			form.getHire().setFreehandContact(fc);
		}
		else {
			// null freehand delivery
			form.getHire().setFreehandContact(null);
		}
	}
	
	@RequestMapping(value="/viewhire.htm", method=RequestMethod.POST)
	protected String onSubmit(Model model,
			@ModelAttribute(FORM_NAME) HireForm form) throws Exception
	{
		//onbind
		onBind(form);
		// get hire
		Hire hire = form.getHire();
		// set currency
		this.currencyServ.setCurrencyFromForm(hire, form.getCurrencyCode(), hire.getContact().getSub().getComp());
		// check hire freehand contact
		FreehandContact fc = hire.getFreehandContact();
		// if freehand details are empty
		if (fc != null && fc.isEmpty()) {
			// set to null so the object is not persisted
			hire.setFreehandContact(null);
		}
		// update hire
		this.hireServ.updateHire(hire);
		model.asMap().clear();
		return "redirect:viewhire.htm?id=" + hire.getId();
	}

	@RequestMapping(value="/viewhire.htm", method=RequestMethod.GET)
	protected String referenceData(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@ModelAttribute(FORM_NAME) HireForm form) throws Exception
	{
		// get files in the root directory
		FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ.getFilesForComponentRoot(Component.HIRE, form.getHire().getId(), newFiles);
		model.addAttribute("currencyList", this.currencyServ.getAllSupportedCurrencys());
		model.addAttribute("invoices", this.invJobLinkServ.getLinksForJobNo(form.getHire().getHireno()));
		model.addAttribute("files", form.getHire().getDirectory() == null ? null : form.getHire().getDirectory().listFiles());
		model.addAttribute(Constants.REFDATA_SYSTEM_COMPONENT, this.scServ.findComponent(Component.HIRE));
		model.addAttribute(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
		model.addAttribute(Constants.REFDATA_FILES_NAME, this.fileBrowserServ.getFilesName(fileBrowserWrapper));
		// get the default currency
		model.addAttribute("defaultCurrency", this.currencyServ.getDefaultCurrency());
		// get private only notes value
		model.addAttribute("privateOnlyNotes", HireNote.privateOnly);
		// get minimum days hire allowed
		model.addAttribute("minHireDays", this.minHireDays);
		// get list of calibration types
		model.addAttribute("caltypes", this.calTypeServ.getCalTypes()
			.stream().map(ct -> new KeyValue<>(ct.getCalTypeId(), ct.getServiceType().getShortName())).collect(Collectors.toList())
		);
		// add a list of currency options for this company
		model.addAttribute("currencyopts", this.currencyServ.getCompanyCurrencyOptions(form.getHire().getContact().getSub().getComp().getCoid()));
		// add default ukas cost to refdata
		model.addAttribute("defaultUkasCal", this.defaultUkasCal);
		// get all hire accessory options
		model.addAttribute("hireAccStatuss", this.statusServ.getAllStatuss(HireAccessoryStatus.class));
		// user has completion permission
		// member of accounts or admin
		Contact contact = this.userService.get(username).getCon();
		Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
		boolean canComplete = this.conServ.contactBelongsToDepartmentOrIsAdmin(contact, DepartmentType.ACCOUNTS, allocatedSubdiv.getComp());
		// get business details
		model.addAttribute(Constants.SESSION_ATTRIBUTE_BUSINESS_DETAILS, this.bdServ.getAllBusinessDetails(contact, (allocatedSubdiv.getDefaultAddress() != null ? allocatedSubdiv.getDefaultAddress() : (allocatedSubdiv.getAddresses().size() > 0 ? allocatedSubdiv.getAddresses().iterator().next() : null)), allocatedSubdiv.getComp()));
		model.addAttribute("canCompleteHire", canComplete);
		model.addAttribute("currentContact", contact);
		model.addAttribute("items", form.getHire().getItems().stream().map(ViewHireItemDto::fromHireItem).collect(Collectors.toList()));
		this.cdServ.prepareCourierDespatchData(subdivDto.getKey(), companyDto.getKey(), model);
		return "trescal/core/hire/viewhire";
	}
}