package org.trescal.cwms.core.hire.entity.hirereplacedinstrument;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "hirereplacedinstrument")
public class HireReplacedInstrument extends Auditable {
	private HireInstrument hireInst;
	private HireItem hireItem;
	private int id;
	private String reason;
	private Contact replacedBy;
	private LocalDate replacedOn;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hireinstid")
	public HireInstrument getHireInst() {
		return this.hireInst;
	}

	public void setHireInst(HireInstrument hireInst) {
		this.hireInst = hireInst;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hireitemid")
	public HireItem getHireItem() {
		return this.hireItem;
	}

	public void setHireItem(HireItem hireItem) {
		this.hireItem = hireItem;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Length(max = 400)
	@Column(name = "reason", length = 400)
	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "replacedbyid", nullable = false)
    public Contact getReplacedBy() {
        return this.replacedBy;
    }

    public void setReplacedBy(Contact replacedBy) {
        this.replacedBy = replacedBy;
    }

    @Column(name = "replacedon", columnDefinition = "date")
    public LocalDate getReplacedOn() {
        return this.replacedOn;
    }

    public void setReplacedOn(LocalDate replacedOn) {
        this.replacedOn = replacedOn;
    }

}
