package org.trescal.cwms.core.hire.dto;

import io.vavr.Tuple;
import io.vavr.Tuple3;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.val;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.hire.entity.hirecrossitem.HireCrossItem;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.TranslationUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@Builder
@AllArgsConstructor
public class ViewHireItemDto {

    private Integer id;
    private Integer calTypeId;
    private Integer itemno;
    private String calTypeShorName;
    private Integer plantId;
    private String plantNo;
    private String serialNo;
    private String modelName;
    private String description;
    private BigDecimal ukasCalCost;
    private BigDecimal hireCost;
    private BigDecimal finalCost;

    private Boolean suspended;
    private Boolean offHire;

    private List<Tuple3<Boolean, Boolean, Integer>> instHires;

    List<HireSuspendItemsDto> suspendItems;
    private Integer hireDays;
    private Integer daysSuspended;
    private LocalDate offHireDate;
    private String offHireCondition;


    public static ViewHireItemDto fromHireItem(HireItem hireItem) {
        val hireInst = Optional.ofNullable(hireItem.getHireInst());
        val inst = hireInst.map(HireInstrument::getInst);
        val crossItem = Optional.ofNullable(hireItem.getHireCrossItem());
        val calType = Optional.ofNullable(hireItem.getCalType());
        return ViewHireItemDto.builder()
            .id(hireItem.getId())
            .itemno(hireItem.getItemno())
            .calTypeId(calType.map(CalibrationType::getCalTypeId).orElse(null))
            .calTypeShorName(
                calType.map(CalibrationType::getServiceType).map(ServiceType::getShortnameTranslation)
                    .flatMap(TranslationUtils::getBestTranslation).orElse(null))
            .plantId(inst.map(Instrument::getPlantid).orElse(null))
            .plantNo(inst.map(Instrument::getPlantno).orElse(crossItem.map(HireCrossItem::getPlantno).orElse(null)))
            .serialNo(inst.map(Instrument::getSerialno).orElse(crossItem.map(HireCrossItem::getSerialno).orElse(null)))
            .modelName(inst.map(i -> InstModelTools.instrumentModelNameViaTranslations(i, false, LocaleContextHolder.getLocale(), Locale.getDefault())).orElse(null))
            .description(inst.map(Instrument::getCustomerDescription).orElse(crossItem.map(HireCrossItem::getDescription).orElse(null)))
            .suspended(hireItem.isSuspended())
            .offHire(hireItem.isOffHire())
            .ukasCalCost(hireItem.getUkasCalCost())
            .hireCost(hireItem.getHireCost())
            .finalCost(hireItem.getFinalCost())
            .daysSuspended(hireItem.getDaysSuspended())
            .hireDays(hireItem.isOffHire()
                ? hireItem.getOffHireDays() != null ? hireItem.getOffHireDays() : 0 : (int) hireItem.getDaysOnHire())
            .offHireDate(hireItem.getOffHireDate())
            .offHireCondition(hireItem.getOffHireCondition())
            .suspendItems(hireItem.getSuspenditems().stream().map(his -> HireSuspendItemsDto.builder().startDate(his.getStartDate()).endDate(his.getEndDate()).daysSuspended(his.getDaysSuspended()).build()).collect(Collectors.toList()))
            .instHires(hireInst.map(HireInstrument::getHireItems).map(Collection::stream)
                .orElse(Stream.empty()).map(hi -> Tuple.of(hi.getHire().isEnquiry(), hi.isOffHire(), hi.getId())).collect(Collectors.toList())
            )
            .build();
    }
}


@Data
@AllArgsConstructor
@Builder
class HireSuspendItemsDto {
    private LocalDate startDate;
    private LocalDate endDate;
    private Long daysSuspended;
}
