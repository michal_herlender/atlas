package org.trescal.cwms.core.hire.entity.hirereplacedinstrument.db;

import java.util.List;

import org.trescal.cwms.core.hire.entity.hirereplacedinstrument.HireReplacedInstrument;

public class HireReplacedInstrumentServiceImpl implements HireReplacedInstrumentService
{
	private HireReplacedInstrumentDao hireReplacedInstrumentDao;

	public HireReplacedInstrument findHireReplacedInstrument(int id)
	{
		return hireReplacedInstrumentDao.find(id);
	}

	public void insertHireReplacedInstrument(HireReplacedInstrument HireReplacedInstrument)
	{
		hireReplacedInstrumentDao.persist(HireReplacedInstrument);
	}

	public void updateHireReplacedInstrument(HireReplacedInstrument HireReplacedInstrument)
	{
		hireReplacedInstrumentDao.update(HireReplacedInstrument);
	}

	public List<HireReplacedInstrument> getAllHireReplacedInstruments()
	{
		return hireReplacedInstrumentDao.findAll();
	}

	public void setHireReplacedInstrumentDao(HireReplacedInstrumentDao hireReplacedInstrumentDao)
	{
		this.hireReplacedInstrumentDao = hireReplacedInstrumentDao;
	}

	public void deleteHireReplacedInstrument(HireReplacedInstrument hirereplacedinstrument)
	{
		this.hireReplacedInstrumentDao.remove(hirereplacedinstrument);
	}
}