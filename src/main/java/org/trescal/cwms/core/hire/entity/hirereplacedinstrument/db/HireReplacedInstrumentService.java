package org.trescal.cwms.core.hire.entity.hirereplacedinstrument.db;

import java.util.List;

import org.trescal.cwms.core.hire.entity.hirereplacedinstrument.HireReplacedInstrument;

public interface HireReplacedInstrumentService
{
	void deleteHireReplacedInstrument(HireReplacedInstrument hirereplacedinstrument);

	HireReplacedInstrument findHireReplacedInstrument(int id);

	List<HireReplacedInstrument> getAllHireReplacedInstruments();

	void insertHireReplacedInstrument(HireReplacedInstrument hirereplacedinstrument);

	void updateHireReplacedInstrument(HireReplacedInstrument hirereplacedinstrument);
}