package org.trescal.cwms.core.hire.view;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxView;
import org.trescal.cwms.core.hire.dto.HireInstrumentRevenueWrapper;
import org.trescal.cwms.core.hire.form.HireInstrumentRevenuesForm;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.reports.view.XlsxViewDecorator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Component
public class HireInstrumentRevenuesXlsxView extends AbstractXlsxView {
	@Autowired
	private MessageSource messages;

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
									  HttpServletResponse response) throws Exception {
		formatData(model, workbook);
		setFileName(response);
	}
	
	// Adapted from old Controller code
	private void formatData(Map<String, Object> model, Workbook wb) {
		Sheet sheet = wb.createSheet("Data");
		XlsxViewDecorator decorator = new XlsxViewDecorator(sheet, null);
		CellStyle boldStyle = decorator.getStyleHolder().getHeaderStyle(); 
		CellStyle integerStyle = decorator.getStyleHolder().getIntegerStyle();
		CellStyle monetaryStyle = decorator.getStyleHolder().getMonetaryStyle();
		Locale locale = LocaleContextHolder.getLocale();
		HireInstrumentRevenuesForm form = (HireInstrumentRevenuesForm) model.get("form");

		// create array of titles
		String[] titles = { 
				this.messages.getMessage("instrument", null, "Instrument", locale),
				this.messages.getMessage("company", null, "Plant No", locale),
				this.messages.getMessage("hireinstr.totalhires", null, "Total Hires", locale),
				this.messages.getMessage("hireinstr.dayshired", null, "Days Hired", locale),
				this.messages.getMessage("hireinstr.revenue", null, "Revenue", locale) };
		// add each title to a cell
		for (int colIndex = 0; colIndex < titles.length; colIndex++) {
			decorator.createCell(0, colIndex, titles[colIndex], boldStyle);
		}
		
		int rowIndex = 1;
		List<HireInstrumentRevenueWrapper> wrappers = form.getRevWrappers();
		for (HireInstrumentRevenueWrapper wrap : wrappers) {
			decorator.createCell(rowIndex, 0, wrap.getHi().getInst().getPlantno(), null);
			decorator.createCell(rowIndex, 1, wrap.getHi().getInst().getDefinitiveInstrument(), null);
			decorator.createCell(rowIndex, 2, wrap.getTimesHired(), integerStyle);
			decorator.createCell(rowIndex, 3, wrap.getDaysOnHire(), integerStyle);
			decorator.createCell(rowIndex, 4, wrap.getRevenue(), monetaryStyle);
			rowIndex++;
		}

		decorator.autoSizeColumns();
	}

	private void setFileName(HttpServletResponse response) {
		Locale locale = LocaleContextHolder.getLocale();

		String fileName =
			this.messages.getMessage("report.hireinstrumentrevenues.filename", null, "Hire Instrument Revenues", locale)
				.concat(" - ")
				.concat(DateTools.dtf_files.format(new Date()))
				.concat(".xlsx");

		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
	}

}
