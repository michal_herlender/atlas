package org.trescal.cwms.core.hire.entity.hirecrossitem.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.hire.entity.hirecrossitem.HireCrossItem;

@Repository("HireCrossItemDao")
public class HireCrossItemDaoImpl extends BaseDaoImpl<HireCrossItem, Integer> implements HireCrossItemDao
{
	@Override
	protected Class<HireCrossItem> getEntity() {
		return HireCrossItem.class;
	}
}