package org.trescal.cwms.core.hire.entity.hiresection.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.hire.entity.hiresection.HireSection;

@Repository("HireSectionDao")
public class HireSectionDaoImpl extends BaseDaoImpl<HireSection, Integer> implements HireSectionDao {
	
	@Override
	protected Class<HireSection> getEntity() {
		return HireSection.class;
	}
}