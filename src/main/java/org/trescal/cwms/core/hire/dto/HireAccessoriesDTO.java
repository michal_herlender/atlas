package org.trescal.cwms.core.hire.dto;

import java.util.List;

import org.trescal.cwms.core.hire.entity.hireaccessory.HireAccessory;

public class HireAccessoriesDTO
{
	private List<HireAccessory> activeHireInstAccessories;
	private List<HireAccessory> deactivatedHireInstAccessories;
	private List<HireAccessory> distinctModelAccessories;

	public List<HireAccessory> getActiveHireInstAccessories()
	{
		return this.activeHireInstAccessories;
	}

	public List<HireAccessory> getDeactivatedHireInstAccessories()
	{
		return this.deactivatedHireInstAccessories;
	}

	public List<HireAccessory> getDistinctModelAccessories()
	{
		return this.distinctModelAccessories;
	}

	public void setActiveHireInstAccessories(List<HireAccessory> activeHireInstAccessories)
	{
		this.activeHireInstAccessories = activeHireInstAccessories;
	}

	public void setDeactivatedHireInstAccessories(List<HireAccessory> deactivatedHireInstAccessories)
	{
		this.deactivatedHireInstAccessories = deactivatedHireInstAccessories;
	}

	public void setDistinctModelAccessories(List<HireAccessory> distinctModelAccessories)
	{
		this.distinctModelAccessories = distinctModelAccessories;
	}

}
