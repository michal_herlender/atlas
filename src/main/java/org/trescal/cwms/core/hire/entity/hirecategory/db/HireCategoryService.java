package org.trescal.cwms.core.hire.entity.hirecategory.db;

import java.util.List;

import org.trescal.cwms.core.hire.entity.hirecategory.HireCategory;

public interface HireCategoryService
{
	void deleteHireCategory(HireCategory hc);

	HireCategory findHireCategory(int id);

	List<HireCategory> getAllHireCategories();

	List<HireCategory> getCategoriesForSection(int sectionId);

	void insertHireCategory(HireCategory hc);

	void saveOrUpdateHireCategory(HireCategory hc);

	void updateHireCategory(HireCategory hc);
}