package org.trescal.cwms.core.hire.controller;

import io.vavr.control.Either;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.dto.HireInstrumentDto;
import org.trescal.cwms.core.hire.dto.HireSearchResultDto;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.db.HireItemAccessoryService;
import org.trescal.cwms.core.hire.entity.hiresuspenditem.db.HireSuspendItemService;
import org.trescal.cwms.core.hire.form.ConvertEnquiryToContractForm;
import org.trescal.cwms.core.hire.form.HireForm;
import org.trescal.cwms.core.hire.form.HireHomeForm;
import org.trescal.cwms.core.instrument.entity.hireinstrument.db.HireInstrumentService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/hire")
public class HireAjaxController {


    @Autowired
    private HireInstrumentService hireInstrumentService;

    @Autowired
    private HireItemAccessoryService hireItemAccessoryService;

    @Autowired
    private HireService hireServ;

    @Autowired
    private HireSuspendItemService hireSuspendItemService;

    @Autowired
    private HireProcessFormUtil processFormUtil;
    @Autowired
    private SubdivService subdivService;

    @InitBinder
    protected void initBinder(ServletRequestDataBinder binder) throws Exception {
        binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
    }


    @GetMapping("searchInstruments.json")
    List<HireInstrumentDto> searchInstruments(@RequestParam(required = false) String plantId,
                                              @RequestParam(required = false) String plantNo,
                                              @RequestParam(required = false) String serialNo,
                                              @RequestParam(required = false) String model,
                                              @RequestParam(required = false) String brand,
                                              @RequestParam(required = false) String subFamily,
                                              @SessionAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyKeyValue
    ) {
        return hireInstrumentService.searchHireInstruments(companyKeyValue.getKey(), plantId, plantNo, serialNo, model, brand, subFamily);
    }

    @ModelAttribute("form")
    protected HireForm formBackingObject(@RequestParam(value = "id", required = false, defaultValue = "0") Integer id) throws Exception {
        HireForm form = new HireForm();
        if (id != 0) form.setHire(this.hireServ.get(id));
        return form;
    }


    @PostMapping(path = "sourceHireInstruments.json", consumes = "application/x-www-form-urlencoded")
    Either<String, Map<String, String>> sourceHireInstruments(HttpServletRequest request, HttpServletResponse response,@ModelAttribute("form") HireForm form) {
        if (form.getHire() != null) {
            val hire = processFormUtil.processItems(form);
            hireServ.updateHire(hire);
            val uriBuilder = ServletUriComponentsBuilder.fromCurrentServletMapping().
                    path("/viewhire.htm").queryParam("id",hire.getId()).build();

            return Either.right(Collections.singletonMap("follow", uriBuilder.toString()));
        } else {
            val flashMap = RequestContextUtils.getOutputFlashMap(request);
            flashMap.putIfAbsent("sourceHireForm", form);
            val flashMapManager = RequestContextUtils.getFlashMapManager(request);
            flashMapManager.saveOutputFlashMap(flashMap, request, response);
            return Either.right(Collections.singletonMap("follow", ServletUriComponentsBuilder.fromCurrentServletMapping().path("/createhireenquiry.htm").build().toString()));
        }
    }

    @GetMapping("searchHires.json")
    PagedResultSet<HireSearchResultDto> searchHires(HireHomeForm form,
                                                    @SessionAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto
    ) {
        Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
        val prs = new PagedResultSet<HireSearchResultDto>(form.getResultsPerPage() == 0 ? Constants.RESULTS_PER_PAGE : form.getResultsPerPage(), form.getPageNo() == 0 ? 1 : form.getPageNo());
        return hireServ.queryHire(form, prs, allocatedSubdiv);
    }

    @GetMapping("validateHireItemAccessories.json")
    ResultWrapper validateHireItemAccessories(@RequestParam Integer hireId) {
        return hireItemAccessoryService.validateHireItemAccessories(hireId);
    }

    @PostMapping("convertEnquiryToContract.json")
    Either<String, Boolean> convertEnquiryToContract(ConvertEnquiryToContractForm form, @SessionAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> allocatedSubdiv) {
        return hireServ.convertEnquiryToContract(form.getHireId(), form.getClientPo(), form.getSpecialinst(), allocatedSubdiv.getKey());

    }

    @PutMapping("suspendHireItem.json")
    Either<String, Boolean> suspendHireItem(@RequestParam Integer hireItemId, @RequestParam LocalDate date) {
        return hireSuspendItemService.suspendHireItem(hireItemId, date);
    }

    @PutMapping("completeHireItemSuspension.json")
    Either<String, Boolean> completeHireItemSuspension(@RequestParam Integer hireItemId, @RequestParam LocalDate date) {
        return hireSuspendItemService.completeHireItemSuspension(hireItemId, date);
    }
}
