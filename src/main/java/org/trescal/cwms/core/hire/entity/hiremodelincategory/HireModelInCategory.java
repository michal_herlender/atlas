package org.trescal.cwms.core.hire.entity.hiremodelincategory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.hire.entity.hiremodelcategory.HireModelCategory;
import org.trescal.cwms.core.instrumentmodel.entity.hiremodel.HireModel;

@Entity
@Table(name = "hiremodelincategory")
public class HireModelInCategory
{
	private HireModel hireModel;
	private HireModelCategory hireModelCategory;
	private int id;

	/**
	 * @return the hire model
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hiremodelid")
	public HireModel getHireModel()
	{
		return this.hireModel;
	}

	/**
	 * @return the hire model category
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hiremodelcatid")
	public HireModelCategory getHireModelCategory()
	{
		return this.hireModelCategory;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	public void setHireModel(HireModel hireModel)
	{
		this.hireModel = hireModel;
	}

	public void setHireModelCategory(HireModelCategory hireModelCategory)
	{
		this.hireModelCategory = hireModelCategory;
	}

	public void setId(int id)
	{
		this.id = id;
	}

}
