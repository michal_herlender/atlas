package org.trescal.cwms.core.hire.entity.hire;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.freehandcontact.FreehandContact;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.hire.entity.hireitem.HireItemComparator;
import org.trescal.cwms.core.hire.entity.hirenote.HireNote;
import org.trescal.cwms.core.hire.entity.hirestatus.HireStatus;
import org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLink;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "hire", uniqueConstraints = {@UniqueConstraint(columnNames = {"hireno"})})
// overrides the column definition for contact inherited from Pricing - allows
// fk to be nullable (invoices don't have a contact)
@AssociationOverride(name = "contact", joinColumns = @JoinColumn(name = "personid"))
public class Hire extends Pricing<Subdiv> implements NoteAwareEntity, ComponentEntity {
	private boolean accountsVarified;
	private Address address;
	private CourierDespatch crdes;
	private File directory;
	private boolean enquiry;
	private LocalDate enquiryDate;
	private Integer enquiryEstDuration;
	private FreehandContact freehandContact;
	private LocalDate hireDate;
	private String hireno;
	private Set<HireItem> items;
	private Set<HireNote> notes;
	private String poNumber;
	private List<InstructionLink<?>> relatedInstructions;
	private List<Email> sentEmails;
	private String specialInst;
	private HireStatus status;
	private TransportOption transportOption;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "addressid")
	public Address getAddress()
	{
		return this.address;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "courierdespatchid")
	public CourierDespatch getCrdes()
	{
		return this.crdes;
	}

	@Override
	@Transient
	public Contact getDefaultContact() {
		return this.contact;
	}

	@Transient
	public File getDirectory() {
		return this.directory;
	}

	@Override
	public void setDirectory(File directory) {
		this.directory = directory;
	}

	@Column(name = "enquirydate", columnDefinition = "date")
	public LocalDate getEnquiryDate() {
		return this.enquiryDate;
	}

	public void setEnquiryDate(LocalDate enquiryDate) {
		this.enquiryDate = enquiryDate;
	}

	@Column(name = "enquiryestduration")
	@Type(type = "int")
	public Integer getEnquiryEstDuration() {
		return this.enquiryEstDuration;
	}

	public void setEnquiryEstDuration(Integer enquiryEstDuration) {
		this.enquiryEstDuration = enquiryEstDuration;
	}

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "freehandid")
	public FreehandContact getFreehandContact() {
		return this.freehandContact;
	}

	public void setFreehandContact(FreehandContact freehandContact) {
		this.freehandContact = freehandContact;
	}

	@Override
	@Transient
	public String getIdentifier() {
		return this.hireno;
	}

	@OneToMany(mappedBy = "hire", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(HireItemComparator.class)
	public Set<HireItem> getItems() {
		return this.items == null ? new TreeSet<>(new HireItemComparator()) : this.items;
	}

	public void setItems(Set<HireItem> items) {
		this.items = items;
	}

	@NotNull
	@Length(min = 1, max = 30)
	@Column(name = "hireno", nullable = false, length = 30)
	public String getHireno() {
		return this.hireno;
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount() {
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount() {
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount() {
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}

	public void setHireno(String hireno) {
		this.hireno = hireno;
	}

	/**
	 * @return the notes
	 */
	@OneToMany(mappedBy = "hire", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(NoteComparator.class)
	public Set<HireNote> getNotes() {
		return this.notes;
	}

	public void setNotes(Set<HireNote> notes) {
		this.notes = notes;
	}

	@Transient
	public ComponentEntity getParentEntity() {
		return null;
	}

	/**
	 * @return the relatedInstructions
	 */
	@Transient
	public List<InstructionLink<?>> getRelatedInstructions() {
		return this.relatedInstructions;
	}

	@Override
	@Transient
	public boolean isAccountsEmail()
	{
		return false;
	}

	@Column(name = "accountsvarified", nullable=false, columnDefinition="bit")
	public boolean isAccountsVarified()
	{
		return this.accountsVarified;
	}

	@Column(name = "enquiry", nullable = false, columnDefinition = "bit")
	public boolean isEnquiry() {
		return this.enquiry;
	}

	public void setAccountsVarified(boolean accountsVarified) {
		this.accountsVarified = accountsVarified;
	}

	@Column(name = "hiredate", columnDefinition = "date")
	public LocalDate getHireDate() {
		return this.hireDate;
	}

	public void setHireDate(LocalDate hireDate) {
		this.hireDate = hireDate;
	}

	@Transient
	public Integer getPrivateActiveNoteCount() {
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Override
	@Transient
	public List<Email> getSentEmails() {
		return this.sentEmails;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setCrdes(CourierDespatch crdes) {
		this.crdes = crdes;
	}

	@Length(max = 50)
	@Column(name = "ponumber", length = 50)
	public String getPoNumber() {
		return this.poNumber;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "transportoptionid")
	public TransportOption getTransportOption() {
		return this.transportOption;
	}

	public void setEnquiry(boolean enquiry) {
		this.enquiry = enquiry;
	}

	@Length(max = 200)
	@Column(name = "specialinst", length = 200)
	public String getSpecialInst() {
		return this.specialInst;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid", nullable = false)
	public HireStatus getStatus() {
		return this.status;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public void setRelatedInstructions(List<InstructionLink<?>> relatedInstructions) {
		this.relatedInstructions = relatedInstructions;
	}

	@Override
	public void setSentEmails(List<Email> sentEmails)
	{
		this.sentEmails = sentEmails;
	}

	public void setSpecialInst(String specialInst)
	{
		this.specialInst = specialInst;
	}

	public void setStatus(HireStatus status)
	{
		this.status = status;
	}

	public void setTransportOption(TransportOption transportOption)
	{
		this.transportOption = transportOption;
	}

}
