package org.trescal.cwms.core.hire.entity.hireaccessory;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.hire.entity.hireaccessorystatus.HireAccessoryStatus;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessory;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;

@Entity
@Table(name = "hireaccessory")
public class HireAccessory extends Auditable
{
	private boolean active;
	private int hireAccessoryId;
	private HireInstrument hireInst;
	private Set<HireItemAccessory> hireItemAccessory;
	private String item;
	private HireAccessoryStatus status;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "hireaccessoryid", nullable = false, unique = true)
	@Type(type = "int")
	public int getHireAccessoryId()
	{
		return this.hireAccessoryId;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hireinstid")
	public HireInstrument getHireInst()
	{
		return this.hireInst;
	}

	@OneToMany(mappedBy = "hireAccessory", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<HireItemAccessory> getHireItemAccessory()
	{
		return this.hireItemAccessory;
	}

	@Length(max = 300, min = 0)
	@Column(name = "item", length = 300, nullable = true)
	public String getItem()
	{
		return this.item;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid", nullable = true)
	public HireAccessoryStatus getStatus()
	{
		return this.status;
	}

	@NotNull
	@Column(name = "active", nullable = false, columnDefinition="bit")
	public boolean isActive()
	{
		return this.active;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public void setHireAccessoryId(int hireAccessoryId)
	{
		this.hireAccessoryId = hireAccessoryId;
	}

	public void setHireInst(HireInstrument hireInst)
	{
		this.hireInst = hireInst;
	}

	public void setHireItemAccessory(Set<HireItemAccessory> hireItemAccessory)
	{
		this.hireItemAccessory = hireItemAccessory;
	}

	public void setItem(String item)
	{
		this.item = item;
	}

	public void setStatus(HireAccessoryStatus status)
	{
		this.status = status;
	}

}
