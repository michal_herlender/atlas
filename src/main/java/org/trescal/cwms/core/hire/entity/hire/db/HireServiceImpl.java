package org.trescal.cwms.core.hire.entity.hire.db;

import io.vavr.control.Either;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.db.CourierDespatchService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.db.CourierDespatchTypeService;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.filenamingservice.FileNamingService;
import org.trescal.cwms.core.documents.filenamingservice.OffHireFileNamingService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.dto.HireDespatchDTO;
import org.trescal.cwms.core.hire.dto.HireSearchResultDto;
import org.trescal.cwms.core.hire.dto.HireSearchResultItem;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.hire.entity.hireitem.db.HireItemService;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessory;
import org.trescal.cwms.core.hire.entity.hirenote.HireNote;
import org.trescal.cwms.core.hire.entity.hirenote.db.HireNoteService;
import org.trescal.cwms.core.hire.entity.hirestatus.HireStatus;
import org.trescal.cwms.core.hire.form.HireHomeForm;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.EmailResultWrapper;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_HireEnquiryConfirmation;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.emailrecipient.RecipientType;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.form.EmailForm;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service("HireService")
public class HireServiceImpl extends BaseServiceImpl<Hire, Integer> implements HireService
{
	@Autowired
	private CourierDespatchService cdServ;
	@Autowired
	private CourierDespatchTypeService cdtServ;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private DocumentService docServ;
	@Autowired
	private EmailContent_HireEnquiryConfirmation emailContentHireEnquiryConfirmation; 
	@Autowired
	private EmailService emailServ;
	@Autowired
	private HireDao hireDao;
	@Autowired
	private HireNoteService hireNoteServ;
	@Autowired
	private HireItemService hireItemService;
	@Autowired
	private SystemComponentService scServ;
	@Autowired
	private UserService userService;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private SubdivService subdivServ;
	
	@Override
	protected BaseDao<Hire, Integer> getBaseDao() {
		return this.hireDao;
	}
	
	@Override
	public ResultWrapper accountsVerifyHire(int hireId, HttpSession session)
	{
		// find hire
		Hire hire = this.get(hireId);
		// hire null?
		if (hire != null)
		{
			// check contact has permission to verify hire
			String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
			@SuppressWarnings("unchecked")
			KeyValue<Integer, String> subdivDto = (KeyValue<Integer, String>) session.getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV);
			Contact contact = this.userService.get(username).getCon();
			Subdiv allocatedSubdiv = this.subdivServ.get(subdivDto.getKey());
			
			if (this.conServ.contactBelongsToDepartmentOrIsAdmin(contact, DepartmentType.ACCOUNTS, allocatedSubdiv.getComp()))
			{
				// update accounts verification value
				hire.setAccountsVarified(true);
				// update hire
				this.updateHire(hire);
				// return successful wrapper
				return new ResultWrapper(true, "", hire, null);
			}
			else
			{
				// return un-successful wrapper
				return new ResultWrapper(false, "You do not have permission to verify this hire", null, null);
			}
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "Hire could not be found", null, null);
		}
	}

	@Override
	public ResultWrapper completeHireContract(int hireId)
	{
		// find hire
		Hire hire = this.get(hireId);
		// hire null?
		if (hire != null)
		{
			// update hire status
			hire.setStatus((HireStatus) this.statusServ.findStatusByName("Complete", HireStatus.class));
			// update hire
			this.updateHire(hire);
			// return success wrapper
			return new ResultWrapper(true, "", hire, null);
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(true, "The hire you wish to complete could not be found", null, null);
		}
	}

	@Override
	public Either<String, Boolean> convertEnquiryToContract(int hireId, String clientpo, String specialInst, Integer allocatedSubdivId) {
		// find hire to be converted
		Hire hire = this.get(hireId);
		// found hire?
		if (hire != null) {
			// check hire has been accounts varified
			if (hire.isAccountsVarified()) {
				hire.setPoNumber(clientpo);
				hire.setSpecialInst(specialInst);
				// switch hire from enquiry to contract
				hire.setEnquiry(false);
				// move on status of hire to contract
				hire.setStatus((HireStatus) this.statusServ.findAcceptedStatus(HireStatus.class));
				// generation of hire contract document should be done separately
				// return successful wrapper
				return Either.right(true);
			}
			else {
				// return un-successful wrapper
				return Either.left("This enquiry has not been accounts verified and so cannot be converted.");
			}
		}
		else {
			// return un-successful wrapper
			return Either.left("Hire could not be found");
		}
	}

	@Override
	public void delete(Hire hire)
	{
		this.hireDao.remove(hire);
	}
	
	@Override
	public Hire get(Integer id)
	{
		Hire hire = this.hireDao.find(id);
		// 2018-12-12 GB : Prevent directory lookup on repeated gets.
		if (hire != null && hire.getDirectory() == null)
		{
			hire.setDirectory(this.compDirServ.getDirectory(this.scServ.findComponent(Component.HIRE), hire.getHireno()));
		}
		return hire;
	}
	
	@Override
	public List<Hire> findHiresForCourierDespatch(int cdId)
	{
		return this.hireDao.findHiresForCourierDespatch(cdId);
	}

	@Override
	public ResultWrapper generateOffHireBirtDocument(int hireid, Locale locale) {
		Hire hire = this.get(hireid);
		// hire null?
		if (hire != null) {
			FileNamingService fnService = new OffHireFileNamingService(hire);
			
			try {
				Document doc = this.docServ.createBirtDocument(hireid, BaseDocumentType.OFF_HIRE, locale, Component.HIRE, fnService);
				return new ResultWrapper(doc.isExists(), "", doc, null);
			}
			catch (Exception e) {
				e.printStackTrace();
				return new ResultWrapper(false, "An error occured generating the off hire document", null, null);
			}
		}
		else {
			return new ResultWrapper(false, "Hire could not be found", null, null);
		}		
	}
	

	@Override
	public List<Hire> getAllActiveHireContracts(Subdiv allocatedSubdiv)
	{
		// only get contracts
		return this.hireDao.getAllActiveHireContractsForStatus((HireStatus) this.statusServ.findAcceptedStatus(HireStatus.class), allocatedSubdiv);
	}

	@Override
	public List<Hire> getAllActiveHireContractsAwaitingInvoice(Subdiv allocatedSubdiv)
	{
		// only get contracts
		return this.hireDao.getAllActiveHireContractsForStatus((HireStatus) this.statusServ.findRequiresAttentionStatus(HireStatus.class), allocatedSubdiv);
	}

	@Override
	public List<Hire> getAllHireEnquiries(Subdiv allocatedSubdiv)
	{
		// only get pending enquiries which are accounts varified
		return this.hireDao.getAllHireEnquiriesForStatus(this.statusServ.findDefaultStatus(HireStatus.class), null, allocatedSubdiv);
	}

	@Override
	public List<Hire> getAllHires()
	{
		return this.hireDao.getAllHires();
	}

	@Override
	public List<Hire> getEnquiriesAwaitingAccountsVarification(Subdiv allocatedSubdiv) {
		// only get pending enquiries which are not accounts varified
		return this.hireDao.getAllHireEnquiriesForStatus(this.statusServ.findDefaultStatus(HireStatus.class), false, allocatedSubdiv);
	}

	@Override
	public Hire getHireByExactHireNo(String hireNo)
	{
		return this.hireDao.getHireByExactHireNo(hireNo);
	}

	@Override
	public void insertHire(Hire hire)
	{
		this.hireDao.insertHire(hire);
	}

	@Override
	public void prepareAndInsertHire(Hire hire) {
		hire.setStatus(this.statusServ.findDefaultStatus(HireStatus.class));
		this.saveOrUpdateHire(hire);
	}

	@Override
	public ResultWrapper removeHireDespatchType(int hireId, int cdId)
	{
		// find hire object
		Hire hire = this.get(hireId);
		// hire null?
		if (hire != null)
		{
			if (cdId != 0)
			{
				// find courier despatch
				CourierDespatch cd = this.cdServ.findCourierDespatch(cdId);
				// courier despatch null?
				if (cd != null)
				{
					// determine if we can delete this courier despatch?
					if ((this.findHiresForCourierDespatch(cdId).size() == 1)
							&& (cd.getDeliveries().size() == 0))
					{
						// remove courier despatch from hire
						hire.setCrdes(null);
						// delete this courier despatch
						this.cdServ.deleteCourierDespatch(cd);
						// return successful wrapper
						return new ResultWrapper(true, "Courier despatch has been deleted from the system and removed from this hire", null, null);
					}
					else
					{
						// remove courier despatch from hire
						hire.setCrdes(null);
						// return successful wrapper with message that schedule
						// could not be deleted
						return new ResultWrapper(true, "Courier despatch could not be deleted but has been removed from this hire", null, null);
					}
				}
				else
				{
					return new ResultWrapper(false, "Courier Despatch could not be found", null, null);
				}
			}
			else
			{
				return new ResultWrapper(true, "Client collection has been removed from this hire", null, null);
			}
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "Hire could not be found", null, null);
		}
	}

	@Override
	public void saveOrUpdateHire(Hire hire)
	{
		this.hireDao.saveOrUpdateHire(hire);
	}

	@Override
	public ResultWrapper sendEnquiryEmailConfirmation(int hireid, HttpSession session)
	{
		// validate hire
		Hire hire = this.get(hireid);
		// hire null?
		if (hire == null)
		{
			return new ResultWrapper(false, "Hire could not be found", null, null);
		}
		else
		{
			// we may want to turn this validation off
			if (hire.getItems().size() == 0)
			{
				return new ResultWrapper(false, "This hire enquiry has no items to confirm", null, null);
			}
			else
			{
				// get current contact
				String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
				// Not used in ResultWrapper so getEagerLoad not needed
				Contact con = this.userService.get(username).getCon();
				//get locale of the hire contact
				Locale hireContactLocale = hire.getContact().getLocale();
				
				try
				{
					EmailContentDto contentDto = this.emailContentHireEnquiryConfirmation.getContent(hire, hireContactLocale);
					String subject = contentDto.getSubject();
					String body = contentDto.getBody();
					// create new email form
					EmailForm mailForm = new EmailForm(Component.HIRE, hire.getId(), con.getEmail(), hire.getContact(), hire.getContact().getEmail(), subject, body, con);
					EmailRecipient mailRecipient = new EmailRecipient();
					mailRecipient.setEmailAddress(hire.getContact().getEmail());
					mailRecipient.setRecipientCon(hire.getContact());
					mailRecipient.setType(RecipientType.EMAIL_TO);
					mailRecipient.setEmail(mailForm.getEmail());
					mailForm.setRecipients(Collections.singletonList(mailRecipient));
					mailForm.getEmail().setRecipients(Collections.singletonList(mailRecipient));

					if (this.emailServ.sendEmailUsingForm(mailForm, Collections.emptyList())) {
						EmailResultWrapper erw = new EmailResultWrapper();
						erw.setEmail(mailForm.getEmail());
						erw.setBody(mailForm.getBody());
						erw.setAttachments(null);

						return new ResultWrapper(true, "", erw, null);
					}
					else
					{
						return new ResultWrapper(false, "The hire enquiry confirmation email could not be sent", null, null);
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
					return new ResultWrapper(false, "An error occured creating the document", null, null);
				}
			}
		}
	}
	
	@Override
	public void updateHire(Hire hire)
	{
		this.hireDao.updateHire(hire);
	}
	
	@Override
	public ResultWrapper updateHireDespatch(Integer hireId, Hire hire, HireDespatchDTO hddto, Integer allocatedSubdivId, HttpSession session)
	{
		// hire passed?
		if (hire == null)
		{
			hire = this.get(hireId);
		}
		// check hire null?
		if (hire != null)
		{
			// get current contact
			String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
			// Not used in ResultWrapper so getEagerLoad not needed
			Contact currentCon = this.userService.get(username).getCon();
			// antech carriage
			if (hddto.getCarriageType().equalsIgnoreCase("courier"))
			{
				if (hddto.getCdId() != 0)
				{
					// find courier despatch with this id
					CourierDespatch cd = this.cdServ.findCourierDespatch(hddto.getCdId());
					// courier despatch found?
					if (cd == null)
					{
						// return un-successful wrapper
						return new ResultWrapper(false, "Chosen existing courier despatch could not be found", null, null);
					}
					// set courier despatch
					hire.setCrdes(cd);
				}
				else {
					// create new courier despatch
					Subdiv allocatedSubdiv = this.subdivServ.get(allocatedSubdivId);
					CourierDespatch cd = new CourierDespatch();
					cd.setConsignmentno(hddto.getCdConsigNo());
					cd.setDespatchDate(hddto.getCdDesDate());
					cd.setCreationDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
					cd.setOrganisation(allocatedSubdiv);
					cd.setCreatedBy(currentCon);
					cd.setCdtype(this.cdtServ.get(hddto.getCdCourierTypeId()));
					// insert new courier despatch
					this.cdServ.insertCourierDespatch(cd);
					// add courier despatch to hire
					hire.setCrdes(cd);
				}
			}
			// return successful wrapper
			return new ResultWrapper(true, "", hire, null);
		}
		else
		{
			return new ResultWrapper(false, "Hire could not be found", null, null);
		}
	}

	@Override
	public ResultWrapper voidHire(int hireId, String reason, HttpSession session)
	{
		// find hire
		Hire hire = this.get(hireId);
		// check reason supplied?
		if ((reason != null) && !reason.isEmpty())
		{
			// found hire?
			if (hire != null)
			{
				// get current contact performing action
				String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
				// Used in ResultWrapper so getEagerLoad is needed
				Contact contact = this.userService.getEagerLoad(username).getCon();
				// default text
				String hiretext = "Hire Enquiry";
				// is this a hire contract?
				if (!hire.isEnquiry())
				{
					hiretext = "Hire Contract";
				}
				// set status of hire to rejected
				hire.setStatus((HireStatus) this.statusServ.findRejectedStatus(HireStatus.class));
				// create new hire note for reason
				HireNote hn = new HireNote();
				hn.setActive(true);
				hn.setPublish(true);
				hn.setHire(hire);
				hn.setLabel(hiretext + " voided by " + contact.getName());
				hn.setNote(reason);
				hn.setSetBy(contact);
				hn.setSetOn(new Date());
				// insert note
				this.hireNoteServ.insertHireNote(hn);
				// add note to hire
				hire.getNotes().add(hn);
				// items on hire?
				if (hire.getItems().size() > 0)
				{
					for (HireItem hi : hire.getItems())
					{
						// hire item on hire?
						if (!hi.isOffHire()) {
							// update off hire values
							hi.setOffHire(true);
							hi.setOffHireCondition("Void " + hiretext);
							hi.setOffHireDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
							hi.setTotalCost(new BigDecimal("0.00"));
							hi.setFinalCost(new BigDecimal("0.00"));
							// update hire item
							this.hireItemService.updateHireItem(hi);
						}
						// set all accessories as returned
						for (HireItemAccessory hia : hi.getAccessories())
						{
							// set returned
							hia.setReturned(true);
						}
					}
				}
				// update hire
				this.updateHire(hire);
				// zero totals
				hire.setTotalCost(new BigDecimal("0.00"));
				hire.setFinalCost(new BigDecimal("0.00"));
				// return successful wrapper
				return new ResultWrapper(true, "", hire, null);
			}
			else
			{
				// return un-successful wrapper
				return new ResultWrapper(false, "Hire could not be found", null, null);
			}
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "Reason for voiding hire not supplied", null, null);
		}
	}

	@Override
	public Long hireCountByCourier(Integer courierId) {
		return this.hireDao.hireCountByCourier(courierId);
	}

	@Override
	public Long hireCountByCourierDespatchType(Integer cdtId) {
		return this.hireDao.hireCountByCourierDespatchType(cdtId);
	}

	@Override
	public PagedResultSet<HireSearchResultDto> queryHire(HireHomeForm form, PagedResultSet<HireSearchResultDto> prs, Subdiv allocatedSubdiv) {
		  hireDao.queryHire(form,prs,allocatedSubdiv);
		 val ids =  prs.getResults().stream().map(HireSearchResultDto::getHireId).collect(Collectors.toSet());
		 val items = hireItemService.findItemsByHireIds(ids).stream().collect(Collectors.groupingBy(HireSearchResultItem::getHireId));
		 prs.getResults().forEach(hire ->{
			val itemsForHire = items.get(hire.getHireId());
            if(itemsForHire != null) hire.setItems(itemsForHire);
			 } );
		return prs;
	}
}