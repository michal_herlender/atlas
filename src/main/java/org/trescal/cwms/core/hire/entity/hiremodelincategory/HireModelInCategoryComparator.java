package org.trescal.cwms.core.hire.entity.hiremodelincategory;

import java.util.Comparator;

/**
 * Comparator class for sorting HireModelInCategory
 * 
 * @author stuarth
 */
public class HireModelInCategoryComparator implements Comparator<HireModelInCategory>
{
	public int compare(HireModelInCategory hmic1, HireModelInCategory hmic2)
	{
		// compare hire sections
		Integer sec1 = hmic1.getHireModelCategory().getHireCategory().getHireSection().getSectioncode();
		Integer sec2 = hmic2.getHireModelCategory().getHireCategory().getHireSection().getSectioncode();

		if (sec1.equals(sec2))
		{
			// compare hire categories
			Integer cat1 = hmic1.getHireModelCategory().getHireCategory().getCategorycode();
			Integer cat2 = hmic2.getHireModelCategory().getHireCategory().getCategorycode();

			if (cat1.equals(cat2))
			{
				// compare hire model categories
				Integer modcat1 = hmic1.getHireModelCategory().getModelcode();
				Integer modcat2 = hmic2.getHireModelCategory().getModelcode();

				if (modcat1.equals(modcat2))
				{
					return ((Integer) hmic1.getId()).compareTo(hmic2.getId());
				}
				else
				{
					return modcat1.compareTo(modcat2);
				}
			}
			else
			{
				return cat1.compareTo(cat2);
			}
		}
		else
		{
			return sec1.compareTo(sec2);
		}
	}
}