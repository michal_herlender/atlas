package org.trescal.cwms.core.hire.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HireBasketItemDto {
    private Integer plantId;
    private  Integer calTypeId;
    private BigDecimal ukas;
    private BigDecimal hireCost;
    private String description;
    private String serialNo;
    private String plantNo;

}
