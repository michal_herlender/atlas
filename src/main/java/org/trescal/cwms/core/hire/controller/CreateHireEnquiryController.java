package org.trescal.cwms.core.hire.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.freehandcontact.FreehandContact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.hire.entity.hireaccessory.HireAccessory;
import org.trescal.cwms.core.hire.entity.hireaccessory.db.HireAccessoryService;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessory;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.db.HireItemAccessoryService;
import org.trescal.cwms.core.hire.form.HireForm;
import org.trescal.cwms.core.hire.form.HireValidator;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@IntranetController
@SessionAttributes(value = {Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class CreateHireEnquiryController {
	@Autowired
	private AddressService addrServ;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private ContactService conServ;
	@Autowired
	private SupportedCurrencyService currencyServ;
	@Value("#{props['default.currency']}")
	private String defaultCurrencyCode;
	@Autowired
	private HireAccessoryService hireAccServ;
	@Autowired
	private HireItemAccessoryService hireItemAccServ;
	@Autowired
	private HireService hireServ;
	@Value("#{props['cwms.config.hire.min_hire_days']}")
	private Integer minHireDays;
	@Autowired
	private UserService userService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private HireValidator validator;
	@Autowired
	private HireProcessFormUtil processFormUtil;

	@Autowired
	private NumerationService numerationService;

	@InitBinder("form")
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("form")
	protected HireForm formBackingObject(
		@ModelAttribute("sourceHireForm") HireForm sourceHireForm) {
		HireForm form = sourceHireForm == null ? new HireForm() : sourceHireForm;
		// create a new blank Hire

		Hire hire = processFormUtil.processItems(form);
		if (form.getHire() == null) form.setHire(hire);
		hire.setEnquiry(true);
		hire.setEnquiryDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		hire.setEnquiryEstDuration(this.minHireDays);
		hire.setHireDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		form.setCurrencyCode(this.defaultCurrencyCode);
		form.setFreehandDelivery(false);
		return form;
	}

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	private void onBind(HireForm form) {
		// create new address string
		String addrString = "";
		// check freehand delivery checked?
		if (form.isFreehandDelivery())
		{
			// create new freehand contact
			FreehandContact fc = new FreehandContact();
			// delivery address supplied?
			if ((form.getFreeAddr1().length() > 0)
					|| (form.getFreeAddr2().length() > 0)
					|| (form.getFreeTown().length() > 0)
					|| (form.getFreeCounty().length() > 0)
					|| (form.getFreePostcode().length() > 0))
			{
				// add address elements to string delimitted by comma's
				addrString = addrString.concat(form.getFreeAddr1() + ","
						+ form.getFreeAddr2() + "," + form.getFreeTown() + ","
						+ form.getFreeCounty() + "," + form.getFreePostcode());
			}
			// set onto freehand contact
			fc.setContact(form.getFreeName());
			fc.setCompany(form.getFreeCompany());
			fc.setAddress(addrString);
			// add freehand contact to form
			form.getHire().setFreehandContact(fc);
		}
		else
		{
			// null freehand delivery
			form.getHire().setFreehandContact(null);
		}
	}

	@RequestMapping(value="/createhireenquiry.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(@Validated @ModelAttribute("form") HireForm form, BindingResult bindingResult,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdiv,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username)
	{
		if(bindingResult.hasErrors()) return referenceData(form);
		onBind(form);
		Subdiv allocatedSubdiv = subdivService.get(subdiv.getKey());
		// get hire from form
		Hire hire = form.getHire();
		hire.setOrganisation(allocatedSubdiv);
		// get current contact
		Contact contact = this.userService.get(username).getCon();
		// set address
		hire.setAddress(this.addrServ.get(form.getAddrid()));
		// find hire contact
		Contact hireContact = this.conServ.get(form.getPersonid());
		// set contact
		hire.setContact(hireContact);
		// set accounts varified
		CompanySettingsForAllocatedCompany companySettings = companySettingsService.getByCompany(hireContact.getSub().getComp(), allocatedSubdiv.getComp());
		hire.setAccountsVarified(!companySettings.isOnStop());
		// either set the currency and rate from the company default or from the
		// other selected currency
		if (form.getCurrencyCode().equals(Constants.COMPANY_DEFAULT_CURRENCY)) {
			hire.setCurrency(hire.getContact().getSub().getComp().getCurrency());
			hire.setRate(hire.getContact().getSub().getComp().getRate());
		} else {
			hire.setCurrency(this.currencyServ.findCurrencyByCode(form.getCurrencyCode()));
			hire.setRate(hire.getCurrency().getDefaultRate());
		}

		FreehandContact fc = hire.getFreehandContact();
		// if freehand details are empty
		if ((fc != null) && (fc.isEmpty())) {
			// set to null so the object is not persisted
			hire.setFreehandContact(null);
		}

		// set not null values
		hire.setCreatedBy(contact);
		hire.setDuration(1);
		hire.setRegdate(hire.getEnquiryDate());
		hire.setTotalCost(new BigDecimal("0.00"));
		hire.setVatRate(new BigDecimal("0.00"));
		hire.setVatValue(new BigDecimal("0.00"));
		hire.setFinalCost(new BigDecimal("0.00"));
		hire.setHireno(numerationService.generateNumber(NumerationType.HIRE, allocatedSubdiv.getComp(), allocatedSubdiv));

		// set the status & new pono and persist
		this.hireServ.prepareAndInsertHire(hire);

		// add accessories to all hire items
		for (HireItem hi : hire.getItems()) {
			if (hi.getHireInst() != null) {
				// get all hire accessories for instrument
				@SuppressWarnings("unchecked")
				List<HireAccessory> accessories = (List<HireAccessory>) this.hireAccServ.getAvailableAccessoriesForHireInstrument(hi.getHireInst().getId()).getResults();
				// accessories returned?
				if (accessories.size() > 0)
				{
					// for each of the hire accessories
					for (HireAccessory ha : accessories)
					{
						// assign it hire item
						HireItemAccessory hia = new HireItemAccessory();
						hia.setHireItem(hi);
						hia.setHireAccessory(ha);
						hia.setReturned(false);
						// insert hire item accessory
						this.hireItemAccServ.insertHireItemAccessory(hia);
						// check accessories null?
						hi.getAccessories().add(hia);
					}
				}
			}
		}

		return new ModelAndView(new RedirectView("/viewhire.htm?id="
			+ hire.getId(), true));
	}

	@RequestMapping(value = "/createhireenquiry.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute("form") HireForm form) {
		Map<String, Object> refData = new HashMap<>();

		refData.put("currencyList", this.currencyServ.getAllSupportedCurrencys());
		refData.put("minHireDays", this.minHireDays);
		refData.put("defaultCurrency", this.currencyServ.getDefaultCurrency());

		return new ModelAndView("trescal/core/hire/createhireenquiry", refData);
	}
}