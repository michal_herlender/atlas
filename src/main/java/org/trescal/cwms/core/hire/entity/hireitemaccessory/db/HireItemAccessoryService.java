package org.trescal.cwms.core.hire.entity.hireitemaccessory.db;

import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessory;

public interface HireItemAccessoryService
{
	ResultWrapper assignAccessoriesToHireItem(Integer[] accessoryIds, int hireItemId);

	void deleteHireItemAccessory(HireItemAccessory hia);

	HireItemAccessory findHireItemAccessory(int id);

	List<HireItemAccessory> getAllHireItemAccessorys();

	void insertHireItemAccessory(HireItemAccessory hia);

	void saveOrUpdateHireItemAccessory(HireItemAccessory hia);

	void updateHireItemAccessory(HireItemAccessory hia);

	ResultWrapper validateHireItemAccessories(int hireId);
}