package org.trescal.cwms.core.hire.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.hire.dto.HireInstrumentRevenueWrapper;
import org.trescal.cwms.core.hire.form.HireInstrumentRevenuesForm;
import org.trescal.cwms.core.hire.view.HireInstrumentRevenuesXlsxView;
import org.trescal.cwms.core.instrument.entity.hireinstrument.db.HireInstrumentService;
import org.trescal.cwms.core.system.Constants;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class ViewHireInstrumentRevenuesController
{
	@Autowired
	private HireInstrumentService hireInstServ;
	@Autowired
	private HireInstrumentRevenuesXlsxView xlsxView;
			
	@ModelAttribute("form")
	protected HireInstrumentRevenuesForm formBackingObject(@RequestParam(value="hiid", required=false, defaultValue="0") Integer hireInstId) throws Exception
	{
		// create new quotation item search form
		HireInstrumentRevenuesForm form = new HireInstrumentRevenuesForm();
		form.setExport(false);
		// hire instrument id passed?
		if (hireInstId != 0) {
			form.setRevWrappers(null);
			form.setHi(this.hireInstServ.findHireInstrument(hireInstId));
		}
		else {
			// revenue wrappers null?
			if (form.getRevWrappers() == null) {
				form.setRevWrappers(new ArrayList<HireInstrumentRevenueWrapper>());
			}
			form.setHi(null);
		}
		return form;
	}
	
	@RequestMapping(value="/viewhireinstrumentrevenues.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(@ModelAttribute("form") HireInstrumentRevenuesForm form, Locale locale) throws Exception
	{
		form.setRevWrappers(this.hireInstServ.searchHireInstrumentsRevenueHQL(form.getMfrid(), form.getMfrname(), form.getModelname(), form.getDescid(), form.getDescname(), form.getBarcode(), form.getPlantno(), form.getSerialno()));
		// create and save spreadsheet of results to the temp folder
		//String relativeXlsPath = this.generateSpreadsheetOfResults(form, locale);
		//form.setSpreadsheetPath(relativeXlsPath);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("form", form);
		// return
		if (form.getExport()) {
			return new ModelAndView(this.xlsxView, model);
		}
		else {
			return new ModelAndView(this.formView(), model);
		}
	}
	
	@RequestMapping(value="/viewhireinstrumentrevenues.htm", method=RequestMethod.GET)
	protected String formView() {
		return "/trescal/core/hire/viewhireinstrumentrevenues";
	}
}