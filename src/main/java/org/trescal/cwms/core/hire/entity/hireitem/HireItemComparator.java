package org.trescal.cwms.core.hire.entity.hireitem;

import java.util.Comparator;

/**
 * Inner comparator class for sorting HireItems when returning collections of
 * hires, each with multiple items
 * 
 * @author stuarth
 */
public class HireItemComparator implements Comparator<HireItem>
{
	public int compare(HireItem hItem1, HireItem hItem2)
	{
		// compare hires
		Integer hireid1 = hItem1.getHire().getId();
		Integer hireid2 = hItem2.getHire().getId();

		if (hireid1.equals(hireid2))
		{
			// compare itemNos
			Integer itemNo1 = hItem1.getItemno();
			Integer itemNo2 = hItem2.getItemno();

			if (itemNo1.equals(itemNo2))
			{
				// this should never happen (but is in so that if something goes
				// wrong and item numbers are the same, both items will still
				// show up on the hire screen)
				Integer id1 = hItem1.getItemno();
				Integer id2 = hItem2.getItemno();

				return id1.compareTo(id2);
			}
			else
			{
				return itemNo1.compareTo(itemNo2);
			}
		}
		else
		{
			return hireid1.compareTo(hireid2);
		}
	}
}