package org.trescal.cwms.core.hire.entity.hireitemaccessory.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.hire.entity.hireitemaccessory.HireItemAccessory;

@Repository("HireItemAccessoryDao")
public class HireItemAccessoryDaoImpl extends BaseDaoImpl<HireItemAccessory, Integer> implements HireItemAccessoryDao {
	
	@Override
	protected Class<HireItemAccessory> getEntity() {
		return HireItemAccessory.class;
	}
}