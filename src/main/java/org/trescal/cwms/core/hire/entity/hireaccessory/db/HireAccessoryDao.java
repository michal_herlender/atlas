package org.trescal.cwms.core.hire.entity.hireaccessory.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.hire.entity.hireaccessory.HireAccessory;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;

public interface HireAccessoryDao extends BaseDao<HireAccessory, Integer>
{
	HireAccessory findEagerHireAccessory(int id);
	
	List<HireAccessory> getAccessoriesForHireInstrument(HireInstrument hireInst, boolean active, boolean available);
	
	List<HireAccessory> getDistinctAccessoriesForHireModel(HireInstrument hireInst);
}