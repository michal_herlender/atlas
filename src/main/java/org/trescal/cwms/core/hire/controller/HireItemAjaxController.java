package org.trescal.cwms.core.hire.controller;

import io.vavr.control.Either;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.hire.dto.HireInstrumentDto;
import org.trescal.cwms.core.hire.dto.OffHireAccessoryDTO;
import org.trescal.cwms.core.hire.dto.ViewHireItemDto;
import org.trescal.cwms.core.hire.entity.hireitem.db.HireItemService;
import org.trescal.cwms.core.instrument.entity.hireinstrument.db.HireInstrumentService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.tools.InstModelTools;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@RestController
@RequestMapping("hireitem")
public class HireItemAjaxController {

    @Autowired
    private HireItemService hireItemService;

    @Autowired
    private HireInstrumentService hireInstrumentService;

    @InitBinder
    protected void initBinder(ServletRequestDataBinder binder) throws Exception {
        binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
    }

    @PostMapping("offHireItem.json")
    public Either<String, Boolean> ajaxOffHireItem(OffHireItemIn in) {
        if (ObjectUtils.isEmpty(in.getOffHireCond()))
            return Either.left("Condition of off hired items not supplied");
        if (ObjectUtils.isEmpty(in.getOffHireDate()))
            return Either.left("Date items off hired not supplied");
        return hireItemService.ajaxOffHireItem(in.getHireItemId(), in.getOffHireCond(), in.getOffHireDate(), in.getOffHireAccs(), in.getGenOffHireDoc());
    }

    @GetMapping("searchReplacement.json")
    Optional<HireInstrumentDto> searchReplacementByBarcode(@RequestParam Integer barcode) {
        return hireInstrumentService.getHireInstrumentByPlantId(barcode)
            .map(hi -> HireInstrumentDto.builder()
                .plantId(hi.getInst().getPlantid())
                .plantNo(hi.getInst().getPlantno())
                .fullInstrumentModelName(InstModelTools.instrumentModelNameViaTranslations(hi.getInst(), LocaleContextHolder.getLocale(), Locale.getDefault()))
                .build());
    }


    @PutMapping("replaceHireItemInstrument.json")
    Either<String, Boolean> replaceHireItemInstrument(@RequestParam Integer hireItemId, @RequestParam Integer newPlantId, @RequestParam String relacementReason) {
        return hireItemService.replaceHireItemInstrument(hireItemId, newPlantId, relacementReason);
    }

    @PutMapping("updateHireItemValues.json")
    Either<String, ViewHireItemDto> updateHireItemValues(@RequestParam Integer hireItemId, @RequestParam Integer newCaltypeId, @RequestParam BigDecimal newHireCost, @RequestParam BigDecimal newUkasCalCost) {
        return hireItemService.updateHireItemValues(hireItemId, newCaltypeId, newHireCost, newUkasCalCost);
    }


    @PutMapping("resetOffHireDate.json")
    Either<String, Boolean> resetOffHireDate(@RequestParam Integer hireItemId) {
        return hireItemService.resetOffHireDate(hireItemId);
    }
}

@Data
class OffHireItemIn {
    Integer hireItemId;
    String offHireCond;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate offHireDate;
    List<OffHireAccessoryDTO> offHireAccs;
    Boolean genOffHireDoc;
}
