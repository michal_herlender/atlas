package org.trescal.cwms.core.hire.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ConvertEnquiryToContractForm {
    private Integer hireId;
    private String clientPo;
    private String specialinst;
}
