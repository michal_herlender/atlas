package org.trescal.cwms.core.hire.entity.hire.db;

import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.hibernate.criterion.MatchMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToSubdivDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier_;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.CourierDespatch_;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType_;
import org.trescal.cwms.core.hire.dto.HireSearchResultDto;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.Hire_;
import org.trescal.cwms.core.hire.entity.hireitem.HireItem_;
import org.trescal.cwms.core.hire.entity.hirestatus.HireStatus;
import org.trescal.cwms.core.hire.entity.hirestatus.HireStatus_;
import org.trescal.cwms.core.hire.form.HireHomeForm;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument;
import org.trescal.cwms.core.instrument.entity.hireinstrument.HireInstrument_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.tools.PagedResultSet;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.trescal.cwms.core.tools.StringJpaUtils.*;

@Repository("HireDao")
public class HireDaoImpl extends AllocatedToSubdivDaoImpl<Hire, Integer> implements HireDao
{

	@Override
	protected Class<Hire> getEntity() {
		return Hire.class;
	}
	
	public List<Hire> findHiresForCourierDespatch(int cdId)
	{
	    return getResultList(cb -> {
	    	val cq = cb.createQuery(Hire.class);
	    	val hire = cq.from(Hire.class);
	    	cq.where(cb.equal(hire.get(Hire_.crdes),cdId));
	    	return cq;
		});
	}

	public List<Hire> getAllActiveHireContractsForStatus(HireStatus status, Subdiv allocatedSubdiv)
	{
		return getResultList(cb -> {
			val cq = cb.createQuery(Hire.class);
			val hire = cq.from(Hire.class);
			cq.where(
					cb.isFalse(hire.get(Hire_.enquiry)),
					cb.equal(hire.get(Hire_.status),status),
					cb.equal(hire.get(Hire_.organisation),allocatedSubdiv)
			);
			cq.orderBy(cb.desc(hire.get(Hire_.id)));
			return cq;
		});
	}

	public List<Hire> getAllHireEnquiriesForStatus(HireStatus status, Boolean accountsVarified, Subdiv allocatedSubdiv)
	{
		return getResultList(cb -> {
			val cq = cb.createQuery(Hire.class);
			val hire = cq.from(Hire.class);
			val predicates = Stream.of(
					cb.isTrue(hire.get(Hire_.enquiry)),
					cb.equal(hire.get(Hire_.organisation),allocatedSubdiv),
					cb.equal(hire.get(Hire_.status),status)
			);
			cq.where(
					Stream.concat(predicates,accountsVarified!=null?Stream.of(
							cb.equal(hire.get(Hire_.accountsVarified),accountsVarified)
					):Stream.empty()).toArray(Predicate[]::new)
			);
			return cq;
		});
	}

	public List<Hire> getAllHires()
	{
	    return findAll();
	}

	@Override
	public Hire getHireByExactHireNo(String hireNo)
	{
		return getFirstResult(cb -> {
			val cq = cb.createQuery(Hire.class);
			val hire = cq.from(Hire.class);
			cq.where(cb.equal(hire.get(Hire_.hireno),hireNo));
			return cq;
		}).orElse(null);
	}

	public void insertHire(Hire hire)
	{
	    getEntityManager().persist(hire);
	}

	@Override
	public PagedResultSet<HireSearchResultDto> queryHire(HireHomeForm form, PagedResultSet<HireSearchResultDto> ps, Subdiv allocatedSubdiv){
		completePagedResultSet(ps, HireSearchResultDto.class, cb -> cq ->{
			val hire = cq.from(Hire.class);
			val hireItem = hire.join(Hire_.items,JoinType.LEFT);
			val instrument = hireItem
					.join(HireItem_.hireInst, JoinType.LEFT)
					.join(HireInstrument_.inst,JoinType.LEFT);
			val model = instrument.join(Instrument_.model,JoinType.LEFT);
			val contact = hire.join(Hire_.contact);
			val subdiv = contact.join(Contact_.sub);
			val company = subdiv.join(Subdiv_.comp);
			val predicates = Stream.of(
					Stream.of(cb.equal(hire.get(Hire_.organisation),allocatedSubdiv)),
					entityByIdPredicate(cb,company,form.getCoid()),
					ilikePredicateStream(cb,hire.get(Hire_.hireno),form.getHireNo(),MatchMode.ANYWHERE),
					barcodePlantNoSerialNoPredicate(cb,instrument,form.getPlantid(),form.getPlantno(),form.getSerialno()),
					ilikePredicateStream(cb,hire.get(Hire_.clientref),form.getClientRef(),MatchMode.ANYWHERE),
					ilikePredicateStream(cb,hire.get(Hire_.poNumber),form.getPurOrder(),MatchMode.ANYWHERE),
				entityByIdPredicate(cb, subdiv.get(Subdiv_.comp), form.getCoid()),
				entityByIdPredicate(cb, contact, form.getPersonid()),
				entityByIdPredicate(cb, hire.get(Hire_.status), form.getStatusid()),
				ilikePredicateStream(cb, model.get(InstrumentModel_.model), form.getModel(), MatchMode.ANYWHERE),
				descriptionPredicate(cb, model, form.getDesc(), form.getDescid()),

				manufacturerPredicate(cb, instrument, model, form),
				hireDatePredicate(cb, hire.get(Hire_.hireDate), form.isHireDateBetween(), form.getHireDate1(), form.getHireDate2())
			).flatMap(Function.identity());
			cq.where(predicates.toArray(Predicate[]::new));
			val ordering = Stream.of(
				cb.desc(cb.literal(1))
			).collect(Collectors.toList());
			val selection = cb.construct(HireSearchResultDto.class,
				hire.get(Hire_.id),
				hire.get(Hire_.hireno), company.get(Company_.coid),
				company.get(Company_.coname),
				contact.get(Contact_.personid),
				trimAndConcatWithWhitespace(contact.get(Contact_.firstName), contact.get(Contact_.lastName))
					.apply(cb),
				hire.get(Hire_.poNumber),
				hire.get(Hire_.clientref),
				hire.get(Hire_.hireDate),
				joinTranslation(cb, hire.join(Hire_.status), HireStatus_.nametranslations, LocaleContextHolder.getLocale()));
			cq.distinct(true);
			return Triple.of(hire, selection, ordering);
		}, true);
		return ps;
	}

	private Stream<Predicate> hireDatePredicate(CriteriaBuilder cb, Path<LocalDate> hireDate, Boolean hireDateBetween, LocalDate hireDate1, LocalDate hireDate2) {
		if (hireDateBetween)
			return Stream.of(cb.between(hireDate, hireDate1, hireDate2));
		if (hireDate1 != null) return Stream.of(cb.equal(hireDate, hireDate1));
		return Stream.empty();
	}

	private Stream<Predicate> descriptionPredicate(CriteriaBuilder cb, Join<Instrument, InstrumentModel> model, String desc, Integer descid) {
		if (descid != null) return Stream.of(cb.equal(model.get(InstrumentModel_.description), descid));
		return ilikePredicateStream(cb, model.join(InstrumentModel_.description, JoinType.LEFT).get(Description_.description), desc, MatchMode.ANYWHERE);
	}

	private Stream<Predicate> manufacturerPredicate(CriteriaBuilder cb, Join<HireInstrument, Instrument> instrument, Join<Instrument, InstrumentModel> instrumentModel, HireHomeForm form) {
		if (form.getMfrid() != null) {
			return Stream.of(
				cb.or(
					cb.equal(instrument.get(Instrument_.mfr), form.getMfrid()),
					cb.isNull(instrument.get(Instrument_.mfr))
				),
				cb.or(
					cb.equal(instrumentModel.get(InstrumentModel_.mfr), form.getMfrid()),
					cb.and(
						cb.notEqual(instrumentModel.get(InstrumentModel_.mfr), form.getMfrid()),
						cb.isTrue(instrumentModel.join(InstrumentModel_.mfr, JoinType.LEFT).get(Mfr_.genericMfr))
		)
					));
		}
		if(StringUtils.isNotBlank(form.getMfr())) {
			val instrumentMfr = instrument.join(Instrument_.mfr,JoinType.LEFT);
			val modelMfr = instrumentModel.join(InstrumentModel_.mfr,JoinType.LEFT);
			val mfrName =
					cb.equal(instrumentMfr.get(Mfr_.name),form.getMfr());
		return Stream.of(
				cb.or(
						cb.equal(instrumentMfr.get(Mfr_.name),form.getMfr()),
					cb.isNull(instrumentMfr)
				),
				cb.or(
						cb.equal(modelMfr.get(Mfr_.name),form.getMfr()),
						cb.and(cb.not(mfrName),cb.isTrue(instrumentMfr.get(Mfr_.genericMfr)))
				)
		);
		}
		return Stream.empty();
	}


	private Stream<Predicate> entityByIdPredicate(CriteriaBuilder cb,Path<?> path,String id){
		if(StringUtils.isNotBlank(id) && StringUtils.isNumeric(id))
			return Stream.of(cb.equal(path,Integer.parseInt(id)));
		return Stream.empty();

	}

	private Stream<Predicate> barcodePlantNoSerialNoPredicate(CriteriaBuilder cb, Join<HireInstrument, Instrument> instrument, String plantid, String plantno, String serialno) {
		if(StringUtils.isNotBlank(plantid) && StringUtils.isNumeric(plantid)) return Stream.of(cb.equal(instrument,Integer.parseInt(plantid)));
		if(StringUtils.isNotBlank(plantno) && StringUtils.isNotBlank(serialno))
		return Stream.of(cb.and(
			ilike(cb,instrument.get(Instrument_.plantno),plantno,MatchMode.ANYWHERE),
				ilike(cb,instrument.get(Instrument_.serialno),serialno,MatchMode.ANYWHERE)
		));
		if(StringUtils.isNotBlank(plantno))
			return Stream.of(ilike(cb,instrument.get(Instrument_.plantno),plantno,MatchMode.ANYWHERE));
		else if(StringUtils.isNotBlank(serialno))
			return Stream.of(ilike(cb,instrument.get(Instrument_.serialno),serialno,MatchMode.ANYWHERE));
		return Stream.empty();
	}



	public void saveOrUpdateHire(Hire hire)
	{
	    getEntityManager().persist(hire);
	}

	public void updateHire(Hire hire)
	{
		getEntityManager().merge(hire);
	}

	
	@Override
	public Long hireCountByCourier(Integer courierId) {
		return getFirstResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Hire> root = cq.from(Hire.class);
			Join<Hire, CourierDespatch> cdJoin = root.join(Hire_.crdes);
			Join<CourierDespatch, CourierDespatchType> cdtJoin = cdJoin.join(CourierDespatch_.cdtype);
			Join<CourierDespatchType, Courier> courierJoin = cdtJoin.join(CourierDespatchType_.courier);
			cq.where(cb.equal(courierJoin.get(Courier_.courierid), courierId));
			cq.select(cb.count(root));
			return cq;
		}).orElse(0L);
	}

	@Override
	public Long hireCountByCourierDespatchType(Integer cdtId) {
		return getFirstResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Hire> root = cq.from(Hire.class);
			Join<Hire, CourierDespatch> cdJoin = root.join(Hire_.crdes);
			Join<CourierDespatch, CourierDespatchType> cdtJoin = cdJoin.join(CourierDespatch_.cdtype);
			cq.where(cb.equal(cdtJoin.get(CourierDespatchType_.cdtid), cdtId));
			cq.select(cb.count(root));
			return cq;
		}).orElse(0L);
	}
}