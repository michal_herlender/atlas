package org.trescal.cwms.core.hire.entity.hirereplacedinstrument.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.hire.entity.hirereplacedinstrument.HireReplacedInstrument;

@Repository("HireReplacedInstrumentDao")
public class HireReplacedInstrumentDaoImpl extends BaseDaoImpl<HireReplacedInstrument, Integer> implements HireReplacedInstrumentDao {
	
	@Override
	protected Class<HireReplacedInstrument> getEntity() {
		return HireReplacedInstrument.class;
	}
}