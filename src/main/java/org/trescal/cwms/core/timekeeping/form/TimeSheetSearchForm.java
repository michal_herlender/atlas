package org.trescal.cwms.core.timekeeping.form;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.Locale;

public class TimeSheetSearchForm {

	private LocalDate date;
	private final WeekFields weekFields;

	public TimeSheetSearchForm(LocalDate date, Locale locale) {
		this.date = date == null ? LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()) : date;
		weekFields = WeekFields.of(locale);
	}

	@DateTimeFormat(iso = ISO.DATE)
	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Integer getWeek() {
		return date.get(weekFields.weekOfYear());
	}

	public Integer getYear() {
		return date.getYear();
	}
}