package org.trescal.cwms.core.timekeeping.enums;

public enum TimeLink {
	INTERNAL_PARK, BUSINESS, ATTENDANCE;
}