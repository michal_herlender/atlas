package org.trescal.cwms.core.timekeeping.entity.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetValidation;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetValidation_;
import org.trescal.cwms.core.timekeeping.enums.TimeSheetValidationStatus;

@Repository
public class TimeSheetValidationDaoImpl extends BaseDaoImpl<TimeSheetValidation, Integer>
		implements TimeSheetValidationDao {

	@Override
	protected Class<TimeSheetValidation> getEntity() {
		return TimeSheetValidation.class;
	}

	@Override
	public Boolean existsNotRejected(Contact employee, Integer week, Integer year) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<TimeSheetValidation> validation = cq.from(TimeSheetValidation.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(validation.get(TimeSheetValidation_.employee), employee));
			clauses.getExpressions().add(cb.equal(validation.get(TimeSheetValidation_.week), week));
			clauses.getExpressions().add(cb.equal(validation.get(TimeSheetValidation_.year), year));
			clauses.getExpressions().add(cb.or(
					cb.equal(validation.get(TimeSheetValidation_.status), TimeSheetValidationStatus.CREATED),
					cb.equal(validation.get(TimeSheetValidation_.status), TimeSheetValidationStatus.VALIDATED)));
			cq.where(clauses);
			cq.select(cb.count(validation));
			return cq;
		}) > 0;
	}

	private TimeSheetValidation findByStatus(Contact employee, Integer week, Integer year,
			TimeSheetValidationStatus status) {
		return getFirstResult(cb -> {
			CriteriaQuery<TimeSheetValidation> cq = cb.createQuery(TimeSheetValidation.class);
			Root<TimeSheetValidation> validation = cq.from(TimeSheetValidation.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(validation.get(TimeSheetValidation_.employee), employee));
			clauses.getExpressions().add(cb.equal(validation.get(TimeSheetValidation_.week), week));
			clauses.getExpressions().add(cb.equal(validation.get(TimeSheetValidation_.year), year));
			clauses.getExpressions().add(cb.equal(validation.get(TimeSheetValidation_.status), status));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

	@Override
	public TimeSheetValidation findReadyForValidation(Contact employee, Integer week, Integer year) {
		return findByStatus(employee, week, year, TimeSheetValidationStatus.CREATED);
	}

	@Override
	public TimeSheetValidation findValidated(Contact employee, Integer week, Integer year) {
		return findByStatus(employee, week, year, TimeSheetValidationStatus.VALIDATED);
	}

	@Override
	public TimeSheetValidation findLatestRejected(Contact employee, Integer week, Integer year) {
		return findByStatus(employee, week, year, TimeSheetValidationStatus.REJECTED);
	}
	
	@Override
	public List<TimeSheetValidation> findAll(Contact employee, Integer week, Integer year) {
		return getResultList(cb -> {
			CriteriaQuery<TimeSheetValidation> cq = cb.createQuery(TimeSheetValidation.class);
			Root<TimeSheetValidation> validation = cq.from(TimeSheetValidation.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(validation.get(TimeSheetValidation_.employee), employee));
			clauses.getExpressions().add(cb.equal(validation.get(TimeSheetValidation_.week), week));
			clauses.getExpressions().add(cb.equal(validation.get(TimeSheetValidation_.year), year));
			cq.where(clauses);
			return cq;
		});
	}

	@Override
	public List<TimeSheetValidation> findAll(Subdiv subdiv, Integer week, Integer year) {
		return getResultList(cb -> {
			CriteriaQuery<TimeSheetValidation> cq = cb.createQuery(TimeSheetValidation.class);
			Root<TimeSheetValidation> validation = cq.from(TimeSheetValidation.class);
			Join<TimeSheetValidation, Contact> employee = validation.join(TimeSheetValidation_.employee);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(employee.get(Contact_.sub), subdiv));
			clauses.getExpressions().add(cb.equal(validation.get(TimeSheetValidation_.week), week));
			clauses.getExpressions().add(cb.equal(validation.get(TimeSheetValidation_.year), year));
			cq.where(clauses);
			return cq;
		});
	}

	@Override
	public List<TimeSheetValidation> findAll(Subdiv subdiv, TimeSheetValidationStatus status) {
		return getResultList(cb -> {
			CriteriaQuery<TimeSheetValidation> cq = cb.createQuery(TimeSheetValidation.class);
			Root<TimeSheetValidation> validation = cq.from(TimeSheetValidation.class);
			Join<TimeSheetValidation, Contact> employee = validation.join(TimeSheetValidation_.employee);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(employee.get(Contact_.sub), subdiv));
			clauses.getExpressions().add(cb.equal(validation.get(TimeSheetValidation_.status), status));
			cq.where(clauses);
			return cq;
		});
	}
}