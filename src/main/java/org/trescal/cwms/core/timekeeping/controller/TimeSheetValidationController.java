package org.trescal.cwms.core.timekeeping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetValidation;
import org.trescal.cwms.core.timekeeping.entity.db.TimeSheetValidationService;
import org.trescal.cwms.core.timekeeping.form.TimeSheetValidationForm;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.List;
import java.util.Locale;
import java.util.SortedMap;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV,
	Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE})
public class TimeSheetValidationController {

	@Autowired
	private SubdivService subdivService;
	@Autowired
	private TimeSheetValidationService timeSheetValidationService;
	@Autowired
	private UserService userService;
	@Autowired
	private AuthenticationService authenticationService;

	@ModelAttribute("validationForm")
	public TimeSheetValidationForm validationForm(@RequestParam(name = "week", required = false) Integer week,
			@RequestParam(name = "year", required = false) Integer year,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE) Locale defaultLocale) {
		if (week == null || year == null) {
			WeekFields weekFields = WeekFields.of(defaultLocale);
			week = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).get(weekFields.weekOfYear()) - 1;
			year = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).getYear();
		}
		return new TimeSheetValidationForm(week, year);
	}

	@ModelAttribute("validationList")
	public List<TimeSheetValidation> validationList(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute("validationForm") TimeSheetValidationForm validationForm) {
		Subdiv subdiv = subdivService.get(subdivDto.getKey());
		return timeSheetValidationService.findAll(subdiv, validationForm.getWeek(), validationForm.getYear());
	}

	@ModelAttribute("readyForValidation")
	public SortedMap<Integer, SortedMap<Integer, List<TimeSheetValidation>>> readyForValidation(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		Subdiv subdiv = subdivService.get(subdivDto.getKey());
		return timeSheetValidationService.findAllReadyForValidation(subdiv);
	}

	@RequestMapping(path="/timesheetvalidation.htm")
	public String showView(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		User user = userService.get(userName);
		if(authenticationService.hasRight(user.getCon(), Permission.TIMESHEET_VALIDATION, subdivDto.getKey()))
		{
			return "/trescal/core/timekeeping/timesheetvalidation";

		}
		else
			throw new AccessDeniedException("You haven't the right to access this page!");
	}		
}