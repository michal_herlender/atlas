package org.trescal.cwms.core.timekeeping.dtos;

import java.time.Duration;

public class TimeSheetStatisticDTO {

	private Duration workingTime;
	private Duration registeredTime;
	private Duration overtime;

	public TimeSheetStatisticDTO() {
	}

	public TimeSheetStatisticDTO(Duration theoreticTime, Duration registeredTime, Duration overtime) {
		this.workingTime = theoreticTime;
		this.registeredTime = registeredTime;
		this.overtime = overtime;
	}

	public Duration getWorkingTime() {
		return workingTime;
	}

	public void setWorkingTime(Duration workingTime) {
		this.workingTime = workingTime;
	}

	public Duration getRegisteredTime() {
		return registeredTime;
	}

	public void setRegisteredTime(Duration registeredTime) {
		this.registeredTime = registeredTime;
	}

	public Duration getOvertime() {
		return overtime;
	}

	public void setOvertime(Duration overtime) {
		this.overtime = overtime;
	}
}