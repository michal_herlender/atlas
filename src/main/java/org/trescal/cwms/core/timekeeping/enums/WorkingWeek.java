package org.trescal.cwms.core.timekeeping.enums;

import java.time.DayOfWeek;

public enum WorkingWeek {
	MONDAY_TO_FRIDAY, MONDAY_TO_SATURDAY;

	public boolean isWorkingDay(DayOfWeek day) {
		switch (this) {
		case MONDAY_TO_FRIDAY:
			return day.ordinal() <= DayOfWeek.FRIDAY.ordinal();
		case MONDAY_TO_SATURDAY:
			return day.ordinal() <= DayOfWeek.SATURDAY.ordinal();
		default:
			return true;
		}
	}
}