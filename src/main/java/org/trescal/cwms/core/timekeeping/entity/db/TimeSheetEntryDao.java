package org.trescal.cwms.core.timekeeping.entity.db;

import java.time.LocalDateTime;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetEntry;

public interface TimeSheetEntryDao extends BaseDao<TimeSheetEntry, Integer> {
	
	List<TimeSheetEntry> findAllForEmployee(Contact employee, LocalDateTime begin, LocalDateTime end);
}