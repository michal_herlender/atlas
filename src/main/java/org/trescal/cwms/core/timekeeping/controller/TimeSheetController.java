package org.trescal.cwms.core.timekeeping.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.WorkingDays;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.WorkingHoursPerDay;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.timekeeping.TimeCollectors;
import org.trescal.cwms.core.timekeeping.dtos.TimeSheetDayDTO;
import org.trescal.cwms.core.timekeeping.dtos.TimeSheetStatisticDTO;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetEntry;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetValidation;
import org.trescal.cwms.core.timekeeping.entity.db.TimeSheetEntryService;
import org.trescal.cwms.core.timekeeping.entity.db.TimeSheetValidationService;
import org.trescal.cwms.core.timekeeping.enums.TimeSheetValidationStatus;
import org.trescal.cwms.core.timekeeping.enums.WorkingDay;
import org.trescal.cwms.core.timekeeping.form.TimeSheetSearchForm;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.time.temporal.WeekFields;
import java.util.*;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE})
public class TimeSheetController {

	private static final Logger logger = LoggerFactory.getLogger(TimeSheetController.class);

	@Autowired
	private JobItemActionService jobItemActionService;
	@Autowired
	private TimeSheetEntryService timeSheetEntryService;
	@Autowired
	private TimeSheetValidationService timeSheetValidationService;
	@Autowired
	private UserService userService;
	@Autowired
	private WorkingDays workingDays;
	@Autowired
	private WorkingHoursPerDay workingHoursPerDay;

	private List<LocalDate> daysOfWeek(LocalDate date) {
		List<LocalDate> daysOfWeek = new ArrayList<>();
		LocalDate monday = date.minusDays(date.getDayOfWeek().ordinal());
		for (int shift = 0; shift < 7; shift++)
			daysOfWeek.add(monday.plusDays(shift));
		return daysOfWeek;
	}

	@ModelAttribute("searchForm")
	public TimeSheetSearchForm initForm(
			@RequestParam(name = "date", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate date,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE) Locale defaultLocale) {
		return new TimeSheetSearchForm(date, defaultLocale);
	}

	@ModelAttribute("closed")
	public boolean isClosed(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName,
			@ModelAttribute("searchForm") TimeSheetSearchForm searchForm) {
		Contact employee = userService.get(userName).getCon();
		return timeSheetValidationService.existsNotRejected(employee, searchForm.getWeek(), searchForm.getYear());
	}

	@ModelAttribute("dayOfWeek")
	public DayOfWeek dayOfWeek(@ModelAttribute("searchForm") TimeSheetSearchForm searchForm) {
		return searchForm.getDate().getDayOfWeek();
	}

	@ModelAttribute("timeSheetMap")
	public SortedMap<DayOfWeek, TimeSheetDayDTO> timeSheetMap(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName,
			@ModelAttribute("searchForm") TimeSheetSearchForm searchForm, Locale locale) {
		SortedMap<DayOfWeek, TimeSheetDayDTO> timeSheetMap = new TreeMap<>();
		Contact employee = userService.get(userName).getCon();
		Company company = employee.getSub().getComp();
		for (LocalDate day : daysOfWeek(searchForm.getDate())) {
			logger.debug("Create Time Sheet for " + day.getDayOfWeek());
			TimeSheetDayDTO dayForm = new TimeSheetDayDTO();
			dayForm.setDate(day);
			dayForm.setDisplayName(day.getDayOfWeek().getDisplayName(TextStyle.FULL, locale) + ", "
				+ day.format(DateTimeFormatter.ofPattern("dd.MM.yy")));
			LocalDateTime startTime = day.atStartOfDay();
			LocalDateTime endTime = startTime.plusDays(1);
			List<TimeSheetEntry> entries = timeSheetEntryService.findAllForEmployee(employee, startTime, endTime);
			dayForm.setEntries(entries);
			dayForm.setTimeRelatedActions(jobItemActionService.findTimeRelated(employee, day, day.plusDays(1)));
			dayForm.setNotTimeRelatedActions(jobItemActionService.findNotTimeRelated(employee, day, day.plusDays(1)));
			Duration timeSpent = Duration
				.ofMinutes(jobItemActionService.summarizeTimeRelated(employee, day, day.plusDays(1)));
			dayForm.setTimeSpentOnTimeRelatedActions(timeSpent);
			dayForm.setTimeSpentOnNotTimeRelatedActions(
				Duration.ofMinutes(jobItemActionService.summarizeNotTimeRelated(employee, day, day.plusDays(1))));
			for (TimeSheetEntry entry : entries)
				timeSpent = timeSpent.plus(entry.getDuration());
			dayForm.setTimeSpent(timeSpent);
			List<WorkingDay> workingDaysValues = workingDays.parseValueHierarchical(Scope.CONTACT, employee, company);
			double workingMinutes = workingHoursPerDay.parseValueHierarchical(Scope.CONTACT, employee, company) * 60;

			if (workingDaysValues.stream().anyMatch(e -> e.name().equals(day.getDayOfWeek().name())))
				dayForm.setWorkingTime(Duration.ofMinutes((int) workingMinutes));
			else
				dayForm.setWorkingTime(Duration.ZERO);

			timeSheetMap.put(day.getDayOfWeek(), dayForm);
		}
		return timeSheetMap;
	}

	@ModelAttribute("weeklyStatistic")
	public TimeSheetStatisticDTO weeklyStatistic(
			@ModelAttribute("timeSheetMap") SortedMap<DayOfWeek, TimeSheetDayDTO> timeSheetMap) {
		Duration workingTime = timeSheetMap.values().stream().map(TimeSheetDayDTO::getWorkingTime)
				.collect(TimeCollectors.summarizeDuration());
		Duration achievedTime = timeSheetMap.values().stream().map(TimeSheetDayDTO::getTimeSpent)
				.collect(TimeCollectors.summarizeDuration());
		Duration overtime = timeSheetMap.values().stream().map(TimeSheetDayDTO::getTimeDifference)
				.collect(TimeCollectors.summarizeDuration());
		return new TimeSheetStatisticDTO(workingTime, achievedTime, overtime);
	}

	@ModelAttribute("monthlyStatistic")
	public TimeSheetStatisticDTO monthlyStatistic(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName,
			@ModelAttribute("searchForm") TimeSheetSearchForm searchForm) {
		Contact employee = userService.get(userName).getCon();
		Company company = employee.getSub().getComp();
		LocalDate date = searchForm.getDate();
		LocalDate firstOfMonth = LocalDate.of(date.getYear(), date.getMonth(), 1);
		LocalDate finalDate = date.getMonth() == LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).getMonth() ? LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())
			: firstOfMonth.plusMonths(1).minusDays(1);
		List<WorkingDay> workingDaysValues = workingDays.parseValueHierarchical(Scope.CONTACT, employee, company);
		int workingDays = 0;
		for (LocalDate d = firstOfMonth; d.compareTo(finalDate) <= 0; d = d.plusDays(1)) {
			LocalDate day = d;
			if (workingDaysValues.stream().anyMatch(e -> e.name().equals(day.getDayOfWeek().name())))
				workingDays++;
		}
		double workingMinutes = workingHoursPerDay.parseValueHierarchical(Scope.CONTACT, employee, company) * 60;
		Duration workingTime = Duration.ofMinutes((long) workingMinutes * workingDays);
		Duration entrySummary = timeSheetEntryService.monthlySummary(employee, firstOfMonth);
		Duration timeSpent = Duration.ofMinutes(
			jobItemActionService.summarizeTimeRelated(employee, firstOfMonth, firstOfMonth.plusMonths(1)));
		return new TimeSheetStatisticDTO(workingTime, entrySummary.plus(timeSpent),
			entrySummary.plus(timeSpent).minus(workingTime));
	}

	@RequestMapping(value = "/timesheet.htm")
	public String onRequestAndSubmit() {
		return "/trescal/core/timekeeping/timesheet";
	}

	@RequestMapping(value = "/timesheet.htm", params = "validation=create")
	public String generateValidation(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE) Locale defaultLocale,
			@ModelAttribute("timeSheetMap") SortedMap<DayOfWeek, TimeSheetDayDTO> timeSheetMap,
			@ModelAttribute("weeklyStatistic") TimeSheetStatisticDTO weeklyStatistic) {
		Contact employee = userService.get(userName).getCon();
		WeekFields weekFields = WeekFields.of(defaultLocale);
		LocalDate monday = timeSheetMap.get(DayOfWeek.MONDAY).getDate();
		int week = monday.get(weekFields.weekOfWeekBasedYear());
		Integer year = monday.get(weekFields.weekBasedYear());
		logger.info("Create time sheet validation for " + employee.getName() + " in week " + week + "/" + year);
		TimeSheetValidation timeSheetValidation = new TimeSheetValidation();
		timeSheetValidation.setEmployee(employee);
		timeSheetValidation.setWeek(week);
		timeSheetValidation.setYear(year);
		timeSheetValidation.setWorkingTime(weeklyStatistic.getWorkingTime());
		timeSheetValidation.setRegisteredTime(weeklyStatistic.getRegisteredTime());
		if (week > 1) {
			TimeSheetValidation lastWeek = timeSheetValidationService.findValidated(employee, week - 1, year);
			if (lastWeek == null)
				lastWeek = timeSheetValidationService.findReadyForValidation(employee, week - 1, year);
			Duration lastCarryOver = lastWeek == null ? Duration.ZERO : lastWeek.getCarryOver();
			timeSheetValidation.setCarryOver(lastCarryOver.plus(weeklyStatistic.getOvertime()));
		} else
			timeSheetValidation.setCarryOver(Duration.ZERO);
		timeSheetValidation.setStatus(TimeSheetValidationStatus.CREATED);
		timeSheetValidationService.save(timeSheetValidation);
		return "redirect:/timesheet.htm?date=" + monday;
	}
}