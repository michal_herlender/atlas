package org.trescal.cwms.core.timekeeping.entity.db;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.timekeeping.TimeCollectors;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetEntry;

@Service
public class TimeSheetEntryServiceImpl extends BaseServiceImpl<TimeSheetEntry, Integer>
		implements TimeSheetEntryService {

	@Autowired
	private TimeSheetEntryDao timeSheetEntryDao;

	@Override
	protected BaseDao<TimeSheetEntry, Integer> getBaseDao() {
		return timeSheetEntryDao;
	}

	@Override
	public List<TimeSheetEntry> findAllForEmployee(Contact employee, LocalDateTime begin, LocalDateTime end) {
		return timeSheetEntryDao.findAllForEmployee(employee, begin, end);
	}

	@Override
	public Duration monthlySummary(Contact employee, LocalDate firstOfMonth) {
		LocalDate firstOfNextMonth = firstOfMonth.plusMonths(1);
		List<TimeSheetEntry> entries = findAllForEmployee(employee, firstOfMonth.atStartOfDay(),
				firstOfNextMonth.atStartOfDay());
		return entries.stream().map(TimeSheetEntry::getDuration).collect(TimeCollectors.summarizeDuration());
	}
}