package org.trescal.cwms.core.timekeeping.enums;

public enum WorkingDay {
	
	MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY;

}