package org.trescal.cwms.core.timekeeping.entity.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetValidation;
import org.trescal.cwms.core.timekeeping.enums.TimeSheetValidationStatus;

public interface TimeSheetValidationDao extends BaseDao<TimeSheetValidation, Integer> {

	Boolean existsNotRejected(Contact employee, Integer week, Integer year);

	TimeSheetValidation findReadyForValidation(Contact employee, Integer week, Integer year);

	TimeSheetValidation findValidated(Contact employee, Integer week, Integer year);

	TimeSheetValidation findLatestRejected(Contact employee, Integer week, Integer year);

	List<TimeSheetValidation> findAll(Contact employee, Integer week, Integer year);

	List<TimeSheetValidation> findAll(Subdiv subdiv, Integer week, Integer year);

	List<TimeSheetValidation> findAll(Subdiv subdiv, TimeSheetValidationStatus status);
}