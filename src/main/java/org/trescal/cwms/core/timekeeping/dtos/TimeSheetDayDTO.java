package org.trescal.cwms.core.timekeeping.dtos;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

import org.trescal.cwms.core.jobs.jobitem.dto.JobItemActionForEmployeeDTO;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetEntry;

public class TimeSheetDayDTO {

	private LocalDate date;
	private String displayName;
	private List<TimeSheetEntry> entries;
	private List<JobItemActionForEmployeeDTO> timeRelatedActions;
	private List<JobItemActionForEmployeeDTO> notTimeRelatedActions;
	private Duration timeSpentOnTimeRelatedActions;
	private Duration timeSpentOnNotTimeRelatedActions;
	private Duration timeSpent;
	private Duration workingTime;

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public List<TimeSheetEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<TimeSheetEntry> entries) {
		this.entries = entries;
	}

	public List<JobItemActionForEmployeeDTO> getTimeRelatedActions() {
		return timeRelatedActions;
	}

	public void setTimeRelatedActions(List<JobItemActionForEmployeeDTO> timeRelatedActions) {
		this.timeRelatedActions = timeRelatedActions;
	}

	public List<JobItemActionForEmployeeDTO> getNotTimeRelatedActions() {
		return notTimeRelatedActions;
	}

	public void setNotTimeRelatedActions(List<JobItemActionForEmployeeDTO> notTimeRelatedActions) {
		this.notTimeRelatedActions = notTimeRelatedActions;
	}

	public Duration getTimeSpentOnTimeRelatedActions() {
		return timeSpentOnTimeRelatedActions;
	}

	public void setTimeSpentOnTimeRelatedActions(Duration timeSpentOnTimeRelatedActions) {
		this.timeSpentOnTimeRelatedActions = timeSpentOnTimeRelatedActions;
	}

	public Duration getTimeSpentOnNotTimeRelatedActions() {
		return timeSpentOnNotTimeRelatedActions;
	}

	public void setTimeSpentOnNotTimeRelatedActions(Duration timeSpentOnNotTimeRelatedActions) {
		this.timeSpentOnNotTimeRelatedActions = timeSpentOnNotTimeRelatedActions;
	}

	public Duration getTimeSpent() {
		return timeSpent;
	}

	public void setTimeSpent(Duration timeSpent) {
		this.timeSpent = timeSpent;
	}

	public Duration getWorkingTime() {
		return workingTime;
	}

	public void setWorkingTime(Duration workingTime) {
		this.workingTime = workingTime;
	}

	public Duration getTimeDifference() {
		return timeSpent.minus(workingTime);
	}
}