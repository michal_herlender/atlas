package org.trescal.cwms.core.timekeeping.enums;

public enum TimeActivity {

	ILLNESS("timeactivity.enum.illness"),
	HOLIDAY("timeactivity.enum.holiday"),
	WORK_SOCIAL("timeactivity.enum.worksocial"),
	WORK_TRAINING_EXTERNAL("timeactivity.enum.worktrainingexternal"),
	WORK_TRAINING_INTERNAL("timeactivity.enum.worktraininginternal"),
	WORK_MANAGEMENT("timeactivity.enum.workmanagement"),
	WORK_LOGISTICS("timeactivity.enum.worklogistics"),
	WORK_DEVELOPMENT("timeactivity.enum.workdevelopment"),
	WORK_ACCREDITATION("timeactivity.enum.workaccreditation"),
	WORK_MEETINGS("timeactivity.enum.workmeetings"),
	WORK_OTHER("timeactivity.enum.workother"),
	WORK_ASSET("timeactivity.enum.workasset"),
	WORK_TECHINCIAL_OPERATIONS("timeactivity.enum.worktechincialoperations"),
	JOBITEM_CALIBRATION("timeactivity.enum.jobitemcalibration"),
	JOBITEM_TEST("timeactivity.enum.jobitemtest"),
	JOBITEM_REPAIR("timeactivity.enum.jobitemrepair"),
	JOBITEM_PROCUREMENT("timeactivity.enum.jobitemprocurement"),
	JOBITEM_ASSET("timeactivity.enum.jobitemasset"),
	JOBITEM_SUPPORT("timeactivity.enum.jobitemsupport"),
	JOBITEM_ENGINEERING("timeactivity.enum.jobitemengineering"),
	JOBITEM_IT("timeactivity.enum.jobitemit"),
	JOB_PREPARATION_TIME("timeactivity.enum.jobpreparationtime"),
	TRAVEL_TIME("timeactivity.enum.traveltime"),
	ONSITE_DOWNTIME("timeactivity.enum.onsitedowntime"),
	ONSITE_CLOSING("timeactivity.enum.onsiteclosing");
	
	private String messageCode;
	
	private TimeActivity(String messageCode) {
		this.messageCode = messageCode;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
}