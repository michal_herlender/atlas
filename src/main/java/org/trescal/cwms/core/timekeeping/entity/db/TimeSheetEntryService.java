package org.trescal.cwms.core.timekeeping.entity.db;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetEntry;

public interface TimeSheetEntryService extends BaseService<TimeSheetEntry, Integer> {

	List<TimeSheetEntry> findAllForEmployee(Contact employee, LocalDateTime begin, LocalDateTime end);
	
	Duration monthlySummary(Contact employee, LocalDate firstOfMonth);
}