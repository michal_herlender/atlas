package org.trescal.cwms.core.timekeeping.entity;

import java.time.Duration;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.timekeeping.enums.TimeSheetValidationStatus;

@Entity
@Table(name = "timesheetvalidation")
public class TimeSheetValidation extends Versioned {

	private Integer id;
	private Contact employee;
	private Integer week;
	private Integer year;
	private Duration workingTime;
	private Duration registeredTime;
	private Duration carryOver;
	private TimeSheetValidationStatus status;
	/*
	 * In case of rejection, validatedBy and validatedOn contains the rejector and the date of rejection
	 */
	private Contact validatedBy;
	private LocalDateTime validatedOn;
	private String rejectionReason;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "employeeid", nullable = false, foreignKey = @ForeignKey(name = "FK_timesheetvalidation_employee"))
	public Contact getEmployee() {
		return employee;
	}

	public void setEmployee(Contact employee) {
		this.employee = employee;
	}

	@NotNull
	@Max(53)
	@Column(name = "week", nullable = false)
	public Integer getWeek() {
		return week;
	}

	public void setWeek(Integer week) {
		this.week = week;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	@Column(name = "workingtime")
	public Duration getWorkingTime() {
		return workingTime;
	}

	public void setWorkingTime(Duration workingTime) {
		this.workingTime = workingTime;
	}

	@Column(name = "registeredtime")
	public Duration getRegisteredTime() {
		return registeredTime;
	}

	public void setRegisteredTime(Duration registeredTime) {
		this.registeredTime = registeredTime;
	}

	@NotNull
	@Column(name = "carryover", nullable = false)
	public Duration getCarryOver() {
		return carryOver;
	}

	public void setCarryOver(Duration carryOver) {
		this.carryOver = carryOver;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "status", nullable = false)
	public TimeSheetValidationStatus getStatus() {
		return status;
	}

	public void setStatus(TimeSheetValidationStatus status) {
		this.status = status;
	}

	@ManyToOne()
	@JoinColumn(name = "validatedby", foreignKey = @ForeignKey(name = "FK_timesheetvalidation_validatedby"))
	public Contact getValidatedBy() {
		return validatedBy;
	}

	public void setValidatedBy(Contact validatedBy) {
		this.validatedBy = validatedBy;
	}

	@Column(name = "validatedon")
	public LocalDateTime getValidatedOn() {
		return validatedOn;
	}

	public void setValidatedOn(LocalDateTime validatedOn) {
		this.validatedOn = validatedOn;
	}

	@Column(name = "rejectionreason")
	public String getRejectionReason() {
		return rejectionReason;
	}

	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}
}