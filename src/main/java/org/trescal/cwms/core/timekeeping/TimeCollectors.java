package org.trescal.cwms.core.timekeeping;

import java.time.Duration;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class TimeCollectors {

	private TimeCollectors() {
	}

	public static Collector<Duration, Duration[], Duration> summarizeDuration() {
		Duration[] zero = new Duration[1];
		zero[0] = Duration.ZERO;
		Supplier<Duration[]> supplier = () -> zero;
		BiConsumer<Duration[], Duration> biConsumer = (sum, d) -> sum[0] = sum[0].plus(d);
		BinaryOperator<Duration[]> binaryOperator = (sum1, sum2) -> {
			sum1[0].plus(sum2[0]);
			return sum1;
		};
		return Collector.of(supplier, biConsumer, binaryOperator, sum -> sum[0]);
	}
}