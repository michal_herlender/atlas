package org.trescal.cwms.core.timekeeping.form;

import lombok.Data;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetEntry;
import org.trescal.cwms.core.timekeeping.enums.TimeActivity;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
public class TimeSheetEntryForm {

	private Integer id;
	@NotNull
	private Contact employee;
	@NotNull
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate startDate;
	@NotNull
	private LocalTime startTime;
	@NotNull
	private Integer minutesSpent;
	private Integer hoursSpent;
	private Boolean fullTime;
	@NotNull
	private TimeActivity timeActivity;
	private BigDecimal expenseValue;
	private Integer expenseCurrencyId;
	private Integer jobId;
	private String jobNo;
	private Integer subdivId;
	private String subdivName;
	private String comment;

	public TimeSheetEntryForm() {
	}

	public TimeSheetEntryForm(@NotNull Contact employee) {
		this.employee = employee;
		startDate = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		startTime = LocalTime.now();
		minutesSpent = 0;
		hoursSpent = 0;
		fullTime = false;
	}

	public TimeSheetEntryForm(@NotNull Contact employee, @NotNull LocalDate date) {
		this.employee = employee;
		startDate = date;
		startTime = LocalTime.MIDNIGHT;
		minutesSpent = 0;
		hoursSpent = 0;
		fullTime = false;
	}

	public TimeSheetEntryForm(TimeSheetEntry entry) {
		id = entry.getId();
		employee = entry.getEmployee();
		startDate = entry.getStart().toLocalDate();
		startTime = entry.getStart().toLocalTime();
		int duration = (int) entry.getDuration().toMinutes();
		minutesSpent = duration % 60;
		hoursSpent = duration / 60;
		fullTime = entry.getFullTime();
		timeActivity = entry.getTimeActivity();
		expenseValue = entry.getExpenseValue();
		expenseCurrencyId = entry.getExpenseCurrency() == null ? null : entry.getExpenseCurrency().getCurrencyId();
		jobId = entry.getJob() == null ? null : entry.getJob().getJobid();
		jobNo = entry.getJob() == null ? "" : entry.getJob().getJobno();
		subdivId = entry.getSubdiv() == null ? null : entry.getSubdiv().getSubdivid();
		subdivName = entry.getSubdiv() == null ? ""
				: entry.getSubdiv().getSubname() + " (" + entry.getSubdiv().getComp().getConame() + ")";
		comment = entry.getComment();
	}
}