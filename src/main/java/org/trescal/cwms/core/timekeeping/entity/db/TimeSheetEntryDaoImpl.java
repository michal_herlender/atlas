package org.trescal.cwms.core.timekeeping.entity.db;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetEntry;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetEntry_;

@Repository
public class TimeSheetEntryDaoImpl extends BaseDaoImpl<TimeSheetEntry, Integer> implements TimeSheetEntryDao {
	
	@Override
	protected Class<TimeSheetEntry> getEntity() {
		return TimeSheetEntry.class;
	}
	
	@Override
	public List<TimeSheetEntry> findAllForEmployee(Contact employee, LocalDateTime begin, LocalDateTime end) {
		return getResultList(cb -> {
			CriteriaQuery<TimeSheetEntry> cq = cb.createQuery(TimeSheetEntry.class);
			Root<TimeSheetEntry> root = cq.from(TimeSheetEntry.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(TimeSheetEntry_.employee), employee));
			clauses.getExpressions().add(cb.greaterThanOrEqualTo(root.get(TimeSheetEntry_.start), begin));
			clauses.getExpressions().add(cb.lessThan(root.get(TimeSheetEntry_.start), end));
			cq.where(clauses);
			return cq;
		});
	}
}