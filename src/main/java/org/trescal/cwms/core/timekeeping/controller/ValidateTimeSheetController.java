package org.trescal.cwms.core.timekeeping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.WorkingDays;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.WorkingHoursPerDay;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.timekeeping.TimeCollectors;
import org.trescal.cwms.core.timekeeping.dtos.TimeSheetDayDTO;
import org.trescal.cwms.core.timekeeping.dtos.TimeSheetStatisticDTO;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetEntry;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetValidation;
import org.trescal.cwms.core.timekeeping.entity.db.TimeSheetEntryService;
import org.trescal.cwms.core.timekeeping.entity.db.TimeSheetValidationService;
import org.trescal.cwms.core.timekeeping.enums.TimeSheetValidationStatus;
import org.trescal.cwms.core.timekeeping.enums.WorkingDay;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.time.temporal.WeekFields;
import java.util.*;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV,
	Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE})
public class ValidateTimeSheetController {

	@Autowired
	private JobItemActionService jobItemActionService;
	@Autowired
	private TimeSheetEntryService timeSheetEntryService;
	@Autowired
	private TimeSheetValidationService timeSheetValidationService;
	@Autowired
	private UserService userService;
	@Autowired
	private WorkingDays workingDays;
	@Autowired
	private WorkingHoursPerDay workingHoursPerDay;

	private List<LocalDate> daysOfWeek(LocalDate date) {
		List<LocalDate> daysOfWeek = new ArrayList<>();
		LocalDate monday = date.minusDays(date.getDayOfWeek().ordinal());
		for (int shift = 0; shift < 7; shift++)
			daysOfWeek.add(monday.plusDays(shift));
		return daysOfWeek;
	}

	@ModelAttribute("timeSheet")
	public TimeSheetValidation timeSheet(@RequestParam(name = "timesheetid") Integer timeSheetId) {
		return timeSheetValidationService.get(timeSheetId);
	}

	@ModelAttribute("lastTimeSheet")
	public TimeSheetValidation lastTimeSheet(@ModelAttribute("timeSheet") TimeSheetValidation timeSheet) {
		if (timeSheet.getWeek() > 0) {
			TimeSheetValidation lastTimeSheet = timeSheetValidationService.findValidated(timeSheet.getEmployee(),
					timeSheet.getWeek() - 1, timeSheet.getYear());
			if (lastTimeSheet == null)
				// if there is no validated time-sheet, accept created, but not rejected
				return timeSheetValidationService.findReadyForValidation(timeSheet.getEmployee(),
						timeSheet.getWeek() - 1, timeSheet.getYear());
			else
				return lastTimeSheet;
		}
		return null;
	}

	@ModelAttribute("timeSheetMap")
	public SortedMap<DayOfWeek, TimeSheetDayDTO> timeSheetMap(
			@ModelAttribute("timeSheet") TimeSheetValidation timeSheet,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE) Locale defaultLocale, Locale locale) {
		SortedMap<DayOfWeek, TimeSheetDayDTO> timeSheetMap = new TreeMap<>();
		Contact employee = timeSheet.getEmployee();
		Company company = employee.getSub().getComp();
		WeekFields weekFields = WeekFields.of(defaultLocale);
		LocalDate monday = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).withYear(timeSheet.getYear())
			.with(weekFields.weekOfYear(), timeSheet.getWeek()).with(weekFields.dayOfWeek(), 1);
		for (LocalDate day : daysOfWeek(monday)) {
			TimeSheetDayDTO dayForm = new TimeSheetDayDTO();
			dayForm.setDate(day);
			dayForm.setDisplayName(day.getDayOfWeek().getDisplayName(TextStyle.FULL, locale) + ", "
				+ day.format(DateTimeFormatter.ofPattern("dd.MM.yy")));
			LocalDateTime startTime = day.atStartOfDay();
			LocalDateTime endTime = startTime.plusDays(1);
			List<TimeSheetEntry> entries = timeSheetEntryService.findAllForEmployee(employee, startTime, endTime);
			dayForm.setEntries(entries);
			dayForm.setTimeRelatedActions(jobItemActionService.findTimeRelated(employee, day, day.plusDays(1)));
			Duration timeSpent = Duration
				.ofMinutes(jobItemActionService.summarizeTimeRelated(employee, day, day.plusDays(1)));
			dayForm.setTimeSpentOnTimeRelatedActions(timeSpent);
			for (TimeSheetEntry entry : entries)
				timeSpent = timeSpent.plus(entry.getDuration());
			dayForm.setTimeSpent(timeSpent);
			List<WorkingDay> workingDaysValues = workingDays.parseValueHierarchical(Scope.CONTACT, employee, company);
			double workingMinutes = workingHoursPerDay.parseValueHierarchical(Scope.CONTACT, employee, company) * 60;

			if (workingDaysValues.stream().anyMatch(e -> e.name().equals(day.getDayOfWeek().name())))
				dayForm.setWorkingTime(Duration.ofMinutes((int) workingMinutes));
			else
				dayForm.setWorkingTime(Duration.ZERO);

			timeSheetMap.put(day.getDayOfWeek(), dayForm);
		}
		return timeSheetMap;
	}

	@ModelAttribute("weeklyStatistic")
	public TimeSheetStatisticDTO weeklyStatistic(
			@ModelAttribute("timeSheetMap") SortedMap<DayOfWeek, TimeSheetDayDTO> timeSheetMap) {
		Duration workingTime = timeSheetMap.values().stream().map(TimeSheetDayDTO::getWorkingTime)
				.collect(TimeCollectors.summarizeDuration());
		Duration achievedTime = timeSheetMap.values().stream().map(TimeSheetDayDTO::getTimeSpent)
				.collect(TimeCollectors.summarizeDuration());
		Duration overtime = timeSheetMap.values().stream().map(TimeSheetDayDTO::getTimeDifference)
				.collect(TimeCollectors.summarizeDuration());
		return new TimeSheetStatisticDTO(workingTime, achievedTime, overtime);
	}

	@ModelAttribute("monthlyStatistic")
	public TimeSheetStatisticDTO monthlyStatistic(@ModelAttribute("timeSheet") TimeSheetValidation timeSheet) {
		Contact employee = timeSheet.getEmployee();
		Company company = employee.getSub().getComp();
		WeekFields weekFields = WeekFields.of(Locale.getDefault());
		LocalDate date = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).withYear(timeSheet.getYear())
			.with(weekFields.weekOfYear(), timeSheet.getWeek()).with(weekFields.dayOfWeek(), 1);
		LocalDate firstOfMonth = LocalDate.of(date.getYear(), date.getMonth(), 1);
		LocalDate finalDate = date.getMonth() == LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).getMonth() ? LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())
			: firstOfMonth.plusMonths(1).minusDays(1);
		List<WorkingDay> workingDaysValues = workingDays.parseValueHierarchical(Scope.CONTACT, employee, company);
		int workingDays = 0;
		for (LocalDate d = firstOfMonth; d.compareTo(finalDate) <= 0; d = d.plusDays(1)) {
			LocalDate day = d;
			if (workingDaysValues.stream().anyMatch(e -> e.name().equals(day.getDayOfWeek().name())))
				workingDays++;
		}
		double workingMinutes = workingHoursPerDay.parseValueHierarchical(Scope.CONTACT, employee, company) * 60;
		Duration workingTime = Duration.ofMinutes((long) workingMinutes * workingDays);
		Duration entrySummary = timeSheetEntryService.monthlySummary(employee, firstOfMonth);
		Duration timeSpent = Duration.ofMinutes(
			jobItemActionService.summarizeTimeRelated(employee, firstOfMonth, firstOfMonth.plusMonths(1)));
		return new TimeSheetStatisticDTO(workingTime, entrySummary.plus(timeSpent),
			entrySummary.plus(timeSpent).minus(workingTime));
	}

	@RequestMapping(value = "/validatetimesheet.htm", method = RequestMethod.GET)
	public String onRequest(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		return "/trescal/core/timekeeping/validatetimesheet";
	}

	@RequestMapping(value = "/validatetimesheet.htm", params = "action=VALIDATE", method = RequestMethod.POST)
	public String onSubmitValidate(@ModelAttribute("timeSheet") TimeSheetValidation timeSheet,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName) {
		Contact contact = userService.get(userName).getCon();
		timeSheet.setStatus(TimeSheetValidationStatus.VALIDATED);
		timeSheet.setValidatedOn(LocalDateTime.now());
		timeSheet.setValidatedBy(contact);
		timeSheetValidationService.merge(timeSheet);
		return "redirect:/timesheetvalidation.htm?week=" + timeSheet.getWeek() + "&year=" + timeSheet.getYear();
	}

	@RequestMapping(value = "/validatetimesheet.htm", params = "action=REJECT", method = RequestMethod.POST)
	public String onSubmitReject(@Validated @ModelAttribute("timeSheet") TimeSheetValidation timeSheet,
			BindingResult bindingResult, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName) {
		if (timeSheet.getRejectionReason() == null || timeSheet.getRejectionReason().isEmpty())
			bindingResult.addError(new FieldError("timeSheet", "rejectionReason", "should be not empty"));
		if (bindingResult.hasErrors())
			return "/trescal/core/timekeeping/validatetimesheet";
		Contact contact = userService.get(userName).getCon();
		timeSheet.setStatus(TimeSheetValidationStatus.REJECTED);
		timeSheet.setValidatedOn(LocalDateTime.now());
		timeSheet.setValidatedBy(contact);
		timeSheetValidationService.merge(timeSheet);
		return "redirect:/timesheetvalidation.htm?week=" + timeSheet.getWeek() + "&year=" + timeSheet.getYear();
	}
}