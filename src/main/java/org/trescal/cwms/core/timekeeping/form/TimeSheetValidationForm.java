package org.trescal.cwms.core.timekeeping.form;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class TimeSheetValidationForm {

	private Integer week;
	private Integer year;
	private Boolean submitted;

	public TimeSheetValidationForm() {
	}

	public TimeSheetValidationForm(Integer week, Integer year) {
		this.week = week;
		this.year = year;
		this.submitted = false;
	}

	@Min(0)
	@Max(53)
	public Integer getWeek() {
		return week;
	}

	public void setWeek(Integer week) {
		this.week = week;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Boolean getSubmitted() {
		return submitted;
	}

	public void setSubmitted(Boolean submitted) {
		this.submitted = submitted;
	}
}