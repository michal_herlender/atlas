package org.trescal.cwms.core.timekeeping.entity.db;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetValidation;
import org.trescal.cwms.core.timekeeping.enums.TimeSheetValidationStatus;

@Service
public class TimeSheetValidationServiceImpl extends BaseServiceImpl<TimeSheetValidation, Integer>
		implements TimeSheetValidationService {

	@Autowired
	private TimeSheetValidationDao timeSheetValidationDao;

	@Override
	protected BaseDao<TimeSheetValidation, Integer> getBaseDao() {
		return timeSheetValidationDao;
	}

	@Override
	public Boolean existsNotRejected(Contact employee, Integer week, Integer year) {
		return timeSheetValidationDao.existsNotRejected(employee, week, year);
	}

	@Override
	public List<TimeSheetValidation> findAll(Contact employee, Integer week, Integer year) {
		return timeSheetValidationDao.findAll(employee, week, year);
	}

	@Override
	public TimeSheetValidation findReadyForValidation(Contact employee, Integer week, Integer year) {
		return timeSheetValidationDao.findReadyForValidation(employee, week, year);
	}

	@Override
	public TimeSheetValidation findValidated(Contact employee, Integer week, Integer year) {
		return timeSheetValidationDao.findValidated(employee, week, year);
	}

	@Override
	public List<TimeSheetValidation> findAll(Subdiv subdiv, Integer week, Integer year) {
		return timeSheetValidationDao.findAll(subdiv, week, year);
	}

	@Override
	public SortedMap<Integer, SortedMap<Integer, List<TimeSheetValidation>>> findAllReadyForValidation(Subdiv subdiv) {
		List<TimeSheetValidation> timeSheets = timeSheetValidationDao.findAll(subdiv,
				TimeSheetValidationStatus.CREATED);
		Comparator<Integer> inverseComparator = (a, b) -> b.compareTo(a);
		SortedMap<Integer, SortedMap<Integer, List<TimeSheetValidation>>> yearMap = new TreeMap<Integer, SortedMap<Integer, List<TimeSheetValidation>>>(
				inverseComparator);
		for (TimeSheetValidation timeSheet : timeSheets) {
			if (!yearMap.containsKey(timeSheet.getYear()))
				yearMap.put(timeSheet.getYear(), new TreeMap<Integer, List<TimeSheetValidation>>(inverseComparator));
			SortedMap<Integer, List<TimeSheetValidation>> weekMap = yearMap.get(timeSheet.getYear());
			if (!weekMap.containsKey(timeSheet.getWeek()))
				weekMap.put(timeSheet.getWeek(), new ArrayList<TimeSheetValidation>());
			weekMap.get(timeSheet.getWeek()).add(timeSheet);
		}
		return yearMap;
	}
}