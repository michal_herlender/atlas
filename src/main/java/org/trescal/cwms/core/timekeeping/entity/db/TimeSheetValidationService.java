package org.trescal.cwms.core.timekeeping.entity.db;

import java.util.List;
import java.util.SortedMap;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetValidation;

public interface TimeSheetValidationService extends BaseService<TimeSheetValidation, Integer> {

	Boolean existsNotRejected(Contact employee, Integer week, Integer year);

	TimeSheetValidation findReadyForValidation(Contact employee, Integer week, Integer year);

	TimeSheetValidation findValidated(Contact employee, Integer week, Integer year);

	List<TimeSheetValidation> findAll(Contact employee, Integer week, Integer year);

	List<TimeSheetValidation> findAll(Subdiv subdiv, Integer week, Integer year);

	SortedMap<Integer, SortedMap<Integer, List<TimeSheetValidation>>> findAllReadyForValidation(Subdiv subdiv);
}