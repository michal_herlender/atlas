package org.trescal.cwms.core.timekeeping.enums;

public enum TimeSheetValidationStatus {
	CREATED,
	VALIDATED,
	REJECTED,
	REPLACED;
}