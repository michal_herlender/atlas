package org.trescal.cwms.core.timekeeping.controller;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetEntry;
import org.trescal.cwms.core.timekeeping.entity.db.TimeSheetEntryService;
import org.trescal.cwms.core.timekeeping.enums.TimeActivity;
import org.trescal.cwms.core.timekeeping.form.TimeSheetEntryForm;

import com.mchange.v1.util.UnexpectedException;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class TimeSheetEntryController {

	private static final Logger logger = LoggerFactory.getLogger(TimeSheetEntryController.class);

	@Autowired
	private JobService jobService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SupportedCurrencyService supportedCurrencyService;
	@Autowired
	private TimeSheetEntryService timeSheetEntryService;
	@Autowired
	private UserService userService;

	@ModelAttribute("entryForm")
	public TimeSheetEntryForm initForm(@RequestParam(name = "entryid", required = false) Integer entryId,
			@RequestParam(name = "date", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate date,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName) {
		Contact currentContact = userService.get(userName).getCon();
		if (entryId == null)
			if (date == null)
				return new TimeSheetEntryForm(currentContact);
			else
				return new TimeSheetEntryForm(currentContact, date);
		else {
			TimeSheetEntry entry = timeSheetEntryService.get(entryId);
			if (entry == null)
				throw new UnexpectedException("NO ENTRY WITH GIVEN ID !!!");
			if (entry.getEmployee().equals(currentContact))
				return new TimeSheetEntryForm(entry);
			else {
				logger.info(currentContact.getName() + " (" + currentContact.getPersonid()
						+ ") tries to edit a time sheet entry of " + entry.getEmployee().getName() + "("
						+ entry.getEmployee().getPersonid() + ").");
				throw new RuntimeException("FORBIDDEN !!!");
			}
		}
	}

	@RequestMapping(value = "/timesheetentry.htm", method = RequestMethod.GET)
	public ModelAndView onRequest(Locale locale) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("timeActivities", TimeActivity.values());
		model.put("currencies", supportedCurrencyService.getAllSupportedCurrencys());
		return new ModelAndView("/trescal/core/timekeeping/timesheetentry", model);
	}

	private void copy(TimeSheetEntryForm form, TimeSheetEntry entry) {
		entry.setStart(form.getStartDate().atStartOfDay().plusNanos(form.getStartTime().toNanoOfDay()));
		entry.setDuration(Duration.of(form.getMinutesSpent() + 60 * form.getHoursSpent(), ChronoUnit.MINUTES));
		entry.setFullTime(form.getFullTime());
		entry.setTimeActivity(form.getTimeActivity());
		if (form.getExpenseValue() == null || form.getExpenseValue().equals(BigDecimal.ZERO)) {
			entry.setExpenseValue(null);
			entry.setExpenseCurrency(null);
		} else {
			entry.setExpenseValue(form.getExpenseValue());
			SupportedCurrency currency = supportedCurrencyService.findSupportedCurrency(form.getExpenseCurrencyId());
			entry.setExpenseCurrency(currency);
		}
		if (form.getJobId() != null)
			entry.setJob(jobService.get(form.getJobId()));
		if (form.getSubdivId() != null)
			entry.setSubdiv(subdivService.get(form.getSubdivId()));
		entry.setComment(form.getComment());
	}

	@RequestMapping(value = "/timesheetentry.htm", method = RequestMethod.POST)
	public ModelAndView onSubmit(@Validated @ModelAttribute("entryForm") TimeSheetEntryForm entryForm,
			BindingResult bindingResult, Locale locale) {
		bindingResult.getAllErrors().forEach(err -> logger.info(err.getDefaultMessage()));
		if (!bindingResult.hasErrors()) {
			TimeSheetEntry entry;
			if (entryForm.getId() == null) {
				entry = new TimeSheetEntry();
				entry.setEmployee(entryForm.getEmployee());
				copy(entryForm, entry);
				timeSheetEntryService.save(entry);
			} else {
				entry = timeSheetEntryService.get(entryForm.getId());
				copy(entryForm, entry);
				timeSheetEntryService.merge(entry);
			}
			return new ModelAndView(new RedirectView("timesheet.htm?date=" + entryForm.getStartDate()));
		} else
			return onRequest(locale);
	}
}