package org.trescal.cwms.core.timekeeping.entity;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.timekeeping.enums.TimeActivity;

import lombok.Setter;

@Setter
@Entity
@Table(name = "timesheetentry", indexes = {
		@Index(name = "IDX_timesheetentry_timeactivity", columnList = "timeactivity") })
public class TimeSheetEntry {

	private Integer id;
	private Contact employee;
	private LocalDateTime start;
	private Duration duration;
	private Boolean fullTime;
	private TimeActivity timeActivity;
	private BigDecimal expenseValue;
	private SupportedCurrency expenseCurrency;
	private Job job;
	private Subdiv subdiv;
	private String comment;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "employeeid", foreignKey = @ForeignKey(name = "FK_timesheetentry_employee"), nullable = false)
	public Contact getEmployee() {
		return employee;
	}

	@NotNull
	@Column(name = "start", nullable = false)
	public LocalDateTime getStart() {
		return start;
	}

	@NotNull
	@Column(name = "duration", nullable = false)
	public Duration getDuration() {
		return duration;
	}

	@NotNull
	@Column(name = "fulltime", nullable = false)
	public Boolean getFullTime() {
		return fullTime;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "timeactivity", nullable = false)
	public TimeActivity getTimeActivity() {
		return timeActivity;
	}

	@Column(name = "expensevalue")
	public BigDecimal getExpenseValue() {
		return expenseValue;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "expensecurrencyid", foreignKey = @ForeignKey(name = "FK_timesheetentry_expensecurrency"))
	public SupportedCurrency getExpenseCurrency() {
		return expenseCurrency;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobid", foreignKey = @ForeignKey(name = "FK_timesheetentry_job"))
	public Job getJob() {
		return job;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subdivid", foreignKey = @ForeignKey(name = "FK_timesheetentry_subdiv"))
	public Subdiv getSubdiv() {
		return subdiv;
	}

	@Column(name = "comment")
	public String getComment() {
		return comment;
	}
}