package org.trescal.cwms.core.audit.entity.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.AuditTriggerSwitch;

@Repository
public class AuditTriggerSwitchDaoImpl extends BaseDaoImpl<AuditTriggerSwitch, Integer> implements AuditTriggerSwitchDao
{
	@Override
	protected Class<AuditTriggerSwitch> getEntity() {
		return AuditTriggerSwitch.class;
	}
}