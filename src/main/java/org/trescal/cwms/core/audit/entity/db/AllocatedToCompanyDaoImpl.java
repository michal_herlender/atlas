package org.trescal.cwms.core.audit.entity.db;

import java.io.Serializable;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.audit.Allocated_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.company.db.CompanyDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivDao;

public abstract class AllocatedToCompanyDaoImpl<Entity extends Allocated<Company>, Identifier extends Serializable>
extends AllocatedDaoImpl<Company, Entity, Identifier>{
	
	@Autowired
	private SubdivDao subdivDao;
	@Autowired
	private CompanyDao companyDao;
	
	@Override
	protected Class<Company> getOrgLevelType() {
		return Company.class;
	}
	
	/**
	 * @deprecated
	 * Use JPA 2
	 */
	@Override
	public Criterion getSubdivCrit(Subdiv subdiv) {
		return this.getOrganisationCrit(subdiv.getComp());
	}
	
	@Override
	public List<Entity> getAllByCompany(Company allocatedCompany) {
		return getResultList(cb -> {
			CriteriaQuery<Entity> cq = cb.createQuery(getEntity());
			Root<Entity> root = cq.from(getEntity());
			cq.where(cb.equal(root.get(Allocated_.organisation), allocatedCompany));
			return cq;
		});
	}
	
	/**
	 * @deprecated
	 * Use JPA 2
	 */
	@Override
	public Criterion getSubdivCrit(Integer subdivid) {
		return this.getOrganisationCrit(subdivDao.find(subdivid).getComp());
	}
	
	@Override
	public List<Entity> getAllBySubdiv(Subdiv allocatedSubdiv) {
		return getResultList(cb -> {
			CriteriaQuery<Entity> cq = cb.createQuery(getEntity());
			Root<Entity> root = cq.from(getEntity());
			Join<Entity, Company> company = root.join("organisation", JoinType.INNER);
			Join<Company, Subdiv> subdiv = company.join(Company_.subdivisions, JoinType.INNER);
			cq.where(cb.equal(subdiv, allocatedSubdiv));
			return cq;
		});
	}
	
	/**
	 * @deprecated
	 * Use JPA 2
	 */
	@Override
	public Criterion getCompanyCrit(Company company) {
		return this.getOrganisationCrit(company);
	}
	
	/**
	 * @deprecated
	 * Use JPA 2
	 */
	@Override
	public Criterion getCompanyCrit(Integer coid) {
		return this.getOrganisationCrit(companyDao.find(coid));
	}
}