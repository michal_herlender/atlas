package org.trescal.cwms.core.audit;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.experimental.SuperBuilder;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.trescal.cwms.core.company.entity.contact.Contact;

/**
 * Change history: 2017-08-16 GB: Refactored to use JPA auditing framework.
 * 
 * Formerly a contact ID was stored in "log_userid" and a Long Unix timestamp
 * was stored in "log_lastmodified". New fields use the same convention as
 * Versioned. (lastModifiedBy and lastModified)
 * 
 * This is meant to ease an entity of moving from Auditable to Versioned when
 * required, however testing should still be done when moving a class to
 * Versioned for unsaved-mapping exceptions etc...
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Auditable {
	private Contact lastModifiedBy;
	private Date lastModified;

	/**
	 * 
	 * @return
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "lastModifiedBy")
	@LastModifiedBy
	public Contact getLastModifiedBy() {
		return this.lastModifiedBy;
	}

	/**
	 * 
	 */
	@Column(name = "lastModified", columnDefinition = "datetime2")
	@LastModifiedDate
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastModified() {
		return this.lastModified;
	}

	public void setLastModifiedBy(Contact lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
}