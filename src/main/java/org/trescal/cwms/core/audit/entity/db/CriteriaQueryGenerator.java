package org.trescal.cwms.core.audit.entity.db;

import java.util.List;
import java.util.function.Function;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;

public interface CriteriaQueryGenerator<ResultEntity> extends
		Function<CriteriaBuilder, Function<CriteriaQuery<?>, Triple<Expression<?>, Selection<ResultEntity>, List<Order>>>> {

}
