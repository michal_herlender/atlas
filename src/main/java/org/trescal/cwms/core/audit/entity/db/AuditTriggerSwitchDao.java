package org.trescal.cwms.core.audit.entity.db;

import org.trescal.cwms.core.audit.entity.AuditTriggerSwitch;

public interface AuditTriggerSwitchDao extends BaseDao<AuditTriggerSwitch, Integer>
{
}