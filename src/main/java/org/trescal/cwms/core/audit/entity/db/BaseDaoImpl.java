package org.trescal.cwms.core.audit.entity.db;

import java.io.Serializable;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

public abstract class BaseDaoImpl<Entity, Identifier extends Serializable> extends AbstractDaoImpl
		implements BaseDao<Entity, Identifier> {

	protected abstract Class<Entity> getEntity();

	protected <PropertyType> List<Entity> findByProperty(SingularAttribute<Entity, PropertyType> property,
			PropertyType value) {
		return getResultList(getFindByPropertyFunction(property, value));
	}

	protected <PropertyType> Entity findByUniqueProperty(SingularAttribute<Entity, PropertyType> property,
			PropertyType value) {
		return getSingleResult(getFindByPropertyFunction(property, value));
	}

	private <PropertyType> Function<CriteriaBuilder, CriteriaQuery<Entity>> getFindByPropertyFunction(
			SingularAttribute<Entity, PropertyType> property, PropertyType value) {
		return cb -> {
			CriteriaQuery<Entity> cq = cb.createQuery(getEntity());
			Root<Entity> entity = cq.from(getEntity());
			cq.where(cb.equal(entity.get(property), value));
			return cq;
		};
	}

	@Override
	public Entity find(Identifier id) {
		return getEntityManager().find(getEntity(), id);
	}

	@Override
	public Entity getReference(Identifier id) {
		return getEntityManager().getReference(getEntity(), id);
	}

	@Override
	public List<Entity> findAll() {
		return getResultList(cb -> {
			CriteriaQuery<Entity> cq = cb.createQuery(getEntity());
			cq.from(getEntity());
			return cq;
		});
	}

	@Override
	public void remove(Entity object) {
		getEntityManager().remove(object);
	}

	@Override
	public void detach(Entity object) {
		getEntityManager().detach(object);
	}

	@Override
	public Entity merge(Entity object) {
		return getEntityManager().merge(object);
	}

	@Override
	public List<Entity> mergeAll(List<Entity> objects) {
		return objects.stream().map(this::merge).collect(Collectors.toList());
	}

	@Override
	public void persist(Entity object) {
		getEntityManager().persist(object);
	}

	/**
	 * @deprecated Use persist and merge in the right way
	 */
	@Override
	public void saveOrUpdate(Entity object) {
		getSession().saveOrUpdate(object);
	}

	/**
	 * @deprecated Use mergeAll in the right way
	 */
	@Override
	public void saveOrUpdateAll(List<Entity> objects) {
		objects.forEach(this::saveOrUpdate);
	}

	/**
	 * @deprecated Use merge in the right way
	 */
	@Override
	public void update(Entity object) {
		getSession().update(object);
	}
}