package org.trescal.cwms.core.audit.entity.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.AuditTriggerSwitch;

@Service
public class AuditTriggerSwitchServiceImpl extends BaseServiceImpl<AuditTriggerSwitch, Integer> implements AuditTriggerSwitchService
{
	@Autowired
	private AuditTriggerSwitchDao auditTriggerSwitchDao;

	@Override
	public AuditTriggerSwitch findAuditTriggerSwitch() {
		List<AuditTriggerSwitch> results = this.auditTriggerSwitchDao.findAll();
		if (results.isEmpty()) return null;
		else return results.get(0);
	}
	
	@Override
	public void updateTriggerSwitch(boolean enabled)
	{
		AuditTriggerSwitch swit = this.findAuditTriggerSwitch();
		if (swit == null)
		{
			swit = new AuditTriggerSwitch();
		}
		swit.setEnabled(enabled);
		this.merge(swit);
	}
	
	@Override
	public boolean getEnabled() {
		AuditTriggerSwitch swit = findAuditTriggerSwitch();
		if (swit == null) return false;
		else return swit.isEnabled();
	}

	@Override
	protected BaseDao<AuditTriggerSwitch, Integer> getBaseDao() {
		// TODO Auto-generated method stub
		return null;
	}
}