package org.trescal.cwms.core.audit.entity.db;

import java.util.List;

import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;


import org.apache.commons.lang3.tuple.Triple;

import lombok.Value;
import lombok.With;

@Value(staticConstructor = "of")
@With
public class RootSelectRestrictionOrder<RootType,SelectType>  {

	Root<RootType> root;
	Selection<SelectType> select;
	Predicate predicate;
	List<Order> order;
	
	public <SelectType2> RootSelectRestrictionOrder<RootType, SelectType2> withSelect2(Selection<SelectType2> select2) {
		return RootSelectRestrictionOrder.of(root, select2, predicate, order);
	}
	
	
	public static <RT,ST> CriteriaBuilderEffect<Triple<Expression<?>,Selection<ST>,List<Order>>> toCriteriaGenerator(RootSelectRestrictionOrder<RT, ST> r) {
		return CriteriaBuilderEffect.lift((cb,cq) -> {
			cq.where(r.predicate);
			return Triple.of(r.root, r.select, r.order);
		});
	}
}
