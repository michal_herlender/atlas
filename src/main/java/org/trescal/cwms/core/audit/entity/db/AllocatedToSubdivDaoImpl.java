package org.trescal.cwms.core.audit.entity.db;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.audit.Allocated_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivDao;

public abstract class AllocatedToSubdivDaoImpl<Entity extends Allocated<Subdiv>, Identifier extends Serializable>
extends AllocatedDaoImpl<Subdiv, Entity, Identifier>
implements AllocatedDao<Subdiv, Entity, Identifier> {
	
	@Autowired
	private CompanyDao companyDao;
	@Autowired
	private SubdivDao subdivDao;
	
	@Override
	protected Class<Subdiv> getOrgLevelType() {
		return Subdiv.class;
	}
	
	/**
	 * @deprecated
	 * Use JPA 2
	 */
	@Override
	public Criterion getSubdivCrit(Subdiv subdiv) {
		return this.getOrganisationCrit(subdiv);
	}
	
	@Override
	public List<Entity> getAllByCompany(Company allocatedCompany) {
		return getResultList(cb -> {
			CriteriaQuery<Entity> cq = cb.createQuery(getEntity());
			Root<Entity> root = cq.from(getEntity());
			Join<Entity, Subdiv> subdiv = root.join("organisation", JoinType.INNER);
			cq.where(cb.equal(subdiv.get(Subdiv_.comp), allocatedCompany));
			return cq;
		});
	}
	
	/**
	 * @deprecated
	 * Use JPA 2
	 */
	@Override
	public Criterion getSubdivCrit(Integer subdivId) {
		Subdiv subdiv = subdivDao.find(subdivId);
		return this.getOrganisationCrit(subdiv);
	}
	
	@Override
	public List<Entity> getAllBySubdiv(Subdiv allocatedSubdiv) {
		return getResultList(cb -> {
			CriteriaQuery<Entity> cq = cb.createQuery(getEntity());
			Root<Entity> root = cq.from(getEntity());
			cq.where(cb.equal(root.get(Allocated_.organisation), allocatedSubdiv));
			return cq;
		});
	}
	
	/**
	 * @deprecated
	 * Use JPA 2
	 */
	@Override
	public Criterion getCompanyCrit(Company company) {
		List<Subdiv> subdivs = company.getSubdivisions().stream().filter(Subdiv::isActive).collect(Collectors.toList());
		Disjunction result = Restrictions.disjunction();
		for(Subdiv subdiv : subdivs) result.add(this.getSubdivCrit(subdiv));
		return result;
	}
	
	/**
	 * @deprecated
	 * Use JPA 2
	 */
	@Override
	public Criterion getCompanyCrit(Integer coId) {
		return getCompanyCrit(companyDao.find(coId));
	}
}