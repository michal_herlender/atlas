package org.trescal.cwms.core.audit;

public class AuditConstants
{
	// action type values
	public enum LOGACTIONTYPE
	{
		LOG_ACTIONTYPE_DELETE("DELETE"), LOG_ACTIONTYPE_UPDATE("UPDATE");

		String sqlValue;

		private LOGACTIONTYPE(String sqlValue)
		{
			this.sqlValue = sqlValue;
		}

		public String getSqlValue()
		{
			return this.sqlValue;
		}

		public void setSqlValue(String sqlValue)
		{
			this.sqlValue = sqlValue;
		}
	}

	// column names of fields set in subclasses of Auditable
	// Deprecated 2017-08-16 GB as still defined in the inactive CWMS auditing tool
	@Deprecated
	public static final String AUDITABLE_USERID_COLUMN = "log_userid";
	@Deprecated
	public static final String AUDITABLE_LASTMODIFIED_COLUMN = "log_lastmodified";

	// prefixes for log tables + triggers
	public static final String LOG_TABLE_PREFIX = "LOG_";
	public static final String LOG_TRIGGER_UPDATE_PREFIX = "log_update_";
	public static final String LOG_TRIGGER_DELETE_PREFIX = "log_delete_";

	// column names in LOG tables
	public static final String LOG_TABLE_COLUMN_USERID = "log_userid";
	public static final String LOG_TABLE_COLUMN_VALIDFROM = "log_validfrom";
	public static final String LOG_TABLE_COLUMN_VALIDTO = "log_validto";
	public static final String LOG_TABLE_COLUMN_ACTIONTYPE = "log_actiontype";

	public static final String LOG_LINKED_VIEW = "LINKED_";
}
