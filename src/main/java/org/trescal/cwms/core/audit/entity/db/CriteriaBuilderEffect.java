package org.trescal.cwms.core.audit.entity.db;

import java.util.function.BiFunction;
import java.util.function.Function;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import lombok.Value;

@Value(staticConstructor = "lift")
public class CriteriaBuilderEffect<EffectType> {

	BiFunction<CriteriaBuilder, CriteriaQuery<?>, EffectType> effect;
		
	public <Effect2> CriteriaBuilderEffect<Effect2> map(Function<EffectType, Effect2> f) {
		return CriteriaBuilderEffect.lift((cb, cq) -> f.apply(this.unsafeApply(cb, cq)) );
	}
	
	public <Effect2> CriteriaBuilderEffect<Effect2> flatMap(Function<EffectType, CriteriaBuilderEffect<Effect2>> f){
		return CriteriaBuilderEffect.lift((cb,cq) -> f.apply(this.unsafeApply(cb, cq)).unsafeApply(cb, cq));
	}
	
	public EffectType unsafeApply(CriteriaBuilder cb, CriteriaQuery<?> cq) {
		return effect.apply(cb,cq);
	}
}
