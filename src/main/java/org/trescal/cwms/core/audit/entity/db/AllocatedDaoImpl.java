package org.trescal.cwms.core.audit.entity.db;

import java.io.Serializable;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.organisation.OrganisationLevel;

/**
 * @author Egbert Fohry
 *
 * @param <OrgLevel> type of the organization level
 * @param <Entity> entity type for the DAO
 * @param <Identifier> identifier type of the entity
 */
public abstract class AllocatedDaoImpl<OrgLevel extends OrganisationLevel, Entity extends Allocated<OrgLevel>, Identifier extends Serializable>
extends BaseDaoImpl<Entity, Identifier>
implements AllocatedDao<OrgLevel, Entity, Identifier> {
	
	/**
	 * @deprecated
	 * Use JPA 2
	 */
	protected Criterion getOrganisationCrit(OrgLevel organisation) {
		return Restrictions.eq("organisation", organisation);
	}
	
	protected abstract Class<OrgLevel> getOrgLevelType();
	
	public List<OrgLevel> getAllOrganisationsWith() {
		return getResultList(cb -> {
			CriteriaQuery<OrgLevel> cq = cb.createQuery(getOrgLevelType());
			Root<Entity> root = cq.from(getEntity());
			cq.where(cb.isNotNull(root.get("organisation")));
			cq.distinct(true);
			cq.select(root.get("organisation"));
			return cq;
		});
	}
}