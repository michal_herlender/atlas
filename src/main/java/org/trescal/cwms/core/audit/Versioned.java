package org.trescal.cwms.core.audit;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.trescal.cwms.core.company.entity.contact.Contact;

/**
 * Should be extended by entities for optimistic locking.
 * 
 * @author Egbert.Fohry
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Versioned {
	
	private Timestamp lastModified;
	private Contact lastModifiedBy;
	
	@Version
	@Column(name="lastModified", nullable=false, columnDefinition="datetime2")
	public Timestamp getLastModified() {
		return this.lastModified;
	}
	
	public void setLastModified(Timestamp lastModified) {
		this.lastModified = lastModified;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="lastModifiedBy")
	@LastModifiedBy
	public Contact getLastModifiedBy() {
		return lastModifiedBy;
	}
	
	public void setLastModifiedBy(Contact lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
}