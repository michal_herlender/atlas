package org.trescal.cwms.core.audit.entity.db;

import org.trescal.cwms.core.audit.entity.AuditTriggerSwitch;

public interface AuditTriggerSwitchService extends BaseService<AuditTriggerSwitch, Integer>
{
	AuditTriggerSwitch findAuditTriggerSwitch();
	
	void updateTriggerSwitch(boolean enabled);
	
	boolean getEnabled();
}