package org.trescal.cwms.core.audit.entity.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Component("springSecurityAuditorAware")
public class SpringSecurityAuditorAware implements AuditorAware<Contact> {

	@Autowired
	private UserService userService;
	@Value("#{props['cwms.users.automatedusername']}")
	private String autoUserName;

	private static final Logger logger = LoggerFactory.getLogger(SpringSecurityAuditorAware.class);

	@NotNull
	@Override
	public Optional<Contact> getCurrentAuditor() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String userName = autoUserName;
		if (auth != null)
			try {
				userName = (String) auth.getPrincipal();
			} catch (ClassCastException ex) {
				logger.error("Spring security principal has wrong type.");
			}
		logger.trace("User name: " + userName);
		User user = userService.get(userName);
		Contact contact = user != null ? user.getCon() : null;
		if (contact == null)
			logger.error("User/contact not found");
		return Optional.ofNullable(contact);
	}

}
