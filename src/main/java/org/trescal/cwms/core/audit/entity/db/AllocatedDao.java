package org.trescal.cwms.core.audit.entity.db;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.organisation.OrganisationLevel;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

public interface AllocatedDao<OrgLevel extends OrganisationLevel, Entity extends Allocated<OrgLevel>, Identifier extends Serializable>
extends BaseDao<Entity, Identifier> {
	
	Criterion getSubdivCrit(Subdiv subdiv);
	
	Criterion getSubdivCrit(Integer subdivId);
	
	Criterion getCompanyCrit(Company company);
	
	Criterion getCompanyCrit(Integer coId);
	
	List<Entity> getAllBySubdiv(Subdiv subdiv);
	
	List<Entity> getAllByCompany(Company company);
	
	List<OrgLevel> getAllOrganisationsWith();
}