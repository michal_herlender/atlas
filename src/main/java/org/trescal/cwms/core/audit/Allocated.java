package org.trescal.cwms.core.audit;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.organisation.OrganisationLevel;

@MappedSuperclass
public abstract class Allocated<OrgLevel extends OrganisationLevel> extends Versioned {
	
	protected OrgLevel organisation;
	
	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="orgid", nullable=false)
	public OrgLevel getOrganisation() {
		return organisation;
	}
	
	public void setOrganisation(OrgLevel organisation) {
		this.organisation = organisation;
	}
}