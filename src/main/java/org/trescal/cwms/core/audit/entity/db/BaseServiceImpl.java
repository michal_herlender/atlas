package org.trescal.cwms.core.audit.entity.db;

import java.io.Serializable;
import java.util.List;

public abstract class BaseServiceImpl<Entity, Identifier extends Serializable> implements BaseService<Entity, Identifier> {

	private static final Integer INTEGER_ZERO = new Integer(0);
	private static final Long LONG_ZERO = new Long(0);
	/*
	 * This bean is not autowired as Spring won't initialize it correctly, even
	 * when defined as its own generic type.
	 * Subclasses must autowire their own Dao and implement the afterPropertiesSet() 
	 * method of the InitializingBean interface and set the baseDao explicitly using 
	 * setBaseDao() with the actual concrete base class.
	 */
	abstract protected BaseDao<Entity, Identifier> getBaseDao();
	
	@Override
	public void evict(Entity object) {
		this.getBaseDao().detach(object);
	}
	
	@Override
	public Entity get(Identifier id) {
		return this.getBaseDao().find(id);
	}
	
	@Override
	public Entity getReference(Identifier id) {
		return this.getBaseDao().getReference(id);
	}
	
	@Override
	public Entity getReferenceOrNull(Identifier id) {
		Entity result = null;
		if (id != null && !id.equals(INTEGER_ZERO) && !id.equals(LONG_ZERO)) {
			result = this.getBaseDao().getReference(id);
		}
		return result;
	}
	
	@Override
	public List<Entity> getAll() {
		return this.getBaseDao().findAll();
	}

	@Override
	public void delete(Entity object) {
		this.getBaseDao().remove(object);
	}
	
	@Override
	public Entity merge(Entity object) {
		return this.getBaseDao().merge(object);
	}
	
	@Override
	public void save(Entity object) {
		this.getBaseDao().persist(object);
	}

	@Override
	@Deprecated
	public void update(Entity object) {
		this.getBaseDao().update(object);
	}
}