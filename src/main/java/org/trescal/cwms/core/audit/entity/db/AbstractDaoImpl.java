package org.trescal.cwms.core.audit.entity.db;

import org.apache.commons.lang3.tuple.Triple;
import org.hibernate.Session;
import org.hibernate.engine.spi.SessionImplementor;
import org.springframework.beans.factory.annotation.Value;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.datatables.DatatableInput;
import org.trescal.cwms.datatables.DatatableOutput;

import javax.persistence.*;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SetAttribute;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

public abstract class AbstractDaoImpl {

    @PersistenceContext(unitName = "entityManagerFactory")
    private EntityManager entityManager;
    @Value("#{props['locale.country']}")
    private String defaultCountry;
    @Value("#{props['locale.lang']}")
    private String defaultLanguage;

    protected EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * @deprecated use JPA 2 CriteriaQuery with EntityManager
     */
    protected Session getSession() {
        return entityManager.unwrap(SessionImplementor.class);
    }

    private Locale defaultLocale() {
        return new Locale(defaultLanguage, defaultCountry);
    }

    protected <T> Stream<T> getResultStream(Function<CriteriaBuilder, CriteriaQuery<T>> queryFunction) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        TypedQuery<T> query = getEntityManager().createQuery(queryFunction.apply(cb));
        return query.getResultStream();
    }

    protected <T> List<T> getResultList(Function<CriteriaBuilder, CriteriaQuery<T>> queryFunction) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        TypedQuery<T> query = getEntityManager().createQuery(queryFunction.apply(cb));
        return query.getResultList();
    }

    protected <T> List<T> getResultList(Function<CriteriaBuilder, CriteriaQuery<T>> queryFunction, Integer firstResult,
                                        Integer maxResults) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        TypedQuery<T> query = getEntityManager().createQuery(queryFunction.apply(cb));
        if (firstResult != null)
            query.setFirstResult(firstResult);
        if (maxResults != null)
            query.setMaxResults(maxResults);
        return query.getResultList();
    }

    protected <T> T getSingleResult(Function<CriteriaBuilder, CriteriaQuery<T>> queryFunction) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		TypedQuery<T> query = getEntityManager().createQuery(queryFunction.apply(cb));
		return query.getSingleResult();
	}

	protected <T> T getSingleResult(Function<CriteriaBuilder, CriteriaQuery<T>> queryFunction,
			EntityGraph<T> entityGraph) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		TypedQuery<T> query = getEntityManager().createQuery(queryFunction.apply(cb));
		query.setHint("javax.persistence.fetchgraph", entityGraph);
		return query.getSingleResult();
	}

	protected <T> Optional<T> getFirstResult(Function<CriteriaBuilder, CriteriaQuery<T>> queryFunction) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		TypedQuery<T> query = getEntityManager().createQuery(queryFunction.apply(cb));
		query.setMaxResults(1);
		try {
			T firstResult = query.getSingleResult();
			// use ofNullable for the case, that there is a database record present, but null
			return Optional.ofNullable(firstResult);
		} catch (NoResultException nre) {
			return Optional.empty();
		}
	}

	/**
	 * Method where it's possible to specify whether result set is distinct, when required
	 * 
	 * Completes a given paged result set. Hint: if root entity equals result
	 * entity, then the selection part of the queryGenerator can be null.
	 * 
	 * @param pagedResultSet   the paged result set, which should be completed
	 * @param resultEntityType class of the results
	 * @param queryGenerator   function, completes from and where part of the query
	 *                         and delivers an expression for counting as well as
	 *                         select and order by part
	 */
	protected <ResultEntity> void completePagedResultSet(PagedResultSet<ResultEntity> pagedResultSet,
			Class<ResultEntity> resultEntityType, CriteriaQueryGenerator<ResultEntity> queryGenerator, Boolean distinct) {
		pagedResultSet.setResultsCount(getCount(queryGenerator,distinct));
		pagedResultSet.setResults(getRecords(queryGenerator, resultEntityType, pagedResultSet.getStartResultsFrom(), pagedResultSet.getResultsPerPage(),distinct));
	}

	/**
	 * Simple method where SELECT DISTINCT is not required or not possible; 
	 * use expanded syntax when distinct=true is required
	 * 
	 * Completes a given paged result set. Hint: if root entity equals result
	 * entity, then the selection part of the queryGenerator can be null.
	 */
	protected <ResultEntity> void completePagedResultSet(PagedResultSet<ResultEntity> pagedResultSet,
														 Class<ResultEntity> resultEntityType, CriteriaQueryGenerator<ResultEntity> queryGenerator) {
		completePagedResultSet(pagedResultSet,resultEntityType,queryGenerator,false);
	}
	protected <OutputColumn, InputData extends DatatableInput> void completeDataTablesOutput(
			DatatableOutput<OutputColumn> dataTableOutput, InputData dataTableInput,
			Class<OutputColumn> resultEntityType, CriteriaQueryGenerator<OutputColumn> queryGenerator,
			Function<CriteriaBuilder, Function<CriteriaQuery<Long>, Expression<?>>> totalQueryGenerator) {

		dataTableOutput.setRecordsTotal(getSingleResult(cb -> {
			CriteriaQuery<Long> totalCount = cb.createQuery(Long.class);
			Expression<?> totalCountExpression = totalQueryGenerator.apply(cb).apply(totalCount);
			totalCount.select(cb.count(totalCountExpression));
			return totalCount;
		}).intValue());

		dataTableOutput.setRecordsFiltered(getCount(queryGenerator));
		dataTableOutput.setData(getRecords(queryGenerator, resultEntityType, dataTableInput.getStart(), dataTableInput.getLength()));
	}

	protected <ResultEntity> Integer getCount(CriteriaQueryGenerator<ResultEntity> queryGenerator){
		return getCount(queryGenerator,false);
	}

	protected <ResultEntity> Integer getCount(CriteriaQueryGenerator<ResultEntity> queryGenerator,Boolean distinct) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> count = cb.createQuery(Long.class);
			Expression<?> countExpression = queryGenerator.apply(cb).apply(count).getLeft();
			count.select(!distinct?cb.count(countExpression):cb.countDistinct(countExpression));
			count.getRoots().forEach(root -> root.getFetches().clear());
			return count;
		}).intValue();
	}

	private <ResultEntity> Collection<ResultEntity> getRecords(CriteriaQueryGenerator<ResultEntity> queryGenerator,
															   Class<ResultEntity> resultEntityType, Integer start, Integer length) {
		return getRecords(queryGenerator,resultEntityType,start,length,false);
	}
	private <ResultEntity> Collection<ResultEntity> getRecords(CriteriaQueryGenerator<ResultEntity> queryGenerator,
			Class<ResultEntity> resultEntityType, Integer start, Integer length, Boolean distinct) {
		return getResultList(cb -> {
			CriteriaQuery<ResultEntity> cq = cb.createQuery(resultEntityType);
			cq.distinct(distinct);
			Triple<Expression<?>, Selection<ResultEntity>, List<Order>> queryParts = queryGenerator.apply(cb).apply(cq);
			cq.select(queryParts.getMiddle());
			cq.orderBy(queryParts.getRight());
			return cq;
		}, start, length);
	}

	protected <T> Expression<String> joinTranslation(CriteriaBuilder cb, From<?, ? extends T> path,
			SetAttribute<T, Translation> translationAttribute, Locale locale) {
		Join<? extends T, Translation> translation = path.join(translationAttribute, JoinType.LEFT);
		translation.on(cb.equal(translation.get(Translation_.locale), locale));
		Join<? extends T, Translation> defaultTranslation = path.join(translationAttribute, JoinType.LEFT);
		defaultTranslation.on(cb.equal(defaultTranslation.get(Translation_.locale), defaultLocale()));
		return cb.coalesce(translation.get(Translation_.translation), defaultTranslation.get(Translation_.translation));
	}

	protected Expression<String> stringAggregation(CriteriaBuilder cb, Expression<?> expression, String delimiter) {
		Expression<String> firstParam = cb.function("castToNVarChar", String.class, expression);
		return cb.function("string_agg", String.class, firstParam, cb.literal(delimiter));
	}
}