package org.trescal.cwms.core.audit.entity.db;

import java.io.Serializable;
import java.util.List;

public interface BaseService<Entity, Identifier extends Serializable> {

	void evict(Entity object);

	Entity get(Identifier id);

	/**
	 * Returns getReference() from entity manager - tests existence and returns 
	 * proxy to entity, otherwise uninitialized except for the Identifier field.
	 * 
	 * Less DB overhead than entity retrieval, but to prevent lazy loading, 
	 * should be only used when the other entity fields will not be accessed, 
	 * i.e for use in creates / updates. 
	 */
	Entity getReference(Identifier id);

	/**
	 * Returns null if the provided id is 0 (Integer/Long) or null, otherwise 
	 * delegates to getReference() - see comments there.
	 * 
	 * Reduces boilerplate zero/null testing for update/create methods. 
	 */
	Entity getReferenceOrNull(Identifier id);
	
	List<Entity> getAll();

	void delete(Entity object);

	Entity merge(Entity object);

	void save(Entity object);

	/**
	 * @deprecated Use merge in the right way
	 */
	void update(Entity object);
}