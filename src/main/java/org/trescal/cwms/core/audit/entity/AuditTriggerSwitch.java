package org.trescal.cwms.core.audit.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "audittriggerswitch")
public class AuditTriggerSwitch
{
	private boolean enabled;
	private int id;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Column(name = "enabled", nullable = false, columnDefinition="bit")
	public boolean isEnabled()
	{
		return this.enabled;
	}

	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}

	public void setId(int id)
	{
		this.id = id;
	}
}
