package org.trescal.cwms.core.audit.entity.db;

import java.io.Serializable;
import java.util.List;

public interface BaseDao<Entity, Identifier extends Serializable> {

	Entity find(Identifier id);
	
	List<Entity> findAll();
	
	Entity getReference(Identifier id);
	
	void remove(Entity object);
	
	void detach(Entity object);
	
	Entity merge(Entity object);
	
	List<Entity> mergeAll(List<Entity> objects);
	
	void persist(Entity object);
	
	/**
	 * @deprecated
	 * Use persist and merge in the right way
	 */
	void saveOrUpdate(Entity object);
	
	/**
	 * @deprecated
	 * Use mergeAll in the right way
	 */
	void saveOrUpdateAll(List<Entity> objects);
	
	/**
	 * @deprecated
	 * Use merge in the right way
	 */
	void update(Entity object);

}
