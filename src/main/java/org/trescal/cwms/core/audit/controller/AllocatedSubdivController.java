package org.trescal.cwms.core.audit.controller;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.CrocodileAuthenticationToken;
import org.trescal.cwms.core.login.GrantedAuthorityComparator;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultTimeZoneType;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.userright.enums.Permission;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class AllocatedSubdivController {

	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;
	@Autowired
	private BusinessDetailsService businessDetailsService;
	@Autowired
	private SystemDefaultTimeZoneType systemDefaultTimeZoneType;

	@RequestMapping(value = "/changeallocatedsubdiv", method = RequestMethod.POST)
	public ModelAndView onChange(@RequestParam int allocatedSubdivid,
								 @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName, HttpServletRequest request) {
		Subdiv subdiv = subdivService.get(allocatedSubdivid);
		HttpSession httpSession = request.getSession();
		httpSession.setAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV, new SubdivKeyValue(subdiv));
		httpSession.setAttribute(Constants.SESSION_ATTRIBUTE_COMPANY, new CompanyKeyValue(subdiv.getComp()));
		httpSession.setAttribute(Constants.SESSION_ATTRIBUTE_BUSINESS_DETAILS,
			businessDetailsService.getAllBusinessDetails(subdiv));
		val timeZone = systemDefaultTimeZoneType.getGenericValueHierachical(Scope.SUBDIV, subdiv.getSubdivid(), subdiv.getComp())._2();
		httpSession.setAttribute(Constants.SESSION_ATTRIBUTE_TIME_ZONE, timeZone);
		User user = userService.get(userName);
		SecurityContext securityContext = SecurityContextHolder.getContext();
		Authentication authentication = securityContext.getAuthentication();
		SortedSet<GrantedAuthority> authorities = new TreeSet<>(new GrantedAuthorityComparator());
		List<Permission> permissions = user.getUserRoles().stream().filter(ur -> ur.getOrganisation().equals(subdiv))
			.flatMap(ur -> ur.getRole().getGroups().stream().flatMap(pg -> pg.getPermissions().stream()))
			.collect(Collectors.toList());
		authorities.addAll(permissions);
		if (!permissions.isEmpty()) authorities.add(new SimpleGrantedAuthority("ROLE_INTERNAL"));
		securityContext.setAuthentication(new CrocodileAuthenticationToken(authentication.getPrincipal(),
			authentication.getCredentials(), authorities));
		return new ModelAndView(new RedirectView("home.htm"));
	}
}