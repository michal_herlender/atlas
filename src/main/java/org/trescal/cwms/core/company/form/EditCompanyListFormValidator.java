package org.trescal.cwms.core.company.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companylist.CompanyList;
import org.trescal.cwms.core.company.entity.companylist.db.CompanyListService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class EditCompanyListFormValidator extends AbstractBeanValidator {
	
	@Autowired
	CompanyService companyService;
	@Autowired
	CompanyListService companyListService;

	@Override
	public boolean supports(Class<?> arg0) {
		return EditCompanyListForm.class.isAssignableFrom(arg0);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			EditCompanyListForm form = (EditCompanyListForm) target;
			CompanyList companyList = companyListService.findCompanyList(form.getCompanylistId());
			
			// the name of the list is unique for the logged-in business company
			if (!companyList.getName().equals(form.getName()) && 
					companyListService.getByName(form.getName(), companyList.getOrganisation()) != null) {
				errors.rejectValue("name", "companylist.name.already.exist", 
						"This name already exists, please enter another name!");
			}
		}
	}

}
