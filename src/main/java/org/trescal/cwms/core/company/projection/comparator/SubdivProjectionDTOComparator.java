package org.trescal.cwms.core.company.projection.comparator;

import java.util.Comparator;

import org.trescal.cwms.core.company.projection.SubdivProjectionDTO;

public class SubdivProjectionDTOComparator implements Comparator<SubdivProjectionDTO> {

	@Override
	public int compare(SubdivProjectionDTO subdivDto1, SubdivProjectionDTO subdivDto2) {
		// To start, just compares by name and then id; could also compare first by company name (company DTO always present)
		int result = subdivDto1.getSubname().compareTo(subdivDto2.getSubname());
		if (result == 0) {
			result = Integer.compare(subdivDto1.getSubdivid(), subdivDto2.getSubdivid());
		}
		return result;
	} 

}
