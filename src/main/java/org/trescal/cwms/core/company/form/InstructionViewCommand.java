package org.trescal.cwms.core.company.form;

import java.util.ArrayList;
import java.util.List;

import org.trescal.cwms.core.company.dto.InstructionLinkDTO;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionEntity;

public class InstructionViewCommand {
	private InstructionEntity instructionEntity;
	private int coid;
	private int subdivid;
	private int personid;
	private String companyName;
	private String subdivName;
	private String contactName;
	private String entityName;	// One of company, subdiv, or contact
	private int companyInstructionCount;
	private int subdivInstructionCount;
	private int contactInstructionCount;
	private List<InstructionLinkDTO> dtos;
	public InstructionViewCommand() {
		dtos = new ArrayList<InstructionLinkDTO>();
	}
	public InstructionEntity getInstructionEntity() {
		return instructionEntity;
	}
	public int getCoid() {
		return coid;
	}
	public int getSubdivid() {
		return subdivid;
	}
	public int getPersonid() {
		return personid;
	}
	public String getCompanyName() {
		return companyName;
	}
	public String getSubdivName() {
		return subdivName;
	}
	public String getContactName() {
		return contactName;
	}
	public void setInstructionEntity(InstructionEntity instructionEntity) {
		this.instructionEntity = instructionEntity;
	}
	public void setCoid(int coid) {
		this.coid = coid;
	}
	public void setSubdivid(int subdivid) {
		this.subdivid = subdivid;
	}
	public void setPersonid(int personid) {
		this.personid = personid;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public void setSubdivName(String subdivName) {
		this.subdivName = subdivName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public List<InstructionLinkDTO> getDtos() {
		return dtos;
	}
	public void addDto(InstructionLinkDTO dto) {
		this.dtos.add(dto);
	}
	public int getCompanyInstructionCount() {
		return companyInstructionCount;
	}
	public int getSubdivInstructionCount() {
		return subdivInstructionCount;
	}
	public int getContactInstructionCount() {
		return contactInstructionCount;
	}
	public void setCompanyInstructionCount(int companyInstructionCount) {
		this.companyInstructionCount = companyInstructionCount;
	}
	public void setSubdivInstructionCount(int subdivInstructionCount) {
		this.subdivInstructionCount = subdivInstructionCount;
	}
	public void setContactInstructionCount(int contactInstructionCount) {
		this.contactInstructionCount = contactInstructionCount;
	}
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public int getTotalCount() {
		return this.companyInstructionCount + this.subdivInstructionCount + this.contactInstructionCount;
	}
}
