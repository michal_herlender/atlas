package org.trescal.cwms.core.company.entity.companygroup.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.admin.form.CompanyGroupForm;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.companygroup.CompanyGroup;
import org.trescal.cwms.core.company.entity.companygroup.dto.CompanyGroupUsageDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Service
public class CompanyGroupServiceImpl extends BaseServiceImpl<CompanyGroup, Integer> implements CompanyGroupService {

	@Autowired
	private CompanyGroupDao baseDao;
	@Autowired
	private MessageSource messages;
	
	@Override
	protected BaseDao<CompanyGroup, Integer> getBaseDao() {
		return baseDao;
	}

	@Override
	public Long countByName(String name, Integer idToExclude) {
		return baseDao.countByName(name, idToExclude);
	}

	@Override
	public Integer createCompanyGroup(CompanyGroupForm form) {
		CompanyGroup group = new CompanyGroup();
		group.setActive(form.getActive());
		group.setGroupName(form.getGroupName());
		this.save(group);
		return group.getId();
	}

	@Override
	public void editCompanyGroup(CompanyGroupForm form) {
		CompanyGroup group = this.get(form.getId());
		group.setActive(form.getActive());
		group.setGroupName(form.getGroupName());
		// Automatically saved via JPA cascade
	}
	
	@Override
	public List<KeyValueIntegerString> getActiveDTOList(CompanyGroup currentCompanyGroup, Locale locale) {
		List<KeyValueIntegerString> results = new ArrayList<>();
		
		String notApplicable = this.messages.getMessage("company.nonespecified", null, "None Specified", locale);
		results.add(new KeyValueIntegerString(0, notApplicable));
		
		if (currentCompanyGroup != null && !currentCompanyGroup.getActive()) {
			results.add(new KeyValueIntegerString(currentCompanyGroup.getId(), currentCompanyGroup.getGroupName()));
		}
		results.addAll(this.getDTOList(true));
		
		return results;
	}
	
	@Override
	public List<KeyValueIntegerString> getDTOList(Boolean activeGroups) {
		return this.baseDao.getDTOList(activeGroups);
	}

	@Override
	public List<CompanyGroupUsageDTO> getUsageDTOList() {
		return this.baseDao.getUsageDTOList();
	}
}
