package org.trescal.cwms.core.company.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditation;
import org.trescal.cwms.core.company.entity.companyaccreditation.db.CompanyAccreditationService;
import org.trescal.cwms.core.company.entity.supplier.Supplier;
import org.trescal.cwms.core.company.entity.supplier.db.SupplierService;
import org.trescal.cwms.core.company.form.CompanyAccreditationCommand;
import org.trescal.cwms.core.company.form.CompanyAccreditationForm;
import org.trescal.cwms.core.company.form.CompanyAccreditationFormValidator;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.system.Constants;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class CompanyAccreditationController {
	@Autowired
	private CompanyAccreditationService compAccServ;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private SupplierService supplierService;
	@Autowired
	private CompanyAccreditationFormValidator validator;
	
	private final Log logger = LogFactory.getLog(CompanyAccreditationController.class); 
	
	// Model attribute keys
	public static final String COMMAND = "command";
	public static final String FORM = "form";
	public static final String VIEW_NAME = "/trescal/core/company/companyaccreditation";
	public static final String URL = "/editcompanyaccreditation.htm";

	@InitBinder(FORM)
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
	
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	@ModelAttribute(COMMAND)
	public CompanyAccreditationCommand commandObject(
		@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue businessDTO,
		@RequestParam(value = "coid") Integer coid) {
		Company company = compServ.get(coid);
		Company businessCompany = compServ.get(businessDTO.getKey());
		logger.debug("Business company has id " + businessCompany.getCoid());
		logger.debug("Company has id " + company.getCoid());
		List<Supplier> activeSuppliers = supplierService.getActiveSuppliers();

		CompanyAccreditationCommand command = new CompanyAccreditationCommand();
		command.setBusinessCompany(businessCompany);
		command.setCompany(company);
		command.setSuppliers(activeSuppliers);
		
		return command;
		
	}
	
	@ModelAttribute(FORM)
	public CompanyAccreditationForm formBackingObject(
			@ModelAttribute(COMMAND) CompanyAccreditationCommand command) {
		CompanyAccreditationForm form = new CompanyAccreditationForm();
		for (Supplier supplier : command.getSuppliers()) {
			Integer id = supplier.getSupplierid();
			CompanyAccreditation companyAccreditation = compAccServ.getForBusinessCompany(command.getCompany(), command.getBusinessCompany(), supplier);
			if (companyAccreditation != null) {
				form.addSupplierId(supplier.getSupplierid());
				LocalDate date = companyAccreditation.getExpiryDate();
				String reason = companyAccreditation.getReason();
				if (date != null) {
					form.addCompanyAccreditationDate(id, date);
				}
				if (reason != null) {
					form.addCompanyAccreditationReason(id, reason);
				}
			}
		}
		return form;
	}
	
	@RequestMapping(value=URL, method=RequestMethod.GET)
	public String referenceData() {
		return VIEW_NAME;
	}
	
	@RequestMapping(value=URL, method=RequestMethod.POST)
	public ModelAndView onSubmit(
			@ModelAttribute(COMMAND) CompanyAccreditationCommand command,
			@Valid @ModelAttribute(FORM) CompanyAccreditationForm form,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) return new ModelAndView(VIEW_NAME);
		Company company = command.getCompany();
		Company businessCompany = command.getBusinessCompany();
		/*
		 * Loop over all the possible accreditations and add new / update 
		 * existing accreditations and remove any that have been unchecked
		 */
		for (Supplier supplier : command.getSuppliers()) {
			int supplierId = supplier.getSupplierid();
			LocalDate expiryDate = form.getCompanyAccreditationDate().get(supplierId);
			String reason = form.getCompanyAccreditationReason().get(supplierId);
			CompanyAccreditation ca = this.compAccServ.getForBusinessCompany(company, businessCompany, supplier);
			if (form.getSupplierIds().contains(supplierId)) {
				// Selected by user, we want to create / update the accreditation
				if (ca == null) {
					ca = new CompanyAccreditation();
					ca.setCompany(company);
					ca.setOrganisation(businessCompany);
					ca.setSupplier(supplier);
				}
				// Update existing
				ca.setExpiryDate(expiryDate);
				ca.setReason(reason);
				this.compAccServ.saveOrUpdateCompanyAccreditation(ca);
			}
			else {
				// Not selected by user, we want to delete the accreditation, if present
				if (ca != null) {
					this.compAccServ.deleteCompanyAccreditation(ca);
				}	
			}	
		}
		return new ModelAndView(new RedirectView("/viewcomp.htm?loadtab=accred-tab&coid="+company.getCoid(), true));
	}
}
