package org.trescal.cwms.core.company.form;

import org.trescal.cwms.core.account.entity.bankdetail.BankDetail;
import org.trescal.cwms.core.account.entity.companyaccount.CompanyAccount;
import org.trescal.cwms.core.account.entity.companyaccount.CompanyAccountSource;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;

import lombok.Data;

@Data
public class CompanyAccountForm
{
	private CompanyAccount account;
	private Address accountAddress;
	private Contact accountContact;
	private Integer addressid;
	private CompanyAccountSource addressSource;
	private BankDetail bankDetails;
	private Company company;
	private CompanyAccountSource contactSource;
	private Integer countryId;
	private boolean includeBankDetails;
	private boolean newAccount;
	private Integer personid;
	private String title;
}