package org.trescal.cwms.core.company.dto;

import org.trescal.cwms.spring.model.KeyValue;

public class AddressKeyValue extends KeyValue<Integer, String> {

	public AddressKeyValue(Integer key, String value) {
		super(key, value);
	}
}