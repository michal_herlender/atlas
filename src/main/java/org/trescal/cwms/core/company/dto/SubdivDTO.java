package org.trescal.cwms.core.company.dto;

import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

public class SubdivDTO {

	private boolean active;
	private String company;
	private String defaultAddress;
	private String defaultContact;
	
	public SubdivDTO(Subdiv subdiv) {
		this.active = subdiv.isActive();
		this.company = subdiv.getComp().getConame();
		this.defaultAddress = subdiv.getDefaultAddress() == null ? "" : subdiv.getDefaultAddress().getAddr1();
		this.defaultContact = subdiv.getDefaultContact() == null ? "" : subdiv.getDefaultContact().getName();
	}
	
	public boolean isActive() {
		return active;
	}
	
	public String getCompany() {
		return company;
	}
	
	public String getDefaultAddress() {
		return defaultAddress;
	}
	
	public String getDefaultContact() {
		return defaultContact;
	}
}