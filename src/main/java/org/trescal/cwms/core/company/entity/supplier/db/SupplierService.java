package org.trescal.cwms.core.company.entity.supplier.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.supplier.Supplier;
import org.trescal.cwms.core.company.entity.supplier.SupplierApprovalType;

public interface SupplierService extends BaseService<Supplier, Integer>
{
	List<Supplier> getActiveSuppliers();
	
	Supplier getForApprovalType(SupplierApprovalType approvalType);
}