package org.trescal.cwms.core.company.entity.companyaccreditation.db;

import java.util.HashMap;
import java.util.List;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditation;
import org.trescal.cwms.core.company.entity.supplier.Supplier;

public class CompanyAccreditationServiceImpl implements CompanyAccreditationService
{
	CompanyAccreditationDao companyAccreditationDao;

	public void deleteCompanyAccreditation(CompanyAccreditation companyAccreditation)
	{
		this.companyAccreditationDao.remove(companyAccreditation);
	}

	public CompanyAccreditation findCompanyAccreditation(Integer supplierid, Integer coid)
	{
		return this.companyAccreditationDao.findCompanyAccreditation(supplierid, coid);
	}

	public List<CompanyAccreditation> findExpiredCompanyAccreditations()
	{
		return this.companyAccreditationDao.findExpiredCompanyAccreditations();
	}

	public List<CompanyAccreditation> getAllCompanyAccreditations()
	{
		return this.companyAccreditationDao.findAll();
	}

	public CompanyAccreditationDao getCompanyAccreditationDao()
	{
		return this.companyAccreditationDao;
	}

	public void insertCompanyAccreditation(CompanyAccreditation companyAccreditation)
	{
		this.companyAccreditationDao.persist(companyAccreditation);
	}

	public void saveOrUpdateCompanyAccreditation(CompanyAccreditation companyAccreditation)
	{
		this.companyAccreditationDao.saveOrUpdate(companyAccreditation);
	}

	public List<CompanyAccreditation> searchCompanyAccreditation(Integer coid)
	{
		return this.companyAccreditationDao.searchCompanyAccreditation(coid);
	}

	@Override
	public HashMap<Integer, CompanyAccreditation> searchCompanyAccreditations(Integer coid)
	{
		List<CompanyAccreditation> list = this.searchCompanyAccreditation(coid);

		HashMap<Integer, CompanyAccreditation> map = new HashMap<Integer, CompanyAccreditation>();
		for (CompanyAccreditation ca : list)
		{
			map.put(ca.getSupplier().getSupplierid(), ca);
		}
		return map;
	}

	public void setCompanyAccreditationDao(CompanyAccreditationDao companyAccreditationDao)
	{
		this.companyAccreditationDao = companyAccreditationDao;
	}

	public void updateCompanyAccreditation(CompanyAccreditation companyAccreditation)
	{
		this.companyAccreditationDao.update(companyAccreditation);
	}
	
	public List<CompanyAccreditation> getForBusinessCompany(Company company, Company businessCompany) {
		return this.companyAccreditationDao.getForBusinessCompany(company, businessCompany);
	}
	
	public CompanyAccreditation getForBusinessCompany(Company company, Company businessCompany, Supplier supplier) {
		return this.companyAccreditationDao.getForBusinessCompany(company, businessCompany, supplier);
	}
}
