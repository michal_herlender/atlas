package org.trescal.cwms.core.company.entity.companylistusage.db;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companylist.db.CompanyListService;
import org.trescal.cwms.core.company.entity.companylistusage.CompanyListUsage;
import org.trescal.cwms.core.company.form.EditCompanyListForm;
import org.trescal.cwms.core.login.entity.user.db.UserService;

@Service("CompanyListUsageService")
public class CompanyListUsageServiceImpl extends BaseServiceImpl<CompanyListUsage, Integer>
		implements CompanyListUsageService {

	@Autowired
	private CompanyListUsageDao companyListUsageDao;
	@Autowired
	private CompanyListService companyListService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private UserService userService;

	@Override
	protected BaseDao<CompanyListUsage, Integer> getBaseDao() {
		return companyListUsageDao;
	}

	@Override
	public List<CompanyListUsage> getUsageByCompanyList(Integer companyListId) {
		return companyListUsageDao.getUsageByCompanyList(companyListId);
	}

	@Override
	public void createNewCompanyListUsage(EditCompanyListForm form, String username) {
		CompanyListUsage newCompanyListUsage = new CompanyListUsage();
		newCompanyListUsage.setCompanyList(companyListService.findCompanyList(form.getCompanylistId()));
		newCompanyListUsage.setCompany(companyService.get(form.getUserCoid()));
		newCompanyListUsage.setLastModified(new Timestamp(System.currentTimeMillis()));
		newCompanyListUsage.setLastModifiedBy(userService.get(username).getCon());
		this.companyListUsageDao.persist(newCompanyListUsage);
	}

	@Override
	public boolean remove(Integer companyListUsageId) {
		try {
			CompanyListUsage companylistUsage = companyListUsageDao.find(companyListUsageId);
			companyListUsageDao.remove(companylistUsage);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

}
