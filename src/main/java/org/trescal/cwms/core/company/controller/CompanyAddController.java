package org.trescal.cwms.core.company.controller;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.businessarea.db.BusinessAreaService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.country.db.CountryService;
import org.trescal.cwms.core.company.form.CompanyAddForm;
import org.trescal.cwms.core.company.form.CompanyAddFormValidator;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Controller used for creating a new {@link Company}.
 */

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY })
public class CompanyAddController {
	@Autowired
	private BusinessAreaService busAreaServ;
	@Autowired
	private CompanyAddFormValidator validator;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private CountryService countryServ;
	@Autowired
	private MessageSource messages;
	@Autowired
	private UserService userService;
	@Autowired
	private SupportedCurrencyService currencyService;

	public final static String FORM_NAME = "addcompanyform";
	
	public final static String CODE_UNNAMED_SUBDIVISION = "unnamedsubdivision";
	public final static String DEFAULT_UNNAMED_SUBDIVISION = "Un-named subdivision";

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	private String getDefaultSubdivisionName(Locale locale) {
		return messages.getMessage(CODE_UNNAMED_SUBDIVISION, null, DEFAULT_UNNAMED_SUBDIVISION, locale);
	}

	@ModelAttribute(FORM_NAME)
	protected CompanyAddForm formBackingObject(Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto) {
		Company allocatedCompany = this.compServ.get(allocatedCompanyDto.getKey());
		
		CompanyAddForm acf = new CompanyAddForm();
		acf.setSubdivName(getDefaultSubdivisionName(locale));
		acf.setCompanyRole(CompanyRole.PROSPECT);
		acf.setCountryId(allocatedCompany.getCountry().getCountryid());
		acf.setCurrencyId(allocatedCompany.getCurrency().getCurrencyId());
		return acf;
	}

	@ModelAttribute("currencies")
	public List<KeyValue<Integer, String>> getCurrencies() {
		List<SupportedCurrency> currencies = this.currencyService.getAllSupportedCurrencys();
		return currencies.stream().map(c -> new KeyValue<Integer, String>(c.getCurrencyId(),
				c.getCurrencyName() + " (" + c.getCurrencySymbol() + ")")).collect(Collectors.toList());
	}

	@RequestMapping(value = "/addcompany.htm", method = RequestMethod.POST)
	public String onSubmit(Model model, Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto,
			@Valid @ModelAttribute(FORM_NAME) CompanyAddForm form, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData(model, locale);
		}
		// TODO consider adding this as selection to form 
		Contact defaultBusinessContact = this.userService.get(username).getCon();
		Company allocatedCompany = this.compServ.get(allocatedCompanyDto.getKey());
		Company company = this.compServ.createCompany(form, allocatedCompany, defaultBusinessContact);
		model.asMap().clear();
		return "redirect:viewcomp.htm?coid=" + company.getCoid();
	}

	@RequestMapping(value = "/addcompany.htm", method = RequestMethod.GET)
	protected String referenceData(Model model, Locale locale) {
		model.addAttribute("coroles", EnumSet.complementOf(EnumSet.of(CompanyRole.NOTHING)));
		model.addAttribute("businessAreas", this.busAreaServ.getAllTranslated(locale));
		model.addAttribute("countries", this.countryServ.getCountries());
		return "trescal/core/company/addcompany";
	}
}