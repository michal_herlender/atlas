package org.trescal.cwms.core.company.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Returns various JSP fragments used for dynamic selection related to company data
 * 
 * (e.g. address select), to expand to others when needed.
 * 
 * @author galen
 *
 */

@Controller @JsonController
public class CompanyFragmentController {
	@Autowired
	private AddressService addressService;
	@Autowired
	private SubdivService subdivService;
	
	@RequestMapping("addressselect.htm")
	public String addressSelect(Model model,
			@RequestParam(name="selectName", required=true) String selectName,
			@RequestParam(name="subdivId", required=true) Integer subdivId,
			@RequestParam(name="addressId", required=false, defaultValue="0") Integer addressId,
			@RequestParam(name="type", required=false, defaultValue="WITHOUT") AddressType type) {

		List<KeyValue<Integer, String>> addresses = this.addressService.getAllActiveSubdivAddressesKeyValue(subdivId, type, true);
		model.addAttribute("options", addresses);
		model.addAttribute("selectedOptionId", addressId);
		model.addAttribute("htmlSelectName", selectName);
		
		return "trescal/core/company/fragments/keyvalueselect";
	}
	
	@RequestMapping("subdivselect.htm")
	public String subdivSelect(Model model,
			@RequestParam(name="selectName", required=true) String selectName,
			@RequestParam(name="companyId", required=true) Integer companyId,
			@RequestParam(name="subdivId", required=false, defaultValue="0") Integer subdivId) {
		
		List<KeyValue<Integer, String>> subdivs = this.subdivService.getAllActiveCompanySubdivsKeyValue(companyId, true);
		model.addAttribute("options", subdivs);
		model.addAttribute("selectedOptionId", subdivId);
		model.addAttribute("htmlSelectName", selectName);
		
		return "trescal/core/company/fragments/keyvalueselect";
	}
}
