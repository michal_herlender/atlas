package org.trescal.cwms.core.company.projection;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StatusProjectionDTO {
	private Integer id;
	private String name;
	
}
