package org.trescal.cwms.core.company.entity.addresssettings.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.dto.AddressTransportOptionsComponentDTO;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedSubdiv;

public interface AddressSettingsForAllocatedSubdivDao extends BaseDao<AddressSettingsForAllocatedSubdiv, Integer> {

	public AddressSettingsForAllocatedSubdiv getBySubdiv(Integer addressId, Integer allocatedSubdivId);

	List<AddressTransportOptionsComponentDTO> getTransportOptions(Integer addressId, Locale locale);
}