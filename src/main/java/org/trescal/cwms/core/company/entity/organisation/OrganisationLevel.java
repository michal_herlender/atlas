package org.trescal.cwms.core.company.entity.organisation;

/**
 * @author Egbert.Fohry
 */
public interface OrganisationLevel
{
	Integer getId();
}