package org.trescal.cwms.core.company.dto;

import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.contact.Contact;

public class ContactDTO {

	private String name;
	private String position;
	private String telephone;
	private String fax;
	private String email;
	private boolean active;
	private String preference;
	private String addr1;
	private String addr2;
	private String addr3;
	private String town;
	private String county;
	private String postcode;
	private boolean defAddress = false;
			
	public ContactDTO(Contact contact, CompanySettingsForAllocatedCompany settings) {
		this.name = contact.getName();
		if (contact.getDefAddress() != null) {
			this.addr1 = contact.getDefAddress().getAddr1();
			this.addr2 = contact.getDefAddress().getAddr2();
			this.addr3 = contact.getDefAddress().getAddr3();
			this.town = contact.getDefAddress().getTown();
			this.county = contact.getDefAddress().getCounty();
			this.postcode = contact.getDefAddress().getPostcode();
			this.defAddress = true;
		}
		this.position = contact.getPosition();
		this.telephone = contact.getTelephone();
		this.fax = contact.getFax();
		this.email = contact.getEmail();
		this.preference = contact.getPreference();

		this.active = settings.isActive();
	}
	
	public boolean isDefAddress() {
		return defAddress;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public String getName() {
		return name;
	}
	
	public String getAddr1() {
		return addr1;
	}
	
	public String getAddr2() {
		return addr2;
	}

	public String getAddr3() {
		return addr3;
	}
	
	public String getTown() {
		return town;
	}
	
	public String getCounty() {
		return county;
	}

	public String getPostcode() {
		return postcode;
	}

	public String getPosition() {
		return position;
	}
	
	public String getTelephone() {
		return telephone;
	}

	public String getFax() {
		return fax;
	}

	public String getEmail() {
		return email;
	}
	
	public String getPreference() {
		return preference;
	}
}