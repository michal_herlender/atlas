package org.trescal.cwms.core.company.projection;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AddressProjectionDTO {
	// Below fields always populated by query
	private Integer addressId;
	private Integer subdivId;
	private Boolean active;
	private String addr1;
	private String addr2;
	private String addr3;
	private Integer countryId;
	private String countryCode;
	private String county;
	private String postcode;
	private String town;
	private String telephone;
	private String fax;
	// Optional relationships
	private SubdivProjectionDTO subdiv;

	// Constructor not created by Lombok - relationships populated at query time
	public AddressProjectionDTO(Integer addressId, Boolean addressActive, String addr1, String addr2,
			String addr3, Integer countryId, String countryCode, String county, String postcode, String town,
			String telephone, String fax, Integer subdivId, String subname, Boolean subdivActive, 
			Integer coid, String coname, Boolean companyActive, Boolean onstop) {
		super();
		this.addressId = addressId;
		this.subdivId = subdivId;
		this.active = addressActive;
		this.addr1 = addr1;
		this.addr2 = addr2;
		this.addr3 = addr3;
		this.countryId = countryId;
		this.countryCode = countryCode;
		this.county = county;
		this.postcode = postcode;
		this.town = town;
		this.telephone = telephone;
		this.fax = fax;
		this.subdiv = new SubdivProjectionDTO(subdivId, subname, subdivActive, coid, coname, companyActive, onstop);
	}
}
