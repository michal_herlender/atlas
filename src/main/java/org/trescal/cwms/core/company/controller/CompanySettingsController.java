package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.avalara.AvalaraConnector;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class CompanySettingsController {

	@Value("${cwms.config.avalara.aware}")
	private boolean avalaraAware;
	@Autowired
	private AvalaraConnector avalaraConnector;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private CompanySettingsForAllocatedCompanyService settingsService;
	@Autowired
	private UserService userService;
	@Autowired
	private SubdivService subdivService;

	@GetMapping(value = "/changecompanyactive.json")
	public Date changeActive(@RequestParam(value = "coid") Integer companyId,
							 @RequestParam(value = "activate") Boolean activate,
							 @RequestParam(value = "reason") String reason,
							 @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		Company company = this.companyService.get(companyId);
		Company allocatedCompany = this.subdivService.get(subdivDto.getKey()).getComp();
		CompanySettingsForAllocatedCompany settings = this.settingsService.getByCompany(company, allocatedCompany);
		Date date = new Date();
		if (settings == null) {
			settings = this.settingsService.initializeForCompany(company, allocatedCompany, false);
			settings.setActive(activate);
			settings.setDeactReason(reason);
			settings.setDeactDate(date);
			companyService.update(company);
		} else {
			settings.setActive(activate);
			settings.setDeactReason(reason);
			settings.setDeactDate(date);
			settingsService.merge(settings);
		}
		if(activate && avalaraAware)
			// Create or update customer record in Avalara
			avalaraConnector.commitCustomer(company, allocatedCompany.getCoid());
		return date;
	}

	@GetMapping(value = "/changecompanyonstop.json")
	public Map<String, String> changeOnStop(
		@RequestParam(value = "coid", required = false, defaultValue = "0") Integer companyId,
		@RequestParam(value = "stopit", required = false, defaultValue = "true") Boolean stopit,
		@RequestParam(value = "reason", required = false, defaultValue = "") String reason,
		@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
		@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		Company company = companyService.get(companyId);
		Company allocatedCompany = subdivService.get(subdivDto.getKey()).getComp();
		CompanySettingsForAllocatedCompany settings = settingsService.getByCompany(company, allocatedCompany);
		Contact contact = userService.get(username).getCon();
		LocalDate date = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		if (settings == null) {
			settings = this.settingsService.initializeForCompany(company, allocatedCompany, false);
			settings.setOnStop(stopit);
			settings.setOnStopReason(reason);
			settings.setOnStopSince(date);
			settings.setOnStopBy(contact);
			companyService.update(company);
		} else {
			settings.setOnStop(stopit);
			settings.setOnStopReason(reason);
			settings.setOnStopSince(date);
			settings.setOnStopBy(contact);
			settingsService.merge(settings);
		}
		Map<String, String> result = new HashMap<>();
		result.put("user", contact.getName());
		result.put("date", DateTools.df.format(date));
		return result;
	}
}