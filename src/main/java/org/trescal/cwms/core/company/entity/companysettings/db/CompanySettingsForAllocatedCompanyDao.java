package org.trescal.cwms.core.company.entity.companysettings.db;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.dto.CompanySettingsDTO;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.quotation.dto.CompanySettingsForAllocatedCompanyDTO;

public interface CompanySettingsForAllocatedCompanyDao
		extends AllocatedDao<Company, CompanySettingsForAllocatedCompany, Integer> {

	CompanySettingsForAllocatedCompany getByCompany(Company company, Company allocatedCompany);

	List<CompanySettingsForAllocatedCompany> getAll(Set<Integer> companyIds, Company allocatedCompany);

	List<CompanySettingsDTO> getDtosForAllocatedCompany(Collection<Integer> companyIds, Integer allocatedCompanyId);
	
	List<CompanySettingsForAllocatedCompanyDTO> getAllSettingsDtosForAllocatedCompany(Collection<Integer> companyIds, Collection<Integer> allocatedCompanyIds);
}