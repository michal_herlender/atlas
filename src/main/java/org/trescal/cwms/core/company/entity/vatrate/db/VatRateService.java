package org.trescal.cwms.core.company.entity.vatrate.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.VatRateType;

public interface VatRateService extends BaseService<VatRate, String> 
{
	List<VatRate> getAllEagerWithCountry();
	VatRate getDefault(Country companyCountry, Country businessCountry, CompanyRole companyRole);
	boolean isVatRateAllowed(VatRate vatRate, Country companyCountry, Country businessCountry, CompanyRole companyRole);
	VatRate getForCountryAndType(Integer countryId, VatRateType type);
	VatRate getForAddress(Address clientAddress, Company businessCompany);
}