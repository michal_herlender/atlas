package org.trescal.cwms.core.company.entity.corole;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.editablecorole.EditableCorole;

/**
 * Entity representing the roles that {@link Company} can fulfil - client,
 * supplier etc.
 * 
 * @author richard
 */
//@Entity
//@Table(name = "corole")
public class Corole
{
	private Set<Company> companies;
	private String corole;
	private int coroleid;
	/**
	 * Defines if this company type supports {@link Department}s.
	 */
	private boolean departmentEnabled;
	private String description;
	/**
	 * Indicates if a {@link Company} with this corole can have it's corole
	 * changed.
	 */
	private boolean editable;
	private Set<EditableCorole> editableFrom;
	
	@OneToMany(mappedBy = "corole")
	public Set<Company> getCompanies()
	{
		return this.companies;
	}

	@Length(max = 255)
	@Column(name = "corole", length = 255)
	public String getCorole()
	{
		return this.corole;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "coroleid", nullable = false, unique = true)
	@Type(type = "int")
	public int getCoroleid()
	{
		return this.coroleid;
	}

	@Length(max = 250)
	@Column(name = "description", length = 250)
	public String getDescription()
	{
		return this.description;
	}

	@OneToMany(mappedBy = "editableFrom")
	public Set<EditableCorole> getEditableFrom()
	{
		return this.editableFrom;
	}
	
	@NotNull
	@Column(name = "departmentenabled", nullable = false, columnDefinition="tinyint")
	public boolean isDepartmentEnabled()
	{
		return this.departmentEnabled;
	}

	@NotNull
	@Column(name = "editable", nullable = false, columnDefinition="tinyint")
	public boolean isEditable()
	{
		return this.editable;
	}

	public void setCompanies(Set<Company> companies)
	{
		this.companies = companies;
	}

	public void setCorole(String corole)
	{
		this.corole = corole;
	}

	public void setCoroleid(int coroleid)
	{
		this.coroleid = coroleid;
	}

	/**
	 * @param departmentEnabled the departmentEnabled to set
	 */
	public void setDepartmentEnabled(boolean departmentEnabled)
	{
		this.departmentEnabled = departmentEnabled;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setEditable(boolean editable)
	{
		this.editable = editable;
	}

	public void setEditableFrom(Set<EditableCorole> editableFrom)
	{
		this.editableFrom = editableFrom;
	}
	
	@Override
	public String toString()
	{
		return Integer.toString(this.coroleid);
	}
}
