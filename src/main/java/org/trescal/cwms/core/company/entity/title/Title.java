package org.trescal.cwms.core.company.entity.title;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.country.Country;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "title")
public class Title {
	
	private String description;
	private String title;
	private Country country;
	private Integer id;

	@Length(max = 255)
	@Column(name = "description", length = 255)
	public String getDescription() {
		return description;
	}

	@Column(name = "title", length = 10)
	public String getTitle() {
		return title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "countryid")
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
