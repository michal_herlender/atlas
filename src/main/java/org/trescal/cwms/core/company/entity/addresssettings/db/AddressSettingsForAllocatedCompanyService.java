package org.trescal.cwms.core.company.entity.addresssettings.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.company.Company;

public interface AddressSettingsForAllocatedCompanyService extends BaseService<AddressSettingsForAllocatedCompany, Integer> {

	public AddressSettingsForAllocatedCompany getByCompany(Address address, Company allocatedCompany);
	
	public AddressSettingsForAllocatedCompany initializeForAddress(Address address, Company allocatedCompany);
}
