package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.jobs.job.dto.BpoDTO;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.List;

@RestController
@RequestMapping("company")
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY})
public class CompanyAjaxController {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private BPOService bpoService;

    @GetMapping("bpo.json")
    List<BpoDTO> getBpo(@RequestParam Integer coid, @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyKeyValue) {
        return bpoService.getAllByCompany(companyService.get(coid), true, companyService.get(companyKeyValue.getKey()));
    }
}
