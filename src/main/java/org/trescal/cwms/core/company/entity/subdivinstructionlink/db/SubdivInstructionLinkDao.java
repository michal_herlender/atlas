package org.trescal.cwms.core.company.entity.subdivinstructionlink.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

public interface SubdivInstructionLinkDao extends BaseDao<SubdivInstructionLink, Integer>
{
	List<SubdivInstructionLink> getForSubdivAndTypes(Subdiv subdiv, Subdiv allocatedSubdiv, InstructionType... types);
}