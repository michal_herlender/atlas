package org.trescal.cwms.core.company.entity.companylist.db;

import java.sql.Timestamp;
import java.util.List;

import org.eclipse.birt.report.model.api.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companylist.CompanyList;
import org.trescal.cwms.core.company.entity.companylistmember.db.CompanyListMemberService;
import org.trescal.cwms.core.company.form.AddCompanyListForm;
import org.trescal.cwms.core.company.form.CompanyListSearchForm;
import org.trescal.cwms.core.company.form.EditCompanyListForm;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.tools.PagedResultSet;

@Service("CompanyListService")
public class CompanyListServiceImpl extends BaseServiceImpl<CompanyList, Integer> implements CompanyListService {

	@Autowired
	private CompanyListDao companyListDao;
	@Autowired
	private UserService userService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private CompanyListMemberService companyListMemberService;

	@Override
	protected BaseDao<CompanyList, Integer> getBaseDao() {
		return companyListDao;
	}

	@Override
	public CompanyList findCompanyList(int id) {
		return companyListDao.find(id);
	}

	@Override
	public CompanyList getByName(String nameList, Company allocatedCompany) {
		return companyListDao.getByName(nameList, allocatedCompany);
	}

	@Override
	public void updateCompanyList(CompanyList companyList) {
		companyListDao.update(companyList);
	}

	@Override
	public void editCompanyList(CompanyList companyList, EditCompanyListForm form) {

		if (!StringUtil.isEmpty(form.getName())) {
			companyList.setName(form.getName());
		}

		if (form.getActivestatus() != companyList.getActive()) {
			companyList.setActive(form.getActivestatus());
		}

		this.updateCompanyList(companyList);
	}

	@Override
	public PagedResultSet<CompanyList> searchCompanyList(CompanyListSearchForm form, Company allocatedCompany,
			PagedResultSet<CompanyList> rs) {

		return this.companyListDao.searchCompanyList(form, allocatedCompany, rs);

	}

	@Override
	public CompanyList createNewCompanyList(AddCompanyListForm form, String username) {
		CompanyList newCompanyList = new CompanyList();
		newCompanyList.setActive(true);
		newCompanyList.setName(form.getNameList());
		newCompanyList.setOrganisation(companyService.get(form.getOrgid()));
		newCompanyList.setLastModified(new Timestamp(System.currentTimeMillis()));
		newCompanyList.setLastModifiedBy(userService.get(username).getCon());
		companyListDao.persist(newCompanyList);
		return newCompanyList;
	}

	@Override
	public Integer getCompanyListByCompanyMember(Integer coid, Company allocatedCompany) {
		return companyListDao.getCompanyListByCompanyMember(coid, allocatedCompany);
	}
	
	public List<Integer> getCompanyListsByUser(Integer coid, Company allocatedCompany){
		return this.companyListDao.getCompanyListsByUser(coid, allocatedCompany);
	}
	
	@Override
	public Boolean supplierInCompanyListUsedClient(Integer clientCompanyId, Integer supplierCompanyId, Company allocatedCompany){
		// get a list on company list ids that the client company use
		List<Integer> companyListIds = this.getCompanyListsByUser(clientCompanyId, allocatedCompany);
		// get a list of companies that are members of the company lists given
		List<Integer> memberCoids = companyListMemberService.getMemberCoidsByCompanyLists(companyListIds);
		// check if the supplier company is a member in a list that the client company use 
		if(memberCoids.contains(supplierCompanyId)){
			return true;
		}
		return false;
	}

}
