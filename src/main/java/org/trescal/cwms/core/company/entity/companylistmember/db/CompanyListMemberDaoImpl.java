package org.trescal.cwms.core.company.entity.companylistmember.db;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companylist.CompanyList_;
import org.trescal.cwms.core.company.entity.companylistmember.CompanyListMember;
import org.trescal.cwms.core.company.entity.companylistmember.CompanyListMember_;

@Repository("CompanyListMemberDao")
public class CompanyListMemberDaoImpl extends BaseDaoImpl<CompanyListMember, Integer> implements CompanyListMemberDao {

	@Override
	protected Class<CompanyListMember> getEntity() {
		return CompanyListMember.class;
	}

	@Override
	public List<CompanyListMember> getMembersByCompanyList(Integer companyListId) {
		return getResultList(cb -> {
			CriteriaQuery<CompanyListMember> cq = cb.createQuery(CompanyListMember.class);
			Root<CompanyListMember> root = cq.from(CompanyListMember.class);
			cq.where(cb.equal(root.get(CompanyListMember_.companyList).get(CompanyList_.id), companyListId));
			cq.orderBy(cb.asc(root.get(CompanyListMember_.company).get(Company_.coname)));
			return cq;
		});
	}

	@Override
	public List<Integer> getMemberCoidsByCompanyLists(List<Integer> companyListIds){
		return getResultList(cb -> {
			CriteriaQuery<CompanyListMember> cq = cb.createQuery(CompanyListMember.class);
			Root<CompanyListMember> root = cq.from(CompanyListMember.class);
			cq.where(root.get(CompanyListMember_.companyList).get(CompanyList_.id).in(companyListIds));
			cq.orderBy(cb.asc(root.get(CompanyListMember_.company).get(Company_.coname)));
			return cq;
		}).stream().map(member->member.getCompany().getCoid()).collect(Collectors.toList());
	}
}
