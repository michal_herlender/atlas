package org.trescal.cwms.core.company.dto;

public class SubdivSearchResultWrapper
{
	private int subdivid;
	private String subname;
	
	public SubdivSearchResultWrapper() {}
	
	public SubdivSearchResultWrapper(int subdivid, String subname)
	{
		this.subdivid = subdivid;
		this.subname = subname;
	}

	public int getSubdivid()
	{
		return subdivid;
	}

	public void setSubdivid(int subdivid)
	{
		this.subdivid = subdivid;
	}

	public String getSubname()
	{
		return subname;
	}

	public void setSubname(String subname)
	{
		this.subname = subname;
	}

}