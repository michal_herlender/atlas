package org.trescal.cwms.core.company.entity.address.db;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.dto.AddrSearchResultWrapper;
import org.trescal.cwms.core.company.dto.AddressPlantillasDTO;
import org.trescal.cwms.core.company.dto.AddressWithTypeDTO;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.addressplantillassettings.AddressPlantillasSettings;
import org.trescal.cwms.core.company.entity.addressplantillassettings.AddressPlantillasSettings_;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.company.projection.AddressProjectionDTO;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.User_;
import org.trescal.cwms.core.tools.CriteriaApiTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.userright.entity.userrole.UserRole;
import org.trescal.cwms.core.userright.entity.userrole.UserRole_;
import org.trescal.cwms.rest.tlm.dto.TLMAddressProjectionDTO;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Repository("AddressDao")
public class AddressDaoImpl extends BaseDaoImpl<Address, Integer> implements AddressDao {

	@Override
	protected Class<Address> getEntity() {
		return Address.class;
	}

	@Override
	public int countAddressInstruments(int addrid) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Instrument> root = cq.from(Instrument.class);
			Join<Instrument, Address> address = root.join(Instrument_.add);
			cq.where(cb.equal(address.get(Address_.addrid), addrid));
			cq.select(cb.count(root));
			return cq;
		}).intValue();
	}

	/*
	 * Returns a count of addresses for the company matching the specified type,
	 * excluding the specified address (which could be null - or transient!)
	 */
	@Override
	public Long countCompanyAddresses(Company company, AddressType type, Address excludeAddress) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Address> root = cq.from(Address.class);
			if (type != null) {
				Join<Address, AddressType> addrType = root.join(Address_.addressType);
				addrType.on(cb.equal(addrType, type));
			}
			Join<Address, Subdiv> subdiv = root.join(Address_.sub);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.comp), company));
			if ((excludeAddress != null) && (excludeAddress.getAddrid() != null) && (excludeAddress.getAddrid() != 0)) {
				clauses.getExpressions().add(cb.notEqual(root, excludeAddress));
			}
			cq.where(clauses);
			cq.select(cb.count(root));
			return cq;
		});
	}

	@Override
	public List<AddrSearchResultWrapper> getActiveUserRoleAddresses(String username) {
		return getResultList(cb -> {
			CriteriaQuery<AddrSearchResultWrapper> cq = cb.createQuery(AddrSearchResultWrapper.class);
			Root<Address> root = cq.from(Address.class);
			Join<Address, Subdiv> subdiv = root.join(Address_.sub);
			// Join<Subdiv, UserRole> userrole = subdiv.join(Subdiv_.userRoles);

			Subquery<Integer> sq_userrole = cq.subquery(Integer.class);
			Root<UserRole> root_sq = sq_userrole.from(UserRole.class);
			Join<UserRole, User> sq_user = root_sq.join(UserRole_.user);
			Join<UserRole, Subdiv> sq_subdiv = root_sq.join(UserRole_.organisation.getName());
			sq_userrole.where(cb.equal(sq_user.get(User_.username), username));
			sq_userrole.distinct(true);

			Join<Subdiv, Address> defaultAddress = subdiv.join(Subdiv_.defaultAddress,
					javax.persistence.criteria.JoinType.LEFT);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(root.get(Address_.active)));
			clauses.getExpressions().add(subdiv.in(sq_userrole.select(sq_subdiv.get(Subdiv_.subdivid))));

			cq.distinct(true);
			cq.select(constructWrapper(cb, root, defaultAddress));
			cq.where(clauses);
			cq.orderBy(cb.asc(root.get(Address_.addr1)));
			return cq;
		});
	}

	@Override
	public List<AddrSearchResultWrapper> getActiveAddressesProjection(Integer coid, Integer subdivid,
			AddressType type) {
		if (coid == null && subdivid == null)
			throw new IllegalArgumentException("Either a coid or subdivid must be provided");
		return getResultList(cb -> {
			CriteriaQuery<AddrSearchResultWrapper> cq = cb.createQuery(AddrSearchResultWrapper.class);
			Root<Address> root = cq.from(Address.class);
			Join<Address, Subdiv> subdiv = root.join(Address_.sub);
			Join<Subdiv, Address> defaultAddress = subdiv.join(Subdiv_.defaultAddress,
					javax.persistence.criteria.JoinType.LEFT);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(root.get(Address_.active)));
			if (coid != null) {
				Join<Subdiv, Company> comp = subdiv.join(Subdiv_.comp);
				clauses.getExpressions().add(cb.equal(comp.get(Company_.coid), coid));
			}
			if (subdivid != null) {
				clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), subdivid));
			}
			if (type != null && type != AddressType.WITHOUT) {
				Join<Address, AddressType> addressType = root.join(Address_.addressType);
				clauses.getExpressions().add(cb.equal(addressType, type));
			}
			cq.distinct(true);
			cq.select(constructWrapper(cb, root, defaultAddress));
			cq.where(clauses);
			cq.orderBy(cb.asc(root.get(Address_.addr1)));
			return cq;
		});
	}
	
	private Selection<AddrSearchResultWrapper> constructWrapper(CriteriaBuilder cb, Root<Address> root, Join<Subdiv,Address> defaultAddress) {
		return cb.construct(AddrSearchResultWrapper.class, root.get(Address_.addrid), root.get(Address_.addr1),
				root.get(Address_.addr2), root.get(Address_.town), root.get(Address_.county),
				root.get(Address_.postcode), defaultAddress.get(Address_.addrid));
	}
	
	@Override
	public List<Integer> getAddressIdsForContactInstruments(Integer contactId) {
		return getResultList(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<Instrument> root = cq.from(Instrument.class);
			Join<Instrument, Contact> contact = root.join(Instrument_.con);
			Join<Instrument, Address> address = root.join(Instrument_.add);
			cq.where(cb.equal(contact.get(Contact_.personid), contactId));
			cq.distinct(true);
			cq.select(address.get(Address_.addrid));
			return cq;
		});
	}

	@Override
	public List<AddrSearchResultWrapper> getAddressesFromIds(Collection<Integer> addressIds) {
		if (addressIds == null || addressIds.isEmpty())
			return Collections.emptyList();
		return getResultList(cb -> {
			CriteriaQuery<AddrSearchResultWrapper> cq = cb.createQuery(AddrSearchResultWrapper.class);
			Root<Address> root = cq.from(Address.class);
			Join<Address, Subdiv> subdiv = root.join(Address_.sub);
			Join<Subdiv, Address> defaultAddress = subdiv.join(Subdiv_.defaultAddress,
					javax.persistence.criteria.JoinType.LEFT);
			
			cq.distinct(true);
			cq.select(constructWrapper(cb, root, defaultAddress));
			cq.where(root.get(Address_.addrid).in(addressIds));
			cq.orderBy(cb.asc(root.get(Address_.addr1)));
			return cq;
		});
	}
	
	
	@Override
	public TLMAddressProjectionDTO getAllCompanyAddresses(int addrid) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<TLMAddressProjectionDTO> cq = cb.createQuery(TLMAddressProjectionDTO.class);

		Root<Address> addressRoot = cq.from(Address.class);
		Join<Address, Subdiv> subdivJoin = addressRoot.join(Address_.sub.getName());
		Join<Subdiv, Company> companyJoin = subdivJoin.join(Subdiv_.comp.getName());
		Join<Company, CompanySettingsForAllocatedCompany> settingsJoin = companyJoin
				.join(Company_.settingsForAllocatedCompanies.getName());

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(addressRoot.get(Address_.addrid), addrid));
		clauses.getExpressions()
				.add(cb.equal(settingsJoin.get(CompanySettingsForAllocatedCompany_.active.getName()), true));

		cq.where(clauses);

		Subquery<Long> invoiceTypeSq = cq.subquery(Long.class);
		Root<Address> invoiceTypeSqRoot = invoiceTypeSq.from(Address.class);
		invoiceTypeSq.select(cb.count(invoiceTypeSqRoot));
		invoiceTypeSq.where(cb.isMember(AddressType.INVOICE, invoiceTypeSqRoot.get(Address_.addressType.getName())),
				cb.equal(invoiceTypeSqRoot.get(Address_.addrid.getName()), addressRoot.get(Address_.addrid.getName())));

		Subquery<Long> deliveryTypeSq = cq.subquery(Long.class);
		Root<Address> deliveryTypeSqRoot = deliveryTypeSq.from(Address.class);
		deliveryTypeSq.select(cb.count(deliveryTypeSqRoot));
		deliveryTypeSq.where(cb.isMember(AddressType.DELIVERY, deliveryTypeSqRoot.get(Address_.addressType.getName())),
				cb.equal(deliveryTypeSqRoot.get(Address_.addrid.getName()),
						addressRoot.get(Address_.addrid.getName())));

		Subquery<Long> certificateTypeSq = cq.subquery(Long.class);
		Root<Address> certificateTypeSqRoot = certificateTypeSq.from(Address.class);
		certificateTypeSq.select(cb.count(certificateTypeSqRoot));
		certificateTypeSq.where(
				cb.isMember(AddressType.CERTIFICATE, certificateTypeSqRoot.get(Address_.addressType.getName())),
				cb.equal(certificateTypeSqRoot.get(Address_.addrid.getName()),
						addressRoot.get(Address_.addrid.getName())));

		Subquery<Long> poformTypeSq = cq.subquery(Long.class);
		Root<Address> poformTypeSqRoot = poformTypeSq.from(Address.class);
		poformTypeSq.select(cb.count(poformTypeSqRoot));
		poformTypeSq.where(cb.isMember(AddressType.POFROM, poformTypeSqRoot.get(Address_.addressType.getName())),
				cb.equal(poformTypeSqRoot.get(Address_.addrid.getName()), addressRoot.get(Address_.addrid.getName())));

		Subquery<Long> legalTypeSq = cq.subquery(Long.class);
		Root<Address> legalTypeSqRoot = legalTypeSq.from(Address.class);
		legalTypeSq.select(cb.count(legalTypeSqRoot));
		legalTypeSq.where(
				cb.isMember(AddressType.LEGAL_REGISTRATION, legalTypeSqRoot.get(Address_.addressType.getName())),
				cb.equal(legalTypeSqRoot.get(Address_.addrid.getName()), addressRoot.get(Address_.addrid.getName())));

		// get results
		cq.select(cb.construct(TLMAddressProjectionDTO.class, addressRoot.get(Address_.addrid.getName()),
				addressRoot.get(Address_.active.getName()), addressRoot.get(Address_.addr1.getName()),
				addressRoot.get(Address_.addr2.getName()), addressRoot.get(Address_.addr3.getName()),
				addressRoot.get(Address_.town.getName()), addressRoot.get(Address_.county.getName()),
				addressRoot.get(Address_.postcode.getName()),
				addressRoot.get(Address_.country.getName()).get(Country_.countryCode.getName()),
				addressRoot.get(Address_.telephone.getName()), addressRoot.get(Address_.fax.getName()),
				addressRoot.get(Address_.lastModified.getName()), invoiceTypeSq.getSelection(),
				deliveryTypeSq.getSelection(), certificateTypeSq.getSelection(), poformTypeSq.getSelection(),
				legalTypeSq.getSelection(), subdivJoin.get(Subdiv_.subdivid.getName()),
				subdivJoin.get(Subdiv_.active.getName()), subdivJoin.get(Subdiv_.subname.getName()),
				subdivJoin.get(Subdiv_.siretNumber.getName()), subdivJoin.get(Subdiv_.lastModified.getName()),
				companyJoin.get(Company_.coid.getName()), companyJoin.get(Company_.companyRole.getName()),
				companyJoin.get(Company_.legalIdentifier.getName()),
				companyJoin.get(Company_.fiscalIdentifier.getName()), companyJoin.get(Company_.coname.getName()),
				companyJoin.get(Company_.lastModified.getName())));

		return getEntityManager().createQuery(cq).getResultList().stream().findFirst().orElse(null);
	}

	@Override
	public PagedResultSet<TLMAddressProjectionDTO> getAllCompanyAddresses(Company company, List<CompanyRole> roles,
			Timestamp companyLastModifiedDate, Timestamp subdivLastModifiedDate, Timestamp addressLastModifiedDate,
			int resultsPerPage, int currentPage) {
		PagedResultSet<TLMAddressProjectionDTO> rs = new PagedResultSet<>(resultsPerPage, currentPage);

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<TLMAddressProjectionDTO> cq = cb.createQuery(TLMAddressProjectionDTO.class);

		Root<Address> addressRoot = cq.from(Address.class);
		addressRoot.alias("addressRoot");
		Join<Address, Subdiv> subdivJoin = addressRoot.join(Address_.sub.getName());
		subdivJoin.alias("subdivJoin");
		Join<Subdiv, Company> companyJoin = subdivJoin.join(Subdiv_.comp.getName());
		companyJoin.alias("companyJoin");
		Join<Company, CompanySettingsForAllocatedCompany> settingsJoin = companyJoin
				.join(Company_.settingsForAllocatedCompanies.getName());
		settingsJoin.alias("settingsJoin");

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(companyJoin.get(Company_.companyRole.getName()).in(roles));
		clauses.getExpressions()
				.add(cb.equal(settingsJoin.get(CompanySettingsForAllocatedCompany_.active.getName()), true));
		clauses.getExpressions()
				.add(cb.equal(settingsJoin.get(CompanySettingsForAllocatedCompany_.organisation.getName()), company));

		if (companyLastModifiedDate != null) {
			clauses.getExpressions().add(
					cb.greaterThanOrEqualTo(companyJoin.get(Company_.lastModified.getName()), companyLastModifiedDate));
		}
		if (subdivLastModifiedDate != null) {
			clauses.getExpressions().add(
					cb.greaterThanOrEqualTo(subdivJoin.get(Subdiv_.lastModified.getName()), subdivLastModifiedDate));
		}
		if (addressLastModifiedDate != null) {
			clauses.getExpressions().add(
					cb.greaterThanOrEqualTo(addressRoot.get(Address_.lastModified.getName()), addressLastModifiedDate));
		}

		cq.where(clauses);

		Subquery<Long> invoiceTypeSq = cq.subquery(Long.class);
		Root<Address> invoiceTypeSqRoot = invoiceTypeSq.from(Address.class);
		invoiceTypeSq.select(cb.count(invoiceTypeSqRoot));
		invoiceTypeSq.where(cb.isMember(AddressType.INVOICE, invoiceTypeSqRoot.get(Address_.addressType.getName())),
				cb.equal(invoiceTypeSqRoot.get(Address_.addrid.getName()), addressRoot.get(Address_.addrid.getName())));

		Subquery<Long> deliveryTypeSq = cq.subquery(Long.class);
		Root<Address> deliveryTypeSqRoot = deliveryTypeSq.from(Address.class);
		deliveryTypeSq.select(cb.count(deliveryTypeSqRoot));
		deliveryTypeSq.where(cb.isMember(AddressType.DELIVERY, deliveryTypeSqRoot.get(Address_.addressType.getName())),
				cb.equal(deliveryTypeSqRoot.get(Address_.addrid.getName()),
						addressRoot.get(Address_.addrid.getName())));

		Subquery<Long> certificateTypeSq = cq.subquery(Long.class);
		Root<Address> certificateTypeSqRoot = certificateTypeSq.from(Address.class);
		certificateTypeSq.select(cb.count(certificateTypeSqRoot));
		certificateTypeSq.where(
				cb.isMember(AddressType.CERTIFICATE, certificateTypeSqRoot.get(Address_.addressType.getName())),
				cb.equal(certificateTypeSqRoot.get(Address_.addrid.getName()),
						addressRoot.get(Address_.addrid.getName())));

		Subquery<Long> poformTypeSq = cq.subquery(Long.class);
		Root<Address> poformTypeSqRoot = poformTypeSq.from(Address.class);
		poformTypeSq.select(cb.count(poformTypeSqRoot));
		poformTypeSq.where(cb.isMember(AddressType.POFROM, poformTypeSqRoot.get(Address_.addressType.getName())),
				cb.equal(poformTypeSqRoot.get(Address_.addrid.getName()), addressRoot.get(Address_.addrid.getName())));

		Subquery<Long> legalTypeSq = cq.subquery(Long.class);
		Root<Address> legalTypeSqRoot = legalTypeSq.from(Address.class);
		legalTypeSq.select(cb.count(legalTypeSqRoot));
		legalTypeSq.where(
				cb.isMember(AddressType.LEGAL_REGISTRATION, legalTypeSqRoot.get(Address_.addressType.getName())),
				cb.equal(legalTypeSqRoot.get(Address_.addrid.getName()), addressRoot.get(Address_.addrid.getName())));

		// count results
		Long count = CriteriaApiTools.count(getEntityManager(), cq, addressRoot);
		rs.setResultsCount(count.intValue());

		// get results
		cq.select(cb.construct(TLMAddressProjectionDTO.class, addressRoot.get(Address_.addrid.getName()),
				addressRoot.get(Address_.active.getName()), addressRoot.get(Address_.addr1.getName()),
				addressRoot.get(Address_.addr2.getName()), addressRoot.get(Address_.addr3.getName()),
				addressRoot.get(Address_.town.getName()), addressRoot.get(Address_.county.getName()),
				addressRoot.get(Address_.postcode.getName()),
				addressRoot.get(Address_.country.getName()).get(Country_.countryCode.getName()),
				addressRoot.get(Address_.telephone.getName()), addressRoot.get(Address_.fax.getName()),
				addressRoot.get(Address_.lastModified.getName()), invoiceTypeSq.getSelection(),
				deliveryTypeSq.getSelection(), certificateTypeSq.getSelection(), poformTypeSq.getSelection(),
				legalTypeSq.getSelection(), subdivJoin.get(Subdiv_.subdivid.getName()),
				subdivJoin.get(Subdiv_.active.getName()), subdivJoin.get(Subdiv_.subname.getName()),
				subdivJoin.get(Subdiv_.siretNumber.getName()), subdivJoin.get(Subdiv_.lastModified.getName()),
				companyJoin.get(Company_.coid.getName()), companyJoin.get(Company_.companyRole.getName()),
				companyJoin.get(Company_.legalIdentifier.getName()),
				companyJoin.get(Company_.fiscalIdentifier.getName()), companyJoin.get(Company_.coname.getName()),
				companyJoin.get(Company_.lastModified.getName())));

		TypedQuery<TLMAddressProjectionDTO> query = getEntityManager().createQuery(cq);
		query.setFirstResult(rs.getStartResultsFrom());
		query.setMaxResults(rs.getResultsPerPage());

		rs.setResults(query.getResultList());

		return rs;
	}

	/**
	 * Find all addresses associated with a given company
	 * 
	 * @param coid
	 * @param type
	 * @return
	 */
	@Override
	public List<Address> getAllCompanyAddresses(int coid, AddressType type, boolean activeOnly) {
		return getResultList(cb -> {
            CriteriaQuery<Address> cq = cb.createQuery(Address.class);
            Root<Address> root = cq.from(Address.class);
            Join<Address, Subdiv> subdiv = root.join(Address_.sub);
            Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.equal(company.get(Company_.coid), coid));
            clauses.getExpressions().add(cb.isTrue(subdiv.get(Subdiv_.ACTIVE)));
            if (activeOnly)
                clauses.getExpressions().add(cb.isTrue(root.get(Address_.active)));
            if (type != null && type != AddressType.WITHOUT)
                clauses.getExpressions().add(cb.equal(root.join(Address_.addressType), type));
            cq.where(clauses);
            cq.orderBy(cb.asc(root.get(Address_.addr1)));
            return cq;
        });
	}

	@Override
	public Address findCompanyAddress(int coid, AddressType type, boolean activeOnly, String addressText) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Address> cq = cb.createQuery(Address.class);
		Root<Address> from = cq.from(Address.class);
		cq.select(from);
		Predicate addressTypePredicate = cb.isTrue(cb.literal(true));
		Predicate activeOnlyPredicate = cb.isTrue(cb.literal(true));
		Predicate addressPredicate = cb.isTrue(cb.literal(true));
		// company id predicate
		Predicate companyIdPredicate = cb.equal(from.get(Address_.sub).get(Subdiv_.comp).get(Company_.coid), coid);
		// address predicate
		if (type != null && (type != AddressType.WITHOUT)) {
			addressTypePredicate = cb.equal(from.get(Address_.addressType.getName()).get("elements"), type);
		}
		// address active predicate
		activeOnlyPredicate = cb.equal(from.get(Address_.active.getName()), activeOnly);
		// address text predicate
		Expression<String> allLinesWithCommas = cb.concat(cb.trim(from.get(Address_.addr1)), ", ");
		allLinesWithCommas = cb.concat(allLinesWithCommas, from.get(Address_.addr2));
		allLinesWithCommas = cb.concat(from.get(Address_.addr2), ", ");
		allLinesWithCommas = cb.concat(allLinesWithCommas, from.get(Address_.addr3));

		Expression<String> allLinesWithSpaces = cb.concat(cb.trim(from.get(Address_.addr1)), " ");
		allLinesWithSpaces = cb.concat(allLinesWithSpaces, from.get(Address_.addr2));
		allLinesWithSpaces = cb.concat(from.get(Address_.addr2), " ");
		allLinesWithSpaces = cb.concat(allLinesWithSpaces, from.get(Address_.addr3));

		Expression<String> instrumentExportFormat = cb.concat(cb.concat(cb.trim(from.get(Address_.addr1)), ", "),
				from.get(Address_.town));

		addressPredicate = cb.or(cb.like(cb.lower(from.get(Address_.addr1)), "%" + addressText.toLowerCase() + "%"),
				cb.like(cb.lower(from.get(Address_.addr2)), "%" + addressText.toLowerCase() + "%"),
				cb.like(cb.lower(from.get(Address_.addr3)), "%" + addressText.toLowerCase() + "%"),
				cb.like(cb.lower(instrumentExportFormat), "%" + addressText.toLowerCase() + "%"),
				cb.like(cb.lower(allLinesWithSpaces), "%" + addressText.toLowerCase() + "%"),
				cb.like(cb.lower(allLinesWithCommas), "%" + addressText.toLowerCase() + "%"));

		cq.where(cb.and(companyIdPredicate, addressTypePredicate, activeOnlyPredicate, addressPredicate));

		TypedQuery<Address> query = getEntityManager().createQuery(cq);
		List<Address> list = query.getResultList();
		if (CollectionUtils.isNotEmpty(list))
			return list.get(0);
		return null;
	}

	/**
	 * Finds all the addresses of a registered company subdivision
	 * 
	 * @param subdivid The subdiv id to search for
	 * @param type     Whether the return addresses should be of type delivery or
	 *                 Invoice
	 * @return
	 */
	@Override
	public List<Address> getAllSubdivAddresses(int subdivid, AddressType type, Boolean active) {
		return getResultList(cb -> {
			CriteriaQuery<Address> cq = cb.createQuery(Address.class);
			Root<Address> address = cq.from(Address.class);
			Join<Address, AddressType> addressType = address.join(Address_.addressType, JoinType.LEFT);
			Join<Address, Subdiv> subdiv = address.join(Address_.sub);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), subdivid));
			if (type != null && type != AddressType.WITHOUT)
				clauses.getExpressions().add(cb.in(addressType).value(type));
			if (active != null)
				clauses.getExpressions().add(cb.equal(address.get(Address_.active), active));
			cq.where(clauses);
			cq.orderBy(cb.asc(address.get(Address_.addr1)));
			cq.distinct(true);
			return cq;
		});
	}

	@Override
	public List<Address> getBusinessAddresses(AddressType type, boolean activeOnly) {
		return getResultList(cb -> {
			CriteriaQuery<Address> cq = cb.createQuery(Address.class);
			Root<Address> address = cq.from(Address.class);
			Join<Address, AddressType> addressType = address.join(Address_.addressType, JoinType.LEFT);
			Join<Address, Subdiv> subdiv = address.join(Address_.sub);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Predicate clauses = cb.conjunction();
			if (type != null && type != AddressType.WITHOUT)
				clauses.getExpressions().add(cb.in(addressType).value(type));
			if (activeOnly)
				clauses.getExpressions().add(cb.isTrue(address.get(Address_.active)));
			clauses.getExpressions().add(cb.equal(company.get(Company_.companyRole), CompanyRole.BUSINESS));
			cq.where(clauses);
			cq.orderBy(cb.asc(company.get(Company_.coname)), cb.asc(subdiv.get(Subdiv_.subname)),
					cb.asc(address.get(Address_.addr1)));
			return cq;
		});
	}

	private Triple<Expression<?>, Selection<AddressPlantillasDTO>, List<Order>> createPlantillasModelQueryParts(
			CriteriaBuilder cb, CriteriaQuery<?> cq, Company allocatedCompany, Integer addressId,
			Boolean includeSyncOnly, Date lastModified, Integer formerCustomerId, int businessSubdivId) {
		Root<Address> address = cq.from(Address.class);
		Join<Address, Subdiv> subdiv = address.join(Address_.sub);
		Join<Subdiv, Contact> defaultContact = subdiv.join(Subdiv_.defaultContact, JoinType.LEFT);
		Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
		Join<Company, CompanySettingsForAllocatedCompany> companySettings = company
				.join(Company_.settingsForAllocatedCompanies);
		companySettings
				.on(cb.equal(companySettings.get(CompanySettingsForAllocatedCompany_.organisation), allocatedCompany));
		Join<Address, AddressPlantillasSettings> plantillasSettings = address.join(Address_.addressPlantillasSettings,
				JoinType.LEFT);
		plantillasSettings
				.on(cb.equal(plantillasSettings.get(AddressPlantillasSettings_.organisation), allocatedCompany));
		Join<AddressPlantillasSettings, Address> managedByAddress = plantillasSettings
				.join(AddressPlantillasSettings_.managedByAddress, JoinType.LEFT);
		Join<AddressPlantillasSettings, Address> certificateAddress = plantillasSettings
				.join(AddressPlantillasSettings_.certificateAddress, JoinType.LEFT);
		Join<Address, Country> country = address.join(Address_.country, JoinType.LEFT);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(company.get(Company_.companyRole).in(CompanyRole.CLIENT, CompanyRole.BUSINESS));
		if (businessSubdivId != 0) {
			Join<Subdiv, SubdivSettingsForAllocatedSubdiv> subdivSetting = subdiv.join(Subdiv_.subdivSettings, JoinType.LEFT);
			Join<SubdivSettingsForAllocatedSubdiv, Subdiv> subdivSettingSubdiv = subdivSetting.join(SubdivSettingsForAllocatedSubdiv_.organisation.getName(), JoinType.LEFT);
			clauses.getExpressions().add(cb.equal(subdivSettingSubdiv.get(Subdiv_.subdivid), businessSubdivId));
		}
		if (addressId != null)
			clauses.getExpressions().add(cb.equal(address.get(Address_.addrid), addressId));
		if (includeSyncOnly)
			clauses.getExpressions()
					.add(cb.isTrue(companySettings.get(CompanySettingsForAllocatedCompany_.syncToPlantillas)));
		if (lastModified != null) {
			Predicate modified = cb.disjunction();
			modified.getExpressions().add(cb.greaterThan(address.get(Address_.lastModified), lastModified));
			modified.getExpressions().add(cb.greaterThan(subdiv.get(Subdiv_.lastModified), lastModified));
			modified.getExpressions().add(cb.greaterThan(company.get(Company_.lastModified), lastModified));
			modified.getExpressions().add(cb.greaterThan(defaultContact.get(Contact_.lastModified), lastModified));
			modified.getExpressions().add(cb
					.greaterThan(companySettings.get(CompanySettingsForAllocatedCompany_.lastModified), lastModified));
			modified.getExpressions()
					.add(cb.greaterThan(plantillasSettings.get(AddressPlantillasSettings_.lastModified), lastModified));
			clauses.getExpressions().add(modified);
		}
		if (formerCustomerId != null)
			clauses.getExpressions().add(
					cb.equal(plantillasSettings.get(AddressPlantillasSettings_.formerCustomerId), formerCustomerId));
		cq.where(clauses);
		cq.distinct(true);
		Selection<AddressPlantillasDTO> selection = cb.construct(AddressPlantillasDTO.class,
				address.get(Address_.addrid), company.get(Company_.coid), subdiv.get(Subdiv_.subdivid),
				address.get(Address_.active), companySettings.get(CompanySettingsForAllocatedCompany_.active),
				subdiv.get(Subdiv_.active), company.get(Company_.legalIdentifier), company.get(Company_.coname),
				subdiv.get(Subdiv_.subname), address.get(Address_.lastModified), company.get(Company_.lastModified),
				subdiv.get(Subdiv_.lastModified), defaultContact.get(Contact_.lastModified),
				companySettings.get(CompanySettingsForAllocatedCompany_.lastModified),
				plantillasSettings.get(AddressPlantillasSettings_.lastModified), managedByAddress.get(Address_.addrid),
				certificateAddress.get(Address_.addrid),
				plantillasSettings.get(AddressPlantillasSettings_.textUncertaintyFailure),
				plantillasSettings.get(AddressPlantillasSettings_.textToleranceFailure), address.get(Address_.addr1),
				address.get(Address_.addr2), address.get(Address_.addr3), address.get(Address_.town),
				address.get(Address_.county), country.get(Country_.countryCode), address.get(Address_.postcode),
				plantillasSettings.get(AddressPlantillasSettings_.sendByFtp),
				plantillasSettings.get(AddressPlantillasSettings_.ftpServer),
				plantillasSettings.get(AddressPlantillasSettings_.ftpUser),
				plantillasSettings.get(AddressPlantillasSettings_.ftpPassword),
				companySettings.get(CompanySettingsForAllocatedCompany_.contract),
				plantillasSettings.get(AddressPlantillasSettings_.nextDateOnCertificate),
				plantillasSettings.get(AddressPlantillasSettings_.metraClient), defaultContact.get(Contact_.email),
				defaultContact.get(Contact_.firstName), defaultContact.get(Contact_.lastName),
				plantillasSettings.get(AddressPlantillasSettings_.formerCustomerId),
				companySettings.get(CompanySettingsForAllocatedCompany_.syncToPlantillas));
		return Triple.of(address.get(Address_.addrid), selection, new ArrayList<>());
	}

	@Override
	public PagedResultSet<AddressPlantillasDTO> getPlantillasAddresses(Company allocatedCompany, Date lastModifiedDate,
			Integer formerCustomerId, int businessSubdivId, Integer resultsPerPage, Integer currentPage) {
		PagedResultSet<AddressPlantillasDTO> prs = new PagedResultSet<>(resultsPerPage, currentPage);
		completePagedResultSet(prs, AddressPlantillasDTO.class, cb -> cq -> createPlantillasModelQueryParts(cb, cq,
				allocatedCompany, null, true, lastModifiedDate, formerCustomerId, businessSubdivId));
		return prs;
	}

	@Override
	public AddressPlantillasDTO getPlantillasAddress(Integer addressId, Company allocatedCompany) {
		return getSingleResult(cb -> {
			CriteriaQuery<AddressPlantillasDTO> cq = cb.createQuery(AddressPlantillasDTO.class);
			Triple<Expression<?>, Selection<AddressPlantillasDTO>, List<Order>> queryParts = createPlantillasModelQueryParts(
					cb, cq, allocatedCompany, addressId, false, null, null, 0);
			cq.select(queryParts.getMiddle());
			return cq;
		});
	}

	@Override
	public List<AddressWithTypeDTO> getForCompany(Integer companyId) {
		return getResultList(cb -> {
			CriteriaQuery<AddressWithTypeDTO> cq = cb.createQuery(AddressWithTypeDTO.class);
			Root<Address> address = cq.from(Address.class);
			Join<Address, Subdiv> subdiv = address.join(Address_.sub);
			Join<Address, AddressType> type = address.join(Address_.addressType, JoinType.LEFT);
			cq.where(cb.equal(subdiv.get(Subdiv_.comp), companyId));
			cq.distinct(true);
			cq.select(cb.construct(AddressWithTypeDTO.class, address.get(Address_.addrid), address.get(Address_.addr1),
					address.get(Address_.town), type));
			return cq;
		});
	}
	
	@Override
	public List<AddressProjectionDTO> getAddressProjectionDTOs(Collection<Integer> addressIds, Integer allocatedCompanyId) {
		return getResultList(cb -> {
			CriteriaQuery<AddressProjectionDTO> cq = cb.createQuery(AddressProjectionDTO.class);
			Root<Address> address = cq.from(Address.class);
			Join<Address, Country> country = address.join(Address_.country, JoinType.INNER);
			Join<Address, Subdiv> subdiv = address.join(Address_.sub, JoinType.INNER);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp, JoinType.INNER);
			Join<Company, CompanySettingsForAllocatedCompany> settings = company.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			settings.on(cb.equal(settings.get(CompanySettingsForAllocatedCompany_.organisation.getName()), allocatedCompanyId));
			
			cq.where(address.get(Address_.addrid).in(addressIds));
			cq.distinct(true);	// Possible in data model to have multiple CompanySettings (should make a constraint) 
			
			cq.select(cb.construct(AddressProjectionDTO.class, address.get(Address_.addrid), address.get(Address_.active),
					address.get(Address_.addr1), address.get(Address_.addr2), address.get(Address_.addr3), 
					country.get(Country_.countryid), country.get(Country_.countryCode), address.get(Address_.county), 
					address.get(Address_.postcode), address.get(Address_.town), address.get(Address_.telephone), 
					address.get(Address_.fax), subdiv.get(Subdiv_.subdivid), subdiv.get(Subdiv_.subname),
					subdiv.get(Subdiv_.active), company.get(Company_.coid), company.get(Company_.coname),
					settings.get(CompanySettingsForAllocatedCompany_.active),
					settings.get(CompanySettingsForAllocatedCompany_.onStop)));
			return cq;
		});
	}
	
	@Override
	public Address getAddressFromSubdiv(Address address){
		return getFirstResult(cb ->{
			CriteriaQuery<Address> cq = cb.createQuery(Address.class);
			Root<Address> root = cq.from(Address.class);
			Join<Address, Subdiv> sub = root.join(Address_.sub, JoinType.INNER);
			Join<Address, Country> country = root.join(Address_.country, JoinType.INNER);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(Address_.town), address.getTown()));
			clauses.getExpressions().add(cb.equal(root.get(Address_.county), address.getCounty()));
			clauses.getExpressions().add(cb.equal(country.get(Country_.countryid), address.getCountry().getCountryid()));
			clauses.getExpressions().add(cb.equal(root.get(Address_.postcode), address.getPostcode()));
			clauses.getExpressions().add(cb.isTrue(root.get(Address_.active)));
			
			if(address.getSub() != null){
				clauses.getExpressions().add(cb.equal(sub.get(Subdiv_.subdivid), address.getSub().getSubdivid()));
			}
			
			if(address.getAddr1() != null){
				clauses.getExpressions().add(cb.equal(root.get(Address_.addr1), address.getAddr1()));
			}
			
			if(address.getAddr2() != null){
				clauses.getExpressions().add(cb.equal(root.get(Address_.addr2), address.getAddr2()));
			}
			
			if(address.getAddr3() != null){
				clauses.getExpressions().add(cb.equal(root.get(Address_.addr3), address.getAddr3()));
			}
			
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}
}