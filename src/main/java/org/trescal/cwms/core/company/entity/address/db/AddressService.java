package org.trescal.cwms.core.company.entity.address.db;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.dto.AddrSearchResultWrapper;
import org.trescal.cwms.core.company.dto.AddressKeyValue;
import org.trescal.cwms.core.company.dto.AddressPlantillasDTO;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.corole.Corole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.projection.AddressProjectionDTO;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.tlm.dto.TLMAddressProjectionDTO;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface AddressService extends BaseService<Address, Integer> {

	Integer checkAddressDeactivationStatus(int addrid);

	int countAddressInstruments(int addrid);

	Long countCompanyAddresses(Company company, AddressType type, Address excludeAddress);

	/**
	 * Returns formatted address from entity consistent with internal DTO creation
	 * @param addr
	 * @return formatted single line address text
	 */
	String getDisplayLine(Address addr);
	
	/**
	 * Returns a list of address IDs in use by contact's instruments
	 * Used by reassignment functions
	 */
	List<Integer> getAddressIdsForContactInstruments(Integer contactId);
	
	/**
	 * Returns formatted addresses for the specified ids using a projection
	 */
	List<KeyValue<Integer, String>> getAddressesKeyValue(Collection<Integer> addressIds);

	List<KeyValue<Integer, String>> getActiveUserRoleAddressesKeyValue(String username, boolean prependNoneDto);

	/**
	 * Returns a list of {@link Address}'s belonging to the {@link Subdiv}
	 * identified by the given id that are currently active.
	 * 
	 * @param subdivid the id of the {@link Subdiv}, not null.
	 * @param type     the type of {@link Address}, leave blank for all.
	 * @return {@link List} of matching {@link Address} entities.
	 */
	List<Address> getAllActiveSubdivAddresses(Subdiv subdiv, AddressType type);

	/**
	 * Returns a {@link List} of {@link AddrSearchResultWrapper} that belong to the
	 * {@link Subdiv} identified by the given subdivid that are currently active.
	 * 
	 * @param subdivid the id of the {@link Subdiv}, not null.
	 * @param type     the type of {@link Address}, leave blank for all.
	 * @return {@link List} of {@link AddrSearchResultWrapper}.
	 */
	List<AddrSearchResultWrapper> getAllActiveSubdivAddressesHQL(int subdivid, String typeString);

	/**
	 * Returns a list of KeyValueIntegerString DTOs where
	 * 
	 * @param coid        - Integer optional coid of company
	 * @param subdivid    - Integer optional subdivid of company
	 * @param active      - Boolean optional active / inactive property of contact
	 * @param prependDtos - KeyValue dtos to prepend, only if IDs not in results
	 * @return list of matching contacts sorted by first name, last name, and then
	 *         ID
	 */
	List<KeyValueIntegerString> getAddressDtoList(Integer coid, Integer subdivid, AddressType type,
			KeyValueIntegerString... prependDtos);

	/**
	 * Returns a KeyValue DTO list of active addresses.
	 * 
	 * @param subdivid       - the subdiv to search for
	 * @param type           - the type of address to search
	 * @param prependNoneDto - whether to prepend None KeyValue vith ID 0
	 * @return
	 */
	List<KeyValue<Integer, String>> getAllActiveSubdivAddressesKeyValue(int subdivid, AddressType type,
			boolean prependNoneDto);

	List<AddressProjectionDTO> getAddressProjectionDTOs(Collection<Integer> addressIds, Integer allocatedCompanyId);	
	
	List<Address> getAllCompanyAddresses(int coid, AddressType type, boolean activeOnly);

	/**
	 * Get addresses for subdiv
	 * 
	 * @param subdivid the ID of the {@link Subdiv} to get addresses for
	 * @param type     the type of {@link Address}, leave blank for all.
	 * @param active   true active only, false inactive, null returns both.
	 * @return the {@link List} of matching {@link Address} entities.
	 */
	List<Address> getAllSubdivAddresses(int subdivid, AddressType type, Boolean active);

	/**
	 * Returns a list of {@link Address}'s belonging to any {@link Subdiv} with a
	 * {@link Corole} of 'Business'.
	 * 
	 * @param type       the type of {@link Address}, leave blank for all.
	 * @param activeOnly determines whether only active addresses should be shown or
	 *                   all addresses.
	 * @return {@link List} of matching {@link Address} entities.
	 */
	List<Address> getBusinessAddresses(AddressType type, boolean activeOnly);

	List<String> getNextNCollectionDates(TransportOption transportOption, int n) throws ParseException;

	AddressPlantillasDTO getPlantillasAddress(int allocatedCompanyId, int addressId);

	/**
	 * Return all Address changed since the specified lastModified Date This allows
	 * data already in Plantillas to be updated. Primarily intended for the
	 * Plantillas to ERP interface.
	 * @param currentPage2 
	 * 
	 * @param      int allocatedCompanyId business company to return settings for
	 * @param Date afterLastModifiedDate (optionally null, then returns all)
	 * @return
	 */
	PagedResultSet<AddressPlantillasDTO> getPlantillasAddresses(int allocatedCompanyId, Date afterLastModifiedDate,
			Integer formerCustomerId, int businessSubdivId, int resultsPerPage, int currentPage);

	/**
	 * return the transport option IN for this address
	 * 
	 * @param addressid
	 * @param direction
	 * @return
	 */
	TransportOption getTransportOptionIn(int addressid, int businessSubdivId);

	/**
	 * method to get next delivery dates for an address as a dwr method
	 * 
	 * @param addrId
	 * @param n
	 * @return
	 * @throws ParseException
	 */
	List<String> webGetNextNCollectionDates(int addrId, int businessSubdivId, int n) throws ParseException;

	public Address findCompanyAddress(int coid, AddressType type, boolean activeOnly, String address);

	TLMAddressProjectionDTO getAddresseById(int addrid);

	PagedResultSet<TLMAddressProjectionDTO> getAllCompanyAddresses(Company company, List<CompanyRole> asList,
			Timestamp companyLastModifiedDate, Timestamp subdivLastModifiedDate, Timestamp addressLastModifiedDate,
			int resultsPerPage, int page);

	List<AddressKeyValue> getForCompany(Integer companyId);
	
	Address getAddressFromSubdiv(Address address);
	
	
}