package org.trescal.cwms.core.company.entity.companyinstructionlink.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

public interface CompanyInstructionLinkDao extends BaseDao<CompanyInstructionLink, Integer>
{
	List<CompanyInstructionLink> getForCompanyAndTypes(Company company, Subdiv allocatedSubdiv, InstructionType... types);
}