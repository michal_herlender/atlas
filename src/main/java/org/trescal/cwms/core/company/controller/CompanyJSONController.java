package org.trescal.cwms.core.company.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.dto.CompanySearchResultWrapper;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.LabelIdDTO;

@Controller
@JsonController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY})
public class CompanyJSONController {

	@Autowired
	private CompanyService companyService;

	public static int MAX_RESULTS = 50;

	/**
	 * Request for Company Search Plugin - requires one element list
	 * 
	 * @param id Identifier of company
	 */
	@RequestMapping(value = "/companyforplugin.json", method = RequestMethod.GET)
	@ResponseBody
	public List<CompanySearchResultWrapper> findSingle(@RequestParam(name = "id", required = true) Integer id) {
		Company company = companyService.get(id);
		CompanySearchResultWrapper wrapped = new CompanySearchResultWrapper(company.getCoid(), company.getConame(),
				company.getCompanyRole().name(), false, company.getLegalIdentifier());
		List<CompanySearchResultWrapper> result = new ArrayList<CompanySearchResultWrapper>();
		result.add(wrapped);
		return result;
	}

	@RequestMapping(value="/searchclienttags.json", method=RequestMethod.GET)
	public @ResponseBody List<LabelIdDTO> getClientTagList(@RequestParam("term") String nameFragment,
													 @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer,String> companyKeyValue) {
		List<LabelIdDTO> tagList = companyService.getClientLabelIdList(nameFragment, companyService.get(companyKeyValue.getKey()), MAX_RESULTS);
		return tagList;
	}
}