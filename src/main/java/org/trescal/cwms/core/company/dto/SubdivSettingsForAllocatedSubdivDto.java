package org.trescal.cwms.core.company.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor @NoArgsConstructor
public class SubdivSettingsForAllocatedSubdivDto {
    private String subdivName;
    private String allocatedForName;
    private Integer contactId;
    private String contactName;
    private String contactEmail;
    private String contactPhone;
}
