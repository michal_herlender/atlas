package org.trescal.cwms.core.company.entity.location.db;

import java.util.Collection;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.dto.LocationSearchResultWrapper;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.LocationDTO;
import org.trescal.cwms.core.company.entity.location.Location_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Repository("LocationDao")
public class LocationDaoImpl extends BaseDaoImpl<Location, Integer> implements LocationDao {
	
	@Override
	protected Class<Location> getEntity() {
		return Location.class;
	}
	
	public Location findLocation(int addrid, String locationName) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Location> cq = cb.createQuery(Location.class);
		
		Root<Location> location = cq.from(Location.class);
		Join<Location,Address> address = location.join(Location_.add);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(location.get(Location_.location),locationName));
		clauses.getExpressions().add(cb.equal(address.get(Address_.addrid),addrid));
		cq.where(clauses);
		cq.select(location);
		
		TypedQuery<Location> query = getEntityManager().createQuery(cq);
		List<Location> results = query.getResultList();
		return results.isEmpty() ? null : results.get(0);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<LocationSearchResultWrapper> getAllActiveAddressLocationsHQL(int addrid, boolean activeOnly) {
		String hql = "select distinct new org.trescal.cwms.core.company.dto.LocationSearchResultWrapper(loc.locationid, loc.location) "
				+ "from Location loc where loc.add.addrid = :addrid ";
		if (activeOnly) hql = hql + "and loc.active = true ";
		hql = hql + "order by loc.location";
		Query query = getSession().createQuery(hql);
		query.setInteger("addrid", addrid);
		List<LocationSearchResultWrapper> locations = query.list();
		return locations;
	}
	
	@Override
	public List<Location> getAllAddressLocations(int addrid, boolean activeOnly) {
		return getResultList(cb -> {
			CriteriaQuery<Location> cq = cb.createQuery(Location.class);
			Root<Location> location = cq.from(Location.class);
			Join<Location, Address> addressJoin = location.join(Location_.add);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(addressJoin.get(Address_.addrid), addrid));
			if(activeOnly){
				clauses.getExpressions().add(cb.isTrue(location.get(Location_.active)));
			}
			cq.where(clauses);
			cq.orderBy(cb.asc(location.get(Location_.location)));
			
			return cq;
		});
	}
	
	@Override
	public List<LocationDTO> getAddressLocations(int addrid, boolean activeOnly) {
		return getResultList(cb -> {
			CriteriaQuery<LocationDTO> cq = cb.createQuery(LocationDTO.class);
			Root<Location> location = cq.from(Location.class);
			Join<Location, Address> addressJoin = location.join(Location_.add);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(addressJoin.get(Address_.addrid), addrid));
			if(activeOnly){
				clauses.getExpressions().add(cb.isTrue(location.get(Location_.active)));
			}
			cq.select(cb.construct(LocationDTO.class, location.get(Location_.locationid),
					location.get(Location_.location)));
			cq.where(clauses);
			cq.orderBy(cb.asc(location.get(Location_.location)));
			
			return cq;
		});
	}
	
	@Override
	public List<Location> getAllBusinessLocations(boolean activeOnly) {
		return getResultList(cb -> {
			CriteriaQuery<Location> cq = cb.createQuery(Location.class);
			Root<Location> location = cq.from(Location.class);
			Join<Location, Address> addressJoin = location.join(Location_.add);
			Join<Address, Subdiv> subdivJoin = addressJoin.join(Address_.sub);
			Join<Subdiv, Company> compJoin = subdivJoin.join(Subdiv_.comp);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(compJoin.get(Company_.companyRole), CompanyRole.BUSINESS));
			if(activeOnly){
				clauses.getExpressions().add(cb.isTrue(location.get(Location_.active)));
			}
			cq.where(clauses);
			cq.orderBy(cb.asc(compJoin.get(Company_.coname)),
					cb.asc(addressJoin.get(Address_.addr1)),
					cb.asc(location.get(Location_.location)));
			return cq;
		});
	}
	
	@Override
	public List<Location> getAllActiveSubdivLocations(Subdiv subdiv) {
		
		return getResultList(cb -> {
			
			CriteriaQuery<Location> cq = cb.createQuery(Location.class);
			Root<Location> location = cq.from(Location.class);
			Join<Location, Address> addrJoin = location.join(Location_.add);
			Join<Address, Subdiv> subdivJoin = addrJoin.join(Address_.sub);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(subdivJoin.get(Subdiv_.subdivid), subdiv.getSubdivid()));
			clauses.getExpressions().add(cb.isTrue(location.get(Location_.active)));
			cq.where(clauses);
			cq.orderBy(
					cb.asc(addrJoin.get(Address_.addr1)),
					cb.asc(location.get(Location_.location)));
			return cq;
		});
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public boolean locationAlreadyExistsAtAddress(String location, int addrId, int excludeLocId) {
		Criteria locationCriteria = getSession().createCriteria(Location.class);
		Criteria addressCriteria = locationCriteria.createCriteria("add");
		addressCriteria.add(Restrictions.idEq(addrId));
		locationCriteria.add(Restrictions.eq("location", location));
		locationCriteria.add(Restrictions.not(Restrictions.idEq(excludeLocId)));
		List<Location> locs = locationCriteria.list();
		return locs.size() > 0;
	}

	@Override
	public List<Location> getLocationsForAutocomplete(Company company, Subdiv subdivision, Address address, String searchTerm) {
		return this.getResultList(cb -> {
			CriteriaQuery<Location> cq = cb.createQuery(Location.class);
			Root<Location> locationsRoot = cq.from(Location.class);
		    Join<Location, Address> locationAddress = locationsRoot.join(Location_.add);
		    Join<Address, Subdiv> addressSubdivision = locationAddress.join(Address_.sub);
		    Predicate conjunction = cb.conjunction();
		    conjunction.getExpressions().add(cb.isTrue(locationsRoot.get(Location_.active)));
		    conjunction.getExpressions().add(cb.equal(addressSubdivision.get(Subdiv_.comp), company));
		    conjunction.getExpressions().add(cb.like(locationsRoot.get(Location_.location),"%" + searchTerm + "%"));
		    if(subdivision != null) conjunction.getExpressions().add(cb.equal(locationAddress.get(Address_.sub), subdivision));
            if(address != null) conjunction.getExpressions().add(cb.equal(locationsRoot.get(Location_.add), address));
		    cq.where(conjunction);
		    return cq;
		});
	}
	
	@Override
	public List<KeyValueIntegerString> getLocationDtos(Collection<Integer> locationIds) {
		if (locationIds == null || locationIds.isEmpty())
			throw new IllegalArgumentException("At least one location ID must be specified!");
		return getResultList(cb -> {
			CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class);
			Root<Location> root = cq.from(Location.class);
			cq.where(root.get(Location_.locationid).in(locationIds));
			cq.select(cb.construct(KeyValueIntegerString.class, 
					root.get(Location_.locationid), 
					root.get(Location_.location)));
			return cq;
		});
	}
	
	@Override
	public List<Location> getAllCompanyLocationsActive(Integer coid){
		
		return getResultList(cb -> {
			CriteriaQuery<Location> cq = cb.createQuery(Location.class);
			Root<Location> location = cq.from(Location.class);
			Join<Location, Address> addrJoin = location.join(Location_.add);
			Join<Address, Subdiv> subdivJoin = addrJoin.join(Address_.sub);
			Join<Subdiv, Company> companyJoin = subdivJoin.join(Subdiv_.comp);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(companyJoin.get(Company_.coid), coid));
			clauses.getExpressions().add(cb.isTrue(location.get(Location_.active)));
			cq.where(clauses);
			cq.orderBy(
					cb.asc(companyJoin.get(Company_.coname)), 
					cb.asc(addrJoin.get(Address_.addr1)),
					cb.asc(location.get(Location_.location)));
			return cq;
		});
	}
}