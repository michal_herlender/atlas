package org.trescal.cwms.core.company.entity.supplier;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.annotation.AllowXHTML;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditation;

@Entity
@Table(name = "supplier")
public class Supplier
{
	private boolean active;
	private boolean allowUse;
	/*
	 *  Specifies period of time to wait (in days) following expiry 
	 *  before this type of supplier approval should be chased
	 */
	private int chaseAfterDays;
	/*
	 *  if set to false auto-e-mails will not be sent to the client
	 *  on expiry, only internally 
	 */	
	private boolean chaseClientOnExpire;
	Set<CompanyAccreditation> companyAccreditations;
	@Deprecated
	private String description;
	private boolean doesNotExpire;
	@Deprecated
	private String name;
	private int supplierid;
	private SupplierApprovalType approvalType;

	@Column(name = "chaseafterdays", nullable = false)
	public int getChaseAfterDays()
	{
		return this.chaseAfterDays;
	}

	@OneToMany(mappedBy = "supplier")
	public Set<CompanyAccreditation> getCompanyAccreditations()
	{
		return this.companyAccreditations;
	}

	@AllowXHTML
	@Length(max = 150)
	@Column(name = "description", length = 150)
	@Deprecated
	private String getDescription()
	{
		return this.description;
	}

	@NotNull
	@Length(min = 1, max = 150)
	@Column(name = "name", length = 150)
	@Deprecated
	public String getName()
	{
		return this.name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "supplierid")
	public int getSupplierid()
	{
		return this.supplierid;
	}

	@Column(name = "active", columnDefinition="tinyint")
	//@Type(type = "boolean")
	public boolean isActive()
	{
		return this.active;
	}

	//@Type(type = "boolean")
	@Column(name = "allowuse", nullable = false, columnDefinition="tinyint")
	public boolean isAllowUse()
	{
		return this.allowUse;
	}

	@Column(name = "chaseclientonexpire", nullable = false, columnDefinition="tinyint")
	public boolean isChaseClientOnExpire()
	{
		return this.chaseClientOnExpire;
	}

	@Column(name = "doesnotexpire", nullable = false, columnDefinition="tinyint")
	public boolean isDoesNotExpire()
	{
		return this.doesNotExpire;
	}
	
	@Column(name = "approvaltype")
	@Enumerated(EnumType.ORDINAL)
	public SupplierApprovalType getApprovalType() {
		return approvalType;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public void setAllowUse(boolean allowUse)
	{
		this.allowUse = allowUse;
	}

	public void setChaseAfterDays(int chaseAfterDays)
	{
		this.chaseAfterDays = chaseAfterDays;
	}

	public void setChaseClientOnExpire(boolean chaseClientOnExpire)
	{
		this.chaseClientOnExpire = chaseClientOnExpire;
	}

	public void setCompanyAccreditations(Set<CompanyAccreditation> companyAccreditations)
	{
		this.companyAccreditations = companyAccreditations;
	}

	@Deprecated
	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setDoesNotExpire(boolean doesNotExpire)
	{
		this.doesNotExpire = doesNotExpire;
	}

	@Deprecated
	public void setName(String name)
	{
		this.name = name;
	}

	public void setSupplierid(int supplierid)
	{
		this.supplierid = supplierid;
	}

	public void setApprovalType(SupplierApprovalType approvalType) {
		this.approvalType = approvalType;
	}

	@Override
	public String toString()
	{
		return Integer.toString(this.supplierid);
	}
}
