package org.trescal.cwms.core.company.entity.companylistmember.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.companylistmember.CompanyListMember;


public interface CompanyListMemberDao extends BaseDao<CompanyListMember, Integer> {

	List<CompanyListMember> getMembersByCompanyList(Integer companyListId);
	
	List<Integer> getMemberCoidsByCompanyLists(List<Integer> companyListIds);
	
}
