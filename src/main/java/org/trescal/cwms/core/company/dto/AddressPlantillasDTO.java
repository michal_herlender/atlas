package org.trescal.cwms.core.company.dto;

import java.util.Date;

public class AddressPlantillasDTO {

	// Last modified information
	private Date lastModifiedAddress;
	private Date lastModifiedSubdiv;
	private Date lastModifiedCompany;
	private Date lastModifiedContact;
	private Date lastModifiedCompanySettings;
	private Date lastModifiedPlantillasSettings;
	// From address, subdiv, and company
	private Integer addrid;
	private Integer subdivid;
	private Integer coid;
	private String legalIdentifier;
	// Whether address, subdiv, and company are active in ERP
	private Boolean addrActive;
	private Boolean companyActive;
	private Boolean subdivActive;
	// From address, subdiv, and company
	private String coname;
	private String subname;
	private String addr1;
	private String addr2;
	private String addr3;
	private String town;
	private String county;
	private String countrycode;
	private String postcode;
	// From contact
	private String email;
	private String firstname;
	private String lastname;
	// Additional settings
	private Integer formerCustomerId;
	private Integer managedByAddrId;
	private Integer certificateAddrId;
	private String textUncertaintyFailure;
	private String textToleranceFailure;
	private Boolean sendByFTP;
	private String ftpServer;
	private String ftpUser;
	private String ftpPassword;
	private Boolean nextDateOnCertificate;
	private Boolean metraClient;
	// Boolean fields from CompanySettingsForAllocatedCompany
	private Boolean contract;
	private Boolean syncToPlantillas;

	public AddressPlantillasDTO(Integer addrid, Integer coid, Integer subdivid, Boolean addrActive,
			Boolean companyActive, Boolean subdivActive, String legalIdentifier, String coname, String subname,
			Date lastModifiedAddress, Date lastModifiedCompany, Date lastModifiedSubdiv, Date lastModifiedContact,
			Date lastModifiedCompanySettings, Date lastModifiedPlantillasSettings, Integer managedByAddrId,
			Integer certificateAddrId, String textUncertaintyFailure, String textToleranceFailure, String addr1,
			String addr2, String addr3, String town, String county, String countrycode, String postcode,
			Boolean sendByFTP, String ftpServer, String ftpUser, String ftpPassword, Boolean contract,
			Boolean nextDateOnCertificate, Boolean metraClient, String email, String firstname, String lastname,
			Integer formerCustomerId, Boolean syncToPlantillas) {
		super();
		this.addrid = addrid;
		this.coid = coid;
		this.subdivid = subdivid;
		this.addrActive = addrActive;
		this.companyActive = companyActive;
		this.subdivActive = subdivActive;
		this.legalIdentifier = legalIdentifier;
		this.coname = coname;
		this.subname = subname;
		this.lastModifiedAddress = lastModifiedAddress;
		this.lastModifiedCompany = lastModifiedCompany;
		this.lastModifiedSubdiv = lastModifiedSubdiv;
		this.lastModifiedContact = lastModifiedContact;
		this.lastModifiedCompanySettings = lastModifiedCompanySettings;
		this.lastModifiedPlantillasSettings = lastModifiedPlantillasSettings;
		this.managedByAddrId = managedByAddrId;
		this.certificateAddrId = certificateAddrId;
		this.textUncertaintyFailure = textUncertaintyFailure;
		this.textToleranceFailure = textToleranceFailure;
		this.addr1 = addr1;
		this.addr2 = addr2;
		this.addr3 = addr3;
		this.town = town;
		this.county = county;
		this.countrycode = countrycode;
		this.postcode = postcode;
		this.sendByFTP = sendByFTP;
		this.ftpServer = ftpServer;
		this.ftpUser = ftpUser;
		this.ftpPassword = ftpPassword;
		this.contract = contract;
		this.nextDateOnCertificate = nextDateOnCertificate;
		this.metraClient = metraClient;
		this.email = email;
		this.firstname = firstname;
		this.lastname = lastname;
		this.formerCustomerId = formerCustomerId;
		this.syncToPlantillas = syncToPlantillas;
	}

	public Date getLastModifiedAddress() {
		return lastModifiedAddress;
	}

	public Date getLastModifiedSubdiv() {
		return lastModifiedSubdiv;
	}

	public Date getLastModifiedCompany() {
		return lastModifiedCompany;
	}

	public Date getLastModifiedContact() {
		return lastModifiedContact;
	}

	public Date getLastModifiedCompanySettings() {
		return lastModifiedCompanySettings;
	}

	public Date getLastModifiedPlantillasSettings() {
		return lastModifiedPlantillasSettings;
	}

	public Integer getAddrid() {
		return addrid;
	}

	public Integer getSubdivid() {
		return subdivid;
	}

	public Integer getCoid() {
		return coid;
	}

	public String getLegalIdentifier() {
		return legalIdentifier;
	}

	public Boolean getAddrActive() {
		return addrActive;
	}

	public Boolean getCompanyActive() {
		return companyActive;
	}

	public Boolean getSubdivActive() {
		return subdivActive;
	}

	public String getConame() {
		return coname;
	}

	public String getSubname() {
		return subname;
	}

	public String getAddr1() {
		return addr1;
	}

	public String getAddr2() {
		return addr2;
	}

	public String getAddr3() {
		return addr3;
	}

	public String getTown() {
		return town;
	}

	public String getCounty() {
		return county;
	}

	public String getCountrycode() {
		return countrycode;
	}

	public String getPostcode() {
		return postcode;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public Integer getFormerCustomerId() {
		return formerCustomerId;
	}

	public Integer getManagedByAddrId() {
		return managedByAddrId;
	}

	public Integer getCertificateAddrId() {
		return certificateAddrId;
	}

	public String getTextUncertaintyFailure() {
		return textUncertaintyFailure;
	}

	public String getTextToleranceFailure() {
		return textToleranceFailure;
	}

	public Boolean getSendByFTP() {
		return sendByFTP;
	}

	public String getFtpServer() {
		return ftpServer;
	}

	public String getFtpUser() {
		return ftpUser;
	}

	public String getFtpPassword() {
		return ftpPassword;
	}

	public Boolean getNextDateOnCertificate() {
		return nextDateOnCertificate;
	}

	public Boolean getMetraClient() {
		return metraClient;
	}

	public Boolean getContract() {
		return contract;
	}

	public Boolean getSyncToPlantillas() {
		return syncToPlantillas;
	}

	public void setLastModifiedAddress(Date lastModifiedAddress) {
		this.lastModifiedAddress = lastModifiedAddress;
	}

	public void setLastModifiedSubdiv(Date lastModifiedSubdiv) {
		this.lastModifiedSubdiv = lastModifiedSubdiv;
	}

	public void setLastModifiedCompany(Date lastModifiedCompany) {
		this.lastModifiedCompany = lastModifiedCompany;
	}

	public void setLastModifiedContact(Date lastModifiedContact) {
		this.lastModifiedContact = lastModifiedContact;
	}

	public void setLastModifiedCompanySettings(Date lastModifiedCompanySettings) {
		this.lastModifiedCompanySettings = lastModifiedCompanySettings;
	}

	public void setLastModifiedPlantillasSettings(Date lastModifiedPlantillasSettings) {
		this.lastModifiedPlantillasSettings = lastModifiedPlantillasSettings;
	}

	public void setAddrid(Integer addrid) {
		this.addrid = addrid;
	}

	public void setSubdivid(Integer subdivid) {
		this.subdivid = subdivid;
	}

	public void setCoid(Integer coid) {
		this.coid = coid;
	}

	public void setLegalIdentifier(String legalIdentifier) {
		this.legalIdentifier = legalIdentifier;
	}

	public void setAddrActive(Boolean addrActive) {
		this.addrActive = addrActive;
	}

	public void setCompanyActive(Boolean companyActive) {
		this.companyActive = companyActive;
	}

	public void setSubdivActive(Boolean subdivActive) {
		this.subdivActive = subdivActive;
	}

	public void setConame(String coname) {
		this.coname = coname;
	}

	public void setSubname(String subname) {
		this.subname = subname;
	}

	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}

	public void setAddr3(String addr3) {
		this.addr3 = addr3;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public void setFormerCustomerId(Integer formerCustomerId) {
		this.formerCustomerId = formerCustomerId;
	}

	public void setManagedByAddrId(Integer managedByAddrId) {
		this.managedByAddrId = managedByAddrId;
	}

	public void setCertificateAddrId(Integer certificateAddrId) {
		this.certificateAddrId = certificateAddrId;
	}

	public void setTextUncertaintyFailure(String textUncertaintyFailure) {
		this.textUncertaintyFailure = textUncertaintyFailure;
	}

	public void setTextToleranceFailure(String textToleranceFailure) {
		this.textToleranceFailure = textToleranceFailure;
	}

	public void setSendByFTP(Boolean sendByFTP) {
		this.sendByFTP = sendByFTP;
	}

	public void setFtpServer(String ftpServer) {
		this.ftpServer = ftpServer;
	}

	public void setFtpUser(String ftpUser) {
		this.ftpUser = ftpUser;
	}

	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}

	public void setNextDateOnCertificate(Boolean nextDateOnCertificate) {
		this.nextDateOnCertificate = nextDateOnCertificate;
	}

	public void setMetraClient(Boolean metraClient) {
		this.metraClient = metraClient;
	}

	public void setContract(Boolean contract) {
		this.contract = contract;
	}

	public void setSyncToPlantillas(Boolean syncToPlantillas) {
		this.syncToPlantillas = syncToPlantillas;
	}

}
