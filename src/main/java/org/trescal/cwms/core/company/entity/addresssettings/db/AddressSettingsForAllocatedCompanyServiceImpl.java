package org.trescal.cwms.core.company.entity.addresssettings.db;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.company.Company;

@Service
public class AddressSettingsForAllocatedCompanyServiceImpl
extends BaseServiceImpl<AddressSettingsForAllocatedCompany, Integer>
implements AddressSettingsForAllocatedCompanyService {

	@Autowired
	private AddressSettingsForAllocatedCompanyDao settingsDao;
	
	@Override
	public AddressSettingsForAllocatedCompany getByCompany(Address address, Company allocatedCompany) {
		return settingsDao.getByCompany(address, allocatedCompany);
	}
	
	@Override
	protected BaseDao<AddressSettingsForAllocatedCompany, Integer> getBaseDao() {
		return settingsDao;
	}

	@Override
	public AddressSettingsForAllocatedCompany initializeForAddress(
			Address address, Company allocatedCompany) {
		AddressSettingsForAllocatedCompany settingsCompany = new AddressSettingsForAllocatedCompany();
		settingsCompany.setAddress(address);
		settingsCompany.setOrganisation(allocatedCompany);
		if (address.getSettingsForAllocatedCompanies() == null) {
			address.setSettingsForAllocatedCompanies(new HashSet<>());
		}
		address.getSettingsForAllocatedCompanies().add(settingsCompany);
		return settingsCompany;
	}
}