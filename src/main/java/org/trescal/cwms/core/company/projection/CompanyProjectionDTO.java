package org.trescal.cwms.core.company.projection;

public class CompanyProjectionDTO {
	// Below result fields always populated by query
	private Integer coid;
	private String coname;
	private Boolean active;
	private Boolean onstop;

	public CompanyProjectionDTO(Integer coid, String coname, Boolean active, Boolean onstop) {
		super();
		this.coid = coid;
		this.coname = coname;
		this.active = active;
		this.onstop = onstop;
	}

	public Integer getCoid() {
		return coid;
	}

	public String getConame() {
		return coname;
	}

	public Boolean getActive() {
		return active;
	}

	public Boolean getOnstop() {
		return onstop;
	}

	public void setCoid(Integer coid) {
		this.coid = coid;
	}

	public void setConame(String coname) {
		this.coname = coname;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public void setOnstop(Boolean onstop) {
		this.onstop = onstop;
	}
}
