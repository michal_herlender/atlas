package org.trescal.cwms.core.company.entity.company;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Setter;
import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.account.entity.bankaccount.BankAccount;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.businessarea.BusinessArea;
import org.trescal.cwms.core.company.entity.businesscompanysettings.BusinessCompanySettings;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditation;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditationComparator;
import org.trescal.cwms.core.company.entity.companygroup.CompanyGroup;
import org.trescal.cwms.core.company.entity.companyhistory.CompanyHistory;
import org.trescal.cwms.core.company.entity.companyhistory.CompanyHistoryComparator;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.corole.Corole;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.editablecorole.EditableCorole;
import org.trescal.cwms.core.company.entity.organisation.OrganisationLevel;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.SubdivComparator;
import org.trescal.cwms.core.documents.images.systemimage.SystemImage;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.supportedcurrency.MultiCurrencyAware;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;
import org.trescal.cwms.spring.model.KeyValue;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

/**
 * Entity representing a company. A company has one {@link Corole}.
 */
@Entity
@Table(name = "company")
@AllArgsConstructor
@Builder
@Setter
public class Company extends Versioned implements ComponentEntity, MultiCurrencyAware, OrganisationLevel {

    private int coid;
    // a single address that is the legal address for the company
    private Address legalAddress;
    private List<BPO> BPOs;
    private BusinessArea businessarea;
    private BusinessCompanySettings businessSettings;
    private Set<CompanyAccreditation> companyAccreditations;
    private String coname;
    private String companyCode;
    private CompanyGroup companyGroup;
    private Corole corole;
    private CompanyRole companyRole;
    private Country country;
    private LocalDate createDate;
    private SupportedCurrency currency;
    private Subdiv defaultSubdiv;
    private File directory;
    private String formerID;
    private String groupName;
    private Set<CompanyHistory> histories;
    private Set<CompanyInstructionLink> instructions;
    private Set<Instrument> instrums;
    private Set<Invoice> invoices;
    private Set<OnBehalfItem> onBehalfJobItems;
    /**
     * @deprecated unused
     */
    private Boolean partnerComp;
    private BigDecimal rate;
    private Set<Subdiv> subdivisions;
    private Set<SystemDefaultApplication> systemDefaults;
    private Set<UserGroup> usergroups;
    private String legalIdentifier;
    private String fiscalIdentifier;
    private Set<CompanySettingsForAllocatedCompany> settingsForAllocatedCompanies;
    private Contact defaultBusinessContact;
    private Set<InstrumentFieldDefinition> instrumentFieldDefinition;
    private Locale documentLanguage;
    private SystemImage logo;
    private List<BankAccount> bankAccounts;

    public Company() {
        partnerComp = Boolean.FALSE;
    }

    public Company(String coname, CompanyRole companyRole, LocalDate createDate) {
        partnerComp = Boolean.FALSE;
        this.coname = coname;
        this.companyRole = companyRole;
        this.createDate = createDate;
    }

    public Company(String coname, LocalDate createDate) {
        this(coname, null, createDate);
    }

    /**
     * @return the bPOs
     */
    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<BPO> getBPOs() {
        return this.BPOs;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "businessareaid")
    public BusinessArea getBusinessarea() {
        return this.businessarea;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "coid")
    @Type(type = "int")
    public int getCoid() {
        return this.coid;
    }

    @Transient
    public Integer getId() {
        return coid;
    }

    @OneToMany(mappedBy = "company")
    @SortComparator(CompanyAccreditationComparator.class)
    public Set<CompanyAccreditation> getCompanyAccreditations() {
        return this.companyAccreditations;
    }

    private static final Map<CompanyRole, Corole> companyRoleToCoroleMap = new HashMap<>();

    @NotNull
    @Length(min = 2, max = 100)
    @Pattern(regexp = "[^,]*", message = "{error.coname.nocommas}")
    @Column(name = "coname", nullable = false, length = 100)
    public String getConame() {
        return this.coname;
    }

    @Length(max = 3)
    @Column(name = "companycode", length = 3)
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * @deprecated use getCompanyRole
     */
    @Transient
    public Corole getCorole() {
        return this.corole;
    }

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "corole")
    public CompanyRole getCompanyRole() {
        return this.companyRole;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "countryid")
    public Country getCountry() {
        return this.country;
    }

    @Column(name = "createdate", columnDefinition = "date")
    public LocalDate getCreateDate() {
        if (this.createDate == null) {
            return LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
        } else {
            return this.createDate;
        }
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "companygroupid")
    public CompanyGroup getCompanyGroup() {
        return this.companyGroup;
    }

    /**
     * The default currency for this company which is a mandatory field.
     * If rate is not set then the default rate for the currency is used.
     */
    @NotNull
    @Override
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currencyid", nullable = false)
    public SupportedCurrency getCurrency() {
        return this.currency;
    }

    @Override
    @Transient
    public Contact getDefaultContact() {
        if (this.defaultSubdiv != null) {
            return this.defaultSubdiv.getDefaultContact();
        }

        return null;
    }

    @Transient
    @Override
    public File getDirectory() {
        return this.directory;
    }

    /*
     * The ID of this company in former systems (at some point, it may need to be business company specific)
     */
    @Length(max = 50)
    @Column(name = "formerid", length = 50)
    public String getFormerID() {
        return formerID;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "defsubdivid")
    public Subdiv getDefaultSubdiv() {
        return this.defaultSubdiv;
    }

    @OneToMany(mappedBy = "comp", cascade = CascadeType.ALL)
    @SortComparator(CompanyHistoryComparator.class)
    public Set<CompanyHistory> getHistories() {
        return this.histories;
    }

    @Override
    @Transient
    public String getIdentifier() {
        return String.valueOf(this.coid);
    }

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL)
    public Set<CompanyInstructionLink> getInstructions() {
        return this.instructions;
    }

    @OneToMany(mappedBy = "comp", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<Instrument> getInstrums() {
        return this.instrums;
    }

    @OneToMany(mappedBy = "comp", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<Invoice> getInvoices() {
        return this.invoices;
    }

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<OnBehalfItem> getOnBehalfJobItems() {
        return this.onBehalfJobItems;
    }

    /*
     * The group name of this company; ultimately will migrate data to companyGroup entity selection
     */
    @Length(max = 255)
    @Column(name = "groupname")
    public String getGroupName() {
        return groupName;
    }

    @Override
    @Transient
    public ComponentEntity getParentEntity() {
        return null;
    }

    @Override
    @Transient
    public List<Email> getSentEmails() {
        return null;
    }

    @OneToMany(mappedBy = "comp", fetch = FetchType.LAZY)
    @SortComparator(SubdivComparator.class)
    public Set<Subdiv> getSubdivisions() {
        return this.subdivisions;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "company")
    public Set<SystemDefaultApplication> getSystemDefaults() {
        return this.systemDefaults;
    }

    @OneToMany(mappedBy = "company")
    public Set<UserGroup> getUsergroups() {
        return this.usergroups;
    }

    /*
     * Legal identification number of company; for France companies, the SIRET number.
     */
    @Column(name = "legalidentifier")
    @NotNull
    public String getLegalIdentifier() {
        return legalIdentifier;
    }

    /**
     * The default rate for the currency for this company (if one is set). If
     * rate is null but currency is not then the default rate for that currency
     * is used instead.
     */
    @Override
    @Column(name = "rate", precision = 10, scale = 2)
    public BigDecimal getRate() {
        return this.rate;
    }

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL)
    public Set<CompanySettingsForAllocatedCompany> getSettingsForAllocatedCompanies() {
        return settingsForAllocatedCompanies;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "defaultbusinesscontact", nullable = false)
    public Contact getDefaultBusinessContact() {
        return this.defaultBusinessContact;
    }

    // Stores global settings for CompanyRole.BUSINESS companies (nullable)
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "businesssettingsid")
    public BusinessCompanySettings getBusinessSettings() {
        return businessSettings;
    }

    @Override
    @Transient
    public boolean isAccountsEmail() {
        return false;
    }

    @Column(name = "partnercomp", columnDefinition = "bit")
    public Boolean getPartnerComp() {
        return this.partnerComp;
    }


    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY)
    public Set<InstrumentFieldDefinition> getInstrumentFieldDefinition() {
        return this.instrumentFieldDefinition;
    }


    // The single legal registration address - may be null when company first created before activation
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "legaladdressid")
    public Address getLegalAddress() {
        return legalAddress;
    }

    // Mandatory, essential field for automatic document and comment generation, not nullable
    @NotNull
    @Column(name = "documentlanguage", nullable = false)
    public Locale getDocumentLanguage() {
        return documentLanguage;
    }

    /**
     * @deprecated use setCompanyRole
     */
    public void setCorole(Corole corole) {
        this.corole = corole;
        this.companyRole = CompanyRole.valueOf(corole.getCorole());
    }

    /*
     * For tax identification number, for EU companies the VAT number.
     */
    @Column(name = "fiscalidentifier")
    // TODO add @NotNull and set upon creation
    public String getFiscalIdentifier() {
        return fiscalIdentifier;
    }

    private Corole createCoroleFromCompanyRole(CompanyRole companyRole) {
        Corole corole = new Corole();
        companyRoleToCoroleMap.put(companyRole, corole);
        corole.setCoroleid(companyRole.getKey());
        corole.setCorole(companyRole.toString());
        corole.setDepartmentEnabled(companyRole.isDepartmentEnabled());
        corole.setDescription(companyRole.toString());
        corole.setEditable(companyRole.isEditable());
        Set<EditableCorole> editableFrom = new HashSet<>();
        int ecid = 0;
        for (CompanyRole cr : companyRole.editableFrom()) {
            EditableCorole ec = new EditableCorole();
            if (companyRoleToCoroleMap.containsKey(cr)) ec.setEditableFrom(companyRoleToCoroleMap.get(cr));
            else ec.setEditableFrom(createCoroleFromCompanyRole(cr));
            ec.setEditableTo(corole);
            ec.setId(++ecid);
            editableFrom.add(ec);
        }
        corole.setEditableFrom(editableFrom);
        return corole;
    }

    public void setCompanyRole(CompanyRole companyRole) {
        if (companyRole != null) this.setCorole(createCoroleFromCompanyRole(companyRole));
        this.companyRole = companyRole;

    }

    @Override
    public void setSentEmails(List<Email> emails) {

    }

    @OneToOne(mappedBy = "businessCompany", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    public SystemImage getLogo() {
        return logo;
    }

    @OneToMany(mappedBy = "company", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public List<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    @Override
    public String toString() {
        return this.coid + " : " + this.coname;
    }

    public KeyValue<Integer, String> dto() {
        return new KeyValue<>(coid, coname);
    }
}