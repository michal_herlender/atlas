package org.trescal.cwms.core.company.entity.businesscompanylogo;

public enum BusinessCompanyLogo {
	TRESCAL("img/trescal/docs_Logo.png"), TECHNALIA("img/trescal/Tecnalia-Trescal.gif");
	
	private BusinessCompanyLogo(String relativeURL) {
		this.relativeUrl = relativeURL;
	}
	private String relativeUrl;
	public String getRelativeUrl() {
		return relativeUrl;
	}
}
