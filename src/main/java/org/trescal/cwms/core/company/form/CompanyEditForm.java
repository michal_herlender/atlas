package org.trescal.cwms.core.company.form;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.*;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;

@Data @NoArgsConstructor
@Builder @AllArgsConstructor
public class CompanyEditForm
{
	private int coid;
	private Integer businessareaid;
	private Integer businessCompanyId;	// Used for validation
	@NotNull
	private Integer primaryBusinessContactId;
	@NotNull
	private Boolean changeBusinessContact;
	private Timestamp lastModified;
	private CompanyRole companyRole;
	@NotNull
	@Length(min = 2, max = 100)
	@Pattern(regexp="[^,]*", message="{error.coname.nocommas}")
	private String coname;
	private String oldConame;
	private String companyCode;
	private String companyRef;
	private boolean contract;
	private Integer countryid;
	private String groupName;
	private boolean saveNameChange;
	private boolean specificCurrency;
	private Integer specificCurrencyId;
	private boolean specificRate;
	private BigDecimal specificRateValue;
	private boolean syncToPlantillas;
	private String vatCode;
	private boolean taxable;
	@NotNull
	private String documentLanguage;
	private String formerId;
	private Integer companyGroupId;
	private Boolean requireSupplierCompanyList;
}