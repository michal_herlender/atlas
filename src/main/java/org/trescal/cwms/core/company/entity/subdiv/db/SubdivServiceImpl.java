package org.trescal.cwms.core.company.entity.subdiv.db;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.dto.SubdivSearchResultWrapper;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.businesssubdivisionsettings.BusinessSubdivisionSettings;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.companysettings.db.SubdivSettingsForAllocatedSubdivService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.form.SubdivEditForm;
import org.trescal.cwms.core.company.projection.SubdivProjectionDTO;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentDao;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.spring.model.KeyValue;

import java.io.File;
import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service("SubdivService")
public class SubdivServiceImpl extends BaseServiceImpl<Subdiv, Integer> implements SubdivService {
	@Autowired
	private AddressService addServ;
	@Autowired
	private ContactService contactService;
	@Autowired
	private SubdivDao subDao;
	@Autowired
	private SystemComponentDao scDao;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private SubdivSettingsForAllocatedSubdivService subdivSettingsService;
	@Autowired
	private MessageSource messages;

	@Override
	protected BaseDao<Subdiv, Integer> getBaseDao() {
		return this.subDao;
	}
	
	@Override
	public Long countBySiretNumber(String siretNumber, CompanyRole companyRole, Integer excludeSubdivId) {
		if (companyRole == null) throw new IllegalArgumentException("companyRole must not be null");
		Set<CompanyRole> companyRoles = EnumSet.copyOf(companyRole.editableTo());
		companyRoles.add(companyRole);
		return this.subDao.countBySiretNumber(siretNumber, companyRoles, excludeSubdivId);
	}
	
	@Override
	public Long countCompanySubdivsByName(Integer coid, String subdivName, Integer excludeSubdivId) {
		return this.subDao.countCompanySubdivsByName(coid, subdivName, excludeSubdivId);
	}

	@Override
	public List<Subdiv> getAllActiveCompanySubdivs(Company company) {
		return company.getSubdivisions().stream().filter(Subdiv::isActive)
				.sorted(Comparator.comparing(Subdiv::getSubname)).collect(Collectors.toList());
	}

	public List<SubdivSearchResultWrapper> getAllActiveCompanySubdivsHQL(int coid) {
		return this.subDao.getAllActiveByCompany(coid);
	}

	public List<Subdiv> getAllActiveSubdivsByRole(CompanyRole companyRole) {
		return this.subDao.getAllActiveSubdivsByRole(companyRole);
	}

	@Override
	public Set<Subdiv> getBusinessCompanySubdivsForUser(User user) {
		return user.getUserRoles().stream().map(Allocated::getOrganisation).collect(Collectors.toSet());
	}

	@Override
	public List<SubdivKeyValue> getAllActiveCompanySubdivsForUser(User user, Integer coid) {
		List<SubdivKeyValue> subdivs = user.getUserRoles().stream().map(ur -> new SubdivKeyValue(ur.getOrganisation())).distinct().sorted(Comparator.comparing(KeyValue::getValue)).collect(Collectors.toList());
		subdivs.removeIf(sub -> this.get(sub.getKey()).getComp().getCoid() != coid || !this.get(sub.getKey()).isActive());
		return subdivs;
	}
	
	@Override
	public void editSubdiv(SubdivEditForm sf, Integer allocatedSubdivId) {
		Subdiv subdiv = this.get(sf.getSubdivId());
		Subdiv allocatedSubdiv = this.get(allocatedSubdivId);
		subdiv.setSubname(sf.getSubdivName());
		subdiv.setSubdivCode(sf.getSubdivCode());
		if (sf.getFormerId() != null) {
			subdiv.setFormerId(sf.getFormerId());
		}
		if (StringUtils.isNotBlank(sf.getSiretNumber()))
			subdiv.setSiretNumber(sf.getSiretNumber());
		if (subdiv.getComp().getCompanyRole() == CompanyRole.BUSINESS) {
			Contact contact = contactService.get(sf.getDefaultBusinessContact());
			if (subdiv.getBusinessSettings() == null) {
				BusinessSubdivisionSettings businessSettings = new BusinessSubdivisionSettings();
				businessSettings.setSubdiv(subdiv);
				businessSettings.setEbitdaHourlyCostRate(BigDecimal.valueOf(0, 2));
				businessSettings.setWorkInstructionReference(false);
				businessSettings.setDefaultBusinessContact(contact);
				subdiv.setBusinessSettings(businessSettings);
			}
			subdiv.setAnalyticalCenter(sf.getAnalyticalCenter());
			subdiv.getBusinessSettings().setDefaultTurnaround(sf.getDefaultTurnaround());
			subdiv.getBusinessSettings().setDefaultBusinessContact(contact);
		}
		SubdivSettingsForAllocatedSubdiv settings = this.subdivSettingsService.get(subdiv, allocatedSubdiv);
		if (sf.getDefaultBusinessContactForAllocatedSubdiv() > 0) {
			if (settings == null) {
				settings = new SubdivSettingsForAllocatedSubdiv();
				settings.setSubdiv(subdiv);
				settings.setOrganisation(allocatedSubdiv);
				subdiv.getSubdivSettings().add(settings);
			}
			Contact contact = contactService.get(sf.getDefaultBusinessContactForAllocatedSubdiv());
			settings.setDefaultBusinessContact(contact);
		} else if (settings != null && sf.getDefaultBusinessContact() == 0) {
			subdiv.getSubdivSettings().remove(settings);
		}
	}
	
	@Override
	public List<SubdivKeyValue> searchBusinessSubdivsByName(String partialSubdivName) {
		return this.subDao.searchBusinessSubdivsByName(partialSubdivName);
	}

	@Override
	public boolean subdivBelongsToBusinessCompany(int subdivid) {
		boolean belongs = false;
		Subdiv s = this.get(subdivid);
		if (s != null) {
			if (s.getComp().getCompanyRole() == CompanyRole.BUSINESS) {
				return true;
			}
		}

		return belongs;
	}

	@Override
	public void updateSubdivAddressDefault(int subdivid, int addressid) {
		Subdiv sub = this.get(subdivid);
		Address addr = this.addServ.get(addressid);

		if (addr.getSub().getSubdivid() == sub.getSubdivid()) {
			sub.setDefaultAddress(addr);
		}
	}

	@Override
	public void updateSubdivContactDefault(int subdivid, int personid) {
		Subdiv sub = this.get(subdivid);
		Contact con = this.contactService.get(personid);

		if (con.getSub().getSubdivid() == sub.getSubdivid()) {
			sub.setDefaultContact(con);
		}
	}

	@Override
	public Subdiv merge(Subdiv subdiv) {
		subdiv = this.subDao.merge(subdiv);
		File path = this.compDirServ.getDirectory(this.scDao.findComponent(Component.SUBDIV),
				String.valueOf(subdiv.getComp().getCoid()), null);
		File subdivDirectory = new File(path, subdiv.getSubname().replaceAll("[^\\w.-]", "_"));
		subdivDirectory.mkdirs();
		return subdiv;
	}

	@Override
	public void save(Subdiv subdiv) {
		this.subDao.persist(subdiv);
		File path = this.compDirServ.getDirectory(this.scDao.findComponent(Component.SUBDIV),
				String.valueOf(subdiv.getComp().getCoid()), null);
		File subdivDirectory = new File(path, subdiv.getSubname().replaceAll("[^\\w.-]", "_"));
		subdivDirectory.mkdirs();
	}

	@Override
	public List<KeyValue<Integer,String>> getAllActiveCompanySubdivsKeyValue(int companyid, boolean prependNoneDto) {
		String noneMessage = this.messages.getMessage("company.none", null, "None", LocaleContextHolder.getLocale());
		return Stream.of(prependNoneDto ? Stream.of(new KeyValue<>(0, noneMessage)) : Stream.<KeyValue<Integer, String>>empty(),
				subDao.getAllActiveByCompany(companyid).stream().map(s -> new KeyValue<>(s.getSubdivid(), s.getSubname()))
		).flatMap(Function.identity()).collect(Collectors.toList());
	}

	@Override
	public List<SubdivKeyValue> getAllSubdivFromCompany(int coid, Boolean isActive) {
		return subDao.getAllSubdivFromCompany(coid, isActive);
	}
	
	@Override
	public List<SubdivProjectionDTO> getSubdivProjectionDTOs(Collection<Integer> sourceSubdivIds, Integer allocatedCompanyId) {
		List<SubdivProjectionDTO> result = Collections.emptyList();
		if (!sourceSubdivIds.isEmpty()) {
			result = subDao.getSubdivProjectionDTOs(sourceSubdivIds, allocatedCompanyId);
		}
		return result;
	}

	@Override
	public List<SubdivProjectionDTO> getSubdivProjectionDTOsByCompany(Integer allocatedCompanyId) {
		return this.subDao.getSubdivProjectionDTOsByCompany(allocatedCompanyId);
	}

}