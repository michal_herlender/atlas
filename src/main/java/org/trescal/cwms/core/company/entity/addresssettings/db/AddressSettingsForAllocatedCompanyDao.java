package org.trescal.cwms.core.company.entity.addresssettings.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.company.Company;

public interface AddressSettingsForAllocatedCompanyDao extends BaseDao<AddressSettingsForAllocatedCompany, Integer> {

	public AddressSettingsForAllocatedCompany getByCompany(Address address, Company allocatedCompany);
}