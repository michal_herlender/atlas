package org.trescal.cwms.core.company.dto;

import java.util.ArrayList;
import java.util.List;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.spring.model.KeyValue;

public class CompanyDTO {

	private Integer coid;
	private boolean active;
	private boolean onStop;
	private String coname;
	private String companyRole;
	private Integer defSubdivId = 0;
	private List<KeyValue<Integer, String>> subDivs = new ArrayList<KeyValue<Integer, String>>();

	public CompanyDTO(Integer coid, String coname, boolean active, boolean onStop) {
		super();
		this.coid = coid;
		this.coname = coname;
		this.active = active;
		this.onStop = onStop;
	}

	public CompanyDTO(Company company, CompanySettingsForAllocatedCompany settings) {
		this.coid = company.getCoid();
		this.active = settings.isActive();
		this.onStop = settings.isOnStop();
		this.coname = company.getConame();
		this.companyRole = company.getCompanyRole().getMessage();
		if (company.getDefaultSubdiv() != null)
			this.defSubdivId = company.getDefaultSubdiv().getSubdivid();
		for (Subdiv sub : company.getSubdivisions()) {
			subDivs.add(new KeyValue<Integer, String>(sub.getSubdivid(), sub.getSubname()));
		}
	}

	public Integer getCoid() {
		return coid;
	}

	public boolean isActive() {
		return active;
	}

	public boolean isOnStop() {
		return onStop;
	}

	public boolean isTrusted() {
		return !onStop;
	}

	public String getConame() {
		return coname;
	}

	public Integer getDefSubdivId() {
		return defSubdivId;
	}

	public String getCompanyRole() {
		return companyRole;
	}

	public List<KeyValue<Integer, String>> getSubDivs() {
		return subDivs;
	}
}