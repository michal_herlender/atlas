package org.trescal.cwms.core.company.dto;

import org.trescal.cwms.core.company.entity.department.Department;

/**
 * DTO object that holds a {@link Department} and a boolean indicating if a user
 * can amend said {@link Department}.
 * 
 * @author Richard
 */
public class EditableDepartmentDTO
{
	Department department;
	boolean editable;

	public Department getDepartment()
	{
		return this.department;
	}

	public boolean isEditable()
	{
		return this.editable;
	}

	public void setDepartment(Department department)
	{
		this.department = department;
	}

	public void setEditable(boolean editable)
	{
		this.editable = editable;
	}

}
