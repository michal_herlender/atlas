package org.trescal.cwms.core.company.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.AutoPopulatingList;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.form.AddEditFlexibleFieldDefinitionForm;
import org.trescal.cwms.core.company.form.FlexibleFieldLibraryValueDTO;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldLibraryValue;
import org.trescal.cwms.core.instrument.entity.instrumentfield.db.InstrumentFieldDefinitionService;
import org.trescal.cwms.core.instrument.entity.instrumentfield.db.InstrumentFieldLibraryValueService;
import org.trescal.cwms.core.tools.FieldType;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@RequestMapping(value = "addeditflexiblefielddefinition.htm")
public class AddEditFlexibleFieldDefinitionController {

	@Autowired
	CompanyService companyService;

	@Autowired
	InstrumentFieldDefinitionService instrumentFieldDefinitionService;

	@Autowired
	InstrumentFieldLibraryValueService instrumentFieldLibraryValueService;

	@ModelAttribute("command")
	public AddEditFlexibleFieldDefinitionForm initializeFormBackingObject(
			@RequestParam(name = "coid", required = false, defaultValue = "0") Integer coid,
			@RequestParam(name = "fielddefid", required = false, defaultValue = "0") Integer fielddefid)
			throws Exception {
		if (fielddefid == 0 && coid == 0)
			throw new Exception("company id and field definition id parameters both empty");

		AddEditFlexibleFieldDefinitionForm command = new AddEditFlexibleFieldDefinitionForm();
		AutoPopulatingList<FlexibleFieldLibraryValueDTO> dtoList = new AutoPopulatingList<>(
				FlexibleFieldLibraryValueDTO.class);
		if (fielddefid == 0) {
			command.setUpdatable(true);
		} else {
			InstrumentFieldDefinition fieldDefinition = instrumentFieldDefinitionService.get(fielddefid);
			command.setName(fieldDefinition.getName());
			command.setUpdatable(fieldDefinition.getIsUpdatable());
			command.setTypeId(fieldDefinition.getFieldType().ordinal());
			if (fieldDefinition.getFieldType().equals(FieldType.SELECTION)
					&& fieldDefinition.getInstrumentFieldLibraryValues() != null
					&& fieldDefinition.getInstrumentFieldLibraryValues().size() > 0) {
				fieldDefinition.getInstrumentFieldLibraryValues().stream().forEach(v -> dtoList
						.add(new FlexibleFieldLibraryValueDTO(v.getInstrumentFieldLibraryid(), v.getName())));
			}
		}
		command.setValues(dtoList);
		command.setCoid(coid);
		command.setFieldDefinitionId(fielddefid);
		return command;
	}

	@ModelAttribute("fieldtypes")
	public List<KeyValue<Integer, String>> initializeFieldTypes() {
		return Arrays.stream(FieldType.values()).map(f -> new KeyValue<Integer, String>(f.ordinal(), f.getName()))
				.collect(Collectors.toList());
	}

	@RequestMapping(method = RequestMethod.GET)
	public String displayForm() {
		return "/trescal/core/company/addeditflexiblefielddefinition";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String processForm(@ModelAttribute("command") AddEditFlexibleFieldDefinitionForm command) throws Exception {
		if (command.getFieldDefinitionId() == 0 && command.getCoid() == 0)
			throw new Exception("company id and field definition id both empty");
		InstrumentFieldDefinition fieldDefinition;
		if (command.getFieldDefinitionId() == 0) {
			fieldDefinition = new InstrumentFieldDefinition();
			fieldDefinition.setClient(companyService.get(command.getCoid()));
		} else {
			fieldDefinition = instrumentFieldDefinitionService.get(command.getFieldDefinitionId());
		}
		fieldDefinition.setName(command.getName());
		fieldDefinition.setFieldType(FieldType.values()[command.getTypeId()]);
		fieldDefinition.setIsUpdatable(command.getUpdatable());
		fieldDefinition = instrumentFieldDefinitionService.merge(fieldDefinition);
		// Add or Update library values but only if type is SELECT
		if (command.getValues() != null && !command.getValues().isEmpty()
				&& fieldDefinition.getFieldType().equals(FieldType.SELECTION)) {
			for (FlexibleFieldLibraryValueDTO valueDTO : command.getValues()) {
				InstrumentFieldLibraryValue libraryValue;
				if (valueDTO.getLibraryValueId() > 0) {
					libraryValue = instrumentFieldLibraryValueService.get(valueDTO.getLibraryValueId());
					libraryValue.setName(valueDTO.getLibraryValueName());
				} else {
					libraryValue = new InstrumentFieldLibraryValue();
					libraryValue.setInstrumentFieldDefinition(fieldDefinition);
					libraryValue.setName(valueDTO.getLibraryValueName());
				}
				instrumentFieldLibraryValueService.merge(libraryValue);
			}
		}
		return "redirect:/viewcomp.htm?coid=" + fieldDefinition.getClient().getCoid() + "&loadtab=flexiblefields-tab";
	}
}