package org.trescal.cwms.core.company.form;

import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.mailgrouptype.MailGroupType;
import org.trescal.cwms.core.company.interceptor.UnescapeString;
import org.trescal.cwms.core.userright.dto.UserRoleDTO;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Data
public class ContactEditForm {
	private Integer addressid;
	@Valid
	private Contact contact;
	private Map<Integer, String> departmentroles;
	private Map<Integer, String> departments;
	private Integer groupid;
	private Integer locationid;
	private Map<MailGroupType, Boolean> mailgroups;
	private String message;
	private String password;
	private String password2;
	private String oldPassword;
	private boolean sendLoginEmail;
	private Integer printerid;
	@Size(max = 10)
	private String title;
	private Locale locale;
	@Email
	@Size(max = 200)
	@NotEmpty()
	private String email;
	private List<UserRoleDTO> userRoles;
	private Integer allocatedSubdivId;

	// Validation group for password (not always entered)
	public interface PasswordGroup {
	}


	@UnescapeString
	public void setEmail(String email) {
		this.email = email.trim();
	}
	
}