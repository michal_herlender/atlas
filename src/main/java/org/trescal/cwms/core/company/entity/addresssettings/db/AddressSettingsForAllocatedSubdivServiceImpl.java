package org.trescal.cwms.core.company.entity.addresssettings.db;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.dto.AddressTransportOptionsComponentDTO;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;

@Service
public class AddressSettingsForAllocatedSubdivServiceImpl
		extends BaseServiceImpl<AddressSettingsForAllocatedSubdiv, Integer>
		implements AddressSettingsForAllocatedSubdivService {

	@Autowired
	private AddressSettingsForAllocatedSubdivDao addressSettingsForAllocatedSubdivDao;
	@Autowired
	private TransportOptionService transportOptionService;

	@Override
	public AddressSettingsForAllocatedSubdiv getBySubdiv(Address address, Subdiv allocatedSubdiv) {
		return this.addressSettingsForAllocatedSubdivDao.getBySubdiv(address.getAddrid(),
				allocatedSubdiv.getSubdivid());
	}

	@Override
	protected BaseDao<AddressSettingsForAllocatedSubdiv, Integer> getBaseDao() {
		return this.addressSettingsForAllocatedSubdivDao;
	}

	@Override
	public AddressSettingsForAllocatedSubdiv initializeForAddress(Address address, Subdiv allocatedSubdiv) {
		AddressSettingsForAllocatedSubdiv settingsSubdiv = new AddressSettingsForAllocatedSubdiv();
		settingsSubdiv.setAddress(address);
		settingsSubdiv.setOrganisation(allocatedSubdiv);
		if (address.getSettingsForAllocatedSubdivs() == null) {
			address.setSettingsForAllocatedSubdivs(new HashSet<>());
		}
		address.getSettingsForAllocatedSubdivs().add(settingsSubdiv);
		return settingsSubdiv;
	}

	@Override
	public List<AddressTransportOptionsComponentDTO> getTransportOptions(Integer addressId) {
		Locale locale = LocaleContextHolder.getLocale();
		return this.addressSettingsForAllocatedSubdivDao.getTransportOptions(addressId, locale);
	}

	@Override
	public void setTransportOptions(Address address, Subdiv allocatedSubdiv, Integer transportInId,
			Integer transportOutId) {
		AddressSettingsForAllocatedSubdiv settings = this.getBySubdiv(address, allocatedSubdiv);
		if (settings == null) {
			settings = new AddressSettingsForAllocatedSubdiv();
			settings.setAddress(address);
			settings.setOrganisation(allocatedSubdiv);
		}
		TransportOption transportIn = transportInId == null || transportInId == 0 ? null
				: this.transportOptionService.get(transportInId);
		TransportOption transportOut = transportOutId == null || transportOutId == 0 ? null
				: this.transportOptionService.get(transportOutId);
		settings.setTransportIn(transportIn);
		settings.setTransportOut(transportOut);
		if (address.getSettingsForAllocatedSubdivs() == null)
			address.setSettingsForAllocatedSubdivs(new HashSet<AddressSettingsForAllocatedSubdiv>());
		address.getSettingsForAllocatedSubdivs().add(settings);
	}
}