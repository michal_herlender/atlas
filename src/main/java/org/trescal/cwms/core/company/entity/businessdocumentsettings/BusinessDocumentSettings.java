package org.trescal.cwms.core.company.entity.businessdocumentsettings;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.company.Company;

import lombok.Setter;

/**
 * Provides settings for document customization by business company
 * It's intended that there is one record providing global defaults, and 
 * currently - optionally at most one additional record per business company.   
 * 
 * If this gets too large, consider splitting into different entities by document type. 
 */
@Setter
@Entity
@Table(name="businessdocumentsettings")
public class BusinessDocumentSettings {
	private int id;
	private Boolean globalDefault;
	private Company businessCompany;
	private Boolean invoiceAmountText;
	private Boolean recipientAddressOnLeft;
	private Boolean deliveryNoteSignedCertificates;
	private Boolean subdivisionFiscalIdentifier ;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	
	/**
	 * Indicates that this record provides the "global default" settings if not defined by company
	 * @return Boolean
	 */
	@NotNull
	@Column(name = "globalDefault", nullable = false, columnDefinition = "bit")
	public Boolean getGlobalDefault() {
		return globalDefault;
	}

	/**
	 * The (optional) business company that this settings record is for
	 * @return Company
	 */
	@ManyToOne(fetch=FetchType.LAZY)	
	@JoinColumn(name="businesscompanyid", foreignKey=@ForeignKey(name = "FK_businessdocsettings_businesscompany"), nullable=true)
	public Company getBusinessCompany() {
		return businessCompany;
	}
	
	/**
	 * Indicates whether invoices should spell out the amount due in words
	 * (e.g. Morocco) 
	 * @return Boolean
	 */
	@NotNull
	@Column(name = "invoiceAmountText", nullable = false, columnDefinition = "bit")
	public Boolean getInvoiceAmountText() {
		return invoiceAmountText;
	}

	/**
	 * Indicates whether the recipient address should appear on the left
	 * (e.g. to fit Germany envelope requirements) 
	 * @return
	 */
	@NotNull
	@Column(name = "recipientAddressOnLeft", nullable = false, columnDefinition = "bit")
	public Boolean getRecipientAddressOnLeft() {
		return recipientAddressOnLeft;
	}

	/**
	 * Indicates whether only "signed" certificates should appear on delivery notes
	 * (e.g. to fit French requirements)
	 * The default configuration includes other statuses (see ProjectedJobDeliveryItemCertificateDataSet) 
	 * @return
	 */
	@NotNull
	@Column(name = "deliveryNoteSignedCertificates", nullable = false, columnDefinition = "bit")
	public Boolean getDeliveryNoteSignedCertificates() {
		return deliveryNoteSignedCertificates;
	}
	@NotNull
	@Column(name = "subdivisionFiscalIdentifier", nullable = false, columnDefinition = "bit")
	public Boolean getSubdivisionFiscalIdentifier() {
		return subdivisionFiscalIdentifier;
	}
}
