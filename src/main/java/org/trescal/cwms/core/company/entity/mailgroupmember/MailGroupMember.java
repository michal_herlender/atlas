package org.trescal.cwms.core.company.entity.mailgroupmember;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.mailgrouptype.MailGroupType;

/**
 * Represents a {@link Contact}'s membership of a {@link MailGroupType}.
 * 
 * @author Richard
 */
@Entity
@Table(name = "mailgroupmember", uniqueConstraints = { @UniqueConstraint(columnNames = { "personid", "mailgroupid" }) })
public class MailGroupMember extends Auditable
{
	private Contact contact;
	private int id;
	private MailGroupType mailGroupType;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", unique = false, nullable = false, insertable = true, updatable = true)
	public Contact getContact()
	{
		return this.contact;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "memberid", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "mailgroupid", unique = false, nullable = false, insertable = true, updatable = true)
	public MailGroupType getMailGroupType()
	{
		return this.mailGroupType;
	}

	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setMailGroupType(MailGroupType mailGroupType)
	{
		this.mailGroupType = mailGroupType;
	}
}
