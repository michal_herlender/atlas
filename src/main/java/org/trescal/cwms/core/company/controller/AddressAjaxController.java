package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.company.dto.AddrSearchResultWrapper;
import org.trescal.cwms.core.company.entity.address.db.AddressService;

import java.util.List;

@RestController
@RequestMapping("address")
public class AddressAjaxController {

    @Autowired
    private AddressService addressService;

    @GetMapping("allActivesBySubdivAndType.json")
    List<AddrSearchResultWrapper> getAllActivesBySubdivAndType(@RequestParam Integer subdivId, @RequestParam String addressType) {
        return addressService.getAllActiveSubdivAddressesHQL(subdivId, addressType);
    }
}
