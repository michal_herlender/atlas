package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.form.CompanyRecallConfigurationForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.RecallAttachmentType;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.RecallCompanyConfiguration;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.RecallPeriodStart;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.RecallSubjectKey;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.RecallTemplateType;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.db.RecallCompanyConfigurationService;

@IntranetController
@Controller
public class CompanyEditRecallConfigurationController {

	@Autowired
	private CompanyService companyService;
	@Autowired
	private RecallCompanyConfigurationService rccService;

	private static final String VIEW_NAME = "trescal/core/company/companyrecall";

	@ModelAttribute("command")
	public CompanyRecallConfigurationForm formBackingObject(@RequestParam(name = "coid", required = true) int coid) {
		CompanyRecallConfigurationForm form = new CompanyRecallConfigurationForm();
		RecallCompanyConfiguration recallConfig = rccService.get(coid);
		if (recallConfig != null) {
			form.setActive(recallConfig.getActive());
			form.setFutureMonths(recallConfig.getFutureMonths());
			form.setPeriodStart(recallConfig.getPeriodStart());
			form.setCustomText(recallConfig.getCustomText());
		} else {
			form.setActive(false);
			form.setFutureMonths(0);
			form.setPeriodStart(RecallPeriodStart.CURRENT_DATE);
			form.setCustomText("");
		}
		return form;
	}

	@RequestMapping(value = "editcompanyrecall.htm", method = RequestMethod.GET)
	public String referenceData(@RequestParam(name = "coid", required = true) int coid, Model model) {
		Company company = this.companyService.get(coid);
		model.addAttribute("company", company);
		model.addAttribute("periodStartTypes", RecallPeriodStart.values());

		return VIEW_NAME;
	}

	private void copyRecallFields(CompanyRecallConfigurationForm form, RecallCompanyConfiguration recallConfig) {
		recallConfig.setActive(form.getActive());
		recallConfig.setCustomText(form.getCustomText());
		recallConfig.setFutureMonths(form.getFutureMonths());
		recallConfig.setPeriodStart(form.getPeriodStart());
	}

	@RequestMapping(value = "editcompanyrecall.htm", method = RequestMethod.POST)
	public String onSubmit(@RequestParam(name = "coid", required = true) int coid, Model model,
			@Validated @ModelAttribute("command") CompanyRecallConfigurationForm form, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData(coid, model);
		}
		RecallCompanyConfiguration recallConfig = this.rccService.get(coid);
		if (recallConfig == null) {
			Company company = this.companyService.get(coid);
			recallConfig = new RecallCompanyConfiguration();
			recallConfig.setAttachmentType(RecallAttachmentType.DEFAULT);
			recallConfig.setCompany(company);
			recallConfig.setPastDue(true);
			recallConfig.setSubjectKey(RecallSubjectKey.DEFAULT);
			recallConfig.setTemplateType(RecallTemplateType.DEFAULT);
			copyRecallFields(form, recallConfig);
			this.rccService.save(recallConfig);
		} else {
			copyRecallFields(form, recallConfig);
			this.rccService.merge(recallConfig);
		}
		return "redirect:viewcomp.htm?coid=" + coid + "&loadtab=settings-tab";
	}
}