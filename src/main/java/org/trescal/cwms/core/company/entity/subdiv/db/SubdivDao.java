package org.trescal.cwms.core.company.entity.subdiv.db;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.dto.SubdivSearchResultWrapper;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.projection.SubdivProjectionDTO;

public interface SubdivDao extends BaseDao<Subdiv, Integer> {

	Long countCompanySubdivsByName(Integer coid, String subdivName, Integer excludeSubdivId);

	List<SubdivSearchResultWrapper> getAllActiveByCompany(int coid);

	List<Subdiv> getAllActiveSubdivsByRole(CompanyRole companyRole);

	List<SubdivKeyValue> searchBusinessSubdivsByName(String partialSubdivName);
	
	List<SubdivKeyValue> getAllSubdivFromCompany(int coid,Boolean isActive);

	/**
	 * Checks for existence of the specified SIRET number (French subdivision specific legal identifier) 
	 * 
	 * @param siretNumber search term
	 * @param companyRoles one or more companyRoles to search by
	 * @param excludeSubdivId optional Integer id of a subdiv to exclude from the search
	 * @return
	 */
	Long countBySiretNumber(String siretNumber, Set<CompanyRole> companyRoles, Integer excludeSubdivId);
	
	List<SubdivProjectionDTO> getSubdivProjectionDTOs(Collection<Integer> sourceSubdivIds, Integer allocatedCompanyId);
	
	List<SubdivProjectionDTO> getSubdivProjectionDTOsByCompany(Integer allocatedCompanyId);
}