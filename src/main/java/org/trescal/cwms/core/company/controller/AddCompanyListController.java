package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.companylist.CompanyList;
import org.trescal.cwms.core.company.entity.companylist.db.CompanyListService;
import org.trescal.cwms.core.company.form.AddCompanyListForm;
import org.trescal.cwms.core.company.form.AddCompanyListFormValidator;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_USERNAME })
public class AddCompanyListController {
	@Autowired
	private AddCompanyListFormValidator validator;
	@Autowired
	private CompanyListService companyListService;

	public static final String FORM_NAME = "addcompanylistform";
	public static final String VIEW_NAME = "trescal/core/company/addcompanylist";

	@ModelAttribute(FORM_NAME)
	public AddCompanyListForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto)
			throws Exception {
		AddCompanyListForm form = new AddCompanyListForm();
		form.setOrgid(companyDto.getKey());
		return form;
	}

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@RequestMapping(value = "/addcompanylist.htm", method = RequestMethod.GET)
	public ModelAndView formView(@ModelAttribute(FORM_NAME) AddCompanyListForm form) {
		return new ModelAndView(VIEW_NAME);
	}

	@RequestMapping(value = "/addcompanylist.htm", method = RequestMethod.POST)
	public ModelAndView onSubmit(Model model, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@Validated @ModelAttribute(FORM_NAME) AddCompanyListForm form, BindingResult bindingResult)
			throws Exception {

		if (bindingResult.hasErrors()) {
			return formView(form);
		}

		// create a new company list
		CompanyList companyList = companyListService.createNewCompanyList(form, username);

		return new ModelAndView("redirect:viewcompanylist.htm?id=" + companyList.getId());
	}
}
