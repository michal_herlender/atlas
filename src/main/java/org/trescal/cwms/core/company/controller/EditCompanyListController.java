package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.companylist.db.CompanyListService;
import org.trescal.cwms.core.company.form.EditCompanyListForm;
import org.trescal.cwms.core.company.form.EditCompanyListFormValidator;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller
@IntranetController
public class EditCompanyListController {

	@Autowired
	private EditCompanyListFormValidator validator;
	@Autowired
	private CompanyListService companyListService;

	private static final String JFS_VIEW = "viewcompanylist.htm?id=";
	public static final String FORM_NAME = "editcompanylistform";

	@ModelAttribute(FORM_NAME)
	protected EditCompanyListForm formBackingObject(
			@RequestParam(value = "compnaylistid", required = true) Integer compnaylistId) {
		EditCompanyListForm form = new EditCompanyListForm();
		form.setCompanylistId(compnaylistId);
		return form;
	}

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@RequestMapping(value = "/editcompanylist.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(Model model, final RedirectAttributes redirectAttributes,
			@Validated @ModelAttribute(FORM_NAME) EditCompanyListForm form, BindingResult bindingResult)
			throws Exception {

		if (bindingResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("model", model);
			redirectAttributes.addFlashAttribute("editcompanylistform", form);
			redirectAttributes.addFlashAttribute("bindingResult", bindingResult);
			redirectAttributes.addFlashAttribute("errorMessage",
					bindingResult.getAllErrors().get(0).getDefaultMessage());
			return new ModelAndView(new RedirectView(JFS_VIEW + form.getCompanylistId() + "&loadtab=edit-tab"));
		}
		companyListService.editCompanyList(companyListService.findCompanyList(form.getCompanylistId()), form);
		model.asMap().clear();
		return new ModelAndView(new RedirectView(JFS_VIEW + form.getCompanylistId() + "&loadtab=edit-tab"));
	}

}
