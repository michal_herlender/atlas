package org.trescal.cwms.core.company.entity.departmentrole;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.login.entity.user.User;

/**
 * Entity representing the available roles that {@link User}s can hold within
 * {@link Department} hierarchy (manager, technician etc).
 * 
 * @author Richard
 */
@Entity
@Table(name = "departmentrole")
public class DepartmentRole
{
	private boolean active;
	private Set<DepartmentMember> departmentMembers;
	private String description;
	private int id;
	private String role;
	private boolean manage;

	@OneToMany(mappedBy = "departmentRole")
	public Set<DepartmentMember> getDepartmentMembers()
	{
		return this.departmentMembers;
	}

	@Length(max = 255)
	@Column(name = "description", length = 255)
	public String getDescription()
	{
		return this.description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "int")
	@Column(name = "id", nullable=false, length = 20)
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@Length(max = 20)
	@Column(name = "role", nullable = false, length = 20)
	public String getRole()
	{
		return this.role;
	}

	@NotNull
	@Column(name = "active", nullable = false, columnDefinition="bit")
	public boolean isActive()
	{
		return this.active;
	}

	@NotNull
	@Column(name = "manage", nullable = false, columnDefinition="bit")
	public boolean isManage()
	{
		return this.manage;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public void setDepartmentMembers(Set<DepartmentMember> departmentMembers)
	{
		this.departmentMembers = departmentMembers;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	public void setManage(boolean manage)
	{
		this.manage = manage;
	}

	public void setRole(String role)
	{
		this.role = role;
	}

}
