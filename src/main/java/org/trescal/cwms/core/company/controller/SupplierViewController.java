package org.trescal.cwms.core.company.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.supplier.Supplier;
import org.trescal.cwms.core.company.entity.supplier.db.SupplierService;
import org.trescal.cwms.core.company.form.SupplierViewForm;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller @IntranetController
public class SupplierViewController
{
	@Autowired
	private SupplierService supplierServ;
	
	private static final String FORM_NAME = "form";
	private static final String VIEW_NAME = "trescal/core/company/supplier";
	private static final String REQUEST_URL = "/supplier.htm";

	private static Logger logger = LoggerFactory.getLogger(SupplierViewController.class);
	
	@ModelAttribute(FORM_NAME)
	protected SupplierViewForm formBackingObject() throws Exception {
		SupplierViewForm sf = new SupplierViewForm();
		Map<Integer,Boolean> activeSupplierIds = new HashMap<Integer,Boolean>();
		
		for (Supplier s : this.supplierServ.getAll())
		{
			if (s.isActive()) activeSupplierIds.put(s.getSupplierid(),Boolean.TRUE);
		}
		sf.setActiveSupplierIds(activeSupplierIds);
		return sf;
	}
	
	@RequestMapping(value=REQUEST_URL, method=RequestMethod.POST)
	protected ModelAndView onSubmit(@ModelAttribute(FORM_NAME) SupplierViewForm sf, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) {
			return new ModelAndView(VIEW_NAME);
		}
		
		logger.debug(sf.getActiveSupplierIds().toString());
		for (Supplier supplier : this.supplierServ.getAll())
		{
			boolean newActiveValue = sf.getActiveSupplierIds().get(supplier.getSupplierid());
			if (supplier.isActive() && !newActiveValue) {
				supplier.setActive(false);
				supplier = this.supplierServ.merge(supplier);
				logger.debug(supplier.getSupplierid()+" "+supplier.getApprovalType().getName() + " updated to false");
			}
			else if (!supplier.isActive() && newActiveValue) {
				supplier.setActive(true);
				supplier = this.supplierServ.merge(supplier);
				logger.debug(supplier.getSupplierid()+" "+supplier.getApprovalType().getName() + " updated to true");
			}
		}
		return new ModelAndView(new RedirectView(REQUEST_URL, true));
	}
	
	@RequestMapping(value=REQUEST_URL, method=RequestMethod.GET)
	protected ModelAndView referenceData() throws Exception
	{
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("suppliers", this.supplierServ.getAll());
		return new ModelAndView(VIEW_NAME, model);
	}
}