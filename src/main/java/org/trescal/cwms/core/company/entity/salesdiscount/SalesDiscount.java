package org.trescal.cwms.core.company.entity.salesdiscount;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.contact.Contact;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Set;

@Entity
@Table(name = "saledisc")
public class SalesDiscount
{
	public static class SalesDiscountComparator implements Comparator<SalesDiscount>
	{
		public int compare(SalesDiscount salesDiscount, SalesDiscount anotherSalesDiscount)
		{
			BigDecimal percent = salesDiscount.getPercent();
			BigDecimal percent2 = anotherSalesDiscount.getPercent();

			return percent.compareTo(percent2);
		}
	}

	private String code;
	private Set<Contact> contacts;
	private BigDecimal percent;
	private String userid;

	// Explicit constructor needed for Hibernate
	public SalesDiscount() {
		super();
	}
	
	public SalesDiscount(String code, BigDecimal percent, String userid)
	{
		super();
		this.code = code;
		this.percent = percent;
		this.percent = this.percent.setScale(2, BigDecimal.ROUND_HALF_UP);
		this.userid = userid;
	}

	@Length(max = 4)
	@Id
	@Column(name = "code", length = 4)
	public String getCode()
	{
		return this.code;
	}

	@OneToMany(mappedBy = "sd", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Contact> getContacts()
	{
		return this.contacts;
	}


	@Column(name = "percentage", precision = 2)
	public BigDecimal getPercent()
	{
		return this.percent;
	}

	@Length(max = 30)
	@Column(name = "userid", length = 30)
	public String getUserid()
	{
		return this.userid;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public void setContacts(Set<Contact> contacts)
	{
		this.contacts = contacts;
	}


	public void setPercent(BigDecimal percent)
	{
		this.percent = percent;
		this.percent = this.percent.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	public void setUserid(String userid)
	{
		this.userid = userid;
	}

	@Override
	public String toString()
	{
		return this.code;
	}
}
