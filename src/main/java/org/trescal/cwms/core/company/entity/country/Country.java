package org.trescal.cwms.core.company.entity.country;

import java.util.Locale;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.company.entity.addressprintformat.AddressPrintFormat;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.title.Title;

@Entity
@Table(name = "country")
public class Country
{
	private Set<Company> companies;
	private String country;
	private String countryCode;
	private int countryid;
	private Boolean memberOfUE;	
	private AddressPrintFormat addressPrintFormat;
	private Set<Title> titles;
	
	public Country() {
		memberOfUE = false;
	}

	@Transient
	public String getLocalizedName() {
		return getNameForLocale(LocaleContextHolder.getLocale());
	}
	
	@Transient
	public String getNameForLocale(Locale locale) {
		return (new Locale("", countryCode)).getDisplayCountry(locale);
	}
	
	@OneToMany(mappedBy = "country")
	public Set<Company> getCompanies()
	{
		return this.companies;
	}

	@NotNull
	@Length(max = 30)
	@Column(name = "country", length = 30, nullable=false)
	public String getCountry()
	{
		return this.country;
	}

	@NotNull
	@Length(max = 6)
	@Column(name = "countrycode", length = 6, nullable=false)
	public String getCountryCode()
	{
		return this.countryCode;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "countryid")
	public int getCountryid()
	{
		return this.countryid;
	}

	@Column(name = "memberOfUE", columnDefinition="bit")
	public Boolean getMemberOfUE()
	{
		return this.memberOfUE;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "addressprintformat", nullable=false)
	public AddressPrintFormat getAddressPrintFormat() {
		return addressPrintFormat;
	}
	
	public void setCompanies(Set<Company> companies)
	{
		this.companies = companies;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public void setCountryCode(String countryCode)
	{
		this.countryCode = countryCode;
	}

	public void setCountryid(int countryid)
	{
		this.countryid = countryid;
	}

	public void setMemberOfUE(Boolean memberOfUE)
	{
		this.memberOfUE = memberOfUE;
	}

	@Override
	public String toString()
	{
		return Integer.toString(this.countryid);
	}

	public void setAddressPrintFormat(AddressPrintFormat addressPrintFormat) {
		this.addressPrintFormat = addressPrintFormat;
	}
	
	@OneToMany(mappedBy = "country")
	public Set<Title> getTitles() {
		return titles;
	}

	public void setTitles(Set<Title> titles) {
		this.titles = titles;
	}
	
	
}
