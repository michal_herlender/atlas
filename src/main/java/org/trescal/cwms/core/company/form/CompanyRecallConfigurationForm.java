package org.trescal.cwms.core.company.form;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.RecallPeriodStart;

public class CompanyRecallConfigurationForm {
	private Boolean active;
	private Integer futureMonths;
	private RecallPeriodStart periodStart;
	private String customText;
	
	@NotNull
	public Boolean getActive() {
		return active;
	}
	@NotNull(message="{error.value.notselected}")
	@Min(value=0)
	@Max(value=3)
	public Integer getFutureMonths() {
		return futureMonths;
	}
	@NotNull
	public RecallPeriodStart getPeriodStart() {
		return periodStart;
	}
	@NotNull
	@Length(max=2000)
	public String getCustomText() {
		return customText;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public void setFutureMonths(Integer futureMonths) {
		this.futureMonths = futureMonths;
	}
	public void setPeriodStart(RecallPeriodStart periodStart) {
		this.periodStart = periodStart;
	}
	public void setCustomText(String customText) {
		this.customText = customText;
	}
}
