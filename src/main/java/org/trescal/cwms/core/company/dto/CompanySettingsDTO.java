package org.trescal.cwms.core.company.dto;

public class CompanySettingsDTO {

	private Integer companyId;
	private boolean active;
	private boolean onStop;

	public CompanySettingsDTO(Integer companyId, boolean active, boolean onStop) {
		this.companyId = companyId;
		this.active = active;
		this.onStop = onStop;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isOnStop() {
		return onStop;
	}

	public void setOnStop(boolean onStop) {
		this.onStop = onStop;
	}
}