package org.trescal.cwms.core.company.form;

import javax.validation.Valid;

import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

public class SubdivAddForm
{
	private Address address;
	private AddressSettingsForAllocatedSubdiv addressSettingsSubdiv;
	private Integer countryId;
	private Subdiv subdiv;
	private Integer transportInId;
	private Integer transportOutId;
	
	@Valid
	public Address getAddress()
	{
		return this.address;
	}

	public Integer getCountryId()
	{
		return this.countryId;
	}
	
	@Valid
	public Subdiv getSubdiv()
	{
		return this.subdiv;
	}

	public Integer getTransportInId()
	{
		return this.transportInId;
	}

	public Integer getTransportOutId()
	{
		return this.transportOutId;
	}

	public void setAddress(Address address)
	{
		this.address = address;
	}

	public void setCountryId(Integer countryId)
	{
		this.countryId = countryId;
	}

	public void setSubdiv(Subdiv subdiv)
	{
		this.subdiv = subdiv;
	}

	public void setTransportInId(Integer transportInId)
	{
		this.transportInId = transportInId;
	}

	public void setTransportOutId(Integer transportOutId)
	{
		this.transportOutId = transportOutId;
	}

	public AddressSettingsForAllocatedSubdiv getAddressSettingsSubdiv() {
		return addressSettingsSubdiv;
	}

	public void setAddressSettingsSubdiv(
			AddressSettingsForAllocatedSubdiv addressSettingsSubdiv) {
		this.addressSettingsSubdiv = addressSettingsSubdiv;
	}
}