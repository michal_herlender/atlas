package org.trescal.cwms.core.company.entity.mailgroupmember.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.mailgroupmember.MailGroupMember;
import org.trescal.cwms.core.company.entity.mailgroupmember.MailGroupMember_;
import org.trescal.cwms.core.company.entity.mailgrouptype.MailGroupType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;

@Repository("MailGroupMemberDao")
public class MailGroupMemberDaoImpl extends BaseDaoImpl<MailGroupMember, Integer> implements MailGroupMemberDao {

	@Override
	protected Class<MailGroupMember> getEntity() {
		return MailGroupMember.class;
	}

	@Override
	public MailGroupMember findMailGroupMember(Integer personid, MailGroupType mailGroupType) {
		return getFirstResult(cb -> {
			CriteriaQuery<MailGroupMember> cq = cb.createQuery(MailGroupMember.class);
			Root<MailGroupMember> root = cq.from(MailGroupMember.class);
			Join<MailGroupMember, Contact> contact = root.join(MailGroupMember_.contact);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(contact.get(Contact_.personid), personid));
			clauses.getExpressions().add(cb.equal(root.get(MailGroupMember_.mailGroupType), mailGroupType));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

	@Override
	public List<MailGroupMember> getContactMailGroups(Integer personid) {
		return getResultList(cb -> {
			CriteriaQuery<MailGroupMember> cq = cb.createQuery(MailGroupMember.class);
			Root<MailGroupMember> root = cq.from(MailGroupMember.class);
			Join<MailGroupMember, Contact> contact = root.join(MailGroupMember_.contact);
			cq.where(cb.equal(contact.get(Contact_.personid), personid));
			return cq;
		});
	}

	@Override
	public List<MailGroupMember> getMembersOfGroupForSubdiv(MailGroupType mailGroupType, int subdivid,
			boolean activeContactsOnly) {
		return getResultList(cb -> {
			CriteriaQuery<MailGroupMember> cq = cb.createQuery(MailGroupMember.class);
			Root<MailGroupMember> root = cq.from(MailGroupMember.class);
			Join<MailGroupMember, Contact> contact = root.join(MailGroupMember_.contact);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(MailGroupMember_.mailGroupType), mailGroupType));
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), subdivid));
			if (activeContactsOnly) {
				clauses.getExpressions().add(cb.isTrue(contact.get(Contact_.active)));
			}
			cq.where(clauses);
			return cq;
		});
	}

}