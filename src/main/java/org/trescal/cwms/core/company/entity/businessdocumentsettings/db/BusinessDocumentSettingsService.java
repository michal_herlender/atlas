package org.trescal.cwms.core.company.entity.businessdocumentsettings.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.businessdocumentsettings.BusinessDocumentSettings;

public interface BusinessDocumentSettingsService extends BaseService<BusinessDocumentSettings, Integer> {

	/**
	 * Returns the document settings for the business company provided; 
	 * or the default settings if no specific settings for business company  
	 * @param businessCompanyId
	 * @return
	 */
	BusinessDocumentSettings getForCompanyOrDefault(Integer businessCompanyId); 
}
