package org.trescal.cwms.core.company.form;

import lombok.val;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import java.util.Objects;

@Component
public class ContactEditFormValidator extends AbstractBeanValidator {

	@Autowired
	private ContactService conServ;
	@Autowired
	private UserService userServ;

	@Autowired
	private AuthenticationService authenticationService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(ContactEditForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		ContactEditForm cef = (ContactEditForm) target;
		// Use ApacheCommons EmailValidator, catches more than @Email
		// annotation; email addresses are required and validated
		if (!EmailValidator.getInstance(false).isValid(cef.getEmail())) {
			errors.rejectValue("email", "error.email", null, "The email address entered is not of a valid format.");
		}
		if (StringUtils.hasLength(cef.getPassword()) || StringUtils.hasLength(cef.getPassword2())) {
			// User if changing password, perform password validation
			super.validate(target, errors, ContactEditForm.PasswordGroup.class);

			val authentication = SecurityContextHolder.getContext().getAuthentication();
			val editor = userServ.get((String) authentication.getPrincipal()).getCon();
			val editingOwnAccount = Objects.equals(cef.getContact().getId(), editor.getId());
			val allowPasswordEdit = authenticationService.hasRight(editor, Permission.PASSWORD_EDIT, cef.getAllocatedSubdivId()) || editingOwnAccount;

			if (!allowPasswordEdit)
				errors.rejectValue("password", "error.password.actionnotallowed");

			if (cef.getPassword().indexOf(' ') > -1)
				errors.rejectValue("password", "error.space.character.notallowed");
			if (cef.getPassword2().indexOf(' ') > -1)
				errors.rejectValue("password2", "error.space.character.notallowed");
			if (!StringUtils.hasLength(cef.getPassword()) || !StringUtils.hasLength(cef.getPassword2())) {
				errors.rejectValue("password", "error.contact.password.required",
					"Please enter a password in both fields");
			} else if (!cef.getPassword().equals(cef.getPassword2())) {
				errors.rejectValue("password", "error.contact.password.mismatch",
						"The chosen passwords do not appear to match, please re-enter");
			}
			ResultWrapper validPassword = userServ.passwordValid(cef.getPassword());
			if (!validPassword.isSuccess()) {
				errors.rejectValue("password", validPassword.getCode(), validPassword.getMessage());
			}
		} else {
			// If not changing password, email reminder cannot be sent (as
			// password encoded = unknown)
			if (cef.isSendLoginEmail()) {
				errors.rejectValue("sendLoginEmail", "error.contact.loginemail");
			}
		}
		// hr id exists
		if (conServ.getHrIdExistsAndIgnoreContact(cef.getContact().getHrid(),cef.getContact())) {
			errors.rejectValue("contact.hrid", "error.contact.hrid.alreadyexists", "The id already exists");
		}

	}
}