package org.trescal.cwms.core.company.entity.salesdiscount.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.salesdiscount.SalesDiscount;


public class SalesDiscountServiceImpl implements SalesDiscountService
{
	SalesDiscountDao sdDao;
	
	public SalesDiscount findSalesDiscount(String id)
	{
		return sdDao.find(id);
	}
	
	public void updateSalesDiscount(SalesDiscount sd)
	{
		sdDao.update(sd);
	}
	
	public List<SalesDiscount> getAllSalesDiscounts()
	{
		return sdDao.findAll();
	}
	
	public void setSdDao(SalesDiscountDao sdDao)
	{
		this.sdDao = sdDao;
	}
}