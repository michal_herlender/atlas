package org.trescal.cwms.core.company.entity.title.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.title.Title;
import org.trescal.cwms.spring.model.KeyValue;

public interface TitleService
{
	Title findTitle(Integer title);
	void insertTitle(Title title);
	void updateTitle(Title title);
	List<Title> getAllTitles();
	List<KeyValue<String,String>> getDTOTitles(String country);
}