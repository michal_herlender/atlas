package org.trescal.cwms.core.company.entity.companystatus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

public enum CompanyStatus {
	NOT_VERIFIED("companystatus.notverified"), TO_CHECK("companystatus.tocheck"), VERIFIED("companystatus.verified");

	private final String messageCode;
	private ReloadableResourceBundleMessageSource messageSource;

	CompanyStatus(String messageCode) {
		this.messageCode = messageCode;
	}

	@Component
	private static class CompanyStatusMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (CompanyStatus cs : EnumSet.allOf(CompanyStatus.class))
               cs.messageSource = messageSource;
        }
	}

	public String getMessageCode() {
		return messageCode;
	}
	
	public String getMessage() {
		return messageSource.getMessage(messageCode, null, this.toString(), LocaleContextHolder.getLocale());
	}
	
	/*
	 * Returns a map of Enums to localized names
	 */
	public static Map<String,String> valueMap() {
		Map<String,String> map = new TreeMap<>();
		for (CompanyStatus status: CompanyStatus.values()) {
			map.put(status.name(), status.getMessage());
		}
		return map;
	}
}
