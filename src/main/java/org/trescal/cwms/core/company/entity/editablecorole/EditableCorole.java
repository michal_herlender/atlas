package org.trescal.cwms.core.company.entity.editablecorole;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.corole.Corole;

//@Entity
//@Table(name = "editablecorole", uniqueConstraints = { @UniqueConstraint(columnNames = { "editfrom", "editto" }) })
public class EditableCorole
{
	private int id;
	private Corole editableFrom;
	private Corole editableTo;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editfrom", nullable = false)
	public Corole getEditableFrom()
	{
		return this.editableFrom;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "editto", nullable = false)
	public Corole getEditableTo()
	{
		return this.editableTo;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	public void setEditableFrom(Corole editableFrom)
	{
		this.editableFrom = editableFrom;
	}

	public void setEditableTo(Corole editableTo)
	{
		this.editableTo = editableTo;
	}

	public void setId(int id)
	{
		this.id = id;
	}

}
