package org.trescal.cwms.core.company.entity.invoicefrequency.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.invoicefrequency.InvoiceFrequency;

public interface InvoiceFrequencyService extends BaseService<InvoiceFrequency, Integer> {
	InvoiceFrequency get(String code);
}
