package org.trescal.cwms.core.company.entity.editablecorole.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.corole.Corole;
import org.trescal.cwms.core.company.entity.editablecorole.EditableCorole;

public interface EditableCoroleService
{
	void deleteEditableCorole(EditableCorole editablecorole);

	List<EditableCorole> findAllEditableCorolesForRole(Corole role);

	EditableCorole findEditableCorole(int id);

	List<EditableCorole> getAllEditableCoroles();

	void insertEditableCorole(EditableCorole editablecorole);

	void saveOrUpdateEditableCorole(EditableCorole editablecorole);

	void updateEditableCorole(EditableCorole editablecorole);
}