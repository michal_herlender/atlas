package org.trescal.cwms.core.company.entity.paymentmode.db;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;

@Repository
public class PaymentModeDaoImpl extends BaseDaoImpl<PaymentMode, Integer>
		implements PaymentModeDao {

	@Override
	protected Class<PaymentMode> getEntity() {
		return PaymentMode.class;
	}

	@Override
	public PaymentMode get(String code) {
		Criteria criteria = getSession().createCriteria(PaymentMode.class);
		criteria.add(Restrictions.eq("code", code));
		return (PaymentMode) criteria.uniqueResult();
	}


}
