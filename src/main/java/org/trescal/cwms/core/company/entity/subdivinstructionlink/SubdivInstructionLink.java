/**
 * 
 */
package org.trescal.cwms.core.company.entity.subdivinstructionlink;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionEntity;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLink;

/**
 * An instruction recorded for a {@link Subdiv}. Can be *optionally* allocated
 * to a business company.
 */
@Entity
@Table(name = "subdivinstructionlink", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "subdivid", "instructionid" }) })
public class SubdivInstructionLink extends Allocated<Company> implements InstructionLink<Subdiv> {

	private int id;
	private Subdiv subdiv;
	private Instruction instruction;
	private boolean isSubdivInstruction;
	private Subdiv businessSubdiv;

	@Transient
	public InstructionEntity getInstructionEntity() {
		return InstructionEntity.SUBDIV;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subdivid")
	public Subdiv getSubdiv() {
		return this.subdiv;
	}

	@Override
	@Transient
	public Subdiv getEntity() {
		return subdiv;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "instructionid")
	@Cascade(value = CascadeType.ALL)
	@Valid
	public Instruction getInstruction() {
		return instruction;
	}

	@Column(name = "issubdivinstruction")
	public boolean isSubdivInstruction() {
		return isSubdivInstruction;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "businesssubdivid")
	public Subdiv getBusinessSubdiv() {
		return businessSubdiv;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setSubdiv(Subdiv subdiv) {
		this.subdiv = subdiv;
	}

	public void setInstruction(Instruction instruction) {
		this.instruction = instruction;
	}

	public void setSubdivInstruction(Boolean subdivInstruction) {
		this.isSubdivInstruction = subdivInstruction == null ? false : subdivInstruction;
	}

	public void setBusinessSubdiv(Subdiv businessSubdiv) {
		this.businessSubdiv = businessSubdiv;
	}
}