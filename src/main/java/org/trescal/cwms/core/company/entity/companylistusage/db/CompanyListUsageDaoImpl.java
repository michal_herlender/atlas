package org.trescal.cwms.core.company.entity.companylistusage.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companylist.CompanyList;
import org.trescal.cwms.core.company.entity.companylist.CompanyList_;
import org.trescal.cwms.core.company.entity.companylistusage.CompanyListUsage;
import org.trescal.cwms.core.company.entity.companylistusage.CompanyListUsage_;

@Repository("CompanyListUsageDao")
public class CompanyListUsageDaoImpl extends BaseDaoImpl<CompanyListUsage, Integer> implements CompanyListUsageDao {

	@Override
	protected Class<CompanyListUsage> getEntity() {
		return CompanyListUsage.class;
	}

	@Override
	public List<CompanyListUsage> getUsageByCompanyList(Integer companyListId) {
		return getResultList(cb -> {

			CriteriaQuery<CompanyListUsage> cq = cb.createQuery(CompanyListUsage.class);
			Root<CompanyListUsage> root = cq.from(CompanyListUsage.class);
			Join<CompanyListUsage, CompanyList> companylistJoin = root.join(CompanyListUsage_.companyList);
			cq.where(cb.equal(companylistJoin.get(CompanyList_.id), companyListId));
			cq.orderBy(cb.asc(root.get(CompanyListUsage_.company).get(Company_.coname)));
			return cq;
		});
	}
}
