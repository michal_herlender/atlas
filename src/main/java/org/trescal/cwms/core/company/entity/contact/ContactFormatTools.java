package org.trescal.cwms.core.company.entity.contact;

/**
 * Extraction of formatting methods from Contact, so that they can be used with projections  
 * @author galen
 *
 */
public class ContactFormatTools {
	
	public static String getInitials(Contact contact) {
		return getInitials(contact.getFirstName(), contact.getLastName());
	}
	
	public static String getInitials(String firstName, String lastName) {
		String initials = "";
		if ((firstName != null) && !firstName.trim().isEmpty()) {
			initials = String.valueOf(firstName.charAt(0));
		}
		if ((lastName != null) && !lastName.trim().isEmpty()) {
			initials = initials + String.valueOf(lastName.charAt(0));
		}
		return initials;
	}
	
	public static String getShortenedName(Contact contact) {
		return getShortenedName(contact.getFirstName(), contact.getLastName());
	}
	
	public static String getShortenedName(String firstName, String lastName) {
		String initial = "";
		if ((firstName != null) && !firstName.trim().isEmpty()) {
			initial = String.valueOf(firstName.charAt(0) + ". ");
		}
		return initial + lastName;
	}
}
