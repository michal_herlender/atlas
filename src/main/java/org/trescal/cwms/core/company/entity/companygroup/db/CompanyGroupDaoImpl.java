package org.trescal.cwms.core.company.entity.companygroup.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.persistence.criteria.Subquery;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companygroup.CompanyGroup;
import org.trescal.cwms.core.company.entity.companygroup.CompanyGroup_;
import org.trescal.cwms.core.company.entity.companygroup.dto.CompanyGroupUsageDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Repository
public class CompanyGroupDaoImpl extends BaseDaoImpl<CompanyGroup, Integer> implements CompanyGroupDao {

	@Override
	protected Class<CompanyGroup> getEntity() {
		return CompanyGroup.class;
	}
	
	@Override
	public Long countByName(String groupName, Integer idToExclude) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<CompanyGroup> companygroup = cq.from(CompanyGroup.class);
			Predicate clauses = cb.conjunction();
			if (groupName != null) {
				clauses.getExpressions().add(cb.equal(companygroup.get(CompanyGroup_.groupName), groupName));
			}
			if (idToExclude != null && (idToExclude != 0)) {
				clauses.getExpressions().add(cb.notEqual(companygroup.get(CompanyGroup_.id), idToExclude));
			}

			cq.select(cb.count(companygroup)).where(clauses);
			return cq;
		});
	}
	
	@Override
	public List<KeyValueIntegerString> getDTOList(Boolean activeGroups) {
		return getResultList(cb -> {
			CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class); 
			Root<CompanyGroup> root = cq.from(CompanyGroup.class);
			
			if (activeGroups != null) {
				cq.where(cb.equal(root.get(CompanyGroup_.active), activeGroups));
			}
			
			cq.orderBy(cb.asc(root.get(CompanyGroup_.groupName)), cb.asc(root.get(CompanyGroup_.id)));
			Selection<KeyValueIntegerString> selection = cb.construct(KeyValueIntegerString.class, 
					root.get(CompanyGroup_.id), root.get(CompanyGroup_.groupName));
			cq.select(selection);
			
			return cq;
		});
	}

	@Override
	public List<CompanyGroupUsageDTO> getUsageDTOList() {
		return getResultList(cb -> {
			CriteriaQuery<CompanyGroupUsageDTO> cq = cb.createQuery(CompanyGroupUsageDTO.class);
			Root<CompanyGroup> root = cq.from(CompanyGroup.class);
			
			Subquery<Long> sqCount = cq.subquery(Long.class);
			Root<Company> sqRoot = sqCount.from(Company.class);
			sqCount.where(cb.equal(sqRoot.get(Company_.companyGroup), root));
			sqCount.select(cb.count(sqRoot));
			
			cq.orderBy(cb.asc(root.get(CompanyGroup_.groupName)), cb.asc(root.get(CompanyGroup_.id)));
			Selection<CompanyGroupUsageDTO> selection = cb.construct(CompanyGroupUsageDTO.class, 
					root.get(CompanyGroup_.id), root.get(CompanyGroup_.groupName),
					root.get(CompanyGroup_.active), sqCount);
			cq.select(selection);
			
			return cq;
		});
	}
}
