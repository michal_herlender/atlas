package org.trescal.cwms.core.company.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ClientCompanyDTO {

	private Integer clientCompanyId;
	private Integer itemNo;
	private Boolean requireSupplierCompanyList;
	
	public ClientCompanyDTO(Integer clientCompanyId, Integer itemNo, Boolean requireSupplierCompanyList) {
		super();
		this.clientCompanyId = clientCompanyId;
		this.itemNo = itemNo;
		this.requireSupplierCompanyList = requireSupplierCompanyList;
	}
}
