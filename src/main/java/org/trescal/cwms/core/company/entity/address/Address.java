package org.trescal.cwms.core.company.entity.address;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.trescal.cwms.core.account.entity.companyaccount.CompanyAccount;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.addressplantillassettings.AddressPlantillasSettings;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.LocationComparator;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.system.entity.labelprinter.LabelPrinter;
import org.trescal.cwms.core.system.entity.printer.Printer;

import lombok.Setter;

@Entity
@Table(name = "address")
@Setter
public class Address extends Versioned implements Serializable {

	private static final long serialVersionUID = 1303205038642844172L;

	// if this field is set then this address is the company's primary account
	// address
	@Deprecated
	private CompanyAccount accountAddress;
	// if this field is set then this address is a company 'generic' address
	// used for accounts purposes
	@Deprecated
	private Company accountCompany;
	private Boolean active;
	private String addr1;
	private String addr2;
	private String addr3;
	private Set<AddressPlantillasSettings> addressPlantillasSettings;
	private Set<AddressType> addressType;
	private Integer addrid;
	private String costCentre;
	private Country country;
	private String county;
	private String deactReason;
	private Date deactTime;
	private LabelPrinter defaultLabelPrinter;
	private Printer defaultPrinter;
	private Set<Department> departments;
	private Set<Instrument> insts;
	private Set<Location> locations;
	private Set<OnBehalfItem> onBehalfJobItems;
	private String postcode;
	private Set<Job> returnJobs;
	private Subdiv sub;
	private String town;
	private Set<AddressSettingsForAllocatedCompany> settingsForAllocatedCompanies;
	private Set<AddressSettingsForAllocatedSubdiv> settingsForAllocatedSubdivs;
	private String telephone;
	private String fax;
	private BigDecimal latitude;
	private BigDecimal longitude;
	private AddressValidator validator;
	private Contact validatedBy;
	private LocalDateTime validatedOn;

	public Address() {
		this.setActive(true);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "addrid", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getAddrid() {
		return this.addrid;
	}

	// Made Transient (performance impact from @OneToOne 2018-09-07 GB
	// Targeted to removal, checking with Adrian about Account Dimensions
//	@OneToOne(mappedBy = "address")
	@Transient
	public CompanyAccount getAccountAddress() {
		return this.accountAddress;
	}

	// Made Transient (performance impact from @OneToOne 2018-09-07 GB
	// Targeted to removal, checking with Adrian about Account Dimensions
//	@OneToOne(mappedBy = "accountAddress")
	@Transient
	public Company getAccountCompany() {
		return this.accountCompany;
	}

	@NotNull
	@Column(name = "active", columnDefinition = "tinyint", nullable = false)
	public Boolean getActive() {
		return this.active;
	}

	@Transient
	public String getAddressLine() {
		String result = getAddr1();
		if (getAddr2() != null && !getAddr2().isEmpty())
			result += ", " + getAddr2();
		if (getAddr3() != null && !getAddr3().isEmpty())
			result += ", " + getAddr3();
		if (getTown() != null && !getTown().isEmpty())
			result += ", " + getTown();
		if (getCounty() != null && !getCounty().isEmpty())
			result += ", " + getCounty();
		if (getPostcode() != null && !getPostcode().isEmpty())
			result += ", " + getPostcode();
		if (getCountry() != null)
			result += ", " + getCountry().getLocalizedName();
		return result;
	}

	/* Changed from length 50 to 100 to accommodate Spain data migration */
	@NotNull
	@Length(min = 1, max = 100)
	@Column(name = "addr1", length = 100, nullable = false)
	public String getAddr1() {
		return this.addr1;
	}

	/* Changed from length 50 to 100 to accommodate Spain data migration */
	@NotNull
	@Length(min = 0, max = 100)
	@Column(name = "addr2", length = 100, nullable = false)
	public String getAddr2() {
		return this.addr2;
	}

	/* Changed from length 50 to 100 to accommodate Spain data migration */
	@NotNull
	@Length(min = 0, max = 100)
	@Column(name = "addr3", length = 100, nullable = false)
	public String getAddr3() {
		return this.addr3;
	}

	@OneToMany(mappedBy = "address", fetch = FetchType.LAZY)
	public Set<AddressPlantillasSettings> getAddressPlantillasSettings() {
		return addressPlantillasSettings;
	}

	@ElementCollection(targetClass = AddressType.class)
	@Enumerated(EnumType.ORDINAL)
	@CollectionTable(name = "addressfunction", joinColumns = @JoinColumn(name = "addressid", referencedColumnName = "addrid"))
	@Column(name = "typeid")
	public Set<AddressType> getAddressType() {
		return this.addressType;
	}

	@Length(min = 0, max = 20)
	@Column(name = "costcentre", length = 20)
	public String getCostCentre() {
		return this.costCentre;
	}

	@NotNull
	@JoinColumn(name = "countryid", nullable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	public Country getCountry() {
		return country;
	}

	@NotNull
	@Length(min = 0, max = 30)
	@Column(name = "county", length = 30, nullable = false)
	public String getCounty() {
		return this.county;
	}

	@Column(name = "deactreason")
	public String getDeactReason() {
		return this.deactReason;
	}

	@Column(name = "deacttime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDeactTime() {
		return this.deactTime;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deflabelprinterid")
	public LabelPrinter getDefaultLabelPrinter() {
		return this.defaultLabelPrinter;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "defprinterid")
	public Printer getDefaultPrinter() {
		return this.defaultPrinter;
	}

	@Length(max = 30)
	@Column(name = "telephone", length = 30)
	public String getTelephone() {
		return telephone;
	}

	@OneToMany(mappedBy = "address", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Department> getDepartments() {
		return this.departments;
	}

	@OneToMany(mappedBy = "add", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Instrument> getInsts() {
		return this.insts;
	}

	@Column(name = "latitude", nullable = true, precision = 9, scale = 6)
	public BigDecimal getLatitude() {
		return latitude;
	}

	@Column(name = "longitude", nullable = true, precision = 9, scale = 6)
	public BigDecimal getLongitude() {
		return longitude;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "add")
	@SortComparator(LocationComparator.class)
	public Set<Location> getLocations() {
		return this.locations;
	}

	@OneToMany(mappedBy = "address", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<OnBehalfItem> getOnBehalfJobItems() {
		return this.onBehalfJobItems;
	}

	@NotNull
	@Length(min = 1, max = 10)
	@Column(name = "postcode", length = 10, nullable = false)
	public String getPostcode() {
		return this.postcode;
	}

	@OneToMany(mappedBy = "returnTo", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Job> getReturnJobs() {
		return this.returnJobs;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subdivid")
	public Subdiv getSub() {
		return this.sub;
	}

	@NotNull
	@Length(min = 1, max = 50)
	@Column(name = "town", nullable = false)
	public String getTown() {
		return this.town;
	}

	@Length(max = 20)
	@Column(name = "fax", length = 20)
	public String getFax() {
		return fax;
	}

	@OneToMany(mappedBy = "address", cascade = CascadeType.ALL)
	public Set<AddressSettingsForAllocatedCompany> getSettingsForAllocatedCompanies() {
		return settingsForAllocatedCompanies;
	}

	@OneToMany(mappedBy = "address", cascade = CascadeType.ALL)
	public Set<AddressSettingsForAllocatedSubdiv> getSettingsForAllocatedSubdivs() {
		return settingsForAllocatedSubdivs;
	}

	@Enumerated(EnumType.ORDINAL)
	public AddressValidator getValidator() {
		return validator;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "validatedby", foreignKey = @ForeignKey(name = "FK_address_validatedby"))
	public Contact getValidatedBy() {
		return validatedBy;
	}

	@Column(name = "validatedon")
	@DateTimeFormat(iso = ISO.DATE_TIME)
	public LocalDateTime getValidatedOn() {
		return validatedOn;
	}

	@Override
	public String toString() {
		return Integer.toString(this.addrid);
	}
}