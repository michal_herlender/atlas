package org.trescal.cwms.core.company.entity.vatrate.db;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.addresssettings.db.AddressSettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.VatRateType;

@Service("VatRateServiceImpl")
public class VatRateServiceImpl extends BaseServiceImpl<VatRate, String> implements VatRateService
{
	@Autowired
	private AddressSettingsForAllocatedCompanyService addressSettingsForAllocatedCompanyService; 
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsForAllocatedCompanyService; 
	@Autowired
	private VatRateDao vatRateDao;
	
	public static final Logger logger = LoggerFactory.getLogger(VatRateServiceImpl.class);

	@Override
	public List<VatRate> getAllEagerWithCountry() {
		return this.vatRateDao.getAllEagerWithCountry();
	}
	
	@Override
	public VatRate getDefault(Country companyCountry, Country allocatedCountry,
			CompanyRole companyRole) {
		if (companyCountry == null) throw new IllegalArgumentException("companyCountry was null");
		if (allocatedCountry == null) throw new IllegalArgumentException("allocatedCountry was null");
		if (companyRole == null) throw new IllegalArgumentException("companyRole was null");
		if (companyRole.equals(CompanyRole.SUPPLIER) ||
			companyRole.equals(CompanyRole.UTILITY)) {
			return getDefault(companyCountry, allocatedCountry);
		}
		else {
			return getDefault(allocatedCountry, companyCountry);
		}
	}
	
	@Override
	public VatRate getForCountryAndType(Integer countryId, VatRateType type) {
		return this.vatRateDao.getForCountryAndType(countryId, type);
	}

	@Override
	public boolean isVatRateAllowed(VatRate vatRate, Country companyCountry,
			Country allocatedCountry, CompanyRole companyRole) {
		if (vatRate == null) throw new IllegalArgumentException("vatRate was null");
		if (companyCountry == null) throw new IllegalArgumentException("companyCountry was null");
		if (allocatedCountry == null) throw new IllegalArgumentException("allocatedCountry was null");
		if (companyRole == null) throw new IllegalArgumentException("companyRole was null");
		if (companyRole.equals(CompanyRole.SUPPLIER) ||
			companyRole.equals(CompanyRole.UTILITY)) {
			return isVatRateAllowed(vatRate, companyCountry, allocatedCountry);
		}
		else {
			return isVatRateAllowed(vatRate, allocatedCountry, companyCountry);
		}
	}
	/*
	 * 4 cases:
	 * a) source and destination are same country, use COUNTRY_DEFAULT for country
	 * b) source not in EU, use NON-EUROZONE
	 * c) source in EU, destination not in EU, use EUROZONE_EXTERNAL
	 * d) source in EU, destination in EU but different country, use EUROZONE_INTERNAL
	 */
	private VatRate getDefault(Country sourceCountry, Country allocatedCountry) {
		VatRate vatRate = null;
		// a) same source and destination country, use COUNTRY_DEFAULT for country
		if (sourceCountry.getCountryid() == allocatedCountry.getCountryid()) {
			vatRate = this.vatRateDao.getForCountryAndType(sourceCountry.getCountryid(), VatRateType.COUNTRY_DEFAULT);
		}
		// source and destination countries are different
		else {
			if (!sourceCountry.getMemberOfUE()) {
				// b) source not in EU, use NON-EUROZONE
				vatRate = this.vatRateDao.getDefaultForType(VatRateType.NON_EUROZONE); 
			}
			else {
				// c) source in EU, destination not in EU, use EUROZONE_EXTERNAL
				if (!allocatedCountry.getMemberOfUE()) {
					vatRate = this.vatRateDao.getDefaultForType(VatRateType.EUROZONE_EXTERNAL);
				}
				// d) source in EU, destination in EU but different country, use EUROZONE_INTERNAL
				else {
					vatRate = this.vatRateDao.getDefaultForType(VatRateType.EUROZONE_INTERNAL);
				}
			}
		}
		
		if (vatRate == null) {
			logger.error("Database setup issue: VatRate not found between "+sourceCountry.getCountryCode()+" and business country "+allocatedCountry.getCountryCode());
		}
		return vatRate;
	}

	/*
	 * 4 cases:
	 * a) source and destination are same country, allow COUNTRY_DEFAULT or COUNTRY_SPECIAL for country
	 * b) source not in EU, use NON-EUROZONE
	 * c) source in EU, destination not in EU, use EUROZONE_EXTERNAL
	 * d) source in EU, destination in EU but different country, use EUROZONE_INTERNAL
	 */
	private boolean isVatRateAllowed(VatRate vatRate, Country sourceCountry, Country allocatedCountry) {
		if (vatRate == null) return false;
		// a) source and destination are same country, allow COUNTRY_DEFAULT or COUNTRY_SPECIAL for country
		if (sourceCountry.getCountryid() == allocatedCountry.getCountryid()) {
			logger.debug("source and destination are same country, only COUNTRY_DEFAULT or COUNTRY_SPECIAL with same country ID allowed");
			return ((vatRate.getType().equals(VatRateType.COUNTRY_DEFAULT) || 
					 vatRate.getType().equals(VatRateType.COUNTRY_SPECIAL)) &&
					 (vatRate.getCountry() != null) &&
					 (vatRate.getCountry().getCountryid() == sourceCountry.getCountryid()));
		}
		// b) source not in EU, use NON-EUROZONE
		if (!sourceCountry.getMemberOfUE()) {
			logger.debug("source not in EU, only NON-EUROZONE allowed");
			return (vatRate.getType().equals(VatRateType.NON_EUROZONE)); 
		}
		else {
			// c) source in EU, destination not in EU, use EUROZONE_EXTERNAL
			if (!allocatedCountry.getMemberOfUE()) {
				logger.debug("source in EU, destination not in EU, only EUROZONE_EXTERNAL allowed");
				return (vatRate.getType().equals(VatRateType.EUROZONE_EXTERNAL));
			}
			else {
				// d) source in EU, destination in EU but different country, use EUROZONE_INTERNAL
				logger.debug("source in EU, destination in EU but different country, only EUROZONE_INTERNAL allowed");
				return (vatRate.getType().equals(VatRateType.EUROZONE_INTERNAL));
			}
		}
	}
	
	@Override
	public VatRate getForAddress(Address address, Company allocatedCompany) {
		// Gets the correct VAT rate for the (client) address and allocated (business) company (address settings, or company settings)
		AddressSettingsForAllocatedCompany addressSettings = this.addressSettingsForAllocatedCompanyService.getByCompany(address, allocatedCompany);
		VatRate vatRate = null;
		if ((addressSettings != null) && addressSettings.getVatRate() != null) {
			vatRate = addressSettings.getVatRate();
		}
		if (vatRate == null) {
			CompanySettingsForAllocatedCompany companySettings = this.companySettingsForAllocatedCompanyService.getByCompany(address.getSub().getComp(), allocatedCompany);
			if (companySettings != null && companySettings.getVatrate() != null) {
				vatRate = companySettings.getVatrate();
			}
		}
		// If vatRate is not determined via settings, return null (user should initialize at company level)
		return vatRate;
	}

	@Override
	protected BaseDao<VatRate, String> getBaseDao() {
		return this.vatRateDao;
	}
}