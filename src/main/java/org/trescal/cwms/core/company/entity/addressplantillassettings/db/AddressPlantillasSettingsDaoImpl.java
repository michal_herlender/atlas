package org.trescal.cwms.core.company.entity.addressplantillassettings.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.addressplantillassettings.AddressPlantillasSettings;
import org.trescal.cwms.core.company.entity.addressplantillassettings.AddressPlantillasSettings_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;

@Repository
public class AddressPlantillasSettingsDaoImpl extends AllocatedToCompanyDaoImpl<AddressPlantillasSettings, Integer>
		implements AddressPlantillasSettingsDao {

	@Override
	protected Class<AddressPlantillasSettings> getEntity() {
		return AddressPlantillasSettings.class;
	}

	@Override
	public AddressPlantillasSettings getForAddress(int addrid, int allocatedcoid) {
		return getFirstResult(cb -> {
			CriteriaQuery<AddressPlantillasSettings> cq = cb.createQuery(AddressPlantillasSettings.class);
			Root<AddressPlantillasSettings> plantillasSettings = cq.from(AddressPlantillasSettings.class);
			Join<AddressPlantillasSettings, Address> address = plantillasSettings
					.join(AddressPlantillasSettings_.address);
			Join<AddressPlantillasSettings, Company> allocatedCompany = plantillasSettings.join("organisation");
			cq.where(cb.and(cb.equal(allocatedCompany.get(Company_.coid), allocatedcoid),
					cb.equal(address.get(Address_.addrid), addrid)));
			return cq;
		}).orElse(null);
	}
}