package org.trescal.cwms.core.company.entity.companysettings.db;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.dto.CompanySettingsDTO;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.quotation.dto.CompanySettingsForAllocatedCompanyDTO;

public interface CompanySettingsForAllocatedCompanyService
		extends BaseService<CompanySettingsForAllocatedCompany, Integer> {

	/**
	 * Gets the settings entity - if it exists - for the company as used by the
	 * business company
	 * 
	 * @param company
	 *            Company that the settings belong to
	 * @param allocatedCompany
	 *            BusinessCompany that the settings are used by
	 * @return CompanySettingsForAllocatedCompany (may be null)
	 */
	CompanySettingsForAllocatedCompany getByCompany(Company company, Company allocatedCompany);

	List<CompanySettingsForAllocatedCompany> getAll(Set<Integer> companyIds, Company allocatedCompany);

	List<CompanySettingsDTO> getDtosForAllocatedCompany(Collection<Integer> companyIds, Integer allocatedCompanyId);

	/**
	 * Creates and initializes a new (unsaved) CompanySettingsForAllocatedCompany
	 * object
	 * 
	 * @param company
	 *            Company that the settings belong to
	 * @param allocatedCompany
	 *            BusinessCompany that the settings are used by
	 * @param newCompany
	 *            indicates if the company is a new company being created here;
	 *            affects companyStatus and active.
	 * @return newly initialized CompanySettingsForAllocatedCompany
	 */
	CompanySettingsForAllocatedCompany initializeForCompany(Company company, Company allocatedCompany,
			boolean newCompany);
	
	List<CompanySettingsForAllocatedCompanyDTO> getAllSettingsDtosForAllocatedCompany(Collection<Integer> companyIds, Collection<Integer> allocatedCompanyIds);
}