package org.trescal.cwms.core.company.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;

import lombok.Setter;

/**
 * Formbacking Object used for adding a new {@link Company}.
 * 
 * @author Richard
 */
@Setter
public class CompanyAddForm {
	private String coname;
	private String legalIdentifier;
	private CompanyRole companyRole;
	private Integer countryId;
	private String subdivName;
	private String siretNumber;
	// For Client companies
	private Integer businessAreaId;
	// For Business companies
	private String companyCode;
	private Integer currencyId;

	// Validated inside CompanyAddFormValidator as this is only used for client
	// companies.
	public Integer getBusinessAreaId() {
		return this.businessAreaId;
	}

	@Size(min = 3, max = 100)
	@Pattern(regexp = "[^,]*", message = "{error.coname.nocommas}")
	public String getConame() {
		return this.coname;
	}

	@NotNull
	public CompanyRole getCompanyRole() {
		return this.companyRole;
	}

	@NotNull
	public Integer getCountryId() {
		return this.countryId;
	}

	@NotEmpty
	public String getSubdivName() {
		return this.subdivName;
	}

	@NotEmpty
	@Size(min = 2, max = 20)
	public String getLegalIdentifier() {
		return legalIdentifier;
	}

	@Size(max = 14)
	public String getSiretNumber() {
		return siretNumber;
	}

	// Only required for Business company
	public String getCompanyCode() {
		return companyCode;
	}

	public Integer getCurrencyId() {
		return currencyId;
	}

}
