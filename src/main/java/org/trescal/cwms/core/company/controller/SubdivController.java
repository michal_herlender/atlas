package org.trescal.cwms.core.company.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.JsonController;

@Controller
@JsonController
public class SubdivController {

	@Autowired
	private SubdivService subdivService;

	@RequestMapping("/searchBusinessSubdivs.json")
	@ResponseBody
	public List<SubdivKeyValue> searchBusinessSubdiv(
			@RequestParam(name = "partialSubdivName", required = true) String partialSubdivName) {
		return subdivService.searchBusinessSubdivsByName(partialSubdivName);
	}
}