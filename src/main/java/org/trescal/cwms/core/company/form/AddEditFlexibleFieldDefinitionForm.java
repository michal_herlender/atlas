package org.trescal.cwms.core.company.form;

import org.springframework.util.AutoPopulatingList;

public class AddEditFlexibleFieldDefinitionForm {
	
	private Integer coid;
	private String name;
	private Integer typeId;
	private AutoPopulatingList<FlexibleFieldLibraryValueDTO> values;
	private Integer fieldDefinitionId;
	private Boolean updatable;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getTypeId() {
		return typeId;
	}
	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}
	public AutoPopulatingList<FlexibleFieldLibraryValueDTO> getValues() {
		return values;
	}
	public void setValues(AutoPopulatingList<FlexibleFieldLibraryValueDTO> values) {
		this.values = values;
	}
	public Integer getCoid() {
		return coid;
	}
	public void setCoid(Integer coid) {
		this.coid = coid;
	}
	public Integer getFieldDefinitionId() {
		return fieldDefinitionId;
	}
	public void setFieldDefinitionId(Integer fieldDefinitionId) {
		this.fieldDefinitionId = fieldDefinitionId;
	}
	public Boolean getUpdatable() {
		return updatable;
	}
	public void setUpdatable(Boolean updatable) {
		this.updatable = updatable;
	}
	
}
