package org.trescal.cwms.core.company.entity.mailgrouptype;

import java.util.EnumSet;
import java.util.Locale;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum MailGroupType {
	UNDEFINED(false, "mailgrouptype.undefined", "Undefined", "mailgrouptype.undefined", "Undefined"),
	COSTINGS(true, "mailgrouptype.costings", "Costings", "mailgrouptype.desccostings", "Defines a group of contacts that a costing must be sent to"), 
	FAULTS(true, "mailgrouptype.faults", "Faults", "mailgrouptype.descfaults", "Defines a group of contacts that notifications about a faulty unit must be sent to"), 
	COSHH(true, "mailgrouptype.coshh", "COSHH", "mailgrouptype.desccoshh", "Defines a group of contacts that a COSHH Declaration request must be sent to"), 
	RECALLSUMMARY(true, "mailgrouptype.recallsummary", "Recall Summary", "mailgrouptype.descrecallsummary", "Defines a group of contacts that will receive recall notification summaries for all recalled instruments in their subdivsion"),
	REVERSE_TRACEABILITY(true, "mailgrouptype.reversetraceability", "Reverse Traceability", "mailgrouptype.descreversetraceability", "Defines a group of contacts that receive a reverse traceability report for a business subdivision");

	private ReloadableResourceBundleMessageSource messages;
	private boolean active;
	private String messageCodeName;
	private String messageCodeDesc;
	private String name;
	private String description;
		
	@Component
	public static class MailGroupTypeEMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (MailGroupType rt : EnumSet.allOf(MailGroupType.class))
               rt.setMessageSource(messages);
        }
	}
	
	public static Set<MailGroupType> getActiveMailGroupTypes() {
		return EnumSet.complementOf(EnumSet.of(UNDEFINED));
	}
	
	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}
	
	private MailGroupType(boolean active, String messageCodeName, String name, String messageCodeDesc, String description) {
		this.active = active;
		this.messageCodeName = messageCodeName;
		this.name = name;
		this.messageCodeDesc = messageCodeDesc;
		this.description = description;
	}
	
	public boolean isActive() {
		return active;
	}
	
	public String getName()
	{
		Locale loc = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCodeName, null, this.name, loc);
		}
		return this.toString();
	}
	
	public String getDescription()
	{
		Locale loc = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCodeDesc, null, this.description, loc);
		}
		return this.toString();
	}
	
	public String getMessageCodeName() {
		return messageCodeName;
	}
	
	public String getMessageCodeDesc() {
		return messageCodeDesc;
	}
}
