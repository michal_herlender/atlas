package org.trescal.cwms.core.company.entity.company.db;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.dto.CompanySearchResultWrapper;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Service("CompanyDWRService")
public class CompanyDWRServiceImpl implements CompanyDWRService {

	@Autowired
	private CompanyDao companyDao;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private SubdivService subServ;
	
	@Override
	public List<CompanySearchResultWrapper> searchCompanyByRolesHQL(String searchName, boolean active,
			boolean includeDeactivatedCompanies, String[] role, HttpSession session) {
		@SuppressWarnings("unchecked")
		KeyValue<Integer, String> companyDto = (KeyValue<Integer, String>) session
				.getAttribute(Constants.SESSION_ATTRIBUTE_COMPANY);
		Company allocatedCompany = companyService.get(companyDto.getKey());
		List<CompanyRole> roles = Arrays.asList(role).stream()
				.map(roleName -> CompanyRole.valueOf(roleName.toUpperCase())).collect(Collectors.toList());
		return this.companyDao.searchByCompanyRoles(searchName, active, roles, includeDeactivatedCompanies, allocatedCompany);
	}

	@Override
	public void updateCompanySubdivDefault(int coid, int subdivid) {
		Company comp = this.companyService.get(coid);
		Subdiv sub = this.subServ.get(subdivid);
		if (sub.getComp().getCoid() == comp.getCoid()) {
			comp.setDefaultSubdiv(sub);
			this.companyService.update(comp);
		}
	}

}