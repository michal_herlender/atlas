package org.trescal.cwms.core.company.entity.addressprintformat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.PrintSubdivisionOnAddress;
import org.trescal.cwms.core.system.enums.Scope;

import com.ibm.icu.text.MessageFormat;

/*
 * Component to properly format a mailing address, based on the country of the mailing address.
 * Formats the country name in the provided Locale.
 * Prints the subdivision name, if configured via system defaults.
 * Prints the company name, if printCompanyName is true (context dependent, e.g. Trescal letterhead)
 * Skips the addr2 and addr3 lines, if null
 * Leaves Address null, in case of bad data.
 */
@Component
public class AddressPrintFormatter {
	@Autowired
	private PrintSubdivisionOnAddress printSubdivisionOnAddress;
	
	public static final String PARAM_ADDR_1 = "addr1";
	public static final String PARAM_ADDR_2 = "addr2";
	public static final String PARAM_ADDR_3 = "addr3";
	public static final String PARAM_TOWN = "town";
	public static final String PARAM_POSTCODE = "postcode";
	public static final String PARAM_REGIONNAME = "regionname";
	public static final String PARAM_REGIONCODE = "regioncode";

	public List<String> getAddressText(Address address, Locale locale, boolean printCompanyName) {
		List<String> result = new ArrayList<>();
		if (address != null) {
			boolean printSubdivisionName = printSubdivisionOnAddress.parseValueHierarchical(Scope.SUBDIV, address.getSub(), null);
			if (printCompanyName)
				result.add(address.getSub().getComp().getConame());
			if (printSubdivisionName)
				result.add(address.getSub().getSubname());
			Map<String,Object> parameters = new HashMap<String, Object>();
			parameters.put(PARAM_ADDR_1, address.getAddr1() != null ? address.getAddr1() : "");
			parameters.put(PARAM_ADDR_2, address.getAddr2() != null ? address.getAddr2() : "");
			parameters.put(PARAM_ADDR_3, address.getAddr3() != null ? address.getAddr3() : "");
			parameters.put(PARAM_POSTCODE, address.getPostcode() != null ? address.getPostcode() : "");
			// TODO replace with region codes, once regions defined in database by country
			parameters.put(PARAM_REGIONCODE, address.getCounty() != null ? address.getCounty() : "");
			parameters.put(PARAM_REGIONNAME, address.getCounty() != null ? address.getCounty() : "");
			parameters.put(PARAM_TOWN, address.getTown() != null ? address.getTown() : "");
			List<String> formatPatterns = address.getCountry().getAddressPrintFormat().getFormatPatterns();
			
			for (String pattern : formatPatterns) {
				// Note, we use the ICU messageformat as it supports parameters by name
				String formattedLine = MessageFormat.format(pattern, parameters);
				if (!formattedLine.trim().isEmpty()) {
					result.add(formattedLine);
				}
			}
			
			String destinationCountryName = (new Locale("", address.getCountry().getCountryCode())).getDisplayCountry(locale);
			result.add(destinationCountryName.toUpperCase(locale));
		}
		return result;
	}
}
