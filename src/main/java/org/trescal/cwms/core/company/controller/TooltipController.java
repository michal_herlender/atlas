package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.dto.CompanyDTO;
import org.trescal.cwms.core.company.dto.ContactDTO;
import org.trescal.cwms.core.company.dto.SubdivDTO;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class TooltipController {
	
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private CompanySettingsForAllocatedCompanyService settingsService;
	@Autowired
	private ContactService contactService;

	@RequestMapping(value="/subdiv.json", method=RequestMethod.GET)
	@ResponseBody
	public SubdivDTO getSubdiv(
			@RequestParam(value="subdivid", required=true) int subdivid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		Subdiv subdiv = subdivService.get(subdivid);
		return new SubdivDTO(subdiv);
	}

	@RequestMapping(value="/company.json", method=RequestMethod.GET)
	@ResponseBody
	public CompanyDTO getCompany(
			@RequestParam(value="coid", required=true) int coid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		Company company = companyService.get(coid);
		Company allocatedcompany = companyService.get(companyDto.getKey());
		CompanySettingsForAllocatedCompany settings = settingsService.getByCompany(company, allocatedcompany);
		if (settings == null) {
			settings = settingsService.initializeForCompany(company, allocatedcompany, false);
		}
		return new CompanyDTO(company, settings);
	}
	
	@RequestMapping(value="/contact.json", method=RequestMethod.GET)
	@ResponseBody
	public ContactDTO getCntact(
			@RequestParam(value="personid", required=true) int personid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		Contact contact = contactService.get(personid);
		Company allocatedcompany = companyService.get(companyDto.getKey());
		CompanySettingsForAllocatedCompany settings = settingsService.getByCompany(contact.getSub().getComp(), allocatedcompany);
		if (settings == null) {
			settings = settingsService.initializeForCompany(contact.getSub().getComp(), allocatedcompany, false);
		}
		return new ContactDTO(contact, settings);
	}
}