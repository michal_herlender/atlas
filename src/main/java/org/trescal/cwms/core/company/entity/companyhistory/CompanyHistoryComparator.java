package org.trescal.cwms.core.company.entity.companyhistory;

import java.util.Comparator;

/**
 * @author jamiev
 */
public class CompanyHistoryComparator implements Comparator<CompanyHistory>
{
	public int compare(CompanyHistory ch1, CompanyHistory ch2)
	{
		if (ch1.getComp().getConame().compareToIgnoreCase(ch2.getComp().getConame()) != 0)
		{
			return ch1.getComp().getConame().compareToIgnoreCase(ch2.getComp().getConame());
		}
		else
		{
			if (ch1.getValidTo().compareTo(ch2.getValidTo()) != 0)
			{
				return ch2.getValidTo().compareTo(ch1.getValidTo());
			}
			else
			{
				return ((Integer) ch2.getId()).compareTo(ch1.getId());
			}
		}
	}
}