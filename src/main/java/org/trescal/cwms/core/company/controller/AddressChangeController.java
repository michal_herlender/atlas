package org.trescal.cwms.core.company.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.avalara.AvalaraConnector;
import org.trescal.cwms.core.company.dto.AddressValidationDTO;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.entity.address.AddressValidator;
import org.trescal.cwms.core.company.entity.location.LocationDTO;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.system.Constants;

import net.avalara.avatax.rest.client.models.AddressValidationInfo;
import net.avalara.avatax.rest.client.models.ValidatedAddressInfo;

@Controller
@JsonController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class AddressChangeController {

	@Autowired
	private AvalaraConnector avalaraConnector;
	@Autowired
	private LocationService locationservice;

	@GetMapping(value = "/addrChange.json")
	@ResponseBody
	public List<LocationDTO> changeAddr(@RequestParam(name = "addrId", required = false) Integer addrid) {
		return locationservice.getAddressLocations(addrid, true);
	}

	@PostMapping("validateaddress.json")
	@ResponseBody
	public AddressValidationDTO validateAddress(@RequestBody AddressValidationInfo address,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyDto) {
		ValidatedAddressInfo addressInfo = avalaraConnector.validateAddress(address, companyDto.getKey());
		if (addressInfo == null)
			return new AddressValidationDTO(false, null, null, null);
		else {
			return new AddressValidationDTO(true, addressInfo, AddressValidator.AVALARA, LocalDateTime.now().toString());
		}
	}
}