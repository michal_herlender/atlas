package org.trescal.cwms.core.company.entity.address;

/*
 * Keep order of elements!
 */
public enum AddressValidator {
	MANUALLY, AVALARA;
}