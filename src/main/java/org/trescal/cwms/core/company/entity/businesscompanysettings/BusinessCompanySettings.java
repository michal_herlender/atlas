package org.trescal.cwms.core.company.entity.businesscompanysettings;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Setter;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.businesscompanylogo.BusinessCompanyLogo;
import org.trescal.cwms.core.company.entity.company.Company;

/*
 * Stores global settings relevant to business companies
 */

@Entity
@Table(name = "businesscompanysettings")
@Setter
public class BusinessCompanySettings extends Versioned {

	private int id;
	private Company company;
	private String capital;
	private Integer defaultQuotationDuration;
	private InterbranchSalesMode interbranchSalesMode;
	private BigDecimal interbranchDiscountRate;
	private BigDecimal interbranchMarkupRate;
	private String legalRegistration1;
	private String legalRegistration2;
	private BusinessCompanyLogo logo;
	private BigDecimal hourlyRate;
	private Boolean usesPlantillas;
	private Boolean usePriceCatalog;
	private Integer warrantyCalibration;
	private Integer warrantyRepaire;
	private Boolean useTaxableOption;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	@NotNull
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="companyid", nullable=false)
	public Company getCompany() {
		return company;
	}

	@NotNull
	@Column(name="capital", length=30, nullable=false)
	public String getCapital() {
		return capital;
	}

	// TODO add @NotNull and database constraints
	@Column(name = "defaultquotationduration")
	@Type(type = "int")
	public Integer getDefaultQuotationDuration() {
		return defaultQuotationDuration;
	}
	
	@Column(name = "interbranchdiscountrate", unique = false, nullable = true, precision = 5, scale = 2)
	public BigDecimal getInterbranchDiscountRate() {
		return interbranchDiscountRate;
	}

	@Column(name = "interbranchmarkuprate", unique = false, nullable = true, precision = 5, scale = 2)
	public BigDecimal getInterbranchMarkupRate() {
		return interbranchMarkupRate;
	}

	// TODO add @NotNull and database constraints
	@Column(name="interbranchsalesmode")
	@Enumerated(EnumType.STRING)
	public InterbranchSalesMode getInterbranchSalesMode() {
		return interbranchSalesMode;
	}

	@NotNull
	@Column(name="legalRegistration1", length=100, nullable=false)
	public String getLegalRegistration1() {
		return legalRegistration1;
	}
	
	@NotNull
	@Column(name="legalRegistration2", length=100, nullable=false)
	public String getLegalRegistration2() {
		return legalRegistration2;
	}

	@NotNull
	@Column(name="logo", nullable=false)
	@Enumerated(EnumType.STRING)
	public BusinessCompanyLogo getLogo() {
		return logo;
	}

	@Column(name = "hourlyrate", unique = false, nullable = true, precision = 10, scale = 2)
	public BigDecimal getHourlyRate() {
		return hourlyRate;
	}

	@NotNull
	@Column(name = "usesPlantillas", nullable = false, columnDefinition = "bit default 0")
	public Boolean getUsesPlantillas() {
		return usesPlantillas;
	}

	@NotNull
	@Column(name = "usePriceCatalog", nullable = false, columnDefinition = "bit default 1")
	public Boolean getUsePriceCatalog() {
		return usePriceCatalog;
	}
	
	@NotNull
	@Column(name = "warrantyRepaire" , nullable  = false)
	public Integer getWarrantyRepaire() {
		return warrantyRepaire;
	}
	
	@NotNull
	@Column(name = "warrantyCalibration" , nullable  = false)
	public Integer getWarrantyCalibration() {
		return warrantyCalibration;
	}

	@NotNull
	@Column(name = "useTaxableOption", nullable = false)
	public Boolean getUseTaxableOption() {
		return useTaxableOption;
	}
}