package org.trescal.cwms.core.company.form;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

/**
 * Custom Validator for validating a new {@link Contact}.
 * 
 * @author Richard
 */
@Component
public class ContactAddFormValidator extends AbstractBeanValidator {
	@Autowired
	private ContactService conServ;
	@Autowired
	private SubdivService subdivServ;
	@Autowired
	private UserService userServ;

	public boolean supports(Class<?> clazz) {
		return clazz.equals(ContactAddForm.class);
	}

	@Override
	public void validate(Object command, Errors errors, Object... validationHints) {
		super.validate(command, errors);
		ContactAddForm caf = (ContactAddForm) command;

		Integer userid = (Integer) validationHints[0];

		Subdiv subdiv = this.subdivServ.get(caf.getSubdivId());

		// make sure the subdivision has at least one address before allowing a
		// contact to be added, added a check via the subdivision in case the
		// user has added after getting this message
		if (subdiv.getAddresses().isEmpty()) {
			errors.reject("error.contact.address",
					"Please add an address to the subdivision before adding your first contact");
		}

		// make sure a contact with the same name doesen't already exist
		if (this.conServ.contactExists(caf.getSubdivId(), caf.getFirstName(), caf.getLastName())) {
			errors.rejectValue("firstName", "error.contact.firstName",
					"A contact with this name for this subdivision already exists");
		}

		// @ron's request: make sure the contact has a phone number
		if (((caf.getTelephone() == null) || caf.getTelephone().trim().isEmpty())
				&& ((caf.getMobile() == null) || caf.getMobile().trim().isEmpty())) {
			errors.rejectValue("telephone", "error.contact.telephone",
					"The contact must have either a telephone or mobile number");

		}

		// Only (potentially) validate user name and password if creating
		// account
		if (caf.isCreateAccount()) {
			// a user name has been set - now validate the user does not already
			// exist
			if (StringUtils.hasLength(caf.getUsername())) {
				super.validate(caf, errors, ContactAddForm.UsernameGroup.class);
				if (caf.getUsername().indexOf(' ') > -1)
					errors.rejectValue("username", "error.space.character.notallowed");
				User user = this.userServ.get(caf.getUsername().trim());
				if (user != null) {
					errors.rejectValue("username", "error.contact.username", new String[] { user.getCon().getName() },
							"The contact {0} already has this username.  Please select another.");
				}
			}

			// A password has been set - validate it
			if (StringUtils.hasLength(caf.getPassword())) {
				super.validate(caf, errors, ContactAddForm.PasswordGroup.class);
				if (caf.getPassword().indexOf(' ') > -1)
					errors.rejectValue("password", "error.space.character.notallowed");
				if (caf.getPassword2().indexOf(' ') > -1)
					errors.rejectValue("password2", "error.space.character.notallowed");
				if (!StringUtils.hasLength(caf.getPassword2())) {
					errors.rejectValue("password", "error.contact.password.required",
							"Please enter a password in both fields");
				} else if (!caf.getPassword().equals(caf.getPassword2())) {
					errors.rejectValue("password", "error.contact.password.mismatch",
							"The chosen passwords do not appear to match, please re-enter");
				}
				ResultWrapper validPassword = userServ.passwordValid(caf.getPassword());
				if (!validPassword.isSuccess()) {
					errors.rejectValue("password", validPassword.getCode(), validPassword.getMessage());
				}
			} else {
				errors.rejectValue("password", "error.contact.password.required",
						"Please enter a password in both fields");
			}
		}

		// All email addresses are validated
		// Use ApacheCommons EmailValidator, catches more than @Email
		// annotation; email addresses are required and validated
		if (!EmailValidator.getInstance(false).isValid(caf.getEmail())) {
			errors.rejectValue("email", "error.email", null, "The email address entered is not of a valid format.");
		}

		// hr id exists
		if (subdiv.getComp().getCompanyRole().equals(CompanyRole.BUSINESS) && conServ.getHrIdExists(caf.getHrid())) {
			errors.rejectValue("hrid", "error.contact.hrid.alreadyexists", "The id already exists");
		}

		if (subdiv.getComp().getCompanyRole().equals(CompanyRole.BUSINESS)) {
			User user = this.conServ.get(userid).getUser();

			if (!user.getAuthorities().contains(Permission.ADD_CONTACT_BUSINESS)) {
				errors.reject("errors.cantcreatebcuser", "You Don't have permission to create a user");
			}
		}

	}

}