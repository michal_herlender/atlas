package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.company.dto.SubdivSearchResultWrapper;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;

import java.util.List;

@RestController
@RequestMapping("subdiv")
public class SubdivAjaxController {

    @Autowired
    private SubdivService subdivService;

    @GetMapping("activeByCoid.json")
    List<SubdivSearchResultWrapper> getActiveByCoid(@RequestParam Integer coid) {
        return subdivService.getAllActiveCompanySubdivsHQL(coid);
    }
}
