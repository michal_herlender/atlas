package org.trescal.cwms.core.company.form;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Setter;
import org.trescal.cwms.core.company.entity.companystatus.CompanyStatus;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;

/*
 * Following fields are shared among all company roles
 */
@Setter
public abstract class CompanyEditFinanceForm {

    public static final String ERROR_CODE_INVOICE_FREQUENCY = "{error.company.finance.invoicefrequency.notnull}";
    public static final String ERROR_CODE_PAYMENT_TERMS = "{error.company.finance.paymentterms.notnull}";
    public static final String ERROR_CODE_PAYMENT_MODE = "{error.company.finance.paymentmode.notnull}";

    private int coid;
    private String legalIdentifier;
    private String fiscalIdentifier;
    private CompanyStatus status;
    private Boolean active;
    private Boolean onStop;
    private Integer invoiceFrequencyId;
    private Integer paymentModeId;
    private PaymentTerm paymentTerm;
    private Boolean invoiceFactoring;

    public int getCoid() {
        return coid;
    }

    @Size(min = 2, max = 20)
    public String getLegalIdentifier() {
        return legalIdentifier;
    }

    @Size(min = 0, max = 30)
    public String getFiscalIdentifier() {
        return fiscalIdentifier;
    }

    @NotNull
    public CompanyStatus getStatus() {
        return status;
    }

    @NotNull
    public Boolean getActive() {
        return active;
    }

    @NotNull
    public Boolean getOnStop() {
        return onStop;
    }

    /*
     * The number 0 is used to indicate no record, so this validates that a record has been selected
     */
    @Min(value = 1, message = ERROR_CODE_INVOICE_FREQUENCY)
    public Integer getInvoiceFrequencyId() {
        return invoiceFrequencyId;
    }

    /*
     * The number 0 is used to indicate no bank account, so this validates that a bank account has been selected
     */
    @Min(value = 1, message = ERROR_CODE_PAYMENT_MODE)
    public Integer getPaymentModeId() {
        return paymentModeId;
    }

    @NotNull
    public PaymentTerm getPaymentTerm() {
        return paymentTerm;
    }

    @NotNull
    public Boolean getInvoiceFactoring() {
        return invoiceFactoring;
    }
}