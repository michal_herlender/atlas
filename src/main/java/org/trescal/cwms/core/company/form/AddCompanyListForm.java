package org.trescal.cwms.core.company.form;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AddCompanyListForm {
	
	@NotNull
	@Length(min=1, max=50)
	private String nameList;
	private Integer orgid;

}
