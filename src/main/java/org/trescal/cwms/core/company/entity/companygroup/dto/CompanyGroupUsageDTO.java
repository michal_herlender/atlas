package org.trescal.cwms.core.company.entity.companygroup.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Provides information about company group together with usage count
 *
 */
@Getter @Setter
public class CompanyGroupUsageDTO {
	private Integer id;
	private String groupName;
	private Boolean active;	
	private Long companyCount;

	/**
	 * Used in JPA query projection
	 */
	public CompanyGroupUsageDTO(Integer id, String groupName, Boolean active, Long companyCount) {
		super();
		this.id = id;
		this.groupName = groupName;
		this.active = active;
		this.companyCount = companyCount;
	}
}
