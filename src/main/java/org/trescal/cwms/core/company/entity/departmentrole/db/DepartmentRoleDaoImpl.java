package org.trescal.cwms.core.company.entity.departmentrole.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.departmentrole.DepartmentRole;

@Repository("DepartmentRoleDao")
public class DepartmentRoleDaoImpl extends BaseDaoImpl<DepartmentRole, Integer> implements DepartmentRoleDao {
	
	@Override
	protected Class<DepartmentRole> getEntity() {
		return DepartmentRole.class;
	}
}