package org.trescal.cwms.core.company.entity.contactinstructionlink.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

public interface ContactInstructionLinkService extends
		BaseService<ContactInstructionLink, Integer>
{
	Long count(Contact contact, Company allocatedCompany);
	List<ContactInstructionLink> getAllForContact(Contact contact, Company allocatedCompany);
	List<ContactInstructionLink> getForContactAndTypes(Contact contact, Company allocatedCompany, InstructionType... types);
}