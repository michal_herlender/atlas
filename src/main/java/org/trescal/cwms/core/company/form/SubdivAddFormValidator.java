package org.trescal.cwms.core.company.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class SubdivAddFormValidator extends AbstractBeanValidator
{
	public static String MESSAGE_CODE_SIZE = "error.size.between";
	public static String DEFAULT_MESSAGE_SIZE = "size must be between {0} and {1}";

	@Autowired
	private SubdivService subServ;
	
	@Autowired
	private AddressAddFormValidator addressAddFormValidator;
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(SubdivAddForm.class);
	}
	
	@Override
	public void validate(Object target, Errors errors)
	{
		super.validate(target, errors);
		SubdivAddForm saf = (SubdivAddForm) target;
		// business logic to prevent duplicate subdiv names
		Subdiv subdiv = saf.getSubdiv();
		Long duplicateCount = this.subServ.countCompanySubdivsByName(subdiv.getComp().getCoid(), subdiv.getSubname().trim(), null); 
		if (duplicateCount > 0)
			errors.rejectValue("subdiv.subname", null, "There is already a subdivision for this company with this name");
		this.addressAddFormValidator.validateLegalRegistration(saf.getAddress(), subdiv.getComp(), errors);
		if (subdiv.getComp().getCompanyRole().equals(CompanyRole.BUSINESS)) {
			verifyField(subdiv.getAnalyticalCenter(), "subdiv.analyticalCenter", errors);
			verifyField(subdiv.getSubdivCode(), "subdiv.subdivCode", errors);
		}
	}
	private void verifyField(String fieldValue, String path, Errors errors) {
		if ((fieldValue == null) ||
			 fieldValue.trim().length() == 0 || 
			 fieldValue.trim().length() > 3) {
				errors.rejectValue(path, MESSAGE_CODE_SIZE, new Integer[] {1,3}, DEFAULT_MESSAGE_SIZE);
			}
	}}