package org.trescal.cwms.core.company.entity.addresssettings;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;

@Entity
@Table(name="addresssettingsforallocatedsubdiv", uniqueConstraints=@UniqueConstraint(columnNames= {"orgid","addressid"}))
public class AddressSettingsForAllocatedSubdiv extends Allocated<Subdiv> {
	private int id;
	private Address address;
	private TransportOption transportIn;
	private TransportOption transportOut;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="addressid", nullable=false)
	public Address getAddress() {
		return address;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "transportinid")
	public TransportOption getTransportIn()
	{
		return this.transportIn;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "transportoutid")
	public TransportOption getTransportOut()
	{
		return this.transportOut;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}

	public void setTransportIn(TransportOption transportIn)
	{
		this.transportIn = transportIn;
	}
	
	public void setTransportOut(TransportOption transportOut)
	{
		this.transportOut = transportOut;
	}	
}