package org.trescal.cwms.core.company.entity.businessarea.db;

import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CompoundSelection;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.businessarea.BusinessArea;
import org.trescal.cwms.core.company.entity.businessarea.BusinessArea_;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Repository("BusinessAreaDao")
public class BusinessAreaDaoImpl extends BaseDaoImpl<BusinessArea, Integer> implements BusinessAreaDao {

	@Override
	protected Class<BusinessArea> getEntity() {
		return BusinessArea.class;
	}

	@Override
	public List<KeyValueIntegerString> getAllTranslated(Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class);
			Root<BusinessArea> root = cq.from(BusinessArea.class);
			Expression<String> translatedName = joinTranslation(cb, root, BusinessArea_.nametranslation, locale);

			CompoundSelection<KeyValueIntegerString> selection = cb.construct(KeyValueIntegerString.class,
					root.get(BusinessArea_.businessareaid), translatedName);

			cq.select(selection);
			cq.distinct(true);
			cq.orderBy(cb.asc(translatedName));

			return cq;
		});
	}
}