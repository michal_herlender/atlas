package org.trescal.cwms.core.company.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.addresssettings.db.AddressSettingsForAllocatedSubdivService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.country.db.CountryService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.form.SubdivAddForm;
import org.trescal.cwms.core.company.form.SubdivAddFormValidator;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Displays a form to add a new {@link Subdiv} and a new address for the
 * {@link Subdiv}.
 *
 * @author richard
 */
@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class SubdivAddController {
	@Autowired
	private AddressService addressService;
	@Autowired
	private AddressSettingsForAllocatedSubdivService addressSettingsService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private CountryService countryService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private TransportOptionService transportOptionService;
	@Autowired
	private SubdivAddFormValidator validator;

	private static final Logger logger = LoggerFactory.getLogger(SubdivAddController.class);

	@ModelAttribute("command")
	protected SubdivAddForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@RequestParam(value = "coid", required = false, defaultValue = "0") Integer coid) throws Exception {
		Company company = this.companyService.get(coid);
		if ((company == null) || (coid == 0)) {
			throw new Exception("A valid company must be specified");
		} else {
			Subdiv s = new Subdiv();
			s.setComp(company);
			Address a = new Address();
			a.setSub(s);
			Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
			AddressSettingsForAllocatedSubdiv addressSettings = this.addressSettingsService.initializeForAddress(a,
					allocatedSubdiv);
			SubdivAddForm saf = new SubdivAddForm();
			saf.setSubdiv(s);
			saf.setAddress(a);
			saf.setAddressSettingsSubdiv(addressSettings);
			saf.setCountryId(company.getCountry().getCountryid());
			return saf;
		}
	}

	@RequestMapping(value = "/addsubdiv.htm", method = RequestMethod.POST)
	public ModelAndView onSubmit(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute("command") SubdivAddForm saf, BindingResult bindingResult) {
		// Manually validated due to need to set country on address
		Address a = saf.getAddress();
		a.setCountry(this.countryService.get(saf.getCountryId()));
		if (a.getAddressType() == null)
			a.setAddressType(EnumSet.noneOf(AddressType.class));
		this.validator.validate(saf, bindingResult);

		if (bindingResult.hasErrors()) {
			logger.debug("Error count: " + bindingResult.getErrorCount());
			for (ObjectError error : bindingResult.getAllErrors()) {
				logger.debug(error.getDefaultMessage());
			}
			return referenceData(subdivDto);
		}
		Subdiv s = saf.getSubdiv();
		s.setCreateDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		s.setActive(true);
		this.subdivService.save(s);
		// Keeping deprecated global address transport in/out for now until fully
		// upgraded
		AddressSettingsForAllocatedSubdiv addressSettings = saf.getAddressSettingsSubdiv();
		if (saf.getTransportInId() != null) {
			TransportOption transportIn = this.transportOptionService.get(saf.getTransportInId());
			addressSettings.setTransportIn(transportIn);
		}
		if (saf.getTransportOutId() != null) {
			TransportOption transportOut = this.transportOptionService.get(saf.getTransportOutId());
			addressSettings.setTransportOut(transportOut);
		}
		a.setCountry(countryService.get(saf.getCountryId()));
		this.addressService.save(a);
		s.setDefaultAddress(a);
		s = this.subdivService.merge(s);
		return new ModelAndView(new RedirectView("viewsub.htm?subdivid=" + s.getSubdivid()));
	}

	@RequestMapping(value = "/addsubdiv.htm", method = RequestMethod.GET)
	public ModelAndView referenceData(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		Map<String, Object> model = new HashMap<>();
		// The "WITHOUT" address type is for DWR queries only so must not be added to
		// user selections
		Set<AddressType> types = EnumSet.complementOf(EnumSet.of(AddressType.WITHOUT));
		model.put("addressTypes", types);
		model.put("transportoptions", this.transportOptionService.getTransportOptionsFromSubdiv(subdivDto.getKey()));
		model.put("countrylist", countryService.getCountries());
		return new ModelAndView("trescal/core/company/subdivadd", model);
	}
}