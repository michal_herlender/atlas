package org.trescal.cwms.core.company.entity.companygroup.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.companygroup.CompanyGroup;
import org.trescal.cwms.core.company.entity.companygroup.dto.CompanyGroupUsageDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface CompanyGroupDao extends BaseDao<CompanyGroup, Integer> {
	public Long countByName(String groupName, Integer idToExclude);
	public List<KeyValueIntegerString> getDTOList(Boolean activeGroups);
	public List<CompanyGroupUsageDTO> getUsageDTOList();
}
