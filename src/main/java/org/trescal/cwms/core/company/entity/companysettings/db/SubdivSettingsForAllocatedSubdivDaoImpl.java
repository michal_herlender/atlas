package org.trescal.cwms.core.company.entity.companysettings.db;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;

import lombok.val;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.dto.SubdivSettingsForAllocatedSubdivDto;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv_;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.tools.StringJpaUtils;

import java.util.List;

import static org.trescal.cwms.core.tools.StringJpaUtils.trimAndConcatWithWhitespace;

@Repository
public class SubdivSettingsForAllocatedSubdivDaoImpl extends BaseDaoImpl<SubdivSettingsForAllocatedSubdiv, Integer> implements SubdivSettingForAllocatedSubdivDao {

	@Override
	protected Class<SubdivSettingsForAllocatedSubdiv> getEntity() {
		return SubdivSettingsForAllocatedSubdiv.class;
	}
	
	@Override
	public SubdivSettingsForAllocatedSubdiv get(Subdiv client, Subdiv business) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<SubdivSettingsForAllocatedSubdiv> cq = cb.createQuery(SubdivSettingsForAllocatedSubdiv.class);
		Root<SubdivSettingsForAllocatedSubdiv> setting = cq.from(SubdivSettingsForAllocatedSubdiv.class);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(setting.get(SubdivSettingsForAllocatedSubdiv_.subdiv), client));
		clauses.getExpressions().add(cb.equal(setting.get(SubdivSettingsForAllocatedSubdiv_.organisation), business));
		cq.where(clauses);
		TypedQuery<SubdivSettingsForAllocatedSubdiv> query = getEntityManager().createQuery(cq);
		try {
			return query.getSingleResult();
		}
		catch(NoResultException noResult) {
			return null;
		}
	}
	
	@Override
	public SubdivSettingsForAllocatedSubdiv get(Integer subdivId, Integer allocatedSubdivId) {
		return getFirstResult(cb -> {
			CriteriaQuery<SubdivSettingsForAllocatedSubdiv> cq = cb.createQuery(SubdivSettingsForAllocatedSubdiv.class);
			Root<SubdivSettingsForAllocatedSubdiv> setting = cq.from(SubdivSettingsForAllocatedSubdiv.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(setting.get(SubdivSettingsForAllocatedSubdiv_.subdiv), subdivId));
			clauses.getExpressions().add(cb.equal(setting.get(SubdivSettingsForAllocatedSubdiv_.organisation), allocatedSubdivId));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

	@Override
	public List<SubdivSettingsForAllocatedSubdivDto> findBySubdivId(Integer subdivId) {
	    return getResultList(cb -> {
	    	val cq = cb.createQuery(SubdivSettingsForAllocatedSubdivDto.class);
	    	val setting = cq.from(SubdivSettingsForAllocatedSubdiv.class);
	    	Join<SubdivSettingsForAllocatedSubdiv,Subdiv> allocated =
					(Join<SubdivSettingsForAllocatedSubdiv, Subdiv>) setting.join(SubdivSettingsForAllocatedSubdiv_.organisation).as(Subdiv.class);
	    	val contact = setting.join(SubdivSettingsForAllocatedSubdiv_.defaultBusinessContact,JoinType.LEFT);
	    	cq.where(cb.equal(setting.get(SubdivSettingsForAllocatedSubdiv_.subdiv),subdivId));
	    	cq.select(cb.construct(SubdivSettingsForAllocatedSubdivDto.class,
					setting.join(SubdivSettingsForAllocatedSubdiv_.subdiv).get(Subdiv_.subname),
					allocated.get(Subdiv_.subname),
					contact.get(Contact_.personid),
					trimAndConcatWithWhitespace(contact.get(Contact_.firstName),contact.get(Contact_.lastName)).apply(cb),
					contact.get(Contact_.email),
					contact.get(Contact_.telephone)
					));
	    	return cq;
		});
	}
}