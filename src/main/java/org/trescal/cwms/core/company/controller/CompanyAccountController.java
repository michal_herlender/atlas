package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.account.entity.bankdetail.BankDetail;
import org.trescal.cwms.core.account.entity.companyaccount.CompanyAccount;
import org.trescal.cwms.core.account.entity.companyaccount.CompanyAccountSource;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.country.db.CountryService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.form.CompanyAccountForm;
import org.trescal.cwms.core.company.form.CompanyAccountValidator;
import org.trescal.cwms.core.exception.controller.IntranetController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Displays the basic information about a {@link Company} and allows creation
 * and updating of an accounts profile for the company (that can be transferred
 * into a 3rd party accounts software package). A {@link Company} can have one
 * {@link CompanyAccount} which has one {@link Contact} and one {@link Address}.
 * The contact and address can be either existing contacts and addresses from
 * any one of the company's existing {@link Subdiv}s, or can be a custom created
 * {@link Address} or {@link Contact} that are solely for use for the accounts
 * profile and do not belong to a {@link Subdiv}.
 */

@Controller
@IntranetController
public class CompanyAccountController {
	@Value("#{props['cwms.config.thirdpartyaccounts.name']}")
	private String accountsSoftware;
	@Autowired
	private AddressService addressServ;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private CountryService countryService;
	@Autowired
	private ContactService contactServ;
	@Autowired
	private CompanyAccountValidator validator;

	public static final String FORM_NAME = "command";

	@InitBinder(FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute(FORM_NAME)
	protected CompanyAccountForm formBackingObject(
			@RequestParam(value = "coid", required = false, defaultValue = "0") Integer coid) throws Exception {
		CompanyAccountForm form = new CompanyAccountForm();
		Company company = this.compServ.get(coid);
		if ((coid == 0) || (company == null))
			throw new Exception("Company could not be found");
		else {
			CompanyAccount account;
			form.setCompany(company);

			account = new CompanyAccount();
			account.setCompany(company);
			account.setLineDiscount(new BigDecimal("0.00"));
			account.setSettlementDiscount(new BigDecimal("0.00"));
			account.setSettlementDays(0);
			form.setAddressSource(CompanyAccountSource.CUSTOM);
			form.setContactSource(CompanyAccountSource.CUSTOM);
			form.setNewAccount(true);
				form.setAccount(account);
			// bank details - if none exists then create a blank instance to
			// bind
			// back to
			if (account.getBankDetails() == null) {
				BankDetail bd = new BankDetail();
				form.setBankDetails(bd);
			} else {
				form.setBankDetails(account.getBankDetails());
				form.setIncludeBankDetails(true);
			}
			// set the account contact if there is one, into the fbo, otherwise
			// an
			// empty one to bind back to
				Contact contact = new Contact();
				contact.setAccountCompany(company);
				form.setAccountContact(contact);
				Address address = new Address();
				address.setAccountCompany(company);
				form.setAccountAddress(address);
			return form;
		}
	}

	@RequestMapping(value = "/companyaccounts.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute(FORM_NAME) CompanyAccountForm form) throws Exception {
		Company company = form.getCompany();
		Map<String, Object> model = new HashMap<>();
		// get a list of all contacts for this company
		model.put("contacts", this.contactServ.getAllCompanyContactsBySubCon(company.getCoid(), true));
		// get a list of addresses for this company
		model.put("addresses", this.addressServ.getAllCompanyAddresses(company.getCoid(), AddressType.WITHOUT, true));
		model.put("accountssoftware", this.accountsSoftware);
		model.put("countrylist", countryService.getAll(company.getCountry()));
		return new ModelAndView("trescal/core/company/companyaccounts", model);
	}

	@RequestMapping(value = "/companyaccounts.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(@Validated @ModelAttribute(FORM_NAME) CompanyAccountForm form, BindingResult result)
			throws Exception {
		if (result.hasErrors())
			return referenceData(form);
		Company company = form.getCompany();
		CompanyAccount account = form.getAccount();
		// save the accounts contact
		if (form.getContactSource() == CompanyAccountSource.CUSTOM) {
			Contact contact = form.getAccountContact();
			contact.setTitle(form.getTitle());
			account.setContact(contact);
		} else
			account.setContact(this.contactServ.get(form.getPersonid()));
		if (form.getAddressSource() == CompanyAccountSource.CUSTOM) {
			Address address = form.getAccountAddress();
			address.setCountry(countryService.get(form.getCountryId()));
			account.setAddress(address);
		} else
			account.setAddress(this.addressServ.get(form.getAddressid()));
		if (form.isIncludeBankDetails()) {
			account.setBankDetails(form.getBankDetails());
			account.getBankDetails().setCompanyAccount(account);
		}
		account.setAccountsStatus(AccountStatus.P);
		this.compServ.update(company);
		return new ModelAndView(new RedirectView("companyaccounts.htm?coid=" + form.getCompany().getCoid(), true));
	}

}
