package org.trescal.cwms.core.company.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.dto.InstructionLinkDTO;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink;
import org.trescal.cwms.core.company.entity.companyinstructionlink.db.CompanyInstructionLinkService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink;
import org.trescal.cwms.core.company.entity.contactinstructionlink.db.ContactInstructionLinkService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.db.SubdivInstructionLinkService;
import org.trescal.cwms.core.company.form.InstructionViewCommand;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionEntity;

/*
 * Component that genrates an InstructionViewCommand for a Company, Subdiv, or Contact
 * Externalized out of controllers to enable code reuse
 * GB 2015-12-04
 */

@Component @IntranetController
public class InstructionViewComponent {
	
	@Autowired
	private CompanyInstructionLinkService companyInstructionLinkService;
	@Autowired
	private SubdivInstructionLinkService subdivInstructionLinkService;
	@Autowired
	private ContactInstructionLinkService contactInstructionLinkService;
	
	public InstructionViewCommand viewCompany(Company company, Subdiv allocatedSubdiv) {
		InstructionViewCommand command = new InstructionViewCommand();
		command.setCoid(company.getCoid());
		command.setInstructionEntity(InstructionEntity.COMPANY);
		command.setEntityName(company.getConame());
		command.setCompanyName(company.getConame());
		addDtos(command, company, allocatedSubdiv);
		return command;
	}
	
	public InstructionViewCommand viewSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv) {
		InstructionViewCommand command = new InstructionViewCommand();
		command.setCoid(subdiv.getComp().getCoid());
		command.setSubdivid(subdiv.getSubdivid());
		command.setInstructionEntity(InstructionEntity.SUBDIV);
		command.setEntityName(subdiv.getSubname());
		command.setCompanyName(subdiv.getComp().getConame());
		command.setSubdivName(subdiv.getSubname());
		addDtos(command, subdiv.getComp(), allocatedSubdiv);
		addDtos(command, subdiv, allocatedSubdiv);
		return command;
	}
	
	public InstructionViewCommand viewContact(Contact contact, Subdiv allocatedSubdiv) {
		InstructionViewCommand command = new InstructionViewCommand();
		command.setCoid(contact.getSub().getComp().getCoid());
		command.setSubdivid(contact.getSub().getSubdivid());
		command.setPersonid(contact.getPersonid());
		command.setInstructionEntity(InstructionEntity.CONTACT);
		command.setEntityName(contact.getName());
		command.setCompanyName(contact.getSub().getComp().getConame());
		command.setSubdivName(contact.getSub().getSubname());
		command.setContactName(contact.getName());
		addDtos(command, contact.getSub().getComp(), allocatedSubdiv);
		addDtos(command, contact.getSub(), allocatedSubdiv);
		addDtos(command, contact, allocatedSubdiv);
		return command;
	}
	private void addDtos(InstructionViewCommand command, Company company, Subdiv allocatedSubdiv) {
		List<CompanyInstructionLink> instructions = this.companyInstructionLinkService.getAllForCompany(company, allocatedSubdiv);
		for (CompanyInstructionLink link: instructions) {
			command.getDtos().add(new InstructionLinkDTO(link));
		}
		command.setCompanyInstructionCount(instructions.size());
	}
	private void addDtos(InstructionViewCommand command, Subdiv subdiv, Subdiv allocatedSubdiv) {
		List<SubdivInstructionLink> instructions = this.subdivInstructionLinkService.getAllForSubdiv(subdiv, allocatedSubdiv);
		for (SubdivInstructionLink link: instructions) {
			command.getDtos().add(new InstructionLinkDTO(link));
		}
		command.setSubdivInstructionCount(instructions.size());
	}
	private void addDtos(InstructionViewCommand command, Contact contact, Subdiv allocatedSubdiv) {
		List<ContactInstructionLink> instructions = this.contactInstructionLinkService.getAllForContact(contact, allocatedSubdiv.getComp());
		for (ContactInstructionLink link: instructions) {
			command.getDtos().add(new InstructionLinkDTO(link));
		}
		command.setContactInstructionCount(instructions.size());
	}
}
