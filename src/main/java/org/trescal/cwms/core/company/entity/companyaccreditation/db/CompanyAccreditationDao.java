package org.trescal.cwms.core.company.entity.companyaccreditation.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditation;
import org.trescal.cwms.core.company.entity.supplier.Supplier;

public interface CompanyAccreditationDao extends BaseDao<CompanyAccreditation, Integer> {
	
	CompanyAccreditation findCompanyAccreditation(Integer supplierid, Integer coid);
	
	List<CompanyAccreditation> searchCompanyAccreditation(Integer coid);
	
	List<CompanyAccreditation> findExpiredCompanyAccreditations();
	
	List<CompanyAccreditation> getForBusinessCompany(Company company, Company businessCompany);
	
	CompanyAccreditation getForBusinessCompany(Company company, Company businessCompany, Supplier supplier);
}