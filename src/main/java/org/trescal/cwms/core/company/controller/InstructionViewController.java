package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Controller for showing all {@link Instruction}s for a {@link Company},
 * {@link Subdiv} or {@link Contact}.
 * 
 * @author Richard
 */
@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class InstructionViewController
{
	@Autowired
	private ContactService contServ;
	@Autowired
	private SubdivService subServ;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private InstructionViewComponent instructionViewComponent;
	
	public static final String VIEW_NAME = "trescal/core/company/viewinstructions"; 
	
	@RequestMapping(value="/viewinstructions.htm", params="coid", method=RequestMethod.GET)
	public String viewCompany(Model model, 
			@RequestParam(value="coid", required=true) Integer coid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Company company = compServ.get(coid);
		Subdiv allocatedSubdiv = this.subServ.get(subdivDto.getKey());
		model.addAttribute("form", instructionViewComponent.viewCompany(company, allocatedSubdiv));
		return VIEW_NAME;
	}
	
	@RequestMapping(value="/viewinstructions.htm", params="subdivid", method=RequestMethod.GET)
	public String viewSubdiv(Model model,
			@RequestParam(value="subdivid", required=true) Integer subdivid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Subdiv subdiv = subServ.get(subdivid);
		Subdiv allocatedSubdiv = this.subServ.get(subdivDto.getKey());
		model.addAttribute("form", instructionViewComponent.viewSubdiv(subdiv, allocatedSubdiv));
		return VIEW_NAME;

	}
	@RequestMapping(value="/viewinstructions.htm", params="personid", method=RequestMethod.GET)
	public String viewPerson(Model model,
			@RequestParam(value="personid", required=true) Integer personid, 
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Contact contact = contServ.get(personid);
		Subdiv allocatedSubdiv = this.subServ.get(subdivDto.getKey());
		model.addAttribute("form", instructionViewComponent.viewContact(contact, allocatedSubdiv));
		return VIEW_NAME;
	}

}
