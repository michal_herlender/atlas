package org.trescal.cwms.core.company.entity.paymentmode.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValue;

@Service
public class PaymentModeServiceImpl extends
		BaseServiceImpl<PaymentMode, Integer> implements PaymentModeService {

	@Autowired
	private MessageSource messageSource;
	@Autowired
	private PaymentModeDao paymentModeDao; 
	@Autowired
	private TranslationService translationService;
	
	public static final String MESSAGE_CODE_NONE = "company.none";
	
	@Override
	protected BaseDao<PaymentMode, Integer> getBaseDao() {
		return this.paymentModeDao;
	}

	@Override
	public PaymentMode get(String code) {
		return this.paymentModeDao.get(code);
	}

	@Override
	public List<KeyValue<Integer, String>> getDTOList(Locale locale, boolean prependNoneDto) {
		List<KeyValue<Integer, String>> list = new ArrayList<>();
		if (prependNoneDto) {
			list.add(new KeyValue<Integer, String>(0, messageSource.getMessage(MESSAGE_CODE_NONE, null, locale)));
		}
		List<PaymentMode> paymentModeList = this.getAll(); 
		for (PaymentMode paymentMode : paymentModeList) {
			String description = paymentMode.getCode() + " - " + this.translationService.getCorrectTranslation(paymentMode.getTranslations(), locale);
			list.add(new KeyValue<Integer, String>(paymentMode.getId(), description));
		}
		return list;
	}
}
