package org.trescal.cwms.core.company.entity.contactinstructionlink.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

@Service
public class ContactInstructionLinkServiceImpl extends
		BaseServiceImpl<ContactInstructionLink, Integer> implements
		ContactInstructionLinkService {
	
	private static final InstructionType[] NO_INSTRUCTIONTYPES = {};

	@Autowired
	private ContactInstructionLinkDao baseDao;
	
	@Override
	protected BaseDao<ContactInstructionLink, Integer> getBaseDao() {
		return baseDao;
	}

	@Override
	public List<ContactInstructionLink> getAllForContact(Contact contact, Company allocatedCompany) {
		return baseDao.getForContactAndTypes(contact, allocatedCompany, NO_INSTRUCTIONTYPES);
	}
	
	@Override
	public Long count(Contact contact, Company allocatedCompany) {
		return baseDao.count(contact, allocatedCompany);
	}

	@Override
	public List<ContactInstructionLink> getForContactAndTypes(Contact contact, Company allocatedCompany, InstructionType... types) {
		return baseDao.getForContactAndTypes(contact, allocatedCompany, types);
	}
}