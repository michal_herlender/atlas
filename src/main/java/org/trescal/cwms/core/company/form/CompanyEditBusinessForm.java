package org.trescal.cwms.core.company.form;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Setter;
import org.trescal.cwms.core.company.entity.businesscompanylogo.BusinessCompanyLogo;
import org.trescal.cwms.core.company.entity.businesscompanysettings.InterbranchSalesMode;

@Setter
public class CompanyEditBusinessForm extends CompanyEditFinanceForm {

    public static final String ERROR_CODE_BANK_ACCOUNT = "{error.company.finance.bankaccount.notnull}";

    private String companyCode;
    private String companyCapital;
    private Boolean useTaxableOptionOnPurchaseOrders;
    private Integer defaultQuotationDuration;
    private String legalRegistration1;
    private String legalRegistration2;
    private BusinessCompanyLogo logo;
    private Integer bankAccountId;
    private BigDecimal hourlyRate;
    private InterbranchSalesMode interbranchSalesMode;
    private BigDecimal interbranchDiscountRate;
    private BigDecimal interbranchMarkupRate;
    private Boolean usePriceCatalog;
    private Integer warrantyCalibration;
    private Integer warrantyRepaire;

    @Size(min = 1, max = 3)
    public String getCompanyCode() {
        return companyCode;
    }

    @Size(min = 1, max = 30)
    public String getCompanyCapital() {
        return companyCapital;
    }

    @NotNull
    public Boolean getUseTaxableOptionOnPurchaseOrders() {
        return this.useTaxableOptionOnPurchaseOrders;
    }

    @NotNull(message = "{error.value.notselected}")
    @Min(value = 1)
    public Integer getDefaultQuotationDuration() {
        return defaultQuotationDuration;
    }

    @Size(min = 1, max = 100)
    public String getLegalRegistration1() {
        return legalRegistration1;
    }

    @Size(min = 1, max = 100)
    public String getLegalRegistration2() {
        return legalRegistration2;
    }

    @NotNull
    public BusinessCompanyLogo getLogo() {
        return logo;
    }

    /*
     * The number 0 is used to indicate no bank account, so this validates that a bank account has been selected
     */
    @Min(value = 1, message = ERROR_CODE_BANK_ACCOUNT)
    public Integer getBankAccountId() {
        return bankAccountId;
    }

    @NotNull
    public BigDecimal getHourlyRate() {
        return hourlyRate;
    }

    @NotNull(message = "{error.value.notselected}")
    public InterbranchSalesMode getInterbranchSalesMode() {
        return interbranchSalesMode;
    }

    public BigDecimal getInterbranchDiscountRate() {
        return interbranchDiscountRate;
    }

    public BigDecimal getInterbranchMarkupRate() {
        return interbranchMarkupRate;
    }

    @NotNull
    public Boolean getUsePriceCatalog() {
        return usePriceCatalog;
    }

    @NotNull
    public Integer getWarrantyCalibration() {
        return warrantyCalibration;
    }

    @NotNull
    public Integer getWarrantyRepaire() {
        return warrantyRepaire;
    }
}