package org.trescal.cwms.core.company.entity.companyhistory;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Entity
@Table(name = "companyhistory")
public class CompanyHistory
{
	private Contact changedBy;
	private Company comp;
	private String coname;
	private int id;
	private Date validTo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "changedbyid")
	public Contact getChangedBy()
	{
		return this.changedBy;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coid")
	public Company getComp()
	{
		return this.comp;
	}
	
	@Length(max = 60)
	@Column(name = "coname", length = 60)
	public String getConame()
	{
		return this.coname;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "companyhistoryid")
	public int getId()
	{
		return this.id;
	}
	
	@Column(name = "validto")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getValidTo()
	{
		return this.validTo;
	}
	
	public void setChangedBy(Contact changedBy)
	{
		this.changedBy = changedBy;
	}
	
	public void setComp(Company comp)
	{
		this.comp = comp;
	}
	
	public void setConame(String coname)
	{
		this.coname = coname;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public void setValidTo(Date validTo)
	{
		this.validTo = validTo;
	}
}