package org.trescal.cwms.core.company.entity.contact;

import java.util.Comparator;

/**
 * Inner comparator class for sorting {@link Contact} entities when returning
 * collections
 * 
 * @author jamiev
 */
public class ContactComparator implements Comparator<Contact>
{
	public int compare(Contact contact, Contact anotherContact)
	{
		String lastname1 = contact.getLastName();
		String lastname2 = anotherContact.getLastName();

		if (lastname1.equals(lastname2))
		{
			String firstname1 = contact.getFirstName();
			String firstname2 = anotherContact.getLastName();

			if (firstname1.equals(firstname2))
			{
				Integer personid1 = contact.getPersonid();
				Integer personid2 = anotherContact.getPersonid();

				return personid1.compareTo(personid2);
			}
			else
			{
				return firstname1.compareTo(firstname2);
			}
		}
		else
		{
			return lastname1.compareTo(lastname2);
		}
	}
}