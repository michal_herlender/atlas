package org.trescal.cwms.core.company.dto;

import org.trescal.cwms.core.company.entity.addresstype.AddressType;

public class AddressWithTypeDTO {

	private Integer id;
	private String address;
	private String town;
	private AddressType type;

	public AddressWithTypeDTO(Integer id, String address, String town, AddressType type) {
		super();
		this.id = id;
		this.address = address;
		this.town = town;
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public AddressType getType() {
		return type;
	}

	public void setType(AddressType type) {
		this.type = type;
	}
}