package org.trescal.cwms.core.company.entity.contact.db;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.dto.ContactSearchResultWrapper;
import org.trescal.cwms.core.company.dto.SearchEmailContactDto;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;

public interface ContactDao extends BaseDao<Contact, Integer> {
	int countContactInstruments(int personid);

	Contact findDWRContact(int id);

	Contact findEagerContact(int id);
	
	List<Contact> getActiveContactsWithRoleForSubdiv(int businessSubdivId);
	
	List<Contact> getAllActiveSubdivContacts(int subdivid);

	List<Contact> getAllCompanyContacts(int coid);

	List<Contact> getAllCompanyContactsBySubCon(int coid, Boolean active);

	List<Contact> getAllOrderedContacts();

	List<Contact> getAllSubdivContacts(int subdivid);

	List<ContactSearchResultWrapper> getAllSubdivContactsToBookJobsHQL(int subdivid, String groupsList);

	List<ContactKeyValue> getBusinessContacts(boolean activeOnly);

	List<ContactKeyValue> getContactDtoList(Integer coid, Integer subdivid, Boolean active);
	
	List<ContactProjectionDTO> getContactProjectionDTOs(Collection<Integer> contactIds, Integer allocatedCompanyId);

	List<Contact> getContactsInUserGroup(int groupid);

	List<ContactSearchResultWrapper> searchByCompanyRoles(String firstName, String lastName, boolean active,
			List<CompanyRole> companyRoles);

	List<Contact> searchContact(String firstName, String lastName, boolean active);

    List<ContactSearchResultWrapper> searchContactsByRolesAndFullName(String fullName, Boolean active, List<CompanyRole> roles);

    List<Contact> searchContactByRoles(String firstName, String lastName, boolean active, List<CompanyRole> companyRoles);

	List<Contact> searchContactCaseInsensitive(int subdivid, String firstName, String lastName);

	List<ContactSearchResultWrapper> searchContactsHQL(String contactName, String subdivName, String companyName,
													   List<CompanyRole> companyRoles, String searchOrder, Integer BusinessCoId);

    List<SearchEmailContactDto> searchEmailContacts(String searchName, List<Integer> coIds);

    List<ContactSearchResultWrapper> webSearchContactHQL(Contact contact, String firstName, String lastName,
                                                         boolean active, Company allocatedCompany);

	List<Contact> getAllCompanyContacts(int coid, boolean active);

	Contact findCompanyContactByFullName(int coid, String fullname, boolean active);

	boolean getHrIdExists(String hrid);

	boolean getHrIdExistsAndIgnoreContact(String hrid, Contact contact);

	Contact getByHrId(String hrid);
	
	List<Contact> getByHrIds(List<String> hrids);
	
	Contact getContactByFirstnameAndLastname(Integer subdivid, String firstName, String lastName);

    Optional<Contact> searchContactByFullName(String fullName, boolean active);
    
    List<ContactSearchResultWrapper> getAllActiveContactsSearchResultWrapperProjection(int subdivid);
    
    ContactProjectionDTO getContactProjectionDTO(Integer contactId);
}