package org.trescal.cwms.core.company.entity.addressplantillassettings.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.addressplantillassettings.AddressPlantillasSettings;

@Service
public class AddressPlantillasSettingsServiceImpl extends BaseServiceImpl<AddressPlantillasSettings, Integer> implements AddressPlantillasSettingsService {

	@Autowired
	private AddressPlantillasSettingsDao plantillasSettingsDao;
	
	@Override
	protected BaseDao<AddressPlantillasSettings, Integer> getBaseDao() {
		return plantillasSettingsDao;
	}

	@Override
	public AddressPlantillasSettings getForAddress(int addrid, int allocatedcoid) {
		return plantillasSettingsDao.getForAddress(addrid, allocatedcoid);
	}

	@Override
	public AddressPlantillasSettings createAndInitilazeNewEntity() {
		
		AddressPlantillasSettings aps = new AddressPlantillasSettings();
		aps.setContract(false);
		aps.setFtpPassword("");
		aps.setFtpServer("");
		aps.setFtpUser("");
		aps.setMetraClient(false);
		aps.setNextDateOnCertificate(false);
		aps.setSendByFtp(false);
		aps.setTextToleranceFailure("");
		aps.setTextUncertaintyFailure("");
		
		return aps;
	}

}
