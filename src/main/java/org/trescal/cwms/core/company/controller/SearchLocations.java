package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.spring.model.LabelIdDTO;

import java.util.List;

@Controller
@JsonController
@RequestMapping("searchlocations.json")
public class SearchLocations {

    @Autowired
    LocationService locationService;
    @Autowired
    CompanyService companyService;
    @Autowired
    SubdivService subdivService;
    @Autowired
    AddressService addressService;

    @GetMapping
    public @ResponseBody List<LabelIdDTO> getLocations(@RequestParam("coid") Integer companyId,
                                  @RequestParam("subdivid") Integer subdivisionId,
                                  @RequestParam("addressid") Integer addressId,
                                  @RequestParam("term") String searchTerm){
        Company company = companyService.get(companyId);
        Subdiv subdivision = subdivService.get(subdivisionId);
        Address address = addressService.get(addressId);
        return locationService.getLocationsForAutocomplete(company, subdivision, address, searchTerm);
    }



}
