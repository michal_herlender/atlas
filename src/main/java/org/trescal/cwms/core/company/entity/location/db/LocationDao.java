package org.trescal.cwms.core.company.entity.location.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.dto.LocationSearchResultWrapper;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.LocationDTO;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface LocationDao extends BaseDao<Location, Integer> {
	
	Location findLocation(int addrid, String location);
	
	List<LocationSearchResultWrapper> getAllActiveAddressLocationsHQL(int addrid, boolean activeOnly);
	
	List<LocationDTO> getAddressLocations(int addrid, boolean activeOnly);
	
	List<Location> getAllBusinessLocations(boolean activeOnly);
	
	List<Location> getAllActiveSubdivLocations(Subdiv subdiv);
	
	boolean locationAlreadyExistsAtAddress(String location, int addrId, int excludeLocId);

    List<Location> getLocationsForAutocomplete(Company company, Subdiv subdivision, Address address, String searchTerm);
    
    List<KeyValueIntegerString> getLocationDtos(Collection<Integer> locationIds);
    
    List<Location> getAllAddressLocations(int addrid, boolean activeOnly);
    
    List<Location> getAllCompanyLocationsActive(Integer coid);
}