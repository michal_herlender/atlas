package org.trescal.cwms.core.company.entity.addressplantillassettings.db;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.addressplantillassettings.AddressPlantillasSettings;
import org.trescal.cwms.core.company.entity.company.Company;

public interface AddressPlantillasSettingsDao extends AllocatedDao<Company, AddressPlantillasSettings, Integer> {
	public AddressPlantillasSettings getForAddress(int addrid, int allocatedcoid);
}
