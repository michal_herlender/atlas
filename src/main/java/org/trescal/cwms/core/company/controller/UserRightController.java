package org.trescal.cwms.core.company.controller;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.userright.dto.UserRoleDTO;
import org.trescal.cwms.core.userright.entity.role.UserRightRole;
import org.trescal.cwms.core.userright.enums.Permission;

@Controller
@JsonController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class UserRightController {

	@Autowired
	private ContactService contactService;
	@Autowired
	private org.trescal.cwms.core.userright.entity.role.db.RoleService userRightRoleService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private org.trescal.cwms.core.userright.entity.userrole.db.UserRoleService newUserRoleService;

	@RequestMapping(value = "/adduserright.json", method = RequestMethod.POST)
	public String addNewUserRight(
		@RequestBody AddUserRightBody body,
		@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Model model) {
		if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(Permission.USER_RIGHT_MANAGEMENT)) {
			User user = contactService.get(body.getPersonId()).getUser();
			Subdiv subdiv = subdivService.get(body.getSubdivId());
			UserRightRole role = userRightRoleService.get(body.getRoleId());
			org.trescal.cwms.core.userright.entity.userrole.UserRole userRole = new org.trescal.cwms.core.userright.entity.userrole.UserRole(
				user, role, subdiv);
			newUserRoleService.save(userRole);
			model.addAttribute("userRole", new UserRoleDTO(userRole.getId(), role.getName(),
				subdiv.getComp().getConame(), subdiv.getSubname()));
		}
		return "trescal/core/company/userroleline";
	}

	@RequestMapping(value = "/deleteuserright.json", method = RequestMethod.POST)
	@ResponseBody
	public boolean deleteNewUserRight(@RequestParam(value = "userRoleId") Integer userRoleId) {
		if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(Permission.USER_RIGHT_MANAGEMENT))
			return newUserRoleService.remove(userRoleId);
		return false;
	}
}


@Data
class AddUserRightBody {
	Integer personId = 0;
	Integer subdivId = 0;
	Integer roleId = 0;
}