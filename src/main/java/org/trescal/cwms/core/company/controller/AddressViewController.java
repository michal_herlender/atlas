package org.trescal.cwms.core.company.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addressplantillassettings.db.AddressPlantillasSettingsService;
import org.trescal.cwms.core.company.entity.addressprintformat.AddressPrintFormatter;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.addresssettings.db.AddressSettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.vatrate.VatRateFormatter;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class AddressViewController {

	@Autowired
	private AddressPlantillasSettingsService addressPlantillasSettingsService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private AddressSettingsForAllocatedCompanyService addressSettingsForCompanyService;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private AddressPrintFormatter addressPrintFormatter;
	@Autowired
	private VatRateFormatter vatRateFormatter;

	public final static String VIEW_ADDRESS_DETAILS = "trescal/core/company/address";
	public final static String KEY_ADDRESS = "address";
	public final static String KEY_SYNC_TO_PLANTILLAS = "syncToPlantillas";
	public final static String KEY_USES_PLANTILLAS = "usesPlantillas";
	public final static String KEY_INSTRUMENT_COUNT = "instrumentCount";
	public final static String KEY_INSTRUMENT_COUNT_MAP = "instrumentCountMap";
	public final static String KEY_PLANTILLAS_SETTINGS = "plantillasSettings";
	public final static String KEY_RECALL_BY_ADDRESS = "recallByAddress";
	public final static String KEY_VATRATE_OVERRIDE = "vatRateOverride";
	public final static String KEY_VATRATE = "vatRate";
	public final static String KEY_FORMATTED_ADDRESS = "formattedAddress";

	public final static String MESSAGE_CODE_NONE_SPECIFIED = "company.nonespecified";

	@RequestMapping(value = "/viewaddress.htm")
	public String showView(Model model, @RequestParam(value = "addrid") Integer addressid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto, Locale locale) {
		Address address = addressService.get(addressid);
		Company allocatedCompany = companyService.get(companyDto.getKey());
		CompanySettingsForAllocatedCompany companySettings = companySettingsService
				.getByCompany(address.getSub().getComp(), allocatedCompany);

		model.addAttribute(KEY_ADDRESS, address);
		model.addAttribute(KEY_USES_PLANTILLAS, allocatedCompany.getBusinessSettings().getUsesPlantillas());
		model.addAttribute(KEY_SYNC_TO_PLANTILLAS,
				companySettings != null ? companySettings.getSyncToPlantillas() : false);
		model.addAttribute(KEY_PLANTILLAS_SETTINGS,
				this.addressPlantillasSettingsService.getForAddress(addressid, companyDto.getKey()));
		model.addAttribute(KEY_INSTRUMENT_COUNT, this.instrumentService.countByAddr(address.getAddrid()));
		model.addAttribute(KEY_INSTRUMENT_COUNT_MAP, getInstrumentCountMap(address));
		model.addAttribute(KEY_FORMATTED_ADDRESS, this.addressPrintFormatter.getAddressText(address, locale, true));
		intializeVatRate(model, address, allocatedCompany, companySettings, locale);
		return VIEW_ADDRESS_DETAILS;
	}

	private Map<Integer, Long> getInstrumentCountMap(Address address) {
		Map<Integer, Long> result = new HashMap<>();
		for (Location location : address.getLocations()) {
			result.put(location.getLocationid(), this.instrumentService.countByLocation(location.getLocationid()));
		}
		return result;
	}

	private void intializeVatRate(Model model, Address address, Company allocatedCompany,
			CompanySettingsForAllocatedCompany companySettings, Locale locale) {
		AddressSettingsForAllocatedCompany addressSettings = addressSettingsForCompanyService.getByCompany(address,
				allocatedCompany);
		if ((addressSettings != null) && (addressSettings.getVatRate() != null)) {
			String formattedRate = this.vatRateFormatter.formatVatRate(addressSettings.getVatRate(), locale);
			model.addAttribute(KEY_VATRATE, formattedRate);
			model.addAttribute(KEY_VATRATE_OVERRIDE, true);
		} else {
			// Determine VAT rate from company

			model.addAttribute(KEY_VATRATE_OVERRIDE, false);
			if ((companySettings != null) && (companySettings.getVatrate() != null)) {
				String formattedRate = this.vatRateFormatter.formatVatRate(companySettings.getVatrate(), locale);
				model.addAttribute(KEY_VATRATE, formattedRate);
			}
		}
	}
}