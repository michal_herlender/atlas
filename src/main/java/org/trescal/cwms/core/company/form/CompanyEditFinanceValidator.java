package org.trescal.cwms.core.company.form;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companystatus.CompanyStatus;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class CompanyEditFinanceValidator extends AbstractBeanValidator {

	// Error codes
	public final static String ERROR_DUPLICATE_IDENTIFIER = "error.companyaddform.duplicateidentifier";
	public final static String ERROR_LEGAL_REGISTRATION = "error.legalregistrationcount";
	public final static String ERROR_ACTIVE_MUST_BE_VERIFIED = "error.company.finance.activemustbeverified";
	public final static String ERROR_TRUSTED_MUST_BE_VERIFIED = "error.company.finance.trustedmustbeverified";
	// Field definitions
	public final static String FIELD_LEGAL_IDENTIFIER = "legalIdentifier";
	public final static String FIELD_ACTIVE = "active";
	public final static String FIELD_ON_STOP = "onStop";
	// Default error messages
	public final static String MESSAGE_LEGAL_REGISTRATION = "Only one legal registration address is permitted.  This company has {} legal registration addresses.";
	
	@Autowired
	private CompanyService companyService;
	@Autowired
	private AddressService addressService;
	
	@Override
	public boolean supports(Class<?> arg0) {
		return CompanyEditFinanceForm.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {
		CompanyEditFinanceForm form = (CompanyEditFinanceForm) target;
		Company company = this.companyService.get(form.getCoid());
		/* 
		 * JPA validation required for all company status values when CompanyStatus.VERIFIED;
		 * this allows migrated and new companies to exist before all fields are known, and for specifics
		 * to be entered by the accounting department
		 */
		if (form.getStatus().equals(CompanyStatus.VERIFIED)) {
			super.validate(target, errors);
			checkLegalRegistration(company, errors);
		}
		else {
			// Business rule that only VERIFIED companies can be trusted and active
			if (form.getActive()) errors.rejectValue(FIELD_ACTIVE, ERROR_ACTIVE_MUST_BE_VERIFIED);
			if (!form.getOnStop()) errors.rejectValue(FIELD_ON_STOP, ERROR_TRUSTED_MUST_BE_VERIFIED);
		}
		
		// Ensure legal identifier not duplicated, regardless of status
		if (this.companyService.existsByLegalIdentifier(form.getLegalIdentifier().trim(), company.getCompanyRole(), company)) {
			errors.rejectValue(FIELD_LEGAL_IDENTIFIER, ERROR_DUPLICATE_IDENTIFIER);
		}
		// TODO Ensure company code not duplicated within country, split into Business/
	}
	
	/*
	 * Ensure that there is one and only one legal registration address
	 */
	protected void checkLegalRegistration(Company company, Errors errors) {
		List<Address> addresses = addressService.getAllCompanyAddresses(company.getCoid(), AddressType.LEGAL_REGISTRATION, false);
		if (addresses.size() != 1) {
			errors.reject(ERROR_LEGAL_REGISTRATION, new Object[] {addresses.size()}, MESSAGE_LEGAL_REGISTRATION);
		}
	}
}
