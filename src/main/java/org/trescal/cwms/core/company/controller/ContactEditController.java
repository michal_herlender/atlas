package org.trescal.cwms.core.company.controller;

import org.hibernate.StaleObjectStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateOptimisticLockingFailureException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.contactinstructionlink.db.ContactInstructionLinkService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.company.entity.departmentmember.db.DepartmentMemberService;
import org.trescal.cwms.core.company.entity.departmentrole.db.DepartmentRoleService;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.company.entity.mailgroupmember.MailGroupMember;
import org.trescal.cwms.core.company.entity.mailgroupmember.db.MailGroupMemberService;
import org.trescal.cwms.core.company.entity.mailgrouptype.MailGroupType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.entity.title.db.TitleService;
import org.trescal.cwms.core.company.form.ContactEditForm;
import org.trescal.cwms.core.company.form.ContactEditFormValidator;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.login.entity.portal.usergroup.db.UserGroupService;
import org.trescal.cwms.core.login.entity.role.Roles;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.login.entity.userpreferences.db.UserPreferencesService;
import org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.CapabilityTrainingRecord;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.alligatorsettings.db.AlligatorSettingsService;
import org.trescal.cwms.core.system.entity.alligatorsettingscontact.db.AlligatorSettingsContactService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.printer.db.PrinterService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultTypeDTO;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.service.SystemDefaultTypeService;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.userright.entity.role.db.RoleService;
import org.trescal.cwms.core.userright.entity.userrole.db.UserRoleService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Displays a form allowing a {@link Contact} to be edited.
 */
@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY,
	Constants.SESSION_ATTRIBUTE_USERNAME})
public class ContactEditController {
	private static final Logger logger = LoggerFactory.getLogger(ContactEditController.class);

	private static final String formView = "trescal/core/company/contactedit";

	@Autowired
	private AddressService addressServ;
	@Autowired
	private AlligatorSettingsContactService alligatorSettingsContactService;
	@Autowired
	private AlligatorSettingsService alligatorSettingsService;
	@Autowired
	private AuthenticationService authenticationService;
	@Autowired
	private BPOService bpoServ;
	@Autowired
	private CertificateService certServ;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private ContactService contactServ;
	@Autowired
	private DepartmentMemberService departmentMemberServ;
	@Autowired
	private DepartmentRoleService departmentRoleServ;
	@Autowired
	private DepartmentService departmentServ;
	@Autowired
	private ContactInstructionLinkService contactInstructionLinkService;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private JobService jobServ;
	@Autowired
	private LocationService locServ;
	@Autowired
	private MailGroupMemberService mailGroupMemberServ;
	@Autowired
	private UserPreferencesService prefServ;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private PrinterService printerServ;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private RoleService roleService;
	@Autowired
	private SubdivService subdivServ;
	@Autowired
	private SystemDefaultTypeService systemDefaultTypeService;
	@Autowired
	private TransportOptionService transOptServ;
	@Autowired
	private UserGroupService userGroupServ;
	@Autowired
	private UserRoleService userRoleService;
	@Autowired
	private UserService userService;
	@Autowired
	private ContactEditFormValidator validator;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private TitleService titleService;

	@InitBinder("contacteditform")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("contacteditform")
	protected ContactEditForm formBackingObject(@RequestParam(value = "personid") Integer personid,
												@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdiv,
												@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto
	) {
		Contact contact = this.contactServ.get(personid);
		if (contact == null)
			throw new RuntimeException("Contact not found for id " + personid);
		ContactEditForm cef = new ContactEditForm();
		cef.setContact(contact);
		cef.setEmail(contact.getEmail());
		if (contact.getUsergroup() != null)
			cef.setGroupid(contact.getUsergroup().getGroupid());
		if (contact.getTitle() != null)
			cef.setTitle(contact.getTitle());
		Set<MailGroupType> mailgroups = MailGroupType.getActiveMailGroupTypes();
		HashMap<MailGroupType, Boolean> mailMap = new HashMap<>();
		for (MailGroupType type : mailgroups) {
			mailMap.put(type, false);
		}
		List<MailGroupMember> members = this.mailGroupMemberServ.getContactMailGroups(cef.getContact().getPersonid());
		for (MailGroupMember member : members) {
			mailMap.put(member.getMailGroupType(), true);
		}
		cef.setMailgroups(mailMap);
		HashMap<Integer, String> deptMap = new HashMap<>();
		HashMap<Integer, String> deptRoles = new HashMap<>();
		List<Department> departments = this.departmentServ
				.getAllSubdivDepartments(cef.getContact().getSub().getSubdivid());
        for (Department d : departments) {
            deptMap.put(d.getDeptid(), "null");
            deptRoles.put(d.getDeptid(), "");
        }
        List<DepartmentMember> deptMembers = this.departmentMemberServ
            .getAllContactDepartmentMembers(cef.getContact().getPersonid()).stream()
            .filter(dm -> dm.getDepartment().getSubdiv().equals(contact.getSub())).collect(Collectors.toList());
        for (DepartmentMember dm : deptMembers)
            deptMap.put(dm.getDepartment().getDeptid(), "on");
        // load up all procedure training record certificates
        for (CapabilityTrainingRecord ptr : cef.getContact().getCapabilityTrainingRecords()) {
            // load up certificate
            this.certServ.findCertificate(ptr.getCert().getCertid());
        }
        cef.setAllocatedSubdivId(subdiv.getKey());
        cef.setDepartmentroles(deptRoles);
        cef.setDepartments(deptMap);
        if (contact.getDefaultPrinter() != null)
            cef.setPrinterid(contact.getDefaultPrinter().getId());
        cef.setUserRoles(userRoleService.findUserRoles(contact.getUser()));

        return cef;
    }

	@ModelAttribute("systemdefaulttypes")
	public List<SystemDefaultTypeDTO> initializeSystemDefaults(Locale locale,
			@RequestParam(value = "personid") Integer personid,
			@ModelAttribute(value = Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto) {
		Contact contact = contactServ.get(personid);
		if (contact == null)
			throw new RuntimeException("Contact not found for id " + personid);
		List<SystemDefaultTypeDTO> result = this.systemDefaultTypeService.getSystemDefaultTypeDTOs(contact,
				allocatedCompanyDto.getKey(), locale);
		logger.info("systemdefaulttypes.size(): " + result.size());
		return result;
	}

    @RequestMapping(value = "/viewperson.htm", method = RequestMethod.POST)
    @PreAuthorize("@canEditUser.check(#personid,authentication.name)")
		protected ModelAndView onSubmit(@ModelAttribute("contacteditform") @Validated ContactEditForm cef,
			BindingResult result, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdiv,
			@RequestParam(value = "personid") Integer personid, Locale locale
	) throws Exception {
		if (result.hasErrors()) {
			// prevent updating contact (TODO note - this throws validation
			// exceptions e.g. blank first name due to flush on tx boundary)
			contactServ.evict(cef.getContact());
			return onRequest(cef, subdiv, username, personid, locale);
		}
			Contact contact = cef.getContact();
			contact.setUsergroup(this.userGroupServ.findGroup(cef.getGroupid()));
			contact.setTitle(cef.getTitle());
			if (cef.getAddressid() != null)
				contact.setDefAddress(this.addressServ.get(cef.getAddressid()));
			if (cef.getLocationid() != null)
				contact.setDefLocation(this.locServ.get(cef.getLocationid()));
			contact.setEmail(cef.getEmail());
			if (cef.getPrinterid() != null)
				contact.setDefaultPrinter(this.printerServ.get(cef.getPrinterid()));
			if (StringUtils.hasLength(cef.getPassword()) && StringUtils.hasLength(cef.getPassword2())) {
				// Changing password (match checked via validator)
				contact.getUser().setPassword(passwordEncoder.encode(cef.getPassword()));
				if (cef.isSendLoginEmail()) {
					this.contactServ.sendPasswordReminder(personid, cef.getPassword());
				}
				// Because the original form is being returned, clear out one
				// time selections otherwise they appear in UI
				cef.setPassword("");
				cef.setPassword2("");
				cef.setSendLoginEmail(false);
			}
			if (cef.getLocale() != null) {
				contact.setLocale(cef.getLocale());
			}
			// reset hr id if empty (3 = country code + '-')
			if ((cef.getContact().getHrid() != null) && cef.getContact().getHrid().length() <= 3) {
				cef.getContact().setHrid(null);
			}
			// if contact has changed we may also need to update the
			// company account - set status to pending so that any changes are
			// marked to be flushed to 3rd party accounts packages
			if (contact.getAccountContact() != null)
				contact.getAccountContact().setAccountsStatus(AccountStatus.P);
			try {
				this.contactServ.merge(contact);
				for (Map.Entry<MailGroupType, Boolean> entry : cef.getMailgroups().entrySet()) {
					MailGroupMember mgm = this.mailGroupMemberServ.findMailGroupMember(contact.getPersonid(),
						entry.getKey());
					if (entry.getValue()) {
						if (mgm == null) {
							mgm = new MailGroupMember();
							mgm.setContact(contact);
							mgm.setMailGroupType(entry.getKey());
							this.mailGroupMemberServ.merge(mgm);
						}
					} else {
						if (mgm != null) {
							this.mailGroupMemberServ.delete(mgm);
						}
					}
				}
				Map<Integer, String> departments = cef.getDepartments();
				Map<Integer, String> departmentRoles = cef.getDepartmentroles();
				Contact editor = this.userService.get(username).getCon();
				Collection<Integer> keys = departments.keySet();
				for (Integer key : keys) {
					Department department = this.departmentServ.get(key);

					// only allow user to update department if he has privileges
					// to do
					// so
					if (this.departmentServ.userCanEditDepartment(editor, department).isSuccess()) {
						if (departments.get(key) != null) {
							DepartmentMember dm = this.departmentMemberServ.findDepartmentMember(contact.getPersonid(),
									department.getDeptid());
							if (dm == null) {
								dm = new DepartmentMember();
								dm.setContact(contact);
								dm.setDepartment(department);
							}

							dm.setDepartmentRole(this.departmentRoleServ
									.findDepartmentRole(Integer.parseInt(departmentRoles.get(department.getDeptid()))));
							this.departmentMemberServ.saveOrUpdateDepartmentMember(dm);
						} else {
							DepartmentMember dm = this.departmentMemberServ.findDepartmentMember(contact.getPersonid(),
									department.getDeptid());
							if (dm != null) {
								this.departmentMemberServ.deleteDepartmentMember(dm);
							}
						}
					}
				}
				cef.setMessage(" The contact details for " + contact.getName() + " have been successfully updated");
			} catch (StaleObjectStateException | HibernateOptimisticLockingFailureException sosex) {
				logger.info(sosex.getMessage());
				cef.setMessage(sosex.getLocalizedMessage());
				cef.setContact(this.contactServ.get(contact.getPersonid()));
			}
		Subdiv allocatedSubdiv = this.subdivServ.get(subdiv.getKey());
		return new ModelAndView(formView, referenceData(locale, cef.getContact(), allocatedSubdiv, username));
	}

	@RequestMapping(value = "/viewperson.htm", method = RequestMethod.GET)
	protected ModelAndView onRequest(@ModelAttribute("contacteditform") ContactEditForm cef,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdiv,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@RequestParam(value = "personid") Integer personid, Locale locale) {
		Subdiv allocatedSubdiv = this.subdivServ.get(subdiv.getKey());
		Contact contact = contactServ.get(personid);
		if (contact == null)
			throw new RuntimeException("Contact not found for id " + personid);
		cef.setContact(contact);
		return new ModelAndView(formView, referenceData(locale, contact, allocatedSubdiv, username));
	}

	private Map<String, Object> referenceData(Locale locale, Contact contact, Subdiv allocatedSubdiv, String username) {
		Contact editor = this.userService.get(username).getCon();
		Map<String, Object> model = new HashMap<>();

		Subdiv sub = contact.getSub();
		TransportOption defaultTransportOptionIn = transOptServ.getTransportOptionIn(
				this.addressServ.get(sub.getDefaultAddress().getAddrid()),
				this.subdivServ.get(allocatedSubdiv.getId()));
		TransportOption defaultTransportOptionOut = transOptServ.getTransportOptionOut(
				this.addressServ.get(sub.getDefaultAddress().getAddrid()),
				this.subdivServ.get(allocatedSubdiv.getId()));
		model.put("bPOsList", bpoServ.getAllByContact(contact, null, allocatedSubdiv.getComp()));
		model.put("usergroups", this.userGroupServ.getSortedUserGroups(contact.getSub().getComp()));
		model.put("companysettings", companySettingsService.getByCompany(sub.getComp(), allocatedSubdiv.getComp()));
		Set<Address> activeAddresses = new HashSet<>();
		for (Address address : sub.getAddresses())
			if (address.getActive())
				activeAddresses.add(address);
		model.put("activeaddresses", activeAddresses);
		model.put("addresses", sub.getAddresses());
		model.put("mailGroupTypes", MailGroupType.getActiveMailGroupTypes());
		model.put("departments",
				this.departmentServ.getAllSubdivDepartmentsEditable(contact.getSub().getSubdivid(), editor));
		model.put("deparmentroles", this.departmentRoleServ.getAllDepartmentRoles());
		model.put("printers", this.printerServ.getDtosForContactByUserRole(contact));
		model.put("dailytransport", this.transOptServ.getAllDaySpecificTransportOptions(allocatedSubdiv.getSubdivid()));
		model.put("defaultTransportOptionInId",
				defaultTransportOptionIn != null ? defaultTransportOptionIn.getId() : null);
		model.put("defaultTransportOptionOutId",
				defaultTransportOptionOut != null ? defaultTransportOptionOut.getId() : null);
		model.put("userpreference", this.prefServ.findUserPreferencesByContact(contact.getPersonid()));
		model.put("businessSubdivs", this.subdivServ.getAllActiveSubdivsByRole(CompanyRole.BUSINESS));
		model.put("defaultrole", Scope.CONTACT);
		model.put("instructionTypes", InstructionType.values());
		model.put("activejobcount", this.jobServ.countActiveByContactAndAllocatedSubdiv(contact, allocatedSubdiv));
		model.put("totaljobcountbysubdiv", this.jobServ.countByContactAndAllocatedSubdiv(contact, allocatedSubdiv));
		model.put("totaljobcount", this.jobServ.countByContact(contact));
		model.put("quotecount", this.quoteServ.countByContactAndAllocatedSubdiv(contact, allocatedSubdiv));
		model.put("recentquotecount",
				this.quoteServ.countActualByContactAndAllocatedSubdiv(2, contact, allocatedSubdiv));
		model.put("instructioncount", contactInstructionLinkService.count(contact, allocatedSubdiv.getComp()));
		model.put("instcount", instrumentService.countByContact(contact.getPersonid()));
		List<Company> companies = companyService.getCompaniesByRole(CompanyRole.BUSINESS);
		Map<KeyValue<Integer, String>, List<KeyValue<Integer, String>>> businessSubdivs = new HashMap<>();
		for (Company c : companies)
			for (Subdiv s : c.getSubdivisions())
				if (businessSubdivs.containsKey(c.dto()))
					businessSubdivs.get(c.dto()).add(s.dto());
				else {
					List<KeyValue<Integer, String>> subdivs = new LinkedList<>();
					subdivs.add(s.dto());
					businessSubdivs.put(c.dto(), subdivs);
				}
		model.put("businessSubs", businessSubdivs);
		model.put("roles", Roles.values());
		model.put("userRightRoles", roleService.getAll());
		model.put("userRoles", userRoleService.findUserRoles(contact.getUser()));
		// All users can edit client web logins; administrator needed for
		// editing business company web logins; can also change "own password"
		boolean editingOwnAccount = contact.getId().intValue() == editor.getId().intValue();
		model.put("editingOwnAccount", editingOwnAccount);
		boolean isOwnAccount = contact.getId().equals(editor.getId());
		model.put("isOwnAccount", isOwnAccount);
		boolean allowPasswordEdit = (contact.getSub().getComp().getCompanyRole() != CompanyRole.BUSINESS)
				|| authenticationService.hasRight(editor, Permission.PASSWORD_EDIT, allocatedSubdiv.getId())
				|| editingOwnAccount;
		model.put("allowPasswordEdit", allowPasswordEdit);
		boolean allowChangeUserRights = authenticationService.hasRight(editor, Permission.USER_RIGHT_MANAGEMENT,
				allocatedSubdiv.getId());
        model.put("allowChangeUserRights", allowChangeUserRights);
        model.put("allowEditUser", authenticationService.allowPrivileges(editor.getPersonid(), contact.getPersonid(), allocatedSubdiv.getId()));
        model.put("locales", supportedLocaleService.getSupportedLocales());
		if (contact.getSub().getComp().getCompanyRole() == CompanyRole.BUSINESS) {
			model.put("alligatorSettingsContact",
					alligatorSettingsContactService.findAlligatorSettingsContact(contact));
			model.put("alligatorSettingsProduction", alligatorSettingsService.getSettings(contact, true));
			model.put("alligatorSettingsTest", alligatorSettingsService.getSettings(contact, false));
		}
		// hrid country prefix
		String hridcountryprefix = "";
		if (contact.getSub().getComp().getCountry() != null) {
			hridcountryprefix = contact.getSub().getComp().getCountry().getCountryCode()
				+ ContactAddController.HRID_DELIMITER;
		}
		List<KeyValue<String, String>> dtoTitles = titleService.getDTOTitles(locale.getCountry());
		model.put("titles",dtoTitles);

		model.put("hridcountryprefix", hridcountryprefix);
		// initialize hrid prefix if field is empty
		if (!StringUtils.hasText(contact.getHrid()))
			contact.setHrid(hridcountryprefix);
		return model;
	}
}