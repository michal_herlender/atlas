package org.trescal.cwms.core.company.form;


public class ContactSearchForm
{
	private Integer personid;

	public Integer getPersonid()
	{
		return personid;
	}

	public void setPersonid(Integer personid)
	{
		this.personid = personid;
	}
}
