package org.trescal.cwms.core.company.entity.mailgroupmember.db;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.mailgroupmember.MailGroupMember;
import org.trescal.cwms.core.company.entity.mailgrouptype.MailGroupType;

@Service("mailGroupMemberService")
public class MailGroupMemberServiceImpl extends BaseServiceImpl<MailGroupMember, Integer> implements MailGroupMemberService
{
	@Autowired
	private MailGroupMemberDao mailgroupmemberDao;

	public static final Logger logger = LoggerFactory.getLogger(MailGroupMemberServiceImpl.class);
	
	@Override
	public MailGroupMember findMailGroupMember(Integer personid, MailGroupType mailGroupType)
	{
		return this.mailgroupmemberDao.findMailGroupMember(personid, mailGroupType);
	}

	@Override
	public List<MailGroupMember> getContactMailGroups(Integer personid)
	{
		return this.mailgroupmemberDao.getContactMailGroups(personid);
	}

	@Override
	public List<MailGroupMember> getMembersOfGroupForSubdiv(MailGroupType mailGroupType, int subdivid, boolean activeContactsOnly)
	{
		return this.mailgroupmemberDao.getMembersOfGroupForSubdiv(mailGroupType, subdivid, activeContactsOnly);
	}

	@Override
	protected BaseDao<MailGroupMember, Integer> getBaseDao() {
		return mailgroupmemberDao;
	}
	
	@Override
	public Set<String> getMembersEmailsOfGroupForSubdiv(MailGroupType mailGroupType, int subdivid) {
		return this.getMembersOfGroupForSubdiv(mailGroupType, subdivid, true).stream()
		.map(member -> member.getContact().getEmail()).collect(Collectors.toSet());
	}

}