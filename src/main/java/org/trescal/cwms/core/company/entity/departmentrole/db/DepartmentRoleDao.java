package org.trescal.cwms.core.company.entity.departmentrole.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.departmentrole.DepartmentRole;

public interface DepartmentRoleDao extends BaseDao<DepartmentRole, Integer> {}