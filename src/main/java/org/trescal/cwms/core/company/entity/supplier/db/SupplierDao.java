package org.trescal.cwms.core.company.entity.supplier.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.supplier.Supplier;
import org.trescal.cwms.core.company.entity.supplier.SupplierApprovalType;


public interface SupplierDao extends BaseDao<Supplier, Integer>
{
	List<Supplier> getActiveSuppliers();
	Supplier getForApprovalType(SupplierApprovalType approvalType);
}
