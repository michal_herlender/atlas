package org.trescal.cwms.core.company.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;

@Component @IntranetController
public class ContactAddInterceptor implements HandlerInterceptor
{
	@Autowired
	private ContactService contactServ;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		ResultWrapper results = this.contactServ.contactCanCreateContact(request);
		if (!results.isSuccess()) {
			HttpSession session = request.getSession(true);
			session.setAttribute(Constants.SYSTEM_AUTHENTICATION_ERROR_MESSAGE, results.getMessage());
			response.sendRedirect("accessdenied.htm");
		}
		return results.isSuccess();
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {}
}
