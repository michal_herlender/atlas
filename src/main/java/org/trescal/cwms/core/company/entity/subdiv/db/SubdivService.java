package org.trescal.cwms.core.company.entity.subdiv.db;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.dto.SubdivSearchResultWrapper;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.form.SubdivEditForm;
import org.trescal.cwms.core.company.projection.SubdivProjectionDTO;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.spring.model.KeyValue;

public interface SubdivService extends BaseService<Subdiv, Integer> {
	/**
	 * Returns a list of all {@link Subdiv} entities that belong to the
	 * {@link Company} identified by the given coid that are active.
	 * 
	 * @param company
	 *            the {@link Company}, not null.
	 * @return a list of {@link Subdiv} entities.
	 */
	List<Subdiv> getAllActiveCompanySubdivs(Company company);

	/**
	 * Returns a {@link List} of {@link SubdivSearchResultWrapper} that belong to
	 * the {@link Company} identified by the given coid that are active.
	 * 
	 * @param coid
	 *            the id of the {@link Company}, not null.
	 * @return {@link List} of {@link SubdivSearchResultWrapper}.
	 */
	List<SubdivSearchResultWrapper> getAllActiveCompanySubdivsHQL(int coid);

	List<Subdiv> getAllActiveSubdivsByRole(CompanyRole companyRole);

	Set<Subdiv> getBusinessCompanySubdivsForUser(User user);

	/**
	 * Checks for existence of the specified SIRET number (French subdivision specific legal identifier) 
	 * 
	 * @param siretNumber search term
	 * @param companyRole companyRole to search by (roles editableTo are also included)
	 * @param excludeSubdivId optional Integer id of a subdiv to exclude from the search
	 * @return
	 */
	Long countBySiretNumber(String siretNumber, CompanyRole companyRole, Integer excludeSubdivId);

	/**
	 * Returns count of subdivs for a company, intended for duplicate detection in creation
	 * @param coid, optional (null = search all companies)
	 * @param subdivName, optional (null = search all subdivs)
	 * @param excludeSubdivId optional Integer subdiv id to exclude (not used if null)
	 * @return count of subdivs (zero, if no duplicates)
	 */
	Long countCompanySubdivsByName(Integer coid, String subdivName, Integer excludeSubdivId);
	
	void editSubdiv(SubdivEditForm sf, Integer allocatedSubdivId);

	List<SubdivKeyValue> searchBusinessSubdivsByName(String partialSubdivName);

	/**
	 * Returns true if the {@link Subdiv} belongs to the business company, false if
	 * not.
	 * 
	 * @param subdivid
	 *            the subdivid to check.
	 * @return
	 */
	boolean subdivBelongsToBusinessCompany(int subdivid);

	/**
	 * Updates the {@link Subdiv} identified by the given subdivid to have the
	 * {@link Address} identified by the given addressid as it's default
	 * {@link Address}.
	 * 
	 * @param subdivid
	 *            the subdivision id, not null.
	 * @param addressid
	 *            the address id, not null.
	 */
	void updateSubdivAddressDefault(int subdivid, int addressid);

	/**
	 * Updates the {@link Subdiv} identified by the given subdivid to have the
	 * {@link Contact} identified by the given personid as it's default
	 * {@link Contact}.
	 * 
	 * @param subdivid
	 *            the subdivision id, not null.
	 * @param personid
	 *            the contact id, not null.
	 */
	void updateSubdivContactDefault(int subdivid, int personid);

	public List<SubdivKeyValue> getAllSubdivFromCompany(int coid, Boolean isActive);
	
	List<KeyValue<Integer,String>> getAllActiveCompanySubdivsKeyValue(int companyid, boolean prependNoneDto);
	
	List<SubdivProjectionDTO> getSubdivProjectionDTOs(Collection<Integer> sourceSubdivIds, Integer allocatedCompanyId);
	
	/**
	 * Return the list of active subdivisions that the user
	 * has the right to access to them
	 * @param user {@link User}
	 * @param coid {@link Company}
	 */
	List<SubdivKeyValue> getAllActiveCompanySubdivsForUser(User user, Integer coid);
	List<SubdivProjectionDTO> getSubdivProjectionDTOsByCompany(Integer allocatedCompanyId);
}