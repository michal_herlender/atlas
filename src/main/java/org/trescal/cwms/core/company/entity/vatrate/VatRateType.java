package org.trescal.cwms.core.company.entity.vatrate;

import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public enum VatRateType {
	COUNTRY_DEFAULT("vatratetype.countrydefault"),	// Indicates the default internal rate for a country
	COUNTRY_SPECIAL("vatratetype.countryspecial"),	// Indicates an optional special rate for a country (subregion) 
	EUROZONE_INTERNAL("vatratetype.euinternal"),	// Indicates a generic rate used from an EU country to another EU country
	EUROZONE_EXTERNAL("vatratetype.euexternal"),	// Indicates a generic rate used from an EU country to outside the EU
	NON_EUROZONE("vatratetype.noneurozone");		// Indicates a rate used from non-EU countries
	
	private String messageCode;
	private MessageSource messageSource; 
	
	private VatRateType(String messageCode) {
		this.messageCode = messageCode;
	}
	
	@Component
	public static class VatRateTypeMessageInjector {
		@Autowired
		private MessageSource messageSource;
		
		@PostConstruct
        public void postConstruct() {
			for (VatRateType type : VatRateType.values()) {
				type.setMessageSource(this.messageSource);
			}
		}
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getMessage() {
		return getMessageForLocale(LocaleContextHolder.getLocale());
	}

	public String getMessageForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, this.toString(), locale);
	}

	public String getMessageCode() {
		return messageCode;
	}
}