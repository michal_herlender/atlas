package org.trescal.cwms.core.company.form;

public class FlexibleFieldLibraryValueDTO {
	
	Integer libraryValueId;
	String libraryValueName;
	
	public FlexibleFieldLibraryValueDTO(){
		
	}
	
	public FlexibleFieldLibraryValueDTO(Integer libraryValueId, String libraryValueName) {
		super();
		this.libraryValueId = libraryValueId;
		this.libraryValueName = libraryValueName;
	}
	
	public Integer getLibraryValueId() {
		return libraryValueId;
	}
	public void setLibraryValueId(Integer libraryValueId) {
		this.libraryValueId = libraryValueId;
	}
	public String getLibraryValueName() {
		return libraryValueName;
	}
	public void setLibraryValueName(String libraryValueName) {
		this.libraryValueName = libraryValueName;
	}
	
}
