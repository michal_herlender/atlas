package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.company.dto.LocationSearchResultWrapper;
import org.trescal.cwms.core.company.entity.location.db.LocationService;

import java.util.List;

@RestController
@RequestMapping("location")
public class LocationAjaxController {

    @Autowired
    private LocationService locationService;

    @GetMapping("allByAddressAndActiveStatus.json")
    List<LocationSearchResultWrapper> getAllByAddressAndActiveStatus(@RequestParam Integer addressId, @RequestParam(required = false, defaultValue = "true") Boolean activeOnly) {
        return locationService.getAllActiveAddressLocationsHQL(addressId, activeOnly);
    }
}
