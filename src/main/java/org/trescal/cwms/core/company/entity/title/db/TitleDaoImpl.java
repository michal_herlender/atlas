package org.trescal.cwms.core.company.entity.title.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.title.Title;

@Repository("TitleDao")
public class TitleDaoImpl extends BaseDaoImpl<Title, Integer> implements TitleDao {
	
	@Override
	protected Class<Title> getEntity() {
		return Title.class;
	}
}