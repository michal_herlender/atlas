package org.trescal.cwms.core.company.projection;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SubdivProjectionDTO {
	// Below result fields always populated by query
	private Integer subdivid;
	private String subname;
	private Boolean active;
	private CompanyProjectionDTO company;

	public SubdivProjectionDTO(Integer subdivid, String subname, Boolean subdivActive, 
			Integer coid, String coname, Boolean companyActive, Boolean onstop) {
		super();
		this.subdivid = subdivid;
		this.subname = subname;
		this.active = subdivActive;
		this.company = new CompanyProjectionDTO(coid, coname, companyActive, onstop);
	}

	public SubdivProjectionDTO(Integer subdivid, String subname) {
		super();
		this.subdivid = subdivid;
		this.subname = subname;
	}
	
	
}
