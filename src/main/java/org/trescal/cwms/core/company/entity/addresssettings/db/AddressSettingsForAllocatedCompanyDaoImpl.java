package org.trescal.cwms.core.company.entity.addresssettings.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.company.Company;

@Repository
public class AddressSettingsForAllocatedCompanyDaoImpl extends BaseDaoImpl<AddressSettingsForAllocatedCompany, Integer>
		implements AddressSettingsForAllocatedCompanyDao {

	@Override
	protected Class<AddressSettingsForAllocatedCompany> getEntity() {
		return AddressSettingsForAllocatedCompany.class;
	}

	@Override
	public AddressSettingsForAllocatedCompany getByCompany(Address address, Company allocatedCompany) {
		return getFirstResult(cb -> {
			CriteriaQuery<AddressSettingsForAllocatedCompany> cq = cb
					.createQuery(AddressSettingsForAllocatedCompany.class);
			Root<AddressSettingsForAllocatedCompany> settings = cq.from(AddressSettingsForAllocatedCompany.class);
			cq.where(cb.and(cb.equal(settings.get(AddressSettingsForAllocatedCompany_.address), address),
					cb.equal(settings.get(AddressSettingsForAllocatedCompany_.organisation), allocatedCompany)));
			return cq;
		}).orElse(null);
	}
}