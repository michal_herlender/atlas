package org.trescal.cwms.core.company.entity.salesdiscount.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.salesdiscount.SalesDiscount;

@Repository("SalesDiscountDao")
public class SalesDiscountDaoImpl extends BaseDaoImpl<SalesDiscount, String> implements SalesDiscountDao {
	
	@Override
	protected Class<SalesDiscount> getEntity() {
		return SalesDiscount.class;
	}
}