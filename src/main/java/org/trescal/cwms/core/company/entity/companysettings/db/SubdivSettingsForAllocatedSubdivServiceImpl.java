package org.trescal.cwms.core.company.entity.companysettings.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.dto.SubdivSettingsForAllocatedSubdivDto;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

import java.util.List;

@Service
public class SubdivSettingsForAllocatedSubdivServiceImpl extends BaseServiceImpl<SubdivSettingsForAllocatedSubdiv, Integer> implements SubdivSettingsForAllocatedSubdivService {
	
	@Autowired
	private SubdivSettingForAllocatedSubdivDao settingsDao;
	
	@Override
	protected BaseDao<SubdivSettingsForAllocatedSubdiv, Integer> getBaseDao() {
		return settingsDao;
	}
	
	@Override
	public SubdivSettingsForAllocatedSubdiv get(Subdiv client, Subdiv business) {
		return settingsDao.get(client, business);
	}
	
	@Override
	public SubdivSettingsForAllocatedSubdiv get(Integer subdivId, Integer allocatedSubdivId) {
		return settingsDao.get(subdivId, allocatedSubdivId);
	}

	@Override
	public List<SubdivSettingsForAllocatedSubdivDto> getAllBySubdivId(Integer subdivId){
		return settingsDao.findBySubdivId(subdivId);
	}
}