package org.trescal.cwms.core.company.form;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class CompanyAddFormValidator extends AbstractBeanValidator implements Validator {
	
	@Autowired
	private CompanyService compServ;
	@Autowired
	private SubdivService subdivService;

	public final static String FIELD_CONAME = "coname";
	public final static String FIELD_LEGAL_IDENTIFIER = "legalIdentifier";
	public final static String FIELD_BUSINESS_AREA = "businessAreaId";
	public final static String FIELD_COMPANY_CODE = "companyCode";
	public final static String FIELD_SIRET_NUMBER = "siretNumber";

	public final static String ERROR_DUPLICATE_NAME = "error.companyaddform.duplicatename";
	public final static String ERROR_DUPLICATE_IDENTIFIER = "error.companyaddform.duplicateidentifier";
	public final static String ERROR_BUSINESS_AREA = "error.companyaddform.businessareaid.notnull";
	public final static String ERROR_COMPANY_CODE = "error.companyaddform.companycode";
	public final static String ERROR_DUPLICATE_SIRET_NUMBER = "error.companyaddform.duplicatedsiretnumber";

	public boolean supports(Class<?> clazz) {
		return clazz.equals(CompanyAddForm.class);
	}

	public void validate(Object obj, Errors errors) {
		super.validate(obj, errors);
		CompanyAddForm acf = (CompanyAddForm) obj;
		
		// Remaining checks require company role (not null) to be set
		if (acf.getCompanyRole() != null) {
			if (this.compServ.existsByName(acf.getConame().trim(), acf.getCompanyRole(), null)) {
				errors.rejectValue(FIELD_CONAME, ERROR_DUPLICATE_NAME);
			}
			
			if (this.compServ.existsByLegalIdentifier(acf.getLegalIdentifier().trim(), acf.getCompanyRole(), null)) {
				errors.rejectValue(FIELD_LEGAL_IDENTIFIER, ERROR_DUPLICATE_IDENTIFIER);
			}
			
			if(StringUtils.isNotBlank(acf.getSiretNumber())){
				// check uniqueness within assignable roles
				Long countSiret = this.subdivService.countBySiretNumber(acf.getSiretNumber(), acf.getCompanyRole(), null);
				if(countSiret != 0){
					errors.rejectValue(FIELD_SIRET_NUMBER, ERROR_DUPLICATE_SIRET_NUMBER);
				}
			}
			
			switch (acf.getCompanyRole()) {
			case CLIENT:
				validateClient(acf, errors);
				break;
			case BUSINESS:
				validateBusiness(acf, errors);
				break;
			default:
				break;
			}
		}
	}

	// client companies (only!) require a business ID to be set
	protected void validateClient(CompanyAddForm acf, Errors errors) {
		if (acf.getBusinessAreaId() == null) {
			errors.rejectValue(FIELD_BUSINESS_AREA, ERROR_BUSINESS_AREA);
		}
	}

	// business companies require a company code to be set
	protected void validateBusiness(CompanyAddForm acf, Errors errors) {
		if (acf.getCompanyCode() == null || acf.getCompanyCode().trim().length() == 0
				|| acf.getCompanyCode().trim().length() > 3) {
			errors.rejectValue(FIELD_COMPANY_CODE, ERROR_COMPANY_CODE);
		}
	}
}