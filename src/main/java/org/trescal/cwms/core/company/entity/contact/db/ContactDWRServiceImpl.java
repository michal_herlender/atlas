package org.trescal.cwms.core.company.entity.contact.db;

import lombok.val;
import org.jasypt.digest.StandardStringDigester;
import org.jasypt.digest.config.SimpleDigesterConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.dto.ContactSearchResultWrapper;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DigestConfigCreator;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service("ContactDWRService")
public class ContactDWRServiceImpl implements ContactDWRService {

    @Autowired
    private ContactDao contactDao;
    @Autowired
    private DigestConfigCreator configCreator;
    @Autowired
    private SessionUtilsService sessionUtilsService;
    @Autowired
    private UserService userServ;

    @Override
    public Integer checkContactDeactivationStatus(int personid) {
		// get a count of this contacts instruments
		Integer instcount = this.countContactInstruments(personid);

		return instcount;
	}

	@Override
	public int countContactInstruments(int personid) {
		return this.contactDao.countContactInstruments(personid);
	}

	public Contact findDWRContact(int id) {
		return this.contactDao.findDWRContact(id);
	}

	private Contact get(int id) {
		return contactDao.find(id);
	}

	public List<ContactSearchResultWrapper> getAllActiveSubdivContactsHQL(int subdivid) {
		return contactDao.getAllActiveContactsSearchResultWrapperProjection(subdivid);
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<ContactSearchResultWrapper> searchContactByRolesHQL(String firstName, String lastName, boolean active,
			String[] role, HttpSession session) {
		KeyValue<Integer, String> companyDto = (KeyValue<Integer, String>) session
				.getAttribute(Constants.SESSION_ATTRIBUTE_COMPANY);
		val companyRoles = Stream.of(role).map(String::trim).map(String::toUpperCase).map(CompanyRole::valueOf)
				.collect(Collectors.toList());
        return contactDao.searchByCompanyRoles(firstName,lastName,active,companyRoles);
	}

	@Override
	public List<ContactSearchResultWrapper> searchContactsHQL(String contactName, String subdivName, String companyName,
			String[] companyRole, String searchOrder, Integer businessCoId)

	{
		val companyRoles = Stream.of(companyRole).map(String::trim).map(String::toUpperCase).map(CompanyRole::valueOf)
				.collect(Collectors.toList());
		return this.contactDao.searchContactsHQL(contactName, subdivName, companyName, companyRoles, searchOrder,
				businessCoId);
	}

	@Override
	public Contact setEncryptedPassword(int personid, String encPassword, HttpServletRequest req) {
		StandardStringDigester digester = new StandardStringDigester();
		SimpleDigesterConfig config = this.configCreator.createDigesterConfig();
		digester.setConfig(config);

		String digested = digester.digest(encPassword);
		Contact con = this.get(personid);
		con.setEncryptedPassword(digested);
		this.updateContact(con, req);

		return con;
	}

	/**
	 * @deprecated should be removed once all session/contact upgrades are complete;
	 *             leaving in for now.
	 */
	@Deprecated
	public void updateContact(Contact c, HttpServletRequest req) {
		this.contactDao.update(c);
		String username = (String) req.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		// No result wrapper so eager load not needed
		Contact currentContact = userServ.get(username).getCon();
		if (currentContact != null) {
			if (currentContact.getPersonid() == c.getPersonid()) {
				// must still be re-loaded to get new details
				Contact con = this.findDWRContact(c.getPersonid());
				this.sessionUtilsService.setCurrentContact(req, con);
			}
		}
	}

}