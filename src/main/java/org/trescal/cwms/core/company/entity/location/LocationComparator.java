package org.trescal.cwms.core.company.entity.location;

import java.util.Comparator;

/**
 * Inner comparator class for sorting {@link Location}s when returning
 * collections of {@link Address}es, each with multiple {@link Location}s.
 * 
 * @author jamiev
 */
public class LocationComparator implements Comparator<Location>
{
	public int compare(Location loc1, Location loc2)
	{
		// compare addresses
		Integer addrid1 = loc1.getAdd().getAddrid();
		Integer addrid2 = loc2.getAdd().getAddrid();

		if (addrid1.equals(addrid2))
		{
			// compare locations
			String locStr1 = loc1.getLocation();
			String locStr2 = loc2.getLocation();
			return locStr1.compareTo(locStr2);
		}
		else
		{
			return addrid1.compareTo(addrid2);
		}
	}
}