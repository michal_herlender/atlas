package org.trescal.cwms.core.company.entity.companysettings.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.dto.CompanySettingsDTO;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.quotation.dto.CompanySettingsForAllocatedCompanyDTO;

@Repository
public class CompanySettingsForAllocatedCompanyDaoImpl
		extends AllocatedToCompanyDaoImpl<CompanySettingsForAllocatedCompany, Integer>
		implements CompanySettingsForAllocatedCompanyDao {
	@Override
	protected Class<CompanySettingsForAllocatedCompany> getEntity() {
		return CompanySettingsForAllocatedCompany.class;
	}

	@Override
	public CompanySettingsForAllocatedCompany getByCompany(Company company, Company allocatedCompany) {
		Criteria criteria = getSession().createCriteria(CompanySettingsForAllocatedCompany.class);
		criteria.add(Restrictions.eq("company", company));
		criteria.add(Restrictions.eq("organisation", allocatedCompany));
		return (CompanySettingsForAllocatedCompany) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CompanySettingsForAllocatedCompany> getAll(Set<Integer> companyIds, Company allocatedCompany) {
		// If there are no companyIds, we return an empty List, as query fails with
		// empty companyId list
		List<CompanySettingsForAllocatedCompany> results = new ArrayList<>();
		if (!companyIds.isEmpty()) {
			Criteria criteria = getSession().createCriteria(CompanySettingsForAllocatedCompany.class);
			criteria.createCriteria("company", "company").add(Restrictions.in("company.coid", companyIds));
			criteria.add(Restrictions.eq("organisation", allocatedCompany));
			criteria.addOrder(Order.asc("company.coid"));
			results = criteria.list();
		}
		return results;
	}

	@Override
	public List<CompanySettingsDTO> getDtosForAllocatedCompany(Collection<Integer> companyIds, Integer allocatedCompanyId) {
		return companyIds == null || companyIds.isEmpty() ? new ArrayList<CompanySettingsDTO>() : getResultList(cb -> {
			CriteriaQuery<CompanySettingsDTO> cq = cb.createQuery(CompanySettingsDTO.class);
			Root<CompanySettingsForAllocatedCompany> settings = cq.from(CompanySettingsForAllocatedCompany.class);
			Join<CompanySettingsForAllocatedCompany, Company> client = settings
					.join(CompanySettingsForAllocatedCompany_.company);
			Join<CompanySettingsForAllocatedCompany, Company> allocatedCompany = settings
					.join(CompanySettingsForAllocatedCompany_.organisation.getName());
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(allocatedCompany.get(Company_.coid), allocatedCompanyId));
			clauses.getExpressions().add(client.get(Company_.coid).in(companyIds));
			cq.where(clauses);
			cq.select(cb.construct(CompanySettingsDTO.class, client.get(Company_.coid),
					settings.get(CompanySettingsForAllocatedCompany_.active),
					settings.get(CompanySettingsForAllocatedCompany_.onStop)));
			return cq;
		});
	}
	
	@Override
	public List<CompanySettingsForAllocatedCompanyDTO> getAllSettingsDtosForAllocatedCompany(Collection<Integer> companyIds, Collection<Integer> allocatedCompanyIds) {
		return  getResultList(cb -> {
			CriteriaQuery<CompanySettingsForAllocatedCompanyDTO> cq = cb.createQuery(CompanySettingsForAllocatedCompanyDTO.class);
			Root<CompanySettingsForAllocatedCompany> settings = cq.from(CompanySettingsForAllocatedCompany.class);
			Join<CompanySettingsForAllocatedCompany, Company> client = settings
					.join(CompanySettingsForAllocatedCompany_.company);
			Join<CompanySettingsForAllocatedCompany, Company> allocatedCompany = settings
					.join(CompanySettingsForAllocatedCompany_.organisation.getName());
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(allocatedCompany.get(Company_.coid).in(allocatedCompanyIds));
			clauses.getExpressions().add(client.get(Company_.coid).in(companyIds));
			cq.where(clauses);
			cq.select(cb.construct(CompanySettingsForAllocatedCompanyDTO.class, allocatedCompany.get(Company_.coid),
					client.get(Company_.coid),
					settings.get(CompanySettingsForAllocatedCompany_.active),
					settings.get(CompanySettingsForAllocatedCompany_.onStop)));
			return cq;
		});
	}
}