package org.trescal.cwms.core.company.form;

import java.util.Map;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.department.Department;

public class DepartmentForm
{
	private Department department;
	private Integer departmentType;
	private Integer addrid;
	private Integer subdivid;
	private String departmentName;
	private String shortName;
	private Map<Integer, String> deptRoles;
	private Map<Integer, String> contacts;
	private boolean newDepartment;
	private boolean userCanEdit;
	
	public Department getDepartment() {
		return department;
	}
	
	@NotNull
	public Integer getDepartmentType() {
		return this.departmentType;
	}
	
	@NotNull
	public Integer getAddrid() {
		return this.addrid;
	}
	
	public Integer getSubdivid() {
		return this.subdivid;
	}
	
	public Map<Integer, String> getContacts() {
		return this.contacts;
	}
	
	public Map<Integer, String> getDeptRoles()
	{
		return this.deptRoles;
	}
	
	@Length(min = 1, max = 40)
	public String getDepartmentName() {
		return departmentName;
	}
	
	@NotNull
	@Length(max = 4)
	public String getShortName() {
		return shortName;
	}
	
	public boolean isNewDepartment() {
		return this.newDepartment;
	}
	
	public boolean isUserCanEdit() {
		return this.userCanEdit;
	}
	
	public void setDepartment(Department department) {
		this.department = department;
	}
	
	public void setDepartmentType(Integer departmentType) {
		this.departmentType = departmentType;
	}
	
	public void setAddrid(Integer addrid) {
		this.addrid = addrid;
	}
	
	public void setSubdivid(Integer subdivid) {
		this.subdivid = subdivid;
	}
	
	public void setContacts(Map<Integer, String> contacts) {
		this.contacts = contacts;
	}
	
	public void setDeptRoles(Map<Integer, String> deptRoles) {
		this.deptRoles = deptRoles;
	}
	
	public void setNewDepartment(boolean newDepartment) {
		this.newDepartment = newDepartment;
	}
	
	public void setUserCanEdit(boolean userCanEdit) {
		this.userCanEdit = userCanEdit;
	}
	
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
}