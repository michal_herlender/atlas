package org.trescal.cwms.core.company.entity.contact;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.SortComparator;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.trescal.cwms.core.account.entity.companyaccount.CompanyAccount;
import org.trescal.cwms.core.account.entity.creditcard.CreditCard;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMemberComparator;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.mailgroupmember.MailGroupMember;
import org.trescal.cwms.core.company.entity.organisation.OrganisationLevel;
import org.trescal.cwms.core.company.entity.salesdiscount.SalesDiscount;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationaction.InstrumentValidationAction;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.InstrumentValidationIssue;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;
import org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.CapabilityTrainingRecord;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.schedule.entity.repeatschedule.RepeatSchedule;
import org.trescal.cwms.core.schedule.entity.repeatschedule.RepeatScheduleComparator;
import org.trescal.cwms.core.system.entity.printer.Printer;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;

/**
 * Entity representing a member of {@link Company}. All {@link Contact}s belong
 * to a {@link Subdiv}.
 */
@Entity
@Table(name = "contact")
public class Contact extends Versioned implements Serializable, OrganisationLevel {
	private static final long serialVersionUID = 1L;

	// if this field is set then this contact is a company 'generic' contact
    // used for accounts purposes
    @Deprecated
    private Company accountCompany;
    // if this field is set then this contact is the company's primary account
    // contact
    @Deprecated
    private CompanyAccount accountContact;
    private Set<CapabilityAuthorization> authorizations;
    private boolean active;
    private List<BPO> BPOs;
    private LocalDate createDate;
    private CreditCard creditCard;
    private String creditRating;
    private String deactReason;
    private Date deactTime;
    private Address defAddress;
    private Printer defaultPrinter;
    private Location defLocation;
    private String email;
    private String encryptedPassword;
    private String fax;
	private String firstName;
	private Set<ContactInstructionLink> instructions;
	private Set<Instrument> instrum;
	private Set<InstrumentValidationAction> instrumentActions;
    private Set<InstrumentValidationIssue> instrumentIssuesRaised;
    private Set<InstrumentValidationIssue> instrumentIssuesResolved;
    private Set<Job> jobs;
    private Date lastActivity;
    private String lastName;
    private Set<MailGroupMember> mailGroupMembers;
    private Set<DepartmentMember> member;
    private String mobile;
    private int personid;
    private String position;
    private String preference;
    private Set<CapabilityTrainingRecord> capabilityTrainingRecords;
    private Set<Quotation> quotations = new HashSet<>(0);
    private Set<RepeatSchedule> repeatSchedules;
    private String salesCommission;
    private SalesDiscount sd;
    private String statusMessage;
    private Subdiv sub;
    private Set<SystemDefaultApplication> systemDefaults;
    private String telephone;
    private Integer telephoneExt;
    private String title;
    private User user;
    private UserGroup usergroup;
    private UserPreferences userPreferences;
    private Locale locale;
    private String hrid;

    public Contact() {
    }

    public Contact(String firstname, String lastname, Subdiv s, LocalDate createDate, String tele, String fax, String mob,
                   String email, Date lastAct, String salesCom, String creditRating) {
        this(firstname, lastname, s, createDate, tele, fax, mob, email, lastAct, salesCom, creditRating,
            "");
    }

    public Contact(String firstname, String lastname, Subdiv s, LocalDate createDate, String tele, String fax, String mob,
                   String email, Date lastAct, String salesCom, String creditRating, String preference) {
        this();
        this.firstName = firstname;
        this.lastName = lastname;
        this.sub = s;
        this.createDate = createDate;
        this.telephone = tele;
        this.fax = fax;
        this.mobile = mob;
        this.email = email;
		this.setLastActivity(lastAct);
		this.salesCommission = salesCom;
		this.creditRating = creditRating;
		this.preference = preference;
		this.position = "";
	}

	public void addUser(User u) {
		this.user = u;
	}

	// Made Transient (performance impact from @OneToOne 2018-09-07 GB
	// Targeted to removal, checking with Adrian about Account Dimensions 
//	@OneToOne(mappedBy = "accountContact")
	@Transient
	public Company getAccountCompany() {
		return this.accountCompany;
    }

    // Made Transient (performance impact from @OneToOne 2018-09-07 GB
    // Targeted to removal, checking with Adrian about Account Dimensions
//	@OneToOne(mappedBy = "contact")
    @Transient
    public CompanyAccount getAccountContact() {
        return this.accountContact;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "authorizationFor")
    public Set<CapabilityAuthorization> getAuthorizations() {
        return this.authorizations;
    }

    /**
     * @return the bPOs
     */
    @OneToMany(mappedBy = "contact", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<BPO> getBPOs() {
        return this.BPOs;
    }

    @Column(name = "createdate", columnDefinition = "date")
    public LocalDate getCreateDate() {
        return this.createDate;
    }

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "creditCard_ccid")
	public CreditCard getCreditCard() {
		return this.creditCard;
	}

	@Length(max = 1)
	@Column(name = "creditrating", length = 1)
	public String getCreditRating() {
		return this.creditRating;
	}

	@Length(max = 255)
	@Column(name = "deactreason")
	public String getDeactReason() {
		return this.deactReason;
	}

	@Column(name = "deacttime", columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDeactTime() {
		return this.deactTime;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "defaddress")
	public Address getDefAddress() {
		return this.defAddress;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "defprinterid")
	public Printer getDefaultPrinter() {
		return this.defaultPrinter;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deflocation")
	public Location getDefLocation() {
		return this.defLocation;
	}

	/**
	 * Note, no @Email validation here, it is done on the form
	 */
	@Length(max = 200)
	@Column(name = "email", length = 200)
	@NotEmpty(message = "{error.contact.email}")
	public String getEmail() {
		return this.email;
	}

	/**
	 * @return the encryptedPassword
	 */
	@Length(max = 255)
	@Column(name = "encryptedpassword")
	public String getEncryptedPassword() {
		return this.encryptedPassword;
	}

	@Length(max = 20)
	@Column(name = "fax", length = 20)
	@Pattern(regexp = "^s*|(\\+[ ]?\\d{1,3}( )?)?((\\(\\d{1,3}\\))[ ]?(\\d)?|\\d{1,3})[- .]?(\\d{2,4}[- .]?){1,4}\\S+$",message = "{error.fax}")
	public String getFax() {
		return this.fax;
	}

	@Length(max = 30)
	@Column(name = "firstname", length = 30)
	@NotEmpty(message = "{error.contact.nofirstname}")
	public String getFirstName() {
		return this.firstName;
	}

	@Transient
	public String getInitials() {
		return ContactFormatTools.getInitials(this);
	}

	@OneToMany(mappedBy = "contact", cascade = CascadeType.ALL)
	public Set<ContactInstructionLink> getInstructions() {
		return this.instructions;
	}

	@OneToMany(mappedBy = "con")
	public Set<Instrument> getInstrum() {
		return this.instrum;
	}

	@OneToMany(mappedBy = "underTakenBy", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<InstrumentValidationAction> getInstrumentActions() {
		return this.instrumentActions;
	}

	@OneToMany(mappedBy = "raisedBy", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<InstrumentValidationIssue> getInstrumentIssuesRaised() {
		return this.instrumentIssuesRaised;
	}

	@OneToMany(mappedBy = "resolvedBy", fetch = FetchType.LAZY)
	public Set<InstrumentValidationIssue> getInstrumentIssuesResolved() {
		return this.instrumentIssuesResolved;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "con")
	public Set<Job> getJobs() {
		return this.jobs;
	}

	@Column(name = "lastactivity", columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastActivity() {
		return this.lastActivity;
	}

	@Length(max = 30)
	@Column(name = "lastname", length = 30)
	public String getLastName() {
		return this.lastName;
	}

	@OneToMany(mappedBy = "contact")
	public Set<MailGroupMember> getMailGroupMembers() {
		return this.mailGroupMembers;
	}

	@OneToMany(mappedBy = "contact")
	@SortComparator(DepartmentMemberComparator.class)
	public Set<DepartmentMember> getMember() {
		return this.member;
	}

	@Length(max = 20)
	@Column(name = "mobile", length = 20)
	public String getMobile() {
		return this.mobile;
	}

	@Transient
	public String getName() {
		return this.getFirstName() + " " + this.getLastName();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "personid", unique = true, nullable = false)
	public int getPersonid() {
		return this.personid;
	}

	@Transient
	public Integer getId() {
		return personid;
	}

	@Length(max = 40)
	@Column(name = "position", length = 40)
    public String getPosition() {
        return this.position;
    }

    @Length(max = 1)
    @Column(name = "preference", length = 1)
    public String getPreference() {
        return this.preference;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "trainee")
    public Set<CapabilityTrainingRecord> getCapabilityTrainingRecords() {
        return this.capabilityTrainingRecords;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "contact")
    public Set<Quotation> getQuotations() {
        return this.quotations;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "con")
    @SortComparator(RepeatScheduleComparator.class)
    public Set<RepeatSchedule> getRepeatSchedules() {
        return this.repeatSchedules;
	}

	@Transient
	public String getReversedName() {
		return this.getLastName() + ", " + this.getFirstName();
	}

	@Length(max = 20)
	@Column(name = "salescomm", length = 20)
	public String getSalesCommission() {
		return this.salesCommission;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "discount")
	public SalesDiscount getSd() {
		return this.sd;
	}

	@Transient
	public String getShortenedName() {
		return ContactFormatTools.getShortenedName(this);
	}

	@Length(max = 200)
	@Column(name = "statusmessage", length = 200)
	public String getStatusMessage() {
		return this.statusMessage;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subdivid")
	public Subdiv getSub() {
		return this.sub;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "contact")
	public Set<SystemDefaultApplication> getSystemDefaults() {
		return this.systemDefaults;
	}

	@Length(max = 30)
	@Column(name = "telephone", length = 30)
	public String getTelephone() {
		return this.telephone;
	}

	@Column(name = "telephoneext")
	public Integer getTelephoneExt() {
		return this.telephoneExt;
	}


	@OneToOne(mappedBy = "con", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public User getUser() {
		return this.user;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "groupid")
	public UserGroup getUsergroup() {
		return this.usergroup;
	}

	@OneToOne(mappedBy = "contact", cascade = CascadeType.ALL)
	public UserPreferences getUserPreferences() {
		return this.userPreferences;
	}

	@Column(name = "active", columnDefinition = "tinyint")
	public boolean isActive() {
		return this.active;
	}

	@Column(name = "locale")
	public Locale getLocale() {
		return locale;
	}

	@Column(name = "hrid")
	public String getHrid() {
		return hrid;
	}


	public String printedPreference() {
		char p = this.preference.charAt(0);
		switch (p) {
		case 'F':
			return "Fax";
		case 'S':
			return "SMS";
		case 'E':
			return "Email";
		case 'L':
			return "Letter";
		default:
			return this.preference;
        }
    }

    public void setAccountCompany(Company accountCompany) {
        this.accountCompany = accountCompany;
    }

    public void setAccountContact(CompanyAccount accountContact) {
        this.accountContact = accountContact;
    }

    public void setAuthorizations(Set<CapabilityAuthorization> authorizations) {
        this.authorizations = authorizations;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @param bPOs the bPOs to set
     */
    public void setBPOs(List<BPO> bPOs) {
        this.BPOs = bPOs;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public void setCreditRating(String creditRating) {
        this.creditRating = creditRating;
    }

	public void setDeactReason(String deactReason) {
		this.deactReason = deactReason;
	}

	public void setDeactTime(Date deactTime) {
		this.deactTime = deactTime;
	}

	public void setDefAddress(Address defAddress) {
		this.defAddress = defAddress;
	}

	public void setDefaultPrinter(Printer defaultPrinter) {
		this.defaultPrinter = defaultPrinter;
	}

	public void setDefLocation(Location defLocation) {
		this.defLocation = defLocation;
	}

	public void setEmail(String email) {
		this.email = email != null ? email.trim() : null;
	}

	/**
	 * @param encryptedPassword
	 *            the encryptedPassword to set
	 */
	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	public void setFax(String fax) {
		this.fax = fax!=null ? fax.trim():"";
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setInstructions(Set<ContactInstructionLink> instructions) {
		this.instructions = instructions;
	}

	public void setInstrum(Set<Instrument> instrum) {
		this.instrum = instrum;
	}

	public void setInstrumentActions(Set<InstrumentValidationAction> instrumentActions) {
		this.instrumentActions = instrumentActions;
	}

	public void setInstrumentIssuesRaised(Set<InstrumentValidationIssue> instrumentIssuesRaised) {
		this.instrumentIssuesRaised = instrumentIssuesRaised;
	}

	public void setInstrumentIssuesResolved(Set<InstrumentValidationIssue> instrumentIssuesResolved) {
		this.instrumentIssuesResolved = instrumentIssuesResolved;
	}

	public void setJobs(Set<Job> jobs) {
		this.jobs = jobs;
	}

	public void setLastActivity(Date lastActivity) {
		this.lastActivity = lastActivity;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setMailGroupMembers(Set<MailGroupMember> mailGroupMembers) {
		this.mailGroupMembers = mailGroupMembers;
	}

	public void setMember(Set<DepartmentMember> member) {
		this.member = member;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile!=null ? mobile.trim():"";
    }

    public void setPersonid(int personid) {
        this.personid = personid;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }

    public void setCapabilityTrainingRecords(Set<CapabilityTrainingRecord> capabilityTrainingRecords) {
        this.capabilityTrainingRecords = capabilityTrainingRecords;
    }

    public void setQuotations(Set<Quotation> quotations) {
        this.quotations = quotations;
    }

    public void setRepeatSchedules(Set<RepeatSchedule> repeatSchedules) {
        this.repeatSchedules = repeatSchedules;
    }

    public void setSalesCommission(String salesCommission) {
        this.salesCommission = salesCommission;
    }

    public void setSd(SalesDiscount sd) {
        this.sd = sd;
    }

    public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public void setSub(Subdiv sub) {
		this.sub = sub;
	}

	/**
	 * @param systemDefaults
	 *            the systemDefaults to set
	 */
	public void setSystemDefaults(Set<SystemDefaultApplication> systemDefaults) {
		this.systemDefaults = systemDefaults;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone != null ? telephone.trim():"";
	}

	public void setTelephoneExt(Integer telephoneExt) {
		this.telephoneExt = telephoneExt;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setUsergroup(UserGroup usergroup) {
		this.usergroup = usergroup;
	}

	public void setUserPreferences(UserPreferences userPreferences) {
		this.userPreferences = userPreferences;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public void setHrid(String hrid) {
		this.hrid = hrid;
	}

	@Override
	public String toString() {
		return (this.personid + " " + this.firstName + " " + this.lastName);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title!=null? title.trim():"";
	}
	
}
