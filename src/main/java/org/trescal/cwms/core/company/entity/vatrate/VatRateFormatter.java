package org.trescal.cwms.core.company.entity.vatrate;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.springframework.stereotype.Component;
import org.trescal.cwms.spring.model.KeyValue;

@Component
public class VatRateFormatter {

	/**
	 * Assembles a localized, ordered map of vat rate choices sorted by group and description 
	 * @param vatRates an list of vatRates, order is not taken into account 
	 * @param locale
	 * @return a Map suitable for usage in a selection of grouped inputs where
	 *         - the keys of the map are String group titles 
	 *         - the values of the map are Sets of KeyValue<Sring.String> where the key is the vatCode, 
	 *         -     and the value is the localized description
	 */
	public Map<String, Set<KeyValue<String,String>>> getOptionGroupMap(List<VatRate> vatRates, Locale locale) {
		Map<String, Set<KeyValue<String,String>>> result = new TreeMap<>();
		for (VatRate vatRate : vatRates) {
			VatRateType type = vatRate.getType();
			String group = type.getMessageForLocale(locale);
			String description = formatDescriptionAndRate(vatRate, locale, vatRate.getRate());
			KeyValue<String,String> option = new KeyValue<>(vatRate.getVatCode(), description);
			if (!result.containsKey(group))
				result.put(group, new TreeSet<>());
			Set<KeyValue<String,String>> options = result.get(group);
			options.add(option);
		}
		
		return result;
	}
	
	/**
	 * Returns a localized description + rate, meant for unique identification within a group only
	 * Handles null vatRate / rate, just as pricing entities technically have nullable vat rates
	 * 
	 * @param vatRate the VatRate entity providing a description of the rate
	 * @param locale the locale to format the description for
	 * @param rate the BigDecimal rate (may be different than the current rate)
	 * 
	 * @return String containing the formatted rate (e.g. "Spain - 20.00%") 
	 */
	public String formatDescriptionAndRate(VatRate vatRate, Locale locale, BigDecimal rate) {
		StringBuffer result = new StringBuffer();
		if (vatRate != null) {
			switch (vatRate.getType()) {
			case COUNTRY_DEFAULT:
				result.append(vatRate.getCountry().getNameForLocale(locale));
				break;
			case COUNTRY_SPECIAL:
				result.append(vatRate.getCountry().getNameForLocale(locale));
				result.append(" - ");
				result.append(vatRate.getDescription());
				break;
			default:
				result.append(vatRate.getDescription());
				break;
			}
		}
		if (rate != null) {
			result.append(" - ");
			result.append(rate.toString());
			result.append("%");
		}
		return result.toString();
	}
	
	/**
	 * Returns a localized group and description, meant for global identification / general use
	 * Handles null vatRate / rate, because pricing entities technically have nullable vat rates
	 * 
	 * @param vatRate the VatRate entity providing a description of the rate
	 * @param locale the locale to format the description for
	 * @param rate the BigDecimal rate (historical values may be different than the current rate)
	 * 
	 * @return String containing the formatted rate (e.g. "Country Default - Spain - 20.00%") 
	 */
	public String formatVatRate(VatRate vatRate, Locale locale, BigDecimal rate) {
		StringBuffer result = new StringBuffer();
		if (vatRate != null) {
			result.append(vatRate.getType().getMessageForLocale(locale));
			result.append(" - ");
		}
		result.append(formatDescriptionAndRate(vatRate, locale, rate));
		return result.toString();
	}
	
	/**
	 * Returns a localized group + description + rate, meant for global identification / general use
	 * Handles null vatRate, because pricing entities technically have nullable vat rates
	 * 
	 * @param vatRate the VatRate entity providing a description of the rate
	 * @param locale the locale to format the description for
	 * 
	 * @return String containing the formatted rate (e.g. "Country Default - Spain - 20.00%") 
	 */
	public String formatVatRate(VatRate vatRate, Locale locale) {
		return formatVatRate(vatRate, locale, vatRate != null ? vatRate.getRate() : null);
	}
}
