package org.trescal.cwms.core.company.entity.corole;

import java.util.EnumSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum CompanyRole
{
	NOTHING("companyrole.nothing", false),
	CLIENT("companyrole.client", false),
	SUPPLIER("companyrole.supplier", false),
	PROSPECT("companyrole.prospect", false),
	UTILITY("companyrole.utility", false),
	BUSINESS("companyrole.business", true);
	
	private String messageCode;
	private ReloadableResourceBundleMessageSource messageSource;
	private boolean departmentEnabled;
	
	@Component
	private static class CompanyRoleMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messageSource;
		@PostConstruct
        public void postConstruct() {
            for (CompanyRole cr : EnumSet.allOf(CompanyRole.class))
               cr.messageSource = messageSource;
        }
	}
	
	private CompanyRole(String messageCode, boolean departmentEnabled) {
		this.messageCode = messageCode;
		this.departmentEnabled = departmentEnabled;
	}
	
	public Set<CompanyRole> editableTo() {
		Set<CompanyRole> editableTo = EnumSet.noneOf(CompanyRole.class);
		switch (this) {
			case SUPPLIER: editableTo.add(UTILITY); break;
			case PROSPECT: editableTo.add(CLIENT); break;
			case UTILITY: editableTo.add(SUPPLIER); break;
			default: break;
		}
		return editableTo;
	}
	
	public Set<CompanyRole> editableFrom() {
		Set<CompanyRole> editableFrom = EnumSet.noneOf(CompanyRole.class);
		switch (this) {
			case SUPPLIER: editableFrom.add(UTILITY); break;
			case CLIENT: editableFrom.add(PROSPECT); break;
			case UTILITY: editableFrom.add(SUPPLIER); break;
			default: break;
		}
		return editableFrom;
	}
	
	public Integer getKey() {
		switch (this) {
			case BUSINESS: return 5;
			case CLIENT: return 1;
			case NOTHING: return 0;
			case PROSPECT: return 3;
			case SUPPLIER: return 2;
			case UTILITY: return 4;
		}
		return 0;
	}
	
	public static CompanyRole get(Integer key) {
		switch (key) {
			case 1: return CLIENT;
			case 2: return SUPPLIER;
			case 3: return PROSPECT;
			case 4: return UTILITY;
			case 5: return BUSINESS;
		}
		return NOTHING;
	}
	
	public String getMessage() {
		return messageSource.getMessage(messageCode, null, this.toString(), LocaleContextHolder.getLocale());
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	
	public boolean isDepartmentEnabled() {
		return departmentEnabled;
	}
	
	public boolean isEditable() {
		return !this.editableTo().isEmpty();
	}
}