package org.trescal.cwms.core.company.entity.contactinstructionlink.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

public interface ContactInstructionLinkDao extends BaseDao<ContactInstructionLink, Integer>
{
	Long count(Contact contact, Company allocatedCompany);
	List<ContactInstructionLink> getForContactAndTypes(Contact contact, Company allocatedCompany, InstructionType... types);
}