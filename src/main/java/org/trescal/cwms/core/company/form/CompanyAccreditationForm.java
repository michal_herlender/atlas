package org.trescal.cwms.core.company.form;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

public class CompanyAccreditationForm {
	private HashMap<Integer, LocalDate> companyAccreditationDate;
	private HashMap<Integer, String> companyAccreditationReason;
	private ArrayList<Integer> supplierIds;

	/*
	 * This form is used for reference data and spring binding;
	 * initialized here to allow add-type methods when populating controller reference data
	 */
	public CompanyAccreditationForm() {
		companyAccreditationDate = new HashMap<>();
		companyAccreditationReason = new HashMap<>();
		supplierIds = new ArrayList<>();
	}

	/*
	 * Generic getters
	 */
	public HashMap<Integer, LocalDate> getCompanyAccreditationDate() {
		return companyAccreditationDate;
	}

	public HashMap<Integer, String> getCompanyAccreditationReason() {
		return companyAccreditationReason;
	}

	public ArrayList<Integer> getSupplierIds() {
		return supplierIds;
	}

	/*
	 * Generic setters
	 */
	public void setCompanyAccreditationDate(
		HashMap<Integer, LocalDate> companyAccreditationDate) {
		this.companyAccreditationDate = companyAccreditationDate;
	}

	public void setCompanyAccreditationReason(
		HashMap<Integer, String> companyAccreditationReason) {
		this.companyAccreditationReason = companyAccreditationReason;
	}

	public void setSupplierIds(ArrayList<Integer> supplierIds) {
		this.supplierIds = supplierIds;
	}

	/*
	 * Additional mutators to easily allow adding reference data from controller
	 */
	public void addCompanyAccreditationDate(Integer id, LocalDate date) {
		this.companyAccreditationDate.put(id, date);
	}

	public void addCompanyAccreditationReason(Integer id, String reason) {
		this.companyAccreditationReason.put(id, reason);
	}
	public void addSupplierId(Integer id) {
		this.supplierIds.add(id);
	}
}