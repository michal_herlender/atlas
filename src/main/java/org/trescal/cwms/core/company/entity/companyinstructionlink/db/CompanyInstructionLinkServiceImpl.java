package org.trescal.cwms.core.company.entity.companyinstructionlink.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

@Service
public class CompanyInstructionLinkServiceImpl extends BaseServiceImpl<CompanyInstructionLink, Integer>
		implements CompanyInstructionLinkService {

	@Autowired
	private CompanyInstructionLinkDao baseDao;

	@Override
	protected BaseDao<CompanyInstructionLink, Integer> getBaseDao() {
		return baseDao;
	}

	@Override
	public List<CompanyInstructionLink> getAllForCompany(Company company, Subdiv allocatedSubdiv) {
		return baseDao.getForCompanyAndTypes(company, allocatedSubdiv, new InstructionType[] {});
	}

	@Override
	public List<CompanyInstructionLink> getForCompanyAndTypes(Company company, Subdiv allocatedSubdiv, InstructionType... types) {
		return baseDao.getForCompanyAndTypes(company, allocatedSubdiv, types);
	}
}