/**
 * 
 */
package org.trescal.cwms.core.company.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.db.CountryService;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.db.VatRateService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

/**
 * Validates editing a {@link Company}.
 * 
 * @author Richard
 */
@Component
public class CompanyEditValidator extends AbstractBeanValidator
{
	@Autowired
	private VatRateService vatRateService; 
	@Autowired
	private CountryService countryService;
	@Autowired
	private CompanyService companyService;

	public static final String PROPERTY_SPECIFIC_RATE_VALUE = "specificRateValue";
	public static final String ERROR_CODE_SPECIFIC_RATE_VALUE = "error.companyedit.specificRateValue";
	public static final String ERROR_MESSAGE_SPECIFIC_RATE_VALUE = "Rate must be set";
	
	public static final String PROPERTY_VAT_RATE = "vatCode";
	public static final String ERROR_CODE_VAT_RATE = "error.vatrate.invalid";
	public static final String ERROR_MESSAGE_VAT_RATE = "Invalid VAT rate selection";
	
	public static final String PROPERTY_PRIMARY_CONTACT = "primaryBusinessContactId";
	public static final String ERROR_CODE_PRIMARY_CONTACT = "error.company.edit.businesscontact.notnull";
	public static final String ERROR_MESSAGE_PRIMARY_CONTACT = "A business contact must be selected";

	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(CompanyEditForm.class);
	}
	
	@Override
	public void validate(Object target, Errors errors)
	{
		super.validate(target, errors);
		CompanyEditForm cef = (CompanyEditForm) target;
		validateCurrencyRate(cef, errors);
		validateVatRate(cef, errors);
		if (cef.getChangeBusinessContact()) {
			if (cef.getPrimaryBusinessContactId() == null || cef.getPrimaryBusinessContactId() == 0) {
				errors.rejectValue(PROPERTY_PRIMARY_CONTACT, ERROR_CODE_PRIMARY_CONTACT, ERROR_MESSAGE_PRIMARY_CONTACT);
			}
		}
	}
	private void validateCurrencyRate(CompanyEditForm cef, Errors errors) {
		// add custom validation for the currencies
		if (cef.isSpecificCurrency() && cef.isSpecificRate())
		{
			if (cef.getSpecificRateValue() == null)
			{
				errors.rejectValue(PROPERTY_SPECIFIC_RATE_VALUE, ERROR_CODE_SPECIFIC_RATE_VALUE, ERROR_MESSAGE_SPECIFIC_RATE_VALUE);
			}
		}
	}
	private void validateVatRate(CompanyEditForm cef, Errors errors) {
		// validation that correct tax rate is selected
		Country businessCountry = this.companyService.get(cef.getBusinessCompanyId()).getCountry();
		Country companyCountry = this.countryService.get(cef.getCountryid()); 
		VatRate vatRate = this.vatRateService.get(cef.getVatCode());
		CompanyRole companyRole = cef.getCompanyRole();
		
		if (!this.vatRateService.isVatRateAllowed(vatRate, companyCountry, businessCountry, companyRole)) {
			errors.rejectValue(PROPERTY_VAT_RATE, ERROR_CODE_VAT_RATE, ERROR_MESSAGE_VAT_RATE);
		}
	}
}