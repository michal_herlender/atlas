package org.trescal.cwms.core.company.entity.companylistusage.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.companylistusage.CompanyListUsage;
import org.trescal.cwms.core.company.form.EditCompanyListForm;

public interface CompanyListUsageService extends BaseService<CompanyListUsage, Integer> {
	
	List<CompanyListUsage> getUsageByCompanyList(Integer companyListId);
	
	void createNewCompanyListUsage(EditCompanyListForm form, String username);
	
	boolean remove(Integer companyListUsageId);

}
