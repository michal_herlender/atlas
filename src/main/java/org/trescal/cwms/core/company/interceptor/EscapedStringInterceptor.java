package org.trescal.cwms.core.company.interceptor;

import org.apache.commons.text.StringEscapeUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Configurable;

@Aspect
@Configurable
public class EscapedStringInterceptor {

    @Around("args(encoded) && @annotation(UnescapeString)")
    public Object unescapeString(ProceedingJoinPoint jp, String encoded) throws Throwable {
        return jp.proceed(new Object[]{StringEscapeUtils.unescapeHtml4(encoded)});
    }
}
