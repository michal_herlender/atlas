package org.trescal.cwms.core.company.form.genericentityvalidator;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.dto.CompanyToContactDetailsDTO;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

/*
 * Appears to be used in validating DWR requests to create a freehand contact+customer
 * Refactored 2015-06-29 to use JPA (and some Hibernate) validations, refactored together with
 * CompanyToContactDetailsDTO due to removal of CustomEmailValidator
 * GB 2015-05-25
 */
@Component
public class CompanyToContactValidator extends AbstractBeanValidator
{
	@Autowired
	private CompanyService companyService;
	
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(CompanyToContactDetailsDTO.class);
	}

	public void validate(Object obj, Errors errors)
	{
		CompanyToContactDetailsDTO ctcdto = (CompanyToContactDetailsDTO) obj;

		if (ctcdto == null)
		{
			errors.rejectValue("Company To Contact DTO", null, 
					"Company to contact dto is null");
			return;
		}
		
		super.validate(ctcdto, errors);
		CompanyRole companyRole = ctcdto.getCompanyRole();
		if (this.companyService.existsByName(ctcdto.getConame().trim(), companyRole, null))
		{
			errors.rejectValue("coname", null, 
					"A company with this name is already a prospect or client");
		}
		// client companies must have a business area set
		if ((ctcdto.getBusinessAreaId() == null)
				&& (ctcdto.getCompanyRole() == CompanyRole.CLIENT))
		{
			errors.rejectValue("busAreaId", null, 
					"A company sector must be specified for a client company");
		}
		// All email addresses are validated using ApacheCommons EmailValidator, 
		// as it catches more than @Email annotation also used on DTO as bean validation; 
		if (!EmailValidator.getInstance(false).isValid(ctcdto.getEmail())) {
			errors.rejectValue("email", "error.email", null,
					"The email address entered is not of a valid format.");
		}
	}
}
