package org.trescal.cwms.core.company.form;

import org.trescal.cwms.core.company.entity.company.Company;

public class CompanyAddCommand {
	private Company allocatedCompany;

	public Company getAllocatedCompany() {
		return allocatedCompany;
	}

	public void setAllocatedCompany(Company allocatedCompany) {
		this.allocatedCompany = allocatedCompany;
	}
}
