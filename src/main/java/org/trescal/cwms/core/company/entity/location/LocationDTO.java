package org.trescal.cwms.core.company.entity.location;

import lombok.Data;

@Data
public class LocationDTO {
	
	private Integer locationid;
	private String location;
	
	public LocationDTO(Integer locationid, String location) {
		this.locationid = locationid;
		this.location = location;
	}

}
