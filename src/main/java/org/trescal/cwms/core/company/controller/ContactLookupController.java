package org.trescal.cwms.core.company.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("contactlookup")
public class ContactLookupController {

    private ContactService contactService;
    private CompanyService companyService;

    public ContactLookupController(ContactService contactService, CompanyService companyService) {
        this.contactService = contactService;
        this.companyService = companyService;
    }

    @GetMapping("/listallatsubdiv/{id}")
    public List<KeyValue<Integer, String>> getContactsAtSubdiv(@PathVariable("id") Integer subdivId) {
        return Optional.ofNullable(contactService.getAllSubdivContacts(subdivId)).orElse(Collections.emptyList())
                .stream()
                .map(c -> new KeyValue<>(c.getPersonid(), c.getName()))
                .collect(Collectors.toList());
    }


    @GetMapping("/listallatcompany/{id}")
    public List<KeyValue<Integer, String>> getContactsAtCompany(@PathVariable("id") Integer companyId) {
        return Optional.ofNullable(contactService.getAllCompanyContacts(companyId)).orElse(Collections.emptyList())
                .stream()
                .map(c -> new KeyValue<>(c.getId(), c.getName()))
                .collect(Collectors.toList());
    }

    @GetMapping("/defaultforclient/{id}")
    public Integer getClientDefaultContactId(@PathVariable("id") Integer companyId) {
        Integer result = 0;
    	Company company = companyService.get(companyId);
    	// Both below are technically nullable fields
        if (company.getDefaultSubdiv() != null && company.getDefaultSubdiv().getDefaultContact() != null) {
        	result = company.getDefaultSubdiv().getDefaultContact().getPersonid(); 
        }
        return result;
    }

    @GetMapping("/defaultBusinessCompanyContactForClient/{id}")
    public Integer getClientDefaultBusinessContactId(@PathVariable("id") Integer companyId) {
        return companyService.get(companyId).getDefaultBusinessContact().getPersonid();
    }

}