package org.trescal.cwms.core.company.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.company.dto.ContactSearchResultWrapper;
import org.trescal.cwms.core.company.entity.contact.db.ContactDWRService;

import java.util.List;

@RestController
@RequestMapping("contactsearch")
public class ContactSearchRestController {
    private final ContactDWRService contactDWRService;

    public ContactSearchRestController(ContactDWRService contactDWRService) {
        this.contactDWRService = contactDWRService;
    }

    @GetMapping("/activesubdivcontacts/{subdivid}")
    public List<ContactSearchResultWrapper> getSubdivContacts(@PathVariable("subdivid") Integer subdivId) {
        return contactDWRService.getAllActiveSubdivContactsHQL(subdivId);
    }
}
