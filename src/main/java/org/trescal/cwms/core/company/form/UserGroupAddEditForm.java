package org.trescal.cwms.core.company.form;

import java.util.List;

public class UserGroupAddEditForm {
	
	Integer coid;
	Integer userGroupId;
	String name;
	String description;
	List<Integer> selectedValues;
	
	public Integer getCoid() {
		return coid;
	}
	public void setCoid(Integer coid) {
		this.coid = coid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Integer> getSelectedValues() {
		return selectedValues;
	}
	public void setSelectedValues(List<Integer> selectedValues) {
		this.selectedValues = selectedValues;
	}
	public Integer getUserGroupId() {
		return userGroupId;
	}
	public void setUserGroupId(Integer userGroupId) {
		this.userGroupId = userGroupId;
	}
}
