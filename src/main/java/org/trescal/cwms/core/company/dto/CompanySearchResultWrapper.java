package org.trescal.cwms.core.company.dto;

import java.util.List;

import org.trescal.cwms.core.company.entity.corole.CompanyRole;

public class CompanySearchResultWrapper {
	private boolean blocked;
	private int coid;
	private String coname;
	private String corole;
	private String newconame;
	private String legalIdentifier;
	private List<String> oldnames;

	public CompanySearchResultWrapper() {
	}

	public CompanySearchResultWrapper(int coid, String coname, CompanyRole corole, String legalIdentifier) {
		this.coid = coid;
		this.coname = coname;
		this.corole = corole.getMessage();
		this.legalIdentifier = legalIdentifier;
	}

	public CompanySearchResultWrapper(int coid, String coname, String corole, boolean blocked, String legalIdentifier) {
		this.coid = coid;
		this.coname = coname;
		this.corole = corole;
		this.blocked = blocked;
		this.legalIdentifier = legalIdentifier;
	}

	public CompanySearchResultWrapper(int coid, String coname, String newconame, String corole,
			String legalIdentifier) {
		this.coid = coid;
		this.coname = coname;
		this.newconame = newconame;
		this.corole = corole;
		this.legalIdentifier = legalIdentifier;
	}

	public int getCoid() {
		return this.coid;
	}

	public String getConame() {
		return this.coname;
	}

	public String getCorole() {
		return this.corole;
	}

	public String getNewconame() {
		return this.newconame;
	}

	public boolean isBlocked() {
		return this.blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public void setCoid(int coid) {
		this.coid = coid;
	}

	public void setConame(String coname) {
		this.coname = coname;
	}

	public void setCorole(String corole) {
		this.corole = corole;
	}

	public void setNewconame(String newconame) {
		this.newconame = newconame;
	}

	public List<String> getOldnames() {
		return oldnames;
	}

	public void setOldnames(List<String> oldnames) {
		this.oldnames = oldnames;
	}

	public String getLegalIdentifier() {
		return legalIdentifier;
	}

	public void setLegalIdentifier(String legalIdentifier) {
		this.legalIdentifier = legalIdentifier;
	}

}