package org.trescal.cwms.core.company.form;

import java.util.Map;

public class SupplierViewForm
{
	private Map<Integer,Boolean> activeSupplierIds;

	public Map<Integer,Boolean> getActiveSupplierIds() {
		return activeSupplierIds;
	}

	public void setActiveSupplierIds(Map<Integer,Boolean> activeSupplierIds) {
		this.activeSupplierIds = activeSupplierIds;
	}

}
