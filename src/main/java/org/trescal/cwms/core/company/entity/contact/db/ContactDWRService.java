package org.trescal.cwms.core.company.entity.contact.db;

import org.trescal.cwms.core.company.dto.ContactSearchResultWrapper;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public interface ContactDWRService {

	Integer checkContactDeactivationStatus(int personid);

	int countContactInstruments(int personid);

	Contact findDWRContact(int id);

	/**
	 * Returns a {@link List} of {@link ContactSearchResultWrapper} that belong to
	 * the {@link Contact} identified by the given subdivid that are active.
	 * 
	 * @param subdivid the id of the {@link Subdiv}, not null.
	 * @return {@link List} of {@link ContactSearchResultWrapper}.
	 */
	List<ContactSearchResultWrapper> getAllActiveSubdivContactsHQL(int subdivid);

	/**
	 * Returns a {@link List} of {@link ContactSearchResultWrapper} whose
	 * (case-insensitive) first and last name matches the given names and are active
	 * or not and the corole is one that has been supplied in the role array.
	 * {@link Contact}s).
	 * 
	 * @param firstName the first name to match.
	 * @param lastName  the last name to match.
	 * @param active    the status of the Company.
	 * @param role      array of role names to match in results.
	 * @return {@link List} of {@link ContactSearchResultWrapper}.
	 */
	List<ContactSearchResultWrapper> searchContactByRolesHQL(String firstName, String lastName, boolean active,
			String[] role, HttpSession session);

	/**
	 * Returns a {@link List} of {@link ContactSearchResultWrapper} whose
	 * (case-insensitive) contact name matches the given name and the subdiv name
	 * matches the given name and the company name matches the given name and the
	 * corole is one that has been supplied in the role array. {@link Contact}s).
	 * 
	 * @param contactName the contact name to match.
	 * @param subdivName  the subdivision name to match.
	 * @param companyName the company name to match.
	 * @param companyRole array of role names to match in results.
	 * @param searchOrder string indicating the requested search order.
	 * @return {@link List} of {@link ContactSearchResultWrapper}.
	 */
	List<ContactSearchResultWrapper> searchContactsHQL(String contactName, String subdivName, String companyName,
			String[] companyRole, String searchOrder, Integer businessCoId);

	Contact setEncryptedPassword(int personid, String encPassword, HttpServletRequest req);

	@Deprecated
	void updateContact(Contact c, HttpServletRequest req);

}