package org.trescal.cwms.core.company.entity.companylist.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companylist.CompanyList;
import org.trescal.cwms.core.company.form.CompanyListSearchForm;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface CompanyListDao extends BaseDao<CompanyList, Integer> {

	CompanyList getByName(String nameList, Company allocatedCompany);

	PagedResultSet<CompanyList> searchCompanyList(CompanyListSearchForm form, Company allocatedCompany,
			PagedResultSet<CompanyList> rs);

	Integer getCompanyListByCompanyMember(Integer coid, Company allocatedCompany);
	
	List<Integer> getCompanyListsByUser(Integer coid, Company allocatedCompany);
}
