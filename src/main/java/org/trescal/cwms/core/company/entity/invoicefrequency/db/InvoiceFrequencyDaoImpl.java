package org.trescal.cwms.core.company.entity.invoicefrequency.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.invoicefrequency.InvoiceFrequency;
import org.trescal.cwms.core.company.entity.invoicefrequency.InvoiceFrequency_;

@Repository
public class InvoiceFrequencyDaoImpl extends
		BaseDaoImpl<InvoiceFrequency, Integer> implements InvoiceFrequencyDao {

	@Override
	protected Class<InvoiceFrequency> getEntity() {
		return InvoiceFrequency.class;
	}

	@Override
	public InvoiceFrequency get(String code) {
		return getFirstResult(cb ->{
			CriteriaQuery<InvoiceFrequency> cq = cb.createQuery(InvoiceFrequency.class);
			Root<InvoiceFrequency> root = cq.from(InvoiceFrequency.class);
			cb.equal(root.get(InvoiceFrequency_.code), code);
			return cq;
		}).orElse(null);
	}

}
