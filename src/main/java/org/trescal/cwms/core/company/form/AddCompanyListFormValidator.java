package org.trescal.cwms.core.company.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companylist.db.CompanyListService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class AddCompanyListFormValidator extends AbstractBeanValidator {

	@Autowired
	private CompanyListService companyListService;
	@Autowired
	private CompanyService companyService;

	@Override
	public boolean supports(Class<?> arg0) {
		return AddCompanyListForm.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {

		super.validate(target, errors);

		if (!errors.hasErrors()) {

			AddCompanyListForm form = (AddCompanyListForm) target;
			Company allocatedCompany = companyService.get(form.getOrgid());

			// the name of the list is unique for the logged-in business company
			if (companyListService.getByName(form.getNameList(), allocatedCompany) != null) {
				errors.rejectValue("nameList", "companylist.name.already.exist");
			}
		}
	}

}
