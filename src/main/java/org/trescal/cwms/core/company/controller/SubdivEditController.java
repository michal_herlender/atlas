package org.trescal.cwms.core.company.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.companysettings.db.SubdivSettingsForAllocatedSubdivService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.form.SubdivEditForm;
import org.trescal.cwms.core.company.form.SubdivEditFormValidator;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class SubdivEditController {
	private static final String COMMAND_NAME = "form";

	@Autowired
	private ContactService contactService;
	@Autowired
	private MessageSource messages;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SubdivEditFormValidator validator;
	@Autowired
	private SubdivSettingsForAllocatedSubdivService subdivSettingsService;
	@Autowired
	private UserService userService;
	@Autowired
	private AuthenticationService authServ;

	@ModelAttribute(COMMAND_NAME)
	private SubdivEditForm initSubdivEditForm(
			@RequestParam(value = "subdivid", required = false, defaultValue = "0") Integer subdivId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> allocatedSubdivDto) {
		Subdiv subdiv = subdivService.get(subdivId);
		SubdivSettingsForAllocatedSubdiv subdivSettings = this.subdivSettingsService.get(subdivId,
				allocatedSubdivDto.getKey());

		SubdivEditForm form = new SubdivEditForm();
		form.setAllocatedSubdivId(allocatedSubdivDto.getKey());
		form.setAnalyticalCenter(subdiv.getAnalyticalCenter());
		form.setSiretNumber(subdiv.getSiretNumber());
		form.setSubdivCode(subdiv.getSubdivCode());
		form.setSubdivId(subdiv.getId());
		form.setSubdivName(subdiv.getSubname());
		form.setFormerId(subdiv.getFormerId());

		if (subdiv.getComp().getCompanyRole() == CompanyRole.BUSINESS) {
			form.setDefaultTurnaround(
					subdiv.getBusinessSettings() != null ? subdiv.getBusinessSettings().getDefaultTurnaround() : null);
		}

		if (subdivSettings != null && subdivSettings.getDefaultBusinessContact() != null) {
			form.setDefaultBusinessContactForAllocatedSubdiv(subdivSettings.getDefaultBusinessContact().getId());
		} else {
			form.setDefaultBusinessContactForAllocatedSubdiv(0);
		}
		
		if (subdiv.getBusinessSettings() != null) {
			form.setDefaultBusinessContact(subdiv.getBusinessSettings().getDefaultBusinessContact().getId());
		} else {
			form.setDefaultBusinessContact(0);
		}

		return form;
	}

	@InitBinder(COMMAND_NAME)
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@RequestMapping(value = "/editsubdiv.htm", method = RequestMethod.POST)
	public String onSubmit(Model model, Locale locale, @Validated @ModelAttribute(COMMAND_NAME) SubdivEditForm sf,
			BindingResult result,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> allocatedSubdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName) throws Exception {
		if (result.hasErrors()) {
			return onRequest(model, locale, sf.getSubdivId(), allocatedSubdivDto, allocatedCompanyDto, userName);
		} else {
			this.subdivService.editSubdiv(sf, allocatedSubdivDto.getKey());
			return "redirect:viewsub.htm?subdivid=" + sf.getSubdivId();
		}
	}

	/**
	 * We use a DTO list here both for performance (1 query) and because the
	 * currently selected contact in settings may no longer be a valid, active
	 * contact in this subdivision.
	 * 
	 * @param subdivId
	 * @param allocatedSubdivId
	 * @param locale
	 * @return
	 */
	private List<ContactKeyValue> getContactDtosForAllocatedSubdiv(Integer subdivId, Integer allocatedSubdivId, Integer allocatedCompanyId, Locale locale) {
		String noneMessage = this.messages.getMessage("company.nonespecified", null, "None Specified", locale);
		ContactKeyValue noneDto = new ContactKeyValue(0, "--- " + noneMessage + " ---");

		List<ContactKeyValue> prependDtos = new ArrayList<>();
		prependDtos.add(noneDto);
		SubdivSettingsForAllocatedSubdiv settings = this.subdivSettingsService.get(subdivId, allocatedSubdivId);
		if (settings != null && settings.getDefaultBusinessContact() != null) {
			Contact contact = settings.getDefaultBusinessContact();
			prependDtos.add(new ContactKeyValue(contact.getId(), contact.getName()));
		}

		return this.contactService.getContactDtoList(allocatedCompanyId, null, true,
				prependDtos.toArray(new ContactKeyValue[prependDtos.size()]));
	}
	
	private List<ContactKeyValue> getContactDtos(Integer subdivId, Integer allocatedSubdivId, Integer allocatedCompanyId, Locale locale) {
		String noneMessage = this.messages.getMessage("company.nonespecified", null, "None Specified", locale);
		ContactKeyValue noneDto = new ContactKeyValue(0, "--- " + noneMessage + " ---");

		List<ContactKeyValue> prependDtos = new ArrayList<>();
		prependDtos.add(noneDto);
		Subdiv subdiv = this.subdivService.get(subdivId);
		if (subdiv.getBusinessSettings() != null) {
			Contact contact = subdiv.getBusinessSettings().getDefaultBusinessContact();
			prependDtos.add(new ContactKeyValue(contact.getId(), contact.getName()));
		}
		
		return this.contactService.getContactDtoList(allocatedCompanyId, null, true,
				prependDtos.toArray(new ContactKeyValue[prependDtos.size()]));
	}

	@RequestMapping(value = "/editsubdiv.htm", method = RequestMethod.GET)
	public String onRequest(Model model, Locale locale,
			@RequestParam(value = "subdivid", required = false, defaultValue = "0") Integer subdivId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> allocatedSubdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName) {

		Subdiv subdiv = subdivService.get(subdivId);
		User currentUser = userService.get(userName);

		model.addAttribute("businessContacts", getContactDtos(subdivId, subdivId, subdiv.getComp().getCoid(), locale));
		model.addAttribute("businessContactsForAllocatedSubdiv", getContactDtosForAllocatedSubdiv(subdivId, allocatedSubdivDto.getKey(), allocatedCompanyDto.getKey(), locale));
		model.addAttribute("subdiv", subdiv);
		model.addAttribute("isAdmin", authServ.hasRight(currentUser.getCon(), Permission.SUBDIV_TURNAROUND_EDIT,
				allocatedSubdivDto.getKey()));

		return "trescal/core/company/subdivedit";
	}
}