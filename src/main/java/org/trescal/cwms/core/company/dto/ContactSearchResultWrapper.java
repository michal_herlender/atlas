package org.trescal.cwms.core.company.dto;

import java.util.Objects;

import org.trescal.cwms.core.company.entity.corole.CompanyRole;

public class ContactSearchResultWrapper {
	private int coid;
	private String coname;
	private String corole;
	private boolean defaultContact;
	private boolean highlight;
	private String name;
	private Integer personid;
	private String subname;

	public ContactSearchResultWrapper() {
	}

	public ContactSearchResultWrapper(int coid, String coname, CompanyRole companyRole, Integer defaultContactId,
			String firstName, String lastName, Integer personid, String subname) {

		this.coid = coid;
		this.coname = coname;
		this.corole = companyRole.toString();

		this.defaultContact = Objects.equals(defaultContactId, personid);
//		if (defaultContactId == personid)
//			this.defaultContact = true;
//		else
//			this.defaultContact = false;

		this.highlight = false;
		this.name = firstName + " " + lastName;
		this.personid = personid;
		this.subname = subname;

	}

	public ContactSearchResultWrapper(Integer personid, String name, int coid, String coname, String corole,
			String subname) {
		this.personid = personid;
		this.name = name;
		this.coid = coid;
		this.coname = coname;
		this.corole = corole;
		this.subname = subname;
		this.highlight = false;
		this.defaultContact = false;
	}

	public ContactSearchResultWrapper(Integer personid, String name, int coid, String coname, CompanyRole companyRole,
			String subname) {
		this(personid, name, coid, coname, companyRole.name(), subname);
	}

	public ContactSearchResultWrapper(Integer personid, String name, int coid, String coname, String corole, String subname,
			String groupname, boolean defaultContact) {
		this.personid = personid;
		this.name = name;
		this.coid = coid;
		this.coname = coname;
		this.corole = corole;
		this.subname = subname;
		this.setHighlight(false);
		this.defaultContact = defaultContact;
		if (groupname.equals("Calibration Officer")) {
			this.setHighlight(true);
		}
	}

	public ContactSearchResultWrapper(Integer personid, String name, int coid, String coname, CompanyRole companyRole,
			String subname, String groupname, boolean defaultContact) {
		this(personid, name, coid, coname, companyRole.name(), subname, groupname, defaultContact);
	}

	public ContactSearchResultWrapper(Integer personid, String firstname, String lastname, int coid, String coname,
			String corole, String subname) {
		this.personid = personid;
		this.name = lastname + ", " + firstname;
		this.coid = coid;
		this.coname = coname;
		this.corole = corole;
		this.subname = subname;
		this.highlight = false;
		this.defaultContact = false;
	}

	public ContactSearchResultWrapper(Integer personid, String firstname, String lastname, int coid, String coname,
			CompanyRole companyRole, String subname) {
		this(personid, firstname, lastname, coid, coname, companyRole.name(), subname);
	}

	public int getCoid() {
		return this.coid;
	}

	public String getConame() {
		return this.coname;
	}

	public String getCorole() {
		return this.corole;
	}

	public String getName() {
		return this.name;
	}

	public int getPersonid() {
		return this.personid;
	}

	public String getSubname() {
		return this.subname;
	}

	public boolean isDefaultContact() {
		return this.defaultContact;
	}

	public boolean isHighlight() {
		return this.highlight;
	}

	public void setCoid(int coid) {
		this.coid = coid;
	}

	public void setConame(String coname) {
		this.coname = coname;
	}

	public void setCorole(String corole) {
		this.corole = corole;
	}

	public void setDefaultContact(boolean defaultContact) {
		this.defaultContact = defaultContact;
	}

	public void setHighlight(boolean highlight) {
		this.highlight = highlight;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPersonid(int personid) {
		this.personid = personid;
	}

	public void setSubname(String subname) {
		this.subname = subname;
	}
}