package org.trescal.cwms.core.company.entity.companylistmember.db;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companylist.db.CompanyListService;
import org.trescal.cwms.core.company.entity.companylistmember.CompanyListMember;
import org.trescal.cwms.core.company.form.EditCompanyListForm;
import org.trescal.cwms.core.login.entity.user.db.UserService;

@Service("CompanyListMemberService")
public class CompanyListMemberServiceImpl extends BaseServiceImpl<CompanyListMember, Integer> implements CompanyListMemberService{

	@Autowired
	private CompanyListMemberDao companyListMemberDao;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private CompanyListService companyListService;
	@Autowired
	private UserService userService;
	
	@Override
	protected BaseDao<CompanyListMember, Integer> getBaseDao() {
		return companyListMemberDao;
	}
	
	@Override
	public List<CompanyListMember> getMembersByCompanyList(Integer companyListId){
		return this.companyListMemberDao.getMembersByCompanyList(companyListId);
	}
	
	@Override
	public List<Integer> getMemberCoidsByCompanyLists(List<Integer> companyListIds){
		return this.companyListMemberDao.getMemberCoidsByCompanyLists(companyListIds);
	}
	
	@Override
	public void createNewCompanyListMember(EditCompanyListForm form, String username){
		CompanyListMember newMember = new CompanyListMember();
		newMember.setActive(form.getActivestatus());
		newMember.setCompany(companyService.get(form.getMemberCoid()));
		newMember.setCompanyList(companyListService.findCompanyList(form.getCompanylistId()));
		newMember.setLastModified(new Timestamp(System.currentTimeMillis()));
		newMember.setLastModifiedBy(userService.get(username).getCon());
		this.companyListMemberDao.persist(newMember);
	}

	@Override
	public boolean remove(Integer companyListMemberId) {
		try {
			CompanyListMember companylistMember = companyListMemberDao.find(companyListMemberId);
			companyListMemberDao.remove(companylistMember);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}
}
