package org.trescal.cwms.core.company.entity.companygroup;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.company.Company;

import lombok.Setter;

@Entity
@Table(name = "companygroup")
@Setter
public class CompanyGroup extends Versioned
{
	private int id;
	private String groupName;
	private Boolean active;	
	private Set<Company> companies;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}	

	
	@NotNull
	@Length(max = 75)
	@Column(name = "groupname", length = 75, nullable=false)
	public String getGroupName() {
		return groupName;
	}

	@NotNull
	@Column(name = "active", columnDefinition="bit", nullable=false)
	public Boolean getActive() {
		return this.active;
	}
	
	@OneToMany(mappedBy = "companyGroup", fetch = FetchType.LAZY)
	public Set<Company> getCompanies() {
		return companies;
	}
	
}
