package org.trescal.cwms.core.company.form;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.db.CountryService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.db.VatRateService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
@Slf4j
public class AddressAddFormValidator extends AbstractBeanValidator
{
	public final static String FIELD_ADDRESS_TYPE = "address.addressType";
	public final static String ERROR_LEGAL_REGISTRATION = "error.legalregistrationcount";
	public final static String MESSAGE_LEGAL_REGISTRATION = "Only one legal registration address is permitted.  This company has {} other legal registration addresses.";

	public final static String FIELD_OVERRIDE_COMPANY_VATRATE = "overrideCompanyVatRate";
	public final static String ERROR_OVERRIDE = "error.vatrate.override";
	public final static String MESSAGE_OVERRIDE = "VAT Rate override required for selected address country";
	
	public final static String FIELD_VATCODE = "overrideVatCode";
	public static final String ERROR_CODE_VAT_RATE = "error.vatrate.invalid";
	public static final String ERROR_MESSAGE_VAT_RATE = "Invalid VAT rate selection";
	
	public final static String FIELD_DUPLICATE_ADDRESS = "address.addr1";
	public static final String ERROR_CODE_DUPLICATE_ADDRESS = "address.duplicate";
	public static final String ERROR_MESSAGE_DUPLICATE_ADDRESS = "There is already another address with the same parameters.";

	@Autowired
	private AddressService addressService; 
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	
	@Autowired
	private CountryService countryService; 
	
	@Autowired
	private SubdivService subdivService;
	
	@Autowired
	private VatRateService vatRateService; 
	
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(AddressAddForm.class);
	}
	

	@Override
	public void validate(Object target, Errors errors)
	{
		super.validate(target, errors);
		AddressAddForm aaf = (AddressAddForm) target;
		Address address = aaf.getAddress();
		Company company = this.subdivService.get(address.getSub().getSubdivid()).getComp();
		validateLegalRegistration(address, company, errors);
		validateTaxRate(aaf, company, errors);
		// add new address case
		if(aaf.getAddressId() == 0) {
			Address addresss = this.addressService.getAddressFromSubdiv(address);
			if(addresss != null) {
				errors.rejectValue(FIELD_DUPLICATE_ADDRESS, ERROR_CODE_DUPLICATE_ADDRESS, new Object[] {}, 
						ERROR_MESSAGE_DUPLICATE_ADDRESS);
			}
		}
	}
	
	/*
	 * Verify that not more than one legal registration address exists for the company.
	 * Note that the address may be a new or detached entity at this point, so the company is provided separately. 
	 */
	public void validateLegalRegistration(Address address, Company company, Errors errors) {
		if (address.getAddressType().contains(AddressType.LEGAL_REGISTRATION)) {
			log.debug("Checking coid " + company.getCoid() + " and addrid " + address.getAddrid());
			Long countOtherAddresses = addressService.countCompanyAddresses(company, AddressType.LEGAL_REGISTRATION, address.getAddrid() != null ? address : null);
			if (countOtherAddresses > 0) {
				errors.rejectValue(FIELD_ADDRESS_TYPE, ERROR_LEGAL_REGISTRATION, new Object[]{countOtherAddresses}, MESSAGE_LEGAL_REGISTRATION);
			}
		}
	}
	/*
	 * 
	 */
	public void validateTaxRate(AddressAddForm aaf, Company company, Errors errors) {
		Country country = this.countryService.get(aaf.getCountryId());
		Company allocatedCompany = this.companyService.get(aaf.getBusinessCompanyId());
		if (aaf.isOverrideCompanyVatRate()) {
			VatRate vatRate = this.vatRateService.get(aaf.getOverrideVatCode());
			if (!vatRateService.isVatRateAllowed(vatRate, country, allocatedCompany.getCountry(), company.getCompanyRole())) {
				errors.rejectValue(FIELD_VATCODE, ERROR_CODE_VAT_RATE , ERROR_MESSAGE_VAT_RATE);
			}
		}
		else {
			CompanySettingsForAllocatedCompany companySettings = this.companySettingsService.getByCompany(company, allocatedCompany);
			VatRate vatRate = companySettings.getVatrate();
			if (!vatRateService.isVatRateAllowed(vatRate, country, allocatedCompany.getCountry(), company.getCompanyRole())) {
				errors.rejectValue(FIELD_OVERRIDE_COMPANY_VATRATE, ERROR_OVERRIDE, MESSAGE_OVERRIDE);
			}
		}
	}
}