package org.trescal.cwms.core.company.entity.companysettings.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.dto.SubdivSettingsForAllocatedSubdivDto;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

import java.util.List;

public interface SubdivSettingsForAllocatedSubdivService extends BaseService<SubdivSettingsForAllocatedSubdiv, Integer> {
	
	SubdivSettingsForAllocatedSubdiv get(Subdiv client, Subdiv business);
	
	SubdivSettingsForAllocatedSubdiv get(Integer subdivId, Integer allocatedSubdivId);

    List<SubdivSettingsForAllocatedSubdivDto> getAllBySubdivId(Integer subdivId);
}