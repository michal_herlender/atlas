package org.trescal.cwms.core.company.entity.vatrate.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.VatRateType;
import org.trescal.cwms.core.company.entity.vatrate.VatRate_;

@Repository("VatRateDao")
public class VatRateDaoImpl extends BaseDaoImpl<VatRate, String> implements VatRateDao
{
	@Override
	public List<VatRate> getAllEagerWithCountry() {
		return super.getResultList(cb -> {
			CriteriaQuery<VatRate> cq = cb.createQuery(VatRate.class);
			Root<VatRate> root = cq.from(VatRate.class);
			root.fetch(VatRate_.country, JoinType.LEFT);
			return cq;
		});
	}
	
	@Override
	public VatRate getDefaultForType(VatRateType type) {
		// Returns only result where country is null, to allow special cases
		return super.getFirstResult(cb -> {
			CriteriaQuery<VatRate> cq = cb.createQuery(VatRate.class);
			Root<VatRate> root = cq.from(VatRate.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(VatRate_.type), type));
			clauses.getExpressions().add(cb.isNull(root.get(VatRate_.country)));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

	@Override
	public VatRate getForCountryAndType(Integer countryId, VatRateType type) {
		// Expected to be used for VatRateType.COUNTRY_DEFAULT only
		return super.getFirstResult(cb -> {
			CriteriaQuery<VatRate> cq = cb.createQuery(VatRate.class);
			Root<VatRate> root = cq.from(VatRate.class);
			Join<VatRate, Country> country = root.join(VatRate_.country, JoinType.LEFT);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(VatRate_.type), type));
			clauses.getExpressions().add(cb.equal(country.get(Country_.countryid), countryId));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

	@Override
	protected Class<VatRate> getEntity() {
		return VatRate.class;
	}
}