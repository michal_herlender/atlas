package org.trescal.cwms.core.company.entity.addressprintformat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Sets up defined formats for addresses by country
 * @author galen
 * Definitions used by message pattern: 
 *  {addr1} addr1
 *  {addr2} addr2
 *  {addr3} addr3
 *  {town} town/city
 *  {regionname} region/state/province
 *  {regioncode} region/state/province code (once implemented, same as region/state/province for now)
 *  {postcode} postcode
 */
public enum AddressPrintFormat {
	DEFAULT(false, "{postcode} {town}"),										// Use where not yet determined/set
	POSTCODE_TOWN(false, "{postcode} {town}"),									// e.g. Most of Europe, Morocco, Tunisia
	POSTCODE_TOWN_COMMA_REGION(false, "{postcode} {town}, {regionname}"),		// e.g. China, Mexico
	POSTCODE_TOWN_REGIONCODE(false, "{postcode} {town} {regioncode}"), 			// e.g. Italy
	POSTCODE_TOWN_NEWLINE_REGION(false, "{postcode} {town}", "{regionname}"),	// e.g. Malaysia
	SPECIAL_SPAIN(false, "{postcode} {town} ({regionname})"),						// Region name in parenthesis
	SPECIAL_BRASIL(false, "{postcode} {town} - {regioncode}"),						// Region code as -suffix
	SPECIAL_HUNGARY(true, "{town}", "{addr1}", "{addr2}", "{addr3}", "{postcode}"),	// Town before address
	SPECIAL_JAPAN(false, "{town}","{postcode}","{regionname}"),						// Separate lines
	TOWN_NEWLINE_POSTCODE(false, "{town}", "{postcode}"),							// e.g. UK, Russia, Ukraine, Kazakh 
	TOWN_POSTCODE(false, "{town} {postcode}"),									// e.g. New Zealand, Thailand, Singapore
	TOWN_REGIONCODE_POSTCODE(false, "{town} {regioncode}  {postcode}"),			// e.g. US, Canada, Australia (extra space intentional)
	TOWN_COMMA_REGION_POSTCODE(false, "{town}, {regionname} {postcode}");		// e.g. Taiwan (region name blank for large cities)
	
	private List<String> formatPatterns;
	
	private AddressPrintFormat(boolean fullFormat, String... formatLines) {
		formatPatterns = new ArrayList<>();
		if (fullFormat) {
			formatPatterns.addAll(Arrays.asList(formatLines));
		}
		else {
			formatPatterns.add("{addr1}");
			formatPatterns.add("{addr2}");
			formatPatterns.add("{addr3}");
			formatPatterns.addAll(Arrays.asList(formatLines));
		}
	}

	public List<String> getFormatPatterns() {
		return formatPatterns;
	}
}