package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Date;

@IntranetController
@RestController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class SubdivSettingsController {

    @Autowired
    private SubdivService subdivService;

    @GetMapping(value = "/changesubdivactive.json")
    public Date changeActive(@RequestParam(value = "subdivid") Integer subdivId,
                             @RequestParam(value = "activate") Boolean activate,
                             @RequestParam(value = "reason") String reason,
                             @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
        Subdiv subdiv = subdivService.get(subdivId);
        Date date = new Date();
        subdiv.setActive(activate);
        subdiv.setDeactReason(reason);
        subdiv.setDeactTime(date);
        subdivService.merge(subdiv);
        return date;
    }
}