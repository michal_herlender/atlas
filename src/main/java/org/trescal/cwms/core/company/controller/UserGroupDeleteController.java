package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;
import org.trescal.cwms.core.login.entity.portal.usergroup.db.UserGroupService;

@Controller @IntranetController
public class UserGroupDeleteController {
	@Autowired
	private UserGroupService ugServ;

	@RequestMapping("/deletegroup.htm")
	public RedirectView handleRequest(@RequestParam(value="groupid") int groupid) {
		UserGroup group = (UserGroup) ugServ.findGroup(groupid);
		Company company = group.getCompany();
		ugServ.deleteUserGroup(group);
		return new RedirectView("/viewcomp.htm?coid="+company.getCoid()+"&loadtab=usergroups-tab", true);
	}
}
