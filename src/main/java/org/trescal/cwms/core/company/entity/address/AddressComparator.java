package org.trescal.cwms.core.company.entity.address;

import java.util.Comparator;

/**
 * Inner comparator class for sorting {@link Address}es when returning
 * collections
 */
public class AddressComparator implements Comparator<Address>
{
	public int compare(final Address address, final Address anotherAddress)
	{
		if ((address.getAddr1() == null) || address.getAddr1().equals(anotherAddress.getAddr1())) {
			Integer id1 = address.getAddrid();
			Integer id2 = anotherAddress.getAddrid();
			return id1.compareTo(id2);
		}
		else return address.getAddr1().compareTo(anotherAddress.getAddr1());
	}
}