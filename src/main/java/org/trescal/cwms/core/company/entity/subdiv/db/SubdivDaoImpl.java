package org.trescal.cwms.core.company.entity.subdiv.db;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.dto.SubdivSearchResultWrapper;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.company.projection.SubdivProjectionDTO;
import org.trescal.cwms.core.system.Constants;

@Repository("SubdivDao")
public class SubdivDaoImpl extends BaseDaoImpl<Subdiv, Integer> implements SubdivDao {

	@Override
	protected Class<Subdiv> getEntity() {
		return Subdiv.class;
	}

	@Override
	public Long countCompanySubdivsByName(Integer coid, String subdivName, Integer excludeSubdivId) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Subdiv> subdiv = cq.from(Subdiv.class);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Predicate clauses = cb.conjunction();
			if (coid != null) {
				clauses.getExpressions().add(cb.equal(company.get(Company_.coid), coid));
			}
			if (subdivName != null) {
				clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subname), subdivName));
			}
			if (excludeSubdivId != null) {
				clauses.getExpressions().add(cb.notEqual(subdiv.get(Subdiv_.subdivid), excludeSubdivId));
			}
			cq.select(cb.count(subdiv)).where(clauses);
			return cq;
		});
	}

	@Override
	public List<SubdivSearchResultWrapper> getAllActiveByCompany(int coid) {
		return getResultList(cb -> {
			CriteriaQuery<SubdivSearchResultWrapper> cq = cb.createQuery(SubdivSearchResultWrapper.class);
			Root<Subdiv> subdiv = cq.from(Subdiv.class);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(company.get(Company_.coid), coid));
			clauses.getExpressions().add(cb.isTrue(subdiv.get(Subdiv_.active)));
			cq.where(clauses);
			cq.orderBy(cb.asc(subdiv.get(Subdiv_.subname)));
			cq.select(cb.construct(SubdivSearchResultWrapper.class, subdiv.get(Subdiv_.subdivid),
					subdiv.get(Subdiv_.subname)));
			return cq;
		});
	}

	@Override
	public List<Subdiv> getAllActiveSubdivsByRole(CompanyRole companyRole) {
		return getResultList(cb -> {
			CriteriaQuery<Subdiv> cq = cb.createQuery(Subdiv.class);
			Root<Subdiv> subdiv = cq.from(Subdiv.class);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(company.get(Company_.companyRole), companyRole));
			clauses.getExpressions().add(cb.isTrue(subdiv.get(Subdiv_.active)));
			cq.where(clauses);
			cq.orderBy(cb.asc(subdiv.get(Subdiv_.subname)));
			return cq;
		});
	}

	@Override
	public List<SubdivKeyValue> searchBusinessSubdivsByName(String partialSubdivName) {
		return getResultList(cb -> {
			CriteriaQuery<SubdivKeyValue> cq = cb.createQuery(SubdivKeyValue.class);
			Root<Subdiv> subdiv = cq.from(Subdiv.class);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(company.get(Company_.companyRole), CompanyRole.BUSINESS));
			clauses.getExpressions().add(cb.like(subdiv.get(Subdiv_.subname), partialSubdivName + "%"));
			clauses.getExpressions().add(cb.isTrue(subdiv.get(Subdiv_.active)));
			cq.where(clauses);
			cq.orderBy(cb.asc(subdiv.get(Subdiv_.subname)));
			cq.select(cb.construct(SubdivKeyValue.class, subdiv.get(Subdiv_.subdivid), cb.concat(
					subdiv.get(Subdiv_.subname), cb.concat(" (", cb.concat(company.get(Company_.coname), ")")))));
			return cq;
		}, 0, Constants.RESULTS_PER_PAGE);
	}

	@Override
	public Long countBySiretNumber(String siretNumber, Set<CompanyRole> companyRoles, Integer excludeSubdivId) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Subdiv> root = cq.from(Subdiv.class);
			Join<Subdiv, Company> company = root.join(Subdiv_.comp);

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(Subdiv_.siretNumber), siretNumber));
			clauses.getExpressions().add(company.get(Company_.companyRole).in(companyRoles));

			cq.select(cb.count(root));
			cq.where(clauses);
			return cq;
		});
	}

	@Override
	public List<SubdivKeyValue> getAllSubdivFromCompany(int coid, Boolean isActive) {
		return getResultList(cb -> {
			CriteriaQuery<SubdivKeyValue> cq = cb.createQuery(SubdivKeyValue.class);
			Root<Subdiv> subdiv = cq.from(Subdiv.class);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(company.get(Company_.coid), coid));
			if (isActive != null)
				clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.active), isActive));
			cq.where(clauses);
			cq.orderBy(cb.asc(subdiv.get(Subdiv_.subname)));
			cq.select(cb.construct(SubdivKeyValue.class, subdiv.get(Subdiv_.subdivid), subdiv.get(Subdiv_.subname)));
			return cq;
		});
	}

	/*
	 * public SubdivProjectionDTO(Integer subdivid, String subname, Boolean
	 * subdivActive, Integer coid, String coname, Boolean companyActive, Boolean
	 * onstop) {
	 */

	@Override
	public List<SubdivProjectionDTO> getSubdivProjectionDTOs(Collection<Integer> sourceSubdivIds,
			Integer allocatedCompanyId) {
		return getResultList(cb -> {
			CriteriaQuery<SubdivProjectionDTO> cq = cb.createQuery(SubdivProjectionDTO.class);
			Root<Subdiv> subdiv = cq.from(Subdiv.class);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp, JoinType.INNER);
			Join<Company, CompanySettingsForAllocatedCompany> settings = company
					.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			Join<CompanySettingsForAllocatedCompany, Company> settingsCompany = settings
					.join(CompanySettingsForAllocatedCompany_.organisation.getName(), JoinType.LEFT);
			settingsCompany.on(cb.equal(settingsCompany.get(Company_.coid), allocatedCompanyId));

			cq.where(subdiv.get(Subdiv_.subdivid).in(sourceSubdivIds));
			cq.distinct(true); // Possible in data model to have multiple
								// CompanySettings (should make a constraint)

			cq.select(cb.construct(SubdivProjectionDTO.class, subdiv.get(Subdiv_.subdivid), subdiv.get(Subdiv_.subname),
					subdiv.get(Subdiv_.active), company.get(Company_.coid), company.get(Company_.coname),
					settings.get(CompanySettingsForAllocatedCompany_.active),
					settings.get(CompanySettingsForAllocatedCompany_.onStop)));
			return cq;
		});
	}

	@Override
	public List<SubdivProjectionDTO> getSubdivProjectionDTOsByCompany(Integer allocatedCompanyId) {
		return getResultList(cb -> {
			CriteriaQuery<SubdivProjectionDTO> cq = cb.createQuery(SubdivProjectionDTO.class);
			Root<Subdiv> subdiv = cq.from(Subdiv.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.comp).get(Company_.coid), allocatedCompanyId));
			cq.where(clauses);
			cq.orderBy(cb.asc(subdiv.get(Subdiv_.subname)));
			cq.select(
					cb.construct(SubdivProjectionDTO.class, subdiv.get(Subdiv_.subdivid), subdiv.get(Subdiv_.subname)));
			return cq;
		});
	}

}