package org.trescal.cwms.core.company.dto;

import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionEntity;

public class InstructionLinkDTO {
	private Instruction instruction;
	private InstructionEntity instructionEntity;
	private int entityid;
	private int linkid;
	private String entityName;
	private boolean globalInstruction;
	private boolean isSubdivInstruction;
	private String businessSubdivName;

	public InstructionLinkDTO(CompanyInstructionLink instructionLink) {
		this.instruction = instructionLink.getInstruction();
		this.instructionEntity = instructionLink.getInstructionEntity();
		this.entityid = instructionLink.getEntity().getCoid();
		this.linkid = instructionLink.getId();
		this.entityName = instructionLink.getEntity().getConame();
		this.globalInstruction = (instructionLink.getOrganisation() == null);
		this.isSubdivInstruction = instructionLink.isSubdivInstruction();
		if (instructionLink.getBusinessSubdiv() != null)
			this.businessSubdivName = instructionLink.getBusinessSubdiv().getSubname();
	}

	public InstructionLinkDTO(SubdivInstructionLink instructionLink) {
		this.instruction = instructionLink.getInstruction();
		this.instructionEntity = instructionLink.getInstructionEntity();
		this.entityid = instructionLink.getEntity().getSubdivid();
		this.linkid = instructionLink.getId();
		this.entityName = instructionLink.getEntity().getSubname();
		this.globalInstruction = (instructionLink.getOrganisation() == null);
		this.isSubdivInstruction = instructionLink.isSubdivInstruction();
		if (instructionLink.getBusinessSubdiv() != null)
			this.businessSubdivName = instructionLink.getBusinessSubdiv().getSubname();
	}

	public InstructionLinkDTO(ContactInstructionLink instructionLink) {
		this.instruction = instructionLink.getInstruction();
		this.instructionEntity = instructionLink.getInstructionEntity();
		this.entityid = instructionLink.getEntity().getPersonid();
		this.linkid = instructionLink.getId();
		this.entityName = instructionLink.getEntity().getName();
		this.globalInstruction = (instructionLink.getOrganisation() == null);
	}

	public Instruction getInstruction() {
		return instruction;
	}

	public InstructionEntity getInstructionEntity() {
		return instructionEntity;
	}

	public int getEntityid() {
		return entityid;
	}

	public String getEntityName() {
		return entityName;
	}

	public int getLinkid() {
		return linkid;
	}

	public boolean isGlobalInstruction() {
		return globalInstruction;
	}

	public boolean isSubdivInstruction() {
		return isSubdivInstruction;
	}

	public void setSubdivInstruction(boolean isSubdivInstruction) {
		this.isSubdivInstruction = isSubdivInstruction;
	}

	public String getBusinessSubdivName() {
		return businessSubdivName;
	}

	public void setBusinessSubdivName(String businessSubdivName) {
		this.businessSubdivName = businessSubdivName;
	}

}
