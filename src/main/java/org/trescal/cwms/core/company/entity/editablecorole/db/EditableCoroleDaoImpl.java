package org.trescal.cwms.core.company.entity.editablecorole.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.corole.Corole;
import org.trescal.cwms.core.company.entity.editablecorole.EditableCorole;

@Repository("EditableCoroleDao")
public class EditableCoroleDaoImpl extends BaseDaoImpl<EditableCorole, Integer> implements EditableCoroleDao {
	
	@Override
	protected Class<EditableCorole> getEntity() {
		return EditableCorole.class;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<EditableCorole> findAllEditableCorolesForRole(Corole role) {
		Criteria crit = getSession().createCriteria(EditableCorole.class);
		crit.createCriteria("editableFrom").add(Restrictions.idEq(role.getCoroleid()));
		return (List<EditableCorole>) crit.list();
	}
}