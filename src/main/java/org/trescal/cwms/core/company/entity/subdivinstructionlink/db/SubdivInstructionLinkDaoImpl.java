package org.trescal.cwms.core.company.entity.subdivinstructionlink.db;

import java.util.Arrays;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink_;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instruction.Instruction_;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

@Repository
public class SubdivInstructionLinkDaoImpl extends BaseDaoImpl<SubdivInstructionLink, Integer>
		implements SubdivInstructionLinkDao {

	@Override
	protected Class<SubdivInstructionLink> getEntity() {
		return SubdivInstructionLink.class;
	}

	@Override
	public List<SubdivInstructionLink> getForSubdivAndTypes(Subdiv subdiv, Subdiv allocatedSubdiv,
			InstructionType... types) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<SubdivInstructionLink> cq = cb.createQuery(SubdivInstructionLink.class);
		Root<SubdivInstructionLink> root = cq.from(SubdivInstructionLink.class);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(root.get(SubdivInstructionLink_.subdiv), subdiv));
		clauses.getExpressions()
				.add(cb.equal(root.get(SubdivInstructionLink_.organisation), allocatedSubdiv.getComp()));
		Predicate disjunction = cb.disjunction();
		disjunction.getExpressions().add(cb.equal(root.get(SubdivInstructionLink_.businessSubdiv), allocatedSubdiv));
		disjunction.getExpressions().add(cb.isFalse(root.get(SubdivInstructionLink_.subdivInstruction)));
		clauses.getExpressions().add(disjunction);
		if (types.length > 0) {
			Join<SubdivInstructionLink, Instruction> instruction = root.join(SubdivInstructionLink_.instruction);
			clauses.getExpressions().add(instruction.get(Instruction_.instructiontype).in(Arrays.asList(types)));
		}
		cq.where(clauses);
		cq.select(root);
		return getEntityManager().createQuery(cq).getResultList();
	}
}