package org.trescal.cwms.core.company.entity.company.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.dto.CompanyDTO;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.dto.CompanySearchResultWrapper;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.projection.CompanyProjectionDTO;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface CompanyDao extends BaseDao<Company, Integer> {

	void detach(Company company);

	List<CompanyKeyValue> findCustomerCompanies(Contact contact, Scope scope, Boolean active);

	List<CompanyKeyValue> findCustomerCompaniesBySubdiv(Subdiv subdiv, Boolean active);

	List<Company> getCompaniesByRole(CompanyRole companyRole);

	Boolean existsByName(String name, CompanyRole companyRole, Company excludeCompany);

	Boolean existsByLegalIdentifier(String legalIdentifier, CompanyRole companyRole, Company excludeCompany);

	List<CompanySearchResultWrapper> searchByCompanyRoles(String searchString, List<CompanyRole> roles,
                                                          Company allocatedCompany, Boolean active, Boolean anywhereActive, Boolean searchName, Boolean searchEverywhere);

	List<CompanySearchResultWrapper> searchByCompanyRoles(String searchName, boolean active, List<CompanyRole> roles,
			boolean includeDeactivatedCompanies, Company allocatedCompany);

	List<CompanyKeyValue> getCompaniesFromSubdivs(List<Integer> subdivs);
	
	List<CompanyProjectionDTO> getDTOList(Collection<Integer> companyIds, Integer allocatedCompanyId);

	CompanyDTO getForLinkInfo(Integer companyId, Integer allocatedCompanyId);
	
	List<KeyValueIntegerString> getCompaniesByRoleForAllocatedCompany(List<CompanyRole> roles, Company allocatedCompany,
			List<Integer> companiesIdsAlreadyExist);


	

}