package org.trescal.cwms.core.company.entity.address.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.dto.AddrSearchResultWrapper;
import org.trescal.cwms.core.company.dto.AddressKeyValue;
import org.trescal.cwms.core.company.dto.AddressPlantillasDTO;
import org.trescal.cwms.core.company.dto.AddressWithTypeDTO;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.AddressComparator;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.addresssettings.db.AddressSettingsForAllocatedSubdivService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.projection.AddressProjectionDTO;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.SystemTime;
import org.trescal.cwms.rest.tlm.dto.TLMAddressProjectionDTO;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service("addressService")
public class AddressServiceImpl extends BaseServiceImpl<Address, Integer> implements AddressService {

	@Autowired
	private AddressDao addDao;
	@Autowired
	private AddressSettingsForAllocatedSubdivService addressSettingsForAllocatedSubdivService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private TransportOptionService transportOptionService;

	private final static Logger logger = LoggerFactory.getLogger(AddressServiceImpl.class);

	@Override
	protected BaseDao<Address, Integer> getBaseDao() {
		return addDao;
	}

	@Override
	public Integer checkAddressDeactivationStatus(int addrid) {
		// get a count of this addresses instruments
		return this.countAddressInstruments(addrid);
	}

	@Override
	public int countAddressInstruments(int addrid) {
		return this.addDao.countAddressInstruments(addrid);
	}

	@Override
	public Long countCompanyAddresses(Company company, AddressType type, Address excludeAddress) {
		return this.addDao.countCompanyAddresses(company, type, excludeAddress);
	}

	private String getDisplayLine(AddrSearchResultWrapper addr) {
		StringBuilder message = new StringBuilder();
		message.append(addr.getAddr1());
		if (!addr.getAddr2().isEmpty())
			message.append(", ");
		message.append(addr.getAddr2());
		if (!addr.getTown().isEmpty())
			message.append(", ");
		message.append(addr.getTown());
		if (!addr.getCounty().isEmpty())
			message.append(", ");
		message.append(addr.getCounty());
		return message.toString();
	}

	/**
	 * Allows external creation of DTO for existing selected addresses using entity
	 * Consider moving both getDisplayLine methods to AddressPrintFormatter as
	 * "single line" format methods
	 * 
	 * @return String containing address format
	 */
	@Override
	public String getDisplayLine(Address addr) {
		StringBuilder message = new StringBuilder();
		message.append(addr.getAddr1());
		if (!addr.getAddr2().isEmpty())
			message.append(", ");
		message.append(addr.getAddr2());
		if (!addr.getTown().isEmpty())
			message.append(", ");
		message.append(addr.getTown());
		if (!addr.getCounty().isEmpty())
			message.append(", ");
		message.append(addr.getCounty());
		return message.toString();
	}

	@Override
	public List<KeyValue<Integer, String>> getActiveUserRoleAddressesKeyValue(String username, boolean prependNoneDto) {
		List<KeyValue<Integer, String>> result = new ArrayList<>();
		if (prependNoneDto) {
			Locale locale = LocaleContextHolder.getLocale();
			String noneMessage = this.messageSource.getMessage("company.none", null, "None", locale);
			result.add(new KeyValue<>(0, noneMessage));
		}
		List<AddrSearchResultWrapper> searchResult = this.addDao.getActiveUserRoleAddresses(username);
		for (AddrSearchResultWrapper addr : searchResult) {
			result.add(new KeyValue<>(addr.getAddrid(), getDisplayLine(addr)));
		}
		return result;
	}

	public List<Integer> getAddressIdsForContactInstruments(Integer contactId) {
		return addDao.getAddressIdsForContactInstruments(contactId);
	}

	/**
	 * Returns formatted addresses for the specified ids using a projection
	 */
	public List<KeyValue<Integer, String>> getAddressesKeyValue(Collection<Integer> addressIds) {
		List<AddrSearchResultWrapper> searchResult = this.addDao.getAddressesFromIds(addressIds);
		return searchResult.stream().
				map(addr -> new KeyValue<>(addr.getAddrid(), getDisplayLine(addr))).
				collect(Collectors.toList());
	}

	@Override
	public List<Address> getAllActiveSubdivAddresses(Subdiv subdiv, AddressType type) {
		return subdiv != null && subdiv.getAddresses() != null && !subdiv.getAddresses().isEmpty() ? subdiv
				.getAddresses().stream()
				.filter(addr -> addr.getActive()
						&& (type == null || type.equals(AddressType.WITHOUT) || addr.getAddressType().contains(type)))
				.sorted(new AddressComparator()).collect(Collectors.toList()) : new ArrayList<>();
	}

	@Override
	public List<AddrSearchResultWrapper> getAllActiveSubdivAddressesHQL(int subdivid, String typeString) {
		/*
		 * Some DWR calls use a "" to return all addresses which is interpreted here
		 * AddressType.WITHOUT is treated as a special "any" case inside the Dao search
		 * accordingly
		 */
		AddressType type = AddressType.WITHOUT;
		if (typeString.length() > 0)
			type = AddressType.valueOf(typeString.toUpperCase());
		return this.addDao.getActiveAddressesProjection(null, subdivid, type);
	}

	@Override
	public List<KeyValueIntegerString> getAddressDtoList(Integer coid, Integer subdivid, AddressType type,
			KeyValueIntegerString... prependDtos) {
		List<KeyValueIntegerString> result = new ArrayList<>();
		List<AddrSearchResultWrapper> queryResult = this.addDao.getActiveAddressesProjection(coid, subdivid, type);
		if (prependDtos.length > 0) {
			Set<Integer> ids = queryResult.stream().map(AddrSearchResultWrapper::getAddrid).collect(Collectors.toSet());
			for (KeyValueIntegerString dto : prependDtos) {
				// Important - we only prepend the DTO if its ID is not already in the query
				// results
				// this lets us handle potentially inactive / not present default values
				// and show them properly to the user
				if (!ids.contains(dto.getKey())) {
					result.add(dto);
				}
			}
		}
		for (AddrSearchResultWrapper addr : queryResult) {
			result.add(new KeyValueIntegerString(addr.getAddrid(), getDisplayLine(addr)));
		}
		return result;
	}

	@Override
	public List<KeyValue<Integer, String>> getAllActiveSubdivAddressesKeyValue(int subdivid, AddressType type,
			boolean prependNoneDto) {
		List<KeyValue<Integer, String>> result = new ArrayList<>();
		if (prependNoneDto) {
			Locale locale = LocaleContextHolder.getLocale();
			String noneMessage = this.messageSource.getMessage("company.none", null, "None", locale);
			result.add(new KeyValue<>(0, noneMessage));
		}
		if (subdivid != 0) {
			List<AddrSearchResultWrapper> searchResult = this.addDao.getActiveAddressesProjection(null, subdivid, type);
			logger.debug(searchResult.size() + " addresses for subdivid " + subdivid);
			for (AddrSearchResultWrapper addr : searchResult) {
				result.add(new KeyValue<>(addr.getAddrid(), getDisplayLine(addr)));
			}
		}
		return result;
	}

	public List<AddressProjectionDTO> getAddressProjectionDTOs(Collection<Integer> addressIds,
			Integer allocatedCompanyId) {
		List<AddressProjectionDTO> result = Collections.emptyList();
		if (!addressIds.isEmpty()) {
			result = this.addDao.getAddressProjectionDTOs(addressIds, allocatedCompanyId);
		}
		return result;
	}

	@Override
	public PagedResultSet<TLMAddressProjectionDTO> getAllCompanyAddresses(Company company, List<CompanyRole> roles,
			Timestamp companyLastModifiedDate, Timestamp subdivLastModifiedDate, Timestamp addressLastModifiedDate,
			int resultsPerPage, int page) {
		return this.addDao.getAllCompanyAddresses(company, roles, companyLastModifiedDate, subdivLastModifiedDate,
				addressLastModifiedDate, resultsPerPage, page);
	}

	@Override
	public TLMAddressProjectionDTO getAddresseById(int addrid) {
		return this.addDao.getAllCompanyAddresses(addrid);
	}

	@Override
	public List<Address> getAllCompanyAddresses(int coid, AddressType type, boolean activeOnly) {
		return this.addDao.getAllCompanyAddresses(coid, type, activeOnly);
	}

	@Override
	public List<Address> getAllSubdivAddresses(int subdivid, AddressType type, Boolean active) {
		return this.addDao.getAllSubdivAddresses(subdivid, type, active);
	}

	@Override
	public List<Address> getBusinessAddresses(AddressType type, boolean activeOnly) {
		return this.addDao.getBusinessAddresses(type, activeOnly);
	}

	/*
	 * Called internally but exposed for unit testing. Consider moving to
	 * TransportOptionService with DWR changes.
	 */
	@Override
	public List<String> getNextNCollectionDates(TransportOption transportOption, int n) {
		List<String> results = new ArrayList<>();
		if (transportOption != null) {
			Integer collectionDay = transportOption.getDayOfWeek();
			if (collectionDay != null) {
				int day = collectionDay;
				Calendar cal = SystemTime.nowCal();
				// get next occurrence of (valid) collection day
				int count = 0;
				while ((cal.get(Calendar.DAY_OF_WEEK) != day) || (count < 1)) {
					cal.add(Calendar.DATE, 1);
					count++;
				}
				// if collection day is only 1 day in the future make sure the
				// time
				// is before noon, otherwise don't allow this as an option
				if (count < 2 && cal.get(Calendar.AM_PM) == Calendar.PM)
					cal.add(Calendar.DATE, 7);
				// add one day per week for next n weeks
				Locale locale = LocaleContextHolder.getLocale();
				SimpleDateFormat pattern = new SimpleDateFormat("EEE dd MMM yyyy", locale);
				for (int i = 0; i < n; i++, cal.add(Calendar.DATE, 7))
					results.add(pattern.format(cal.getTime()));
			}
		}
		return results;
	}

	@Override
	public AddressPlantillasDTO getPlantillasAddress(int allocatedCompanyId, int addressId) {
		Company allocatedCompany = companyService.get(allocatedCompanyId);
		return this.addDao.getPlantillasAddress(addressId, allocatedCompany);
	}

	@Override
	public PagedResultSet<AddressPlantillasDTO> getPlantillasAddresses(int allocatedCompanyId,
			Date afterLastModifiedDate, Integer formerCustomerId, int businessSubdivId, int resultsPerPage,
			int currentPage) {
		Company allocatedCompany = companyService.get(allocatedCompanyId);
		return this.addDao.getPlantillasAddresses(allocatedCompany, afterLastModifiedDate, formerCustomerId,
				businessSubdivId, resultsPerPage, currentPage);
	}

	/*
	 * DWR call from client web site - consider moving to a TransportOption JSON
	 * controller.
	 */
	@Override
	public TransportOption getTransportOptionIn(int addressid, int businessSubdivId) {
		Address address = this.get(addressid);
		Subdiv businessSubdiv = this.subdivService.get(businessSubdivId);
		AddressSettingsForAllocatedSubdiv settings = this.addressSettingsForAllocatedSubdivService.getBySubdiv(address,
				businessSubdiv);

		if ((settings != null) && (settings.getTransportIn() != null)) {
			return this.transportOptionService.getEager(settings.getTransportIn().getId());
		} else {
			return null;
		}
	}

	/*
	 * DWR call from client web site - consider moving to a TransportOption JSON
	 * controller.
	 */
	@Override
	public List<String> webGetNextNCollectionDates(int addrId, int businessSubdivId, int n) {
		Address address = this.get(addrId);
		Subdiv businessSubdiv = this.subdivService.get(businessSubdivId);
		AddressSettingsForAllocatedSubdiv settings = this.addressSettingsForAllocatedSubdivService.getBySubdiv(address,
				businessSubdiv);
		if ((settings != null) && (settings.getTransportIn() != null)) {
			return this.getNextNCollectionDates(settings.getTransportIn(), n);
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public Address findCompanyAddress(int coid, AddressType type, boolean activeOnly, String address) {
		return addDao.findCompanyAddress(coid, type, activeOnly, address);
	}

	@Override
	public List<AddressKeyValue> getForCompany(Integer companyId) {
		return addDao.getForCompany(companyId).stream().collect(Collectors.groupingBy(AddressWithTypeDTO::getId))
				.entrySet().stream().map(entry -> {
					AddressWithTypeDTO first = entry.getValue().get(0);
					String address = first.getAddress() + ", " + first.getTown();
					if (first.getType() != null) {
						address += " [" + first.getType().getType();
						for (int i = 1; i < entry.getValue().size(); i++)
							address += "," + entry.getValue().get(i).getType().getType();
						address += "]";
					}
					return new AddressKeyValue(entry.getKey(), address);
				}).collect(Collectors.toList());
	}

	@Override
	public Address getAddressFromSubdiv(Address address) {
		return this.addDao.getAddressFromSubdiv(address);
	}
}