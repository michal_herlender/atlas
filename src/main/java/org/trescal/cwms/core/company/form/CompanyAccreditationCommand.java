package org.trescal.cwms.core.company.form;

import java.util.List;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.supplier.Supplier;

public class CompanyAccreditationCommand {
	private Company company;
	private Company businessCompany;
	private List<Supplier> suppliers;
	public Company getCompany() {
		return company;
	}
	public Company getBusinessCompany() {
		return businessCompany;
	}
	public List<Supplier> getSuppliers() {
		return suppliers;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public void setBusinessCompany(Company businessCompany) {
		this.businessCompany = businessCompany;
	}
	public void setSuppliers(List<Supplier> suppliers) {
		this.suppliers = suppliers;
	}
}
