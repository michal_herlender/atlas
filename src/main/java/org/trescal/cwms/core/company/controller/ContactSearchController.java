package org.trescal.cwms.core.company.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.company.dto.ContactSearchResultWrapper;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller @IntranetController
public class ContactSearchController
{
	protected final Log logger = LogFactory.getLog(ContactSearchController.class);
	
	@Autowired
	private ContactService contactService;
	
	@ModelAttribute("companyRoles")
	public CompanyRole[] getCompanyRoles() {
		return CompanyRole.values();
	}
	
	@RequestMapping(value="/contactsearch.htm", method=RequestMethod.GET)
	public String handleRequest() throws Exception
	{
		return "trescal/core/company/contactsearch";
	}
	
	@RequestMapping(value="/contactsearchrequest.json", method=RequestMethod.GET)
	@ResponseBody
	public List<ContactSearchResultWrapper> getContacts(
			@RequestParam(value="firstName", required=false, defaultValue="") String firstName,
			@RequestParam(value="lastName", required=false, defaultValue="") String lastName,
			@RequestParam(value="active", required=false, defaultValue="true") Boolean active,
			@RequestParam(value="companyRoles[]", required=false, defaultValue="") List<CompanyRole> companyRoles)
	{
		List<ContactSearchResultWrapper> results = contactService.searchByCompanyRoles(firstName, lastName, active, companyRoles);
		return results;
	}
}