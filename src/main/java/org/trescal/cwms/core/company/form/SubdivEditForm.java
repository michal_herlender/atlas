package org.trescal.cwms.core.company.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class SubdivEditForm {
	private Integer subdivId;
	private Integer allocatedSubdivId;		// Required for validation
	private String analyticalCenter;
	private Integer defaultBusinessContactForAllocatedSubdiv;
	private Integer defaultBusinessContact;
	private Integer defaultTurnaround;
	private String subdivName;
	private String subdivCode;
	private String siretNumber;
	private String formerId;

	public SubdivEditForm() {
	}

	@NotNull
	public Integer getSubdivId() {
		return subdivId;
	}

	public void setSubdivId(Integer subdivId) {
		this.subdivId = subdivId;
	}

	@NotNull
	public Integer getAllocatedSubdivId() {
		return allocatedSubdivId;
	}

	public void setAllocatedSubdivId(Integer allocatedSubdivId) {
		this.allocatedSubdivId = allocatedSubdivId;
	}

	public String getAnalyticalCenter() {
		return analyticalCenter;
	}

	public void setAnalyticalCenter(String analyticalCenter) {
		this.analyticalCenter = analyticalCenter;
	}

	@NotNull
	public Integer getDefaultBusinessContact() {
		return defaultBusinessContact;
	}

	public void setDefaultBusinessContact(Integer defaultBusinessContact) {
		this.defaultBusinessContact = defaultBusinessContact;
	}

	@NotNull
	public Integer getDefaultBusinessContactForAllocatedSubdiv() {
		return defaultBusinessContactForAllocatedSubdiv;
	}

	public void setDefaultBusinessContactForAllocatedSubdiv(Integer defaultBusinessContactForAllocatedSubdiv) {
		this.defaultBusinessContactForAllocatedSubdiv = defaultBusinessContactForAllocatedSubdiv;
	}

	public Integer getDefaultTurnaround() {
		return defaultTurnaround;
	}

	public void setDefaultTurnaround(Integer defaultTurnaround) {
		this.defaultTurnaround = defaultTurnaround;
	}

	@NotNull
	@Size(min = 1, max = 75)
	public String getSubdivName() {
		return subdivName;
	}

	public void setSubdivName(String subdivName) {
		this.subdivName = subdivName;
	}

	@Size(max = 3)
	public String getSubdivCode() {
		return subdivCode;
	}

	public void setSubdivCode(String subdivCode) {
		this.subdivCode = subdivCode;
	}

	@Size(max = 14)
	public String getSiretNumber() {
		return siretNumber;
	}

	public void setSiretNumber(String siretNumber) {
		this.siretNumber = siretNumber;
	}

	public String getFormerId() {
		return formerId;
	}

	public void setFormerId(String formerId) {
		this.formerId = formerId;
	}
	
	
}