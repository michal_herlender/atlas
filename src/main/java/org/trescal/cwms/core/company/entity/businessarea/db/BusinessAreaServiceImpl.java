package org.trescal.cwms.core.company.entity.businessarea.db;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.businessarea.BusinessArea;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Service("businessAreaService")
public class BusinessAreaServiceImpl extends BaseServiceImpl<BusinessArea, Integer> implements BusinessAreaService {

	@Autowired
	private BusinessAreaDao businessAreaDao;

	@Override
	protected BaseDao<BusinessArea, Integer> getBaseDao() {
		return businessAreaDao;
	}

	@Override
	public List<KeyValueIntegerString> getAllTranslated(Locale locale) {
		return businessAreaDao.getAllTranslated(locale);
	}
}