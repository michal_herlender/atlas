package org.trescal.cwms.core.company.dto;

import org.trescal.cwms.core.company.entity.address.Address;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AddressDTO {
	
	private Integer id;
	private String line1;
	private String line2;
	private String line3;
	private String city;
	private String telephoneNo;
	private String countryCode;
	private String postCode;
	private String county;
	private Boolean typeDefault;
	private Boolean typeDelivery;
	private Boolean typeInvoice;
	private Boolean typeLegal;
	
	public AddressDTO(String line1, String line2, String line3, String city, String telephoneNo, String countryCode,
			String postCode, String county, Boolean typeDefault, Long isDeliveryType, Long isInvoiceType, Long isLegalType) {
		super();
		this.line1 = line1;
		this.line2 = line2;
		this.line3 = line3;
		this.city = city;
		this.telephoneNo = telephoneNo;
		this.countryCode = countryCode;
		this.postCode = postCode;
		this.county = county;
		this.typeDefault = typeDefault;
		this.typeDelivery = isDeliveryType == 1;
		this.typeInvoice = isInvoiceType == 1;
		this.typeLegal = isLegalType == 1;
	}
	
	public AddressDTO(Address address) {
		this.id = address.getAddrid();
		this.line1 = address.getAddr1();
		this.city = address.getTown();
	}
}
