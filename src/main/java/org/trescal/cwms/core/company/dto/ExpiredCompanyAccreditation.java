package org.trescal.cwms.core.company.dto;

import java.util.Set;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditation;

public class ExpiredCompanyAccreditation
{
	Set<CompanyAccreditation> accreditations;
	boolean emailed;
	Company company;

	public ExpiredCompanyAccreditation()
	{
	}

	public ExpiredCompanyAccreditation(Set<CompanyAccreditation> accreditations, boolean emailed, Company company)
	{
		super();
		this.accreditations = accreditations;
		this.emailed = emailed;
		this.company = company;
	}

	public Set<CompanyAccreditation> getAccreditations()
	{
		return this.accreditations;
	}

	public Company getCompany()
	{
		return this.company;
	}

	public boolean isEmailed()
	{
		return this.emailed;
	}

	public void setAccreditations(Set<CompanyAccreditation> accreditations)
	{
		this.accreditations = accreditations;
	}

	public void setCompany(Company company)
	{
		this.company = company;
	}

	public void setEmailed(boolean emailed)
	{
		this.emailed = emailed;
	}
}
