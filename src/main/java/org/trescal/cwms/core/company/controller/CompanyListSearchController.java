package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companylist.CompanyList;
import org.trescal.cwms.core.company.entity.companylist.db.CompanyListService;
import org.trescal.cwms.core.company.form.CompanyListSearchForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class CompanyListSearchController {

	@Autowired
	private CompanyListService companyListService;
	@Autowired
	private CompanyService companyService;

	public static final String FORM_NAME = "companylistform";
	public static final String VIEW_NAME = "trescal/core/company/companylistsearch";
	public static final String RESULTS_NAME = "trescal/core/company/companylistresults";

	@ModelAttribute(FORM_NAME)
	public CompanyListSearchForm formBackingObject() {
		return new CompanyListSearchForm();
	}

	@RequestMapping(value = "/companylistsearch.htm", method = RequestMethod.POST)
	protected String onSubmit(@ModelAttribute(FORM_NAME) CompanyListSearchForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto)
			throws Exception {
		PagedResultSet<CompanyList> rs = this.companyListService.searchCompanyList(form,
				companyService.get(allocatedCompanyDto.getKey()),
				new PagedResultSet<CompanyList>(
						form.getResultsPerPage() == 0 ? Constants.RESULTS_PER_PAGE : form.getResultsPerPage(),
						form.getPageNo() == 0 ? 1 : form.getPageNo()));
		form.setRs(rs);
		return RESULTS_NAME;
	}

	@RequestMapping(value = "/companylistsearch.htm", method = RequestMethod.GET)
	public String referenceData(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto)
			throws Exception {
		return VIEW_NAME;
	}

}
