package org.trescal.cwms.core.company.entity.businessarea.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.businessarea.BusinessArea;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface BusinessAreaService extends BaseService<BusinessArea, Integer> {

	/**
	 * get All translated business areas
	 * @param locale the {@link Locale} .
	 */
	List<KeyValueIntegerString> getAllTranslated(Locale locale);
}