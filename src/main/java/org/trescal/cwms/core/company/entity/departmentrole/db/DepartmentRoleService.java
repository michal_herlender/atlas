package org.trescal.cwms.core.company.entity.departmentrole.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.departmentrole.DepartmentRole;

public interface DepartmentRoleService
{
	DepartmentRole findDepartmentRole(int id);

	List<DepartmentRole> getAllDepartmentRoles();

	void insertDepartmentRole(DepartmentRole departmentrole);

	void updateDepartmentRole(DepartmentRole departmentrole);
}