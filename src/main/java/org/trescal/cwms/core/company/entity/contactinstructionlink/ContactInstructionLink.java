/**
 * 
 */
package org.trescal.cwms.core.company.entity.contactinstructionlink;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionEntity;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLink;

/**
 * An instruction recorded for a {@link Contact}. Can be *optionally* allocated
 * to a business company.
 * 
 * @author Richard
 */
@Entity
@Table(name = "contactinstructionlink", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "personid", "instructionid" }) })
public class ContactInstructionLink extends Allocated<Company> implements InstructionLink<Contact> {

	private int id;
	private Contact contact;
	private Instruction instruction;

	@Transient
	public InstructionEntity getInstructionEntity() {
		return InstructionEntity.CONTACT;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid")
	public Contact getContact() {
		return this.contact;
	}

	@Override
	@Transient
	public Contact getEntity() {
		return contact;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "instructionid")
	@Cascade(value = CascadeType.ALL)
	@Valid
	public Instruction getInstruction() {
		return instruction;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public void setInstruction(Instruction instruction) {
		this.instruction = instruction;
	}
}
