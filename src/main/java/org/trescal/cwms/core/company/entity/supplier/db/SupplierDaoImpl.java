package org.trescal.cwms.core.company.entity.supplier.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.supplier.Supplier;
import org.trescal.cwms.core.company.entity.supplier.SupplierApprovalType;
import org.trescal.cwms.core.company.entity.supplier.Supplier_;

@Repository("SupplierDao")
public class SupplierDaoImpl extends BaseDaoImpl<Supplier, Integer> implements SupplierDao {
	
	@Override
	protected Class<Supplier> getEntity() {
		return Supplier.class;
	}
	
	@Override
	public List<Supplier> getActiveSuppliers() {
		return getResultList(cb ->{
			CriteriaQuery<Supplier> cq = cb.createQuery(Supplier.class);
			Root<Supplier> supplier = cq.from(Supplier.class);
			cq.where(cb.isTrue(supplier.get(Supplier_.active)));
			return cq;
		});
	}
	
	@Override
	public Supplier getForApprovalType(SupplierApprovalType approvalType) {
		return getFirstResult(cb->{
			CriteriaQuery<Supplier> cq = cb.createQuery(Supplier.class);
			Root<Supplier> supplier = cq.from(Supplier.class);
			cq.where(cb.equal(supplier.get(Supplier_.approvalType), approvalType));
			return cq;
		}).orElse(null);
	}
}