package org.trescal.cwms.core.company.entity.subdiv;

import java.util.Comparator;

/**
 * Inner comparator class for sorting {@link Subdiv}s when returning collections
 * 
 * @author jamiev
 */
public class SubdivComparator implements Comparator<Subdiv>
{
	public int compare(Subdiv subdiv, Subdiv anotherSubdiv)
	{
		// names are for some reason the same, so return id comparison!
		if (subdiv.getSubname().compareTo(anotherSubdiv.getSubname()) == 0)
		{
			Integer sub1 = subdiv.getSubdivid();
			Integer sub2 = anotherSubdiv.getSubdivid();

			return sub1.compareTo(sub2);
		}
		else
		{
			return subdiv.getSubname().compareTo(anotherSubdiv.getSubname());
		}
	}
}