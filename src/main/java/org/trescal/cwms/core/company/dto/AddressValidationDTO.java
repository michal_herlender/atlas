package org.trescal.cwms.core.company.dto;

import org.trescal.cwms.core.company.entity.address.AddressValidator;

import lombok.AllArgsConstructor;
import lombok.Data;
import net.avalara.avatax.rest.client.models.ValidatedAddressInfo;

@Data
@AllArgsConstructor
public class AddressValidationDTO {

	private boolean successful;
	private ValidatedAddressInfo validatedAddress;
	private AddressValidator validator;
	private String validatedOn;
}