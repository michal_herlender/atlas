package org.trescal.cwms.core.company.entity.companyhistory.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.dto.CompanySearchResultWrapper;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companyhistory.CompanyHistory;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;

public interface CompanyHistoryDao extends BaseDao<CompanyHistory, Integer>
{
	List<CompanySearchResultWrapper> searchByCompanyRoles(String searchString, List<CompanyRole> roles,
			Company allocatedCompany, Boolean active, Boolean anywhereActive);
}