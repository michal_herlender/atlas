package org.trescal.cwms.core.company.entity.department.db;

import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.Department_;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.User_;
import org.trescal.cwms.core.userright.entity.userrole.UserRole;

@Repository
public class DepartmentDaoImpl extends BaseDaoImpl<Department, Integer> implements DepartmentDao {
	@Override
	protected Class<Department> getEntity() {
		return Department.class;
	}
	
	@Override
	public List<Department> findBySubdivAndTypes(Subdiv subdiv, Collection<DepartmentType> types) {
		return getResultList(cb -> {
			CriteriaQuery<Department> cq = cb.createQuery(Department.class);
			Root<Department> root = cq.from(Department.class);
			Join<Department, Subdiv> subdivJoin = root.join(Department_.subdiv);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(root.get(Department_.type).in(types));
			clauses.getExpressions().add(cb.equal(subdivJoin, subdiv));
			cq.where(clauses);
			cq.orderBy(cb.asc(root.get(Department_.name)));

			return cq;
		});

	}

	public List<Department> getAllSubdivDepartments(Integer subdivid) {
		return getResultList(cb -> {
			CriteriaQuery<Department> cq = cb.createQuery(Department.class);
			Root<Department> department = cq.from(Department.class);
			cq.where(cb.equal(department.get(Department_.subdiv), subdivid));
			return cq;
		});
	}
	
	@Override
	public List<Department> getBusinessDepartments() {
		return getResultList(cb -> {
			CriteriaQuery<Department> cq = cb.createQuery(Department.class);
			Root<Department> root = cq.from(Department.class);
			Join<Department, Subdiv> subdivJoin = root.join(Department_.subdiv);
			Join<Subdiv, Company> companyJoin = subdivJoin.join(Subdiv_.comp);
			cq.where(cb.equal(companyJoin.get(Company_.companyRole), CompanyRole.BUSINESS));
			cq.orderBy(cb.asc(root.get(Department_.name)));
			return cq;
		});
	}
	
	@Override
	public List<Department> getByTypeAndSubdiv(DepartmentType type, Integer subdivId) {
		return getResultList(cb -> {
			CriteriaQuery<Department> cq = cb.createQuery(Department.class);
			Root<Department> root = cq.from(Department.class);
			Join<Department, Subdiv> subdivJoin = root.join(Department_.subdiv);
			Join<Subdiv, Company> companyJoin = subdivJoin.join(Subdiv_.comp);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(Department_.type), type));
			clauses.getExpressions().add(cb.equal(companyJoin.get(Company_.companyRole), CompanyRole.BUSINESS));
			clauses.getExpressions().add(cb.equal(subdivJoin.get(Subdiv_.subdivid), subdivId));
			cq.where(clauses);
			return cq;
		});

	}

	@Override
	public List<Contact> getAllActiveAvailableDepartmentContact(List<Department> dept) {
		return getResultList(cb -> {
			CriteriaQuery<Contact> cq = cb.createQuery(Contact.class);
			Root<Contact> contactRoot = cq.from(Contact.class);
			Join<Contact,User> users = contactRoot.join(Contact_.user);
			Join<User,UserRole> userRoles = users.join(User_.userRoles);
			Join<UserRole,Subdiv> subdivs = userRoles.join("organisation");
			Join<Subdiv,Department> departs = subdivs.join(Subdiv_.departments);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(departs.in(dept));
			clauses.getExpressions().add(cb.equal(contactRoot.get(Contact_.active), true));
			cq.where(clauses);
			return cq.distinct(true);
		});
	}
}