package org.trescal.cwms.core.company.projection;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ContactProjectionDTO {
	// Below result fields always populated by query
	private Integer personid;
	private String firstName;
	private String lastName;
	private String email;
	private String telephone;
	private Boolean active;
	// Optional fields
	private SubdivProjectionDTO subdiv;

	public ContactProjectionDTO(Integer personid, String firstName, String lastName, 
			String email, String telephone, Boolean contactActive,
			Integer subdivid, String subname, Boolean subdivActive, 
			Integer coid, String coname, Boolean companyActive, Boolean onstop) {
		super();
		this.personid = personid;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.telephone = telephone;
		this.active = contactActive;
		this.subdiv = new SubdivProjectionDTO(subdivid, subname, subdivActive, coid, coname, companyActive, onstop);
	}
	
	public String getName() {
		StringBuffer result = new StringBuffer();
		if (firstName != null) result.append(firstName);
		if (result.length() > 0) result.append(" ");
		if (lastName != null) result.append(lastName);
		return result.toString();
	}
	
	@Override
    public int hashCode() {
        return this.personid;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ContactProjectionDTO && this.personid.equals(((ContactProjectionDTO) obj).getPersonid());
    }

}
