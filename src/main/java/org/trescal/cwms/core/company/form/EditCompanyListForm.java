package org.trescal.cwms.core.company.form;

import org.hibernate.validator.constraints.Length;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditCompanyListForm {
	
	@Length(min=1, max=50)
	private String name;
	private Integer companylistId;
	private Boolean activestatus;
	private Integer memberCoid;
	private Integer userCoid;

}
