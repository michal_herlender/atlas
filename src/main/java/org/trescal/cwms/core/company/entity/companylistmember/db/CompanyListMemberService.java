package org.trescal.cwms.core.company.entity.companylistmember.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.companylistmember.CompanyListMember;
import org.trescal.cwms.core.company.form.EditCompanyListForm;

public interface CompanyListMemberService extends BaseService<CompanyListMember, Integer> {
	
	List<CompanyListMember> getMembersByCompanyList(Integer companyListId);
	
	List<Integer> getMemberCoidsByCompanyLists(List<Integer> companyListIds);
	
	void createNewCompanyListMember(EditCompanyListForm form, String username);
	
	boolean remove(Integer companyListMemberId);

}
