package org.trescal.cwms.core.company.entity.businessdocumentsettings.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.businessdocumentsettings.BusinessDocumentSettings;
import org.trescal.cwms.core.company.entity.businessdocumentsettings.BusinessDocumentSettings_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;

@Repository
public class BusinessDocumentSettingsDaoImpl extends BaseDaoImpl<BusinessDocumentSettings, Integer> implements BusinessDocumentSettingsDao {

	@Override
	protected Class<BusinessDocumentSettings> getEntity() {
		return BusinessDocumentSettings.class;
	}

	@Override
	public List<BusinessDocumentSettings> getForCompanyAndDefault(Integer businessCompanyId) {
		return getResultList(cb -> {
			CriteriaQuery<BusinessDocumentSettings> cq = cb.createQuery(BusinessDocumentSettings.class);
			Root<BusinessDocumentSettings> root = cq.from(BusinessDocumentSettings.class);
			Join<BusinessDocumentSettings, Company> businessCompany = root.join(BusinessDocumentSettings_.businessCompany, JoinType.LEFT);
			Predicate clauses = cb.disjunction();
			clauses.getExpressions().add(cb.isTrue(root.get(BusinessDocumentSettings_.globalDefault)));
			clauses.getExpressions().add(cb.equal(businessCompany.get(Company_.coid), businessCompanyId));
			cq.where(clauses);
			
			// Results not ordered, caller is responsible for getting the one they want
			return cq;
		});
	}

}
