package org.trescal.cwms.core.company.controller;

import java.util.Locale;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.form.CompanyEditNonBusinessForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
/**
 * Controller used for editing finance settings of companies with role
 * other than 'business' : 'client', 'supplier', 'utility', 'finance'
 *
 * @author galen
 */
public class CompanyEditNonBusinessController extends CompanyEditFinanceController {

    @ModelAttribute(FORM_NAME)
    public CompanyEditNonBusinessForm formBackingObject(@RequestParam(PARAM_COID) Integer companyId,
                                                        @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto) {
        Company company = this.companyService.get(companyId);
        Company allocatedCompany = this.companyService.get(allocatedCompanyDto.getKey());
        CompanyEditNonBusinessForm form = new CompanyEditNonBusinessForm();
        super.initForm(form, company, allocatedCompany);
        return form;
    }

    @RequestMapping(value = "companyeditnonbusiness.htm", method = RequestMethod.GET)
    public ModelAndView referenceData(@ModelAttribute(FORM_NAME) CompanyEditNonBusinessForm form, Locale locale) {
        Map<String, Object> map = super.initReferenceMap(form.getCoid(), locale);
        return new ModelAndView(VIEW_NAME_EDIT, REFERENCE_NAME, map);
    }

    @RequestMapping(value = "companyeditnonbusiness.htm", method = RequestMethod.POST)
    public ModelAndView onSubmit(
            @Validated @ModelAttribute(FORM_NAME) CompanyEditNonBusinessForm form,
            BindingResult bindingResult,
            Locale locale,
            @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto) {
        if (bindingResult.hasErrors())
            return referenceData(form, locale);
        Company company = this.companyService.get(form.getCoid());
        Company allocatedCompany = this.companyService.get(allocatedCompanyDto.getKey());
        super.performUpdates(form, company, allocatedCompany);
        this.companyService.save(company);
        return new ModelAndView(new RedirectView(VIEW_NAME_COMPANY + company.getCoid(), true));
    }
}