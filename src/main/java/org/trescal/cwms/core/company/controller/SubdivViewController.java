package org.trescal.cwms.core.company.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.companysettings.db.SubdivSettingsForAllocatedSubdivService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.external.adveso.service.AdminActivitiesManagementService;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.procedure.entity.procedurecategory.db.CapabilityCategoryService;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultTypeDTO;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.service.SystemDefaultTypeService;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivityType;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Controller
@Slf4j
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY,
    Constants.SESSION_ATTRIBUTE_USERNAME})
public class SubdivViewController {
    @Autowired
    private AdminActivitiesManagementService adminActivitiesManagementService;
    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private BPOService bpoService;
    @Autowired
    private CompanySettingsForAllocatedCompanyService companySettingsService;
    @Autowired
    private InstructionViewComponent instructionViewComponent;
    @Autowired
    private InstrumService instrumentService;
    @Autowired
    private JobService jobService;
    @Autowired
    private CapabilityCategoryService capabilityCategoryService;
    @Autowired
    private QuotationService quotationService;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private SubdivSettingsForAllocatedSubdivService subdivSettingsService;
    @Autowired
    private SystemDefaultTypeService systemDefaultTypeService;
    @Autowired
    private UserService userService;
    @Autowired
    private AuthenticationService authServ;


	@ModelAttribute("systemdefaulttypes")
	public List<SystemDefaultTypeDTO> initializeSystemDefaults(Locale locale,
                                                               @RequestParam(value = "subdivid") Integer subdivid,
                                                               @ModelAttribute(value = Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto) {
        Subdiv subdiv = subdivService.get(subdivid);
        if (subdiv == null)
            throw new RuntimeException("Subdiv not found for id " + subdivid);
        List<SystemDefaultTypeDTO> result = this.systemDefaultTypeService.getSystemDefaultTypeDTOs(subdiv,
            allocatedCompanyDto.getKey(), locale);
        log.info("systemdefaulttypes.size(): " + result.size());
        return result;
    }

	private boolean allowAddDepts(User user, Subdiv subdiv) {
		boolean result = false;
		if (subdiv.getComp().getCompanyRole().equals(CompanyRole.BUSINESS)) {
			// User should be an admin for the subdiv being edited to add
			// departments
			result = this.authenticationService.hasRight(user.getCon(), Permission.DEPARTMENT_ADD_EDIT, subdiv.getId());
		}
		return result;
	}

	private boolean allowEditCategories(User user, Subdiv subdiv) {
		boolean result = false;
		if (subdiv.getComp().getCompanyRole().equals(CompanyRole.BUSINESS)) {
			// User should be an admin or director for the subdiv being edited
			// to edit procedure categories
			result = this.authenticationService.hasRight(user.getCon(), Permission.CATEGORY_EDIT, subdiv.getId());
		}
		return result;
	}

	@RequestMapping(value = { "/viewsub.htm", "/companyequip.htm" }, method = RequestMethod.GET)
	protected String handleRequestInternal(Model model,
                                           @RequestParam(value = "subdivid") Integer subdivid,
                                           @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName,
                                           @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                           @ModelAttribute("systemdefaulttypes") List<SystemDefaultTypeDTO> sdt, Locale locale) throws Exception {
        User user = userService.get(userName);
        Subdiv subdiv = this.subdivService.get(subdivid);
        if (subdiv == null)
            throw new RuntimeException("Subdiv not found for id " + subdivid);
        Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
        Company allocatedCompany = allocatedSubdiv.getComp();
        CompanySettingsForAllocatedCompany companySettings = companySettingsService.getByCompany(subdiv.getComp(),
            allocatedCompany);
        SubdivSettingsForAllocatedSubdiv subdivSettings = this.subdivSettingsService.get(subdiv, allocatedSubdiv);
        List<Contact> activeContacts = new ArrayList<>();
        List<Contact> inactiveContacts = new ArrayList<>();
        for (Contact contact : subdiv.getContacts())
            if (contact.isActive())
                activeContacts.add(contact);
            else
                inactiveContacts.add(contact);
        List<Address> activeAddresses = new ArrayList<>();
        List<Address> inactiveAddresses = new ArrayList<>();
        for (Address address : subdiv.getAddresses())
            if (address.getActive())
                activeAddresses.add(address);
            else
                inactiveAddresses.add(address);
        model.addAttribute("allowAddDepts", allowAddDepts(user, subdiv));
        model.addAttribute("allowEditCategories", allowEditCategories(user, subdiv));
        model.addAttribute("subdiv", subdiv);
        model.addAttribute("bPOsList", this.bpoService.getAllBySubdiv(subdiv, null, allocatedCompany));
        model.addAttribute("companysettings", companySettings);
        model.addAttribute("subdivsettings", subdivSettings);
        model.addAttribute("activecontacts", activeContacts);
        model.addAttribute("inactivecontacts", inactiveContacts);
        model.addAttribute("activeaddresses", activeAddresses);
        model.addAttribute("inactiveaddresses", inactiveAddresses);
        model.addAttribute("defaultrole", Scope.SUBDIV);
        model.addAttribute("instructions", this.instructionViewComponent.viewSubdiv(subdiv, allocatedSubdiv));
        model.addAttribute("procedureCategories", this.capabilityCategoryService.getAllSorted(subdiv));
        model.addAttribute("activejobcount",
            this.jobService.countActiveBySubdivAndAllocatedSubdiv(subdiv, allocatedSubdiv));
        model.addAttribute("totaljobcountbysubdiv",
            this.jobService.countBySubdivAndAllocatedSubdiv(subdiv, allocatedSubdiv));
        model.addAttribute("totaljobcount", this.jobService.countBySubdiv(subdiv));
        model.addAttribute("quotecount",
            this.quotationService.countBySubdivAndAllocatedSubdiv(subdiv, allocatedSubdiv));
        model.addAttribute("recentquotecount",
            this.quotationService.countActualBySubdivAndAllocatedSubdiv(2, subdiv, allocatedSubdiv));
        model.addAttribute("instcount", this.instrumentService.countBySubdiv(subdivid));
        model.addAttribute("itemActivityType", ItemActivityType.values());

		// initiation of subdivision adveso activities
		adminActivitiesManagementService.initSubdivActivities(model, sdt, subdivid, locale);

		model.addAttribute("hasPermisionOnEF", this.authServ.hasRight(user.getCon(), Permission.EXCHANGE_FORMAT));
		model.addAttribute("subdivs",subdivSettingsService.getAllBySubdivId(subdivid));

		return "trescal/core/company/subdiv";
	}
}