package org.trescal.cwms.core.company.entity.companyinstructionlink.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

public interface CompanyInstructionLinkService extends
		BaseService<CompanyInstructionLink, Integer>
{
	List<CompanyInstructionLink> getAllForCompany(Company company, Subdiv allocatedSubdiv);
	List<CompanyInstructionLink> getForCompanyAndTypes(Company company, Subdiv allocatedSubdiv, InstructionType... types);
}