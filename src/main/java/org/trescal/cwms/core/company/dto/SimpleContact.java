package org.trescal.cwms.core.company.dto;

public class SimpleContact
{
	private String name;
	private int personid;


	public SimpleContact(String name, int personid)
	{
		super();
		this.name = name;
		this.personid = personid;
	}

	public String getName()
	{
		return this.name;
	}

	public int getPersonid()
	{
		return this.personid;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setPersonid(int personid)
	{
		this.personid = personid;
	}

}
