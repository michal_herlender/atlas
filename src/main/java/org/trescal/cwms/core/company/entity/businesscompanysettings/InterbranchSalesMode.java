package org.trescal.cwms.core.company.entity.businesscompanysettings;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum InterbranchSalesMode {
	SALES_CATALOG("interbranchsalesmode.salescatalog"),
	HOURLY("interbranchsalesmode.hourly");
	
	private String messageCode;
	private ReloadableResourceBundleMessageSource messageSource;
	
	@Component
	public static class InterbranchSalesModeMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messageSource;
		
		@PostConstruct
        public void postConstruct() {
            for (InterbranchSalesMode rt : EnumSet.allOf(InterbranchSalesMode.class))
               rt.setMessageSource(messageSource);
        }
	}
	
	private InterbranchSalesMode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessageCode() {
		return messageCode;
	}

	private void setMessageSource(ReloadableResourceBundleMessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	public String getMessage() {
		Locale locale = LocaleContextHolder.getLocale();
		return messageSource.getMessage(messageCode, null, toString(), locale);
	}
}
