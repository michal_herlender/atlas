package org.trescal.cwms.core.company.entity.businessarea;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "businessarea")
public class BusinessArea extends Auditable
{
	private int businessareaid;
	private Set<Company> companies;
	private String description;
	private String name;
	private Set<Translation> nametranslation;
	private Set<Translation> descriptiontranslation;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "businessareaid")
	public int getBusinessareaid()
	{
		return this.businessareaid;
	}

	@OneToMany(mappedBy = "businessarea")
	public Set<Company> getCompanies()
	{
		return this.companies;
	}

	@Length(max = 250)
	@Column(name = "description", length = 250)
	public String getDescription()
	{
		return this.description;
	}

	@NotNull
	@Length(min = 1, max = 60)
	@Column(name = "name", length = 60)
	public String getName()
	{
		return this.name;
	}
	
	public void setBusinessareaid(int businessareaid)
	{
		this.businessareaid = businessareaid;
	}

	public void setCompanies(Set<Company> companies)
	{
		this.companies = companies;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="businessareanametranslation", joinColumns=@JoinColumn(name="businessareaid"))
	public Set<Translation> getNametranslation() {
		return nametranslation;
	}

	public void setNametranslation(Set<Translation> nametranslation) {
		this.nametranslation = nametranslation;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="businessareadescriptiontranslation", joinColumns=@JoinColumn(name="businessareaid"))
	public Set<Translation> getDescriptiontranslation() {
		return descriptiontranslation;
	}

	public void setDescriptiontranslation(Set<Translation> descriptiontranslation) {
		this.descriptiontranslation = descriptiontranslation;
	}
}
