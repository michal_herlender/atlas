package org.trescal.cwms.core.company.entity.departmentmember.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.login.entity.user.User;

public interface DepartmentMemberService
{
	void deleteDepartmentMember(DepartmentMember departmentMember);

	DepartmentMember findDepartmentMember(Integer personid, Integer deptid);

	List<DepartmentMember> getAllContactDepartmentMembers(Integer personid);

	List<DepartmentMember> getAllDepartmentMembers();

	List<DepartmentMember> getDepartmentMembers(int deptid);

	void insertDepartmentMember(DepartmentMember departmentmember);

	void saveOrUpdateDepartmentMember(DepartmentMember departmentMember);

	void updateDepartmentMember(DepartmentMember departmentmember);

	List<User> getDepartmentUsers(List<Department> depts);
}