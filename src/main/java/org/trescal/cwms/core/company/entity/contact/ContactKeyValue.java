package org.trescal.cwms.core.company.entity.contact;

import org.trescal.cwms.spring.model.KeyValue;

public class ContactKeyValue extends KeyValue<Integer, String> {
	public ContactKeyValue(Integer personid, String first, String last) {
		super(personid, first + " "+ last);
	}
	public ContactKeyValue(Integer key, String value) {
		super(key, value);
	}
}
