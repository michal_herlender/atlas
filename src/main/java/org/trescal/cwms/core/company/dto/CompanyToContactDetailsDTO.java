package org.trescal.cwms.core.company.dto;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.spring.model.KeyValue;

import lombok.Getter;
import lombok.Setter;

/*
 * Transfer object used for adding contact / company via DWR.
 * Added constraints.  Presumed min lengths of 3 except for some values which could conceivably have smaller lengths
 */
@Getter @Setter
public class CompanyToContactDetailsDTO
{
	@NotNull
	@Size(min=3)
	private String addr1;
	private String addr2;
	private String addr3;
	private Integer businessAreaId;
	@NotNull
	@Size(min=3, max=60)
	private String coname;
	@NotNull
	@Size(min=2, max=20)
	private String legalIdentifier;
	private CompanyRole companyRole;
	private Integer countryId;
	private String county;
	@Email
	private String email;	// Email validation is done in validator, and via Hibernate annotation - works best to combine 
	private String fax;
	@NotNull
	@Size(min=1)
	private String firstname;
	private String lastname;
	private String mobile;
	@NotNull
	@Size(min=3)
	private String postcode;
	@NotNull
	@Size(min=3, max=60)
	private String subname;
	@NotNull
	@Size(min=3)
	private String telephone;
	@NotNull
	@Size(min=1)
	private String town;
	private Integer transportinId;
	private Integer transportoutId;
	private Integer currencyId;
	private Boolean failed;
	private List<KeyValue<String,String>> errors;

	public CompanyToContactDetailsDTO()
	{
	}

	public CompanyToContactDetailsDTO(Boolean failed, List<KeyValue<String,String>> errors){
        this.failed = failed;
        this.errors = errors;
	}
}