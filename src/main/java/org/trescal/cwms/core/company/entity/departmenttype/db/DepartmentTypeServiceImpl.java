package org.trescal.cwms.core.company.entity.departmenttype.db;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.spring.model.KeyValue;

@Service
public class DepartmentTypeServiceImpl implements DepartmentTypeService
{
	@Override
	public List<DepartmentType> getProcEnabledDepartmentTypes() {
		List<DepartmentType> result = new ArrayList<DepartmentType>();
		
		for(DepartmentType dt : EnumSet.allOf(DepartmentType.class)) {
			if (dt.isProcedures()) result.add(dt);
		}
		return result;
	}
	
	@Override
	public List<DepartmentType> getActiveDepartmentTypes() {
		List<DepartmentType> result = new ArrayList<DepartmentType>();
		
		for(DepartmentType dt : EnumSet.allOf(DepartmentType.class)) {
			if (dt.isActive()) result.add(dt);
		}
		return result;
	}

	@Override
	public List<KeyValue<Integer, String>> getActiveDepartmentTypeDtos() {
		return getActiveDepartmentTypes().stream().map(dt -> dt.dto()).collect(Collectors.toList());
	}
}