package org.trescal.cwms.core.company.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.account.entity.bankaccount.BankAccount;
import org.trescal.cwms.core.company.entity.businesscompanylogo.BusinessCompanyLogo;
import org.trescal.cwms.core.company.entity.businesscompanysettings.BusinessCompanySettings;
import org.trescal.cwms.core.company.entity.businesscompanysettings.InterbranchSalesMode;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.form.CompanyEditBusinessForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.spring.model.KeyValue;

/*
 * Controller specific for Finance to edit the settings of a Business Company
 * GB 2015-12-17
 */
@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class CompanyEditBusinessController extends CompanyEditFinanceController {

	@Autowired
	private SupportedCurrencyService supportedCurrencyService; 
	
	public static final String MESSAGE_CODE_NO = "no";
	public static final String MESSAGE_CODE_YES = "yes";
	public static final String MESSAGE_NO = "No";
	public static final String MESSAGE_YES = "Yes";
	
	@ModelAttribute(FORM_NAME)
	public CompanyEditBusinessForm formBackingObject(
			@RequestParam(PARAM_COID) int coid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto) {
		Company company = this.companyService.get(coid);
		Company allocatedCompany = this.companyService.get(allocatedCompanyDto.getKey());
		
		CompanyEditBusinessForm form = new CompanyEditBusinessForm();
		CompanySettingsForAllocatedCompany settings = super.initForm(form, company, allocatedCompany);
		form.setCompanyCode(company.getCompanyCode());
		// Initialize defaults in case that a company doesn't have businessSettings yet
		if (company.getBusinessSettings() == null) {
			form.setLogo(BusinessCompanyLogo.TRESCAL);
			form.setInterbranchSalesMode(InterbranchSalesMode.SALES_CATALOG);
			form.setUsePriceCatalog(true);
			form.setUseTaxableOptionOnPurchaseOrders(false);
		}
		else {
			form.setCompanyCapital(company.getBusinessSettings().getCapital());
			form.setDefaultQuotationDuration(company.getBusinessSettings().getDefaultQuotationDuration());
			form.setLegalRegistration1(company.getBusinessSettings().getLegalRegistration1());
			form.setLegalRegistration2(company.getBusinessSettings().getLegalRegistration2());
			form.setLogo(company.getBusinessSettings().getLogo());
			form.setHourlyRate(company.getBusinessSettings().getHourlyRate());
			form.setInterbranchDiscountRate(company.getBusinessSettings().getInterbranchDiscountRate());
			form.setInterbranchMarkupRate(company.getBusinessSettings().getInterbranchMarkupRate());
			form.setInterbranchSalesMode(company.getBusinessSettings().getInterbranchSalesMode());
			form.setUsePriceCatalog(company.getBusinessSettings().getUsePriceCatalog());
			form.setWarrantyCalibration(company.getBusinessSettings().getWarrantyCalibration());
			form.setWarrantyRepaire(company.getBusinessSettings().getWarrantyRepaire());
			form.setUseTaxableOptionOnPurchaseOrders(company.getBusinessSettings().getUseTaxableOption());
		}
		if ((settings != null) && settings.getBankAccount() != null) {
			form.setBankAccountId(settings.getBankAccount().getId());
		}
		else {
			form.setBankAccountId(0);
		}
		return form;
	}
	
	@RequestMapping(value="companyeditbusiness.htm", method=RequestMethod.GET)
	public ModelAndView referenceData(@ModelAttribute(FORM_NAME) CompanyEditBusinessForm form, BindingResult bindingResult, Locale locale) {
		Company company = this.companyService.get(form.getCoid());
		List<KeyValue<Integer, String>> bankAccounts = getBankAccountDtos(company, locale);
		if (bankAccounts.size() == 1) {
			bindingResult.reject(ERROR_CODE_NO_BANK_ACCOUNTS, null, ERROR_MESSAGE_NO_BANK_ACCOUNTS);
		}
		
		Map<String, Object> map = super.initReferenceMap(form.getCoid(), locale);
		// Business Company specific reference data here
		map.put("bankaccounts", bankAccounts);
		map.put("logo", BusinessCompanyLogo.values());
		map.put("defaultcurrency", this.supportedCurrencyService.getDefaultCurrency());
		map.put("interbranchSalesModes", InterbranchSalesMode.values());
		map.put("booleanDtos", getBooleanDtos(locale));
		
		return new ModelAndView(VIEW_NAME_EDIT, REFERENCE_NAME, map);
	}
	
	private List<KeyValue<Integer, String>> getBankAccountDtos(Company company, Locale locale) {
		List<KeyValue<Integer, String>> result = new ArrayList<>();
		result.add(new KeyValue<Integer, String>(0, messageSource.getMessage(MESSAGE_CODE_NONE, null, locale)));
		List<BankAccount> bankAccounts = this.bankAccountService.searchByCompany(company);
		for (BankAccount bankAccount : bankAccounts) {
			StringBuffer value = new StringBuffer();
			value.append(bankAccount.getIban());
			value.append(" - ");
			value.append(bankAccount.getSwiftBic());
			value.append(" - ");
			value.append(bankAccount.getCurrency().getCurrencyCode());
			
			result.add(new KeyValue<>(bankAccount.getId(), value.toString()));
		}
		return result;
	}
	
	protected List<KeyValue<Boolean, String>> getBooleanDtos(Locale locale) {
		List<KeyValue<Boolean, String>> result = new ArrayList<>();
		result.add(new KeyValue<>(Boolean.TRUE, messageSource.getMessage(MESSAGE_CODE_YES, null, MESSAGE_YES, locale)));
		result.add(new KeyValue<>(Boolean.FALSE, messageSource.getMessage(MESSAGE_CODE_NO, null, MESSAGE_NO, locale)));
		return result;
	}

	@RequestMapping(value="companyeditbusiness.htm", method=RequestMethod.POST)
	public ModelAndView onSubmit(
			@Validated @ModelAttribute(FORM_NAME) CompanyEditBusinessForm form,
			BindingResult bindingResult,
			Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto) {
		if (bindingResult.hasErrors()) {
			return referenceData(form, bindingResult, locale);
		}
		Company company = this.companyService.get(form.getCoid());
		Company allocatedCompany = this.companyService.get(allocatedCompanyDto.getKey());
		BankAccount bankAccount = null;
		if (form.getBankAccountId() != null) {
			bankAccount = this.bankAccountService.get(form.getBankAccountId());
		}
		
		if (company.getBusinessSettings() == null) {
			company.setBusinessSettings(new BusinessCompanySettings());
			company.getBusinessSettings().setUsesPlantillas(false);
		}
		company.setCompanyCode(form.getCompanyCode());
		company.setLegalIdentifier(form.getLegalIdentifier());
		company.setFiscalIdentifier(form.getFiscalIdentifier());
		company.getBusinessSettings().setCapital(form.getCompanyCapital());
		company.getBusinessSettings().setCompany(company);
		company.getBusinessSettings().setDefaultQuotationDuration(form.getDefaultQuotationDuration());
		company.getBusinessSettings().setInterbranchDiscountRate(form.getInterbranchDiscountRate());
		company.getBusinessSettings().setInterbranchMarkupRate(form.getInterbranchMarkupRate());
		company.getBusinessSettings().setInterbranchSalesMode(form.getInterbranchSalesMode());
		company.getBusinessSettings().setLegalRegistration1(form.getLegalRegistration1());
		company.getBusinessSettings().setLegalRegistration2(form.getLegalRegistration2());
		company.getBusinessSettings().setLogo(form.getLogo());
		company.getBusinessSettings().setHourlyRate(form.getHourlyRate());
		company.getBusinessSettings().setUsePriceCatalog(form.getUsePriceCatalog());
		company.getBusinessSettings().setWarrantyCalibration(form.getWarrantyCalibration());
		company.getBusinessSettings().setWarrantyRepaire(form.getWarrantyRepaire());
		company.getBusinessSettings().setUseTaxableOption(form.getUseTaxableOptionOnPurchaseOrders());
		CompanySettingsForAllocatedCompany settings = super.performUpdates(form, company, allocatedCompany);
		settings.setBankAccount(bankAccount);
				
		this.companyService.save(company);
		return new ModelAndView(new RedirectView(VIEW_NAME_COMPANY+company.getCoid(), true));
	}
}
