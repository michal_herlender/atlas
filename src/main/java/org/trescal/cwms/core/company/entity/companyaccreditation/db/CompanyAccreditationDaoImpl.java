package org.trescal.cwms.core.company.entity.companyaccreditation.db;

import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditation;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditation_;
import org.trescal.cwms.core.company.entity.supplier.Supplier;


@Repository("CompanyAccreditationDao")
public class CompanyAccreditationDaoImpl extends BaseDaoImpl<CompanyAccreditation, Integer> implements CompanyAccreditationDao {
	
	@Override
	protected Class<CompanyAccreditation> getEntity() {
		return CompanyAccreditation.class;
	}
	
	@Override
	public CompanyAccreditation findCompanyAccreditation(Integer supplierid, Integer coid) {
		Criteria companyAccreditationCriteria = getSession().createCriteria(CompanyAccreditation.class);
		companyAccreditationCriteria.add(Restrictions.eq("company.coid", coid));
		companyAccreditationCriteria.add(Restrictions.eq("supplier.supplierid", supplierid));
		return (CompanyAccreditation) companyAccreditationCriteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyAccreditation> findExpiredCompanyAccreditations() {
		Criteria companyAccreditationCritiera = getSession().createCriteria(CompanyAccreditation.class);
		companyAccreditationCritiera.add(Restrictions.le("expiryDate", new Date()));
		companyAccreditationCritiera.add(Restrictions.eq("emailAlert", false));
		Criteria supCrit = companyAccreditationCritiera.createCriteria("supplier");
		supCrit.add(Restrictions.eq("active", true));
		supCrit.add(Restrictions.eq("doesNotExpire", false));
		companyAccreditationCritiera.setFetchMode("company", org.hibernate.FetchMode.JOIN);
		companyAccreditationCritiera.setFetchMode("supplier", org.hibernate.FetchMode.JOIN);
		return companyAccreditationCritiera.list();
	}
	
	@Override
	public List<CompanyAccreditation> searchCompanyAccreditation(Integer coid) {
		return getResultList(cb -> {
			CriteriaQuery<CompanyAccreditation> cq = cb.createQuery(CompanyAccreditation.class);
			Root<CompanyAccreditation> companyAccreditation = cq.from(CompanyAccreditation.class);
			Join<CompanyAccreditation, Company> companyJoin = companyAccreditation.join(CompanyAccreditation_.company);
			cq.where(cb.equal(companyJoin.get(Company_.coid), coid));
			return cq;
		});
	}
	
	@Override
	public List<CompanyAccreditation> getForBusinessCompany(Company company, Company businessCompany) {
		return getResultList(cb -> {
			CriteriaQuery<CompanyAccreditation> cq = cb.createQuery(CompanyAccreditation.class);
			Root<CompanyAccreditation> companyAccreditation = cq.from(CompanyAccreditation.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(companyAccreditation.get(CompanyAccreditation_.company), company));
			clauses.getExpressions().add(cb.equal(companyAccreditation.get(CompanyAccreditation_.organisation), businessCompany));
			cq.where(clauses);
			return cq;
		});
	}
	
	@Override
	public CompanyAccreditation getForBusinessCompany(Company company, Company businessCompany, Supplier supplier) {
		return getFirstResult(cb -> {
			CriteriaQuery<CompanyAccreditation> cq = cb.createQuery(CompanyAccreditation.class);
			Root<CompanyAccreditation> companyAccreditation = cq.from(CompanyAccreditation.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(companyAccreditation.get(CompanyAccreditation_.company), company));
			clauses.getExpressions().add(cb.equal(companyAccreditation.get(CompanyAccreditation_.organisation), businessCompany));
			clauses.getExpressions().add(cb.equal(companyAccreditation.get(CompanyAccreditation_.supplier), supplier));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}
}