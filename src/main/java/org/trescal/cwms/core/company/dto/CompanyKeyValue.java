package org.trescal.cwms.core.company.dto;

import java.io.Serializable;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.spring.model.KeyValue;

public class CompanyKeyValue extends KeyValue<Integer, String> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public CompanyKeyValue() {
		super();
	}
	
	/*
	 * Intended for typical serialization use
	 */
	public CompanyKeyValue(Company company) {
		super(company.getCoid(), company.getConame());
	}
	/*
	 * Intended for unit testing 
	 */
	public CompanyKeyValue(int coid, String coname) {
		super(coid, coname);
	}
}
