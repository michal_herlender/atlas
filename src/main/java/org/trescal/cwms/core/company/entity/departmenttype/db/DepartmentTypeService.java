package org.trescal.cwms.core.company.entity.departmenttype.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.spring.model.KeyValue;

public interface DepartmentTypeService
{
	List<DepartmentType> getProcEnabledDepartmentTypes();
	
	List<DepartmentType> getActiveDepartmentTypes();

	List<KeyValue<Integer, String>> getActiveDepartmentTypeDtos();
}