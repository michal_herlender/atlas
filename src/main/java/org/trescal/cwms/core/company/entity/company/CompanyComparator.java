package org.trescal.cwms.core.company.entity.company;

import java.util.Comparator;

/**
 * Inner comparator class for sorting {@link Company} entities when returning
 * collections
 * 
 * @author jamiev
 */
public class CompanyComparator implements Comparator<Company>
{
	public int compare(Company o1, Company o2)
	{
		return o1.getConame().compareToIgnoreCase(o2.getConame());
	}
}