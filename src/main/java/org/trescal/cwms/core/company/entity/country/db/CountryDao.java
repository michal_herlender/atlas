package org.trescal.cwms.core.company.entity.country.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.country.Country;

public interface CountryDao extends BaseDao<Country, Integer>
{
	Country get(String countryName);
}