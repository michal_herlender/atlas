package org.trescal.cwms.core.company.entity.mailgroupmember.db;

import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.mailgroupmember.MailGroupMember;
import org.trescal.cwms.core.company.entity.mailgrouptype.MailGroupType;

public interface MailGroupMemberService extends BaseService<MailGroupMember, Integer>
{
	MailGroupMember findMailGroupMember(Integer personid, MailGroupType mailGroupType);

	List<MailGroupMember> getContactMailGroups(Integer personid);

	List<MailGroupMember> getMembersOfGroupForSubdiv(MailGroupType mailGroupType, int subdivid, boolean activeContactsOnly);
	
	Set<String> getMembersEmailsOfGroupForSubdiv(MailGroupType mailGroupType, int subdivid);
}