package org.trescal.cwms.core.company.entity.department.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.dto.EditableDepartmentDTO;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;

import java.util.List;

public interface DepartmentService extends BaseService<Department, Integer> {

    /**
     * Returns a list of all {@link Department}'s for
     * {@link Subdiv} that have procedures enabled, then by department
     * name.
     *
     * @return list of {@link Department}s.
     */
    List<Department> getAllCapabilityEnabledSubdivDepartments(Subdiv subdiv);
	
	List<Department> getAllSubdivDepartments(Integer subdivid);

	/**
	 * Returns a list of {@link EditableDepartmentDTO} containing all
	 * {@link Department} entities belonging to the {@link Subdiv} and a boolean
	 * indicating if the {@link Contact} is able to edit each {@link Department}
	 * .
	 * 
	 * @param subdivid the id of the {@link Subdiv}.
	 * @param contact the {@link Contact} performing the editing.
	 * @return list of {@link EditableDepartmentDTO}.
	 */
	List<EditableDepartmentDTO> getAllSubdivDepartmentsEditable(Integer subdivid, Contact contact);

	/**
	 * Returns all business {@link Department}s.
	 * 
	 * @return all {@link Department}.
	 */
	List<Department> getBusinessDepartments();

	List<Department> getByTypeAndSubdiv(DepartmentType type, Integer subdivId);
	
	/**
	 * Tests that the {@link Contact} editor is able to update the given
	 * {@link Department}. This function checks that the {@link Contact} is a
	 * manager of the {@link Department}. All {@link User} with the ADMIN or
	 * DIRECTOR security roles can manage all departments. MANAGER and USER are
	 * validated to see if they are managers of the {@link Department}.
	 * 
	 * @param editor the {@link Contact} performing the editing.
	 * @param procedure the {@link Department} being edited.
	 * @return {@link ResultWrapper} indicating if the user is allowed and if
	 *         not a message explaining why.
	 */
	ResultWrapper userCanEditDepartment(Contact editor, Department department);
	
	/**
	 * Called by DWR, deletes department with specified ID.
	 */
	public void deleteDepartment(int id);
	
	public List<Contact> getAllActiveAvailableDepartmentContact(List<Department> dept);
}