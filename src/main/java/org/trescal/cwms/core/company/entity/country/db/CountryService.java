package org.trescal.cwms.core.company.entity.country.db;

import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface CountryService extends BaseService<Country, Integer>
{
	Country get(String countryName);
	
	List<Country> getAll(Country defaultCountry);
	
	Set<KeyValueIntegerString> getCountries();
}