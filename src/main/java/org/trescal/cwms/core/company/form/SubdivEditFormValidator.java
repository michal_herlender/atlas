package org.trescal.cwms.core.company.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class SubdivEditFormValidator extends AbstractBeanValidator {
	public static String MESSAGE_CODE_SIZE = "error.size.between";
	public static String DEFAULT_MESSAGE_SIZE = "size must be between {0} and {1}";

	@Autowired
	private ContactService contactService;
	@Autowired
	private SubdivService subdivService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(SubdivEditForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			SubdivEditForm sef = (SubdivEditForm) target;

			// business logic to prevent duplicate subdiv names
			Subdiv subdiv = subdivService.get(sef.getSubdivId());
			Long duplicateCount = this.subdivService.countCompanySubdivsByName(subdiv.getComp().getId(),
					sef.getSubdivName().trim(), sef.getSubdivId());
			if (duplicateCount > 0) {
				errors.rejectValue("subdivName", null, "There is already a subdivision for this company with this name");
			}

			if (subdiv.getComp().getCompanyRole().equals(CompanyRole.BUSINESS)) {
				verifyField(sef.getAnalyticalCenter(), "analyticalCenter", errors);
				verifyField(sef.getSubdivCode(), "subdivCode", errors);
				
				if(sef.getDefaultTurnaround() == null)
					errors.rejectValue("defaultTurnaround", "repairinspection.validator.mandatory", "Mandatory Field");
			}

			// We allow duplicated SIRET numbers within a company
			// The business contact, if selected, should be active in the current subdivision.
			if (sef.getDefaultBusinessContactForAllocatedSubdiv() > 0) {
				Contact contact = this.contactService.get(sef.getDefaultBusinessContactForAllocatedSubdiv());
				if (!contact.isActive()) {
					errors.rejectValue("defaultBusinessContactForAllocatedSubdiv", null, "The contact is inactive");
				}
//				if (contact.getSub().getSubdivid() != sef.getAllocatedSubdivId()) {
//					errors.rejectValue("defaultBusinessContact", null, "The contact belongs to another subdivision");
//				}
			}
			if (sef.getDefaultBusinessContact() > 0) {
				Contact contact = this.contactService.get(sef.getDefaultBusinessContact());
				if (!contact.isActive()) {
					errors.rejectValue("defaultBusinessContact", null, "The contact is inactive");
				}
			}
		}
	}

	private void verifyField(String fieldValue, String path, Errors errors) {
		if ((fieldValue == null) || fieldValue.trim().length() == 0 || fieldValue.trim().length() > 3) {
			errors.rejectValue(path, MESSAGE_CODE_SIZE, new Integer[] { 1, 3 }, DEFAULT_MESSAGE_SIZE);
		}
	}
}