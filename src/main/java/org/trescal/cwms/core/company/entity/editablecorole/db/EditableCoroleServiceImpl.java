package org.trescal.cwms.core.company.entity.editablecorole.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.corole.Corole;
import org.trescal.cwms.core.company.entity.editablecorole.EditableCorole;

public class EditableCoroleServiceImpl implements EditableCoroleService
{
	EditableCoroleDao editableCoroleDao;

	public void deleteEditableCorole(EditableCorole editablecorole)
	{
		this.editableCoroleDao.remove(editablecorole);
	}

	@Override
	public List<EditableCorole> findAllEditableCorolesForRole(Corole role)
	{
		return this.editableCoroleDao.findAllEditableCorolesForRole(role);
	}

	public EditableCorole findEditableCorole(int id)
	{
		return this.editableCoroleDao.find(id);
	}

	public List<EditableCorole> getAllEditableCoroles()
	{
		return this.editableCoroleDao.findAll();
	}

	public void insertEditableCorole(EditableCorole EditableCorole)
	{
		this.editableCoroleDao.persist(EditableCorole);
	}

	public void saveOrUpdateEditableCorole(EditableCorole editablecorole)
	{
		this.editableCoroleDao.saveOrUpdate(editablecorole);
	}

	public void setEditableCoroleDao(EditableCoroleDao editableCoroleDao)
	{
		this.editableCoroleDao = editableCoroleDao;
	}

	public void updateEditableCorole(EditableCorole EditableCorole)
	{
		this.editableCoroleDao.update(EditableCorole);
	}
}