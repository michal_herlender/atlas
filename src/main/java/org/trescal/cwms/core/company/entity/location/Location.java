package org.trescal.cwms.core.company.entity.location;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

/**
 * Represents a building / department that is located at a particular
 * {@link Address}.
 * 
 * @author Richard
 */
@Entity
@Table(name = "location", uniqueConstraints = { @UniqueConstraint(columnNames = { "addressid", "location" }) })
public class Location extends Auditable
{
	private boolean active;
	private Address add;
	private Set<Contact> contacts;
	private Set<Instrument> instrums;
	private String location;
	private int locationid;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "addressid", nullable = false)
	public Address getAdd()
	{
		return this.add;
	}

	@OneToMany(mappedBy = "defLocation", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Contact> getContacts()
	{
		return this.contacts;
	}

	@OneToMany(mappedBy = "loc", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Instrument> getInstrums()
	{
		return this.instrums;
	}

	@NotNull
	@Length(min = 1, max = 100)
	@Column(name = "location", length = 100, nullable = false)
	public String getLocation()
	{
		return this.location;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "locationid")
	public int getLocationid()
	{
		return this.locationid;
	}

	@NotNull
	@Column(name = "active", nullable = false, columnDefinition="tinyint")
	public boolean isActive()
	{
		return this.active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active)
	{
		this.active = active;
	}

	public void setAdd(Address add)
	{
		this.add = add;
	}

	public void setContacts(Set<Contact> contacts)
	{
		this.contacts = contacts;
	}

	public void setInstrums(Set<Instrument> instrums)
	{
		this.instrums = instrums;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public void setLocationid(int locationid)
	{
		this.locationid = locationid;
	}

	@Override
	public String toString()
	{
		return Integer.toString(this.locationid);
	}
}
