package org.trescal.cwms.core.company.entity.title.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.title.Title;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TitleServiceImpl implements TitleService
{
	@Autowired
	private TitleDao titleDao;
	@Autowired
	private CompanyService companyService;

	public Title findTitle(Integer title)
	{
		return titleDao.find(title);
	}

	public void insertTitle(Title title)
	{
		titleDao.persist(title);
	}

	public void updateTitle(Title title)
	{
		titleDao.update(title);
	}

	public List<Title> getAllTitles()
	{
		return titleDao.findAll();
	}

	@Override
	public List<KeyValue<String, String>> getDTOTitles(String country) {
		return titleDao.findAll().stream()
				.filter(t -> t.getCountry().getCountryCode().equals(country))
				.map(title -> new KeyValue<>(title.getTitle(),title.getTitle()))
				.collect(Collectors.toList());
	}

	public TitleDao getTitleDao()
	{
		return titleDao;
	}

	public void setTitleDao(TitleDao titleDao)
	{
		this.titleDao = titleDao;
	}
}