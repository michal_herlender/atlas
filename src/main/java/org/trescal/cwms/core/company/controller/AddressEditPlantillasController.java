package org.trescal.cwms.core.company.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addressplantillassettings.AddressPlantillasSettings;
import org.trescal.cwms.core.company.entity.addressplantillassettings.db.AddressPlantillasSettingsService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.form.AddressPlantillasForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class AddressEditPlantillasController {
	
	@Autowired
	private AddressService addressService;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private AddressPlantillasSettingsService addressPlantillasSettingsService; 
	
	public static final String VIEW_NAME = "trescal/core/company/plantillasedit";
	public static final String REQUEST_URL = "plantillasedit.htm";

	@ModelAttribute("form")
	public AddressPlantillasForm formBackingObject(
			@RequestParam(name="addrid", required=true) int addrid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		AddressPlantillasForm form = new AddressPlantillasForm();
		AddressPlantillasSettings settings = this.addressPlantillasSettingsService.getForAddress(addrid, companyDto.getKey());
		if (settings != null) initForm(form, settings);  
		return form;
	}
	
	private void initForm(AddressPlantillasForm form, AddressPlantillasSettings settings) {
		form.setContract(settings.getContract());
		form.setFormerCustomerId(settings.getFormerCustomerId());
		form.setFtpPassword(settings.getFtpPassword());
		form.setFtpServer(settings.getFtpServer());
		form.setFtpUser(settings.getFtpUser());
		form.setMetraClient(settings.getMetraClient());
		form.setNextDateOnCertificate(settings.getNextDateOnCertificate());
		form.setSendByFtp(settings.getSendByFtp());
		form.setTextToleranceFailure(settings.getTextToleranceFailure());
		form.setTextUncertaintyFailure(settings.getTextUncertaintyFailure());
	}
	
	@ModelAttribute("address")
	public Address getAddress(@RequestParam(name="addrid", required=true) int addrid) {
		return this.addressService.get(addrid);
	}
	
	@RequestMapping(value=REQUEST_URL, method=RequestMethod.GET)
	public String showView(Model model) {
		return VIEW_NAME;
	}
	
	@RequestMapping(value=REQUEST_URL, method=RequestMethod.POST)
	public String onSubmit(
			@RequestParam(name="addrid", required=true) int addrid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@Valid @ModelAttribute("form") AddressPlantillasForm form, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return VIEW_NAME;
		}
		AddressPlantillasSettings plantillasSettings = this.addressPlantillasSettingsService.getForAddress(addrid, companyDto.getKey());
		if (plantillasSettings == null) {
			Address address = this.addressService.get(addrid);
			Company allocatedCompany = this.companyService.get(companyDto.getKey());
			plantillasSettings = new AddressPlantillasSettings();
			plantillasSettings.setAddress(address);
			plantillasSettings.setOrganisation(allocatedCompany);
		}
		plantillasSettings.setContract(form.getContract());
		plantillasSettings.setFormerCustomerId(form.getFormerCustomerId());
		plantillasSettings.setFtpPassword(form.getFtpPassword());
		plantillasSettings.setFtpServer(form.getFtpServer());
		plantillasSettings.setFtpUser(form.getFtpUser());
		plantillasSettings.setMetraClient(form.getMetraClient());
		plantillasSettings.setNextDateOnCertificate(form.getNextDateOnCertificate());
		plantillasSettings.setSendByFtp(form.getSendByFtp());
		plantillasSettings.setTextToleranceFailure(form.getTextToleranceFailure());
		plantillasSettings.setTextUncertaintyFailure(form.getTextUncertaintyFailure());
		
		this.addressPlantillasSettingsService.merge(plantillasSettings);
		
		return "redirect:viewaddress.htm?addrid="+addrid+"&loadtab=plantillas-tab";
	}
}
