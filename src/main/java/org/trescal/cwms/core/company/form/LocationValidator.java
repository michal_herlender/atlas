package org.trescal.cwms.core.company.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class LocationValidator extends AbstractBeanValidator
{
	@Autowired
	private LocationService locServ;
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(Location.class);
	}
	
	@Override
	public void validate(Object target, Errors errors)
	{
		super.validate(target, errors);
		Location location = (Location) target;

		if ((location.getLocation() != null) && (location.getAdd() != null))
		{
			// check if the location already exists but is not the location
			// being edited!
			if (this.locServ.locationAlreadyExistsAtAddress(location.getLocation(), location.getAdd().getAddrid(), location.getLocationid()))
			{
				errors.rejectValue("location", null, "This address already contains a location named '"
						+ location.getLocation() + "'");
			}
		}
	}
}