package org.trescal.cwms.core.company.dto;

import java.io.Serializable;

import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.spring.model.KeyValue;

public class SubdivKeyValue extends KeyValue<Integer, String> implements Serializable {
	
	private static final long serialVersionUID = 1L;
	/*
	 * Intended for typical serialization use
	 */
	public SubdivKeyValue(Subdiv subdiv) {
		super(subdiv.getSubdivid(), subdiv.getSubname());
	}
	/*
	 * Intended for unit testing 
	 */
	public SubdivKeyValue(int subdivid, String subname) {
		super(subdivid, subname);
	}
}
