package org.trescal.cwms.core.company.entity.departmenttype;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Represents a type of {@link Department} available to all business companies.
 * Changed from an Entity to a Enum GB 2016-05-03 (best fix for language comparison bugs)
 * 
 * @author Richard
 */
public enum DepartmentType
{
	UNDEFINED(false, "departmenttype.undefined", false),
	CUSTOMER_SERVICES(true, "departmenttype.customerservices", false),
	ACCOUNTS(true, "departmenttype.accounts", false),
	LABORATORY(true, "departmenttype.laboratory", true),
	GOODS_IN_OUT(true, "departmenttype.goodsinout", false),
	ONSITE(true, "departmenttype.onsite", false),
	SUBCONTRACTING(true, "departmenttype.subcontracting", false),
	THIRD_PARTY(true, "departmenttype.thirdparty", false);

	private DepartmentType(boolean active, String messageCode, boolean procedures) {
		this.active = active;
		this.messageCode = messageCode;
		this.procedures = procedures;
	}
	
	private boolean active;
	private boolean procedures;
	private String messageCode;
	private ReloadableResourceBundleMessageSource messageSource;

	public static DepartmentType get(Integer key) {
		switch (key) {
			case 1: return CUSTOMER_SERVICES;
			case 2: return ACCOUNTS;
			case 3: return LABORATORY;
			case 4: return GOODS_IN_OUT;
			case 5: return ONSITE;
			case 6: return SUBCONTRACTING;
			case 7: return THIRD_PARTY;
		}
		return UNDEFINED;
	}

	@Component
	private static class DepartmentTypeMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messageSource;
		@PostConstruct
        public void postConstruct() {
            for (DepartmentType dt : EnumSet.allOf(DepartmentType.class))
               dt.messageSource = messageSource;
        }
	}

	public String getMessage() {
		return messageSource.getMessage(messageCode, null, this.toString(), LocaleContextHolder.getLocale());
	}
	
	public boolean isActive()
	{
		return this.active;
	}

	public boolean isProcedures()
	{
		return this.procedures;
	}

	public String getMessageCode() {
		return messageCode;
	}
	
	public KeyValue<Integer, String> dto() {
		return new KeyValue<>(this.ordinal(), getMessage());
	}
}
