package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.company.entity.departmentmember.db.DepartmentMemberService;
import org.trescal.cwms.core.company.entity.departmentrole.db.DepartmentRoleService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.departmenttype.db.DepartmentTypeService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.form.DepartmentForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.db.CapabilityCategoryService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Displays a form allowing the user to create a new or edit an existing
 * {@link Department} for a given {@link Subdiv} and to add {@link Contact}s to
 * that {@link Department}.
 */
@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE})
public class DepartmentController {
    @Autowired
    private AddressService addressServ;
    @Autowired
    private ContactService contactServ;
    @Autowired
    private DepartmentMemberService departmentMemberServ;
    @Autowired
    private DepartmentRoleService departmentRoleServ;
    @Autowired
    private DepartmentService departmentServ;
    @Autowired
    private DepartmentTypeService departmentTypeServ;
    @Autowired
    private CapabilityCategoryService procCatService;
    @Autowired
    private SubdivService subdivServ;
    @Autowired
    private UserService userService;

    @ModelAttribute("addresses")
    protected List<KeyValue<Integer, String>> addresses(@ModelAttribute("command") DepartmentForm daf) {
        Subdiv subdiv = subdivServ.get(daf.getSubdivid());
        return addressServ.getAllActiveSubdivAddresses(subdiv, AddressType.WITHOUT).stream().map(
            addr -> new KeyValue<Integer, String>(addr.getAddrid(), addr.getAddressLine())
        ).collect(Collectors.toList());
    }

    @ModelAttribute("procedurecategories")
    protected List<CapabilityCategory> procedureCategories(
        @RequestParam(value = "deptid", required = false, defaultValue = "0") Integer deptid) {
        if (deptid == 0) return new ArrayList<CapabilityCategory>();
        else {
            Department department = departmentServ.get(deptid);
            return procCatService.getAll(department);
        }
    }

    @ModelAttribute("command")
    protected DepartmentForm formBackingObject(
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
        @RequestParam(value = "deptid", required = false, defaultValue = "0") Integer deptid,
        @RequestParam(value = "subdivid", required = false, defaultValue = "0") Integer subdivid) throws Exception {


        DepartmentForm daf = new DepartmentForm();
		Department dep = null;
		if (deptid > 0) {
			dep = this.departmentServ.get(deptid);
			subdivid = dep.getSubdiv().getSubdivid();
			daf.setNewDepartment(false);
			Contact editor = this.userService.get(username).getCon();
			daf.setUserCanEdit(this.departmentServ.userCanEditDepartment(editor, dep).isSuccess());
			daf.setDepartmentType(dep.getType().ordinal());
			daf.setDepartmentName(dep.getName());
			daf.setShortName(dep.getShortName());
			daf.setAddrid(dep.getAddress().getAddrid());
		}
		else {
			dep = new Department();
			dep.setSubdiv(this.subdivServ.get(subdivid));
			daf.setNewDepartment(true);
			daf.setUserCanEdit(true);
		}
		daf.setDepartment(dep);
		daf.setSubdivid(subdivid);
		Map<Integer, String> cons = new HashMap<Integer, String>();
		HashMap<Integer, String> deptRoles = new HashMap<Integer, String>();
		// populate a list of all active contacts available to the Subdivision
		List<Contact> contacts = this.contactServ.getActiveContactsWithRoleForSubdiv(subdivid);
		
// Axel's code - throws transient exception when creating new department
//		List<Department> depts = new ArrayList<Department>();
//		depts.add(dep);
// 		List<Contact> contacts = this.departmentServ.getAllActiveAvailableDepartmentContact(depts);
// original method
// 		this.contactServ.getAllActiveSubdivContacts(subdivid);
		for (Contact c : contacts) {
			cons.put(c.getPersonid(), "null");
			deptRoles.put(c.getPersonid(), "");
		}
		// now add any Contacts that have already been added to the Department
		if (dep.getDeptid() != null) {
			List<DepartmentMember> departmentMembers = this.departmentMemberServ.getDepartmentMembers(dep.getDeptid());
			for (DepartmentMember c : departmentMembers) {
				cons.put(c.getContact().getPersonid(), "on");
				deptRoles.put(c.getContact().getPersonid(), String.valueOf(c.getDepartmentRole().getId()));
			}
		}
		daf.setContacts(cons);
		daf.setDeptRoles(deptRoles);
		return daf;
	}
	
	private void updateDepartment(Department department, DepartmentForm form) {
		department.setName(form.getDepartmentName());
		department.setShortName(form.getShortName());
		department.setType(DepartmentType.get(form.getDepartmentType()));
		department.setAddress(addressServ.get(form.getAddrid()));
	}
	
	@RequestMapping(value="/department.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@Valid @ModelAttribute("command") DepartmentForm daf, BindingResult result) throws Exception
	{
		if (result.hasErrors()) return referenceData(daf);
		else {
			Department department = daf.getDepartment();
			Contact editor = this.userService.get(username).getCon();
			// only allow certain users edit the deparmtent, all users can create
			if (daf.isNewDepartment() || this.departmentServ.userCanEditDepartment(editor, department).isSuccess()) {
				updateDepartment(department, daf);
				department = this.departmentServ.merge(department);
				Map<Integer, String> contacts = daf.getContacts();
				Map<Integer, String> departmentRoles = daf.getDeptRoles();
				Collection<Integer> keys = contacts.keySet();
				for (Integer key : keys) {
					// add a new department role
					if (contacts.get(key) != null) {
						DepartmentMember member = this.departmentMemberServ.findDepartmentMember(key, department.getDeptid());
						if (member != null) {
							member.setContact(this.contactServ.get(key));
							member.setDepartmentRole(this.departmentRoleServ.findDepartmentRole(Integer.parseInt(departmentRoles.get(key))));
							member.setDepartment(department);
							this.departmentMemberServ.updateDepartmentMember(member);
						}
						else {
							member = new DepartmentMember();
							member.setContact(this.contactServ.get(key));
							member.setDepartmentRole(this.departmentRoleServ.findDepartmentRole(Integer.parseInt(departmentRoles.get(key))));
							member.setDepartment(department);
							this.departmentMemberServ.insertDepartmentMember(member);
						}
					}
					else {
						DepartmentMember dm = this.departmentMemberServ.findDepartmentMember(key, department.getDeptid());
						if (dm != null) {
							this.departmentMemberServ.deleteDepartmentMember(dm);
						}
					}
				}
			}
			String tab = "&loadtab=department-tab";
			return new ModelAndView(new RedirectView("viewsub.htm?subdivid=" + daf.getSubdivid() + tab));
		}
	}
	
	@RequestMapping(value="/department.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(
			@ModelAttribute("command") DepartmentForm daf) throws Exception
	{
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("roles", this.departmentRoleServ.getAllDepartmentRoles());
		List<Department> depts = new ArrayList<Department>();
		depts.add(daf.getDepartment());
		model.put("cons", this.contactServ.getActiveContactsWithRoleForSubdiv(daf.getSubdivid()));
//		model.put("cons", this.departmentServ.getAllActiveAvailableDepartmentContact(depts));
		model.put("departmenttypes", this.departmentTypeServ.getActiveDepartmentTypeDtos());
		return new ModelAndView("trescal/core/company/department", model);
	}
}