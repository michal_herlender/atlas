package org.trescal.cwms.core.company.entity.contactinstructionlink.db;

import java.util.Arrays;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink_;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instruction.Instruction_;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

@Repository
public class ContactInstructionLinkDaoImpl extends BaseDaoImpl<ContactInstructionLink, Integer>
		implements ContactInstructionLinkDao {

	@Override
	protected Class<ContactInstructionLink> getEntity() {
		return ContactInstructionLink.class;
	}

	@Override
	public List<ContactInstructionLink> getForContactAndTypes(Contact contact, Company allocatedCompany,
			InstructionType... types) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ContactInstructionLink> cq = cb.createQuery(ContactInstructionLink.class);

		Root<ContactInstructionLink> root = cq.from(ContactInstructionLink.class);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(root.get(ContactInstructionLink_.contact), contact));
		clauses.getExpressions()
				.add(cb.equal(root.get(ContactInstructionLink_.organisation.getName()), allocatedCompany));
		if (types.length > 0) {
			Join<ContactInstructionLink, Instruction> instruction = root.join(ContactInstructionLink_.instruction);
			clauses.getExpressions().add(instruction.get(Instruction_.instructiontype).in(Arrays.asList(types)));
		}
		cq.where(clauses);
		cq.select(root);
		return getEntityManager().createQuery(cq).getResultList();
	}

	@Override
	public Long count(Contact contact, Company allocatedCompany) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);

		Root<ContactInstructionLink> root = cq.from(ContactInstructionLink.class);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(root.get(ContactInstructionLink_.contact), contact));
		clauses.getExpressions()
				.add(cb.equal(root.get(ContactInstructionLink_.organisation.getName()), allocatedCompany));

		cq.where(clauses);
		cq.select(cb.count(root));

		return getEntityManager().createQuery(cq).getSingleResult();
	}
}