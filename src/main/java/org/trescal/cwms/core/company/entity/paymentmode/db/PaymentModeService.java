package org.trescal.cwms.core.company.entity.paymentmode.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.spring.model.KeyValue;

public interface PaymentModeService extends BaseService<PaymentMode, Integer> {
	PaymentMode get(String code);
	List<KeyValue<Integer, String>> getDTOList(Locale locale, boolean prependNoneDto);
}
