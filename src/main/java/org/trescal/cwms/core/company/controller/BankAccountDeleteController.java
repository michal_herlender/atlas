package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.account.entity.bankaccount.BankAccount;
import org.trescal.cwms.core.account.entity.bankaccount.db.BankAccountService;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller @IntranetController
public class BankAccountDeleteController {
	@Autowired
	private BankAccountService bankAccountService; 
	
	@RequestMapping(value="/deletebankaccount.json", method=RequestMethod.GET)
	@ResponseBody
	public Boolean referenceData(@RequestParam(value="groupid", required=true) Integer baId) {
		
		
		
		// TODO: Check if Bank account is used in other companies
		BankAccount ba = this.bankAccountService.get(baId);
		if (ba != null && !this.bankAccountService.isUsedInCompanySettings(ba)) {
			this.bankAccountService.delete(ba);
			return true;
			
		}
		return false;
	}
}
