package org.trescal.cwms.core.company.form;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.account.entity.bankdetail.BankDetail;
import org.trescal.cwms.core.account.entity.companyaccount.CompanyAccount;
import org.trescal.cwms.core.account.entity.companyaccount.CompanyAccountSource;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class CompanyAccountValidator extends AbstractBeanValidator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.isAssignableFrom(CompanyAccountForm.class);
	}
	
	@Override
	public void validate(Object command, Errors errors)
	{
		CompanyAccountForm form = (CompanyAccountForm) command;

		if (form.getContactSource() == CompanyAccountSource.CUSTOM)
		{
			Contact contact = form.getAccountContact();
			Address address = form.getAccountAddress();
			// validate the custom accounts contact
			Set<ConstraintViolation<Contact>> contactViolations = super.getConstraintViolations(contact);
			for (ConstraintViolation<Contact> viol : contactViolations)
			{
				errors.rejectValue("accountContact." + viol.getPropertyPath(), null, viol.getMessage());
			}
			
			// validate the custom accounts address
			Set<ConstraintViolation<Address>> addressViolations = super.getConstraintViolations(address);
			for (ConstraintViolation<Address> viol : addressViolations)
			{
				if(!viol.getPropertyPath().toString().equals("country")){
					errors.rejectValue("accountAddress." + viol.getPropertyPath(), null, viol.getMessage());
				}	
			}
			// validate country
			if (form.getCountryId() == null)
			{
				errors.rejectValue("accountAddress.country", null, "You must enter a country for this address");
			}
		}
		else
		{
			// check a contact has been selected
			if (form.getPersonid() == null)
			{
				errors.rejectValue("personid", "", null, "A contact must be selected");
			}
			
			// check an adderss has been selected
			if (form.getAddressid() == null)
			{
				errors.rejectValue("addressid", "", null, "An address must be selected");
			}
			
		}

		// only validate the bank details if user has specified we include them
		if (form.isIncludeBankDetails())
		{
			Set<ConstraintViolation<BankDetail>> bankViolations = super.getConstraintViolations(form.getBankDetails());
			for (ConstraintViolation<BankDetail> viol : bankViolations)
			{
				errors.rejectValue("bankDetails." + viol.getPropertyPath(), null, viol.getMessage());
			}
		}

		// we can only really validate the company account note field on company
		// account
		Set<ConstraintViolation<CompanyAccount>> accountViolations = super.getConstraintViolations(form.getAccount());
		for (ConstraintViolation<CompanyAccount> viol : accountViolations)
		{
			errors.rejectValue("account." + viol.getPropertyPath(), null, viol.getMessage());
		}

		// Dimensions business logic - if settlement discount set then must set
		// settlement days
		if ((form.getAccount().getSettlementDiscount() != null)
				&& (form.getAccount().getSettlementDiscount().doubleValue() > 0))
		{
			if (form.getAccount().getSettlementDays() < 1)
			{
				errors.rejectValue("account.settlementDays", null, "Settlement days must be greater than 0 if a settlement discount is set");
			}
		}
	}
}