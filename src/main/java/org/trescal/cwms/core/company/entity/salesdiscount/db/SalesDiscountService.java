package org.trescal.cwms.core.company.entity.salesdiscount.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.salesdiscount.SalesDiscount;


public interface SalesDiscountService 
{
	SalesDiscount findSalesDiscount(String id);
	void updateSalesDiscount(SalesDiscount sd);
	List<SalesDiscount> getAllSalesDiscounts();
}