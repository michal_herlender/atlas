package org.trescal.cwms.core.company.entity.contact.db;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.criteria.CompoundSelection;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.MatchMode;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.dto.ContactSearchResultWrapper;
import org.trescal.cwms.core.company.dto.SearchEmailContactDto;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup_;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.User_;
import org.trescal.cwms.core.tools.OptionalsUtils;
import org.trescal.cwms.core.userright.entity.userrole.UserRole;

import lombok.val;

import static org.trescal.cwms.core.tools.StringJpaUtils.*;

@Repository("ContactDao")
public class ContactDaoImpl extends BaseDaoImpl<Contact, Integer> implements ContactDao {
	@Override
	protected Class<Contact> getEntity() {
		return Contact.class;
	}

	@Override
	public int countContactInstruments(int personid) {
		return getSingleResult(cb -> {
			val cq = cb.createQuery(Long.class);
			val inst = cq.from(Instrument.class);
			cq.where(cb.equal(inst.join(Instrument_.con), personid));
			cq.select(cb.count(cb.literal(1)));
			return cq;
		}).intValue();
	}

	@Override
	public Contact findDWRContact(int id) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(Contact.class);
			val con = cq.from(Contact.class);
			cq.where(cb.equal(con, id));
			con.fetch(Contact_.defAddress, JoinType.LEFT).fetch(Address_.locations, JoinType.LEFT);
			con.fetch(Contact_.user, JoinType.LEFT).fetch(User_.userRoles, JoinType.LEFT);
			con.fetch(Contact_.userPreferences, JoinType.LEFT);
			return cq;
		}).orElse(null);
	}

	@Override
	public Contact findEagerContact(int id) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(Contact.class);
			val con = cq.from(Contact.class);
			cq.where(cb.equal(con, id));
			con.fetch(Contact_.sub, JoinType.LEFT).fetch(Subdiv_.comp);
			con.fetch(Contact_.user, JoinType.LEFT);
			con.fetch(Contact_.userPreferences, JoinType.LEFT);
			return cq;
		}).orElse(null);
	}

	@Override
	public List<Contact> getActiveContactsWithRoleForSubdiv(int businessSubdivId) {
		return getResultList(cb -> {
			CriteriaQuery<Contact> cq = cb.createQuery(Contact.class);
			Root<Contact> contactRoot = cq.from(Contact.class);
			Join<Contact, User> users = contactRoot.join(Contact_.user);
			Join<User, UserRole> userRoles = users.join(User_.userRoles);
			Join<UserRole, Subdiv> subdiv = userRoles.join("organisation");
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), businessSubdivId));
			clauses.getExpressions().add(cb.equal(contactRoot.get(Contact_.active), true));
			cq.where(clauses);
			return cq.distinct(true);
		});
	}

	@Override
	public List<Contact> getAllActiveSubdivContacts(int subdivid) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Contact.class);
			val con = cq.from(Contact.class);
			cq.where(cb.and(cb.equal(con.get(Contact_.sub), subdivid), cb.isTrue(con.get(Contact_.active))));
			con.fetch(Contact_.user, JoinType.LEFT);
			con.fetch(Contact_.userPreferences, JoinType.LEFT);
			cq.distinct(true);
			cq.orderBy(cb.asc(con.get(Contact_.firstName)));
			return cq;
		});
	}

	@Override
	public List<Contact> getAllCompanyContacts(int coid) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Contact.class);
			val con = cq.from(Contact.class);
			cq.where(cb.equal(con.join(Contact_.sub).join(Subdiv_.comp), coid));
			con.fetch(Contact_.user, JoinType.LEFT);
			con.fetch(Contact_.userPreferences, JoinType.LEFT);
			cq.distinct(true);
			cq.orderBy(cb.asc(con.get(Contact_.firstName)));
			return cq;
		});
	}

	@Override
	public List<Contact> getAllCompanyContacts(int coid, boolean active) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Contact.class);
			val con = cq.from(Contact.class);
			cq.where(cb.and(cb.equal(con.join(Contact_.sub).join(Subdiv_.comp), coid),
					cb.isTrue(con.get(Contact_.active))));
			con.fetch(Contact_.user, JoinType.LEFT);
			con.fetch(Contact_.userPreferences, JoinType.LEFT);
			cq.distinct(true);
			cq.orderBy(cb.asc(con.get(Contact_.firstName)));
			return cq;
		});
	}

	@Override
	public Contact findCompanyContactByFullName(int coid, String fullname, boolean active) {

		return getFirstResult(cb -> {
			CriteriaQuery<Contact> cq = cb.createQuery(Contact.class);
			Root<Contact> con = cq.from(Contact.class);

			// full name
			val expr1 = trimAndConcatWithWhitespace(con.get(Contact_.firstName), con.get(Contact_.lastName)).apply(cb);
			val expr2 = trimAndConcatWithWhitespace(con.get(Contact_.lastName), con.get(Contact_.firstName)).apply(cb);
			Predicate fullNamePredicate = cb.or(ilike(cb, expr1, fullname, MatchMode.ANYWHERE),
					ilike(cb, expr2, fullname, MatchMode.ANYWHERE));

			// company id
			Predicate companyIdPredicate = cb.equal(con.get(Contact_.sub).get(Subdiv_.comp), coid);

			// contact active
			Predicate activePredicate = cb.equal(con.get(Contact_.active), active);

			cq.where(cb.and(fullNamePredicate, companyIdPredicate, activePredicate));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<Contact> getAllCompanyContactsBySubCon(int coid, Boolean active) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Contact.class);
			val con = cq.from(Contact.class);
			val sub = con.join(Contact_.sub);
			cq.where(cb.and(cb.equal(sub.join(Subdiv_.comp), coid), cb.isTrue(con.get(Contact_.active))));
			con.fetch(Contact_.user, JoinType.LEFT);
			con.fetch(Contact_.userPreferences, JoinType.LEFT);
			cq.distinct(true);
			cq.orderBy(
					// Removed subdiv sort, as results in SQL exception -
					// consider deprecating /
					// replacing entire method.
					// cb.asc(sub.get(Subdiv_.subname)),
					cb.asc(con.get(Contact_.firstName)), cb.asc(con.get(Contact_.lastName)));
			return cq;
		});
	}

	@Override
	public List<ContactKeyValue> getContactDtoList(Integer coid, Integer subdivid, Boolean active) {
		return getResultList(cb -> {
			CriteriaQuery<ContactKeyValue> cq = cb.createQuery(ContactKeyValue.class);
			Root<Contact> contact = cq.from(Contact.class);
			Predicate conjunction = cb.conjunction();

			if (coid != null || subdivid != null) {
				Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub, JoinType.INNER);
				if (subdivid != null) {
					conjunction.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), subdivid));
				}
				if (coid != null) {
					Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp, JoinType.INNER);
					conjunction.getExpressions().add(cb.equal(company.get(Company_.coid), coid));
				}
			}
			if (active != null) {
				conjunction.getExpressions().add(cb.equal(contact.get(Contact_.active), active));
			}
			cq.where(conjunction);
			cq.orderBy(cb.asc(contact.get(Contact_.firstName)), cb.asc(contact.get(Contact_.lastName)),
					cb.asc(contact.get(Contact_.personid)));
			cq.select(cb.construct(ContactKeyValue.class, contact.get(Contact_.personid),
					contact.get(Contact_.firstName), contact.get(Contact_.lastName)));
			return cq;
		});
	}

	@Override
	public List<ContactProjectionDTO> getContactProjectionDTOs(Collection<Integer> contactIds,
			Integer allocatedCompanyId) {
		return getResultList(cb -> {
			CriteriaQuery<ContactProjectionDTO> cq = cb.createQuery(ContactProjectionDTO.class);
			Root<Contact> root = cq.from(Contact.class);
			Join<Contact, Subdiv> subdiv = root.join(Contact_.sub, JoinType.INNER);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp, JoinType.INNER);
			Join<Company, CompanySettingsForAllocatedCompany> settings = company
					.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			settings.on(cb.equal(settings.get(CompanySettingsForAllocatedCompany_.organisation.getName()),
					allocatedCompanyId));
			cq.where(root.get(Contact_.personid).in(contactIds));
//			cq.where(root.get(Contact_.personid).in(contactIds),
//					cb.equal(settingsCompany.get(Company_.coid), allocatedCompanyId));
			CompoundSelection<ContactProjectionDTO> selection = cb.construct(ContactProjectionDTO.class,
					root.get(Contact_.personid), root.get(Contact_.firstName), root.get(Contact_.lastName),
					root.get(Contact_.email), root.get(Contact_.telephone), root.get(Contact_.active),
					subdiv.get(Subdiv_.subdivid), subdiv.get(Subdiv_.subname), subdiv.get(Subdiv_.active),
					company.get(Company_.coid), company.get(Company_.coname),
					settings.get(CompanySettingsForAllocatedCompany_.active),
					settings.get(CompanySettingsForAllocatedCompany_.onStop));

			cq.select(selection);

			return cq;
		});
	}

	@Override
	public List<Contact> getAllOrderedContacts() {
		return getResultList(cb -> {
			val cq = cb.createQuery(Contact.class);
			val con = cq.from(Contact.class);
			cq.orderBy(cb.asc(con.get(Contact_.firstName)));
			return cq;
		});
	}

	@Override
	public List<Contact> getAllSubdivContacts(int subdivid) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Contact.class);
			val con = cq.from(Contact.class);
			cq.where(cb.equal(con.get(Contact_.sub), subdivid));
			con.fetch(Contact_.user, JoinType.LEFT);
			con.fetch(Contact_.userPreferences, JoinType.LEFT);
			cq.distinct(true);
			cq.orderBy(cb.asc(con.get(Contact_.firstName)));
			return cq;
		});
	}

	@Override
	public List<ContactSearchResultWrapper> getAllSubdivContactsToBookJobsHQL(int subdivid, String groupsList) {
		val groupList = Stream.of(groupsList.split(",")).map(String::trim).collect(Collectors.toList());
		return getResultList(cb -> {
			val cq = cb.createQuery(ContactSearchResultWrapper.class);
			val con = cq.from(Contact.class);
			val sub = con.join(Contact_.sub, JoinType.LEFT);
			val com = sub.join(Subdiv_.comp, JoinType.LEFT);
			val userGroup = con.join(Contact_.usergroup, JoinType.LEFT);
			val defCon = sub.join(Subdiv_.defaultContact, JoinType.LEFT);
			val fullName = trimAndConcatWithWhitespace(con.get(Contact_.firstName), con.get(Contact_.lastName))
					.apply(cb);
			cq.orderBy(cb.asc(fullName));
			cq.where(cb.and(cb.equal(sub, subdivid), userGroup.get(UserGroup_.groupid).in(groupList),
					cb.isTrue(con.get(Contact_.active))

			));
			cq.select(cb.construct(ContactSearchResultWrapper.class, con.get(Contact_.personid), fullName,
					com.get(Company_.coid), com.get(Company_.coname), com.get(Company_.companyRole),
					sub.get(Subdiv_.subname), userGroup.get(UserGroup_.name),
					cb.selectCase().when(cb.equal(con, defCon), true).otherwise(false)));
			return cq;
		});
	}

	@Override
	public List<ContactKeyValue> getBusinessContacts(boolean activeOnly) {
		return getResultList(cb -> {
			CriteriaQuery<ContactKeyValue> cq = cb.createQuery(ContactKeyValue.class);
			Root<Contact> contact = cq.from(Contact.class);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(company.get(Company_.companyRole), CompanyRole.BUSINESS));
			if (activeOnly)
				clauses.getExpressions().add(cb.isTrue(contact.get(Contact_.active)));
			cq.where(clauses);		
			cq.orderBy(cb.asc(contact.get(Contact_.firstName)), cb.asc(contact.get(Contact_.lastName)),
					cb.asc(contact.get(Contact_.personid)));
			cq.select(cb.construct(ContactKeyValue.class, contact.get(Contact_.personid),
					contact.get(Contact_.firstName), contact.get(Contact_.lastName)));
			return cq;
		});
	}

	@Override
	public List<Contact> getContactsInUserGroup(int groupid) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Contact.class);
			val con = cq.from(Contact.class);
			val userGroup = con.join(Contact_.usergroup, JoinType.LEFT);
			cq.where(cb.equal(userGroup, groupid));
			return cq;
		});
	}

	@Override
	public List<ContactSearchResultWrapper> searchByCompanyRoles(String firstName, String lastName, boolean active,
			List<CompanyRole> companyRoles) {
		return getResultList(cb -> {
			val cq = cb.createQuery(ContactSearchResultWrapper.class);
			val con = cq.from(Contact.class);
			val sub = con.join(Contact_.sub);
			val comp = sub.join(Subdiv_.comp);
			val fullName = trimAndConcatWithWhitespace(con.get(Contact_.firstName), con.get(Contact_.lastName))
					.apply(cb);
			cq.select(cb.construct(ContactSearchResultWrapper.class, con.get(Contact_.personid), fullName,
					comp.get(Company_.coid), comp.get(Company_.coname), comp.get(Company_.companyRole),
					sub.get(Subdiv_.subname)));
			cq.where(cb.and(Stream
					.of(Optional.ofNullable(firstName).filter(s -> !s.isEmpty())
							.map(s -> ilike(cb, con.get(Contact_.firstName), s, MatchMode.START)),
							Optional.ofNullable(lastName).filter(s -> !s.isEmpty())
									.map(s -> ilike(cb, con.get(Contact_.lastName), s, MatchMode.START)),
							Optional.of(cb.equal(con.get(Contact_.active), active)),
							Optional.of(comp.get(Company_.companyRole).in(companyRoles)))
					.flatMap(OptionalsUtils::optionalToStream).toArray(Predicate[]::new)));
			cq.orderBy(cb.asc(con.get(Contact_.firstName)));
			return cq;
		});
	}

	@Override
	public List<Contact> searchContact(String firstName, String lastName, boolean active) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Contact.class);
			val con = cq.from(Contact.class);
			cq.where(cb.and(Stream
					.of(Optional.ofNullable(firstName).filter(s -> !s.isEmpty())
							.map(s -> ilike(cb, con.get(Contact_.firstName), s, MatchMode.START)),
							Optional.ofNullable(lastName).filter(s -> !s.isEmpty())
									.map(s -> ilike(cb, con.get(Contact_.lastName), s, MatchMode.START)),
							Optional.of(cb.equal(con.get(Contact_.active), active)))
					.flatMap(OptionalsUtils::optionalToStream).toArray(Predicate[]::new)));
			cq.orderBy(cb.asc(con.get(Contact_.firstName)));
			return cq;
		});
	}

	@Override
	public  List<ContactSearchResultWrapper> searchContactsByRolesAndFullName(String fullName, Boolean active, List<CompanyRole> roles){
		return getResultList(cb -> {
			val cq = cb.createQuery(ContactSearchResultWrapper.class);
			val con = cq.from(Contact.class);
			val sub = con.join(Contact_.sub);
			val comp = sub.join(Subdiv_.comp);
			val fullNameExp = trimAndConcatWithWhitespace(con.get(Contact_.firstName), con.get(Contact_.lastName))
					.apply(cb);
			cq.select(cb.construct(ContactSearchResultWrapper.class, con.get(Contact_.personid), fullNameExp,
					comp.get(Company_.coid), comp.get(Company_.coname), comp.get(Company_.companyRole),
					sub.get(Subdiv_.subname)));

			cq.where(Stream
					.of(ilikePredicateStream(cb,fullNameExp,fullName,MatchMode.ANYWHERE),
							Stream.of(cb.equal(con.get(Contact_.active), active)),
							Stream.of(comp.get(Company_.companyRole).in(roles)))
					.flatMap(Function.identity()).toArray(Predicate[]::new));
			cq.orderBy(cb.asc(con.get(Contact_.firstName)));
			return cq;
		},0,50);
	}

	@Override
	public List<Contact> searchContactByRoles(String firstName, String lastName, boolean active,
			List<CompanyRole> companyRoles) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Contact.class);
			val con = cq.from(Contact.class);
			val sub = con.join(Contact_.sub);
			val comp = sub.join(Subdiv_.comp);
			cq.where(cb.and(Stream
					.of(Optional.ofNullable(firstName).filter(s -> !s.isEmpty())
							.map(s -> ilike(cb, con.get(Contact_.firstName), s, MatchMode.START)),
							Optional.ofNullable(lastName).filter(s -> !s.isEmpty())
									.map(s -> ilike(cb, con.get(Contact_.lastName), s, MatchMode.START)),
							Optional.of(cb.equal(con.get(Contact_.active), active)),
							Optional.of(comp.get(Company_.companyRole).in(companyRoles)))
					.flatMap(OptionalsUtils::optionalToStream).toArray(Predicate[]::new)));
			cq.orderBy(cb.asc(con.get(Contact_.firstName)));
			return cq;
		});
	}

	@Override
	public List<Contact> searchContactCaseInsensitive(int subdivid, String firstName, String lastName) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Contact.class);
			val con = cq.from(Contact.class);
			val sub = con.join(Contact_.sub);

			cq.where(cb.and(Stream
					.of(Optional.ofNullable(firstName).filter(s -> !s.isEmpty())
							.map(s -> ilike(cb, con.get(Contact_.firstName), s, MatchMode.EXACT)),
							Optional.ofNullable(lastName).filter(s -> !s.isEmpty())
									.map(s -> ilike(cb, con.get(Contact_.lastName), s, MatchMode.EXACT)),
							Optional.of(cb.equal(sub, subdivid)))
					.flatMap(OptionalsUtils::optionalToStream).toArray(Predicate[]::new)));
			return cq;
		});
	}

	@Override
	public List<ContactSearchResultWrapper> searchContactsHQL(String contactName, String subdivName, String companyName,
			List<CompanyRole> companyRoles, String searchOrder, Integer businessCoId) {

		return getResultList(cb -> {
			val cq = cb.createQuery(ContactSearchResultWrapper.class);
			val con = cq.from(Contact.class);
			val sub = con.join(Contact_.sub);
			val comp = sub.join(Subdiv_.comp);
			val fullName = trimAndConcatWithWhitespace(con.get(Contact_.firstName), con.get(Contact_.lastName))
					.apply(cb);
			cq.select(cb.construct(ContactSearchResultWrapper.class, con.get(Contact_.personid), fullName,
					comp.get(Company_.coid), comp.get(Company_.coname), comp.get(Company_.companyRole),
					sub.get(Subdiv_.subname)));
			cq.where(cb.and(cb.isTrue(con.get(Contact_.active)), comp.get(Company_.companyRole).in(companyRoles),
					ilike(cb,
							trimAndConcatWithWhitespace(con.get(Contact_.firstName), con.get(Contact_.lastName))
									.apply(cb),
							contactName, MatchMode.ANYWHERE),
					ilike(cb, comp.get(Company_.coname), companyName, MatchMode.START),
					ilike(cb, sub.get(Subdiv_.subname), subdivName, MatchMode.START),
					cb.equal(comp.join(Company_.settingsForAllocatedCompanies)
							.join(CompanySettingsForAllocatedCompany_.organisation), businessCoId)));
			if (searchOrder.equals("company"))
				cq.orderBy(cb.asc(comp.get(Company_.coname)), cb.asc(sub.get(Subdiv_.subname)),
						cb.asc(con.get(Contact_.firstName)), cb.asc(con.get(Contact_.lastName)));
			else
				cq.orderBy(cb.asc(con.get(Contact_.firstName)), cb.asc(con.get(Contact_.lastName)));
			return cq;
		});
	}

	@Override
	public List<SearchEmailContactDto> searchEmailContacts(String searchName, List<Integer> coIds) {
		if ((coIds == null || coIds.isEmpty() || coIds.stream().allMatch(i -> i == 0))
				&& checkIfNullOrEmpty(searchName))
			throw new IllegalArgumentException("Both coid and search name cannot be empty");
		return getResultList(cb -> {
			val cq = cb.createQuery(SearchEmailContactDto.class);
			val contact = cq.from(Contact.class);
			val sub = contact.join(Contact_.sub);
			val firstName = contact.get(Contact_.firstName);
			val lastName = contact.get(Contact_.lastName);
			val firstLastName = trimAndConcatWithWhitespace(firstName, lastName).apply(cb);
			Stream<Predicate> namePredicates = !checkIfNullOrEmpty(searchName)
					? Stream.of(cb.or(ilike(cb, firstName, searchName, MatchMode.START),
							ilike(cb, lastName, searchName, MatchMode.START),
							ilike(cb, firstLastName, searchName, MatchMode.START)))
					: Stream.empty();
			Stream<Predicate> coIdsPredicate = (coIds != null && !coIds.isEmpty())
					? Stream.of(sub.join(Subdiv_.comp).in(coIds))
					: Stream.empty();
			cq.where(Stream.concat(coIdsPredicate, namePredicates).toArray(Predicate[]::new));
			cq.select(cb.construct(SearchEmailContactDto.class, contact.get(Contact_.personid), firstLastName,
					contact.get(Contact_.email), sub.get(Subdiv_.subname)));
			return cq;
		});
	}

	@Override
	public List<ContactSearchResultWrapper> webSearchContactHQL(Contact contact, String firstName, String lastName,
			boolean active, Company allocatedCompany) {
		if (contact == null)
			return Collections.emptyList();
		return getResultList(cb -> {
			val cq = cb.createQuery(ContactSearchResultWrapper.class);
			val con = cq.from(Contact.class);
			val sub = con.join(Contact_.sub);
			val comp = sub.join(Subdiv_.comp);
			val fullName = trimAndConcatWithWhitespace(con.get(Contact_.firstName), con.get(Contact_.lastName))
					.apply(cb);
			cq.select(cb.construct(ContactSearchResultWrapper.class, con.get(Contact_.personid), fullName,
					comp.get(Company_.coid), comp.get(Company_.coname), comp.get(Company_.companyRole),
					sub.get(Subdiv_.subname)));
			cq.where(cb.and(ilike(cb, con.get(Contact_.firstName), firstName, MatchMode.START),
					ilike(cb, con.get(Contact_.lastName), lastName, MatchMode.START),
					cb.equal(con.get(Contact_.active), active), sub.in(contact.getSub().getComp().getSubdivisions())));
			cq.orderBy(cb.asc(con.get(Contact_.firstName)), cb.asc(con.get(Contact_.lastName)),
					cb.asc(con.get(Contact_.personid)));
			return cq;
		});
	}

	@Override
	public boolean getHrIdExists(String hrid) {
		return getFirstResult(cb -> {
			CriteriaQuery<Long> q = cb.createQuery(Long.class);
			Root<Contact> r = q.from(Contact.class);
			q.select(cb.count(r)).where(cb.equal(r.get(Contact_.hrid), hrid));
			return q;
		}).filter(cnt -> cnt > 0).isPresent();
	}

	@Override
	public boolean getHrIdExistsAndIgnoreContact(String hrid, Contact contact) {
		return getFirstResult(cb -> {
			CriteriaQuery<Long> q = cb.createQuery(Long.class);
			Root<Contact> r = q.from(Contact.class);
			q.select(cb.count(r)).where(cb.and(cb.equal(r.get(Contact_.hrid), hrid),
					cb.notEqual(r.get(Contact_.personid), contact.getPersonid())));
			return q;
		}).filter(cnt -> cnt > 0).isPresent();
	}

	@Override
	public Contact getByHrId(String hrid) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(Contact.class);
			val r = cq.from(Contact.class);
			cq.where(cb.equal(r.get(Contact_.hrid), hrid));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<Contact> getByHrIds(List<String> hrids) {
		if (hrids == null || hrids.stream().noneMatch(StringUtils::isNoneBlank))
			return Collections.emptyList();
		return getResultList(cb -> {
			CriteriaQuery<Contact> q = cb.createQuery(Contact.class);
			Root<Contact> r = q.from(Contact.class);
			q.distinct(true);
			q.where(r.get(Contact_.hrid).in(hrids));
			return q;
		});
	}

	public Contact getContactByFirstnameAndLastname(Integer subdivid, String firstName, String lastName) {

		return getFirstResult(cb -> {
			CriteriaQuery<Contact> cq = cb.createQuery(Contact.class);
			Root<Contact> contact = cq.from(Contact.class);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(contact.get(Contact_.firstName), firstName));
			clauses.getExpressions().add(cb.equal(contact.get(Contact_.lastName), lastName));
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), subdivid));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

	@Override
	public Optional<Contact> searchContactByFullName(String fullName, boolean active) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(Contact.class);
			val con = cq.from(Contact.class);
			val fullNameExpr = cb.or(
					ilike(cb,
							trimAndConcatWithWhitespace(con.get(Contact_.firstName), con.get(Contact_.lastName))
									.apply(cb),
							fullName, MatchMode.ANYWHERE),
					ilike(cb, trimAndConcatWithWhitespace(con.get(Contact_.lastName), con.get(Contact_.firstName))
							.apply(cb), fullName, MatchMode.ANYWHERE));
			cq.where(cb.and(fullNameExpr, cb.equal(con.get(Contact_.active), active)));
			return cq;
		});
	}

	@Override
	public List<ContactSearchResultWrapper> getAllActiveContactsSearchResultWrapperProjection(int subdivid) {
		return getResultList(cb -> {
			
			CriteriaQuery<ContactSearchResultWrapper> cq = cb.createQuery(ContactSearchResultWrapper.class);
			Root<Contact> con = cq.from(Contact.class);
			val sub = con.join(Contact_.sub);
			val defaulContact = sub.get(Subdiv_.defaultContact);
			Predicate andClauses = cb.conjunction();

			andClauses.getExpressions().add(cb.equal(con.get(Contact_.sub), subdivid));
			andClauses.getExpressions().add(cb.equal(con.get(Contact_.active), true));

			cq.where(andClauses);

			cq.select(cb.construct(ContactSearchResultWrapper.class,
					sub.get(Subdiv_.comp).get(Company_.coid),
					sub.get(Subdiv_.comp).get(Company_.coname),
					sub.get(Subdiv_.comp).get(Company_.companyRole),
					cb.selectCase().when(cb.isNull(defaulContact), con.get(Contact_.personid))
				      .otherwise(defaulContact.get(Contact_.personid)),
					con.get(Contact_.firstName), con.get(Contact_.lastName), con.get(Contact_.personid),
					con.get(Contact_.sub).get(Subdiv_.subname)));

			return cq;
		});
	}

	@Override
	public ContactProjectionDTO getContactProjectionDTO(Integer contactId) {
		return getFirstResult(cb -> {
			CriteriaQuery<ContactProjectionDTO> cq = cb.createQuery(ContactProjectionDTO.class);
			Root<Contact> root = cq.from(Contact.class);
			Join<Contact, Subdiv> subdiv = root.join(Contact_.sub, JoinType.INNER);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp, JoinType.INNER);
			Join<Company, CompanySettingsForAllocatedCompany> settings = company
					.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			cq.where(cb.equal(root.get(Contact_.personid), contactId));
			CompoundSelection<ContactProjectionDTO> selection = cb.construct(ContactProjectionDTO.class,
					root.get(Contact_.personid), root.get(Contact_.firstName), root.get(Contact_.lastName),
					root.get(Contact_.email), root.get(Contact_.telephone), root.get(Contact_.active),
					subdiv.get(Subdiv_.subdivid), subdiv.get(Subdiv_.subname), subdiv.get(Subdiv_.active),
					company.get(Company_.coid), company.get(Company_.coname),
					settings.get(CompanySettingsForAllocatedCompany_.active),
					settings.get(CompanySettingsForAllocatedCompany_.onStop));

			cq.select(selection);

			return cq;
		}).orElse(null);
	}

}