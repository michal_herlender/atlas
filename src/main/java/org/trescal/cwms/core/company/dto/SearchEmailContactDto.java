package org.trescal.cwms.core.company.dto;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class SearchEmailContactDto {
    Integer personid;
    String name;
    String email;
    String subName;
}
