package org.trescal.cwms.core.company.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.account.entity.bankaccount.BankAccount;
import org.trescal.cwms.core.account.entity.bankaccount.db.BankAccountService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.form.BankAccountForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;

@Controller
@IntranetController
public class BankAccountAddEditController {

    public static final String FORM_NAME = "form";
    public static final String REFERENCE_NAME = "reference";
    public static final String VIEW_NAME = "trescal/core/company/bankaccount";
    public static final String VIEW_NAME_COMPANY = "viewcomp.htm?loadtab=bankaccounts-tab&coid=";
    public static final String PARAM_BANK_ACCOUNT_ID = "accountId";
    public static final String PARAM_COMPANY_ID = "companyId";

    private BankAccountService bankAccountService;
    private CompanyService companyService;
    private SupportedCurrencyService supportedCurrencyService;

    public BankAccountAddEditController(BankAccountService bankAccountService, CompanyService companyService, SupportedCurrencyService supportedCurrencyService) {
        this.bankAccountService = bankAccountService;
        this.companyService = companyService;
        this.supportedCurrencyService = supportedCurrencyService;
    }

    @ModelAttribute(FORM_NAME)
    public BankAccountForm formBackingObject(@RequestParam(value = PARAM_BANK_ACCOUNT_ID, required = false) Integer bankAccountId,
                                             @RequestParam(value = PARAM_COMPANY_ID, required = false) Integer companyId) {
        if (bankAccountId != null) {
            BankAccount bankAccount = bankAccountService.get(bankAccountId);
            if (bankAccount == null)
                throw new IllegalArgumentException("There isn't a bank account for the given identifier!");
            return BankAccountForm.builder()
                    .entityId(bankAccount.getCompany().getCoid())
                    .bankAccountId(bankAccountId)
                    .accountNo(bankAccount.getAccountNo())
                    .bankName(bankAccount.getBankName())
                    .currencyId(bankAccount.getCurrency().getCurrencyId())
                    .iban(bankAccount.getIban())
                    .swiftBic(bankAccount.getSwiftBic())
                    .includeOnFactoredInvoices(bankAccount.getIncludeOnFactoredInvoices())
                    .includeOnNonFactoredInvoices(bankAccount.getIncludeOnNonFactoredInvoices())
                    .build();
        } else if (companyId != null) {
            Company company = companyService.get(companyId);
            if (company == null) throw new IllegalArgumentException("There isn't a company for the given identifier!");
            return BankAccountForm.builder()
                    .entityId(companyId)
                    .accountNo("")
                    .bankName("")
                    .currencyId(company.getCurrency() == null ? supportedCurrencyService.getDefaultCurrency().getCurrencyId() : company.getCurrency().getCurrencyId())
                    .iban("")
                    .swiftBic("")
                    .includeOnFactoredInvoices(false)
                    .includeOnNonFactoredInvoices(false)
                    .build();
        } else throw new IllegalArgumentException("One of the parameters accountId or companyId must be present");
    }

    @GetMapping({"/addbankaccount.htm", "/editbankaccount.htm"})
    public ModelAndView referenceData(@RequestParam(PARAM_COMPANY_ID) int companyId) {
        Company company = companyService.get(companyId);
        if (company == null) throw new IllegalArgumentException("There isn't a company for the given identifier!");
        Map<String, Object> model = new HashMap<>();
        model.put("currencies", supportedCurrencyService.getAllSupportedCurrencys());
        model.put("company", company);
        return new ModelAndView(VIEW_NAME, REFERENCE_NAME, model);
    }

    private void bindOnEntity(BankAccount bankAccount, BankAccountForm form) {
        bankAccount.setAccountNo(form.getAccountNo());
        bankAccount.setBankName(form.getBankName());
        Company company = companyService.get(form.getEntityId());
        bankAccount.setCompany(company);
        SupportedCurrency currency = supportedCurrencyService.findSupportedCurrency(form.getCurrencyId());
        bankAccount.setCurrency(currency);
        bankAccount.setIban(form.getIban());
        bankAccount.setSwiftBic(form.getSwiftBic());
        bankAccount.setIncludeOnFactoredInvoices(form.getIncludeOnFactoredInvoices());
        bankAccount.setIncludeOnNonFactoredInvoices(form.getIncludeOnNonFactoredInvoices());
    }

    /*
        Split post mappings, so that you cannot add or edit bank accounts, if you haven't the right permission
     */

    @PostMapping("/addbankaccount.htm")
    public ModelAndView onSubmitAdd(@Valid @ModelAttribute(FORM_NAME) BankAccountForm form, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return referenceData(form.getEntityId());
        BankAccount bankAccount = new BankAccount();
        bindOnEntity(bankAccount, form);
        bankAccountService.save(bankAccount);
        return new ModelAndView(new RedirectView(VIEW_NAME_COMPANY + form.getEntityId()));
    }

    @PostMapping("/editbankaccount.htm")
    public ModelAndView onSubmitEdit(@Valid @ModelAttribute(FORM_NAME) BankAccountForm form, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return referenceData(form.getEntityId());
        BankAccount bankAccount = this.bankAccountService.get(form.getBankAccountId());
        bindOnEntity(bankAccount, form);
        bankAccountService.merge(bankAccount);
        return new ModelAndView(new RedirectView(VIEW_NAME_COMPANY + form.getEntityId()));
    }
}