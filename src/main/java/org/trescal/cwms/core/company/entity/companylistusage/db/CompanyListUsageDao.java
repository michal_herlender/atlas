package org.trescal.cwms.core.company.entity.companylistusage.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.companylistusage.CompanyListUsage;

public interface CompanyListUsageDao extends BaseDao<CompanyListUsage, Integer> {

	List<CompanyListUsage> getUsageByCompanyList(Integer companyListId);
}
