package org.trescal.cwms.core.company.controller;

import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.dto.ContactSearchResultWrapper;
import org.trescal.cwms.core.company.dto.SearchEmailContactDto;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME,
		Constants.SESSION_ATTRIBUTE_COMPANY })
@JsonController
public class ContactAjaxController {

	@Autowired
	private UserService userService;

	@Autowired
	private ContactService contactService;
	@Autowired
	private JobService jobServ;
	@Autowired
	private SubdivService subdivServ;

	@RequestMapping(value = "resetPassword.json", method = RequestMethod.PUT)
	@ResponseBody
	public ResultWrapper resetPassword(@RequestParam Integer personId) {
		return new ResultWrapper(true, userService.resetPassword(personId));
	}

	@GetMapping("searchEmails.json")
	public Either<String, List<SearchEmailContactDto>> searchEmails(@RequestParam(required = false) String name,
			@RequestParam(required = false) List<Integer> coIds) {
		return contactService.searchEmailContact(name, coIds);
	}

	@GetMapping("searchContacts.json")
	public List<ContactSearchResultWrapper> searchContacts(
			@RequestParam(value="fullName", required=false, defaultValue="") String fullName,
			@RequestParam(value="active", required=false, defaultValue="true") Boolean active,
			@RequestParam(value="roles", required=false, defaultValue="") List<CompanyRole> roles
	){
		return contactService.searchContactsByRolesAndFullName(fullName,active,roles);
	}

	@GetMapping("bc.json")
	public KeyValue<Integer, String> getBc(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		return companyDto;
	}

	@GetMapping("checkContactDeactivationStatus.json")
	public Map<String, Integer> checkContactDeactivationStatus(
			@RequestParam(name = "personId", required = true) Integer personId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> allocatedSubdiv) {

		Map<String, Integer> map = new HashMap<>();
		map.put("instcount", this.contactService.checkContactDeactivationStatus(personId));
		map.put("activeJobs", this.jobServ.countActiveByContactAndAllocatedSubdiv(this.contactService.get(personId),
				this.subdivServ.get(allocatedSubdiv.getKey())).intValue());

		return map;
	}
}