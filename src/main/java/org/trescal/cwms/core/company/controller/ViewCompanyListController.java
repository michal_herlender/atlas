package org.trescal.cwms.core.company.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companylist.CompanyList;
import org.trescal.cwms.core.company.entity.companylist.db.CompanyListService;
import org.trescal.cwms.core.company.entity.companylistmember.db.CompanyListMemberService;
import org.trescal.cwms.core.company.entity.companylistusage.db.CompanyListUsageService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.form.EditCompanyListForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class ViewCompanyListController {

	@Autowired
	CompanyListService companyListService;
	@Autowired
	CompanyListMemberService companyListMemberService;
	@Autowired
	CompanyListUsageService companyListUsageService;
	@Autowired
	CompanyService companyService;

	public static final String FORM_NAME = "editcompanylistform";
	public static final String REQUEST_URL = "/viewcompanylist.htm";
	public static final String VIEW_NAME = "trescal/core/company/viewcompanylist";

	@ModelAttribute(FORM_NAME)
	protected EditCompanyListForm formBackingObject() {
		return new EditCompanyListForm();
	}

	@RequestMapping(value = REQUEST_URL, method = RequestMethod.GET)
	protected ModelAndView referenceData(Model model, 
			@RequestParam(value = "id", required = true) Integer id,
			@ModelAttribute(FORM_NAME) EditCompanyListForm form) throws Exception {

		CompanyList companyList = this.companyListService.findCompanyList(id);

		Map<String, Object> modelMap = model.asMap();
		
		if ((companyListService == null)) {
			throw new Exception("Company could not be found");
		}

		model.addAttribute("companylist", companyList);
		model.addAttribute("companylistBusinessCompany", companyList.getOrganisation().getConame());
		model.addAttribute("members", companyListMemberService.getMembersByCompanyList(companyList.getId()));
		model.addAttribute("users", companyListUsageService.getUsageByCompanyList(companyList.getId()));
		model.addAttribute("errorMessage", modelMap.get("errorMessage"));

		return new ModelAndView(VIEW_NAME);
	}

	@RequestMapping(value = "/addcompanylistmember.htm", method = RequestMethod.POST)
	public ModelAndView onSubmit_addmembertocompanylist(Model model, 
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(FORM_NAME) EditCompanyListForm form) {
		companyListMemberService.createNewCompanyListMember(form, username);
		return new ModelAndView(new RedirectView("viewcompanylist.htm?id=" + form.getCompanylistId()));
	}
	
	@RequestMapping(value = "/addcompanylistmember.htm", method = RequestMethod.GET)
	public ModelAndView addmembertocompanylist(Model model, @RequestParam("companylistid") String companylistid) {
		
		CompanyList companylist = companyListService.get(Integer.parseInt(companylistid));
		// get all company ids that are members of the company list
		List<Integer> memberCoids = new ArrayList<>();
		companyListMemberService.getMembersByCompanyList(Integer.parseInt(companylistid)).stream().forEach(member -> {
			memberCoids.add(member.getCompany().getCoid());
		});
		// be able to add active SUPPLIER and BUSINESS companies as a member of the company list
		List<CompanyRole> roles = new ArrayList<>();
		roles.add(CompanyRole.SUPPLIER);
		roles.add(CompanyRole.BUSINESS);
		List<KeyValueIntegerString> possibleMembers = companyService.getCompaniesByRoleForAllocatedCompany(roles,
				companyService.get(companylist.getOrganisation().getCoid()), memberCoids);
		model.addAttribute("companylist", companylist);
		model.addAttribute("possiblemembers", possibleMembers);
		return new ModelAndView("trescal/core/company/addcompanylistmember");
	}
	
	@RequestMapping(value = "/addcompanylistusage.htm", method = RequestMethod.POST)
	public ModelAndView onSubmit_addmembertocompanyusage(Model model, 
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(FORM_NAME) EditCompanyListForm form) {
		
		companyListUsageService.createNewCompanyListUsage(form, username);
		return new ModelAndView(new RedirectView("viewcompanylist.htm?id=" + form.getCompanylistId() + "&loadtab=usage-tab"));
	}
	
	@RequestMapping(value = "/addcompanylistusage.htm", method = RequestMethod.GET)
	public ModelAndView addmembertocompanyusage(Model model, @RequestParam("companylistid") String companylistid) {
		
		CompanyList companylist = companyListService.get(Integer.parseInt(companylistid));
		// get all company ids that use the company list
		List<Integer> companyListUsageCoids = new ArrayList<>();
		companyListUsageService.getUsageByCompanyList(Integer.parseInt(companylistid)).stream().forEach(member -> {
			companyListUsageCoids.add(member.getCompany().getCoid());
		});
		// be able to add active BUSINESS or CLIENT companies to the usage of the list
		List<CompanyRole> roles = new ArrayList<>();
		roles.add(CompanyRole.CLIENT);
		roles.add(CompanyRole.BUSINESS);
		List<KeyValueIntegerString> possibleCompanyListUsage = companyService.getCompaniesByRoleForAllocatedCompany(roles,
				companyService.get(companylist.getOrganisation().getCoid()), companyListUsageCoids);
		
		model.addAttribute("possibleusers", possibleCompanyListUsage);
		model.addAttribute("companylist", companylist);
		return new ModelAndView("trescal/core/company/addcompanylistusage");
	}
	
	@RequestMapping(value = "/deletecompanylistmember.json", method = RequestMethod.POST)
	@ResponseBody
	public boolean deleteCompanyListMember(@RequestParam(value = "companyListMemberId", required = true) Integer companyListMemberId) {
		return companyListMemberService.remove(companyListMemberId);
	}
	
	@RequestMapping(value = "/deletecompanylistusage.json", method = RequestMethod.POST)
	@ResponseBody
	public boolean deleteCompanyListUsage(@RequestParam(value = "companyListUsageId", required = true) Integer companyListUsageId) {
		return companyListUsageService.remove(companyListUsageId);
	}
	
}
