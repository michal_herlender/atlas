package org.trescal.cwms.core.company.entity.title.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.title.Title;

public interface TitleDao extends BaseDao<Title, Integer> {}