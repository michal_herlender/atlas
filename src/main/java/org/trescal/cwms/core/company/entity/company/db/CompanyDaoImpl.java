package org.trescal.cwms.core.company.entity.company.db;

import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.dto.CompanyDTO;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.dto.CompanySearchResultWrapper;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.company.projection.CompanyProjectionDTO;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Repository("CompanyDao")
public class CompanyDaoImpl extends BaseDaoImpl<Company, Integer> implements CompanyDao {

	// Restrict results of company searches to reasonable number, otherwise
	// searching for 'A', 'AB' marshals long lists of data.
	public static int MAX_RESULTS_WRAPPER = 50;

	@Override
	protected Class<Company> getEntity() {
		return Company.class;
	}

	@Override
	public List<CompanyKeyValue> findCustomerCompanies(Contact contact, Scope scope, Boolean active) {
		return getResultList(cb -> {
			CriteriaQuery<CompanyKeyValue> cq = cb.createQuery(CompanyKeyValue.class);
			Root<Company> company = cq.from(Company.class);
			Join<Company, CompanySettingsForAllocatedCompany> settings = company
					.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			settings.on(cb.equal(settings.get(CompanySettingsForAllocatedCompany_.organisation),
					contact.getSub().getComp()));
			Join<Company, Contact> defaultContact = company.join(Company_.defaultBusinessContact);
			Join<Contact, Subdiv> defaultSubdiv = defaultContact.join(Contact_.sub);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(company.get(Company_.companyRole), CompanyRole.CLIENT));
			if (active)
				clauses.getExpressions().add(cb.isTrue(settings.get(CompanySettingsForAllocatedCompany_.active)));
			switch (scope) {
			case COMPANY:
				clauses.getExpressions().add(cb.equal(defaultSubdiv.get(Subdiv_.comp), contact.getSub().getComp()));
				break;
			case CONTACT:
				clauses.getExpressions().add(cb.equal(company.get(Company_.defaultBusinessContact), contact));
				break;
			case SUBDIV:
				clauses.getExpressions().add(cb.equal(defaultContact.get(Contact_.sub), contact.getSub()));
				break;
			case SYSTEM:
			}
			cq.where(clauses);
			cq.select(cb.construct(CompanyKeyValue.class, company.get(Company_.coid), company.get(Company_.coname)));
			return cq;
		});
	}

	@SuppressWarnings("unused")
	@Override
	public List<CompanyKeyValue> findCustomerCompaniesBySubdiv(Subdiv subdiv, Boolean active) {
		return getResultList(cb -> {
			CriteriaQuery<CompanyKeyValue> cq = cb.createQuery(CompanyKeyValue.class);
			Root<Company> company = cq.from(Company.class);
			Join<Company, CompanySettingsForAllocatedCompany> settings = company
					.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			settings.on(cb.equal(settings.get(CompanySettingsForAllocatedCompany_.organisation), subdiv.getComp()));
			Join<Company, Contact> defaultContact = company.join(Company_.defaultBusinessContact);
			Join<Contact, Subdiv> defaultSubdiv = defaultContact.join(Contact_.sub);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(company.get(Company_.companyRole), CompanyRole.CLIENT));
			if (active != null)
				clauses.getExpressions().add(cb.isTrue(settings.get(CompanySettingsForAllocatedCompany_.active)));
			clauses.getExpressions().add(cb.equal(defaultContact.get(Contact_.sub), subdiv));
			cq.where(clauses);
			cq.select(cb.construct(CompanyKeyValue.class, company.get(Company_.coid), company.get(Company_.coname)));
			return cq;
		});
	}

	@Override
	public List<Company> getCompaniesByRole(CompanyRole companyRole) {
		return getResultList(cb -> {
			CriteriaQuery<Company> cq = cb.createQuery(Company.class);
			Root<Company> company = cq.from(Company.class);
			cq.where(cb.equal(company.get(Company_.companyRole), companyRole));
			cq.orderBy(cb.asc(company.get(Company_.coname)));
			return cq;
		});
	}

	@Override
	public Boolean existsByName(String name, CompanyRole companyRole, Company excludeCompany) {
		return exists(Company_.coname, name, companyRole, excludeCompany);
	}

	@Override
	public Boolean existsByLegalIdentifier(String legalIdentifier, CompanyRole companyRole, Company excludeCompany) {
		return exists(Company_.legalIdentifier, legalIdentifier, companyRole, excludeCompany);
	}

	/*
	 * The legal identifier and name searches use this common functionality.
	 */
	private Boolean exists(SingularAttribute<Company, String> property, String propertyValue, CompanyRole companyRole,
			Company excludeCompany) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Company> company = cq.from(Company.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(company.get(Company_.companyRole), companyRole));
			clauses.getExpressions().add(cb.equal(company.get(property), propertyValue));
			if (excludeCompany != null)
				clauses.getExpressions().add(cb.notEqual(company, excludeCompany));
			cq.where(clauses);
			cq.select(cb.count(company));
			return cq;
		}) > 0;
	}

	private List<CompanySearchResultWrapper> searchByCompanyRoles(String searchString, List<CompanyRole> roles,
			Company allocatedCompany, Boolean active, Boolean includeDeactivatedCompanies, Boolean anywhereActive, Boolean searchName, Boolean prefix) {
		return getResultList(cb -> {
			CriteriaQuery<CompanySearchResultWrapper> cq = cb.createQuery(CompanySearchResultWrapper.class);
			Root<Company> company = cq.from(Company.class);
			Join<Company, CompanySettingsForAllocatedCompany> settings = company
					.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			if(!includeDeactivatedCompanies){
				settings.on(cb.isTrue(settings.get(CompanySettingsForAllocatedCompany_.active)));
			}
			Predicate clauses = cb.conjunction();
			if(!roles.isEmpty()) clauses.getExpressions().add(company.get(Company_.companyRole).in(roles));
			if (searchName)
				clauses.getExpressions()
						.add(cb.like(company.get(Company_.coname), (prefix ? "" : "%") + searchString + "%"));
			else
				clauses.getExpressions()
						.add(cb.like(company.get(Company_.legalIdentifier), (prefix ? "" : "%") + searchString + "%"));
			if (active)
				clauses.getExpressions().add(
						cb.equal(settings.get(CompanySettingsForAllocatedCompany_.organisation), allocatedCompany));
			else if (anywhereActive)
				clauses.getExpressions().add(cb.isNotNull(settings.get(CompanySettingsForAllocatedCompany_.active)));
			else
				clauses.getExpressions().add(cb.isNull(settings.get(CompanySettingsForAllocatedCompany_.active)));
			cq.where(clauses);
			cq.orderBy(cb.asc(company.get(Company_.coname)));
			cq.distinct(true);
			cq.select(cb.construct(CompanySearchResultWrapper.class, company.get(Company_.coid),
					company.get(Company_.coname), company.get(Company_.companyRole),
					company.get(Company_.legalIdentifier)));
			return cq;
		}, 0, MAX_RESULTS_WRAPPER);
	}

	@Override
	public List<CompanySearchResultWrapper> searchByCompanyRoles(String searchString, List<CompanyRole> roles,
			Company allocatedCompany, Boolean active, Boolean anywhereActive, Boolean searchName, Boolean searchEverywhere) {
		return searchByCompanyRoles(searchString, roles, allocatedCompany, active, false, anywhereActive, searchName,!searchEverywhere );
	}

	@Override
	public List<CompanySearchResultWrapper> searchByCompanyRoles(String searchName, boolean active,
			List<CompanyRole> roles, boolean includeDeactivatedCompanies, Company allocatedCompany) {
		return searchByCompanyRoles(searchName, roles, allocatedCompany, active, includeDeactivatedCompanies, false, true, true);
	}

	@Override
	public List<CompanyKeyValue> getCompaniesFromSubdivs(List<Integer> subdivs) {
		return getResultList(cb -> {
			CriteriaQuery<CompanyKeyValue> cq = cb.createQuery(CompanyKeyValue.class);
			Root<Company> companyRoot = cq.from(Company.class);
			Join<Company, Subdiv> subdivJoin = companyRoot.join(Company_.subdivisions);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(subdivJoin.in(subdivs));
			cq.where(clauses);
			return cq.distinct(true).select(cb.construct(CompanyKeyValue.class, companyRoot.get(Company_.coid),
					companyRoot.get(Company_.coname)));
		});
	}
	
	@Override
	public List<CompanyProjectionDTO> getDTOList(Collection<Integer> companyIds, Integer allocatedCompanyId) {
		if (companyIds.isEmpty()) throw new IllegalArgumentException("companyIds must not be empty");
		return getResultList(cb -> {
			CriteriaQuery<CompanyProjectionDTO> cq = cb.createQuery(CompanyProjectionDTO.class);
			Root<Company> root = cq.from(Company.class);
			Join<Company, CompanySettingsForAllocatedCompany> settings = root.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			settings.on(cb.equal(settings.get(CompanySettingsForAllocatedCompany_.organisation), allocatedCompanyId));

			cq.where(root.get(Company_.coid).in(companyIds));
			cq.select(cb.construct(CompanyProjectionDTO.class, 
					root.get(Company_.coid), root.get(Company_.coname), 
					settings.get(CompanySettingsForAllocatedCompany_.active),
					settings.get(CompanySettingsForAllocatedCompany_.onStop)));

			cq.orderBy(cb.asc(root.get(Company_.coname)));
			
			return cq;
		});
	}

	@Override
	public CompanyDTO getForLinkInfo(Integer companyId, Integer allocatedCompanyId) {
		return getSingleResult(cb -> {
			CriteriaQuery<CompanyDTO> cq = cb.createQuery(CompanyDTO.class);
			Root<Company> company = cq.from(Company.class);
			Join<Company, CompanySettingsForAllocatedCompany> settings = company
					.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			settings.on(cb.equal(settings.get(CompanySettingsForAllocatedCompany_.organisation), allocatedCompanyId));
			cq.where(cb.equal(company.get(Company_.coid), companyId));
			cq.select(cb.construct(CompanyDTO.class, company.get(Company_.coid), company.get(Company_.coname),
					settings.get(CompanySettingsForAllocatedCompany_.active),
					settings.get(CompanySettingsForAllocatedCompany_.onStop)));
			return cq;
		});
	}
	
	@Override
	public List<KeyValueIntegerString> getCompaniesByRoleForAllocatedCompany(List<CompanyRole> roles, Company allocatedCompany,
			List<Integer> companiesIdsAlreadyExist){
		return getResultList(cb -> {
			CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class);
			Root<Company> company = cq.from(Company.class);
			Join<Company, CompanySettingsForAllocatedCompany> settings = company.join(Company_.settingsForAllocatedCompanies);
			settings.on(cb.equal(settings.get(CompanySettingsForAllocatedCompany_.organisation), allocatedCompany));
			Predicate clauses = cb.conjunction();
			// select only active companies
			clauses.getExpressions().add(cb.isTrue(settings.get(CompanySettingsForAllocatedCompany_.active)));
			// ignore companies that are already users/members of the Company List
			if(!companiesIdsAlreadyExist.isEmpty()){
				clauses.getExpressions().add(cb.not(company.get(Company_.coid).in(companiesIdsAlreadyExist)));
			}
			//select only companies with given roles
			if(!roles.isEmpty()){
				clauses.getExpressions().add(company.get(Company_.companyRole).in(roles));
			}
			
			cq.where(clauses);
			cq.select(cb.construct(KeyValueIntegerString.class, company.get(Company_.coid),
					company.get(Company_.coname)));
			cq.distinct(true);
			cq.orderBy(cb.asc(company.get(Company_.coname)));
			return cq;
		});
	}



}