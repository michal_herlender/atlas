package org.trescal.cwms.core.company.form;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Data
public class BankAccountForm {

    /*
    Currency @NotNull
    IBAN 34 chars @NotEmpty
    Swift-BIC 8 or 11 chars @NotEmpty
    Bank Name 100 chars @NotEmpty
    Local Account Number 50 chars optional
     */
    @NotNull
    private Integer entityId;    // Customer ID for adding
    private Integer bankAccountId;
    @Size(min = 1, max = 100)
    private String bankName;
    @Size(min = 1, max = 34)
    private String iban;
    @Size(min = 8, max = 11)
    private String swiftBic;
    @NotNull
    private Integer currencyId;
    @Size(max = 50)
    @NotNull
    private String accountNo;
    private Boolean includeOnFactoredInvoices;
    private Boolean includeOnNonFactoredInvoices;

    public Boolean getIsEdit() {
        return (bankAccountId != null && bankAccountId > 0);
    }
}