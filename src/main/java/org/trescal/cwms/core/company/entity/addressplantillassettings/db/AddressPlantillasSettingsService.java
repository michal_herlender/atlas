package org.trescal.cwms.core.company.entity.addressplantillassettings.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.addressplantillassettings.AddressPlantillasSettings;

public interface AddressPlantillasSettingsService extends BaseService<AddressPlantillasSettings, Integer> {
	public AddressPlantillasSettings getForAddress(int addrid, int allocatedcoid);
	public AddressPlantillasSettings createAndInitilazeNewEntity();
}
