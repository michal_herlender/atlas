package org.trescal.cwms.core.company.entity.companylist;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;

import lombok.Setter;

@Entity
@Table(name = "companylist")
@Setter
public class CompanyList extends Allocated<Company> {

	private Integer id;
	private String name;
	private Boolean active;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getId() {
		return id;
	}

	@NotNull
	@Length(min = 1, max = 50)
	@Column(name = "name", nullable = false, length = 50)
	public String getName() {
		return name;
	}

	@NotNull
	@Column(name = "active", columnDefinition = "bit")
	public Boolean getActive() {
		return active;
	}

}
