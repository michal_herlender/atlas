package org.trescal.cwms.core.company.entity.companyhistory.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.dto.CompanySearchResultWrapper;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companyhistory.CompanyHistory;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;

@Service("companyHistoryService")
public class CompanyHistoryServiceImpl extends BaseServiceImpl<CompanyHistory, Integer>
implements CompanyHistoryService
{
	@Autowired
	CompanyHistoryDao companyHistoryDao;
	
	@Override
	protected BaseDao<CompanyHistory, Integer> getBaseDao() {
		return companyHistoryDao;
	}
	
	public List<CompanySearchResultWrapper> searchByCompanyRoles(String searchString, List<CompanyRole> roles,
			Company allocatedCompany, Boolean active, Boolean anywhereActive)
	{
		return companyHistoryDao.searchByCompanyRoles(searchString, roles, allocatedCompany, active, anywhereActive);
	}
}