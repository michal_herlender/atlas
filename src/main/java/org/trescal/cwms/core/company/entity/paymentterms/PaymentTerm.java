package org.trescal.cwms.core.company.entity.paymentterms;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum PaymentTerm {

	P30("paymentterms.30", "30", "30 days net"), P30EOM("paymentterms.30EOM", "30EOM", "30 days end of month"),
	PEOM30("paymentterms.EOM30", "EOM30", "End of month 30 days"),
	P30EOM10("paymentterms.30EOM10", "30EOM10", "30 days end of month the 10th"),
	P45("paymentterms.45", "45", "45 days net"), P45EOM("paymentterms.45EOM", "45EOM", "45 days end of month"),
	PEOM45("paymentterms.EOM45", "EOM45", "End of months 45 days"), P60("paymentterms.60", "60", "60 days net"),
	P85("paymentterms.85", "85", "85 days net"), P90("paymentterms.90", "90", "90 days net"),
	P120("paymentterms.120", "120", "120 days net"), P150("paymentterms.150", "150", "150 days net"),
	P180("paymentterms.180", "180", "180 days net"), P0("paymentterms.0", "0", "0 day"),
	PNM25("paymentterms.NM25", "NM25", "Next month the 25th"),
	P30EOM15("paymentterms.30EOM15", "30EOM15", "30 days end of month the 15th"),
	P50("paymentterms.50", "50", "50 days net"),
	P14("paymentterms.14", "14", "14 days net");
	//	GB 2020-05-07 : Commenting out new payment terms for now (see DEV-1539, also note that computePaymentDate() and PaymentTermsTests should be updated, if we add new payment terms) 
	//	P75("paymentterms.75","75","75 days net");

	private ReloadableResourceBundleMessageSource messages;
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	private String messageCode;
	private String code;
	private String description;

	private PaymentTerm(String messageCode, String code, String description) {
		this.messageCode = messageCode;
		this.description = description;
		this.code = code;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Component
	public static class PaymenttermMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;

		@PostConstruct
		public void postConstruct() {
			for (PaymentTerm pt : EnumSet.allOf(PaymentTerm.class))
				pt.setMessageSource(messages);
		}
	}

	public void setMessageSource(ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}

	public String getMessage() {
		if (messages != null)
			return messages.getMessage(messageCode, null, this.toString(), LocaleContextHolder.getLocale());
		else
			return description;
	}

	public String getMessageForLocale(Locale locale) {
		return messages.getMessage(this.messageCode, null, this.description, locale);
	}

	public LocalDate computePaymentDate(LocalDate issueDate) {
		LocalDate paymentDate = null;
		switch (this) {
		case P120:
			paymentDate = issueDate
				.plusDays(120);
			break;
		case P150:
			paymentDate = issueDate
				.plusDays(150);
			break;
		case P180:
			paymentDate = issueDate
				.plusDays(180);
			break;
		case P30:
			paymentDate = issueDate
				.plusDays(30);
			break;
		case P30EOM:
			// Keeping implementation of +1 month (not always 30 days)
			paymentDate = issueDate
				.plusMonths(1)
				.with(TemporalAdjusters.lastDayOfMonth());
			break;
		case P30EOM10:
			paymentDate = issueDate
				.plusMonths(2)
				.withDayOfMonth(10);
			break;
		case P30EOM15:
			paymentDate = issueDate
				.plusMonths(2)
				.withDayOfMonth(15);
			break;
		case P45:
			paymentDate = issueDate
				.plusDays(45);
			break;
		case P45EOM:
			paymentDate = issueDate
				.plusDays(45)
				.with(TemporalAdjusters.lastDayOfMonth());
			break;
		case P60:
			paymentDate = issueDate
				.plusDays(60);
			break;
		case P85:
			paymentDate = issueDate
				.plusDays(85);
			break;
		case P90:
			paymentDate = issueDate
				.plusDays(90);
			break;
		case PEOM30:
			paymentDate = issueDate
				.with(TemporalAdjusters.lastDayOfMonth())
				.plusDays(30);
			break;
		case PEOM45:
			paymentDate = issueDate
				.with(TemporalAdjusters.lastDayOfMonth())
				.plusDays(45);
			break;
		case PNM25:
			paymentDate = issueDate
				.plusMonths(1)
				.withDayOfMonth(25);
			break;
		case P50:
			paymentDate = issueDate
				.plusDays(50);
			break;
		case P14:
			paymentDate = issueDate
				.plusDays(14);
			break;
		case P0:
			paymentDate = issueDate;
			break;
		}
		return paymentDate;
	}
}