package org.trescal.cwms.core.company.entity.companyhistory.db;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.dto.CompanySearchResultWrapper;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companyhistory.CompanyHistory;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;

@Repository("CompanyHistoryDao")
public class CompanyHistoryDaoImpl extends BaseDaoImpl<CompanyHistory, Integer> implements CompanyHistoryDao
{
	@Override
	protected Class<CompanyHistory> getEntity() {
		return CompanyHistory.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<CompanySearchResultWrapper> searchByCompanyRoles(String searchString, List<CompanyRole> roles,
			Company allocatedCompany, Boolean active, Boolean anywhereActive)
	{
		List<CompanySearchResultWrapper> resultList = new ArrayList<CompanySearchResultWrapper>();
		Criteria criteria = getSession().createCriteria(CompanyHistory.class, "companyHistory");
		criteria.createCriteria("comp", "company");
		/* Name criterion */
		criteria.add(Restrictions.like("companyHistory.coname", "%" + searchString + "%"));
		/* Role criterion */
		Disjunction disjunction = Restrictions.disjunction();
		for(CompanyRole cr: roles) disjunction.add(Restrictions.eq("company.companyRole", cr));
		criteria.add(disjunction);
		/* Active criterion */
		criteria.createCriteria("company.settingsForAllocatedCompanies", "activeSettings", JoinType.LEFT_OUTER_JOIN, Restrictions.eq("active", true));
		if (active) criteria.add(Restrictions.eq("activeSettings.organisation", allocatedCompany));
		else if (anywhereActive) criteria.add(Restrictions.isNotNull("activeSettings.active"));
		else criteria.add(Restrictions.isNull("activeSettings.active"));
		/* Projections for DTO */
		ProjectionList projections = Projections.projectionList();
		projections.add(Projections.distinct(Projections.property("companyHistory.id")));
		projections.add(Projections.property("company.coid"));
		projections.add(Projections.property("company.coname"));
		projections.add(Projections.property("company.companyRole"));
		projections.add(Projections.property("companyHistory.coname"));
		projections.add(Projections.property("company.legalIdentifier"));
		criteria.setProjection(projections);
		/* Order */
		criteria.addOrder(Order.asc("companyHistory.coname"));
		/* Get result */
		List<Object[]> result = criteria.list();
		for(Object[] c: result) {
			try {
				CompanySearchResultWrapper csrw = new CompanySearchResultWrapper();
				csrw.setCoid((Integer) c[1]);
				csrw.setNewconame((String) c[2]);
				csrw.setCorole(((CompanyRole) c[3]).getMessage());
				csrw.setConame((String) c[4]);
				csrw.setLegalIdentifier((String) c[5]);
				resultList.add(csrw);
			}
			catch (Exception ex) {}
		}
		return resultList;
	}
}