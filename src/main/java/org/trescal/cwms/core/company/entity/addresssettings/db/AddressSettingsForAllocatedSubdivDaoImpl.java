package org.trescal.cwms.core.company.entity.addresssettings.db;

import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.dto.AddressTransportOptionsComponentDTO;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedSubdiv_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod_;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption_;

@Repository
public class AddressSettingsForAllocatedSubdivDaoImpl extends BaseDaoImpl<AddressSettingsForAllocatedSubdiv, Integer>
		implements AddressSettingsForAllocatedSubdivDao {

	@Override
	protected Class<AddressSettingsForAllocatedSubdiv> getEntity() {
		return AddressSettingsForAllocatedSubdiv.class;
	}

	@Override
	public AddressSettingsForAllocatedSubdiv getBySubdiv(Integer addressId, Integer allocatedSubdivId) {
		return getFirstResult(cb -> {
			CriteriaQuery<AddressSettingsForAllocatedSubdiv> cq = cb
					.createQuery(AddressSettingsForAllocatedSubdiv.class);
			Root<AddressSettingsForAllocatedSubdiv> settings = cq.from(AddressSettingsForAllocatedSubdiv.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(settings.get(AddressSettingsForAllocatedSubdiv_.address), addressId));
			clauses.getExpressions()
					.add(cb.equal(settings.get(AddressSettingsForAllocatedSubdiv_.ORGANISATION), allocatedSubdivId));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

	@Override
	public List<AddressTransportOptionsComponentDTO> getTransportOptions(Integer addressId, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<AddressTransportOptionsComponentDTO> cq = cb
					.createQuery(AddressTransportOptionsComponentDTO.class);
			Root<AddressSettingsForAllocatedSubdiv> settings = cq.from(AddressSettingsForAllocatedSubdiv.class);
			Join<AddressSettingsForAllocatedSubdiv, Subdiv> subdiv = settings
					.join(AddressSettingsForAllocatedSubdiv_.ORGANISATION);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Join<AddressSettingsForAllocatedSubdiv, TransportOption> transportInOption = settings
					.join(AddressSettingsForAllocatedSubdiv_.transportIn, JoinType.LEFT);
			Join<TransportOption, TransportMethod> transportInMethod = transportInOption.join(TransportOption_.method,
					JoinType.LEFT);
			Expression<String> transportInName = joinTranslation(cb, transportInMethod,
					TransportMethod_.methodTranslation, locale);
			Join<AddressSettingsForAllocatedSubdiv, TransportOption> transportOutOption = settings
					.join(AddressSettingsForAllocatedSubdiv_.transportOut, JoinType.LEFT);
			Join<TransportOption, TransportMethod> transportOutMethod = transportOutOption.join(TransportOption_.method,
					JoinType.LEFT);
			Expression<String> transportOutName = joinTranslation(cb, transportOutMethod,
					TransportMethod_.methodTranslation, locale);
			cq.where(cb.equal(settings.get(AddressSettingsForAllocatedSubdiv_.address), addressId));
			cq.orderBy(cb.asc(company.get(Company_.coname)), cb.asc(subdiv.get(Subdiv_.subname)));
			cq.select(cb.construct(AddressTransportOptionsComponentDTO.class,
					settings.get(AddressSettingsForAllocatedSubdiv_.id), company.get(Company_.coname),
					subdiv.get(Subdiv_.subdivid), subdiv.get(Subdiv_.subname), transportInName, transportInOption.get(TransportOption_.localizedName),
					transportOutName, transportOutOption.get(TransportOption_.localizedName)));
			return cq;
		});
	}
}