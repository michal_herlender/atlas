package org.trescal.cwms.core.company.entity.department.db;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.dto.EditableDepartmentDTO;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.company.entity.departmentmember.db.DepartmentMemberService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.departmenttype.db.DepartmentTypeService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.ProcedureCategoryDepartmentComparator;
import org.trescal.cwms.core.userright.enums.Permission;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;

@Service("DepartmentService")
public class DepartmentServiceImpl extends BaseServiceImpl<Department, Integer> implements DepartmentService {
	@Autowired
	private DepartmentDao departmentDao;
	@Autowired
	private DepartmentMemberService deptMemberServ;
	@Autowired
	private DepartmentTypeService departmentTypeService;

	protected static final Log logger = LogFactory.getLog(DepartmentServiceImpl.class);

	@Override
	protected BaseDao<Department, Integer> getBaseDao() {
		return departmentDao;
	}
	
	@Override
    public List<Department> getAllCapabilityEnabledSubdivDepartments(Subdiv subdiv) {
        List<DepartmentType> types = departmentTypeService.getProcEnabledDepartmentTypes();
        return departmentDao.findBySubdivAndTypes(subdiv, types);
    }
	
	public List<Department> getAllSubdivDepartments(Integer subdivid)
	{
		return this.departmentDao.getAllSubdivDepartments(subdivid);
	}

	@Override
	public List<EditableDepartmentDTO> getAllSubdivDepartmentsEditable(Integer subdivid, Contact contact)
	{
		List<Department> depts = this.getAllSubdivDepartments(subdivid);
		List<EditableDepartmentDTO> eDepts = new ArrayList<EditableDepartmentDTO>();

		for (Department d : depts)
		{
			EditableDepartmentDTO dto = new EditableDepartmentDTO();
			dto.setDepartment(d);
			dto.setEditable(this.userCanEditDepartment(contact, d).isSuccess());
			eDepts.add(dto);
		}

		return eDepts;
	}

	@Override
	public List<Department> getBusinessDepartments()
	{
		return this.departmentDao.getBusinessDepartments();
	}
	
	@Override
	public List<Department> getByTypeAndSubdiv(DepartmentType type, Integer subdivId){
		return this.departmentDao.getByTypeAndSubdiv(type, subdivId);
	}
	
	public void setDeptMemberServ(DepartmentMemberService deptMemberServ)
	{
		this.deptMemberServ = deptMemberServ;
	}
	
	@Override
	public ResultWrapper userCanEditDepartment(Contact editor, Department department)
	{
		boolean proceed = false;
		String message = "";

		// we only need to do validation if the department belongs to a business
		// company
		if (department.getSubdiv().getComp().getCompanyRole() == CompanyRole.BUSINESS)
		{

				Collection<? extends GrantedAuthority> auth  = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
				
				
				for (GrantedAuthority a : auth)
				{
					// if the user has either ADMIN or DIRECTOR rights then
					// proceed
					if (a.getAuthority().equals(Permission.DEPARTMENT_ADD_EDIT.toString()))
					{
						proceed = true;
						break;
					}
					else if(a.getAuthority().equals("ROLE_INTERNAL")) {
						DepartmentMember member = this.deptMemberServ.findDepartmentMember(editor.getPersonid(), department.getDeptid());
						if (member != null) {
							if (member.getDepartmentRole().isActive()
								&& member.getDepartmentRole().isManage()) {
								proceed = true;
								break;
							}
						}

						message = "User is attempting to update a department he is not a manager of.";
						logger.info("The contact is not a manager of the department");
					}
				
			}
		}
		else
		{
			proceed = true;
		}
		return new ResultWrapper(proceed, message, null, null);
	}
	
	private void addGeneralProcedureCategory(Department department) {
        if (department.getCategories() == null)
            department.setCategories(new TreeSet<CapabilityCategory>(new ProcedureCategoryDepartmentComparator()));
        if (department.getCategories().isEmpty()) {
            CapabilityCategory procCat = new CapabilityCategory();
            procCat.setDepartment(department);
            procCat.setName("General");
            procCat.setDescription("Auto generated procedure category");
            procCat.setDepartmentDefault(true);
            department.getCategories().add(procCat);
        }
    }
	
	@Override
	public void save(Department department) {
		if (department.getType().isProcedures()) addGeneralProcedureCategory(department);
		super.save(department);
	}
	
	@Override
	public Department merge(Department department) {
		if (department.getType().isProcedures()) addGeneralProcedureCategory(department);
		return super.merge(department);
	}
	
	@Override
	public void deleteDepartment(int id) {
		Department department = super.get(id);
		this.delete(department);
	}

	@Override
	public List<Contact> getAllActiveAvailableDepartmentContact(List<Department> dept) {
		return this.departmentDao.getAllActiveAvailableDepartmentContact(dept);
	}
}