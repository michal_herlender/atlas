package org.trescal.cwms.core.company.entity.department;

import java.util.Comparator;

public class DepartmentComparator implements Comparator<Department>
{
	@Override
	public int compare(Department d1, Department d2)
	{
		// special business case for third party dept
		if (d1.getName().equalsIgnoreCase("third party"))
		{
			return 1;
		}
		else if (d2.getName().equalsIgnoreCase("third party"))
		{
			return -1;
		}

		if (d1.getName().compareTo(d2.getName()) != 0)
		{
			return d1.getName().compareTo(d2.getName());
		}
		else
		{
			return d1.getDeptid().compareTo(d2.getDeptid());
		}
	}
}
