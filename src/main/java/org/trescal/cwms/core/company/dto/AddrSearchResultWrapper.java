package org.trescal.cwms.core.company.dto;

public class AddrSearchResultWrapper {
	private String addr1;
	private String addr2;
	private int addrid;
	private String county;
	private String postcode;
	private boolean subdivDefault;
	private String town;

	public AddrSearchResultWrapper(int addrid, String addr1, String addr2, String town, String county, String postcode,
			Integer subdivDefaultAddressId) {
		this.addrid = addrid;
		this.addr1 = addr1;
		this.addr2 = addr2;
		this.town = town;
		this.county = county;
		this.postcode = postcode;
		this.subdivDefault = subdivDefaultAddressId != null && subdivDefaultAddressId == addrid;
	}

	public AddrSearchResultWrapper() {
	}

	public String getAddr1() {
		return this.addr1;
	}

	public String getAddr2() {
		return this.addr2;
	}

	public int getAddrid() {
		return this.addrid;
	}

	public String getCounty() {
		return this.county;
	}

	public String getPostcode() {
		return this.postcode;
	}

	public String getTown() {
		return this.town;
	}

	public boolean isSubdivDefault() {
		return this.subdivDefault;
	}

	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}

	public void setAddrid(int addrid) {
		this.addrid = addrid;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public void setSubdivDefault(boolean subdivDefault) {
		this.subdivDefault = subdivDefault;
	}

	public void setTown(String town) {
		this.town = town;
	}
}