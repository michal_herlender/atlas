package org.trescal.cwms.core.company.entity.departmentmember.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.login.entity.user.User;

public class DepartmentMemberServiceImpl implements DepartmentMemberService
{
	DepartmentMemberDao departmentmemberDao;

	public void deleteDepartmentMember(DepartmentMember departmentMember)
	{
		this.departmentmemberDao.remove(departmentMember);
	}

	public DepartmentMember findDepartmentMember(Integer personid, Integer deptid)
	{
		return this.departmentmemberDao.findDepartmentMember(personid, deptid);
	}

	public List<DepartmentMember> getAllContactDepartmentMembers(Integer personid)
	{
		return this.departmentmemberDao.getAllContactDepartmentMembers(personid);
	}

	public List<DepartmentMember> getAllDepartmentMembers()
	{
		return this.departmentmemberDao.findAll();
	}

	public DepartmentMemberDao getDepartmentmemberDao()
	{
		return this.departmentmemberDao;
	}

	public DepartmentMemberDao getDepartmentMemberDao()
	{
		return this.departmentmemberDao;
	}

	@Override
	public List<DepartmentMember> getDepartmentMembers(int deptid)
	{
		return this.departmentmemberDao.getDepartmentMembers(deptid);
	}

	public void insertDepartmentMember(DepartmentMember departmentmember)
	{
		this.departmentmemberDao.persist(departmentmember);
	}

	public void saveOrUpdateDepartmentMember(DepartmentMember departmentMember)
	{
		this.departmentmemberDao.saveOrUpdate(departmentMember);
	}

	public void setDepartmentmemberDao(DepartmentMemberDao departmentmemberDao)
	{
		this.departmentmemberDao = departmentmemberDao;
	}

	public void setDepartmentMemberDao(DepartmentMemberDao departmentmemberDao)
	{
		this.departmentmemberDao = departmentmemberDao;
	}

	public void updateDepartmentMember(DepartmentMember departmentmember)
	{
		this.departmentmemberDao.update(departmentmember);
	}

	@Override
	public List<User> getDepartmentUsers(List<Department> depts) {
		return this.departmentmemberDao.getDepartmentUsers(depts);
	}

}