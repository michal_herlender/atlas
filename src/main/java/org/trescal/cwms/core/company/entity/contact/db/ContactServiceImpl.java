package org.trescal.cwms.core.company.entity.contact.db;

import io.vavr.control.Either;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.ServletRequestUtils;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.dto.ContactSearchResultWrapper;
import org.trescal.cwms.core.company.dto.SearchEmailContactDto;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.form.ContactAddForm;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.entity.portal.usergroup.db.UserGroupService;
import org.trescal.cwms.core.login.entity.role.Roles;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.login.entity.userpreferences.db.UserPreferencesService;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_ContactReminder;
import org.trescal.cwms.core.system.entity.printer.db.PrinterService;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.tools.LoginGenerator;
import org.trescal.cwms.core.userright.entity.role.UserRightRole;
import org.trescal.cwms.core.userright.entity.role.db.RoleService;
import org.trescal.cwms.core.userright.entity.userrole.UserRole;
import org.trescal.cwms.core.userright.entity.userrole.db.UserRoleDao;
import org.trescal.cwms.core.userright.entity.userrole.db.UserRoleService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service("ContactService")
@Slf4j
public class ContactServiceImpl extends BaseServiceImpl<Contact, Integer> implements ContactService {
	@Value("${cwms.config.email.noreply}")
	private String noreplyAddress;

	@Autowired
	private AddressService addressServ;
	@Autowired
	private AuthenticationService authenticationService;
	@Autowired
	private ContactDao conDao;
	@Autowired
	private EmailContent_ContactReminder emailContentContactReminder;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private LoginGenerator loginGenerator;
	@Autowired
	private MessageSource messages;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private PrinterService printerServ;
	@Autowired
	private RoleService roleService;
	@Autowired
	private SubdivService subdivServ;
	@Autowired
	private UserGroupService userGroupServ;
	@Autowired
	private UserRoleDao userRoleDao;
	@Autowired
	private UserPreferencesService userPrefsServ;
	@Autowired
	private UserRoleService newUserRoleServ;
	@Autowired
	private UserService userServ;


	@Override
	protected BaseDao<Contact, Integer> getBaseDao() {
		return conDao;
	}

	@Override
	public boolean contactBelongsToDepartmentOrIsAdmin(Contact contact, DepartmentType departmentType,
			Company businessCompany) {
		boolean authenticated = false;
		User user = contact.getUser();

		for (org.trescal.cwms.core.userright.entity.userrole.UserRole role : user.getUserRoles()) {
			if (role.getOrganisation().getComp().getId().equals(businessCompany.getId())) {
				// If you have the permission you are an ADMIN
				if (userRoleDao.getAllPermission(role).contains(Permission.DEPARTMENT_ADD_EDIT)) {
					authenticated = true;
					break;
				}
				if (contactBelongsToDepartmentForSubdiv(contact, departmentType, role.getOrganisation())) {
					authenticated = true;
					break;
				}

			}
		}
		return authenticated;
	}

	/*
	 * Just used internally
	 */
	public boolean contactBelongsToDepartmentForSubdiv(Contact contact, DepartmentType departmentType,
			Subdiv businessSubdiv) {
		boolean result = false;
		for (DepartmentMember member : contact.getMember()) {
			if (member.getDepartment().getSubdiv().getId().equals(businessSubdiv.getId())) {
				if (member.getDepartment().getType().equals(departmentType)) {
					result = true;
					break;
				}
			}
		}
		return result;
	}

	@Override
	public boolean contactManagesDepartmentOrIsAdmin(Contact contact, DepartmentType departmentType, Integer subdivId) {
		boolean authorised = this.authenticationService.hasRight(contact, Permission.DEPARTMENT_ADD_EDIT, subdivId);
		if (!authorised) {
			authorised = contact.getMember().stream().filter(dm -> dm.getDepartmentRole().isManage())
				.filter(dm -> dm.getDepartment().getType() == departmentType)
				.anyMatch(dm -> dm.getDepartment().getSubdiv().getSubdivid() == subdivId);
		}
		return authorised;
	}

	@Override
	public ResultWrapper contactCanCreateContact(HttpServletRequest req) {
		int subdivid = ServletRequestUtils.getIntParameter(req, "subdivid", 0);

		// test if subdiv belongs to a business company
		if (this.subdivServ.subdivBelongsToBusinessCompany(subdivid)) {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			if (authentication != null) {
				Collection<? extends GrantedAuthority> auth = authentication.getAuthorities();
				for (GrantedAuthority a : auth) {
					// if the user has either ADMIN or DIRECTOR rights then
					// proceed without any more checks
					if (a.getAuthority().equals(Permission.PERSON_ADD.toString())) {
						return new ResultWrapper(true, "");
					}
				}
			}
			// if we get here then the Contact is not an admin or director, so
			// return false
			return new ResultWrapper(false, "", null, null);
		} else {
			// subdiv belongs to a non-business company, allow user to add
			// contact's regardless of role
			return new ResultWrapper(true, this.messages.getMessage("error.contact.create.forbidden", null,
					"You do not have sufficient priviliges to add user's to this subdivision", null));
		}
	}

	@Override
	public boolean contactExists(int subdivid, String fname, String lname) {
		List<Contact> list = (this.conDao.searchContactCaseInsensitive(subdivid, fname, lname));
		return list.size() > 0;
	}

	@Override
	public Integer createContact(ContactAddForm caf) {
		Subdiv subdiv = this.subdivServ.get(caf.getSubdivId());
		Contact contact = new Contact();
		contact.setActive(true);
		contact.setUsergroup(this.userGroupServ.findGroup(caf.getGroupid()));
		contact.setTitle(caf.getTitle());
		contact.setDefAddress(
			this.addressServ.get(caf.getAddressid()));
		if ((caf.getPrinterid() != null) && (caf.getPrinterid() != 0))
			contact.setDefaultPrinter(this.printerServ.get(caf.getPrinterid()));
		contact.setLocale(Locale.forLanguageTag(caf.getLanguageTag()));
		contact.setSd(null);
		contact.setCreateDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		contact.setEmail(caf.getEmail());
		contact.setFax(caf.getFax());
		contact.setFirstName(caf.getFirstName());
		contact.setLastName(caf.getLastName());
		contact.setMobile(caf.getMobile());
		contact.setPosition(caf.getPosition());
		contact.setPreference(caf.getPreference());
		contact.setSub(subdiv);
		contact.setTelephone(caf.getTelephone());
		contact.setTelephoneExt(caf.getTelephoneExt());

		// reset hr id if empty (3 = country code + '-')
		if (caf.getHrid().length() <= 3) {
			contact.setHrid(null);
		} else {
			contact.setHrid(caf.getHrid());
		}
		if (subdiv.getDefaultContact() == null) {
			subdiv.setDefaultContact(contact);
		}
		this.save(contact);
		if (caf.isCreateAccount()) {
			// create new User associated with Contact
			String username = caf.getUsername().trim();
			if (username.length() == 0)
				username = loginGenerator.generateUsername(contact);
			String password = caf.getPassword().trim();
			if (password.length() == 0)
				password = loginGenerator.generatePassword();
			User u = new User(username, passwordEncoder.encode(password), caf.getRole().equals(Roles.ROLE_WEB.name()));
			u.setCon(contact);
			// initially allow web access
			u.setWebAware(true);
			this.userServ.insertUser(u);
			if (!caf.getRole().equals(Roles.ROLE_WEB.name())) {
				int roleId = 0;
				for (UserRightRole r : roleService.getAll())
					if (r.getName().equals(caf.getRole()))
						roleId = r.getId();
				UserRightRole role = roleService.get(roleId);
				UserRole userRole = new UserRole(u, role, subdiv);
				newUserRoleServ.save(userRole);
			}
			// send confirmation e-mail to user if checked
			if (caf.isLoginEmail()) {
				// OneToOne normally initialized by Hibernate, but here it
				// is a new User
				contact.setUser(u);
				String confirmationText = this.sendPasswordReminder(contact.getPersonid(), password);
				log.info(confirmationText);
			}
			// add user preferences if new business user
			if (contact.getSub().getComp().getCompanyRole().equals(CompanyRole.BUSINESS)) {
				this.userPrefsServ.addDefaultPreferencesForUser(u);
			}
		}
		return contact.getPersonid();
	}

	@Override
	public Contact findEagerContact(int id) {
		return this.conDao.findEagerContact(id);
	}

	@Override
	public List<Contact> getActiveContactsWithRoleForSubdiv(int businessSubdivId) {
		return this.conDao.getActiveContactsWithRoleForSubdiv(businessSubdivId);
	}

	@Override
	public List<Contact> getAllActiveSubdivContacts(int subdivid) {
		return this.conDao.getAllActiveSubdivContacts(subdivid);
	}

	@Override
	public List<Contact> getAllCompanyContacts(int coid) {
		return this.conDao.getAllCompanyContacts(coid);
	}

	@Override
	public List<Contact> getAllCompanyContacts(int coid, boolean active) {
		return this.conDao.getAllCompanyContacts(coid, active);
	}

	@Override
	public Contact findCompanyContactByFullName(int coid, String fullname, boolean active) {
		return this.conDao.findCompanyContactByFullName(coid, fullname, active);
	}

	@Override
	public List<Contact> getAllCompanyContactsBySubCon(int coid, Boolean active) {
		return this.conDao.getAllCompanyContactsBySubCon(coid, active);
	}

	public List<Contact> getAllOrderedContacts() {
		return this.conDao.getAllOrderedContacts();
	}

	public List<Contact> getAllSubdivContacts(int subdivid) {
		return this.conDao.getAllSubdivContacts(subdivid);
	}

	@Override
	public List<ContactKeyValue> getBusinessContacts(boolean activeOnly) {
		return this.conDao.getBusinessContacts(activeOnly);
	}

	@Override
	public List<Contact> getContactsInUserGroup(int groupid) {
		return this.conDao.getContactsInUserGroup(groupid);
	}
	
	@Override
	public List<ContactProjectionDTO> getContactProjectionDTOs(Collection<Integer> personIds, Integer allocatedCompanyId) {
		List<ContactProjectionDTO> result = Collections.emptyList();
		if (!personIds.isEmpty()) {
			result = this.conDao.getContactProjectionDTOs(personIds, allocatedCompanyId);
		}
		return result;
	}

	@Override
	public List<ContactKeyValue> getContactDtoList(Integer coid, Integer subdivid, Boolean active,
			ContactKeyValue... prependDtos) {
		List<ContactKeyValue> result;
		List<ContactKeyValue> queryResult = this.conDao.getContactDtoList(coid, subdivid, active);
		if (prependDtos.length > 0) {
			result = new ArrayList<>();
			Set<Integer> ids = queryResult.stream().map(KeyValue::getKey).collect(Collectors.toSet());
			for (ContactKeyValue dto : prependDtos) {
				// Important - we only prepend the DTO if its ID is not already
				// in the query results
				// this lets us handle potentially inactive / not present
				// default values
				if (!ids.contains(dto.getKey()))
					result.add(dto);
			}
			result.addAll(queryResult);
		} else {
			result = queryResult;
		}
		return result;
	}

	public List<ContactSearchResultWrapper> searchByCompanyRoles(String firstName, String lastName, boolean active,
			List<CompanyRole> companyRoles) {
		return conDao.searchByCompanyRoles(firstName, lastName, active, companyRoles);
	}

	@Override
	public Either<String,Contact> searchContactByFullName(String fullName, boolean active) {
		return this.conDao.searchContactByFullName(fullName, active).<Either<String, Contact>>map(Either::right).orElse(Either.left("Failed to find contact with name: " + fullName + ". "));
	}

	@Override
	public  List<ContactSearchResultWrapper> searchContactsByRolesAndFullName(String fullName, Boolean active, List<CompanyRole> roles){
		return this.conDao.searchContactsByRolesAndFullName(fullName,active,roles);
	}

	@Override
	public List<Contact> searchContact(String firstName, String lastName, boolean active) {
		return this.conDao.searchContact(firstName, lastName, active);
	}

	@Override
	public List<Contact> searchContactByRoles(String firstName, String lastName, boolean active, String[] role) {
		val companyRoles = Stream.of(role).map(String::trim).map(String::toUpperCase).map(CompanyRole::valueOf)
				.collect(Collectors.toList());
		return this.conDao.searchContactByRoles(firstName, lastName, active, companyRoles);
	}

	/**
	 * The call to this method must indicate the unencrypted password, which has
	 * just been set This method does not save the user account, encrypt/update
	 * the password, etc... which must be done by the calling logic.
	 */
	public String sendPasswordReminder(int personid, String newPassword) {
		Contact con = this.get(personid);
		String confirmation;
		if ((con.getEmail() != null) && !con.getEmail().trim().equals("")) {
			EmailContentDto contentDto = this.emailContentContactReminder.getContent(con, newPassword, con.getLocale());
			log.info(contentDto.getBody());
			String from = noreplyAddress;
			String to = con.getEmail();
			// We no longer bcc the local webmaster email address 2018-07-17 -
			// security issue
			boolean success = this.emailServ.sendAdvancedEmail(contentDto.getSubject(), from, Collections.singletonList(to), Collections.emptyList(),
				Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), contentDto.getBody(), true, false);
			if (success) {
				confirmation = "A confirmation e-mail has been sent to " + con.getName() + " (" + con.getEmail() + ").";
			} else {
				confirmation = "There was an error and the confirmation e-mail could not be sent.";
			}
		} else {
			confirmation = "A confirmation e-mail cannot be sent to a contact with no e-mail address.";
		}
		return confirmation;
	}

	public Contact setEncryptedPassword(int personid, String encPassword) {
		Contact contact = get(personid);
		contact.setEncryptedPassword(encPassword);
		merge(contact);
		return contact;
	}

	@Override
	public void updateAll(java.util.Collection<Contact> contacts) {
		for (Contact c : contacts)
			conDao.merge(c);
	}

	@Override
	public boolean getHrIdExists(String hrid) {
		return conDao.getHrIdExists(hrid);
	}

	@Override
	public boolean getHrIdExistsAndIgnoreContact(String hrid, Contact contact) {
		return conDao.getHrIdExistsAndIgnoreContact(hrid, contact);
	}

	@Override
	public Contact getByHrId(String hrid) {
		return conDao.getByHrId(hrid);
	}

	@Override
	public Contact getContactByFirstnameAndLastname(Integer subdivid, String firstName, String lastName) {
		return conDao.getContactByFirstnameAndLastname(subdivid, firstName, lastName);
	}

	@Override
	public List<Contact> getByHrIds(List<String> hrids) {
		return conDao.getByHrIds(hrids);
	}

	@Override
	public Either<String,List<SearchEmailContactDto>> searchEmailContact(String name, List<Integer> coIds){
		try {
			return Either.right(conDao.searchEmailContacts(name,coIds));
		}
		catch (Exception e)
		{return Either.left(e.getMessage());
		}
	}

	@Override
	public Integer checkContactDeactivationStatus(int personid) {
		return this.conDao.countContactInstruments(personid);
	}
	
	/**
	 * If specific contact id specified, then we use it, otherwise defer to logged-in user via security authorization
	 * @param contactId
	 * @param principalUserName
	 * @return
	 */
	@Override
	public Contact getContactOrLoggedInContact(Integer contactId) {
		Contact contact = null;
		if (contactId != null) {
			contact = this.get(contactId);
		}
		else {
			String principalUserName = null;
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			if (authentication != null) principalUserName = authentication.getName();
			if (authentication == null)
				log.error("Authentication was null");
			if (principalUserName == null)
				log.error("No username could be determined via authentication");
			else 
				contact = this.userServ.get(principalUserName).getCon();
		}
		return contact;
	}

	@Override
	public ContactProjectionDTO getContactProjectionDTO(Integer contactId) {
		return this.conDao.getContactProjectionDTO(contactId);
	}

}