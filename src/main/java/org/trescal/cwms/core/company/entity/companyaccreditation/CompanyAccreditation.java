package org.trescal.cwms.core.company.entity.companyaccreditation;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.supplier.Supplier;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "companyaccreditation", uniqueConstraints = {@UniqueConstraint(columnNames = {"coid", "supplierid", "orgid"})})
public class CompanyAccreditation extends Allocated<Company> {
	private Company company;
	private boolean emailAlert;
	private LocalDate expiryDate;
	private int id;
	private LocalDate lastEmailAlert;
    private String reason;
    private Supplier supplier;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coid", nullable = false)
    public Company getCompany() {
        return this.company;
    }

    @Column(name = "expirydate", columnDefinition = "date")
    public LocalDate getExpiryDate() {
        return this.expiryDate;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Column(name = "lastemailalert", columnDefinition = "date")
    public LocalDate getLastEmailAlert() {
        return this.lastEmailAlert;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "supplierid", nullable = false)
    public Supplier getSupplier() {
        return this.supplier;
    }

	@Column(name = "emailalert", columnDefinition = "bit")
	public boolean isEmailAlert() {
		return this.emailAlert;
	}

	public void setLastEmailAlert(LocalDate lastEmailAlert) {
		this.lastEmailAlert = lastEmailAlert;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setEmailAlert(boolean emailAlert) {
		this.emailAlert = emailAlert;
	}

	@Length(max = 100)
	@Column(name = "reason", length = 100)
	public String getReason() {
		return this.reason;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Transient
	public boolean isExpired() {
		return this.expiryDate != null && this.expiryDate.isBefore(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public void setSupplier(Supplier supplier)
	{
		this.supplier = supplier;
	}
}
