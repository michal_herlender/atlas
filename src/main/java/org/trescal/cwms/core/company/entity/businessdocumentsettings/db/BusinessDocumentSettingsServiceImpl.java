package org.trescal.cwms.core.company.entity.businessdocumentsettings.db;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.businessdocumentsettings.BusinessDocumentSettings;

@Service
public class BusinessDocumentSettingsServiceImpl extends BaseServiceImpl<BusinessDocumentSettings, Integer> implements BusinessDocumentSettingsService {

	@Autowired
	private BusinessDocumentSettingsDao baseDao; 
	
	@Override
	protected BaseDao<BusinessDocumentSettings, Integer> getBaseDao() {
		return baseDao;
	}
	
	private static Logger logger = LoggerFactory.getLogger(BusinessDocumentSettingsServiceImpl.class);

	@Override
	public BusinessDocumentSettings getForCompanyOrDefault(Integer businessCompanyId) {
		if (businessCompanyId == null) throw new IllegalArgumentException("businessCompanyId was null");
		List<BusinessDocumentSettings> results = this.baseDao.getForCompanyAndDefault(businessCompanyId);
		// If single result, return it, otherwise filter to company specific result
		if (results.isEmpty()) throw new RuntimeException("Configuration error, no BusinessDocumentSettings found");
		BusinessDocumentSettings result = null;
		if (results.size() == 1) {
			result = results.get(0);
		}
		else {
			result = results.stream().
					filter(settings -> !settings.getGlobalDefault()).
					findFirst().
					orElse(null);
		}
		if (result == null)
			throw new RuntimeException("Configuration error, "+results.size()+" results with no company specific BusinessDocumentSettings found");
		logger.info("Returning result id "+result.getId()+" out of "+results.size()+" potential matches");
		return result;
	}
	
}
