package org.trescal.cwms.core.company.entity.subdivinstructionlink.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

@Service
public class SubdivInstructionLinkServiceImpl extends
		BaseServiceImpl<SubdivInstructionLink, Integer> implements
		SubdivInstructionLinkService {

	@Autowired
	private SubdivInstructionLinkDao baseDao;
	
	@Override
	protected BaseDao<SubdivInstructionLink, Integer> getBaseDao() {
		return baseDao;
	}

	@Override
	public List<SubdivInstructionLink> getAllForSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv) {
		return baseDao.getForSubdivAndTypes(subdiv, allocatedSubdiv, new InstructionType[] {});
	}
	
	@Override
	public List<SubdivInstructionLink> getForSubdivAndTypes(Subdiv subdiv, Subdiv allocatedSubdiv, InstructionType... types) {
		return baseDao.getForSubdivAndTypes(subdiv, allocatedSubdiv, types);
	}
}