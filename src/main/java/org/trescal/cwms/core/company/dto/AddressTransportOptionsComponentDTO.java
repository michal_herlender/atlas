package org.trescal.cwms.core.company.dto;

import lombok.Data;

@Data
public class AddressTransportOptionsComponentDTO {

	private Integer addressSettingsId;
	private String businessCompany;
	private Integer businessSubdivId;
	private String businessSubdiv;
	private String transportIn;
	private String localizedNameTransportIn;
	private String transportOut;
	private String localizedNameTransportOut;
	private Boolean hasRightToDelete;

	public AddressTransportOptionsComponentDTO(Integer addressSettingsId, String businessCompany, Integer businessSubdivId,
			String businessSubdiv, String transportIn, String localizedNameTransportIn, String transportOut, String localizedNameTransportOut) {
		this.addressSettingsId = addressSettingsId;
		this.businessCompany = businessCompany;
		this.businessSubdivId = businessSubdivId;
		this.businessSubdiv = businessSubdiv;
		this.transportIn = transportIn;
		this.localizedNameTransportIn = localizedNameTransportIn;
		this.transportOut = transportOut;
		this.localizedNameTransportOut = localizedNameTransportOut;
		this.hasRightToDelete = false;
	}
}