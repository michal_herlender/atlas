package org.trescal.cwms.core.company.entity.company.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.dto.CompanyDTO;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.dto.CompanySearchResultWrapper;
import org.trescal.cwms.core.company.dto.CompanyToContactDetailsDTO;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.addresssettings.db.AddressSettingsForAllocatedSubdivService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.businessarea.db.BusinessAreaService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditation;
import org.trescal.cwms.core.company.entity.companyaccreditation.db.CompanyAccreditationService;
import org.trescal.cwms.core.company.entity.companygroup.db.CompanyGroupService;
import org.trescal.cwms.core.company.entity.companyhistory.CompanyHistory;
import org.trescal.cwms.core.company.entity.companyhistory.db.CompanyHistoryService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.companystatus.CompanyStatus;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.country.db.CountryService;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.entity.supplier.Supplier;
import org.trescal.cwms.core.company.entity.supplier.SupplierApprovalType;
import org.trescal.cwms.core.company.entity.supplier.db.SupplierService;
import org.trescal.cwms.core.company.entity.vatrate.db.VatRateService;
import org.trescal.cwms.core.company.form.CompanyAddForm;
import org.trescal.cwms.core.company.form.CompanyEditForm;
import org.trescal.cwms.core.company.form.genericentityvalidator.CompanyToContactValidator;
import org.trescal.cwms.core.company.projection.CompanyProjectionDTO;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.portal.usergroup.db.UserGroupService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentDao;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.userright.entity.limitcompany.LimitCompany;
import org.trescal.cwms.core.userright.entity.limitcompany.db.LimitCompanyService;
import org.trescal.cwms.core.userright.entity.permissionlimit.db.PermissionLimitDao;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_CompanyOnStop;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_CompanyTrusted;
import org.trescal.cwms.core.workflow.dto.SelectedJobItemDTO;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.workassignment.db.WorkAssignmentService;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;
import org.trescal.cwms.spring.model.LabelIdDTO;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service("CompanyService")
public class CompanyServiceImpl extends BaseServiceImpl<Company, Integer> implements CompanyService {

	@Autowired
	private AddressSettingsForAllocatedSubdivService addressSettingsService;
	@Autowired
	private AddressService addrServ;
	@Autowired
	private BusinessAreaService businessAreaServ;
	@Autowired
	private CompanyDao compDao;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private CompanyToContactValidator validator;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private CompanyAccreditationService compAccServ;
	@Autowired
	private CompanyGroupService companyGroupService;
	@Autowired
	private CompanyHistoryService companyHistoryService;
	@Autowired
	private ContactService conServ;
	@Autowired
	private CountryService countryServ;
	@Autowired
	private HookInterceptor_CompanyOnStop interceptor_onStop;
	@Autowired
	private HookInterceptor_CompanyTrusted interceptor_trusted;
	@Autowired
	private LimitCompanyService limitCompanyService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private PermissionLimitDao permissionLimitDao;
	@Autowired
	private SubdivService subServ;
	@Autowired
	private SupplierService supplierServ;
	@Autowired
	private SupportedCurrencyService currencyService;
	@Autowired
	private SystemComponentDao scDao;
	@Autowired
	private TransportOptionService transOptServ;
	@Autowired
	private UserService userService;
	@Autowired
	private UserGroupService userGroupServ;
	@Autowired
	private VatRateService vatRateService;
	@Autowired
	private ItemStateService itemStateServ;
	@Autowired
	private WorkAssignmentService workAssignmentServ;
	@Autowired
	private JobItemService jobItemService;


	@Override
	protected BaseDao<Company, Integer> getBaseDao() {
		return this.compDao;
	}

	@Override
	public Company createCompany(CompanyAddForm acf, Company allocatedCompany, Contact defaultBusinessContact) {
		// create the Company
		Company company = new Company(acf.getConame().trim(), acf.getCompanyRole(), LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		company.setLegalIdentifier(acf.getLegalIdentifier().trim());
		company.setDefaultBusinessContact(defaultBusinessContact);
		company.setDocumentLanguage(allocatedCompany.getDocumentLanguage());
		company.setCountry(this.countryServ.get(acf.getCountryId()));
		company.setCurrency(this.currencyService.findSupportedCurrency(acf.getCurrencyId()));
		// create settings for current allocated (business) company (saved via
		// cascade)
		this.companySettingsService.initializeForCompany(company, allocatedCompany, true);

		// add business specific fields
		if (acf.getCompanyRole() == CompanyRole.BUSINESS) {
			company.setCompanyCode(acf.getCompanyCode().trim());
		} else {
			company.setCompanyCode("");
		}

		// add client specific fields
		if (acf.getCompanyRole() == CompanyRole.CLIENT && (acf.getBusinessAreaId() != null)) {
			company.setBusinessarea(this.businessAreaServ.get(acf.getBusinessAreaId()));
		}

		this.save(company);

		// Init Purchase limit permission
		if (company.getCompanyRole() == CompanyRole.BUSINESS) {
			LimitCompany limit;

			List<Permission> permissionList = new ArrayList<>();
			permissionList.add(Permission.PURCHASE_ORDER_OPEX_LIMIT_1);
			permissionList.add(Permission.PURCHASE_ORDER_OPEX_LIMIT_2);
			permissionList.add(Permission.PURCHASE_ORDER_OPEX_LIMIT_3);
			permissionList.add(Permission.PURCHASE_ORDER_OPEX_LIMIT_4);
			permissionList.add(Permission.PURCHASE_ORDER_OPEX_LIMIT_5);
			permissionList.add(Permission.PURCHASE_ORDER_OPEX_LIMIT_6);
			permissionList.add(Permission.PURCHASE_ORDER_OPEX_LIMIT_7);
			permissionList.add(Permission.PURCHASE_ORDER_GENEX_LIMIT_1);
			permissionList.add(Permission.PURCHASE_ORDER_GENEX_LIMIT_2);
			permissionList.add(Permission.PURCHASE_ORDER_GENEX_LIMIT_3);
			permissionList.add(Permission.PURCHASE_ORDER_GENEX_LIMIT_4);
			permissionList.add(Permission.PURCHASE_ORDER_GENEX_LIMIT_5);
			permissionList.add(Permission.PURCHASE_ORDER_GENEX_LIMIT_6);
			permissionList.add(Permission.PURCHASE_ORDER_GENEX_LIMIT_7);
			permissionList.add(Permission.PURCHASE_ORDER_CAPEX_LIMIT_1);
			permissionList.add(Permission.PURCHASE_ORDER_CAPEX_LIMIT_2);
			permissionList.add(Permission.PURCHASE_ORDER_CAPEX_LIMIT_3);
			permissionList.add(Permission.PURCHASE_ORDER_CAPEX_LIMIT_4);
			permissionList.add(Permission.PURCHASE_ORDER_CAPEX_LIMIT_5);
			permissionList.add(Permission.PURCHASE_ORDER_CAPEX_LIMIT_6);
			permissionList.add(Permission.PURCHASE_ORDER_CAPEX_LIMIT_7);
			permissionList.add(Permission.QUOTATION_CUSTOMER_LOW_LIMIT);
			permissionList.add(Permission.QUOTATION_CUSTOMER_MEDIUM_LIMIT);
			permissionList.add(Permission.QUOTATION_CUSTOMER_HIGH_LIMIT);
			for (Permission perm : permissionList) {
				limit = new LimitCompany();
				limit.setMaximum(new BigDecimal(0));
				limit.setOrganisation(company);
				limit.setPermissionLimit(permissionLimitDao.find(perm));
				limitCompanyService.save(limit);
			}
		}
		// set initial non-approved status for supplier
		if (acf.getCompanyRole() == CompanyRole.SUPPLIER) {
			Supplier supplier = this.supplierServ.getForApprovalType(SupplierApprovalType.NOT_APPROVED);
			CompanyAccreditation ca = new CompanyAccreditation();
			ca.setOrganisation(allocatedCompany);
			ca.setCompany(company);
			ca.setSupplier(supplier);
			this.compAccServ.saveOrUpdateCompanyAccreditation(ca);
		}
		// create a new default Subdivision
		Subdiv defaultSubdiv = new Subdiv();
		defaultSubdiv.setComp(company);
		defaultSubdiv.setSiretNumber(acf.getSiretNumber());
		defaultSubdiv.setCreateDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		defaultSubdiv.setSubname(acf.getSubdivName());
		// create subdiv settings
		defaultSubdiv.setActive(true);
		this.subServ.save(defaultSubdiv);
		company.setDefaultSubdiv(defaultSubdiv);
		return company;
	}

	@Override
	public void editCompany(CompanyEditForm cf, String username) {
		Company allocatedCompany = this.get(cf.getBusinessCompanyId());
		Company c = this.get(cf.getCoid());

		// if the name is changing (and user specified to save this name)
		// add a
		// row for the name history
		if (!c.getConame().equals(cf.getConame())) {
			if (cf.isSaveNameChange()) {
				Contact editor = this.userService.get(username).getCon();
				CompanyHistory ch = new CompanyHistory();
				ch.setComp(c);
				ch.setConame(c.getConame());
				ch.setValidTo(Calendar.getInstance().getTime());
				ch.setChangedBy(editor);
				this.companyHistoryService.save(ch);
			}
			c.setConame(cf.getConame());
		}
		if (c.getCompanyRole() == CompanyRole.CLIENT) {
			c.setBusinessarea(this.businessAreaServ.get(cf.getBusinessareaid()));
		}
		c.setCompanyGroup(cf.getCompanyGroupId() == 0 ? null : this.companyGroupService.get(cf.getCompanyGroupId()));
		c.setCountry(this.countryServ.get(cf.getCountryid()));
		c.setCompanyCode(cf.getCompanyCode());
		if (cf.getChangeBusinessContact()) {
			c.setDefaultBusinessContact(conServ.get(cf.getPrimaryBusinessContactId()));
		}
		c.setGroupName(cf.getGroupName());
		CompanySettingsForAllocatedCompany settings = this.companySettingsService.getByCompany(c, allocatedCompany);
		if (c.getSettingsForAllocatedCompanies() == null) {
			settings = this.companySettingsService.initializeForCompany(c, allocatedCompany, false);
		}
		settings.setCompanyReference(cf.getCompanyRef());
		settings.setContract(cf.isContract());
		settings.setSyncToPlantillas(cf.isSyncToPlantillas());
		settings.setTaxable(cf.isTaxable());
		settings.setRequireSupplierCompanyList(cf.getRequireSupplierCompanyList());
		settings.setVatrate(vatRateService.get(cf.getVatCode()));
		// If changing from PROSPECT to CLIENT, set To-Check, Inactive, and
		// On-Stop
		if ((c.getCompanyRole() == CompanyRole.PROSPECT) && (cf.getCompanyRole() == CompanyRole.CLIENT)) {
			settings.setActive(false);
			settings.setOnStop(true);
			settings.setStatus(CompanyStatus.TO_CHECK);
		}
		c.setCompanyRole(cf.getCompanyRole());
		// update the currency settings for the company
		c.setCurrency(cf.isSpecificCurrency() ? this.currencyService.findSupportedCurrency(cf.getSpecificCurrencyId())
				: null);
		c.setRate(cf.isSpecificCurrency() && cf.isSpecificRate() ? cf.getSpecificRateValue() : null);
		c.setDocumentLanguage(Locale.forLanguageTag(cf.getDocumentLanguage()));
		if (cf.getFormerId() != null) {
			c.setFormerID(cf.getFormerId());
		}
	}

	@Override
	public boolean existsByName(String name, CompanyRole companyRole, Company excludeCompany) {
		if (compDao.existsByName(name, companyRole, excludeCompany))
			return true;
		else {
			for (CompanyRole cr : companyRole.editableTo())
				if (compDao.existsByName(name, cr, excludeCompany))
					return true;
		}
		return false;
	}

	@Override
	public boolean existsByLegalIdentifier(String legalIdentifier, CompanyRole companyRole, Company excludeCompany) {
		if (compDao.existsByLegalIdentifier(legalIdentifier, companyRole, excludeCompany))
			return true;
		else {
			for (CompanyRole cr : companyRole.editableTo())
				if (compDao.existsByLegalIdentifier(legalIdentifier, cr, excludeCompany))
					return true;
		}
		return false;
	}

	@Override
	public Company get(Integer id) {
		Company c = this.compDao.find(id);
		// 2018-05-15 GB : Prevent directory lookup on repeated gets.
		if (c != null && c.getDirectory() == null) {
			c.setDirectory(this.compDirServ.getDirectory(this.scDao.findComponent(Component.COMPANY),
					String.valueOf(c.getCoid()), null));
		}
		return c;
	}

	@Override
	public List<CompanyKeyValue> findCustomerCompanies(Contact contact, Scope scope, Boolean active) {
		return compDao.findCustomerCompanies(contact, scope, active);
	}

	public List<CompanyKeyValue> findCustomerCompaniesBySubdiv(Subdiv subdiv, Boolean active) {
		return compDao.findCustomerCompaniesBySubdiv(subdiv, active);
	}

	@Override
	public List<Company> getCompaniesByRole(CompanyRole companyRole) {
		return this.compDao.getCompaniesByRole(companyRole);
	}

	@Override
	public void save(Company c) {
		this.compDao.persist(c);
		// call to compDirServ to create the new directory for the company
		this.compDirServ.getDirectory(this.scDao.findComponent(Component.COMPANY), String.valueOf(c.getCoid()), null);
	}

	@Override
	public void overrideItemsToOnStop(List<JobItem> items) {
		this.interceptor_onStop.recordAction(items);
	}

	@Override
	public void overrideItemsToTrusted(List<JobItem> items) {
		this.interceptor_trusted.recordAction(items);
	}

	@Override
	public List<CompanySearchResultWrapper> searchByCompanyRoles(String searchString, List<CompanyRole> roles,
																 Company allocatedCompany, Boolean active, Boolean anywhereActive, Boolean searchName, Boolean searchEverywhere) {
		return this.compDao.searchByCompanyRoles(searchString, roles, allocatedCompany, active, anywhereActive,
				searchName, searchEverywhere);
	}

	@Override
	public SupportedCurrency getCurrency(Company company) {
		return (company.getCurrency() != null) ? company.getCurrency() : currencyService.getDefaultCurrency();
	}

	@Override
	public SupportedCurrency getCurrencyById(Integer companyId){
		return getCurrency(get(companyId));
	}

	@Override
	public CompanyToContactDetailsDTO insertFreehandContact(CompanyToContactDetailsDTO ctcdto, Integer subdivId,
			String username) {

		// get the current contact
		Contact currentContact = this.userService.get(username).getCon();
		// get allocated company
		Subdiv allocatedSubdiv = subServ.get(subdivId);
		// validate the company information
		BindException errors = new BindException(ctcdto, "ctcdto");
		this.validator.validate(ctcdto, errors);
		// does the form contain errors?
		if (errors.hasErrors()) {

			List<KeyValue<String, String>> fieldErrorMessages = errors.getAllErrors().stream()
				.filter(oe -> oe instanceof FieldError).map(oe -> (FieldError) oe)
				.map(fe -> new KeyValue<>(fe.getField(), fe.getDefaultMessage())).collect(Collectors.toList());

			return new CompanyToContactDetailsDTO(true, fieldErrorMessages);
		}
		// create the Company
		Company company = new Company(ctcdto.getConame().trim(), ctcdto.getCompanyRole(), LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		company.setLegalIdentifier(ctcdto.getLegalIdentifier());
		company.setDefaultBusinessContact(currentContact);
		company.setCountry(this.countryServ.get(ctcdto.getCountryId()));
		company.setBusinessarea(this.businessAreaServ.get(ctcdto.getBusinessAreaId()));
		company.setCurrency(this.currencyService.findSupportedCurrency(ctcdto.getCurrencyId()));
		company.setDocumentLanguage(allocatedSubdiv.getComp().getDocumentLanguage());
		// create settings for current allocated (business) company
		this.companySettingsService.initializeForCompany(company, allocatedSubdiv.getComp(), true);
		// insert the new company
		this.compDao.persist(company);
		// create a new default subdivision for company
		Subdiv defaultSubdiv = new Subdiv();
		defaultSubdiv.setComp(company);
		defaultSubdiv.setCreateDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		if ((ctcdto.getSubname() != null) && !ctcdto.getSubname().equals(""))
			defaultSubdiv.setSubname(ctcdto.getSubname());
		else {
			Locale locale = LocaleContextHolder.getLocale();
			String defaultName = messageSource.getMessage("unnamedsubdivision", null, "Un-named subdivision", locale);
			defaultSubdiv.setSubname(defaultName);
		}
		defaultSubdiv.setActive(true);
		// insert the new subdivision
		this.subServ.save(defaultSubdiv);
		// create a new address for subdivision
		Address defAddress = new Address();
		defAddress.setActive(true);
		defAddress.setAddr1(ctcdto.getAddr1());
		defAddress.setAddr2(ctcdto.getAddr2() != null ? ctcdto.getAddr2() : "");
		defAddress.setAddr3(ctcdto.getAddr3() != null ? ctcdto.getAddr3() : "");
		defAddress.setTown(ctcdto.getTown());
		defAddress.setCounty(ctcdto.getCounty());
		defAddress.setPostcode(ctcdto.getPostcode());
		defAddress.setCountry(this.countryServ.get(ctcdto.getCountryId()));
		defAddress.setSub(defaultSubdiv);
		AddressSettingsForAllocatedSubdiv addressSettings = this.addressSettingsService.initializeForAddress(defAddress,
				allocatedSubdiv);
		if (ctcdto.getTransportinId() != 0) {
			addressSettings.setTransportIn(this.transOptServ.get(ctcdto.getTransportinId()));
		}
		if (ctcdto.getTransportoutId() != 0) {
			addressSettings.setTransportOut(this.transOptServ.get(ctcdto.getTransportoutId()));
		}

		// get all address types
		AddressType[] addTypeList = AddressType.class.getEnumConstants();
		// add address types for invoice and delivery
		for (AddressType at : addTypeList)
			if (at.equals(AddressType.DELIVERY) || at.equals(AddressType.INVOICE)) {
				if (defAddress.getAddressType() == null)
					defAddress.setAddressType(new TreeSet<>());
				defAddress.getAddressType().add(at);
			}
		// insert the new address
		this.addrServ.save(defAddress);
		// create a new contact for subdivision
		Contact contact = new Contact();
		contact.setActive(true);
		contact.setFirstName(ctcdto.getFirstname());
		contact.setLastName(ctcdto.getLastname());
		contact.setTelephone(ctcdto.getTelephone());
		contact.setMobile(ctcdto.getMobile());
		contact.setEmail(ctcdto.getEmail());
		contact.setFax(ctcdto.getFax());
		contact.setSub(defaultSubdiv);
		contact.setCreateDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		contact.setUsergroup(this.userGroupServ.getDefaultGroup());
		contact.setDefAddress(defAddress);
		contact.setDefaultPrinter(null);
		contact.setSd(null);
		contact.setLocale(allocatedSubdiv.getComp().getDocumentLanguage());
		// insert the new contact (without creating user account!)
		this.conServ.save(contact);
		// set default contact on subdivision (will save automatically via
		// cascade)
		defaultSubdiv.setDefaultContact(contact);
		defaultSubdiv.setDefaultAddress(defAddress);
		// update subdivision
		// this.subServ.update(defaultSubdiv);
		return ctcdto;
	}

	@Override
	public List<LabelIdDTO> getClientLabelIdList(String nameFragment, Company allocatedCompany, Integer maxResults) {
		List<CompanyRole> roles = Collections.singletonList(CompanyRole.CLIENT);
		List<CompanySearchResultWrapper> companies = compDao.searchByCompanyRoles(nameFragment, true, roles,
				false, allocatedCompany);
		return companies.stream().map(c -> new LabelIdDTO(c.getConame(), c.getCoid())).collect(Collectors.toList());
	}

	@Override
	public List<CompanyKeyValue> getCompaniesFromSubdivs(List<Integer> subdivs) {
		return this.compDao.getCompaniesFromSubdivs(subdivs);
	}

	@Override
	public List<CompanyProjectionDTO> getDTOList(Collection<Integer> companyIds, Integer allocatedCompanyId) {
		List<CompanyProjectionDTO> result = new ArrayList<>();
		if (!companyIds.isEmpty()) {
			result = this.compDao.getDTOList(companyIds, allocatedCompanyId);
		}
		return result;
	}

	@Override
	public CompanyDTO getForLinkInfo(Integer companyId, Integer allocatedCompanyId) {
		return compDao.getForLinkInfo(companyId, allocatedCompanyId);
	}

	@Override
	public Integer getCompanyId(Integer clientSubdivId) {
		return subServ.get(clientSubdivId).getComp().getId();
	}
	
	@Override
	public List<KeyValueIntegerString> getCompaniesByRoleForAllocatedCompany(List<CompanyRole> roles , Company allocatedCompany, 
			List<Integer> companiesIdsAlreadyExist) {
		return compDao.getCompaniesByRoleForAllocatedCompany(roles, allocatedCompany, companiesIdsAlreadyExist);
	}

	@Override
	public List<CompanyProjectionDTO> getClientCompaniesDTosFromSubdiv(Subdiv subdiv) {

		EnumSet<StateGroup> stateGroups = EnumSet.of(StateGroup.CALIBRATION, StateGroup.AWAITINGCALIBRATION,
				StateGroup.RESUMECALIBRATION, StateGroup.AWAITINGGENERALSERVICEOPERATION,
				StateGroup.GENERALSERVICEOPERATION, StateGroup.RESUMEGENERALSERVICEOPERATION,
				StateGroup.AWAITING_REPAIR_WORK_BY_TECHNICIAN);
		List<Integer> stateIds = itemStateServ.getItemStateIdsForStateGroups(stateGroups);

		List<Integer> procIds = new ArrayList<Integer>();


			for (Department dept : subdiv.getDepartments()) {
				if (dept.getType().isProcedures()) {
					procIds.addAll(workAssignmentServ.getCapabilitiesIdsAssignedToDepartment(dept));
				}
			}

		List<SelectedJobItemDTO> selectedJobItems = this.jobItemService.getActiveJobItems(procIds, stateIds, false,
				subdiv, null, null);
		Set<Integer> clientCompanyIds = selectedJobItems.stream().map(SelectedJobItemDTO::getClientCompanyId)
				.collect(Collectors.toSet());

		return this.getDTOList(clientCompanyIds,null);
	}

}