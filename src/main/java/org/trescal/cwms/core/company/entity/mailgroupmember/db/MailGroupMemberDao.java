package org.trescal.cwms.core.company.entity.mailgroupmember.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.mailgroupmember.MailGroupMember;
import org.trescal.cwms.core.company.entity.mailgrouptype.MailGroupType;

public interface MailGroupMemberDao extends BaseDao<MailGroupMember, Integer> {
	
	MailGroupMember findMailGroupMember(Integer personid, MailGroupType mailGroupType);
	
	List<MailGroupMember> getContactMailGroups(Integer personid);
	
	List<MailGroupMember> getMembersOfGroupForSubdiv(MailGroupType mailGroupType, int subdivid, boolean activeContactsOnly);
}