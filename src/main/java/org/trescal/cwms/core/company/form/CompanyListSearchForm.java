package org.trescal.cwms.core.company.form;

import org.trescal.cwms.core.company.entity.companylist.CompanyList;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CompanyListSearchForm {

	private Integer orgid;
	private String name;
	private Boolean activestatus;
	// results data
	private PagedResultSet<CompanyList> rs;
	private int pageNo;
	private int resultsPerPage;
}
