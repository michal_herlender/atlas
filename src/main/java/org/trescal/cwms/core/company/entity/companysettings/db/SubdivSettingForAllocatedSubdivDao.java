package org.trescal.cwms.core.company.entity.companysettings.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.dto.SubdivSettingsForAllocatedSubdivDto;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

import java.util.List;

public interface SubdivSettingForAllocatedSubdivDao extends BaseDao<SubdivSettingsForAllocatedSubdiv, Integer> {
	
	SubdivSettingsForAllocatedSubdiv get(Subdiv client, Subdiv business);
	
	SubdivSettingsForAllocatedSubdiv get(Integer subdivId, Integer allocatedSubdivId);

    List<SubdivSettingsForAllocatedSubdivDto> findBySubdivId(Integer subdivId);
}