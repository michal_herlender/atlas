package org.trescal.cwms.core.company.form;

import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;

import lombok.Data;

@Data
public class AddressAddForm {
	
	@Valid
	private Address address;
	private Set<AddressType> addressTypes;
	private AddressSettingsForAllocatedCompany addressSettingsCompany;
	@NotNull
	private Integer countryId;
	private Integer labelPrinterId;
	private Integer printerId;
	private Integer transportInId;
	private Integer transportOutId;
	private boolean overrideCompanyVatRate;
	private String overrideVatCode;
	private Integer businessCompanyId;	// Needed to pass to validation
	private Integer addressId;
 }