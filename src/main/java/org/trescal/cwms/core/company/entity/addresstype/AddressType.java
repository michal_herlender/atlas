package org.trescal.cwms.core.company.entity.addresstype;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum AddressType {
	WITHOUT("addresstype.undefined","Undefined"),	// Zero based enum defined due to data migration constraints
	INVOICE("addresstype.invoice", "Invoice"), 
	DELIVERY("addresstype.delivery", "Delivery"),
	CERTIFICATE("addresstype.certificate", "Certificate"), 
	POFROM("addresstype.pofrom", "PO From"),
	LEGAL_REGISTRATION("addresstype.legalregistration", "Legal Registration");

	private ReloadableResourceBundleMessageSource messages;
	private String messageCode;
	private String defaultMessage;
		
	private AddressType(String messageCode, String defaultMessage) {
		this.messageCode = messageCode;
		this.defaultMessage = defaultMessage;
	}
	
	@Component
	public static class AddressTypeMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (AddressType rt : EnumSet.allOf(AddressType.class))
               rt.setMessageSource(messages);
        }
	}
	
	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}
	
	public static AddressType getById(Integer id) {
		return AddressType.values()[id];
	}
	
	/*
	 * Required to be able to use existing calls like $type.id in velocity templates 
	 */
	public int getId() {
		return super.ordinal();
	}
	
	public String getType() {
		Locale locale = LocaleContextHolder.getLocale();
		if (messages != null) 
			return messages.getMessage(messageCode, null, defaultMessage, locale);
		else
			return defaultMessage;
	}
}
