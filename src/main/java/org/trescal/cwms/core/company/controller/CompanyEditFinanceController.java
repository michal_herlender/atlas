package org.trescal.cwms.core.company.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.trescal.cwms.core.account.entity.bankaccount.db.BankAccountService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.companystatus.CompanyStatus;
import org.trescal.cwms.core.company.entity.invoicefrequency.InvoiceFrequency;
import org.trescal.cwms.core.company.entity.invoicefrequency.db.InvoiceFrequencyService;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentmode.db.PaymentModeService;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.company.form.CompanyEditFinanceForm;
import org.trescal.cwms.core.company.form.CompanyEditFinanceValidator;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValue;


/*
 * Base definitions used by all finance edit screen variants as they share the same view
 * with controllers separated by company role.
 */

public abstract class CompanyEditFinanceController {
	public static final String FORM_NAME = "form";
	public static final String REFERENCE_NAME = "reference";
	public static final String VIEW_NAME_EDIT = "trescal/core/company/companyeditfinance";
	public static final String VIEW_NAME_COMPANY = "viewcomp.htm?loadtab=finance-tab&coid=";
	public static final String PARAM_COID = "coid";
	public static final String MESSAGE_CODE_NONE = "company.none";
	public static final String ERROR_CODE_NO_BANK_ACCOUNTS = "error.company.finance.nobankaccounts";
	public static final String ERROR_MESSAGE_NO_BANK_ACCOUNTS = "No bank accounts are defined for this company";

	@Autowired
	protected BankAccountService bankAccountService;
	@Autowired
	protected CompanyEditFinanceValidator validator;
	@Autowired
	protected CompanyService companyService;
	@Autowired
	protected CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	protected InvoiceFrequencyService invoiceFrequencyService;
	@Autowired
	protected PaymentModeService paymentModeService; 	
	@Autowired
	protected MessageSource messageSource;
	@Autowired
	protected TranslationService translationService; 
	
	@InitBinder(FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	/*
	 * Performs common updates shared between different company roles.
	 * Returns CompanySettingsForAllocatedCompany for further potential use.
	 */
	protected CompanySettingsForAllocatedCompany performUpdates(CompanyEditFinanceForm form, Company company, Company allocatedCompany) {
		company.setLegalIdentifier(form.getLegalIdentifier());
		company.setFiscalIdentifier(form.getFiscalIdentifier());
		
		CompanySettingsForAllocatedCompany settings = this.companySettingsService.getByCompany(company, allocatedCompany);
		if (settings == null) settings = this.companySettingsService.initializeForCompany(company, allocatedCompany, false);
		settings.setActive(form.getActive());
		settings.setOnStop(form.getOnStop());
		settings.setStatus(form.getStatus());
		settings.setInvoiceFrequency(this.invoiceFrequencyService.get(form.getInvoiceFrequencyId()));
		settings.setPaymentterm(form.getPaymentTerm());
		settings.setPaymentMode(this.paymentModeService.get(form.getPaymentModeId()));
		settings.setInvoiceFactoring(form.getInvoiceFactoring());
		return settings;
	}
	/*
	 * Initializes shared settings on form regardless of company role
	 */
	protected CompanySettingsForAllocatedCompany initForm(CompanyEditFinanceForm form, Company company, Company allocatedCompany) {
		CompanySettingsForAllocatedCompany settings = this.companySettingsService.getByCompany(company, allocatedCompany);
		if (settings == null) settings = this.companySettingsService.initializeForCompany(company, allocatedCompany, false);
		form.setLegalIdentifier(company.getLegalIdentifier());
		form.setFiscalIdentifier(company.getFiscalIdentifier() == null ? "" : company.getFiscalIdentifier());
		form.setActive(settings.isActive());
		form.setOnStop(settings.isOnStop());
		form.setStatus(settings.getStatus());
		form.setInvoiceFrequencyId(settings.getInvoiceFrequency() == null ? 0 : settings.getInvoiceFrequency().getId());
		form.setPaymentModeId(settings.getPaymentMode() == null ? 0 : settings.getPaymentMode().getId());
		form.setPaymentTerm(settings.getPaymentterm());
		form.setInvoiceFactoring(settings.getInvoiceFactoring());
		form.getClass().isAssignableFrom(org.trescal.cwms.core.company.form.CompanyEditBusinessForm.class);
		return settings;
	}
	protected Map<String, Object> initReferenceMap(int coid, Locale locale) {
		Map<String, Object> map = new HashMap<>();
		
		map.put("paymentmodes", getPaymentModeDTOs(locale));
		map.put("invoicefrequencies", getInvoiceFrequencyDTOs(locale)); 
		map.put("paymentterms", Arrays.stream(PaymentTerm.values()).collect(Collectors.toMap(PaymentTerm:: name, PaymentTerm::getMessage)));
		map.put("status", CompanyStatus.valueMap());
		map.put("company", this.companyService.get(coid));
		return map;
	}
	private List<KeyValue<Integer, String>> getPaymentModeDTOs(Locale locale) {
		List<KeyValue<Integer, String>> list = new ArrayList<>();
		list.add(new KeyValue<Integer, String>(0, messageSource.getMessage(MESSAGE_CODE_NONE, null, locale)));
		List<PaymentMode> paymentModes = this.paymentModeService.getAll();
		for (PaymentMode paymentMode : paymentModes) {
			String description = paymentMode.getCode() + " - " + this.translationService.getCorrectTranslation(paymentMode.getTranslations(), locale);
			list.add(new KeyValue<Integer, String>(paymentMode.getId(), description));
		}
		return list;
	}
	private List<KeyValue<Integer, String>> getInvoiceFrequencyDTOs(Locale locale) {
		List<KeyValue<Integer, String>> list = new ArrayList<>();
		list.add(new KeyValue<Integer, String>(0, messageSource.getMessage(MESSAGE_CODE_NONE, null, locale)));
		List<InvoiceFrequency> invoiceFrequencies = this.invoiceFrequencyService.getAll(); 
		for (InvoiceFrequency invoiceFrequency : invoiceFrequencies) {
			String description = invoiceFrequency.getCode() + " - " + this.translationService.getCorrectTranslation(invoiceFrequency.getTranslations(), locale);
			list.add(new KeyValue<Integer, String>(invoiceFrequency.getId(), description));
		}
		return list;
	}
}
