/**
 * 
 */
package org.trescal.cwms.core.company.entity.company.db;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.company.dto.CompanySearchResultWrapper;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;

/**
 * 2015-10-29 GB - created in order to separate apart DWR calls only such that
 * company service is not exposed, as new BaseService methods were not being
 * properly converted. Also helps identify what is truly DWR for future upgrade
 * purposes.
 */
public interface CompanyDWRService {

	/**
	 * Returns a {@link List} of {@link CompanySearchResultWrapper} whose
	 * (case-insensitive) name matches the given name and are active or not and the
	 * corole is one that has been supplied in the role array. {@link Mfr} s).
	 * 
	 * @param nameFragment the name to match.
	 * @param active       the status of the Company.
	 * @param role         array of role names to match in results.
	 * @return {@link List} of {@link CompanySearchResultWrapper}.
	 */
	List<CompanySearchResultWrapper> searchCompanyByRolesHQL(String searchName, boolean active, boolean includeDeactivatedCompanies, 
			String[] role, HttpSession session);

	/**
	 * Updates the {@link Company} identified by the given coid to have the
	 * {@link Subdiv} identified by the given subdivid as it's default
	 * {@link Subdiv}.
	 * 
	 * @param coid     the company id, not null.
	 * @param subdivid the subdivid, not null.
	 */
	void updateCompanySubdivDefault(int coid, int subdivid);
}