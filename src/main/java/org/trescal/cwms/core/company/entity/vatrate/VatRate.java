package org.trescal.cwms.core.company.entity.vatrate;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.country.Country;

@Entity
@Table(name = "vatrate")
public class VatRate extends Auditable
{
	private Set<CompanySettingsForAllocatedCompany> settingsForAllocatedCompanies;
	private String description;
	private BigDecimal rate;
	private String vatCode;
	private Country country;
	private VatRateType type;

	@OneToMany(mappedBy="vatrate")
	public Set<CompanySettingsForAllocatedCompany> getSettingsForAllocatedCompanies() {
		return settingsForAllocatedCompanies;
	}

	@Length(max = 50)
	@Column(name = "description", length = 50)
	public String getDescription()
	{
		return this.description;
	}

	@Column(name = "rate", precision = 4, scale = 2)
	public BigDecimal getRate()
	{
		return this.rate;
	}

	/*
	 * Description for display purposes e.g.:
	 * 20.00% - Country Default - Spain 
	 * 0.00% - Country Special - Spain - Canary Islands 
	 * 0.00% - Eurozone Internal
	 * 0.00% - Eurozone External
	 * 0.00% - Non-Eurozone Internal
	 * @deprecated use VatRateFormatter component
	 */
	@Transient
	@Deprecated
	public String getExtendedDescription() {
		StringBuffer result = new StringBuffer();
		if (getRate() != null) {
			result.append(getRate().toString());
			result.append("%");
		}
		if (getType() != null ) {
			result.append(" - ");
			result.append(getType().getMessage());
		}
		if (getCountry() != null) {
			result.append(" - ");
			result.append(getCountry().getLocalizedName());
			if (VatRateType.COUNTRY_SPECIAL.equals(getType())) {
				result.append(" - ");
				result.append(getDescription());
			}
		}
		
		return result.toString();
	}
	
	@Length(max = 1)
	@Id
	@Column(name = "vatcode", length = 1)
	public String getVatCode()
	{
		return this.vatCode;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name="type")
	public VatRateType getType() {
		return type;
	}

	@ManyToOne
	@JoinColumn(name="countryid")
	public Country getCountry() {
		return country;
	}

	public void setSettingsForAllocatedCompanies(
			Set<CompanySettingsForAllocatedCompany> settingsForAllocatedCompanies) {
		this.settingsForAllocatedCompanies = settingsForAllocatedCompanies;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setRate(BigDecimal rate)
	{
		this.rate = rate;
	}

	public void setVatCode(String vatCode)
	{
		this.vatCode = vatCode;
	}

	public void setType(VatRateType type) {
		this.type = type;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
}
