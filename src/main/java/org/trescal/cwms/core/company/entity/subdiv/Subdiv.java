package org.trescal.cwms.core.company.entity.subdiv;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Setter;
import org.hibernate.annotations.SortComparator;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.AddressComparator;
import org.trescal.cwms.core.company.entity.businesssubdivisionsettings.BusinessSubdivisionSettings;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.FirstNameContactComparator;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.DepartmentComparator;
import org.trescal.cwms.core.company.entity.organisation.OrganisationLevel;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;
import org.trescal.cwms.spring.model.KeyValue;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Entity representing a division of a {@link Company}.
 */
@Entity
@Table(name = "subdiv")
@AllArgsConstructor
@Builder
@Setter
public class Subdiv extends Versioned implements OrganisationLevel {

    private int subdivid;
    private Integer accContact;
    private Set<Address> addresses;
    private List<BPO> BPOs;
    private Company comp;
    private Set<Contact> contacts;
    private LocalDate createDate;
    private Address defaultAddress;
    private Contact defaultContact;
    private Set<Department> departments;
    private Set<SubdivInstructionLink> instructions;
    private String analyticalCenter;
    private String subname;
    private String subdivCode;
    private Set<SystemDefaultApplication> systemDefaults;
    private BusinessSubdivisionSettings businessSettings;
    private List<SubdivSettingsForAllocatedSubdiv> subdivSettings;
    private boolean active;
    private String deactReason;
    private Date deactTime;
    private String siretNumber;
    private List<AdvesoOverriddenActivitySetting> advesoOverriddenActivitiesSetting;
    private List<Contract> contracts;
    private String formerId;
    private String fiscalIdentifier;

    @OneToMany(mappedBy = "subdiv", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<AdvesoOverriddenActivitySetting> getAdvesoOverriddenActivitiesSetting() {
        return this.advesoOverriddenActivitiesSetting;
    }

    public void setAdvesoOverriddenActivitiesSetting(List<AdvesoOverriddenActivitySetting> advesoOverriddenActivitiesSetting) {
        this.advesoOverriddenActivitiesSetting = advesoOverriddenActivitiesSetting;
    }

    public Subdiv() {
    }

    public Subdiv(String subname, LocalDate createDate, Integer accContact, Company comp) {
        this.subname = subname;
        this.createDate = createDate;
        this.accContact = accContact;
        this.comp = comp;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subdivid")
    public int getSubdivid() {
        return this.subdivid;
    }

    @Transient
    public Integer getId() {
        return subdivid;
    }

    @Column(name = "acccontact")
    public Integer getAccContact() {
        return this.accContact;
    }

    @OneToMany(mappedBy = "sub", fetch = FetchType.LAZY)
    @SortComparator(AddressComparator.class)
    public Set<Address> getAddresses() {
        return this.addresses;
    }

    /**
     * @return the bPOs
     */
    @OneToMany(mappedBy = "subdiv", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<BPO> getBPOs() {
        return this.BPOs;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coid")
    public Company getComp() {
        return this.comp;
    }

    @OneToMany(mappedBy = "sub", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @SortComparator(FirstNameContactComparator.class)
    public Set<Contact> getContacts() {
        return this.contacts;
    }

    @Column(name = "createdate", columnDefinition = "date")
    public LocalDate getCreateDate() {
        return this.createDate;
    }

    @Column(name = "deactreason")
    public String getDeactReason() {
        return this.deactReason;
    }

    @Column(name = "deacttime")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getDeactTime() {
        return this.deactTime;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "defaddrid")
    public Address getDefaultAddress() {
        return this.defaultAddress;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "defpersonid")
    public Contact getDefaultContact() {
        return this.defaultContact;
    }

    @OneToMany(mappedBy = "subdiv")
    @SortComparator(DepartmentComparator.class)
    public Set<Department> getDepartments() {
        return this.departments;
    }

    /*
     * Analytical center; used only for business company subdivisions
     */
    @Column(name = "analyticalCenter")
    public String getAnalyticalCenter() {
        return analyticalCenter;
    }

    @OneToMany(mappedBy = "subdiv", cascade = CascadeType.ALL)
    public Set<SubdivInstructionLink> getInstructions() {
        return this.instructions;
    }

    @NotNull
    @Size(min = 1, max = 75)
    @Column(name = "subname", length = 75, nullable = false)
    public String getSubname() {
        return this.subname;
    }

    @Size(max = 3)
    @Column(name = "subdivcode", length = 3)
    public String getSubdivCode() {
        return subdivCode;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "subdiv")
    public Set<SystemDefaultApplication> getSystemDefaults() {
        return this.systemDefaults;
    }

    @Column(name = "active", columnDefinition = "tinyint", nullable = false)
    public boolean isActive() {
        return this.active;
    }

    // Stores subdivision specific settings for CompanyRole.BUSINESS companies (therefore nullable)
    // TODO : Refactor and remove; use service layer to obtain settings to avoid Hibernate auto-fetch of entity
    @OneToOne(mappedBy = "subdiv", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public BusinessSubdivisionSettings getBusinessSettings() {
        return businessSettings;
    }

    @OneToMany(mappedBy = "subdiv", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<SubdivSettingsForAllocatedSubdiv> getSubdivSettings() {
        return subdivSettings;
    }

    @Size(max = 14)
    @Column(name = "siretNumber", length = 14)
    public String getSiretNumber() {
        return siretNumber;
    }

    @ManyToMany(mappedBy = "subdivs", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    public List<Contract> getContracts() {
        return contracts;
    }

    @Override
    public String toString() {
        return Integer.toString(this.subdivid);
    }

    public KeyValue<Integer, String> dto() {
        return new KeyValue<>(subdivid, subname);
    }

    @Length(max = 50)
    @Column(name = "formerid", length = 50)
    public String getFormerId() {
        return formerId;
    }

    @Column(name = "fiscalidentifier")
    public String getFiscalIdentifier() {
        return fiscalIdentifier;
    }

}