package org.trescal.cwms.core.company.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.company.dto.SubdivSearchResultWrapper;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("subdivlookup")
public class SubdivLookupController {

    SubdivService subdivService;

    public SubdivLookupController(SubdivService subdivService) {
        this.subdivService = subdivService;
    }

    @GetMapping("/listforcompany/{id}")
    public List<KeyValue<Integer, String>> getSubdiv(@PathVariable("id") Integer companyID) {
        List<SubdivSearchResultWrapper> subdiv = subdivService.getAllActiveCompanySubdivsHQL(companyID);
        return subdiv.stream()
                .map(c -> new KeyValue<>(c.getSubdivid(), c.getSubname()))
                .collect(Collectors.toList());
    }
}	