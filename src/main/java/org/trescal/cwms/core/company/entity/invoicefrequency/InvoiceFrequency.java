package org.trescal.cwms.core.company.entity.invoicefrequency;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name="invoicefrequency", uniqueConstraints={@UniqueConstraint(columnNames={"code"})})
public class InvoiceFrequency {
	private int id;
	private String code;
	private String description;	
	private Set<Translation> translations;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@Type(type = "int")
	public int getId() {
		return id;
	}
		
	@Column(name = "code")
	public String getCode() {
		return code;
	}
	
	@Column(name = "description")
	public String getDescription() {
		return description;
	}	
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="invoicefrequencytranslation", joinColumns=@JoinColumn(name="invoicefrequencyid"))
	public Set<Translation> getTranslations() {
		return translations;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setDescription(String description) {
		this.description = description;
	}		
	public void setTranslations(Set<Translation> translations) {
		this.translations = translations;
	}
}
