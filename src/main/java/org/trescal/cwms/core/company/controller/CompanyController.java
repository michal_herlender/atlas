package org.trescal.cwms.core.company.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.account.entity.bankaccount.db.BankAccountService;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditation;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditationComparator;
import org.trescal.cwms.core.company.entity.companyaccreditation.db.CompanyAccreditationService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.companystatus.CompanyStatus;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.entity.vatrate.VatRateFormatter;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentfield.InstrumentFieldDefinition;
import org.trescal.cwms.core.instrument.entity.instrumentfield.db.InstrumentFieldDefinitionService;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.login.entity.portal.usergroup.db.UserGroupService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.db.RecallCompanyConfigurationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultTypeDTO;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.service.SystemDefaultTypeService;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.*;

@Slf4j
@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV,
	Constants.SESSION_ATTRIBUTE_COMPANY, Constants.HTTPSESS_NEW_FILE_LIST})
public class CompanyController {
	@Autowired
	private AddressService addressService;
	@Value("${cwms.config.thirdpartyaccounts.supported}")
	private Boolean accountsSoftwareSupported;
	@Autowired
	private BankAccountService bankAccountService;
	@Autowired
	private BPOService bpoService;
	@Autowired
	private CompanyAccreditationService companyAccreditationService;
	@Autowired
	private CompanyService compService;
	@Autowired
	private CompanySettingsForAllocatedCompanyService settingsService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private FileBrowserService fileBrowserService;
	@Autowired
	private InstructionViewComponent instructionViewComponent;
	@Autowired
	private InstrumService instService;
	@Autowired
	private RecallCompanyConfigurationService rccService;
	@Autowired
	private SystemDefaultTypeService systemDefaultTypeService;
	@Autowired
	private SystemComponentService scService;
	@Autowired
	private SupportedCurrencyService currencyService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserGroupService userGroupService;
	@Autowired
	private JobService jobService;
	@Autowired
	private QuotationService quoteService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private InstrumentFieldDefinitionService instrumentFieldDefinitionService;
	@Autowired
	private ExchangeFormatService exchangeFormatService;
	@Autowired
	private VatRateFormatter vatRateFormatter;
	@Autowired
	private AuthenticationService authServ;


	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
		return new ArrayList<>();
	}

	@ModelAttribute("flexiblefields")
	public Set<InstrumentFieldDefinition> initializeFlexibleFields(
		@RequestParam(value = "coid") int coid) {
		Company company = compService.get(coid);
		return instrumentFieldDefinitionService.getInstrumentFieldDefinitionsForCompany(company);
	}

	@ModelAttribute("systemdefaulttypes")
	public List<SystemDefaultTypeDTO> initializeSystemDefaults(Locale locale,
															   @RequestParam(value = "coid") int coid,
															   @ModelAttribute(value = Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto) {
		Company company = compService.get(coid);
		if (company == null)
			throw new RuntimeException("Company not found for id " + coid);
		List<SystemDefaultTypeDTO> result = this.systemDefaultTypeService.getSystemDefaultTypeDTOs(company,
			allocatedCompanyDto.getKey(), locale);
		log.info("systemdefaulttypes.size(): " + result.size());
		return result;
	}


	@RequestMapping(value = "/viewcomp.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@RequestParam(value = "coid") int coid, Locale locale,
										 @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
										 @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
										 @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		Contact currentContact = this.userService.get(username).getCon();
		Company company = compService.get(coid);
		if (company == null)
			throw new RuntimeException("Company not found for id " + coid);
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Company allocatedCompany = allocatedSubdiv.getComp();
		// get files in the root directory
		FileBrowserWrapper fileBrowserWrapper = this.fileBrowserService.getFilesForComponentRoot(Component.COMPANY, company.getCoid(), newFiles);
		Map<String, Object> model = new HashMap<>();
		model.put("company", company);
		model.put("defaultrole", Scope.COMPANY);
		// load system component for file browsing/uploading
		model.put(Constants.REFDATA_SYSTEM_COMPONENT, this.scService.findComponent(Component.COMPANY));
		model.put(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
		model.put(Constants.REFDATA_FILES_NAME, this.fileBrowserService.getFilesName(fileBrowserWrapper));
		model.put("groups", this.userGroupService.getSortedUserGroups(company));
		model.put("defaultcurrency", this.currencyService.getDefaultCurrency());
		model.put("bPOsList", this.bpoService.getAllByCompany(company, null, allocatedCompany));
		// get counts for the company's stuff
		model.put("activejobcount", jobService.countActiveByCompanyAndAllocatedSubdiv(company, allocatedSubdiv));
		model.put("bankaccounts", bankAccountService.searchByCompany(company));
		model.put("totaljobcountbysubdiv", jobService.countByCompanyAndAllocatedSubdiv(company, allocatedSubdiv));
		model.put("totaljobcount", jobService.countByCompany(company));
		model.put("quotecount", this.quoteService.countByCompanyAndAllocatedCompany(company, allocatedCompany));
		model.put("recallConfig", this.rccService.get(company.getId()));
		model.put("recentquotecount",
			this.quoteService.countActualByCompanyAndAllocatedCompany(2, company, allocatedCompany));
		model.put("instcount", this.instService.getCompanyInstrumentCount(company.getCoid()));
		model.put("instructions", instructionViewComponent.viewCompany(company, allocatedSubdiv));
		model.put("legaladdresses",
			this.addressService.getAllCompanyAddresses(company.getCoid(), AddressType.LEGAL_REGISTRATION, false));
		CompanySettingsForAllocatedCompany settings = settingsService.getByCompany(company, allocatedSubdiv.getComp());
		if (settings == null) {
			settings = settingsService.initializeForCompany(company, allocatedSubdiv.getComp(), false);
		}
		model.put("settings", settings);
		if (settings.getVatrate() != null) {
			model.put("vatRate", this.vatRateFormatter.formatVatRate(settings.getVatrate(), locale));
		}

		// To update on-stop or active, user must have permission and company
		// must be verified
		boolean updateOnstopActive = false;
		// To update finance settings, user must have permission
		boolean updateFinance = false;
		// either director, admin, member of accounts departmemt
		if (this.contactService.contactBelongsToDepartmentOrIsAdmin(currentContact, DepartmentType.ACCOUNTS,
			allocatedCompany)) {
			updateFinance = true;
			if (settings.getStatus().equals(CompanyStatus.VERIFIED)) {
				updateOnstopActive = true;
			}
		}
		model.put("updateOnstopActive", updateOnstopActive);
		model.put("updateFinance", updateFinance);
		model.put("usesPlantillas", allocatedCompany.getBusinessSettings().getUsesPlantillas());
		model.put("accountsSoftwareSupported", accountsSoftwareSupported);

		TreeSet<CompanyAccreditation> accreditations = new TreeSet<>(new CompanyAccreditationComparator());
		accreditations.addAll(companyAccreditationService.getForBusinessCompany(company, allocatedCompany));
		model.put("accreditations", accreditations);

		Integer countEfs;

		if (company.getCompanyRole().equals(CompanyRole.CLIENT)) {
			countEfs = this.exchangeFormatService.countExchangeFormats(null, null, null, company);
		} else {
			countEfs = this.exchangeFormatService.countExchangeFormats(null, company, null, null);
		}

		model.put("countExchangeFormats", countEfs);
		model.put("hasPermisionOnEF", this.authServ.hasRight(currentContact, Permission.EXCHANGE_FORMAT));

		return new ModelAndView("trescal/core/company/company", model);
	}
}