package org.trescal.cwms.core.company.entity.departmentmember;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.departmentrole.DepartmentRole;

/**
 * Entity representing the membership of one {@link Contact} to a particular
 * {@link Department} and their role within the {@link Department}.
 * 
 * @author Richard
 */
@Entity
@Table(name = "departmentmember", uniqueConstraints = { @UniqueConstraint(columnNames = { "personid", "deptid", "role" }) })
public class DepartmentMember extends Auditable
{
	private Contact contact;
	private Department department;
	private DepartmentRole departmentRole;
	private int id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", unique = false, nullable = false)
	public Contact getContact()
	{
		return this.contact;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deptid", unique = false, nullable = false)
	public Department getDepartment()
	{
		return this.department;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "role")
	public DepartmentRole getDepartmentRole()
	{
		return this.departmentRole;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "int")
	@Column(name = "id", nullable = false)
	public int getId()
	{
		return this.id;
	}

	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	public void setDepartment(Department department)
	{
		this.department = department;
	}

	public void setDepartmentRole(DepartmentRole departmentRole)
	{
		this.departmentRole = departmentRole;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}
}
