package org.trescal.cwms.core.company.entity.paymentmode.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;

public interface PaymentModeDao extends BaseDao<PaymentMode, Integer> {
	PaymentMode get(String code);
}