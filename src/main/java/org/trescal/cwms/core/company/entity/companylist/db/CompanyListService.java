package org.trescal.cwms.core.company.entity.companylist.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companylist.CompanyList;
import org.trescal.cwms.core.company.form.AddCompanyListForm;
import org.trescal.cwms.core.company.form.CompanyListSearchForm;
import org.trescal.cwms.core.company.form.EditCompanyListForm;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface CompanyListService extends BaseService<CompanyList, Integer> {

	CompanyList findCompanyList(int id);

	CompanyList getByName(String nameList, Company allocatedCompany);

	void updateCompanyList(CompanyList companyList);

	void editCompanyList(CompanyList companyList, EditCompanyListForm form);

	PagedResultSet<CompanyList> searchCompanyList(CompanyListSearchForm form, Company allocatedCompany,
			PagedResultSet<CompanyList> rs);

	CompanyList createNewCompanyList(AddCompanyListForm form, String username);

	Integer getCompanyListByCompanyMember(Integer coid, Company allocatedCompany);
	
	List<Integer> getCompanyListsByUser(Integer coid, Company allocatedCompany);
	
	Boolean supplierInCompanyListUsedClient(Integer clientCompanyId, Integer supplierCompanyId, Company allocatedCompany);
}
