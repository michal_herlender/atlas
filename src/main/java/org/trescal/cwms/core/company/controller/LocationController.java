package org.trescal.cwms.core.company.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.company.form.LocationValidator;
import org.trescal.cwms.core.exception.controller.IntranetController;

/**
 * Displays a form to add a new {@link Location}.
 */
@Controller
@IntranetController
public class LocationController {

	@Autowired
	private AddressService addressServ;
	@Autowired
	private LocationService locationServ;
	@Autowired
	private LocationValidator validator;

	public static final String URL_REDIRECT = "viewaddress.htm?loadtab=locations-tab&addrid=";
	public static final String FORM_NAME = "command";

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute(FORM_NAME)
	protected Object formBackingObject(
			@RequestParam(value = "locid", required = false, defaultValue = "0") Integer locid,
			@RequestParam(value = "addrid", required = false, defaultValue = "0") Integer addrid) throws Exception {
		Location location = null;
		if (locid != 0) {
			location = this.locationServ.get(locid);
		} else {
			location = new Location();
			location.setActive(true);
			location.setAdd(this.addressServ.get(addrid));
		}
		return location;
	}

	@RequestMapping(value = "/location.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(@Valid @ModelAttribute(FORM_NAME) Location loc, BindingResult bindingResult)
			throws Exception {
		if (bindingResult.hasErrors())
			return new ModelAndView(formView());
		else {
			loc = this.locationServ.merge(loc);
			return new ModelAndView(new RedirectView(URL_REDIRECT + loc.getAdd().getAddrid()));

		}
	}

	@RequestMapping(value = "/location.htm", method = RequestMethod.GET)
	protected String formView() throws Exception {
		return "trescal/core/company/locationadd";
	}
}