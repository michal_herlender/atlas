package org.trescal.cwms.core.company.entity.invoicefrequency.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.invoicefrequency.InvoiceFrequency;

public interface InvoiceFrequencyDao extends BaseDao<InvoiceFrequency, Integer> {
	InvoiceFrequency get(String code);
}
