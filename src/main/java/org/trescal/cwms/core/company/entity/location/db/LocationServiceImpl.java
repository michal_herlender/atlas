package org.trescal.cwms.core.company.entity.location.db;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.dto.LocationSearchResultWrapper;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.LocationDTO;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.spring.model.KeyValueIntegerString;
import org.trescal.cwms.spring.model.LabelIdDTO;

@Service("locationService")
public class LocationServiceImpl extends BaseServiceImpl<Location, Integer> implements LocationService {

	@Autowired
	private LocationDao locationDao;

	@Override
	public Location findLocation(int addrid, String location) {
		return this.locationDao.findLocation(addrid, location);
	}

	@Override
	public List<LocationSearchResultWrapper> getAllActiveAddressLocationsHQL(int addrid, boolean activeOnly) {
		return this.locationDao.getAllActiveAddressLocationsHQL(addrid, activeOnly);
	}

	@Override
	public List<Location> getAllAddressLocations(int addrid, boolean activeOnly) {
		return this.locationDao.getAllAddressLocations(addrid, activeOnly);
	}
	
	@Override
	public List<LocationDTO> getAddressLocations(int addrid, boolean activeOnly) {
		return this.locationDao.getAddressLocations(addrid, activeOnly);
	}

	@Override
	public List<Location> getAllBusinessLocations(boolean activeOnly) {
		return this.locationDao.getAllBusinessLocations(activeOnly);
	}

	@Override
	public List<Location> getAllActiveSubdivLocations(Subdiv subdiv) {
		return this.locationDao.getAllActiveSubdivLocations(subdiv);
	}

	@Override
	public void insertLocation(Location location) {
		this.locationDao.persist(location);
	}

	@Override
	public boolean locationAlreadyExistsAtAddress(String location, int addrId, int excludeLocId) {
		return this.locationDao.locationAlreadyExistsAtAddress(location, addrId, excludeLocId);
	}

	@Override
	public void saveOrUpdate(Location location) {
		this.locationDao.saveOrUpdate(location);
	}

	@Override
	public List<LabelIdDTO> getLocationsForAutocomplete(Company company, Subdiv subdivision, Address address,
			String searchTerm) {
		List<Location> locations = this.locationDao.getLocationsForAutocomplete(company, subdivision, address,
				searchTerm);
		return locations.stream().map(l -> new LabelIdDTO(l.getLocation(), l.getLocationid()))
				.collect(Collectors.toList());
	}
	
	@Override
	public List<KeyValueIntegerString> getLocationDtos(Collection<Integer> locationIds) {
		List<KeyValueIntegerString> result = Collections.emptyList();
		if (locationIds != null && !locationIds.isEmpty()) {
			result = this.locationDao.getLocationDtos(locationIds);
		}
		return result;
	}

	@Override
	public List<Location> getAllCompanyLocationsActive(Integer coid){
		return locationDao.getAllCompanyLocationsActive(coid);
	}
	
	@Override
	protected BaseDao<Location, Integer> getBaseDao() {
		return locationDao;
	}
}