package org.trescal.cwms.core.company.controller;

import java.util.EnumSet;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.avalara.AvalaraConnector;
import org.trescal.cwms.core.avalara.avalaraconfig.AvalaraConfiguration;
import org.trescal.cwms.core.avalara.avalaraconfig.db.AvalaraConfigurationService;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.addresssettings.db.AddressSettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.addresssettings.db.AddressSettingsForAllocatedSubdivService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.country.db.CountryService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.VatRateFormatter;
import org.trescal.cwms.core.company.entity.vatrate.db.VatRateService;
import org.trescal.cwms.core.company.form.AddressAddForm;
import org.trescal.cwms.core.company.form.AddressAddFormValidator;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.labelprinter.db.LabelPrinterService;
import org.trescal.cwms.core.system.entity.printer.db.PrinterService;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Controller used to update or insert an {@link Address}.
 */
@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME })
public class AddressAddController {

	@Value("${cwms.config.avalara.aware}")
	private Boolean avalaraAware;
	@Autowired
	private AddressService addServ;
	@Autowired
	private AddressSettingsForAllocatedCompanyService addressSettingsForCompanyService;
	@Autowired
	private AddressSettingsForAllocatedSubdivService settingsForAllocatedSubdivService;
	@Autowired
	private AvalaraConfigurationService avalaraConfigurationService;
	@Autowired
	private AvalaraConnector avalaraConnector;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private ContactService contactServ;
	@Autowired
	private CountryService countryService;
	@Autowired
	private LabelPrinterService labelPrinterServ;
	@Autowired
	private PrinterService printerServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private TransportOptionService transOptServ;
	@Autowired
	private VatRateFormatter vatRateFormatter;
	@Autowired
	private VatRateService vatRateService;
	@Autowired
	private AddressAddFormValidator validator;

	public final static String VIEW_NAME_ADDRESS_EDIT = "trescal/core/company/addressedit";

	@InitBinder("command")
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("command")
	protected AddressAddForm formBackingObject(
			@RequestParam(value = "subdivid", required = false, defaultValue = "0") Integer subdivid,
			@RequestParam(value = "addrid", required = false, defaultValue = "0") Integer addressid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		AddressAddForm aaf = new AddressAddForm();
		aaf.setAddressId(addressid);
		Address address;
		AddressSettingsForAllocatedCompany addressSettingsCompany = null;
		AddressSettingsForAllocatedSubdiv addressSettingsSubdiv = null;
		CompanySettingsForAllocatedCompany companySettings = null;
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Company allocatedCompany = allocatedSubdiv.getComp();
		aaf.setBusinessCompanyId(allocatedCompany.getId());
		if (addressid == 0) {
			Subdiv subdiv = subdivService.get(subdivid);
			address = new Address();
			address.setActive(true);
			address.setSub(subdiv);
			companySettings = this.companySettingsService.getByCompany(subdiv.getComp(), allocatedCompany);
			aaf.setCountryId(address.getSub().getComp().getCountry().getCountryid());
			if (companySettings != null && companySettings.getVatrate() != null)
				aaf.setOverrideVatCode(companySettings.getVatrate().getVatCode());
		} else {
			address = addServ.get(addressid);
			aaf.setAddressTypes(address.getAddressType());
			aaf.setCountryId(address.getCountry().getCountryid());
			addressSettingsCompany = addressSettingsForCompanyService.getByCompany(address, allocatedCompany);
			addressSettingsSubdiv = settingsForAllocatedSubdivService.getBySubdiv(address, allocatedSubdiv);
			companySettings = this.companySettingsService.getByCompany(address.getSub().getComp(), allocatedCompany);
			if ((addressSettingsCompany != null) && (addressSettingsCompany.getVatRate() != null)) {
				aaf.setOverrideCompanyVatRate(true);
				aaf.setOverrideVatCode(addressSettingsCompany.getVatRate().getVatCode());
			} else if (companySettings != null && companySettings.getVatrate() != null) {
				aaf.setOverrideVatCode(companySettings.getVatrate().getVatCode());
			}
			if ((addressSettingsSubdiv != null) && (addressSettingsSubdiv.getTransportIn() != null)) {
				aaf.setTransportInId(addressSettingsSubdiv.getTransportIn().getId());
			}
			if ((addressSettingsSubdiv != null) && (addressSettingsSubdiv.getTransportOut() != null)) {
				aaf.setTransportOutId(addressSettingsSubdiv.getTransportOut().getId());
			}
		}
		if (addressSettingsCompany == null) {
			addressSettingsCompany = this.addressSettingsForCompanyService.initializeForAddress(address,
					allocatedCompany);
		}
		aaf.setAddress(address);
		aaf.setAddressSettingsCompany(addressSettingsCompany);
		return aaf;
	}

	@RequestMapping(value = { "/editaddress.htm", "/addaddress.htm" }, method = RequestMethod.POST)
	protected String onSubmit(Model model, Locale locale, @ModelAttribute("command") AddressAddForm aaf,
			BindingResult result,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName) throws Exception {
		Address address = aaf.getAddress();
		address.setAddressType(aaf.getAddressTypes());
		address.setCountry(countryService.get(aaf.getCountryId()));
		// Manually validated due to need to set country on Address
		this.validator.validate(aaf, result);
		if (result.hasErrors()) {
			result.getAllErrors().stream().forEach(System.out::println);
			return referenceData(model, locale, aaf, subdivDto);
		} else {
			Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
			Company allocatedCompany = allocatedSubdiv.getComp();
			updateSettingsCompany(aaf, allocatedCompany);
			this.settingsForAllocatedSubdivService.setTransportOptions(address, allocatedSubdiv, aaf.getTransportInId(),
					aaf.getTransportOutId());
			if (aaf.getPrinterId() != null) {
				address.setDefaultPrinter(this.printerServ.get(aaf.getPrinterId()));
			}
			if (aaf.getLabelPrinterId() != null) {
				address.setDefaultLabelPrinter(this.labelPrinterServ.findLabelPrinter(aaf.getLabelPrinterId()));
			}
			// after merging+persisting the detached address (+settings) the new reference
			// should be used
			if (address.getAddrid() == null || address.getAddrid() == 0)
				this.addServ.save(address);
			else
				this.addServ.merge(address);
			// If the address is a legal registration address, update the company's legal
			// registration address
			if (address.getAddressType().contains(AddressType.LEGAL_REGISTRATION)) {
				Company company = this.companyService.get(address.getSub().getComp().getId());
				company.setLegalAddress(address);
				this.companyService.save(company);
				if (avalaraAware)
					avalaraConnector.commitCustomer(company, allocatedCompany.getCoid());
			}
			// If the subdiv does not have a default address set yet, use the new address as
			// the default address
			Subdiv subdiv = this.subdivService.get(address.getSub().getSubdivid());
			if (subdiv.getDefaultAddress() == null) {
				subdiv.setDefaultAddress(address);
				this.subdivService.save(subdiv);
			}
			model.asMap().clear();
			return "redirect:viewaddress.htm?addrid=" + address.getAddrid();
		}
	}

	private void updateSettingsCompany(AddressAddForm aaf, Company allocatedCompany) {
		AddressSettingsForAllocatedCompany settingsCompany = aaf.getAddressSettingsCompany();
		if (aaf.isOverrideCompanyVatRate()) {
			settingsCompany.setVatRate(this.vatRateService.get(aaf.getOverrideVatCode()));
		} else {
			// VAT rate at company level will take precedence
			settingsCompany.setVatRate(null);
		}
	}

	@RequestMapping(value = { "/editaddress.htm", "/addaddress.htm" }, method = RequestMethod.GET)
	protected String referenceData(Model model, Locale locale, @ModelAttribute("command") AddressAddForm aaf,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Integer addressId = aaf.getAddress().getAddrid();
		int addrId = addressId == null ? 0 : addressId;
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		AvalaraConfiguration avalaraConfig = avalaraConfigurationService.get(allocatedSubdiv.getComp().getCoid());
		List<VatRate> vatRates = this.vatRateService.getAllEagerWithCountry();
		model.addAttribute("avalaraAware", avalaraAware && avalaraConfig != null);
		model.addAttribute("addresstypes", EnumSet.complementOf(EnumSet.of(AddressType.WITHOUT)));
		model.addAttribute("transportoptions",
				this.transOptServ.getTransportOptionsFromSubdiv(allocatedSubdiv.getId()));
		model.addAttribute("addressprinters", this.printerServ.getPrintersAtAddress(addrId));
		model.addAttribute("labelprinters", this.labelPrinterServ.getLabelPrintersAtAddress(addrId));
		model.addAttribute("subdivcontacts",
				this.contactServ.getAllActiveSubdivContacts(aaf.getAddress().getSub().getSubdivid()));
		model.addAttribute("countrylist", countryService.getCountries());
		model.addAttribute("vatRates", this.vatRateFormatter.getOptionGroupMap(vatRates, locale));
		return VIEW_NAME_ADDRESS_EDIT;
	}
}