package org.trescal.cwms.core.company.entity.companyinstructionlink.db;

import java.util.Arrays;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instruction.Instruction_;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

@Repository
public class CompanyInstructionLinkDaoImpl extends BaseDaoImpl<CompanyInstructionLink, Integer>
		implements CompanyInstructionLinkDao {

	@Override
	protected Class<CompanyInstructionLink> getEntity() {
		return CompanyInstructionLink.class;
	}

	@Override
	public List<CompanyInstructionLink> getForCompanyAndTypes(Company company, Subdiv allocatedSubdiv,
			InstructionType... types) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<CompanyInstructionLink> cq = cb.createQuery(CompanyInstructionLink.class);
		Root<CompanyInstructionLink> root = cq.from(CompanyInstructionLink.class);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(root.get(CompanyInstructionLink_.company), company));
		clauses.getExpressions()
				.add(cb.equal(root.get(CompanyInstructionLink_.organisation), allocatedSubdiv.getComp()));
		Predicate disjunction = cb.disjunction();
		disjunction.getExpressions().add(cb.equal(root.get(CompanyInstructionLink_.businessSubdiv), allocatedSubdiv));
		disjunction.getExpressions().add(cb.isFalse(root.get(CompanyInstructionLink_.subdivInstruction)));
		clauses.getExpressions().add(disjunction);
		if (types.length > 0) {
			Join<CompanyInstructionLink, Instruction> instruction = root.join(CompanyInstructionLink_.instruction);
			clauses.getExpressions().add(instruction.get(Instruction_.instructiontype).in(Arrays.asList(types)));
		}
		cq.where(clauses);
		cq.select(root);
		return getEntityManager().createQuery(cq).getResultList();
	}
}