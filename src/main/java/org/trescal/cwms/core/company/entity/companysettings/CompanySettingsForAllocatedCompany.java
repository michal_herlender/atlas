package org.trescal.cwms.core.company.entity.companysettings;

import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.account.entity.bankaccount.BankAccount;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companystatus.CompanyStatus;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.invoicefrequency.InvoiceFrequency;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

@Setter
@Entity
@Table(name="companysettingsforallocatedcompany", uniqueConstraints=@UniqueConstraint(columnNames= {"orgid","companyid"}))
public class CompanySettingsForAllocatedCompany extends Allocated<Company> {

	private int id;
	private Company company;
	private boolean active;
	private String deactReason;
	private Date deactDate;
	private boolean onStop;
	private Contact onStopBy;
	private String onStopReason;
	private LocalDate onStopSince;
	// Remainder of fields sorted in alphabetical order
	@Deprecated
	private BankAccount bankAccount;
	private String companyReference;
	private Boolean contract;
	private BigDecimal creditLimit;
	private InvoiceFrequency invoiceFrequency;
	private PaymentMode paymentMode;
	private String softwareAccountNumber;
	private CompanyStatus status;
	private Boolean syncToPlantillas;
	private boolean taxable;
	private VatRate vatrate;
	private boolean poRequired;
	private PaymentTerm paymentterm;
	private Boolean requireSupplierCompanyList;
	private Boolean invoiceFactoring;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="companyid", nullable=false)
	public Company getCompany() {
		return company;
	}
	
	@Column(name = "active", nullable=false)
	public boolean isActive() {
		return active;
	}
	
	@Column(name = "onstop", nullable=false)
	public boolean isOnStop() {
		return this.onStop;
	}

	/*
	 * Permanent authorization for the allocated company to process work without having a PO from the company 
	 */
	@Column(name = "porequired")
	public boolean isPoRequired() {
		return poRequired;
	}
	
	/*
	 * If false, then no application of VAT is performed in our relationship with this company
	 */
	@Column(name = "taxable")
	public boolean isTaxable() {
		return taxable;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "onstopby")
    public Contact getOnStopBy() {
        return this.onStopBy;
    }

    @Length(max = 255)
    @Column(name = "onstopreason")
    public String getOnStopReason() {
        return this.onStopReason;
    }

    @Column(name = "onstopsince", columnDefinition = "date")
    public LocalDate getOnStopSince() {
        return this.onStopSince;
    }

    /**
     * EF 2022-02-07
     * Marked deprecated - there is no primary bank account anymore. Use bankAccounts from class {@link Company}
     * GB 2018-12-03
     * Purpose elaboration, depending on the role of the company that this record is for
     *  ("company" field, not the "organisation" allocated company):
     * - Business, then the bank account owned by this company that the the allocated company should send payments to
     * - Supplier, then the bank account owned by this company that the the allocated company should send payments to
	 * - Client, then the bank account owned by this client that the the allocated company should send payments to, if needed
	 * 
	 * - And, if company and allocated company are the same (e.g. editing ones own company) it's the default used for invoicing
	 * 
	 * - it seems the Supplier and Client information has been loaded by data migration
	 * - only the Business to Business information is editable in the user interface currently
	 * 
	 * The "Business" relationship is used for invoicing, all relationships are used for accounting interface currently
	 * - Possibly, we should move this information to two different fields:
	 *   (a) outgoing payment from allocated company to client / supplier / business company (e.g. supplier invoice processing)
	 *   (b) incoming payment from client / supplier / business company to allocated company (e.g. Trescal invoice generation) 
	 * - and, have the default bank account configured separately
	 * - we may also need to better handle multi-currency cases
	 * 
	 * TLDR : the bank account set here is always for payments from the "allocated company" (organisation) to the "company" (company) 
	 */
	@Deprecated
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="bankaccountid")
	public BankAccount getBankAccount() {
		return bankAccount;
	}

	/*
	 * The company's reference number to us, i.e. to this allocated business company
	 */
	@Column(name = "companyreference")
	public String getCompanyReference() {
		return companyReference;
	}

	@NotNull
	@Column(name = "contract", nullable = false, columnDefinition = "bit default 0")
	public Boolean getContract() {
		return contract;
	}

	@Column(name="softwareaccountnumber", length=50)
	public String getSoftwareAccountNumber() {
		return softwareAccountNumber;
	}

	/*
	 * The maximum amount of credit extended to the company by this allocated business company
	 */
	@Column(name = "creditlimit", precision = 10, scale = 2)
	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	@Column(name = "deactDate")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDeactDate() {
		return deactDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoicefrequencyid")
	public InvoiceFrequency getInvoiceFrequency() {
		return invoiceFrequency;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "paymentmodeid")
	public PaymentMode getPaymentMode() {
		return paymentMode;
	}


	@NotNull
	@Column(name = "syncToPlantillas", nullable = false, columnDefinition = "bit default 0")
	public Boolean getSyncToPlantillas() {
		return syncToPlantillas;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vatrate")
	public VatRate getVatrate() {
		return this.vatrate;
	}

	@NotNull
	@Column(name = "status", nullable=false)
	@Enumerated(EnumType.STRING)
	public CompanyStatus getStatus() {
		return status;
	}
	
	@Column(name = "paymentterms")
	@Enumerated(EnumType.STRING)
	public PaymentTerm getPaymentterm() {
		return paymentterm;
	}
	
	@NotNull
	@Column(name = "requiresuppliercompanylist", nullable = false, columnDefinition = "bit default 0")
	public Boolean getRequireSupplierCompanyList() {
		return requireSupplierCompanyList;
	}
	
	public String getDeactReason() {
		return deactReason;
	}

	@NotNull
	@Column(name = "invoiceFactoring", nullable = false)
	public Boolean getInvoiceFactoring() {
		return invoiceFactoring;
	}
}