package org.trescal.cwms.core.company.entity.country.db;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Service("countryService")
public class CountryServiceImpl extends BaseServiceImpl<Country, Integer> implements CountryService {
	@Autowired
	private CountryDao countryDao;

	@Override
	protected BaseDao<Country, Integer> getBaseDao() {
		return countryDao;
	}

	@Override
	public Country get(String countryName) {
		return countryDao.get(countryName);
	}

	@Override
	public List<Country> getAll(Country defaultCountry) {
		List<Country> result = countryDao.findAll();
		if (defaultCountry != null) {
			result.remove(defaultCountry);
			result.add(0, defaultCountry);
		}
		return result;
	}

	@Override
	public Set<KeyValueIntegerString> getCountries() {
		Set<KeyValueIntegerString> result = new TreeSet<>();
		this.getAll().stream().map(c -> new KeyValueIntegerString(c.getCountryid(), c.getLocalizedName()))
				.forEach(kv -> result.add(kv));
		return result;
	}
}