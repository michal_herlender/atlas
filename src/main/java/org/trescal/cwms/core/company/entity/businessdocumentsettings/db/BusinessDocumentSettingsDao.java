package org.trescal.cwms.core.company.entity.businessdocumentsettings.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.businessdocumentsettings.BusinessDocumentSettings;

public interface BusinessDocumentSettingsDao extends BaseDao<BusinessDocumentSettings, Integer> {
	
	/**
	 * Returns both the default and the company-specific document settings in one query
	 * @param businessCompanyId
	 * @return
	 */
	List<BusinessDocumentSettings> getForCompanyAndDefault(Integer businessCompanyId);
}
