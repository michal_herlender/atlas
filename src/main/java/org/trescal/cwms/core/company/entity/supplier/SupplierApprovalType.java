package org.trescal.cwms.core.company.entity.supplier;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public enum SupplierApprovalType {
	/*
	 * Original IDs in CWMS commented below for reference
	 */
	UNDEFINED("supplierapprovaltype.undefined", "Undefined"), // Placeholder, not used in CWMS
	ISO_17025("supplierapprovaltype.iso17025", "ISO 17025"),  // 1
	OEM("supplierapprovaltype.oem", "Original Equipment Manufacturer"),                // 2
	ISO_9001("supplierapprovaltype.iso9001", "ISO 9001"),     // 3
	APPROVAL_NOT_REQUIRED("supplierapprovaltype.approvalnotrequired", "Approval Not Required"), // 4
	APPROVED("supplierapprovaltype.approved", "Approved"),    // 5
	AWAITING_APPROVAL("supplierapprovaltype.awaitingapproval", "Awaiting Approval"),   // 6
	NOT_APPROVED("supplierapprovaltype.notapproved", "Not Approved");                  // 7
	
	private String messageCode;
	private String defaultName;
	private MessageSource messages;
	
	@Component
	public static class SupplierApprovalTypeMessageSourceInjector {
		@Autowired
		private MessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (SupplierApprovalType type : EnumSet.allOf(SupplierApprovalType.class))
               type.setMessageSource(messages);
        }
	}
	
	public void setMessageSource (MessageSource messages) {
		this.messages = messages;
	}	
	private SupplierApprovalType(String messageCode, String defaultName) {
		this.messageCode = messageCode;
		this.defaultName = defaultName;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public String getName()
	{
		Locale loc = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.defaultName, loc);
		}
		return this.defaultName;
	}}
