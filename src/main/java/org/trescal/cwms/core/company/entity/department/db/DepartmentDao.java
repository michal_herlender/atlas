package org.trescal.cwms.core.company.entity.department.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

public interface DepartmentDao extends BaseDao<Department, Integer>
{
	List<Department> findBySubdivAndTypes(Subdiv subdiv, Collection<DepartmentType> types);
	
	List<Department> getAllSubdivDepartments(Integer subdivid);

	List<Department> getBusinessDepartments();
	
	List<Department> getByTypeAndSubdiv(DepartmentType type, Integer subdivId);
	
	List<Contact> getAllActiveAvailableDepartmentContact(List<Department> dept);
}