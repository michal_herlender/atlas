package org.trescal.cwms.core.company.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.company.entity.supplier.Supplier;
import org.trescal.cwms.core.company.entity.supplier.db.SupplierService;

@Component
public class CompanyAccreditationFormValidator implements Validator {

	@Autowired
	private SupplierService supplierService; 
	
	@Override
	public boolean supports(Class<?> arg0) {
		return (CompanyAccreditationForm.class.equals(arg0));
	}	
	
	@Override
	public void validate(Object target, Errors errors)
	{
		CompanyAccreditationForm form = (CompanyAccreditationForm) target;
		if (form.getSupplierIds().isEmpty()) {
			// At least one supplier level should be set for the supplier if updating
			errors.rejectValue("supplierIds", "error.company.supplier.accreditation");
			return;
		}
		// Only verify for checked suppliers
		for (Integer supplierId : form.getSupplierIds()) {
			// Value in set is ID for checked suppliers
			Supplier supplier = this.supplierService.get(supplierId);
			// If supplier can expire, then a date must be set if the supplier ID is checked (otherwise not required)
			if (!supplier.isDoesNotExpire()) {
				if (form.getCompanyAccreditationDate().get(supplierId) == null) {
					errors.rejectValue("companyAccreditationDate[" + supplierId + "]", "error.company.supplier.accreditation.date");
				}
			}
		}
	}
}
