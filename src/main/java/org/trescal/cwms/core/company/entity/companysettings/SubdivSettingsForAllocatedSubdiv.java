package org.trescal.cwms.core.company.entity.companysettings;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

import lombok.Setter;

@Setter
@Entity
@Table(name="subdivsettingsforallocatedsubdiv",
	uniqueConstraints=@UniqueConstraint(columnNames= {"orgid","subdivid"}, name="UK_subdivsettingsforallocatedsubdiv_orgid_subdivid"),
	indexes=@Index(columnList="subdivid", name="IDX_subdivsettingsforallocatedsubdiv_subdivid"))
public class SubdivSettingsForAllocatedSubdiv extends Allocated<Subdiv> {

	private Integer id;
	private Subdiv subdiv;
	private Contact defaultBusinessContact;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="subdivid", nullable=false, foreignKey=@ForeignKey(name="FK_subdivsettingsforallocatedsubdiv_subdivid"))
	public Subdiv getSubdiv() {
		return subdiv;
	}
	
	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="businesscontactid", nullable=false, foreignKey=@ForeignKey(name="FK_subdivsettingsforallocatedsubdiv_businesscontactid"))
	public Contact getDefaultBusinessContact() {
		return defaultBusinessContact;
	}
	
}