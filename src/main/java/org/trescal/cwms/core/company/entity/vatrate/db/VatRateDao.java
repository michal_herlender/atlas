package org.trescal.cwms.core.company.entity.vatrate.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.VatRateType;

public interface VatRateDao extends BaseDao<VatRate, String>
{	
	List<VatRate> getAllEagerWithCountry();
	VatRate getDefaultForType(VatRateType type);
	VatRate getForCountryAndType(Integer countryId, VatRateType type);
}