package org.trescal.cwms.core.company.entity.businessarea.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.businessarea.BusinessArea;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface BusinessAreaDao extends BaseDao<BusinessArea, Integer> {

	List<KeyValueIntegerString> getAllTranslated(Locale locale);
}