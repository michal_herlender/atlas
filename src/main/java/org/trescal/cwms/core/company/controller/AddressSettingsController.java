package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.dto.AddressTransportOptionsComponentDTO;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.addresssettings.db.AddressSettingsForAllocatedSubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.rest.common.dto.output.RestFieldErrorDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@RestController
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class AddressSettingsController {

	@Autowired
	private AddressService addressService;
	@Autowired
	private AddressSettingsForAllocatedSubdivService settingsService;

	@GetMapping(value = "/changeaddressactive.json")
	public void changeActive(@RequestParam(value = "addressid") Integer addressId,
							 @RequestParam(value = "activate") Boolean activate,
							 @RequestParam(value = "reason") String reason) {
		Address address = addressService.get(addressId);
		address.setActive(activate);
		address.setDeactReason(reason);
		Date date = new Date();
		address.setDeactTime(date);
		addressService.merge(address);
	}

	@GetMapping(value = "addresstransportoptions.json")
	public List<AddressTransportOptionsComponentDTO> getAddressTransportOptions(
		@RequestParam(name = "addressid") Integer addressId,
		@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue allocatedSubdiv) {
		List<AddressTransportOptionsComponentDTO> transportOptions = settingsService.getTransportOptions(addressId);
		Collection<? extends GrantedAuthority> auth = SecurityContextHolder.getContext().getAuthentication()
			.getAuthorities();
		boolean hasAuthToDeleteSettings = auth.contains(Permission.ADDRESS_SETTINGS_DELETE);
		if (hasAuthToDeleteSettings)
			transportOptions.forEach(to -> {
				if (to.getBusinessSubdivId().equals(allocatedSubdiv.getKey()))
					to.setHasRightToDelete(true);
			});
		return transportOptions;
	}

	@PostMapping(value = "deleteaddresssettingsforsubdiv.json")
	@PreAuthorize("hasAuthority('ADDRESS_SETTINGS_DELETE')")
	public RestResultDTO<String> deleteAddressSettings(@RequestParam(name = "id") Integer id,
													   @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue allocatedSubdiv) {
		try {
			AddressSettingsForAllocatedSubdiv settings = settingsService.get(id);
			if (settings == null)
				throw new Exception("No settings found for id " + id + "!");
			else if (allocatedSubdiv.getKey().equals(settings.getOrganisation().getSubdivid())) {
				settingsService.delete(settings);
				return new RestResultDTO<>(true, "");
			} else
				throw new Exception("You're not logged-in to the right subdivision!");
		} catch (Exception e) {
			RestResultDTO<String> result = new RestResultDTO<>(false, "");
			result.addError(new RestFieldErrorDTO("", e.toString()));
			return result;
		}
	}
}