package org.trescal.cwms.core.company.entity.companyaccreditation.db;

import java.util.HashMap;
import java.util.List;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditation;
import org.trescal.cwms.core.company.entity.supplier.Supplier;

public interface CompanyAccreditationService
{
	void deleteCompanyAccreditation(CompanyAccreditation companyAccreditation);

	CompanyAccreditation findCompanyAccreditation(Integer supplierid, Integer coid);

	List<CompanyAccreditation> findExpiredCompanyAccreditations();

	List<CompanyAccreditation> getAllCompanyAccreditations();

	void insertCompanyAccreditation(CompanyAccreditation companyAccreditation);

	void saveOrUpdateCompanyAccreditation(CompanyAccreditation companyAccreditation);

	List<CompanyAccreditation> searchCompanyAccreditation(Integer coid);
	
	List<CompanyAccreditation> getForBusinessCompany(Company company, Company businessCompany);
	
	CompanyAccreditation getForBusinessCompany(Company company, Company businessCompany, Supplier supplier);

	/**
	 * Returns a {@link HashMap} of {@link CompanyAccreditation} belonging to
	 * the {@link Company} identified by the given coid where the key of the
	 * {@link HashMap} is the id of the {@link Supplier}.
	 * 
	 * @param coid the company id, not null.
	 * @return {@link HashMap} of {@link CompanyAccreditation}.
	 */
	HashMap<Integer, CompanyAccreditation> searchCompanyAccreditations(Integer coid);

	void updateCompanyAccreditation(CompanyAccreditation companyAccreditation);
}
