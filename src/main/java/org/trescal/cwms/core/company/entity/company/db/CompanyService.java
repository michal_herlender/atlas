package org.trescal.cwms.core.company.entity.company.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.dto.CompanyDTO;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.dto.CompanySearchResultWrapper;
import org.trescal.cwms.core.company.dto.CompanyToContactDetailsDTO;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.form.CompanyAddForm;
import org.trescal.cwms.core.company.form.CompanyEditForm;
import org.trescal.cwms.core.company.projection.CompanyProjectionDTO;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.spring.model.KeyValueIntegerString;
import org.trescal.cwms.spring.model.LabelIdDTO;

public interface CompanyService extends BaseService<Company, Integer> {
	
	Company createCompany(CompanyAddForm form, Company allocatedCompany, Contact defaultBusinessContact);
	
	void editCompany(CompanyEditForm form, String username);

	/**
	 * Checks if a {@link Company} with the given name for the specified
	 * {@link CompanyRole} (or any editableTo) already exists in the database.
	 * 
	 * @param name           the name of the {@link Company}.
	 * @param companyRoles   the {@link CompanyRole} to be checked.
	 * @param excludeCompany optionally a (@link Company} to exclude from the search
	 *                       (if editing existing company) (can be null)
	 * @return true if the {@link Company} already exists.
	 */
	boolean existsByName(String name, CompanyRole companyRole, Company excludeCompany);

	/**
	 * Checks if a Company exists with the given legal identifier for the role or
	 * any editable to.
	 * 
	 * @param legalIdentifier the legal identifier of the {@link Company}.
	 * @param companyRoles    the {@link CompanyRole} to be checked.
	 * @param excludeCompany  optionally a (@link Company} to exclude from the
	 *                        search (if editing existing company) (can be null)
	 * @return true if the {@link Company} already exists.
	 */
	boolean existsByLegalIdentifier(String legalIdentifier, CompanyRole companyRoles, Company excludeCompany);

	List<CompanyKeyValue> findCustomerCompanies(Contact contact, Scope scope, Boolean active);

	List<CompanyKeyValue> findCustomerCompaniesBySubdiv(Subdiv subdiv, Boolean active);

	/*
	 * searchName - if true, search the name field, if false, search the legal
	 * identifier
	 */
	List<CompanySearchResultWrapper> searchByCompanyRoles(String searchString, List<CompanyRole> companyRoles,
														  Company allocatedCompany, Boolean active, Boolean anywhereActive, Boolean searchName, Boolean searchEverywhere);

	List<Company> getCompaniesByRole(CompanyRole companyRole);

	// Called by AOP
	void overrideItemsToOnStop(List<JobItem> items);

	// Called by AOP
	public void overrideItemsToTrusted(List<JobItem> items);

	void update(Company c);

	// Returns the currency set against the company or the default currency if
	// one has not been explicitly set
	SupportedCurrency getCurrency(Company company);

    SupportedCurrency getCurrencyById(Integer companyId);

    /**
	 * this method creates a new company, subdivision, address and contact all in
	 * one action using the bare essential fields.
	 *
	 * @param ctcdto {@link CompanyToContactDetailsDTO} dto with all fields
	 *               necessary for method
	 * @return {@link ResultWrapper}
	 */
	CompanyToContactDetailsDTO insertFreehandContact(CompanyToContactDetailsDTO ctcdto, Integer subdivId,
			String username);

	/**
	 * This method returns a list of companies in the right format for an input box
	 * with jQuery autocomplete
	 * 
	 * @param nameFragment     The fragment of the compay name that the user has
	 *                         typed in
	 * @param allocatesCompany The user's allocated company
	 * @param maxResults       The maximum number of results to show in the
	 *                         autocomplete list
	 *
	 */
	List<LabelIdDTO> getClientLabelIdList(String nameFragment, Company allocatesCompany, Integer maxResults);

	List<CompanyKeyValue> getCompaniesFromSubdivs(List<Integer> subdivs);
	
	List<CompanyProjectionDTO> getDTOList(Collection<Integer> companyIds, Integer allocatedCompanyId);

	CompanyDTO getForLinkInfo(Integer companyId, Integer allocatedCompanyId);

	Integer getCompanyId(Integer clientSubdivId);
	
	List<KeyValueIntegerString> getCompaniesByRoleForAllocatedCompany(List<CompanyRole> roles, Company allocatedCompany
			, List<Integer> companiesIdsAlreadyExist);

	List<CompanyProjectionDTO> getClientCompaniesDTosFromSubdiv(Subdiv subdiv);

}