package org.trescal.cwms.core.company.entity.departmentrole.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.departmentrole.DepartmentRole;

public class DepartmentRoleServiceImpl implements DepartmentRoleService
{
	DepartmentRoleDao departmentroleDao;

	public DepartmentRole findDepartmentRole(int id)
	{
		return this.departmentroleDao.find(id);
	}

	public List<DepartmentRole> getAllDepartmentRoles()
	{
		return this.departmentroleDao.findAll();
	}

	public DepartmentRoleDao getDepartmentroleDao()
	{
		return this.departmentroleDao;
	}

	public DepartmentRoleDao getDepartmentRoleDao()
	{
		return this.departmentroleDao;
	}

	public void insertDepartmentRole(DepartmentRole departmentrole)
	{
		this.departmentroleDao.persist(departmentrole);
	}

	public void setDepartmentroleDao(DepartmentRoleDao departmentroleDao)
	{
		this.departmentroleDao = departmentroleDao;
	}

	public void updateDepartmentRole(DepartmentRole departmentrole)
	{
		this.departmentroleDao.update(departmentrole);
	}

}