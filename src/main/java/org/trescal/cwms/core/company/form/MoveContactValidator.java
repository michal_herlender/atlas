package org.trescal.cwms.core.company.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class MoveContactValidator extends AbstractBeanValidator {

	@Autowired
	private AddressService addressService;
	@Autowired
	private ContactService contactService;

	@Override
	public boolean supports(Class<?> clazz) {
		return MoveContactForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		MoveContactForm form = (MoveContactForm) target;
		if (!errors.hasErrors()) {
			Address newAddress = this.addressService.get(form.getNewAddressId());
			if (newAddress.getSub().getSubdivid() != form.getNewSubdivId()) {
				errors.rejectValue("newAddressId", null,
						"You must select a new default address from the new subdivision");
			}
			Contact contact = contactService.get(form.getContactId());
			if (contact.equals(contact.getSub().getDefaultContact()) && !contact.getSub().equals(newAddress.getSub()))
				errors.reject("error.contact.movedefaultofsubdiv",
						"It's not possible to move the default contact of a subdivision to another subdivision.");
		}
		if (form.isChangeInstrumOwner()) {
			// must select a new owner for the contact's equipment
			super.validate(target, errors, MoveContactForm.ChangeInstrumentOwner.class);
		}
	}
}
