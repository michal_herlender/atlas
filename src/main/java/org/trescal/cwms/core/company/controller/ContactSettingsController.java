package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.IntranetController;

import java.util.Date;

@RestController
@IntranetController
public class ContactSettingsController {

    @Autowired
    private ContactService contactService;

    @GetMapping(value = "/changecontactactive.json")
    public void changeActive(@RequestParam(value = "personid") Integer personId,
                             @RequestParam(value = "activate") Boolean activate,
                             @RequestParam(value = "reason") String reason) {
        Contact contact = contactService.get(personId);
        contact.setActive(activate);
        contact.setDeactReason(reason);
        Date date = new Date();
        contact.setDeactTime(date);
        contactService.merge(contact);
    }
}