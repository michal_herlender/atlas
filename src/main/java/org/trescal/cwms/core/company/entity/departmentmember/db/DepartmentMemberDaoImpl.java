package org.trescal.cwms.core.company.entity.departmentmember.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.Department_;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.User_;
import org.trescal.cwms.core.userright.entity.userrole.UserRole;

@Repository("DepartmentMemberDao")
public class DepartmentMemberDaoImpl extends BaseDaoImpl<DepartmentMember, Integer> implements DepartmentMemberDao {
	
	@Override
	protected Class<DepartmentMember> getEntity() {
		return DepartmentMember.class;
	}
	
	public DepartmentMember findDepartmentMember(Integer personid, Integer deptid) {
		Criteria criteria = getSession().createCriteria(DepartmentMember.class);
		criteria.add(Restrictions.eq("contact.personid", personid));
		criteria.add(Restrictions.eq("department.deptid", deptid));
		return (DepartmentMember) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<DepartmentMember> getAllContactDepartmentMembers(Integer personid) {
		Criteria criteria = getSession().createCriteria(DepartmentMember.class);
		criteria.add(Restrictions.eq("contact.personid", personid));
		return (List<DepartmentMember>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DepartmentMember> getDepartmentMembers(int deptid) {
		Criteria criteria = getSession().createCriteria(DepartmentMember.class);
		criteria.add(Restrictions.eq("department.deptid", deptid));
		return (List<DepartmentMember>) criteria.list();
	}
	
	@Override
	public List<User> getDepartmentUsers(List<Department> depts) {
		return getResultList(cb -> {
			CriteriaQuery<User> cq = cb.createQuery(User.class);
			Root<Department> deptRoot = cq.from(Department.class);
			Join<Department,DepartmentMember> members = deptRoot.join(Department_.members);
			Join<DepartmentMember,Contact> contacts = members.join(DepartmentMember_.contact);
			Join<Contact,User> users = contacts.join(Contact_.user);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(deptRoot.in(depts));
			clauses.getExpressions().add(cb.equal(contacts.get(Contact_.active), true));
			cq.where(clauses);
			return cq.select(users).distinct(true);
		});
	}
}