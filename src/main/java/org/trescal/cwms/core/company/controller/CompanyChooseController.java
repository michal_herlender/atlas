package org.trescal.cwms.core.company.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller @IntranetController
public class CompanyChooseController
{
	@RequestMapping(value="/choosecompany.htm", method=RequestMethod.GET) 
	public ModelAndView handleRequest() throws Exception {
		return new ModelAndView("trescal/core/company/choosecompany");
	}
}
