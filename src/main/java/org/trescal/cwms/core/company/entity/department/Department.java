package org.trescal.cwms.core.company.entity.department;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.organisation.OrganisationLevel;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.ProcedureCategoryDepartmentComparator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Entity represeting a department / laboratory within the business company(ies)
 * on the system.
 *
 * @author Richard
 */
@Entity
@Table(name = "department")
public class Department implements OrganisationLevel {
    private Address address;
    private Set<CapabilityCategory> categories;
    private Integer deptid;
    // private Location location;
    private Set<DepartmentMember> members;
    private String name;
    private String shortName;
    private Subdiv subdiv;
    private DepartmentType type;
    private Set<WorkRequirement> workRequirements;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "addrid", nullable = false)
	public Address getAddress()
	{
		return this.address;
	}

    @OneToMany(mappedBy = "department", cascade = CascadeType.ALL)
    @SortComparator(ProcedureCategoryDepartmentComparator.class)
    public Set<CapabilityCategory> getCategories() {
        return this.categories;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "deptid", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getDeptid()
	{
		return this.deptid;
	}
	
	@Transient
	public Integer getId() {
		return deptid;
	}
	
	@OneToMany(mappedBy = "department", cascade = { CascadeType.ALL })
	public Set<DepartmentMember> getMembers()
	{
		return this.members;
	}

	@NotNull
	@Length(min = 1, max = 40)
	@Column(name = "name", length = 40)
	public String getName()
	{
		return this.name;
	}

	@NotNull
	@Length(max = 4)
	@Column(name = "shortname", length = 4)
	public String getShortName()
	{
		return this.shortName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subdivid")
	public Subdiv getSubdiv()
	{
		return this.subdiv;
	}

	@Enumerated(EnumType.ORDINAL)
	@NotNull
	@Column(name = "typeid", nullable = false)
	public DepartmentType getType()
	{
		return this.type;
	}

	@OneToMany(mappedBy = "department", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Set<WorkRequirement> getWorkRequirements() {
        return this.workRequirements;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setCategories(Set<CapabilityCategory> categories) {
        this.categories = categories;
    }

    public void setDeptid(Integer deptid) {
        this.deptid = deptid;
    }

    public void setMembers(Set<DepartmentMember> members) {
        this.members = members;
    }

	public void setName(String name)
	{
		this.name = name;
	}

	public void setShortName(String shortName)
	{
		this.shortName = shortName;
	}

	public void setSubdiv(Subdiv subdiv)
	{
		this.subdiv = subdiv;
	}

	public void setType(DepartmentType type)
	{
		this.type = type;
	}

	public void setWorkRequirements(Set<WorkRequirement> workRequirements)
	{
		this.workRequirements = workRequirements;
	}

}
