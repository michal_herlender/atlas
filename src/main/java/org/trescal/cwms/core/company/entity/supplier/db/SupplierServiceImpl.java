package org.trescal.cwms.core.company.entity.supplier.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.supplier.Supplier;
import org.trescal.cwms.core.company.entity.supplier.SupplierApprovalType;

@Service("supplierService")
public class SupplierServiceImpl extends BaseServiceImpl<Supplier, Integer> implements SupplierService
{
	@Autowired
	private SupplierDao suppDao;

	@Override
	public List<Supplier> getActiveSuppliers()
	{
		return suppDao.getActiveSuppliers();
	}
	
	@Override
	public Supplier getForApprovalType(SupplierApprovalType approvalType) {
		return suppDao.getForApprovalType(approvalType);
	}

	@Override
	protected BaseDao<Supplier, Integer> getBaseDao() {
		return suppDao;
	}
}
