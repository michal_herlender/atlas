package org.trescal.cwms.core.company.entity.editablecorole.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.corole.Corole;
import org.trescal.cwms.core.company.entity.editablecorole.EditableCorole;

public interface EditableCoroleDao extends BaseDao<EditableCorole, Integer> {
	
	List<EditableCorole> findAllEditableCorolesForRole(Corole role);
}