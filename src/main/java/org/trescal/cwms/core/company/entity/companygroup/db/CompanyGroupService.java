package org.trescal.cwms.core.company.entity.companygroup.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.admin.form.CompanyGroupForm;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.companygroup.CompanyGroup;
import org.trescal.cwms.core.company.entity.companygroup.dto.CompanyGroupUsageDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface CompanyGroupService extends BaseService<CompanyGroup, Integer> {
	public Long countByName(String groupName, Integer idToExclude);
	public Integer createCompanyGroup(CompanyGroupForm form);
	public void editCompanyGroup(CompanyGroupForm form);
	
	/**
	 * Returns a sorted list of active groups for selection together with an N/A option
	 * @param currentCompanyGroup (which may not be active; it will still always be in the list so that an invisible change is not forced) 
	 * @param locale (locale used for adding N/A option)
	 * @return
	 */
	public List<KeyValueIntegerString> getActiveDTOList(CompanyGroup currentCompanyGroup, Locale locale);
	
	/**
	 * Returns a list of groups, normally external functions will want to use getActiveDTOList(...) instead
	 * This is internally used by getActiveDTOList(...)
	 * @param activeGroups (optional to restrict to just active / inactive groups)
	 * @return
	 */
	public List<KeyValueIntegerString> getDTOList(Boolean activeGroups);
	
	/**
	 * Returns a sorted list of all company groups, together with usage information
	 * (i.e. the number of companies globally using the group)
	 */
	public List<CompanyGroupUsageDTO> getUsageDTOList();
}
