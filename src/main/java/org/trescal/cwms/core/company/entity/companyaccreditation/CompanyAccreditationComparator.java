package org.trescal.cwms.core.company.entity.companyaccreditation;

import java.util.Comparator;

public class CompanyAccreditationComparator implements Comparator<CompanyAccreditation> {
	@Override
	public int compare(CompanyAccreditation o1, CompanyAccreditation o2)
	{
		int result = o1.getCompany().getConame().compareTo(o2.getCompany().getConame());
		if (result == 0) result = o1.getCompany().getCoid() - o2.getCompany().getCoid();
		if (result == 0) result = o1.getSupplier().getName().compareTo(o2.getSupplier().getName());
		if (result == 0) result = o1.getSupplier().getSupplierid() - o2.getSupplier().getSupplierid();
		if (result == 0) result = o1.getId() - o2.getId();
		return result;
	}
}
