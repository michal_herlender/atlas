package org.trescal.cwms.core.company.entity.contact.db;

import io.vavr.control.Either;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.dto.ContactSearchResultWrapper;
import org.trescal.cwms.core.company.dto.SearchEmailContactDto;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.form.ContactAddForm;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.List;

public interface ContactService extends BaseService<Contact, Integer> {

    /**
     * Tests if the specified Contact has ROLE_ADMIN/ROLE_DIRECTOR privileges
     * for any subdivision of the specified business company or belongs to the
     * specified department type for any subdivisions within the business
     * company.
     */
    boolean contactBelongsToDepartmentOrIsAdmin(Contact contact, DepartmentType departmentType,
                                                Company businessCompany);

    /**
     * Tests if the specified Contact has ROLE_ADMIN/ROLE_DIRECTOR privileges
     * for the specified subdivision, or is a manager of at least one department
     * of the specified type within the subdivision.
     */
    boolean contactManagesDepartmentOrIsAdmin(Contact contact, DepartmentType departmentType, Integer subdivId);

    /**
     * Test's if the currently logged in {@link Contact} is able to create a new
     * {@link Contact} for the subdivd held in the {@link HttpServletRequest}.
     * The basic rule here is that only ROLE_DIRECTOR and ROLE_ADMIN can add new
     * business {@link Contact}s, whilst all other roles can create contacts for
     * all other companies.
     *
     * @return {@link ResultWrapper} indicating if user can create and messages
     * as to why not.
     */
    ResultWrapper contactCanCreateContact(HttpServletRequest req);

    /**
     * Tests if a {@link Contact} with the same (case insensitive) first and
     * last names already exists for the given subdivision.
     *
     * @param subdivid the id of the {@link Subdiv}.
     * @param fname    firstname of the {@link Contact}.
     * @param lname    firstname of the {@link Contact}.
     * @return true if a contact already exists.
     */
    boolean contactExists(int subdivid, String fname, String lname);

    /**
     * Creates contact and returns ID of newly created contact
     */
    Integer createContact(ContactAddForm form);

    /**
     * Loads the {@link Contact} with the given id and eagerly load's the
     * contacts {@link Company} and {@link Subdiv} information.
     *
     * @param id the id of the {@link Contact}.
     * @return the {@link Contact}.
     */
    Contact findEagerContact(int id);

    List<Contact> getActiveContactsWithRoleForSubdiv(int businessSubdivId);

    /**
     * Returns a list of all active {@link Contact}s belonging to the
     * {@link Subdiv} identified by the given subdivid.
     *
     * @param subdivid the id of the {@link Subdiv}, not null.
     * @return list of active {@link Contact}s.
     */
    List<Contact> getAllActiveSubdivContacts(int subdivid);

    List<Contact> getAllCompanyContacts(int coid);

    /**
     * Gets all {@link Contact}s belonging to the {@link Company} with the given
     * id, order by {@link Subdiv}.subname then first name.
     *
     * @param coid   the {@link Company} coid.
     * @param active true active only, false inactive, null returns both.
     * @return {@link List} matching {@link Contact}.
     */
    List<Contact> getAllCompanyContactsBySubCon(int coid, Boolean active);

    List<Contact> getAllOrderedContacts();

    List<Contact> getAllSubdivContacts(int subdivid);

    /**
     * Return a list ContactKeyValue extends KeyValue<Integer,String> DTOs.
     *
     * @param activeOnly if true then only active contacts are returned.
     * @return list of matching {@link Contact}.
     */
    List<ContactKeyValue> getBusinessContacts(boolean activeOnly);

    /**
     * Returns a list of ContactKeyValue extends KeyValue<Integer,String> DTOs where
     *
     * @param coid        - Integer optional coid of company
     * @param subdivid    - Integer optional subdivid of company
     * @param active      - Boolean optional active / inactive property of contact
     * @param prependDtos - CompanyKeyValue dtos to prepend, only if IDs not in results
     * @return list of matching contacts sorted by first name, last name, and then ID
     */
    List<ContactKeyValue> getContactDtoList(Integer coid, Integer subdivid, Boolean active,
                                            ContactKeyValue... prependDtos);

    List<ContactProjectionDTO> getContactProjectionDTOs(Collection<Integer> personIds, Integer allocatedCompanyId);

    /**
     * Returns a {@link List} of {@link Contact}s that all belong to the
     * {@link UserGroup} identified by the given id.
     *
     * @param groupid the id of the {@link UserGroup}.
     * @return {@link List} of {@link Contact}s.
     */
    List<Contact> getContactsInUserGroup(int groupid);

    List<ContactSearchResultWrapper> searchByCompanyRoles(String firstName, String lastName, boolean active,
                                                          List<CompanyRole> companyRoles);

    List<ContactSearchResultWrapper> searchContactsByRolesAndFullName(String fullName, Boolean active, List<CompanyRole> roles);

    List<Contact> searchContact(String firstName, String lastName, boolean active);

    Either<String, Contact> searchContactByFullName(String fullName, boolean active);

    List<Contact> searchContactByRoles(String firstName, String lastName, boolean active, String[] role);

    /**
     * Sends an e-mail to the {@link Contact} identified by the given id to
     * remind them of their username and give them their password. Does not
     * perform a password reset.
     *
     * @param personid the id of the {@link Contact}.
     * @return message indicating success / fail of send.
     */
    String sendPasswordReminder(int personid, String newPassword);

    Contact setEncryptedPassword(int personid, String encPassword);

    void updateAll(Collection<Contact> contacts);

    List<Contact> getAllCompanyContacts(int coid, boolean active);

    Contact findCompanyContactByFullName(int coid, String fullname, boolean active);

    boolean getHrIdExists(String hrid);

    boolean getHrIdExistsAndIgnoreContact(String hrid, Contact contact);

    Contact getByHrId(String hrid);

    List<Contact> getByHrIds(List<String> hrids);

    Contact getContactByFirstnameAndLastname(Integer subdivid, String firstName, String lastName);

    Either<String, List<SearchEmailContactDto>> searchEmailContact(String name, List<Integer> coIds);
    
    Integer checkContactDeactivationStatus(int personid);
    
    ContactProjectionDTO getContactProjectionDTO(Integer contactId);
    /**
     * Lookups specified contact by ID or defers to logged-in contact if no ID specified 
     * @param contactId optional contact ID to look up; if null, 
     * then returns the contact associated with the current security context.
     * Useful for service account operations which can optionally specify a contact ID
     */
	Contact getContactOrLoggedInContact(Integer contactId);
  
}