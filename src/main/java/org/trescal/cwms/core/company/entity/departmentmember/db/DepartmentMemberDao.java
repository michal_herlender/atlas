package org.trescal.cwms.core.company.entity.departmentmember.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.login.entity.user.User;

public interface DepartmentMemberDao extends BaseDao<DepartmentMember, Integer> {
	
	DepartmentMember findDepartmentMember(Integer personid, Integer deptid);
	
	List<DepartmentMember> getAllContactDepartmentMembers(Integer personid);
	
	List<DepartmentMember> getDepartmentMembers(int deptid);

	List<User> getDepartmentUsers(List<Department> depts);
}