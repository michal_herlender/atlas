package org.trescal.cwms.core.company.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.businessarea.BusinessArea;
import org.trescal.cwms.core.company.entity.businessarea.db.BusinessAreaService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companyaccreditation.CompanyAccreditation;
import org.trescal.cwms.core.company.entity.companygroup.db.CompanyGroupService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.country.db.CountryService;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.VatRateFormatter;
import org.trescal.cwms.core.company.entity.vatrate.db.VatRateService;
import org.trescal.cwms.core.company.form.CompanyEditForm;
import org.trescal.cwms.core.company.form.CompanyEditValidator;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.TimeStampEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Displays an editable form for updating a {@link Company}'s profile and it's
 * {@link CompanyAccreditation}'s.
 */
@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY })
public class CompanyEditController {
	private static final Logger logger = LoggerFactory.getLogger(CompanyEditController.class);
	private final String VIEW_NAME = "trescal/core/company/companyedit";
	private final String FORM_NAME = "companyform";

	@Autowired
	private BusinessAreaService businessAreaService;
	@Autowired
	private CompanyEditValidator validator;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private CompanyGroupService companyGroupService;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private CountryService countryService;
	@Autowired
	private MessageSource messages;
	@Autowired
	private SupportedCurrencyService currencyService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private VatRateFormatter vatRateFormatter;
	@Autowired
	private VatRateService vatRateService;

	@InitBinder(FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		TimeStampEditor editor2 = new TimeStampEditor();
		binder.registerCustomEditor(Timestamp.class, editor2);
	}

	@ModelAttribute(FORM_NAME)
	public CompanyEditForm formBackingObject(@RequestParam(value = "coid", required = true) Integer coid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		Company company = this.companyService.get(coid);
		if (company == null)
			throw new RuntimeException("Company for id " + coid + " could not be found");
		
		CompanyEditForm cf = new CompanyEditForm();
		updateCompanyForm(cf, company, companyDto.getKey());
		return cf;
	}

	/*
	 * This is separated from the formBackingObject call so that the form can be
	 * refreshed with updated data if an optimistic locking failure is detected.
	 */
	private void updateCompanyForm(CompanyEditForm cf, Company company, int allocatedCompanyId) {
		Company allocatedCompany = this.companyService.get(allocatedCompanyId);
		BusinessArea businessArea = company.getBusinessarea();
		if (businessArea != null)
			cf.setBusinessareaid(businessArea.getBusinessareaid());
		cf.setBusinessCompanyId(allocatedCompany.getId());
		cf.setCompanyCode(company.getCompanyCode());
		cf.setCompanyRole(company.getCompanyRole());
		cf.setCompanyGroupId(company.getCompanyGroup() != null ? company.getCompanyGroup().getId() : 0);
		cf.setCoid(company.getCoid());
		cf.setConame(company.getConame());
		cf.setFormerId(company.getFormerID());
		cf.setOldConame(company.getConame());
		cf.setDocumentLanguage(company.getDocumentLanguage().toLanguageTag());
		cf.setChangeBusinessContact(allocatedCompanyId == company.getDefaultBusinessContact().getSub().getComp().getCoid());
		cf.setPrimaryBusinessContactId(company.getDefaultBusinessContact().getId());
		cf.setGroupName(company.getGroupName());
		if (company.getCountry() != null)
			cf.setCountryid(company.getCountry().getCountryid());
		cf.setLastModified(company.getLastModified());
		// set up any currency specific info on the fbo
		if (company.getCurrency() != null) {
			cf.setSpecificCurrency(true);
			cf.setSpecificCurrencyId(company.getCurrency().getCurrencyId());
			if (company.getRate() != null) {
				cf.setSpecificRate(true);
				cf.setSpecificRateValue(company.getRate());
			}
		} else {
			cf.setSpecificCurrency(false);
			cf.setSpecificCurrencyId(0);
			cf.setSpecificRate(false);
			cf.setSpecificRateValue(null);
		}
		CompanySettingsForAllocatedCompany settings = this.companySettingsService.getByCompany(company,
				allocatedCompany);
		if (settings != null) {
			cf.setCompanyRef(settings.getCompanyReference());
			cf.setContract(settings.getContract());
			cf.setTaxable(settings.isTaxable());
			cf.setSyncToPlantillas(settings.getSyncToPlantillas());
			cf.setRequireSupplierCompanyList(settings.getRequireSupplierCompanyList());
		} else {
			cf.setCompanyRef("");
			cf.setContract(false);
			cf.setTaxable(true);
			cf.setSyncToPlantillas(false);
			cf.setRequireSupplierCompanyList(false);
		}
		if ((settings != null) && settings.getVatrate() != null) {
			cf.setVatCode(settings.getVatrate().getVatCode());
		} else {
			VatRate defaultVatRate = this.vatRateService.getDefault(company.getCountry(), allocatedCompany.getCountry(),
					company.getCompanyRole());
			if (defaultVatRate != null)
				cf.setVatCode(defaultVatRate.getVatCode());
		}
	}

	@RequestMapping(value = "/editcompany.htm", method = RequestMethod.POST)
	protected String onSubmit(Model model, Locale locale, 
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@Valid @ModelAttribute(FORM_NAME) CompanyEditForm cf, BindingResult bindingResult) throws Exception {
		Company c = this.companyService.get(cf.getCoid());

		if (!c.getLastModified().equals(cf.getLastModified())) {
			logger.info("Last modified difference on Company " + c.getCoid());
			bindingResult.addError(new ObjectError("Company", new String[] { "error.lastModified" }, null,
					"Company Already Updated"));
			updateCompanyForm(cf, c, companyDto.getKey());
			return referenceData(model, locale, cf);
		}
		if (bindingResult.hasErrors()) {
			// return to view without reset the changes
			return referenceData(model, locale, cf);
		} else {
			this.companyService.editCompany(cf, username);
			// Remove attributes from model to prevent inclusion after redirect
			model.asMap().clear();
			return "redirect:viewcomp.htm?coid=" + cf.getCoid();
		}
	}

	@RequestMapping(value = "/editcompany.htm", method = RequestMethod.GET)
	public String referenceData(Model model, Locale locale, 
			@ModelAttribute(FORM_NAME) CompanyEditForm cf) {
		Company company = this.companyService.get(cf.getCoid());
		Company allocatedCompany = this.companyService.get(cf.getBusinessCompanyId());
		
		model.addAttribute("defaultBusinessCompanyName", company.getDefaultBusinessContact().getSub().getComp().getConame());
		model.addAttribute("defaultBusinessContactName", company.getDefaultBusinessContact().getName());
		model.addAttribute("requirePrimaryBusinessChange", company.getDefaultBusinessContact().getSub().getComp().getCoid() != allocatedCompany.getCoid());
		model.addAttribute("allocatedCompanyName", allocatedCompany.getConame());
		model.addAttribute("primaryBusinessContacts", primaryBusinessContacts(company, allocatedCompany, locale));
		model.addAttribute("companyGroups", this.companyGroupService.getActiveDTOList(company.getCompanyGroup(), locale));
		model.addAttribute("businessAreas", this.businessAreaService.getAllTranslated(locale));
		model.addAttribute("vatRates", getVatRates(locale));
		model.addAttribute("countries", this.countryService.getCountries());
		model.addAttribute("currencies", this.currencyService.getAllSupportedCurrenciesWithExRates());
		model.addAttribute("usesPlantillas", allocatedCompany.getBusinessSettings().getUsesPlantillas());
		model.addAttribute("locales", supportedLocaleService.getSupportedDTOList(locale));
		return VIEW_NAME;
	}
	
	private Map<String, Set<KeyValue<String, String>>> getVatRates(Locale locale) {
		List<VatRate> vatRates = this.vatRateService.getAllEagerWithCountry();
		return this.vatRateFormatter.getOptionGroupMap(vatRates, locale);
	}

	/**
	 * Provides a list of default business contacts from the current allocated company
	 * (note, separate logic used to indicate whether to switch to current business company)
	 * 
	 * @param company
	 * @param allocatedCompany
	 * @return
	 */
	private List<ContactKeyValue> primaryBusinessContacts(Company company, Company allocatedCompany, Locale locale) {
		Contact currentDefault = company.getDefaultBusinessContact();
		
		List<ContactKeyValue> results = null;
		ContactKeyValue noneDto = new ContactKeyValue(0, messages.getMessage("company.none", null, "None", locale));
		if (currentDefault.getSub().getComp().getCoid() == allocatedCompany.getCoid()) {
			// Note, current default contact may not be active; include on list
			ContactKeyValue prependDto = new ContactKeyValue(currentDefault.getPersonid(), currentDefault.getFirstName(), currentDefault.getLastName());
			results = this.contactService.getContactDtoList(allocatedCompany.getCoid(), null, true, noneDto, prependDto);
		}
		else {
			results = this.contactService.getContactDtoList(allocatedCompany.getCoid(), null, true, noneDto);
		}
		
		
		return results;
	}
}