package org.trescal.cwms.core.company.entity.departmentmember;

import java.util.Comparator;

public class DepartmentMemberComparator implements Comparator<DepartmentMember>
{
	@Override
	public int compare(DepartmentMember o1, DepartmentMember o2)
	{
		return ((Integer) o1.getId()).compareTo(o2.getId());
	}
}