package org.trescal.cwms.core.company.entity.companylist.db;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companylist.CompanyList;
import org.trescal.cwms.core.company.entity.companylist.CompanyList_;
import org.trescal.cwms.core.company.entity.companylistusage.CompanyListUsage;
import org.trescal.cwms.core.company.entity.companylistusage.CompanyListUsage_;
import org.trescal.cwms.core.company.form.CompanyListSearchForm;
import org.trescal.cwms.core.tools.PagedResultSet;

@Repository("CompanyListDao")
public class CompanyListDaoImpl extends BaseDaoImpl<CompanyList, Integer> implements CompanyListDao {

	@Override
	protected Class<CompanyList> getEntity() {
		return CompanyList.class;
	}

	@Override
	public CompanyList getByName(String nameList, Company allocatedCompany) {

		return getFirstResult(cb -> {
			CriteriaQuery<CompanyList> cq = cb.createQuery(CompanyList.class);
			Root<CompanyList> root = cq.from(CompanyList.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.like(root.get(CompanyList_.name), nameList));
			clauses.getExpressions().add(cb.equal(root.get(CompanyList_.organisation), allocatedCompany));
			cq.where(clauses);
			return cq;
		}).orElse(null);

	}

	@Override
	public PagedResultSet<CompanyList> searchCompanyList(CompanyListSearchForm form, Company allocatedCompany,
			PagedResultSet<CompanyList> rs) {

		completePagedResultSet(rs, CompanyList.class, cb -> cq -> {
			Root<CompanyList> root = cq.from(CompanyList.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(CompanyList_.organisation), allocatedCompany));
			if (form.getName() != null && !form.getName().trim().equals("")) {
				clauses.getExpressions().add(cb.greaterThan(cb.locate(root.get(CompanyList_.name), form.getName()), 0));
			}

			if (form.getActivestatus() != null) {
				if (form.getActivestatus()) {
					clauses.getExpressions().add(cb.isTrue(root.get(CompanyList_.active)));
				} else
					clauses.getExpressions().add(cb.isFalse(root.get(CompanyList_.active)));
			}

			cq.where(clauses);
			List<Order> order = new ArrayList<>();
			order.add(cb.desc(root.get(CompanyList_.lastModified)));
			return Triple.of(root.get(CompanyList_.id), null, order);
		});

		return rs;
	}

	@Override
	public Integer getCompanyListByCompanyMember(Integer coid, Company allocatedCompany) {
		CompanyListUsage user = getFirstResult(cb -> {
			CriteriaQuery<CompanyListUsage> cq = cb.createQuery(CompanyListUsage.class);
			Root<CompanyListUsage> root = cq.from(CompanyListUsage.class);
			Predicate clauses = cb.conjunction();
			Join<CompanyListUsage, Company> companyJoin = root.join(CompanyListUsage_.company);
			Join<CompanyListUsage, CompanyList> companyListJoin = root.join(CompanyListUsage_.companyList);
			clauses.getExpressions().add(cb.equal(companyJoin.get(Company_.coid), coid));
			clauses.getExpressions().add(cb.equal(companyListJoin.get(CompanyList_.organisation), allocatedCompany));
			cq.where(clauses);
			return cq;
		}).orElse(null);
		if (user != null) {
			return user.getCompanyList().getId();
		}
		return 0;
	}
	
	@Override
	public List<Integer> getCompanyListsByUser(Integer coid, Company allocatedCompany) {
		return getResultList(cb -> {
			CriteriaQuery<CompanyListUsage> cq = cb.createQuery(CompanyListUsage.class);
			Root<CompanyListUsage> root = cq.from(CompanyListUsage.class);
			Predicate clauses = cb.conjunction();
			Join<CompanyListUsage, Company> companyJoin = root.join(CompanyListUsage_.company);
			Join<CompanyListUsage, CompanyList> companyListJoin = root.join(CompanyListUsage_.companyList);
			clauses.getExpressions().add(cb.equal(companyJoin.get(Company_.coid), coid));
			clauses.getExpressions().add(cb.equal(companyListJoin.get(CompanyList_.organisation), allocatedCompany));
			cq.where(clauses);
			cq.distinct(true);
			return cq;
		}).stream().map(user -> user.getCompanyList().getId()).collect(Collectors.toList());
	}
}
