package org.trescal.cwms.core.company.entity.companyaccreditation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.dto.ExpiredCompanyAccreditation;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companyaccreditation.db.CompanyAccreditationService;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;

import java.time.LocalDate;
import java.util.*;

/**
 * Task that e-mails out a simple reminder when the {@link CompanyAccreditation}
 * for a company expires.
 * 
 * TODO - determine if needs to be a Service for transaction availability if resurrected
 * 
 * @author Richard
 */
@Component("AccreditationMailerTarget")
public class AccreditationMailer
{
	public static void main(String[] args) {
		ClassPathXmlApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext-jdbc.xml", "applicationContext-dao.xml", "applicationContext-service.xml", "applicationContext-acegi-security.xml", "applicationContext-schedule.xml", "applicationContext-html.xml", "applicationContext-intercept-antech.xml");

		// of course, an ApplicationContext is just a BeanFactory

		AccreditationMailer accMailer = (AccreditationMailer) ((BeanFactory) appContext).getBean("AccreditationMailerTarget");
		accMailer.setMailToExpiredCompanies(true);
		accMailer.findAndMailExpiredAccreditations();

		// call exit or Spring will just sit and BE.
		appContext.close();
		System.exit(0);
	}

	@Autowired
	private CompanyAccreditationService compAccServ;
	
	@Autowired
	private EmailService emailService;

	@Value("${cwms.admin.email}")
	private String mailFrom;

	@Value("${cwms.config.company.expiredaccreditiation.mailinternalsummary}")
	private boolean mailInternalSummary;

	@Value("${cwms.config.company.expiredaccreditiation.mailinternalsummaryaddress}")
	private String mailInternalSummaryAddress;

	@Value("${cwms.config.company.expiredaccreditiation.mailtoexpiredcompany}")
	private boolean mailToExpiredCompanies;
	
	@Autowired
	private ScheduledTaskService schTaskServ;

	@Autowired
	private MessageSource messages;

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public void findAndMailExpiredAccreditations()
	{
		if (this.schTaskServ.scheduledTaskIsActive(this.getClass().getName(), Thread.currentThread().getStackTrace()))
		{
			boolean summarySent = false;

			List<CompanyAccreditation> companyAccreditations = this.compAccServ.findExpiredCompanyAccreditations();

			if (companyAccreditations.size() > 0) {
				HashMap<Integer, ExpiredCompanyAccreditation> expired = this.prepareDistinctCompanyAccreditation(companyAccreditations);

				// send the external summary
				for (Integer key : expired.keySet()) {
					this.sendExternalMail(expired.get(key));
				}

				// send an internal summary if needed
				summarySent = this.sendInternalSummary();
			}

			// report results of task run
			String message = companyAccreditations.size()
					+ " expired accreditations found";
			message = message
					+ ((summarySent) ? ", notification sent to "
							+ this.mailInternalSummaryAddress : "");
			this.schTaskServ.reportOnScheduledTaskRun(this.getClass().getName(), Thread.currentThread().getStackTrace(), true, message);
		}
		else
		{
			this.logger.info("Supplier Accreditation Mailer not running: scheduled task cannot be found or is turned off");
		}
	}

	/**
	 * Takes the list of expired {@link CompanyAccreditation} and pushes them
	 * into a dto wrapper {@link ExpiredCompanyAccreditation}, with one entry
	 * per {@link Company}.
	 * 
	 * @param companyAccreditations list of expired {@link CompanyAccreditation}
	 *        .
	 * @return {@link HashMap} where the {@link Company} id is the key.
	 */
	protected HashMap<Integer, ExpiredCompanyAccreditation> prepareDistinctCompanyAccreditation(List<CompanyAccreditation> companyAccreditations) {
		HashMap<Integer, ExpiredCompanyAccreditation> expired = new HashMap<>();

		// prepare a distinct list of companies and their expired
		// accreditations
		for (CompanyAccreditation ca : companyAccreditations) {
			// make sure the we have waited the set period following expiration
			// before continuing
			if (ca.getSupplier().getChaseAfterDays() > 0) {
				LocalDate currentDate = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());

				if (!currentDate.isAfter(ca.getExpiryDate())
					&& currentDate != ca.getExpiryDate()) {
					break;
				}
			}

			if (!expired.containsKey(ca.getCompany().getCoid())) {
				expired.put(ca.getCompany().getCoid(), new ExpiredCompanyAccreditation(new HashSet<>(), false, ca.getCompany()));
			}
			expired.get(ca.getCompany().getCoid()).getAccreditations().add(ca);
		}
		return expired;
	}

	/**
	 * Sends an e-mail to the company who will have one or more expired
	 * {@link CompanyAccreditation}.
	 * <p>
	 * TODO we don't know who to e-mail this to just yet, so the implementation
	 * isn't working properly.
	 */
	protected void sendExternalMail(ExpiredCompanyAccreditation accred)
	{
		if (this.mailToExpiredCompanies)
		{
			accred.setEmailed(true);
		}

		// update the status of each accreditation to show that it's been
		// e-mailed
		for (CompanyAccreditation ca : accred.getAccreditations()) {
			ca.setEmailAlert(true);
			ca.setLastEmailAlert(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			this.compAccServ.updateCompanyAccreditation(ca);
		}
	}

	/**
	 * Generates an html summary of all the {@link CompanyAccreditation}s that
	 * have expired.
	 */
	protected boolean sendInternalSummary() {
		boolean sentSummary = false;

		if (this.mailInternalSummary) {
			Locale locale = Locale.getDefault();

			// TODO - migrate Velocity to Thymeleaf if reactivated - this feature currently not used
			String docString = "TODO - Migrate email/companyaccreditationsummary.vm to Thymeleaf";
			// String docString = VelocityEngineUtils.mergeTemplateIntoString(this.velocityEngine, "/velocity/email/companyaccreditationsummary.vm", templateEncoding, refData);
			String subject = this.messages.getMessage("email.companyaccreditationsummary.subject", null, "Expired Company Accreditations", locale);

			sentSummary = this.emailService.sendAdvancedEmail(subject, this.mailFrom, Collections.singletonList(this.mailInternalSummaryAddress), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), docString, true, false);
		}

		return sentSummary;
	}
	public void setMailInternalSummary(boolean mailInternalSummary)
	{
		this.mailInternalSummary = mailInternalSummary;
	}

	public void setMailToExpiredCompanies(boolean mailToExpiredCompanies)
	{
		this.mailToExpiredCompanies = mailToExpiredCompanies;
	}
}
