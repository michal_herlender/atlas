package org.trescal.cwms.core.company.entity.addresssettings;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;

@Entity
@Table(name="addresssettingsforallocatedcompany", uniqueConstraints=@UniqueConstraint(columnNames= {"orgid","addressid"}))
public class AddressSettingsForAllocatedCompany extends Allocated<Company>
{
	private int id;
	private Address address;
	private VatRate vatRate;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="addressid", nullable=false)
	public Address getAddress() {
		return address;
	}
	
	/*
	 * Optionally stores a VAT rate if used to override the company VAT rate, otherwise null
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vatrateid")
	public VatRate getVatRate() {
		return vatRate;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	public void setVatRate(VatRate vatRate) {
		this.vatRate = vatRate;
	}
}