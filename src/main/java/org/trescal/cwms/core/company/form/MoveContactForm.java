/**
 * 
 */
package org.trescal.cwms.core.company.form;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.controller.MoveContactController;

/**
 * FormBackingObject for {@link MoveContactController}.
 * 
 * @author Richard
 */
public class MoveContactForm
{
	private boolean changeInstrumOwner;
	private Integer contactId;
	private Integer newAddressId;
	private Integer newOwnerId;
	private Integer newSubdivId;

	public static interface ChangeInstrumentOwner {};
	
	@NotNull
	public Integer getContactId()
	{
		return this.contactId;
	}

	@NotNull
	@Min(value=1, message="{error.value.notselected}")
	public Integer getNewAddressId()
	{
		return this.newAddressId;
	}

	@NotNull
	@Min(value=1, message="{error.value.notselected}", groups={ChangeInstrumentOwner.class})
	public Integer getNewOwnerId()
	{
		return this.newOwnerId;
	}

	@NotNull
	@Min(value=1, message="{error.value.notselected}")
	public Integer getNewSubdivId()
	{
		return this.newSubdivId;
	}

	public boolean isChangeInstrumOwner()
	{
		return this.changeInstrumOwner;
	}

	public void setChangeInstrumOwner(boolean changeInstrumOwner)
	{
		this.changeInstrumOwner = changeInstrumOwner;
	}

	public void setContactId(Integer contactId)
	{
		this.contactId = contactId;
	}

	public void setNewAddressId(Integer newAddressId)
	{
		this.newAddressId = newAddressId;
	}

	public void setNewOwnerId(Integer newOwnerId)
	{
		this.newOwnerId = newOwnerId;
	}

	public void setNewSubdivId(Integer newSubdivId)
	{
		this.newSubdivId = newSubdivId;
	}
}
