package org.trescal.cwms.core.company.entity.addresssettings.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.dto.AddressTransportOptionsComponentDTO;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addresssettings.AddressSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

public interface AddressSettingsForAllocatedSubdivService
		extends BaseService<AddressSettingsForAllocatedSubdiv, Integer> {

	AddressSettingsForAllocatedSubdiv getBySubdiv(Address address, Subdiv allocatedSubdiv);

	AddressSettingsForAllocatedSubdiv initializeForAddress(Address address, Subdiv allocatedSubdiv);

	List<AddressTransportOptionsComponentDTO> getTransportOptions(Integer addressId);

	void setTransportOptions(Address address, Subdiv allocatedSubdiv, Integer transportInId, Integer transportOutId);
}