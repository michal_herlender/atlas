package org.trescal.cwms.core.company.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.dto.CompanySearchResultWrapper;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companyhistory.db.CompanyHistoryService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static org.trescal.cwms.core.company.controller.CompanySearchController.CompanySearchField.CURRENT_NAME;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class CompanySearchController {
	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private CompanyHistoryService companyHistoryService;
	@Autowired
	private CompanyService companyService;

	public enum CompanySearchField {
		CURRENT_NAME, HISTORIC_NAME, LEGAL_IDENTIFIER;
	}

	@RequestMapping(value = "/companysearch.htm")
	public ModelAndView handleRequest() throws Exception {
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("companyRoles", EnumSet.allOf(CompanyRole.class));
		return new ModelAndView("trescal/core/company/companysearch", model);
	}

	@RequestMapping(value = "/companysearchrequest.json", method = RequestMethod.GET)
	@ResponseBody
	public List<CompanySearchResultWrapper> getCompanies(
			SearchCompanyIn searchCompanyIn,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto) {
		Company allocatedCompany = companyService.get(allocatedCompanyDto.getKey());
		switch (searchCompanyIn.getSearchField()) {
			case CURRENT_NAME:
				return companyService.searchByCompanyRoles(searchCompanyIn.getSearchName(), searchCompanyIn.getCompanyRolesEnum(), allocatedCompany, searchCompanyIn.getActive(), searchCompanyIn.getActiveAnywhere(), true, searchCompanyIn.getSearchEverywhere());
			case HISTORIC_NAME:
				return companyHistoryService.searchByCompanyRoles(searchCompanyIn.getSearchName(), searchCompanyIn.getCompanyRolesEnum(), allocatedCompany, searchCompanyIn.getActive(), searchCompanyIn.getActiveAnywhere());
			case LEGAL_IDENTIFIER:
				return companyService.searchByCompanyRoles(searchCompanyIn.getSearchName(), searchCompanyIn.getCompanyRolesEnum(), allocatedCompany, searchCompanyIn.getActive(), searchCompanyIn.getActiveAnywhere(), false, searchCompanyIn.searchEverywhere);
			default:
				logger.error("No searchField parameter specified!");
				return null;
		}
	}


	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	static class SearchCompanyIn {
		String searchName = "";
		Boolean active = true;
		Boolean activeAnywhere = true;
		CompanySearchField searchField = CURRENT_NAME;
		Boolean searchEverywhere = true;
		List<String> companyRoles = Collections.emptyList();


		public List<CompanyRole> getCompanyRolesEnum() {
			return companyRoles.stream().map(String::toUpperCase).map(CompanyRole::valueOf).collect(Collectors.toList());
		}
	}

}