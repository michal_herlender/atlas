package org.trescal.cwms.core.company.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.trescal.cwms.core.company.interceptor.UnescapeString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class ContactAddForm {
	private Integer subdivId;
	private Integer addressid;
	private String firstName;
	private String lastName;
	private String position;
	private String telephone;
	private Integer telephoneExt;
	private String mobile;
	private String fax;
	private String email;
	private String preference;
	private String hrid;
	private boolean createAccount;
	private Integer groupid;
	private String languageTag;
	private boolean loginEmail;
	private String password;
	private String password2;
	private Integer printerid;
	private String role;
	private String title;
	private String username;

	// Validation group for user name (not always entered)
	public interface UsernameGroup {
	}

	// Validation group for password (not always entered)
	public interface PasswordGroup {
	}

	@NotNull
	public Integer getSubdivId() {
		return subdivId;
	}

	public String getHrid() {
		return hrid;
	}

	@NotNull(message = "{error.value.notselected}")
	public Integer getAddressid() {
		return this.addressid;
	}

	@Length(max = 30)
	@NotEmpty(message = "{error.contact.nofirstname}")
	public String getFirstName() {
		return firstName;
	}

	@Length(max = 30)
	public String getLastName() {
		return lastName;
	}

	@Length(max = 40)
	public String getPosition() {
		return position;
	}

	@Length(max = 30)
	@Pattern(regexp = "^s*|(\\+[ ]?\\d{1,3}( )?)?((\\(\\d{1,3}\\))[ ]?(\\d)?|\\d{1,3})[- .]?(\\d{2,4}[- .]?){1,4}\\S+$",message = "{error.telephone}")
	public String getTelephone() {
		return telephone;
	}

	public Integer getTelephoneExt() {
		return telephoneExt;
	}

	@Length(max = 20)
	@Pattern(regexp = "^s*|(\\+[ ]?\\d{1,3}( )?)?((\\(\\d{1,3}\\))[ ]?(\\d)?|\\d{1,3})[- .]?(\\d{2,4}[- .]?){1,4}\\S+$",message = "{error.mobile}")
	public String getMobile() {
		return mobile;
	}

	@Length(max = 20)
	@Pattern(regexp = "^s*|(\\+[ ]?\\d{1,3}( )?)?((\\(\\d{1,3}\\))[ ]?(\\d)?|\\d{1,3})[- .]?(\\d{2,4}[- .]?){1,4}\\S+$",message = "{error.fax}")
	public String getFax() {
		return fax;
	}

	/**
	 * Apache commons EmailValidator used via validator, as well as Hibernate @Email
	 * validator (they check different things)
	 */
	@Email
	@Length(max = 200)
	@NotEmpty(message = "{error.contact.email}")
	public String getEmail() {
		return email;
	}

	@Length(max = 1)
	public String getPreference() {
		return preference;
	}

	public boolean isCreateAccount() {
		return createAccount;
	}

	public Integer getGroupid() {
		return this.groupid;
	}

	@NotNull(message = "{error.value.notselected}")
	public String getLanguageTag() {
		return languageTag;
	}

	public String getPassword() {
		return this.password;
	}

	public String getPassword2() {
		return this.password2;
	}

	public Integer getPrinterid() {
		return this.printerid;
	}

	public String getRole() {
		return this.role;
	}

	@NotNull
	@Size(max = 10)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title.trim();
	}

	@Size(min = 3, max = 20, groups = UsernameGroup.class)
	public String getUsername() {
		return this.username;
	}

	public boolean isLoginEmail() {
		return this.loginEmail;
	}

	public void setSubdivId(Integer subdivId) {
		this.subdivId = subdivId;
	}

	public void setHrid(String hrid) {
		this.hrid = hrid;
	}

	public void setAddressid(Integer addressid) {
		this.addressid = addressid;
	}

	public void setCreateAccount(boolean createAccount) {
		this.createAccount = createAccount;
	}

	public void setGroupid(Integer groupid) {
		this.groupid = groupid;
	}

	public void setLanguageTag(String languageTag) {
		this.languageTag = languageTag;
	}

	public void setLoginEmail(boolean loginEmail) {
		this.loginEmail = loginEmail;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public void setPrinterid(Integer printerid) {
		this.printerid = printerid;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone.trim();
	}

	public void setTelephoneExt(Integer telephoneExt) {
		this.telephoneExt = telephoneExt;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile.trim();
	}

	public void setFax(String fax) {
		this.fax = fax.trim();
	}

	@UnescapeString
	public void setEmail(String email) {
		this.email = email.trim();
	}

	public void setPreference(String preference) {
		this.preference = preference;
	}
}
