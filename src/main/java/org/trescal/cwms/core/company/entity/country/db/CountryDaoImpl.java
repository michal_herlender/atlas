package org.trescal.cwms.core.company.entity.country.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.Country_;

@Repository("CountryDao")
public class CountryDaoImpl extends BaseDaoImpl<Country, Integer> implements CountryDao {

	@Override
	protected Class<Country> getEntity() {
		return Country.class;
	}

	@Override
	public Country get(String countryName) {
		return findByUniqueProperty(Country_.country, countryName);
	}

	@Override
	public List<Country> findAll() {
		return getResultList(cb -> {
			CriteriaQuery<Country> cq = cb.createQuery(Country.class);
			Root<Country> country = cq.from(Country.class);
			cq.orderBy(cb.asc(country.get(Country_.country)));
			return cq;
		});
	}
}