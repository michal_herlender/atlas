package org.trescal.cwms.core.company.entity.title.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.title.Title;
import org.trescal.cwms.core.company.entity.title.db.TitleService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.system.Constants;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@JsonController
public class TitleJsonController {

	private final TitleService titleService;
	private final CompanyService companyService;

	public TitleJsonController(TitleService titleService, CompanyService companyService) {
		this.titleService = titleService;
		this.companyService = companyService;
	}

	@GetMapping(value = "/getTitles.json", produces = Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
	@ResponseBody
	public List<Title> getTitles(@RequestParam(value = "coid") Integer coid, @RequestParam(value = "searchText", required = false) String searchText) {
		Company comp = this.companyService.get(coid);

		if (StringUtils.isNotBlank(searchText)){
			return this.titleService.getAllTitles().stream()
					.filter(t -> t.getCountry().equals(comp.getCountry()))
					.filter(title -> title.getTitle().matches("(?i).*" + searchText + ".*"))
					.collect(Collectors.toList());
		}
		return this.titleService.getAllTitles().stream()
				.filter(t -> t.getCountry().equals(comp.getCountry()))
				.collect(Collectors.toList());
	}
}
