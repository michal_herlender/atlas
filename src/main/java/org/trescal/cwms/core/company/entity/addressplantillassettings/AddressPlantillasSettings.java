package org.trescal.cwms.core.company.entity.addressplantillassettings;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;

@Entity
@Table(name="addressplantillassettings", uniqueConstraints={@UniqueConstraint(columnNames={"addressId"})})
public class AddressPlantillasSettings extends Allocated<Company> {
	private int id;
	private Address address;
	private Integer formerCustomerId;
	private Address managedByAddress;
	private Address certificateAddress;
	private String textUncertaintyFailure;
	private String textToleranceFailure;
	private Boolean sendByFtp;
	private String ftpServer;
	private String ftpUser;
	private String ftpPassword;
	private Boolean metraClient;
	private Boolean nextDateOnCertificate;
	private Boolean contract;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@Type(type = "int")
	public int getId() {
		return id;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="addressId", nullable=false)
	public Address getAddress() {
		return address;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="managedByAddressId", nullable=true)
	public Address getManagedByAddress() {
		return managedByAddress;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="certificateAddressId", nullable=true)
	public Address getCertificateAddress() {
		return certificateAddress;
	}
	
	@NotNull
	@Length(max = 100)
	@Column(name = "textUncertaintyFailure", length = 100, nullable=false)
	public String getTextUncertaintyFailure() {
		return textUncertaintyFailure;
	}

	@NotNull
	@Length(max = 100)
	@Column(name = "textToleranceFailure", length = 100, nullable=false)
	public String getTextToleranceFailure() {
		return textToleranceFailure;
	}
	
	@NotNull
	@Column(name = "sendByFtp", nullable = false, columnDefinition = "bit default 0")
	public Boolean getSendByFtp() {
		return sendByFtp;
	}

	@NotNull
	@Column(name = "formerCustomerId", nullable = false, columnDefinition = "int default 0")
	public Integer getFormerCustomerId() {
		return formerCustomerId;
	}

	@NotNull
	@Length(max = 100)
	@Column(name = "ftpServer", length = 100, nullable=false)
	public String getFtpServer() {
		return ftpServer;
	}

	@NotNull
	@Length(max = 100)
	@Column(name = "ftpUser", length = 100, nullable=false)
	public String getFtpUser() {
		return ftpUser;
	}

	@NotNull
	@Length(max = 100)
	@Column(name = "ftpPassword", length = 100, nullable=false)
	public String getFtpPassword() {
		return ftpPassword;
	}
	
	@NotNull
	@Column(name = "metraClient", nullable = false, columnDefinition = "bit default 0")
	public Boolean getMetraClient() {
		return metraClient;
	}

	@NotNull
	@Column(name = "nextDateOnCertificate", nullable=false, columnDefinition = "bit default 0")
	public Boolean getNextDateOnCertificate() {
		return nextDateOnCertificate;
	}

	@NotNull
	@Column(name = "contract", nullable=false, columnDefinition = "bit default 0")
	public Boolean getContract() {
		return contract;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public void setManagedByAddress(Address managedByAddress) {
		this.managedByAddress = managedByAddress;
	}
	public void setCertificateAddress(Address certificateAddress) {
		this.certificateAddress = certificateAddress;
	}
	public void setTextUncertaintyFailure(String textUncertaintyFailure) {
		this.textUncertaintyFailure = textUncertaintyFailure;
	}
	public void setTextToleranceFailure(String textToleranceFailure) {
		this.textToleranceFailure = textToleranceFailure;
	}
	public void setSendByFtp(Boolean sendByFtp) {
		this.sendByFtp = sendByFtp;
	}
	public void setFtpServer(String ftpServer) {
		this.ftpServer = ftpServer;
	}
	public void setFtpUser(String ftpUser) {
		this.ftpUser = ftpUser;
	}
	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}
	public void setMetraClient(Boolean metraClient) {
		this.metraClient = metraClient;
	}
	public void setNextDateOnCertificate(Boolean nextDateOnCertificate) {
		this.nextDateOnCertificate = nextDateOnCertificate;
	}
	public void setFormerCustomerId(Integer formerCustomerId) {
		this.formerCustomerId = formerCustomerId;
	}
	public void setContract(Boolean contract) {
		this.contract = contract;
	}
}
