package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.entity.title.db.TitleService;
import org.trescal.cwms.core.company.form.ContactAddForm;
import org.trescal.cwms.core.company.form.ContactAddFormValidator;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.portal.usergroup.db.UserGroupService;
import org.trescal.cwms.core.login.entity.role.Roles;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.printer.db.PrinterService;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.userright.entity.role.db.RoleService;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME })
public class ContactAddController {

	@Autowired
	private AddressService addressServ;
	@Autowired
	private ContactService contactServ;
	@Autowired
	private UserService userServ;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private PrinterService printerServ;
	@Autowired
	private RoleService roleServ;
	@Autowired
	private SubdivService subdivServ;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private UserGroupService userGroupServ;
	@Autowired
	private ContactAddFormValidator validator;
	@Autowired
	private TitleService titleService;

	public static final String VIEW_NAME = "trescal/core/company/contactadd";

	public static final String HRID_DELIMITER = "-";

	@ModelAttribute("command")
	public ContactAddForm formBackingObject(Locale locale,
			@RequestParam(value = "subdivid", required = true) Integer subdivId) {
		Subdiv sub = this.subdivServ.get(subdivId);
		if (sub == null)
			throw new RuntimeException("No subdivision found for id " + subdivId);
		ContactAddForm caf = new ContactAddForm();
		caf.setSubdivId(subdivId);
		caf.setLoginEmail(true);
		caf.setLanguageTag(sub.getComp().getDocumentLanguage().toLanguageTag());
		caf.setGroupid(this.userGroupServ.getDefaultGroup().getGroupid());
		if (sub.getComp().getCompanyRole().equals(CompanyRole.BUSINESS)) {
			// For business companies, account creation on by default
			caf.setCreateAccount(true);
			caf.setRole("");
		} else {
			// For all other companies, user account creation off by default
			caf.setCreateAccount(false);
			caf.setRole(Roles.ROLE_WEB.name());
		}
		if (sub.getDefaultAddress() != null) {
			caf.setAddressid(sub.getDefaultAddress().getAddrid());
		}
		caf.setHrid(sub.getComp().getCountry().getCountryCode() + HRID_DELIMITER);
		return caf;
	}

	@RequestMapping(value = "/addperson.htm", method = RequestMethod.POST)
	public String onSubmit(Locale locale, Model model, @ModelAttribute("command") ContactAddForm caf,
			BindingResult result, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {

		validator.validate(caf, result, this.userServ.get(username).getCon().getPersonid());
		if (result.hasErrors()) {
			return this.referenceData(locale, model, caf);
		} else {
			Integer personId = this.contactServ.createContact(caf);
			model.asMap().clear();
			return "redirect:viewperson.htm?personid=" + personId;
		}
	}

	@RequestMapping(value = "/addperson.htm", method = RequestMethod.GET)
	public String referenceData(Locale displayLocale, Model model, @ModelAttribute("command") ContactAddForm caf) {
		Subdiv subdiv = this.subdivServ.get(caf.getSubdivId());
		model.addAttribute("subdiv", subdiv);
		model.addAttribute("usergroups", this.userGroupServ.getSortedUserGroups(subdiv.getComp()));
		model.addAttribute("addresses",
				this.addressServ.getAllSubdivAddresses(subdiv.getSubdivid(), AddressType.WITHOUT, true));
		List<Locale> locales = this.supportedLocaleService.getSupportedLocales();
		model.addAttribute("supportedLocales", this.supportedLocaleService.getDTOList(locales, displayLocale));
		List<KeyValue<Integer, String>> printerDtos = new ArrayList<>();
		printerDtos.add(new KeyValue<>(0, messageSource.getMessage("contactadd.defaultnone", null, displayLocale)));
		printerDtos.addAll(this.printerServ.getDtosBySubdiv(subdiv));
		model.addAttribute("printers", printerDtos);
		List<KeyValue<String, String>> dtoTitles = titleService.getDTOTitles(displayLocale.getCountry());
		model.addAttribute("titles",dtoTitles);
		if (subdiv.getComp().getCompanyRole().equals(CompanyRole.BUSINESS)) {
			// add all of the available roles
			model.addAttribute("roles", roleServ.getAllRoleName());
		} else {
			model.addAttribute("roles", EnumSet.of(Roles.ROLE_WEB));
		}
		// add a default role update default role may be add a colum to role to
		// know
		// which one is a default role
		// hrid country prefix
		model.addAttribute("hridcountryprefix", subdiv.getComp().getCountry().getCountryCode() + HRID_DELIMITER);
		return VIEW_NAME;
	}
}