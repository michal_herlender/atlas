package org.trescal.cwms.core.company.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.company.entity.company.db.CompanyDWRService;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller @IntranetController
public class UpdateCompanySubdivDefaultController {
	
	@Autowired
	CompanyDWRService companyDWRService;
	
	@RequestMapping(value="/updatecompanysubdivdefault.json", method = RequestMethod.GET)
	@ResponseBody
	protected void updateCompanySubdivDefault(@RequestParam(required = true) Integer coid, 
			@RequestParam(required = true) Integer subdivid) throws Exception {
		companyDWRService.updateCompanySubdivDefault(coid, subdivid);
	}

}
