/**
 * 
 */
package org.trescal.cwms.core.company.controller;

import java.util.List;
import java.util.Locale;

import javax.sound.midi.Instrument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.company.entity.departmentmember.db.DepartmentMemberService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.form.MoveContactForm;
import org.trescal.cwms.core.company.form.MoveContactValidator;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;

/**
 * Presents a form allowing a user to move a {@link Contact} from it's current
 * {@link Subdiv} to another {@link Subdiv} within the same {@link Company}. 
 * 
 * The user must select a new {@link Subdiv} and a new {@link Address} to assign to
 * the {@link Contact}.
 * 
 * Furthermore, if the {@link Contact} has any equipment the user must decide to move 
 * those {@link Instrument}s to the {@link Contact}'s new {@link Subdiv} or reassign 
 * the {@link Instrument}s to another {@link Contact} within the current {@link Subdiv}. 
 * 
 * Options are also presented to allow the user to update {@link Job}, {@link Quotation},
 * {@link TPQuotation} and {@link TPQuoteRequest} history for the {@link Contact}.
 */
@Controller @IntranetController
@SessionAttributes(names=Constants.SESSION_ATTRIBUTE_USERNAME)
public class MoveContactController
{
	@Autowired
	private AddressService addressService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private DepartmentMemberService deptMemberService;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;
	@Autowired
	private MoveContactValidator validator; 
	
	@InitBinder("command")
    protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
    }
	
	@ModelAttribute("command")
	protected MoveContactForm formBackingObject(
			@RequestParam(value="personid", required=true) Integer personid) throws Exception {
		MoveContactForm form = new MoveContactForm();
		form.setContactId(personid);
		form.setNewAddressId(0);
		form.setNewOwnerId(0);
		form.setNewSubdivId(0);
		return form;
	}
	
	@RequestMapping(value="/movecontact.htm", method=RequestMethod.POST)
	protected String onSubmit(Model model, Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@Validated @ModelAttribute("command") MoveContactForm form, BindingResult result) throws Exception {
		if (result.hasErrors()) {
			return referenceData(model, locale, form);
		}
		Subdiv newSub = this.subdivService.get(form.getNewSubdivId());
		Address newAddress = this.addressService.get(form.getNewAddressId());
		
		Contact userContact = this.userService.get(username).getCon();
		Contact contact = this.contactService.get(form.getContactId());
		contact.setSub(newSub);
		contact.setDefAddress(newAddress);
		contact = this.contactService.merge(contact);
		
		// remove the contact from any departments he may belong to
		List<DepartmentMember> members = this.deptMemberService.getAllContactDepartmentMembers(contact.getPersonid());
		for (DepartmentMember member : members)
		{
			this.deptMemberService.deleteDepartmentMember(member);
		}
		
		// now update the contacts instruments to another Contact in the
		// original subdivision
		if (form.isChangeInstrumOwner())
		{
			this.instrumentService.updateInstrumentOwnerReferences(form.getContactId(), form.getNewOwnerId(), userContact);
			//this.instrumentService.updateInstrumentOwner(contact, this.contactService.get(form.getNewOwnerId()), userContact);
		}
		// or move the instruments over to the new subdivision and remove any
		// references to old locations
		else
		{
			this.instrumentService.moveToSubdivisionReferences(form.getContactId(), form.getNewAddressId(), userContact);
			//this.instrumentService.moveToSubdivision(this.instrumentService.getAllContactInstrums(contact.getPersonid()), newAddress, userContact);
		}
		
		return "redirect:viewperson.htm?personid=" + form.getContactId();
	}
	
	@RequestMapping(value="/movecontact.htm", method=RequestMethod.GET)
	protected String referenceData(Model model, Locale locale,
			@ModelAttribute("command") MoveContactForm form) throws Exception
	{
		Contact contact = this.contactService.get(form.getContactId());
		
		model.addAttribute("contact", contact);
		
		// provide initial dto list of addresses, note subdiv may be selected in form (e.g. validation error)
		model.addAttribute("addressList", this.addressService.getAllActiveSubdivAddressesKeyValue(form.getNewSubdivId(), AddressType.WITHOUT, true)); 

		// get a list of all available subdivisions for the company
		int coid = contact.getSub().getComp().getCoid();
		model.addAttribute("subdivList", this.subdivService.getAllActiveCompanySubdivsKeyValue(coid, true));
		
		// get a list of all contacts available for the current contact's
		// instruments to be reassigned tos
		model.addAttribute("contactlist", this.contactService.getAllActiveSubdivContacts(contact.getSub().getSubdivid()));
		
		return "trescal/core/company/movecontact";
	}
}