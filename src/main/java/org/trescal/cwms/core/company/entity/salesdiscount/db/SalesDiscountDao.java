package org.trescal.cwms.core.company.entity.salesdiscount.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.salesdiscount.SalesDiscount;


public interface SalesDiscountDao extends BaseDao<SalesDiscount, String> {}