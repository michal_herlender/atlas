package org.trescal.cwms.core.company.form;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class AddressPlantillasForm {
	private Integer formerCustomerId;
	private String textUncertaintyFailure;
	private String textToleranceFailure;
	private boolean sendByFtp;
	private String ftpServer;
	private String ftpUser;
	private String ftpPassword;
	private boolean metraClient;
	private boolean nextDateOnCertificate;
	private boolean contract;
	
	@NotNull(message="{error.numericvalue.notentered}")
	public Integer getFormerCustomerId() {
		return formerCustomerId;
	}

	@NotNull
	@Length(max=100)
	public String getTextUncertaintyFailure() {
		return textUncertaintyFailure;
	}
	
	@NotNull
	@Length(max=100)
	public String getTextToleranceFailure() {
		return textToleranceFailure;
	}
	
	@NotNull
	public boolean getSendByFtp() {
		return sendByFtp;
	}
	
	@NotNull
	@Length(max=100)
	public String getFtpServer() {
		return ftpServer;
	}
	
	@NotNull
	@Length(max=100)
	public String getFtpUser() {
		return ftpUser;
	}
	
	@NotNull
	@Length(max=100)
	public String getFtpPassword() {
		return ftpPassword;
	}
	
	@NotNull
	public boolean getMetraClient() {
		return metraClient;
	}
	
	@NotNull
	public boolean getNextDateOnCertificate() {
		return nextDateOnCertificate;
	}

	@NotNull
	public boolean getContract() {
		return contract;
	}

	public void setFormerCustomerId(Integer formerCustomerId) {
		this.formerCustomerId = formerCustomerId;
	}	
	public void setTextUncertaintyFailure(String textUncertaintyFailure) {
		this.textUncertaintyFailure = textUncertaintyFailure;
	}
	public void setTextToleranceFailure(String textToleranceFailure) {
		this.textToleranceFailure = textToleranceFailure;
	}
	public void setSendByFtp(boolean sendByFtp) {
		this.sendByFtp = sendByFtp;
	}
	public void setFtpServer(String ftpServer) {
		this.ftpServer = ftpServer;
	}
	public void setFtpUser(String ftpUser) {
		this.ftpUser = ftpUser;
	}
	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}
	public void setMetraClient(boolean metraClient) {
		this.metraClient = metraClient;
	}
	public void setNextDateOnCertificate(boolean nextDateOnCertificate) {
		this.nextDateOnCertificate = nextDateOnCertificate;
	}
	public void setContract(boolean contract) {
		this.contract = contract;
	}
}
