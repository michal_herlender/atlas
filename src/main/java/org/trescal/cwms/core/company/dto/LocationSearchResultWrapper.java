package org.trescal.cwms.core.company.dto;

public class LocationSearchResultWrapper
{
	private String location;
	private int locationid;

	public LocationSearchResultWrapper(int locationid, String location)
	{
		this.locationid = locationid;
		this.location = location;
	}

	public String getLocation()
	{
		return this.location;
	}

	public int getLocationid()
	{
		return this.locationid;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public void setLocationid(int locationid)
	{
		this.locationid = locationid;
	}

}