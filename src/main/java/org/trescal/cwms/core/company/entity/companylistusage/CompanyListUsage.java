package org.trescal.cwms.core.company.entity.companylistusage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companylist.CompanyList;

import lombok.Setter;

@Entity @Setter
@Table(name="companylistusage")
public class CompanyListUsage extends Versioned{

	private Integer id;
	private CompanyList companyList;
	private Company company;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getId() {
		return id;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "companylistid", foreignKey = @ForeignKey(name = "FK_companylistusage_companylist"), nullable = false)
	public CompanyList getCompanyList() {
		return companyList;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coid", foreignKey = @ForeignKey(name = "FK_companylistusage_company"), nullable = false)
	public Company getCompany() {
		return company;
	}

}
