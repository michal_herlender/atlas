package org.trescal.cwms.core.company.entity.invoicefrequency.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.invoicefrequency.InvoiceFrequency;

@Service
public class InvoiceFrequencyServiceImpl extends
		BaseServiceImpl<InvoiceFrequency, Integer> implements
		InvoiceFrequencyService {

	@Autowired
	private InvoiceFrequencyDao invoiceFrequencyDao;
	
	@Override
	protected BaseDao<InvoiceFrequency, Integer> getBaseDao() {
		return this.invoiceFrequencyDao;
	}

	@Override
	public InvoiceFrequency get(String code) {
		return this.invoiceFrequencyDao.get(code);
	}


}
