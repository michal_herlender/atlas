package org.trescal.cwms.core.company.entity.location.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.dto.LocationSearchResultWrapper;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.LocationDTO;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.spring.model.KeyValueIntegerString;
import org.trescal.cwms.spring.model.LabelIdDTO;

public interface LocationService extends BaseService<Location, Integer> {

	Location findLocation(int addrid, String location);

	/**
	 * Returns a {@link List} of {@link LocationSearchResultWrapper} that belong to
	 * the {@link Address} identified by the given addrid that are currently active.
	 * 
	 * @param addrid     the id of the {@link Address}, not null.
	 * @param activeOnly boolean indicating if we want only active locations.
	 * @return {@link List} of {@link LocationSearchResultWrapper}.
	 */
	List<LocationSearchResultWrapper> getAllActiveAddressLocationsHQL(int addrid, boolean activeOnly);

	List<Location> getAllAddressLocations(int addrid, boolean activeOnly);
	
	List<LocationDTO> getAddressLocations(int addrid, boolean activeOnly);

	List<Location> getAllBusinessLocations(boolean activeOnly);

	List<Location> getAllActiveSubdivLocations(Subdiv subdiv);

	/**
	 * @deprecated use save()
	 */
	void insertLocation(Location location);

	boolean locationAlreadyExistsAtAddress(String location, int addrId, int excludeLocId);

	/**
	 * @deprecated use save / merge
	 */
	void saveOrUpdate(Location location);

	List<LabelIdDTO> getLocationsForAutocomplete(Company company, Subdiv subdivision, Address address,
			String searchTerm);
	
	List<KeyValueIntegerString> getLocationDtos(Collection<Integer> locationIds);
	
	List<Location> getAllCompanyLocationsActive(Integer coid);
}