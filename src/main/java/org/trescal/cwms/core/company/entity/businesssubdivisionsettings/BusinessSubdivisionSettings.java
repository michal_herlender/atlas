package org.trescal.cwms.core.company.entity.businesssubdivisionsettings;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

/*
 * Stores settings specific to subdivisions of business companies 
 */

@Entity
@Table(name = "businesssubdivisionsettings")
public class BusinessSubdivisionSettings extends Versioned {
	private int id;
	private Subdiv subdiv;
	private BigDecimal ebitdaHourlyCostRate;
	private Integer defaultTurnaround;
    private Boolean workInstructionReference;
    private Contact defaultBusinessContact;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	@NotNull
	@OneToOne
	@JoinColumn(name = "subdivid", nullable = false)
	public Subdiv getSubdiv() {
		return subdiv;
	}
	
	@NotNull
	@Column(name = "ebitdahourlycostrate", unique = false, nullable = false, precision = 10, scale = 2)
	public BigDecimal getEbitdaHourlyCostRate() {
		return ebitdaHourlyCostRate;
	}

	@NotNull
	@Column(name = "defaultturnaround", nullable=false)
	public Integer getDefaultTurnaround() {
		return defaultTurnaround;
	}
	
	/**
	 * Indicates that we prefer to use a "work instruction" based procedure reference for certificate, default is false.
	 * @return Boolean
	 */
	@NotNull
	@Column(name="workinstructionreference", nullable=false, columnDefinition="bit")
	public Boolean getWorkInstructionReference() {
		return workInstructionReference;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "defaultbusinesscontact")
	public Contact getDefaultBusinessContact() {
		return this.defaultBusinessContact;
	}
	
	public void setWorkInstructionReference(Boolean workInstructionReference) {
		this.workInstructionReference = workInstructionReference;
	}
	

	public void setId(int id) {
		this.id = id;
	}

	public void setSubdiv(Subdiv subdiv) {
		this.subdiv = subdiv;
	}

	public void setEbitdaHourlyCostRate(BigDecimal ebitdaHourlyCostRate) {
		this.ebitdaHourlyCostRate = ebitdaHourlyCostRate;
	}

	public void setDefaultTurnaround(Integer defaultTurnaround) {
		this.defaultTurnaround = defaultTurnaround;
	}

	public void setDefaultBusinessContact(Contact defaultBusinessContact) {
		this.defaultBusinessContact = defaultBusinessContact;
	}
	
}