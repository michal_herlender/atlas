package org.trescal.cwms.core.company.entity.companysettings.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.dto.CompanySettingsDTO;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companystatus.CompanyStatus;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.invoicefrequency.InvoiceFrequency;
import org.trescal.cwms.core.company.entity.invoicefrequency.db.InvoiceFrequencyService;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentmode.db.PaymentModeService;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.db.VatRateService;
import org.trescal.cwms.core.quotation.dto.CompanySettingsForAllocatedCompanyDTO;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

@Service
public class CompanySettingsForAllocatedCompanyServiceImpl
		extends BaseServiceImpl<CompanySettingsForAllocatedCompany, Integer>
		implements CompanySettingsForAllocatedCompanyService {
	// At present time the defaults are defined here, in future we may need to
	// configure via database
	public static final String DEFAULT_CODE_INVOICE_FREQUENCY = "OG";
	public static final String DEFAULT_CODE_PAYMENT_MODE = "WT";

	private final Logger logger = LoggerFactory.getLogger(CompanySettingsForAllocatedCompanyServiceImpl.class);

	@Autowired
	private CompanySettingsForAllocatedCompanyDao baseDao;

	@Autowired
	private InvoiceFrequencyService invoiceFrequencyService;

	@Autowired
	private PaymentModeService paymentModeService;

	@Autowired
	private VatRateService vatRateService;

	@Override
	protected BaseDao<CompanySettingsForAllocatedCompany, Integer> getBaseDao() {
		return baseDao;
	}

	@Override
	public CompanySettingsForAllocatedCompany getByCompany(Company company, Company allocatedCompany) {
		return baseDao.getByCompany(company, allocatedCompany);
	}

	@Override
	public List<CompanySettingsForAllocatedCompany> getAll(Set<Integer> companyIds, Company allocatedCompany) {
		return baseDao.getAll(companyIds, allocatedCompany);
	}

	@Override
	public List<CompanySettingsDTO> getDtosForAllocatedCompany(Collection<Integer> companyIds, Integer allocatedCompanyId) {
		return baseDao.getDtosForAllocatedCompany(companyIds, allocatedCompanyId);
	}

	/*
	 * The default settings for a new CompanySettingsForAllocatedCompany are defined
	 * here This also attaches the settings to the Company. Note that this method
	 * does not save the settings; it is up to the caller to save the company (which
	 * cascades save to CompanySettings) if desired.
	 */
	@Override
	public CompanySettingsForAllocatedCompany initializeForCompany(Company company, Company allocatedCompany,
			boolean newCompany) {
		CompanySettingsForAllocatedCompany settings = new CompanySettingsForAllocatedCompany();
		settings.setActive(getDefaultActive(company.getCompanyRole(), newCompany));
		// BankAccount remains uninitialized / null (chosen by finance dept upon
		// activation)
		settings.setCompany(company);
		settings.setCompanyReference("");
		settings.setContract(false);
		settings.setCreditLimit(new BigDecimal(0));
		settings.setDeactDate(new Date());
		settings.setDeactReason("");
		settings.setInvoiceFrequency(getDefaultInvoiceFrequency());
		settings.setInvoiceFactoring(false);
		settings.setOnStop(getDefaultOnStop(company.getCompanyRole()));
		// OnStopBy remains uninitialized / null for newly initialized companies
		settings.setOnStopReason("");
		settings.setOnStopSince(getDefaultOnStopSince(company.getCompanyRole()));
		settings.setOrganisation(allocatedCompany);
		settings.setPaymentMode(getDefaultPaymentMode());
		//settings.setPaymentTerms(getDefaultPaymentTerms(company.getCompanyRole()));
		settings.setPaymentterm(getDefaultPaymentTerm(company.getCompanyRole()));
		settings.setPoRequired(true);
		settings.setSoftwareAccountNumber("");
		// set the requireSupplierCompanyList setting to false
		settings.setRequireSupplierCompanyList(false);
		// In specialized cases this may be overwritten afterwards, i.e. user created a
		// company that is to be checked.
		settings.setStatus(getDefaultStatus(company.getCompanyRole(), newCompany));
		settings.setSyncToPlantillas(false);
		settings.setTaxable(true);
		settings.setVatrate(
				getDefaultVatrate(company.getCountry(), allocatedCompany.getCountry(), company.getCompanyRole()));

		if (company.getSettingsForAllocatedCompanies() == null) {
			company.setSettingsForAllocatedCompanies(new HashSet<>());
		}
		company.getSettingsForAllocatedCompanies().add(settings);
		return settings;
	}

	private boolean getDefaultActive(CompanyRole companyRole, boolean newCompany) {
		// If company is a new company (user creating/owning the company),
		// set status to TO_CHECK and active=false except for Prospect,
		// for which status stays in NOT_VERIFIED and active=true
		// For existing companies, status should start in NOT_VERIFIED and active=false
		// so that they can be explicitly assigned TO_CHECK only when needed.
		return newCompany && (companyRole == CompanyRole.PROSPECT);
	}

	private boolean getDefaultOnStop(CompanyRole companyRole) {
		// if new client or prospect company, set immediately to on-stop by default
		return companyRole == CompanyRole.CLIENT || companyRole == CompanyRole.PROSPECT;
	}

	private LocalDate getDefaultOnStopSince(CompanyRole companyRole) {
		// if new client or prospect company, set to on-stop date as today
		if (companyRole == CompanyRole.CLIENT || companyRole == CompanyRole.PROSPECT) {
			return LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		} else {
			return null;
		}
	}

	private InvoiceFrequency getDefaultInvoiceFrequency() {
		InvoiceFrequency invoiceFrequency = this.invoiceFrequencyService.get(DEFAULT_CODE_INVOICE_FREQUENCY);
		if (invoiceFrequency == null) {
			logger.error("Database setup issue: InvoiceFrequency not found for code " + DEFAULT_CODE_INVOICE_FREQUENCY);
		}
		return invoiceFrequency;
	}

	private PaymentMode getDefaultPaymentMode() {
		PaymentMode paymentMode = this.paymentModeService.get(DEFAULT_CODE_PAYMENT_MODE);
		if (paymentMode == null) {
			logger.error("Database setup issue: PaymentMode not found for code " + DEFAULT_CODE_PAYMENT_MODE);
		}
		return paymentMode;
	}

	private PaymentTerm getDefaultPaymentTerm(CompanyRole companyRole){
		PaymentTerm paymentTerm = null;
		if (companyRole == CompanyRole.CLIENT || companyRole == CompanyRole.PROSPECT) {
			paymentTerm = PaymentTerm.P30;
		} else if (companyRole == CompanyRole.SUPPLIER || companyRole == CompanyRole.UTILITY) {
			paymentTerm = PaymentTerm.P45EOM;
		} else if (companyRole == CompanyRole.BUSINESS) {
			paymentTerm = PaymentTerm.PNM25;
		}
		if (paymentTerm == null) {
			logger.error("Database setup issue: PaymentMode not found for code " + DEFAULT_CODE_PAYMENT_MODE
					+ " via role " + companyRole.name());
		}
		return paymentTerm;
	}

	private CompanyStatus getDefaultStatus(CompanyRole companyRole, boolean newCompany) {
		// If company is a new company (user creating/owning the company),
		// set status to TO_CHECK and active=false except for Prospect,
		// for which status stays in NOT_VERIFIED and active=true
		// For existing companies, status should start in NOT_VERIFIED and active=false
		// so that they can be explicitly assigned TO_CHECK only when needed.
		if (newCompany && (companyRole != CompanyRole.PROSPECT)) {
			return CompanyStatus.TO_CHECK;
		} else {
			return CompanyStatus.NOT_VERIFIED;
		}
	}

	private VatRate getDefaultVatrate(Country companyCountry, Country businessCountry, CompanyRole companyRole) {
		VatRate vatRate = this.vatRateService.getDefault(companyCountry, businessCountry, companyRole);
		if (vatRate == null) {
			logger.error("Database setup issue: VatRate not found between " + companyRole + " country "
					+ companyCountry.getCountryCode() + " and business country " + businessCountry.getCountryCode());
		}
		return vatRate;
	}

	@Override
	public List<CompanySettingsForAllocatedCompanyDTO> getAllSettingsDtosForAllocatedCompany(
			Collection<Integer> companyIds, Collection<Integer> allocatedCompanyIds) {
		return this.baseDao.getAllSettingsDtosForAllocatedCompany(companyIds, allocatedCompanyIds);
	}
}