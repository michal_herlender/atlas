package org.trescal.cwms.core.company.controller;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.form.UserGroupAddEditForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;
import org.trescal.cwms.core.login.entity.portal.UserScope;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;
import org.trescal.cwms.core.login.entity.portal.usergroup.db.UserGroupService;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.UserGroupPropertyValue;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.db.UserGroupPropertyValueService;

@Controller
@IntranetController
@RequestMapping(value = "addeditusergroup.htm")
public class UserGroupAddEditController {

	@Autowired
	private CompanyService companyService;
	@Autowired
	private UserGroupService userGroupService;
	@Autowired
	private UserGroupPropertyValueService userGroupPropertyValueService;

	private static final String FORM_NAME = "command";
	
	@ModelAttribute(FORM_NAME)
	public UserGroupAddEditForm initializeForm() {
		return new UserGroupAddEditForm();
	}

	@ModelAttribute("allproperties")
	public UserGroupProperty[] initializeAllProperties() {
		return UserGroupProperty.values();
	}

	@ModelAttribute("allscopes")
	public UserScope[] initializeAllScopes() {
		return UserScope.values();
	}

	@RequestMapping(method = RequestMethod.GET)
	public String displayForm(@RequestParam(name = "coid", required = false, defaultValue = "0") Integer coId,
			@RequestParam(name = "groupid", required = false, defaultValue = "0") Integer groupId,
			@ModelAttribute(FORM_NAME) UserGroupAddEditForm userGroupAddEditForm) {
		List<Integer> values = Arrays.stream(UserGroupProperty.values()).map(p -> p.getDefaultValue().ordinal())
				.collect(Collectors.toList());
		userGroupAddEditForm.setSelectedValues(values);
		userGroupAddEditForm.setCoid(coId);
		userGroupAddEditForm.setUserGroupId(groupId);
		if (groupId > 0) {
			UserGroup userGroup = userGroupService.findGroup(groupId);
			userGroupAddEditForm.setName(userGroup.getName());
			userGroupAddEditForm.setDescription(userGroup.getDescription());
			userGroupAddEditForm.setCoid(userGroup.getCompany().getId());
			for (UserGroupPropertyValue userGroupPropertyValue : userGroup.getProperties()) {
				values.set(userGroupPropertyValue.getUserGroupProperty().ordinal(),
						userGroupPropertyValue.getScope().ordinal());
			}
		}
		return "trescal/core/company/usergroupaddedit";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String processForm(@ModelAttribute(FORM_NAME) UserGroupAddEditForm userGroupAddEditForm) {
		UserGroup userGroup;
		Integer coid = userGroupAddEditForm.getCoid();
		if (userGroupAddEditForm.getUserGroupId() == 0) {
			userGroup = new UserGroup();
			Set<UserGroupPropertyValue> properties = new HashSet<>();
			for (UserGroupProperty userGroupProperty : UserGroupProperty.values()) {
				UserScope scope = UserScope.values()[userGroupAddEditForm.getSelectedValues()
						.get(userGroupProperty.ordinal())];
				UserGroupPropertyValue value = new UserGroupPropertyValue();
				value.setUserGroup(userGroup);
				value.setUserGroupProperty(userGroupProperty);
				value.setScope(scope);
				properties.add(value);
			}
			userGroup.setProperties(properties);
			userGroup.setCompany(companyService.get(userGroupAddEditForm.getCoid()));
			userGroup.setName(userGroupAddEditForm.getName());
			userGroup.setDescription(userGroupAddEditForm.getDescription());
			userGroupService.insertUserGroup(userGroup);
		} else {
			userGroup = userGroupService.findGroup(userGroupAddEditForm.getUserGroupId());
			userGroup.setName(userGroupAddEditForm.getName());
			userGroup.setDescription(userGroupAddEditForm.getDescription());
			userGroupService.updateUserGroup(userGroup);
			Set<UserGroupPropertyValue> properties = new HashSet<>(userGroup.getProperties());
			for (UserGroupPropertyValue userGroupPropertyValue : properties) {
				UserGroupProperty userGroupProperty = userGroupPropertyValue.getUserGroupProperty();
				UserScope scope = UserScope.values()[userGroupAddEditForm.getSelectedValues()
						.get(userGroupProperty.ordinal())];
				userGroupPropertyValue.setScope(scope);
				userGroupPropertyValueService.merge(userGroupPropertyValue);
			}
		}
		return "redirect:/viewcomp.htm?coid=" + coid;
	}
}
