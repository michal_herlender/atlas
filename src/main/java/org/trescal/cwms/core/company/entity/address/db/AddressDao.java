package org.trescal.cwms.core.company.entity.address.db;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.dto.AddrSearchResultWrapper;
import org.trescal.cwms.core.company.dto.AddressPlantillasDTO;
import org.trescal.cwms.core.company.dto.AddressWithTypeDTO;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.projection.AddressProjectionDTO;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.tlm.dto.TLMAddressProjectionDTO;

public interface AddressDao extends BaseDao<Address, Integer> {
	int countAddressInstruments(int addrid);

	Long countCompanyAddresses(Company company, AddressType type, Address excludeAddress);

	List<AddrSearchResultWrapper> getActiveUserRoleAddresses(String username);

	/**
	 * Returns a projection list of addresses; either a coid or subdivid must be specified
	 * (to prevent returning all results in the database)
	 *  
	 * @param coid optional id of Company
	 * @param subdivid optional id of Subdiv
	 * @param type optional AddressType
	 * @return List of AddrSearchResultWrapper
	 */
	List<AddrSearchResultWrapper> getActiveAddressesProjection(Integer coid, Integer subdivid, AddressType type);
	
	List<Integer> getAddressIdsForContactInstruments(Integer contactId);
	
	List<AddrSearchResultWrapper> getAddressesFromIds(Collection<Integer> addressIds);

	List<Address> getAllCompanyAddresses(int coid, AddressType type, boolean activeOnly);

	List<Address> getAllSubdivAddresses(int subdivid, AddressType type, Boolean active);

	List<Address> getBusinessAddresses(AddressType type, boolean activeOnly);

	AddressPlantillasDTO getPlantillasAddress(Integer addressId, Company allocatedCompany);

	PagedResultSet<AddressPlantillasDTO> getPlantillasAddresses(Company allocatedCompany, Date afterLastModifiedDate,
			Integer formerCustomerId, int businessSubdivId, Integer resultsPerPage, Integer currentPage);

	Address findCompanyAddress(int coid, AddressType type, boolean activeOnly, String address);

	TLMAddressProjectionDTO getAllCompanyAddresses(int addrid);

	PagedResultSet<TLMAddressProjectionDTO> getAllCompanyAddresses(Company company, List<CompanyRole> roles,
			Timestamp companyLastModifiedDate, Timestamp subdivLastModifiedDate, Timestamp addressLastModifiedDate,
			int resultsPerPage, int page);
	
	List<AddressWithTypeDTO> getForCompany(Integer companyId);
	
	List<AddressProjectionDTO> getAddressProjectionDTOs(Collection<Integer> addressIds, Integer allocatedCompanyId);
	
	Address getAddressFromSubdiv(Address address);
}