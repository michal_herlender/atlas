/**
 * 
 */
package org.trescal.cwms.core.validation;

import org.springframework.validation.BindException;
import org.springframework.validation.Validator;

/**
 * Abstract validator wrapper that provides access to a {@link Validator} from
 * it's errors object. Also enables one validator to create another validator
 * and delegate to it to perform validation for one particular entity.
 * 
 * @author Richard
 * 
 * TODO: Note - this does not appear to be threadsafe whatsoever!
 * Discuss/investigate deprecation / removal.  GB 2015-06-25
 */
public abstract class AbstractEntityValidator implements Validator
{
	private String command = "";
	protected BindException errors;

	/**
	 * Returns the current errors object for this {@link Validator}.
	 * 
	 * @return {@link BindException}.
	 */
	public BindException getErrors()
	{
		return this.errors;
	}

	/**
	 * Convenience function which can be called to set the full property path
	 * for the given property based on the command name.
	 * 
	 * @param field the field being validated.
	 * @return full path to the field.
	 */
	protected String getFullPropertyPath(String field)
	{
		return this.command.equals("") ? field : this.command.concat(".").concat(field);
	}

	/**
	 * Setter to set the command name. The command name is only used if this
	 * validator is being called from another validator and thus we need the
	 * path to the field(s) being validated from the calling validator.
	 * 
	 * @param command
	 */
	public void setCommand(String command)
	{
		this.command = command;
	}
}
