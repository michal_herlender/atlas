package org.trescal.cwms.core.validation;

import org.springframework.stereotype.Component;

/*
 * Actual instance class of AbstractBeanValidator for instantiation / autowiring.
 */
@Component("BeanValidator")
public class BeanValidator extends AbstractBeanValidator {

	@Override
	public boolean supports(Class<?> arg0) {
		return true;
	}
}