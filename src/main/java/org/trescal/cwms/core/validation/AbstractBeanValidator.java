package org.trescal.cwms.core.validation;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/*
 * This is a Spring Framework validator that will check JSR-303 (JPA validation) annotations
 * on any type of class.  It can be referenced by other existing validators to replace the
 * AnnotationValidator which will not pick up the new javax.validation annotations. 
 * 
 * Implemented as per example as written by one of the Spring Validation developers 
 * http://blog.trifork.com/2009/08/04/bean-validation-integrating-jsr-303-with-spring/
 * 
 * GB 2015-06-24
 */
@Component
public abstract class AbstractBeanValidator implements org.springframework.validation.Validator {
	@Autowired
	private LocalValidatorFactoryBean validator;

	private static final Logger logger = LoggerFactory.getLogger(AbstractBeanValidator.class);

	@Override
	/**
	 * Implementation of Spring validator using JPA annotations. Subclasses
	 * should normally call this to validate bean (form/object) using JPA
	 * annotation constraints.
	 */
	public void validate(Object target, Errors errors) {
		Set<ConstraintViolation<Object>> violations = validator.validate(target);
		addConstraintViolationsToErrors(violations, errors);
	}

	/**
	 * Alternate call that validates just the parameters in the specified
	 * validation group(s). Subclasses can use this to validate only specific
	 * parameters in a bean.
	 * 
	 * @param target
	 *            the bean (e.g. form/object) to be validated
	 * @param errors
	 *            the Spring Errors object
	 * @param groups
	 *            one or more validation groups
	 */
	public void validate(Object target, Errors errors, Class<?>... groups) {
		Set<ConstraintViolation<Object>> violations = validator.validate(target, groups);
		addConstraintViolationsToErrors(violations, errors);
	}

	public void validate(Object target, Errors errors, String propertyPath) {
		Set<ConstraintViolation<Object>> violations = validator.validateProperty(target, propertyPath);
		addConstraintViolationsToErrors(violations, errors);
	}

	public void validate(Object target, Errors errors, Object... validationHints) {
		validator.validate(target, errors, validationHints);
	}

	private void addConstraintViolationsToErrors(Set<ConstraintViolation<Object>> violations, Errors errors) {
		for (ConstraintViolation<Object> constraintViolation : violations) {
			String propertyPath = constraintViolation.getPropertyPath().toString();
			String message = constraintViolation.getMessage();
			logger.debug(
					"Rejecting " + propertyPath + " : " + message + " : " + constraintViolation.getMessageTemplate());
			errors.rejectValue(propertyPath, constraintViolation.getMessageTemplate(), message);
		}
	}

	public <T> Set<ConstraintViolation<T>> getConstraintViolations(T target) {
		return validator.validate(target);
	}

	public <T> Set<ConstraintViolation<T>> getConstraintViolations(T target, String propertyPath) {
		return validator.validateProperty(target, propertyPath);
	}
}