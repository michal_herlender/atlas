package org.trescal.cwms.core.tools.filebrowser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.tools.EncryptionTools;

import lombok.Data;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.Date;

@Data
public class FileWrapper {
	private String extension;
	private String fileName;
	private String filePath;
	private String highlightColour;
	private boolean isDirectory;
	private Date lastModified;

	private static final Logger logger = LoggerFactory.getLogger(FileWrapper.class);

	public FileWrapper(String fileName, String filePath, Date lastModified, String highlightColour, String extension,
			boolean isDirectory) {
		this.fileName = fileName;
		this.filePath = filePath;
		this.lastModified = lastModified;
		this.highlightColour = highlightColour;
		this.extension = extension;
		this.isDirectory = isDirectory;
	}

	/**
	 * Returns a non-URL encoded, but encrypted file name
	 */
	public String getFileNameEncrypted() {
		try {
			return EncryptionTools.encrypt(this.fileName);
		} catch (Exception ex) {
			logger.error("Error encrypting fileName", ex);
			return "";
		}
	}

	public String getFilePathEncrypted() {
		try {
			return URLEncoder.encode(EncryptionTools.encrypt(this.filePath), "UTF-8");
		} catch (Exception ex) {
			logger.error("Error encrypting filePath", ex);
			return "";
		}
	}
	
	public String getFormattedDate() {
		DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.SHORT,
				LocaleContextHolder.getLocale());
		return dateFormatter.format(this.lastModified);
	}
}