package org.trescal.cwms.core.tools.supportedlocale;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.spring.model.KeyValue;

public interface SupportedLocaleService {
	/*
	 * Returns the primary locale for the application 
	 */
	public Locale getPrimaryLocale();
	/*
	 * Returns a set of all locales supported for the application EXCEPT for the primary locale
	 */
	public List<Locale> getAdditionalLocales();
	/*
	 * Returns a set of all locales supported for the application
	 */
	public List<Locale> getSupportedLocales();
	/*
	 * Returns the best locale for the contact, falling back to the primary locale if no preference has been specified
	 */
	public Locale getBestLocale(Contact contact);
	/**
	 * Returns a list of DTO Key/Value pairs using the toLanguageTag / forLanguage tag convention in the current locale
	 * Note that while Locale can be bound directly by Spring, it isn't Comparable as a Key, so this is preferred
	 * for select lists
	 */
	public List<KeyValue<String,String>> getDTOList(List<Locale> locales, Locale displayLocale);
	/**
	 * Returns a list of DTO Key/Value pairs for all supported locales
	 * using the toLanguageTag / forLanguage tag convention in the current locale
	 * Note that while Locale can be bound directly by Spring, 
	 * it isn't Comparable as a Key, so this is preferred for select lists
	 */
	public List<KeyValue<String,String>> getSupportedDTOList(Locale displayLocale);
	/*
	 * Returns the best matching system supported locale for the given "ideal locale"
	 * e.g. if en or en_US is specified, and only en_GB is supported, returns en_GB
	 * This ensures that only the supported locale is ever used in a session.
	 */
	public Locale getBestSupportedLocale(Locale locale);
	
}
