package org.trescal.cwms.core.tools.transitcalendar;

import java.time.LocalDate;
import java.util.Comparator;

public class ClientTransitComparator implements Comparator<ClientTransit>
{
	public int compare(ClientTransit ct1, ClientTransit ct2) {
		Integer ct1toid = ct1.getClientTransit().getId();
		int ct2toid = ct2.getClientTransit().getId();

		if (ct1toid == ct2toid) {
			LocalDate date1 = ct1.getTransitDate();
			LocalDate date2 = ct2.getTransitDate();

			if (date1.isEqual(date2)) {
				return 1;
			} else {
				return date1.compareTo(date2);
			}
		}
		else
		{
			return ct1toid.compareTo(ct2toid);
		}
	}
}