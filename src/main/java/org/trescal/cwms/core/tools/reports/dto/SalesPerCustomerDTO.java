package org.trescal.cwms.core.tools.reports.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SalesPerCustomerDTO
{
	private String company;
	private BigDecimal sales;

	// this constructor is used by aliasToBean transformers in ReportsDaoImpl
	public SalesPerCustomerDTO() {}
	
	public String getCompany()
	{
		return this.company;
	}

	public BigDecimal getSales()
	{
		return this.sales;
	}

	public void setCompany(String company)
	{
		this.company = company;
	}

	public void setSales(BigDecimal sales)
	{
		this.sales = sales.setScale(2, RoundingMode.HALF_UP);
	}
}