package org.trescal.cwms.core.tools.pathservice;

import java.io.File;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.ServletContextAware;

@Service("PathService")
public class PathServiceImpl implements PathService, ServletContextAware, InitializingBean
{
	static enum FilePathResolution {
		RELATIVE_TO_WORKING_DIRECTORY,
		RELATIVE_TO_USER_DIRECTORY,
		ABSOLUTE,
	}
	
	@Value("#{props['cwms.config.filepathresolution']}")
	private String filePathResolution;
	private FilePathResolution filepaths; 
	@Value("#{props['cwms.config.fileserver.root']}")
	private String fileserverRoot;
	@Value("#{systemProperties['user.home']}")
	private String userHome;
	private ServletContext servletContext;
	
	/*
	 * Resolves actual file server root as determined through configuration attributes
	 * May be relative or absolute file path
	 * It is intended that this should be used for most file storage usage 
	 */
	@Override
	public String getFileServerRootPath() {
		if (filepaths.equals(FilePathResolution.RELATIVE_TO_USER_DIRECTORY)) {
			return userHome.concat(File.separator).concat(fileserverRoot);
		}
		return fileserverRoot;
	}
	/*
	 * Provides a temporary private directory for file generation
	 * It is intended that this be provided by the servlet context
	 */
	public String getTempDirPath() {
		File file = (File)servletContext.getAttribute("javax.servlet.context.tempdir");
		return file.getAbsolutePath();
	}
	
	@Override
	public File getTempDirFile() {
		File file = (File)servletContext.getAttribute("javax.servlet.context.tempdir");
		return file;
	}	
	
	@Override
	public void setServletContext(ServletContext arg0) {
		this.servletContext = arg0;
	}
	@Override
	public void afterPropertiesSet() throws Exception {
		// Spring appears to not be able to set enums directly so converting from string once set
		filepaths = FilePathResolution.valueOf(filePathResolution);		
	}

}