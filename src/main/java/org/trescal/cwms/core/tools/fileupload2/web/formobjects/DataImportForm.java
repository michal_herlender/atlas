package org.trescal.cwms.core.tools.fileupload2.web.formobjects;

import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;

/**
 * This class represents a Data Import form.
 * 
 * @author Gunnar Hillert
 * @author richard
 * @author jamiev
 */
public class DataImportForm extends AbstractFileUploadForm
{
	/**
	 * The id of the entity the file is being uploaded for.
	 */
	private String compEntityId;

	/**
	 * The version of the entity that the file is being uploaded for.
	 */
	private String compEntityVersion;

	/**
	 * The {@link SystemComponent} id that the file is being uploaded for.
	 */
	private String compid;

	/**
	 * The String to rename the file to after upload without extension. Used for
	 * certificates, etc.
	 */
	private String renameTo;

	/**
	 * The subdirectory (if any) that the file should be stored in within the
	 * {@link SystemComponent}
	 */
	private String subdirectory;

	public String getCompEntityId()
	{
		return this.compEntityId;
	}

	public String getCompEntityVersion()
	{
		return this.compEntityVersion;
	}

	public String getCompid()
	{
		return this.compid;
	}

	public String getRenameTo()
	{
		return this.renameTo;
	}

	/**
	 * @return the subdirectory
	 */
	public String getSubdirectory()
	{
		return this.subdirectory;
	}

	/**
	 * @return Returns the file.
	 */

	public void setCompEntityId(String compEntityId)
	{
		this.compEntityId = compEntityId;
	}

	public void setCompEntityVersion(String compEntityVersion)
	{
		this.compEntityVersion = compEntityVersion;
	}

	public void setCompid(String compid)
	{
		this.compid = compid;
	}

	public void setRenameTo(String renameTo)
	{
		this.renameTo = renameTo;
	}

	/**
	 * @param file The file to set.
	 */

	/**
	 * @param subdirectory the subdirectory to set
	 */
	public void setSubdirectory(String subdirectory)
	{
		this.subdirectory = subdirectory;
	}
}
