package org.trescal.cwms.core.tools;

import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.beans.Beans;
import java.io.File;

import javax.imageio.ImageIO;

import com.topaz.sigplus.SigPlus;
import com.topaz.sigplus.SigPlusEvent0;
import com.topaz.sigplus.SigPlusListener;

import gnu.io.RXTXCommDriver;

/**
 * Signature capture stand-alone application that has a main method that can be
 * used for testing the main signing functionality. The application can read the
 * input from a Topaz device and see the pen strokes in real-time on screen. The
 * result can then be saved as a JPEG image.
 * 
 * @author jamiev
 */
@SuppressWarnings("serial")
public class SignatureCapture extends Frame implements Runnable
{
	private static final int PADDING_X = 10;
	private static final int PADDING_Y = 10;
	private static final double SCALE_Y = 200;

	public static void main(String Args[])
	{
		SignatureCapture demo = new SignatureCapture("C:", "20001/10/A");
		System.out.println("Device connected: "
				+ demo.tabletConnectionIsAvailable());
		demo.openCaptureWindow();
	}

	private String deliveryNo;
	private String deliveryPath;
	private Thread eventThread;
	private boolean readyToClose = false;
	private SigPlus sigObj = null;

	public SignatureCapture(String deliveryPath, String deliveryNo)
	{
		this.deliveryPath = deliveryPath;
		this.deliveryNo = deliveryNo;

		try
		{
			ClassLoader cl = (com.topaz.sigplus.SigPlus.class).getClassLoader();
			this.sigObj = (SigPlus) Beans.instantiate(cl, "com.topaz.sigplus.SigPlus");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		if (this.sigObj != null)
		{
			this.sigObj.setTabletModel("SignatureGem4X5");
			this.sigObj.setTabletComPort("HID1");
			SignatureCapture.this.sigObj.setTabletComTest(true);

			String drivername = "gnu.io.RXTXCommDriver";
			// String drivername = "com.sun.comm.Win32Driver";
			try
			{
				RXTXCommDriver driver = (RXTXCommDriver) Class.forName(drivername).newInstance();
				// CommDriver driver = (CommDriver)
				// Class.forName(drivername).newInstance();
				driver.initialize();
			}
			// this must be Throwable so that it catches errors (of which
			// UnsatisfiedLinkError is a child class) as well as exceptions
			catch (Throwable e)
			{
				e.printStackTrace();
			}
		}
	}

	private boolean connectToTablet()
	{
		if (this.sigObj != null)
		{
			SignatureCapture.this.sigObj.setTabletState(0);

			try
			{
				SignatureCapture.this.sigObj.setTabletState(1);
				boolean poll1 = (SignatureCapture.this.sigObj.getTabletState() == 1) ? true : false;
				SignatureCapture.this.sigObj.setTabletState(1);
				boolean poll2 = (SignatureCapture.this.sigObj.getTabletState() == 1) ? true : false;

				return (poll1 && poll2) ? true : false;
			}
			catch (UnsatisfiedLinkError e)
			{
				e.printStackTrace();
				return false;
			}
			catch (NoClassDefFoundError e)
			{
				e.printStackTrace();
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public void openCaptureWindow()
	{
		String safeDeliveryNo = this.deliveryNo.replaceAll("/", "-");
		final String sigImgPath = this.deliveryPath.concat(File.separator).concat(safeDeliveryNo).concat("-sig").concat(".jpg");

		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gc = new GridBagConstraints();
		this.setLayout(gbl);
		Panel controlPanel = new Panel();
		this.setConstraints(controlPanel, gbl, gc, 0, 0, GridBagConstraints.REMAINDER, 1, 0, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, 0, 0, 0, 0);
		this.add(controlPanel, gc);

		Button clearButton = new Button("CLEAR");
		controlPanel.add(clearButton);

		Button saveJpgButton = new Button("SAVE SIGNATURE");
		controlPanel.add(saveJpgButton);

		Button okButton = new Button("CANCEL");
		controlPanel.add(okButton);

		try
		{
			if ((this.sigObj != null) && this.connectToTablet())
			{
				this.setConstraints(this.sigObj, gbl, gc, 0, 1, GridBagConstraints.REMAINDER, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, 5, 0, 5, 0);
				this.add(this.sigObj, gc);
				this.sigObj.setSize(100, 100);
				this.sigObj.clearTablet();
				this.setTitle("CWMS Electronic Signature Capture for Delivery No. "
						+ this.deliveryNo);

				okButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						SignatureCapture.this.sigObj.setTabletState(0);
						SignatureCapture.this.readyToClose = true;
					}
				});

				clearButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						SignatureCapture.this.sigObj.clearTablet();
					}
				});

				saveJpgButton.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						try
						{
							SignatureCapture.this.sigObj.setTabletState(0);
							SignatureCapture.this.sigObj.setImageJustifyMode(5);
							SignatureCapture.this.sigObj.setImagePenWidth(2);
							SignatureCapture.this.sigObj.setImageJustifyX(PADDING_X);
							SignatureCapture.this.sigObj.setImageJustifyY(PADDING_Y);

							int originalX = SignatureCapture.this.sigObj.getXExtent();
							int originalY = SignatureCapture.this.sigObj.getYExtent();

							double scaleX = SCALE_Y / originalY;
							int finalX = ((Double) (scaleX * originalX)).intValue()
									+ PADDING_X;
							int finalY = ((Double) SCALE_Y).intValue()
									+ PADDING_Y;

							SignatureCapture.this.sigObj.setImageXSize(finalX);
							SignatureCapture.this.sigObj.setImageYSize(finalY);

							BufferedImage sigImage = SignatureCapture.this.sigObj.sigImage();
							int w = sigImage.getWidth(null);
							int h = sigImage.getHeight(null);
							int[] pixels = new int[(w * h) * 2];

							sigImage.setRGB(0, 0, 0, 0, pixels, 0, 0);

							
							
							ImageIO.write( sigImage, "jpeg", new File ( sigImgPath ) );
							
							SignatureCapture.this.readyToClose = true;
						}
						catch (Throwable th)
						{
							th.printStackTrace();
						}
					}
				});

				this.addWindowListener(new WindowAdapter()
				{
					@Override
					public void windowClosed(WindowEvent we)
					{
						SignatureCapture.this.readyToClose = true;
					}

					@Override
					public void windowClosing(WindowEvent we)
					{
						SignatureCapture.this.sigObj.setTabletState(0);
						SignatureCapture.this.readyToClose = true;
					}
				});

				this.sigObj.addSigPlusListener(new SigPlusListener()
				{
					public void handleKeyPadData(SigPlusEvent0 evt)
					{
					}

					public void handleNewTabletData(SigPlusEvent0 evt)
					{
					}

					public void handleTabletTimerEvent(SigPlusEvent0 evt)
					{
					}
				});

				this.eventThread = new Thread(this);
				this.eventThread.start();

				this.setSize(750, 600);
				this.setVisible(true);
				this.setBackground(Color.lightGray);
			}
			else
			{
				SignatureCapture.this.sigObj.setTabletState(0);
				System.out.println("Not started - no connection!");
				SignatureCapture.this.readyToClose = true;
			}
		}
		catch (Exception e)
		{
			return;
		}
	}

	public void run()
	{
		try
		{
			while (!this.readyToClose)
			{
				Thread.sleep(100);
			}

			this.dispose();
		}
		catch (InterruptedException e)
		{
		}
	}

	// Convenience method for GridBagLayout
	private void setConstraints(Component comp, GridBagLayout gbl, GridBagConstraints gc, int gridx, int gridy, int gridwidth, int gridheight, int weightx, int weighty, int anchor, int fill, int top, int left, int bottom, int right)
	{
		gc.gridx = gridx;
		gc.gridy = gridy;
		gc.gridwidth = gridwidth;
		gc.gridheight = gridheight;
		gc.weightx = weightx;
		gc.weighty = weighty;
		gc.anchor = anchor;
		gc.fill = fill;
		gc.insets = new Insets(top, left, bottom, right);
		gbl.setConstraints(comp, gc);
	}

	public boolean tabletConnectionIsAvailable()
	{
		if (this.sigObj != null)
		{
			boolean connected = this.connectToTablet();
			this.sigObj.setTabletState(0);
			return connected;
		}
		else
		{
			return false;
		}
	}
}
