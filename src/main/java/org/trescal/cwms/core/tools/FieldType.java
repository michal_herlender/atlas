package org.trescal.cwms.core.tools;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public enum FieldType {
	STRING("flexiblefieldtype.string"),
	NUMERIC("flexiblefieldtype.numeric"),
	BOOLEAN("flexiblefieldtype.boolean"),
	DATETIME("flexiblefieldtype.date"),
	SELECTION("flexiblefieldtype.selection");
	
	private final String nameCode;
	private MessageSource messageSource;
	
	private FieldType(String nameCode){
		this.nameCode = nameCode;
	}
	
	@Component
    public static class FieldTypeMessageSourceInjector
    {
        @Autowired
        private MessageSource messageSource;

        @PostConstruct
        public void postConstruct() {
            for (FieldType fieldType : EnumSet.allOf(FieldType.class)) {
                fieldType.setMessageSource(messageSource);
            }
        }
    }
	
	 public void setMessageSource(MessageSource messageSource) {
	        this.messageSource = messageSource;
	 }
	
	public String getName() {
        return messageSource.getMessage(nameCode, null, LocaleContextHolder.getLocale());
    }
}
