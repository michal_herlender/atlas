package org.trescal.cwms.core.tools.symboliclink;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.tools.DateTools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component("CreateSymbolicLinkTool")
public class CreateSymbolicLinkTool {
	@Value("${cwms.admin.email}")
	private String adminEmail;
	@Autowired
	private EmailService emailService;
	@Value("${cwms.config.symboliclinks.createatstartup}")
	boolean enabled;
	/**
	 * {@link List} of {@link String} arrays where each array position 0 is the
	 * source file (the file to create as a junction) and position is the file
	 * name for the directory to junction to.
	 * 
	 * TODO for resurrection - reassemble from (via individual properties?)
			<list>
				<list>
					<value>${cwms.config.symboliclinks.systemimages.src}</value>
					<value>${cwms.config.symboliclinks.systemimages.dest}</value>
				</list>
				<list>
					<value>${cwms.config.symboliclinks.webcamimages.src}</value>
					<value>${cwms.config.symboliclinks.webcamimages.dest}</value>
				</list>
				<list>
					<value>${cwms.config.symboliclinks.procedures.src}</value>
					<value>${cwms.config.symboliclinks.procedures.dest}</value>
				</list>
			</list>
	 */
	private List<String[]> links;

	public static final String BATCH_FILE_EXT = ".bat";
	public static final String BATCH_FILE_NAME = "cwmssymbolic";

	public static void main(String[] args) {
		ClassPathXmlApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext-jdbc.xml", "applicationContext-dao.xml", "applicationContext-service.xml", "applicationContext-acegi-security.xml", "applicationContext-schedule.xml", "applicationContext-html.xml", "applicationContext-ws.xml", "applicationContext-intercept-antech.xml");

		CreateSymbolicLinkTool csTool = (CreateSymbolicLinkTool) ((BeanFactory) appContext).getBean("CreateSymbolicLinkTool");
		csTool.setEnabled(true);

		csTool.links = new ArrayList<>();

		String[] links = new String[2];
		links[0] = "\\\\IT1\\c$\\temp\\source";
		links[1] = "\\\\IT1\\c$\\temp\\destination";
		csTool.links.add(links);

		csTool.createLinks();

		appContext.close();
		// call exit or Spring will just sit and BE.
		System.exit(0);
	}

	public int count = 0;


	// this is just a default value for unit tests, will be overwritten in real
	// executions
	public String fullBatchFile = "cwmssymbolic.bat";

	private final Log logger = LogFactory.getLog(this.getClass());

	public void createLinks()
	{
		if (this.isEnabled()) {
			List<String[]> failedLinks = new ArrayList<>();
			boolean massiveFailure = false;

			try {
				for (String[] l : this.links) {
					// we have to generate a new batch file name each time
					// because i think even after deletion, the server still has
					// a handle on the file name preventing others with the same
					// name from being created
					this.count++;
					this.fullBatchFile = BATCH_FILE_NAME.concat(String.valueOf(this.count)).concat(BATCH_FILE_EXT);

					boolean outCome = false;
					if ((l[0] != null) && !l[0].equals("") && (l[1] != null)
							&& !l[1].equals(""))
					{
						outCome = this.runBatchFile(this.writeBatchFile(l[0], l[1]));
					}
					if (!outCome)
					{
						failedLinks.add(l);
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				massiveFailure = true;
			}

			if (massiveFailure) {

				String mailBody = "The symbolic link creation task on " + this.getComputerName() + " has encountered what I like to call a \"massive failure\"! <br/><br/>" +
					"Please check the logs to see what happened and to determine whether any links need creating manually.<br/>" +
					"<br/>Timestamp: " + DateTools.dtf.format(new Date());
				this.emailService.sendBasicEmail("Create Symbolic Links at startup has completely tanked ", this.adminEmail, this.adminEmail, mailBody);
			}
			else if (failedLinks.size() > 0) {
				StringBuilder mailBody = new StringBuilder();
				mailBody.append("The symbolic link creation task on ").append(this.getComputerName()).append(" failed for one or more links: <br/><br/>");
				for (String[] f : failedLinks) {
					mailBody.append(f[0]).append(" linking to ").append(f[1]).append(" <br/>");
				}
				mailBody.append("<br/>Timestamp: ").append(DateTools.dtf.format(new Date()));

				this.emailService.sendBasicEmail("Create Symbolic Links at startup has failed ", this.adminEmail, this.adminEmail, mailBody.toString());
			}
		}
		else
		{
			this.logger.info("Symbolic link creation is disabled");
		}
	}

	/**
	 * Returns the name of the computer this code is running on - for logging
	 * purposes only.
	 * 
	 * @return String.
	 */
	public String getComputerName()
	{
		String computerName = "";
		try
		{
			computerName = InetAddress.getLocalHost().getHostName();
		}
		catch (UnknownHostException e)
		{
			e.printStackTrace();
		}
		return computerName;
	}

	public boolean isEnabled()
	{
		return this.enabled;
	}

	public boolean runBatchFile(File bFile) {
		boolean outCome;
		try {
			Process p = Runtime.getRuntime().exec(this.fullBatchFile);
			p.waitFor();
			int result = p.exitValue();
			if (result > 0) {
				this.logger.info("Failed creating symbolic links " + result);
				outCome = false;
			}
			else
			{
				this.logger.info("Created Symbolic Links");
				outCome = true;
			}
		}
		catch (Exception e)
		{
			// catch the exception here - don't let it take down the app
			e.printStackTrace();
			outCome = false;
		}
		finally
		{
			// remove the batch file regardless of the outcome
			boolean t = bFile.delete();
			this.logger.info("Tried to delete " + this.fullBatchFile
					+ " : successful = " + t);
		}
		return outCome;
	}

	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}

	public void setLinks(List<String[]> links)
	{
		this.links = links;
	}

	public File writeBatchFile(String srcPath, String destPath)
	{
		// it is assumed that the src path is always somewhere in the webapp
		// and therefore relative to the webapp root - use path service to get
		// the real path, UNLESS the srcPath begins with a double backslash and
		// in this case it is assumed the path is a shared folder and therefore
		// not within the webapp root
		// Commenting this out 2015-07-30 GB due to path service replacement
		// This tool might never be used again?
		// It seems a ridiculous way to solve a problem (whatever it is!)

		// delete the srcPath - mlink doesn't seem to be able to create a
		// junction on an existing folder
		File srcFile = new File(srcPath);
		if (srcFile.exists())
		{
			boolean t = srcFile.delete();
			this.logger.info("Tried to delete " + srcPath + " : successful = "
					+ t);
		}

		File bFile = new File(this.fullBatchFile);
		if (bFile.exists())
		{
			boolean t = bFile.delete();
			this.logger.info("Tried to delete " + this.fullBatchFile
					+ " : successful = " + t);
		}

		String fileContents = "mklink /d \"" + srcPath + "\" \"" + destPath
				+ "\"";

		BufferedWriter bOut = null;
		FileWriter fOut = null;
		try
		{
			fOut = new FileWriter(bFile);
			bOut = new BufferedWriter(fOut);
			bOut.write(fileContents);
			this.logger.info("Wrote symbolic link file: " + fileContents);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try {
				assert bOut != null;
				bOut.close();
				fOut.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}

		return bFile;
	}
}
