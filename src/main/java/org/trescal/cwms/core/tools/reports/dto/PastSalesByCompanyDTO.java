package org.trescal.cwms.core.tools.reports.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.TreeMap;

import org.trescal.cwms.core.tools.MathTools;

public class PastSalesByCompanyDTO
{
	private Map<String, BigDecimal> breakdown;
	String currencySymbol;
	private String label;
	private Double pcNettChange;
	private BigDecimal totalNett;

	public PastSalesByCompanyDTO(String label, String currencySymbol)
	{
		this.label = label;
		this.currencySymbol = currencySymbol;
		this.breakdown = new TreeMap<String, BigDecimal>();
	}

	public void calculateNettChange(BigDecimal originalNett)
	{
		if (this.totalNett.compareTo(new BigDecimal("0.00")) != 0)
		{
			BigDecimal percentage = ((originalNett.subtract(this.totalNett)).divide(this.totalNett, MathTools.mc)).multiply(new BigDecimal("100.00"), MathTools.mc);
			this.pcNettChange = percentage.doubleValue();
		}
	}

	public Map<String, BigDecimal> getBreakdown()
	{
		return this.breakdown;
	}

	public String getFormattedPcNettChange()
	{
		return (this.pcNettChange == null) ? "" : this.pcNettChange + "%";
	}

	public String getFormattedTotalNett()
	{
		return totalNett.setScale(2, RoundingMode.HALF_UP) + " " + currencySymbol;
	}

	public String getLabel()
	{
		return this.label;
	}

	public Double getPcNettChange()
	{
		return this.pcNettChange;
	}

	public BigDecimal getTotalNett()
	{
		return this.totalNett;
	}

	public void setBreakdown(Map<String, BigDecimal> breakdown)
	{
		breakdown.forEach((k,v) -> v.setScale(2, RoundingMode.HALF_UP)); 
		this.breakdown = breakdown;
	}

	public void setLabel(String label)
	{
		this.label = label;
	}

	public void setPcNettChange(Double pcNettChange)
	{
		this.pcNettChange = pcNettChange;
	}

	public void setTotalNett(BigDecimal totalNett)
	{
		this.totalNett = totalNett.setScale(2, RoundingMode.HALF_UP);
	}
}
