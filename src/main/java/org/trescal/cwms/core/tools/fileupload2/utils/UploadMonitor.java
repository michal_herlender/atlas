/* Licence:
 *   Use this however/wherever you like, just don't blame me if it breaks anything.
 *
 * Credit:
 *   If you're nice, you'll leave this bit:
 *
 *   Class by Pierre-Alexandre Losson -- http://www.telio.be/blog
 *   email : plosson@users.sourceforge.net
 */
package org.trescal.cwms.core.tools.fileupload2.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContextFactory;

/**
 * DWR service that makes an {@link HttpSession} attribute 'uploadInfo'
 * available to dwr.
 */
public class UploadMonitor
{
	public UploadInfo getUploadInfo()
	{
		HttpServletRequest req = WebContextFactory.get().getHttpServletRequest();

		if (req.getSession().getAttribute("uploadInfo") != null)
		{
			return (UploadInfo) req.getSession().getAttribute("uploadInfo");
		}
		else
		{
			return new UploadInfo();
		}
	}
}
