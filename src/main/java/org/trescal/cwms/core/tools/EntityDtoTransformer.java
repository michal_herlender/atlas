package org.trescal.cwms.core.tools;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

public interface EntityDtoTransformer<ENTITY, DTO extends Serializable> {

    ENTITY transform(DTO dto);

    DTO transform(ENTITY entity);

    default List<DTO> convertList(List<ENTITY> entities) {
        return entities.stream().map(this::transform).collect(Collectors.toList());
    }

}
