package org.trescal.cwms.core.tools.labelprinting.certificate;

import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;

/**
 * DTO class that encapsulates settings for a particular CertLink (Job Item + Certificate) to be printed
 * Galen Beck - 2016-05-19
 */
public class CertificateLabelSettings {
	private boolean accredited;
	private AccreditedLab accreditedLab;
	private boolean labelCertificateNumber;
	private boolean labelPlantNumber;
	private String labelRecallDate;
	
	public CertificateLabelSettings(boolean accredited, AccreditedLab accreditedLab, boolean labelCertificateNumber, boolean labelPlantNumber, String labelRecallDate) {
		this.accredited = accredited;
		this.accreditedLab = accreditedLab;
		this.labelCertificateNumber = labelCertificateNumber;
		this.labelPlantNumber = labelPlantNumber;
		this.labelRecallDate = labelRecallDate;
	}

	public boolean isAccredited() {
		return accredited;
	}

	public AccreditedLab getAccreditedLab() {
		return accreditedLab;
	}

	public boolean isLabelCertificateNumber() {
		return labelCertificateNumber;
	}

	public boolean isLabelPlantNumber() {
		return labelPlantNumber;
	}

	public String getLabelRecallDate() {
		return labelRecallDate;
	}
}
