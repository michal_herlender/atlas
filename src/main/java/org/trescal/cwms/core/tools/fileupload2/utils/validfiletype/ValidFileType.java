package org.trescal.cwms.core.tools.fileupload2.utils.validfiletype;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;


@Entity
@Table(name = "validfiletype", uniqueConstraints = { @UniqueConstraint(columnNames = { "extension", "usagetype" }) })
public class ValidFileType
{
	public enum FileTypeUsage
	{
		GENERAL, IMAGE
	};

	private String extension;
	private int fileTypeId;
	private FileTypeUsage usage;

	@Length(min = 1, max = 6)
	@Column(name = "extension", length = 6, nullable = false)
	public String getExtension()
	{
		return this.extension;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "filetypeid", nullable = false, unique = true)
	@Type(type = "int")
	public int getFileTypeId()
	{
		return this.fileTypeId;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "usagetype", nullable = false)
	public FileTypeUsage getUsage()
	{
		return this.usage;
	}

	public void setExtension(String extension)
	{
		this.extension = extension;
	}

	public void setFileTypeId(int fileTypeId)
	{
		this.fileTypeId = fileTypeId;
	}

	public void setUsage(FileTypeUsage usage)
	{
		this.usage = usage;
	}
}
