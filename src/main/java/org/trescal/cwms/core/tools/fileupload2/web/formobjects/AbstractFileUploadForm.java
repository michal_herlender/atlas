package org.trescal.cwms.core.tools.fileupload2.web.formobjects;

import org.springframework.web.multipart.MultipartFile;

public abstract class AbstractFileUploadForm
{
	private MultipartFile file;

	public MultipartFile getFile()
	{
		return this.file;
	}

	public void setFile(final MultipartFile file)
	{
		this.file = file;
	}

}
