package org.trescal.cwms.core.tools.fileupload2.utils.validfiletype.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.tools.fileupload2.utils.validfiletype.ValidFileType;
import org.trescal.cwms.core.tools.fileupload2.utils.validfiletype.ValidFileType_;

@Repository("ValidFileTypeDao")
public class ValidFileTypeDaoImpl extends BaseDaoImpl<ValidFileType, Integer> implements ValidFileTypeDao
{
	@Override
	protected Class<ValidFileType> getEntity() {
		return ValidFileType.class;
	}
	
	public boolean isValidFileType(String extension, ValidFileType.FileTypeUsage usage) {
		return getResultList(cb->{
			CriteriaQuery<ValidFileType> cq = cb.createQuery(ValidFileType.class);
			Root<ValidFileType> validFileType = cq.from(ValidFileType.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(validFileType.get(ValidFileType_.extension), extension));
			clauses.getExpressions().add(cb.equal(validFileType.get(ValidFileType_.usage), usage));
			cq.where(clauses);
			return cq;
		}).size() >0;
	}
}