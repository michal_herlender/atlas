package org.trescal.cwms.core.tools.labelprinting.alligator.xml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="printJob")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder={"result", "labels","templates","media","printer"})
public class XmlAlligatorPrintJob {
	
	@XmlElement
	private XmlAlligatorResult result;

	@XmlElementWrapper(name="labels")
	@XmlElement(name="label")
	private List<XmlAlligatorLabel> labels;
	
	@XmlElementWrapper(name="templates")
	@XmlElement(name="template")
	private List<XmlAlligatorTemplate> templates;
	
	@XmlElement
	private XmlAlligatorMedia media;

	@XmlElement
	private XmlAlligatorPrinter printer;
	
	public XmlAlligatorPrintJob() {
		// Default constructor for serialization
	}
	
	public XmlAlligatorPrintJob(boolean success, String message) {
		this.result = new XmlAlligatorResult(success, message);
		this.labels = new ArrayList<>();
		this.templates = new ArrayList<>();
	}

	public XmlAlligatorResult getResult() {
		return result;
	}

	public List<XmlAlligatorLabel> getLabels() {
		return labels;
	}

	public List<XmlAlligatorTemplate> getTemplates() {
		return templates;
	}

	public XmlAlligatorMedia getMedia() {
		return media;
	}

	public XmlAlligatorPrinter getPrinter() {
		return printer;
	}

	public void setResult(XmlAlligatorResult result) {
		this.result = result;
	}

	public void setLabels(List<XmlAlligatorLabel> labels) {
		this.labels = labels;
	}

	public void setTemplates(List<XmlAlligatorTemplate> templates) {
		this.templates = templates;
	}

	public void setMedia(XmlAlligatorMedia media) {
		this.media = media;
	}

	public void setPrinter(XmlAlligatorPrinter printer) {
		this.printer = printer;
	}
}