package org.trescal.cwms.core.tools;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordTools
{
	public static Integer getPasswordStrength(String password)
	{
		/**
		 * Currently using lite version because people are complaining that
		 * passwords need to be too complex in comparison to old system
		 */
		return getPasswordStrength_Lite(password);
	}

	public static Integer getPasswordStrength_Hardcore(String password)
	{
		if (password != null)
		{
			int score = 0;

			// if password bigger/equal than 8 characters give 1 point
			if (password.length() >= 8)
			{
				score++;
			}

			// if password has both lower and upper-case characters give 1 point
			Pattern pattern = Pattern.compile("[a-z]");
			Matcher matcher = pattern.matcher(password);
			Pattern patternUpper = Pattern.compile("[A-Z]");
			Matcher matcherUpper = patternUpper.matcher(password);
			if (matcher.find() && matcherUpper.find())
			{
				score++;
			}

			// if password has at least one number give 1 point
			pattern = Pattern.compile("[0-9]");
			matcher = pattern.matcher(password);
			if (matcher.find())
			{
				score++;
			}

			// if password has at least one special character give 1 point
			pattern = Pattern.compile("[/,+=!@ \\[#$-]");
			matcher = pattern.matcher(password);
			if (matcher.find())
			{
				score++;
			}

			// if password bigger/equal than 14 characters give another 1 point
			if (password.length() >= 14)
			{
				score++;
			}

			return score;
		}
		else
		{
			return null;
		}
	}

	public static Integer getPasswordStrength_Lite(String password)
	{
		if (password != null)
		{
			int score = 0;

			// if password bigger/equal than 6 characters give 1 point
			if (password.length() >= 6)
			{
				score += 2;

				// if password has both lower and upper-case characters give 1
				// point
				Pattern pattern = Pattern.compile("[a-z]");
				Matcher matcher = pattern.matcher(password);
				Pattern patternUpper = Pattern.compile("[A-Z]");
				Matcher matcherUpper = patternUpper.matcher(password);
				if (matcher.find() && matcherUpper.find())
				{
					score++;
				}

				// if password has at least one special character give 1 point
				pattern = Pattern.compile("[/,+=!@ \\[#$-]");
				matcher = pattern.matcher(password);
				if (matcher.find())
				{
					score++;
				}

				// if password has at least one number give 2 points
				pattern = Pattern.compile("[0-9]");
				matcher = pattern.matcher(password);
				if (matcher.find())
				{
					score++;
				}
			}

			return score;
		}
		else
		{
			return null;
		}
	}
}
