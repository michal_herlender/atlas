package org.trescal.cwms.core.tools.reports.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.reports.dto.ItemsAtTPWrapper;
import org.trescal.cwms.core.workflow.view.SubcontractedJobItemsXlsxView;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class ItemsAtThirdPartyController
{	
	
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SubcontractedJobItemsXlsxView subcontractedJobItemsXlsxView;
	
	@RequestMapping(value="/itemsattp.htm")
	protected ModelAndView handleRequestInternal(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception
	{
		return new ModelAndView("trescal/core/tools/reports/itemsatthirdparty");
	}
	
	@RequestMapping(value = "/subcontractedJobItemsexport.xlsx", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView exportData(Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@RequestParam(value = "subdivId", required = false) Integer subdivId){
		Map<String, Object> model = new HashMap<String, Object>();
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		List<ItemsAtTPWrapper> ItemsatTP = this.jiServ.getItemsAtThirdParty(allocatedSubdiv);
		model.put("itemsAtTPWrapper",ItemsatTP);
		List<Integer> subdivids = ItemsatTP.stream().map(ItemsAtTPWrapper::getSubdivId).collect(Collectors.toList());
		model.put("itemAtTP", this.jiServ.getAllItemsAtSpecificThirdParty(subdivids, subdivDto.getKey(), locale));
		return new ModelAndView(subcontractedJobItemsXlsxView, model);
	}
}