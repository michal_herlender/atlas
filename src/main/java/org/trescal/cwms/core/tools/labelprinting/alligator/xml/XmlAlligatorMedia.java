package org.trescal.cwms.core.tools.labelprinting.alligator.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"name","width","height","rotationAngle","gap","paddingLeft","paddingTop","paddingRight","paddingBottom"})
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlAlligatorMedia {
	@XmlElement
	private String name;
	@XmlElement
	private Double width;
	@XmlElement	
	private Double height;
	@XmlElement	
	private Double rotationAngle;
	@XmlElement	
	private Double gap;
	@XmlElement	
	private Double paddingLeft;
	@XmlElement	
	private Double paddingTop;
	@XmlElement	
	private Double paddingRight;
	@XmlElement	
	private Double paddingBottom;
	
	public String getName() {
		return name;
	}
	public Double getWidth() {
		return width;
	}
	public Double getHeight() {
		return height;
	}
	public Double getRotationAngle() {
		return rotationAngle;
	}
	public Double getGap() {
		return gap;
	}
	public Double getPaddingLeft() {
		return paddingLeft;
	}
	public Double getPaddingTop() {
		return paddingTop;
	}
	public Double getPaddingRight() {
		return paddingRight;
	}
	public Double getPaddingBottom() {
		return paddingBottom;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setWidth(Double width) {
		this.width = width;
	}
	public void setHeight(Double height) {
		this.height = height;
	}
	public void setRotationAngle(Double rotationAngle) {
		this.rotationAngle = rotationAngle;
	}
	public void setGap(Double gap) {
		this.gap = gap;
	}
	public void setPaddingLeft(Double paddingLeft) {
		this.paddingLeft = paddingLeft;
	}
	public void setPaddingTop(Double paddingTop) {
		this.paddingTop = paddingTop;
	}
	public void setPaddingRight(Double paddingRight) {
		this.paddingRight = paddingRight;
	}
	public void setPaddingBottom(Double paddingBottom) {
		this.paddingBottom = paddingBottom;
	}	
}
