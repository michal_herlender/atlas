package org.trescal.cwms.core.tools.filebrowser;

import java.io.File;
import java.util.List;

import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;

/**
 * Contains initialized properties pertaining to a particular file browsing session
 * for a specific entity 
 * 
 * Externalized from FileBrowserServiceImpl as it was not threadsafe
 * 
 * @author galen
 *
 */
public class FileBrowserProperties {
	public FileBrowserProperties(ComponentEntity entity, int entityId, SystemComponent sc, File directoryForEntity, List<String> newFiles) {
		super();
		this.entity = entity;
		this.entityId = entityId;
		this.sc = sc;
		this.directoryForEntity = directoryForEntity;
		this.newFiles = newFiles;
	}
	
	private ComponentEntity entity;
	private int entityId;
	private SystemComponent sc;
	private File directoryForEntity;
	private List<String> newFiles;
	
	public ComponentEntity getEntity() {
		return entity;
	}
	public int getEntityId() {
		return entityId;
	}
	public SystemComponent getSc() {
		return sc;
	}
	public File getDirectoryForEntity() {
		return directoryForEntity;
	}
	public void setDirectoryForEntity(File directoryForEntity) {
		this.directoryForEntity = directoryForEntity;
	}
	public List<String> getNewFiles() {
		return newFiles;
	}
	public void setNewFiles(List<String> newFiles) {
		this.newFiles = newFiles;
	}
}