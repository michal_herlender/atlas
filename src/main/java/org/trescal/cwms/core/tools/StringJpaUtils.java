package org.trescal.cwms.core.tools;

import lombok.AllArgsConstructor;
import org.hibernate.criterion.MatchMode;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public class StringJpaUtils {

    public static Predicate notIlike(CriteriaBuilder cb, Path<String> path, String pattern) {

        return cb.notLike(cb.lower(path), pattern);

    }

    public static  Predicate ilike(CriteriaBuilder cb, Expression<String> path, String pattern, MatchMode matchMode) {

        return ilike(cb, path, matchMode.toMatchString(pattern));

    }

    public static Predicate ilike(CriteriaBuilder cb, Expression<String> path, String pattern) {

        return cb.like(cb.lower(path), pattern.toLowerCase());

    }

    public static Stream<Predicate> ilikePredicateStream(CriteriaBuilder cb, Expression<String> path, String text, MatchMode matchMode){
        if (!checkIfNullOrEmpty(text))

            return Stream.of(ilike(cb, path, text, matchMode));

        return Stream.empty();
    }

    public static List<Predicate> ilikePredicateList(CriteriaBuilder cb, Expression<String> path, String text, MatchMode matchMode) {

        if (!checkIfNullOrEmpty(text))

            return Collections.singletonList(ilike(cb, path, text, matchMode));

        return Collections.emptyList();

    }

    public static Boolean checkIfNullOrEmpty(String string) {

        return string == null || string.trim().isEmpty();

    }

    public static Stream<Predicate> termInOneOfTwoFieldPredicate(CriteriaBuilder cb, Path<String> path1, Path<String> path2, String term) {
        if (checkIfNullOrEmpty(term)) return Stream.empty();
        return Stream.of(cb.or(
                ilike(cb, path1, term, MatchMode.ANYWHERE),
                ilike(cb, path2, term, MatchMode.ANYWHERE)
        ));
    }

    public static Concatenator trimAndConcatWithWhitespace(Expression<String> exp1,
                                                                                      Expression<String> exp2) {
        return new Concatenator(cb -> cb.concat(cb.concat(cb.trim(exp1), " "), cb.trim(exp2)));
    }

    public static Concatenator trimAndConcatWithSeparator(Expression<String> exp1,
                                                                                           Expression<String> exp2,
                                                                                           String separator) {
        return new Concatenator(cb -> cb.concat(cb.concat(cb.trim(exp1), separator), cb.trim(exp2)));
    }

    @AllArgsConstructor
    public static class Concatenator implements Function<CriteriaBuilder, Expression<String>> {
        private final Function<CriteriaBuilder, Expression<String>> expression;

        public Concatenator append(Expression<String> appendant) {
            return new Concatenator(cb -> cb.concat(cb.concat(cb.trim(expression.apply(cb)), " "), cb.trim(appendant)));
        }

        public Concatenator appendWithSeparator(Expression<String> appendant, String separator) {
            return new Concatenator(cb -> cb.concat(cb.concat(cb.trim(expression.apply(cb)), separator), cb.trim(appendant)));
        }


        @Override
        public Expression<String> apply(CriteriaBuilder criteriaBuilder) {
            return expression.apply(criteriaBuilder);
        }
    }


    public static Concatenator contactFullName(Path<Contact> contact) {
        return trimAndConcatWithWhitespace(contact.get(Contact_.firstName), contact.get(Contact_.lastName));
    }
}

