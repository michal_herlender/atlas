package org.trescal.cwms.core.tools.labelprinting.alligator.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public abstract class XmlAlligatorElement {

	@XmlAttribute
	private Integer ypos;
	@XmlAttribute
	private Integer xpos;
	@XmlAttribute
	private Integer height;
	@XmlAttribute
	private Integer width;
	@XmlAttribute
	private Integer rotationAngle;

	public Integer getYpos() {
		return ypos;
	}

	public Integer getXpos() {
		return xpos;
	}

	public Integer getHeight() {
		return height;
	}

	public Integer getWidth() {
		return width;
	}

	public Integer getRotationAngle() {
		return rotationAngle;
	}

	public void setYpos(Integer ypos) {
		this.ypos = ypos;
	}

	public void setXpos(Integer xpos) {
		this.xpos = xpos;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public void setRotationAngle(Integer rotationAngle) {
		this.rotationAngle = rotationAngle;
	}

}
