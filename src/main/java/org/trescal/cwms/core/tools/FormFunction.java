package org.trescal.cwms.core.tools;

public enum FormFunction
{
	CREATE, EDIT, DELETE;
}
