package org.trescal.cwms.core.tools;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.Set;

public class CriteriaApiTools {

	/*
	 * Count all results from a given Criteria Query T : return type R : target
	 * entity (root)
	 */
	public static <T, R> long count(final EntityManager entityManager, final CriteriaQuery<T> cq, Root<R> addressRoot) {
		CriteriaQuery<Long> query = createCountQuery(entityManager.getCriteriaBuilder(), cq, addressRoot);
		TypedQuery<Long> queryCount = entityManager.createQuery(query);
		return queryCount.getSingleResult();
	}

	private static <T, R> CriteriaQuery<Long> createCountQuery(final CriteriaBuilder cb,
			final CriteriaQuery<T> criteria, final Root<R> root) {
		final CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
		final Root<R> countRoot = countQuery.from(root.getModel());

		// counter used to name aliases
		Integer counter = 0;
		doJoins(root.getJoins(), countRoot, counter);
		doJoinsOnFetches(root.getFetches(), countRoot, counter);

		countQuery.select(cb.count(countRoot));
		countQuery.where(criteria.getRestriction());

		countRoot.alias(root.getAlias());

		return countQuery.distinct(criteria.isDistinct());
	}

	@SuppressWarnings("unchecked")
	private static void doJoinsOnFetches(Set<? extends Fetch<?, ?>> joins, Root<?> root, Integer counter) {
		doJoins((Set<? extends Join<?, ?>>) joins, root, counter);
	}

	private static void doJoins(Set<? extends Join<?, ?>> joins, Root<?> root, Integer counter) {
		for (Join<?, ?> join : joins) {
			// fix join alias, important for making the link between old joins
			// and the new ones
			if (StringUtils.isBlank(join.getAlias())) {
				join.alias(join.getAttribute().getName() + "_" + counter);
				incrementCounter(counter);
			}
			Join<?, ?> joined = root.join(join.getAttribute().getName(), join.getJoinType());
			joined.alias(join.getAlias());
			doJoins(join.getJoins(), joined, counter);
		}
	}

	private static void doJoins(Set<? extends Join<?, ?>> joins, Join<?, ?> root, Integer counter) {
		for (Join<?, ?> join : joins) {
			// fix join alias, important for making the link between old joins
			// and the new ones
			if (StringUtils.isBlank(join.getAlias())) {
				join.alias(join.getAttribute().getName() + "_" + counter);
				incrementCounter(counter);
			}
			Join<?, ?> joined = root.join(join.getAttribute().getName(), join.getJoinType());
			joined.alias(join.getAlias());
			doJoins(join.getJoins(), joined, counter);
		}
	}

	private static synchronized void incrementCounter(Integer c) {
		c++;
	}

}
