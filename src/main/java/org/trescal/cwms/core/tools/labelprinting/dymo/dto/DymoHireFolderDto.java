package org.trescal.cwms.core.tools.labelprinting.dymo.dto;

public class DymoHireFolderDto extends DymoDto {
	private String label_title;
	private String label_hire_number;
	private String label_items;
	private String label_company;
	private String label_contact;
	private String label_hire_date;
	private String hireno;
	private String subdiv;
	private String itemno;
	private String coname;
	private String name;
	private String date;
	
	public String getLabel_title() {
		return label_title;
	}
	public void setLabel_title(String label_title) {
		this.label_title = label_title;
	}
	public String getLabel_hire_number() {
		return label_hire_number;
	}
	public void setLabel_hire_number(String label_hire_number) {
		this.label_hire_number = label_hire_number;
	}
	public String getLabel_items() {
		return label_items;
	}
	public void setLabel_items(String label_items) {
		this.label_items = label_items;
	}
	public String getLabel_company() {
		return label_company;
	}
	public void setLabel_company(String label_company) {
		this.label_company = label_company;
	}
	public String getLabel_contact() {
		return label_contact;
	}
	public void setLabel_contact(String label_contact) {
		this.label_contact = label_contact;
	}
	public String getLabel_hire_date() {
		return label_hire_date;
	}
	public void setLabel_hire_date(String label_hire_date) {
		this.label_hire_date = label_hire_date;
	}
	public String getHireno() {
		return hireno;
	}
	public void setHireno(String hireno) {
		this.hireno = hireno;
	}
	public String getSubdiv() {
		return subdiv;
	}
	public void setSubdiv(String subdiv) {
		this.subdiv = subdiv;
	}
	public String getItemno() {
		return itemno;
	}
	public void setItemno(String itemno) {
		this.itemno = itemno;
	}
	public String getConame() {
		return coname;
	}
	public void setConame(String coname) {
		this.coname = coname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
}
