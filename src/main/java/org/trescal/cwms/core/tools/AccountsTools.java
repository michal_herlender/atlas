package org.trescal.cwms.core.tools;

import org.trescal.cwms.core.account.entity.FinancialPeriod;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AccountsTools
{
	/**
	 * Gets the current financial period. This is calculated as the financial
	 * calendar where month 1 is January as of 2016-06-14
	 * 
	 * @return int representing the current financial period.
	 */
	public static int getCurrentFinancialPeriod()
	{
		GregorianCalendar cal = new GregorianCalendar();
		int month = cal.get(Calendar.MONTH);
		return (month + 1);
	}

	/**
	 * Gets the current {@link FinancialPeriod} based on the current date.
	 *
	 * @param date the date for the period.
	 * @return {@link FinancialPeriod}.
	 */

	public static FinancialPeriod getFinancialPeriod(LocalDate date) {
		return getFinancialPeriod(date.getMonthValue());
	}

	public static FinancialPeriod getFinancialPeriod(Date date) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		return getFinancialPeriod(cal.get(Calendar.MONTH));
	}

	/**
	 * Returns a FinancialPeriod object representing the month given in the
	 * period. (Where the financial calendar starts in April - i.e. April = 1,
	 * May = 2.
	 * 
	 * @param month the java Calendar.MONTH field representing the period.
	 * @return a {@link FinancialPeriod}.
	 */
	public static FinancialPeriod getFinancialPeriod(int month)
	{
		FinancialPeriod fp = new FinancialPeriod();

		int period = 0;
		if (month >= 3) {
			period = month - 2;
		} else {
			period = month + 10;
		}

		fp.setPeriod(period);
		fp.setRealMonth(month);
		fp.setMonthName(DateTools.getMonthInWords(month));
		fp.setCurrentPeriod(period == getCurrentFinancialPeriod());
		return fp;
	}

	/**
	 * Gets a list of financial periods. Starts two months prior to the current
	 * month and ends six months forwards from the current month. Returns as a
	 * list of {@link FinancialPeriod} wrappers. (Where the financial calendar
	 * starts in April - so 1 = April, 2 = May etc.
	 * 
	 * @return list of {@link FinancialPeriod}.
	 */
	public static ArrayList<FinancialPeriod> getFinancialPeriodRange()
	{
		ArrayList<FinancialPeriod> fpList = new ArrayList<FinancialPeriod>();

		GregorianCalendar cal = new GregorianCalendar();
		cal.add(Calendar.MONTH, -2);

		int count = 0;
		while (count < 5)
		{
			int month = cal.get(Calendar.MONTH);
			int period = 0;
			if (month >= 3) {
				period = month - 2;
			} else {
				period = month + 10;
			}

			fpList.add(new FinancialPeriod(month, DateTools.getMonthInWords(month), period, period == getCurrentFinancialPeriod()));

			cal.add(Calendar.MONTH, 1);
			count = count + 1;
		}

		return fpList;
	}

}
