/* Licence:
 *   Use this however/wherever you like, just don't blame me if it breaks anything.
 *
 * Credit:
 *   If you're nice, you'll leave this bit:
 *
 *   Class by Pierre-Alexandre Losson -- http://www.telio.be/blog
 *   email : plosson@users.sourceforge.net
 */
package org.trescal.cwms.core.tools.fileupload2.utils;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Upload Listener that tracks a file upload, collects data on it's progress and
 * saves the data in an {@link UploadInfo} object that gets put into the
 * {@link HttpSession} attribute referenced by 'uploadInfo'.
 *
 * @author Original : plosson on 06-janv.-2006 15:05:44
 * @author Richard
 */
@Slf4j
public class UploadListener implements OutputStreamListener {
	private final long delay;
	private final HttpServletRequest request;
	private final long startTime;
	private int totalBytesRead = 0;
	private int totalFiles = -1;
	private final int totalToRead;

	public UploadListener(HttpServletRequest request, long debugDelay) {
		this.request = request;
		this.delay = debugDelay;
		this.totalToRead = request.getContentLength();
		this.startTime = System.currentTimeMillis();
	}

	public void bytesRead(int bytesRead)
	{
		this.totalBytesRead = this.totalBytesRead + bytesRead;
		this.updateUploadInfo("progress");

		try
		{
			Thread.sleep(this.delay);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

	public void done()
	{
		this.updateUploadInfo("checking");
	}

	public void error(String message)
	{
		this.updateUploadInfo("error");
	}

	public long getDelta()
	{
		return (System.currentTimeMillis() - this.startTime) / 1000;
	}

	public void start()
	{
		this.totalFiles++;
		this.updateUploadInfo("start");
	}

	private void updateUploadInfo(String status) {
		long delta = (System.currentTimeMillis() - this.startTime) / 1000;
		this.request.getSession().setAttribute("uploadInfo", new UploadInfo(this.totalFiles, this.totalToRead, this.totalBytesRead, delta, status));
		log.debug("Total Files:" + this.totalFiles + ", Total to read:" + this.totalToRead + ", Total read:" + this.totalBytesRead + ", Delta:" + delta + ", Status:" + status);
	}

}
