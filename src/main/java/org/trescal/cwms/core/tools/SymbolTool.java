package org.trescal.cwms.core.tools;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * In documents, this class can be injected and the escape() method used to
 * replace special characters with their HTML escaped counterparts.
 */
/*
 * Note, unicode escape sequences are used in this file to prevent encoding
 * issues. The file should now be saved as (project default) Cp1252 encoding,
 * and not UTF-8!
 */
public class SymbolTool
{
	public static final Map<BigDecimal, String> fractionMap;

	/**
	 * Defines a list of escaped HTML characters that cannot be displayed in the
	 * Symbol font and that instead require being wrapped in a pre-existing HTML
	 * tag that can be interpreted successfully by FOP (e.g. <sup> or <sub>)
	 */
	public static final Map<String, String> htmlExchangeMap;

	/**
	 * Defines characters that cannot be rendered in the VelocityEngine used
	 * when creating documents or using the default PDF font in FOP. In FOP the
	 * escaped values also need to use the "Symbol" font instead of the default
	 * (i.e. Helvetica) - handled in DVSLUtil.java using this map
	 */
	public static final Map<String, String> symbolMap;

	static
	{
		Map<String, String> map = new HashMap<String, String>();
		map.put("\u03A9", "&#8486;"); // upper case omega
		map.put("\u2126", "&#8486;"); // alternate "ohm sign"
		map.put("\u03C0", "&#960;"); // lower case pi
		map.put("\u0394", "&#916;"); // upper case delta
		map.put("\u2082", "&#8322;"); // subscript 2
		map.put("\u2083", "&#8323;"); // subscript 3
		map.put("\u2084", "&#8324;"); // subscript 4
		map.put("\u00B2", "&#178;"); // superscript 2
		map.put("\u00B3", "&#179;"); // superscript 3
		map.put("\u2074", "&#8308;"); // superscript 4
		map.put("\u2075", "&#8309;"); // superscript 5
		map.put("\u2076", "&#8310;"); // superscript 6
		map.put("\u2077", "&#8311;"); // superscript 7
		map.put("\u2078", "&#8312;"); // superscript 8
		symbolMap = Collections.unmodifiableMap(map);

		map = new HashMap<String, String>();
		map.put("&#8322;", "<sub>2</sub>");
		map.put("&#8323;", "<sub>3</sub>");
		map.put("&#8324;", "<sub>4</sub>");
		map.put("&#178;", "<sup>2</sup>");
		map.put("&#179;", "<sup>3</sup>");
		map.put("&#8308;", "<sup>4</sup>");
		map.put("&#8309;", "<sup>5</sup>");
		map.put("&#8310;", "<sup>6</sup>");
		map.put("&#8311;", "<sup>7</sup>");
		map.put("&#8312;", "<sup>8</sup>");
		htmlExchangeMap = Collections.unmodifiableMap(map);

		Map<BigDecimal, String> numberMap = new HashMap<BigDecimal, String>();
		numberMap.put(new BigDecimal("0.125"), "<sup>1</sup>/<sub>8</sub>");
		numberMap.put(new BigDecimal("0.2"), "<sup>1</sup>/<sub>5</sub>");
		numberMap.put(new BigDecimal("0.25"), "<sup>1</sup>/<sub>4</sub>");
		numberMap.put(new BigDecimal("0.375"), "<sup>3</sup>/<sub>8</sub>");
		numberMap.put(new BigDecimal("0.4"), "<sup>2</sup>/<sub>5</sub>");
		numberMap.put(new BigDecimal("0.5"), "<sup>1</sup>/<sub>2</sub>");
		numberMap.put(new BigDecimal("0.6"), "<sup>3</sup>/<sub>5</sub>");
		numberMap.put(new BigDecimal("0.625"), "<sup>5</sup>/<sub>8</sub>");
		numberMap.put(new BigDecimal("0.75"), "<sup>3</sup>/<sub>4</sub>");
		numberMap.put(new BigDecimal("0.8"), "<sup>4</sup>/<sub>5</sub>");
		numberMap.put(new BigDecimal("0.875"), "<sup>7</sup>/<sub>8</sub>");
		fractionMap = Collections.unmodifiableMap(numberMap);
	}

	public static String convertDecimalToFraction(String decimal)
	{
		BigDecimal temp = new BigDecimal(decimal);
		int i = temp.intValue();
		if (temp.compareTo(new BigDecimal(i)) == 0)
		{
			return String.valueOf(i);
		}
		String safeDecimal = temp.toPlainString();
		String afterDecPoint = "";
		if (safeDecimal.indexOf(".") >= 0)
		{
			String x = safeDecimal.substring(safeDecimal.indexOf("."));
			BigDecimal fract = new BigDecimal(x);

			for (Entry<BigDecimal, String> pair : fractionMap.entrySet())
			{
				if (pair.getKey().compareTo(fract) == 0)
				{
					afterDecPoint = pair.getValue();
					break;
				}
			}

			if (afterDecPoint.trim().isEmpty())
			{
				return decimal;
			}
		}
		String toReturn = (i != 0) ? String.valueOf(i) : "";
		return toReturn.concat(afterDecPoint);
	}

	public static String escape(String str)
	{
		for (Entry<String, String> pair : symbolMap.entrySet())
		{
			str = str.replaceAll(pair.getKey(), pair.getValue());
		}

		for (Entry<String, String> pair : htmlExchangeMap.entrySet())
		{
			str = str.replaceAll(pair.getKey(), pair.getValue());
		}

		return str;
	}
}