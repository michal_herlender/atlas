package org.trescal.cwms.core.tools;

import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactDWRService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.userright.entity.permissiongroup.PermissionGroup;
import org.trescal.cwms.core.userright.enums.Permission;

import java.util.Set;

/**
 * Service created to check whether or not a {@link Contact} (often the
 * currently logged-in {@link Contact}) has authority over another. This is used
 * for things such as when the logged-in {@link Contact} is trying to view a
 * private {@link Email} that another {@link Contact} has sent, or when the
 * logged-in {@link Contact} is trying to delete a {@link JobItemActivity} that
 * was performed by another {@link Contact}.
 * <p>
 * Explanation of Roles:
 * <p>
 * STAFF_USERS have authority only over themselves.
 * MANAGERS have authority over themselves and any STAFF_USERS in their department.
 * ADMINISTRATORS have authority over everybody except for other ADMINISTRATORS and DIRECTORS
 * DIRECTORS have authority over everybody except for other ADMINISTRATORS and DIRECTORS
 *
 * @author jamiev
 */
@Service("AuthenticationService")
public class AuthenticationService {
    private final ContactDWRService conServ;

    public AuthenticationService(ContactDWRService conServ) {
        this.conServ = conServ;
    }

    /**
     * Checks if the first {@link Contact} has authority over the second
     * {@link Contact}
     *
     * @param currentConId the {@link Contact} requesting authority
     * @param checkAgainstConId the {@link Contact} to check against
     * @return true if the first {@link Contact} has authority, false otherwise.
     */
    public Boolean allowPrivileges(int currentConId, int checkAgainstConId, int subdivId) {
        Contact currentCon = this.conServ.findDWRContact(currentConId);
        Contact checkAgainstCon = this.conServ.findDWRContact(checkAgainstConId);
        // Check if not a business company contact
        if (checkAgainstCon.getSub().getComp().getCompanyRole() != CompanyRole.BUSINESS)
            return true;

        boolean isSubdivUser = checkAgainstCon.getUser() != null && checkAgainstCon.getUser().getUserRoles() != null &&
                checkAgainstCon.getUser().getUserRoles().stream().anyMatch(ur -> ur.getOrganisation().getSubdivid() == subdivId);
        if (currentCon.getPersonid() == checkAgainstCon.getPersonid())
            // it is the same user
            return true;
        else if (isSubdivUser) {
            if (this.hasRight(currentCon, Permission.ADMIN_MANAGE_USER_COMPANY, subdivId))
                return true;
            else if (this.hasRight(currentCon, Permission.MANAGE_USER_COMPANY, subdivId))
                return !this.hasRight(checkAgainstCon, Permission.ADMIN_MANAGE_USER_COMPANY, subdivId)
                        && !this.hasRight(checkAgainstCon, Permission.MANAGE_USER_COMPANY, subdivId);
            else return this.isManagerOf(currentCon, checkAgainstCon);
		} else if (checkAgainstCon.getUser() != null && checkAgainstCon.getUser().getUserRoles().size() == 0 && subdivId == checkAgainstCon.getSub().getId())
            return true;
        else return checkAgainstCon.getUser() == null && subdivId == checkAgainstCon.getSub().getId();
    }

    /**
     * Checks if the given {@link Contact} has one of the given rights for the Subdiv.
     *
     * @param con the {@link Contact}
     * @param userRight to test
     * @return true if the {@link Contact} has the permission, false otherwise.
	 */
	public boolean hasRight(Contact con, Permission userRight) {
		return this.hasRight(con, userRight, null);
    }

    /**
     * Checks if the given {@link Contact} has one of the given rights for the Subdiv.
     *
     * @param con the {@link Contact}
     * @param userRight to test
     * @param subdivId the subdivision ID the user should have the role for (may be null, match any subdiv)
     * @return true if the {@link Contact} has the permission, false otherwise.
	 */
	public boolean hasRight(Contact con, Permission userRight, Integer subdivId) {
		if ((userRight != null) && (con.getUser() != null)) {
			for (org.trescal.cwms.core.userright.entity.userrole.UserRole ur : con.getUser().getUserRoles()) {
				if(subdivId!=null && ur.getOrganisation().getSubdivid()!=subdivId)
					continue;
				for(PermissionGroup grp : ur.getRole().getGroups()) {
					if(grp.getPermissions().contains(userRight))
						return true;
				}
			}
		}
		return false;
	}

	/**
	 * Checks if the first {@link Contact} given is the Manager of the second
	 * {@link Contact} given.
	 * 
	 * @param manager the proposed Manager {@link Contact}
	 * @param staffMember the proposed department staff member {@link Contact}
	 * @return true if the first {@link Contact} is the Manager of the second
	 *         {@link Contact}, false otherwise.
	 */
	public boolean isManagerOf(Contact manager, Contact staffMember)
	{
		boolean isManagerOf = false;

		Set<DepartmentMember> managerDepts = manager.getMember();
		Set<DepartmentMember> staffDepts = staffMember.getMember();

		for (DepartmentMember managerDept : managerDepts)
		{
			if (managerDept.getDepartmentRole().getRole().equals("Manager")
					|| managerDept.getDepartmentRole().getRole().equals("Director"))
			{
				for (DepartmentMember staffDept : staffDepts)
				{
					if (managerDept.getDepartment().getDeptid().intValue() == staffDept.getDepartment().getDeptid().intValue())
					{
						if (staffDept.getDepartmentRole().getRole().equals("Member"))
						{
							isManagerOf = true;
						}
					}
				}
			}
		}

		return isManagerOf;
	}
}