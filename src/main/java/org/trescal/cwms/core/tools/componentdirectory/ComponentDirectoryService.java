package org.trescal.cwms.core.tools.componentdirectory;

import java.io.File;

import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;

public interface ComponentDirectoryService
{
	/**
	 * Returns a {@link File} reference to the directory for the given
	 * {@link SystemComponent} and the entity of that component type identified
	 * by the given id. If the file does not exist then it is created. If any
	 * problems occur then null is returned.
	 * 
	 * @param sc the {@link SystemComponent} of this entity.
	 * @param id the id of the entity.
	 * @return the {@link File} or null.
	 */
	public File getDirectory(SystemComponent sc, String id);

	/**
	 * Returns a {@link File} reference to the directory for the given
	 * {@link SystemComponent} and the entity of that component type identified
	 * by the given id. Some entities require versioned subfolders, if this is
	 * the case then the given version is used in the file path generation. If
	 * the file does not exist then it is created. If any problems occur then
	 * null is returned.
	 * 
	 * @param sc the {@link SystemComponent} of this entity.
	 * @param id the id of the entity.
	 * @param version the version of the entity.
	 * @return the {@link File} or null.
	 */
	public File getDirectory(SystemComponent sc, String id, Integer version);
}
