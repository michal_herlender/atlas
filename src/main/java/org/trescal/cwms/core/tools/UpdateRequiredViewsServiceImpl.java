package org.trescal.cwms.core.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.SQLServer2012Dialect;
import org.hibernate.dialect.SQLServerDialect;
import org.hibernate.exception.SQLGrammarException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.hibernateextension.TrescalSQLServerDialect;

/**
 * Bean whose role it is to parse sql files in a preset location and execute any
 * sql scripts found in those files. This is principally designed for recreating
 * views at system startup.
 * 
 * @author Richard
 */
@Service("updateRequiredViewsTarget")
public class UpdateRequiredViewsServiceImpl implements ResourceLoaderAware, UpdateRequiredViewsIF
{
	@Value("${cwms.admin.email}")
	private String adminEmail;

	@Value("${cwms.config.dbmaintenance.createviewsatstartup}")
	private boolean createViewsOnStartUp;
	
	@Autowired
	private UpdateRequiredViewsDao dao;
	
	@Value("${hibernate.dialect}")
	private Class<? extends Dialect> dialect;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private ResourceLoader resourceLoader;

	@Value("${cwms.config.dbmaintenance.viewscriptloc}")
	private String viewScriptRoot;

	@Value("${cwms.config.dbmaintenance.viewscriptfiles}")
	private String viewSQLFileList;

	// TODO should not be in bean variable if ever run twice
	private List<String> errors = new ArrayList<String>();
	protected final Log logger = LogFactory.getLog(this.getClass());

	/**
	 * Splits a comma separated list of file names into an array of file names.
	 * 
	 * @return string array of file names.
	 */
	public String[] getFileNameList()
	{
		return this.viewSQLFileList.split(",");
	}

	protected String getPathToScripts()
	{
		if (this.dialect != null)
		{
			if (this.dialect.equals(SQLServerDialect.class) || 
				this.dialect.equals(SQLServer2012Dialect.class) ||
					this.dialect.equals(TrescalSQLServerDialect.class))
			{
				return this.viewScriptRoot.concat(File.separator).concat(Constants.SQLSERVER);
			}
		}
		return this.viewScriptRoot;
	}

	private void handleError(Exception e, String path, String sql)
	{
		this.logger.error("Error parsing and running "+path);
		this.errors.add("Error parsing and running "+path);
		if (!sql.equals(""))
		{
			this.errors.add("With SQL Statement: " + sql);
			this.logger.error("Failed executing sql: " + sql);
			this.logger.error("(Note sql statements must end with ';')");
		}
		this.errors.add(e.toString());
		e.printStackTrace();
	}

	/**
	 * Runs through the given file looking for 'valid' sql statements. An SQL
	 * statement is considered to be terminated by a ';'. All lines containing
	 * '--' are assumed to be comments and stripped.
	 * 
	 * @param file the {@link File} to parse.
	 * @return list of valid SQL statements.
	 */
	protected List<String> parseForValidSQL(BufferedReader br) throws Exception
	{
		ArrayList<String> sqlStrings = new ArrayList<String>();

		// read the file into a string buffer
		StringBuffer buff = new StringBuffer();
		String s = "";
		while ((s = br.readLine()) != null)
		{
			// ignore any comment lines
			if (!s.trim().contains("--"))
			{
				buff.append(" " + s);
			}
		}
		br.close();

		// now split the resulting string into sql statements
		String[] stmts = buff.toString().split(";");
		for (String sql : stmts)
		{
			sqlStrings.add(sql.concat(";"));
		}

		if (this.dialect.equals(SQLServerDialect.class) || this.dialect.equals(SQLServer2012Dialect.class) || this.dialect.equals(TrescalSQLServerDialect.class))
		{
			// second layer of processing to remove 'GO' statements from sql
			// server queuries
			ArrayList<String> sql = new ArrayList<String>();
			for (String st : sqlStrings)
			{
				String[] stmts2 = st.split("GO");
				for (String clean : stmts2)
				{
					sql.add(clean);
				}
			}
			sqlStrings = sql;
		}

		this.logger.info("found " + sqlStrings.size() + " sql statements");
		return sqlStrings;
	}

	public void setResourceLoader(ResourceLoader resourceLoader)
	{
		this.resourceLoader = resourceLoader;
	}

	public void setViewSQLFileList(String viewSQLFileList)
	{
		this.viewSQLFileList = viewSQLFileList;
	}

	public void updateDatabaseViews()
	{
		// check if the update view preference has been set
		if (this.createViewsOnStartUp)
		{
			int scriptsRun = 0;
			this.logger.info("Checking for database views to create");
			this.logger.info(viewSQLFileList);
			String[] fileNameList = this.getFileNameList();
			this.logger.info("Script count to run: "+viewSQLFileList.length());

			// parse each view file and extract the sql statements
			for (String fileName : fileNameList)
			{
				String path = getPathToScripts()+File.separator+fileName.trim();

				List<String> sqlStrings = new ArrayList<String>();
				try
				{
					this.logger.info("parsing "+path+" for view script");
					Resource resource = resourceLoader.getResource(path);
					InputStream is = resource.getInputStream();
					BufferedReader br = new BufferedReader(new InputStreamReader(is));
					// parse each file and get the sql strings
					sqlStrings = this.parseForValidSQL(br);
				}
				catch (Exception e)
				{
					this.handleError(e, path, "");
				}

				// execute and handle result
				for (String sql : sqlStrings)
				{
					try
					{
						this.dao.runSQLUpdate(sql);
						scriptsRun++;
					}
					catch (SQLGrammarException e)
					{
						this.handleError(e, path, sql);
					}
				}
			}

			this.logger.info("Completed. " + scriptsRun
					+ " scripts successfully executed with "
					+ this.errors.size() + " errors");

			if (this.errors.size() > 0)
			{
				StringBuffer buff = new StringBuffer();
				buff.append("<html><body>");
				for (String error : this.errors)
				{
					buff.append("<br/>").append(error);
				}
				buff.append("</body></html>");

				this.emailService.sendAdvancedEmail("Error creating views at startup", this.adminEmail, Collections.singletonList(this.adminEmail), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), buff.toString(), true, true);
			}
		}
		else
		{
			this.logger.info("Automatic database view creation disabled");
		}
	}
}
