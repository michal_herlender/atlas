package org.trescal.cwms.core.tools;

import lombok.extern.slf4j.Slf4j;

import javax.print.*;
import javax.print.attribute.DocAttributeSet;
import javax.print.attribute.HashDocAttributeSet;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@Slf4j
public class PrinterProfiler {
	public static void main(String[] args) {

		String printerName = "\\\\ANTYARDC08\\Accounts HP";
		String filePath = "V:\\Users\\Jamie Vinall\\Sample PCL files\\papersource7.pcl";
		File printableFile = new File(filePath);

		PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
		DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
		PrintService[] printServices = PrintServiceLookup.lookupPrintServices(flavor, null);
		FileInputStream fis = null;

		try {
			if (printServices.length > 0) {
				PrintService printService = null;
				for (PrintService ps : printServices) {
					if (ps.getName().trim().equalsIgnoreCase(printerName)) {
						printService = ps;
					}
				}

				if (printService == null) {
					throw new Exception("There is no print service matching that name.");
				}

				DocPrintJob job = printService.createPrintJob();

				fis = new FileInputStream(printableFile.getAbsolutePath());
				DocAttributeSet das = new HashDocAttributeSet();
				Doc doc = new SimpleDoc(fis, flavor, das);

				job.print(doc, pras);
				log.debug("Print job sent...");
			} else {
				throw new Exception("No compatible printers for this document type.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
