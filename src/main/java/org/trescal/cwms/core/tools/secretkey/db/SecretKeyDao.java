package org.trescal.cwms.core.tools.secretkey.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.tools.secretkey.SecretKey;

public interface SecretKeyDao extends BaseDao<SecretKey, byte[]> {
	
}