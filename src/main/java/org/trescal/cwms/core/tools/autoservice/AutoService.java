package org.trescal.cwms.core.tools.autoservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.JobItemTransit;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.autoservice.EmailContent_ChaseItems;
import org.trescal.cwms.core.system.entity.emailcontent.autoservice.EmailContent_ChaseSummary;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.emailrecipient.RecipientType;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultBooleanType;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.system.form.EmailForm;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.userright.enums.Permission;

import java.util.*;

public abstract class AutoService {
	@Autowired
	private AuthenticationService authServ;
	@Autowired
	protected BusinessDetailsService bdServ;
	@Autowired
	private EmailContent_ChaseItems ecChaseItems;
	@Autowired
	private EmailContent_ChaseSummary ecChaseSummary;
	@Autowired
	protected EmailService emailServ;
	@Autowired
	protected JobItemService jiServ;
	@Autowired
	protected MessageSource messages;
	@Autowired
	protected ScheduledTaskService schTaskServ;
	@Autowired
	protected SupportedLocaleService supportedLocaleService;
	@Autowired
	private UserService userServ;
	
	@Value("${cwms.admin.email}")
	private String adminEmail;
	@Value("${cwms.config.autoservice.client.first_chase_days}")
	private int firstChaseAfter;
	@Value("${cwms.config.autoservice.client.chase_email_from}")
	private String fromAddress;
	@Value("${cwms.config.autoservice.client.further_chase_days}")
	private int furtherChasesAfter;
	@Value("${cwms.config.autoservice.client.chase_email_replyto}")
	private String replyToAddress;
	@Value("${cwms.config.autoservice.client.summary_email_to}")
	private String summaryTo;
	@Value("${cwms.config.autoservice.client.total_chases}")
	protected int totalChases;

	abstract protected SystemDefaultBooleanType getChaseDefault();
	
	abstract protected void sendEmail(Contact con, Map<Integer, List<JobItem>> itemsToChaseNow, String fromAddress, String replyToAddress, int totalChases);
	
	protected static final Logger logger = LoggerFactory.getLogger(AutoService.class);

	public AutoServiceSummary chase(Map<Contact, List<JobItem>> toChase)
	{
		AutoServiceSummary summary = new AutoServiceSummary(totalChases);

		for (Contact con : toChase.keySet()) {
			List<JobItem> jis = toChase.get(con);

			for (JobItem ji : jis) {
				// figure out last chase (if any) - factor in progress
				// activities!

				JobItemAction lastAction = null;

				int numChases = 0;
				TreeSet<JobItemAction> actions = new TreeSet<>(new JobItemActionComparator());
				actions.addAll(ji.getActions());

				// loop backwards through actions
				for (JobItemAction action : actions.descendingSet()) {
					// if action is deleted, ignore and move to next in loop
					if (!action.isDeleted()) {
						if (action instanceof JobItemActivity) {
							JobItemActivity activity = (JobItemActivity) action;

							if (lastAction == null) {
								lastAction = action;
							}

							boolean autoServUser = (activity.isComplete() && this.authServ.hasRight(con, Permission.AUTO_CWMS));
							if ((activity.getActivity().isProgress())
								|| autoServUser) {
								numChases++;
							} else {
								break;
							}
						} else if (action instanceof JobItemTransit) {
							if (lastAction == null) {
								lastAction = action;
							}

							break;
						} else {
							break;
						}
					}
				}

				// if not yet chased, wait x days otherwise wait y days
				int daysBeforeChase = (numChases < 1) ? this.firstChaseAfter : this.furtherChasesAfter;

				if (((lastAction instanceof JobItemActivity) && ((JobItemActivity) lastAction).isComplete())
						|| (lastAction instanceof JobItemTransit))
				{
					if (DateTools.isMoreThanXDaysAgo(lastAction.getEndStamp(), daysBeforeChase))
					{
						if (numChases >= this.totalChases)
						{
							summary.addFailChaseLimit(con, ji);
						}
						else if (ji.getNeverToBeChased()
								|| !ji.getChasingActive())
						{
							summary.addFailDisabledChase(con, ji);
						}
						else
						{
							int chaseToSend = numChases + 1;
							summary.addChased(con, chaseToSend, ji);
						}
					}
				}
			}
			Map<Integer, List<JobItem>> itemsToChaseNow = summary.getChased().get(con); 
			if ((con.getEmail() == null) || con.getEmail().isEmpty())
			{
				summary.addFailNoEmail(con, toChase.get(con));
			}
			else if (getChaseDefault().parseValue(getChaseDefault().getValueHierarchical(Scope.CONTACT, con, null).getValue()))
			{
				summary.addFailNoChasing(con, toChase.get(con));
			}
			else if (itemsToChaseNow != null && !itemsToChaseNow.isEmpty())
			{
				sendEmail(con, itemsToChaseNow, fromAddress, replyToAddress, totalChases);
				summary.incrementEmailsSent();
			}
		}

		return summary;
	}

	public boolean sendSummary(AutoServiceSummary summary, AutoServiceType autoServiceType)
	{
		Locale locale = supportedLocaleService.getPrimaryLocale();
		
		EmailContentDto contentDto = this.ecChaseSummary.getContent(summary, locale, autoServiceType);
		String body = contentDto.getBody();
		String subject = contentDto.getSubject();

		return this.emailServ.sendAdvancedEmail(subject, this.fromAddress, Collections.singletonList(this.summaryTo), Collections.singletonList(adminEmail), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), body, true, false);
	}

	public Email sendChaseEmail(Contact con,
			Map<Integer, List<JobItem>> itemsToChaseNow, String fromAddress,
			String replyToAddress, int totalChases, AutoServiceType autoServiceType) {
		for (Integer chaseNumber : itemsToChaseNow.keySet()) {
			Set<Job> distinctJobs = new HashSet<>();

			for (JobItem ji : itemsToChaseNow.get(chaseNumber)) {
				distinctJobs.add(ji.getJob());
			}

			Locale locale = con.getLocale();
			List<JobItem> jobItems = itemsToChaseNow.get(chaseNumber);

			EmailContentDto contentDto = this.ecChaseItems.getContent(con, jobItems, chaseNumber, totalChases, locale, autoServiceType);
			String body = contentDto.getBody();
			String subject = contentDto.getSubject();

			Contact cwmsAutoUser = this.userServ.findSystemContact();

			boolean emailSent = false;
			for (Job j : distinctJobs) {

				EmailForm mailForm = new EmailForm(Component.JOB,
					j.getJobid(), fromAddress, con, con.getEmail(),
					subject, body, cwmsAutoUser);
				EmailRecipient mailRecipient = new EmailRecipient();
				mailRecipient.setEmailAddress(con.getEmail());
				mailRecipient.setRecipientCon(con);
				mailRecipient.setType(RecipientType.EMAIL_TO);
				mailRecipient.setEmail(mailForm.getEmail());
				mailForm.setRecipients(Collections.singletonList(mailRecipient));
				mailForm.getEmail().setRecipients(Collections.singletonList(mailRecipient));

				List<String> toList = new ArrayList<>();
				List<String> ccList = new ArrayList<>();

				for (EmailRecipient recip : mailForm.getRecipients()) {
					if (recip.getType() == RecipientType.EMAIL_TO) {
						toList.add(recip.getEmailAddress());
					} else {
						ccList.add(recip.getEmailAddress());
					}
				}

				if (!emailSent) {
					// System.out.println("Sending e-mail this time!");
					this.emailServ.sendAdvancedEmail(subject, fromAddress, toList,
						ccList, Collections.emptyList(), Collections.singletonList(replyToAddress), Collections.emptyList(),
						body, true, false);
					emailSent = true;
				}

				this.emailServ.persistEmailAndRecipients(mailForm.getEmail(),
						mailForm.getRecipients());
			}
		}

		return null;
	}
}
