package org.trescal.cwms.core.tools.secretkey.db;


import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.tools.secretkey.SecretKey;

@Transactional
@Repository("SecretKeyDao")
public class SecretKeyDaoImpl extends BaseDaoImpl<SecretKey, byte[]> implements SecretKeyDao {
	
	@Override
	protected Class<SecretKey> getEntity() {
		return SecretKey.class;
	}
	
}