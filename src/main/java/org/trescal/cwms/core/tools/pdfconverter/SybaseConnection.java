package org.trescal.cwms.core.tools.pdfconverter;

public class SybaseConnection
{
	private int port;
	private String pwd;
	private String server;
	private String serviceName;
	private String user;

	public SybaseConnection()
	{
	}

	public SybaseConnection(String server, String serviceName, String user, String pwd, int port)
	{
		super();
		this.server = server;
		this.serviceName = serviceName;
		this.user = user;
		this.pwd = pwd;
		this.port = port;
	}

	public int getPort()
	{
		return this.port;
	}

	public String getPwd()
	{
		return this.pwd;
	}

	public String getServer()
	{
		return this.server;
	}

	public String getServiceName()
	{
		return this.serviceName;
	}

	public String getUser()
	{
		return this.user;
	}

	public void setPort(int port)
	{
		this.port = port;
	}

	public void setPwd(String pwd)
	{
		this.pwd = pwd;
	}

	public void setServer(String server)
	{
		this.server = server;
	}

	public void setServiceName(String serviceName)
	{
		this.serviceName = serviceName;
	}

	public void setUser(String user)
	{
		this.user = user;
	}
}