package org.trescal.cwms.core.tools.labelprinting.dymo.dto;

public class DymoJobFolderDto extends DymoDto {
	private String label_title;
	private String label_job_number;
	private String label_items;
	private String label_company;
	private String label_contact;
	private String label_date_in;
	private String label_date_due;
	private String jobno;
	private String subdiv;
	private String itemno;
	private String coname;
	private String name;
	private String date;
	private String date_due;
	
	public String getLabel_title() {
		return label_title;
	}
	public void setLabel_title(String label_title) {
		this.label_title = label_title;
	}
	public String getLabel_job_number() {
		return label_job_number;
	}
	public void setLabel_job_number(String label_job_number) {
		this.label_job_number = label_job_number;
	}
	public String getLabel_items() {
		return label_items;
	}
	public void setLabel_items(String label_items) {
		this.label_items = label_items;
	}
	public String getLabel_company() {
		return label_company;
	}
	public void setLabel_company(String label_company) {
		this.label_company = label_company;
	}
	public String getLabel_contact() {
		return label_contact;
	}
	public void setLabel_contact(String label_contact) {
		this.label_contact = label_contact;
	}
	public String getLabel_date_in() {
		return label_date_in;
	}
	public void setLabel_date_in(String label_date_in) {
		this.label_date_in = label_date_in;
	}
	public String getJobno() {
		return jobno;
	}
	public void setJobno(String jobno) {
		this.jobno = jobno;
	}
	public String getSubdiv() {
		return subdiv;
	}
	public void setSubdiv(String subdiv) {
		this.subdiv = subdiv;
	}
	public String getItemno() {
		return itemno;
	}
	public void setItemno(String itemno) {
		this.itemno = itemno;
	}
	public String getConame() {
		return coname;
	}
	public void setConame(String coname) {
		this.coname = coname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getLabel_date_due() {
		return label_date_due;
	}
	public String getDate_due() {
		return date_due;
	}
	public void setLabel_date_due(String label_date_due) {
		this.label_date_due = label_date_due;
	}
	public void setDate_due(String date_due) {
		this.date_due = date_due;
	}
}
