package org.trescal.cwms.core.tools;

import org.apache.commons.lang3.StringUtils;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.CalibrationPoint;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.CalibrationPointSet;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationrange.CalibrationRange;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;

import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;

public class StringTools {
	/**
	 * from an array of integers return a 'tidy' string representation eg from
	 * 1,2,3,5,6,7,8 return 1-3, 5,6, 7,8 this method sourced from cwms1 as a
	 * quick fix, could be updated?
	 */
	public static String combineSequenceListIntegerNumbers(Collection<Integer> items) {
		Integer[] ns = new Integer[items.size()];
		int x = 0;
		for (Integer item : items) {
			ns[x] = item;
			x++;
		}

		return StringTools.combineSequenceNumbers(ns);
	}

	public static String combineSequenceCRJINumbers(Collection<ContractReviewItem> items) {
		Integer[] ns = new Integer[items.size()];
		int x = 0;
		for (ContractReviewItem item : items) {
			ns[x] = item.getJobitem().getItemNo();
			x++;
		}

		return StringTools.combineSequenceNumbers(ns);
	}

	public static String combineSequenceJobCostingItemNumbers(Collection<JobCostingItem> items) {
		Integer[] ns = new Integer[items.size()];
		int x = 0;
		for (JobCostingItem item : items) {
			ns[x] = item.getJobItem().getItemNo();
			x++;
		}

		return StringTools.combineSequenceNumbers(ns);
	}

	public static String combineSequenceJobItemNumbers(Collection<JobItem> items) {
		Integer[] ns = new Integer[items.size()];
		int x = 0;
		for (JobItem item : items) {
			ns[x] = item.getItemNo();
			x++;
		}

		return StringTools.combineSequenceNumbers(ns);
	}

	public static String combineSequenceNumbers(Integer[] numbers) {
		if ((numbers != null) && (numbers.length > 0)) {
			// first ensure the list is sorted
			List<Integer> list = Arrays.asList(numbers);
			Collections.sort(list);
			Integer[] ns = (Integer[]) list.toArray();

			// then begin concatenating a string
			String s = String.valueOf(ns[0]);
			int lastn = ns[0];
			int listcounter = 0;
			boolean bIsList = false;
			int counter = 0;
			for (int i = 1; i < ns.length; i++) {
				if (bIsList && ((lastn + 1) != ns[i])) {
					s = s + " - " + lastn + ", " + ns[i];
					bIsList = false;
					listcounter = 0;

				} else if ((lastn + 1) != ns[i]) {
					s = s + ", " + ns[i];
				} else {
					if ((listcounter != 0) || ((lastn + 1) == ns[i])) {
						bIsList = true;
					} else {
						listcounter = ns[i];
					}
				}
				lastn = ns[i];
				counter = i;
			}

			if (counter > 0) {
				// include final tidy
				if (((lastn - 1) == ns[counter - 1]) && bIsList)
				// handles when list is a complete list i.e. 1,2,3,4,5,6,7,8 =
				// 1-8
				{
					s = s + " - " + lastn;
				} else if (!bIsList && ((lastn - 1) == ns[counter - 1]))
				// handles when list followed by couple of stragglers, i.e.
				// 1,2,3,4,5,6,7,8,10,11 = 1-8, 10,11
				{
					s = s + ", " + lastn;
				}
			}
			return s;
		} else {
			return "";
		}
	}

	public static String convertCalibrationPointSetToString(CalibrationPointSet ps) {
		String s = "";
		int count = 0;

		for (CalibrationPoint point : ps.getPoints()) {
			count++;

			s = s + point.getFormattedPoint() + " " + point.getUom().toString();

			if (point.getRelatedPoint() != null) {
				s = s + " @ " + point.getFormattedRelatedPoint() + " " + point.getRelatedUom().toString() + "";
			}

			if (count < ps.getPoints().size()) {
				s = s + " | ";
			}
		}

		return s;
	}

	public static String convertCalibrationRangeToString(CalibrationRange r) {
		String s = r.toString();

		if (r.isRelational()) {
			s = s + " @ " + r.getFormattedRelatedPoint() + " " + r.getRelatedUom().toString() + "";
		}

		return s;
	}

	/**
	 * Turns a set of calibration requirements into a tidy, single-line string.
	 * For use through-out the system where these are displayed as notes, etc.
	 * 
	 * @param cr
	 *            the {@link CalReq} object to convert to String
	 * @return the single-line String of the requirements
	 */
	public static String convertCalReqsToSingleString(CalReq cr) {
		if (cr == null) {
			return "";
		}
		StringBuffer s = new StringBuffer();

		// first list any points or ranges
		if (cr.getPointSet() != null) {
			s.append("Calibration points: ");
			s.append(convertCalibrationPointSetToString(cr.getPointSet()));
			s.append(". ");
		}
		if (cr.getRange() != null) {
			if (s.length() > 0)
				s.append("; ");
			s.append("Calibration range: ");
			s.append(convertCalibrationRangeToString(cr.getRange()));
			s.append(". ");
		}

		boolean hasPublic = (cr.getPublicInstructions() != null) && !cr.getPublicInstructions().trim().isEmpty();
		boolean hasPrivate = (cr.getPrivateInstructions() != null) && !cr.getPrivateInstructions().trim().isEmpty();

		// then add any text instructions
		if (hasPublic || hasPrivate) {
			if (s.length() > 0)
				s.append("; ");
			s.append("Calibration instructions: ");
			if (hasPublic) {
				s.append(StringTools.removeLineBreaks(cr.getPublicInstructions()));
				s.append(". ");
			}
			if (hasPrivate) {
				s.append(StringTools.removeLineBreaks(cr.getPrivateInstructions()));
				s.append(". ");
			}
		}

		return s.toString();
	}

	/**
	 * Returns the amount of times the given {@link char} c appears in the
	 * {@link String} str. The boolean matchCase indicates whether or not to
	 * match the case of the char in the String. e.g. If the given String str =
	 * "Hello World" and the given char c = 'h', if matchCase = true the method
	 * would return 0, otherwise it would return 1.
	 * 
	 * @param str
	 *            the {@link String} to check for the given char
	 * @param c
	 *            the {@link char} to check for
	 * @param matchCase
	 *            if true matches the case when checking, if false the case is
	 *            ignored
	 * @return the amount of occurrences of char c in String str
	 */
	public static int countCharacterInString(String str, char c, boolean matchCase) {
		char[] chars = str.toCharArray();
		int count = 0;

		for (char charInList : chars) {
			if (matchCase) {
				if (charInList == c) {
					count++;
				}
			} else {
				if (String.valueOf(charInList).equalsIgnoreCase(String.valueOf(c))) {
					count++;
				}
			}
		}

		return count;
	}

	/**
	 * Returns the amount of times the given {@link String} sub appears in the
	 * {@link String} str. Changed to use Appache commons method 2015-2-3 GB as
	 * previous use of special character replace appeared to be causing
	 * internationalization problems and/or failing test.
	 * 
	 * @param str
	 *            the {@link String} to check for the given char
	 * @param sub
	 *            the {@link String} to check for
	 * @return the amount of occurrences of String sub in String str
	 */
	public static int countSubstringInString(String str, String sub) {
		return StringUtils.countMatches(str, sub);
	}

	/**
	 * Converts the given {@link BigDecimal} to a string and strips any trailing
	 * zeros. If the value ends in .00 then the decimal point is also removed.
	 * 
	 * @param bd
	 *            the {@link BigDecimal};
	 * @return string.
	 */
	public static String formatBigDecimalTrailingZeros(BigDecimal bd) {
		String value = "";

		if (bd != null) {
			value = bd.toEngineeringString();
			if (value.indexOf('.') > -1) {
				String start = value.substring(0, value.indexOf('.'));
				String end = value.substring(value.indexOf('.'), value.length());

				value = start;

				// now iterate over the remainder and chop off trailing zeros
				if (!end.equals("")) {
					if (!end.equals(".")) {
						String newString = "";
						char[] rem = end.toCharArray();
						boolean foundContent = false;
						for (int i = rem.length - 1; i >= 0; i--) {
							char c = rem[i];
							if (foundContent) {
								newString = String.valueOf(c).concat(newString);
							} else {
								if (!Character.isWhitespace(c)) {
									if ((c != '0') && (c != '.')) {
										newString = String.valueOf(c).concat(newString);
										foundContent = true;
									}
								}
							}
						}
						value = start.concat(newString);
					}
				}
			}

			// grab everything after the deimal point
		}

		return value;
	}

	public static String removeLineBreaks(String s) {
		return replaceLineBreaksWithSpecifiedString(s, "");
	}

	public static String replaceLineBreaks(String s) {
		return replaceLineBreaksWithSpecifiedString(s, "&nbsp;<br />");
	}

	public static String replaceLineBreaksWithSpecifiedString(String originalString, String replacementForLineBreaks) {
		String replacedString = originalString.replaceAll("\n", replacementForLineBreaks);
		return replacedString;
	}

	/**
	 * method that converts a delimited string to a list of strings separated by
	 * the delimitor.
	 * 
	 * @param string
	 *            the string to modify
	 * @param delimitor
	 *            the delimitor to separate string
	 * @return List
	 */
	public static List<String> string2List(String string, String delimitor) {
		List<String> list = new Vector<String>();
		String[] a = string.split(delimitor);
		for (String s : a) {
			list.add(s);
		}

		return list;
	}

	public static String toLowerNoSpaceCase(String s) {
		char[] chars = s.trim().toLowerCase().toCharArray();
		String newString = "";
		for (int i = 0; i < chars.length; i++) {
			if (!Character.isWhitespace(chars[i])) {
				newString = newString + chars[i];
			}
		}

		return newString;
	}

	/**
	 * method that takes a string and converts the first character to uppercase.
	 * 
	 * @param s
	 *            the string to modify.
	 * @return String
	 */
	public static String toTitleCase(String s) {
		char[] chars = s.trim().toLowerCase().toCharArray();
		boolean found = false;

		for (int i = 0; i < chars.length; i++) {
			if (!found && Character.isLetter(chars[i])) {
				chars[i] = Character.toUpperCase(chars[i]);
				found = true;
			} else if (Character.isWhitespace(chars[i])) {
				found = false;
			}
		}

		return String.valueOf(chars);
	}

	public static String trimToLength(String textToTrim, int length) {
		String text = textToTrim;
		if ((length > 0) && !StringUtils.isEmpty(text)) {
			if (textToTrim.length() > length) {
				text = textToTrim.substring(0, length - 1);
			}
		}
		return text;
	}

	public static String wordToTitleCase(String s) {
		StringBuilder b = new StringBuilder(s.toLowerCase());
		Character ch = b.charAt(0);
		b.setCharAt(0, Character.toUpperCase(ch));
		return b.toString();
	}

	public static String encodeUTF8(String input) {
		try {
			return URLEncoder.encode(input, "UTF-8");
		} catch (Exception ignored) {
		}
		return null;
	}
}