package org.trescal.cwms.core.tools;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.init.ScriptUtils;

public class UpdateDatabaseWithScriptFiles {

	private static final Logger logger = LoggerFactory.getLogger(UpdateDatabaseWithScriptFiles.class);
	public static final String PROPERTIES_FILE = "properties/atlas-develop.properties";
	public static final String SCRIPTS_FOLDER = "scripts/spain/cleaning";

	private String dbDriver;
	private String dbName;
	private String dbUser;
	private String dbPassword;

	public UpdateDatabaseWithScriptFiles() {
		Properties properties = new Properties();
		try (InputStream is = this.getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE)) {
			if (is == null)
				throw new RuntimeException("Could not load resource : "+PROPERTIES_FILE);
			properties.load(is);
			this.dbDriver = properties.getProperty("db.driver");
			this.dbName = properties.getProperty("db.name");
			this.dbUser = properties.getProperty("db.user");
			this.dbPassword = properties.getProperty("db.password");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void execute(String url) throws Exception {
		// Initializing driver class using 'old' backwards compatible way for
		// DriverManager
		@SuppressWarnings("unused")
		Class<?> driverClass = Class.forName(dbDriver);
		Connection jdbcConnection;
		if (StringUtils.isNoneBlank(url)) {
			logger.info("connecting to " + url);
			jdbcConnection = DriverManager.getConnection(url, dbUser, dbPassword);
		} else {
			logger.info("connecting to " + dbName);
			jdbcConnection = DriverManager.getConnection(dbName, dbUser, dbPassword);
		}
		logger.info("Connected database successfully...");

		// get latest db version
		int latestDbVersion = getLatestDbVersion(jdbcConnection);

		if (latestDbVersion != 0) {
			List<File> scriptsToExecute = getScriptsToExecute(latestDbVersion);
			logger.info(scriptsToExecute.size() + " scripts to be executed");
			for (File file : scriptsToExecute) {
				logger.info("executing script : " + file.getName() + " ----------------------------------");

				// read script
				String script = new String(Files.readAllBytes(file.toPath())).replaceAll("(?i)go", "GO");

				List<String> statements = new LinkedList<>();
				// split
				ScriptUtils.splitSqlScript(script, "GO", statements);

				Statement stmt = jdbcConnection.createStatement();
				try {
					for (String statement : statements) {
						stmt.execute(statement);
						int rowsAffected = stmt.getUpdateCount();
						if (logger.isDebugEnabled()) {
							logger.debug(rowsAffected + " returned as update count for SQL: " + statement);
							SQLWarning warningToLog = stmt.getWarnings();
							while (warningToLog != null) {
								logger.debug("SQLWarning ignored: SQL state '" + warningToLog.getSQLState()
										+ "', error code '" + warningToLog.getErrorCode() + "', message ["
										+ warningToLog.getMessage() + "]");
								warningToLog = warningToLog.getNextWarning();
							}
						}
					}
				} finally {
					try {
						stmt.close();
					} catch (Throwable ex) {
						logger.debug("Could not close JDBC Statement", ex);
					}
				}

			}

		}
	}

	private List<File> getScriptsToExecute(int latestDbVersion) throws IOException, URISyntaxException {

		return Files.list(Paths.get(this.getClass().getClassLoader().getResource(SCRIPTS_FOLDER).toURI())).filter(
			path -> {
				File f = path.toFile();
				String sub = StringUtils.substringBefore(f.getName(), ".");
				sub = sub.split("[\\D]")[0].trim();
				int fileNumber = Integer.parseInt(sub);
				return fileNumber > latestDbVersion;
			}).map(Path::toFile).collect(Collectors.toList());
	}

	private Integer getLatestDbVersion(Connection jdbcConnection) throws SQLException {
		String sql = "select  max(version) from dbversion";
		Statement smt = jdbcConnection.createStatement();
		ResultSet rs = smt.executeQuery(sql);
		int ver = 0;
		while (rs.next()) {
			ver = rs.getInt(1);
			logger.info("Latest db version : " + ver);
		}
		return ver;
	}

	public static void main(String[] args) throws IOException {

		String url = "";
		if (args.length > 0)
			url = args[0];

		try {
			new UpdateDatabaseWithScriptFiles().execute(url);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

}
