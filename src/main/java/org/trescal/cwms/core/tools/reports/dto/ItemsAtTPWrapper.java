package org.trescal.cwms.core.tools.reports.dto;

import java.util.Locale;

import javax.persistence.Transient;

import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;

public class ItemsAtTPWrapper
{
	private int compId;
	private String compName;
	private int itemCount;
	private String subdivName; 
	private int subdivId;
	private String companyRole; 
	private String countryCode; 
	private String country;
	
	public ItemsAtTPWrapper(int compId, String compName, int subdivId, String subdivName, CompanyRole companyRole, String countryCode, Number itemCount,
			String country)
	{
		this.compId = compId;
		this.compName = compName;
		this.subdivId = subdivId;
		this.subdivName = subdivName;
		this.companyRole = companyRole.getMessage();
		this.countryCode = countryCode;
		this.itemCount = itemCount.intValue();
		this.country = country;
	}

	public int getCompId()
	{
		return this.compId;
	}

	public String getCompName()
	{
		return this.compName;
	}

	public int getItemCount()
	{
		return this.itemCount;
	}

	public int getSubdivId() {
		return subdivId;
	}

	public String getSubdivName() {
		return subdivName;
	}

	public String getCompanyRole() {
		return companyRole;
	}

	public String getCountryCode() {
		return countryCode;
	}

	@Transient
	public String getLocalizedCountryName() {
		return (new Locale("", countryCode)).getDisplayCountry(LocaleContextHolder.getLocale());
	}

	public void setCompId(int compId)
	{
		this.compId = compId;
	}

	public void setCompName(String compName)
	{
		this.compName = compName;
	}

	public void setItemCount(int itemCount)
	{
		this.itemCount = itemCount;
	}

	public void setSubdivId(int subdivId) {
		this.subdivId = subdivId;
	}

	public void setSubdivName(String subdivName) {
		this.subdivName = subdivName;
	}

	public void setCompanyRole(String corole) {
		this.companyRole = corole;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = new Locale("", countryCode).getDisplayCountry(LocaleContextHolder.getLocale());
	}
}
