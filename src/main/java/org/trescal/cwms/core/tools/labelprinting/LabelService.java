package org.trescal.cwms.core.tools.labelprinting;

import java.io.OutputStream;
import java.util.Locale;

import org.eclipse.birt.report.engine.api.EngineException;
import org.trescal.cwms.core.company.entity.company.Company;

/**
 * Interface implemented by all services that perform certifiate label printing functions.
 * 
 * @author jamiev
 * GB 2016-12-06 - Removed Dymo printing calls as now separated into Ajax/Json controller.
 */
public interface LabelService
{
	PrintRequest printAssetLabel(int assetId, boolean includeIPAddresses);

	PrintRequest printCertificateLabel(int certLinkId, String templateKey, boolean includeRecallDate);

	PrintRequest printCertificateLabelForSelectedLinks(int[] certLinkIds, String templateKey, boolean includeRecallDate);

	PrintRequest printInstDateLabel(String templateKey, int plantId, String date, String serial);

	PrintRequest printNonDataBasedLabel(String templateKey, Integer quantity);

	PrintRequest printStringDateLabel(String templateKey, String str, String date);

	// Small Certificate Label - by cert link ID
	boolean printCertificateLabel(int certLinkId, String templateKey, boolean includeRecallDate, Locale locale,
			OutputStream out, Company company) throws EngineException;

	// Regular label - by cert link ID
	boolean printCalSpecificLabelForJobItem(int certLinkId, boolean includeRecallDate, Locale locale,  
			OutputStream out, Company company) throws EngineException;

	boolean printInstDateLabel(String templateKey, int plantId, String date, String serial, OutputStream out, Locale locale, Company company) throws EngineException;

	boolean printNonDataBasedLabel(String templateKey, Integer quantity,OutputStream out, Locale locale, Company company) throws EngineException;

	boolean printStringDateLabel(String templateKey, String str, String date,OutputStream out, Locale locale, Company company) throws EngineException;

	boolean printSpecificLabel(int certLinkId, String templateKey, boolean includeRecallDate, 
			String technician,String correction,String incertainty,String exptrescal,String icp,String quality,
			Locale locale, OutputStream out, Company company) throws EngineException;

	boolean printCustCalLabel(int certId, String templateKey, int instrumentId, Locale locale, OutputStream out, Company comp)
			throws EngineException;
}
