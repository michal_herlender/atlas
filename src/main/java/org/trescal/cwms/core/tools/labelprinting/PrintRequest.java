package org.trescal.cwms.core.tools.labelprinting;

import java.util.Map;

public class PrintRequest
{
	private String message;
	private Map<String, Object> printData;
	private boolean success;

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return this.message;
	}

	/**
	 * @return the printData
	 */
	public Map<String, Object> getPrintData()
	{
		return this.printData;
	}

	/**
	 * @return the success
	 */
	public boolean isSuccess()
	{
		return this.success;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}

	/**
	 * @param printData the printData to set
	 */
	public void setPrintData(Map<String, Object> printData)
	{
		this.printData = printData;
	}

	/**
	 * @param success the success to set
	 */
	public void setSuccess(boolean success)
	{
		this.success = success;
	}
}
