package org.trescal.cwms.core.tools.transitcalendar;

import java.time.LocalDate;

public abstract class Transit {
	private LocalDate transitDate;

	public LocalDate getTransitDate() {
		return this.transitDate;
	}

	public abstract boolean isBase();

	public void setTransitDate(LocalDate transitDate) {
		this.transitDate = transitDate;
	}

	@Override
	public abstract String toString();
}