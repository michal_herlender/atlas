package org.trescal.cwms.core.tools;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CompressWhitespace
{
	/**
	 * Remove whitespace from raw markup
	 * 
	 * @param rawMarkup
	 * @return rawMarkup with compressed whitespace.
	 */
	protected static String compressWhitespace(String rawMarkup)
	{
		// We don't want to compress whitespace inside <pre> tags, so we look
		// for matches and:
		//  - Do whitespace compression on everything before the first match.
		//  - Append the <pre>.*?</pre> match with no compression.
		//  - Loop to find the next match.
		//  - Append with compression everything between the two matches.
		//  - Repeat until no match, then special-case the fragment after the
		//    last <pre>.
		
		Pattern preBlock = Pattern.compile("<pre>.*?</pre>", Pattern.DOTALL | Pattern.MULTILINE);
		Matcher m = preBlock.matcher(rawMarkup);
		int lastend = 0;
		StringBuffer sb = null;
		while (true)
		{
			boolean matched = m.find();
			String nonPre = matched
					? rawMarkup.substring(lastend, m.start())
					: rawMarkup.substring(lastend);
			nonPre = nonPre.replaceAll("[ \\t]+", " ");
			nonPre = nonPre.replaceAll("( ?[\\r\\n] ?)+", "\n");
			
			// Don't create a StringBuffer if we don't actually need one.
			// This optimises the trivial common case where there is no <pre>
			// tag at all down to just doing the replaceAlls above.
			if (lastend == 0)
			{
				if (matched)
				{
					sb = new StringBuffer(rawMarkup.length());
				}
				else
				{
					return nonPre;
				}
			}
			sb.append(nonPre);
			if (matched)
			{
				sb.append(m.group());
				lastend = m.end();
			}
			else
			{
				break;
			}
		}
		return sb.toString();
	}
}
