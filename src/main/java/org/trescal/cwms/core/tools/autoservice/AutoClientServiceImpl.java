package org.trescal.cwms.core.tools.autoservice;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.ChaseClientApproval;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultBooleanType;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_AutoServiceClient;

@Service("AutoClientService")
public class AutoClientServiceImpl extends AutoService implements AutoClientService
{
	@Autowired
	private ChaseClientApproval chaseClientApproval;
	@Autowired
	private HookInterceptor_AutoServiceClient interceptorClient;
	
	public void chaseClients()
	{
		if (this.schTaskServ.scheduledTaskIsActive(this.getClass().getName(), Thread.currentThread().getStackTrace()))
		{
			// find items requiring chase grouped by contact
			Map<Contact, List<JobItem>> toChase = this.jiServ.getItemsRequiringClientChase();

			// send chase e-mails to clients
			AutoServiceSummary summary = this.chase(toChase);
			
			// send summary e-mail to us
			boolean summarySent = this.sendSummary(summary, AutoServiceType.CLIENT);

			// report results of task run
			String message = summary.getEmailsSent() + " emails sent to clients, ";
			message = message
					+ ((summarySent) ? "summary successfully sent" : "summary failed to send");
			this.schTaskServ.reportOnScheduledTaskRun(this.getClass().getName(), Thread.currentThread().getStackTrace(), true, message);
		}
		else
		{
			logger.info("Auto-Service Clients not running: scheduled task cannot be found or is turned off");
		}
	}
	
	@Override
	protected SystemDefaultBooleanType getChaseDefault() {
		return chaseClientApproval;
	}
	
	@Override
	protected void sendEmail(Contact con, Map<Integer, List<JobItem>> itemsToChaseNow, String fromAddress, String replyToAddress, int totalChases) {
		this.sendChaseEmail(con, itemsToChaseNow, fromAddress, replyToAddress, totalChases, AutoServiceType.CLIENT);
		interceptorClient.recordAction(itemsToChaseNow);
	}
}