package org.trescal.cwms.core.tools.labelprinting.dymo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.tools.labelprinting.dymo.DymoLabelJobItem;
import org.trescal.cwms.core.tools.labelprinting.dymo.form.DymoLabelJobItemForm;

@Controller @JsonController
public class DymoLabelJobItemController {
	@Autowired
	private DymoLabelJobItem labelJobItem;
	@Autowired
	private JobService jobService;
	
	public static final String REQUEST_URL = "/dymojobitemlabels.json";
	public static final String VIEW_NAME = "trescal/core/tools/labelprinting/dymo/multiplejobitemlabels";
	public static final String FORM_NAME = "form";
	
	@ModelAttribute(FORM_NAME)
	public DymoLabelJobItemForm formBackingObject(@RequestParam(name="jobid", required=true) int jobid) {
		DymoLabelJobItemForm form = new DymoLabelJobItemForm();
		form.setCopiesAccessoryLabel(new HashMap<>());
		form.setCopiesJobItemLabel(new HashMap<>());
		form.setPrintAccessoryLabel(new HashMap<>());
		form.setPrintJobItemLabel(new HashMap<>());
		form.setJobId(jobid);
		Job job = this.jobService.get(jobid);
		for(JobItem jobItem : job.getItems()) {
			form.getPrintJobItemLabel().put(jobItem.getJobItemId(), true);
			form.getCopiesJobItemLabel().put(jobItem.getJobItemId(), 1);
			if (labelJobItem.hasAccessories(jobItem)) {
				form.getPrintAccessoryLabel().put(jobItem.getJobItemId(), true);
				form.getCopiesAccessoryLabel().put(jobItem.getJobItemId(), 1);
			}
			else {
				form.getPrintAccessoryLabel().put(jobItem.getJobItemId(), false);
				form.getCopiesAccessoryLabel().put(jobItem.getJobItemId(), 0);
			}
		}
		return form;
	}
	
	@RequestMapping(value=REQUEST_URL, method=RequestMethod.GET)
	public String showView(Locale locale, Model model, @RequestParam(name="jobid", required=true) int jobid) {
		Job job = this.jobService.get(jobid); 
		model.addAttribute("job", job);
		Map<Integer, String> accessories = new HashMap<>();
		for(JobItem jobItem : job.getItems()) {
			if (labelJobItem.hasAccessories(jobItem)) {
				accessories.put(jobItem.getJobItemId(), labelJobItem.formatAccessories(jobItem, locale));
			}
			else {
				accessories.put(jobItem.getJobItemId(), labelJobItem.formatNoAccessories(jobItem, locale));				
			}
		}
		model.addAttribute("accessories", accessories);
		return VIEW_NAME;
	}
	
	// , consumes={MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE}
	// , produces={Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8}
	@ResponseBody
	@RequestMapping(value=REQUEST_URL, method=RequestMethod.POST)
	public List<Object> onSubmit(Locale locale, 
			@RequestParam(name="jobid", required=true) int jobid,
			@ModelAttribute(FORM_NAME) DymoLabelJobItemForm form) {
		Job job = this.jobService.get(jobid);
		List<Object> result = new ArrayList<>();
		for (JobItem jobItem : job.getItems()) {
			int jobItemId = jobItem.getJobItemId();
			if (form.getPrintJobItemLabel().get(jobItemId)) {
				int copies = form.getCopiesJobItemLabel().get(jobItemId);
				result.add(labelJobItem.getLabelDto(jobItem, copies, locale,true));
			}
			if (form.getPrintAccessoryLabel().get(jobItem.getJobItemId())) {
				int copies = form.getCopiesAccessoryLabel().get(jobItemId);
				result.add(labelJobItem.getLabelDtoAccessories(jobItem, copies, locale,true));
			}
		}
		System.out.println("Returning result with element count : "+result.size());
		return result;
	}
}