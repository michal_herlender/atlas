package org.trescal.cwms.core.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;

/**
 * Generates a username and password for a {@link User}.
 */
@Component
public class LoginGenerator
{
	@Value("#{props['cwms.config.usernameMaxLength']}")
	private Integer usernameMaxLength;
	@Value("#{props['cwms.config.passwordlength']}")
	private Integer passwordLength;
	@Autowired
	private UserService userService;
	
	public String generatePassword()
	{
		List<Character> validChars = new ArrayList<Character>();
		for(char c='a'; c<='z'; c++) validChars.add(c);
		for(char c='A'; c<='Z'; c++) validChars.add(c);
		for(char c='0'; c<='9'; c++) validChars.add(c);
		for(char c : "$%+-*#!?".toCharArray()) validChars.add(c);
		Stream<String> pwStream = new Random().ints(passwordLength, 0, validChars.size())
				.mapToObj(i -> validChars.get(i).toString());
		return pwStream.collect(Collectors.joining("", "", ""));
	}
	
	/**
	 * Generate a new username and password for the given {@link Contact}.
	 * 
	 * @param con the {@link Contact}, not null.
	 * @param usrServ a {@link UserService}.
	 * @return {@link HashMap} with a "username" and "password" entry.
	 */
	public String generateUsername(Contact con)
	{
		int addFirstNameCount = 1;
		int addNumberCount = 1;
		String firstName = con.getFirstName() == null ? "" : con.getFirstName().toLowerCase().trim();
		String lastName = con.getLastName() == null ? "" : con.getLastName().toLowerCase().trim();
		firstName = stripBadCharacters(firstName);
		lastName = stripBadCharacters(lastName);
		// try and create a user name like jamiev, richardd
		String username = this.userGenByLastNameInitial(firstName, lastName, addFirstNameCount, 0);
		if (userService.get(username) != null) {
			// try and create a user name using full first name and last
			// name
			username = this.userGenByFullName(firstName, lastName, usernameMaxLength);
			while (userService.get(username) != null) {
				addFirstNameCount++;
				if (addFirstNameCount > 3) addNumberCount++;
				username = this.userGenByLastNameInitial(firstName, lastName, addFirstNameCount, addNumberCount);
			}
		}
		return username;
	}
	
	// remove apostrophes, whitespance - basically any non alphanumeric
	private String stripBadCharacters(String toClean)
	{
		if (toClean != null)
		{
			// remove any non-numeric/whitespace/alphabet chars
			toClean = toClean.replaceAll("[^a-zA-Z 0-9]", "");

			// check for any extended whitespace and remove
			if (toClean.contains(" "))
			{
				String[] d = toClean.split(" ");
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < d.length; i++)
				{
					if (!d[i].trim().equals(""))
					{
						sb.append(d[i]);
					}
				}
				toClean = sb.toString();
			}
		}
		return toClean;
	}
	
	private String trimToMaxLength(String username)
	{
		if (username.length() > usernameMaxLength - 1)
		{
			username = username.substring(0, usernameMaxLength - 1);
		}
		return username;
	}
	
	protected String userGenByFullName(String firstName, String lastName, int maxLength)
	{
		String username = firstName.concat(lastName);
		return trimToMaxLength(username);
	}

	protected String userGenByLastNameInitial(String firstName, String lastName, int addFirstNameCount, int addNumberCount)
	{
		String username = lastName;
		if (lastName.equals(""))
		{
			// last name is blank, try and use the first name only
			username = trimToMaxLength(firstName);
			if (addNumberCount > 1)
			{
				// first name is already in use, append an incremeting number to
				// the end until we get a unique
				username = username + addNumberCount;
			}
			return username;
		}
		else if ((firstName.length() >= addFirstNameCount)
				&& (addFirstNameCount < 4))
		{
			username = username.concat(firstName.substring(0, addFirstNameCount));

			if (username.length() > usernameMaxLength)
			{
				username = username.substring(0, usernameMaxLength);
			}

			return trimToMaxLength(username);
		}
		else
		{
			if (firstName.length() > 0)
			{
				username = username + firstName.charAt(0);
			}
			if (addNumberCount > 0)
			{
				username = username + addNumberCount;
			}
			return username;
		}
	}
}
