package org.trescal.cwms.core.tools.fileupload2.utils.validfiletype.db;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.tools.fileupload2.utils.validfiletype.ValidFileType;

public class ValidFileTypeServiceImpl implements ValidFileTypeService
{
	ValidFileTypeDao validFileTypeDao;

	public void deleteValidFileType(ValidFileType validfiletype)
	{
		this.validFileTypeDao.remove(validfiletype);
	}

	public ValidFileType findValidFileType(int id)
	{
		return this.validFileTypeDao.find(id);
	}

	public List<ValidFileType> getAllValidFileTypes()
	{
		return this.validFileTypeDao.findAll();
	}

	public void insertValidFileType(ValidFileType ValidFileType)
	{
		this.validFileTypeDao.persist(ValidFileType);
	}

	public boolean isValidFileType(MultipartFile file, ValidFileType.FileTypeUsage usage)
	{
		String filename = file.getOriginalFilename();
		String ext = (filename.lastIndexOf(".") == -1) ? null : filename.substring(filename.lastIndexOf(".") + 1, filename.length());

		if (ext == null)
		{
			return false;
		}
		else
		{
			return this.validFileTypeDao.isValidFileType(ext, usage);
		}
	}

	public void saveOrUpdateValidFileType(ValidFileType validfiletype)
	{
		this.validFileTypeDao.saveOrUpdate(validfiletype);
	}

	public void setValidFileTypeDao(ValidFileTypeDao validFileTypeDao)
	{
		this.validFileTypeDao = validFileTypeDao;
	}

	public void updateValidFileType(ValidFileType ValidFileType)
	{
		this.validFileTypeDao.update(ValidFileType);
	}
}