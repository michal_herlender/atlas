package org.trescal.cwms.core.tools.secretkey.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.tools.secretkey.SecretKey;

public interface SecretKeyService extends BaseService<SecretKey, byte[]>
{
	SecretKey getKey();
}