package org.trescal.cwms.core.tools;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

@Repository("updateRequiredViewsDao")
public class UpdateRequiredViewsDao {
	
	@PersistenceContext(unitName="entityManagerFactory")
	private EntityManager entityManager;
	
	public int runSQLUpdate(String sql) {
		Query query = entityManager.createNativeQuery(sql);
		return query.executeUpdate();
	}
}