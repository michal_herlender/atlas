package org.trescal.cwms.core.tools.autoservice;

import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItemAgeComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItemComparator;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.SystemTime;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TreeSet;

/**
 * A tool that is designed to be put on the velocity context when creating
 * auto-service e-mails that provides some functions too complex for coding in
 * velocity
 * 
 * GB note 2018-08-08 : We could refactor if re-enabling to do calculations first, and put calculated dates in model.
 * 
 * @author JamieV
 */
public class AutoServiceCalculationTool
{
	private static DeliveryItem findItemOnDeliveryToCompany(JobItem ji, Company comp)
	{
		int compId = comp.getCoid();

		TreeSet<JobDeliveryItem> dis = new TreeSet<>(new JobDeliveryItemAgeComparator());
		dis.addAll(ji.getDeliveryItems());

		// loop backwards through recent delivery items
		for (JobDeliveryItem jdi : dis.descendingSet())
		{
			// if the delivery has a freehand contact/company
			if ((jdi.getDelivery().getContact() == null)
					&& (jdi.getDelivery().getFreehandContact() != null)
					&& (jdi.getDelivery().getFreehandContact().getCompany() != null))
			{
				// just try to match the company name (yuck!)
				if (comp.getConame().trim().equalsIgnoreCase(jdi.getDelivery().getFreehandContact().getCompany().trim()))
				{
					return jdi;
				}
			}
			// only if the purchase order and delivery address match
			else if (jdi.getDelivery().getContact().getSub().getComp().getCoid() == compId)
			{
				// return the delivery item
				return jdi;
			}
		}

		// nope, no deliveries found on this item to this company
		return null;
	}

	/**
	 * Calculates how long the given {@link JobItem} has been away at the
	 * supplier using the relevant {@link PurchaseOrder}'s issue date
	 * 
	 * @param ji the {@link JobItem}
	 * @return the amount of days the item has been away
	 */
	public static int getDaysAwayAtSupplier(JobItem ji, boolean chasing) {
		PurchaseOrderItem lastPoItem = getSupplierPurchaseOrderItem(ji);

		if ((lastPoItem != null) && lastPoItem.getOrder().isIssued()) {
			LocalDate today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
			return (int) lastPoItem.getOrder().getIssuedate().until(today, ChronoUnit.DAYS);
		} else
		{
			DeliveryItem di = getSupplierDeliveryNoteItem(ji, chasing);

			if (di != null)
			{
				Date today = SystemTime.nowDate();
				Calendar delDate = new GregorianCalendar();
				delDate.setTime(DateTools.dateFromLocalDate(di.getDelivery().getDeliverydate()));

				return DateTools.getDaysDifference(delDate.getTime(), today);
			}
		}

		return 0;
	}

	public static Date getOurDeliveryDate(JobItem ji, boolean chasing)
	{
		DeliveryItem delItem = getSupplierDeliveryNoteItem(ji, chasing);

		if (delItem != null) {
			return DateTools.dateFromLocalDate(delItem.getDelivery().getDeliverydate());
		}

		return null;
	}

	public static Date getPromisedReturnDate(JobItem ji) {
		PurchaseOrderItem lastPoItem = getSupplierPurchaseOrderItem(ji);

		if (lastPoItem != null) {
			return DateTools.dateFromLocalDate(lastPoItem.getDeliveryDate());
		}

		return null;
	}

	public static DeliveryItem getSupplierDeliveryNoteItem(JobItem ji, boolean chasing) {
		DeliveryItem di = null;

		// get address that last purchase order went to
		PurchaseOrderItem lastPoItem = getSupplierPurchaseOrderItem(ji);

		if (lastPoItem != null) {
			Company comp = lastPoItem.getOrder().getContact().getSub().getComp();

			di = findItemOnDeliveryToCompany(ji, comp);
		}

		// no PO yet, but item may be at supplier before this is raised (this
		// following if block is helpful for the Items At Third Party report,
		// but should NOT be used when chasing suppliers, hence the 'chasing'
		// boolean)
		if ((di == null) && (ji.getCurrentAddr() != null) && !chasing) {
			Company comp = ji.getCurrentAddr().getSub().getComp();

			di = findItemOnDeliveryToCompany(ji, comp);
		}

		return di;
	}

	public static PurchaseOrderItem getSupplierPurchaseOrderItem(JobItem ji) {
		if ((ji.getPurchaseOrderItems() != null)
			&& (ji.getPurchaseOrderItems().size() > 0)) {
			// get item on last purchase order when sorted by creation date
			TreeSet<PurchaseOrderJobItem> pos = new TreeSet<>(new PurchaseOrderJobItemComparator());
			pos.addAll(ji.getPurchaseOrderItems());
			PurchaseOrderItem lastPoItem = pos.last().getPoitem();

			int lastPurOrdCompId = lastPoItem.getOrder().getContact().getSub().getComp().getCoid();
			if (ji.getCurrentAddr() != null) {
				int currentCompId = ji.getCurrentAddr().getSub().getComp().getCoid();

				// if the item is at the same company the last PO was issued to
				if (currentCompId == lastPurOrdCompId)
				{
					// we're safe to return the item
					return lastPoItem;
				}
			}
		}

		// otherwise we're screwed!
		return null;
	}
}