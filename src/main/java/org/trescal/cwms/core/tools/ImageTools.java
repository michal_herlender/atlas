package org.trescal.cwms.core.tools;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.trescal.cwms.core.documents.images.image.Image;
import org.trescal.cwms.files.images.entity.ImageThumbData;

public class ImageTools
{
	
	public static Image generateThumbNailImageDb(double maxThumbnailEdge, Image image){

		try{
			InputStream in = new ByteArrayInputStream(image.getFileData());
			BufferedImage img = ImageIO.read(in);
			
			int longestEdge = (img.getHeight() >= img.getWidth()) ? img.getHeight() : img.getWidth();
			double scale = maxThumbnailEdge / longestEdge;
			int width = (int) (img.getWidth() * scale);
			int height = (int) (img.getHeight() * scale);

			// get the scaled image
			BufferedImage scaledImage = img;
			
			if (maxThumbnailEdge < longestEdge)
			{
				scaledImage = getScaledInstance(img, width, height, RenderingHints.VALUE_INTERPOLATION_BICUBIC, true);
			}
			
			String ext = (image.getFileName().lastIndexOf(".") == -1) ? "" : image.getFileName().substring(image.getFileName().lastIndexOf(".") + 1, image.getFileName().length());
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(scaledImage, ext, baos);
			baos.flush();
			byte[] imgBytes = baos.toByteArray();
			baos.close();
			ImageThumbData itd = new ImageThumbData();
			itd.setThumbData(imgBytes);
			image.setThumbFile(itd);
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		return image;
		
	}
	
	@Deprecated
	public static void generateThumbNailImage(double maxThumbnailEdge, File srcFile) throws IOException
	{
		BufferedImage img = ImageIO.read(new FileInputStream(srcFile));

		int longestEdge = (img.getHeight() >= img.getWidth()) ? img.getHeight() : img.getWidth();

		double scale = maxThumbnailEdge / longestEdge;

		// work out what to scale the image to
		int width = (int) (img.getWidth() * scale);
		int height = (int) (img.getHeight() * scale);

		// get the scaled image
		BufferedImage image = img;

		// only resize the image if the image is larger than the thumb nail
		// width, otherwise just right another copy of the image and name it the
		// thumbnail version
		if (maxThumbnailEdge < longestEdge)
		{
			image = getScaledInstance(img, width, height, RenderingHints.VALUE_INTERPOLATION_BICUBIC, true);
		}

		// work out the new name for the file - just append a '-tb' before the
		// extension
		String name = srcFile.getAbsolutePath();
		if (name.indexOf('.') > -1)
		{
			name = name.substring(0, name.lastIndexOf('.')).concat("-tb").concat(".").concat(FileTools.getFileExtension(srcFile));
		}
		else
		{
			name = name.concat("-tb");
		}
		File file = new File(name);

		// write the new image out
		ImageIO.write(image, FileTools.getFileExtension(srcFile), file);
	}

	/**
	 * Convenience method that returns a scaled instance of the provided
	 * {@code BufferedImage}.
	 * 
	 * @param img the original image to be scaled
	 * @param targetWidth the desired width of the scaled instance, in pixels
	 * @param targetHeight the desired height of the scaled instance, in pixels
	 * @param hint one of the rendering hints that corresponds to
	 *        {@code RenderingHints.KEY_INTERPOLATION} (e.g.
	 *        {@code RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR},
	 *        {@code RenderingHints.VALUE_INTERPOLATION_BILINEAR},
	 *        {@code RenderingHints.VALUE_INTERPOLATION_BICUBIC})
	 * @param higherQuality if true, this method will use a multi-step scaling
	 *        technique that provides higher quality than the usual one-step
	 *        technique (only useful in downscaling cases, where
	 *        {@code targetWidth} or {@code targetHeight} is smaller than the
	 *        original dimensions, and generally only when the {@code BILINEAR}
	 *        hint is specified)
	 * @return a scaled version of the original {@code BufferedImage}
	 */
	public static BufferedImage getScaledInstance(BufferedImage img, int targetWidth, int targetHeight, Object hint, boolean higherQuality)
	{
		int type = (img.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
		BufferedImage ret = img;
		int w, h;
		if (higherQuality)
		{
			// Use multi-step technique: start with original size, then
			// scale down in multiple passes with drawImage()
			// until the target size is reached
			w = img.getWidth();
			h = img.getHeight();
		}
		else
		{
			// Use one-step technique: scale directly from original
			// size to target size with a single drawImage() call
			w = targetWidth;
			h = targetHeight;
		}

		do
		{
			if (higherQuality && (w > targetWidth))
			{
				w /= 2;
				if (w < targetWidth)
				{
					w = targetWidth;
				}
			}

			if (higherQuality && (h > targetHeight))
			{
				h /= 2;
				if (h < targetHeight)
				{
					h = targetHeight;
				}
			}

			BufferedImage tmp = new BufferedImage(w, h, type);
			Graphics2D g2 = tmp.createGraphics();
			g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
			g2.drawImage(ret, 0, 0, w, h, null);
			g2.dispose();

			ret = tmp;
		}
		while ((w != targetWidth) || (h != targetHeight));

		return ret;
	}
}
