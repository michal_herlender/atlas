package org.trescal.cwms.core.tools;

import java.math.BigDecimal;
import java.text.Format;
import java.util.Map;
import java.util.TreeMap;

import com.ibm.icu.text.DecimalFormat;
import com.ibm.icu.text.DecimalFormatSymbols;

public class NumberTools
{
	public static String appendAptLettersToNumber(int number)
	{
		int digit = number % 10;
		int last2digits = number % 100;

		switch (digit)
		{
			case 1:
				switch (last2digits)
				{
					case 11:
						return number + "th";
					default:
						return number + "st";
				}
			case 2:
				switch (last2digits)
				{
					case 12:
						return number + "th";
					default:
						return number + "nd";
				}
			case 3:
				switch (last2digits)
				{
					case 13:
						return number + "th";
					default:
						return number + "rd";
				}
			default:
				return number + "th";
		}
	}

	public static String convertDoubleToFormattedString(String d)
	{
		double db = Double.parseDouble(d);

		Format formatter = new DecimalFormat("#.##########");

		String formattedString = formatter.format(db);

		return formattedString;
	}

	public static String displayBigDecimalTo2DecimalPlaces(BigDecimal bd)
	{
	    DecimalFormatSymbols symbols = new DecimalFormatSymbols();
	    symbols.setDecimalSeparator('.');
		DecimalFormat dFormat = new DecimalFormat("0.00",symbols);
		return dFormat.format(bd);
	}

	/**
	 * Prints a double value to 2 decimal places, handy for cost and currency
	 * business. e.g. (0.0 -> 0.00, 2.5 -> 2.50, 2.555 -> 2.6)
	 * 
	 * @param d the double value to format
	 * @return the formatted value to 2 decimal places
	 */
	public static String displayDoubleTo2DecimalPlaces(double d)
	{
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		DecimalFormat dFormat = new DecimalFormat("0.00",symbols);
		return dFormat.format(d);
	}

	public static boolean doubleIsZero(Double db)
	{
		if ((db.doubleValue() < 0) || (db.doubleValue() > 0))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	/**
	 * Gets the double percentage value of the value as a percentage of the
	 * total. Returns result formatted to one decimal place.
	 * 
	 * @param value
	 * @param total
	 * @return percentage value.
	 */
	public static double getPct(double value, double total, double defaultVal)
	{
		Double d = value / total * 1000 + 0.5;
		return d.intValue() / 10.0;
	}

	/**
	 * Tests if the given text can be safely converted into a {@link Double}.
	 * 
	 * @param text the text to convert.
	 * @return true if the conversion can take place.
	 */
	public static boolean isADouble(String text)
	{
		try
		{
			Double.parseDouble(text);
			return true;
		}
		catch (NumberFormatException e)
		{
			return false;
		}
	}

	/**
	 * Cludge method that tests if the given text can be safely converted to an
	 * {@link Integer}.
	 * 
	 * @param text the text to convert.
	 * @return true if the conversion can take place.
	 */
	public static boolean isAnInteger(String text)
	{
		try
		{
			Integer.parseInt(text);
			return true;
		}
		catch (NumberFormatException e)
		{
			return false;
		}
	}

	/**
	 * method that tests if the given text is a valid telephone extension
	 * 
	 * @param text the text to check.
	 * @return true if the phone number is valid.
	 */
	public static boolean isValidExtension(String text)
	{
		char[] numbers = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };

		try
		{
			char[] chars = text.toCharArray();

			for (char c : chars)
			{
				boolean match = false;
				for (char n : numbers)
				{
					if (c == n)
					{
						match = true;
					}
				}
				if (!match)
				{
					return false;
				}
			}

			return true;
		}
		catch (NumberFormatException e)
		{
			return false;
		}
	}

	/**
	 * method that tests if the given text is a valid telephone number
	 * 
	 * @param text the text to check.
	 * @return true if the phone number is valid.
	 */
	public static boolean isValidPhoneNumber(String text)
	{
		char[] numbers = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '+', ' ', '(', ')' };

		try
		{
			char[] chars = text.toCharArray();

			for (char c : chars)
			{
				boolean match = false;
				for (char n : numbers)
				{
					if (c == n)
					{
						match = true;
					}
				}
				if (!match)
				{
					return false;
				}
			}

			return true;
		}
		catch (NumberFormatException e)
		{
			return false;
		}
	}

	/**
	 * Returns the given {@link Double} as a {@link String}, without decimal
	 * places if the double has only zeros after the decimal place. e.g. a
	 * double of 20.00 returns "20", a double of 20.85 returns "20.85"
	 * 
	 * @param d the {@link Double} to convert
	 * @return the {@link String}
	 */
	public static String printPreciseDecimal(Double d)
	{
		int i1 = d.intValue();
		Integer i2 = i1;

		if (d.compareTo(i2.doubleValue()) != 0)
		{
			return String.valueOf(d);
		}
		else
		{
			return String.valueOf(i2);
		}
	}

	/**
	 * Returns the given {@link String} double as a {@link String}, without
	 * decimal places if the double has only zeros after the decimal place. e.g.
	 * a double of 20.00 returns "20", a double of 20.85 returns "20.85"
	 * 
	 * @param dbl the {@link String} to convert
	 * @return the {@link String}
	 */
	public static String printPreciseDecimalString(String dbl)
	{
		try
		{
			Double d = Double.parseDouble(dbl);

			int i1 = d.intValue();
			Integer i2 = i1;

			if (d.compareTo(i2.doubleValue()) != 0)
			{
				return String.valueOf(d);
			}
			else
			{
				return String.valueOf(i2);
			}
		}
		catch (NumberFormatException e)
		{
			return dbl;
		}
	}

	/**
	 * Rounds the given value to the given number of decimal places.
	 * 
	 * @param d the number to round
	 * @param decimalPlaces the number of decimal places to round to
	 * @return the rounded number
	 */
	public static double roundDouble(double d, int decimalPlaces)
	{
		return Math.round(d * Math.pow(10, decimalPlaces))
				/ Math.pow(10, decimalPlaces);
	}

	/**
	 * Splits numbers within a range by a certain interval and returns the
	 * sub-ranges as key:value mappings. e.g. start = 1, end = 430, interval =
	 * 100 would return a map consisting of the following key-value pairs:
	 * 1:100, 101:200, 201:300, 301:400, 401:430
	 * 
	 * @param start the start value (included)
	 * @param end the end value (included)
	 * @param interval the interval to split the numbers by
	 * @return the {@link Map} of start/end pairs
	 */
	public static Map<Integer, Integer> splitNumbers(int start, int end, int interval)
	{
		Map<Integer, Integer> pairs = new TreeMap<Integer, Integer>();

		for (int i = start; i <= end; i = i + interval)
		{
			if (i == end)
			{
				pairs.put(i, end);
			}
			else if ((i + interval - 1) >= end)
			{
				pairs.put(i, end);
			}
			else
			{
				pairs.put(i, (i + interval - 1));
			}
		}

		return pairs;
	}

	public static String stripTrailingZeros(String number)
	{
		if ((number != null) && !number.trim().isEmpty())
		{
			BigDecimal n = new BigDecimal(number);

			// special case for zero values (e.g. 0.00) where the trailing zeros
			// aren't stripped by the BigDecimal stripTrailingZeros method!
			if (n.compareTo(BigDecimal.ZERO) == 0)
			{
				return "0";
			}

			return n.stripTrailingZeros().toPlainString();
		}
		else
		{
			return "";
		}
	}
}
