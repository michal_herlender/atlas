package org.trescal.cwms.core.tools;

import org.apache.commons.net.util.SubnetUtils.SubnetInfo;

/**
 * This class exists only to define a working version of the isInRange method.
 * The method was bugged in commons-net-2.0 (see JIRA issue NET-282 @
 * http://osdir.com/ml/issues-commons-apache/2010-03/msg00180.html) but has been
 * fixed in 2.1 and onwards. Unfortunately, it seems impossible to find a 2.1
 * release at the moment - I think they rolled back the only release candidate -
 * but when it is eventually re-released we can remove this class and use the
 * commons implementation.
 * 
 * @author jamiev
 */
public class MySubnetUtils
{
	public static boolean isInRange(SubnetInfo info, String remoteAddr)
	{
		int address = info.asInteger(remoteAddr);
		int low = info.asInteger(info.getLowAddress());
		int high = info.asInteger(info.getHighAddress());
		return (low <= address) && (address <= high);
	}
}