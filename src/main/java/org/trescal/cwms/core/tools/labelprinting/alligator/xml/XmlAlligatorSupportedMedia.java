package org.trescal.cwms.core.tools.labelprinting.alligator.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlAlligatorSupportedMedia {
	@XmlAttribute
	private String name;
	@XmlAttribute
	private Boolean isCurrent;
	
	public XmlAlligatorSupportedMedia() {
		// Default constructor
	}
	
	public XmlAlligatorSupportedMedia(String name, Boolean isCurrent) {
		this.name = name;
		this.isCurrent = isCurrent;
	}
	
	public String getName() {
		return name;
	}
	public Boolean getIsCurrent() {
		return isCurrent;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setIsCurrent(Boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

}
