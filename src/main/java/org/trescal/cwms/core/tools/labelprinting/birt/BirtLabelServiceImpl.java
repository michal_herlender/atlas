package org.trescal.cwms.core.tools.labelprinting.birt;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.eclipse.birt.report.engine.api.EngineException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.db.CertLinkService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.accreditationbodyresource.AccreditationBodyResource;
import org.trescal.cwms.core.system.entity.accreditationbodyresource.AccreditationBodyResourceType;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.core.system.entity.accreditedlabcaltype.db.AccreditedLabCalTypeService;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.labelprinter.LabelPrinter;
import org.trescal.cwms.core.system.entity.labelprinter.db.LabelPrinterService;
import org.trescal.cwms.core.system.entity.labeltemplate.db.LabelTemplateService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AccreditedLabelCertificateNumber;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AccreditedLabelPlantNumber;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.StandardLabelCertificateNumber;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.StandardLabelPlantNumber;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.BirtEngine;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.labelprinting.LabelService;
import org.trescal.cwms.core.tools.labelprinting.PrintRequest;
import org.trescal.cwms.core.tools.pathservice.PathService;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.websocket.PrintHandler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;

public class BirtLabelServiceImpl implements LabelService {
    private static final Logger LOG = LoggerFactory.getLogger(BirtLabelServiceImpl.class);

    private Properties labelProperties;
    private CertLinkService certLinkServ;
    private LabelPrinterService labelPrinterServ;
    private BirtEngine birtEngine;
    private PathService pathService;
    private InstrumService instrumServ;
    private SessionUtilsService sessionServ;
    private AccreditedLabCalTypeService alctServ;
    private CertificateService certServ;
    private PrintHandler printHandler;
    private CompanyService compServ;
    private AccreditedLabelPlantNumber accreditedLabelPlantNumber;
    private AccreditedLabelCertificateNumber accreditedLabelCertificateNumber;
    private StandardLabelPlantNumber standardLabelPlantNumber;
    private StandardLabelCertificateNumber standardLabelCertificateNumber;
    private LabelTemplateService labelTemplateServ;
	
	private static String DEFAULT_LABEL_TEMPLATE = "/labels/birt/birt_label_standard.rptdesign";

	@Override
	public PrintRequest printCertificateLabelForSelectedLinks(int[] certLinkIds, String templateKey,
			boolean includeRecallDate) {
		PrintRequest result = new PrintRequest();

		PrintRequest tempPr;
		boolean overallSuccess = true;

		// recursively call this method for each individual job item
		for (int certLinkId : certLinkIds) {
			tempPr = this.printCertificateLabel(certLinkId, templateKey, includeRecallDate);
			if (!tempPr.isSuccess()) {
				overallSuccess = false;
			}
		}

		result.setSuccess(overallSuccess);
		if (overallSuccess) {
			result.setMessage("Group certificate: various files output successfully");
		} else {
			result.setMessage("ERROR: One or more labels failed to have their file output!");
		}

		return result;
	}

	@Override
	public PrintRequest printCertificateLabel(int certLinkId, String templateKey, boolean includeRecallDate) {
		PrintRequest pr = new PrintRequest();

		Locale locale = sessionServ.getCurrentContact().getLocale();

		LabelPrinter printer = this.labelPrinterServ.getDefaultLabelPrinterForCurrentContact();
		// we'll use it one day

		String printPrepend = this.labelProperties.getProperty("prepend_print");
		String fileName = "labelprint" + certLinkId;
		try {
			File f = new File(pathService.getTempDirFile(), printPrepend.concat(fileName).concat(".pdf"));
			FileOutputStream out = new FileOutputStream(f);

			if (printCertificateLabel(certLinkId, templateKey, includeRecallDate, locale, out, currentCompany())) {
				pr.setSuccess(true);
				pr.setMessage("File output to: " + f.getAbsolutePath());

				printHandler.print(printer, f);
			} else {
				pr.setSuccess(false);
				pr.setMessage("ERROR: No Label template filepath matching key: '" + templateKey + "'");
			}

		} catch (Exception e) {
			pr.setSuccess(false);
			pr.setMessage(e.toString());
		}

		return pr;
	}

	@Override
	public boolean printCertificateLabel(int certLinkId, String templateKey, boolean includeRecallDate, Locale locale,
			OutputStream out, Company comp) throws EngineException {
		CertLink certLink = this.certLinkServ.findCertLink(certLinkId);
		String templatePath = this.labelProperties.getProperty(templateKey);

		if (templatePath != null) {
			try {
				// Put relevant data into map
				HashMap<String, Object> map = new HashMap<>();
				Certificate cert = certLink.getCert();
                JobItem jobItem = certLink.getJobItem();

                map.put("caldate", cert.getCalDate());
                map.put("plantid", String.valueOf(jobItem.getInst().getPlantid()));
                if (includeRecallDate) {
                    map.put("shownextcal", true);
                    map.put("nextcaldate", getRecallDate(certLink));
                }
                map.put("serialno", jobItem.getInst().getSerialno());

                setAccreditationParams(map, jobItem.getCapability(), cert.getCalType());
                setPlantNumberParams(map, cert.getCalType(), comp, jobItem.getInst());
                setCertificateNumberParams(map, comp, jobItem.getInst().getComp(), cert);
                setThirdPartyParams(map, cert);

                if (cert.getType().equals(CertificateType.CERT_THIRDPARTY)) {
                    templatePath = this.labelProperties.getProperty("label_tp");
                }

                // Generate the file from the template
                birtEngine.generatePDF(templatePath, map, out, locale);
				return true;
			} finally {
				try {
					out.close();
				} catch (IOException e) {
					// ignored
				}
			}
		} else {
			LOG.error("No Label template filepath matching key: '{}'", templateKey);
			return false;
		}
	}

	private Company currentCompany() {
		WebContext ctx = WebContextFactory.get();
		@SuppressWarnings("unchecked")
		KeyValue<Integer, String> ckv = (KeyValue<Integer, String>) ctx.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_COMPANY);
		return compServ.get(ckv.getKey());
	}

	private void setThirdPartyParams(HashMap<String, Object> map, Certificate cert) {
		if (cert.getType().equals(CertificateType.CERT_THIRDPARTY)) {
			map.put("thirdcertno", cert.getThirdCertNo());
			map.put("thirdconame", cert.getThirdDiv().getComp().getConame());
		}
	}

	private void setPlantNumberParams(HashMap<String, Object> map, CalibrationType calType, Company currentCompany,
			Instrument inst) {
		boolean show;
		if (calType.getAccreditationSpecific()) {
			show = this.accreditedLabelPlantNumber.getValueByScope(Scope.COMPANY, inst.getComp(), currentCompany);
		} else {
			show = this.standardLabelPlantNumber.getValueByScope(Scope.COMPANY, inst.getComp(), currentCompany);
		}
		map.put("customerid", show ? inst.getPlantno() : null);
	}

    private void setCertificateNumberParams(HashMap<String, Object> map, Company currentCompany, Company customer, Certificate cert) {
        boolean show;
        if (cert.getCalType().getAccreditationSpecific()) {
            show = this.accreditedLabelCertificateNumber.getValueByScope(Scope.COMPANY, customer, currentCompany);
        } else {
            show = this.standardLabelCertificateNumber.getValueByScope(Scope.COMPANY, customer, currentCompany);
        }
        map.put("certno", show ? cert.getCertno() : null);
    }

    private void setAccreditationParams(HashMap<String, Object> map, Capability proc, CalibrationType calType) {
        if ((proc != null) && calType.getAccreditationSpecific()) {
            AccreditedLab labAccreditation = proc.getLabAccreditation();
            if (labAccreditation != null) {
                map.put("accreditationnumber", labAccreditation.getLabNo());

                for (AccreditationBodyResource abr : labAccreditation.getAccreditationBody()
                    .getAccreditationBodyResources()) {
                    if (AccreditationBodyResourceType.CAL_LOGO_271_354 == abr.getType()) {
                        map.put("logo", abr.getResourcePath());
                        break;
                    }
                }
            }
        }
    }

    // Hardcoded as European dd-mm-yyyy format, used in Spain only!
    private String getRecallDate(CertLink cl) {
        LocalDate calDueDate = cl.getJobItem().getInst().getNextCalDueDate();
        String result = null;
        if (calDueDate != null) {
            result = calDueDate.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        }
        return result;
    }

    /**
     * Safely get the customer description (used in some customer specific templates) without
     * throwing NPEs for uninitialised data
     */
    private String getCustomerDescription(Instrument inst) {
        String result = "";
        if (inst.getCustomerDescription() != null) {
            result = inst.getCustomerDescription();
		}
		else if ((inst.getInstrumentComplementaryField() != null) && (inst.getInstrumentComplementaryField().getFormerLocalDescription() != null)) {
			result = inst.getInstrumentComplementaryField().getFormerLocalDescription();
		}
		return result;
	}

	@Override
	public boolean printCalSpecificLabelForJobItem(int certLinkId, boolean includeRecallDate, Locale locale,  
			OutputStream out, Company comp) throws EngineException {

        CertLink certLink = this.certLinkServ.findCertLink(certLinkId);
        Certificate cert = certLink.getCert();

        CalibrationType calType = (cert.getCal() == null) ? cert.getCalType() : cert.getCal().getCalType();
        Capability proc = (cert.getCal() == null) ? null : cert.getCal().getCapability();
        String templatePath = ((proc != null) && calType.getAccreditationSpecific())
            ? this.alctServ.getLabelTemplate(proc.getLabAccreditation(), calType) : DEFAULT_LABEL_TEMPLATE;

        if (templatePath != null) {
            try {
                Instrument inst = certLink.getJobItem().getInst();

                String plantid = String.valueOf(inst.getPlantid());

                // Put relevant data into map
                HashMap<String, Object> map = new HashMap<>();
                map.put("caldate", cert.getCalDate());
				map.put("plantid", plantid);
				if (includeRecallDate) {
					map.put("shownextcal", true);
					map.put("nextcaldate", getRecallDate(cert.getLinks().iterator().next()));
				}
				map.put("serialno", inst.getSerialno());

				this.setAccreditationParams(map, proc, calType);
				this.setPlantNumberParams(map, calType, comp, inst);
				this.setCertificateNumberParams(map, comp, inst.getComp(), cert);
				this.setThirdPartyParams(map, cert);
				
				if (cert.getType().equals(CertificateType.CERT_THIRDPARTY)){
					templatePath = this.labelProperties.getProperty("label_tp");
				}
				
				// Generate the file from the template
				birtEngine.generatePDF(templatePath, map, out, locale);
				return true;
			} finally {
				try {
					out.close();
				} catch (IOException e) {
					// ignored
				}
			}
		} else {
			LOG.error("No Label template filepath for calibration type: '{}'",
					cert.getCal().getCalType().getServiceType().getShortName());
		}

		return false;
	}

	@Override
	public PrintRequest printAssetLabel(int assetId, boolean includeIPAddresses) {
		throw new NoSuchMethodError();
	}

	@Override
	public PrintRequest printInstDateLabel(String templateKey, int plantId, String date, String serial)
	{
		PrintRequest result = new PrintRequest();

		Locale locale = sessionServ.getCurrentContact().getLocale();

		// Generate filename and create the filewriter
		LabelPrinter printer = this.labelPrinterServ.getDefaultLabelPrinterForCurrentContact();
		String printPrepend = this.labelProperties.getProperty("prepend_print");

		String fileName = "patlabelprint" + plantId;
		File f = new File(pathService.getTempDirFile(), printPrepend.concat(fileName).concat(".pdf"));

		try {
			FileOutputStream out = new FileOutputStream(f);
			
			if (printInstDateLabel(templateKey,plantId,date,serial,out,locale, currentCompany())) {
				result.setSuccess(true);
				result.setMessage("File output to: " + f.getAbsolutePath());

				printHandler.print(printer, f);
			} else {
				result.setSuccess(false);
				result.setMessage("ERROR: Label template filepath could not be found for instrument '" + plantId + "'");

			}
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(e.getMessage());
		}

		return result;
	}
	
	@Override
	public boolean printInstDateLabel(String templateKey, int plantId, String date, String serial, OutputStream out, Locale locale, Company company) throws EngineException
	{
//		PrintRequest pr = new PrintRequest();

		Instrument inst = this.instrumServ.get(plantId);

		String plantid = "";
		String serialno = serial;

		if (inst != null)
		{
			plantid = this.toString(inst.getPlantid());
			serialno = this.toString(inst.getSerialno());
		}

		String templatePath = this.labelProperties.getProperty(templateKey); 
		Date d = this.getDateFromFormattedString(date);

		if (templatePath != null)
		{
			try {
				// Put relevant data into map
				HashMap<String, Object> map = new HashMap<>();
				map.put("plantid", this.toString(plantid));
				map.put("serialno", this.toString(serialno));
				map.put("thedate", d);

				// Generate the file from the template
				birtEngine.generatePDF(templatePath, map, out, locale);
				return true;
			} finally {
				try {
					out.close();
				} catch (IOException e) {
					// ignored
				}
			}
		} else {
			LOG.error("No Label template filepath for type: '{}'",templateKey);
		}

		return false;
	}

	@Override
	public PrintRequest printNonDataBasedLabel(String templateKey, Integer quantity) {
		PrintRequest result = new PrintRequest();

		Locale locale = sessionServ.getCurrentContact().getLocale();

		// Generate filename and create the filewriter
		LabelPrinter printer = this.labelPrinterServ.getDefaultLabelPrinterForCurrentContact();
		String printPrepend = this.labelProperties.getProperty("prepend_print");

		String fileName = "labelprint";
		File f = new File(pathService.getTempDirFile(), printPrepend.concat(fileName).concat(".pdf"));

		try {
			FileOutputStream out = new FileOutputStream(f);
			
			if (printNonDataBasedLabel(templateKey,quantity,out,locale, currentCompany())) {
				result.setSuccess(true);
				result.setMessage("File output to: " + f.getAbsolutePath());

				printHandler.print(printer, f);
			} else {
				result.setSuccess(false);
				result.setMessage("ERROR: Label template filepath could not be found");

			}
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(e.getMessage());
		}

		return result;
	}

	@Override
	public boolean printNonDataBasedLabel(String templateKey, Integer quantity, OutputStream out, Locale locale, Company company) throws EngineException
	{
		String templatePath = this.labelProperties.getProperty(templateKey);
		if (templatePath != null)
		{
			try {
				// Put relevant data into map
				HashMap<String, Object> map = new HashMap<>();
				map.put("quantity", this.toString(quantity));
				map.put("plantid", "");
				map.put("serialno", "");
				map.put("thedate", null);
				map.put("thestr", "");
				map.put("caldate", null);
				map.put("shownextcal", true);
				map.put("nextcaldate", null);
				map.put("certno", "");
				map.put("customerid", "");
				map.put("thirdcertno", "");
				map.put("thirdconame", "");

				// Generate the file from the template
				birtEngine.generatePDF(templatePath, map, out, locale);
				return true;
			} finally {
				try {
					out.close();
				} catch (IOException e) {
					// ignored
				}
			}
		} else {
			LOG.error("No Label template filepath for type: '{}'",templateKey);
		}

		return false;
	}
	
	@Override
	public PrintRequest printStringDateLabel(String templateKey, String str, String date) {
		PrintRequest result = new PrintRequest();

		Locale locale = sessionServ.getCurrentContact().getLocale();

		// Generate filename and create the filewriter
		LabelPrinter printer = this.labelPrinterServ.getDefaultLabelPrinterForCurrentContact();
		String printPrepend = this.labelProperties.getProperty("prepend_print");

		File f = new File(pathService.getTempDirFile(), printPrepend.concat(str).concat(".pdf"));

		try {
			FileOutputStream out = new FileOutputStream(f);

			if (printStringDateLabel(templateKey, str, date, out, locale, currentCompany())) {
				result.setSuccess(true);
				result.setMessage("File output to: " + f.getAbsolutePath());

				printHandler.print(printer, f);
			} else {
				result.setSuccess(false);
				result.setMessage("ERROR: Label template filepath could not be found");

			}
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(e.getMessage());
		}

		return result;
	}

	@Override
	public boolean printStringDateLabel(String templateKey, String str, String date, OutputStream out, Locale locale, Company company) throws EngineException
	{
		String templatePath = this.labelProperties.getProperty(templateKey);
		Date d = this.getDateFromFormattedString(date);

		if (templatePath != null)
		{
			try {
				// Put relevant data into map
				HashMap<String, Object> map = new HashMap<>();
				map.put("thedate", d);
				map.put("thestr", this.toString(str));

				// Generate the file from the template
				birtEngine.generatePDF(templatePath, map, out, locale);
				return true;
			} finally {
				try {
					out.close();
				} catch (IOException e) {
					// ignored
				}
			}
		} else {
			LOG.error("No Label template filepath for type: '{}'",templateKey);
		}

		return false;
	}
	
	public Properties getLabelProperties() {
		return labelProperties;
	}

	public void setLabelProperties(Properties labelProperties) {
		this.labelProperties = labelProperties;
	}

	public CertLinkService getCertLinkServ() {
		return certLinkServ;
	}

	public void setCertLinkServ(CertLinkService certLinkServ) {
		this.certLinkServ = certLinkServ;
	}

	public LabelPrinterService getLabelPrinterServ() {
		return labelPrinterServ;
	}

	public void setLabelPrinterServ(LabelPrinterService labelPrinterServ) {
		this.labelPrinterServ = labelPrinterServ;
	}

	public BirtEngine getBirtEngine() {
		return birtEngine;
	}

	public void setBirtEngine(BirtEngine birtEngine) {
		this.birtEngine = birtEngine;
	}

	public PathService getPathService() {
		return pathService;
	}

	public void setPathService(PathService pathService) {
		this.pathService = pathService;
	}

	public InstrumService getInstrumServ() {
		return instrumServ;
	}

	public void setInstrumServ(InstrumService instrumServ) {
		this.instrumServ = instrumServ;
	}

	public SessionUtilsService getSessionServ() {
		return sessionServ;
	}

	public void setSessionServ(SessionUtilsService sessionServ) {
		this.sessionServ = sessionServ;
	}

	public AccreditedLabCalTypeService getAlctServ() {
		return alctServ;
	}

	public void setAlctServ(AccreditedLabCalTypeService alctServ) {
		this.alctServ = alctServ;
	}

	public CertificateService getCertServ() {
		return certServ;
	}

	public void setCertServ(CertificateService certServ) {
		this.certServ = certServ;
	}

	public PrintHandler getPrintHandler() {
		return printHandler;
	}

	public void setPrintHandler(PrintHandler printHandler) {
		this.printHandler = printHandler;
	}

	public CompanyService getCompServ() {
		return compServ;
	}

	public void setCompServ(CompanyService compServ) {
		this.compServ = compServ;
	}

	public AccreditedLabelPlantNumber getAccreditedLabelPlantNumber() {
		return accreditedLabelPlantNumber;
	}

	public void setAccreditedLabelPlantNumber(AccreditedLabelPlantNumber accreditedLabelPlantNumber) {
		this.accreditedLabelPlantNumber = accreditedLabelPlantNumber;
	}

	public AccreditedLabelCertificateNumber getAccreditedLabelCertificateNumber() {
		return accreditedLabelCertificateNumber;
	}

	public void setAccreditedLabelCertificateNumber(AccreditedLabelCertificateNumber accreditedLabelCertificateNumber) {
		this.accreditedLabelCertificateNumber = accreditedLabelCertificateNumber;
	}

	public StandardLabelPlantNumber getStandardLabelPlantNumber() {
		return standardLabelPlantNumber;
	}

	public void setStandardLabelPlantNumber(StandardLabelPlantNumber standardLabelPlantNumber) {
		this.standardLabelPlantNumber = standardLabelPlantNumber;
	}

	public StandardLabelCertificateNumber getStandardLabelCertificateNumber() {
		return standardLabelCertificateNumber;
	}

	public void setStandardLabelCertificateNumber(StandardLabelCertificateNumber standardLabelCertificateNumber) {
		this.standardLabelCertificateNumber = standardLabelCertificateNumber;
	}

	public LabelTemplateService getLabelTemplateServ() {
		return labelTemplateServ;
	}

	public void setLabelTemplateServ(LabelTemplateService labelTemplateServ) {
		this.labelTemplateServ = labelTemplateServ;
	}
	
	public Date getDateFromFormattedString(String formattedDate)
	{
		if (formattedDate == null)
		{
			return null;
		}

		try
		{
			return DateTools.df.parse(formattedDate);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public String toString(Object obj)
	{
		try
		{
			if (obj.toString().trim().length() > 0)
			{
				return obj.toString().trim();
			}
			else
			{
				return " ";
			}
		}
		catch (Exception e)
		{
			return " ";
		}
	}

	@Override
	public boolean printSpecificLabel(int certLinkId, String templateKey,
			boolean includeRecallDate, String technician, String correction,
			String incertainty, String exptrescal, String icp, String quality,
			Locale locale, OutputStream out, Company comp)
			throws EngineException {
		CertLink certLink = this.certLinkServ.findCertLink(certLinkId);
		String templatePath = this.labelProperties.getProperty(templateKey);

		if (templatePath != null) {
			try {
				// Put relevant data into map
				HashMap<String, Object> map = new HashMap<>();
				Certificate cert = certLink.getCert();
				JobItem jobItem = certLink.getJobItem();

				map.put("ReportLocale", locale.toString());
				map.put("caldate", cert.getCalDate());
				map.put("plantid", String.valueOf(jobItem.getInst().getPlantid()));
				if (includeRecallDate) {
					map.put("shownextcal", true);
                    map.put("nextcaldate", getRecallDate(certLink));
                }
                map.put("serialno", jobItem.getInst().getSerialno());
                map.put("plantname", getCustomerDescription(jobItem.getInst()));
                map.put("technician", technician);
                map.put("correction", correction);
                map.put("incertainty", incertainty);
                map.put("icp", icp);
                map.put("quality", quality);

                setAccreditationParams(map, jobItem.getCapability(), cert.getCalType());
                setPlantNumberParams(map, cert.getCalType(), comp, jobItem.getInst());
                setCertificateNumberParams(map, comp, jobItem.getInst().getComp(), cert);
                setThirdPartyParams(map, cert);

                if (cert.getType().equals(CertificateType.CERT_THIRDPARTY)) {
                    templatePath = this.labelProperties.getProperty("label_tp");
                }

                // Generate the file from the template
                birtEngine.generatePDF(templatePath, map, out, locale);
				return true;
			} finally {
				try {
					out.close();
				} catch (IOException e) {
					// ignored
				}
			}
		} else {
			LOG.error("No Label template filepath matching key: '{}'", templateKey);
			return false;
		}
	}
	
	@Override
	public boolean printCustCalLabel(int certId, String templateKey, int instrumentId,
			Locale locale, OutputStream out, Company comp)
			throws EngineException {
		
		String templatePath = this.labelProperties.getProperty(templateKey);
		Certificate certificate = this.certServ.findCertificate(certId);
		Instrument instrument = this.instrumServ.get(instrumentId);

		if (templatePath != null) {
            try {
                // Put relevant data into map
                HashMap<String, Object> map = new HashMap<>();

                map.put("ReportLocale", locale.toString());
                map.put("caldate", certificate.getCalDate());
                map.put("plantid", String.valueOf(instrument.getPlantid()));
                map.put("shownextcal", true);
                map.put("nextcaldate", instrument.getNextCalDueDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
                map.put("serialno", instrument.getSerialno());
                map.put("plantname", getCustomerDescription(instrument));
                map.put("customerid", instrument.getPlantno());
                map.put("certno", certificate.getCertno());

                // Generate the file from the template
                birtEngine.generatePDF(templatePath, map, out, locale);
                return true;
			} finally {
				try {
					out.close();
				} catch (IOException e) {
					// ignored
				}
			}
		} else {
			LOG.error("No Label template filepath matching key: '{}'", templateKey);
			return false;
		}
	}
}
