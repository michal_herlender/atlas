package org.trescal.cwms.core.tools.fileupload2.utils.validfiletype.db;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.tools.fileupload2.utils.validfiletype.ValidFileType;

public interface ValidFileTypeService
{
	void deleteValidFileType(ValidFileType validfiletype);

	ValidFileType findValidFileType(int id);

	List<ValidFileType> getAllValidFileTypes();

	void insertValidFileType(ValidFileType validfiletype);

	boolean isValidFileType(MultipartFile file, ValidFileType.FileTypeUsage usage);

	void saveOrUpdateValidFileType(ValidFileType validfiletype);

	void updateValidFileType(ValidFileType validfiletype);
}