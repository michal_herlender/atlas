package org.trescal.cwms.core.tools.fileupload2.service;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.*;

/**
 * @author admin
 */
public class UploadServiceImpl implements UploadService
{
	private static final Log logger = LogFactory.getLog(UploadServiceImpl.class);

	public void saveFile(InputStream inputStream, File toDirectory, String fileName) {
		File file;

		if ((toDirectory == null) || !toDirectory.exists() || !toDirectory.canWrite() || !toDirectory.isDirectory()) {
			// for some reason we can't write to the give file so use the temp
			// directory instead
			file = new File(System.getProperty("java.io.tmpdir") + File.separator + fileName);

			logger.error("saveFile() - Could not write file, saving to temp directory " + System.getProperty("java.io.tmpdir") + File.separator + fileName);
		} else
		{
			file = new File(toDirectory.getAbsolutePath().concat(File.separator).concat(fileName));
		}

		FileOutputStream fileOut = null;
		try
		{
			logger.info("saveFile() - saving file to "+file.getAbsolutePath());
			fileOut = new FileOutputStream(file);
			IOUtils.copy(inputStream, fileOut);
		}
		catch (FileNotFoundException e)
		{
			logger.error("saveFile() - File Not Found." + e);
		}
		catch (IOException e)
		{
			logger.error("saveFile() - Error while saving file." + e);
		}
		finally
		{
			try
			{
				inputStream.close();
				if (fileOut != null)
				{
					fileOut.close();
				}
			}
			catch (IOException e)
			{
				logger.error(e);
			}
		}
	}

}
