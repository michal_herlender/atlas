package org.trescal.cwms.core.tools;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class FileTools {
	public static boolean copyFile(File originalLoc, File copyLoc) {
		FileInputStream fis;
		FileOutputStream fos;

		try {
			fis = new FileInputStream(originalLoc);
			fos = new FileOutputStream(copyLoc);
			byte[] buf = new byte[1024];
			int i;
			while ((i = fis.read(buf)) != -1) {
				fos.write(buf, 0, i);
			}

			fis.close();
			fos.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean copyFile(String originalLocPath, String copyLocPath) {
		return FileTools.copyFile(new File(originalLocPath), new File(copyLocPath));
	}

	/**
	 * this method counts files in a directory (including files in all
	 * subdirectories)
	 *
	 * @param directory the directory to start in
	 * @return the total number of files
	 */
	public static int countFilesInDirectory(File directory)
	{
		// variable to hold count
		int count = 0;
		// check all files in directory (only if directory is accessible - otherwise log error and return null)
		File[] files = directory.listFiles();
		if (files != null) {
			for (File file : Objects.requireNonNull(directory.listFiles())) {
				if (file.isFile() && !file.getName().equalsIgnoreCase("thumbs.db")) {
					count++;
				}
				if (file.isDirectory()) {
					count += countFilesInDirectory(file);
				}
			}
		}
		else {
			log.error("Path "+directory.getAbsolutePath()+" is inaccessible or does not resolve as a directory!");
		}
		// return final count
		return count;
	}

	public static boolean deleteFiles(File file)
	{
		if (file.isDirectory()) {
			boolean success = true;
			for (File f : Objects.requireNonNull(file.listFiles())) {
				boolean thisFile = deleteFiles(f);
				success = thisFile && success;
			}
			boolean dir = file.delete();
			success = dir && success;
			return success;
		} else {
			return file.delete();
		}
	}

	public static String getAllTextFromFile(File file) {
		FileReader reader = null;
		BufferedReader input = null;
		try {
			reader = new FileReader(file);
			input = new BufferedReader(reader);
			String line;
			StringBuilder contents = new StringBuilder();

			while ((line = input.readLine()) != null) {
				contents.append(line);
				contents.append(System.getProperty("line.separator"));
			}

			return contents.toString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			try {
				if (input != null) {
					input.close();
					reader.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}

	public static String getAllTextFromFileByPath(String filepath) {
		File file = new File(filepath);
		return getAllTextFromFile(file);
	}

	public static String getFileExtension(File file) {
		String filename = file.getName();
		return (filename.lastIndexOf(".") == -1) ? "" : filename.substring(filename.lastIndexOf(".") + 1);
	}

	public static List<File> getFileListForDir(File dir) {
		FileFilter fileFilter = file -> (!file.isDirectory());

		return Arrays.asList(Objects.requireNonNull(dir.listFiles(fileFilter)));
	}

	public static List<File> getFileListForDir(String dirPath, final List<String> acceptedExtensions) {
		File dir = new File(dirPath);

		FileFilter fileFilter = file -> {
			boolean matchesExtRules = true;
			if ((acceptedExtensions != null)
				&& !acceptedExtensions.isEmpty()) {
				matchesExtRules = false;
				for (String ext : acceptedExtensions) {
					if (file.getName().endsWith(ext)) {
						matchesExtRules = true;
					}
				}
			}

			return (!file.isDirectory() && matchesExtRules);
		};

		File[] filesAsArray = dir.listFiles(fileFilter);

		if (filesAsArray == null) {
			// Either dir does not exist or is not a directory
			return Collections.emptyList();
		} else {
			return Stream.of(filesAsArray).collect(Collectors.toList());
		}
	}

	public static boolean saveFile(byte[] file, String fileName) {
		try {
			File f = new File(fileName);
			FileOutputStream outpf = new FileOutputStream(f);
			outpf.write(file);
			outpf.close();
			return true;
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	public static void SaveFileString(String content, String path) {
		FileWriter fstream;
		BufferedWriter out = null;
		try {
			fstream = new FileWriter(path);
			out = new BufferedWriter(fstream);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error: " + e.getMessage());
		}
		finally
		{
			try {
				if (out != null) {
					out.write(content);
					out.close();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				System.err.println("Error: " + e.getMessage());
			}

		}
	}
}
