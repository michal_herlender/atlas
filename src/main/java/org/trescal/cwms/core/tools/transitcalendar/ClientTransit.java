package org.trescal.cwms.core.tools.transitcalendar;

import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;

public class ClientTransit extends Transit
{
	private TransportOption clientTransit;

	public TransportOption getClientTransit()
	{
		return this.clientTransit;
	}

	@Override
	public boolean isBase()
	{
		return false;
	}

	public void setClientTransit(TransportOption clientTransit)
	{
		this.clientTransit = clientTransit;
	}

	@Override
	public String toString()
	{
		return this.clientTransit.toString();
	}
}