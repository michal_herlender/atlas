package org.trescal.cwms.core.tools.fileupload2.utils.validfiletype.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.tools.fileupload2.utils.validfiletype.ValidFileType;

public interface ValidFileTypeDao extends BaseDao<ValidFileType, Integer> {
	
	boolean isValidFileType(String extension, ValidFileType.FileTypeUsage usage);
}