package org.trescal.cwms.core.tools.labelprinting.alligator.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlAlligatorBarcode extends XmlAlligatorElement {
	@XmlAttribute
	private String value;
	@XmlAttribute
	private String type;
	
	public String getValue() {
		return value;
	}
	public String getType() {
		return type;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setType(String type) {
		this.type = type;
	}
}
