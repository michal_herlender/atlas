package org.trescal.cwms.core.tools;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.JFreeChart;

public class ChartTools
{
	public static boolean saveChartAsImage(JFreeChart chart, int width, int height, String imageDestinationPath, ChartRenderingInfo info)
	{
		try
		{
			File f = new File(imageDestinationPath);
			String ext = FileTools.getFileExtension(f);
			BufferedImage bi = null;

			if (info != null)
			{
				bi = chart.createBufferedImage(width, height, info);
			}
			else
			{
				bi = chart.createBufferedImage(width, height);
			}

			boolean finished = false;
			Iterator<ImageWriter> writers = ImageIO.getImageWritersByFormatName(ext);
			ImageWriter writer = writers.next();
//			writer.addIIOWriteProgressListener(new MyWriteProgressListener(finished));
			ImageOutputStream ios = ImageIO.createImageOutputStream(f);
			writer.setOutput(ios);
			writer.write(bi);
			ios.close();
			return true;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
	}
}
