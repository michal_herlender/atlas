package org.trescal.cwms.core.tools.filebrowser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.EncryptionTools;
import org.trescal.cwms.core.tools.FileTools;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;

import java.io.File;
import java.net.URLDecoder;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service("FileBrowserService")
public class FileBrowserServiceImpl implements FileBrowserService {
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private MessageSource messages;
	@Autowired
	private SystemComponentService scServ;

	private static final String DEFAULT_COLOUR = "black";
	private static final String NAME_MATCH_COLOUR = "red";
	private static final String NEW_FILE_COLOUR = "green";
	
	private final static Logger logger = LoggerFactory.getLogger(FileBrowserServiceImpl.class);
	
	private String decrypt(String input) {
		String result = null;
		try {
			result = EncryptionTools.decrypt(input);
		}
		catch (Exception ex) {
			logger.error("Error decrypting input", ex);
		}
		return result;
	}
	
	private String decodeURIComponent(String input) {
		String result = null;
		try {
			result = URLDecoder.decode(input, "UTF-8");
		}
		catch (Exception ex) {
			logger.error("Error decoding input", ex);
		}
		return result;
	}
	
	@Override
	public boolean deleteFile(String fileName, String subdirectoryName, int scId, int entityId) {
		FileBrowserProperties properties = this.initialiseFileBrowser(scId, entityId, Collections.emptyList());
		File directoryForEntity = properties.getDirectoryForEntity();

		String decodedFilename = decrypt(fileName);
		logger.debug("deleteFile : directoryForEntity = " + directoryForEntity.getAbsolutePath());
		logger.debug("deleteFile : decodedFileName = " + (decodedFilename == null ? "null" : decodedFilename));
		logger.debug("deleteFile : subdirectoryName = " + (subdirectoryName == null ? "null" : subdirectoryName));

		boolean deleted = false;
		File file;
		if (subdirectoryName != null && !subdirectoryName.isEmpty()) {
			File matchingSubdirectory = getMatchingSubdirectory(directoryForEntity, subdirectoryName);
			file = getMatchingFile(matchingSubdirectory, decodedFilename);
		} else {
			file = getMatchingFile(directoryForEntity, decodedFilename);
		}

		if (file == null) {
			logger.debug("deleteFile : not found through navigation");
		}
		
		if (file != null && file.exists())
		{
			deleted = file.delete();
		}
		logger.debug("deleteFile : deleted = "+deleted);

		return deleted;
	}

	public boolean findRegexInFileName(String fileName, List<RegularExpression> regExs)
	{
		Pattern pattern;
		Matcher matcher;
		boolean found = false;

		for (RegularExpression regEx : regExs)
		{
			pattern = Pattern.compile(regEx.getRegEx(), Pattern.CASE_INSENSITIVE);
			matcher = pattern.matcher(fileName);

			if (matcher.find())
			{
				found = true;
			}
		}

		return found;
	}

	public File getDirectoryForComponentEntity(SystemComponent sc, String identifier, ComponentEntity parentEntity)
	{
		if (sc.isNumberedSubFolders())
		{
			if (identifier.contains(Constants.VERSION_TEXT))
			{
				String path = identifier.substring(0, identifier.indexOf(Constants.VERSION_TEXT));
				String ver = identifier.substring(identifier.lastIndexOf(Constants.VERSION_TEXT));
				ver = ver.replaceAll(Constants.VERSION_TEXT, "");
				int version = Integer.parseInt(ver);
				return this.compDirServ.getDirectory(sc, path, version);
			}
			else
			{
				throw new RuntimeException("System Components with numbered subfolders should use the correct identifier naming convention.");
			}
		}
		else if (sc.isUsingParentSettings())
		{
			return this.compDirServ.getDirectory(sc, parentEntity.getIdentifier());
		}
		else
		{
			return this.compDirServ.getDirectory(sc, identifier);
		}
	}

	public String getExtensionForFile(File file)
	{
		if (file.isDirectory()) return null;
		else {
			String[] parts = file.getName().split("\\.");
			return parts[parts.length - 1];
		}
	}
	
	@Override
	public FileBrowserWrapper getFilesForComponentRoot(Component component, int entityId, List<String> newFiles)
	{
		FileBrowserProperties properties = this.initialiseFileBrowser(component, entityId, newFiles);
		
		FileBrowserWrapper fileBrowserWrapper = new FileBrowserWrapper();
		File directoryForEntity = properties.getDirectoryForEntity();
		List<FileWrapper> fwList = this.getFileWrapperListForDirectory(directoryForEntity, properties);
		fileBrowserWrapper.setFiles(fwList);
		// set number of files (not directories)
		fileBrowserWrapper.setNumberOfFiles(FileTools.countFilesInDirectory(directoryForEntity));
		return fileBrowserWrapper;
	}
	
	private List<FileWrapper> getFileWrapperListForDirectory(File directory, FileBrowserProperties properties) {
		ComponentEntity entity = properties.getEntity();
		List<String> newFiles = properties.getNewFiles();
		SystemComponent sc = properties.getSc();

		List<FileWrapper> fwList = new ArrayList<>();
		if (directory.exists() && directory.isDirectory()) {
			// get the list of files in the directory (might not be the root "entity" directory, but subdirectory)
			File[] fileList = directory.listFiles();
			// make a wrapper object for each file with the required information
			assert fileList != null;
			for (File file : fileList) {
				FileWrapper fw = new FileWrapper(file.getName(), file.getAbsolutePath(), new Date(file.lastModified()),
					this.getHighlightColour(entity, file, newFiles, sc.getFileNameRegexs()), this.getExtensionForFile(file), file.isDirectory());
				fwList.add(fw);
			}
			// sort the files alphabetically, with directories listed first
			fwList.sort(new FileWrapperComparator());
		}
		// Log debug message only; directory may not exist yet if it's not needed.

		return fwList;
	}

	private File getMatchingSubdirectory(File directory, String subdirectoryPath) {
		logger.debug("getMatchingSubdirectory : directory.absolutePath = "+directory.getAbsolutePath());
		logger.debug("getMatchingSubdirectory : subdirectoryPath = "+subdirectoryPath);
		File matchingSubdirectory = directory;
		List<String> subdirectoryNames = Arrays.asList(subdirectoryPath.split("/"));
		for(String subdirectoryName: subdirectoryNames)
			matchingSubdirectory = getSingleMatchingSubdirectory(matchingSubdirectory, subdirectoryName);
		return matchingSubdirectory;
	}

	private File getSingleMatchingSubdirectory(File directory, String subdirectoryName) {
		File matchingSubdirectory = null;
		if (directory.exists() && directory.isDirectory()) {
			// Find the desired subdirectory in the base directory (prevents injection of paths)
			for (File file : Objects.requireNonNull(directory.listFiles())) {
				logger.debug("testing : " + file.getName());
				if (file.getName().equals(subdirectoryName) && file.isDirectory()) {
					matchingSubdirectory = file;
					logger.debug("success");
					break;
				}
			}
		}
		return matchingSubdirectory;
	}

	private File getMatchingFile(File directory, String fileName) {
		logger.debug("getMatchingFile : directory.absolutePath = "+directory.getAbsolutePath());
		logger.debug("getMatchingFile : fileName = "+fileName);
		File matchingFile = null;
		if (directory.exists() && directory.isDirectory()) {
			// Find the desired file in the base directory (prevents injection of paths)
			for (File file : Objects.requireNonNull(directory.listFiles())) {
				logger.debug("testing : " + file.getName());
				if (file.getName().equals(fileName) && file.isFile()) {
					matchingFile = file;
					logger.debug("success");
					break;
				}
			}
		}
		return matchingFile;
	}
	
	
	/**
	 * 2019-06-15 : Changed "directory" to be just the subdirectory name (previously was the whole path)
	 * Note, we can't yet use a system component reference, because named subdirectories exist for the company, etc...
	 * Moving file system to a different storage mechanism could allow some virtual linking.
	 * 2019-06-20 : Processed subdirectoryName as an "escaped" name 
	 * 
	 * Calling this method implies that the root directory should exist if a subdirectoryName is provided.
	 * If the subdirectoryName is empty or null, the root directory is provided
	 */
	
	@Override
	public ResultWrapper getFilesForDirectory(String escapedSubdirectoryName, int scId, int entityId, List<String> newFiles) {
		FileBrowserProperties properties = this.initialiseFileBrowser(scId, entityId, newFiles);
		File directoryForEntity = properties.getDirectoryForEntity();

		ResultWrapper result;
		File matchingDirectory;
		if (escapedSubdirectoryName != null && !escapedSubdirectoryName.isEmpty()) {
			String subdirectoryName = decodeURIComponent(escapedSubdirectoryName);
			matchingDirectory = getMatchingSubdirectory(directoryForEntity, subdirectoryName);
		} else {
			matchingDirectory = properties.getDirectoryForEntity();
		}
		if (matchingDirectory == null) {
			String errorMessage = this.messages.getMessage("error.filebrowser.notadirectory", null, "Not a directory", null);
			result = new ResultWrapper(false, errorMessage, null, null);
		}
		else {
			List<FileWrapper> fwList = this.getFileWrapperListForDirectory(matchingDirectory, properties);
			result = new ResultWrapper(true, null, fwList, null);
		}
		return result;
	}
	
	public List<String> getFilesName (FileBrowserWrapper fileBrowserWrapper) {
		List<String> filesName = new ArrayList<>();
		fileBrowserWrapper.getFiles().forEach(file -> {
			if (file.isDirectory() && !file.getFileName().equals("Certificates")) {
				filesName.add(file.getFileName());
			}
		});
		return filesName;
	}
		
	private String getHighlightColour(ComponentEntity entity, File file, List<String> newFiles, List<RegularExpression> regExs)
	{
		String colour = FileBrowserServiceImpl.DEFAULT_COLOUR;

		// check session attribute for new files to highlight
		if (newFiles != null)
		{
			if (newFiles.contains(file.getName()))
			{
				colour = FileBrowserServiceImpl.NEW_FILE_COLOUR;
				newFiles.remove(file.getName());
			}
		}

		// check against system component specific regular expressions
		if ((entity.getIdentifier() != null)
				&& colour.equals(FileBrowserServiceImpl.DEFAULT_COLOUR))
		{
			String identifier = entity.getIdentifier().replaceAll("/", ".");
			if (file.getName().contains(identifier))
			{
				colour = FileBrowserServiceImpl.NAME_MATCH_COLOUR;
			}

			if (this.findRegexInFileName(file.getName(), regExs))
			{
				colour = FileBrowserServiceImpl.NAME_MATCH_COLOUR;
			}
		}

		return colour;
	}

	private FileBrowserProperties initialiseFileBrowser(int scId, int entityId, List<String> newFiles)
	{
		SystemComponent sc = this.scServ.findComponent(scId);
		return initialiseFileBrowser(sc, entityId, newFiles);
	}
	
	private FileBrowserProperties initialiseFileBrowser(Component component, int entityId, List<String> newFiles)
	{
		SystemComponent sc = this.scServ.findComponent(component);
		return initialiseFileBrowser(sc, entityId, newFiles);
	}
	
	private FileBrowserProperties initialiseFileBrowser(SystemComponent sc, int entityId, List<String> newFiles) {
		Object object = this.scServ.findComponentEntity(sc.getComponent(), entityId);
		ComponentEntity entity = (ComponentEntity) object;
		// This replaces the dreaded transient getDirectory() / setDirectory() properties on the entity
		File directoryForEntity = this.getDirectoryForComponentEntity(sc, entity.getIdentifier(), entity.getParentEntity());
		return new FileBrowserProperties(entity, entityId, sc, directoryForEntity, newFiles);
	}
}