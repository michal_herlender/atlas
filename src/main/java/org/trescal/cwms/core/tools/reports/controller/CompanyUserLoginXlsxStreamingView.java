package org.trescal.cwms.core.tools.reports.controller;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
import org.trescal.cwms.core.login.dto.UserLoginReportDTO;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.reports.view.XlsxViewDecorator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Component
public class CompanyUserLoginXlsxStreamingView extends AbstractXlsxStreamingView {

	@Autowired
	private MessageSource messages;

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
									  HttpServletResponse response) throws Exception {
		Locale locale = LocaleContextHolder.getLocale();
		@SuppressWarnings("unchecked")
		List<UserLoginReportDTO> dtos = (List<UserLoginReportDTO>) model.get("dtos");
		String companyName = (String) model.get("companyName");

		String fileName = this.messages.getMessage("report.companyuserlogin.filename", null, "Company User Login Report", locale) +
			" " +
			companyName +
			" - " +
			DateTools.dtf.format(new Date()) +
			".xlsx";
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

		SXSSFWorkbook streamingWorkbook = (SXSSFWorkbook) workbook;
		Sheet sheet = workbook.createSheet("Data");
		XlsxViewDecorator decorator = new XlsxViewDecorator(sheet, streamingWorkbook, true, null);

		addHeaders(decorator, locale);
		addValues(dtos, decorator);
		decorator.autoSizeColumns();
	}

	private void addHeaders(XlsxViewDecorator decorator, Locale locale) {
		int rowIndex = 0;
		int colIndex = 0;
		CellStyle styleText = decorator.getStyleHolder().getTextStyle();

		String textPosition = this.messages.getMessage("position", null, "Position", locale);
		String textVisitor = this.messages.getMessage("visitor", null, "Visitor", locale);
		String textSubdivision = this.messages.getMessage("subdivision", null, "Subdivision", locale);
		String textJobRole = this.messages.getMessage("jobrole", null, "Job Role", locale);
		String textLogins = this.messages.getMessage("numberoflogins", null, "Number Of Logins", locale);

		decorator.createCell(rowIndex, colIndex++, textPosition, styleText);
		decorator.createCell(rowIndex, colIndex++, textVisitor, styleText);
		decorator.createCell(rowIndex, colIndex++, textSubdivision, styleText);
		decorator.createCell(rowIndex, colIndex++, textJobRole, styleText);
		decorator.createCell(rowIndex, colIndex, textLogins, styleText);
	}

	private void addValues(List<UserLoginReportDTO> dtos, XlsxViewDecorator decorator) {
		CellStyle styleText = decorator.getStyleHolder().getTextStyle();
		CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();

		int rowIndex = 1;
		for (UserLoginReportDTO dto : dtos) {
			int colIndex = 0;
			decorator.createCell(rowIndex, colIndex++, dto.getPos(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getName(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getSubname(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getRole(), styleText);
			decorator.createCell(rowIndex, colIndex, dto.getTotal(), styleInteger);
			rowIndex++;
		}
	}
}
