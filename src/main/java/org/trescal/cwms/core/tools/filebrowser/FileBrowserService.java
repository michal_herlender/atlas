package org.trescal.cwms.core.tools.filebrowser;

import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;

/**
 * Interface with functions to browse files for a {@link SystemComponent}.
 * 
 * @author jamiev
 */
public interface FileBrowserService
{
	/**
	 * Deletes the file located at the given filePath
	 * 
	 * @param fileName the name of the file to delete
	 * @param subdirectoryName the subdirectory name, if any
	 * @param scId the ID of the {@link SystemComponent} the directory belongs
	 *        to
	 * @param entityId the ID of the component entity the directory is
	 *        associated with
	 * 
	 * @return true if the file was successfully deleted, false otherwise
	 */
	boolean deleteFile(String fileName, String subdirectoryName, int scId, int entityId);

	/**
	 * Returns a {@link List} of {@link FileWrapper}s containing details on all
	 * of the files in the root directory of the {@link Component} with the
	 * given ID
	 * 
	 * @param component the {@link Component} of the entity
	 * @param entityId the ID of the entity
	 * @param newFiles added by method caller
	 * @return a {@link ResultWrapper} containing the success of the operation,
	 *         any error messages and the {@link FileWrapper} results
	 */
	FileBrowserWrapper getFilesForComponentRoot(Component component, int entityId, List<String> newFiles);

	/**
	 * Returns a {@link List} of {@link FileWrapper}s containing details on all
	 * of the files in the given directory
	 * 
	 * @param subdirectoryName the directory to get the files from 
	 *        (just the subdir name 2019-06-15)
	 * @param scId the ID of the {@link SystemComponent} the directory belongs
	 *        to
	 * @param entityId the ID of the component entity the directory is
	 *        associated with
	 * @param newFiles added by method caller
	 * @return a {@link ResultWrapper} containing the success of the operation,
	 *         any error messages and the {@link FileWrapper} results
	 */
	ResultWrapper getFilesForDirectory(String subdirectoryName, int scId, int entityId, List<String> newFiles);
	
	List<String> getFilesName (FileBrowserWrapper fileBrowserWrapper);
}
