package org.trescal.cwms.core.tools;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;

import javax.sql.DataSource;

import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.EngineConstants;
import org.eclipse.birt.report.engine.api.EngineException;
import org.eclipse.birt.report.engine.api.IPDFRenderOption;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportEngineFactory;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.engine.api.PDFRenderOption;
import org.eclipse.birt.report.model.api.DesignElementHandle;
import org.eclipse.birt.report.model.api.IResourceLocator;
import org.eclipse.birt.report.model.api.MasterPageHandle;
import org.eclipse.birt.report.model.api.ModuleHandle;
import org.eclipse.birt.report.model.api.SlotHandle;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.eclipse.birt.report.model.api.elements.DesignChoiceConstants;
import org.eclipse.birt.report.model.api.metadata.IElementDefn;
import org.eclipse.core.internal.registry.RegistryProviderFactory;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;

import java.util.logging.Logger;

@Component("birtEngine")
public class BirtEngine implements InitializingBean, DisposableBean {
	@Autowired
	private ApplicationContext applicationContext; 
	@Autowired
	private DataSource dataSource;

	private IReportEngine engine;
	private IResourceLocator resourceLocator;
	
	private final static org.slf4j.Logger logger = LoggerFactory.getILoggerFactory().getLogger(BirtEngine.class.getName());
	private final static Logger javaLogger = Logger.getLogger(BirtEngine.class.getName());
	public final static String APP_CONTEXT_SPRING_KEY = "spring";	// Allows for storing Spring ApplicationContext in report appContext
	public final static String APP_CONTEXT_MODEL = "model";			// Allows for storing user defined model in report appContext

	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			// initialize BirtEngine (unfortunately requires Java logger)
			EngineConfig config = new EngineConfig();
			config.setLogger(javaLogger);
			resourceLocator = new IResourceLocator() {
				@Override
				public URL findResource(ModuleHandle handle, String filename, int type,
						@SuppressWarnings("rawtypes") Map context) {
					return findResource(handle, filename, type);
				}

				@Override
				public URL findResource(ModuleHandle handle, String filename, int type) {
					logger.debug("requested resource: "+filename+" type "+type);
					if (filename.startsWith("file:")) {
						try {
							logger.debug("file resource: "+filename);
							return new URL(filename);
						} catch (MalformedURLException e) {
							logger.error(e.getMessage(), e);
						}
					} else if (!filename.startsWith("/")) {
						filename = "/" + filename;
					}
					// It seems library js/jar resources may be searched for with same type
					// Message properties are being loaded as 'others'
					if ((type == IResourceLocator.LIBRARY) && filename.endsWith(".rptlibrary") ||
						(type == IResourceLocator.IMAGE) ||
						(type == IResourceLocator.OTHERS)) {
						filename = "/birt/library"+filename;
					}
					logger.debug("finding resource: "+filename);
					return this.getClass().getResource(filename);
				}
			};
			config.setResourceLocator(resourceLocator);

			Platform.startup(config);

			IReportEngineFactory factory = (IReportEngineFactory) Platform
					.createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY);
			engine = factory.createReportEngine(config);
			engine.changeLogLevel(Level.WARNING);	// Was Level.FINEST 2018-04-20, changed to WARNING
		} catch (Exception ex) {
			javaLogger.log(Level.SEVERE, "Can't start BirtEngine", ex);
			throw ex;
		}
	}

	@Override
	public void destroy() {
		try {
			engine.destroy();
			Platform.shutdown();
			RegistryProviderFactory.releaseDefault();
		} catch (Exception e1) {
			// Ignore
		}
	}

	/**
	 * Method to use when for adjusting page size from A4 to US letter size
	 * (thereby not affecting "custom" PDFs like stickers)  
	 * TODO : consider adjustment of margins for alignment with envelope, etc...
	 * @param reportRunnable
	 */
    @SuppressWarnings("unchecked")
	private void adjustSizeToUsLetter(IReportRunnable reportRunnable) {
	    DesignElementHandle designHandle = reportRunnable.getDesignHandle();
	    IElementDefn elementDefn = designHandle.getDefn();
	    for (int i = 0; i < elementDefn.getSlotCount(); i++) {
	      SlotHandle slotHandle = designHandle.getSlot(i);
	      for (DesignElementHandle elementHandle: (List<DesignElementHandle>) slotHandle.getContents()) {
	        if (!(elementHandle instanceof MasterPageHandle)) continue;

			try {
				MasterPageHandle mph = (MasterPageHandle) elementHandle;
				if (mph.getPageType().equals(DesignChoiceConstants.PAGE_SIZE_A4)) {
					mph.setPageType(DesignChoiceConstants.PAGE_SIZE_US_LETTER);
				}
			}
			catch (SemanticException e) {
				throw new RuntimeException(e);
			}
	      }
	    }
	}
	
	@SuppressWarnings("unchecked")
	public void generatePDF(String reportPath, Map<String, Object> parameters, OutputStream os, Locale locale)
			throws EngineException {

        IReportRunnable design = engine.openReportDesign(this.getClass().getResourceAsStream(reportPath));
        if (locale.equals(SupportedLocales.ENGLISH_US)) {
        	adjustSizeToUsLetter(design);
        }
        
        // Create task to run and render the report,
        IRunAndRenderTask task = engine.createRunAndRenderTask(design);

        try (Connection con = dataSource.getConnection()) {
            // Set parent classloader for engine
            task.getAppContext().put(EngineConstants.APPCONTEXT_CLASSLOADER_KEY, this.getClass().getClassLoader());
            task.getAppContext().put("OdaJDBCDriverPassInConnection", con);
            task.getAppContext().put("OdaJDBCDriverPassInConnectionCloseAfterUse", false);
            task.getAppContext().put(APP_CONTEXT_SPRING_KEY, applicationContext);
            task.setLocale(locale);

            // Set parameter values and validate
            task.setParameterValues(parameters);
            task.validateParameters();

            // Setup rendering to HTML
            PDFRenderOption options = new PDFRenderOption();
            options.setOutputStream(os);
            options.setOption(IPDFRenderOption.PDF_HYPHENATION, true);
            options.setOption(IPDFRenderOption.PDF_TEXT_WRAPPING, true);
            options.setOutputFormat("pdf");

            // Setting this to true removes html and body tags
            task.setRenderOption(options);
            // run and render report
            long startTime = System.currentTimeMillis();
            javaLogger.info("before task.run()");
            task.run();
            long finishTime = System.currentTimeMillis();
            javaLogger.info("after task.run() in "+(finishTime-startTime)+" ms");
            task.close();
            javaLogger.info("after task.close()");
            if (!task.getErrors().isEmpty()) {
                for (Object o : task.getErrors()) {
                    javaLogger.warning("Birt error: "+o.toString());
                }
                throw new EngineException("UNKNOWN");
            }
        } catch (SQLException e) {
            throw new EngineException("Unable to get database connection: {}", e.getMessage());
        }
	}

	public byte[] generatePDFAsByteArray(String reportPath, Map<String, Object> parameters, Locale locale) throws EngineException {
	    ByteArrayOutputStream os = new ByteArrayOutputStream();
        this.generatePDF(reportPath, parameters, os, locale);
	    return os.toByteArray();
	}
}
