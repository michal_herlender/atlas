package org.trescal.cwms.core.tools.pathservice;

import java.io.File;

public interface PathService
{
	/*
	 * Resolves actual file server root as determined through configuration attributes
	 * May be relative or absolute file path
	 * It is intended that this should be used for most file storage usage 
	 */
	public String getFileServerRootPath();
	/*
	 * Provides a temporary private directory for file generation
	 * It is intended that this be provided by the servlet context
	 */
	public String getTempDirPath();

	public File getTempDirFile();
}