package org.trescal.cwms.core.tools.labelprinting.dymo;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.labelprinting.dymo.dto.DymoJobFolderDto;

@Component
public class DymoLabelJobFolder {
	@Autowired
	private MessageSource messageSource;
	
	public static final String FILENAME = "jobtagfolder.label";
	public static final String TEMPLATE_URL = "labels/dymo/jobtagfolder.label";
	
	// Default English values for labels if no translation loaded
	public static final String LABEL_TITLE = "Job File";
	public static final String LABEL_JOB_NUMBER = "Job number:";
	public static final String LABEL_ITEMS = "Items:";
	public static final String LABEL_COMPANY = "Company:";
	public static final String LABEL_CONTACT = "Contact:";
	public static final String LABEL_DATE_IN = "Date received:";
	public static final String LABEL_DATE_DUE = "Date due:";
	
	// Message codes for labels
	public static final String MESSAGE_CODE_TITLE = "dymo.jobtagfolder.title";
	public static final String MESSAGE_CODE_JOB_NUMBER = "dymo.jobtagfolder.jobnumber";
	public static final String MESSAGE_CODE_ITEMS = "dymo.jobtagfolder.items";
	public static final String MESSAGE_CODE_COMPANY = "dymo.jobtagfolder.company";
	public static final String MESSAGE_CODE_CONTACT = "dymo.jobtagfolder.contact";
	public static final String MESSAGE_CODE_DATE_IN = "dymo.jobtagfolder.datein";
	public static final String MESSAGE_CODE_DATE_DUE = "dymo.jobtagfolder.datedue";
	
	public DymoJobFolderDto getLabelDto(Job job, Locale locale) {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(locale);
        LocalDate dateIn = job.getReceiptDate() != null ? DateTools.localDateTimeFromZonedDateTime(job.getReceiptDate()).toLocalDate() : null;

        DymoJobFolderDto dto = new DymoJobFolderDto();
        dto.setCopies(1);
        dto.setLabelTemplateUrl(TEMPLATE_URL);
        dto.setConame(job.getCon().getSub().getComp().getConame());
        dto.setDate(dateIn != null ? dateIn.format(formatter) : "");
        dto.setDate_due(job.getAgreedDelDate() != null ? job.getAgreedDelDate().format(formatter) : "");
        dto.setItemno(String.valueOf(job.getItems().size()));
        dto.setJobno(job.getJobno());
        dto.setLabel_company(messageSource.getMessage(MESSAGE_CODE_COMPANY, null, LABEL_COMPANY, locale));
        dto.setLabel_contact(messageSource.getMessage(MESSAGE_CODE_CONTACT, null, LABEL_CONTACT, locale));
        dto.setLabel_date_in(messageSource.getMessage(MESSAGE_CODE_DATE_IN, null, LABEL_DATE_IN, locale));
        dto.setLabel_date_due(messageSource.getMessage(MESSAGE_CODE_DATE_DUE, null, LABEL_DATE_DUE, locale));
        dto.setLabel_items(messageSource.getMessage(MESSAGE_CODE_ITEMS, null, LABEL_ITEMS, locale));
        dto.setLabel_job_number(messageSource.getMessage(MESSAGE_CODE_JOB_NUMBER, null, LABEL_JOB_NUMBER, locale));
        dto.setLabel_title(messageSource.getMessage(MESSAGE_CODE_TITLE, null, LABEL_TITLE, locale));
        dto.setName(job.getCon().getName());
		dto.setSubdiv(job.getCon().getSub().getSubname());
		return dto;
	}
	
}
