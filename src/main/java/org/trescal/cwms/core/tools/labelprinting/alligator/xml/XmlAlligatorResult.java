package org.trescal.cwms.core.tools.labelprinting.alligator.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder={"success", "message"})
public class XmlAlligatorResult {
	@XmlAttribute
	private Boolean success;
	@XmlAttribute
	private String message;
	
	public XmlAlligatorResult() {
		this.success = true;
		this.message = "";
	}
	
	public XmlAlligatorResult(boolean success, String message) {
		this.success = success;
		this.message = message;
	}
	
}
