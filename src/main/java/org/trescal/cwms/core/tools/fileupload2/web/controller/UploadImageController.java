package org.trescal.cwms.core.tools.fileupload2.web.controller;

import java.io.IOException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.documents.images.image.Image;
import org.trescal.cwms.core.documents.images.image.ImageType;
import org.trescal.cwms.core.documents.images.image.db.ImageService;
import org.trescal.cwms.core.documents.images.instrumentmodelimage.InstrumentModelImage;
import org.trescal.cwms.core.documents.images.jobitemimage.JobItemImage;
import org.trescal.cwms.core.documents.images.labelimage.LabelImage;
import org.trescal.cwms.core.exception.controller.FileController;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.tools.ImageTools;
import org.trescal.cwms.core.tools.fileupload2.utils.UploadInfo;
import org.trescal.cwms.core.tools.fileupload2.utils.UploadListener;
import org.trescal.cwms.core.tools.fileupload2.utils.validfiletype.ValidFileType;
import org.trescal.cwms.core.tools.fileupload2.utils.validfiletype.db.ValidFileTypeService;
import org.trescal.cwms.core.tools.fileupload2.web.ajax.AjaxFileUploadMultipartResolver;
import org.trescal.cwms.core.tools.fileupload2.web.formobjects.ImageUploadForm;
import org.trescal.cwms.core.tools.fileupload2.web.propertyEditors.MultipartFilePropertyEditor;
import org.trescal.cwms.files.images.entity.ImageFileData;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Controller called by the AJAX file uploader. When a file is uploaded Spring
 * will now manage it's upload via {@link AjaxFileUploadMultipartResolver},
 * which starts the {@link UploadListener} that updates the progress of the file
 * upload via a session variable called 'uploadInfo' which resolves to an
 * instance of {@link UploadInfo}. {@link UploadInfo} contains the current
 * progress data on the upload (which is updated by the {@link UploadListener})
 * and extra file information (such as the status of the upload and any errors
 * encountered). This controller is only called once the files have been
 * uploaded. It's purpose is to validate that the file uploaded is allowed and
 * then to copy the file to the requested location - a {@link SystemComponent}
 * file location.
 */
@Controller @FileController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class UploadImageController
{
	private static final Logger logger = LoggerFactory.getLogger(UploadImageController.class);
	
	@Value("#{props['cwms.config.images.tempupload']}")
	private String tempImageFileUploadFolder;
	@Value("#{props['cwms.config.images.thumbNailWidth']}")
	private Double thumbNailWidth;
	@Autowired
	private InstrumentModelService modelService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private UserService userService;
	@Autowired
	private ValidFileTypeService vftServ;
	@Autowired
	private ImageService imgServ;
	
	/**
	 * Register proerty editor for handling multipart files.
	 *
	 * @param binder ServletRequestDataBinder
	 * @throws ServletException
	 */
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws ServletException
	{
		binder.registerCustomEditor(MultipartFile.class, new MultipartFilePropertyEditor());
	}
	
	@ModelAttribute("command")
	protected ImageUploadForm formBackingObject() {
		return new ImageUploadForm();
	}

	/**
	 * Spring-MVC method - executed after form submission.
	 * 
	 * @param form ImageUploadForm
	 * @param errors BindException
	 * @param request HttpServletRequest
	 * @throws Exception
	 * @return ModelAndView
	 */
	@RequestMapping(value="/uploadimage.htm", method=RequestMethod.POST)
	public final ModelAndView onSubmit(HttpServletRequest request, @ModelAttribute("command") ImageUploadForm form, BindingResult errors) throws Exception
	{
		//ImageUploadForm form = (ImageUploadForm) command;
		if (request.getParameter("cancel") != null) {
			logger.info("Canceling operation.");
		}
		else {
			MultipartFile multipartFile = form.getFile();
			UploadInfo info = new UploadInfo();
			if (request.getSession().getAttribute("uploadInfo") != null) {
				info = (UploadInfo) request.getSession().getAttribute("uploadInfo");
			}
			// check that the file type is on the 'allowed' list for images
			if (!this.vftServ.isValidFileType(multipartFile, ValidFileType.FileTypeUsage.IMAGE)) {
				errors.rejectValue("file", "errors.upload.extension.invalid", null, "invalid file type");
				logger.warn("The file type is invalid.");
				info.setStatus("filetypeerror");
			}
			else {
				Image image = null;
				if (form.getEntityType().equals(ImageType.JOBITEM)){
					JobItemImage jimage = new JobItemImage();
					image = jimage;
				} 
				else if (form.getEntityType().equals(ImageType.INSTRUMENTMODEL)){
					InstrumentModelImage iimage = new InstrumentModelImage();
					image = iimage;
				}
				else if (form.getEntityType().equals(ImageType.LABEL)) {
					LabelImage labelImage = new LabelImage();
					image = labelImage;
				}
				image.setFileName(multipartFile.getOriginalFilename());
				if(image.getImageFile() == null) image.setImageFile(new ImageFileData());
				image.getImageFile().setFileData(multipartFile.getBytes());
                imgServ.uploadImage(image, request);
                info.setFileId(image.getId());
				info.setFileName(multipartFile.getOriginalFilename());
				info.setForm(form);
				info.setStatus("done");
			}
			request.getSession().setAttribute("uploadInfo", info);
		}
		return new ModelAndView("/trescal/core/tools/fileupload/fileupload");
	}
	
	@RequestMapping(value="/uploadimage.htm", method=RequestMethod.GET)
	public String referenceData() {
		return "/trescal/core/tools/fileupload/fileupload";
	}
	
	@RequestMapping(value="/uploadimage.json", method=RequestMethod.POST)
	@ResponseBody
	public Integer uploadImage(
			@ModelAttribute("image") String encodedImage,
			@ModelAttribute("entityType") ImageType entityType,
			@ModelAttribute("entityId") Integer entityId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName) throws Exception
	{
		Subdiv subdiv = subdivService.get(subdivDto.getKey());
		Contact contact = userService.get(userName).getCon();
		Decoder decoder = Base64.getDecoder();
		try {
			// Remove the first 23 characters (meta information)
			byte[] buffer = decoder.decode(encodedImage.substring(23));
			String imageNumber = numerationService.generateNumber(NumerationType.IMAGE, subdiv.getComp(), subdiv);
			String fileName = imageNumber + ".jpg";
			// generate and save image
			Image image;
			if (entityType.equals(ImageType.INSTRUMENTMODEL)) {
				InstrumentModelImage modelImage = new InstrumentModelImage();
				modelImage.setModel(modelService.findLazyInstrumentModel(entityId));
				image = modelImage;
			}
			else if (entityType.equals(ImageType.JOBITEM)) {
				JobItemImage jobItemImage = new JobItemImage();
				jobItemImage.setJobitem(jobItemService.findJobItem(entityId));
				image = jobItemImage;
			}
			else if (entityType.equals(ImageType.LABEL)) {
				LabelImage labelImage = new LabelImage();
				image = labelImage;
			}
			else throw new Exception("Unsupported entity type + '" + entityType + "'");
			image.setFileName(fileName);
			if(image.getImageFile() == null) image.setImageFile(new ImageFileData());
			image.getImageFile().setFileData(buffer);
            image.setAddedBy(contact);
            image.setAddedOn(new Date());
            ImageTools.generateThumbNailImageDb(thumbNailWidth, image);
            return imgServ.insertImage(image);
		}
		catch(IOException ex) {
			logger.error(ex.getMessage());
			throw ex;
		}
		catch(Exception ex) {
			logger.error(ex.getMessage());
			throw ex;
		}
	}
}