package org.trescal.cwms.core.tools.labelprinting.certificate;

import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;

/**
 * Encapsulation of logic to look up certificate label settings 
 * @author Galen Beck - 2016-05-19
 */
public interface CertificateLabelSettingsService {
	
	CertificateLabelSettings get(CertLink certLink);
	
	AccreditedLab getAccreditedLab(Certificate certificate);
}
