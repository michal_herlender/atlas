package org.trescal.cwms.core.tools.printjob;

import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.print.DocFlavor;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaTray;
import javax.print.attribute.standard.PageRanges;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.papertype.PaperType;
import org.trescal.cwms.core.system.entity.printer.Printer;
import org.trescal.cwms.core.system.entity.printer.db.PrinterService;
import org.trescal.cwms.core.system.entity.printertray.PrinterTray;
import org.trescal.cwms.core.system.entity.printertray.db.PrinterTrayService;
import org.trescal.cwms.core.system.entity.printfilerule.PrintFileRule;
import org.trescal.cwms.core.system.entity.printfilerule.db.PrintFileRuleService;
import org.trescal.cwms.core.tools.FileTools;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

public class PrintJobServiceImpl implements PrintJobService {
	// this is 11.69" x 8.27" calculated as 72nds of inches
	private static final double A4_HEIGHT = 841;
	private static final double A4_WIDTH = 595;

	// doc flavor for all print jobs
	private static final DocFlavor DOC_FLAVOR = DocFlavor.INPUT_STREAM.AUTOSENSE;

	// this is 11.69" x 8.27" shrunk to 96% (as per Adobe
	// Acrobat) and calculated as 72nds of inches
	private static final double IMAGEABLE_HEIGHT = 808;
	private static final double IMAGEABLE_WIDTH = 571;
	// margin is HALF of the difference between A4 size and imageable area so
	// that the resulting document is centralised
	private static final double IMAGEABLE_X = 12;
	private static final double IMAGEABLE_Y = 16;

	private PrintFileRuleService pfrServ;
	private String printerFileType;
	private PrinterService printerServ;
	private PrinterTrayService printTrayServ;

	private void addPageToBookInCorrectOrientation(PageFormat portrait, PageFormat landscape, Book book,
			PrintablePDFPage painter, PDFFile pdfFile, int pageNo) {
		PDFPage page = pdfFile.getPage(pageNo);

		if (page.getHeight() < page.getWidth()) {
			book.append(painter, landscape, 1);
		} else {
			book.append(painter, portrait, 1);
		}
	}

	@Override
	public ResultWrapper ajaxPrintFile(String filePath, int personId, int quantity) {
		Printer printer = this.printerServ.getDefaultPrinterForContact(personId);
		if (printer == null) {
			return new ResultWrapper(false, "Please set a default printer.", null, null);
		}

		if (this.printFile(filePath, printer, quantity)) {
			return new ResultWrapper(true, null, null, null);
		} else {
			return new ResultWrapper(false, "An unexpected error occurred.", null, null);
		}
	}

	/**
	 * Looks for the {@link PaperType} defined in Constants to use if the PDF
	 * document sent to print has no specified media tray.
	 * 
	 * @param printer the {@link Printer} being used.
	 * @return the {@link MediaTray}
	 */
	private MediaTray[] getDefaultMediaTray(Printer printer) {
		// set defaults as MAIN
		MediaTray[] trays = new MediaTray[2];
		trays[0] = MediaTray.MAIN;
		trays[1] = MediaTray.MAIN;

		for (PrinterTray tray : printer.getTrays()) {
			if (tray.getPaperType().equals(Constants.DEFAULT_PAPER_TYPE)) {
				MediaTray mt = (tray.getMediaTrayRef() != null) ? tray.getMediaTrayRef().getMediaTray()
						: MediaTray.MAIN;
				trays[0] = mt;
				trays[1] = mt;
			}
		}

		return trays;
	}

	private MediaTray[] getMediaTraysForPrinterAndPaperTypes(Printer printer, PaperType firstType, PaperType restType) {
		if (firstType == null) {
			firstType = Constants.DEFAULT_PAPER_TYPE;
		}
		if (restType == null) {
			restType = Constants.DEFAULT_PAPER_TYPE;
		}

		MediaTray firstTray = this.printTrayServ.getMediaTrayForPaperType(printer.getId(), firstType);
		MediaTray restTray = this.printTrayServ.getMediaTrayForPaperType(printer.getId(), restType);
		MediaTray fallbackTray = this.printTrayServ.getMediaTrayForPaperType(printer.getId(),
				Constants.DEFAULT_PAPER_TYPE);

		if (firstTray == null) {
			firstTray = fallbackTray;
		}
		if (restTray == null) {
			restTray = fallbackTray;
		}

		if ((firstTray != null) || (restTray != null)) {
			MediaTray[] trays = new MediaTray[2];
			trays[0] = firstTray;
			trays[1] = restTray;
			return trays;
		}

		return null;
	}

	/**
	 * Finds a Java-friendly {@link PrintService} for the given {@link Printer}
	 * 
	 * @param printer the {@link Printer} to get the {@link PrintService} for
	 * @return the {@link PrintService} if found, otherwise null
	 */
	private PrintService getPrintServiceForPrinter(Printer printer) {
		PrintService printService = null;
		PrintService[] printServices = PrintServiceLookup.lookupPrintServices(DOC_FLAVOR, null);
		if (printServices.length > 0) {
			printerLoop: for (PrintService ps : printServices) {
				if (ps.getName().trim().equalsIgnoreCase(printer.getPath())) {
					printService = ps;
					break printerLoop;
				}
			}
		}
		return printService;
	}

	/**
	 * Creates and returns a new {@link PrinterJob} for the given
	 * {@link PrintService} and sets the amount of copies required
	 * 
	 * @param printService the {@link PrintService} to use
	 * @param copies       the amount of copies required
	 * @return the initialised {@link PrinterJob}
	 */
	private PrinterJob initialiseNewJobForPrintService(PrintService printService, int copies) {
		try {
			PrinterJob job = PrinterJob.getPrinterJob();
			job.setPrintService(printService);
			job.setCopies(copies);
			job.setJobName("CWMS Print Job");
			return job;
		} catch (PrinterException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean printFile(String filePath, Printer printer, int quantity) {
		return this.printFile(filePath, printer, quantity, false);
	}

	private boolean printFile(String filePath, Printer printer, int quantity, boolean ignoreQuantityOverrides) {
		if ((filePath != null) && !filePath.trim().isEmpty() && new File(filePath).exists()) {
			File file = new File(filePath);
			String ext = FileTools.getFileExtension(file);

			// if file is html, print using PCL
			if (ext.equalsIgnoreCase("html") || ext.equalsIgnoreCase("htm")) {
				return this.printHTMLFile(filePath, printer, quantity, ignoreQuantityOverrides);
			}
			// if file is PDF, print using PDFRenderer/2D Graphics mash-up
			else if (ext.equalsIgnoreCase("pdf")) {
				return this.printPDFFile(filePath, printer, quantity, ignoreQuantityOverrides);
			}
			// otherwise we can't print the file
			else {
				System.out.println("Printing not supported for file type '" + ext + "'.");
				return false;
			}
		}

		return false;
	}

	@Override
	public boolean printFileIgnoringQuantityOverrides(String filePath, Printer printer, int quantity) {
		return this.printFile(filePath, printer, quantity, true);
	}

	private boolean printFileUsingGraphicsEngine(FileInputStream fis, Printer printer, MediaTray[] mediaTray,
			int quantity) {
		try {
			// read in file
			byte[] fileContent = new byte[fis.available()];
			fis.read(fileContent, 0, fis.available());
			ByteBuffer bb = ByteBuffer.wrap(fileContent);

			// convert file to printable PDF pages
			PDFFile pdfFile = new PDFFile(bb);
			PrintablePDFPage pages = new PrintablePDFPage(pdfFile);

			// get print service
			PrintService printService = this.getPrintServiceForPrinter(printer);

			// if print service could not be found
			if (printService == null) {
				System.out.println(printer);
				System.out.println("No print service found for printer");
				return false;
			}

			// prepare a new job for the print service
			PrinterJob job = this.initialiseNewJobForPrintService(printService, quantity);

			// initialise static page sizes and orientation
			PageFormat portrait = job.defaultPage();
			PageFormat landscape = job.defaultPage();
			landscape.setOrientation(PageFormat.LANDSCAPE);
			Paper paper = new Paper();
			paper.setSize(A4_WIDTH, A4_HEIGHT);
			paper.setImageableArea(IMAGEABLE_X, IMAGEABLE_Y, IMAGEABLE_WIDTH, IMAGEABLE_HEIGHT);
			portrait.setPaper(paper);
			landscape.setPaper(paper);

			// create new book to append PDF pages to
			Book book = new Book();

			// if all pages are being printed to same tray
			if (mediaTray[0].equals(mediaTray[1])) {
				// iterate over all pages of the PDF
				for (int pageNo = 1; pageNo <= pdfFile.getNumPages(); pageNo++) {
					// add page ensuring correctly oriented
					this.addPageToBookInCorrectOrientation(portrait, landscape, book, pages, pdfFile, pageNo);
				}

				// set paged book into print job
				job.setPageable(book);

				// set printer tray
				PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
				pras.add(mediaTray[0]);

				// print the job
				job.print(pras);
			}
			// else if first page tray differs from the rest
			else {
				// for each copy
				for (int i = quantity; i > 0; i--) {
					// iterate over all pages of the PDF
					for (int pageNo = 1; pageNo <= pdfFile.getNumPages(); pageNo++) {
						// we have to send through each page as a new print job
						// in
						// this case (limitation of the java print library)
						job = this.initialiseNewJobForPrintService(printService, 1);

						// add page ensuring correctly oriented
						this.addPageToBookInCorrectOrientation(portrait, landscape, book, pages, pdfFile, pageNo);

						// set paged book into print job
						job.setPageable(book);

						// set printer tray and page to print
						PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
						pras.add((pageNo == 1) ? mediaTray[0] : mediaTray[1]);
						pras.add(new PageRanges(pageNo));

						// print the job
						job.print(pras);
					}
				}
			}

			// file was successfully printed!
			return true;
		}
		// an error occurred reading the PDF file
		catch (IOException ioe) {
			ioe.printStackTrace();
			return false;
		}
		// an error occurred printing with the given settings
		catch (PrinterException pe) {
			pe.printStackTrace();
			return false;
		}
	}

	private boolean printHTMLFile(String filePath, Printer printer, int quantity, boolean ignoreQuantityOverrides) {
		boolean success = false;
		// reloading to prevent lazy initialisation exception
		printer = this.printerServ.get(printer.getId());
		if (this.printerFileType.trim().equalsIgnoreCase("PCL")) {
			// get the name of the file from filePath
			File file = new File(filePath);
			String fileName = file.getName();

			// find any rules for a file with this name
			PrintFileRule rule = this.pfrServ.findRuleForFileName(fileName);

			// if there are no file-specific rules
			if (rule != null)
				// check if rule specifies overriding printer
				if (rule.getPrinter() != null)
					printer = rule.getPrinter();
			// check if rule specifies overriding quantity
			if ((rule.getCopies() != null) && !ignoreQuantityOverrides)
				quantity = rule.getCopies();
		}
		return success;
	}

	private boolean printPDFFile(String filePath, Printer printer, int quantity, boolean ignoreQuantityOverrides) {
		boolean success = false;
		// reloading to prevent lazy initialisation exception
		printer = this.printerServ.get(printer.getId());

		MediaTray mediaTrays[] = null;

		// get the name of the file from filePath
		File file = new File(filePath);
		String fileName = file.getName();

		// find any rules for a file with this name
		PrintFileRule rule = this.pfrServ.findRuleForFileName(fileName);

		// if there are no file-specific rules
		if (rule == null) {
			mediaTrays = this.getDefaultMediaTray(printer);
		}
		// else if there are rules
		else {
			// check if rule specifies overriding printer
			if (rule.getPrinter() != null) {
				printer = rule.getPrinter();
			}

			// if rule specifies which pages to paper types to print on
			if ((rule.getFirstPagePaperType() != null) || (rule.getRestPagePaperType() != null)) {
				// get trays in the printer for these paper types
				mediaTrays = this.getMediaTraysForPrinterAndPaperTypes(printer, rule.getFirstPagePaperType(),
						rule.getRestPagePaperType());
			}
			// otherwise
			else {
				// get trays for printer for the default paper type
				mediaTrays = this.getDefaultMediaTray(printer);
			}

			// check if rule specifies overriding quantity
			if ((rule.getCopies() != null) && !ignoreQuantityOverrides) {
				quantity = rule.getCopies();
			}
		}

		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);

			success = this.printFileUsingGraphicsEngine(fis, printer, mediaTrays, quantity);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			success = false;
		}

		return success;
	}

	public void setPfrServ(PrintFileRuleService pfrServ) {
		this.pfrServ = pfrServ;
	}

	public void setPrinterFileType(String printerFileType) {
		this.printerFileType = printerFileType;
	}

	public void setPrinterServ(PrinterService printerServ) {
		this.printerServ = printerServ;
	}

	public void setPrintTrayServ(PrinterTrayService printTrayServ) {
		this.printTrayServ = printTrayServ;
	}

	public void setSignPrintedCerts(boolean signPrintedCerts) {
	}

	public void setUserServ(UserService userServ) {
	}
}
