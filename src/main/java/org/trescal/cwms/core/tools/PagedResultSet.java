package org.trescal.cwms.core.tools;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.trescal.cwms.core.system.Constants;

/**
 * 'Holder' bean which can be used to paginate a collection of results which
 * needs to span several pages.
 * 
 * @author richard
 */
public class PagedResultSet<T> {
	/**
	 * The page number of the current page in the results.
	 */
	private int currentPage;
	/**
	 * The number of pages that will be generated from the results and the
	 * resultsperpage.
	 */
	private int pageCount;

	/**
	 * The collection of results to display on the *current* page - i.e. a subset of
	 * the full set of results.
	 */
	private Collection<T> results;

	/**
	 * Count of the total number of results prior to pagination.
	 */
	private int resultsCount;

	/**
	 * Collection to hold the id's of the full resultsCount for excell export use
	 */
	private Collection<Integer> resultsIds;
	/**
	 * Holds the maximum number of results (list entries) to show per page.
	 */
	private int resultsPerPage;

	/**
	 * Indicates which row the results start from (in the collection).
	 */
	private int startResultsFrom;

	/**
	 * Complementary metadata of the results
	 */
	private Object metaData;

	/**
	 * Constructor for PagedResultSet; as of 2020-11-04 will use defaults if 0 or null provided 
	 * (e.g. resultsPerPage = Constants.RESULTS_PER_PAGE and currentPage = 1) 
	 * This avoids the same boilerplate check in 100s of places
	 */
	public PagedResultSet(Integer resultsPerPage, Integer currentPage) {
		this.resultsPerPage = (resultsPerPage != null && resultsPerPage > 0) ? resultsPerPage : Constants.RESULTS_PER_PAGE;
		this.currentPage = (currentPage != null && currentPage > 0) ? currentPage : 1;
	}

	public int getCurrentPage() {
		return this.currentPage;
	}

	public int getPageCount() {
		return this.pageCount;
	}

	public Collection<T> getResults() {
		return this.results;
	}

	public int getResultsCount() {
		return this.resultsCount;
	}

	public Collection<Integer> getResultsIds() {
		return resultsIds;
	}

	public int getResultsPerPage() {
		return this.resultsPerPage;
	}

	public int getStartResultsFrom() {
		return this.startResultsFrom;
	}

	public boolean isMorePages() {
		return this.currentPage < this.pageCount;
	}

	public boolean isNextPage() {
		return this.currentPage < this.pageCount;
	}

	public boolean isPreviousPage() {
		return this.currentPage > 1;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public void setResults(Collection<T> results) {
		this.results = results;
	}

	/**
	 * Sets the total number of results and from this calculates the number of pages
	 * required to display the results with resultsPerPage factored in.
	 * 
	 * @param resultsCount
	 */
	public void setResultsCount(int resultsCount) {
		this.resultsCount = resultsCount;
		if (this.resultsCount > 0) {
			this.setPageCount((int) Math.ceil(((double) this.resultsCount) / ((double) this.resultsPerPage)));
		} else {
			this.setPageCount(1);
		}

		// set the row the results should start from to show the correct results
		// for this page
		this.setStartResultsFrom((this.getResultsPerPage() * this.getCurrentPage()) - this.getResultsPerPage());
	}

	public void setResultsIds(Collection<Integer> resultsIds) {
		this.resultsIds = resultsIds;
	}

	public void setResultsPerPage(int resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}

	public void setStartResultsFrom(int startResultsFrom) {
		this.startResultsFrom = startResultsFrom;
	}

	public <S> PagedResultSet<S> apply(Function<T, S> func) {
		PagedResultSet<S> result = new PagedResultSet<S>(this.getResultsPerPage(), this.getCurrentPage());
		result.setPageCount(this.getPageCount());
		result.setResultsCount(this.getResultsCount());
		result.setStartResultsFrom(this.getStartResultsFrom());
		result.setResults(this.getResults().stream().map(func).collect(Collectors.toList()));
		return null;
	}

	public Object getMetaData() {
		return metaData;
	}

	public void setMetaData(Object metaData) {
		this.metaData = metaData;
	}
}