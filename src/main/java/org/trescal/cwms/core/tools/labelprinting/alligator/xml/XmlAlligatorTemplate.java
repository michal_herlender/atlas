package org.trescal.cwms.core.tools.labelprinting.alligator.xml;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="template")
public class XmlAlligatorTemplate {
	@XmlElementWrapper(name="parameters")
	@XmlElement(name="parameter")
	private List<XmlAlligatorParameterNameValue> parameters;
	@XmlElement
	private String name;
	@XmlElement
	private Integer height;
	@XmlElement
	private Integer width;
	@XmlElement(name="string")
	private List<XmlAlligatorString> strings;
	@XmlElement(name="image")
	private List<XmlAlligatorImage> images;
	@XmlElement(name="barcode")
	private List<XmlAlligatorBarcode> barcodes;
	@XmlElement(name="rectangle")
	private List<XmlAlligatorRectangle> rectangles;
	
	@XmlElement
	private String defaultPrinter;
	
	public List<XmlAlligatorParameterNameValue> getParameters() {
		return parameters;
	}
	public String getName() {
		return name;
	}
	public Integer getHeight() {
		return height;
	}
	public Integer getWidth() {
		return width;
	}
	public List<XmlAlligatorString> getStrings() {
		return strings;
	}
	public List<XmlAlligatorImage> getImages() {
		return images;
	}
	public String getDefaultPrinter() {
		return defaultPrinter;
	}

	public void setParameters(List<XmlAlligatorParameterNameValue> parameters) {
		this.parameters = parameters;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	public void setStrings(List<XmlAlligatorString> strings) {
		this.strings = strings;
	}
	public void setImages(List<XmlAlligatorImage> images) {
		this.images = images;
	}
	public void setDefaultPrinter(String defaultPrinter) {
		this.defaultPrinter = defaultPrinter;
	}
}
