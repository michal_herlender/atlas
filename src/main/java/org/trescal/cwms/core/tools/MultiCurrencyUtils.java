package org.trescal.cwms.core.tools;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

public class MultiCurrencyUtils
{

	public static BigDecimal convertCurrencyValue(BigDecimal value, SupportedCurrency inputCurrency,
			SupportedCurrency outputCurrency) {
		return value.multiply(
				outputCurrency.getDefaultRate().divide(inputCurrency.getDefaultRate(), 100, RoundingMode.HALF_EVEN))
				.setScale(2, RoundingMode.HALF_EVEN);
	}

}