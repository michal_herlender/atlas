package org.trescal.cwms.core.tools;


import io.vavr.Predicates;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static io.vavr.API.*;

@Component
public class EmailResourceProcessor {

    @Autowired
    private ResourceLoader resourceLoader;

   public Optional<ImageResource> processImage(String path) {
       return Match(path).option(
               Case($("img/logo_blue.gif"),ImageResource.of("logo",resourceLoader.getResource("/img/logo_blue.gif"))),
               Case($(Predicates.anyOf((String s) -> s.startsWith("img"),
                       (String s) -> s.startsWith("/img")
                       )),ImageResource.of(path,resourceLoader.getResource(path)))
       ).toJavaOptional();
   }

   @Value(staticConstructor = "of")
   public static class ImageResource {
      String id;
      @EqualsAndHashCode.Exclude
      Resource resource;
   }
}
