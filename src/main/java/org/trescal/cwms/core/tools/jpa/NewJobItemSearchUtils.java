package org.trescal.cwms.core.tools.jpa;

import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchInstrumentForm;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.stream.Stream;

import static io.vavr.API.*;

public class NewJobItemSearchUtils {

    public static Stream<Predicate> searchByCompanyOrGroupOrEveryWherePredicate(CriteriaBuilder cb, Root<Instrument> instrument, NewJobItemSearchInstrumentForm form) {
        return Match(form.getAction()).option(
                Case($("Search"), cb.equal(instrument.get(Instrument_.comp), form.getCoid())),
                Case($("GroupSearch"), cb.equal(instrument.join(Instrument_.comp).get(Company_.companyGroup), form.getGroupId()))
        ).toJavaStream();
    }
}
