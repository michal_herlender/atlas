package org.trescal.cwms.core.tools;

import lombok.val;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.system.entity.translation.Translation;

import java.util.Collection;
import java.util.Locale;
import java.util.Optional;

public class TranslationUtils {

    public static Optional<String> getBestTranslation(Collection<Translation> translations) {
        val locale = LocaleContextHolder.getLocale();
        val defaultLocale = Locale.getDefault();
        val maybeTransltation =  TranslationUtils.findByLocale(translations,locale);
        return (maybeTransltation.isPresent()?maybeTransltation:TranslationUtils.findByLocale(translations,defaultLocale))
                .map(Translation::getTranslation);
    }

    private static Optional<Translation> findByLocale(Collection<Translation> translations,Locale locale){
        return translations.stream().filter(t -> t.getLocale().equals(locale)).findFirst();
    }
}
