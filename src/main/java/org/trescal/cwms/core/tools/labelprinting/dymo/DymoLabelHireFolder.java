package org.trescal.cwms.core.tools.labelprinting.dymo;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.tools.labelprinting.dymo.dto.DymoHireFolderDto;

@Component
public class DymoLabelHireFolder {
	@Autowired
	private MessageSource messageSource;

	public static final String TEMPLATE_URL = "labels/dymo/hiretagfolder.label";

	public static final String MESSAGE_CODE_TITLE = "dymo.hiretagfolder.title";
	public static final String MESSAGE_CODE_ITEMS = "dymo.hiretagfolder.items";
	public static final String MESSAGE_CODE_COMPANY = "dymo.hiretagfolder.company";
	public static final String MESSAGE_CODE_CONTACT = "dymo.hiretagfolder.contact";
	public static final String MESSAGE_CODE_HIREDATE = "dymo.hiretagfolder.hiredate";
	
	public DymoHireFolderDto getLabelDto(Hire hire, Locale locale) {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(locale);

        DymoHireFolderDto dto = new DymoHireFolderDto();
        dto.setCopies(1);
        dto.setLabelTemplateUrl(TEMPLATE_URL);
        dto.setConame(hire.getContact().getSub().getComp().getConame());
        dto.setDate(hire.getHireDate() == null ? "" : hire.getHireDate().format(formatter));
        dto.setHireno(hire.getHireno());
        dto.setItemno(String.valueOf(hire.getItems().size()));
        dto.setLabel_company(messageSource.getMessage(MESSAGE_CODE_COMPANY, null, locale));
        dto.setLabel_contact(messageSource.getMessage(MESSAGE_CODE_CONTACT, null, locale));
        dto.setLabel_hire_date(messageSource.getMessage(MESSAGE_CODE_HIREDATE, null, locale));
        dto.setLabel_hire_number(messageSource.getMessage(MESSAGE_CODE_TITLE, null, locale));
        dto.setLabel_items(messageSource.getMessage(MESSAGE_CODE_ITEMS, null, locale));
        dto.setLabel_title(messageSource.getMessage(MESSAGE_CODE_TITLE, null, locale));
        dto.setName(hire.getContact().getName());
        dto.setSubdiv(hire.getContact().getSub().getSubname());
		return dto;
	}
}
