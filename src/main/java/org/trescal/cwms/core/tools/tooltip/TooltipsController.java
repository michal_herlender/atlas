package org.trescal.cwms.core.tools.tooltip;

import io.vavr.control.Either;
import lombok.Value;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.instrument.dto.InstrumentTooltipDto;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.certificate.dto.CertificateForInstrumentTooltipDto;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionService;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemForInstrumentTooltipDto;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionResult;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.note.db.NoteService;
import org.trescal.cwms.core.system.entity.note.dto.NoteForInstrumentTooltipDto;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("tooltip")
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class TooltipsController {

    @org.springframework.beans.factory.annotation.Value("#{props['cwms.config.instrument.use_threads']}")
    private boolean useThreads;
    @Autowired
    private InstrumService instrumService;
    @Autowired
    private JobItemService jobItemService;

    @Autowired
    private JobItemProjectionService jobItemProjectionService;

    @Autowired
    private JobProjectionService jobProjectionService;

    @Autowired
    private NoteService noteService;

    @Autowired
    private CertificateService certificateService;

    @GetMapping("instrument.json")
    Either<String, InstrumentTooltipOut> getInstrument(@RequestParam Integer plantId) {
        val instrument = instrumService.loadInstrumentForTooltip(plantId);
        val jobItems = jobItemService.findJobItemsForInstrumentTooltipByPlantId(plantId);
        val certificates = certificateService.findCertificatesForInstrumentTooltipByPlantId(plantId);
        val notes = noteService.findNotesForInstrumentTooltipByPlantId(plantId);
        return instrument.map(i -> InstrumentTooltipOut.of(i, useThreads, jobItems, certificates, notes))
            .<Either<String, InstrumentTooltipOut>>map(Either::right)
            .orElse(Either.left("Unable to find instrument with the id: " + plantId));
    }


    @GetMapping("jobItem.json")
    JobItemProjectionDTO getJobItemTooltip(@RequestParam Integer jobItemId, Locale locale, @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer,String> companyDto) {
        Integer allocatedCompanyId = companyDto.getKey();
        JobItemProjectionCommand command = new JobItemProjectionCommand(locale, allocatedCompanyId,
            JobItemProjectionCommand.JI_LOAD_INSTRUMENTS.class, JobItemProjectionCommand.JI_LOAD_SERVICE_TYPES.class, JobItemProjectionCommand.JI_LOAD_ITEM_STATES.class, JobItemProjectionCommand.JI_LOAD_CONTRACT_REVIEW_PRICE.class);

        JobItemProjectionResult jipResult = this.jobItemProjectionService.getProjectionDTOsForJobItemIds(
            Collections.singleton(jobItemId), command);
        if (jipResult.getJiDtos().isEmpty()) throw new RuntimeException("Job item not found for id " + jobItemId);
        JobItemProjectionDTO jiDto = jipResult.getJiDtos().get(0);
        jobItemProjectionService.loadAdditionalProjections(jiDto, allocatedCompanyId, locale);
        return jiDto;
    }


    @GetMapping("job.json")
    JobProjectionDTO getJob(@RequestParam Integer jobId, Locale locale,
                            @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {

        Integer allocatedCompanyId = companyDto.getKey();

        JobProjectionCommand jobCommand = new JobProjectionCommand(
            locale, allocatedCompanyId, JobProjectionCommand.JOB_LOAD_CONTACT.class, JobProjectionCommand.JOB_LOAD_STATUS.class);
        JobProjectionDTO jobDto = this.jobProjectionService.getDTOByJobId(jobId, jobCommand);

        // Load with short names for service types only
        JobItemProjectionCommand jiCommand = new JobItemProjectionCommand(locale, allocatedCompanyId,
            JobItemProjectionCommand.JI_LOAD_INSTRUMENTS.class, JobItemProjectionCommand.JI_LOAD_SERVICE_TYPES.class, JobItemProjectionCommand.JI_LOAD_ITEM_STATES.class);

        List<JobItemProjectionDTO> jobItems = this.jobItemProjectionService.getProjectionDTOsForJobIds(
            Collections.singleton(jobId), jiCommand);
        jobDto.setJobitems(jobItems);
        return jobDto;
    }


}



@Value(staticConstructor = "of")
class  InstrumentTooltipOut {
    InstrumentTooltipDto summary;
    Boolean useThreads;
    List<JobItemForInstrumentTooltipDto> jobItems;
    List<CertificateForInstrumentTooltipDto> certificates;
    List<NoteForInstrumentTooltipDto> notes;
}