package org.trescal.cwms.core.tools;

import org.trescal.cwms.core.vdi.service.Formatter;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Consumer;

public class GenericTools
{
	/**
	 * Converts an int array to an arraylist of integers
	 * 
	 * @param intArray the {@link Array} to convert to {@link ArrayList}.
	 */
	public static ArrayList<Integer> convertIntArrayToArrayList(int[] intArray)
	{
		ArrayList<Integer> itemNos = new ArrayList<>();
		for (int i : intArray) {
			itemNos.add(i);
		}
		return itemNos;
	}

	/**
	 * Returns the name of the method running when the given
	 * {@link StackTraceElement} array was taken
	 *
	 * @param ste the {@link StackTraceElement} array
	 * @return the method name
	 */
	public static String getMethodNameFromStackTrace(StackTraceElement[] ste) {
		String methodName = "";
		boolean flag = false;

		for (StackTraceElement s : ste) {
			if (flag) {
				methodName = s.getMethodName();
				break;
			}
			flag = s.getMethodName().equals("getStackTrace");
		}
		return methodName;
	}

	/**
	 * Converts an Integer List into a comma separated String
	 * 
	 * @param list Integer List to convert
	 * @return String
	 */
	public static String intListToCSV(List<Integer> list) {
		StringBuilder csv = new StringBuilder();
		for (Integer integer : list) {
			csv.append(integer);
			csv.append(", ");
		}
		return csv.toString();
	}

	/**
	 * Method takes a set of objects and returns a HashMap of the objects with
	 * the value of the given field name as the key.
	 * 
	 * @param set set of Objects to return.
	 * @param keyFieldName The name of the field to use as the key.
	 * @return Hashmap of objects
	 */
	public static HashMap<Object, Object> setToHashMap(Set<Object> set, String keyFieldName) {
		HashMap<Object, Object> mappedList = new HashMap<>(0);
		if (set != null) {
			for (Object obj : set) {
				// get a list of all fields that belong to the object
				Field[] fields = obj.getClass().getDeclaredFields();
				for (Field field : fields) {
					if (field.getName().equalsIgnoreCase(keyFieldName))
					{
						try
						{
							// set field to accessible (make sure that we can
							// access private methods)
							field.setAccessible(true);

							// add the object to the HashMap using the value of
							// the field as the key
							mappedList.put(field.get(obj), obj);
						}
						catch (IllegalAccessException iae)
						{
							iae.printStackTrace();
						}
					}
				}
			}
		}
		return mappedList;
	}

	public static List<Integer> stringToIntegerList(String csv)
	{
		List<Integer> result = new ArrayList<>();
		if ((csv != null) && !csv.equals("")) {
			String[] stringArray = csv.split(", ");
			for (String s : stringArray) {
				result.add(Integer.parseInt(s));
			}
		}
		return result;
	}
	public static  <A> void applyIfNonEmpty(A value, Consumer<A> f) {
		if (value != null) {
			if (value instanceof String && !((String) value).isEmpty())
				f.accept(value);
			if (value instanceof Collection<?> && !((Collection<?>) value).isEmpty())
				f.accept(value);
			if (!(value instanceof String) && !(value instanceof Collection<?>))
				f.accept(value);
		}
	}

	public static  <A> void applyWithFormaterIfNonEmpty(A value, Consumer<String> f, Formatter<A> formatter) {
		if (value != null)
			applyIfNonEmpty(formatter.apply(value), f);
	}
}
