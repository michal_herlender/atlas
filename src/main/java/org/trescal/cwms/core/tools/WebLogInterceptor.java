package org.trescal.cwms.core.tools;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.web.utilities.WebLogWriter;

public class WebLogInterceptor implements HandlerInterceptor
{
	private WebLogWriter logWriter;
	private boolean webLogEnabled;

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception
	{
		if (this.webLogEnabled)
		{
			String logFilePath = (String) request.getSession().getAttribute(Constants.WEB_LOG_FILEPATH);
			if (logFilePath != null)
			{
				String url = request.getRequestURI();

				if (url.contains("/dwr/") || url.contains("\\dwr\\"))
				{
					if (url.contains("/dwr/call")
							|| url.contains("\\dwr\\call"))
					{
						String ajaxCall = url.substring(url.lastIndexOf("/"));
						this.logWriter.addAjaxCallToLog(logFilePath, ajaxCall);
					}
				}
				else
				{
					String queryStr = request.getQueryString();
					this.logWriter.addURLToLog(logFilePath, url, queryStr);
				}
			}
		}
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception
	{
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
	{
		return true;
	}
	
	public void setLogWriter(WebLogWriter logWriter)
	{
		this.logWriter = logWriter;
	}

	public void setWebLogEnabled(boolean webLogEnabled)
	{
		this.webLogEnabled = webLogEnabled;
	}
}