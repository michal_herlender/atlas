package org.trescal.cwms.core.tools.transitcalendar;

import java.util.Comparator;

public class TransitDateComparator implements Comparator<Transit>
{
	public int compare(Transit t1, Transit t2)
	{
		if (!t1.getTransitDate().equals(t2.getTransitDate()))
		{
			return t1.getTransitDate().compareTo(t2.getTransitDate());
		}
		else
		{
			return 1;
		}
	}
}