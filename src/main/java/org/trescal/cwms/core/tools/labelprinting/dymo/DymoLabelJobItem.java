package org.trescal.cwms.core.tools.labelprinting.dymo;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.labelprinting.dymo.dto.DymoAccessoryDto;
import org.trescal.cwms.core.tools.labelprinting.dymo.dto.DymoJobItemDto;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@Component
public class DymoLabelJobItem {
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private SupportedLocaleService supportedLocaleService;

	public static final String TEMPLATE_JOBITEM_URL = "labels/dymo/jobtag-2.label";
	public static final String TEMPLATE_ACCESSORY_URL = "labels/dymo/itemaccessories.label";

	public static final String MESSAGE_CODE_JOB_ITEM = "dymo.certenvelope.item";
	public static final String MESSAGE_CODE_ITEM_NUMBER_OF_TOTAL = "dymo.jobitem.itemnumberoftotal";
	public static final String MESSAGE_CODE_SERIAL_NUMBER = "dymo.jobitem.serialnumber";
	public static final String MESSAGE_CODE_PLANT_NUMBER = "dymo.jobitem.plantnumber";
	public static final String MESSAGE_CODE_DATE_IN = "dymo.jobitem.datein";
	public static final String MESSAGE_CODE_DATE_DUE = "dymo.jobitem.datedue";
	public static final String MESSAGE_CODE_ACCESSORIES = "dymo.jobitem.accessories";
	public static final String MESSAGE_CODE_NO_ACCESSORIES = "dymo.jobitem.noaccessories";

	public DymoJobItemDto getLabelDto(JobItem jobItem, int copies, Locale locale, Boolean printJobItemCount) {
		DymoJobItemDto dto = new DymoJobItemDto();
		dto.setCopies(copies);
		dto.setLabelTemplateUrl(TEMPLATE_JOBITEM_URL);
		dto.setCompany(jobItem.getJob().getCon().getSub().getComp().getConame());
		dto.setSubdiv(jobItem.getJob().getCon().getSub().getSubname());
		dto.setDate(formatDate(jobItem, locale));
		dto.setEquip(formatEquip(jobItem, locale));
		dto.setJobno(formatJobNoPrefix(jobItem));
		dto.setJobno_1(formatJobNoSuffix(jobItem));
		if (printJobItemCount)
			dto.setJobno_2(formatJobItemCount(jobItem, locale));
		else
			dto.setJobno_2(formatJobItem(jobItem, locale));
		dto.setPlantid(String.valueOf(jobItem.getInst().getPlantid()));
		dto.setType(formatType(jobItem));
		dto.setNoAccessories(formatNoAccessories(jobItem, locale));

		return dto;
	}

	public DymoAccessoryDto getLabelDtoAccessories(JobItem jobItem, int copies, Locale locale,
			Boolean printJobItemCount) {
		DymoAccessoryDto dto = new DymoAccessoryDto();
		dto.setCopies(copies);
		dto.setLabelTemplateUrl(TEMPLATE_ACCESSORY_URL);
		dto.setAccessories(formatAccessories(jobItem, locale));
		dto.setAccessoriesTitle(messageSource.getMessage(MESSAGE_CODE_ACCESSORIES, null, locale));
		dto.setJobno(formatJobNoPrefix(jobItem));
		dto.setJobno_1(formatJobNoSuffix(jobItem));
		if (printJobItemCount)
			dto.setJobno_2(formatJobItemCount(jobItem, locale));
		else
			dto.setJobno_2(formatJobItem(jobItem, locale));

		return dto;
	}

	/*
	 * Returns true if the job item has accessories (at present time, no group
	 * accessory tag)
	 */
	public boolean hasAccessories(JobItem jobItem) {
        if (!jobItem.getAccessories().isEmpty())
            return true;
        return (jobItem.getAccessoryFreeText() != null) && !jobItem.getAccessoryFreeText().isEmpty();
    }

	/*
	 * Include everything before the last dash in the Job Number If there is no
	 * dash in the job number (or it starts with the only dash), return nothing
	 */
	protected String formatJobNoPrefix(JobItem jobItem) {
		String jobNo = jobItem.getJob().getJobno();

		int indexLastDash = jobNo.lastIndexOf('-');
		if (indexLastDash <= 0)
			return "";

		return jobNo.substring(0, indexLastDash);
	}

	/*
	 * Include the last numerical sequence in the Job Number If there are no
	 * numerical sequences in the job number, this will return everything
	 */
	protected String formatJobNoSuffix(JobItem jobItem) {
		String formattedJobNo = jobItem.getJob().getJobno();
		String[] numberSequenceArray = jobItem.getJob().getJobno().split("[^0-9]+");
		if (numberSequenceArray.length > 0) {
			formattedJobNo = numberSequenceArray[numberSequenceArray.length - 1];
		}
		return formattedJobNo;
	}

	/*
	 * Returns job item count in the format "Item 1 of 5" or similar translation
	 */
	protected String formatJobItemCount(JobItem jobItem, Locale locale) {
		Object[] args = new Object[] { jobItem.getItemNo(), jobItem.getJob().getItems().size() };
		return messageSource.getMessage(MESSAGE_CODE_ITEM_NUMBER_OF_TOTAL, args, locale);
	}

	protected String formatJobItem(JobItem jobItem, Locale locale) {
		return messageSource.getMessage(MESSAGE_CODE_JOB_ITEM, null, locale) + " " + jobItem.getItemNo();
	}

	protected String formatEquip(JobItem jobItem, Locale locale) {
        StringBuilder result = new StringBuilder();
        Locale fallbackLocale = this.supportedLocaleService.getPrimaryLocale();

        String instrumentName = InstModelTools.instrumentModelNameViaTranslations(jobItem.getInst(), false, locale, fallbackLocale);
        result.append(insertLineBreaks(instrumentName));
        result.append("\r\n");
        result.append(messageSource.getMessage(MESSAGE_CODE_SERIAL_NUMBER, null, locale));
        result.append(" ");
        result.append(jobItem.getInst().getSerialno());
        if (jobItem.getInst().getPlantno() != null) {
            result.append("\r\n");
			result.append(messageSource.getMessage(MESSAGE_CODE_PLANT_NUMBER, null, locale));
			result.append(" ");
			result.append(jobItem.getInst().getPlantno());
		}
		// Transport Option Out
		if (jobItem.getReturnOption() != null) {
			result.append("\r\n");
			result.append(jobItem.getReturnOption().toString());
		}

		return result.toString();
	}

	protected String formatDate(JobItem jobItem, Locale locale) {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(locale);

        StringBuffer result = new StringBuffer();
        result.append(this.messageSource.getMessage(MESSAGE_CODE_DATE_IN, null, locale));
        result.append(" ");
        result.append(jobItem.getDateIn().format(formatter));
        result.append("\r\n");
        result.append(this.messageSource.getMessage(MESSAGE_CODE_DATE_DUE, null, locale));
        result.append(" ");
        result.append(jobItem.getDueDate().format(formatter));
        return result.toString();
    }

	protected String formatType(JobItem jobItem) {
		// Work Requirement Department (if next work requirement is defined)
		if ((jobItem.getNextWorkReq() != null)
				&& (jobItem.getNextWorkReq().getWorkRequirement().getDepartment() != null)) {
			return jobItem.getNextWorkReq().getWorkRequirement().getDepartment().getShortName();
		} else {
			return "";
		}
	}

	public String formatNoAccessories(JobItem jobItem, Locale locale) {
		if (hasAccessories(jobItem)) {
			return "";
		} else {
			return this.messageSource.getMessage(MESSAGE_CODE_NO_ACCESSORIES, null, locale);
		}
	}

	public String formatAccessories(JobItem jobItem, Locale locale) {
        StringBuilder result = new StringBuilder();
        for (ItemAccessory accessory : jobItem.getAccessories()) {
            result.append(accessory.getQuantity());
            result.append(" x ");
            // Insert a line break at every X characters to prevent text size reduction
            result.append(insertLineBreaks(
                this.translationService.getCorrectTranslation(accessory.getAccessory().getTranslations(), locale)));
            result.append("\r\n");
        }
        if ((jobItem.getAccessoryFreeText() != null) && (!jobItem.getAccessoryFreeText().isEmpty())) {
            result.append(insertLineBreaks(jobItem.getAccessoryFreeText()));
		}
		return result.toString();
	}

	/*
	 * Inserts line breaks every 25 characters to prevent text size reduction
	 */
	protected String insertLineBreaks(String text) {
		return text.replaceAll("(.{25})", "$1\r\n");
	}
}