package org.trescal.cwms.core.tools.componentdirectory;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.system.entity.componentsubdirectory.ComponentSubDirectory;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.tools.pathservice.PathService;

/**
 * Implementation of {@link ComponentDirectoryService} which handles
 * {@link SystemComponent} directory creation and retreival.
 * 
 * @author richard
 */
@Service("ComponentDirectoryService")
public class ComponentDirectoryServiceImpl implements ComponentDirectoryService
{
	@Autowired
	private PathService pathService;
	protected final Log logger = LogFactory.getLog(this.getClass());

	private void createSubDirs(SystemComponent sc, String componentFilePath)
	{
		for (ComponentSubDirectory sd : sc.getSubDirs())
		{
			File f = new File(componentFilePath.concat(File.separator).concat(sd.getDirName()));
			//System.out.println("Mkdirs: " + f.getAbsolutePath());
			f.mkdirs();
		}
	}
	
	@Override
	public File getDirectory(SystemComponent sc, String id)
	{
		return this.getDirectory(sc, id, null);
	}

	@Override
	public File getDirectory(SystemComponent sc, String id, Integer version)
	{
		File f = null;
		try
		{
			SystemComponent scParent = sc.getParentComponent();
			String componentRoot = "";
			if ((scParent != null) && sc.isUsingParentSettings())
			{
				componentRoot = this.pathService.getFileServerRootPath().concat(File.separator).concat(scParent.getRootDir());
			}
			else
			{
				componentRoot = this.pathService.getFileServerRootPath().concat(File.separator).concat(sc.getRootDir());
			}
			// build the basic path
			String path = this.getFilePath(id, version, sc, componentRoot);
			// add any additional path required
			if (sc.isUsingParentSettings()
					&& (sc.getAppendToParentPath() != null))
			{
				path = this.getFilePath(id, version, sc, componentRoot);
				path = path.concat(File.separator).concat(sc.getAppendToParentPath());
			}
			f = new File(path);
			//System.out.println("Mkdirs: " + f.getAbsolutePath());
			f.mkdirs();
			if (sc.getSubDirs() != null)
			{
				this.createSubDirs(sc, f.toString());
			}
		}
		catch (Exception e)
		{
			String name = " unnamed ";
			if (sc != null)
			{
				name = sc.getComponentName();
			}
			this.logger.error("Error occurred getting filepath for component "
					+ name + " (id:" + id + ", version:" + version);
			e.printStackTrace();
		}
		return f;
	}
	
	/**
	 * convert the number to a file path by replacing several separators by the file separator of the system,
	 * prepend the root directory
	 * @param number component number
	 * @param componentRoot root directory
	 * @return file path
	 */
	public String getFilePath(String number, Integer version, SystemComponent sysComp, String componentRoot)
	{
		String componentFilePath = componentRoot == null ? "" :
			componentRoot.endsWith(File.separator) ? componentRoot : componentRoot + File.separator;
		componentFilePath = componentFilePath.concat(
				number.replace('/', File.separatorChar)
				.replace('-', File.separatorChar)
				.replace('.', File.separatorChar)
				.replace(',', File.separatorChar)
				.replace(';', File.separatorChar)
				.replace(';', File.separatorChar));
		if (sysComp.isNumberedSubFolders())
			componentFilePath = componentFilePath.concat(File.separator).concat(this.getMainSubFolder(version, sysComp));
		return componentFilePath;
	}
	
	private String getMainSubFolder(Integer ver, SystemComponent sysComp)
	{
		return (sysComp.getNumberedSubFolderDivider() == null ? "" : sysComp.getNumberedSubFolderDivider()).concat((ver == null ? "" : ver).toString());
	}
}
