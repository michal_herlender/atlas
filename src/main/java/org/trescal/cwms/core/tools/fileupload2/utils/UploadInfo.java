/* Licence:
 *   Use this however/wherever you like, just don't blame me if it breaks anything.
 *
 * Credit:
 *   If you're nice, you'll leave this bit:
 *
 *   Class by Pierre-Alexandre Losson -- http://www.telio.be/blog
 *   email : plosson@users.sourceforge.net
 */
package org.trescal.cwms.core.tools.fileupload2.utils;

import org.trescal.cwms.core.tools.fileupload2.web.formobjects.AbstractFileUploadForm;

/**
 * @author Original : plosson on 06-janv.-2006 12:19:14
 */
public class UploadInfo
{
	private long totalSize = 0;
	private long bytesRead = 0;
	private long elapsedTime = 0;
	private String status = "done";
	private int fileIndex = 0;
	private String fileName = "";
	private AbstractFileUploadForm form;
	private int fileId; // image db id.

	/**
	 * Maximum file size of file uploads - only set if a file exceeds the max
	 * size. Configured in Spring AjaxFileUploadMultipartResolver.
	 */
	private long maxFileSize = 0;

	public UploadInfo()
	{
	}

	public UploadInfo(int fileIndex, long totalSize, long bytesRead, long elapsedTime, String status)
	{
		this.fileIndex = fileIndex;
		this.totalSize = totalSize;
		this.bytesRead = bytesRead;
		this.elapsedTime = elapsedTime;
		this.status = status;
	}

	public long getBytesRead()
	{
		return this.bytesRead;
	}

	public long getElapsedTime()
	{
		return this.elapsedTime;
	}

	public int getFileIndex()
	{
		return this.fileIndex;
	}

	public String getFileName()
	{
		return this.fileName;
	}

	public AbstractFileUploadForm getForm()
	{
		return this.form;
	}

	public long getMaxFileSize()
	{
		return this.maxFileSize;
	}

	public String getStatus()
	{
		return this.status;
	}

	public long getTotalSize()
	{
		return this.totalSize;
	}

	public int getFileId() {
		return fileId;
	}

	/**
	 * Convenience getter for testing if the file has been uploaded and is now
	 * undergoing file validation before the upload completion.
	 * 
	 * @return true if the file is being verified.
	 */
	public boolean isBeingVerified()
	{
		return "checking".equals(this.status);
	}

	/**
	 * Convenience getter to test if a file has been rejected for exceding the
	 * file size limit set in the AjaxFileUploadMultipartReolver bean
	 * definition.
	 * 
	 * @return true if the file has exceded the file size.
	 */
	public boolean isFailedFileSize()
	{
		return this.status.equals("filesizeerror");
	}

	/**
	 * Conveniece getter to test if a file has been rejected for being a
	 * non-approved file type.
	 * 
	 * @return true if the upload was stopped because of an invalid file type.
	 */
	public boolean isFailedFileType()
	{
		return this.status.equals("filetypeerror");
	}

	public boolean isInProgress()
	{
		return "progress".equals(this.status) || "start".equals(this.status);
	}

	public void setBytesRead(long bytesRead)
	{
		this.bytesRead = bytesRead;
	}

	public void setElapsedTime(long elapsedTime)
	{
		this.elapsedTime = elapsedTime;
	}

	public void setFileIndex(int fileIndex)
	{
		this.fileIndex = fileIndex;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	public void setForm(AbstractFileUploadForm form)
	{
		this.form = form;
	}

	public void setMaxFileSize(long maxFileSize)
	{
		this.maxFileSize = maxFileSize;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public void setTotalSize(long totalSize)
	{
		this.totalSize = totalSize;
	}

	public void setFileId(int fileId) {
		this.fileId = fileId;
	}

}
