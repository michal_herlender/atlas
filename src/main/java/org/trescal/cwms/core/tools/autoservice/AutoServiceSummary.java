package org.trescal.cwms.core.tools.autoservice;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

/**
 * DTO object, extracted out of AutoService
 * @author galen
 *
 */
public class AutoServiceSummary {
	private int totalChases;
	private int emailsSent;
	private Map<Contact, Map<Integer, List<JobItem>>> chased;
	private Map<Contact, List<JobItem>> failChaseLimit;
	private Map<Contact, List<JobItem>> failDisabledChase;
	private Map<Contact, List<JobItem>> failNoChasing;
	private Map<Contact, List<JobItem>> failNoEmail;
		
	public AutoServiceSummary(int totalChases) {
		this.totalChases = totalChases;
		this.emailsSent = 0;
		this.chased = new HashMap<Contact, Map<Integer, List<JobItem>>>();
		this.failChaseLimit = new HashMap<Contact, List<JobItem>>();
		this.failDisabledChase = new HashMap<Contact, List<JobItem>>();
		this.failNoChasing = new HashMap<Contact, List<JobItem>>();
		this.failNoEmail = new HashMap<Contact, List<JobItem>>();
	}

	public Map<Contact, Map<Integer, List<JobItem>>> getChased() {
		return chased;
	}

	public Map<Contact, List<JobItem>> getFailChaseLimit() {
		return failChaseLimit;
	}

	public Map<Contact, List<JobItem>> getFailDisabledChase() {
		return failDisabledChase;
	}

	public Map<Contact, List<JobItem>> getFailNoChasing() {
		return failNoChasing;
	}

	public Map<Contact, List<JobItem>> getFailNoEmail() {
		return failNoEmail;
	}

	public void addChased(Contact contact, int chaseToSend, JobItem jobItem) {
		Map<Integer, List<JobItem>> map = this.chased.get(contact); 
		if (map == null) {
			map = new HashMap<>();
			this.chased.put(contact, map);
		}
		List<JobItem> list = map.get(chaseToSend);
		if (list == null) {
			list = new ArrayList<>();
			map.put(chaseToSend, list);
		}
		list.add(jobItem);
	}

	public void addFailChaseLimit(Contact contact, JobItem jobItem) {
		List<JobItem> list = this.failChaseLimit.get(contact);
		if (list == null) {
			list = new ArrayList<>();
			this.failChaseLimit.put(contact, list);
		}
		list.add(jobItem);
	}

	public void addFailDisabledChase(Contact contact, JobItem jobItem) {
		List<JobItem> list = this.failDisabledChase.get(contact);
		if (list == null) {
			list = new ArrayList<>();
			this.failDisabledChase.put(contact, list);
		}
		list.add(jobItem);
	}

	public void addFailNoChasing(Contact contact, Collection<JobItem> jobItems) {
		List<JobItem> list = this.failNoChasing.get(contact);
		if (list == null) {
			list = new ArrayList<>();
			this.failNoChasing.put(contact, list);
		}
		list.addAll(jobItems);
	}

	public void addFailNoEmail(Contact contact, Collection<JobItem> jobItems) {
		List<JobItem> list = this.failNoEmail.get(contact);
		if (list == null) {
			list = new ArrayList<>();
			this.failNoEmail.put(contact, list);
		}
		list.addAll(jobItems);
	}

	public int getEmailsSent() {
		return emailsSent;
	}

	public void incrementEmailsSent() {
		this.emailsSent++;
	}

	public int getTotalChases() {
		return totalChases;
	}
}
