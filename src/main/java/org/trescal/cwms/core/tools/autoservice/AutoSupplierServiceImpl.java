package org.trescal.cwms.core.tools.autoservice;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.ChaseThirdParty;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultBooleanType;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_AutoServiceSupplier;

@Service("AutoSupplierService")
public class AutoSupplierServiceImpl extends AutoService implements AutoSupplierService
{
	@Autowired
	private ChaseThirdParty chaseThirdParty;
	@Autowired
	private HookInterceptor_AutoServiceSupplier interceptorSupplier;
	
	public void chaseSuppliers()
	{
		logger.info("run job");
		if (this.schTaskServ.scheduledTaskIsActive(this.getClass().getName(), Thread.currentThread().getStackTrace()))
		{
			// find items requiring chase grouped by contact
			Map<Contact, List<JobItem>> toChase = this.jiServ.getItemsRequiringSupplierChase();

			// send chase e-mails to suppliers
			AutoServiceSummary summary = this.chase(toChase);
			
			// send summary e-mail to us
			boolean summarySent = this.sendSummary(summary, AutoServiceType.SUPPLIER);

			// report results of task run
			String message = summary.getEmailsSent() + " emails sent to suppliers, ";
			message = message
					+ ((summarySent) ? "summary successfully sent" : "summary failed to send");
			this.schTaskServ.reportOnScheduledTaskRun(this.getClass().getName(), Thread.currentThread().getStackTrace(), true, message);
		}
		else
		{
			logger.info("Auto-Service Suppliers not running: scheduled task cannot be found or is turned off");
		}
	}

	@Override
	protected SystemDefaultBooleanType getChaseDefault() {
		return chaseThirdParty;
	}
	
	@Override
	protected void sendEmail(Contact con, Map<Integer, List<JobItem>> itemsToChaseNow, String fromAddress,
			String replyToAddress, int totalChases) {
		super.sendChaseEmail(con, itemsToChaseNow, fromAddress, replyToAddress, totalChases, AutoServiceType.SUPPLIER);
		interceptorSupplier.recordAction(itemsToChaseNow);
	}
}