package org.trescal.cwms.core.tools.secretkey;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Setter;

@Entity
@Table(name = "secretkey")
@Setter
public class SecretKey {
	
	private byte[] keyValue;

	@Id
	@Column(name = "keyvalue", nullable = false, unique = true)
	public byte[] getKeyValue()
	{
		return this.keyValue;
	}
}
