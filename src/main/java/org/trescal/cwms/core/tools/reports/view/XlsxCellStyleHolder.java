package org.trescal.cwms.core.tools.reports.view;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;

import lombok.Getter;

/**
 * Allows reuse of custom styles between worksheets in a workbook
 */
@Getter
public class XlsxCellStyleHolder {
	private Workbook wb;
	private CellStyle headerStyle;
	private CellStyle dateStyle;
	private CellStyle dateTimeStyle;
	private CellStyle integerStyle;
	private CellStyle monetaryStyle;
	private CellStyle textStyle;

	public XlsxCellStyleHolder(Workbook wb) {
		this.wb = wb;
	}
	
	public CellStyle getHeaderStyle() {
		if (this.headerStyle == null) {
			// Font name needed for autosize support
			Font boldFont = wb.createFont();
			boldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			boldFont.setFontName("Calibri");
			boldFont.setFontHeightInPoints((short) 11);
			boldFont.setColor(IndexedColors.WHITE.index);
			this.headerStyle = wb.createCellStyle();
			this.headerStyle.setFillForegroundColor(IndexedColors.BLUE_GREY.index);
			this.headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
			this.headerStyle.setFont(boldFont);
			this.headerStyle.setDataFormat((short) BuiltinFormats.getBuiltinFormat("text"));
		}
		return this.headerStyle;
	}
	
	public CellStyle getDateStyle() {
		if (this.dateStyle == null) {
			dateStyle = wb.createCellStyle();
			this.dateStyle.setDataFormat((short) BuiltinFormats.getBuiltinFormat("m/d/yy"));
		}
		return this.dateStyle;
	}
	
	public CellStyle getDateTimeStyle() {
		if (this.dateTimeStyle == null) {
			dateTimeStyle = wb.createCellStyle();
			this.dateTimeStyle.setDataFormat((short) BuiltinFormats.getBuiltinFormat("m/d/yy h:mm"));
		}
		return this.dateTimeStyle;
	}
	
	public CellStyle getIntegerStyle() {
		if (this.integerStyle == null) {
			integerStyle = wb.createCellStyle();
			this.integerStyle.setAlignment(CellStyle.ALIGN_RIGHT);
			this.integerStyle.setDataFormat((short) BuiltinFormats.getBuiltinFormat("0"));
		}
		return this.integerStyle;
	}
	
	public CellStyle getMonetaryStyle() {
		if (this.monetaryStyle == null) {
			monetaryStyle = wb.createCellStyle();
			this.monetaryStyle.setAlignment(CellStyle.ALIGN_RIGHT);
			this.monetaryStyle.setDataFormat((short) BuiltinFormats.getBuiltinFormat("#,##0.00"));
		}
		return this.monetaryStyle;
	}
	
	public CellStyle getTextStyle() {
		if (this.textStyle == null) {
			textStyle = wb.createCellStyle();
			this.textStyle.setDataFormat((short) BuiltinFormats.getBuiltinFormat("text"));
		}
		return this.textStyle;
	}
	
}
