package org.trescal.cwms.core.tools.applets;

import java.applet.Applet;
import java.awt.Button;
import java.awt.Color;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.AccessController;
import java.security.PrivilegedAction;

import org.trescal.cwms.core.tools.SignatureCapture;

/**
 * Applet wrapper for the SignatureCapture application. This allows the
 * stand-alone application to actually run client side via a web browser so that
 * the device used to capture the signature can be detected on a local USB port.
 * 
 * @author jamiev
 */
@SuppressWarnings("serial")
public class SignatureCaptureApplet extends Applet
{
	// applet parameters to be passed in html declaration
	private String deliveryNo;
	private String deliveryPath;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void init()
	{
		// get the parameters passed from the applet call
		this.deliveryNo = this.getParameter("deliveryno");
		this.deliveryPath = this.getParameter("deliverypath");

		// initialise potential button and label
		final Button signBtn = new Button(" Capture Signature ");
		final Label errorLbl = new Label(" Signature pad not connected! ");

		// set background colour to be that of the thickbox the applet is
		// launched in so that it appears transparent
		this.setBackground(new Color(223, 234, 244));

		// use access controller to perform all signature capture / driver
		// loading with sufficient privileges
		AccessController.doPrivileged(new PrivilegedAction()
		{
			public Object run()
			{
				// create the signature capture object
				final SignatureCapture demo = new SignatureCapture(SignatureCaptureApplet.this.deliveryPath, SignatureCaptureApplet.this.deliveryNo);

				// if the device is connected and drivers are installed on the
				// client machine
				if (demo.tabletConnectionIsAvailable())
				{
					// add the signing button to the applet display
					SignatureCaptureApplet.this.add(signBtn);
					// add listener for when the button is clicked
					signBtn.addActionListener(new ActionListener()
					{
						@Override
						public void actionPerformed(ActionEvent arg0)
						{
							// open the capture window
							demo.openCaptureWindow();
						}
					});
				}
				// if the device is not connected or drivers are missing from
				// the client machine
				else
				{
					// add the error message to alert the user
					SignatureCaptureApplet.this.add(errorLbl);
				}

				return null;
			}
		});
	}
}