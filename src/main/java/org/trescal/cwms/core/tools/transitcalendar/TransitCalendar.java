package org.trescal.cwms.core.tools.transitcalendar;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;

import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

public class TransitCalendar
{
	private HashMap<Integer, ArrayList<ClientTransit>> clientTransits;
	private Subdiv sub;
	private TreeMap<Date, ArrayList<Transit>> transitsByDate;
	private int weeks;

	public TransitCalendar(Subdiv sub, int weeks,  HashMap<Integer, ArrayList<ClientTransit>> clientTransits, TreeMap<Date, ArrayList<Transit>> transitsByDate)
	{
		this.sub = sub;
		this.weeks = weeks;
		this.clientTransits = clientTransits;
		this.transitsByDate = transitsByDate;
	}

	public HashMap<Integer, ArrayList<ClientTransit>> getClientTransits()
	{
		return this.clientTransits;
	}

	public Subdiv getSub()
	{
		return this.sub;
	}

	public TreeMap<Date, ArrayList<Transit>> getTransitsByDate()
	{
		return this.transitsByDate;
	}

	public int getWeeks()
	{
		return this.weeks;
	}

	public void setClientTransits(HashMap<Integer, ArrayList<ClientTransit>> clientTransits)
	{
		this.clientTransits = clientTransits;
	}

	public void setSub(Subdiv sub)
	{
		this.sub = sub;
	}

	public void setTransitsByDate(TreeMap<Date, ArrayList<Transit>> transitsByDate)
	{
		this.transitsByDate = transitsByDate;
	}

	public void setWeeks(int weeks)
	{
		this.weeks = weeks;
	}
}