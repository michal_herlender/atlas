package org.trescal.cwms.core.tools.reports.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

public class CustomerSalesForPreviousMonthDTO
{
	private String company;
	private String description;
	private String mfr;
	private Date minDateIn;
	private String model;
	private BigDecimal sales;

	// this constructor is used by aliasToBean transformers in ReportsDaoImpl
	public CustomerSalesForPreviousMonthDTO() {}
	
	public String getCompany()
	{
		return this.company;
	}

	public String getDescription()
	{
		return this.description;
	}

	public String getMfr()
	{
		return this.mfr;
	}

	public Date getMinDateIn()
	{
		return this.minDateIn;
	}

	public String getModel()
	{
		return this.model;
	}

	public BigDecimal getSales()
	{
		return this.sales;
	}

	public void setCompany(String company)
	{
		this.company = company;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setMfr(String mfr)
	{
		this.mfr = mfr;
	}

	public void setMinDateIn(Date minDateIn)
	{
		this.minDateIn = minDateIn;
	}

	public void setModel(String model)
	{
		this.model = model;
	}

	public void setSales(BigDecimal sales)
	{
		this.sales = sales.setScale(2, RoundingMode.HALF_UP);
	}
}