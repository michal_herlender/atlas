package org.trescal.cwms.core.tools.fileupload2.web.formobjects;

import org.trescal.cwms.core.documents.images.image.ImageType;

public class ImageUploadForm extends AbstractFileUploadForm {
	private ImageType entityType;
	private Integer entityId;
	private String relativeImagePath;

	public Integer getEntityId() {
		return this.entityId;
	}

	public ImageType getEntityType() {
		return this.entityType;
	}

	public String getRelativeImagePath() {
		return this.relativeImagePath;
	}

	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}

	public void setEntityType(ImageType entityType) {
		this.entityType = entityType;
	}

	public void setRelativeImagePath(String relativeImagePath) {
		this.relativeImagePath = relativeImagePath;
	}

}
