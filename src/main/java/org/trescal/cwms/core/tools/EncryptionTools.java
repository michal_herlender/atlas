package org.trescal.cwms.core.tools;

import java.security.Key;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.tools.secretkey.SecretKey;
import org.trescal.cwms.core.tools.secretkey.db.SecretKeyService;

@Component
public class EncryptionTools implements ApplicationContextAware {
	private static final String SECKEY_ALGO = "AES";
	private static final String CD_ALGO = "AES/ECB/PKCS5Padding";
	private static final byte[] keyValue = new byte[] { 'C','r','o','c','o','d','i','l','e'};
    public static final String PASSWORD_HASH_ALGORITHM = "MD5";
    private static ApplicationContext appContext;

	public static String encrypt(String Data) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(CD_ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(Data.getBytes());
        String encryptedValue = new Base64().encodeToString(encVal);
        return encryptedValue;
    }

    public static String decrypt(String encryptedData) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(CD_ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decodedValue = new Base64().decode(encryptedData);
        byte[] decValue = c.doFinal(decodedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }
    
    private static Key generateKey() throws Exception {
    	SecretKeyService secretKeyService = appContext.getBean(SecretKeyService.class);
    	SecretKey storedKey = secretKeyService.getKey();
    	if(storedKey != null)
    		return new SecretKeySpec(storedKey.getKeyValue(), SECKEY_ALGO);
    	else {
	    	MessageDigest digester = MessageDigest.getInstance(PASSWORD_HASH_ALGORITHM);
	        digester.update(String.valueOf(keyValue).getBytes("UTF-8"));
	        Key key = new SecretKeySpec(digester.digest(), SECKEY_ALGO);
	        return key;
    	}
    }

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		EncryptionTools.appContext = applicationContext;
	}
}