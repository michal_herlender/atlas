package org.trescal.cwms.core.tools.labelprinting.birt;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.eclipse.birt.report.engine.api.EngineException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
//import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.FileController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.labelprinting.LabelService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @FileController
public class BirtLabelController {
	@Resource(name = "BirtLabelService")
	LabelService labelService;
	@Autowired
	CompanyService compServ;

	/*
	 * Small Cal Label
	 */
	@RequestMapping(value = "/birtcallabel", method = RequestMethod.GET)
	public void downloadSmallCallLabel(@RequestParam("certLinkId") int certLinkId,
			@RequestParam("templateKey") String templateKey,
			@RequestParam("includeRecallDate") boolean includeRecallDate, HttpServletResponse response, HttpSession session, Locale locale)
			throws IOException, EngineException {

		ByteArrayOutputStream baos = new ByteArrayOutputStream(10 * 1024);

		if (labelService.printCertificateLabel(certLinkId, templateKey, includeRecallDate, locale, baos, currentCompany(session))) {
			String filename = "callabel_" + templateKey + "_" + certLinkId + ".pdf";
			response.setContentType(URLConnection.guessContentTypeFromName(filename));
			response.setContentLength(baos.size());
			response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

			FileCopyUtils.copy(baos.toByteArray(), response.getOutputStream());
		}
	}
	
	/*
	 * Regular Label using Cert ID (optionally job item for future)
	 */
	@RequestMapping(value = "/birtcalspeclabel", method = RequestMethod.GET)
	public void downloadSpecificCalLabel(@RequestParam("certLinkId") int certLinkId,			
			@RequestParam("includeRecallDate") boolean includeRecallDate, HttpServletResponse response, HttpSession session, Locale locale)
			throws IOException, EngineException {

		ByteArrayOutputStream baos = new ByteArrayOutputStream(10 * 1024);

		if (labelService.printCalSpecificLabelForJobItem(certLinkId, includeRecallDate, locale, baos, currentCompany(session))) {
			String filename = "calspeclabel_" + certLinkId + ".pdf";
			response.setContentType(URLConnection.guessContentTypeFromName(filename));
			response.setContentLength(baos.size());
			response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

			FileCopyUtils.copy(baos.toByteArray(), response.getOutputStream());
		}
	}

	@RequestMapping(value = "/patlabel", method = RequestMethod.GET)
	public void downloadPATLabel(@RequestParam("plantId") int plantId,
			@RequestParam("serial") String serial,
			@RequestParam("date") String date,
			@RequestParam("templateKey") String templateKey,
			HttpServletResponse response, HttpSession session, Locale locale)
			throws IOException, EngineException {

		ByteArrayOutputStream baos = new ByteArrayOutputStream(10 * 1024);
		
		if (labelService.printInstDateLabel(templateKey, plantId, date, serial, baos, locale, currentCompany(session)))
		{
			String filename = "patlabel_" + templateKey + "_" + plantId + ".pdf";
			response.setContentType(URLConnection.guessContentTypeFromName(filename));
			response.setContentLength(baos.size());
			response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

			FileCopyUtils.copy(baos.toByteArray(), response.getOutputStream());
		}
	}

	@RequestMapping(value = "/stringdatelabel", method = RequestMethod.GET)
	public void downloadStringDateLabel(@RequestParam("serial") String serial,
			@RequestParam("date") String date,
			@RequestParam("templateKey") String templateKey,
			HttpServletResponse response, HttpSession session, Locale locale)
			throws IOException, EngineException {

		ByteArrayOutputStream baos = new ByteArrayOutputStream(10 * 1024);
		
		if (labelService.printStringDateLabel(templateKey, serial, date, baos, locale, currentCompany(session)))
		{
			String filename = "stringdatelabel_" + templateKey + "_" + serial + ".pdf";
			response.setContentType(URLConnection.guessContentTypeFromName(filename));
			response.setContentLength(baos.size());
			response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

			FileCopyUtils.copy(baos.toByteArray(), response.getOutputStream());
		}
	}

	@RequestMapping(value = "/nondatabasedlabel", method = RequestMethod.GET)
	public void downloadNonDatabasedLabel(@RequestParam("quantity") int quantity,
			@RequestParam("templateKey") String templateKey,
			HttpServletResponse response, HttpSession session, Locale locale)
			throws IOException, EngineException {

		ByteArrayOutputStream baos = new ByteArrayOutputStream(10 * 1024);
		
		if (labelService.printNonDataBasedLabel(templateKey, quantity, baos, locale, currentCompany(session)))
		{
			String filename = "nondatabasedlabel_" + templateKey + "_" + quantity + ".pdf";
			response.setContentType(URLConnection.guessContentTypeFromName(filename));
			response.setContentLength(baos.size());
			response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

			FileCopyUtils.copy(baos.toByteArray(), response.getOutputStream());
		}
	}
	
	private Company currentCompany(HttpSession session) {
		@SuppressWarnings("unchecked")
		KeyValue<Integer, String> ckv = (KeyValue<Integer, String>) session.getAttribute(Constants.SESSION_ATTRIBUTE_COMPANY);
//		CompanyKeyValue ckv = (CompanyKeyValue) session.getAttribute(Constants.SESSION_ATTRIBUTE_COMPANY);
		Company currentCompany = compServ.get(ckv.getKey());
		return currentCompany;
	}
}
