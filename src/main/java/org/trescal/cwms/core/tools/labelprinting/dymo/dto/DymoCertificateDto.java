package org.trescal.cwms.core.tools.labelprinting.dymo.dto;

public class DymoCertificateDto extends DymoDto {
	private String title;
	private String label_customer_name;
	private String label_customer_reference;
	private String label_contact_name;
	private String label_job_number;
	private String label_certificate_number;
	private String custname;
	private String custref;
	private String contact;
	private String jobno;
	private String certno;
	private String freetext;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLabel_customer_name() {
		return label_customer_name;
	}
	public void setLabel_customer_name(String label_customer_name) {
		this.label_customer_name = label_customer_name;
	}
	public String getLabel_customer_reference() {
		return label_customer_reference;
	}
	public void setLabel_customer_reference(String label_customer_reference) {
		this.label_customer_reference = label_customer_reference;
	}
	public String getLabel_contact_name() {
		return label_contact_name;
	}
	public void setLabel_contact_name(String label_contact_name) {
		this.label_contact_name = label_contact_name;
	}
	public String getLabel_job_number() {
		return label_job_number;
	}
	public void setLabel_job_number(String label_job_number) {
		this.label_job_number = label_job_number;
	}
	public String getLabel_certificate_number() {
		return label_certificate_number;
	}
	public void setLabel_certificate_number(String label_certificate_number) {
		this.label_certificate_number = label_certificate_number;
	}
	public String getCustname() {
		return custname;
	}
	public void setCustname(String custname) {
		this.custname = custname;
	}
	public String getCustref() {
		return custref;
	}
	public void setCustref(String custref) {
		this.custref = custref;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getJobno() {
		return jobno;
	}
	public void setJobno(String jobno) {
		this.jobno = jobno;
	}
	public String getCertno() {
		return certno;
	}
	public void setCertno(String certno) {
		this.certno = certno;
	}
	public String getFreetext() {
		return freetext;
	}
	public void setFreetext(String freetext) {
		this.freetext = freetext;
	}
}
