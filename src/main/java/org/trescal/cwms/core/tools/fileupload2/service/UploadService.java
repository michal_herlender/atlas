/**
 * Gunnar Hillert, 2006
 */
package org.trescal.cwms.core.tools.fileupload2.service;

import java.io.File;
import java.io.InputStream;

/**
 * Central service class.
 * 
 * @author Gunnar Hillert
 */
public interface UploadService
{

	/**
	 * Save the file to the given directory or if this is null or does not exist
	 * then the default temp directory.
	 * 
	 * @param inputStream Data of the file.
	 * @param toDirectory the directory to output the uploaded file to.
	 * @param fileName Name of the file uploaded.
	 */
	void saveFile(InputStream inputStream, File toDirectory, String fileName);

}
