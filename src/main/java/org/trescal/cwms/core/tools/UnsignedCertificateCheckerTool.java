package org.trescal.cwms.core.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_UnsignedCertificateNotification;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * TODO evaluate if this class needs to be a Service (for transaction demarcation) if it is resurrected and used
 */
@org.springframework.stereotype.Component("UnsignedCertificateTool")
public class UnsignedCertificateCheckerTool {
    @Autowired
    private CertificateService certServ;
    @Autowired
    private EmailContent_UnsignedCertificateNotification ecUnsignedCertificateNotification;
    @Autowired
    private EmailService emailServ;
    @Autowired
    private ScheduledTaskService schTaskServ;

    @Value("${cwms.admin.email}")
    private String adminEmail;

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public void checkUnsignedCertificates()
	{
		if (this.schTaskServ.scheduledTaskIsActive(this.getClass().getName(), Thread.currentThread().getStackTrace()))
		{
			List<Certificate> certs = this.certServ.getAllCertificatesAwaitingSignature();

			int noDocumentCount = 0;
			int unsignedCount = 0;

			if (certs.size() > 0)
			{
				// check each certificate
				for (Certificate c : certs)
				{
					// make sure certificate has a calibration, some valve certs
					// taken out in advance break this
					if (c.getCal() != null  && c.getCal().getStatus().getName().equals(CalibrationStatus.COMPLETE)) {
                        // e-mail engineer (CC lab manager)
                        // create array for to list
                        ArrayList<String> ccList = new ArrayList<String>();
                        // get department under which procedure is categorised
                        // and find any managers to cc in email
                        if (c.getCal().getCapability() != null) {
                            for (CategorisedCapability cat : c.getCal().getCapability().getCategories()) {
                                for (DepartmentMember dm : cat.getCategory().getDepartment().getMembers()) {
                                    if (dm.getDepartmentRole().isManage()) {
                                        if ((dm.getContact().getEmail() != null)
                                            && !dm.getContact().getEmail().equals("")) {
                                            ccList.add(dm.getContact().getEmail());
                                        }
                                    }
                                }
                            }
                        }

                        boolean fileExists = false;
                        // if this is a word certificate
                        if (c.getCal().getCalProcess().getProcess().trim().equalsIgnoreCase("word")) {
                            // check if word document exists
							for (CertLink cl : c.getLinks())
							{
								this.certServ.setCertificateDirectory(c);
								if (cl.getCert().hasWordFile())
								{
									fileExists = true;
									break;
								}
							}
						}
						// other processes don't require document to exist
						// before signing so set condition as met
						else
						{
							fileExists = true;
						}

						// update the respective counter
						if (fileExists)
						{
							unsignedCount++;
						}
						else
						{
							noDocumentCount++;
						}

						// create array for to list
						ArrayList<String> toList = new ArrayList<String>();
						// find engineer
						Contact engineer = c.getCal().getCompletedBy();
						Locale locale = engineer.getLocale();
						toList.add(engineer.getEmail());

						EmailContentDto contentDto = this.ecUnsignedCertificateNotification.getContent(c, fileExists, locale);
						String subject = contentDto.getSubject(); 
						String body = contentDto.getBody();
						// send email
						this.emailServ.sendAdvancedEmail(subject, adminEmail, toList, ccList, Collections.emptyList(), 
								Collections.emptyList(), Collections.emptyList(), body, true, true);
					}
				}
			}

			// report results of task run
			String message = unsignedCount
					+ " certificates found that remain unsigned, "
					+ noDocumentCount
					+ " certificates found that have no document";

			// send basic email to admin so we can see that it is running
			// properly
			this.emailServ.sendBasicEmail("Un-signed certificate schedule task result", adminEmail, adminEmail, message);

			this.schTaskServ.reportOnScheduledTaskRun(this.getClass().getName(), Thread.currentThread().getStackTrace(), true, message);
		}
		else
		{
			this.logger.info("Unsigned Certificate Checker not running: scheduled task cannot be found or is turned off");
		}
	}
}