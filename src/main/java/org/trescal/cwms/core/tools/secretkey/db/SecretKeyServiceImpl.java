package org.trescal.cwms.core.tools.secretkey.db;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.tools.secretkey.SecretKey;

import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.List;

@Service("SecretKeyService")
@Slf4j
public class SecretKeyServiceImpl extends
BaseServiceImpl<SecretKey, byte[]>  implements SecretKeyService, InitializingBean {
	 
	private static final String PASSWORD_HASH_ALGORITHM = "MD5";
	private static final byte[] keyValue = new byte[] { 'C','r','o','c','o','d','i','l','e'};
	private static final String SECKEY_ALGO = "AES";
	
	@Autowired
	@Qualifier("transactionManager")
	private PlatformTransactionManager transactionManager;
	@Autowired
	private SecretKeyDao secretKeyDao;

	@Override
	protected BaseDao<SecretKey, byte[]> getBaseDao() {
		return this.secretKeyDao;
	}
	
	
	
	@Override
	public void afterPropertiesSet() throws Exception {
		log.info("Generate Secret Key");
		MessageDigest digester = MessageDigest.getInstance(PASSWORD_HASH_ALGORITHM);
        digester.update(Arrays.toString(keyValue).getBytes(StandardCharsets.UTF_8));
        Key key = new SecretKeySpec(digester.digest(), SECKEY_ALGO);
        SecretKey myKey = new SecretKey();
        myKey.setKeyValue(key.getEncoded());
        TransactionTemplate tmpl = new TransactionTemplate(transactionManager);
        tmpl.execute(new TransactionCallbackWithoutResult() {
		
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				List<SecretKey> existingKeys = getAll();
				// save new generated key only if no key exist on DB
				if(CollectionUtils.isEmpty(existingKeys))
					save(myKey);
			}
		});
        
	}



	@Override
	@Cacheable("secretkey")
	public SecretKey getKey() {
		List<SecretKey> storedKey = this.getAll();
    	if(!storedKey.isEmpty())
    		return storedKey.get(0);
    	else 
    		return null;
	}
}