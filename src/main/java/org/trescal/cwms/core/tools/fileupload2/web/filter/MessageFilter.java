package org.trescal.cwms.core.tools.fileupload2.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class MessageFilter implements Filter
{

	public void destroy()
	{
	}

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException
	{
		HttpServletRequest request = (HttpServletRequest) req;

		Object message = request.getSession().getAttribute("message");

		if (message != null)
		{
			request.setAttribute("message", message);
			request.getSession().removeAttribute("message");
		}

		request.setAttribute("requestURL", request.getRequestURI());
		chain.doFilter(req, res);
	}

	public void init(FilterConfig filterConfig)
	{
	}
}
