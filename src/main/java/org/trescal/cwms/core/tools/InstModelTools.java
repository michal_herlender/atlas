package org.trescal.cwms.core.tools;

import lombok.val;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.RangeType;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicType;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.description.SubfamilyTypology;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.system.entity.translation.Translation;

import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

public class InstModelTools {


	public static String instrumentModelNameWithTypology(InstrumentModel model, Locale locale) {
		val modelNameBuilder = new StringBuilder(getBestTranslation(model.getNameTranslations(), locale, Locale.getDefault()));
		if (model.getDescription().getSubfamilyTypology() != SubfamilyTypology.UNDEFINED)
			modelNameBuilder.append("(").append(model.getDescription().getSubfamilyTypology().toString()).append(")");
		return modelNameBuilder.toString();
	}

	/**
	 * Returns a description of an instrument, using in preference:
	 * (a) for brand models, the full description from the instrument model
	 * (b) for subfamily models, the description from the instrument, with 
     *     the brand and model name fields from the instrument if populated  
	 * @param hideTypology (normally true = external customer use/document, false = internal use)
	 * @return description of instrument
	 */
	public static String instrumentModelNameViaTranslations(Instrument instrument, 
			boolean hideTypology, Locale locale, Locale fallbackLocale) {
		StringBuilder result = new StringBuilder();
		InstrumentModel instModel = instrument.getModel();
		result.append(getBestTranslation(instModel.getNameTranslations(), locale, fallbackLocale));
		if (!hideTypology &&
			!instrument.getModel().getDescription().getSubfamilyTypology().equals(SubfamilyTypology.UNDEFINED)) {
			// Display subfamily typology of BM or SFC, used everywhere in internal application that's not shown to customers 
			result.append(" (");
			result.append(instrument.getModel().getDescription().getSubfamilyTypology().toString());
			result.append(")");
		}
		if (instModel.getModelMfrType().equals(ModelMfrType.MFR_GENERIC) &&
				instrument.getMfr() != null &&
				!instrument.getMfr().getGenericMfr()) {
			result.append(" ");
			result.append(instrument.getMfr().getName());
		}
		if (instModel.getModel().equals("/") &&
				instrument.getModelname() != null &&
				!instrument.getModelname().equals("/")) {
			result.append(" ");
			result.append(instrument.getModelname());
		}
		return result.toString();
	}
	
	/**
	 * Convenience method with hidden typology (for now) - to be reviewed
	 */
	public static String instrumentModelNameViaTranslations(Instrument instrument, Locale locale, Locale fallbackLocale) {
		return instrumentModelNameViaTranslations(instrument, true, locale, fallbackLocale);
	}
	
	/**
	 * Returns same description of an instrument as the entity method
	 * instrumentModelNameViaTranslations(...), but via basic field
	 * values (allowing use of projections to build instrument model name)
	 */
	public static String instrumentModelNameViaFields(String instrumentModelTranslation,
			SubfamilyTypology typology, ModelMfrType modelMfrType, String instrumentMfrName, Boolean instrumentMfrGeneric, 
			String modelname, String instrumentModelname, boolean hideTypology) {
		StringBuilder result = new StringBuilder();
		result.append(instrumentModelTranslation);
		if (!hideTypology && !typology.equals(SubfamilyTypology.UNDEFINED)) {
			// Display subfamily typology of BM or SFC, used everywhere in internal application that's not shown to customers 
			result.append(" (");
			result.append(typology);
			result.append(")");
		}
		if (modelMfrType.equals(ModelMfrType.MFR_GENERIC) &&
				instrumentMfrName != null && instrumentMfrGeneric != null &&
				!instrumentMfrGeneric) {
			result.append(" ");
			result.append(instrumentMfrName);
		}
		if ((modelname == null || modelname.equals("/")) &&
				instrumentModelname != null &&
				!instrumentModelname.equals("/")) {
			result.append(" ");
			result.append(instrumentModelname);
		}
		return result.toString();
	}
	
	/**
	 * Utility static method to return a string containing a correctly formatted
	 * and localized full model name.  (Use for updating translations only!)
	 * Note - this will be SLOW in general use - use modelNameViaTranslations instead
	 */
	public static String modelName(InstrumentModel model, Locale locale, Locale defaultLocale) {

		//Set decimal formating
		DecimalFormat decimalFormat = new DecimalFormat("0.#");

		Set<ModelRange> ranges = new TreeSet<>();
		// populate ranges from model if any exists
		if ((model.getRanges() != null) && !model.getRanges().isEmpty()) {
			ranges = model.getSortedCharacteristics();
		}
		//String buffer to build name in.
		StringBuilder result = new StringBuilder();

		// Case A - sales category model, use translation of sales category
		if (model.getModelType().getSalescategory() && (model.getSalesCategory() != null)) {
			result.append(getBestTranslation(model.getSalesCategory().getTranslations(), locale, defaultLocale));
		}
		// Historically - Subfamily Typology P or MP (2 or 3) see RG2 in functional spec document - Subfamily Type
		// Changed (and simplified logic) to use this characteristic naming only for SFC models without brand and model, 
		// 2018-11-28 at request of Didier TERRIEN.  Otherwise the brand/model naming below is used.
		else if (model.getDescription().getSubfamilyTypology().equals(SubfamilyTypology.SFC) &&
			(model.getModel() == null || model.getModel().equals("/") || model.getModel().isEmpty()) &&
				(model.getModelMfrType().equals(ModelMfrType.MFR_GENERIC) || model.getMfr().getGenericMfr())) {
			// Add sub-family first
			result.append(getBestTranslation(model.getDescription().getTranslations(), locale, defaultLocale));
			result.append(" ");
			//Create set of characteristics
			SortedSet<ModelRange> characteristics = ranges.stream()
				.filter(r -> r.getCharacteristicType() == RangeType.CHARACTERISTIC)
				.filter(r -> r.getCharacteristicDescription().getPartOfKey() != null
					&& r.getCharacteristicDescription().getPartOfKey())
				.filter(r -> (r.getCharacteristicDescription().getCharacteristicType()
					.equals(CharacteristicType.FREE_FIELD) && (r.getMaxvalue() != null || r.getMinvalue() != null))
					|| (r.getCharacteristicDescription().getCharacteristicType().equals(
					CharacteristicType.VALUE_WITH_UNIT) && (r.getStart() != null || r.getEnd() != null))).collect(Collectors.toCollection(TreeSet::new));
			// now add characteristics
			boolean firstCharacteristic = true;
			if (!characteristics.isEmpty()) {
				for (ModelRange characteristic : characteristics) {
					if (!firstCharacteristic) result.append("; ");
					// FREE_FIELD characteristics first
					if (characteristic.getCharacteristicDescription().getCharacteristicType() == CharacteristicType.FREE_FIELD) {
						result.append(getCharacteristicShortName(characteristic, locale, defaultLocale));
						result.append(": ");
						// Free field characteristic
						if (characteristic.getMinvalue() != null && !characteristic.getMinvalue().isEmpty())
							result.append(characteristic.getMinvalue());
					}
					// value with unit characteristic
					if (characteristic.getCharacteristicDescription()
							.getCharacteristicType() == CharacteristicType.VALUE_WITH_UNIT) {
						//only a minimum value
						if (characteristic.getStart() != null && characteristic.getEnd() == null) {
							result.append(decimalFormat.format(characteristic.getStart()));
							result.append(" ");
							result.append(characteristic.getUom().getFormattedSymbol());
							result.append(" <= ");
							result.append(getCharacteristicShortName(characteristic, locale, defaultLocale));
						}
						//only a maximum value
						if (characteristic.getStart() == null && characteristic.getEnd() != null) {
							result.append(getCharacteristicShortName(characteristic, locale, defaultLocale));
							result.append(" < ");
							result.append(decimalFormat.format(characteristic.getEnd()));
							result.append(" ");
							result.append(characteristic.getUom().getFormattedSymbol());
						}
						//max and min values
						if (characteristic.getStart() != null && characteristic.getEnd() != null) {
							// max and min equal
							if (characteristic.getStart().equals(characteristic.getEnd())){
								result.append(getCharacteristicShortName(characteristic, locale, defaultLocale));
								result.append(" = ");
								result.append(decimalFormat.format(characteristic.getStart()));
								result.append(" ");
								result.append(characteristic.getUom().getFormattedSymbol());
							} else {
								result.append(decimalFormat.format(characteristic.getStart()));
								result.append(" ");
								result.append(characteristic.getUom().getFormattedSymbol());
								result.append(" <= ");
								result.append(getCharacteristicShortName(characteristic, locale, defaultLocale));
								result.append(" < ");
								result.append(decimalFormat.format(characteristic.getEnd()));
								result.append(" ");
								result.append(characteristic.getMaxUom() != null ? characteristic.getMaxUom().getFormattedSymbol() : characteristic.getUom().getFormattedSymbol());
							}
						}
					}
					firstCharacteristic = false;
				}
			}
			//Create map of library values
			Map<CharacteristicDescription, List<CharacteristicLibrary>> libraryValues = model.getLibraryCharacteristics();
			SortedMap<CharacteristicDescription, List<CharacteristicLibrary>> sortedLibraryValues =
				new TreeMap<>(libraryValues);
			for (Map.Entry<CharacteristicDescription, List<CharacteristicLibrary>> characteristic : sortedLibraryValues.entrySet()) {
				if (!firstCharacteristic) result.append("; ");
				boolean firstLibrary = true;
				// Sort library values before appending (2018-11-21, previously unsorted)
				List<CharacteristicLibrary> sortedValues = characteristic.getValue().stream()
					.sorted(Comparator.comparing(CharacteristicLibrary::getName))
					.collect(Collectors.toList());
				for (CharacteristicLibrary library : sortedValues) {
					if (!firstLibrary) result.append(" - ");
					if (library.getTranslation() == null || library.getTranslation().isEmpty())
						result.append(library.getName());
					else result.append(getBestTranslation(library.getTranslation(), locale, defaultLocale));
					firstLibrary = false;
				}
				firstCharacteristic = false;
			}
		}
		// Historically : Subfamily Typology M (1) see RG3 in functional spec document - Brand Model Type
		// Naming strategy also used for the UNDEFINED typology that's used for service models
		// Now used as default case except (a) sales category model and (b) SFC typology where brand and model defined - 2018-11-21
		else {
			// Add sub-family first
			result.append(getBestTranslation(model.getDescription().getTranslations(), locale, defaultLocale));

			// then Add manufacturer if model isn't generic
			if (model.getModelMfrType() != ModelMfrType.MFR_GENERIC && !model.getMfr().getName().equals("/")) {
				result.append(" ");
				result.append(model.getMfr().getName());
			}

			// then add model if it's not empty
			if (!model.getModel().isEmpty() && !model.getModel().equals("/")) {
				result.append(" ");
				result.append(model.getModel());
			}

			// Create set of options - now sorted as of 2018-11-21
			SortedSet<ModelRange> options = ranges.stream()
				.filter(r -> r.getCharacteristicType() == RangeType.OPTION).collect(Collectors.toCollection(TreeSet::new));
			// now add any options
			if (!options.isEmpty()) {
				for (ModelRange option : options) {
					result.append(" /");
					result.append(option.getModelOption().getCode());
				}
			}
		}
		return result.toString();
	}

	public static String modelNameViaTranslations(InstrumentModel model, Locale userLocale, Locale fallBackLocale) {
		return getBestTranslation(model.getNameTranslations(), userLocale, fallBackLocale);
	}

	private static String findTranslation(Set<Translation> translations, Locale locale) {

		for (Translation translation : translations) {
			if (translation.getLocale().equals(locale)) {
				return translation.getTranslation();
			}
		}

		return null;
	}

	private static String getBestTranslation(Set<Translation> translations, Locale userLocale,
			Locale fallBackLocale) {

		if (translations == null) return "No Translations Loaded";
		
		String translation = findTranslation(translations, userLocale);

		if (translation == null) {
			translation = findTranslation(translations, fallBackLocale);
		}

		if (translation == null) {
			translation = "No Valid Translation";
		}

		return translation;

	}
	
	
	private static String getCharacteristicShortName(ModelRange characteristic, Locale locale, Locale defaultLocale){
		// Characteristic name
				if (characteristic.getCharacteristicDescription().getShortNameTranslations() == null
						|| characteristic.getCharacteristicDescription().getShortNameTranslations().isEmpty()) {
					return characteristic.getCharacteristicDescription().getShortName();
				} else {
					return getBestTranslation(characteristic.getCharacteristicDescription().getShortNameTranslations(),
							locale, defaultLocale);
				}
	}
}
