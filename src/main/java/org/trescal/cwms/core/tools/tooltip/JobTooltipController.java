package org.trescal.cwms.core.tools.tooltip;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.db.ContractReviewService;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand.JOB_LOAD_CONTACT;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand.JOB_LOAD_STATUS;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_CONTRACT_REVIEW_PRICE;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_INSTRUMENTS;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_ITEM_STATES;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_SERVICE_TYPES;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionResult;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * 2019-12-17 - Replacement for DWR tooltip that returns overlay fragments using projections
 * TODO - consider different error handler
 */
@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class JobTooltipController {
    @Autowired
    private JobItemActionService jobItemActionService;
    @Autowired
    private ContractReviewService contractReviewService;
    @Autowired
    private JobProjectionService jobProjectionService;
    @Autowired
    private JobItemProjectionService jobItemProjectionService;

    @RequestMapping(path = "tooltipjob.htm", method = RequestMethod.GET)
    public String getJobContent(Model model, Locale locale,
                                @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
                                @RequestParam(name = "jobid") Integer jobId) {

        Integer allocatedCompanyId = companyDto.getKey();

        JobProjectionCommand jobCommand = new JobProjectionCommand(
            locale, allocatedCompanyId, JOB_LOAD_CONTACT.class, JOB_LOAD_STATUS.class);
        JobProjectionDTO jobDto = this.jobProjectionService.getDTOByJobId(jobId, jobCommand);

        // Load with short names for service types only
        JobItemProjectionCommand jiCommand = new JobItemProjectionCommand(locale, allocatedCompanyId,
            JI_LOAD_INSTRUMENTS.class, JI_LOAD_SERVICE_TYPES.class, JI_LOAD_ITEM_STATES.class);

        List<JobItemProjectionDTO> jobItems = this.jobItemProjectionService.getProjectionDTOsForJobIds(
            Collections.singleton(jobId), jiCommand);
        jobItems.sort(Comparator.comparingInt(JobItemProjectionDTO::getItemno));
        jobDto.setJobitems(jobItems);

        model.addAttribute("jobDto", jobDto);
        return "trescal/core/tools/tooltip/tooltip_job";
    }


    @RequestMapping(path = "tooltipjobitem.htm", method = RequestMethod.GET)
    public String getJobItemContent(Model model, Locale locale,
                                    @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
                                    @RequestParam(name = "jobitemid") Integer jobItemId) {

        Integer allocatedCompanyId = companyDto.getKey();

        // Load with short names for service types only
        JobItemProjectionCommand command = new JobItemProjectionCommand(locale, allocatedCompanyId,
            JI_LOAD_INSTRUMENTS.class, JI_LOAD_SERVICE_TYPES.class, JI_LOAD_ITEM_STATES.class, JI_LOAD_CONTRACT_REVIEW_PRICE.class);

        JobItemProjectionResult jipResult = this.jobItemProjectionService.getProjectionDTOsForJobItemIds(
            Collections.singleton(jobItemId), command);
        if (jipResult.getJiDtos().isEmpty()) throw new RuntimeException("Job item not found for id " + jobItemId);
        JobItemProjectionDTO jiDto = jipResult.getJiDtos().get(0);
        jobItemProjectionService.loadAdditionalProjections(jiDto, allocatedCompanyId, locale);
        model.addAttribute("jiDto", jiDto);
        return "trescal/core/tools/tooltip/tooltip_jobitem";
    }
	
}
