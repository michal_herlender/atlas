package org.trescal.cwms.core.tools.filebrowser;

import java.util.ArrayList;
import java.util.List;

public class FileBrowserWrapper
{
	private List<FileWrapper> files = new ArrayList<FileWrapper>();
	private int numberOfFiles;

	public List<FileWrapper> getFiles()
	{
		return this.files;
	}

	public int getNumberOfFiles()
	{
		return this.numberOfFiles;
	}

	public void setFiles(List<FileWrapper> files)
	{
		this.files = files;
	}

	public void setNumberOfFiles(int numberOfFiles)
	{
		this.numberOfFiles = numberOfFiles;
	}

}
