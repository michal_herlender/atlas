package org.trescal.cwms.core.tools.filebrowser;

import java.util.Comparator;

/**
 * Inner comparator class for sorting {@link FileWrapper} objects.
 * 
 * @author jamiev
 */
public class FileWrapperComparator implements Comparator<FileWrapper>
{
	public int compare(FileWrapper fw1, FileWrapper fw2)
	{
		// if directory
		if (fw1.isDirectory() && fw2.isDirectory())
		{
			// otherwise alphabetise
			String name1 = fw1.getFileName();
			String name2 = fw2.getFileName();

			return name1.compareTo(name2);
		}
		else if (fw1.isDirectory() && !fw2.isDirectory())
		{
			return -1;
		}
		else if (fw2.isDirectory() && !fw1.isDirectory())
		{
			return 1;
		}

		// otherwise alphabetise
		String name1 = fw1.getFileName();
		String name2 = fw2.getFileName();

		return name1.compareTo(name2);
	}
}