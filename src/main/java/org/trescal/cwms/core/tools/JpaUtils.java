package org.trescal.cwms.core.tools;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.util.function.Function;
import java.util.stream.Stream;

public class JpaUtils {

    public static Stream<Predicate> entityByIdPredicate(CriteriaBuilder cb, Path<?> path, Integer id) {
        if (id != null && id > 0)
            return Stream.of(cb.equal(path, id));
        return Stream.empty();
    }

    public static Stream<Predicate> equalStringsIfNotBlankPredicate(CriteriaBuilder cb, Path<String> path, String value) {
        if (StringUtils.isBlank(value)) return Stream.empty();
        return Stream.of(cb.equal(path, value));
    }

    public static boolean checkIfNullOrFalse(Boolean test) {
        return test == null || !test;
    }

    public static Boolean checkIfNullOrZero(Integer number) {
        return number == null || number == -1;
    }

    public static Function<CriteriaBuilder, Expression<String>> calculateTypology(Path<Integer> typology) {
        return cb -> cb.<String>selectCase()
                .when(cb.equal(typology, 1), cb.literal("(BM)"))
                .when(cb.equal(typology, 2), cb.literal("(SFC)"))
                .otherwise("UNDEFINED");
    }
}
