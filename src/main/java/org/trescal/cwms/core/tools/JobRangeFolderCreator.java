package org.trescal.cwms.core.tools;

import java.io.File;

public class JobRangeFolderCreator
{
	/**
	 * Finds the complete cert path using both the jobno and certno fields
	 * 
	 * @param jobno
	 * @param certno
	 * @return The full path to the certificate as a String
	 */
	public static String getCertPath(String jobno, String certno)
	{
		String filePath = getJobFolder(jobno);
		filePath = filePath.concat("Certificates\\" + certno + ".pdf");

		return filePath;
	}

	/**
	 * Finds the job folder in the file structure when given the unique jobno
	 * 
	 * @param jobno
	 * @return The file path to the job folder in a String
	 */
	public static String getJobFolder(String jobno)
	{
		/*
		 * try { File currDir = new File("."); System.out.println ( "Dir : " +
		 * currDir ); System.out.println ( "Dir : " + currDir.getCanonicalPath()
		 * ); Properties properties = new Properties(); properties.load(new
		 * FileInputStream(new File("../project.properties")));
		 * System.out.println
		 * ("docsFolder= "+properties.getProperty("docs.folder")); } catch
		 * (IOException e) { e.printStackTrace(); }
		 */

		// Extract the year from the last two digits of the jobno
		String year = "20"
				+ jobno.substring(jobno.lastIndexOf("/") + 1, jobno.length());

		String file = "\\\\mdcketms03\\MDdocs\\Jobs\\" + year;
		jobno = jobno.replace('/', '.');

		String jobRange = jobno.substring(2, 5);
		int range = Integer.parseInt(jobRange);

		int upperBound = 0;
		int lowerBound = 0;

		if (range % 100 == 0)
		{
			upperBound = range;
			lowerBound = range - 99;
		}
		else
		{
			lowerBound = range / 100;
			upperBound = lowerBound + 1;
			lowerBound = (lowerBound * 100) + 1;
			upperBound = upperBound * 100;
		}

		String lb = String.valueOf(lowerBound);
		String ub = String.valueOf(upperBound);
		if (lb.length() < 3)
		{
			lb = "001";
		}
		String lowBound = jobno.substring(0, 2) + lb;
		String upBound = "";
		if (ub.length() == 4)
		{
			String inc = jobno.substring(0, 2);
			int p = Integer.parseInt(inc);
			p = p + 1;
			inc = String.valueOf(p);
			ub = ub.substring(1, ub.length());
			upBound = inc + ub;
		}
		else
		{
			upBound = jobno.substring(0, 2) + ub;
		}

		// Special case to handle the first directory starting at 10000
		if (upBound.equals("10100"))
		{
			lowBound = "10000";
		}

		file = file.concat("\\" + lowBound + "-" + upBound + "\\" + jobno
				+ "\\");

		File f = new File(file);
		if (!f.exists())
		{
			f.mkdir();

			// Make the contents directories for certs etc also
			File cert = new File(file.concat("Certificates\\"));
			cert.mkdir();
			File deli = new File(file.concat("Delivery\\"));
			deli.mkdir();
		}

		return file;
	}

	public static void main(String[] args)
	{
		String thing = getCertPath("10345/06", "12345");
		System.out.println(thing);

		/*
		 * int year = 2005; while(year<2016) { String file = "C:\\docs\\Jobs\\"
		 * + year + "\\"; int lowerBound = 10000; int upperBound =
		 * lowerBound+100; while(upperBound<12100) { String create =
		 * file.concat(lowerBound + "-" + upperBound + "\\"); File f = new
		 * File(create); if (!f.exists()) { f.mkdir(); } lowerBound =
		 * upperBound+1; upperBound = upperBound+100; } year++; }
		 */
	}

	private String docsFolder;

	public String getDocsFolder()
	{
		return this.docsFolder;
	}

	public void setDocsFolder(String docsFolder)
	{
		this.docsFolder = docsFolder;
	}
}
