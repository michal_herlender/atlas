package org.trescal.cwms.core.tools.autoservice;

public enum AutoServiceType {
	CLIENT, 
	SUPPLIER;
}
