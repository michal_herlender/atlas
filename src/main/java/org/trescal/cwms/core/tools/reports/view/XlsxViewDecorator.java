package org.trescal.cwms.core.tools.reports.view;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.tools.DateTools;

/**
 * 
 * 
 * See https://poi.apache.org/apidocs/dev/org/apache/poi/ss/usermodel/BuiltinFormats.html for date definitions
 */
public class XlsxViewDecorator {
	private Sheet sheet;
	private XlsxCellStyleHolder styleHolder;
	// Settings used for auto-sizing support of workbooks
	private boolean automaticAutosize;
	private boolean autosizePerformed;
	private int rowThreshold;
	private Set<Integer> columnsInUse;
	private Set<Integer> firstRowsInUse;
	
	public static final Logger logger = LoggerFactory.getLogger(XlsxViewDecorator.class);
	
	/**
	 * Constructor used without autosizing of sheet.  The sheet must be provided as an argument.
	 * CellStyleHolder may be null; if so it is initialized.
	 * CellStyleHolder should be used between sheets so that custom styles don't have to be recreated between multiple sheets.
	 */
	
	public XlsxViewDecorator(Sheet sheet, XlsxCellStyleHolder styleHolder) {
		this.sheet = sheet;
		this.automaticAutosize = false;
		this.autosizePerformed = false;
		this.columnsInUse = new HashSet<>();
		this.styleHolder = styleHolder == null ? new XlsxCellStyleHolder(sheet.getWorkbook()) : styleHolder;
	}

	/**
	 * Special constructor used for auto-sizing support of "streaming" workbooks
	 * By default only ~100 rows are held in memory with an SXSSF workbook
	 * So we automatically auto-size the workbook, once the first ~50 rows are populated 
	 * After the last row is entered, performAutosize() should be called in case <50 rows have been populated
	 * 
	 * The assumption here, is that the first 50 rows are "good enough" to determine auto-sizing.
	 * 
	 * If we upgrade POI versions (related BIRT dependency problem!) we could fix this via SXSSFSheet tracking support in newer POI.
	 * https://bz.apache.org/bugzilla/show_bug.cgi?id=57450
	 *  
	 * @param wb
	 * @param automaticAutosize
	 */
	public XlsxViewDecorator(Sheet sheet, SXSSFWorkbook sxxfWorkbook, boolean automaticAutosize, XlsxCellStyleHolder styleHolder) {
		this.sheet = sheet;
		this.columnsInUse = new HashSet<>();
		this.automaticAutosize = automaticAutosize;
		this.autosizePerformed = false;
		this.styleHolder = styleHolder == null ? new XlsxCellStyleHolder(sheet.getWorkbook()) : styleHolder;
		// Remaining fields only used for automatic autosizing
		this.rowThreshold = sxxfWorkbook.getRandomAccessWindowSize() / 2;
		this.firstRowsInUse = new HashSet<>();
	}
	
	public Sheet getSheet() {
		return sheet;
	}
	
	public void markCellAsUsed(int rowIndex, int colIndex) {
		markColumnAsUsed(colIndex);
		markRowAsUsed(rowIndex);
	}
	
	public void markColumnAsUsed(int colIndex) {
		if (!this.columnsInUse.contains(colIndex)) {
			this.columnsInUse.add(colIndex);
		}
	}
	
	public void markRowAsUsed(int rowIndex) {
		if (automaticAutosize && !autosizePerformed) {
			// We only need to track first rows in use, until the autosize has been performed
			if (!this.firstRowsInUse.contains(rowIndex)) {
				this.firstRowsInUse.add(rowIndex);
			}
		}
	}
	
	public Cell getCell(int rowIndex, int colIndex) {
		Row row = sheet.getRow(rowIndex);
		if (row == null) {
			row = sheet.createRow(rowIndex);
		}
		Cell cell = row.getCell(colIndex, Row.CREATE_NULL_AS_BLANK);
		markCellAsUsed(rowIndex, colIndex);
		return cell;
	}
	
	public void createCell(int rowIndex, int colIndex, String value, CellStyle style) {
		if (value != null) {
			Cell cell = getCell(rowIndex, colIndex);
			cell.setCellValue(value);
			if (style != null) {
				cell.setCellStyle(style);
			}
			autoSizeColumnsIfRequired();
		}
	}
	public void createCell(int rowIndex, int colIndex, LocalDate value, CellStyle style) {
		if (value != null) {
			createCell(rowIndex, colIndex, DateTools.dateFromLocalDate(value), style);
		}
	}
	public void createCell(int rowIndex, int colIndex, Date value, CellStyle style) {
		if (value != null) {
			Cell cell = getCell(rowIndex, colIndex);
			cell.setCellValue(value);
			if (style != null) {
				cell.setCellStyle(style);
			}
			autoSizeColumnsIfRequired();
		}
	}
	public void createCell(int rowIndex, int colIndex, Long value, CellStyle style) {
		if (value != null) {
			Cell cell = getCell(rowIndex, colIndex);
			cell.setCellValue(value.doubleValue());
			if (style != null) {
				cell.setCellStyle(style);
			}
			autoSizeColumnsIfRequired();
		}
	}
	public void createCell(int rowIndex, int colIndex, Integer value, CellStyle style) {
		if (value != null) {
			Cell cell = getCell(rowIndex, colIndex);
			cell.setCellValue(value.doubleValue());
			if (style != null) {
				cell.setCellStyle(style);
			}
			autoSizeColumnsIfRequired();
		}
	}
	public void createCell(int rowIndex, int colIndex, BigDecimal value, CellStyle style) {
		if (value != null) {
			Cell cell = getCell(rowIndex, colIndex);
			cell.setCellValue(value.doubleValue());
			if (style != null) {
				cell.setCellStyle(style);
			}
			autoSizeColumnsIfRequired();
		}
	}
	
	/**
	 * Internally called to check if the auto-size threshold has been reached, 
	 * and if so, performs an autosizing.  
	 */
	private void autoSizeColumnsIfRequired() {
		if (automaticAutosize && !autosizePerformed && 
				firstRowsInUse.size() > rowThreshold) {
			autoSizeColumns();
		}
	}

	/**
	 * Performs an autosize, if not already done
	 * Should be called by external logic once all rows have been written 
	 */
	public void autoSizeColumns() {
		if (!autosizePerformed) {
			for (Integer index : columnsInUse) {
				sheet.autoSizeColumn(index);
			}
			autosizePerformed = true;
		}
	}

	public XlsxCellStyleHolder getStyleHolder() {
		return styleHolder;
	}
}