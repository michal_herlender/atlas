package org.trescal.cwms.core.tools.reports.controller;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
import org.trescal.cwms.core.schedule.dto.CompanyWebSchedulesDTO;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.reports.view.XlsxViewDecorator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Component
public class CompanyCollectionsXlsxStreamingView extends AbstractXlsxStreamingView {

	@Autowired
	private MessageSource messages;

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
									  HttpServletResponse response) throws Exception {
		@SuppressWarnings("unchecked")
		List<CompanyWebSchedulesDTO> dtos = (List<CompanyWebSchedulesDTO>) model.get("dtos");
		String companyName = (String) model.get("companyName");
		Locale locale = LocaleContextHolder.getLocale();

		// create report file name

		String fileName = messages.getMessage("report.companycollection.filename", null, "Company Collection Report", null) +
			companyName +
			" - " +
			DateTools.dtf.format(new Date()) +
			".xlsx";
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

		SXSSFWorkbook streamingWorkbook = (SXSSFWorkbook) workbook;
		Sheet sheet = workbook.createSheet("Data");
		XlsxViewDecorator decorator = new XlsxViewDecorator(sheet, streamingWorkbook, true, null);

		addHeaders(decorator, locale);
		addValues(dtos, decorator);
		decorator.autoSizeColumns();
	}
	
	private void addHeaders(XlsxViewDecorator decorator, Locale locale) {
		CellStyle style = decorator.getStyleHolder().getHeaderStyle();
		int rowIndex = 0;
		int colIndex = 0;

		String titlePosition = this.messages.getMessage("position", null, "Position", locale);
		String visitorPosition = this.messages.getMessage("visitor", null, "Visitor", locale);
		String subdivisionPosition = this.messages.getMessage("subdivision", null, "Subdivision", locale);
		String countPosition = this.messages.getMessage("numberofcollections", null, "Number Of Collections", locale);

		decorator.createCell(rowIndex, colIndex++, titlePosition, style);
		decorator.createCell(rowIndex, colIndex++, visitorPosition, style);
		decorator.createCell(rowIndex, colIndex++, subdivisionPosition, style);
		decorator.createCell(rowIndex, colIndex, countPosition, style);
	}

	private void addValues(List<CompanyWebSchedulesDTO> dtos, XlsxViewDecorator decorator) {
		int rowIndex = 0;

		for (CompanyWebSchedulesDTO dto : dtos) {
			int colIndex = 0;

			CellStyle styleText = decorator.getStyleHolder().getTextStyle();
			CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();

			decorator.createCell(rowIndex, colIndex++, dto.getPos(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getName(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getSubname(), styleText);
			decorator.createCell(rowIndex, colIndex, dto.getTotal(), styleInteger);

			rowIndex++;
		}
	}
}