package org.trescal.cwms.core.tools.labelprinting.dymo.dto;

public class DymoDeliveryDto extends DymoDto {
	private String label_contact;
	private String label_delivery_address;
	private String label_business_address;
	private String label_date;
	private String label_type;
	private String label_job;
	private String deliveryid;
	
	public String getLabel_contact() {
		return label_contact;
	}
	public void setLabel_contact(String label_contact) {
		this.label_contact = label_contact;
	}
	public String getLabel_delivery_address() {
		return label_delivery_address;
	}
	public void setLabel_delivery_address(String label_delivery_address) {
		this.label_delivery_address = label_delivery_address;
	}
	public String getLabel_business_address() {
		return label_business_address;
	}
	public void setLabel_business_address(String label_business_address) {
		this.label_business_address = label_business_address;
	}
	public String getLabel_date() {
		return label_date;
	}
	public void setLabel_date(String label_date) {
		this.label_date = label_date;
	}
	public String getLabel_type() {
		return label_type;
	}
	public void setLabel_type(String label_type) {
		this.label_type = label_type;
	}
	public String getLabel_job() {
		return label_job;
	}
	public void setLabel_job(String label_job) {
		this.label_job = label_job;
	}
	public String getDeliveryid() {
		return deliveryid;
	}
	public void setDeliveryid(String deliveryid) {
		this.deliveryid = deliveryid;
	}
}
