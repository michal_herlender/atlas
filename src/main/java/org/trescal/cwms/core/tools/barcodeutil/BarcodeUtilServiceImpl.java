package org.trescal.cwms.core.tools.barcodeutil;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.OutputStream;

import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.DefaultConfigurationBuilder;
import org.krysalis.barcode4j.BarcodeGenerator;
import org.krysalis.barcode4j.BarcodeUtil;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.dwr.ResultWrapper;

@Service("BarcodeUtilService")
public class BarcodeUtilServiceImpl implements BarcodeUtilService
{
	@Autowired
	private ApplicationContext applicationContext;
	@Value("${cwms.config.barcode.config_file}")
	private String barcodeConfigPath;
	
	@Override
	public void generateBarcode(String barcodeText, OutputStream os) throws Exception {
		DefaultConfigurationBuilder builder = new DefaultConfigurationBuilder();
		Resource resource = this.applicationContext.getResource(this.barcodeConfigPath);
		Configuration cfg = builder.build(resource.getInputStream());

		BarcodeGenerator gen = BarcodeUtil.getInstance().createBarcodeGenerator(cfg);

		String mimeCode = "image/gif";
		// String mimeCode = "image/x-png";
		// String mimeCode = "image/jpeg";
		BitmapCanvasProvider provider = new BitmapCanvasProvider(os, mimeCode, 300, BufferedImage.TYPE_BYTE_GRAY, true, 0);
		gen.generateBarcode(provider, barcodeText);
		provider.finish();
	}
	
	@Override
	public ResultWrapper generateBarcode(String barcodeText, String outputPath)
	{
		try (OutputStream os = new java.io.FileOutputStream(outputPath))
		{
			generateBarcode(barcodeText, os);

			return new ResultWrapper(true, null, new File(outputPath), null);
		}
		catch (Exception e)
		{
			e.printStackTrace();

			return new ResultWrapper(false, e.getMessage(), null, null);
		}
	}

	@Override
	public ResultWrapper generateBarcodeForDelivery(Delivery delivery)
	{
		String outputPath = delivery.getDirectory().getAbsolutePath().concat(File.separator).concat("barcode-").concat(delivery.getDeliveryno().replace("/", "-")).concat(".gif");
		return this.generateBarcode(delivery.getDeliveryno(), outputPath);
	}
}