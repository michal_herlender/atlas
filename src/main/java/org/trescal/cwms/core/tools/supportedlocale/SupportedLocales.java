package org.trescal.cwms.core.tools.supportedlocale;

import java.util.Locale;

/**
 * Static class to provide references to supported locales 
 * that are included in the codebase for various purposes
 * 
 * Note, it's fine to have additional supported locales in the configuration,
 * without needing to define them here.
 * 
 * @author galen
 *
 */
public class SupportedLocales {
	public static Locale ENGLISH_GB = new Locale("en", "GB");
	public static Locale GERMAN_DE = new Locale("de", "DE");
	public static Locale SPANISH_ES = new Locale("es", "ES");
	public static Locale FRENCH_FR = new Locale("fr", "FR");
	public static Locale PORTUGUESE_PT = new Locale("pt", "PT");
	public static Locale ENGLISH_US = new Locale("en", "US");
}
