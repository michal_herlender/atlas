package org.trescal.cwms.core.tools.labelprinting.dymo.dto;

public class DymoAccessoryDto extends DymoDto{
	private String jobno;
	private String jobno_1;
	private String jobno_2;
	private String accessoriesTitle;
	private String accessories;
	
	public String getJobno() {
		return jobno;
	}
	public void setJobno(String jobno) {
		this.jobno = jobno;
	}
	public String getJobno_1() {
		return jobno_1;
	}
	public void setJobno_1(String jobno_1) {
		this.jobno_1 = jobno_1;
	}
	public String getJobno_2() {
		return jobno_2;
	}
	public void setJobno_2(String jobno_2) {
		this.jobno_2 = jobno_2;
	}
	public String getAccessoriesTitle() {
		return accessoriesTitle;
	}
	public void setAccessoriesTitle(String accessoriesTitle) {
		this.accessoriesTitle = accessoriesTitle;
	}
	public String getAccessories() {
		return accessories;
	}
	public void setAccessories(String accessories) {
		this.accessories = accessories;
	}
}
