package org.trescal.cwms.core.tools.printjob;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.system.entity.printer.Printer;

public interface PrintJobService {

	/**
	 * Prints the file at the given location using the default {@link Printer} for
	 * the {@link Contact} with the given ID.
	 * 
	 * @param filePath the path to the file.
	 * @param personId the {@link Contact} ID.
	 * @param quantity the quantity to print.
	 * @return true if the operation was successful, false otherwise.
	 */
	ResultWrapper ajaxPrintFile(String filePath, int personId, int quantity);

	/**
	 * Prints the file at the given location using the given {@link Printer} .
	 * 
	 * @param filePath the path to the file.
	 * @param printer  the {@link Printer} to use.
	 * @param quantity the quantity to print.
	 * @return true if the operation was successful, false otherwise.
	 */
	boolean printFile(String filePath, Printer printer, int quantity);

	/**
	 * Prints the file at the given location using the given {@link Printer}, but
	 * takes the quantity specified as gospel and ignores any rules that may be in
	 * place in the database to print a certain amount of copies based on file name
	 * patterns
	 * 
	 * @param filePath the path to the file.
	 * @param printer  the {@link Printer} to use.
	 * @param quantity the quantity to print.
	 * @return true if the operation was successful, false otherwise.
	 */
	boolean printFileIgnoringQuantityOverrides(String filePath, Printer printer, int quantity);
}