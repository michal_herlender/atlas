package org.trescal.cwms.core.tools.labelprinting.dymo;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.tools.labelprinting.dymo.dto.DymoCertificateDto;

@Component
public class DymoLabelCertificateEnvelope {
	@Autowired
	private MessageSource messageSource;
	
	public static final String FILENAME = "certenvelope.label";
	public static final String TEMPLATE_URL = "labels/dymo/certenvelope.label";
	
	public static final String LABEL_TITLE = "title";
	public static final String LABEL_CUSTOMER_NAME = "label_customer_name";
	public static final String LABEL_CUSTOMER_REFERENCE = "label_customer_reference";
	public static final String LABEL_CONTACT_NAME = "label_contact_name";
	public static final String LABEL_JOB_NUMBER = "label_job_number";
	public static final String LABEL_CERTIFICATE_NUMBER = "label_certificate_number";

	public static final String MESSAGE_CODE_TITLE = "dymo.certenvelope.title";
	public static final String MESSAGE_CODE_CUSTOMER_NAME = "dymo.certenvelope.customername";
	public static final String MESSAGE_CODE_CUSTOMER_REFERENCE = "dymo.certenvelope.customereference";
	public static final String MESSAGE_CODE_CONTACT_NAME = "dymo.certenvelope.contactname";
	public static final String MESSAGE_CODE_JOB_NUMBER = "dymo.certenvelope.jobno";
	public static final String MESSAGE_CODE_CERTIFICATE_NUMBER = "dymo.certenvelope.certificateno";
	// Used in job item number
	public static final String MESSAGE_CODE_ITEM = "dymo.certenvelope.item";

	public DymoCertificateDto getLabelDto(CertLink certLink, String freetext, Locale locale) {
		DymoCertificateDto dto = new DymoCertificateDto();
		dto.setCopies(1);
		dto.setLabelTemplateUrl(TEMPLATE_URL);
		dto.setCertno(getCertNo(certLink));
		dto.setContact(certLink.getJobItem().getJob().getCon().getName());
		dto.setCustname(certLink.getJobItem().getJob().getCon().getSub().getComp().getConame());
		dto.setCustref(getCustRef(certLink));
		dto.setFreetext(freetext);
		String itemText = messageSource.getMessage(DymoLabelCertificateEnvelope.MESSAGE_CODE_ITEM, null, locale);
		dto.setJobno(certLink.getJobItem().getJob().getJobno()+' '+itemText+' '+String.valueOf(certLink.getJobItem().getItemNo()));
		dto.setLabel_certificate_number(messageSource.getMessage(MESSAGE_CODE_CERTIFICATE_NUMBER, null, locale));
		dto.setLabel_contact_name(messageSource.getMessage(MESSAGE_CODE_CONTACT_NAME, null, locale));
		dto.setLabel_customer_name(messageSource.getMessage(MESSAGE_CODE_CUSTOMER_NAME, null, locale));
		dto.setLabel_customer_reference(messageSource.getMessage(MESSAGE_CODE_CUSTOMER_REFERENCE, null, locale));
		dto.setLabel_job_number(messageSource.getMessage(MESSAGE_CODE_JOB_NUMBER, null, locale));
		dto.setTitle(messageSource.getMessage(MESSAGE_CODE_TITLE, null, locale));
		return dto;
	}

	protected String getCustRef(CertLink certLink) {
		// evaluate data
		String ref = "";
		if ((certLink.getJobItem().getItemPOs() != null)
				&& (certLink.getJobItem().getItemPOs().size() > 0))
		{
			JobItemPO jipo = certLink.getJobItem().getItemPOs().iterator().next();
			if (jipo.getPo() != null)
			{
				ref = jipo.getPo().getPoNumber();
			}
			else if (jipo.getBpo() != null)
			{
				ref = jipo.getBpo().getPoNumber();
			}
		}
		return ref;
	}
	
	protected String getCertNo(CertLink certLink) {
		Certificate suppFor = certLink.getCert().getSupplementaryFor();
		for (CertLink link : certLink.getJobItem().getCertLinks())
		{
			if (link.getCert().getSupplementaryFor() != null)
			{
				if (link.getCert().getSupplementaryFor().getCertid() == certLink.getCert().getCertid())
				{
					suppFor = link.getCert();
				}
			}
		}
		String certNo = (suppFor != null) ? certLink.getCert().getCertno().concat(", ").concat(suppFor.getCertno()) : certLink.getCert().getCertno();
		return certNo;
	}	
}
