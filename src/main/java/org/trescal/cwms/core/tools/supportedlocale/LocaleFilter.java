package org.trescal.cwms.core.tools.supportedlocale;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/*
 * LocaleFilter to set the LocaleContextHolder on each and every request using the localeResolver.
 * Should resolves issues where DWR calls were not determining the locale and reverting to browser or server locale.
 * GB 2016-05-05
 * 
 * Implementation taken directly from:
 * https://templth.wordpress.com/2010/07/21/configuring-locale-switching-with-spring-mvc-3/
 */
public class LocaleFilter extends OncePerRequestFilter {
    
	private LocaleResolver localeResolver;
	
	private Logger logger = LoggerFactory.getLogger(LocaleFilter.class);

    protected void initFilterBean() throws ServletException {
        WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(
                     getServletContext());
        Map<String, LocaleResolver> resolvers = wac.getBeansOfType(LocaleResolver.class);
        for (LocaleResolver resolver : resolvers.values()) {
        	if (resolver.getClass().equals(SessionLocaleResolver.class)) {
        		localeResolver = resolver;
                logger.info("Found desired localeResolver of class "+localeResolver.getClass().getName());
        	}
        }
        
        if (localeResolver == null) {
        	logger.error("Could not determine localeResolver, resolvers.size()="+resolvers.size());
        }
    }
    
	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
        if (localeResolver != null) {
            Locale locale = localeResolver.resolveLocale(request);
            logger.trace("Setting locale to "+locale);
            LocaleContextHolder.setLocale(locale);
        }
        else {
            logger.trace("localeResolver null, existing locale is "+LocaleContextHolder.getLocale());
        }

        chain.doFilter(request, response);

        if (localeResolver!=null) {
            LocaleContextHolder.resetLocaleContext();
        }
    }

}
