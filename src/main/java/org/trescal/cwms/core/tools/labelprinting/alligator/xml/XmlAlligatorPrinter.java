package org.trescal.cwms.core.tools.labelprinting.alligator.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"supportedMedia","name","sharedPrinterName","ipv4","resolution","darknessLevel","printerLanguage"})
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlAlligatorPrinter {
	@XmlElement
	private XmlAlligatorSupportedMedia supportedMedia;
	@XmlElement
	private String name;
	@XmlElement
	private String sharedPrinterName;
	@XmlElement
	private String ipv4;
	@XmlElement
	private Integer resolution;
	@XmlElement
	private Integer darknessLevel;
	@XmlElement
	private String printerLanguage;

	public XmlAlligatorSupportedMedia getSupportedMedia() {
		return supportedMedia;
	}

	public String getName() {
		return name;
	}

	public String getSharedPrinterName() {
		return sharedPrinterName;
	}

	public String getIpv4() {
		return ipv4;
	}

	public Integer getResolution() {
		return resolution;
	}

	public Integer getDarknessLevel() {
		return darknessLevel;
	}

	public String getPrinterLanguage() {
		return printerLanguage;
	}

	public void setSupportedMedia(XmlAlligatorSupportedMedia supportedMedia) {
		this.supportedMedia = supportedMedia;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSharedPrinterName(String sharedPrinterName) {
		this.sharedPrinterName = sharedPrinterName;
	}

	public void setIpv4(String ipv4) {
		this.ipv4 = ipv4;
	}

	public void setResolution(Integer resolution) {
		this.resolution = resolution;
	}

	public void setDarknessLevel(Integer darknessLevel) {
		this.darknessLevel = darknessLevel;
	}

	public void setPrinterLanguage(String printerLanguage) {
		this.printerLanguage = printerLanguage;
	}
}
