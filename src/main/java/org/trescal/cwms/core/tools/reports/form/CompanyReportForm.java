package org.trescal.cwms.core.tools.reports.form;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
@Getter @Setter
public class CompanyReportForm {
	
	@NotNull
    @Min(value = 1, message = "{error.value.notselected}")
    private Integer coid;
    private String coname;
    @NotNull(message = "{error.value.notselected}")
    private LocalDate dateFrom;
    @NotNull(message = "{error.value.notselected}")
    private LocalDate dateTo;

}
