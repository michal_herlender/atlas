package org.trescal.cwms.core.tools.transitcalendar;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.tools.DateTools;

import java.time.LocalDate;
import java.util.*;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDate;

@Service
public class TransitCalendarServiceImpl implements TransitCalendarService {
	@Autowired
	private SubdivService subServ;
	@Autowired
	private TransportOptionService transOptServ;

	private void addMissingDates(TreeMap<Date, ArrayList<Transit>> map, int weeks) {
		Calendar end = new GregorianCalendar();
		end.add(Calendar.WEEK_OF_YEAR, weeks);

		Calendar temp = new GregorianCalendar();
		DateTools.setTimeToZeros(temp);

		while (temp.before(end)) {
			if (!map.containsKey(temp.getTime()))
			{
				map.put(temp.getTime(), new ArrayList<>());
			}

			temp.add(Calendar.DATE, 1);
		}

	}

	public TransitCalendar generateCalendar(int subdivid, int weeks)
	{
		Subdiv sub = this.subServ.get(subdivid);
		List<ClientTransit> cts = this.transOptServ.listClientTransits(subdivid, weeks);

		HashMap<Integer, ArrayList<ClientTransit>> clientTransits = this.indexClientTransits(cts);
		TreeMap<Date, ArrayList<Transit>> transitsByDate = this.indexAllTransitsByDate(cts);
		this.addMissingDates(transitsByDate, weeks);

		return new TransitCalendar(sub, weeks, clientTransits, transitsByDate);
	}

	@Override
	public ClientTransit getLastClientTransitBeforeDate(TransitCalendar transCal, int transOptId, LocalDate beforeDate) {
		val dayAfterBeforeDate = beforeDate.plusDays(1);

		ArrayList<ClientTransit> eligibleTransits = new ArrayList<>();
		HashMap<Integer, ArrayList<ClientTransit>> clientTransits = transCal.getClientTransits();

		// if there are transits of this type, get eligible
		if (clientTransits.containsKey(transOptId)) {
			ArrayList<ClientTransit> aptTransits = clientTransits.get(transOptId);

			for (ClientTransit ct : aptTransits) {
				val transitDate = ct.getTransitDate();

				if (transitDate.isBefore(dayAfterBeforeDate)) {
					eligibleTransits.add(ct);
				}
			}
		}

		// return the last in the list or null if there are none
		if (eligibleTransits.size() < 1)
		{
			return null;
		}
		else
		{
			eligibleTransits.sort(new TransitDateComparator());
			return eligibleTransits.get(eligibleTransits.size() - 1);
		}
	}

	public ClientTransit getNextAvailableClientTransit(TransitCalendar transCal, int transOptId)
	{
		ArrayList<ClientTransit> eligibleTransits = new ArrayList<>();
		HashMap<Integer, ArrayList<ClientTransit>> clientTransits = transCal.getClientTransits();

		// if there are transits of this type, get next available
		if (clientTransits.containsKey(transOptId))
		{
			ArrayList<ClientTransit> aptTransits = clientTransits.get(transOptId);

			eligibleTransits.addAll(aptTransits);
		}

		// return the first in the list or null if there are none
		if (eligibleTransits.size() < 1)
		{
			return null;
		}
		else
		{
			eligibleTransits.sort(new TransitDateComparator());
			return eligibleTransits.get(0);
		}
	}

	private TreeMap<Date, ArrayList<Transit>> indexAllTransitsByDate( List<ClientTransit> cts)
	{
		ArrayList<Transit> fullList = new ArrayList<>(cts);
		fullList.sort(new TransitDateComparator());

		TreeMap<Date, ArrayList<Transit>> map = new TreeMap<>();
		ArrayList<Transit> temp;

		if (fullList.size() > 0) {
			LocalDate listDate = fullList.get(0).getTransitDate();

			temp = new ArrayList<>();

			for (Transit t : fullList) {
				if (listDate.isEqual(t.getTransitDate())) {
					map.put(dateFromLocalDate(listDate), temp);
					listDate = t.getTransitDate();
					temp = new ArrayList<>();
				}

				temp.add(t);
			}

			map.put(dateFromLocalDate(listDate), temp);
		}

		return map;
	}

	private HashMap<Integer, ArrayList<ClientTransit>> indexClientTransits(List<ClientTransit> cts) {
		HashMap<Integer, ArrayList<ClientTransit>> map = new HashMap<>();
		cts.sort(new ClientTransitComparator());

		ArrayList<ClientTransit> temp;
		if (cts.size() > 0) {
			int transOptId = cts.get(0).getClientTransit().getId();

			temp = new ArrayList<>();

			for (ClientTransit ct : cts)
			{
				if (ct.getClientTransit().getId() != transOptId)
				{
					map.put(transOptId, temp);
					transOptId = ct.getClientTransit().getId();
					temp = new ArrayList<>();
				}

				temp.add(ct);
			}

			map.put(transOptId, temp);
		}

		return map;
	}
}