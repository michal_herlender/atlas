package org.trescal.cwms.core.tools.filebrowser;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;

@Entity
@Table(name = "regularexpression")
public class RegularExpression
{
	private int id;
	private String regEx;
	private SystemComponent sc;

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Length(min = 1, max = 100)
	@Column(name = "regex", length = 100, nullable = false)
	public String getRegEx()
	{
		return this.regEx;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sc")
	public SystemComponent getSc()
	{
		return this.sc;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setRegEx(String regEx)
	{
		this.regEx = regEx;
	}

	public void setSc(SystemComponent sc)
	{
		this.sc = sc;
	}
}