package org.trescal.cwms.core.tools.labelprinting.alligator.xml;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlAlligatorLabel {
	@XmlElement
	private String name;
	
	@XmlElementWrapper(name="parameters")
	@XmlElement(name="parameter")
	private List<XmlAlligatorParameterNameValue> parameters;
	
	@XmlElement
	private Integer quantity;
	
	public class Parameter {
	}

	public String getName() {
		return name;
	}

	public List<XmlAlligatorParameterNameValue> getParameters() {
		return parameters;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setParameters(List<XmlAlligatorParameterNameValue> parameters) {
		this.parameters = parameters;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
}