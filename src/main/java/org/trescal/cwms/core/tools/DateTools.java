package org.trescal.cwms.core.tools;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateTools {
	/**
	 * Global date format for CWMS
	 */
	public static final SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");

	public static final SimpleDateFormat df_ISO8601 = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat dtf_ISO8601 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
	public static final SimpleDateFormat ldtf_ISO8601 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

	/**
	 * Shows month and year only
	 *
	 * @deprecated localization issues, replace by: new SimpleDateFormat("MMMM yyyy", locale)
	 */
	public static final SimpleDateFormat dfmy = new SimpleDateFormat("MMMM yyyy");

	/**
	 * Global date and time format for CWMS
	 */
	public static final SimpleDateFormat dtf = new SimpleDateFormat("dd.MM.yyyy - h:mm:ss a");

	/**
	 * Date and time format used for job item actions (no seconds required)
	 */
	public static final SimpleDateFormat dtf_activities = new SimpleDateFormat("dd.MM.yyyy - HH:mm");

	/**
	 * Date and time format fit for file names
	 */
	public static final SimpleDateFormat dtf_files = new SimpleDateFormat("dd.MM.yyyy - HH.mm.ss");

	/**
	 * Date format for recalls.
	 */
	public static final SimpleDateFormat rdf = new SimpleDateFormat("M-yyyy");

	/**
	 * Date format for sql server
	 */
	public static final SimpleDateFormat sqlserverformat = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * Global time format for CWMS
	 */
	public static final SimpleDateFormat tf = new SimpleDateFormat("h:mm:ss a");

	/**
	 * Date format used for REST web services 
	 */
	public static final SimpleDateFormat rest_dtf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

	public static Date add(Date date, int field, int amount)
	{
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(field, amount);
		return cal.getTime();
	}

	public static String getDayInShortWords(int dayOfWeek)
	{
		String day;

		switch (dayOfWeek)
		{
			case Calendar.MONDAY:
				day = "Mon";
				break;
			case Calendar.TUESDAY:
				day = "Tue";
				break;
			case Calendar.WEDNESDAY:
				day = "Wed";
				break;
			case Calendar.THURSDAY:
				day = "Thu";
				break;
			case Calendar.FRIDAY:
				day = "Fri";
				break;
			case Calendar.SATURDAY:
				day = "Sat";
				break;
			case Calendar.SUNDAY:
				day = "Sun";
				break;
			default:
				day = null;
				break;
		}

		return day;
	}

	/**
	 * Returns the difference in days between two {@link Date}s, without taking
	 * into consideration the time of day. e.g. fromTime = 2nd January 09 -
	 * 11:00:00 PM, toTime = 3rd January 09 - 09:00:00 AM, would return 1 day
	 * even though there is only 10 hours difference
	 * 
	 * @param fromTime the first {@link Date}
	 * @param toTime the last {@link Date}
	 * @return the difference in days
	 */
	public static int getDaysDifference(Date fromTime, Date toTime)
	{
		fromTime = DateTools.setTimeToZeros(fromTime);
		toTime = DateTools.setTimeToZeros(toTime);

		long diffInMilliseconds = toTime.getTime() - fromTime.getTime();
		long diffInSeconds = diffInMilliseconds / 1000;
		long diffInMinutes = diffInSeconds / 60;
		long diffInHours = diffInMinutes / 60;
		long diffInDays = diffInHours / 24;

		return ((Long) diffInDays).intValue();
	}

	public static Date getFirstOfTheMonth(Date date)
	{
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.set(Calendar.DATE, 1);
		return cal.getTime();
	}

	/**
	 * Returns the latest of two Date values, accommodating null date values by returning the other date value if not null.
	 */
	public static Date getLatest(Date date1, Date date2) {
		Date result = null;
		if ((date1 == null) && (date2 != null)) {
			result = date2;
		}
		else if ((date1 != null) && (date2 == null)) {
			result = date1;
		}
		else if (date1 != null) {
			if (date2.after(date1)) result = date2;
			else result = date1;
		}
		return result;
	}

	/**
	 * Returns the difference in minutes between two {@link Date}s, rounding the
	 * answer to upper 5 minutes. e.g. the difference between 10:25 and 10:33 if
	 * both {@link Date}s were on the same day would be returned as 10 minutes.
	 * 
	 * @param fromTime the first {@link Date}
	 * @param toTime the last {@link Date}
	 * @return the difference in minutes (rounded to upper 5 minutes)
	 */
	public static Integer getMinutesDifference(Date fromTime, Date toTime)
	{
		if (toTime.after(fromTime))
		{
			Calendar cal = new GregorianCalendar();

			cal.setTime(fromTime);
			Long fromMin = cal.getTimeInMillis();
			cal.setTime(toTime);
			Long toMin = cal.getTimeInMillis();

			long diffInMillis = toMin - fromMin;
			long diffInMinutes = diffInMillis / (60 * 1000);

			try
			{
				BigDecimal num = BigDecimal.valueOf(diffInMinutes);
				BigDecimal num1 = num.divide(BigDecimal.valueOf(5.0), RoundingMode.UP);
				BigDecimal num2 = num1.multiply(BigDecimal.valueOf(5.0));
				BigInteger val = num2.toBigInteger();

				if (val.intValue() == 0)
				{
					return 5;
				}
				else
				{
					return val.intValue();
				}
			}
			catch (Exception e)
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}

	public static String getMonthInWords(int month)
	{
		String wMonth = null;
		switch (month)
		{
			case Calendar.JANUARY:
				wMonth = "January";
				break;
			case Calendar.FEBRUARY:
				wMonth = "February";
				break;
			case Calendar.MARCH:
				wMonth = "March";
				break;
			case Calendar.APRIL:
				wMonth = "April";
				break;
			case Calendar.MAY:
				wMonth = "May";
				break;
			case Calendar.JUNE:
				wMonth = "June";
				break;
			case Calendar.JULY:
				wMonth = "July";
				break;
			case Calendar.AUGUST:
				wMonth = "August";
				break;
			case Calendar.SEPTEMBER:
				wMonth = "September";
				break;
			case Calendar.OCTOBER:
				wMonth = "October";
				break;
			case Calendar.NOVEMBER:
				wMonth = "November";
				break;
			case Calendar.DECEMBER:
				wMonth = "December";
				break;
		}

		return wMonth;
	}

	public static String getMonthInWords(String month)
	{
		int m = Integer.parseInt(month);
		return getMonthInWords(m);
	}

	/**
	 * Calculates and returns a string representation of the next recall due
	 * date based on the given date. Subtracts a month from the given date.
	 *
	 * @return string representation of recall due date.
	 */
	public static String getRecallPeriod(LocalDate recallDueDate) {
		if (recallDueDate != null) {
			return recallDueDate.minusMonths(1).format(DateTimeFormatter.ofPattern("MMMM yyyy"));
		} else {
			return "Unable to calculate due date";
		}
	}

	/**
	 * this method checks to see if the last modified date is within the
	 * supplied timescale using the amount (i.e. -10 last ten days, months,
	 * years) and the type (i.e. "d" for days, "m" for month and "y" for year).
	 * 
	 * @param amount should be a minus figure to check against
	 * @param type string to indicate calendar type to minus (i.e. "d" for days,
	 *        "m" for month or "y" for year).
	 * @return boolean value indicating whether the modified date is within the
	 *         supplied timescale
	 */
	public static boolean isDateAfterXTimescale(int amount, String type, Date lastModified)
	{
		boolean result = false;
		// Migrated data may not always contain a lastModified date
		if (lastModified != null) {
			// create new calendar
			Calendar cal = new GregorianCalendar();

			// check the type of units to add or subtract
			if (type.trim().equalsIgnoreCase("d"))
			{
				cal.add(Calendar.DAY_OF_YEAR, amount);
			}
			else if (type.trim().equalsIgnoreCase("m"))
			{
				cal.add(Calendar.MONTH, amount);
			}
			else
			{
				cal.add(Calendar.YEAR, amount);
			}
			// create the timescale date we are testing
			Date dateTimescale = cal.getTime();
			// modified since timescale date tested
			result = lastModified.after(dateTimescale);
		}
		return result;		
	}

	/**
	 * Checks whether the given {@link Date} is more than a given amount of days
	 * ago
	 * 
	 * @param date the {@link Date} to check
	 * @param numOfDays the number of days to check
	 * @return true if 'date' is more than 'numOfDays' days ago, false otherwise
	 */
	public static boolean isMoreThanXDaysAgo(Date date, int numOfDays)
	{
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.DATE, -numOfDays);
		return date.before(cal.getTime());
	}

	/**
	 * Checks whether two {@link Date}s have the exact same day (i.e. the same
	 * day, month and year). This allows dates to be compared regardless of what
	 * is held in the time (i.e. hour, minute, second, millisecond) fields.
	 * 
	 * @param date1 the first {@link Date} to compare
	 * @param date2 the second {@link Date} to compare
	 * @return true if the {@link Date}s have the same day, otherwise false.
	 */
	public static boolean isSameDay(Date date1, Date date2)
	{
		return DateTools.dateToLocalDate(date1).equals(DateTools.dateToLocalDate(date2));
	}

	/**
	 * Similar to isSameDay but also supporting null dates, returning true if both dates are null or equal
	 */
	public static boolean isSameLocalDateOrBothNull(LocalDate date1, LocalDate date2) {
		if ((date1 == null) ^ (date2 == null)) {
			return false;
		} else if (date1 == null) {
			return true;
		} else {
			return date1.isEqual(date2);
		}
	}

	public static boolean isSameDayOrBothNull(Date date1, Date date2) {
		if ((date1 == null) ^ (date2 == null)) {
			return false;
		} else if (date1 == null) {
			return true;
		} else {
			return isSameDay(date1, date2);
		}
	}
	/**
	 * Checks whether two {@link Date}s have the exact same second (i.e. the
	 * same year, month, day, hour, minute and second). This allows dates to be
	 * compared regardless of what is held in the millisecond field.
	 * 
	 * @param date1 the first {@link Date} to compare
	 * @param date2 the second {@link Date} to compare
	 * @return true if the {@link Date}s are the exact same second in time,
	 *         otherwise false.
	 */
	public static boolean isSameSecond(Date date1, Date date2)
	{
		Calendar cal1 = new GregorianCalendar();
		cal1.setTime(date1);
		Calendar cal2 = new GregorianCalendar();
		cal2.setTime(date2);

		boolean sameYear = (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR));
		boolean sameMonth = (cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH));
		boolean sameDay = (cal1.get(Calendar.DATE) == cal2.get(Calendar.DATE));
		boolean sameHour = (cal1.get(Calendar.HOUR_OF_DAY) == cal2.get(Calendar.HOUR_OF_DAY));
		boolean sameMinute = (cal1.get(Calendar.MINUTE) == cal2.get(Calendar.MINUTE));
		boolean sameSecond = (cal1.get(Calendar.SECOND) == cal2.get(Calendar.SECOND));

		return sameYear && sameMonth && sameDay && sameHour && sameMinute
				&& sameSecond;
	}

	/**
	 * Checks whether the given {@link Date} is the exact same day as today
	 * (i.e. the same day, month and year). This allows the date to be compared
	 * regardless of what is held in the time (i.e. hour, minute, second,
	 * millisecond) fields.
	 * 
	 * @param date the {@link Date} to compare to today
	 * @return true if the {@link Date} is today, otherwise false.
	 */
	public static boolean isToday(Date date)
	{
		return isSameDay(date, new Date());
	}

	/**
	 * Sets the time fields of a {@link Calendar} object (i.e. hour, minute,
	 * second, millisecond) to zero so that the {@link Calendar} can be compared
	 * more easily.
	 * 
	 * @param cal the {@link Calendar} object
	 * @return the {@link Calendar} object
	 */
	public static Calendar setTimeToZeros(Calendar cal)
	{
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		return cal;
	}

	/**
	 * Sets the time fields of a {@link Date} object (i.e. hour, minute, second,
	 * millisecond) to zero so that the {@link Date} can be compared more
	 * easily.
	 * 
	 * @param date the {@link Date} object
	 * @return the {@link Date} object
	 */
	public static Date setTimeToZeros(Date date)
	{
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		setTimeToZeros(cal);
		date = cal.getTime();

		return date;
	}

	public static Date datePlusInterval(Date dateFrom, Integer interval, IntervalUnit intervalUnit ){
        if(interval == null) interval=12;
        if(intervalUnit == null) intervalUnit=IntervalUnit.MONTH;
		Instant instant = Instant.ofEpochMilli(dateFrom.getTime());
		LocalDate calInput = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
		LocalDate calOutput = datePlusInterval(calInput, interval, intervalUnit);
		return DateTools.dateFromLocalDate(calOutput);
	}

	public static LocalDate datePlusInterval(LocalDate dateFrom, Integer interval, IntervalUnit intervalUnit) {
		switch (intervalUnit) {
			case YEAR:
				return dateFrom.plusYears(interval);
			case MONTH:
				return dateFrom.plusMonths(interval);
			case WEEK:
				return dateFrom.plusWeeks(interval);
			case DAY:
				return dateFrom.plusDays(interval);
			default:
				throw new RuntimeException("Unknown interval unit");
		}
	}

	/**
	 * Logger for this class and subclasses
	 */
	protected final Log logger = LogFactory.getLog(this.getClass());

	public static String formatDateToISODate(Date date) {
		if (date == null) return "";
		if (date instanceof java.sql.Date)
			//java8 type inference for poor
			return ((java.sql.Date) date).toLocalDate().format(DateTimeFormatter.ISO_DATE);
		return date.toInstant().atZone(LocaleContextHolder.getTimeZone().toZoneId())
			.toLocalDate().format(DateTimeFormatter.ISO_DATE);
	}

	public static String formatDateToISODatetime(Date date) {
		if (date == null) return "";
		return dtf_ISO8601.format(date);
	}

	public static String formatDateToTimeString(Date date) {
		if (date == null) return "";
		return date.toInstant().atZone(LocaleContextHolder.getTimeZone().toZoneId()).toLocalTime()
			.toString();
	}

	public static Date dateFromLocalDate(LocalDate localDate) {
		if (localDate == null) return null;
		return Date.from(localDate.atStartOfDay(LocaleContextHolder.getTimeZone().toZoneId()).toInstant());
	}


	public static Date dateFromZonedDateTime(ZonedDateTime dateTime) {
		if (dateTime == null) return null;
		return Date.from(dateTime.toInstant());
	}

	public static ZonedDateTime localDateTimeToZonedDateTime(LocalDateTime dateTime) {
		if (dateTime == null) return null;
		return dateTime.atZone(LocaleContextHolder.getTimeZone().toZoneId());
	}

	public static Date dateFromLocalDateTime(LocalDateTime dateTime) {
		if (dateTime == null) return null;
		return Date.from(dateTime.atZone(LocaleContextHolder.getTimeZone().toZoneId()).toInstant());
	}

	public static ZonedDateTime dateToZonedDateTime(Date date) {
		if (date == null) return null;
		if (date instanceof java.sql.Date)
			return ((java.sql.Date) date).toLocalDate().atStartOfDay(LocaleContextHolder.getTimeZone().toZoneId());
		return date.toInstant().atZone(LocaleContextHolder.getTimeZone().toZoneId());
	}

	public static LocalDate dateToLocalDate(Date date) {
        if (date == null)
            return null;
        else if (date instanceof java.sql.Date)
            // No support for toInstant() with java.sql.Date
            return ((java.sql.Date) date).toLocalDate();
        else
            return date.toInstant().atZone(LocaleContextHolder.getTimeZone().toZoneId()).toLocalDate();
    }

	public static LocalDateTime dateToLocalDateTime(Date date) {
		return date != null ? date.toInstant().atZone(LocaleContextHolder.getTimeZone().toZoneId()).toLocalDateTime() : null;
	}

	public static LocalDateTime dateToLocalDateTime(Date date, ZoneId zoneId) {
		return date != null ? date.toInstant().atZone(zoneId).toLocalDateTime() : null;
	}


	public static LocalDateTime localDateTimeFromZonedDateTime(ZonedDateTime date) {
		return date != null ? date.withZoneSameInstant(LocaleContextHolder.getTimeZone().toZoneId()).toLocalDateTime() : null;
	}

	public static ZonedDateTime localDateToZonedDateTime(LocalDate date) {
		return date.atStartOfDay(LocaleContextHolder.getTimeZone().toZoneId());
	}

    public static String getTimeZone() {
        return LocaleContextHolder.getTimeZone().getID();
    }
}
