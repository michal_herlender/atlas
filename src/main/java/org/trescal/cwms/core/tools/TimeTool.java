package org.trescal.cwms.core.tools;

import io.vavr.Tuple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Locale;
import java.util.stream.Stream;

public class TimeTool {
	private static MessageSource messageSource;

	public static void setMessageSource(MessageSource messageSource) {
		TimeTool.messageSource = messageSource;
	}

	@Component
	public static class TimeToolMessageInjector {
		@Autowired
		private MessageSource messageSource;
		@PostConstruct
		public void postConstruct() {
			TimeTool.setMessageSource(this.messageSource);
		}
	}
	/**
	 * Returns a string detailing the time difference between two dates. Formats
	 * the results by showing the highest time period.
	 * 
	 * @param start the start date.
	 * @param end the end date.
	 * @return formatted date or empty string if no range found or if an
	 *         exception occurs.
	 */
	public static String calculateDateDifference(Date start, Date end)
	{
		try
		{
			long d1 = start.getTime();
			long d2 = end == null ? new Date().getTime() : end.getTime();
			long result = Math.abs(d1 - d2); // Abs value in case.

			// long represents millisecond.
			long sec_term = 1000;
			long min_term = 60 * 1000;
			long hour_term = 60 * min_term;
			long day_term = 24 * hour_term;
			long year_term = 365 * day_term;

			int year = (int) ((result / year_term));
			int day = (int) ((result % year_term) / day_term);
			int hour = (int) ((result % day_term) / hour_term);
			int min = (int) ((result % hour_term) / min_term);
			int sec = (int) ((result % min_term) / sec_term);

			if (year > 0)
			{
				Locale locale = LocaleContextHolder.getLocale();
				String singleMessage = messageSource.getMessage("timephrase.singleyear", null, "1 year ago", locale);
				String pluralMessage = messageSource.getMessage("timephrase.pluralyear", new Object[]{year}, "{0} years ago", locale);
				return year > 1 ? pluralMessage : singleMessage ;
			}
			else if (day > 0)
			{
				Locale locale = LocaleContextHolder.getLocale();
				String singleMessage = messageSource.getMessage("timephrase.singleday", null, "1 day ago", locale);
				String pluralMessage = messageSource.getMessage("timephrase.pluralday", new Object[]{day}, "{0} days ago", locale);
				return day > 1 ? pluralMessage : singleMessage ;
			}
			else if (hour > 0)
			{
				Locale locale = LocaleContextHolder.getLocale();
				String singleMessage = messageSource.getMessage("timephrase.singlehour", null, "1 hour ago", locale);
				String pluralMessage = messageSource.getMessage("timephrase.pluralhour", new Object[]{hour}, "{0} hours ago", locale);
				return hour > 1 ? pluralMessage : singleMessage ;
			}
			else if (min > 0)
			{
				Locale locale = LocaleContextHolder.getLocale();
				String singleMessage = messageSource.getMessage("timephrase.singleminute", null, "1 minute ago", locale);
				String pluralMessage = messageSource.getMessage("timephrase.pluralminute", new Object[]{min}, "{0} minutes ago", locale);
				return min > 1 ? pluralMessage : singleMessage ;
			}
			else
			{
				Locale locale = LocaleContextHolder.getLocale();
				String singleMessage = messageSource.getMessage("timephrase.singlesec", null, "1 second ago", locale);
				String pluralMessage = messageSource.getMessage("timephrase.pluralsec", new Object[]{sec}, "{0} seconds ago", locale);
				return sec > 1 ? pluralMessage : singleMessage;
			}
		} catch (Exception e) {
			return "";
		}
	}


	public static String formatPeriod(Period period) {
		Locale locale = LocaleContextHolder.getLocale();
		return Stream.of(Tuple.of(ChronoUnit.YEARS, period.getYears()),
			Tuple.of(ChronoUnit.MONTHS, period.getMonths()),
			Tuple.of(ChronoUnit.DAYS, period.getDays())
		).filter(p -> p._2 > 0).findFirst()
			.map(p -> TimeTool.formatUnit(p._1, p._2, locale)).orElse("");
	}

	private static String formatUnit(ChronoUnit unit, Integer value, Locale locale) {
		if (value == null || value == 0) return "";
		Integer[] values = {value};
		switch (unit) {
			case YEARS:
				return value > 1 ? messageSource.getMessage("timephrase.pluralyear", values, "{0} years ago", locale) : messageSource.getMessage("timephrase.singularyear", null, "1 year ago", locale);
			case MONTHS:
				return value > 1 ? messageSource.getMessage("timephrase.pluralmonth", values, "{0} months ago", locale) : messageSource.getMessage("timephrase.singularmotnh", null, "1 month ago", locale);
			case DAYS:
				return value > 1 ? messageSource.getMessage("timephrase.pluralday", values, "{0} days ago", locale) : messageSource.getMessage("timephrase.singularday", null, "1 day ago", locale);
			case HOURS:
				return value > 1 ? messageSource.getMessage("timephrase.pluralhour", values, "{0} hours ago", locale) : messageSource.getMessage("timephrase.singularhour", null, "1 hour ago", locale);
			case MINUTES:
				return value > 1 ? messageSource.getMessage("timephrase.pluralminute", values, "{0} minutes ago", locale) : messageSource.getMessage("timephrase.singularminute", null, "1 minute ago", locale);
			case SECONDS:
				return value > 1 ? messageSource.getMessage("timephrase.pluralsecond", values, "{0} seconds ago", locale) : messageSource.getMessage("timephrase.singularsecond", null, "1 second ago", locale);
			default:
				return "";
		}
	}
}
