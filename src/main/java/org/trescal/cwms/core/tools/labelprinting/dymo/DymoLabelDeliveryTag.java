package org.trescal.cwms.core.tools.labelprinting.dymo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.tools.labelprinting.dymo.dto.DymoDeliveryDto;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

@Component
public class DymoLabelDeliveryTag {
	@Autowired
	private MessageSource messageSource;

	public static final String TEMPLATE_URL = "labels/dymo/delitag.label";

	public static final String MESSAGE_CODE_TO = "dymo.delitag.to";
	
	/*
	 * Can be used for either a job delivery, or a general delivery
	 */
	public DymoDeliveryDto getLabelDto(Delivery delivery, Locale locale) {
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(locale);

        DymoDeliveryDto dto = new DymoDeliveryDto();
        dto.setCopies(1);
        dto.setLabelTemplateUrl(TEMPLATE_URL);
        dto.setDeliveryid(delivery.getDeliveryno());
        dto.setLabel_business_address(formatBusinessAddress(delivery.getOrganisation()));
        dto.setLabel_contact(messageSource.getMessage(MESSAGE_CODE_TO, null, locale) + ' ' + delivery.getContact().getName());
        dto.setLabel_date(delivery.getDeliverydate().format(formatter));
        dto.setLabel_delivery_address(formatAddress(delivery.getAddress()));
        dto.setLabel_job("");
        dto.setLabel_type(delivery.getType().getDescription());
        return dto;
    }

	private String formatAddress(Address address) {
		StringBuilder result = new StringBuilder();
		if (address.getAddr1().trim().length() > 0) result.append(address.getAddr1()).append("\r\n");
		if (address.getAddr2().trim().length() > 0) result.append(address.getAddr2()).append("\r\n");
		if (address.getAddr3().trim().length() > 0) {
			result.append(address.getAddr3()).append("\r\n");
		}
		if (address.getTown().trim().length() > 0) result.append(address.getTown()).append("\r\n");
		if (address.getCounty().trim().length() > 0) result.append(address.getCounty()).append("\r\n");
		if (address.getPostcode().trim().length() > 0) result.append(address.getPostcode()).append("\r\n");
		result.append(address.getCountry().getLocalizedName()).append("\r\n");
		return result.toString();
	}
		
	private String formatBusinessAddress(Subdiv allocatedSubdiv) {
		// Currently, the default address for the business subdiv is used
		// If in the future multiple addresses are needed, the delivery will need a source address.
		String result = "";
		if (allocatedSubdiv.getDefaultAddress() != null) {
			result = formatAddress(allocatedSubdiv.getDefaultAddress());
		}
		return result;
	}
}
