package org.trescal.cwms.core.tools.transitcalendar;

import java.time.LocalDate;

/**
 * @author jamiev
 */
public interface TransitCalendarService {
    TransitCalendar generateCalendar(int subdivid, int weeks);

    ClientTransit getLastClientTransitBeforeDate(TransitCalendar transCal, int transOptId, LocalDate beforeDate);

    ClientTransit getNextAvailableClientTransit(TransitCalendar transCal, int transOptId);
}