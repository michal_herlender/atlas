/*
 * Copyright 2002-2005 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.trescal.cwms.core.tools.fileupload2.web.ajax;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;
import org.trescal.cwms.core.tools.fileupload2.utils.MonitoredDiskFileItemFactory;
import org.trescal.cwms.core.tools.fileupload2.utils.UploadInfo;
import org.trescal.cwms.core.tools.fileupload2.utils.UploadListener;
import org.trescal.cwms.core.tools.pathservice.PathService;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * This class extends Spring's CommonsMultipartResolver (Spring 2.0) and
 * overrides the resolveMultipart method. A few lines of code have been added to
 * that method.
 * 
 * @author Gunnar Hillert
 * @see org.springframework.web.multipart.commons.CommonsMultipartResolver
 */
public class AjaxFileUploadMultipartResolver extends CommonsMultipartResolver
{

	/**
	 * Constructor.
	 */
	public AjaxFileUploadMultipartResolver()
	{
		super();
	}

	/**
	 * Constructor for standalone usage. Determines the servlet container's
	 * temporary directory via the given ServletContext.
	 *
	 * @param servletContext the ServletContext to use
	 */
	public AjaxFileUploadMultipartResolver(ServletContext servletContext) {
		this();
		this.setServletContext(servletContext);
	}

	@Value("#{props['cwms.config.filebrowser.fileSizeThreshold']}")
	private int fileSizeThreshold;
	@Autowired
	PathService pathServ;

	@Override
	public MultipartHttpServletRequest resolveMultipart(HttpServletRequest request) throws MultipartException {
		String encoding = this.determineEncoding(request);
		FileUpload fileUpload1 = this.prepareFileUpload(encoding); // renamed

		// Beginn of added code
		UploadListener listener = new UploadListener(request, 30);
		DiskFileItemFactory factory = new MonitoredDiskFileItemFactory(listener);
		factory.setSizeThreshold(fileSizeThreshold);
		ServletFileUpload fileUpload = new ServletFileUpload(factory);
		fileUpload.setSizeMax(fileUpload1.getSizeMax());
		fileUpload.setHeaderEncoding(fileUpload1.getHeaderEncoding());
		// end of added code
		
		try
		{
			List<FileItem> fileItems = fileUpload.parseRequest(request);
			MultipartParsingResult parsingResult = this.parseFileItems(fileItems, encoding);
			return new DefaultMultipartHttpServletRequest(request, parsingResult.getMultipartFiles(), parsingResult.getMultipartParameters(), null);
		}
		catch (FileUploadBase.SizeLimitExceededException ex)
		{
			// set a file uploadInfo object into the session to show that this
			// upload has failed because of file size.
			UploadInfo info = new UploadInfo();
			info.setStatus("filesizeerror");
			info.setMaxFileSize(fileUpload1.getSizeMax());
			request.getSession().setAttribute("uploadInfo", info);

			throw new MaxUploadSizeExceededException(fileUpload.getSizeMax(), ex);
		}
		catch (FileUploadException ex)
		{
			throw new MultipartException("Could not parse multipart servlet request", ex);
		}
	}
}
