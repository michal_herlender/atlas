package org.trescal.cwms.core.tools;

import org.jasypt.digest.config.SimpleDigesterConfig;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.salt.RandomSaltGenerator;

public class DigestConfigCreator
{
	public String digestAlgorithm;
	public Integer digestIterations;
	public String pwBasedAlgorithm;
	public String pwBasedKeyword;

	public SimpleDigesterConfig createDigesterConfig()
	{
		final SimpleDigesterConfig config = new SimpleDigesterConfig();
		config.setAlgorithm(this.digestAlgorithm);
		config.setIterations(this.digestIterations);
		config.setSaltGenerator(new RandomSaltGenerator());

		return config;
	}

	public StandardPBEStringEncryptor getStringEncryptor()
	{
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setPassword(this.pwBasedKeyword);
		encryptor.setAlgorithm(this.pwBasedAlgorithm);

		return encryptor;
	}

	public void setDigestAlgorithm(String digestAlgorithm)
	{
		this.digestAlgorithm = digestAlgorithm;
	}

	public void setDigestIterations(Integer digestIterations)
	{
		this.digestIterations = digestIterations;
	}

	public void setPwBasedAlgorithm(String pwBasedAlgorithm)
	{
		this.pwBasedAlgorithm = pwBasedAlgorithm;
	}

	public void setPwBasedKeyword(String pwBasedKeyword)
	{
		this.pwBasedKeyword = pwBasedKeyword;
	}
}