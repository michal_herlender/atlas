package org.trescal.cwms.core.tools.tooltip;

import io.vavr.control.Either;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.db.GenericPricingEntityService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.MultiCurrencySupport;
import org.trescal.cwms.spring.model.KeyValue;

import java.math.BigDecimal;

@RestController
@RequestMapping("tooltip/currency")
@SessionAttributes(value ={Constants.SESSION_ATTRIBUTE_COMPANY,Constants.HTTPSESS_PRICING_CLASS_NAME})
public class CurrencyConvertTooltipController {

    @Autowired
    private GenericPricingEntityService genericPricingEntityService;

    @Autowired
    private CompanyService companyService;

    @GetMapping("baseValue.json")
    Either<String,BigDecimal> baseValue(@RequestParam Integer entityId, @RequestParam BigDecimal ammount, @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer,String> company,@ModelAttribute(Constants.HTTPSESS_PRICING_CLASS_NAME) Class<? extends MultiCurrencySupport> pricingClassName){
        val baseCurrency = companyService.getCurrencyById(company.getKey());
        return genericPricingEntityService.convertValueToBaseCurrency(entityId,ammount,baseCurrency, pricingClassName);
    }
}
