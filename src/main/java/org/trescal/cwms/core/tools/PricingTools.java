package org.trescal.cwms.core.tools;

import java.math.BigDecimal;

import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;

/**
 * No longer used due to poor performance for large quotations - 
 * would iterate over 1000s of quotation items!
 * Replaced with QuotationItemDao:findSummaryDTOs(...)
 */
@Deprecated
public class PricingTools
{
	@Deprecated
	public static BigDecimal calculateQuoteCaltypeTotal(Quotation q, int caltypeId)
	{
		// create running total
		BigDecimal runningTotal = new BigDecimal(0.00);
		// accumulate all quote items for caltype
		for (Quotationitem qi : q.getQuotationitems())
		{
			// caltype the same?
			if (qi.getCaltype().getCalTypeId() == caltypeId)
			{
				runningTotal = runningTotal.add(qi.getFinalCost());
			}
		}
		// return the total for calibration type
		return runningTotal;
	}

	@Deprecated
	public static BigDecimal calculateQuoteHeadingCaltypeTotal(QuoteHeading qh, int caltypeId)
	{
		// create running total
		BigDecimal runningTotal = new BigDecimal(0.00);
		// accumulate all quote heading items
		for (Quotationitem qi : qh.getQuoteitems())
		{
			// check caltype matches?
			if (qi.getCaltype().getCalTypeId() == caltypeId)
			{
				runningTotal = runningTotal.add(qi.getFinalCost());
			}
		}
		// return the total for quote heading
		return runningTotal;
	}

	@Deprecated
	public static BigDecimal calculateQuoteHeadingTotal(QuoteHeading qh)
	{
		// create running total
		BigDecimal runningTotal = new BigDecimal(0.00);
		// accumulate all quote heading items
		for (Quotationitem qi : qh.getQuoteitems())
		{
			runningTotal = runningTotal.add(qi.getFinalCost());
		}
		// return the total for quote heading
		return runningTotal;
	}
}
