package org.trescal.cwms.core.tools;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Utility class that provides the current system datetime stamp. The
 * SystemTime.nowDate() function can be used in preference to new Date()
 * anywhere in the system. The class provides a replacementDate property which
 * gives one major benefit whilst unit testing - the mock stub for this class
 * provides a setter method for this property and therefore the date returned by
 * calls to these methods can be different to the system time. This helps to
 * write unit tests where the results vary depending on the current time.
 * 
 * @author JamieV
 */
public class SystemTime
{
	protected static Date replacementDate = null;

	public static Calendar nowCal()
	{
		Calendar cal = new GregorianCalendar();
		cal.setTime(SystemTime.nowDate());
		return cal;
	}

	public static Date nowDate()
	{
		Date date = (SystemTime.replacementDate != null) ? SystemTime.replacementDate : new Date();
		SystemTime.replacementDate = null;
		return date;
	}
}