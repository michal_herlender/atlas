package org.trescal.cwms.core.tools;

import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MathTools
{
	public static final MathContext mc = new MathContext(4, RoundingMode.HALF_UP);

	/**
	 * Rounding mode used for displaying financial calculations.
	 */
	public static final RoundingMode ROUND_MODE_FIN = RoundingMode.HALF_UP;
	
	/**
	 * Rounding mode for calibration procedures
	 */
	public static final RoundingMode ROUND_MODE_CALI = RoundingMode.HALF_UP;
	

	public static final MathContext caliContext = new MathContext(10,ROUND_MODE_CALI);

	/**
	 * Scale used for displaying and saving financial decimal values.
	 */
	public static final int SCALE_FIN = 2;

	/**
	 * Scale used for rounding values in financial arithmetic.
	 */
	public static final int SCALE_FIN_CALC = 4;

	public static final DecimalFormat getDecimalFormat()
	{
		DecimalFormat df = new DecimalFormat();
		df.setMinimumFractionDigits(2);
		return df;
	}

	public static final NumberFormat getNumberFormat()
	{
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		nf.setMinimumFractionDigits(2);
		return nf;
	}
}
