package org.trescal.cwms.core.tools.labelprinting.dymo.dto;

public abstract class DymoDto {
	private int copies;
	private String labelTemplateUrl;
	public int getCopies() {
		return copies;
	}
	public void setCopies(int copies) {
		this.copies = copies;
	}
	public String getLabelTemplateUrl() {
		return labelTemplateUrl;
	}
	public void setLabelTemplateUrl(String labelTemplateUrl) {
		this.labelTemplateUrl = labelTemplateUrl;
	}
}
