package org.trescal.cwms.core.tools.reports.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

public class QuotesForSalesFiguresDTO
{
	private Date acceptedOn;
	private String company;
	private String quoteNo;
	private BigDecimal sales;
	private String version;

	// this constructor is used by aliasToBean transformers in ReportsDaoImpl
	public QuotesForSalesFiguresDTO() {}
	
	public Date getAcceptedOn()
	{
		return this.acceptedOn;
	}

	public String getCompany()
	{
		return this.company;
	}

	public String getQuoteNo()
	{
		return this.quoteNo;
	}

	public BigDecimal getSales()
	{
		return this.sales;
	}

	public String getVersion()
	{
		return this.version;
	}

	public void setAcceptedOn(Date acceptedOn)
	{
		this.acceptedOn = acceptedOn;
	}

	public void setCompany(String company)
	{
		this.company = company;
	}

	public void setQuoteNo(String quoteNo)
	{
		this.quoteNo = quoteNo;
	}

	public void setSales(BigDecimal sales)
	{
		this.sales = sales.setScale(2, RoundingMode.HALF_UP);
	}

	public void setVersion(String version)
	{
		this.version = version;
	}
}