package org.trescal.cwms.core.tools;

public interface UpdateRequiredViewsIF {
	public void updateDatabaseViews();
}
