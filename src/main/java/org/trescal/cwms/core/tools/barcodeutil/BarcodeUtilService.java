package org.trescal.cwms.core.tools.barcodeutil;

import java.io.OutputStream;

import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.dwr.ResultWrapper;

public interface BarcodeUtilService
{
	void generateBarcode(String barcodeText, OutputStream os) throws Exception;
	
	/**
	 * Generates a barcode image using the system's default barcode
	 * configuration and saves it to the given output path.
	 * 
	 * @param barcodeText the text to generate a barcode for
	 * @param outputPath the location to save the barcode image file
	 * @return a {@link ResultWrapper} indicating the success of the operation
	 */
	ResultWrapper generateBarcode(String barcodeText, String outputPath);

	/**
	 * Generates a barcode image using the system's default barcode
	 * configuration for the given {@link Delivery} and saves it to the
	 * {@link Delivery} folder.
	 * 
	 * @param delivery the delivery to create a barcode for
	 * @return a {@link ResultWrapper} indicating the success of the operation
	 */
	ResultWrapper generateBarcodeForDelivery(Delivery delivery);
}