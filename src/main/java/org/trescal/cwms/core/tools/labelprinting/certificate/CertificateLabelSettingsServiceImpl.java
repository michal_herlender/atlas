package org.trescal.cwms.core.tools.labelprinting.certificate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.*;
import org.trescal.cwms.core.system.enums.Scope;

@Service
public class CertificateLabelSettingsServiceImpl implements CertificateLabelSettingsService {
	// System Defaults for label printing
	@Autowired
	private AccreditedLabelRecallDate accreditedLabelRecallDate;
	@Autowired
	private AccreditedLabelCertificateNumber accreditedLabelCertificateNumber;
	@Autowired
	private AccreditedLabelPlantNumber accreditedLabelPlantNumber;
	@Autowired
	private StandardLabelRecallDate standardLabelRecallDate; 
	@Autowired
	private StandardLabelCertificateNumber standardLabelCertificateNumber; 
	@Autowired
	private StandardLabelPlantNumber standardLabelPlantNumber; 
	
	/**
	 * Returns the AccreditedLab entity (lab number) of the accreditation attached to the certificate,
	 * otherwise null if there is no accreditation.  Implemented to work on both reserved certificates
	 * and on certificates assigned to a calibration.
	 * 
	 * To receive an Accredited Label all of the following must be satisfied:
	 * (a) Certificate is an "inhouse" certificate (not third party or client)
	 * (b) Calibration Type is accredited
	 * (c) Procedure for Calibration has a valid lab accreditation specified
	 * (d) The lab accreditation has to have an accreditation body (otherwise indicates no-accreditation)
	 *
	 */
	@Override
	public AccreditedLab getAccreditedLab(Certificate certificate) {
		AccreditedLab result = null;
		if (certificate.getCalType() != null && 
				certificate.getCalType().getAccreditationSpecific()) {
            Capability capability = null;
            // If linked to calibration, use cal details
            if (certificate.getCal() != null) {
                capability = certificate.getCal().getCapability();
            } else if (certificate.getLinks() != null && !certificate.getLinks().isEmpty()) {
                JobItem ji = certificate.getLinks().iterator().next().getJobItem();
                // Otherwise attempt to use next work requirement details (e.g. reserved certificate)
                if (ji.getNextWorkReq() != null && (ji.getNextWorkReq().getWorkRequirement().getCapability() != null)) {
                    capability = ji.getNextWorkReq().getWorkRequirement().getCapability();
                }
            }
            if (capability != null &&
                capability.getLabAccreditation() != null &&
                capability.getLabAccreditation().getAccreditationBody() != null) {
                result = capability.getLabAccreditation();
            }
        }
		return result;
	}
	
	@Override
	public CertificateLabelSettings get(CertLink certLink) {
		// System defaults are currently global, however allocated company needed for query, so we use Job allocated business company
		Company allocatedCompanyOnJob = certLink.getJobItem().getJob().getOrganisation().getComp();
		Contact contactOnInstrument = certLink.getJobItem().getInst().getCon();
		boolean accredited;
		boolean labelCertificateNumber;
		boolean labelPlantNumber;
		String labelRecallDate;
		
		AccreditedLab accreditedLab = getAccreditedLab(certLink.getCert());
		if (accreditedLab != null) {
			accredited = true;
			labelCertificateNumber = this.accreditedLabelCertificateNumber.getValueByScope(Scope.CONTACT, contactOnInstrument, allocatedCompanyOnJob);
			labelPlantNumber = this.accreditedLabelPlantNumber.getValueByScope(Scope.CONTACT, contactOnInstrument, allocatedCompanyOnJob);
			labelRecallDate = this.accreditedLabelRecallDate.getValueHierarchical(Scope.CONTACT, contactOnInstrument, allocatedCompanyOnJob).getValue();
		}
		else {
			accredited = false;
			labelCertificateNumber = this.standardLabelCertificateNumber.getValueByScope(Scope.CONTACT, contactOnInstrument, allocatedCompanyOnJob);
			labelPlantNumber = this.standardLabelPlantNumber.getValueByScope(Scope.CONTACT, contactOnInstrument, allocatedCompanyOnJob);
			labelRecallDate = this.standardLabelRecallDate.getValueHierarchical(Scope.CONTACT, contactOnInstrument, allocatedCompanyOnJob).getValue();
		}
		return new CertificateLabelSettings(accredited, accreditedLab, labelCertificateNumber, labelPlantNumber, labelRecallDate);
	}
}
