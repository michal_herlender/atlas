package org.trescal.cwms.core.tools.reports.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.dto.UserLoginReportDTO;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.reports.form.CompanyReportForm;
import org.trescal.cwms.core.validation.BeanValidator;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY )
public class CompanyUserLoginController {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private BeanValidator validator;
    @Autowired
    private UserService userService;
    @Autowired
    private CompanyUserLoginXlsxStreamingView xlsxView;

    public static final String FORM_NAME = "form";
    
    @InitBinder(FORM_NAME)
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.setValidator(validator);
        binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
    }

    @ModelAttribute(FORM_NAME)
    public CompanyReportForm formBackingObject(@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer,String> companyDto) {
        CompanyReportForm form = new CompanyReportForm();
        // Sensible default for user logins - 1 year prior, up to today's date
        LocalDate now = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
        form.setDateTo(now);
        form.setCoid(companyDto.getKey());
        form.setDateFrom(now.minusYears(1));
        return form;
    }
	
	@RequestMapping(path="companyuserlogin.xlsx", method=RequestMethod.GET)
	public ModelAndView referenceData(@ModelAttribute(FORM_NAME) CompanyReportForm form) {
        Locale locale = LocaleContextHolder.getLocale();
        String reportTitle = this.messageSource.getMessage("tools.createuserloginreport", null, "Create User Login Report", locale);
        Map<String, Object> model = new HashMap<>();
        model.put("reportTitle", reportTitle);
        if (form.getCoid() != 0) {
            Company company = this.companyService.get(form.getCoid());
            model.put("companyName", company.getConame());
        }
        return new ModelAndView("trescal/core/reports/companyreports", model);
    }
	
	@RequestMapping(path="companyuserlogin.xlsx", method=RequestMethod.POST)
	public ModelAndView performRequest(@Validated @ModelAttribute(FORM_NAME) CompanyReportForm form, 
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData(form);
		}
		
		Company company = this.companyService.get(form.getCoid());
		// load all user login data for company between dates provided
		List<UserLoginReportDTO> userlogins = this.userService.getAllCompanyWebUserLoginsForPeriod(
				form.getCoid(), form.getDateFrom(), form.getDateTo());
		
		Map<String,Object> xlsxModel = new HashMap<>();
		xlsxModel.put("dtos", userlogins);
		xlsxModel.put("companyName", company.getConame());
		return new ModelAndView(xlsxView, xlsxModel);
	}	
}
