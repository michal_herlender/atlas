package org.trescal.cwms.core.tools;

import io.vavr.control.Either;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class OptionalsUtils {
    static public <A> Stream<A> optionalToStream(Optional<A> o){
        return o.map(Stream::of).orElseGet(Stream::empty);
    }

    static public <A,B> Function<Optional<B>,Either<A,B>> optionalToEither(Supplier<A> left){
        return o -> o.map(Either::<A,B>right).orElseGet(() -> Either.left(left.get()));
    }
}
