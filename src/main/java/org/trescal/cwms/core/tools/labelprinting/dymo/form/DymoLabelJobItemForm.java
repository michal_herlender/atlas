package org.trescal.cwms.core.tools.labelprinting.dymo.form;

import java.util.Map;

public class DymoLabelJobItemForm {
	private int jobId;
	private Map<Integer, Boolean> printJobItemLabel;
	private Map<Integer, Integer> copiesJobItemLabel;
	private Map<Integer, Boolean> printAccessoryLabel;
	private Map<Integer, Integer> copiesAccessoryLabel;
	
	public Map<Integer, Boolean> getPrintJobItemLabel() {
		return printJobItemLabel;
	}
	public void setPrintJobItemLabel(Map<Integer, Boolean> printJobItemLabel) {
		this.printJobItemLabel = printJobItemLabel;
	}
	public Map<Integer, Integer> getCopiesJobItemLabel() {
		return copiesJobItemLabel;
	}
	public void setCopiesJobItemLabel(Map<Integer, Integer> copiesJobItemLabel) {
		this.copiesJobItemLabel = copiesJobItemLabel;
	}
	public Map<Integer, Boolean> getPrintAccessoryLabel() {
		return printAccessoryLabel;
	}
	public void setPrintAccessoryLabel(Map<Integer, Boolean> printAccessoryLabel) {
		this.printAccessoryLabel = printAccessoryLabel;
	}
	public Map<Integer, Integer> getCopiesAccessoryLabel() {
		return copiesAccessoryLabel;
	}
	public void setCopiesAccessoryLabel(Map<Integer, Integer> copiesAccessoryLabel) {
		this.copiesAccessoryLabel = copiesAccessoryLabel;
	}
	public int getJobId() {
		return jobId;
	}
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}
}
