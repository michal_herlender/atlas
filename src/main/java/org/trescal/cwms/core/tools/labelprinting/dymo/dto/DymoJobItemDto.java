package org.trescal.cwms.core.tools.labelprinting.dymo.dto;

public class DymoJobItemDto extends DymoDto {
	private String jobno;
	private String jobno_1;
	private String jobno_2;
	private String company;
	private String subdiv;
	private String equip;
	private String date;
	private String type;
	private String plantid;
	private String noAccessories;
	
	public String getJobno() {
		return jobno;
	}
	public void setJobno(String jobno) {
		this.jobno = jobno;
	}
	public String getJobno_1() {
		return jobno_1;
	}
	public void setJobno_1(String jobno_1) {
		this.jobno_1 = jobno_1;
	}
	public String getJobno_2() {
		return jobno_2;
	}
	public void setJobno_2(String jobno_2) {
		this.jobno_2 = jobno_2;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getEquip() {
		return equip;
	}
	public void setEquip(String equip) {
		this.equip = equip;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getPlantid() {
		return plantid;
	}
	public void setPlantid(String plantid) {
		this.plantid = plantid;
	}
	public String getNoAccessories() {
		return noAccessories;
	}
	public void setNoAccessories(String noAccessories) {
		this.noAccessories = noAccessories;
	}
	public String getSubdiv() {
		return subdiv;
	}
	public void setSubdiv(String subdiv) {
		this.subdiv = subdiv;
	}
}
