package org.trescal.cwms.core.tools.labelprinting.dymo.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.db.CertLinkService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.tools.labelprinting.dymo.DymoLabelCertificateEnvelope;
import org.trescal.cwms.core.tools.labelprinting.dymo.DymoLabelDeliveryTag;
import org.trescal.cwms.core.tools.labelprinting.dymo.DymoLabelHireFolder;
import org.trescal.cwms.core.tools.labelprinting.dymo.DymoLabelJobFolder;
import org.trescal.cwms.core.tools.labelprinting.dymo.DymoLabelJobItem;
import org.trescal.cwms.core.tools.labelprinting.dymo.dto.DymoAccessoryDto;
import org.trescal.cwms.core.tools.labelprinting.dymo.dto.DymoCertificateDto;
import org.trescal.cwms.core.tools.labelprinting.dymo.dto.DymoDeliveryDto;
import org.trescal.cwms.core.tools.labelprinting.dymo.dto.DymoHireFolderDto;
import org.trescal.cwms.core.tools.labelprinting.dymo.dto.DymoJobFolderDto;

@Controller
@JsonController
public class DymoLabelController {
	// Components for printing different types of labels
	@Autowired
	private DymoLabelCertificateEnvelope labelCertificate;
	@Autowired
	private DymoLabelDeliveryTag labelDelivery;
	@Autowired
	private DymoLabelHireFolder labelHire;
	@Autowired
	private DymoLabelJobFolder labelJob;
	@Autowired
	private DymoLabelJobItem labelJobItem;
	// Services for data lookup
	@Autowired
	private CertLinkService certLinkService;
	@Autowired
	private DeliveryService deliveryService;
	@Autowired
	private HireService hireService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private JobService jobService;

	public static final String CERTIFICATE_REQUEST = "dymocertificate.json";
	public static final String CERTIFICATE_ID = "certlinkid";
	public static final String CERTIFICATE_FREETEXT = "freetext";

	public static final String DELIVERY_REQUEST = "dymodelivery.json";
	public static final String DELIVERY_ID = "deliveryid";

	public static final String HIREFOLDER_REQUEST = "dymohirefolder.json";
	public static final String HIRE_ID = "hireid";

	public static final String JOBFOLDER_REQUEST = "dymojobfolder.json";
	public static final String JOBFOLDER_ID = "jobid";

	// This is for label(s) for a single job item - form based printing is
	// handled by DymoLabelJobItemController
	public static final String JOBITEM_REQUEST = "dymojobitem.json";
	public static final String JOBITEM_ACCESSORY_REQUEST = "dymojobitemaccessory.json";
	public static final String JOBITEM_ID = "jobitemid";

	// This is to print all labels for a job - including job folder and all job
	// items
	public static final String ALL_JOB_REQUEST = "dymoalljob.json";
	public static final String ALL_JOB_ID = "jobid";

	// This is to print all job item labels for the specified job
	public static final String ALL_JOBITEM_REQUEST = "dymoalljobitem.json";
	public static final String ALL_JOBITEM_ID = "jobid";

	// This is to print selected job items
	public static final String SELECTED_JOBITEM_REQUEST = "dymoselectedjobitem.json";

	@ResponseBody
	@RequestMapping(value = CERTIFICATE_REQUEST)
	public List<DymoCertificateDto> getCertificateLabel(Locale locale,
			@RequestParam(name = CERTIFICATE_FREETEXT, required = false, defaultValue = "") String freeText,
			@RequestParam(name = CERTIFICATE_ID, required = true) int certLinkId) {
		CertLink certLink = this.certLinkService.findCertLink(certLinkId);
		return Collections.singletonList(labelCertificate.getLabelDto(certLink, freeText, locale));
	}

	@ResponseBody
	@RequestMapping(value = DELIVERY_REQUEST)
	public List<DymoDeliveryDto> getDeliveryLabel(Locale locale,
			@RequestParam(name = DELIVERY_ID, required = true) int deliveryId) {
		Delivery delivery = this.deliveryService.get(deliveryId);
		return Collections.singletonList(labelDelivery.getLabelDto(delivery, locale));
	}

	@ResponseBody
	@RequestMapping(value = HIREFOLDER_REQUEST)
	public List<DymoHireFolderDto> getHireLabel(Locale locale,
			@RequestParam(name = HIRE_ID, required = true) int hireId) {
		Hire hire = this.hireService.get(hireId);
		return Collections.singletonList(labelHire.getLabelDto(hire, locale));
	}

	@ResponseBody
	@RequestMapping(value = JOBFOLDER_REQUEST)
	public List<DymoJobFolderDto> getJobLabel(Locale locale,
			@RequestParam(name = JOBFOLDER_ID, required = true) int jobId) {
		Job job = this.jobService.get(jobId);
		return Collections.singletonList(labelJob.getLabelDto(job, locale));
	}

	protected void addLabelsForJobItem(List<Object> result, JobItem jobItem, Locale locale, Boolean printJobItemCount) {
		result.add(this.labelJobItem.getLabelDto(jobItem, 1, locale, printJobItemCount));
		if (this.labelJobItem.hasAccessories(jobItem)) {
			result.add(this.labelJobItem.getLabelDtoAccessories(jobItem, 1, locale, printJobItemCount));
		}
	}

	/**
	 * This is for label(s) for a single job item - form based printing is
	 * handled by DymoLabelJobItemController Multiple return types - to prevent
	 * type narrowing in deserialization we return List<Object>.
	 */
	@ResponseBody
	@RequestMapping(value = JOBITEM_REQUEST)
	public List<Object> getJobItemLabel(Locale locale,
			@RequestParam(name = JOBITEM_ID, required = true) int jobItemId,@RequestParam(required=false,defaultValue="true") Boolean printJobItemCount) {
		JobItem jobItem = this.jobItemService.findJobItem(jobItemId);
		List<Object> result = new ArrayList<>();
		addLabelsForJobItem(result, jobItem, locale,printJobItemCount);
		return result;
	}
	
	/**
	 * This is to print a single job item accessory label
	 */
	@ResponseBody
	@RequestMapping(value = JOBITEM_ACCESSORY_REQUEST)
	public List<DymoAccessoryDto> getAccessoryLabel(Locale locale,
			@RequestParam(name = JOBITEM_ID, required = true) int jobItemId) {
		JobItem jobItem = this.jobItemService.findJobItem(jobItemId);
		List<DymoAccessoryDto> result = new ArrayList<>();
		if (this.labelJobItem.hasAccessories(jobItem)) {
			result.add(this.labelJobItem.getLabelDtoAccessories(jobItem, 1, locale, true));
		}
		return result;
	}

	/*
	 * This is to print all labels for a job - including job folder and all job
	 * items Multiple return types - to prevent type narrowing in
	 * deserialization we return List<Object>.
	 */
	@ResponseBody
	@RequestMapping(value=ALL_JOB_REQUEST)
	public List<Object> getAllJobLabels(Locale locale,
			@RequestParam(name=ALL_JOB_ID, required=true) int jobId) {
		Job job = this.jobService.get(jobId);
		List<Object> result = new ArrayList<>();
		// Add job label
		result.add(this.labelJob.getLabelDto(job, locale));
		// Add job item labels
		for(JobItem jobItem : job.getItems()) {
			addLabelsForJobItem(result, jobItem, locale,true);
		}
		return result;
	}

	/*
	 * This is to print all job item labels for the specified job Multiple
	 * return types - to prevent type narrowing in deserialization we return
	 * List<Object>.
	 */
	@ResponseBody
	@RequestMapping(value = ALL_JOBITEM_REQUEST)
	public List<Object> getAllJobItemLabels(Locale locale,
			@RequestParam(name = ALL_JOBITEM_ID, required = true) int jobId) {
		Job job = this.jobService.get(jobId);
		List<Object> result = new ArrayList<>();
		// Add job item labels
		for (JobItem jobItem : job.getItems()) {
			addLabelsForJobItem(result, jobItem, locale, true);
		}
		return result;
	}

	@ResponseBody
	@RequestMapping(value = SELECTED_JOBITEM_REQUEST)
	public List<Object> getSelectedJobItemLabels(Locale locale,
			@RequestParam(required = true) List<Integer> jobItemIds) {
		List<Object> result = new ArrayList<>();
		// Add job item labels
		for (Integer jobitemId : jobItemIds) {
			JobItem jobItem = jobItemService.findJobItem(jobitemId);
			addLabelsForJobItem(result, jobItem, locale, false);
		}
		return result;
	}

}
