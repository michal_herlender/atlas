package org.trescal.cwms.core.tools.pdfconverter;

import com.crystaldecisions.sdk.occa.report.application.OpenReportOptions;
import com.crystaldecisions.sdk.occa.report.application.ParameterFieldController;
import com.crystaldecisions.sdk.occa.report.application.ReportAppSession;
import com.crystaldecisions.sdk.occa.report.application.ReportClientDocument;
import com.crystaldecisions.sdk.occa.report.exportoptions.ExportOptions;
import com.crystaldecisions.sdk.occa.report.exportoptions.ReportExportFormat;
import com.crystaldecisions.sdk.occa.report.lib.ReportSDKInvalidParameterFieldCurrentValueException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.db.StandardUsedService;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.format.DateTimeFormatter;

@Service("MetcalCertRetrievalService")
public class MetcalCertRetrievalService
{
	public static void main(String[] args)
	{
		new MetcalCertRetrievalService("antyarwsus");
	}

	protected final Log logger = LogFactory.getLog(this.getClass());
	private String rasServer;
	@Autowired
	private StandardUsedService suServ;
	@Autowired
	private SupportedLocaleService supportedLocaleService;

	public MetcalCertRetrievalService()
	{

	}

	public MetcalCertRetrievalService(String rasServer)
	{
		this.setRasServer(rasServer);
		SybaseConnection syB = new SybaseConnection("antyardb09", "metdb", "onlinecerts", "onl1nec3rts", 2638);
		boolean testBool = this.metGet("UKAS_ELEC_R1.04_TEST_DONOTUSE.rpt", "3324-08", "\\\\antyardb09\\DFS\\CWMS\\Metcal Certs\\", "\\\\antyardb09\\DFS\\CWMS\\Metcal Certs\\", syB, null);
	}

	public boolean metGet(String reportName, String certno, String storagePath, String metcalTemplatePath, SybaseConnection syB, Integer calId)
	{
		try
		{
			// Create a new Report Application Session
			ReportAppSession ra = new ReportAppSession();

			// Create a Report Application Server Service
			ra.createService("com.crystaldecisions.sdk.occa.report.application.ReportClientDocument");

			// Set the RAS Server to be used for the service (i.e. RAS could be
			// on seperate server)
			this.logger.debug("RAS server at: " + this.rasServer);
			ra.setReportAppServer(this.rasServer);

			// Initialize RAS
			ra.initialize();

			// Create the report client document object
			ReportClientDocument clientDoc = new ReportClientDocument();

			// Set the RAS Server to be used for the Client Document
			clientDoc.setReportAppServer(ra.getReportAppServer());

			// Open the report, and set the open type to Read Only
			this.logger.debug("Metcal template path : " + metcalTemplatePath
					+ reportName);
			clientDoc.open(metcalTemplatePath + reportName, OpenReportOptions._openAsReadOnly);

			// Login to the Database Controller through login method
			clientDoc.getDatabaseController().logon(syB.getUser(), syB.getPwd());

			// Create the METCAL Sybase connection
			Class.forName("com.sybase.jdbc2.jdbc.SybDriver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:sybase:Tds:"
					+ syB.getServer() + ":" + syB.getPort() + "?ServiceName="
					+ syB.getServiceName(), syB.getUser(), syB.getPwd());
			Statement stmt = con.createStatement();

			// Query the Sybase database for the CTAG based on the cert no
			ResultSet rs1 = stmt.executeQuery("select CTAG from mt.calibration where lower(rtrim(ltrim(C2354))) = lower(rtrim(ltrim('"
					+ certno + "'))) order by C2335 desc");

			if (rs1.next()) {
                String ctag = rs1.getString(1);
                // String ctag = "93152015:1291969720";
                // String ctag = "5893H03:1292426601";

                StringBuilder stdPlants = new StringBuilder();
                StringBuilder stdDescs = new StringBuilder();
                StringBuilder stdDates = new StringBuilder();
                // we are running the class as part of CWMS
                if (calId != null) {
                    for (StandardUsed su : this.suServ.getAllStandardsUsedOnCal(calId)) {
                        stdPlants.append((su.getInst().getPlantno() != null) ? su.getInst().getPlantno() : "").append("\n");
                        stdDescs.append(InstModelTools.modelNameViaTranslations(su.getInst().getModel(), LocaleContextHolder.getLocale(), supportedLocaleService.getPrimaryLocale())).append("\n");
                        stdDates.append((su.getNextCalDueDate() != null) ? su.getNextCalDueDate().format(DateTimeFormatter.ofPattern("dd MMM yyyy)")) : "").append("\n");
                    }
                }
				// we are running the class stand-alone (e.g. testing purposes)
				else {
                    stdPlants = new StringBuilder("STD0111" + "\n" + "STD0222" + "\n" + "STD0333"
                        + "\n");
                    stdDescs = new StringBuilder("Test Instrument One" + "\n"
                        + "Test Instrument Two" + "\n"
                        + "Test Instrument Three" + "\n");
                    stdDates = new StringBuilder("01 Jan 2011" + "\n" + "02 Feb 2012" + "\n"
                        + "03 Mar 2013" + "\n");
                }

				// set up to pass in parameter values to crystal report document
				ParameterFieldController paramController = clientDoc.getDataDefController().getParameterFieldController();

				// pass in CTAG from metcal db
				paramController.setCurrentValue("", "asset_ctag", ctag);

				try {
                    // Pass in information about the standards used
                    paramController.setCurrentValue("", "stdPlants", stdPlants.toString());
                    paramController.setCurrentValue("", "stdDescs", stdDescs.toString());
                    paramController.setCurrentValue("", "stdDates", stdDates.toString());
                }
				catch (ReportSDKInvalidParameterFieldCurrentValueException e)
				{
					this.logger.info("Report template '"
							+ reportName
							+ "' does not currently contain a section for the standards used.");
				}

				// Set up to export the report to a ByteArrayInputStream
				ByteArrayInputStream byteIS = (ByteArrayInputStream) clientDoc.getPrintOutputController().export(ReportExportFormat.RTF);

				// export the report - PDF or RTF only
				ExportOptions exportOptions = new ExportOptions();
				ReportExportFormat exportFormat = ReportExportFormat.RTF;
				exportOptions.setExportFormatType(exportFormat);

                // Create a byte[] (same size as the exported
                // ByteArrayInputStream)
                byte[] buf = new byte[byteIS.available()];

                FileOutputStream f = new FileOutputStream(storagePath + certno
                    + ".rtf");
                while ((byteIS.read(buf)) != -1) {
                    f.write(buf);
                }
                f.flush();
                f.close();
                clientDoc.dispose();
                clientDoc.close();
                rs1.close();
                stmt.close();
                con.close();
                return true;
            }
			else
			{
				clientDoc.dispose();
				clientDoc.close();
				rs1.close();
				stmt.close();
				con.close();
				this.logger.info("No certificates found in Metcal for the above query");
				return false;
			}

		}
		catch (Exception e)
		{
			this.logger.error("ERROR CAUGHT BY SYSTEM - STACKTRACE FOR INFORMATION PURPOSES ONLY");
			e.printStackTrace();
			return false;
		}
	}

	public void setRasServer(String rasServer)
	{
		this.rasServer = rasServer;
	}

	public void setSuServ(StandardUsedService suServ)
	{
		this.suServ = suServ;
	}
}