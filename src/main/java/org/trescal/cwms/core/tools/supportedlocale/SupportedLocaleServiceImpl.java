package org.trescal.cwms.core.tools.supportedlocale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.spring.model.KeyValue;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SupportedLocaleServiceImpl implements SupportedLocaleService {
	@Value("#{props['locale.country']}")
	String primaryCountry;

	@Value("#{props['locale.lang']}")
	String primaryLanguage;

	@Value("#{props['supportedLocales']}")
	String supportedLocaleNames;	
	
	private Locale primaryLocale;
	private List<Locale> supportedLocales;
	
	private static final Logger logger = LoggerFactory.getLogger(SupportedLocaleServiceImpl.class);
	
	/*
	 * Initialization properties are in format:
	 * locale.country=GB
	 * locale.lang=en
	 * supportedLocales=en_GB,fr_FR,de_DE
	 */
	@PostConstruct
	public void init() {
		primaryLocale = new Locale(primaryLanguage.trim(), primaryCountry.trim());
		logger.info("Primary locale is " + primaryLocale);
		supportedLocales = new ArrayList<>();
		String[] localeTokens = supportedLocaleNames.split(",");
		for (String localeToken : localeTokens) {
			String languageAndCountry = localeToken.trim();
			String[] tokens = languageAndCountry.split("_");
			supportedLocales.add(new Locale(tokens[0], tokens[1]));
		}
		logger.info("Supported locales are: " + Arrays.toString(supportedLocales.toArray()));
	}
	
	@Override
	public Locale getPrimaryLocale() {
		return primaryLocale;
	}
	@Override
	public List<Locale> getAdditionalLocales() {
		return supportedLocales.stream()
				.filter(locale -> !locale.equals(primaryLocale))
				.collect(Collectors.toList());
	}
	@Override
	public List<Locale> getSupportedLocales() {
		return Collections.unmodifiableList(supportedLocales);
	}
	@Override
	public Locale getBestLocale(Contact contact) {
		Locale bestLocale = primaryLocale;
		if (contact.getLocale() != null) {
			bestLocale = contact.getLocale();
		}
		return bestLocale;
	}
	
	@Override
	public List<KeyValue<String,String>> getDTOList(List<Locale> locales, Locale displayLocale) {
		List<KeyValue<String,String>> results = new ArrayList<>();
		for (Locale locale : locales) {
			results.add(new KeyValue<>(locale.toLanguageTag(), locale.getDisplayName(displayLocale)));
		}
		return results;
	}
	
	@Override
	public List<KeyValue<String,String>> getSupportedDTOList(Locale displayLocale) {
		return getDTOList(supportedLocales, displayLocale);
	}
	
	@Override
	public Locale getBestSupportedLocale(Locale locale) {
		Locale bestSupportedLocale = null;
		// Step 1 - find match by language and country
		for (Locale supportedLocale : supportedLocales) {
			if (supportedLocale.getLanguage().equals(locale.getCountry()) &&
				supportedLocale.getLanguage().equals(locale.getLanguage())) {
				bestSupportedLocale = supportedLocale;
			}
		}
		// Step 2 - find match by language
		if (bestSupportedLocale == null) {
			for (Locale supportedLocale : supportedLocales) {
				if (supportedLocale.getLanguage().equals(locale.getLanguage())) {
					bestSupportedLocale = supportedLocale;
				}
			}
			// Step 3 - if no match, return default locale
			if (bestSupportedLocale == null) {
				bestSupportedLocale = primaryLocale;
			}
		}
		return bestSupportedLocale;
	}
}