package org.trescal.cwms.core.annotation;

import java.lang.annotation.Documented;

@Documented
public @interface ClassDefinition
{
	String author();
	String date() default "dd/MM/yyyy";
	String lastModified() default "n/a";
	String lastModifiedBy() default "n/a";;
	String[] reviewers();

}
