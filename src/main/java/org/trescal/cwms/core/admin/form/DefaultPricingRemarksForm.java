package org.trescal.cwms.core.admin.form;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.trescal.cwms.core.admin.dto.DefaultPricingRemarkDto;

public class DefaultPricingRemarksForm {
	private Integer businessCompanyId;
	private List<DefaultPricingRemarkDto> calibrationRemarks;
	private List<DefaultPricingRemarkDto> repairRemarks;
	private List<DefaultPricingRemarkDto> adjustmentRemarks;
	private List<DefaultPricingRemarkDto> purchaseRemarks;

	@NotNull
	public Integer getBusinessCompanyId() {
		return businessCompanyId;
	}
	@Validated
	public List<DefaultPricingRemarkDto> getCalibrationRemarks() {
		return calibrationRemarks;
	}
	@Validated
	public List<DefaultPricingRemarkDto> getRepairRemarks() {
		return repairRemarks;
	}
	@Validated
	public List<DefaultPricingRemarkDto> getAdjustmentRemarks() {
		return adjustmentRemarks;
	}
	@Validated
	public List<DefaultPricingRemarkDto> getPurchaseRemarks() {
		return purchaseRemarks;
	}
	
	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}
	public void setCalibrationRemarks(List<DefaultPricingRemarkDto> calibrationRemarks) {
		this.calibrationRemarks = calibrationRemarks;
	}
	public void setRepairRemarks(List<DefaultPricingRemarkDto> repairRemarks) {
		this.repairRemarks = repairRemarks;
	}
	public void setAdjustmentRemarks(List<DefaultPricingRemarkDto> adjustmentRemarks) {
		this.adjustmentRemarks = adjustmentRemarks;
	}
	public void setPurchaseRemarks(List<DefaultPricingRemarkDto> purchaseRemarks) {
		this.purchaseRemarks = purchaseRemarks;
	}
}
