package org.trescal.cwms.core.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.db.AdvesoTransferableActivityService;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivityType;

@Controller
@IntranetController
public class ManageActivitiesController {

	@Autowired
	private AdvesoTransferableActivityService advesoTransferableActivityService;

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/manageactivities.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest request, @RequestParam(value = "id") List<Integer> ids,
			Model model) throws Exception {
		for (Integer id : ids) {
			AdvesoTransferableActivity ata = advesoTransferableActivityService.get(id);
			boolean istransReq = ServletRequestUtils.getBooleanParameter(request, "istrans_" + id, false);
			boolean isdispReq = ServletRequestUtils.getBooleanParameter(request, "isdisp_" + id, false);
			if (ata != null && (ata.getIsTransferredToAdveso() != istransReq
					|| ata.getIsDisplayedOnJobOrderDetails() != isdispReq)) {
				ata.setIsTransferredToAdveso(istransReq);
				ata.setIsDisplayedOnJobOrderDetails(isdispReq);
				advesoTransferableActivityService.update(ata);
			}
		}
		return new ModelAndView(new RedirectView("manageactivities.htm"));
	}

	@RequestMapping(value = "/manageactivities.htm", method = RequestMethod.GET)
	protected String formView(Model model) {
		init(model);
		return "trescal/core/admin/manageactivities";
	}

	private void init(Model model) {
		List<AdvesoTransferableActivity> transferableActivities = advesoTransferableActivityService.getAll();
		model.addAttribute("activities", transferableActivities);
		model.addAttribute("itemActivityType", ItemActivityType.values());
	}
}