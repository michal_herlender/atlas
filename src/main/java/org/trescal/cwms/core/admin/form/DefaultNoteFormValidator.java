package org.trescal.cwms.core.admin.form;

import java.util.Locale;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class DefaultNoteFormValidator extends AbstractBeanValidator {
	public static final String CODE_TOO_LONG = "error.field-length-too-long";
	public static final String ERROR_TOO_LONG = "The data entered was too long (max {0})";
	
	@Override
	public boolean supports(Class<?> clazz) {
		return DefaultNoteForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		DefaultNoteForm form = (DefaultNoteForm) target;
		for (Locale locale : form.getLabelTranslations().keySet()) {
			String label = form.getLabelTranslations().get(locale);
			if (label.length() > 50) errors.rejectValue("labelTranslations["+locale.toString()+"]", CODE_TOO_LONG, new Object[] {50}, ERROR_TOO_LONG);
		}
		for (Locale locale : form.getNoteTranslations().keySet()) {
			String note = form.getNoteTranslations().get(locale);
			if (note.length() > 5000) errors.rejectValue("noteTranslations["+locale.toString()+"]", CODE_TOO_LONG, new Object[] {5000}, ERROR_TOO_LONG);
		}
	}

}
