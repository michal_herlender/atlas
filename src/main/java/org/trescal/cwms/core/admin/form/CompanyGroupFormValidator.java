package org.trescal.cwms.core.admin.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.companygroup.db.CompanyGroupService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class CompanyGroupFormValidator extends AbstractBeanValidator {

	@Autowired
	private CompanyGroupService companyGroupService;
	
	@Override
	public boolean supports(Class<?> arg0) {
		return CompanyGroupForm.class.equals(arg0);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		CompanyGroupForm form = (CompanyGroupForm) target;
		
		// Only proceed with detailed validation unless the form has no basic errors (null values, disallowed sizes, etc...)
		if (!errors.hasErrors()) {
			Long duplicateCount = this.companyGroupService.countByName(form.getGroupName().trim(), form.getId()); 
			if (duplicateCount > 0)
				errors.rejectValue("groupName", null, "There is already a company group with this name");
		}

		
	}
}
