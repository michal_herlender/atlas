package org.trescal.cwms.core.admin.form;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UpgradeForm {
	public String action;
	private boolean upgradePerformed;
	private String upgradeResult;
	private Integer offset;
	private Integer batchSize;
}
