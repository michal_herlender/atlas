package org.trescal.cwms.core.admin.form;

import javax.validation.Valid;

import org.trescal.cwms.core.system.entity.printfilerule.PrintFileRule;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EditPrintFileRuleForm {

	@Valid
	private PrintFileRule pfr;
	private Integer printerId;

}