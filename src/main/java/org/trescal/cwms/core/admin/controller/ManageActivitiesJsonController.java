package org.trescal.cwms.core.admin.controller;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.db.AdvesoTransferableActivityService;
import org.trescal.cwms.core.workflow.dto.ItemStateDTO;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivityType;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;

@Controller
@IntranetController
public class ManageActivitiesJsonController {

	@Autowired
	private ItemStateService itemStateService;

	@Autowired
	private AdvesoTransferableActivityService advesoTransferableActivityService;

	@RequestMapping(value = "/deleteActivity.json", method = RequestMethod.GET)
	public @ResponseBody ResultWrapper delete(@RequestParam(value = "id", required = true) int id, Locale locale) {

		AdvesoTransferableActivity ata = advesoTransferableActivityService.get(id);
		advesoTransferableActivityService.delete(ata);

		return new ResultWrapper(true, "ok");
	}

	@RequestMapping(value = "/getActivities.json", method = RequestMethod.GET)
	public @ResponseBody List<ItemStateDTO> getActivities(@RequestParam(value = "type", required = true) String type,
			Locale locale) {

		List<Integer> exist_list = advesoTransferableActivityService.getAll().stream()
				.map(a -> a.getItemActivity().getStateid()).collect(Collectors.toList());
		if (ItemActivityType.HOLD_ACTIVITY.name().equals(type))
			return itemStateService.getHoldActivities(exist_list);
		else if (ItemActivityType.OVERRIDE_ACTIVITY.name().equals(type))
			return itemStateService.getOverrideActivities(exist_list);
		else if (ItemActivityType.PROGRESS_ACTIVITY.name().equals(type))
			return itemStateService.getProgressActivities(exist_list);
		else if (ItemActivityType.TRANSIT_ACTIVITY.name().equals(type))
			return itemStateService.getTransitActivities(exist_list);
		else if (ItemActivityType.WORK_ACTIVITY.name().equals(type))
			return itemStateService.getWorkActivities(exist_list);
		else
			return null;
	}

	@RequestMapping(value = "/addNewActivity.json", method = {RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody ResultWrapper addNewActivity(@RequestParam(value = "id", required = true) Integer id,
			@RequestParam(value = "istransferable", required = true) boolean istransferable,
			@RequestParam(value = "isdisplayed", required = true) boolean isdisplayed, Locale locale) {

		ItemActivity ia = (ItemActivity) itemStateService.findItemState(id);
		AdvesoTransferableActivity ata = new AdvesoTransferableActivity();
		ata.setItemActivity(ia);
		ata.setIsDisplayedOnJobOrderDetails(isdisplayed);
		ata.setIsTransferredToAdveso(istransferable);
		advesoTransferableActivityService.save(ata);

		ResultWrapper rw = new ResultWrapper();
		if (ata.getId() != 0) {
			rw.setMessage("new Activity added successfuly");
			rw.setSuccess(true);
		} else {
			rw.setMessage("error");
			rw.setSuccess(false);
		}
		return rw;
	}
}