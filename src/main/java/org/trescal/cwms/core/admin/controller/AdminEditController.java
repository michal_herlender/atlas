package org.trescal.cwms.core.admin.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.audit.entity.db.AuditTriggerSwitchService;
import org.trescal.cwms.core.company.entity.departmentrole.db.DepartmentRoleService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.userright.entity.role.db.RoleService;

@Controller @IntranetController
public class AdminEditController
{
	@Autowired
	private AuditTriggerSwitchService auditTriggerSwitchService; 
	@Autowired
	private DepartmentRoleService departmentRoleService;
	@Autowired
	private RoleService roleServ;
	
	@RequestMapping(value="/adminarea.htm")
	@PreAuthorize("hasAuthority('ADMIN_AREA')")
	protected ModelAndView referenceData() throws Exception
	{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("roles", this.departmentRoleService.getAllDepartmentRoles());
		map.put("userRoles", this.roleServ.getAll());
		map.put("auditTriggersEnabled", this.auditTriggerSwitchService.getEnabled());
		return new ModelAndView("trescal/core/admin/adminarea", map);
	}
	/*
	 * Simple mappings, no need for separate controllers
	 */
	@RequestMapping(value="/dymosandbox.htm")
	public String getDymoSandbox() {
		return "trescal/core/admin/dymosandbox";
	}
}