package org.trescal.cwms.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.scheduledtask.ScheduledTask;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;

/**
 * The controller for listing all {@link ScheduledTask}s.
 * 
 * @author jamiev
 */
@Controller @IntranetController
public class ViewScheduledTasksController
{
	@Autowired
	private ScheduledTaskService schTaskServ;
	@Value("${cwms.config.scheduledtask.globalactive}")
	private boolean globalActive;

	@RequestMapping(value="/viewscheduledtasks.htm")
	protected String handleRequestInternal(Model model) throws Exception
	{
		model.addAttribute("globalActive", this.globalActive);
		model.addAttribute("scheduledTasks", this.schTaskServ.getAllScheduledTasks());
		return "trescal/core/admin/viewscheduledtasks";
	}
}