package org.trescal.cwms.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.db.AlligatorLabelTemplateService;

@Controller @IntranetController
public class ViewLabelTemplateController {
	@Autowired
	private AlligatorLabelTemplateService labelTemplateService;
	
	@RequestMapping(path="viewlabeltemplate.htm", method={RequestMethod.GET})
	public String referenceData(Model model,
			@RequestParam(name="id", required=true) Integer id) {
		model.addAttribute("template", this.labelTemplateService.get(id));
		return "trescal/core/admin/labels/viewlabeltemplate";
	}	
}
