package org.trescal.cwms.core.admin.validator;


import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;

@Component
public class CourierValidator implements Validator
{
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(Courier.class);
	}

	public void validate(Object obj, Errors errors)
	{
		Courier courier = (Courier) obj;
		
		if (courier.getName() == null || courier.getName().length() < 1 || courier.getName().length() > 100)
		{
			errors.rejectValue("name", "name", null, "the name must be not null and length between 1 and 100");
		}

		if (courier.getWebTrackURL() != null && courier.getName().length() > 1000)
		{
			errors.rejectValue("webTrackURL", "webTrackURL", null, "the webTrackURL length must be less or equal than 1000");
		}
	}
}