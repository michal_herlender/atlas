package org.trescal.cwms.core.admin.dto;

import java.util.ArrayList;
import java.util.List;

import org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.TransportOptionScheduleRule;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditTransportOptionDTO {
	
	private Integer id;
	private Integer transportOptionId;
	private String localizedName;
	private String externalRef;
	private List<TransportOptionScheduleRule> scheduleRule;
	private Boolean active;
	
	public EditTransportOptionDTO() {
		super();
		this.scheduleRule = new ArrayList<TransportOptionScheduleRule>();
	}
	
}
