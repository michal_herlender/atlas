package org.trescal.cwms.core.admin.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;

@Controller @IntranetController
public class SupportedCurrencyController
{
	@Autowired
	SupportedCurrencyService currencyServ;

	@RequestMapping(value="/supportedcurrency.htm")
	protected ModelAndView handleRequestInternal() throws Exception
	{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("defaultcurrency", this.currencyServ.getDefaultCurrency());
		map.put("currencies", this.currencyServ.getAllSupportedCurrencys());
		return new ModelAndView("trescal/core/admin/supportedcurrency", "command", map);
	}
}