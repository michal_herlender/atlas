package org.trescal.cwms.core.admin.controller;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.admin.form.UpgradeForm;
import org.trescal.cwms.core.cdc.service.CdcRecoveryService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.login.entity.oauth2.clientdetails.db.PersistentClientDetailsService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME })
public class UpgradeController {

	@Autowired
	private CdcRecoveryService cdcRecoveryService;
	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private PersistentClientDetailsService pcdService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;

	public static final String FORM_NAME = "form";
	public static final String ACTION_NONE = "none";
	public static final String ACTION_CDC_RECOVERY_ABORT = "cdc_recovery_abort";
	public static final String ACTION_CDC_RECOVERY_COMMIT = "cdc_recovery_commit"; 
	public static final String ACTION_DEBUG_SESSION = "debug_session";
	public static final String ACTION_MODEL_COUNT = "model_count";
	public static final String ACTION_MODEL_TRANSLATIONS = "model_translations";
	public static final String ACTION_ENCODE_CLIENT_SECRETS = "encode_client_secrets";
	
	public static final int MAX_BATCH_SIZE = 5000;
	public static final int DEFAULT_BATCH_SIZE = 5000;
	
	private final static Logger logger = LoggerFactory.getLogger(UpgradeController.class);

	@ModelAttribute(FORM_NAME)
	protected UpgradeForm formBackingObject() {
		UpgradeForm form = new UpgradeForm();
		form.setAction("none");
		form.setUpgradePerformed(false);
		form.setUpgradeResult("");
		form.setOffset(0);
		form.setBatchSize(DEFAULT_BATCH_SIZE);
		return form;
	}

	@ModelAttribute("actions")
	Map<String, String> getActions() {
		Map<String, String> result = new TreeMap<>();
		result.put(ACTION_NONE, "-- None Selected --");
		result.put(ACTION_DEBUG_SESSION, "Turn on debug mode for current HttpSession - logout to exit");
		result.put(ACTION_MODEL_COUNT,
				"InstrumentModelService.getCount() - ignores inputs");
		result.put(ACTION_MODEL_TRANSLATIONS,
				"InstrumentModelService.updateAllInstrumentModelTranslations() - enter offset and batch size");
		result.put(ACTION_CDC_RECOVERY_ABORT,
				"CdcRecoveryService.performRecovery20210802(false) - ignores inputs - test first");
		result.put(ACTION_CDC_RECOVERY_COMMIT,
				"CdcRecoveryService.performRecovery20210802(true) - ignores inputs - run after test with abort");
		result.put(ACTION_ENCODE_CLIENT_SECRETS,
				"PersistentClientDetailsService.encodeClientSecrets() - ignores inputs");
		
		return result;
	}

	@RequestMapping(value = "/upgrade.htm", method = RequestMethod.GET)
	public String getUpgrade() {
		return "trescal/core/admin/upgrade";
	}

	@RequestMapping(value = "/upgrade.htm", method = RequestMethod.POST)
	public String doUpgrade(HttpSession session,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(FORM_NAME) UpgradeForm form, Locale locale) {
		logger.info("User " + username + " performing upgrade of type " + form.getAction());
		switch (form.getAction()) {
		case ACTION_DEBUG_SESSION:
			session.setAttribute(Constants.SESSION_ATTRIBUTE_SYSTEM_TYPE, "debug");
			form.setUpgradeResult("systemType : "+session.getAttribute(Constants.SESSION_ATTRIBUTE_SYSTEM_TYPE));
			break;		
		case ACTION_MODEL_COUNT: {
			long count = this.instrumentModelService.getCount();
			form.setUpgradePerformed(false);
			form.setUpgradeResult("Instrument Model Count : "+count);
			break;
		}
		case ACTION_MODEL_TRANSLATIONS: {
			if (form.getOffset() == null || form.getBatchSize() == null || form.getBatchSize() > MAX_BATCH_SIZE) {
				form.setUpgradePerformed(false);
				form.setUpgradeResult("Offset and batch size < "+MAX_BATCH_SIZE+" required");
			}
			else {
				long startTime = System.currentTimeMillis();
				List<Locale> locales = this.supportedLocaleService.getSupportedLocales();
				String result = this.instrumentModelService.updateAllInstrumentModelTranslations(locales, form.getOffset(), form.getBatchSize());
				long duration = System.currentTimeMillis() - startTime;
				form.setUpgradePerformed(true);
				form.setUpgradeResult("Result in "+duration+" ms : "+result);
			}
			break;
		}
		case ACTION_CDC_RECOVERY_ABORT: {
			long startTime = System.currentTimeMillis();
			String result = this.cdcRecoveryService.performRecovery20210802(false);
			long duration = System.currentTimeMillis() - startTime;
			form.setUpgradePerformed(false);
			form.setUpgradeResult("Result in "+duration+" ms : "+result);
			break;
		}
		case ACTION_CDC_RECOVERY_COMMIT : {
			long startTime = System.currentTimeMillis();
			String result = this.cdcRecoveryService.performRecovery20210802(true);
			long duration = System.currentTimeMillis() - startTime;
			form.setUpgradePerformed(true);
			form.setUpgradeResult("Result in "+duration+" ms : "+result);
			break;
		}
		case ACTION_ENCODE_CLIENT_SECRETS : {
			String result = this.pcdService.encodeClientSecrets();
			form.setUpgradePerformed(true);
			form.setUpgradeResult(result);
			break;
		}
		
		default: {
			// do nothing
			form.setUpgradePerformed(false);
			form.setUpgradeResult("No action selected");
			break;
		}
		}
		return "trescal/core/admin/upgrade";
	}
}