package org.trescal.cwms.core.admin.form;

import javax.validation.constraints.NotNull;

import org.springframework.util.AutoPopulatingList;
import org.trescal.cwms.core.admin.dto.EditTransportOptionDTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TransportOptionForm {
	@NotNull
	private Integer subdivid;
	private AutoPopulatingList<EditTransportOptionDTO> options;

	public TransportOptionForm() {
		this.options = new AutoPopulatingList<>(EditTransportOptionDTO.class);
	}
}
