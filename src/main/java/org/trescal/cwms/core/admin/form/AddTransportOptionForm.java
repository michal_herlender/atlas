package org.trescal.cwms.core.admin.form;

import java.util.ArrayList;
import java.util.List;

import org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.TransportOptionScheduleRule;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AddTransportOptionForm {
	
	private Integer subdivid;
	private Integer methodId;
	private Boolean active;
	private String localizedName;
	private String externalRef;
	private List<TransportOptionScheduleRule> scheduleRule;
	
	
	public AddTransportOptionForm() {
		super();
		this.scheduleRule = new ArrayList<TransportOptionScheduleRule>();
	}

}
