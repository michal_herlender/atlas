package org.trescal.cwms.core.admin.entity.db;

import org.trescal.cwms.core.admin.entity.BusinessEmails;
import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

public interface BusinessEmailsDao extends AllocatedDao<Subdiv, BusinessEmails, Integer> {}