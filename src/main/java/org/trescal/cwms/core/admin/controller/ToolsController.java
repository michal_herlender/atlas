package org.trescal.cwms.core.admin.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.labelprinter.db.LabelPrinterService;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class ToolsController 
{
	@Autowired
	private UserService userService;
	
	@Autowired
	private LabelPrinterService labelPrinterService; 
	
	@RequestMapping(value="/tools.htm")
	protected ModelAndView referenceData(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception
	{
		Contact contact = this.userService.get(username).getCon();
		
		Map<String,Object> map = new HashMap<>();
		List<Integer> labelCounts = new ArrayList<>();
		for (int i = 1; i <= 40; i++) {
			labelCounts.add(i);
		}
		map.put("labelCounts", labelCounts);
		map.put("labelPrinter", labelPrinterService.getDefaultLabelPrinterForContact(contact));
		
		return new ModelAndView("trescal/core/tools/tools", map);
	}
}