package org.trescal.cwms.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.admin.form.DefaultNoteForm;
import org.trescal.cwms.core.admin.form.DefaultNoteFormValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.defaultnote.DefaultNote;
import org.trescal.cwms.core.system.entity.defaultnote.db.DefaultNoteService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY})
public class EditDefaultNoteController {
    @Autowired
    private DefaultNoteService defaultNoteService;
    @Autowired
    private DefaultNoteFormValidator validator;
    @Autowired
    private SupportedLocaleService supportedLocaleService;
	
	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}	
	
	@ModelAttribute("form")
	public DefaultNoteForm formBackingObject(
			@RequestParam(name="id", required=false, defaultValue="0") Integer defaultNoteId) {
		DefaultNoteForm form = new DefaultNoteForm();
		if (defaultNoteId == 0) {
			form.setPublish(true);
			form.setComponent(Component.QUOTATION);
		}
		else {
			DefaultNote note = this.defaultNoteService.get(defaultNoteId);
			form.setComponent(note.getComponent().getComponent());
			form.setDefaultLabel(note.getLabel());
			form.setDefaultNote(note.getNote());
			form.setLabelTranslations(getMap(note.getLabelTranslation()));
			form.setNoteTranslations(getMap(note.getNoteTranslation()));
			form.setPublish(note.isPublish());
		}
		return form;
	}
	
	private Map<Locale,String> getMap(Set<Translation> translations) {
        return translations.stream().collect(Collectors.toMap(Translation::getLocale, Translation::getTranslation));
	}
	
	/**
	 * Only a few of the components support default notes
	 * @return list of components that support default notes
	 */
	private List<Component> getComponents() {
        return Stream.of(Component.CREDIT_NOTE,
                        Component.INVOICE,
                        Component.QUOTATION,
                        Component.PURCHASEORDER)
                .collect(Collectors.toList());
    }
	
	@RequestMapping(value="editdefaultnote.htm", method=RequestMethod.GET)
	public String referenceData(Model model) {
		model.addAttribute("additionalLocales", supportedLocaleService.getAdditionalLocales());
		model.addAttribute("primaryLocale", supportedLocaleService.getPrimaryLocale());
		model.addAttribute("components", getComponents());
		return "/trescal/core/admin/editdefaultnote";
	}
	
	@RequestMapping(value="editdefaultnote.htm", method=RequestMethod.POST)
	public String onSubmit(@Validated @ModelAttribute("form") DefaultNoteForm form,
			BindingResult bindingResult, Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer,String> companyDto,
			@RequestParam(name="id", required=false, defaultValue="0") Integer defaultNoteId) {
		if (bindingResult.hasErrors()) {
			return referenceData(model);
		}
		if (defaultNoteId == 0) {
			// New note
			defaultNoteService.createNote(companyDto.getKey(), form);
		}
		else {
			// Existing note
			defaultNoteService.updateNote(defaultNoteId, form);			
		}
		return "redirect:viewdefaultnotes.htm";
	}
}
