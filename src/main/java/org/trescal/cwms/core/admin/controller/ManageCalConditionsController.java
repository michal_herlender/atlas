package org.trescal.cwms.core.admin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.admin.form.CalibrationAdminForm;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.DefaultCalibrationCondition;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.db.DefaultCalibrationConditionService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class ManageCalConditionsController
{
	private final Logger logger = LoggerFactory.getLogger(ManageCalConditionsController.class);
	
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private DefaultCalibrationConditionService defCalConServ;
	
	@ModelAttribute("managecalconditions")
	protected CalibrationAdminForm formBackingObject(
			@RequestParam(value="id", required=false, defaultValue="0") Integer id,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) throws Exception
	{
		this.logger.info("Entering ManageCalConditionsController formBackingObject method ....");
		Company company = companyService.get(companyDto.getKey());
		CalibrationAdminForm calAdminForm = new CalibrationAdminForm();
		DefaultCalibrationCondition condition = this.defCalConServ.getLatest(id, company);
		if(condition == null) condition = new DefaultCalibrationCondition();
		calAdminForm.setCondition(condition);
		calAdminForm.setCalTypeId(id);
		return calAdminForm;
	}
	
	@RequestMapping(value="/managecalconditions.htm", method=RequestMethod.POST)
	protected RedirectView onSubmit(
			@ModelAttribute("managecalconditions") CalibrationAdminForm tcForm,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) throws Exception
	{
		this.logger.info("Entering ManageConditionsController ModelAndView method ....");
		Company company = companyService.get(companyDto.getKey());
		// set last update user and date
		DefaultCalibrationCondition condition = tcForm.getCondition();
		CalibrationType calType = calTypeServ.find(tcForm.getCalTypeId());
		condition.setCaltype(calType);
		condition.setOrganisation(company);
		// save into DB as new object
		this.defCalConServ.save(condition);
		return new RedirectView("managecalconditions.htm" + "?id=" + calType.getCalTypeId());
	}
	
	@RequestMapping(value="/managecalconditions.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(
			@RequestParam(value="id", required=false, defaultValue="0") Integer id,
			@ModelAttribute("managecalconditions") CalibrationAdminForm calAdminForm)
	{
		Map<String, Object> model = new HashMap<String, Object>();
		// load a list of caltypes to get the caltype defaults terms and
		// conditions
		List<CalibrationType> caltypes = this.calTypeServ.getCalTypes();
		if(id == 0 && caltypes.size() > 0) {
			return new ModelAndView(new RedirectView("managecalconditions.htm" + "?id=" + caltypes.get(0).getCalTypeId()));
		}
		else {
			model.put("caltypes", caltypes);
			return new ModelAndView("trescal/core/admin/managecalconditions", model);
		}
	}
}