package org.trescal.cwms.core.admin.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.admin.form.TransportOptionForm;
import org.trescal.cwms.core.admin.validator.TransportOptionFormValidator;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.db.TransportMethodService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller
@IntranetController
public class EditTransportOptionsController {
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private TransportOptionService transportOptionService;
	@Autowired
	private TransportMethodService transportMethodService;
	@Autowired
	private TransportOptionFormValidator validator;

	public static final String FORM_NAME = "form";

	@ModelAttribute(FORM_NAME)
	public TransportOptionForm formBackingObject(@RequestParam(name = "subdivid", required = true) int subdivid) {
		TransportOptionForm form = new TransportOptionForm();
		form.setSubdivid(subdivid);
		return form;
	}

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) throws Exception {
		binder.setValidator(validator);
	}

	@RequestMapping(path = "edittransportoptions.htm", method = RequestMethod.GET)
	public String referenceData(Model model, @ModelAttribute(FORM_NAME) TransportOptionForm form) {
		model.addAttribute("transportOptionsWithActiveMethod",
				this.transportOptionService.transportOptionsFromSubdivWithActiveMethod(form.getSubdivid()));
		model.addAttribute("transportOptionsFromSubdiv",
				this.transportOptionService.getTransportOptionsFromSubdiv(form.getSubdivid()));
		model.addAttribute("subdiv", this.subdivService.get(form.getSubdivid()));
		return "trescal/core/admin/edittransportoptions";
	}

	@RequestMapping(path = "edittransportoptions.htm", method = RequestMethod.POST)
	public String performUpdate(Model model, @Valid @ModelAttribute(FORM_NAME) TransportOptionForm form,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData(model, form);
		}

		this.transportOptionService.editTransportOptions(form);

		return "redirect:viewtransportoptions.htm?subdivid=" + form.getSubdivid();
	}

	@ResponseBody
	@RequestMapping(value = "/isOptionPerDayOfWeek.json", method = RequestMethod.GET)
	public Boolean isOptionPerDayOfWeek(
			@RequestParam(value = "transportMethodId", required = true) int transportMethodId) throws Exception {
		TransportMethod method = this.transportMethodService.findTransportMethod(transportMethodId);
		if (method != null) {
			return method.isOptionPerDayOfWeek();
		}
		return false;
	}

}
