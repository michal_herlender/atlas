/**
 * 
 */
package org.trescal.cwms.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRange;
import org.trescal.cwms.core.system.entity.markups.markuprange.db.MarkupRangeService;

/**
 * Controller used to delete a {@link MarkupRange}.
 * 
 * @author Richard
 */
@Controller @IntranetController
public class DeleteMarkupRangeController
{
	@Autowired
	private MarkupRangeService markupRangeServ;

	@RequestMapping(value="/deletemarkuprange.htm")
	protected RedirectView handleRequestInternal(@RequestParam(value="id", required=false, defaultValue="0") Integer id) throws Exception
	{
		if (id != 0) this.markupRangeServ.deleteMarkupRange(this.markupRangeServ.findMarkupRange(id));
		return new RedirectView("/viewmarkup.htm", true);
	}
}