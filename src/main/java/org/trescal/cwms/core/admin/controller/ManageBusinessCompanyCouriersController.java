package org.trescal.cwms.core.admin.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.deliverynote.entity.courier.db.CourierService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class ManageBusinessCompanyCouriersController {

	@Autowired
	private CourierService courierServ;
	
	@RequestMapping(value = "/managebusinesscompcouriers.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest request, @RequestParam(value = "id") List<Integer> ids,
			Model model) throws Exception {
		
		return new ModelAndView(new RedirectView("manageactivities.htm"));
	}

	@RequestMapping(value = "/managebusinesscompcouriers.htm", method = RequestMethod.GET)
	protected ModelAndView formView(@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("allocatedCompany", companyDto);
		model.put("couriers", this.courierServ.getCouriersByBusinessCompany(companyDto.getKey()));
		return new ModelAndView("trescal/core/admin/managebusinesscompanycouriers", model);
	}

}