package org.trescal.cwms.core.admin.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.admin.validator.CourierValidator;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courier.db.CourierService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * The controller for adding a new Courier.
 * 
 * @author JamieV
 */
@Controller @IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_COMPANY })
public class AddCourierController
{
	@Autowired
	private CourierService courierServ;
	@Autowired
	private CompanyService companyServ;
	@Autowired
	private CourierValidator validator;
	
	@ModelAttribute("obj")
	public Courier formBackingObject() throws Exception
	{
		return new Courier();
	}
	
	@InitBinder("obj")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	@RequestMapping(value="/addcourier.htm", method=RequestMethod.POST)
	public ModelAndView onSubmit(@Valid @ModelAttribute("obj") Courier c, BindingResult result, @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto, Model model) throws Exception
	{
		if (result.hasErrors()) return new ModelAndView(formView(model));
		else {
			Company comp = companyServ.get(companyDto.getKey());
			c.setOrganisation(comp);
			this.courierServ.insertCourier(c);
			return new ModelAndView(new RedirectView("viewcouriers.htm"));
		}
	}
	
	@RequestMapping(value="/addcourier.htm", method=RequestMethod.GET)
	public String formView(Model model) {
		return "trescal/core/admin/addcourier";
	}
}