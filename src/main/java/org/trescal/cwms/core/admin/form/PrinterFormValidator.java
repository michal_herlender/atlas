package org.trescal.cwms.core.admin.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class PrinterFormValidator extends AbstractBeanValidator {

	public static final String ERROR_CODE = "error.editprinter";
	public static final String ERROR_MESSAGE = "At least one printer tray must be added";
	
	@Override
	public boolean supports(Class<?> arg0) {
		return PrinterForm.class.equals(arg0);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		PrinterForm form = (PrinterForm) target;
		// If one list is null, all are expected to be null
		if ((form.getMediaTrayRefs() == null) || (form.getMediaTrayRefs().isEmpty())) {
			errors.reject(ERROR_CODE, ERROR_MESSAGE);
		}
		else if ((form.getPaperSources() == null) || (form.getPaperSources().isEmpty())) {
			errors.reject(ERROR_CODE, ERROR_MESSAGE);
		}
		else if ((form.getPaperTypes() == null) || (form.getPaperTypes().isEmpty())) {
			errors.reject(ERROR_CODE, ERROR_MESSAGE);
		}
	}

}
