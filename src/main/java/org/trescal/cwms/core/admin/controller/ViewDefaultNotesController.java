package org.trescal.cwms.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.defaultnote.DefaultNote;
import org.trescal.cwms.core.system.entity.defaultnote.db.DefaultNoteService;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.*;
import java.util.function.Function;

@Controller
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY})
public class ViewDefaultNotesController {
    @Autowired
    private DefaultNoteService defaultNoteService;
    @Autowired
    private SupportedLocaleService supportedLocaleService;

    @RequestMapping(value = "viewdefaultnotes.htm", method = RequestMethod.GET)
	public String referenceData(Model model, @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer,String> companyDto) {
        List<DefaultNote> notes = defaultNoteService.getDefaultNotesByComponent(null, companyDto.getKey());
        model.addAttribute("notes", notes);
        model.addAttribute("supportedLocales", supportedLocaleService.getSupportedLocales());
        model.addAttribute("labelTranslations", getTranslationMap(notes, DefaultNote::getLabelTranslation));
        model.addAttribute("noteTranslations", getTranslationMap(notes, DefaultNote::getNoteTranslation));

        return "/trescal/core/admin/viewdefaultnotes";
    }
	
	private Map<Integer,Map<Locale,String>> getTranslationMap(List<DefaultNote> notes, Function<DefaultNote, Set<Translation>> function) {
        Map<Integer, Map<Locale, String>> result = new HashMap<>();
        notes.forEach(note -> result.put(note.getDefaultNoteId(), new HashMap<>()));
        notes.forEach(
                note -> function.apply(note).forEach(
                        translation -> result.get(note.getDefaultNoteId()).
                                put(translation.getLocale(), translation.getTranslation())));
        return result;
    }
}
