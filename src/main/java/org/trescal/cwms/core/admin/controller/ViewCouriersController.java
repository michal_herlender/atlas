package org.trescal.cwms.core.admin.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courier.db.CourierService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * The controller for listing/editing details of all {@link Courier} entities.
 * 
 * @author jamiev
 */
@Controller @IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_COMPANY })
public class ViewCouriersController
{
	@Autowired
	private CourierService courierServ;

	@RequestMapping(value="/viewcouriers.htm")
	protected ModelAndView handleRequestInternal(@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) throws Exception
	{
		return new ModelAndView("trescal/core/admin/viewcouriers", "couriers", this.courierServ.getCouriersByBusinessCompany(companyDto.getKey()));
	}
}