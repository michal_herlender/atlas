package org.trescal.cwms.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.dto.ReverseTraceabilitySettingsDTO;
import org.trescal.cwms.core.jobs.calibration.entity.reversetraceability.db.ReverseTraceabilitySettingsService;
import org.trescal.cwms.core.jobs.calibration.form.ReverseTraceabilitySettingsForm;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class ManageReverseTraceabilityReportController {

	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ReverseTraceabilitySettingsService reverseTraceabilitySettingsService;

	public static final String VIEW_NAME = "trescal/core/admin/managereversetraceabilityreport";
	public static final String VIEW_ENABLE_RT_NAME = "trescal/core/admin/enablereversetraceabilityreport";
	public static final String FORM_NAME = "form";

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	@ModelAttribute(FORM_NAME)
	protected ReverseTraceabilitySettingsForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto)
			throws Exception {
		ReverseTraceabilitySettingsForm form = new ReverseTraceabilitySettingsForm();
		form.setBusinessCompanyId(companyDto.getKey());
		return form;
	}

	@RequestMapping(value = "/managereversetraceabilityreport.htm", method = RequestMethod.POST)
	public ModelAndView formView_Onsubmit(Model model, @ModelAttribute(FORM_NAME) ReverseTraceabilitySettingsForm form) {
		this.reverseTraceabilitySettingsService.editReverseTraceabilitySettings(form);
		return formView(model, form);
	}

	@RequestMapping(value = "/managereversetraceabilityreport.htm", method = RequestMethod.GET)
	protected ModelAndView formView(Model model, @ModelAttribute(FORM_NAME) ReverseTraceabilitySettingsForm form) {

		List<SubdivKeyValue> activeSubdivs = subdivService.getAllSubdivFromCompany(form.getBusinessCompanyId(), true);
		List<Integer> activeSubdivsIds = activeSubdivs.stream().map(KeyValue::getKey)
			.collect(Collectors.toList());
		List<ReverseTraceabilitySettingsDTO> reverseTraceabilitySettingsList = reverseTraceabilitySettingsService
			.getReverseTraceabilitySettingsFormSubdivIds(activeSubdivsIds);
		List<Integer> subdivIdsWithReverseTraceabilitySettings = reverseTraceabilitySettingsList.stream()
			.map(ReverseTraceabilitySettingsDTO::getSubdivid).collect(Collectors.toList());
		List<SubdivKeyValue> activeSubdivsNotAddedYet = activeSubdivs.stream()
			.filter(sub -> !subdivIdsWithReverseTraceabilitySettings.contains(sub.getKey()))
			.collect(Collectors.toList());
		model.addAttribute("reverseTraceabilitySettingsList", reverseTraceabilitySettingsList);
		model.addAttribute("activeSubdivsNotAddedYet", activeSubdivsNotAddedYet);
		return new ModelAndView(VIEW_NAME);
	}

	@RequestMapping(value = "/enablereversetraceabilitysettings.htm", method = RequestMethod.POST)
	public String enableReverseTraceabilitySettings_OnSubmit(
			@ModelAttribute(FORM_NAME) ReverseTraceabilitySettingsForm form, RedirectAttributes redirectAttributes) {
		this.reverseTraceabilitySettingsService
				.enableReverseTraceabilitySettings(form.getReverseTraceabilitySettingsDto());
		redirectAttributes.addFlashAttribute(FORM_NAME, form);
		return "redirect:managereversetraceabilityreport.htm";
	}

	@RequestMapping(value = "/enablereversetraceabilitysettings.htm", method = RequestMethod.GET)
	public ModelAndView enableReverseTraceabilitySettings(Model model,
			@ModelAttribute(FORM_NAME) ReverseTraceabilitySettingsForm form) {
		List<SubdivKeyValue> activeSubdivs = subdivService.getAllSubdivFromCompany(form.getBusinessCompanyId(), true);
		List<Integer> activeSubdivsIds = activeSubdivs.stream().map(KeyValue::getKey)
			.collect(Collectors.toList());
		List<ReverseTraceabilitySettingsDTO> reverseTraceabilitySettingsList = reverseTraceabilitySettingsService
			.getReverseTraceabilitySettingsFormSubdivIds(activeSubdivsIds);
		List<Integer> subdivIdsWithReverseTraceabilitySettings = reverseTraceabilitySettingsList.stream()
			.map(ReverseTraceabilitySettingsDTO::getSubdivid).collect(Collectors.toList());
		List<SubdivKeyValue> activeSubdivsNotAddedYet = activeSubdivs.stream()
			.filter(sub -> !subdivIdsWithReverseTraceabilitySettings.contains(sub.getKey()))
			.collect(Collectors.toList());
		model.addAttribute("activeSubdivsNotAddedYet", activeSubdivsNotAddedYet);
		return new ModelAndView(VIEW_ENABLE_RT_NAME);
	}

}
