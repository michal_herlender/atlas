package org.trescal.cwms.core.admin.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.deliverynote.entity.courier.db.CourierService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.db.CourierDespatchTypeService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_COMPANY })
public class ManageBusinessCompanyCourierJsonController {
	@Autowired
	private CourierService courierService; 
	@Autowired
	private CourierDespatchTypeService courierDespatchTypeService; 
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping(value="/updateDefaultCourierForCompany.json", method=RequestMethod.POST)
	@ResponseBody
	public Boolean updateDefaultCourierForCompany(@RequestParam(value="coid", required=true) Integer coid,
			@RequestParam(value="courierid", required=true) Integer courierid) {
		
		this.courierService.updateDefaultCompanyCourier(courierid, coid);
		return true;
	}
	
	@RequestMapping(value="/updateCourier.json", method=RequestMethod.POST)
	@ResponseBody
	public Boolean updateCourier(@RequestParam(value="courierid", required=true) Integer courierid,
			@RequestParam(value="name", required=true) String name,
			@RequestParam(value="webtrackurl", required=true) String webtrackurl) {
		
		this.courierService.editCourier(courierid, name, webtrackurl);
		return true;
	}
	
	@RequestMapping(value="/deleteCourier.json", method=RequestMethod.POST)
	@ResponseBody
	public ResultWrapper deleteCourier(@RequestParam(value="courierid", required=true) Integer courierid) {
		
		Long linkedEntitiesCount = this.courierService.linkedEntitiesCount(courierid);
		if(linkedEntitiesCount > 0) {
			String message = this.messageSource.getMessage("removecourier.errormessage", new Object[]{linkedEntitiesCount},
					"You can not delete this courier : "+linkedEntitiesCount+" items depend on it", LocaleContextHolder.getLocale());
			return new ResultWrapper(false, message);
		}
		this.courierService.deleteCourier(courierid);
		return new ResultWrapper(true, null);
	}
	
	@RequestMapping(value="/updateDefaultCourierDespatchTypeForCourier.json", method=RequestMethod.POST)
	@ResponseBody
	public Boolean updateDefaultCourierDespatchForCourier(@RequestParam(value="courierid", required=true) Integer courierid,
			@RequestParam(value="cdtid", required=true) Integer cdtid) {
		
		this.courierDespatchTypeService.updateDefaultCourierDespatchType(courierid, cdtid);
		return true;
	}
	
	@RequestMapping(value="/updateCourierDespatchType.json", method=RequestMethod.POST)
	@ResponseBody
	public ResultWrapper updateCourierDespatchType(@RequestParam(value="courierid", required=true) Integer courierid,
			@RequestParam(value="cdtid", required=true) Integer cdtid,
			@RequestParam(value="description", required=true) String description) {
		
		ResultWrapper response = this.courierDespatchTypeService.editType(cdtid, description);
		return response;
	}
	
	@RequestMapping(value="/deleteCourierDespatchType.json", method=RequestMethod.POST)
	@ResponseBody
	public ResultWrapper deleteCourierDespatchType(@RequestParam(value="courierid", required=true) Integer courierid,
			@RequestParam(value="cdtid", required=true) Integer cdtid) {
		
		Long linkedEntitiesCount = this.courierDespatchTypeService.linkedEntitiesCount(cdtid);
		if(linkedEntitiesCount > 0) {
			String message = this.messageSource.getMessage("removecourierdespatchtype.errormessage", new Object[]{linkedEntitiesCount},
					"You can not delete this courier despatch type : " + cdtid + " items depend on it", LocaleContextHolder.getLocale());
			return new ResultWrapper(false, message);
		}
		this.courierDespatchTypeService.deleteCourierDespatchType(cdtid);
		return new ResultWrapper(true, null);
	}
	
	@RequestMapping(value="/addNewCourier.json", method=RequestMethod.POST)
	@ResponseBody
	public ResultWrapper addNewCourier(@RequestParam(value="couriername", required=true) String couriername,
			@RequestParam(value="webtrackurl", required=true) String webtrackurl,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDt) {
		
		if(StringUtils.isNotBlank(couriername)) {
			int courierId = this.courierService.addNewCourier(couriername, webtrackurl, companyDt.getKey());
			return new ResultWrapper(true, String.valueOf(courierId));
		}
		else
			return new ResultWrapper(false, null);
	}
	
	
	@RequestMapping(value="/addNewCourierDespatchType.json", method=RequestMethod.POST)
	@ResponseBody
	public ResultWrapper addNewCourierDespatchType(@RequestParam(value="description", required=true) String description,
			@RequestParam(value="courierid", required=true) Integer courierid) {
		
		ResultWrapper response = this.courierDespatchTypeService.newDespatchTypeForCourier(courierid, description);
		return response;
	}
}
