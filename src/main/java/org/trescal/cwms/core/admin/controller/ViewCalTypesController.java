package org.trescal.cwms.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;

/**
 * The controller for listing all {@link CalibrationType} entities.
 * 
 * @author jamiev
 */
@Controller @IntranetController
public class ViewCalTypesController
{
	@Autowired
	private CalibrationTypeService calTypes;

	/*
	 * Overriding handleRequestInternal method creating a View listing all
	 * Calibration Types.
	 */
	@RequestMapping(value="/viewcaltypes.htm")
	protected ModelAndView handleRequestInternal() throws Exception
	{
		return new ModelAndView("trescal/core/admin/viewcaltypes", "calTypes", this.calTypes.getCalTypes());
	}
}