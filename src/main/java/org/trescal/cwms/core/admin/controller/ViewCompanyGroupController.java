package org.trescal.cwms.core.admin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.trescal.cwms.core.company.entity.companygroup.db.CompanyGroupService;
import org.trescal.cwms.core.company.entity.companygroup.dto.CompanyGroupUsageDTO;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller 
@IntranetController
public class ViewCompanyGroupController {
	
	@Autowired
	CompanyGroupService companygroupService;
	
	@RequestMapping(path="viewcompanygroups.htm", method=RequestMethod.GET)
	protected String handleRequestInternal(Model model) {
		List<CompanyGroupUsageDTO> companyGroups = this.companygroupService.getUsageDTOList();
		model.addAttribute("companyGroups", companyGroups);
		
		return "/trescal/core/admin/viewcompanygroups";
	}
}


