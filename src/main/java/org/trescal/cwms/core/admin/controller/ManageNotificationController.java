package org.trescal.cwms.core.admin.controller;

import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.external.adveso.dto.AdvesoNotificationDTO;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemmodel.db.NotificationSystemModelService;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemEntityClassEnum;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemOperationTypeEnum;
import org.trescal.cwms.core.external.adveso.form.SearchNotificationForm;
import org.trescal.cwms.core.external.adveso.form.SearchNotificationFormValidator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.adveso.service.RestClientAdvesoQueueService;

@Controller
@IntranetController
public class ManageNotificationController {
	@Autowired
	private NotificationSystemModelService notificationSystemModelService;

	@Autowired
	private RestClientAdvesoQueueService restClientAdvesoQueueService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private SearchNotificationFormValidator searchNotificationFormValidator;

	public static final String VIEW_NAME = "trescal/core/admin/sendnotifications";
	public static final String FORM_NAME = "form";

	@InitBinder(FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(searchNotificationFormValidator);
	}

	@ModelAttribute(FORM_NAME)
	public SearchNotificationForm form(@ModelAttribute(FORM_NAME) SearchNotificationForm form,
			@RequestParam(name = "pageNo", required = false, defaultValue = "1") Integer pageNo) {
		if (form == null)
			form = new SearchNotificationForm();
		PagedResultSet<AdvesoNotificationDTO> rs = new PagedResultSet<>(0, 0);
		rs.setCurrentPage(pageNo);
		form.setRs(rs);
		notificationSystemModelService.getNotificationsToResent(form);
		return form;
	}

	@RequestMapping(value = "/managenotifications.json", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	protected List<AdvesoNotificationDTO> onSubmitSearchForm(
			@RequestParam(name = "pageNo", required = false, defaultValue = "1") Integer pageNo,
			@Valid @ModelAttribute(FORM_NAME) SearchNotificationForm form, BindingResult bindingResult)
			throws Exception {
		if (bindingResult.hasErrors()) {
			return (List<AdvesoNotificationDTO>) form.getRs().getResults();
		}

		for (AdvesoNotificationDTO anotif : form.getRs().getResults()) {
			JobItem ji = notificationSystemModelService.getNotificationJobItem(anotif.getId());
			if (ji != null){
				anotif.setJobItemId(ji.getJobItemId());
				anotif.setJobItemNo(ji.getItemNo());
				anotif.setJobNo(ji.getJob().getJobno());
			}
		}
		return (List<AdvesoNotificationDTO>) form.getRs().getResults();
	}

	@RequestMapping(value = "/resendnotification.json", method = RequestMethod.POST)
	protected ModelAndView resendEvent(
			@RequestParam(name = "pageNo", required = false, defaultValue = "1") Integer pageNo,
			@RequestParam(name = "idsNotification", required = true) List<Integer> idsNotification,
			@Valid @ModelAttribute(FORM_NAME) SearchNotificationForm form, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) throws Exception {

		if (bindingResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("form", form);
			redirectAttributes.addFlashAttribute("pageNo", pageNo);
		}

		if (!idsNotification.isEmpty()) {
			restClientAdvesoQueueService.sendNotifications(idsNotification);
		}

		redirectAttributes.addFlashAttribute("form", form);
		redirectAttributes.addFlashAttribute("pageNo", pageNo);
		return new ModelAndView(new RedirectView("/managenotifications.json", true));
	}

	@RequestMapping(value = "/sendnotifications.htm", method = { RequestMethod.GET, RequestMethod.POST })
	protected String formView(@RequestParam(name = "coid", required = false) Integer coid,
			@Valid @ModelAttribute(FORM_NAME) SearchNotificationForm form, BindingResult bindingResult, Model model)
			throws ParseException {

		if (bindingResult.hasErrors()) {
			return VIEW_NAME;
		}

		if (coid != null) {
			Company comp = companyService.get(coid);
			form.setCoid(comp.getCoid());
			form.setConame(comp.getConame());
		}
		
		for (AdvesoNotificationDTO anotif : form.getRs().getResults()) {
			JobItem ji = notificationSystemModelService.getNotificationJobItem(anotif.getId());
			if (ji != null){
				anotif.setJobItemId(ji.getJobItemId());
				anotif.setJobItemNo(ji.getItemNo());
				anotif.setJobNo(ji.getJob().getJobno());
			}
		}

		model.addAttribute("notifications", form.getRs().getResults());
		model.addAttribute("entitiesClass", NotificationSystemEntityClassEnum.values());
		model.addAttribute("operationsType", NotificationSystemOperationTypeEnum.values());
		model.addAttribute("rs", form.getRs());
		return VIEW_NAME;
	}
}