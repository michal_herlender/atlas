package org.trescal.cwms.core.admin.dto;

import java.util.List;

import org.trescal.cwms.core.documents.images.labelimage.LabelImage;

public class LabelImageEntityDto {
	private List<LabelImage> images;

	public List<LabelImage> getImages() {
		return images;
	}

	public void setImages(List<LabelImage> images) {
		this.images = images;
	}
}
