package org.trescal.cwms.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.admin.form.CompanyGroupForm;
import org.trescal.cwms.core.admin.form.CompanyGroupFormValidator;
import org.trescal.cwms.core.company.entity.companygroup.CompanyGroup;
import org.trescal.cwms.core.company.entity.companygroup.db.CompanyGroupService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
public class EditCompanyGroupController 
{
	public static final String FORM_NAME = "form";
	
	@Autowired
	private CompanyGroupService companyGroupService;
	
	@Autowired
	private CompanyGroupFormValidator validator; 
	
	@InitBinder(FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	@ModelAttribute(FORM_NAME)
	private CompanyGroupForm formBackingObject (
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@RequestParam(value="companygroupid", required=false, defaultValue="0") Integer companyGroupId
			)
	{
		CompanyGroupForm form = new CompanyGroupForm();
		if (companyGroupId != 0) {
			CompanyGroup companyGroup = companyGroupService.get(companyGroupId);
			
			form.setId(companyGroupId);
			form.setGroupName(companyGroup.getGroupName());
			form.setActive(companyGroup.getActive());
		}
		else {
			form.setId(0);
			form.setActive(true);
		}
		return form;
	}

	@RequestMapping(value="/editcompanygroup.htm", method=RequestMethod.POST)
	protected String onSubmit(
			@Validated @ModelAttribute(FORM_NAME) CompanyGroupForm form, BindingResult bindingResult)
	{
		if (bindingResult.hasErrors()) {
			return referenceData();
		}

		if (form.getId() == 0) {
			this.companyGroupService.createCompanyGroup(form);
		}
		else {
			this.companyGroupService.editCompanyGroup(form);
		}
		return "redirect:viewcompanygroups.htm";
	}
	
	
	@RequestMapping(value="/editcompanygroup.htm", method=RequestMethod.GET)
	protected String referenceData()
	{
		return "trescal/core/admin/editcompanygroup";
	}
	
}


