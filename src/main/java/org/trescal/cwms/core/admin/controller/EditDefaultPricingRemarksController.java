package org.trescal.cwms.core.admin.controller;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.admin.dto.DefaultPricingRemarkDto;
import org.trescal.cwms.core.admin.form.DefaultPricingRemarksForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.pricingremark.DefaultPricingRemark;
import org.trescal.cwms.core.pricing.entity.pricingremark.PricingRemarkType;
import org.trescal.cwms.core.pricing.entity.pricingremark.db.DefaultPricingRemarkService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class EditDefaultPricingRemarksController {
	
	@Autowired
	private DefaultPricingRemarkService dprService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	
	@ModelAttribute("form")
	public DefaultPricingRemarksForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer,String> companyDto) {
		
		List<DefaultPricingRemark> remarks = this.dprService.getRemarksForOrganisation(companyDto.getKey(), null);

		DefaultPricingRemarksForm form = new DefaultPricingRemarksForm();
		form.setAdjustmentRemarks(getRemarkDtoList(remarks, CostType.ADJUSTMENT));
		form.setCalibrationRemarks(getRemarkDtoList(remarks, CostType.CALIBRATION));
		form.setPurchaseRemarks(getRemarkDtoList(remarks, CostType.PURCHASE));
		form.setRepairRemarks(getRemarkDtoList(remarks, CostType.REPAIR));
		
		return form;
	}
	
	public List<DefaultPricingRemarkDto> getRemarkDtoList(List<DefaultPricingRemark> remarks, CostType costType) {
		List<DefaultPricingRemarkDto> results = new ArrayList<>();
		
		List<Locale> locales = this.supportedLocaleService.getSupportedLocales();
		EnumSet<PricingRemarkType> remarkTypes = EnumSet.allOf(PricingRemarkType.class);
		
		for (PricingRemarkType remarkType : remarkTypes) {
			for (Locale locale : locales) {
				DefaultPricingRemarkDto dto = new DefaultPricingRemarkDto();
				dto.setLocale(locale);
				dto.setRemarkType(remarkType);
				DefaultPricingRemark existingRemark = findRemark(remarks, costType, remarkType, locale);
				
				if (existingRemark != null) {
					dto.setId(existingRemark.getId());
					dto.setRemark(existingRemark.getRemark());
				}
				else {
					dto.setId(0);
				}
			}
		}		
		return results;
	}
	
	public DefaultPricingRemark findRemark(List<DefaultPricingRemark> remarks, CostType costType,
			PricingRemarkType remarkType, Locale locale) {
		DefaultPricingRemark result = remarks.stream()
				.filter(remark -> remark.getCostType().equals(costType))
				.filter(remark -> remark.getRemarkType().equals(remarkType))
				.filter(remark -> remark.getLocale().equals(locale))
				.findFirst().orElse(null);
		return result;
	}
	
	@RequestMapping(value="editdefaultpricingremarks.htm", method=RequestMethod.GET)
	public String referenceData() {
		return "trescal/core/admin/editdefaultpricingremarks";
	}
	
	@RequestMapping(value="editdefaultpricingremarks.htm", method=RequestMethod.POST)
	public String doPost(@Validated DefaultPricingRemarksForm form, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData();
		}
		this.dprService.perFormEdit(form);
		return "trescal/core/admin/editdefaultpricingremarks";
	}
}
