package org.trescal.cwms.core.admin.entity.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.admin.entity.BusinessEmails;
import org.trescal.cwms.core.audit.entity.db.AllocatedToSubdivDaoImpl;

@Repository
public class BusinessEmailsDaoImpl extends AllocatedToSubdivDaoImpl<BusinessEmails, Integer> implements BusinessEmailsDao
{
	@Override
	protected Class<BusinessEmails> getEntity() {
		return BusinessEmails.class;
	}
}