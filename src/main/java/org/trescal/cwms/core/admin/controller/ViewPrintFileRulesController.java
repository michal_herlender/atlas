package org.trescal.cwms.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.printfilerule.PrintFileRule;
import org.trescal.cwms.core.system.entity.printfilerule.db.PrintFileRuleService;

/**
 * The controller for listing all {@link PrintFileRule}s.
 * 
 * @author jamiev
 */
@Controller @IntranetController
public class ViewPrintFileRulesController
{
	@Autowired
	private PrintFileRuleService pfrServ;

	@RequestMapping(value="/viewprintfilerules.htm")
	protected ModelAndView handleRequestInternal() throws Exception
	{
		return new ModelAndView("trescal/core/admin/viewprintfilerules", "rules", this.pfrServ.getAllPrintFileRules());
	}
}