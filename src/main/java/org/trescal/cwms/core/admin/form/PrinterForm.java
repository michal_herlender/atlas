package org.trescal.cwms.core.admin.form;

import java.util.List;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotBlank;
import org.trescal.cwms.core.system.entity.papertype.PaperType;
import org.trescal.cwms.core.system.entity.printertray.MediaTrayRef;

public class PrinterForm
{
	private Integer printerId;
	private String path;
	private String description;
	private Integer addrId;
	private List<Integer> trayIds;
	private List<MediaTrayRef> mediaTrayRefs;
	private List<Integer> paperSources;
	private List<PaperType> paperTypes;

	public static final String ERROR_CODE_VALUE_NOT_SELECTED = "{error.value.notselected}"; 

	@Min(value=1, message=ERROR_CODE_VALUE_NOT_SELECTED)
	public Integer getAddrId()
	{
		return this.addrId;
	}

	public List<MediaTrayRef> getMediaTrayRefs()
	{
		return this.mediaTrayRefs;
	}

	public List<Integer> getPaperSources()
	{
		return this.paperSources;
	}

	public List<PaperType> getPaperTypes()
	{
		return this.paperTypes;
	}

	@NotBlank
	public String getPath() {
		return path;
	}

	@NotBlank
	public String getDescription() {
		return description;
	}

	public void setAddrId(Integer addrId)
	{
		this.addrId = addrId;
	}

	public void setMediaTrayRefs(List<MediaTrayRef> mediaTrayRefs)
	{
		this.mediaTrayRefs = mediaTrayRefs;
	}

	public void setPaperSources(List<Integer> paperSources)
	{
		this.paperSources = paperSources;
	}

	public void setPaperTypes(List<PaperType> paperTypes)
	{
		this.paperTypes = paperTypes;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Integer> getTrayIds() {
		return trayIds;
	}

	public void setTrayIndices(List<Integer> trayIds) {
		this.trayIds = trayIds;
	}

	public Integer getPrinterId() {
		return printerId;
	}

	public void setPrinterId(Integer printerId) {
		this.printerId = printerId;
	}

}
