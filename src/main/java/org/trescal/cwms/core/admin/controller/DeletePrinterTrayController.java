package org.trescal.cwms.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.printer.Printer;
import org.trescal.cwms.core.system.entity.printertray.PrinterTray;
import org.trescal.cwms.core.system.entity.printertray.db.PrinterTrayService;

@Controller @IntranetController
public class DeletePrinterTrayController {
	@Autowired
	private PrinterTrayService printerTrayService; 
	
	public static final String REQUEST_URL= "deleteprintertray.htm";
	public static final String REDIRECT_URL= "editprinter.htm?printerid=";
	
	/**
	 * To be deletable the tray must be the last tray in the printer, and there must be at least 2 trays.
	 * This provides the ability to delete printer trays while keeping the implentation simple. 
	 * The delete link should only be displayed in the UI if deletable, but we do a basic check here to ensure 
	 * data consistency.  These messages are not expected to appear in normal use so are not internationalized. 
	 */
	@RequestMapping(value=REQUEST_URL)
	public RedirectView deletePrinterTray(@RequestParam(name="trayid", required=true) Integer trayid) throws Exception {
		PrinterTray printerTray = this.printerTrayService.findPrinterTray(trayid);
		if (printerTray.getTrayNo() == 1) throw new Exception("The first tray is not deletable!");
		if (printerTray.getTrayNo() != printerTray.getPrinter().getTrays().size()) throw new Exception("Only the last tray is deletable!");
		
		Printer printer = printerTray.getPrinter();
		printer.getTrays().remove(printerTray);
		this.printerTrayService.deletePrinterTray(printerTray);
		
		return new RedirectView(REDIRECT_URL+printer.getId(), true); 
	}
}
