package org.trescal.cwms.core.admin.entity.db;

import org.trescal.cwms.core.admin.entity.BusinessEmails;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

public interface BusinessEmailsService extends BaseService<BusinessEmails, Integer>
{
	BusinessEmails getBySubdiv(Subdiv subdiv);
}