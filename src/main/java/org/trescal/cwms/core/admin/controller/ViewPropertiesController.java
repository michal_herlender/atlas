package org.trescal.cwms.core.admin.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.referential.SynchronizeReferentialIf;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.pathservice.PathService;

@Controller @IntranetController
public class ViewPropertiesController {
	
	@Autowired
	@Qualifier("props")
	private Properties atlasProperties;
	
	@Autowired
	private PathService pathService;
	
	@Autowired
	private SynchronizeReferentialIf synchronizeReferential;

	// We just allow some key properties for debugging (without exposing passwords, etc...) here
	private List<String> getAllowedProperties() {
		List<String> result = new ArrayList<>();
		result.add("mail.server");
		result.add("cwms.config.scheduledtask.globalactive");
		result.add("cwms.config.filepathresolution");
		result.add("cwms.config.fileserver.root");
		result.add("cwms.recall.sender.sendemail");
		return result;
	}
	
	private Map<String, String> getPropertyMap() {
		Map<String, String> result = new TreeMap<>();
		for (String key : getAllowedProperties()) {
			result.put(key, atlasProperties.getProperty(key));
		}
		return result;
	}
	
	private Map<String, Object> getFileServerTests() {
		File rootPath = new File(this.pathService.getFileServerRootPath());
		
		Map<String, Object> result = new TreeMap<>();
		result.put("rootPath.getPath()",rootPath.getPath());
		result.put("rootPath.exists()",rootPath.exists());
		result.put("rootPath.canWrite()",rootPath.canWrite());
		result.put("rootPath.canRead()",rootPath.canRead());
		result.put("rootPath.isAbsolute()",rootPath.isAbsolute());
		result.put("rootPath.isFile()",rootPath.isFile());
		result.put("rootPath.isDirectory()",rootPath.isDirectory());
		
		return result;
	}
	
	private Map<String, Object> getOtherMap(HttpSession session) {
		Map<String, Object> result = new TreeMap<>();
		result.put(Constants.SESSION_ATTRIBUTE_TIME_ZONE, session.getAttribute(Constants.SESSION_ATTRIBUTE_TIME_ZONE));
		result.put("TimeZone.getDefault()", TimeZone.getDefault());
		return result;
	}
	
	@RequestMapping(value="viewproperties.htm", method=RequestMethod.GET)
	public String referenceData(Model model, HttpSession session) {
		model.addAttribute("atlasProperties", getPropertyMap());
		model.addAttribute("otherValues", getOtherMap(session));
		model.addAttribute("fileServerTests", getFileServerTests());
		return "trescal/core/admin/viewproperties";
	}

	/**
	 * Can be repurposed as needed
	 * @param model
	 * @return
	 */
	@RequestMapping(value="viewproperties.htm", method=RequestMethod.POST)
	public String performTest(Model model, HttpSession session) {
		String result = this.synchronizeReferential.testConnection();
		model.addAttribute("testResult", result);
		
		return referenceData(model, session);
	}

}