package org.trescal.cwms.core.admin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.admin.dto.LabelImageEntityDto;
import org.trescal.cwms.core.documents.images.image.ImageType;
import org.trescal.cwms.core.documents.images.image.db.ImageService;
import org.trescal.cwms.core.documents.images.labelimage.LabelImage;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY})
public class ViewLabelImages {
	
	@Autowired
	private ImageService imageService;
	
	@RequestMapping("viewlabelimages.htm")
	public String referenceData(Model model, 
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer,String> companyDto) {
		List<LabelImage> images = this.imageService.getAllLabelImages();
		LabelImageEntityDto dto = new LabelImageEntityDto();
		dto.setImages(images);
		model.addAttribute("entity", dto);
		model.addAttribute("imageType", ImageType.LABEL);
		model.addAttribute("entityId", companyDto.getKey());
		return "trescal/core/admin/viewlabelimages";
	}
}
