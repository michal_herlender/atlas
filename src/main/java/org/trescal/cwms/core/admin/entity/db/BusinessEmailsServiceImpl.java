package org.trescal.cwms.core.admin.entity.db;

import java.util.List;

import javax.validation.ConstraintDefinitionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.admin.entity.BusinessEmails;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

@Service
public class BusinessEmailsServiceImpl extends BaseServiceImpl<BusinessEmails, Integer> implements BusinessEmailsService
{
	@Autowired
	private BusinessEmailsDao businessEmailsDao;
	
	@Override
	protected BaseDao<BusinessEmails, Integer> getBaseDao() {
		return businessEmailsDao;
	}
	
	@Override
	public BusinessEmails getBySubdiv(Subdiv subdiv) {
		List<BusinessEmails> businessEmailsList = businessEmailsDao.getAllBySubdiv(subdiv);
		if(businessEmailsList == null || businessEmailsList.isEmpty()) return null;
		else if(businessEmailsList.size() == 1) return businessEmailsList.get(0);
		else throw new ConstraintDefinitionException();
	}
}