package org.trescal.cwms.core.admin.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.validator.constraints.Email;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

@Entity
@Table(name="businessemails", uniqueConstraints=@UniqueConstraint(columnNames="orgid"))
public class BusinessEmails extends Allocated<Subdiv> {

	private Integer id;
	private String account;
	private String autoService;
	private String collections;
	private String goodsIn;
	private String quality;
	private String quotations;
	private String recall;
	private String sales;
	private String web;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Email
	public String getAccount() {
		return account;
	}
	
	public void setAccount(String account) {
		this.account = account;
	}
	
	@Email
	public String getAutoService() {
		return autoService;
	}
	
	public void setAutoService(String autoService) {
		this.autoService = autoService;
	}
	
	@Email
	public String getCollections() {
		return collections;
	}
	
	public void setCollections(String collections) {
		this.collections = collections;
	}
	
	@Email
	public String getGoodsIn() {
		return goodsIn;
	}
	
	public void setGoodsIn(String goodsIn) {
		this.goodsIn = goodsIn;
	}
	
	@Email
	public String getQuality() {
		return quality;
	}
	
	public void setQuality(String quality) {
		this.quality = quality;
	}
	
	@Email
	public String getQuotations() {
		return quotations;
	}
	
	public void setQuotations(String quotations) {
		this.quotations = quotations;
	}
	
	@Email
	public String getRecall() {
		return recall;
	}
	
	public void setRecall(String recall) {
		this.recall = recall;
	}
	
	@Email
	public String getSales() {
		return sales;
	}
	
	public void setSales(String sales) {
		this.sales = sales;
	}
	
	@Email
	public String getWeb() {
		return web;
	}
	
	public void setWeb(String web) {
		this.web = web;
	}
}