package org.trescal.cwms.core.admin.form;

import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.DefaultCalibrationCondition;

public class CalibrationAdminForm
{
	private DefaultCalibrationCondition condition;
	private Integer calTypeId;
	
	public DefaultCalibrationCondition getCondition() {
		return this.condition;
	}
	
	public Integer getCalTypeId() {
		return calTypeId;
	}
	
	public void setCondition(DefaultCalibrationCondition condition) {
		this.condition = condition;
	}
	
	public void setCalTypeId(Integer calTypeId) {
		this.calTypeId = calTypeId;
	}
}