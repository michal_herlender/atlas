package org.trescal.cwms.core.admin.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general.DefaultGeneralCondition;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general.db.DefaultGeneralConditionService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class ManageConditionsController {
	private final Logger logger = LoggerFactory.getLogger(ManageConditionsController.class);

	@Autowired
	private CompanyService companyService;
	@Autowired
	private DefaultGeneralConditionService defGenConServ;

	@ModelAttribute("manageconditions")
	protected DefaultGeneralCondition formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto)
			throws Exception {
		this.logger.info("Entering ManageConditionsController formBackingObject method ....");
		Company allocatedCompany = companyService.get(companyDto.getKey());
		DefaultGeneralCondition cond = this.defGenConServ.getLatest(allocatedCompany);
		if (cond == null) {
			cond = new DefaultGeneralCondition();
			cond.setOrganisation(allocatedCompany);
			cond.setConditionText("");
		}
		return cond;
	}

	@RequestMapping(value = "/manageconditions.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(@Valid @ModelAttribute("manageconditions") DefaultGeneralCondition condition,
			BindingResult result) throws Exception {
		if (result.hasErrors())
			return new ModelAndView(formView());
		else {
			this.logger.info("Entering ManageConditionsController ModelAndView method ....");
			// save into database as new object
			this.defGenConServ.save(condition);
			return new ModelAndView(new RedirectView("manageconditions.htm"));
		}
	}

	@RequestMapping(value = "/manageconditions.htm", method = RequestMethod.GET)
	protected String formView() {
		return "trescal/core/admin/manageconditions";
	}
}