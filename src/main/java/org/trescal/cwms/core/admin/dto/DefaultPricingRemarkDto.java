package org.trescal.cwms.core.admin.dto;

import java.util.Locale;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.pricing.entity.pricingremark.PricingRemarkType;

public class DefaultPricingRemarkDto {
	private Integer id;
	private Locale locale;
	private PricingRemarkType remarkType;
	private String remark;
	
	/**
	 * If 0, then is a potentially new remark, otherwise existing
	 * @return
	 */
	@NotNull
	public Integer getId() {
		return id;
	}
	@NotNull
	public Locale getLocale() {
		return locale;
	}
	@NotNull
	public PricingRemarkType getRemarkType() {
		return remarkType;
	}
	@NotNull
	@Length(max=255)
	public String getRemark() {
		return remark;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	public void setRemarkType(PricingRemarkType remarkType) {
		this.remarkType = remarkType;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}