package org.trescal.cwms.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.db.AlligatorLabelTemplateService;

@Controller @IntranetController
public class ViewLabelTemplatesController {
	@Autowired
	private AlligatorLabelTemplateService labelTemplateService;
	
	@RequestMapping(path="viewlabeltemplates.htm", method={RequestMethod.GET})
	public String referenceData(Model model) {
		model.addAttribute("templates", this.labelTemplateService.getAll());
		return "trescal/core/admin/labels/viewlabeltemplates";
	}
	
}
