/**
 * 
 */
package org.trescal.cwms.core.admin.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRange;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRangeAction;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRangeComparator;
import org.trescal.cwms.core.system.entity.markups.markuprange.db.MarkupRangeService;
import org.trescal.cwms.core.system.entity.markups.markuptype.MarkupType;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * View Controller for displaying all application markups. Page displays
 * functionality to delete a {@link MarkupRange} and links to edit a
 * {@link MarkupRange}.
 * 
 * @author Richard
 */
@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class ViewMarkupController
{
	@Autowired
	private MarkupRangeService markupRangeServ;
	@Autowired
	private CompanyService companyService;
	
	@RequestMapping(value="/viewmarkup.htm")
	protected String handleRequestInternal(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer,String> companyDto) throws Exception
	{
		Company allocatedCompany = this.companyService.get(companyDto.getKey());
		
		HashMap<Integer, List<MarkupRange>> markupTypes = new HashMap<Integer, List<MarkupRange>>();
		for (MarkupType mt: MarkupType.values()) {
			//Get MarkupRange for each MarkUpType
			List<MarkupRange> lmr = markupRangeServ.getAllMarkupRanges(mt);
			Collections.sort(lmr,new MarkupRangeComparator());
			
			markupTypes.put(Integer.valueOf(mt.getId()), lmr);
		}
		model.addAttribute("markupRanges", markupTypes);
		model.addAttribute("markupTypes", MarkupType.values());
		model.addAttribute("rangeActions", MarkupRangeAction.values());
		model.addAttribute("currency", allocatedCompany.getCurrency());
		
		return "trescal/core/admin/viewmarkup";
	}
}