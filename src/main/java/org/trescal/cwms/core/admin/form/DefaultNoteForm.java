package org.trescal.cwms.core.admin.form;

import java.util.Locale;
import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

public class DefaultNoteForm {
	private Boolean publish;
	private Component component;
	private String defaultLabel;
	private String defaultNote;
	private Map<Locale,String> labelTranslations;
	private Map<Locale,String> noteTranslations;

	@NotNull
	public Boolean getPublish() {
		return publish;
	}
	@NotNull
	public Component getComponent() {
		return component;
	}
	@NotEmpty
	@Size(max=50)
	public String getDefaultLabel() {
		return defaultLabel;
	}
	@NotEmpty
	@Size(max=5000)
	public String getDefaultNote() {
		return defaultNote;
	}
	@NotNull
	public Map<Locale, String> getLabelTranslations() {
		return labelTranslations;
	}
	@NotNull
	public Map<Locale, String> getNoteTranslations() {
		return noteTranslations;
	}

	public void setPublish(Boolean publish) {
		this.publish = publish;
	}
	public void setDefaultLabel(String defaultLabel) {
		this.defaultLabel = defaultLabel;
	}
	public void setDefaultNote(String defaultNote) {
		this.defaultNote = defaultNote;
	}
	public void setLabelTranslations(Map<Locale, String> labelTranslations) {
		this.labelTranslations = labelTranslations;
	}
	public void setNoteTranslations(Map<Locale, String> noteTranslations) {
		this.noteTranslations = noteTranslations;
	}
	public void setComponent(Component component) {
		this.component = component;
	}
}
