package org.trescal.cwms.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.admin.entity.BusinessEmails;
import org.trescal.cwms.core.admin.entity.db.BusinessEmailsService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class BusinessEmailsController {

	@Autowired
	private BusinessEmailsService businessEmailsService;
	@Autowired
	private SubdivService subdivService;

	@ModelAttribute("businessEmails")
	public BusinessEmails businessEmails(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		BusinessEmails businessEmails = businessEmailsService.getBySubdiv(allocatedSubdiv);
		if (businessEmails == null) {
			businessEmails = new BusinessEmails();
			businessEmails.setOrganisation(allocatedSubdiv);
		}
		return businessEmails;
	}

	@RequestMapping(value = "businessEmails.htm", method = RequestMethod.GET)
	public ModelAndView onRequest() {
		return new ModelAndView("trescal/core/admin/businessEmails");
	}

	@RequestMapping(value = "businessEmails.htm", method = RequestMethod.POST)
	public ModelAndView onSubmit(@Validated @ModelAttribute("businessEmails") BusinessEmails businessEmails,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return onRequest();
		else {
			businessEmailsService.merge(businessEmails);
			return new ModelAndView(new RedirectView("adminarea.htm"));
		}
	}
}