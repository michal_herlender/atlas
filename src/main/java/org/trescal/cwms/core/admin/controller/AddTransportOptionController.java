package org.trescal.cwms.core.admin.controller;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.admin.form.AddTransportOptionForm;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.db.TransportMethodService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.db.TransportOptionScheduleRuleService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME })
public class AddTransportOptionController {

	@Autowired
	private TransportOptionService transportOptionService;
	@Autowired
	private TransportMethodService transportMethodService;
	@Autowired
	private TransportOptionScheduleRuleService transportOptionScheduleRuleService;

	public static final String FORM_NAME = "form";

	@ModelAttribute(FORM_NAME)
	public AddTransportOptionForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		AddTransportOptionForm form = new AddTransportOptionForm();
		form.setSubdivid(subdivDto.getKey());
		return form;
	}

	@RequestMapping(value = "/addtransportoption.htm", method = RequestMethod.POST)
	public ModelAndView onSubmit_createTransportOption(@ModelAttribute(FORM_NAME) AddTransportOptionForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		TransportOption newTransportOption = transportOptionService.addTransportOption(form, username);
		if (newTransportOption != null) {
			transportOptionScheduleRuleService.addTransportOptionScheduleRule(form, newTransportOption);
		}
		return new ModelAndView(new RedirectView("edittransportoptions.htm?subdivid=" + form.getSubdivid()));
	}

	@RequestMapping(value = "/addtransportoption.htm", method = RequestMethod.GET)
	public ModelAndView createTransportOption(Model model, @ModelAttribute(FORM_NAME) AddTransportOptionForm form) {
		model.addAttribute("activeMethods", this.transportMethodService.getAllTransportMethods().stream()
				.filter(method -> method.getActive()).collect(Collectors.toList()));
		return new ModelAndView("trescal/core/admin/addtransportoption");
	}

	@ResponseBody
	@RequestMapping(value = "/checkLocalisedNameUniqueness.json", method = RequestMethod.GET)
	public Boolean isOptionPerDayOfWeek(@RequestParam(value = "localizedName", required = true) String localizedName,
			@RequestParam(value = "transportMethodId", required = true) int transportMethodId,
			@ModelAttribute(FORM_NAME) AddTransportOptionForm form) throws Exception {
		return this.transportOptionService.checkLocalizedNameUniqueness(form.getSubdivid(), localizedName,
				transportMethodId);
	}

}
