package org.trescal.cwms.core.admin.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.admin.form.PrinterFormValidator;
import org.trescal.cwms.core.admin.form.PrinterForm;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.papertype.PaperType;
import org.trescal.cwms.core.system.entity.printer.Printer;
import org.trescal.cwms.core.system.entity.printer.db.PrinterService;
import org.trescal.cwms.core.system.entity.printertray.MediaTrayRef;
import org.trescal.cwms.core.system.entity.printertray.PrinterTray;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class EditPrinterController
{
	@Autowired
	private AddressService addrServ;
	@Autowired
	private PrinterService printerServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private PrinterFormValidator validator; 

	public static final String FORM_NAME = "form";
	
	@InitBinder(FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	@ModelAttribute(FORM_NAME)
	protected PrinterForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto, 
			@RequestParam(value="printerid", required=false, defaultValue="0") Integer printerId) throws Exception
	{
		PrinterForm form = new PrinterForm();
		if (printerId != 0) {
			Printer printer = this.printerServ.get(printerId);
			form.setPrinterId(printerId);
			form.setAddrId(printer.getAddr().getAddrid());
			form.setDescription(printer.getDescription());
			form.setPath(printer.getPath());
			form.setMediaTrayRefs(printer.getTrays().stream().map(tray -> tray.getMediaTrayRef()).collect(Collectors.toList()));
			form.setPaperSources(printer.getTrays().stream().map(tray -> tray.getPaperSource()).collect(Collectors.toList()));
			form.setPaperTypes(printer.getTrays().stream().map(tray -> tray.getPaperType()).collect(Collectors.toList()));
			form.setTrayIndices(printer.getTrays().stream().map(tray -> tray.getId()).collect(Collectors.toList()));
		}
		else {
			Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
			if (allocatedSubdiv.getDefaultAddress() != null) {
				form.setAddrId(allocatedSubdiv.getDefaultAddress().getAddrid());
			}
			else if (!allocatedSubdiv.getAddresses().isEmpty()) {
				form.setAddrId(allocatedSubdiv.getAddresses().iterator().next().getAddrid());
			}
			form.setPath("");
			form.setDescription("");
			form.setPrinterId(0);
		}
		return form;
	}
	
	@RequestMapping(value="/editprinter.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(@RequestParam(value="printerid", required=false, defaultValue="0") Integer printerId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto, 
			@Validated @ModelAttribute(FORM_NAME) PrinterForm form, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) {
			return referenceData();
		}
		Printer printer = null;
		if (printerId != 0) {
			printer = this.printerServ.get(printerId);
		}
		else {
			printer = new Printer();
			printer.setTrays(new HashSet<PrinterTray>()); // Unsorted but that's OK, updated below
		}
		
		printer.setAddr(this.addrServ.get(form.getAddrId()));
		printer.setDescription(form.getDescription());
		printer.setPath(form.getPath());
		
		if (printer.getTrays() != null) {
			if (printer.getTrays().size() < form.getPaperSources().size()) {
				while (printer.getTrays().size() < form.getPaperSources().size()) {
					int currentTray = printer.getTrays().size() + 1;
					PrinterTray tray = new PrinterTray();
					tray.setPrinter(printer);
					tray.setTrayNo(currentTray);
					printer.getTrays().add(tray);
				}
			}
			for (PrinterTray tray : printer.getTrays()) {
				tray.setPaperType(form.getPaperTypes().get(tray.getTrayNo() - 1));
				tray.setPaperSource(form.getPaperSources().get(tray.getTrayNo() - 1));
				tray.setMediaTrayRef(form.getMediaTrayRefs().get(tray.getTrayNo() - 1));
			}
		}
		this.printerServ.merge(printer);
		return new ModelAndView(new RedirectView("viewprinters.htm"));
	}
	
	@RequestMapping(value="/editprinter.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData() throws Exception
	{
		Map<String, Object> refData = new HashMap<String, Object>();
		refData.put("addresses", getAddressDtos());
		refData.put("papertypes", PaperType.values());
		refData.put("papersources", getPaperSources());
		refData.put("mediatrayrefs", MediaTrayRef.values());
		return new ModelAndView("trescal/core/admin/editprinter", refData);
	}
	
	private List<Integer> getPaperSources() {
		List<Integer> result = new ArrayList<>();
		for (int i = 1; i <= 9; i++) {
			result.add(i);
		}
		return result;
	}
	
	private Set<KeyValue<Integer, String>> getAddressDtos() {
		Set<KeyValue<Integer, String>> result = new TreeSet<>();
		for(Address address : this.addrServ.getBusinessAddresses(AddressType.DELIVERY, true)) {
			StringBuffer value = new StringBuffer();
			value.append(address.getCountry().getLocalizedName());
			value.append(" - ");
			value.append(address.getSub().getComp().getConame());
			value.append(" - ");
			value.append(address.getSub().getSubname());
			value.append(" - ");
			value.append(address.getAddr1());
			
			result.add(new KeyValue<Integer, String>(address.getAddrid(), value.toString()));
		}
		return result;
	}
}