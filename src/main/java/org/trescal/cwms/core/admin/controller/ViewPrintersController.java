package org.trescal.cwms.core.admin.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.labelprinter.db.LabelPrinterService;
import org.trescal.cwms.core.system.entity.printer.Printer;
import org.trescal.cwms.core.system.entity.printer.db.PrinterService;

/**
 * The controller for listing all {@link Printer}s.
 * 
 * @author jamiev
 */
@Controller @IntranetController
public class ViewPrintersController 
{
	@Autowired
	private LabelPrinterService labelPrinterServ;
	@Autowired
	private PrinterService printerServ;

	@RequestMapping(value="/viewprinters.htm")
	protected ModelAndView handleRequestInternal() throws Exception
	{
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("printers", this.printerServ.getAll());
		model.put("labelPrinters", this.labelPrinterServ.getAllLabelPrinters());
		return new ModelAndView("trescal/core/admin/viewprinters", "model", model);
	}
}