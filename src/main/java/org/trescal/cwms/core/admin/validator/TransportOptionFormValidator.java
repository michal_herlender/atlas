package org.trescal.cwms.core.admin.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.admin.dto.EditTransportOptionDTO;
import org.trescal.cwms.core.admin.form.TransportOptionForm;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.deliverynote.entity.transportoptionschedulerule.TransportOptionScheduleRule;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class TransportOptionFormValidator extends AbstractBeanValidator {

	@Autowired
	TransportOptionService transportOptionService;

	@Override
	public boolean supports(Class<?> arg0) {
		return TransportOptionForm.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);

		if (!errors.hasErrors()) {

			TransportOptionForm form = (TransportOptionForm) target;
			int index = 0;
			for (EditTransportOptionDTO dto : form.getOptions()) {
				TransportOption transportOption = this.transportOptionService.get(dto.getTransportOptionId());
				// check if localized name is duplicated
				if (!StringUtils.isEmpty(dto.getLocalizedName()) && !dto.getLocalizedName().equals(transportOption.getLocalizedName())
						&& this.transportOptionService.transportOptionByLocalizedName(form.getSubdivid(), dto.getLocalizedName(),
								transportOption.getMethod().getId()).size() >= 1) {
					errors.rejectValue("options[" + index + "].localizedName", "transportoptions.error.localizedname");
				}
				// check if the time is entered when the day is checked
				int scheduleRuleIndex = 0;
				for(TransportOptionScheduleRule scheduleRule : dto.getScheduleRule()){	
					if(scheduleRule.getDayOfWeek() != null && scheduleRule.getCutoffTime() == null){
						errors.rejectValue("options["+ index +"].scheduleRule["+ scheduleRuleIndex +"].cutoffTime", "transportoptions.error.time.empty");
					}
					scheduleRuleIndex++;
				}
				index++;
			}
		}
	}

}
