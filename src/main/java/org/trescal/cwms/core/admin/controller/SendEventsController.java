package org.trescal.cwms.core.admin.controller;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.external.adveso.dto.AdvesoJobItemActivityDTO;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.db.AdvesoJobItemActivityService;
import org.trescal.cwms.core.external.adveso.enums.EventStatusSearchForm;
import org.trescal.cwms.core.external.adveso.form.SearchEventForm;
import org.trescal.cwms.core.external.adveso.form.SearchEventFormValidator;
import org.trescal.cwms.core.misc.tool.CustomLocalDateTimeEditor;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.adveso.service.RestClientAdvesoQueueService;

@Controller
@IntranetController
public class SendEventsController {

	@Autowired
	private AdvesoJobItemActivityService advesoJobItemActivityServ;
	@Autowired
	private RestClientAdvesoQueueService restClientAdvesoQueueService;
	@Autowired
	private SearchEventFormValidator searchEventFormValidator;

	public static final String VIEW_NAME = "trescal/core/admin/sendevents";
	public static final String FORM_NAME = "form";

	@InitBinder(FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(searchEventFormValidator);
		binder.registerCustomEditor(LocalDateTime.class, new CustomLocalDateTimeEditor(DateTimeFormatter.ISO_DATE_TIME));
	}

	@ModelAttribute(FORM_NAME)
	public SearchEventForm form() {
		SearchEventForm form = new SearchEventForm();
		form.setStatus(EventStatusSearchForm.PENDING);
		form.setAction("pagination");
		return form;
	}

	@RequestMapping(value = "/sendevents.htm", method = { RequestMethod.GET, RequestMethod.POST })
	protected String formView(Model model, @Valid @ModelAttribute(FORM_NAME) SearchEventForm form,
			BindingResult bindingResult) throws ParseException {

		model.addAttribute("statusOptions", EventStatusSearchForm.values());

		if (bindingResult.hasErrors()) {
			return VIEW_NAME;
		}
		if (form.getAction().equals("search") || form.getAction().equals("resend")) {
			if (form.getResendIds() != null) {
				List<Integer> resendIds = form.getResendIds().entrySet().stream()
						.filter(entry -> entry.getValue() != null && entry.getValue()).map(Map.Entry::getKey)
						.collect(Collectors.toList());

				this.restClientAdvesoQueueService.sendActivities(resendIds);
			}
			PagedResultSet<AdvesoJobItemActivityDTO> prs = new PagedResultSet<>(form.getResultsPerPage(), 1);
			this.advesoJobItemActivityServ.findActivities(form, prs);
			form.setAction("pagination");
			model.addAttribute("rs", prs);
		} else if (form.getAction().equals("pagination")) {
			PagedResultSet<AdvesoJobItemActivityDTO> prs = new PagedResultSet<>(form.getResultsPerPage(),
					form.getPageNo());
			this.advesoJobItemActivityServ.findActivities(form, prs);
			model.addAttribute("rs", prs);
		}

		return VIEW_NAME;
	}
}