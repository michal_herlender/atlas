package org.trescal.cwms.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.userright.enums.Permission;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class ViewTransportOptionsController {

	@Autowired
	private SubdivService subdivService;
	@Autowired
	private TransportOptionService transportOptionService;
	@Autowired
	private UserService userServ;
	@Autowired
	private AuthenticationService authServ;

	@RequestMapping(path = "viewtransportoptions.htm", method = RequestMethod.GET)
	public String referenceData(Model model, @RequestParam(value = "subdivid") Integer subdivid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName) {
		Subdiv subdiv = this.subdivService.get(subdivid);
		User user = userServ.get(userName);
		boolean editAuthorized = this.authServ.hasRight(user.getCon(), Permission.ADMIN_EDIT_TRANSPORT_OPTION, subdivid);
		model.addAttribute("subdiv", subdiv);
		model.addAttribute("editAuthorized", editAuthorized);
		model.addAttribute("transportOptionsWithActiveMethod",
				this.transportOptionService.transportOptionsFromSubdivWithActiveMethod(subdivid));

		return "trescal/core/admin/viewtransportoptions";
	}
	
	@RequestMapping(path = "viewtransportoptionsdetails.htm", method = RequestMethod.GET)
	public String referenceDataDetails(Model model, @RequestParam(value = "subdivid") Integer subdivid) {
		Subdiv subdiv = this.subdivService.get(subdivid);
		model.addAttribute("subdiv", subdiv);
		model.addAttribute("transportOptionsWithActiveMethod",
				this.transportOptionService.transportOptionsFromSubdivWithActiveMethod(subdivid));

		return "trescal/core/admin/viewtransportoptionsdetails";
	}

}
