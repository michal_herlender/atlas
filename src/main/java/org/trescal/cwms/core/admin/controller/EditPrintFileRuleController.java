package org.trescal.cwms.core.admin.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.admin.form.EditPrintFileRuleForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.papertype.PaperType;
import org.trescal.cwms.core.system.entity.printer.db.PrinterService;
import org.trescal.cwms.core.system.entity.printfilerule.PrintFileRule;
import org.trescal.cwms.core.system.entity.printfilerule.db.PrintFileRuleService;

@Controller @IntranetController
public class EditPrintFileRuleController
{
	@Autowired
	private PrintFileRuleService pfrServ;
	@Autowired
	private PrinterService printerServ;
	
	@ModelAttribute("form")
	protected EditPrintFileRuleForm formBackingObject(@RequestParam(value="printfileruleid", required=false, defaultValue="0") Integer pfrId) throws Exception
	{
		EditPrintFileRuleForm form = new EditPrintFileRuleForm();
		PrintFileRule pfr = null;
		if (pfrId != 0) {
			pfr = this.pfrServ.findPrintFileRule(pfrId);
			if (pfr == null) {
				throw new Exception("Print File Rule with ID " + pfrId + " could not be found");
			}
		}
		else {
			pfr = new PrintFileRule();
		}
		form.setPfr(pfr);
		return form;
	}
	
	@RequestMapping(value="/editprintfilerule.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(@Validated @ModelAttribute("form") EditPrintFileRuleForm form, BindingResult errors) throws Exception
	{
			if(errors.hasErrors()){
				return referenceData();
			}
			else {
				form.getPfr().setPrinter(this.printerServ.get(form.getPrinterId()));
				this.pfrServ.saveOrUpdatePrintFileRule(form.getPfr());
				return new ModelAndView (new RedirectView("viewprintfilerules.htm"));
			}
	}
	
	@RequestMapping(value="/editprintfilerule.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData() throws Exception
	{
		Map<String, Object> refData = new HashMap<String, Object>();
		refData.put("papertypes", PaperType.values());
		refData.put("printers", this.printerServ.getAll());
		return new ModelAndView("trescal/core/admin/editprintfilerule", refData);
	}
}