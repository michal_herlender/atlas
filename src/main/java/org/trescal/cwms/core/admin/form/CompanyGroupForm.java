package org.trescal.cwms.core.admin.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Setter;

@Setter
public class CompanyGroupForm {
	private Integer id;
	private String groupName;
	private Boolean active;
	
	@NotNull
	public Integer getId() {
		return id;
	}

	@NotNull
	@Size(min=1, max=75)
	public String getGroupName() {
		return groupName;
	}

	@NotNull
	public Boolean getActive() {
		return active;
	}

}
