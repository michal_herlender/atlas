package org.trescal.cwms.core.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.system.entity.defaultnote.DefaultNote;
import org.trescal.cwms.core.system.entity.defaultnote.db.DefaultNoteService;

@Controller
public class DeleteDefaultNoteController {
	@Autowired
	private DefaultNoteService defaultNoteService;
	
	@RequestMapping(value="deletedefaultnote.htm")
	public String performDelete(@RequestParam(name="id", required=true) Integer defaultNoteId) {
		DefaultNote note = this.defaultNoteService.get(defaultNoteId);
		this.defaultNoteService.delete(note);
		return "redirect:viewdefaultnotes.htm";
	}
}
