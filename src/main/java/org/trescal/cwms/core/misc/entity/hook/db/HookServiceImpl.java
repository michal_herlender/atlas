package org.trescal.cwms.core.misc.entity.hook.db;

import java.util.List;

import org.trescal.cwms.core.misc.entity.hook.Hook;

public class HookServiceImpl implements HookService
{
	HookDao hookDao;

	public void deleteHook(Hook hook)
	{
		this.hookDao.remove(hook);
	}

	public Hook findHook(int id)
	{
		return this.hookDao.find(id);
	}

	public Hook findHookByName(String name)
	{
		return this.hookDao.findHookByName(name);
	}
	
	public List<Hook> getAllHooks()
	{
		return this.hookDao.findAll();
	}

	public void insertHook(Hook Hook)
	{
		this.hookDao.persist(Hook);
	}
	
	public void saveOrUpdateHook(Hook hook)
	{
		this.hookDao.saveOrUpdate(hook);
	}

	public void setHookDao(HookDao hookDao)
	{
		this.hookDao = hookDao;
	}

	public void updateHook(Hook Hook)
	{
		this.hookDao.update(Hook);
	}
}