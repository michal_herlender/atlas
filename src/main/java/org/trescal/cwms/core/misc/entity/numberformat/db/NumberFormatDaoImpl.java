package org.trescal.cwms.core.misc.entity.numberformat.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.misc.entity.numberformat.NumberFormat;
import org.trescal.cwms.core.misc.entity.numberformat.NumberFormat_;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;

@Repository
public class NumberFormatDaoImpl extends AllocatedToCompanyDaoImpl<NumberFormat, Integer> implements NumberFormatDao {

	@Override
	protected Class<NumberFormat> getEntity() {
		return NumberFormat.class;
	}

	@Override
	public NumberFormat find(NumerationType type, Company company) {
		return getSingleResult(cb -> {
			CriteriaQuery<NumberFormat> cq = cb.createQuery(NumberFormat.class);
			Root<NumberFormat> root = cq.from(NumberFormat.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(NumberFormat_.numerationType), type));
			clauses.getExpressions().add(cb.equal(root.get(NumberFormat_.organisation), company));
			cq.where(clauses);
			return cq;
		});
	}
}