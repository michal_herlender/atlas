package org.trescal.cwms.core.misc.form;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AddFeedbackForm
{
	private String title;
	private String url;
	private String description;
	private boolean feedbackLeft;

}
