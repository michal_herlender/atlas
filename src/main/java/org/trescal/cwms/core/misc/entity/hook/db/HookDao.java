package org.trescal.cwms.core.misc.entity.hook.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.misc.entity.hook.Hook;

public interface HookDao extends BaseDao<Hook, Integer> {
	
	Hook findHookByName(String name);
}