package org.trescal.cwms.core.misc.entity.hook.db;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.misc.entity.hook.Hook;

@Repository("HookDao")
public class HookDaoImpl extends BaseDaoImpl<Hook, Integer> implements HookDao {
	
	@Override
	protected Class<Hook> getEntity() {
		return Hook.class;
	}
	
	public Hook findHookByName(String name) {
		Criteria criteria = getSession().createCriteria(Hook.class);
		criteria.add(Restrictions.eq("name", name));
		return (Hook) criteria.uniqueResult();
	}
}