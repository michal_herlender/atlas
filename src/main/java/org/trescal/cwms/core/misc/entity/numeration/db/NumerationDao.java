package org.trescal.cwms.core.misc.entity.numeration.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.misc.entity.numeration.Numeration;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;

public interface NumerationDao extends BaseDao<Numeration, Integer> {

	Numeration get(NumerationType type, Company company, Integer year);
}