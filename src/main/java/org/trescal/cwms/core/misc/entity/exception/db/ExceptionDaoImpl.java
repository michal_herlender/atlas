package org.trescal.cwms.core.misc.entity.exception.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.misc.entity.exception.Exception;

@Repository("ExceptionDao")
public class ExceptionDaoImpl extends BaseDaoImpl<Exception, Integer> implements ExceptionDao {
	
	@Override
	protected Class<Exception> getEntity() {
		return Exception.class;
	}
}