package org.trescal.cwms.core.misc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.entity.quickscan.db.QuickScanService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class QuickScanController {
	
	@Autowired
	private QuickScanService quickScanService;
	
	@RequestMapping(value="quickScan.htm")
	public ModelAndView quickScan(
			@RequestHeader(name="referer", required=false, defaultValue="home.htm") String referrer,
			@RequestParam(name="scan", required=false, defaultValue="") String scan,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		ResultWrapper wrapper = quickScanService.quickScan(scan, subdivDto.getKey());
		if(wrapper.isSuccess())
			return new ModelAndView(new RedirectView((String) wrapper.getResults()));
		else return new ModelAndView(new RedirectView(referrer));
	}
}