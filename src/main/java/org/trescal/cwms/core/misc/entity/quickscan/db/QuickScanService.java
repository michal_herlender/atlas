package org.trescal.cwms.core.misc.entity.quickscan.db;

import org.trescal.cwms.core.dwr.ResultWrapper;

public interface QuickScanService
{
	/**
	 * Uses a string input by the user to find a matching entity to redirect to.
	 * (i.e. 10234/11/A will redirect the user to a job with the matching job
	 * number, 50124/11/A will redirect the user to a quotation with the
	 * matching quote number etc....)
	 * 
	 * @param source the number of an entity which the user wishes to view
	 * @return a {@link ResultWrapper} indicating the success of the operation
	 *         and link to entity required
	 */
	ResultWrapper quickScan(String source, int allocatedSubdivId);
}
