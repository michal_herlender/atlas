package org.trescal.cwms.core.misc.entity.numeration;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;

@Entity
@Table(name = "numeration")
public class Numeration extends Allocated<Company>
{
	private Integer id;
	private NumerationType numerationType;
	private Integer year;
	private Integer count;
	
	public Numeration() {}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	@Enumerated(EnumType.STRING)
	public NumerationType getNumerationType() {
		return numerationType;
	}
	
	public Integer getYear() {
		return year;
	}
	
	public Integer getCount() {
		return count;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setNumerationType(NumerationType numerationType) {
		this.numerationType = numerationType;
	}
	
	public void setYear(Integer year) {
		this.year = year;
	}
	
	public void setCount(Integer count) {
		this.count = count;
	}
}