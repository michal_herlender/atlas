package org.trescal.cwms.core.misc.entity.exception.db;

import java.util.List;

import org.trescal.cwms.core.misc.entity.exception.Exception;

public class ExceptionServiceImpl implements ExceptionService
{
	ExceptionDao exceptionDao;

	public Exception findException(int id)
	{
		return exceptionDao.find(id);
	}

	public void insertException(Exception exception)
	{
		exceptionDao.persist(exception);
	}

	public void updateException(Exception exception)
	{
		exceptionDao.update(exception);
	}

	public List<Exception> getAllExceptions()
	{
		return exceptionDao.findAll();
	}

	public ExceptionDao getExceptionDao()
	{
		return exceptionDao;
	}

	public void setExceptionDao(ExceptionDao exceptionDao)
	{
		this.exceptionDao = exceptionDao;
	}

	public void changeException(int id, String comment, boolean reviewed)
	{
		Exception ex = findException(id);
		ex.setComment(comment);
		ex.setReviewed(reviewed);
		updateException(ex);
	}
}