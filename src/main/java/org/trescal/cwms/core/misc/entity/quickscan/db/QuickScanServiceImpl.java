package org.trescal.cwms.core.misc.entity.quickscan.db;

import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;

public class QuickScanServiceImpl implements QuickScanService
{
	private CertificateService certServ;
	private HireService hireServ;
	private InstrumService instServ;
	private JobService jobServ;
	private PurchaseOrderService poServ;
	private QuotationService quoteServ;
	private TPQuoteRequestService tpqrServ;
	private TPQuotationService tpqServ;

	/**
	 * this method finds a job by the job number
	 * 
	 * @param jobNo number of job to find
	 * @return {@link Job}
	 */
	private Job findJob(String jobNo)
	{
		// find job
		Job job = this.jobServ.getJobByExactJobNo(jobNo);
		// return job
		return job;
	}

	public ResultWrapper quickScan(String source, int allocatedSubdivId)
	{
		// trim source just in case any spaces passed
		source = source.trim();
		// create new result wrapper in case of failure to find entity or
		// redirect
		ResultWrapper wrapper = new ResultWrapper(false, "", null, null);
		// source null or empty ?
		if ((source != null) && !source.isEmpty())
		{
			List<Instrument> instruments = this.instServ.findInstrumByBarCode(source);
			
			if (instruments.size() >= 1)
			{
				if (instruments.size() > 1)
				{
					wrapper.setSuccess(true);
					wrapper.setResults("searchinstrument.htm?fromquickscan=true&capturedtext="
							+ source);
				}
				else
				{
					wrapper.setSuccess(true);
					wrapper.setResults("viewinstrument.htm?plantid="
							+ instruments.get(0).getPlantid());
				}
				return wrapper;
			}
		}
		
		if (((source != null) && !source.isEmpty())
				&& (source.matches("\\d{6}")
						|| source.matches("[Tt]?[0-9]{5}/[0-9]{2}/[A-Za-z]{1}")
						|| source.matches("[Uu]{1}[0-9]{5}-[0-9]{2}") || source.matches("[Tt]?[0-9]+-[0-9]{2}")))
		{
			// source likely to be a barcode?
			if ((NumberTools.isAnInteger(source)) && (source.matches("\\d{6}")))
			{
				// find the instrument
				Instrument instrum = this.instServ.get(Integer.parseInt(source));
				// instrument null?
				if (instrum != null)
				{
					// change wrapper to success wrapper
					wrapper.setSuccess(true);
					wrapper.setResults("viewinstrument.htm?plantid="
							+ instrum.getPlantid());
				}
				else
				{
					// add message that instrument could not be found
					wrapper.setMessage("The instrument you were searching for could not be found");
				}
			}
			else if (source.matches("[Uu]{1}[0-9]{5}-[0-9]{2}")
					|| source.matches("[Tt]?[0-9]+-[0-9]{2}"))
			{
				// find certificate
				Certificate cert = this.certServ.findCertificateByCertNo(source);
				// certificate null?
				if (cert != null)
				{
					// change wrapper to success wrapper
					wrapper.setSuccess(true);
					wrapper.setResults("viewcertificate.htm?certid="
							+ cert.getCertid());
				}
				else
				{
					// add message that certificate could not be found
					wrapper.setMessage("The certificate you were searching for could not be found");
				}
			}
			else
			{
				// get first digit of source supplied
				String firstDigit = source.substring(0, 1);
				// check that first digit is not 'T'
				if (!firstDigit.matches("[Tt]"))
				{
					// initialise objects
					Job job = null;
					Hire hire = null;
					Quotation quote = null;
					PurchaseOrder po = null;
					TPQuoteRequest tpqr = null;
					// find correct entity by number
					switch (Integer.parseInt(firstDigit))
					{
						case 1: // find job
							job = this.findJob(source);
							// job null?
							if (job != null)
							{
								// change wrapper to success wrapper
								wrapper.setSuccess(true);
								wrapper.setResults("viewjob.htm?jobid="
										+ job.getJobid());
							}
							else
							{
								// add message that job could not be found
								wrapper.setMessage("The job you were searching for could not be found");
							}
							break;
						case 2: // find tpqr
							tpqr = this.tpqrServ.getTPQuoteReqByExactTPQRNo(source, allocatedSubdivId);
							// tpqr null?
							if (tpqr != null)
							{
								// change wrapper to success wrapper
								wrapper.setSuccess(true);
								wrapper.setResults("viewtpquoterequest.htm?id="
										+ tpqr.getId());
							}
							else
							{
								// add message that third party quote request
								// could
								// not
								// be found
								wrapper.setMessage("The third party quote request you were searching for could not be found");
							}
							break;
						case 3: // find job
							job = this.findJob(source);
							// job null?
							if (job != null)
							{
								// change wrapper to success wrapper
								wrapper.setSuccess(true);
								wrapper.setResults("viewjob.htm?jobid="
										+ job.getJobid());
							}
							else
							{
								// add message that job could not be found
								wrapper.setMessage("The job you were searching for could not be found");
							}
							break;
						case 4: // find purchase order
							po = this.poServ.getPOByExactPONo(source);
							// po null?
							if (po != null)
							{
								// change wrapper to success wrapper
								wrapper.setSuccess(true);
								wrapper.setResults("viewpurchaseorder.htm?id="
										+ po.getId());
							}
							else
							{
								// add message that purchase order could not be
								// found
								wrapper.setMessage("The purchase order you were searching for could not be found");
							}
							break;
						case 5: // find quotation
							quote = this.quoteServ.getQuotationByExactQuoteNo(source);
							// quote null?
							if (quote != null)
							{
								// change wrapper to success wrapper
								wrapper.setSuccess(true);
								wrapper.setResults("viewquotation.htm?id="
										+ quote.getId());
							}
							else
							{
								// add message that quotation could not be found
								wrapper.setMessage("The quotation you were searching for could not be found");
							}
							break;
						case 8: // find hire
							hire = this.hireServ.getHireByExactHireNo(source);
							// hire null?
							if (hire != null)
							{
								// change wrapper to success wrapper
								wrapper.setSuccess(true);
								wrapper.setResults("viewhire.htm?id="
										+ hire.getId());
							}
							else
							{
								// add message that hire could not be found
								wrapper.setMessage("The hire you were searching for could not be found");
							}
							break;
					}
				}
				else
				{
					// find third party quote
					TPQuotation tpq = this.tpqServ.getTPQuoteByExactTPQNo(source);
					// third party quote null?
					if (tpq != null)
					{
						// change wrapper to success wrapper
						wrapper.setSuccess(true);
						wrapper.setResults("viewtpquote.htm?id=" + tpq.getId());
					}
					else
					{
						// add message that quotation could not be found
						wrapper.setMessage("The third party quote you were searching for could not be found");
					}
				}
			}

		}
		else
		{
			// add message that source is null or empty
			wrapper.setMessage("The input you supplied was not a valid identifier");
		}
		// return result wrapper
		return wrapper;
	}

	public void setCertServ(CertificateService certServ)
	{
		this.certServ = certServ;
	}

	public void setHireServ(HireService hireServ)
	{
		this.hireServ = hireServ;
	}

	public void setInstServ(InstrumService instServ)
	{
		this.instServ = instServ;
	}

	public void setJobServ(JobService jobServ)
	{
		this.jobServ = jobServ;
	}

	public void setPoServ(PurchaseOrderService poServ)
	{
		this.poServ = poServ;
	}

	public void setQuoteServ(QuotationService quoteServ)
	{
		this.quoteServ = quoteServ;
	}

	public void setTpqrServ(TPQuoteRequestService tpqrServ)
	{
		this.tpqrServ = tpqrServ;
	}

	public void setTpqServ(TPQuotationService tpqServ)
	{
		this.tpqServ = tpqServ;
	}
}
