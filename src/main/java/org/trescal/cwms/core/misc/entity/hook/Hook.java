package org.trescal.cwms.core.misc.entity.hook;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.misc.entity.hookactivity.HookActivity;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;

@Entity
@Table(name = "hook", uniqueConstraints={@UniqueConstraint(columnNames={"name"})})
public class Hook
{
	private boolean active;
	private ItemActivity activity;
	private Boolean alwaysPossible;
	private Boolean beginActivity;
	private Boolean completeActivity;
	private int id;
	private String name;
	private Set<HookActivity> hookActivities;

	public Hook() {
		// Defaults for unit testing 
		this.alwaysPossible = Boolean.FALSE;
		this.beginActivity = Boolean.FALSE;
		this.completeActivity = Boolean.FALSE;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "activityid")
	public ItemActivity getActivity()
	{
		return this.activity;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@Length(min = 1, max = 100)
	@Column(name = "name", nullable = false, length = 100)
	public String getName()
	{
		return this.name;
	}

	@NotNull
	@Column(name = "active", nullable = false, columnDefinition="bit")
	public boolean isActive()
	{
		return this.active;
	}

	@NotNull
	@Column(name = "alwayspossible", nullable = false, columnDefinition="bit")
	public Boolean getAlwaysPossible()
	{
		return this.alwaysPossible;
	}

	@NotNull
	@Column(name = "beginactivity", nullable = false, columnDefinition="bit")
	public Boolean getBeginActivity()
	{
		return this.beginActivity;
	}

	@NotNull
	@Column(name = "completeactivity", nullable = false, columnDefinition="bit")
	public Boolean getCompleteActivity()
	{
		return this.completeActivity;
	}

	@OneToMany(mappedBy="hook", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	@OrderBy("id")
	public Set<HookActivity> getHookActivities() {
		return hookActivities;
	}
	
	
	public void setActive(boolean active)
	{
		this.active = active;
	}

	public void setActivity(ItemActivity activity)
	{
		this.activity = activity;
	}

	public void setAlwaysPossible(Boolean alwaysPossible)
	{
		this.alwaysPossible = alwaysPossible;
	}

	public void setBeginActivity(Boolean beginActivity)
	{
		this.beginActivity = beginActivity;
	}

	public void setCompleteActivity(Boolean completeActivity)
	{
		this.completeActivity = completeActivity;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setHookActivities(Set<HookActivity> hookActivities) {
		this.hookActivities = hookActivities;
	}
}
