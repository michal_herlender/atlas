package org.trescal.cwms.core.misc.entity.numberformat;

public class SplitNumberException extends RuntimeException {

	private static final long serialVersionUID = 6339247421817092718L;

	public SplitNumberException() {
		super();
	}

	public SplitNumberException(String message) {
		super(message);
	}
}