package org.trescal.cwms.core.misc.tool;

import java.beans.PropertyEditorSupport;
import java.util.Currency;

public class CurrencyPropertyEditor extends PropertyEditorSupport
{
	@Override
	public void setAsText(String currencyCode)
	{
		Currency currency = Currency.getInstance(currencyCode);
		this.setValue(currency);
	}
}