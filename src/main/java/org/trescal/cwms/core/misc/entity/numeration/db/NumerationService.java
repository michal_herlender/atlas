package org.trescal.cwms.core.misc.entity.numeration.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.misc.entity.numeration.Numeration;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;

/**
 * Methods are named as generate* as get* methods run in a read-only context;
 * this ensures that transactions are committed in situations without
 * auto-flushing (e.g. when running in background task beans, no
 * OpenEntityManager etc..)
 */
public interface NumerationService extends BaseService<Numeration, Integer> {

	Integer generateNextNumber(NumerationType type, Company company, Integer year);

	String generateNumber(NumerationType type, Company company, Subdiv subdiv);
}