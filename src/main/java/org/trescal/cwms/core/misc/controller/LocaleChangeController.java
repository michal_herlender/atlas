/*
 *
 */
package org.trescal.cwms.core.misc.controller;

import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.form.LocaleChangeForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author Galen Beck
 * Created 2015-1-15
 */
@Controller @IntranetController
public class LocaleChangeController
{
	@Autowired
	private LocaleResolver localeResolver;
	@Autowired
	private UserService userService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	
	@ModelAttribute("command")
	protected LocaleChangeForm formBackingObject(HttpServletRequest request) throws Exception
	{
		LocaleChangeForm localeChangeForm = new LocaleChangeForm();
		Locale currentLocale = localeResolver.resolveLocale(request);
		localeChangeForm.setDesiredLocale(currentLocale);
		localeChangeForm.setRefererUrl(request.getHeader("referer"));
		
		return localeChangeForm;
	}
	
	/*
	 * Handles user request - 
	 * (a) if command is null then populate view with supported locales for the application
	 * (b) otherwise command is new locale, change locale in session and return success
	 */
	@RequestMapping(value="/changelocale.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, 
			@ModelAttribute("command") LocaleChangeForm form,  BindingResult errors)
	{
		Locale updatedLocale = form.getDesiredLocale();
		if (updatedLocale == null) {
			errors.rejectValue("desiredLocale", "Locale not specified", "The locale was not specified; please choose a locale");
			return referenceData();
		}
		else {
			WebUtils.setSessionAttribute(request, Constants.SESSION_ATTRIBUTE_LOCALE, updatedLocale);
			String username = (String) request.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
			Contact contact = userService.get(username).getCon();
			contact.setLocale(updatedLocale);
			contactService.merge(contact);
		}
		String unescapedUrl = StringEscapeUtils.unescapeHtml4(form.getRefererUrl());
		return new ModelAndView(new RedirectView(unescapedUrl, true));
	}
	
	@RequestMapping(value="/changelocale.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData() {
        Map<String, Object> model = new HashMap<>();
        model.put("supportedLocales", supportedLocaleService.getSupportedLocales());
        return new ModelAndView("trescal/core/misc/changelocale", model);
    }
}