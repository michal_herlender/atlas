package org.trescal.cwms.core.misc.entity.hookactivity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;

@Entity
@Table(name = "hookactivity")
public class HookActivity
{
	private ItemActivity activity;
	private boolean beginActivity;
	private boolean completeActivity;
	private Hook hook;
	private int id;
	private ItemState state;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "activityid", nullable = true)
	public ItemActivity getActivity()
	{
		return this.activity;
	}

	// TODO should be nullable=false, change here and in DB, add annotation
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hookid", nullable = true)
	public Hook getHook()
	{
		return this.hook;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stateid", nullable = true)
	public ItemState getState()
	{
		return this.state;
	}

	@NotNull
	@Column(name = "beginactivity", nullable = false, columnDefinition="bit")
	public boolean isBeginActivity()
	{
		return this.beginActivity;
	}

	@NotNull
	@Column(name = "completeactivity", nullable = false, columnDefinition="bit")
	public boolean isCompleteActivity()
	{
		return this.completeActivity;
	}

	public void setActivity(ItemActivity activity)
	{
		this.activity = activity;
	}

	public void setBeginActivity(boolean beginActivity)
	{
		this.beginActivity = beginActivity;
	}

	public void setCompleteActivity(boolean completeActivity)
	{
		this.completeActivity = completeActivity;
	}

	public void setHook(Hook hook)
	{
		this.hook = hook;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setState(ItemState state)
	{
		this.state = state;
	}
}