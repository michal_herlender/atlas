package org.trescal.cwms.core.misc.entity.numeration.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.misc.entity.numberformat.db.NumberFormatService;
import org.trescal.cwms.core.misc.entity.numeration.Numeration;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;

import javax.persistence.NoResultException;
import java.time.LocalDate;

@Service
public class NumerationServiceImpl extends BaseServiceImpl<Numeration, Integer> implements NumerationService {
	@Autowired
	private NumerationDao numerationDao;
	@Autowired
	private NumberFormatService numberFormatService;

	@Override
	protected BaseDao<Numeration, Integer> getBaseDao() {
		return this.numerationDao;
	}

	@Override
	public Integer generateNextNumber(NumerationType type, Company company, Integer year) {
		try {
			Numeration numeration = numerationDao.get(type, company, year);
			Integer next = numeration.getCount() + 1;
			numeration.setCount(next);
			numerationDao.merge(numeration);
			return next;
		} catch (NoResultException ex) {
			Numeration numeration = new Numeration();
			numeration.setCount(1);
			numeration.setNumerationType(type);
			numeration.setOrganisation(company);
			numeration.setYear(year);
			numerationDao.persist(numeration);
			return 1;
		}
	}

	@Override
	public String generateNumber(NumerationType type, Company company, Subdiv subdiv) {
		Integer year = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).getYear();
		Integer count = this.generateNextNumber(type, company, year);
		return numberFormatService.generateNumber(type, count, year, company, subdiv);
	}
}