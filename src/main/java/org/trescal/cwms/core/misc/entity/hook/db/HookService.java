package org.trescal.cwms.core.misc.entity.hook.db;

import java.util.List;

import org.trescal.cwms.core.misc.entity.hook.Hook;

public interface HookService
{
	void deleteHook(Hook hook);

	Hook findHook(int id);

	Hook findHookByName(String name);
	
	List<Hook> getAllHooks();
	
	void insertHook(Hook hook);
	
	void saveOrUpdateHook(Hook hook);
	
	void updateHook(Hook hook);
}