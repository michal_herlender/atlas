package org.trescal.cwms.core.misc.entity.numeration;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.organisation.OrganisationLevel;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

public enum NumerationType
{
	DELIVERY_NOTE(Subdiv.class, "DN"),
	JOB(Subdiv.class, "JI"),
	SITE_JOB(Subdiv.class, "JO"),			// Unused, but retaining until counter deleted 2016-10-31 GB
	VALVE_JOB(Subdiv.class, "VJ"),			// Unused, but retaining until counter deleted 2016-10-31 GB
	QUOTATION(Company.class, "QC"),
	TPQUOTATION(Company.class, "QS"),
	HIRE(Company.class, "HC"),
	PURCHASE_ORDER(Company.class, "PO"),
	INVOICE(Company.class, "IN"),
	ASSET(Company.class, "AS"),
	QUOTATION_REQUEST(Company.class, "RC"),
	TP_QUOTATION_REQUEST(Company.class, "RS"),
	GENERAL_DELIVERY(Subdiv.class, "GD"),
	PRICE_LIST(Company.class, "PL"),
	CREDIT_NOTE(Company.class, "CN"),
	CERTIFICATE(Subdiv.class, "CI"),
	TP_CERTIFICATE(Subdiv.class, "CS"),
	CLIENT_CERTIFICATE(Subdiv.class, "CC"),
	IMAGE(Company.class, "IM"),
	FAULT_REPORT(Subdiv.class, "FR"),
	CONTRACT(Company.class, "CO"),
	REPAIR_INSPECTION_REPORT(Subdiv.class, "RIR"),
	REPAIR_COMPLETION_REPORT(Subdiv.class, "RCR"),
	GENERAL_SERVICE_OPERATION(Subdiv.class, "GSO");
	
	private Class<? extends OrganisationLevel> organisationLevel;
	private String token;
	
	private NumerationType(Class<? extends OrganisationLevel> organisationLevel, String token) {
		this.organisationLevel = organisationLevel;
		this.token = token;
	}
	
	public Class<? extends OrganisationLevel> getOrganisationLevel() {
		return this.organisationLevel;
	}
	
	public String getToken() {
		return this.token;
	}
}