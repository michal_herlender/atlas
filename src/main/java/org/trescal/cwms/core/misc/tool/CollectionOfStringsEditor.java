package org.trescal.cwms.core.misc.tool;

import java.beans.PropertyEditorSupport;
import java.util.Collection;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.springframework.util.StringUtils;

@SuppressWarnings("unchecked")
public class CollectionOfStringsEditor extends PropertyEditorSupport
{
	public static final String separator = ",";
	private final Collection<String> collectionInstance;

	public CollectionOfStringsEditor(Class<?> collectionClass, Class<?> stringClass)
	{
		super();
		this.collectionInstance = this.checkClasses(stringClass, collectionClass);
	}

	private Collection<String> checkClasses(Class<?> stringClass, Class<?> collectionClass) throws IllegalArgumentException
	{
		Collection<String> collectionInstance;
		// Ensure we have a subclass of String
		if ((stringClass == null) || !String.class.isAssignableFrom(stringClass))
		{
			throw new IllegalArgumentException("String class must be a subclass of " + String.class.getName());
		}
		// Ensure we have a subclass of Collection
		if ((collectionClass == null) || !Collection.class.isAssignableFrom(collectionClass))
		{
			throw new IllegalArgumentException("Collection class must be a subclass of " + Collection.class.getName());
		}
		// Ensure we have a instantiatable subclass of Collection
		try
		{
			collectionInstance = (Collection<String>) collectionClass.newInstance();
		}
		catch (InstantiationException e)
		{
			throw new IllegalArgumentException("Could not instantiate " + collectionClass.getName() + ".\n" + "The Collection class must be instantiatable.");
		}
		catch (IllegalAccessException e)
		{
			throw new IllegalArgumentException("Could no instantiate " + collectionClass.getName() + ".\n" + "The Collection class must be instantiatable.");
		}
		return collectionInstance;
	}

	/**
	 * This method creates a CSV of all the values in the underlying collection
	 */
	@Override
	public String getAsText()
	{
		if (this.getValue() == null)
		{
			return "";
		}
		StringBuilder sb = new StringBuilder();
		for (Iterator<String> iterator = ((Collection<String>) this.getValue()).iterator(); iterator.hasNext();)
		{
			sb.append(iterator.next());
			if (iterator.hasNext())
			{
				sb.append(separator + " ");
			}
		}
		return sb.toString();
	}

	/**
	 * This method takes a CSV list of values as input and creates a collection
	 * of the specified collection type, containing elements of the specified
	 * stringType
	 */
	@Override
	public void setAsText(String string) throws IllegalArgumentException
	{
		// Set the Value. We start off with an empty collection, and
		// can continue working with the collection instance.
		this.setValue(this.collectionInstance);
		if (!StringUtils.hasText(string))
		{
			return;
		}
		for (StringTokenizer tk = new StringTokenizer(string, separator); tk.hasMoreTokens();)
		{
			String str = tk.nextToken();
			if (str == null)
			{
				continue;
			}
			else if (!str.trim().isEmpty())
			{
				this.collectionInstance.add(str.trim());
			}
		}
	}
}