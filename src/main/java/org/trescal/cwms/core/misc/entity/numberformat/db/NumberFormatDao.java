package org.trescal.cwms.core.misc.entity.numberformat.db;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.misc.entity.numberformat.NumberFormat;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;

public interface NumberFormatDao extends AllocatedDao<Company, NumberFormat, Integer> {
	
	NumberFormat find(NumerationType type, Company company);
}