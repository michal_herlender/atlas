package org.trescal.cwms.core.misc.tool;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class CustomLocalDateEditor extends PropertyEditorSupport {

    private final DateTimeFormatter formatter;

    public CustomLocalDateEditor(DateTimeFormatter formatter){
        super();
        this.formatter = formatter;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        try {
            setValue(LocalDate.parse(text, formatter));
        } catch (DateTimeParseException e){
            setValue(null);
        }
    }

    @Override
    public String getAsText() {
        return  getValue() != null? ((LocalDate) getValue()).format(formatter):"";
    }
}
