package org.trescal.cwms.core.misc.entity.hookactivity.db;

import lombok.val;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.misc.entity.hookactivity.HookActivity;
import org.trescal.cwms.core.misc.entity.hookactivity.HookActivity_;

import java.util.List;
import java.util.Optional;

@Repository("HookActivityDao")
public class HookActivityDaoImpl extends BaseDaoImpl<HookActivity, Integer> implements HookActivityDao {
	
	@Override
	protected Class<HookActivity> getEntity() {
		return HookActivity.class;
	}

	public List<HookActivity> findForHook(int hookId) {
		return getResultList(cb -> {
			val cq = cb.createQuery(HookActivity.class);
			val hook = cq.from(HookActivity.class);
			cq.where(cb.equal(hook.get(HookActivity_.hook), hookId));
			return cq;
		});
	}

	public Optional<HookActivity> findHookActivityForHookAndState(int hookId, int stateId) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(HookActivity.class);
			val hook = cq.from(HookActivity.class);
			cq.where(cb.equal(hook.get(HookActivity_.hook), hookId),
				cb.equal(hook.get(HookActivity_.state), stateId)
			);
			return cq;
		});
	}
}