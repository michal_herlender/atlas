package org.trescal.cwms.core.misc.entity.exception;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "exception")
public class Exception
{
	private String comment;
	private Date date;
	private String exception;
	private Integer id;
	private boolean reviewed;
	private String stackTrace;
	private String url;
	private String user;

	public Exception()
	{
		this.setReviewed(false);
	}

	@Column(name = "comment")
	public String getComment()
	{
		return this.comment;
	}

	@Column(name = "date", columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDate()
	{
		return this.date;
	}

	@Length(max = 1000)
	@Column(name = "exception", length = 1000)
	public String getException()
	{
		return this.exception;
	}

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId()
	{
		return this.id;
	}

	@Column(name = "stacktrace", columnDefinition="varchar(max)")
	public String getStackTrace()
	{
		return this.stackTrace;
	}

	@Column(name = "url")
	public String getUrl()
	{
		return this.url;
	}

	@Column(name = "userid", length = 20)
	public String getUser()
	{
		return this.user;
	}

	@NotNull
	@Column(name = "reviewed", nullable=false, columnDefinition="bit")
	public boolean isReviewed()
	{
		return this.reviewed;
	}

	public void setComment(String comment)
	{
		this.comment = comment;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public void setException(String exception)
	{
		this.exception = exception;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public void setReviewed(boolean reviewed)
	{
		this.reviewed = reviewed;
	}

	public void setStackTrace(String stackTrace)
	{
		this.stackTrace = stackTrace;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public void setUser(String user)
	{
		this.user = user;
	}

}
