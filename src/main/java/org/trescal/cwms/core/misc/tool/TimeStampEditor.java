package org.trescal.cwms.core.misc.tool;

import java.beans.PropertyEditorSupport;
import java.sql.Timestamp;

public class TimeStampEditor extends PropertyEditorSupport {
	 
    public void setAsText(String text) throws IllegalArgumentException {
        Timestamp time = Timestamp.valueOf(text);
        setValue(time);
    }
}