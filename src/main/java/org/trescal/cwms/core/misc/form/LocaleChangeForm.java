package org.trescal.cwms.core.misc.form;

import java.util.Locale;

public class LocaleChangeForm {
	private Locale desiredLocale;
	private String refererUrl;

	public Locale getDesiredLocale() {
		return desiredLocale;
	}

	public void setDesiredLocale(Locale desiredLocale) {
		this.desiredLocale = desiredLocale;
	}

	public String getRefererUrl() {
		return refererUrl;
	}

	public void setRefererUrl(String refererUrl) {
		this.refererUrl = refererUrl;
	}
}
