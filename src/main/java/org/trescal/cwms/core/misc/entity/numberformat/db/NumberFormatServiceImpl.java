package org.trescal.cwms.core.misc.entity.numberformat.db;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.misc.entity.numberformat.NumberFormat;
import org.trescal.cwms.core.misc.entity.numberformat.SplitNumberException;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;

@Service
public class NumberFormatServiceImpl extends BaseServiceImpl<NumberFormat, Integer> implements NumberFormatService {

	@Autowired
	private NumberFormatDao numberFormatDao;

	@Override
	protected BaseDao<NumberFormat, Integer> getBaseDao() {
		return numberFormatDao;
	}

	@Override
	public NumberFormat find(NumerationType type, Company company) {
		return numberFormatDao.find(type, company);
	}

	@Override
	public String generateNumber(NumerationType type, Integer counter, Integer year, Company company, Subdiv subdiv) {
		String format;
		try {
			format = this.find(type, company).getFormat();
		} catch (Exception ex) {
			format = type.getOrganisationLevel() == Company.class ? "lc-t-yy000000" : "lc-s-t-yy000000";
		} // TODO: default, should be replaced when the number format editor is
			// implemented?
		String number = "";
		int index = 0;
		while (index < format.length()) {
			switch (format.toLowerCase().charAt(index)) {
			case 'y':
				String rest = format.substring(index);
				if (rest.toLowerCase().startsWith("yyyy")) {
					number += year;
					index += 4;
				} else if (rest.toLowerCase().startsWith("yy")) {
					number += year % 100;
					index += 2;
				} else {
					number += year % 10;
					index += 1;
				}
				break;
			case 't':
				number += type.getToken();
				index += 1;
				break;
			case 'l':
				number += company == null || company.getCountry() == null ? "" : company.getCountry().getCountryCode();
				index += 1;
				break;
			case 'c':
				number += company == null || company.getCompanyCode() == null ? "" : company.getCompanyCode();
				index += 1;
				break;
			case 's':
				number += subdiv == null || subdiv.getSubdivCode() == null ? "" : subdiv.getSubdivCode();
				index += 1;
				break;
			case '#':
				int sharpCounter = 1;
				int max = 10;
				while (index + sharpCounter < format.length() && format.charAt(index + sharpCounter) == '#') {
					sharpCounter += 1;
					max *= 10;
				}
				number += counter % max;
				index += sharpCounter;
				break;
			case '0':
				int zeroCounter = 1;
				max = 10;
				while (index + zeroCounter < format.length() && format.charAt(index + zeroCounter) == '0') {
					zeroCounter += 1;
					max *= 10;
				}
				number += ("" + (counter % max + max)).substring(1);
				index += zeroCounter;
				break;
			case '\\':
				number += format.charAt(index + 1);
				index += 2;
				break;
			default:
				number += format.charAt(index);
				index += 1;
			}
		}
		return number;
	}

	@Override
	public Pair<String, String> splitNumber(String number, NumerationType type, Company company, Subdiv subdiv) {
		String format;
		try {
			format = this.find(type, company).getFormat();
		} catch (Exception ex) {
			format = type.getOrganisationLevel() == Company.class ? "lc-t-yy000000" : "lc-s-t-yy000000";
		} // TODO: default, should be replaced when the number format editor is
			// implemented?
		int formatIndex = 0;
		int numberIndex = 0;
		String left = new String();
		String right = new String();
		while (formatIndex < format.length()) {
			switch (format.toLowerCase().charAt(formatIndex)) {
			case 'y':
				String rest = format.substring(formatIndex);
				if (rest.toLowerCase().startsWith("yyyy")) {
					String year = number.substring(numberIndex, numberIndex + 4);
					if (year.matches("\\d{4}")) {
						right += year;
						numberIndex += 4;
						formatIndex += 4;
					} else
						throw new SplitNumberException(
								"Four digits for year expected, but '" + number.substring(numberIndex) + "' found.");
				} else if (rest.toLowerCase().startsWith("yy")) {
					String year = number.substring(numberIndex, numberIndex + 2);
					if (year.matches("\\d{2}")) {
						right += year;
						numberIndex += 2;
						formatIndex += 2;
					} else
						throw new SplitNumberException(
								"Two digits for year expected, but '" + number.substring(numberIndex) + "' found.");
				} else {
					Character year = number.charAt(numberIndex);
					if (Character.isDigit(year)) {
						right += year;
						numberIndex += 1;
						formatIndex += 1;
					} else
						throw new SplitNumberException(
								"One digits for year expected, but '" + number.substring(numberIndex) + "' found.");
				}
				break;
			case 't':
				if (number.substring(numberIndex).startsWith(type.getToken())) {
					right += type.getToken();
					numberIndex += type.getToken().length();
					formatIndex += 1;
				} else
					throw new SplitNumberException("Numeration token '" + type.getToken() + "' expected, but '"
							+ number.substring(numberIndex) + "' found.");
				break;
			case 'l':
				String countryCode = company == null || company.getCountry() == null ? ""
						: company.getCountry().getCountryCode();
				if (number.substring(numberIndex).startsWith(countryCode)) {
					left += countryCode;
					numberIndex += countryCode.length();
					formatIndex += 1;
				} else
					throw new SplitNumberException("Country code '" + countryCode + "' expected, but '"
							+ number.substring(numberIndex) + "' found.");
				break;
			case 'c':
				String companyCode = company == null ? "" : company.getCompanyCode();
				if (number.substring(numberIndex).startsWith(companyCode)) {
					left += companyCode;
					numberIndex += companyCode.length();
					formatIndex += 1;
				} else
					throw new SplitNumberException("Company code '" + companyCode + "' expected, but '"
							+ number.substring(numberIndex) + "' found.");
				break;
			case 's':
				String subdivCode = subdiv == null ? "" : subdiv.getSubdivCode();
				if (number.substring(numberIndex).startsWith(subdivCode)) {
					left += subdivCode;
					numberIndex += subdivCode.length();
					formatIndex += 1;
				} else
					throw new SplitNumberException("Subdiv code '" + subdivCode + "' expected, but '"
							+ number.substring(numberIndex) + "' found.");
				break;
			case '\\':
				Character fixChar = format.charAt(formatIndex + 1);
				if (fixChar.equals(number.charAt(numberIndex))) {
					left += fixChar;
					numberIndex += 1;
					formatIndex += 2;
				} else
					throw new SplitNumberException(
							"Character '" + fixChar + "' expected, but '" + number.substring(numberIndex) + "' found.");
				break;
			case '#':
				while (format.charAt(formatIndex) == '#') {
					if (Character.isDigit(number.charAt(numberIndex)))
						numberIndex++;
					formatIndex++;
				}
				break;
			case '0':
				int zeroCounter = 1;
				while (formatIndex + zeroCounter < format.length() && format.charAt(formatIndex + zeroCounter) == '0')
					zeroCounter += 1;
				String ongoingNo = number.substring(numberIndex, numberIndex + zeroCounter);
				if (ongoingNo.matches("\\d{" + zeroCounter + "}")) {
					right += ongoingNo;
					numberIndex += zeroCounter;
					formatIndex += zeroCounter;
				} else
					throw new SplitNumberException(
							zeroCounter + "digit(s) expected, but '" + number.substring(numberIndex) + "' found.");
				break;
			default:
				Character fixChar2 = format.charAt(formatIndex);
				if (fixChar2.equals(number.charAt(numberIndex))) {
					left += fixChar2;
					numberIndex += 1;
					formatIndex += 1;
				} else
					throw new SplitNumberException("Character '" + fixChar2 + "' expected, but '"
							+ number.substring(numberIndex) + "' found.");
			}
		}
		return Pair.of(left, right);
	}
}