package org.trescal.cwms.core.misc.entity.numberformat.db;

import org.apache.commons.lang3.tuple.Pair;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.misc.entity.numberformat.NumberFormat;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;

public interface NumberFormatService extends BaseService<NumberFormat, Integer> {

	NumberFormat find(NumerationType type, Company company);

	String generateNumber(NumerationType type, Integer counter, Integer year, Company company, Subdiv subdiv);

	/**
	 * Split the number in two parts. The left part contains country, company
	 * and subdivision code in the order they occur inside the number. The right
	 * part contains the numeration type token and all digits, which are not
	 * part of the codes. All other signs, e.g. separators, are omitted.
	 * 
	 * ATTENTION:
	 * 
	 * 1) If some of the codes or the number format has changed in between, a
	 * SplitNumberException should be thrown (if you're not really unlucky and
	 * something crazy happens).
	 * 
	 * 2) If the number format contains #'s (optional digits) followed by
	 * something, which starts with a digit (either '0', 'y' or maybe a code),
	 * then the parsing will fail and a SplitNumberException will be thrown.
	 * 
	 * @param number
	 *            number, which should be split
	 * @param type
	 *            numeration type of the number
	 * @param company
	 *            business company
	 * @param subdiv
	 *            business subdivision
	 * @return left and right part of split number
	 */
	Pair<String, String> splitNumber(String number, NumerationType type, Company company, Subdiv subdiv);
}