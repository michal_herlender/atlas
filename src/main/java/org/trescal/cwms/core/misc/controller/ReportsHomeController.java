package org.trescal.cwms.core.misc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller @IntranetController
public class ReportsHomeController
{
	@RequestMapping(value="/reports.htm")
	protected String handleRequest() throws Exception
	{
		return "/trescal/core/reports/reportshome";
	}
}