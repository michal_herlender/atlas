package org.trescal.cwms.core.misc.entity.exception.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.misc.entity.exception.Exception;

public interface ExceptionDao extends BaseDao<Exception, Integer> {}