package org.trescal.cwms.core.misc.entity.hookactivity.db;

import org.trescal.cwms.core.misc.entity.hookactivity.HookActivity;

import java.util.List;
import java.util.Optional;

public class HookActivityServiceImpl implements HookActivityService
{
	HookActivityDao hookActivityDao;

	@Override
	public void deleteHookActivity(HookActivity hookactivity)
	{
		this.hookActivityDao.remove(hookactivity);
	}

	@Override
	public HookActivity findHookActivity(int id)
	{
		return this.hookActivityDao.find(id);
	}

	@Override
	public List<HookActivity> findForHook(int hookId) {
		return this.hookActivityDao.findForHook(hookId);
	}

	@Override
	public Optional<HookActivity> findHookActivityForHookAndState(int hookId, int stateId) {
		return this.hookActivityDao.findHookActivityForHookAndState(hookId, stateId);
	}

	@Override
	public List<HookActivity> getAllHookActivitys()
	{
		return this.hookActivityDao.findAll();
	}

	@Override
	public void insertHookActivity(HookActivity HookActivity)
	{
		this.hookActivityDao.persist(HookActivity);
	}

	@Override
	public void saveOrUpdateHookActivity(HookActivity hookactivity) {
		this.hookActivityDao.merge(hookactivity);
	}

	public void setHookActivityDao(HookActivityDao hookActivityDao)
	{
		this.hookActivityDao = hookActivityDao;
	}

	@Override
	public void updateHookActivity(HookActivity HookActivity) {
		this.hookActivityDao.merge(HookActivity);
	}
}