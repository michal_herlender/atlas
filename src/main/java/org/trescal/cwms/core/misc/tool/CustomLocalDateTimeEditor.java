package org.trescal.cwms.core.misc.tool;

import java.beans.PropertyEditorSupport;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class CustomLocalDateTimeEditor extends PropertyEditorSupport {

    private final DateTimeFormatter formatter;

    public CustomLocalDateTimeEditor(DateTimeFormatter formatter) {
        super();
        this.formatter = formatter;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        try {
            setValue(LocalDateTime.parse(text, formatter));
        } catch (DateTimeParseException e) {
            setValue(null);
        }
    }

    @Override
    public String getAsText() {
        return getValue() != null ? ((LocalDateTime) getValue()).format(formatter) : "";
    }
}
