package org.trescal.cwms.core.misc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.entity.exception.db.ExceptionService;

@Controller @IntranetController
public class ExceptionViewController
{
	@Autowired
	private ExceptionService exceptionServ;
	
	@RequestMapping(value="/viewexceptions.htm")
	public ModelAndView handleRequest() throws Exception
	{
		return new ModelAndView("trescal/core/misc/exceptionview", "exceptions", exceptionServ.getAllExceptions());
	}
}