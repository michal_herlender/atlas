package org.trescal.cwms.core.misc.entity.hookactivity.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.misc.entity.hookactivity.HookActivity;

import java.util.List;
import java.util.Optional;

public interface HookActivityDao extends BaseDao<HookActivity, Integer> {

	List<HookActivity> findForHook(int hookId);

	Optional<HookActivity> findHookActivityForHookAndState(int hookId, int stateId);
}