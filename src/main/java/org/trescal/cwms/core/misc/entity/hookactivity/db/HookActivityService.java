package org.trescal.cwms.core.misc.entity.hookactivity.db;

import org.trescal.cwms.core.misc.entity.hookactivity.HookActivity;

import java.util.List;
import java.util.Optional;

public interface HookActivityService {
    void deleteHookActivity(HookActivity hookactivity);

    HookActivity findHookActivity(int id);

    List<HookActivity> findForHook(int hookId);

    Optional<HookActivity> findHookActivityForHookAndState(int hookId, int stateId);

    List<HookActivity> getAllHookActivitys();

    void insertHookActivity(HookActivity hookactivity);

    void saveOrUpdateHookActivity(HookActivity hookactivity);

    void updateHookActivity(HookActivity hookactivity);
}