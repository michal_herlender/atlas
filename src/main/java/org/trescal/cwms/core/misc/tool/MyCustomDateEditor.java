package org.trescal.cwms.core.misc.tool;

import java.text.DateFormat;

import org.springframework.beans.propertyeditors.CustomDateEditor;

public class MyCustomDateEditor extends CustomDateEditor {
	
	public MyCustomDateEditor(DateFormat dt, boolean allowEmpty) {
		super(dt, allowEmpty);
	}

	public void setAsText(String text) {
		super.setAsText(text);
	}

	public String getAsText() {
		if (getValue() == null) {
			return "";
		} else {
			return super.getAsText();
		}
	}
}
