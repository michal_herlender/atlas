package org.trescal.cwms.core.misc.controller;

import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.form.AddFeedbackForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_FeedbackConfirmation;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_FeedbackConfirmation.EmailType;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

/**
 * Temporary web feedback form.
 * 
 * @author richard
 */
@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class AddFeedbackController
{
	@Autowired
	private EmailService emailServ;
	@Autowired
	private UserService userService;
	@Autowired
	private ServletContext servletContext;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private MessageSource messages;
	@Autowired
	private EmailContent_FeedbackConfirmation emailContentFeedbackConfirmation;
	
	@Value("#{props['cwms.admin.email']}")
	private String cwmsAdminEmail;
	@Value("#{props['cwms.config.email.noreply']}")
	private String noreplyAddress;
	
	@ModelAttribute("command")
	protected AddFeedbackForm formBackingObject(HttpServletRequest request) throws Exception
	{
		AddFeedbackForm form = new AddFeedbackForm();
		form.setUrl(ServletRequestUtils.getStringParameter(request, "url", ""));
		//ServletRequestUtils.getStringParameter(request, "referrer");
		HttpSession session = request.getSession(true);
		if (session.getAttribute("feedbackleft") != null) {
			form.setFeedbackLeft(true);
		}
		else {
			session.setAttribute("url", form.getUrl());
		}
		session.removeAttribute("feedbackleft");
		return form;
	}
	
	@RequestMapping(value="/addfeedback.htm", method=RequestMethod.POST)
	protected RedirectView onSubmit(HttpServletRequest request, 
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("command") AddFeedbackForm form) throws Exception
	{
		//TODO : consider sending to local country support in local locale, or just use English.
		Locale locale = this.supportedLocaleService.getPrimaryLocale();
		
		request.getSession().setAttribute("feedbackleft", true);
		if (form.getUrl() != null && form.getUrl().indexOf(',') > -1) {
			form.setUrl(form.getUrl().substring(0, form.getUrl().indexOf(',')));
		}
		Contact contact = this.userService.get(username).getCon();
		String build = servletContext.getInitParameter("build-number");
		String requestURI = request.getRequestURI();
		String userAgent = request.getHeader("user-agent");
		String title = form.getTitle();
		String description = form.getDescription();
		
		EmailContentDto contentDto = this.emailContentFeedbackConfirmation.getContent(contact, description, title, build, requestURI, userAgent, locale, EmailType.INTERNAL);
		// create email text
		String htmlText = contentDto.getBody();
		String subject = contentDto.getSubject();
		this.emailServ.sendBasicEmail(subject, noreplyAddress, cwmsAdminEmail, htmlText);
		// create e-mail for auto-ticket creation at assembla
		String ticketSubject = this.messages.getMessage("email.feedbackconfirmation.subjectforassembla", 
				new Object[] {contact.getName(),form.getTitle()}, 
				"Feedback [" + 
				contact.getName() + 
				"]: " + 
				form.getTitle(), locale);
 		String ticketBody = this.messages.getMessage("email.feedbackconfirmation.body", 
 				new Object[] {form.getDescription()}, 
 				"Milestone: Unsorted Feedback<br/>" + 
 				"Priority: 3<br/>" + 
 				"Assigned-to: Stuart Harrold<br/>" + 
 				"Description:<br/>" + 
 				form.getDescription() + 
 				"<br />.<br/>", locale);
		this.emailServ.sendBasicEmail(ticketSubject, cwmsAdminEmail, "cwms@tickets.assembla.com", ticketBody);
		
		return new RedirectView("/addfeedback.htm", true);
	}
	
	@RequestMapping(value="/addfeedback.htm", method=RequestMethod.GET)
	protected String formView() {
		return "trescal/core/misc/addfeedback";
	}
}
