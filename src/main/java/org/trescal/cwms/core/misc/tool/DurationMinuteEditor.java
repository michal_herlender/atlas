package org.trescal.cwms.core.misc.tool;

import java.beans.PropertyEditorSupport;
import java.time.Duration;

public class DurationMinuteEditor extends PropertyEditorSupport {

	@Override
	public String getAsText() throws ClassCastException {
		Duration duration = (Duration) getValue();
		return duration == null ? "" : "" + duration.toMinutes();
	}
	
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (!text.isEmpty()) {
			Long minutes = Long.parseLong(text);
			setValue(Duration.ofMinutes(minutes));
		}
		else  {
			setValue(null);
		}
	}
}