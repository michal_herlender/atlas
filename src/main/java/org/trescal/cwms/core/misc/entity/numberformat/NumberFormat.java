package org.trescal.cwms.core.misc.entity.numberformat;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;

@Entity
@Table(name = "numberformat", uniqueConstraints= {@UniqueConstraint(columnNames= {"orgid", "numerationType"})})
public class NumberFormat extends Allocated<Company>
{
	private Integer id;
	private NumerationType numerationType;
	private String format;
	private boolean global;
	private boolean universal;
	
	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}
	
	@Enumerated(EnumType.STRING)
	public NumerationType getNumerationType() {
		return numerationType;
	}
	
	public String getFormat() {
		return format;
	}
	
	public boolean isGlobal() {
		return global;
	}
	
	public boolean isUniversal() {
		return universal;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setNumerationType(NumerationType numerationType) {
		this.numerationType = numerationType;
	}
	
	public void setFormat(String format) {
		this.format = format;
	}
	
	public void setGlobal(boolean global) {
		this.global = global;
	}
	
	public void setUniversal(boolean universal) {
		this.universal = universal;
	}
}