package org.trescal.cwms.core.misc.entity.numeration.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.misc.entity.numeration.Numeration;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.Numeration_;

@Repository
public class NumerationDaoImpl extends AllocatedToCompanyDaoImpl<Numeration, Integer> implements NumerationDao {

	@Override
	protected Class<Numeration> getEntity() {
		return Numeration.class;
	}

	@Override
	public Numeration get(NumerationType type, Company company, Integer year) {
		return getSingleResult(cb -> {
			CriteriaQuery<Numeration> cq = cb.createQuery(Numeration.class);
			Root<Numeration> root = cq.from(Numeration.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(Numeration_.numerationType), type));
			clauses.getExpressions().add(cb.equal(root.get(Numeration_.organisation), company));
			clauses.getExpressions().add(cb.equal(root.get(Numeration_.year), year));
			cq.where(clauses);
			return cq;
		});
	}
}