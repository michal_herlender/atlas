package org.trescal.cwms.core.misc.entity.exception.db;

import java.util.List;

import org.trescal.cwms.core.misc.entity.exception.Exception;

public interface ExceptionService
{
	Exception findException(int id);
	void insertException(Exception exception);
	void updateException(Exception exception);
	List<Exception> getAllExceptions();
	void changeException(int id, String comment, boolean reviewed);
}