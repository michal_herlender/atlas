package org.trescal.cwms.core.dwr;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.trescal.cwms.core.company.entity.contact.Contact;

/**
 * Interface for performing general DWR-based functions
 * 
 * @author jamiev
 */
public interface DWRService
{
	/**
	 * Accessor method used just for unit testing that allows us to add objects
	 * to the mock dwr session vairable. This allows us to mock session
	 * attributes that would normally be fetched by the dwe {@link WebContext}
	 * session. Has no function outside of unit testing.
	 * 
	 * @param attributeName the name of the attribute in the session.
	 * @param attributeValue the value of the attribute to add to the session.
	 */
	void addSessionAttribute(String attributeName, Object attributeValue);

	/**
	 * Returns a string arraylist of usernames who are currently logged into the
	 * system using the session registry of acegi.
	 * 
	 * @return arraylist of users currently logged into the system.
	 */
	ArrayList<String> getAllLoggedInUsers();

	/**
	 * Interrogates the the {@link HttpSession} for an attribute with the given
	 * name and attempts to cast the object (if found) into a {@link Class} of
	 * the given type. If none is found then null is returned. Strategy is to
	 * check the ajax session first before then attempting to access the
	 * {@link HttpServletRequest}.
	 * 
	 * @param classKey the {@link HttpSession} attribute name.
	 * @return the {@link Class} or null if not found.
	 */
	public Class<?> getClass(String classKey, HttpServletRequest request);

	/**
	 * Should be replaced with username based lookup and deleted - contact in session is stale and can lead to proxying issues
	 * 2016-05-05 GB
	 * Returns the current user contact stored in the session.
	 * @return the {@link Contact} currently using the system OR null if there
	 *         is an error getting this information from the session.
	 */
	@Deprecated
	Contact getCurrentUser();

	/**
	 * Returns the {@link Object} currently stored under the given attribute in
	 * the session.
	 * 
	 * @param attrName the name of the attribute.
	 * @param request the {@link HttpServletRequest} to get the attribute from
	 *        as a fallback.
	 * @return the {@link Object}
	 */
	Object getDWRSessionAttribute(String attrName, HttpServletRequest request);

	/**
	 * Attempts to get an {@link HttpServletRequest} object from dwr and if not
	 * returns null.
	 * 
	 * @return {@link HttpServletRequest} or null.
	 */
	HttpServletRequest getHttpServletRequest();

	/**
	 * Attempts to use DWR to return the realpath to the given resource, if
	 * there is no access to the dwr scope then the {@link HttpServletRequest}
	 * will be used instead.
	 * 
	 * @param request {@link HttpServletRequest}.
	 * @param resource the resource on the webapp file system to get the path
	 *        to.
	 * @return the path or null if not found.
	 */
	String getRealPath(HttpServletRequest request, String resource);

	void registerUserSession();

	/**
	 * As with addSessionAttribute(String attributeName, Object attributeValue),
	 * this function is only intended for use with unit tests and has no
	 * implementation in the application proper. Function removes the mock
	 * session attribute identified by the given key. Intended to be called from
	 * 
	 * @after unit test functions to clean up following a unit test.
	 * @param attributeName
	 */
	void removeSessionAttribute(String attributeName);

	/**
	 * sets session variable for menu navigation visibility
	 * 
	 * @param visible boolean to indicate if the menu should be visible
	 */
	void setMenuVisibility(boolean visible);

	/**
	 * sets session attribute for the supplied notes visibility
	 * 
	 * @param attributeName string attribute name which refers to only one
	 *        entity id
	 * @param visible boolean to indicate if the notes should be visible
	 */
	void setNoteVisibility(String attributeName, boolean visible);

	/**
	 * sets session attribute for the supplied scroll top value
	 * 
	 * @param attributeName string attribute name which refers to only one
	 *        entity id
	 * @param scrollTop the scroll top id of element on page
	 */
	void setScrollTopMarker(String attributeName, String scrollTop);
}
