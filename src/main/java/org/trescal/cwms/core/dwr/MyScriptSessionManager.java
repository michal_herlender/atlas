package org.trescal.cwms.core.dwr;

import java.util.HashMap;
import java.util.Map;

import org.directwebremoting.impl.DefaultScriptSessionManager;

public class MyScriptSessionManager extends DefaultScriptSessionManager
{
	public static Map<String, String> effectiveMap;

	public MyScriptSessionManager()
	{
		MyScriptSessionManager.effectiveMap = new HashMap<String, String>();
		this.addScriptSessionListener(new MyScriptSessionListener());
	}
}
