package org.trescal.cwms.core.dwr;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletContext;

import org.directwebremoting.hibernate.H4SessionAjaxFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/*
 * Required to make Hibernate4 converter aware of the sessionFactory.
 * GB 2015-02-22 
 */
@Component
public class H4BeanConverterInitializer {
	
	@PersistenceContext(unitName="entityManagerFactory")
	private EntityManager entityManager;
	@Autowired
	private ServletContext servletContext;
	
	@PostConstruct
	public void setup() {
		H4SessionAjaxFilter.setEntityManagerFactory(servletContext, entityManager);
	}
}