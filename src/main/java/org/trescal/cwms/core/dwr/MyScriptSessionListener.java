package org.trescal.cwms.core.dwr;

import org.directwebremoting.ScriptSession;
import org.directwebremoting.WebContextFactory;
import org.directwebremoting.event.ScriptSessionEvent;
import org.directwebremoting.event.ScriptSessionListener;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.Constants;

import javax.servlet.http.HttpSession;

public class MyScriptSessionListener implements ScriptSessionListener {
	@Override
	public void sessionCreated(ScriptSessionEvent event) {
		HttpSession session = WebContextFactory.get().getSession();
		ScriptSession scriptSession = event.getSession();
		Contact con = (Contact) session.getAttribute(Constants.SESSION_ATTRIBUTE_CONTACT);
		if (con == null) {
			scriptSession.invalidate();
		} else {
			scriptSession.setAttribute(Constants.SESSION_ATTRIBUTE_CONTACT, con);
			scriptSession.setAttribute("httpSessionId", session.getId());
			MyScriptSessionManager.effectiveMap.put(session.getId(), scriptSession.getId());
		}
	}

	@Override
	public void sessionDestroyed(ScriptSessionEvent event)
	{
	}
}
