package org.trescal.cwms.core.dwr;

import java.util.ArrayList;
import java.util.List;

//import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.springframework.beans.BeansException;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.session.SessionRegistry;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.Constants;

public class DWRServiceImpl implements DWRService, ApplicationContextAware
{
	private ApplicationContext applicationContext;
	protected final Log logger = LogFactory.getLog(this.getClass());
//	@Autowired
//	private ServletContext servletContext;

	@Override
	public void addSessionAttribute(String attributeName, Object attributeValue)
	{

	}

	public ArrayList<String> getAllLoggedInUsers()
	{
		// get web context
		//WebContext wctx = WebContextFactory.get();

		// initialise session registry
		//SessionRegistry sr = null;

		// web context may not be available if calling this from another webapp
		// via web services, so fall back and get the bean from the application
		// context
		/*if (wctx == null)
		{
			// get the session registry
			sr = (SessionRegistry) this.applicationContext.getBean("sessionRegistry");
		}*/
		// otherwise, use the same way as it has always been done, getting the
		// context from the web context
		/*else
		{
			// get servlet context
			// WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(wctx.getHttpServletRequest().getSession(true).getServletContext());
			// get the session registry
			sr = (SessionRegistry) ctx.getBean("sessionRegistry");
		}*/
		// GB 2015-05-04 Commented out all the above as part of servlet 3.1 upgrade, this should do it: 
		SessionRegistry sr = (SessionRegistry) applicationContext.getBean("sessionRegistry");
		
		// get all principals from session registry
		List<Object> principals = sr.getAllPrincipals();
		ArrayList<String> loggedInList = new ArrayList<String>();
		// add all usernames to arraylist
		for (Object prince : principals)
		{
			loggedInList.add(prince.toString());
		}

		return loggedInList;
	}

	@Override
	public Class<?> getClass(String classKey, HttpServletRequest request)
	{
		Class<?> clazz = null;
		try
		{
			WebContext wctx = WebContextFactory.get();
			if (wctx != null)
			{
				if (wctx.getHttpServletRequest().getSession(true).getAttribute(classKey) != null)
				{
					clazz = (Class<?>) wctx.getHttpServletRequest().getSession(true).getAttribute(classKey);
				}
			}
			else if (request != null)
			{
				if (request.getSession().getAttribute(classKey) != null)
				{
					clazz = (Class<?>) request.getSession().getAttribute(classKey);
				}
			}

		}
		catch (Exception e)
		{
			this.logger.info("There was an error retrieving the attribute '"
					+ classKey + "' from the session: ");
			this.logger.info(e.toString());
		}
		return clazz;
	}

	/*
	 * Should be replaced with username based lookup and deleted - contact in session is stale and can lead to proxying issues
	 * 2016-05-05 GB
	 */
	@Deprecated
	public Contact getCurrentUser()
	{
		try
		{
			WebContext wctx = WebContextFactory.get();
			return (Contact) wctx.getHttpServletRequest().getSession(true).getAttribute(Constants.SESSION_ATTRIBUTE_CONTACT);
		}
		catch (NullPointerException e)
		{
			this.logger.info("There was an error retrieving the contact from the session: ");
			this.logger.info(e.toString());
			return null;
		}
	}

	public Object getDWRSessionAttribute(String attrName, HttpServletRequest request)
	{
		try
		{
			WebContext wctx = WebContextFactory.get();
			if (wctx != null)
			{
				return wctx.getHttpServletRequest().getSession(true).getAttribute(attrName);
			}
			else if (request != null)
			{
				return request.getSession().getAttribute(attrName);
			}
			else
			{
				return null;
			}
		}
		catch (Exception e)
		{
			this.logger.info("There was an error retrieving the attribute '"
					+ attrName + "' from the session: ");
			this.logger.info(e.toString());
			return null;
		}
	}

	@Override
	public HttpServletRequest getHttpServletRequest()
	{
		WebContext wctx = WebContextFactory.get();
		if (wctx != null)
		{
			return wctx.getHttpServletRequest();
		}
		else
		{
			return null;
		}
	}

	@Override
	public String getRealPath(HttpServletRequest request, String resource)
	{
		String path = "";
		WebContext wctx = WebContextFactory.get();
		if (wctx != null)
		{
			path = wctx.getHttpServletRequest().getSession().getServletContext().getRealPath("/".concat(resource));
			//servletContext.getRealPath("/".concat(resource));
		}
		else if (request != null)
		{
			path = request.getSession().getServletContext().getRealPath("/".concat(resource));
			//path = servletContext.getRealPath("/".concat(resource));
		}
		return path;
	}

	public void registerUserSession()
	{
		WebContext wctx = WebContextFactory.get();

		Contact con = (Contact) wctx.getHttpServletRequest().getSession(true).getAttribute(Constants.SESSION_ATTRIBUTE_CONTACT);

		if (wctx.getScriptSession().getAttribute("con") == null)
		{
			this.logger.info("Adding " + con.getName()
					+ " to the DWR session (from " + wctx.getCurrentPage()
					+ ")");

			// try and add him to the script scope
			wctx.getScriptSession().setAttribute("con", con);
		}
	}

	@Override
	public void removeSessionAttribute(String attributeName)
	{

	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
	{
		this.applicationContext = applicationContext;
	}

	@Override
	public void setMenuVisibility(boolean visible)
	{
		WebContext wctx = WebContextFactory.get();
		wctx.getHttpServletRequest().getSession(true).setAttribute("navigationVisibility", visible);
	}

	@Override
	public void setNoteVisibility(String attributeName, boolean visible)
	{
		WebContext wctx = WebContextFactory.get();
		wctx.getHttpServletRequest().getSession(true).setAttribute(attributeName, visible);
	}

	@Override
	public void setScrollTopMarker(String attributeName, String scrollTop)
	{
		WebContext wctx = WebContextFactory.get();
		wctx.getHttpServletRequest().getSession(true).setAttribute(attributeName, scrollTop);
	}
}
