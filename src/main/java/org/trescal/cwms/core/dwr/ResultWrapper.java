/**
 * 
 */
package org.trescal.cwms.core.dwr;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * Wrapper class used to return the results of a (typically DWR) service
 * Function.
 * 
 * @author Richard
 */
public class ResultWrapper {

	public static class ResultWrapperJsonView {
	}

	@JsonView(ResultWrapperJsonView.class)
	protected List<ObjectError> errorList;
	
	@JsonView(ResultWrapperJsonView.class)
	protected BindException errors;
	
	@JsonView(ResultWrapperJsonView.class)
	protected String message;
	
	@JsonView(ResultWrapperJsonView.class)
	protected String code;
	
	@JsonView(ResultWrapperJsonView.class)
	protected Object results;
	
	@JsonView(ResultWrapperJsonView.class)
	protected boolean success;

	public ResultWrapper() {

	}

	/**
	 * Constructor for {@link ResultWrapper}'s created via spring validation.
	 * Constructor attempts to decide on the outcome of the operation based on
	 * the errors object it receives.
	 * 
	 * @param errors
	 *            {@link BindException} results of spring validation.
	 * @param results
	 *            entities returned by the operation.
	 */
	public ResultWrapper(BindException errors, Object results) {
		if (errors.hasErrors()) {
			this.success = false;
			this.setErrors(errors);
		} else {
			this.success = true;
			this.results = results;
			this.setErrors(errors);
		}
	}

	public ResultWrapper(boolean success, String message) {
		super();
		this.success = success;
		this.message = message;
		this.results = null;
		this.errors = null;
	}
	
	public ResultWrapper(boolean success, String message, String code) {
		super();
		this.success = success;
		this.message = message;
		this.code = code;
		this.results = null;
		this.errors = null;
	}

	/**
	 * @param success
	 *            true if the operation succeded.
	 * @param message
	 */
	public ResultWrapper(boolean success, String message, Object results, BindException errors) {
		super();
		this.success = success;
		this.message = message;
		this.results = results;
		this.errors = errors;
		this.errorList = this.errors != null ? this.errors.getAllErrors() : new ArrayList<ObjectError>();
	}

	public List<ObjectError> getErrorList() {
		return this.errorList;
	}

	public BindException getErrors() {
		return this.errors;
	}

	public String getMessage() {
		return this.message;
	}

	public Object getResults() {
		return this.results;
	}
	
	public String getCode() {
		return code;
	}

	public boolean isSuccess() {
		return this.success;
	}

	public void setErrorList(List<ObjectError> errorList) {
		this.errorList = errorList;
	}

	public void setErrors(BindException errors) {
		this.errors = errors;
		this.errorList = this.errors != null ? this.errors.getAllErrors() : new ArrayList<ObjectError>();
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public void setCode(String code) {
		this.code = code;
	}

	public void setResults(Object results) {
		this.results = results;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
