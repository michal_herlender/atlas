package org.trescal.cwms.core.dwr;

import java.util.HashSet;
import java.util.Set;

import org.directwebremoting.ScriptSession;

/**
 * First attempt at a single Reverse Ajax Thread through which all functions are
 * executed. Not yet implemented anywhere.
 * 
 * @author JamieV
 */
public class ReverseAjaxThread
{
	private static ReverseAjaxThread INSTANCE;

	public static synchronized ReverseAjaxThread getInstance() throws InterruptedException
	{
		if (INSTANCE == null)
		{
			INSTANCE = new ReverseAjaxThread();
			INSTANCE.run();
		}
		return INSTANCE;
	}

	private Set<ScriptSession> scriptSessions = new HashSet<ScriptSession>();

	public synchronized void addScriptSession(ScriptSession scriptSession)
	{
		// use a copy so that code reading from scriptSessions does not need to
		// be synchronized
		Set<ScriptSession> scriptSessionsCopy = new HashSet<ScriptSession>(this.scriptSessions);
		scriptSessionsCopy.add(scriptSession);
		this.scriptSessions = scriptSessionsCopy;
	}

	public void run() throws InterruptedException
	{
		while (true)
		{
			for (ScriptSession scriptSession : this.scriptSessions)
			{
				if (!scriptSession.isInvalidated())
				{
					// alert('hello')
					// new ScriptProxy(scriptSession).addFunctionCall("alert",
					// "hello");
				}
				else
				{
					synchronized (this)
					{
						this.scriptSessions.remove(scriptSession);
					}
				}
			}
			Thread.sleep(10000); // sleep for 10 seconds
		}
	}

}
