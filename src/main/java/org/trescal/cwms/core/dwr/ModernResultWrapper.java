package org.trescal.cwms.core.dwr;

import lombok.EqualsAndHashCode;
import lombok.Value;

public abstract class ModernResultWrapper<T> {

    public static <T> ModernResultWrapper<T> success(T result){
        return new SuccessResultWrapper<>(result);
    }

    public static <T> ModernResultWrapper<T> failure(String message){
        return new FailureResultWrapper<>(message);
    }

    @Value @EqualsAndHashCode(callSuper = true)
    private  static  class SuccessResultWrapper<T> extends ModernResultWrapper<T> {

        Boolean isValid ;
        T result;
        SuccessResultWrapper(T result) {
            super();
            this.isValid = true;
            this.result = result;
        }

    }

    @Value @EqualsAndHashCode(callSuper = true)
    private  static class FailureResultWrapper<T> extends ModernResultWrapper<T> {

        String message;
        Boolean isValid ;

        FailureResultWrapper(String message) {
            super();
            this.isValid = false;
            this.message = message;
        }
    }
}


