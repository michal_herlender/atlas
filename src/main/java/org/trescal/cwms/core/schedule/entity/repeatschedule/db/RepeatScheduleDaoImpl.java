package org.trescal.cwms.core.schedule.entity.repeatschedule.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption_;
import org.trescal.cwms.core.schedule.entity.repeatschedule.RepeatSchedule;
import org.trescal.cwms.core.schedule.entity.repeatschedule.RepeatSchedule_;

@Repository("RepeatScheduleDao")
public class RepeatScheduleDaoImpl extends BaseDaoImpl<RepeatSchedule, Integer> implements RepeatScheduleDao {
	@Override
	protected Class<RepeatSchedule> getEntity() {
		return RepeatSchedule.class;
	}

	@Override
	public List<RepeatSchedule> getAllActive() {
		return getResultList(cb -> {
			CriteriaQuery<RepeatSchedule> cq = cb.createQuery(RepeatSchedule.class);
			Root<RepeatSchedule> root = cq.from(RepeatSchedule.class);
			cq.where(cb.isTrue(root.get(RepeatSchedule_.active)));
			return cq;
		});
	}

	@Override
	public List<RepeatSchedule> getRepeatSchedulesFrom_Contact_TransportOption_Address(Integer transportOptionId, Integer contactId, Integer addressId) {
		return getResultList(cb -> {
			CriteriaQuery<RepeatSchedule> cq = cb.createQuery(RepeatSchedule.class);
			Root<RepeatSchedule> root = cq.from(RepeatSchedule.class);
			Join<RepeatSchedule, TransportOption> transportOption = root.join(RepeatSchedule_.transportOption);
			Join<RepeatSchedule, Contact> contact = root.join(RepeatSchedule_.con);
			Join<RepeatSchedule, Address> address = root.join(RepeatSchedule_.addr);

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(root.get(RepeatSchedule_.active)));
			clauses.getExpressions().add(cb.equal(transportOption.get(TransportOption_.id), transportOptionId));
			clauses.getExpressions().add(cb.equal(contact.get(Contact_.personid), contactId));
			clauses.getExpressions().add(cb.equal(address.get(Address_.addrid), addressId));

			cq.where(clauses);
			return cq;
		});
	}

}