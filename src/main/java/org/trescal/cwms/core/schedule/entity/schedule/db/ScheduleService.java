package org.trescal.cwms.core.schedule.entity.schedule.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.schedule.dto.CompanyWebSchedulesDTO;
import org.trescal.cwms.core.schedule.dto.ScheduleDTO;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;
import org.trescal.cwms.core.schedule.form.CreateScheduleForm;
import org.trescal.cwms.core.schedule.form.EditScheduleForm;
import org.trescal.cwms.core.schedule.form.ScheduleSearchForm;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * Interface for accessing and manipulating {@link Schedule} entities.
 *
 * @author jamiev
 */
public interface ScheduleService extends BaseService<Schedule, Integer> {

    /**
     * Looks for any schedules going to the given {@link Delivery} {@link Address}
     * on the {@link Delivery} {@link Date} and adds the {@link Delivery} to it if
     * there is one. If not, creates a new {@link Schedule} to the {@link Address}
     * and adds the {@link Delivery} to it.
     *
     * @param delivery the {@link Delivery}
     * @return the {@link Schedule} the {@link Delivery} has been added to
     */
    Schedule addDeliveryToSchedule(Delivery delivery, Subdiv allocatedSubdiv);

    /**
     * Deletes the {@link Schedule} with the given ID from the database.
     *
     * @param id the ID of the {@link Schedule} to delete.
     */
    void deleteScheduleById(int id);

    /**
     * Finds any {@link Schedule}s marked as repeat schedules for the given date,
     * person and address.
     *
     * @param contact the {@link Contact}
     * @param address the {@link Address}
     * @param type    the {@link ScheduleType}
     * @param date    the {@link Date} of the {@link Schedule}
     * @return a {@link Schedule} match if one is found, otherwise null
     */
    Schedule findRepeatScheduleForDate(Contact contact, Address address, ScheduleType type, LocalDate date);

    /**
     * this method gets all collections requested via the website which are actually
     * {@link Schedule}'s created by {@link Company} contacts between two dates.
     *
     * @param company  the company to retrieve schedules for
     * @param dateFrom the date from which we want to search
     * @param dateTo   the date to which we want to search
     * @return {@link List} {@link CompanyWebSchedulesDTO} objects
     */
    List<CompanyWebSchedulesDTO> getAllCompanyWebSchedulesRequestedForPeriod(Company company, LocalDate dateFrom, LocalDate dateTo);

    ResultWrapper getSchedulesForAddress(int addrId);

    /**
     * Returns all {@link Schedule} entities where the contact is associated with
     * the {@link Address} with the given ID.
	 * 
	 * @param addrId
	 *            the {@link Address} ID for the {@link Schedule}'s {@link Contact}.
	 * @param includeHistory
	 *            if schedules in the past are to be included in the search, this
	 *            will be set to true. Otherwise, false.
	 * @param type
	 *            the schedule type we want to retrieve (i.e. 'Collection',
	 *            'Delivery'), null for both
	 * @return the {@link List} of {@link Schedule} entities.
	 */
	List<Schedule> getSchedulesForAddress(int addrId, Boolean includeHistory, ScheduleType type);

	/**
	 * Returns all {@link Schedule} entities where the {@link Contact} with the
	 * given ID.
	 * 
	 * @param personId
	 *            the {@link Contact} ID for the {@link Schedule}'s {@link Contact}.
	 * @param includeHistory
	 *            if schedules in the past are to be included in the search, this
	 *            will be set to true. Otherwise, false.
	 * @param type
	 *            the schedule type we want to retrieve (i.e. 'Collection',
	 *            'Delivery'), null for both
	 * @return the {@link List} of {@link Schedule} entities.
	 */
	List<Schedule> getSchedulesForContact(int personId, Boolean includeHistory, ScheduleType type);

	/**
	 * Returns all {@link ScheduleDTO} entities with the given scheduleDate.
     *
     * @param date
     *            the {@link Date} to find {@link Schedule} entities for.
     * @return the {@link List} of {@link Schedule} entities.
     */
    List<ScheduleDTO> getSchedulesForDate(LocalDate date, Boolean futureDate, Boolean provisional, Boolean sourceWeb, Integer allocatedSubdivid,
                                          Integer allocatedCompanyId);

    /**
     * Returns all {@link Schedule} entities where the contact is associated with
     * the Subdivision with the given ID.
     *
     * @param subdivId
     *            the {@link Subdiv} ID for the {@link Schedule}'s {@link Contact}.
     * @param includeHistory
     *            if schedules in the past are to be included in the search, this
     *            will be set to true. Otherwise, false.
     * @param type
     *            the schedule type we want to retrieve (i.e. 'Collection',
     *            'Delivery'), null for both
     * @return the {@link List} of {@link Schedule} entities.
	 */
	List<Schedule> getSchedulesForSubdiv(int subdivId, Boolean includeHistory, ScheduleType type);
	
	/**
	 * Inserts the given {@link Schedule} into the database.
	 * 
	 * @param schedule
	 *            the {@link Schedule} to insert.
	 */
	void insertSchedule(Schedule schedule);

	/**
	 * Sends a confirmation e-mail to the contact listed on the schedule detailing
	 * important information regarding the schedule.
	 * 
	 * @param id
	 *            the {@link Schedule} ID.
	 * @return a {@link String} message stating whether or not the e-mail has been
	 *         sent successfully.
	 */
	String sendConfirmationEmail(int id);

	/**
	 * send an email to 'toaddr' with details of the collection request
	 * 
	 * @param s
	 * @param toAddr
	 * @return
	 */
	boolean webRequestSchedule(Schedule s, String filePath, Contact con);

	/**
	 * this method validates a collection requested via the website.
	 * 
	 * @param addrId
	 *            the id of address supplied
	 * @param rrDate
	 *            date specified for collection
	 * @param customDate
	 *            date specified for collection if not default
	 * @param poCal
	 *            po number provided for calibration
	 * @param poRep
	 *            po number proveded for repairs
	 * @param furtherInfo
	 *            any collection instruction text
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper webValidateScheduleRequest(Integer addrId, String rrDate, String customDate, String poCal,
			String poRep, String furtherInfo);
	
	Schedule createSchedule(CreateScheduleForm form, String username);
	
	void editSchedule (EditScheduleForm form);
	
	List<Schedule> schedulesFromAddress(Integer addrid, Subdiv allocatedSubdiv);
	
	PagedResultSet<ScheduleDTO> searchSchedules(ScheduleSearchForm form, Subdiv allocatedSubdiv, Company allocatedCompany, PagedResultSet<ScheduleDTO> prs);
	
	List<Schedule> schedulesByTransportOption(Integer transportOptionId, Boolean provisional);
	
	List<ScheduleDTO> schedulesExport(LocalDate date, Integer allocatedSubdivid);
	
	Schedule scheduleFromTransportOptionAndDateAndSubdiv(Integer transportOptionId, LocalDate scheduleDate, 
    		Integer destinationSubdivId);
}