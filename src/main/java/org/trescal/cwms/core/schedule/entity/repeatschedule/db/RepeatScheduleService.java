package org.trescal.cwms.core.schedule.entity.repeatschedule.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.schedule.entity.repeatschedule.RepeatSchedule;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;

public interface RepeatScheduleService extends BaseService<RepeatSchedule, Integer>
{
	ResultWrapper ajaxCreateRepeatSchedule(Integer addrId, Integer personid, ScheduleType schType, String notes, Integer transOptId
			, Integer subdivid);
	
	ResultWrapper ajaxDeactivateRepeatSchedule(int rsId);
	
	RepeatSchedule createRepeatSchedule(Address addr, Contact con, ScheduleType type, String notes, TransportOption transOpt, Subdiv scheduleFromSubdiv);
	
	void deactivateRepeatSchedule(RepeatSchedule repeatSchedule);
	
	List<RepeatSchedule> getAllActiveRepeatSchedules();
	
	List<RepeatSchedule> getRepeatSchedulesFrom_Contact_TransportOption_Address(Integer transportOptionId, Integer contactId, 
			Integer addressId);
}