package org.trescal.cwms.core.schedule.dto;

public class CompanyWebSchedulesDTO {
	private String name;
	private Integer pos;
	private String subname;
	private Integer total;

	public CompanyWebSchedulesDTO() {
	}

	public CompanyWebSchedulesDTO(Long total, String name, String subname) {
		this.total = total.intValue();
		this.name = name;
		this.subname = subname;
	}

	public String getName() {
		return this.name;
	}

	public Integer getPos() {
		return this.pos;
	}

	public String getSubname() {
		return this.subname;
	}

	public Integer getTotal() {
		return this.total;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPos(Integer pos) {
		this.pos = pos;
	}

	public void setSubname(String subname) {
		this.subname = subname;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}
}