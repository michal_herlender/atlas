package org.trescal.cwms.core.schedule.dto;

import java.time.LocalDate;
import java.util.Comparator;

public class ScheduledDeliveryDTOComparator implements Comparator<ScheduledDeliveryDTO> {

	@Override
	public int compare(ScheduledDeliveryDTO o1, ScheduledDeliveryDTO o2) {
		LocalDate date0 = o1.getScheduleDate();
		LocalDate date1 = o2.getScheduleDate();
		int result = date0.compareTo(date1);
		if (result == 0) {
			String transportOption0 = o1.getTransportOptionLocalizedName() != null
					? o1.getTransportOptionLocalizedName() : o1.getTransportMethodName();
			String transportOption1 = o2.getTransportOptionLocalizedName() != null
					? o2.getTransportOptionLocalizedName() : o2.getTransportMethodName();
			result = transportOption0.compareTo(transportOption1);
			if (result == 0) {
				int scheduleId0 = o1.getScheduleId();
				int scheduleId1 = o2.getScheduleId();
				result = scheduleId0 - scheduleId1;
				if (result == 0) {
					result = o1.getScheduledDeliveryId() - o2.getScheduledDeliveryId();
				}
			}
		}
		return result;
	}

}
