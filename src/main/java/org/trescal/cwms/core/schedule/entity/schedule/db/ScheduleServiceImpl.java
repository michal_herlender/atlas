package org.trescal.cwms.core.schedule.entity.schedule.db;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.schedule.dto.CompanyWebSchedulesDTO;
import org.trescal.cwms.core.schedule.dto.ScheduleDTO;
import org.trescal.cwms.core.schedule.dto.ScheduleNoteDTO;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.ScheduledDelivery;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.db.ScheduledDeliveryService;
import org.trescal.cwms.core.schedule.entity.schedulenote.db.ScheduleNoteService;
import org.trescal.cwms.core.schedule.entity.schedulesource.ScheduleSource;
import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;
import org.trescal.cwms.core.schedule.form.CreateScheduleForm;
import org.trescal.cwms.core.schedule.form.EditScheduleForm;
import org.trescal.cwms.core.schedule.form.ScheduleSearchForm;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_ScheduleConfirmation;
import org.trescal.cwms.core.system.entity.emailcontent.web.EmailContent_ClientCollection;
import org.trescal.cwms.core.system.entity.emailcontent.web.EmailContent_Collection;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.web.collections.dto.WebCollectionFieldWrapper;
import org.trescal.cwms.web.collections.form.genericentityvalidator.WebCollectionValidator;

@Service("ScheduleService")
public class ScheduleServiceImpl extends BaseServiceImpl<Schedule, Integer> implements ScheduleService {

	@Autowired
	private AddressService addressServ;
	@Autowired
	private BusinessDetailsService bdServ;
	@Autowired
	private UserService userService;
	@Autowired
	private ContactService conServ;
	@Autowired
	private EmailContent_Collection emailContentCollection;
	@Autowired
	private EmailContent_ClientCollection emailContentClientCollection;
	@Autowired
	private EmailContent_ScheduleConfirmation emailContentScheduleConfirmation;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private ScheduleDao scheduleDao;
	@Autowired
	private SessionUtilsService sessionServ;
	@Autowired
	private SubdivService subdivServ;
	@Autowired
	private CompanyService companyService;
	@Value("#{props['cwms.admin.email']}")
	private String sysAdminEmail;
	@Autowired
	private WebCollectionValidator webCollectValidator;
	@Autowired
	private MessageSource messages;
	@Autowired
	private ScheduleNoteService schNoteServ;
	@Autowired
	private TransportOptionService transportOptionService;
	@Autowired
	private ScheduledDeliveryService scheduledDeliveryService;

	@Override
	protected BaseDao<Schedule, Integer> getBaseDao() {
		return this.scheduleDao;
	}

	@Override
	public Schedule addDeliveryToSchedule(Delivery delivery, Subdiv allocatedSubdiv) {
		// find any schedules going to delivery address
		List<Schedule> existingSchedules = this.getSchedulesForAddress(delivery.getAddress().getAddrid(), false, null);
		// if there is one, add the delivery
		if (existingSchedules.size() > 0) {
			Schedule s = existingSchedules.get(0);
			if (s.getType().equals(ScheduleType.COLLECTION))
				s.setType(ScheduleType.COLLECTIONANDDELIVERY);
			this.scheduledDeliveryService.addScheduledDelivery(delivery, s);
			return this.merge(s);
		}
		// else, create new schedule
		else {
			Schedule s = new Schedule();
			s.setRepeatSchedule(false);
			s.setAddress(delivery.getAddress());
			s.setContact(delivery.getContact());
			s.setCreatedBy(this.sessionServ.getCurrentContact());
			s.setCreatedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			s.setScheduleDate(delivery.getDeliverydate());
			s.setStatus(ScheduleStatus.getDefaultScheduleStatus());
			s.setType(ScheduleType.DELIVERY);
			s.setSource(ScheduleSource.COMPANY);
			s.setOrganisation(allocatedSubdiv);
			this.insertSchedule(s);
			this.scheduledDeliveryService.addScheduledDelivery(delivery, s);
			return s;
		}
	}

	@Override
	public void delete(Schedule schedule) {
		List<ScheduledDelivery> scheduledDeliveryList = this.scheduledDeliveryService.scheduledDeliveryFromScheduleAndOrDelivery(schedule.getScheduleId(), null);
		for(ScheduledDelivery scheduledDelivery : scheduledDeliveryList){
			this.scheduledDeliveryService.delete(scheduledDelivery);
		}	
		this.scheduleDao.remove(schedule);
	}

	@Override
	public void deleteScheduleById(int id) {
		this.delete(this.scheduleDao.find(id));
	}

	@Override
	public Schedule findRepeatScheduleForDate(Contact contact, Address address, ScheduleType type, LocalDate date) {
		return this.scheduleDao.findRepeatScheduleForDate(contact, address, type, date);
	}

	@Override
	public List<CompanyWebSchedulesDTO> getAllCompanyWebSchedulesRequestedForPeriod(Company company, LocalDate dateFrom,
																					LocalDate dateTo) {
		return this.scheduleDao.getAllCompanyWebSchedulesRequestedForPeriod(company, dateFrom, dateTo);
	}

	@Override
	public ResultWrapper getSchedulesForAddress(int addrId) {
		// find address
		Address add = this.addressServ.get(addrId);
		// address null?
		if (add != null) {
			// get list of schedules for address
			List<Schedule> scheds = this.getSchedulesForAddress(addrId, false, null);
			// return list of schedules
			return new ResultWrapper(true, "", scheds, null);
		} else {
			return new ResultWrapper(false, "Schedule address could not be found", null, null);
		}
	}

	@Override
	public List<Schedule> getSchedulesForAddress(int addrId, Boolean includeHistory, ScheduleType type) {
		return this.scheduleDao.getSchedulesForAddress(addrId, includeHistory, type);
	}

	@Override
	public List<Schedule> getSchedulesForContact(int personId, Boolean includeHistory, ScheduleType type) {
		return this.scheduleDao.getSchedulesForContact(personId, includeHistory, type);
	}

	@Override
	public List<ScheduleDTO> getSchedulesForDate(LocalDate date, Boolean futureDate, Boolean provisional, Boolean sourceWeb, Integer allocatedSubdivid, Integer allocatedCompanyId) {
		Subdiv allocatedSubdiv = this.subdivServ.get(allocatedSubdivid);
		Company allocatedCompany = this.companyService.get(allocatedCompanyId);
		List<ScheduleDTO> schedules = this.scheduleDao.getSchedulesForDate(date, futureDate, provisional, sourceWeb, allocatedSubdiv, allocatedCompany);
		// set notes for each schedule 
		schedules.forEach(schedule -> {
			List<ScheduleNoteDTO> notes = this.schNoteServ.notesByScheduleId(schedule.getScheduleId());
			schedule.setNotes(notes);
		});
		return schedules;
	}

	@Override
	public List<Schedule> getSchedulesForSubdiv(int subdivId, Boolean includeHistory, ScheduleType type) {
		return this.scheduleDao.getSchedulesForSubdiv(subdivId, includeHistory, type);
	}

	@Override
	public void insertSchedule(Schedule Schedule) {
		this.scheduleDao.persist(Schedule);
	}

	@Override
	public String sendConfirmationEmail(int id) {
		Schedule schedule = this.scheduleDao.find(id);
		Contact contact = schedule.getContact();
		Locale locale = contact.getLocale();
		String confirmation;
		BusinessDetails bdetails = this.bdServ.getAllBusinessDetails(schedule.getOrganisation());
		String collectEmail = bdetails.getCollectEmail();
		EmailContentDto contentDto = this.emailContentScheduleConfirmation.getContent(schedule, locale);
		boolean success = this.emailServ.sendBasicEmail(contentDto.getSubject(), collectEmail, contact.getEmail(),
			contentDto.getBody());
		if (success) {
			confirmation = this.messages.getMessage("email.scheduleconfirmation.success",
				new Object[]{contact.getName(), contact.getEmail()},
				"A confirmation e-mail has been sent to " + contact.getName() + " (" + contact.getEmail() + ").",
				this.sessionServ.getCurrentContact().getLocale());
		} else {
			confirmation = this.messages.getMessage("email.scheduleconfirmation.error", null,
					"There was an error and the confirmation e-mail could not be sent.",
					this.sessionServ.getCurrentContact().getLocale());
		}
		return confirmation;
	}

	@Override
	public boolean webRequestSchedule(Schedule schedule, String filePath, Contact con) {
		BusinessDetails bdetails = this.bdServ.getAllBusinessDetails(schedule.getOrganisation());

		Locale locale = con.getLocale();

		String collectEmail = bdetails.getCollectEmail();

		List<String> toList = new ArrayList<>();
		toList.add(bdetails.getSalesEmail());
		toList.add(bdetails.getGoodsinEmail());

		List<String> ccList = new ArrayList<>();
		ccList.add(this.sysAdminEmail);

		List<File> fileList = new ArrayList<>();
		if ((filePath != null) && (!filePath.isEmpty())) {
			File f = new File(filePath);
			fileList.add(f);
		}

		// send a confirmation to the user
		if ((con.getEmail() != null) && !con.getEmail().isEmpty()) {
			List<String> contactToList = new ArrayList<>();
			contactToList.add(con.getEmail());

			// send contact a mail notifying of this collection request
			EmailContentDto clientContentDto = this.emailContentClientCollection.getContent(schedule, locale);
			this.emailServ.sendAdvancedEmail(clientContentDto.getSubject(), collectEmail, contactToList, Collections.emptyList(), Collections.emptyList(),
				Collections.emptyList(), fileList, clientContentDto.getBody(), true, false);
		}

		// create email text
		EmailContentDto internalContentDto = this.emailContentCollection.getContent(schedule, locale);

		return this.emailServ.sendAdvancedEmail(internalContentDto.getSubject(), collectEmail, toList, ccList, Collections.emptyList(),
				Collections.emptyList(), fileList, internalContentDto.getBody(), true, false);
	}

	@Override
	public ResultWrapper webValidateScheduleRequest(Integer addrId, String rrDate, String customDate, String poCal,
			String poRep, String furtherInfo) {
		WebCollectionFieldWrapper collectionFields = new WebCollectionFieldWrapper(addrId, rrDate, customDate, poCal,
				poRep, furtherInfo);

		// validate collectionFields for data type
		BindException errors = new BindException(collectionFields, "collection");
		this.webCollectValidator.validate(collectionFields, errors);

		return new ResultWrapper(this.webCollectValidator.getErrors(), collectionFields);
	}
	
	@Override
	public Schedule createSchedule(CreateScheduleForm form, String username) {
		Schedule schedule = new Schedule();
		schedule.setRepeatSchedule(false);
		schedule.setOrganisation(this.subdivServ.get(form.getBusinessSubdivid()));
		if (form.getAddrid() != null)
			schedule.setAddress(this.addressServ.get(form.getAddrid()));
		if (form.getPersonid() != null)
			schedule.setContact(this.conServ.get(form.getPersonid()));
		if (form.getManualEntryDate()) {
			schedule.setScheduleDate(form.getScheduleDate());
		} else {
			LocalDate scheduleDate = this.transportOptionService.getNextAvailable(form.getTransportOptionId(), null);
			schedule.setScheduleDate(scheduleDate);
		}
		schedule.setCreatedBy(this.userService.getEagerLoad(username).getCon());
		schedule.setCreatedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		schedule.setStatus(form.getStatus());
		schedule.setType(form.getType());
		schedule.setSource(form.getSource());
		schedule.setTransportOption(transportOptionService.get(form.getTransportOptionId()));

		this.insertSchedule(schedule);

		if (form.getNote() != null && form.getNote().trim().length() > 0) {
			this.schNoteServ.createScheduleNote(schedule, form.getNote());
		}
		
		return schedule;
	}
	
	@Override
	public void editSchedule (EditScheduleForm form) {
		Schedule schedule = form.getSchedule();
		
		if(form.getType() != null){
			schedule.setType(form.getType());
		}
		
		if(form.getStatus() != null) {
			schedule.setStatus(form.getStatus());
		}
		
		if(form.getTransportOptionid() != null){
			schedule.setTransportOption(transportOptionService.get(form.getTransportOptionid()));
		}
		
		if(form.getAddrid() != null){
			schedule.setAddress(this.addressServ.get(form.getAddrid()));
		}
		
		if (form.getPersonid() != null)
			schedule.setContact(this.conServ.get(form.getPersonid()));
		
		if(form.getScheduleDate() != null){
			schedule.setScheduleDate(form.getScheduleDate());
		}
		
		this.merge(schedule);
	}
	
	@Override
	public List<Schedule> schedulesFromAddress(Integer addrid, Subdiv allocatedSubdiv){
		return this.scheduleDao.schedulesFromAddress(addrid, allocatedSubdiv);
	}

	@Override
	public PagedResultSet<ScheduleDTO> searchSchedules(ScheduleSearchForm form, Subdiv allocatedSubdiv, Company allactedCompany, PagedResultSet<ScheduleDTO> prs) {

		PagedResultSet<ScheduleDTO> dtos = this.scheduleDao.searchSchedules(form, allocatedSubdiv, allactedCompany, prs);
		// set notes for each schedule
		dtos.getResults().forEach(schedule -> {
			List<ScheduleNoteDTO> notes = this.schNoteServ.notesByScheduleId(schedule.getScheduleId());
			schedule.setNotes(notes);
		});

		return dtos;
	}
	
	@Override
	public List<Schedule> schedulesByTransportOption(Integer transportOptionId, Boolean provisional){
		return this.scheduleDao.schedulesByTransportOption(transportOptionId, provisional);
	}
	
	@Override
	public List<ScheduleDTO> schedulesExport(LocalDate date, Integer allocatedSubdivid){
		Subdiv allocatedSubdiv = this.subdivServ.get(allocatedSubdivid);
		return this.scheduleDao.schedulesExport(date, allocatedSubdiv);
	}
	
	@Override
	public Schedule scheduleFromTransportOptionAndDateAndSubdiv(Integer transportOptionId, LocalDate scheduleDate, 
    		Integer destinationSubdivId){
		return this.scheduleDao.scheduleFromTransportOptionAndDateAndSubdiv(transportOptionId, scheduleDate, 
				destinationSubdivId);
	}
}