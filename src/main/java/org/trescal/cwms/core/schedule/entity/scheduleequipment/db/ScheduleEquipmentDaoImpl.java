package org.trescal.cwms.core.schedule.entity.scheduleequipment.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.schedule.entity.scheduleequipment.ScheduleEquipment;

@Repository("ScheduleEquipmentDao")
public class ScheduleEquipmentDaoImpl extends BaseDaoImpl<ScheduleEquipment, Integer> implements ScheduleEquipmentDao {
	
	@Override
	protected Class<ScheduleEquipment> getEntity() {
		return ScheduleEquipment.class;
	}
}