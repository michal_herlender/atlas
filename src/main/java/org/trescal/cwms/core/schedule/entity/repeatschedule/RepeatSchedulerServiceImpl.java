package org.trescal.cwms.core.schedule.entity.repeatschedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.schedule.entity.repeatschedule.db.RepeatScheduleService;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;
import org.trescal.cwms.core.schedule.entity.schedulenote.ScheduleNote;
import org.trescal.cwms.core.schedule.entity.schedulesource.ScheduleSource;
import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

@Service("RepeatSchedulerService")
public class RepeatSchedulerServiceImpl implements RepeatSchedulerService {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Value("${cwms.config.schedule.repeat_schedule_weeks}")
    private int numWeeks;
    @Autowired
    private RepeatScheduleService repSchServ;
    @Autowired
    private ScheduleService schedServ;
    @Autowired
    private ScheduledTaskService schTaskServ;
    @Autowired
    private TransportOptionService transportOptionService;

    @Override
    public void bookRepeatScheduleForNew(RepeatSchedule rs) {
        this.bookRepeatsForComingWeeks(Collections.singletonList(rs));
    }

    /**
     * Should fire daily/weekly at night to automatically book any schedules
     * that are due to repeat
     */
    @Override
    public void bookRepeatSchedules() {
        if (this.schTaskServ.scheduledTaskIsActive(this.getClass().getName(), Thread.currentThread().getStackTrace())) {
			// find all active repeats
			List<RepeatSchedule> repeats = this.repSchServ.getAllActiveRepeatSchedules();

			// make sure all active repeats are booked for coming weeks
			int added = this.bookRepeatsForComingWeeks(repeats);

			// report results of task run
			String message = added
					+ " new future repeat schedules added to database";
			this.schTaskServ.reportOnScheduledTaskRun(this.getClass().getName(), Thread.currentThread().getStackTrace(), true, message);
		}
		else
		{
			this.logger.info("Repeat Scheduler not running: scheduled task cannot be found or is turned off");
		}
	}

	private int bookRepeatsForComingWeeks(List<RepeatSchedule> repeats) {
		int added = 0;
		if (repeats != null) {
			// for each repeat schedule
			for (RepeatSchedule rs : repeats) {
                // if repeat schedule is a collection
                if (rs.getType().equals(ScheduleType.COLLECTION)) {
                    List<LocalDate> dates = this.transportOptionService.availableDateForNWeeks(rs.getTransportOption(), this.numWeeks);
                    for (LocalDate date : dates) {
                        // find existing repeat collection for this person,
                        // address and date
                        Schedule existingSchedule = this.schedServ.findRepeatScheduleForDate(rs.getCon(), rs.getAddr(), rs.getType(), date);
                        // if repeat schedule doesn't exist yet
                        if (existingSchedule == null) {
                            // create schedule
                            Schedule s = new Schedule();
                            s.setType(rs.getType());
                            s.setAddress(rs.getAddr());
                            s.setContact(rs.getCon());
                            s.setCreatedBy(rs.getCreatedBy());
                            s.setCreatedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
                            s.setScheduleDate(date);
                            s.setRepeatSchedule(true);
                            s.setSource(ScheduleSource.COMPANY);
                            s.setStatus(ScheduleStatus.getDefaultScheduleStatus());
                            s.setTransportOption(rs.getTransportOption());
                            // set the scheduled from subdivision using the base
                            // address
                            s.setOrganisation(rs.getOrganisation());
                            // add any notes to schedule
                            if (rs.getNotes() != null && !rs.getNotes().isEmpty()) {
                                ScheduleNote note = new ScheduleNote();
                                note.setActive(true);
                                note.setNote(rs.getNotes());
                                note.setPublish(false);
                                note.setSetBy(rs.getCreatedBy());
                                note.setSetOn(rs.getCreatedOn());
                                note.setSchedule(s);
                                s.setNotes(new TreeSet<>(new NoteComparator()));
                                s.getNotes().add(note);
                            }
                            // persist schedule
                            this.schedServ.insertSchedule(s);
                            // increment count of added repeat schedules
                            added++;
                        }
                    }

                }
            }
			}
		return added;
	}

	@Override
	public int removeRepeatsForComingWeeks(RepeatSchedule rs) {
		int removed = 0;
		// repeat schedule not null?
		if (rs != null) {
			// if repeat schedule is a collection
			if (rs.getType().equals(ScheduleType.COLLECTION)) {
                List<LocalDate> dates = this.transportOptionService.availableDateForNWeeks(rs.getTransportOption(), this.numWeeks);
                for (LocalDate date : dates) {
                    // find existing repeat collection for this person,
                    // address and date
                    Schedule existingSchedule = this.schedServ.findRepeatScheduleForDate(rs.getCon(), rs.getAddr(), rs.getType(), date);
                    // if repeat schedule doesn't exist yet
                    if (existingSchedule != null) {
                        if (existingSchedule.isRepeatSchedule()) {
                            // remove schedule
                            this.schedServ.delete(existingSchedule);
                            // increment count of removed repeat schedules
                            removed++;
                        }
                    }
                }
            }
		}
		return removed;
	}
}
