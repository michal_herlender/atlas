package org.trescal.cwms.core.schedule.entity.schedule;

import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.schedule.entity.scheduleequipment.ScheduleEquipment;
import org.trescal.cwms.core.schedule.entity.scheduleequipment.ScheduleEquipmentComparator;
import org.trescal.cwms.core.schedule.entity.schedulenote.ScheduleNote;
import org.trescal.cwms.core.schedule.entity.schedulesource.ScheduleSource;
import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Setter
@Table(name = "schedule")
public class Schedule extends Allocated<Subdiv> implements NoteAwareEntity {

    private Address address;
    private Contact contact;
    private Contact createdBy;
    private LocalDate createdOn;
    private Set<ScheduleNote> notes;
    private boolean repeatSchedule;
    private LocalDate scheduleDate;
    private Set<ScheduleEquipment> scheduleequipment;
    private int scheduleId;
    private ScheduleSource source;
    private ScheduleStatus status;
    private ScheduleType type;
    private TransportOption transportOption;

    public Schedule() {
    }

    /**
     * @return the address
     */
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "addressid")
    public Address getAddress() {
        return this.address;
    }

	/**
	 * @return the contact
	 */
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contactid")
	public Contact getContact() {
		return this.contact;
    }

    /**
     * @return the createdBy
     */
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "createdby")
    public Contact getCreatedBy() {
        return this.createdBy;
    }

    /**
     * @return the createdOn
     */
    @NotNull
    @Column(name = "createdon", columnDefinition = "date")
    public LocalDate getCreatedOn() {
        return this.createdOn;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transportoptionid", foreignKey = @ForeignKey(name = "FK_scheduled_transportoption"), nullable = false)
    public TransportOption getTransportOption() {
        return this.transportOption;
    }

    /**
     * @return the notes
	 */
	@Cascade(CascadeType.ALL)
	@OneToMany(mappedBy = "schedule", fetch = FetchType.LAZY)
	@SortComparator(NoteComparator.class)
	public Set<ScheduleNote> getNotes() {
		return this.notes;
	}

	@Transient
	public Integer getPrivateActiveNoteCount() {
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount() {
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
    }

    @Transient
    public Integer getPublicActiveNoteCount() {
        return NoteCountCalculator.getPublicActiveNoteCount(this);
    }

    @Transient
    public Integer getPublicDeactivatedNoteCount() {
        return NoteCountCalculator.getPublicDeactivedNoteCount(this);
    }

    /**
     * @return the scheduleDate
     */
    @NotNull
    @Column(name = "scheduledate", columnDefinition = "date")
    public LocalDate getScheduleDate() {
        return this.scheduleDate;
    }

    /**
     * @return the schedule equipment
     */
    @Cascade(CascadeType.ALL)
    @OneToMany(mappedBy = "schedule", fetch = FetchType.LAZY)
    @SortComparator(ScheduleEquipmentComparator.class)
    public Set<ScheduleEquipment> getScheduleequipment() {
        return this.scheduleequipment;
	}

	/**
	 * @return the scheduleId
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "scheduleid", nullable = false, unique = true)
	@Type(type = "int")
	public int getScheduleId() {
		return this.scheduleId;
	}

	@NotNull
	@Enumerated(EnumType.ORDINAL)
	@Column(name="schedulesourceid")
	public ScheduleSource getSource() {
		return this.source;
	}

	@NotNull
	@Enumerated(EnumType.ORDINAL)
	@Column(name="schedulestatusid")
	public ScheduleStatus getStatus() {
		return this.status;
	}

	@NotNull
	@Enumerated(EnumType.ORDINAL)
	@Column(name="scheduletypeid")
	public ScheduleType getType() {
		return this.type;
	}

	@Column(name = "repeatschedule", columnDefinition = "tinyint")
	public boolean isRepeatSchedule() {
		return this.repeatSchedule;
	}
	
}