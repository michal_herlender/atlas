package org.trescal.cwms.core.schedule.entity.repeatschedule.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.schedule.entity.repeatschedule.RepeatSchedule;

public interface RepeatScheduleDao extends BaseDao<RepeatSchedule, Integer>
{
	List<RepeatSchedule> getAllActive();
	
	List<RepeatSchedule> getRepeatSchedulesFrom_Contact_TransportOption_Address(Integer transportOptionId, Integer contactId, 
			Integer addressId);
}