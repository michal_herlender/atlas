package org.trescal.cwms.core.schedule.entity.schedulenote.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.schedule.dto.ScheduleNoteDTO;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule_;
import org.trescal.cwms.core.schedule.entity.schedulenote.ScheduleNote;
import org.trescal.cwms.core.schedule.entity.schedulenote.ScheduleNote_;

@Repository("ScheduleNoteDao")
public class ScheduleNoteDaoImpl extends BaseDaoImpl<ScheduleNote, Integer> implements ScheduleNoteDao
{
	@Override
	protected Class<ScheduleNote> getEntity() {
		return ScheduleNote.class;
	}
	
	@Override
	public List<ScheduleNoteDTO> notesByScheduleId(Integer scheduleId){
		 return getResultList( cb ->{
			CriteriaQuery<ScheduleNoteDTO> cq = cb.createQuery(ScheduleNoteDTO.class);
			Root<ScheduleNote> root = cq.from(ScheduleNote.class);
			Join<ScheduleNote, Schedule> schedule = root.join(ScheduleNote_.schedule);
			
			Join<ScheduleNote, Contact> contact = root.join(ScheduleNote_.setBy);
			cq.where(cb.equal(schedule.get(Schedule_.scheduleId), scheduleId));
			cq.select(cb.construct(ScheduleNoteDTO.class, 
					root.get(ScheduleNote_.noteid),
					root.get(ScheduleNote_.setOn),
					root.get(ScheduleNote_.active),
					contact.get(Contact_.firstName),
					contact.get(Contact_.lastName),
					root.get(ScheduleNote_.note)
					));
			return cq;
		});
	}
}