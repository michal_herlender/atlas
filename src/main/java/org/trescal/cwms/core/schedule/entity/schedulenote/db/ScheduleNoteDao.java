package org.trescal.cwms.core.schedule.entity.schedulenote.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.schedule.dto.ScheduleNoteDTO;
import org.trescal.cwms.core.schedule.entity.schedulenote.ScheduleNote;

public interface ScheduleNoteDao extends BaseDao<ScheduleNote, Integer> {
	
	List<ScheduleNoteDTO> notesByScheduleId(Integer scheduleId);
}