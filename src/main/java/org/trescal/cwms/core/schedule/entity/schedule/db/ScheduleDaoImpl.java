package org.trescal.cwms.core.schedule.entity.schedule.db;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CompoundSelection;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery_;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.DeliveryNote;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.DeliveryNote_;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption_;
import org.trescal.cwms.core.schedule.dto.CompanyWebSchedulesDTO;
import org.trescal.cwms.core.schedule.dto.ScheduleDTO;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule_;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.ScheduledDelivery;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.ScheduledDelivery_;
import org.trescal.cwms.core.schedule.entity.scheduleequipment.ScheduleEquipment;
import org.trescal.cwms.core.schedule.entity.scheduleequipment.ScheduleEquipment_;
import org.trescal.cwms.core.schedule.entity.schedulesource.ScheduleSource;
import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;
import org.trescal.cwms.core.schedule.form.ScheduleSearchForm;
import org.trescal.cwms.core.tools.PagedResultSet;

@Repository("ScheduleDao")
public class ScheduleDaoImpl extends BaseDaoImpl<Schedule, Integer> implements ScheduleDao {

	@Override
	protected Class<Schedule> getEntity() {
		return Schedule.class;
	}

	@Override
	public Schedule findRepeatScheduleForDate(Contact contact, Address address, ScheduleType type, LocalDate date) {
		return getFirstResult(cb -> {
			CriteriaQuery<Schedule> cq = cb.createQuery(Schedule.class);
			Root<Schedule> schedule = cq.from(Schedule.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(schedule.get(Schedule_.contact), contact));
			clauses.getExpressions().add(cb.equal(schedule.get(Schedule_.address), address));
			clauses.getExpressions().add(cb.equal(schedule.get(Schedule_.type), type));
			clauses.getExpressions().add(cb.equal(schedule.get(Schedule_.scheduleDate), date));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

	@Override
	public List<CompanyWebSchedulesDTO> getAllCompanyWebSchedulesRequestedForPeriod(Company company, LocalDate dateFrom,
																					LocalDate dateTo) {
		// create sql to retrieve all schedules created for company between
		// set dates, this includes any schedules that have
		// been deleted in the meantime as we are interested in how many
		// have been requested
		// no logging anymore -> change behavior here
		return getResultList(cb -> {
			CriteriaQuery<CompanyWebSchedulesDTO> cq = cb.createQuery(CompanyWebSchedulesDTO.class);
			Root<Schedule> schedule = cq.from(Schedule.class);
			Join<Schedule, Contact> contact = schedule.join(Schedule_.contact);
			Join<Schedule, Address> address = schedule.join(Schedule_.address);
			Join<Address, Subdiv> subdiv = address.join(Address_.sub);
			Expression<Long> counter = cb.count(schedule);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(schedule.get(Schedule_.source), ScheduleSource.WEB));
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.comp), company));
			clauses.getExpressions().add(cb.between(schedule.get(Schedule_.createdOn), dateFrom, dateTo));
			cq.where(clauses);
			cq.groupBy(contact.get(Contact_.firstName), contact.get(Contact_.lastName), subdiv.get(Subdiv_.subname));
			cq.orderBy(cb.desc(counter));
			cq.select(cb.construct(CompanyWebSchedulesDTO.class, counter,
					cb.concat(cb.concat(contact.get(Contact_.firstName), " "), contact.get(Contact_.lastName)),
					subdiv.get(Subdiv_.subname)));
			return cq;
		});
	}

	@Override
	public List<Schedule> getSchedulesForAddress(int addrId, Boolean includeHistory, ScheduleType type) {
		return getResultList(cb -> {
			CriteriaQuery<Schedule> cq = cb.createQuery(Schedule.class);
			Root<Schedule> schedule = cq.from(Schedule.class);
			Join<Schedule, Address> address = schedule.join(Schedule_.address);
			schedule.fetch(Schedule_.contact, JoinType.LEFT);
			Predicate clauses = cb.conjunction();
			if (includeHistory != null && !includeHistory)
				clauses.getExpressions().add(cb.greaterThanOrEqualTo(schedule.get(Schedule_.scheduleDate), LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())));
			if (type != null)
				clauses.getExpressions().add(cb.equal(schedule.get(Schedule_.type), type));
			clauses.getExpressions().add(cb.equal(address.get(Address_.addrid), addrId));
			cq.where(clauses);
			cq.orderBy(cb.asc(schedule.get(Schedule_.scheduleDate)));
			return cq;
		});
	}

	@Override
	public List<Schedule> getSchedulesForContact(int personId, Boolean includeHistory, ScheduleType type) {
		return getResultList(cb -> {
			CriteriaQuery<Schedule> cq = cb.createQuery(Schedule.class);
			Root<Schedule> schedule = cq.from(Schedule.class);
			Join<Schedule, Contact> contact = schedule.join(Schedule_.contact);
			Predicate clauses = cb.conjunction();
			if (includeHistory != null && !includeHistory)
				clauses.getExpressions().add(cb.greaterThanOrEqualTo(schedule.get(Schedule_.scheduleDate), LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())));
			if (type != null)
				clauses.getExpressions().add(cb.equal(schedule.get(Schedule_.type), type));
			clauses.getExpressions().add(cb.equal(contact.get(Contact_.personid), personId));
			cq.where(clauses);
			cq.orderBy(cb.asc(schedule.get(Schedule_.scheduleDate)));
			return cq;
		});
	}

	@Override
	public List<ScheduleDTO> getSchedulesForDate(LocalDate date, Boolean futureDate, Boolean provisional, Boolean sourceWeb, Subdiv allocatedSubdiv, Company allocatedCompany) {
		return getResultList(cb -> {
			CriteriaQuery<ScheduleDTO> cq = cb.createQuery(ScheduleDTO.class);
			Root<Schedule> root = cq.from(Schedule.class);
			Join<Schedule, Address> address = root.join(Schedule_.address, JoinType.LEFT);
			Join<Schedule, TransportOption> transportOption = root.join(Schedule_.transportOption);
			Join<Schedule, Subdiv> organisation = root.join(Schedule_.ORGANISATION);
			Join<Schedule, Contact> contact = root.join(Schedule_.contact);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Join<Company, CompanySettingsForAllocatedCompany> settings = company
					.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			settings.on(cb.equal(settings.get(CompanySettingsForAllocatedCompany_.organisation), allocatedCompany));
			
			// get the schedule equipments count
			Subquery<Long> scheduleEquipmentsCountSq = cq.subquery(Long.class);
			Root<ScheduleEquipment> scheduleEquipmentsCount = scheduleEquipmentsCountSq.from(ScheduleEquipment.class);
			scheduleEquipmentsCountSq.where(cb.equal(scheduleEquipmentsCount.get(ScheduleEquipment_.schedule), root));
			scheduleEquipmentsCountSq.select(cb.count(scheduleEquipmentsCount));

			// get the deliveries count in a schedule
			Subquery<Long> scheduledDeliveryCountSq = cq.subquery(Long.class);
			Root<ScheduledDelivery> scheduledDeliveryCount = scheduledDeliveryCountSq.from(ScheduledDelivery.class);
			scheduledDeliveryCountSq.where(cb.equal(scheduledDeliveryCount.get(ScheduledDelivery_.schedule), root));
			scheduledDeliveryCountSq.select(cb.count(scheduledDeliveryCount));

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(organisation, allocatedSubdiv));
			if (date != null) {
				clauses.getExpressions().add(cb.equal(root.get(Schedule_.scheduleDate), date));
			} else if (futureDate) {
				clauses.getExpressions().add(cb.greaterThan(root.get(Schedule_.scheduleDate), LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())));
			}

			if (provisional != null)
				if (provisional)
					clauses.getExpressions().add(cb.equal(root.get(Schedule_.status), ScheduleStatus.PROVISIONAL));
				else
					clauses.getExpressions().add(cb.equal(root.get(Schedule_.status), ScheduleStatus.AGREED));
			
			if( sourceWeb != null && sourceWeb){
				clauses.getExpressions().add(cb.equal(root.get(Schedule_.source), ScheduleSource.WEB));
			}
			cq.where(clauses);
			cq.select(cb.construct(ScheduleDTO.class, 
					root.get(Schedule_.scheduleId),
					root.get(Schedule_.scheduleDate),
					root.get(Schedule_.source),
					root.get(Schedule_.type),
					root.get(Schedule_.status),
					transportOption.get(TransportOption_.id),
					root.get(Schedule_.repeatSchedule),
					organisation.get(Subdiv_.subname),
					company.get(Company_.coid),
					company.get(Company_.coname),
					settings.get(CompanySettingsForAllocatedCompany_.active),
					settings.get(CompanySettingsForAllocatedCompany_.onStop),
					subdiv.get(Subdiv_.subdivid),
					subdiv.get(Subdiv_.subname),
					subdiv.get(Subdiv_.active),
					address.get(Address_.addr1),
					address.get(Address_.addr2),
					address.get(Address_.addr3),
					address.get(Address_.town),
					address.get(Address_.country).get(Country_.country),
					address.get(Address_.county),
					address.get(Address_.postcode),
					contact.get(Contact_.personid),
					contact.get(Contact_.firstName),
					contact.get(Contact_.lastName),
					contact.get(Contact_.active),
					contact.get(Contact_.telephone),
					contact.get(Contact_.mobile),
					contact.get(Contact_.email),
					scheduleEquipmentsCountSq.getSelection(),
					scheduledDeliveryCountSq.getSelection()
					));
			cq.orderBy(cb.asc(address.get(Address_.county)), cb.asc(address.get(Address_.town)),
					cb.asc(root.get(Schedule_.scheduleDate)));
			return cq;
		});
	}

	@Override
	public List<Schedule> getSchedulesForSubdiv(int subdivId, Boolean includeHistory, ScheduleType type) {
		return getResultList(cb -> {
			CriteriaQuery<Schedule> cq = cb.createQuery(Schedule.class);
			Root<Schedule> schedule = cq.from(Schedule.class);
			Join<Schedule, Contact> contact = schedule.join(Schedule_.contact);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			Predicate clauses = cb.conjunction();
			if (includeHistory != null && !includeHistory)
				clauses.getExpressions().add(cb.greaterThanOrEqualTo(schedule.get(Schedule_.scheduleDate), LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())));
			if (type != null)
				clauses.getExpressions().add(cb.equal(schedule.get(Schedule_.type), type));
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), subdivId));
			cq.where(clauses);
			cq.orderBy(cb.asc(schedule.get(Schedule_.scheduleDate)));
			return cq;
		});
	}
	
	@Override
	public List<Schedule> schedulesFromAddress(Integer addrid, Subdiv allocatedSubdiv) {
		return getResultList(cb  ->{
			CriteriaQuery<Schedule> cq = cb.createQuery(Schedule.class);
			Root<Schedule> root = cq.from(Schedule.class);
			Join<Schedule, Address> address = root.join(Schedule_.address);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(address.get(Address_.addrid), addrid));
			clauses.getExpressions().add(cb.equal(root.get(Schedule_.organisation), allocatedSubdiv));
			cq.where(clauses);
			cq.orderBy(cb.desc(root.get(Schedule_.createdOn)));
			
			return cq;
		});
	}
	
	@Override
	public PagedResultSet<ScheduleDTO> searchSchedules(ScheduleSearchForm form, Subdiv allocatedSubdiv, Company allocatedCompany, PagedResultSet<ScheduleDTO> prs){
		
		completePagedResultSet(prs, ScheduleDTO.class, cb -> cq -> {
			Root<Schedule> root = cq.from(Schedule.class);
			Join<Schedule, Address> address = root.join(Schedule_.address);
			Join<Schedule, TransportOption> transportOption = root.join(Schedule_.transportOption);
			Join<Schedule, Subdiv> organisation = root.join(Schedule_.ORGANISATION);
			Join<Schedule, Contact> contact = root.join(Schedule_.contact);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Join<Company, CompanySettingsForAllocatedCompany> settings = company
					.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			settings.on(cb.equal(settings.get(CompanySettingsForAllocatedCompany_.organisation), allocatedCompany));
			
			Subquery<Long> scheduleEquipmentsCountSq = cq.subquery(Long.class);
			Root<ScheduleEquipment> scheduleEquipmentsCount = scheduleEquipmentsCountSq.from(ScheduleEquipment.class);
			scheduleEquipmentsCountSq.where(cb.equal(scheduleEquipmentsCount.get(ScheduleEquipment_.schedule), root));
			scheduleEquipmentsCountSq.select(cb.count(scheduleEquipmentsCount));
			
			Subquery<Long> scheduledDeliveryCountSq = cq.subquery(Long.class);
			Root<ScheduledDelivery> scheduledDeliveryCount = scheduledDeliveryCountSq.from(ScheduledDelivery.class);
			scheduledDeliveryCountSq.where(cb.equal(scheduledDeliveryCount.get(ScheduledDelivery_.schedule), root));
			scheduledDeliveryCountSq.select(cb.count(scheduledDeliveryCount));
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(organisation, allocatedSubdiv));
			
			if(form.getStatus() != null){
				clauses.getExpressions().add(cb.equal(root.get(Schedule_.status), form.getStatus()));
			}
			
			if(form.getCoid() != null){
				clauses.getExpressions().add(cb.equal(company.get(Company_.coid), form.getCoid()));
			}
			
			if (form.getDateSingle() != null)
				clauses.getExpressions().add(cb.equal(root.get(Schedule_.scheduleDate), form.getDateSingle()));
			else if (form.getDateBegin() != null && form.getDateEnd() != null)
				clauses.getExpressions().add(cb.between(root.get(Schedule_.scheduleDate), form.getDateBegin(), form.getDateEnd()));
			else if (!form.isIncludeHistory())
				clauses.getExpressions().add(cb.greaterThanOrEqualTo(root.get(Schedule_.scheduleDate), LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())));
			
			cq.where(clauses);
			cq.distinct(true);
			
			CompoundSelection<ScheduleDTO> selection = cb.construct(ScheduleDTO.class, 
					root.get(Schedule_.scheduleId),
					root.get(Schedule_.scheduleDate),
					root.get(Schedule_.source),
					root.get(Schedule_.type),
					root.get(Schedule_.status),
					transportOption.get(TransportOption_.id),
					root.get(Schedule_.repeatSchedule),
					organisation.get(Subdiv_.subname),
					company.get(Company_.coid),
					company.get(Company_.coname),
					settings.get(CompanySettingsForAllocatedCompany_.active),
					settings.get(CompanySettingsForAllocatedCompany_.onStop),
					subdiv.get(Subdiv_.subdivid),
					subdiv.get(Subdiv_.subname),
					subdiv.get(Subdiv_.active),
					address.get(Address_.addr1),
					address.get(Address_.addr2),
					address.get(Address_.addr3),
					address.get(Address_.town),
					address.get(Address_.country).get(Country_.country),
					address.get(Address_.county),
					address.get(Address_.postcode),
					contact.get(Contact_.personid),
					contact.get(Contact_.firstName),
					contact.get(Contact_.lastName),
					contact.get(Contact_.active),
					contact.get(Contact_.telephone),
					contact.get(Contact_.mobile),
					contact.get(Contact_.email),
					scheduleEquipmentsCountSq.getSelection(),
					scheduledDeliveryCountSq.getSelection()
					);
		
			List<Order> order = new ArrayList<>();
			order.add(cb.asc(address.get(Address_.county)));
			order.add(cb.asc(address.get(Address_.town)));
			order.add(cb.asc(root.get(Schedule_.scheduleDate)));
			
			return Triple.of(root, selection, order);
		});
		
		return prs;
	}		
	
	@Override
	public List<Schedule> schedulesByTransportOption(Integer transportOptionId, Boolean provisional){
		return getResultList(cb ->{
			CriteriaQuery<Schedule> cq = cb.createQuery(Schedule.class);
			Root<Schedule> root = cq.from(Schedule.class);
			Join<Schedule, TransportOption> transportOption = root.join(Schedule_.transportOption);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(transportOption.get(TransportOption_.id), transportOptionId));
			if (provisional != null)
				if (provisional)
					clauses.getExpressions().add(cb.equal(root.get(Schedule_.status), ScheduleStatus.PROVISIONAL));
				else
					clauses.getExpressions().add(cb.equal(root.get(Schedule_.status), ScheduleStatus.AGREED));
			cq.where(clauses);
			return cq;
		});
	}
	
	@Override
	public List<ScheduleDTO> schedulesExport(LocalDate date, Subdiv allocatedSubdiv) {
		return getResultList( cb ->{
			CriteriaQuery<ScheduleDTO> cq = cb.createQuery(ScheduleDTO.class);
			Root<Schedule> root = cq.from(Schedule.class);
			Join<Schedule, TransportOption> transportOption = root.join(Schedule_.transportOption);
			Join<Schedule, Address> address = root.join(Schedule_.address);
			Join<Address, Subdiv> subdiv = address.join(Address_.sub);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Join<Schedule, Contact> contact = root.join(Schedule_.contact);
			
			Subquery<String> scheduledDeliverySq = cq.subquery(String.class);
			Root<ScheduledDelivery> scheduledDelivery = scheduledDeliverySq.from(ScheduledDelivery.class);
			Join<ScheduledDelivery, Delivery> deliveryJoin = scheduledDelivery.join(ScheduledDelivery_.delivery);
			Join<Delivery, DeliveryNote> notes = deliveryJoin.join(Delivery_.notes, JoinType.LEFT);		
			scheduledDeliverySq.where(cb.and(cb.equal(scheduledDelivery.get(ScheduledDelivery_.schedule), root), 
					(cb.isTrue(notes.get(DeliveryNote_.active)))));
			scheduledDeliverySq.select(notes.get(DeliveryNote_.note));
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(Schedule_.organisation), allocatedSubdiv));
			clauses.getExpressions().add(cb.equal(root.get(Schedule_.scheduleDate), date));
			clauses.getExpressions().add(cb.equal(root.get(Schedule_.status), ScheduleStatus.AGREED));
			cq.where(clauses);
			cq.orderBy(cb.asc(root.get(Schedule_.scheduleDate)));
			cq.select(cb.construct(ScheduleDTO.class,
					root.get(Schedule_.scheduleId),
					root.get(Schedule_.scheduleDate),
					root.get(Schedule_.type),
					transportOption.get(TransportOption_.externalRef),
					company.get(Company_.fiscalIdentifier),
					company.get(Company_.coname),
					address.get(Address_.addr1),
					address.get(Address_.addr2),
					address.get(Address_.town),
					address.get(Address_.county),
					address.get(Address_.postcode),
					contact.get(Contact_.firstName),
					contact.get(Contact_.lastName),
					contact.get(Contact_.telephone),
					contact.get(Contact_.mobile),
					scheduledDeliverySq.getSelection()));
			return cq;
		});
	}
	
	@Override
	public Schedule scheduleFromTransportOptionAndDateAndSubdiv(Integer transportOptionId, LocalDate scheduleDate, 
    		Integer destinationSubdivId){
		return getFirstResult(cb->{
			CriteriaQuery<Schedule> cq = cb.createQuery(Schedule.class);
			Root<Schedule> root = cq.from(Schedule.class);
			Join<Schedule, TransportOption> transportOption = root.join(Schedule_.transportOption);
			Join<Schedule, Address> address = root.join(Schedule_.address);
			Join<Address, Subdiv> destinationSubdiv = address.join(Address_.sub);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(Schedule_.scheduleDate), scheduleDate));
			clauses.getExpressions().add(cb.equal(transportOption.get(TransportOption_.id), transportOptionId));
			clauses.getExpressions().add(cb.equal(destinationSubdiv.get(Subdiv_.subdivid), destinationSubdivId));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

	
}