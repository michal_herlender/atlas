package org.trescal.cwms.core.schedule.dto;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScheduleResultDTO {

    private Integer transportOptionInId;
    private String transportOptionInName;
    private Integer transportOptionOutId;
    private String transportOptionOutName;
    private Integer scheduleId;
    private Map<Integer, String> schedulesCreatedForAddressMap;
}
