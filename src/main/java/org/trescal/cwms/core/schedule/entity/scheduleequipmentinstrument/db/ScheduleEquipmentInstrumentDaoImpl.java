package org.trescal.cwms.core.schedule.entity.scheduleequipmentinstrument.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.schedule.entity.scheduleequipmentinstrument.ScheduleEquipmentInstrument;

@Repository("ScheduleEquipmentInstrumentDao")
public class ScheduleEquipmentInstrumentDaoImpl extends BaseDaoImpl<ScheduleEquipmentInstrument, Integer> implements ScheduleEquipmentInstrumentDao {
	
	@Override
	protected Class<ScheduleEquipmentInstrument> getEntity() {
		return ScheduleEquipmentInstrument.class;
	}
}