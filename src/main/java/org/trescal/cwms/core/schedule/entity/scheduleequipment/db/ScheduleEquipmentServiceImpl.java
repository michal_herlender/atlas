package org.trescal.cwms.core.schedule.entity.scheduleequipment.db;

import java.util.List;

import org.trescal.cwms.core.schedule.entity.scheduleequipment.ScheduleEquipment;

public class ScheduleEquipmentServiceImpl implements ScheduleEquipmentService
{
	ScheduleEquipmentDao scheduleEquipmentDao;

	public void deleteScheduleEquipment(ScheduleEquipment scheduleequipment)
	{
		this.scheduleEquipmentDao.remove(scheduleequipment);
	}

	public ScheduleEquipment findScheduleEquipment(int id)
	{
		return this.scheduleEquipmentDao.find(id);
	}

	public List<ScheduleEquipment> getAllScheduleEquipment()
	{
		return this.scheduleEquipmentDao.findAll();
	}

	public void insertScheduleEquipment(ScheduleEquipment ScheduleEquipment)
	{
		this.scheduleEquipmentDao.persist(ScheduleEquipment);
	}

	public void saveOrUpdateScheduleEquipment(ScheduleEquipment scheduleequipment)
	{
		this.scheduleEquipmentDao.saveOrUpdate(scheduleequipment);
	}

	public void setScheduleEquipmentDao(ScheduleEquipmentDao scheduleEquipmentDao)
	{
		this.scheduleEquipmentDao = scheduleEquipmentDao;
	}

	public void updateScheduleEquipment(ScheduleEquipment ScheduleEquipment)
	{
		this.scheduleEquipmentDao.update(ScheduleEquipment);
	}
}