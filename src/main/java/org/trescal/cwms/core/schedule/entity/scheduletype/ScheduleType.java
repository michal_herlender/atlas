package org.trescal.cwms.core.schedule.entity.scheduletype;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum ScheduleType {
	COLLECTION("scheduletype.collection", "Collection"),
	DELIVERY("scheduletype.delivery", "Delivery"),
	COLLECTIONANDDELIVERY("scheduletype.collectionanddelivery", "Collection and Delivery");

	private ReloadableResourceBundleMessageSource messages;
	private Locale loc = null;
	private String messageCode;
	private String name;

	@Component
	public static class ScheduleTypeMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;

		@PostConstruct
		public void postConstruct() {
			for (ScheduleType rt : EnumSet.allOf(ScheduleType.class))
				rt.setMessageSource(messages);
		}
	}

	public void setMessageSource(ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}

	private ScheduleType(String messageCode, String name) {
		this.messageCode = messageCode;
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		loc = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.name, loc);
		}
		return this.toString();
	}

	public String getMessageCode() {
		return messageCode;
	}
}