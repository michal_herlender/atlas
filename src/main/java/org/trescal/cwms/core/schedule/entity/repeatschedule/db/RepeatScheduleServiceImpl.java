package org.trescal.cwms.core.schedule.entity.repeatschedule.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.schedule.entity.repeatschedule.RepeatSchedule;
import org.trescal.cwms.core.schedule.entity.repeatschedule.RepeatScheduleDTO;
import org.trescal.cwms.core.schedule.entity.repeatschedule.RepeatSchedulerService;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;

import java.util.Date;
import java.util.List;

@Service("RepeatScheduleService")
public class RepeatScheduleServiceImpl extends BaseServiceImpl<RepeatSchedule, Integer>
	implements RepeatScheduleService {
	@Autowired
	private AddressService addrServ;
	@Autowired
	private ContactService contactService;
	@Autowired
	private RepeatScheduleDao repeatScheduleDao;
	@Autowired
	private RepeatSchedulerService repeatScheduler;
	@Autowired
	private RepeatScheduleService repeatScheduleService;
	@Autowired
	private SessionUtilsService sessionServ;
	@Autowired
	private SubdivService subdivServ;
	@Autowired
	private TransportOptionService transOptServ;
	@Autowired
	private MessageSource messages;

	@Override
	protected BaseDao<RepeatSchedule, Integer> getBaseDao() {
		return repeatScheduleDao;
	}

	@Override
	public ResultWrapper ajaxCreateRepeatSchedule(Integer addrId, Integer personid, ScheduleType schType, String notes,
			Integer transOptId, Integer subdivid) {
		Address addr = this.addrServ.get(addrId);
		Contact contact = this.contactService.get(personid);
		Subdiv allocatedSubdiv = this.subdivServ.get(subdivid);
		TransportOption transOpt = (transOptId != null) ? this.transOptServ.get(transOptId) : null;

		if(transOpt.getScheduleRules().isEmpty()){
			return new ResultWrapper(false, this.messages.getMessage("schedule.error.no.nextavailable", null,
					"No schedule rules for the selected transport option", null), null, null);
		} else {
			List<RepeatSchedule> repeatSchedulesExist = this.repeatScheduleService.getRepeatSchedulesFrom_Contact_TransportOption_Address(transOptId, 
					personid, addrId);
			if(!repeatSchedulesExist.isEmpty()){
				return new ResultWrapper(false, this.messages.getMessage("schedule.repeat.error.duplicateschedule", null,
						"There is already a Repeat Schedule with the same transport option/address/contact ", null), null, null);
			} else {
				// create a new repeat schedule
				this.createRepeatSchedule(addr, contact, schType, notes, transOpt, allocatedSubdiv);
				return new ResultWrapper(true, null, null, null);
			}
		}
		
	}

	@Override
	public ResultWrapper ajaxDeactivateRepeatSchedule(int rsId) {
		RepeatSchedule rs = this.get(rsId);
		if (rs != null) {
			// de-activate this repeat schedule
			this.deactivateRepeatSchedule(rs);
			// manually trigger job that deletes schedules for the coming weeks
			this.repeatScheduler.removeRepeatsForComingWeeks(rs);
			// return result wrapper
			RepeatScheduleDTO dto = new RepeatScheduleDTO();
			dto.setDeactivatedByName(rs.getDeactivatedBy().getName());
			dto.setDeactivatedOn(rs.getDeactivatedOn());
			return new ResultWrapper(true, null, dto, null);
		} else {
			return new ResultWrapper(false, "Repeat schedule not found.");
		}
	}

	@Override
	public RepeatSchedule createRepeatSchedule(Address addr, Contact con, ScheduleType type, String notes,
			TransportOption transOpt, Subdiv allocatedSubdiv) {
		RepeatSchedule rs = new RepeatSchedule();

		rs.setActive(true);
		rs.setCreatedBy(this.sessionServ.getCurrentContact());
		rs.setCreatedOn(new Date());
		rs.setAddr(addr);
		rs.setCon(con);
		rs.setNotes(notes);
		rs.setTransportOption(transOpt);
		rs.setType(type);
		rs.setOrganisation(allocatedSubdiv);

		this.save(rs);

		// manually trigger job that creates schedules for the month
		 this.repeatScheduler.bookRepeatScheduleForNew(rs);

		return rs;
	}

	@Override
	public void deactivateRepeatSchedule(RepeatSchedule repeatSchedule) {
		repeatSchedule.setActive(false);
		repeatSchedule.setDeactivatedBy(this.sessionServ.getCurrentContact());
		repeatSchedule.setDeactivatedOn(new Date());
		this.update(repeatSchedule);
	}

	@Override
	public List<RepeatSchedule> getAllActiveRepeatSchedules() {
		return this.repeatScheduleDao.getAllActive();
	}
	
	@Override
	public List<RepeatSchedule> getRepeatSchedulesFrom_Contact_TransportOption_Address(Integer transportOptionId, Integer contactId, 
			Integer addressId){
		return this.repeatScheduleDao.getRepeatSchedulesFrom_Contact_TransportOption_Address(transportOptionId, 
				contactId, addressId);
	}
	
}