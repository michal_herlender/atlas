package org.trescal.cwms.core.schedule.entity.repeatschedule;

import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Setter
@Table(name = "repeatschedule")
public class RepeatSchedule extends Allocated<Subdiv> {

	private boolean active;
	private Address addr;
	private Contact con;
	private Contact createdBy;
	private Date createdOn;
	private Contact deactivatedBy;
	private Date deactivatedOn;
	private int id;
	private String notes;
	private TransportOption transportOption;
	private ScheduleType type;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "addressid")
	@NotNull
	public Address getAddr() {
		return this.addr;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid")
	@NotNull
	public Contact getCon() {
		return this.con;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "createdbyid")
	public Contact getCreatedBy() {
		return this.createdBy;
	}

	@NotNull
	@Column(name = "createdon")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreatedOn() {
		return this.createdOn;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deactivatedbyid")
	public Contact getDeactivatedBy() {
		return this.deactivatedBy;
	}

	@Column(name = "deactivatedon")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDeactivatedOn() {
		return this.deactivatedOn;
	}

	@Length(max = 1000)
	@Column(name = "notes", length = 1000)
	public String getNotes() {
		return this.notes;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "transoptid")
	@NotNull
	public TransportOption getTransportOption() {
		return this.transportOption;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "scheduletypeid")
	public ScheduleType getType() {
		return this.type;
	}

	@Column(name = "active", columnDefinition = "tinyint")
	public boolean isActive() {
		return this.active;
	}
}