package org.trescal.cwms.core.schedule.controller;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.deliverynote.dto.TransportOptionScheduleRuleDTO;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.schedule.dto.ScheduledDeliveryDTO;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.db.ScheduledDeliveryService;
import org.trescal.cwms.core.schedule.form.ScheduleUpdaterForm;
import org.trescal.cwms.core.schedule.form.ScheduleUpdaterFormValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.translation.TranslationService;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class ScheduledDeliveriesController {
	
	@Autowired
	private TransportOptionService transportOptionService;
	@Autowired
	private ScheduledDeliveryService scheduledDeliveryService;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private ScheduleUpdaterFormValidator scheduleUpdaterFormValidator;

	public static final String FORM_NAME = "form";
	
	
	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) throws Exception {
		binder.setValidator(scheduleUpdaterFormValidator);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}
	
	@RequestMapping(value = "/scheduledeliveries.htm", method = RequestMethod.POST)
	protected String onSubmit(Model model, Locale locale, 
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@Valid @ModelAttribute(FORM_NAME) ScheduleUpdaterForm form, BindingResult bindingResult) throws Exception {
		if(bindingResult.hasErrors()){
			return referenceData(model, locale, form);
		}
		
		this.scheduledDeliveryService.updateScheduleUpdater(form, username);
		return "redirect:scheduleupdater.htm";
	}
	
	@RequestMapping(value = "/scheduledeliveries.htm", method = RequestMethod.GET)
	protected String referenceData(Model model, Locale locale, @ModelAttribute(FORM_NAME) ScheduleUpdaterForm form)
			throws Exception {
		LocalDate nextAvailableDate = transportOptionService.getNextAvailable(form.getTransportOptionId(),
				form.getScheduleDate());
		List<ScheduledDeliveryDTO> scheduleDeliveries = scheduledDeliveryService
				.pendingScheduledDeliveriesFromTransportOptionAndDate(form.getTransportOptionId(),
			form.getScheduleDate(), locale);
		Optional<ScheduledDeliveryDTO> dtoOpt = scheduleDeliveries.stream().findFirst();
		dtoOpt.ifPresent(dto -> {
			String transportOptionName = dto.getTransportOptionLocalizedName() != null
					? dto.getTransportOptionLocalizedName() : dto.getTransportMethodName();
			model.addAttribute("transportOptionName", transportOptionName);
		});
		if (nextAvailableDate != null) {
			model.addAttribute("nextDate", nextAvailableDate);
		}
		model.addAttribute("scheduleDeliveries", scheduleDeliveries);
		return "trescal/core/schedule/scheduledeliveries";
	}

	@RequestMapping(value = "/manualreschedule.json", method = RequestMethod.GET)
	@ResponseBody
	public Map<DayOfWeek, Map<LocalDate, List<TransportOptionScheduleRuleDTO>>> manualReschedule(
			@RequestParam(value = "subdivid") Integer subdivid, Locale locale) throws Exception {

		List<TransportOptionScheduleRuleDTO> listDtos = this.transportOptionService
				.getTransportOptionsSchedulable(subdivid).stream().filter(tr -> tr.isActive())
				.flatMap(transportOption -> transportOptionService.availableDateForNWeeks(transportOption, 2).stream()
						.map(date -> {
					TransportOptionScheduleRuleDTO dto = new TransportOptionScheduleRuleDTO();
					dto.setTransportOptionId(transportOption.getId());
							dto.setTransportOptionName(!StringUtils.isEmpty(transportOption.getLocalizedName())
									? transportOption.getLocalizedName()
						: translationService.getCorrectTranslation(
						transportOption.getMethod().getMethodTranslation(), locale));
					dto.setDate(date);
							dto.setDayOfWeek(date.getDayOfWeek());
					return dto;
						}))
				.sorted(Comparator.comparing(TransportOptionScheduleRuleDTO::getTransportOptionName))
				.collect(Collectors.toList());

		return getItemsMap(listDtos, locale);
	}

	/*
	 * Returns a map containing Transport Option Schedule rules
	 */
	private Map<DayOfWeek, Map<LocalDate, List<TransportOptionScheduleRuleDTO>>> getItemsMap(
			List<TransportOptionScheduleRuleDTO> dtos, Locale locale) {
		Comparator<LocalDate> comparator = Comparator.nullsFirst(Comparator.naturalOrder());
		Map<DayOfWeek, Map<LocalDate, List<TransportOptionScheduleRuleDTO>>> trMap = new TreeMap<>();
		List<DayOfWeek> days = Arrays.stream(DayOfWeek.values()).collect(Collectors.toList());
		for (TransportOptionScheduleRuleDTO dto : dtos) {
			DayOfWeek dayOfWeek = dto.getDayOfWeek();
			LocalDate date = dto.getDate();
			if (!trMap.containsKey(dayOfWeek)) {
				trMap.put(dayOfWeek, new TreeMap<>(comparator));
}
			Map<LocalDate, List<TransportOptionScheduleRuleDTO>> dateMap = trMap.get(dayOfWeek);
			if (!dateMap.containsKey(date)) {
				dateMap.put(date, new ArrayList<TransportOptionScheduleRuleDTO>());
			}
			dateMap.get(date).add(dto);
			days.remove(dayOfWeek);
		}

		for (DayOfWeek day : days) {
			trMap.put(day, new TreeMap<>());
		}

		return this.sortMapBasingOnLocale(locale, trMap);
	}

	/*
	 * Returns a map sorted by day of week basing on locale
	 */
	private Map<DayOfWeek, Map<LocalDate, List<TransportOptionScheduleRuleDTO>>> sortMapBasingOnLocale(Locale loc,
			Map<DayOfWeek, Map<LocalDate, List<TransportOptionScheduleRuleDTO>>> map) {
		WeekFields wf = WeekFields.of(loc);
		DayOfWeek day = wf.getFirstDayOfWeek();
		Map<DayOfWeek, Map<LocalDate, List<TransportOptionScheduleRuleDTO>>> trMap = new LinkedHashMap<>();
		for (int i = 0; i < DayOfWeek.values().length; i++) {
			trMap.put(day, map.get(day));
			day = day.plus(1);
		}
		return trMap;
	}

}
