package org.trescal.cwms.core.schedule.entity.schedulestatus;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum ScheduleStatus {
	PROVISIONAL("schedulestatus.provisional", "Provisional"),
	AGREED("schedulestatus.agreed", "Agreed");

	private ReloadableResourceBundleMessageSource messages;
	private Locale loc = null;
	private String messageCode;
	private String name;

	@Component
	public static class ScheduleStatusMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;

		@PostConstruct
		public void postConstruct() {
			for (ScheduleStatus rt : EnumSet.allOf(ScheduleStatus.class))
				rt.setMessageSource(messages);
		}
	}

	public void setMessageSource(ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}

	private ScheduleStatus(String messageCode, String name) {
		this.messageCode = messageCode;
		this.name = name;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		loc = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.name, loc);
		}
		return this.toString();
	}

	public static ScheduleStatus getDefaultScheduleStatus() {
		return AGREED; // Standard
	}

	public String getMessageCode() {
		return messageCode;
	}
}