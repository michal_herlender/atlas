package org.trescal.cwms.core.schedule.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.schedule.dto.ScheduleDTO;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;
import org.trescal.cwms.core.schedule.form.ShowSchedulesForm;
import org.trescal.cwms.core.schedule.view.DayByDayScheduleSearchXlsxView;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY})
public class ShowSchedulesController {

    @Autowired
    private ScheduleService schServ;
    @Autowired
    private MessageSource messages;
    @Autowired
    private TransportOptionService transportOptionService;
    @Autowired
    private DayByDayScheduleSearchXlsxView dayByDayScheduleSearchXlsxView;

    public static final String FORM_NAME = "form";

    @ModelAttribute(FORM_NAME)
    protected ShowSchedulesForm formBackingObject(
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto)
        throws Exception {
        ShowSchedulesForm form = new ShowSchedulesForm();
        form.setBusinessSubdivid(subdivDto.getKey());
        form.setBusinessCompnayid(companyDto.getKey());
        return form;
    }

    @RequestMapping(value = "/showschedules.htm", method = RequestMethod.GET)
    protected String referenceData(Model model, Locale locale,
                                   @RequestParam(value = "filter", required = false, defaultValue = "today") String filter,
                                   @RequestParam(value = "date", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
                                   @RequestParam(value = "transportOptionId", required = false, defaultValue = "0") Integer transportOptionId,
                                   @ModelAttribute(FORM_NAME) ShowSchedulesForm form) throws Exception {
        LocalDate now = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
        
        if (date == null) date = now;
        switch (filter) {
            case "today":
                form.setProvisionalSchedules(this.schServ.getSchedulesForDate(now, false, true, false,
                    form.getBusinessSubdivid(), form.getBusinessCompnayid()));
                form.setAgreedSchedules(this.schServ.getSchedulesForDate(now, false, false, false,
                    form.getBusinessSubdivid(), form.getBusinessCompnayid()));
                form.setResultsSize(form.getAgreedSchedules().size() + form.getProvisionalSchedules().size());
                form.setDescription(messages.getMessage("schedule.fortoday", null, locale) + " ("
                    + now.format(DateTimeFormatter.ISO_DATE) + ")");
                form.setNext(this.getAdjacentDate(now, true));
                form.setPrevious(this.getAdjacentDate(now, false));
                form.setFilterToday(true);
                break;
            case "date":
                String formattedDate = date.format(DateTimeFormatter.ISO_DATE);
                form.setProvisionalSchedules(this.schServ.getSchedulesForDate(date, false, true, false,
                    form.getBusinessSubdivid(), form.getBusinessCompnayid()));
                form.setAgreedSchedules(this.schServ.getSchedulesForDate(date, false, false, false, form.getBusinessSubdivid(),
                    form.getBusinessCompnayid()));
                form.setResultsSize(form.getAgreedSchedules().size() + form.getProvisionalSchedules().size());
                form.setDescription("Schedules for " + formattedDate);
                form.setNext(this.getAdjacentDate(date, true));
                form.setPrevious(this.getAdjacentDate(date, false));
                if (date.isEqual(now)) {
                    form.setFilterToday(true);
                }
                break;
            case "tomorrow":
                LocalDate tomorrow = now.plusDays(1);
                form.setProvisionalSchedules(this.schServ.getSchedulesForDate(tomorrow, false, true, false,
                    form.getBusinessSubdivid(), form.getBusinessCompnayid()));
                form.setAgreedSchedules(this.schServ.getSchedulesForDate(tomorrow, false, false, false, form.getBusinessSubdivid(),
                    form.getBusinessCompnayid()));
                form.setResultsSize(form.getAgreedSchedules().size() + form.getProvisionalSchedules().size());
                form.setDescription(messages.getMessage("schedule.fortomorrow", null, locale) + " ("
                   + tomorrow.toString() + ")");
                form.setNext(this.getAdjacentDate(tomorrow, true));
                form.setPrevious(this.getAdjacentDate(tomorrow, false));
                break;
            case "future":
                form.setProvisionalSchedules(this.schServ.getSchedulesForDate(null, true, true, false,
                    form.getBusinessSubdivid(), form.getBusinessCompnayid()));
                form.setAgreedSchedules(this.schServ.getSchedulesForDate(null, true, false, false,
                    form.getBusinessSubdivid(), form.getBusinessCompnayid()));
                form.setResultsSize(form.getAgreedSchedules().size() + form.getProvisionalSchedules().size());
                form.setDescription(messages.getMessage("schedule.forfutureonly", null, locale) + " ("
                    + DateTools.df.format(new Date()) + ")");
                break;
            case "pendingweb":
                form.setProvisionalSchedules(this.schServ.getSchedulesForDate(null, false, true, true, form.getBusinessSubdivid(),
                    form.getBusinessCompnayid()));
                form.setAgreedSchedules(new ArrayList<>());
                form.setResultsSize(form.getProvisionalSchedules().size());
                form.setDescription("Pending Schedules (From Web)");
                break;
        }
        model.addAttribute("transportOptionsSchedulable",
            transportOptionService.getTransportOptionsSchedulable(form.getBusinessSubdivid()));
        return "trescal/core/schedule/showschedules";
    }

    @RequestMapping(value = "/schedulesearch.htm", method = RequestMethod.POST, params = "export")
    public ModelAndView onExport(@ModelAttribute(FORM_NAME) ShowSchedulesForm form) {
        LocalDate today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
        List<ScheduleDTO> model = this.schServ.schedulesExport(today, form.getBusinessSubdivid());
        return new ModelAndView(dayByDayScheduleSearchXlsxView, "schedules", model);
    }

    public LocalDate getAdjacentDate(LocalDate date, boolean next) {
        return next ? date.plusDays(1) : date.minusDays(1);
    }

}