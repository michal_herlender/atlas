package org.trescal.cwms.core.schedule.entity.schedulesource;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum ScheduleSource {
	COMPANY("schedulesource.company", "Company"), WEB("schedulesource.web", "Web");

	private ReloadableResourceBundleMessageSource messages;
	private Locale loc = null;
	private String messageCode;
	private String name;

	@Component
	public static class ScheduleSourceMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;

		@PostConstruct
		public void postConstruct() {
			for (ScheduleSource rt : EnumSet.allOf(ScheduleSource.class))
				rt.setMessageSource(messages);
		}
	}

	public void setMessageSource(ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}

	private ScheduleSource(String messageCode, String name) {
		this.messageCode = messageCode;
		this.name = name;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		loc = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.name, loc);
		}
		return this.toString();
	}

	public String getMessageCode() {
		return messageCode;
	}
}