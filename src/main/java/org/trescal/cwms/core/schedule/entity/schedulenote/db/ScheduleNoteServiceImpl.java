package org.trescal.cwms.core.schedule.entity.schedulenote.db;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.schedule.dto.ScheduleNoteDTO;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedulenote.ScheduleNote;

@Service("ScheduleNoteService")
public class ScheduleNoteServiceImpl extends BaseServiceImpl<ScheduleNote, Integer> implements ScheduleNoteService
{
	@Autowired
	ScheduleNoteDao scheduleNoteDao;
	
	@Override
	protected BaseDao<ScheduleNote, Integer> getBaseDao() {
		return scheduleNoteDao;
	}
	
	@Override
	public void createScheduleNote(Schedule schedule, String note) {
		ScheduleNote scheduleNote = new ScheduleNote();
		scheduleNote.setNote(note);
		scheduleNote.setSetBy(schedule.getCreatedBy());
		scheduleNote.setActive(true);
		scheduleNote.setPublish(true);
		scheduleNote.setSchedule(schedule);
		scheduleNote.setSetOn(new Date());
		this.save(scheduleNote);
	}
	
	@Override
	public List<ScheduleNoteDTO> notesByScheduleId(Integer scheduleId){
		return this.scheduleNoteDao.notesByScheduleId(scheduleId);
	}
}