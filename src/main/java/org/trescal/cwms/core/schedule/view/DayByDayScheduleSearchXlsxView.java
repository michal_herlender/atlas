package org.trescal.cwms.core.schedule.view;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
import org.trescal.cwms.core.schedule.dto.ScheduleDTO;
import org.trescal.cwms.core.tools.DateTools;

@Component
public class DayByDayScheduleSearchXlsxView extends AbstractXlsxStreamingView {

	@Value("${cwms.config.schedule.delivery_time_minutes}")
	private int serviceTime;

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook wb, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		long startTime = System.currentTimeMillis();
		SXSSFWorkbook workbook = (SXSSFWorkbook) wb;
		@SuppressWarnings("unchecked")
		List<ScheduleDTO> agreedSchedules = (List<ScheduleDTO>) model.get("schedules");
		// create a new Excel sheet
		Sheet sheet = workbook.createSheet("Sheet1");
		sheet.setDefaultColumnWidth(25);
		// create style for header cells
		CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);

        // create style for date cells
        CellStyle dateStyle = workbook.createCellStyle();
        dateStyle.setDataFormat((short) 14);

        // create header row
        Row header = sheet.createRow(0);
        int cellNo = 0;

        header.createCell(cellNo).setCellValue("Service Time");
        header.getCell(cellNo).setCellStyle(style);
        cellNo++;

        header.createCell(cellNo).setCellValue("Eligibility");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("Route");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("Type");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("Acct #");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("Name");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("Addr1");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("Addr2");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("City");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("State");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("Zip");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("Contact1");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("Phone1");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("Contact2");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("Phone2");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("Comments");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("105");
		header.getCell(cellNo).setCellStyle(style);
		cellNo++;

		header.createCell(cellNo).setCellValue("106");
		header.getCell(cellNo).setCellStyle(style);

		long startSegmentTime = System.currentTimeMillis(); // To time export in
		// chunks/batches
		logger.debug("Initialized sheet in " + (startSegmentTime - startTime));
		response.setHeader("Content-Disposition", "attachment; filename=\"schedule_export.xlsx\"");

		int rowCount = 1;
		for (ScheduleDTO schedule : agreedSchedules) {
			Row row = sheet.createRow(rowCount++);
			cellNo = 0;
			row.createCell(cellNo).setCellValue(serviceTime);
			cellNo++;
			row.createCell(cellNo).setCellValue(DateTools.dateFromLocalDate(schedule.getScheduleDate()));
			row.getCell(cellNo).setCellStyle(dateStyle);
			cellNo++;
			row.createCell(cellNo).setCellValue(schedule.getExternalRef());
			cellNo++;
			row.createCell(cellNo).setCellValue(schedule.getType().toString());
			cellNo++;
			row.createCell(cellNo).setCellValue(schedule.getFiscalIdentifier());
			cellNo++;
			row.createCell(cellNo).setCellValue(schedule.getConame());
			cellNo++;
			row.createCell(cellNo).setCellValue(schedule.getAddr1());
			cellNo++;
			row.createCell(cellNo).setCellValue(schedule.getAddr2());
			cellNo++;
			row.createCell(cellNo).setCellValue(schedule.getTown());
			cellNo++;
			row.createCell(cellNo).setCellValue(schedule.getCountry());
			cellNo++;
			row.createCell(cellNo).setCellValue(schedule.getPostCode());
			cellNo++;
			row.createCell(cellNo).setCellValue(schedule.getContactName());
			cellNo++;
			row.createCell(cellNo).setCellValue(schedule.getTelephone());
			cellNo++;
			row.createCell(cellNo).setCellValue("");
			cellNo++;
			row.createCell(cellNo).setCellValue(schedule.getMobile());
			cellNo++;
			row.createCell(cellNo).setCellValue(schedule.getComments());
			cellNo++;
			row.createCell(cellNo).setCellValue("");
			cellNo++;
			row.createCell(cellNo).setCellValue("");
		}
	}

}
