package org.trescal.cwms.core.schedule.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ScheduleNoteDTO {
	
	private Integer id;
	private Date setOn;
	private Boolean active;
	private String setByName;
	private String note;
	
	public ScheduleNoteDTO(Integer id, Date setOn, Boolean active, String setByFirstName, String setByLastName, String note) {
		super();
		this.id = id;
		this.setOn = setOn;
		this.active = active;
		this.setByName = setByFirstName + " " + setByLastName;
		this.note = note;
	}
	
	
}
