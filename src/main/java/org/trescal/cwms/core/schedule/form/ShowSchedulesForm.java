package org.trescal.cwms.core.schedule.form;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.schedule.dto.ScheduleDTO;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class ShowSchedulesForm {
    private List<ScheduleDTO> agreedSchedules;
    private List<ScheduleDTO> provisionalSchedules;
    private String description;
    private LocalDate next;
    private LocalDate previous;
    private int resultsSize;
    private Boolean filterToday;
    private Integer businessSubdivid;
    private Integer businessCompnayid;
}