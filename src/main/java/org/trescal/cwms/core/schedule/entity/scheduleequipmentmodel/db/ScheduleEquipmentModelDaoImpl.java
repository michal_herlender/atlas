package org.trescal.cwms.core.schedule.entity.scheduleequipmentmodel.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.schedule.entity.scheduleequipmentmodel.ScheduleEquipmentModel;

@Repository("ScheduleEquipmentModelDao")
public class ScheduleEquipmentModelDaoImpl extends BaseDaoImpl<ScheduleEquipmentModel, Integer> implements ScheduleEquipmentModelDao {
	
	@Override
	protected Class<ScheduleEquipmentModel> getEntity() {
		return ScheduleEquipmentModel.class;
	}
}