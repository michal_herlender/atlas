package org.trescal.cwms.core.schedule.form;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.AutoPopulatingList;
import org.trescal.cwms.core.schedule.dto.ScheduledDeliveryDTO;

import java.time.LocalDate;

@Getter
@Setter
public class ScheduleUpdaterForm {

    private Integer subdivid;
    private Integer scheduledDelivery;
    private Integer transportOptionId;
    private LocalDate scheduleDate;
    private AutoPopulatingList<ScheduledDeliveryDTO> dto;

    public ScheduleUpdaterForm() {
        super();
        dto = new AutoPopulatingList<>(ScheduledDeliveryDTO.class);
    }

}
