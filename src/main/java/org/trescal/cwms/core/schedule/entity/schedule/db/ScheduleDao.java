package org.trescal.cwms.core.schedule.entity.schedule.db;

import java.time.LocalDate;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.schedule.dto.CompanyWebSchedulesDTO;
import org.trescal.cwms.core.schedule.dto.ScheduleDTO;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;
import org.trescal.cwms.core.schedule.form.ScheduleSearchForm;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface ScheduleDao extends BaseDao<Schedule, Integer> {

    Schedule findRepeatScheduleForDate(Contact contact, Address address, ScheduleType type, LocalDate date);

    List<CompanyWebSchedulesDTO> getAllCompanyWebSchedulesRequestedForPeriod(Company company, LocalDate dateFrom,
                                                                             LocalDate dateTo);
    
    List<Schedule> getSchedulesForAddress(int addrId, Boolean includeHistory, ScheduleType type);

    List<Schedule> getSchedulesForContact(int personId, Boolean includeHistory, ScheduleType type);

    List<ScheduleDTO> getSchedulesForDate(LocalDate date, Boolean futureDate, Boolean provisional, Boolean sourceWeb, Subdiv allocatedSubdiv, Company allocatedCompany);

    List<Schedule> getSchedulesForSubdiv(int subdivId, Boolean includeHistory, ScheduleType type);

    List<Schedule> schedulesFromAddress(Integer addrid, Subdiv allocatedSubdiv);

    PagedResultSet<ScheduleDTO> searchSchedules(ScheduleSearchForm form, Subdiv allocatedSubdiv, Company allactedCompany, PagedResultSet<ScheduleDTO> prs);

    List<Schedule> schedulesByTransportOption(Integer transportOptionId, Boolean provisional);

    List<ScheduleDTO> schedulesExport(LocalDate date, Subdiv allocatedSubdiv);
    
    Schedule scheduleFromTransportOptionAndDateAndSubdiv(Integer transportOptionId, LocalDate scheduleDate, 
    		Integer destinationSubdivId);
}