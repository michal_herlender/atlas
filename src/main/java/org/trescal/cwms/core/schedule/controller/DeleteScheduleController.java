package org.trescal.cwms.core.schedule.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;

@Controller @IntranetController
public class DeleteScheduleController
{
	@Autowired
	private ScheduleService schServ;
	@Autowired
	private DeliveryService deliveryService;

	@RequestMapping(value="/deleteschedule.htm")
	public ModelAndView handleRequestInternal(HttpServletRequest req, HttpServletResponse res) throws Exception
	{
		int id = ServletRequestUtils.getIntParameter(req, "schid");
		Schedule schedule = this.schServ.get(id);

		this.schServ.delete(schedule);

		return new ModelAndView(new RedirectView("schedulesearch.htm"));
	}
	
	@RequestMapping(value="/removefromschedule.htm")
	public void removeDeliveryFromSchedule(int delid) throws Exception
	{
		deliveryService.removeDeliveryFromSchedule(delid);
	}
}