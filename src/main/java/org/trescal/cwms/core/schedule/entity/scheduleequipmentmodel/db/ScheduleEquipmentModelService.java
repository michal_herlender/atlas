package org.trescal.cwms.core.schedule.entity.scheduleequipmentmodel.db;

import java.util.List;

import org.trescal.cwms.core.schedule.entity.scheduleequipmentmodel.ScheduleEquipmentModel;

public interface ScheduleEquipmentModelService
{

	/**
	 * Deletes the given {@link ScheduleEquipmentModel} from the database.
	 * 
	 * @param scheduleequipmentmodel the {@link ScheduleEquipmentModel} to
	 *        delete.
	 */
	void deleteScheduleEquipmentModel(ScheduleEquipmentModel scheduleequipmentmodel);

	/**
	 * Returns the {@link ScheduleEquipmentModel} entity with the given ID.
	 * 
	 * @param id the {@link ScheduleEquipmentModel} ID.
	 * @return the {@link ScheduleEquipmentModel}
	 */
	ScheduleEquipmentModel findScheduleEquipmentModel(int id);

	List<ScheduleEquipmentModel> getAllScheduleEquipmentModels();

	/**
	 * Inserts the given {@link insertScheduleEquipmentModel} into the database.
	 * 
	 * @param scheduleequipmentmodel the {@link insertScheduleEquipmentModel} to
	 *        insert.
	 */
	void insertScheduleEquipmentModel(ScheduleEquipmentModel scheduleequipmentmodel);

	void saveOrUpdateScheduleEquipmentModel(ScheduleEquipmentModel scheduleequipmentmodel);

	/**
	 * Updates the given {@link updateScheduleEquipmentModel} in the database.
	 * 
	 * @param scheduleequipmentmodel the {@link updateScheduleEquipmentModel} to
	 *        update.
	 */
	void updateScheduleEquipmentModel(ScheduleEquipmentModel scheduleequipmentmodel);
}