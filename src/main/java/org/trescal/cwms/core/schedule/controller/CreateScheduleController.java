package org.trescal.cwms.core.schedule.controller;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.schedule.dto.ScheduleResultDTO;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;
import org.trescal.cwms.core.schedule.entity.schedulesource.ScheduleSource;
import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;
import org.trescal.cwms.core.schedule.form.CreateScheduleForm;
import org.trescal.cwms.core.schedule.form.CreateScheduleFormValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME })
public class CreateScheduleController {

	@Autowired
	private ScheduleService schServ;
	@Autowired
	private CreateScheduleFormValidator validator;
	@Autowired
	private TransportOptionService transportOptionService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private TranslationService translationService;

	public static final String FORM_NAME = "form";
	public static final String VIEW_NAME = "trescal/core/schedule/createschedule";

	@ModelAttribute(FORM_NAME)
	protected CreateScheduleForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		CreateScheduleForm form = new CreateScheduleForm();
		form.setBusinessSubdivid(subdivDto.getKey());
		form.setScheduleTypes(Arrays.asList(ScheduleType.values()));
		form.setScheduleStatuses(Arrays.asList(ScheduleStatus.values()));
		form.setCoroles(EnumSet.complementOf(EnumSet.of(CompanyRole.NOTHING)));
		// Automatically set to Company source until web implementation:
		form.setSource(ScheduleSource.COMPANY);
		return form;
	}

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}

	@RequestMapping(value = "/createschedule.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(Model model, @ModelAttribute(FORM_NAME) CreateScheduleForm form,
			@RequestParam(value = "scheduleExistsId", required = false) String scheduleExistsId) {
		List<TransportOption> transportOptionsSchedulable = this.transportOptionService
				.getTransportOptionsSchedulable(form.getBusinessSubdivid());
		model.addAttribute("transportOptionsSchedulable", transportOptionsSchedulable);
		model.addAttribute("scheduleExists", scheduleExistsId);
		if (transportOptionsSchedulable.isEmpty()) {
			model.addAttribute("noActiveDOW", true);
		}
		return new ModelAndView(VIEW_NAME);
	}

	@RequestMapping(value = "/createschedule.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(Model model, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@Valid @ModelAttribute(FORM_NAME) CreateScheduleForm form, BindingResult bindingResult) throws Exception {

		if (bindingResult.hasErrors()) {
			// get the id of schedule already created with the same
			// address, the same transport option and the same date.
			if(bindingResult.getFieldError().getField().equals("schedulesCreated")){
				return referenceData(model, form, bindingResult.getFieldError().getArguments()[0].toString());
			}
			return referenceData(model, form, "");
		}

		Schedule schedule = this.schServ.createSchedule(form, username);
		return new ModelAndView(new RedirectView("viewschedule.htm?schid=" + schedule.getScheduleId()));
	}

	@RequestMapping(value = "/getScheduleResult.json", method = RequestMethod.GET)
	@ResponseBody
	public ScheduleResultDTO getScheduleResult(
			@RequestParam(value = "addrid", required = true) Integer addrid,
			@RequestParam(value = "subdivid", required = false) Integer subdivid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Locale locale)
			throws Exception {

		ScheduleResultDTO dto = new ScheduleResultDTO();
		Subdiv allocatedSubdiv = new Subdiv();
		if(subdivid != null){
			// in "Assign Delivery Note to Schedule" case, 
			// we get the subdivision from the current delivery
            allocatedSubdiv = subdivService.get(subdivid);
        } else {
            // in the "Create Schedule" case, we get the subdivision from the
            // log-in subdivision
            allocatedSubdiv = subdivService.get(subdivDto.getKey());
        }
        TransportOption transportOptionOut = transportOptionService.getTransportOptionOut(addressService.get(addrid),
            subdivService.get(subdivDto.getKey()));
        TransportOption transportOptionIn = transportOptionService.getTransportOptionIn(addressService.get(addrid),
            subdivService.get(subdivDto.getKey()));

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", locale);

        Map<Integer, String> schedulesCreatedForAddressMap = new HashMap<Integer, String>();
        schServ.schedulesFromAddress(addrid, allocatedSubdiv).stream().forEach(sch -> {
            schedulesCreatedForAddressMap.put(sch.getScheduleId(), dateFormat.format(sch.getLastModified()) + " -"
                + (!StringUtils.isEmpty(sch.getTransportOption().getLocalizedName()) ? sch.getTransportOption().getLocalizedName()
                : translationService.getCorrectTranslation(
                sch.getTransportOption().getMethod().getMethodTranslation(), locale)));
        });

        if (transportOptionOut != null) {
            dto.setTransportOptionOutId(transportOptionOut.getId());
            dto.setTransportOptionOutName(
                !StringUtils.isEmpty(transportOptionOut.getLocalizedName()) ? transportOptionOut.getLocalizedName()
                    : translationService.getCorrectTranslation(
                    transportOptionOut.getMethod().getMethodTranslation(), locale));
        }

        if (transportOptionIn != null) {
            dto.setTransportOptionInId(transportOptionIn.getId());
            dto.setTransportOptionInName(
                !StringUtils.isEmpty(transportOptionIn.getLocalizedName()) ? transportOptionIn.getLocalizedName()
                    : translationService.getCorrectTranslation(
                    transportOptionIn.getMethod().getMethodTranslation(), locale));
        }

        if (!schedulesCreatedForAddressMap.isEmpty()) {
            dto.setSchedulesCreatedForAddressMap(schedulesCreatedForAddressMap);
        }

        return dto;
    }
}