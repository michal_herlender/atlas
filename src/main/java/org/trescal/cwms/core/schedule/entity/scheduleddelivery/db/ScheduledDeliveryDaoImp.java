package org.trescal.cwms.core.schedule.entity.scheduleddelivery.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery_;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem_;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod_;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption_;
import org.trescal.cwms.core.schedule.dto.ScheduledDeliveryDTO;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule_;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.ScheduledDelivery;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.ScheduledDelivery_;
import org.trescal.cwms.core.schedule.entity.scheduleddeliverystatus.ScheduledDeliveryStatus;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@Repository("ScheduledDeliveryDao")
public class ScheduledDeliveryDaoImp extends BaseDaoImpl<ScheduledDelivery, Integer> implements ScheduledDeliveryDao {

	@Override
	protected Class<ScheduledDelivery> getEntity() {
		return ScheduledDelivery.class;
	}

	@Override
	public List<ScheduledDelivery> scheduledDeliveryFromScheduleAndOrDelivery(Integer scheduleId, Integer deliveryId) {
		return getResultList(cb -> {
			CriteriaQuery<ScheduledDelivery> cq = cb.createQuery(ScheduledDelivery.class);
			Root<ScheduledDelivery> root = cq.from(ScheduledDelivery.class);
			Join<ScheduledDelivery, Schedule> scheduleJoin = root.join(ScheduledDelivery_.schedule);
			Join<ScheduledDelivery, Delivery> deliveryJoin = root.join(ScheduledDelivery_.delivery);
			Predicate clauses = cb.conjunction();
			if (scheduleId != null) {
				clauses.getExpressions().add(cb.equal(scheduleJoin.get(Schedule_.scheduleId), scheduleId));
			}
			if (deliveryId != null) {
				clauses.getExpressions().add(cb.equal(deliveryJoin.get(Delivery_.deliveryid), deliveryId));
			}
			cq.where(clauses);
			return cq;
		});
	}
	
	@Override
	public Set<ScheduledDeliveryDTO> pendingScheduledDeliveries(Subdiv allocatedSubdiv, Locale locale) {
		
		List<ScheduledDeliveryDTO> resultList = getResultList(cb ->{
			CriteriaQuery<ScheduledDeliveryDTO> cq = cb.createQuery(ScheduledDeliveryDTO.class);
			Root<ScheduledDelivery> root = cq.from(ScheduledDelivery.class);
			Join<ScheduledDelivery, Schedule> scheduleJoin = root.join(ScheduledDelivery_.schedule);
			Join<Schedule, TransportOption> transportOptionJoin = scheduleJoin.join(Schedule_.transportOption);
			Join<TransportOption, TransportMethod> transportMethodJoin = transportOptionJoin.join(TransportOption_.method);
			Join<ScheduledDelivery, Delivery> deliveryJoin = root.join(ScheduledDelivery_.delivery);
			Expression<String> transportName = joinTranslation(cb, transportMethodJoin,
					TransportMethod_.methodTranslation, locale);
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(transportOptionJoin.get(TransportOption_.sub), allocatedSubdiv));
			clauses.getExpressions().add(cb.equal(root.get(ScheduledDelivery_.status), ScheduledDeliveryStatus.PENDING));
			
			cq.select(cb.construct(ScheduledDeliveryDTO.class,
					root.get(ScheduledDelivery_.id),
					transportOptionJoin.get(TransportOption_.id),	
					transportOptionJoin.get(TransportOption_.localizedName),	
					transportName,
					deliveryJoin.get(Delivery_.deliveryid),
					scheduleJoin.get(Schedule_.scheduleId),
					scheduleJoin.get(Schedule_.scheduleDate)
					));
			cq.where(clauses);
			
			return cq;
		});
		return new HashSet<ScheduledDeliveryDTO>(resultList);

	}

	@Override
	public List<ScheduledDeliveryDTO> pendingScheduledDeliveriesFromTransportOptionAndDate(Integer transportOptionId,
																						   LocalDate scheduleDate, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<ScheduledDeliveryDTO> cq = cb.createQuery(ScheduledDeliveryDTO.class);
			Root<ScheduledDelivery> root = cq.from(ScheduledDelivery.class);
			Join<ScheduledDelivery, Schedule> scheduleJoin = root.join(ScheduledDelivery_.schedule);
			Join<Schedule, TransportOption> transportOptionJoin = scheduleJoin.join(Schedule_.transportOption);
			Join<TransportOption, TransportMethod> transportMethodJoin = transportOptionJoin.join(TransportOption_.method);
			Join<ScheduledDelivery, Delivery> deliveryJoin = root.join(ScheduledDelivery_.delivery);

			Join<Delivery, Contact> deliveryContact = deliveryJoin.join(Delivery_.contact, JoinType.LEFT);
			Join<Delivery, Address> deliveryAddress = deliveryJoin.join(Delivery_.address, JoinType.LEFT);
			Join<Contact, Subdiv> deliverySub = deliveryContact.join(Contact_.sub);
			Join<Subdiv, Company> deliveryComp = deliverySub.join(Subdiv_.comp);
			Expression<String> transportName = joinTranslation(cb, transportMethodJoin,
					TransportMethod_.methodTranslation, locale);
			
			Subquery<Long> deliveryItemSq = cq.subquery(Long.class);
			Root<DeliveryItem> delItemRoot = deliveryItemSq.from(DeliveryItem.class);
			deliveryItemSq.select(cb.count(delItemRoot));
			deliveryItemSq.where(cb.equal(delItemRoot.get(DeliveryItem_.delivery), deliveryJoin));
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(transportOptionJoin.get(TransportOption_.id), transportOptionId));
			clauses.getExpressions().add(cb.equal(scheduleJoin.get(Schedule_.scheduleDate), scheduleDate));
			clauses.getExpressions().add(cb.equal(root.get(ScheduledDelivery_.status), ScheduledDeliveryStatus.PENDING));
			
			cq.where(clauses);
			cq.distinct(true);
			cq.select(cb.construct(ScheduledDeliveryDTO.class,
					root.get(ScheduledDelivery_.id),
					transportOptionJoin.get(TransportOption_.id),	
					transportOptionJoin.get(TransportOption_.localizedName),	
					transportName,
					deliveryJoin.get(Delivery_.deliveryid),
					scheduleJoin.get(Schedule_.scheduleId),
					deliveryJoin.get(Delivery_.deliveryno),
					deliveryItemSq.getSelection(),
					deliveryComp.get(Company_.coname),
					cb.concat(deliveryAddress.get(Address_.addr1),
							cb.concat(" ", deliveryAddress.get(Address_.addr2)))
					));
			return cq;
		});
	}
	
}
