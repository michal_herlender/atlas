package org.trescal.cwms.core.schedule.entity.scheduleddelivery.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.schedule.dto.ScheduledDeliveryDTO;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.ScheduledDelivery;
import org.trescal.cwms.core.schedule.form.ScheduleUpdaterForm;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public interface ScheduledDeliveryService extends BaseService<ScheduledDelivery, Integer> {

    void updateScheduleDelivery(ScheduledDelivery scheduledDelivery);

    void delete(ScheduledDelivery scheduledDelivery);

    void addScheduledDelivery(Delivery delivery, Schedule schedule);

    List<ScheduledDelivery> scheduledDeliveryFromScheduleAndOrDelivery(Integer scheduleId, Integer deliveryId);

    Set<ScheduledDeliveryDTO> pendingScheduledDeliveries(Subdiv allocatedSubdiv, Locale locale);

    List<ScheduledDeliveryDTO> pendingScheduledDeliveriesFromTransportOptionAndDate(Integer transportOptionId,
                                                                                    LocalDate scheduleDate, Locale locale);

    void updateScheduleUpdater(ScheduleUpdaterForm form, String username);
}
