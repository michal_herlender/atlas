package org.trescal.cwms.core.schedule.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.schedule.dto.ScheduledDeliveryDTO;
import org.trescal.cwms.core.schedule.entity.scheduleddeliverystatus.ScheduledDeliveryStatus;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class ScheduleUpdaterFormValidator extends AbstractBeanValidator {

	public boolean supports(Class<?> clazz)
	 {
		return clazz.equals(ScheduleUpdaterForm.class);
	 }
	
	public void validate(Object target, Errors errors)
	{
		ScheduleUpdaterForm form = (ScheduleUpdaterForm) target;
		
		int index = 0;
		for(ScheduledDeliveryDTO dto : form.getDto()) {
			if (dto.getScheduledDeliveryStatus().equals(ScheduledDeliveryStatus.COMPLETE)
				&& dto.getClientReceiptDate() == null) {
				errors.rejectValue("dto[" + index + "].clientReceiptDate", "schedule.error.receiptdate");
			} else if (dto.getScheduledDeliveryStatus().equals(ScheduledDeliveryStatus.RESCHEDULED_MANUAL)
				&& dto.getNewDate() == null) {
				errors.rejectValue("dto[" + index + "].scheduledDeliveryStatus", "transportoptions.error.notnull");
			}
			index++;
		}
	}
	
}
