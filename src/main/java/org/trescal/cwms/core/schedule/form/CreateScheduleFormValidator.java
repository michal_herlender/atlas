package org.trescal.cwms.core.schedule.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import java.time.LocalDate;
import java.util.List;

@Component
public class CreateScheduleFormValidator extends AbstractBeanValidator {

	@Autowired
	private TransportOptionService transportOptionService;
	@Autowired
	private ScheduleService scheduleService;
	@Autowired
	private SubdivService subdivService;

	@Override
	public boolean supports(Class<?> arg0) {
		return CreateScheduleForm.class.isAssignableFrom(arg0);
	}

	public void validate(Object target, Errors errors) {

		super.validate(target, errors);

		if (!errors.hasErrors()) {
			CreateScheduleForm form = (CreateScheduleForm) target;
			Subdiv allocatedSubdiv = this.subdivService.get(form.getBusinessSubdivid());
			List<Schedule> schedulesCreatedForAddress = scheduleService.schedulesFromAddress(form.getAddrid(), allocatedSubdiv);
			
			// validation only for creation schedule screen (not for assign schedule screen)
			if(form.getDelivery() == null){
				if (form.getCoid() == null ) {
					errors.rejectValue("coid", "error.instrument.company.notnull", null,
							"A company must be specified");
				}
				else if (form.getPersonid() == null) {
					errors.rejectValue("personid", "error.instrument.contact.notnull", null,
							"A contact must be specified");
				}
				else if(form.getAddrid() == null) {
					errors.rejectValue("addrid", "error.instrument.address.notnull", null,
							"An address must be specified");
				}
			}		

			// Restrict note size
			if (form.getNote() != null) {
				if (form.getNote().length() > 1000) {
					errors.rejectValue("note", "error.createeditscheduleform.note", null,
							"The note must not be over 1000 characters in length");
				}
			}
			
			// Manual option must have a specific date
			if(form.getManualEntryDate()) {
				if(form.getScheduleDate() == null){
					errors.rejectValue("scheduleDate", "schedule.error.scheduledate", null,
							"Schedule Date must be specified");
				}
				// if there is a schedule with the same address, schedule Date selected 
				// and transport option, we block the creation of a new schedule
				// the user should use the existing schedule
				else {
					for(Schedule schedule : schedulesCreatedForAddress){
						if (schedule.getScheduleDate().isEqual(form.getScheduleDate())
							&& schedule.getTransportOption().getId() == form.getTransportOptionId()
							&& schedule.getAddress().getAddrid().equals(form.getAddrid())) {
							errors.rejectValue("schedulesCreated", "schedule.error.duplicateschedule", new Object[]{schedule.getScheduleId()},
								"There is already a schedule with the same transport option/address/date");
							break;
						}
					}
				}
			} else {
				LocalDate nextAvailableDate = transportOptionService.getNextAvailable(form.getTransportOptionId(), null);
				// no available day of week for the transport option selected
				if (nextAvailableDate == null) {
					errors.rejectValue("transportOptionId", "schedule.error.no.nextavailable", null,
						"No schedule rules for the selected transport option");
				} else {
					// if there is a schedule with the same address, next available date
					// and transport option, we block the creation of a new schedule
					// the user should use the existing schedule
					for (Schedule schedule : schedulesCreatedForAddress) {
						if (schedule.getScheduleDate().isEqual(nextAvailableDate)
							&& schedule.getTransportOption().getId() == form.getTransportOptionId()
							&& schedule.getAddress().getAddrid().equals(form.getAddrid())) {

							errors.rejectValue("schedulesCreated", "schedule.error.duplicateschedule", new Object[]{schedule.getScheduleId()},
								"There is already a schedule with the same transport option/address/date");
							break;
						}
					}
				}
			}
		}
	}
}