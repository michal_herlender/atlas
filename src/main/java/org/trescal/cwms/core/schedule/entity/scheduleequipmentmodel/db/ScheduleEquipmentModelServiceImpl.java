package org.trescal.cwms.core.schedule.entity.scheduleequipmentmodel.db;

import java.util.List;

import org.trescal.cwms.core.schedule.entity.scheduleequipmentmodel.ScheduleEquipmentModel;

public class ScheduleEquipmentModelServiceImpl implements ScheduleEquipmentModelService
{
	ScheduleEquipmentModelDao scheduleEquipmentModelDao;

	public ScheduleEquipmentModel findScheduleEquipmentModel(int id)
	{
		return scheduleEquipmentModelDao.find(id);
	}

	public void insertScheduleEquipmentModel(ScheduleEquipmentModel ScheduleEquipmentModel)
	{
		scheduleEquipmentModelDao.persist(ScheduleEquipmentModel);
	}

	public void updateScheduleEquipmentModel(ScheduleEquipmentModel ScheduleEquipmentModel)
	{
		scheduleEquipmentModelDao.update(ScheduleEquipmentModel);
	}

	public List<ScheduleEquipmentModel> getAllScheduleEquipmentModels()
	{
		return scheduleEquipmentModelDao.findAll();
	}

	public void setScheduleEquipmentModelDao(ScheduleEquipmentModelDao scheduleEquipmentModelDao)
	{
		this.scheduleEquipmentModelDao = scheduleEquipmentModelDao;
	}

	public void deleteScheduleEquipmentModel(ScheduleEquipmentModel scheduleequipmentmodel)
	{
		this.scheduleEquipmentModelDao.remove(scheduleequipmentmodel);
	}

	public void saveOrUpdateScheduleEquipmentModel(ScheduleEquipmentModel scheduleequipmentmodel)
	{
		this.scheduleEquipmentModelDao.saveOrUpdate(scheduleequipmentmodel);
	}
}