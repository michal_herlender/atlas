package org.trescal.cwms.core.schedule.entity.scheduleequipment.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.schedule.entity.scheduleequipment.ScheduleEquipment;

public interface ScheduleEquipmentDao extends BaseDao<ScheduleEquipment, Integer> {}