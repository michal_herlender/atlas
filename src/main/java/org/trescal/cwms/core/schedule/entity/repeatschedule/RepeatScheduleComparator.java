package org.trescal.cwms.core.schedule.entity.repeatschedule;

import java.util.Comparator;

public class RepeatScheduleComparator implements Comparator<RepeatSchedule>
{
	public int compare(RepeatSchedule rs1, RepeatSchedule rs2)
	{
		if ((rs1.getCreatedOn() == null) || (rs2.getCreatedOn() == null))
		{
			Integer id1 = rs1.getId();
			Integer id2 = rs2.getId();

			return id1.compareTo(id2);
		}
		else
		{
			if (rs1.getCreatedOn().equals(rs2.getCreatedOn()))
			{
				Integer id1 = rs1.getId();
				Integer id2 = rs2.getId();

				return id1.compareTo(id2);
			}

			return rs2.getCreatedOn().compareTo(rs1.getCreatedOn());
		}

	}
}
