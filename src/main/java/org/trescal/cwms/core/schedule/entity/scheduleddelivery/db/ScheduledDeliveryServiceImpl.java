package org.trescal.cwms.core.schedule.entity.scheduleddelivery.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.deliverynote.form.EditDeliveryNoteForm;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.schedule.dto.ScheduledDeliveryDTO;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.ScheduledDelivery;
import org.trescal.cwms.core.schedule.entity.scheduleddeliverystatus.ScheduledDeliveryStatus;
import org.trescal.cwms.core.schedule.form.ScheduleUpdaterForm;
import org.trescal.cwms.core.tools.DateTools;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@Service("ScheduledDeliveryService")
public class ScheduledDeliveryServiceImpl extends BaseServiceImpl<ScheduledDelivery, Integer>
	implements ScheduledDeliveryService {

	@Autowired
	private ScheduledDeliveryDao scheduledDeliveryDao;
	@Autowired
	private DeliveryService deliveryService;
	@Autowired
	private TransportOptionService transportOptionService;
	@Autowired
	private ScheduleService scheduleService;
	@Autowired
	private UserService userService;

	@Override
	protected BaseDao<ScheduledDelivery, Integer> getBaseDao() {
		return this.scheduledDeliveryDao;
	}

	@Override
	public void updateScheduleDelivery(ScheduledDelivery scheduledDelivery) {
		this.scheduledDeliveryDao.merge(scheduledDelivery);
	}

	@Override
	public void delete(ScheduledDelivery scheduledDelivery) {
		this.scheduledDeliveryDao.remove(scheduledDelivery);
	}

    @Override
    public void addScheduledDelivery(Delivery delivery, Schedule schedule) {
        ScheduledDelivery scheduledDelivery = new ScheduledDelivery();
        scheduledDelivery.setStatus(ScheduledDeliveryStatus.PENDING);
        scheduledDelivery.setDelivery(delivery);
        scheduledDelivery.setSchedule(schedule);
        this.save(scheduledDelivery);
    }

    @Override
    public List<ScheduledDelivery> scheduledDeliveryFromScheduleAndOrDelivery(Integer scheduleId, Integer deliveryId) {
        return this.scheduledDeliveryDao.scheduledDeliveryFromScheduleAndOrDelivery(scheduleId, deliveryId);
    }

    @Override
    public Set<ScheduledDeliveryDTO> pendingScheduledDeliveries(Subdiv allocatedSubdiv, Locale locale) {
        return this.scheduledDeliveryDao.pendingScheduledDeliveries(allocatedSubdiv, locale);
    }

    @Override
    public List<ScheduledDeliveryDTO> pendingScheduledDeliveriesFromTransportOptionAndDate(Integer transportOptionId,
                                                                                           LocalDate scheduleDate, Locale locale) {
        return this.scheduledDeliveryDao.pendingScheduledDeliveriesFromTransportOptionAndDate(transportOptionId, scheduleDate, locale);
    }

    @Override
    public void updateScheduleUpdater(ScheduleUpdaterForm form, String username) {

        for (ScheduledDeliveryDTO dto : form.getDto()) {
            ScheduledDelivery scheduledDelivery = this.get(dto.getScheduledDeliveryId());
            if (dto.getScheduledDeliveryStatus().equals(ScheduledDeliveryStatus.COMPLETE)) {
                EditDeliveryNoteForm editDeliveryNoteForm = new EditDeliveryNoteForm();
                Delivery delivery = deliveryService.get(dto.getDeliveryId());
                deliveryService.editDeliveryNote(delivery, delivery.getDeliverydate(), editDeliveryNoteForm);
                if (dto.getClientReceiptDate() != null)
                    deliveryService.updateDestinationReceiptDate(delivery, DateTools.localDateToZonedDateTime(dto.getClientReceiptDate()), delivery.getOrganisation().getComp().getCoid());
                scheduledDelivery.setStatus(ScheduledDeliveryStatus.COMPLETE);
            } else if (dto.getScheduledDeliveryStatus().equals(ScheduledDeliveryStatus.RESCHEDULED_AUTO)) {
                Schedule schedule = scheduledDelivery.getSchedule();
                LocalDate newScheduleDate = this.transportOptionService.getNextAvailable(schedule.getTransportOption().getId(), null);
                schedule.setLastModifiedBy(userService.get(username).getCon());
                schedule.setLastModified(new Timestamp(System.currentTimeMillis()));
                scheduledDelivery.setStatus(ScheduledDeliveryStatus.PENDING);
                if (newScheduleDate != null) {
                    schedule.setScheduleDate(newScheduleDate);
                }
                this.scheduleService.merge(schedule);

            } else if (dto.getScheduledDeliveryStatus().equals(ScheduledDeliveryStatus.RESCHEDULED_MANUAL)) {
                Schedule schedule = scheduledDelivery.getSchedule();
                schedule.setLastModifiedBy(userService.get(username).getCon());
                schedule.setLastModified(new Timestamp(System.currentTimeMillis()));
                scheduledDelivery.setStatus(ScheduledDeliveryStatus.PENDING);
                schedule.setTransportOption(this.transportOptionService.get(dto.getNewTransportOptionId()));
                schedule.setScheduleDate(dto.getNewDate());
                this.scheduleService.merge(schedule);
            }
            this.updateScheduleDelivery(scheduledDelivery);
        }
    }


}
