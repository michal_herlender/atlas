package org.trescal.cwms.core.schedule.entity.scheduleequipment;

import java.util.Comparator;
import java.util.Set;

import org.trescal.cwms.core.schedule.entity.scheduleequipmentinstrument.ScheduleEquipmentInstrument;
import org.trescal.cwms.core.schedule.entity.scheduleequipmentmodel.ScheduleEquipmentModel;

/**
 * Complicated comparator used for comparing a {@link Set} of
 * {@link ScheduleEquipment}.
 * 
 * @author Stuart
 */
public class ScheduleEquipmentComparator implements Comparator<ScheduleEquipment>
{
	@Override
	public int compare(ScheduleEquipment o1, ScheduleEquipment o2)
	{
		// both instruments?
		if ((o1 instanceof ScheduleEquipmentInstrument)
				&& (o2 instanceof ScheduleEquipmentInstrument))
		{
			// cast instruments
			ScheduleEquipmentInstrument inst1 = (ScheduleEquipmentInstrument) o1;
			ScheduleEquipmentInstrument inst2 = (ScheduleEquipmentInstrument) o2;

			// both mfr generic?
			if (!inst1.getEquipInst().getModel().getModelMfrType().isMfrRequiredForModel()
					&& !inst2.getEquipInst().getModel().getModelMfrType().isMfrRequiredForModel())
			{
				// mfr names are equal?
				if (inst1.getEquipInst().getMfr().getName().equals(inst2.getEquipInst().getMfr().getName()))
				{
					// model names are equal?
					if (inst1.getEquipInst().getModel().getModel().equals(inst2.getEquipInst().getModel().getModel()))
					{
						// description names are equal?
						if (inst1.getEquipInst().getModel().getDescription().getDescription().equals(inst2.getEquipInst().getModel().getDescription().getDescription()))
						{
							Integer int1 = inst1.getEquipInst().getPlantid();
							Integer int2 = inst2.getEquipInst().getPlantid();
							// compare ids
							return int1.compareTo(int2);
						}
						// compare model descriptions
						else
						{
							return inst1.getEquipInst().getModel().getDescription().getDescription().compareTo(inst2.getEquipInst().getModel().getDescription().getDescription());
						}

					}
					// compare model names
					else
					{
						return inst1.getEquipInst().getModel().getModel().compareTo(inst2.getEquipInst().getModel().getModel());
					}
				}
				// compare mfr names
				else
				{
					return inst1.getEquipInst().getMfr().getName().compareTo(inst2.getEquipInst().getMfr().getName());
				}
			}
			// both mfr specific?
			else if (inst1.getEquipInst().getModel().getModelMfrType().isMfrRequiredForModel()
					&& inst2.getEquipInst().getModel().getModelMfrType().isMfrRequiredForModel())
			{
				// mfr names are equal?
				if (inst1.getEquipInst().getModel().getMfr().getName().equals(inst2.getEquipInst().getModel().getMfr().getName()))
				{
					// model names are equal?
					if (inst1.getEquipInst().getModel().getModel().equals(inst2.getEquipInst().getModel().getModel()))
					{
						// descriptions names are equal?
						if (inst1.getEquipInst().getModel().getDescription().getDescription().equals(inst2.getEquipInst().getModel().getDescription().getDescription()))
						{
							Integer int1 = inst1.getEquipInst().getPlantid();
							Integer int2 = inst2.getEquipInst().getPlantid();
							// compare ids
							return int1.compareTo(int2);
						}
						// compare description text
						else
						{
							return inst1.getEquipInst().getModel().getDescription().getDescription().compareTo(inst2.getEquipInst().getModel().getDescription().getDescription());
						}
					}
					// compare model names
					else
					{
						return inst1.getEquipInst().getModel().getModel().compareTo(inst2.getEquipInst().getModel().getModel());
					}
				}
				// compare mfr names
				else
				{
					return inst1.getEquipInst().getModel().getMfr().getName().compareTo(inst2.getEquipInst().getModel().getMfr().getName());
				}
			}
			// one mfr generic one mfr specific?
			else if (!inst1.getEquipInst().getModel().getModelMfrType().isMfrRequiredForModel()
					&& inst2.getEquipInst().getModel().getModelMfrType().isMfrRequiredForModel())
			{
				return -1;
			}
			// one mfr specific one mfr generic?
			else
			{
				return 1;
			}
		}
		// both models?
		else if ((o1 instanceof ScheduleEquipmentModel)
				&& (o2 instanceof ScheduleEquipmentModel))
		{
			// cast models
			ScheduleEquipmentModel mod1 = (ScheduleEquipmentModel) o1;
			ScheduleEquipmentModel mod2 = (ScheduleEquipmentModel) o2;

			// both have mfrs?
			if ((mod1.getEquipModel().getMfr() != null)
					&& (mod2.getEquipModel().getMfr() != null))
			{
				// mfr names equal?
				if (mod1.getEquipModel().getMfr().getName().equals(mod2.getEquipModel().getMfr().getName()))
				{
					// model names equal?
					if (mod1.getEquipModel().getModel().equals(mod2.getEquipModel().getModel()))
					{
						// descriptions equal?
						if (mod1.getEquipModel().getDescription().getDescription().equals(mod2.getEquipModel().getDescription().getDescription()))
						{
							Integer int1 = mod1.getEquipModel().getModelid();
							Integer int2 = mod2.getEquipModel().getModelid();
							// compare ids
							return int1.compareTo(int2);
						}
						// compare description names
						else
						{
							return mod1.getEquipModel().getDescription().getDescription().compareTo(mod2.getEquipModel().getDescription().getDescription());
						}
					}
					// compare model names
					else
					{
						return mod1.getEquipModel().getModel().compareTo(mod2.getEquipModel().getModel());
					}
				}
				// compare mfr names
				else
				{
					return mod1.getEquipModel().getMfr().getName().compareTo(mod2.getEquipModel().getMfr().getName());
				}
			}
			// mod1 has mfr mod2 doesn't
			else if ((mod1.getEquipModel().getMfr() != null)
					&& (mod2.getEquipModel().getMfr() == null))
			{
				return -1;
			}
			// mod1 doesn't have mfr mod2 has mfr
			else
			{
				return 1;
			}
		}
		// one instrument one model?
		else if ((o1 instanceof ScheduleEquipmentInstrument)
				&& (o2 instanceof ScheduleEquipmentModel))
		{
			return -1;
		}
		// one model one instrument?
		else
		{
			return 1;
		}
	}
}
