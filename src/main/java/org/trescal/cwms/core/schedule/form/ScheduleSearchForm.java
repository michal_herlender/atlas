package org.trescal.cwms.core.schedule.form;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.schedule.dto.ScheduleDTO;
import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ScheduleSearchForm {
    private Integer companyId;
    private Integer transportOptionId;
    private Integer coid;
    private LocalDate dateBegin;
    private LocalDate dateEnd;
    private int dateSearchType;
    private LocalDate dateSingle;
    private boolean includeHistory;
    private int numOfNewWebSchedules;
    private ScheduleStatus status;
    private List<ScheduleStatus> scheduleStatuses;
    private Set<CompanyRole> coroles;

    // results data
    private PagedResultSet<ScheduleDTO> rs;
    private int pageNo;
    private int resultsPerPage;

}