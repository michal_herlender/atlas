package org.trescal.cwms.core.schedule.entity.repeatschedule;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class RepeatScheduleDTO {

    private String deactivatedByName;
    private Date deactivatedOn;

}
