package org.trescal.cwms.core.schedule.entity.scheduleddelivery;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.scheduleddeliverystatus.ScheduledDeliveryStatus;

import lombok.Setter;

@Entity
@Setter
@Table(name = "scheduleddelivery")
public class ScheduledDelivery {

	private Integer id;
	private Delivery delivery;
	private Schedule schedule;
	private ScheduledDeliveryStatus status;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getId() {
		return id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deliveryid", foreignKey = @ForeignKey(name = "FK_scheduleddelivery_delivery"), nullable = false)
	public Delivery getDelivery() {
		return delivery;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scheduleid", foreignKey = @ForeignKey(name = "FK_scheduleddelivery_schedule"), nullable = false)
	public Schedule getSchedule() {
		return schedule;
	}

	@NotNull
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "scheduleddeliverystatusid", nullable = false)
	public ScheduledDeliveryStatus getStatus() {
		return status;
	}

}
