package org.trescal.cwms.core.schedule.form;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class EditScheduleForm {

    private Schedule schedule;
    private Integer personid;
    private Integer addrid;
    private Integer transportOptionid;
    private ScheduleType type;
    private ScheduleStatus status;
    private List<ScheduleStatus> scheduleStatuses;
    private List<ScheduleType> scheduleTypes;
    @NotNull
    private LocalDate scheduleDate;


}
