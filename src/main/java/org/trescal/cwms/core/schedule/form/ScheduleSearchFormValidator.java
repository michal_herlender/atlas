package org.trescal.cwms.core.schedule.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class ScheduleSearchFormValidator extends AbstractBeanValidator
{
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(ScheduleSearchForm.class);
	}

	public void validate(Object target, Errors errors)
	{
		ScheduleSearchForm command = (ScheduleSearchForm) target;

		super.validate(command, errors);
		// If range of dates, check that the specified begin date is not
		// chronologically after the end date
		if ((command.getDateBegin() != null) && (command.getDateEnd() != null)
				&& (command.getDateSearchType() == 2))
		{
			if (command.getDateBegin().compareTo(command.getDateEnd()) > 0)
			{
				errors.rejectValue("dateEnd", "error.schedulesearchform.dateEnd", null, "The end date must be after the before date.");
			}
		}

		
	}
}