package org.trescal.cwms.core.schedule.entity.scheduleequipmentinstrument.db;

import java.util.List;

import org.trescal.cwms.core.schedule.entity.scheduleequipmentinstrument.ScheduleEquipmentInstrument;

public class ScheduleEquipmentInstrumentServiceImpl implements ScheduleEquipmentInstrumentService
{
	ScheduleEquipmentInstrumentDao scheduleEquipmentInstrumentDao;

	public ScheduleEquipmentInstrument findScheduleEquipmentInstrument(int id)
	{
		return scheduleEquipmentInstrumentDao.find(id);
	}

	public void insertScheduleEquipmentInstrument(ScheduleEquipmentInstrument ScheduleEquipmentInstrument)
	{
		scheduleEquipmentInstrumentDao.persist(ScheduleEquipmentInstrument);
	}

	public void updateScheduleEquipmentInstrument(ScheduleEquipmentInstrument ScheduleEquipmentInstrument)
	{
		scheduleEquipmentInstrumentDao.update(ScheduleEquipmentInstrument);
	}

	public List<ScheduleEquipmentInstrument> getAllScheduleEquipmentInstruments()
	{
		return scheduleEquipmentInstrumentDao.findAll();
	}

	public void setScheduleEquipmentInstrumentDao(ScheduleEquipmentInstrumentDao scheduleEquipmentInstrumentDao)
	{
		this.scheduleEquipmentInstrumentDao = scheduleEquipmentInstrumentDao;
	}

	public void deleteScheduleEquipmentInstrument(ScheduleEquipmentInstrument scheduleequipmentinstrument)
	{
		this.scheduleEquipmentInstrumentDao.remove(scheduleequipmentinstrument);
	}

	public void saveOrUpdateScheduleEquipmentInstrument(ScheduleEquipmentInstrument scheduleequipmentinstrument)
	{
		this.scheduleEquipmentInstrumentDao.saveOrUpdate(scheduleequipmentinstrument);
	}
}