package org.trescal.cwms.core.schedule.entity.scheduleequipment.db;

import java.util.List;

import org.trescal.cwms.core.schedule.entity.scheduleequipment.ScheduleEquipment;

public interface ScheduleEquipmentService
{

	/**
	 * Deletes the given {@link ScheduleEquipment} from the database.
	 * 
	 * @param scheduleequipment the {@link ScheduleEquipment} to delete.
	 */
	void deleteScheduleEquipment(ScheduleEquipment scheduleequipment);

	/**
	 * Returns the {@link ScheduleEquipment} entity with the given ID.
	 * 
	 * @param id the {@link ScheduleEquipment} ID.
	 * @return the {@link ScheduleEquipment}
	 */
	ScheduleEquipment findScheduleEquipment(int id);

	List<ScheduleEquipment> getAllScheduleEquipment();

	/**
	 * Inserts the given {@link ScheduleEquipment} into the database.
	 * 
	 * @param scheduleequipment the {@link ScheduleEquipment} to insert.
	 */
	void insertScheduleEquipment(ScheduleEquipment scheduleequipment);

	void saveOrUpdateScheduleEquipment(ScheduleEquipment scheduleequipment);

	/**
	 * Updates the given {@link ScheduleEquipment} in the database.
	 * 
	 * @param scheduleequipment the {@link ScheduleEquipment} to update.
	 */
	void updateScheduleEquipment(ScheduleEquipment scheduleequipment);
}