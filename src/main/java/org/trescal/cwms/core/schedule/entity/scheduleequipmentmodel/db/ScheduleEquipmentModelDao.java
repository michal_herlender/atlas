package org.trescal.cwms.core.schedule.entity.scheduleequipmentmodel.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.schedule.entity.scheduleequipmentmodel.ScheduleEquipmentModel;

public interface ScheduleEquipmentModelDao extends BaseDao<ScheduleEquipmentModel, Integer> {}