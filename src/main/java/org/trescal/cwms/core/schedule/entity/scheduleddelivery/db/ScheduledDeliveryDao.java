package org.trescal.cwms.core.schedule.entity.scheduleddelivery.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.schedule.dto.ScheduledDeliveryDTO;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.ScheduledDelivery;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public interface ScheduledDeliveryDao extends BaseDao<ScheduledDelivery, Integer> {

    List<ScheduledDelivery> scheduledDeliveryFromScheduleAndOrDelivery(Integer scheduleId, Integer deliveryId);

    Set<ScheduledDeliveryDTO> pendingScheduledDeliveries(Subdiv allocatedSubdiv, Locale locale);

    List<ScheduledDeliveryDTO> pendingScheduledDeliveriesFromTransportOptionAndDate(Integer transportOptionId,
                                                                                    LocalDate scheduleDate, Locale locale);
}
