package org.trescal.cwms.core.schedule.entity.schedulenote.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.schedule.dto.ScheduleNoteDTO;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedulenote.ScheduleNote;

/**
 * Interface for accessing and manipulating {@link ScheduleNote} entities.
 */
public interface ScheduleNoteService extends BaseService<ScheduleNote, Integer> {
	
	void createScheduleNote(Schedule schedule, String note);
	
	List<ScheduleNoteDTO> notesByScheduleId(Integer scheduleId);
}