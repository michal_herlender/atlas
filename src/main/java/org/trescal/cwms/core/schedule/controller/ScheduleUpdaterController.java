package org.trescal.cwms.core.schedule.controller;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.schedule.dto.ScheduledDeliveryDTO;
import org.trescal.cwms.core.schedule.dto.ScheduledDeliveryDTOComparator;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.db.ScheduledDeliveryService;
import org.trescal.cwms.core.schedule.form.ScheduleUpdaterForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class ScheduleUpdaterController {

	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ScheduledDeliveryService scheduledDeliveryService;

	public static final String FORM_NAME = "form";

	@ModelAttribute(FORM_NAME)
	protected ScheduleUpdaterForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		ScheduleUpdaterForm form = new ScheduleUpdaterForm();
		form.setSubdivid(subdivDto.getKey());
		return form;
	}

	@RequestMapping(value = "/scheduleupdater.htm", method = RequestMethod.POST)
	protected String onSubmit(Model model, Locale locale, @ModelAttribute(FORM_NAME) ScheduleUpdaterForm form,
			final RedirectAttributes redirectAttributes)
			throws Exception {
		redirectAttributes.addFlashAttribute("form", form);
		return "redirect:scheduledeliveries.htm";
	}
	
	@RequestMapping(value = "/scheduleupdater.htm", method = RequestMethod.GET)
	protected String referenceData(Model model, Locale locale, @ModelAttribute(FORM_NAME) ScheduleUpdaterForm form)
			throws Exception {
		
		Subdiv allocatedSubdiv = subdivService.get(form.getSubdivid());
		Set<ScheduledDeliveryDTO>  scheduledDeliveries = scheduledDeliveryService.pendingScheduledDeliveries(allocatedSubdiv, locale);
		Map<LocalDate, Map<Integer, Set<ScheduledDeliveryDTO>>> dtoMap = getItemsMap(scheduledDeliveries);

		model.addAttribute("dtoMap", dtoMap);
		return "trescal/core/schedule/scheduleupdater";
	}
	
	private Map<LocalDate, Map<Integer, Set<ScheduledDeliveryDTO>>> getItemsMap(Set<ScheduledDeliveryDTO> items) {
		
		Map<LocalDate, Map<Integer, Set<ScheduledDeliveryDTO>>> dateTransportOptionMap = new LinkedHashMap<>();

		List<ScheduledDeliveryDTO> sortedItems = items.stream().sorted(new ScheduledDeliveryDTOComparator())
				.collect(Collectors.toList());

		for (ScheduledDeliveryDTO dto : sortedItems) {

			LocalDate date = dto.getScheduleDate(); 
			Integer transportOptionId = dto.getTransportOptionId();
			if (!dateTransportOptionMap.containsKey(date)) {
				dateTransportOptionMap.put(date, new TreeMap<>());
			}
			Map<Integer, Set<ScheduledDeliveryDTO>> transportOptionMap = dateTransportOptionMap.get(date);

			if (!transportOptionMap.containsKey(transportOptionId)) {
				transportOptionMap.put(transportOptionId, new TreeSet<ScheduledDeliveryDTO>(new ScheduledDeliveryDTOComparator()));
			}
			transportOptionMap.get(transportOptionId).add(dto);
		}

		return dateTransportOptionMap;
	}

}
