package org.trescal.cwms.core.schedule.form;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedulesource.ScheduleSource;
import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class CreateScheduleForm {

    private int businessSubdivid;
    private Integer addrid;
    private Integer coid;
    private Integer personid;
    private Integer schId;
    private Integer subdivid;
    private String note;
    private ScheduleSource source;
    private ScheduleStatus status;
    private ScheduleType type;
    private LocalDate scheduleDate;
    private Boolean manualEntryDate;
    private Delivery delivery;
    private Set<CompanyRole> coroles;
    private List<ScheduleStatus> scheduleStatuses;
    private List<ScheduleType> scheduleTypes;
    private List<Schedule> validSchedules;
    private Boolean alreadyAssigned;
    private Boolean schedulesCreated;
    @NotNull
    private Integer transportOptionId;
    private Date currentDate = new Date();

}