package org.trescal.cwms.core.schedule.dto;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;
import org.trescal.cwms.core.schedule.entity.scheduleddeliverystatus.ScheduledDeliveryStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScheduledDeliveryDTO {

    private Integer scheduledDeliveryId;
    private Integer transportOptionId;
    private String transportOptionLocalizedName;
    private String transportMethodName;
    private Integer deliveryId;
    private Integer scheduleId;
    private String deliveryNo;
    private Long deliveryItemCount;
    private String deliveryCompany;
    private String deliveryAddress;
    private LocalDate clientReceiptDate;
    private LocalDate scheduleDate;
    private ScheduledDeliveryStatus scheduledDeliveryStatus;
    private Integer newTransportOptionId;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate newDate;

    public ScheduledDeliveryDTO() {
        super();
    }

    public ScheduledDeliveryDTO(Integer scheduledDeliveryId, Integer transportOptionId, String transportOptionLocalizedName,
                                String transportMethodName, Integer deliveryId, Integer scheduleId, LocalDate scheduleDate) {
        super();
        this.scheduledDeliveryId = scheduledDeliveryId;
        this.transportOptionId = transportOptionId;
        this.transportOptionLocalizedName = transportOptionLocalizedName;
        this.transportMethodName = transportMethodName;
        this.scheduleId = scheduleId;
        this.scheduleDate = scheduleDate;
    }

    public ScheduledDeliveryDTO(Integer scheduledDeliveryId, Integer transportOptionId,
                                String transportOptionLocalizedName, String transportMethodName, Integer deliveryId, Integer scheduleId,
                                String deliveryNo, Long deliveryItemCount, String deliveryCompany, String deliveryAddress) {
        super();
        this.scheduledDeliveryId = scheduledDeliveryId;
        this.transportOptionId = transportOptionId;
        this.transportOptionLocalizedName = transportOptionLocalizedName;
        this.transportMethodName = transportMethodName;
        this.deliveryId = deliveryId;
		this.scheduleId = scheduleId;
		this.deliveryNo = deliveryNo;
		this.deliveryItemCount = deliveryItemCount;
		this.deliveryCompany = deliveryCompany;
		this.deliveryAddress = deliveryAddress;
	}

	public ScheduledDeliveryDTO(Integer scheduledDeliveryId, Integer scheduleId, Long deliveryItemCount) {
		super();
		this.scheduledDeliveryId = scheduledDeliveryId;
		this.scheduleId = scheduleId;
		this.deliveryItemCount = deliveryItemCount;
	}

	
}
