package org.trescal.cwms.core.schedule.dto;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.schedule.entity.schedulesource.ScheduleSource;
import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class ScheduleDTO {

    private Integer scheduleId;
    private LocalDate scheduleDate;
    private ScheduleSource source;
    private ScheduleType type;
    private ScheduleStatus status;
    private Integer transportOptionId;
    private Boolean repeatSchedule;
    private String organisationName;
    private Integer coid;
    private String coname;
    private Boolean companyActive;
    private Boolean companyOnStop;
    private Integer subdivid;
    private String subname;
    private Boolean subdivActive;
    private String externalRef;
    private String fiscalIdentifier;
    private String addr1;
    private String addr2;
    private String addr3;
    private String town;
    private String country;
    private String county;
    private String postCode;
    private Integer personid;
    private String contactName;
    private Boolean contactActive;
    private String telephone;
    private String mobile;
    private String email;
    private String comments;
    private Long scheduleEquipmentsCount;
    private Long scheduledDeliveryCount;
    private List<ScheduleNoteDTO> notes;

    public ScheduleDTO(Integer scheduleId, LocalDate scheduleDate, ScheduleSource source, ScheduleType type,
                       ScheduleStatus status, Integer transportOptionId, Boolean repeatSchedule, String organisationName, Integer coid, String coname, Boolean companyActive,
                       Boolean companyOnStop, Integer subdivid, String subname, Boolean subdivActive, String addr1,
                       String addr2, String addr3, String town, String country, String county, String postCode, Integer personid, String firstName, String lastName,
                       Boolean contactActive, String telephone, String mobile, String email, Long scheduleEquipmentsCount,
                       Long scheduledDeliveryCount) {
        super();
        this.scheduleId = scheduleId;
        this.scheduleDate = scheduleDate;
        this.source = source;
        this.type = type;
        this.status = status;
        this.transportOptionId = transportOptionId;
        this.repeatSchedule = repeatSchedule;
        this.organisationName = organisationName;
        this.coid = coid;
        this.coname = coname;
        this.companyActive = companyActive;
        this.companyOnStop = companyOnStop;
        this.subdivid = subdivid;
        this.subname = subname;
        this.subdivActive = subdivActive;
        this.addr1 = addr1;
        this.addr2 = addr2;
        this.addr3 = addr3;
        this.town = town;
		this.country = country;
		this.county = county;
        this.postCode = postCode;
        this.personid = personid;
        this.contactName = firstName + " " + lastName;
        this.contactActive = contactActive;
        this.telephone = telephone;
        this.mobile = mobile;
        this.email = email;
        this.scheduleEquipmentsCount = scheduleEquipmentsCount;
        this.scheduledDeliveryCount = scheduledDeliveryCount;
    }

    // construct used to export schedule data (workwave)
    public ScheduleDTO(Integer scheduleId, LocalDate scheduleDate, ScheduleType type, String externalRef, String fiscalIdentifier, String coname,
                       String addr1, String addr2, String town, String country, String postCode, String firstName, String lastName,
                       String telephone, String mobile, String comments) {

        super();
        this.scheduleId = scheduleId;
        this.scheduleDate = scheduleDate;
        this.type = type;
        this.externalRef = externalRef;
        this.fiscalIdentifier = fiscalIdentifier;
        this.coname = coname;
        this.addr1 = addr1;
        this.addr2 = addr2;
        this.town = town;
        this.country = country;
        this.postCode = postCode;
        this.contactName = firstName + " " + lastName;
        this.telephone = telephone;
        this.mobile = mobile;
        this.comments = comments;
    }

}
