package org.trescal.cwms.core.schedule.entity.scheduleequipmentmodel;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.schedule.entity.scheduleequipment.ScheduleEquipment;

@Entity
@DiscriminatorValue("model")
public class ScheduleEquipmentModel extends ScheduleEquipment
{
	private InstrumentModel equipModel;
	private Integer qty;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelid")
	public InstrumentModel getEquipModel()
	{
		return this.equipModel;
	}

	@Column(name = "qty")
	public Integer getQty()
	{
		return this.qty;
	}

	@Override
	@Transient
	public boolean isScheduleEquipmentInstrument()
	{
		return false;
	}

	public void setEquipModel(InstrumentModel equipModel)
	{
		this.equipModel = equipModel;
	}

	public void setQty(Integer qty)
	{
		this.qty = qty;
	}
}