package org.trescal.cwms.core.schedule.entity.schedulenote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;

@Entity
@Table(name = "schedulenote")
public class ScheduleNote extends Note
{
	private Schedule schedule;
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.SCHEDULENOTE;
	}
	
	/**
	 * @return the schedule
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scheduleid")
	public Schedule getSchedule() {
		return schedule;
	}
	
	/**
	 * @param schedule the schedule to set
	 */
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
}