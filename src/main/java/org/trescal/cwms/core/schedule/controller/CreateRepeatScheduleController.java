package org.trescal.cwms.core.schedule.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.schedule.entity.repeatschedule.db.RepeatScheduleService;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@JsonController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class CreateRepeatScheduleController {

	@Autowired
	private RepeatScheduleService repeatScheduleService;
	@Autowired
	private MessageSource messages;

	@RequestMapping(value = "/createschedulerepeat.json")
	@ResponseBody
	public ResultWrapper createScheduleRepeat(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@RequestParam(value = "addrid", required = true) Integer addrid,
			@RequestParam(value = "personid", required = true) Integer personid,
			@RequestParam(value = "transoptionid", required = false) Integer transoptionid,
			@RequestParam(value = "notes", required = false) String notes) throws Exception {

		if (transoptionid == null || transoptionid == 0) {
			return new ResultWrapper(false, this.messages.getMessage("transportoptions.error.notnull", null,
					"A Transport Option must be specified", null), null, null);
		} else {
			return this.repeatScheduleService.ajaxCreateRepeatSchedule(addrid, personid, ScheduleType.COLLECTION, notes,
					transoptionid, subdivDto.getKey());
		}
	}

	@RequestMapping(value = "/deactivaterepeatschedule.json")
	@ResponseBody
	public ResultWrapper deactivateRepeatSchedule(@RequestParam(value = "rsid", required = true) Integer rsid)
			throws Exception {
		return this.repeatScheduleService.ajaxDeactivateRepeatSchedule(rsid);
	}
}
