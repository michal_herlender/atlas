package org.trescal.cwms.core.schedule.entity;

/**
 * Enumeration of the different turnaround options for schedule equipment.
 * 
 * @author Stuart
 */
public enum ScheduleEquipmentTurn
{
	FAST_TRACK("FAST TRACK", 3), SAME_DAY("SAME DAY", 1), STANDARD("STANDARD", 7);

	String turn;
	int turnDays;

	ScheduleEquipmentTurn(String turn, int turnDays)
	{
		this.turn = turn;
		this.turnDays = turnDays;
	}

	public String getTurn()
	{
		return this.turn;
	}

	public int getTurnDays()
	{
		return this.turnDays;
	}

	public void setTurn(String turn)
	{
		this.turn = turn;
	}

	public void setTurnDays(int turnDays)
	{
		this.turnDays = turnDays;
	}

}
