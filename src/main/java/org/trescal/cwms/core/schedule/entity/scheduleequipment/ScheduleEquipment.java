package org.trescal.cwms.core.schedule.entity.scheduleequipment;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.schedule.entity.ScheduleEquipmentTurn;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

/**
 * Superclass for and entities.
 * 
 * @author stuarth
 */
@Entity
@Table(name = "scheduleequipment")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public abstract class ScheduleEquipment extends Auditable
{
	private CalibrationType caltype;
	private String commentDesc;
	private String faultDesc;
	private Boolean faulty;
	private int id;
	private String ponumber;
	private Schedule schedule;
	private String supportDesc;
	private Boolean supported;
	private ScheduleEquipmentTurn turn;

	public ScheduleEquipment()
	{

	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "caltypeid")
	public CalibrationType getCaltype()
	{
		return this.caltype;
	}

	/**
	 * @return the fault description
	 */
	@Column(name = "commentdesc", length = 250)
	public String getCommentDesc()
	{
		return this.commentDesc;
	}

	/**
	 * @return the fault description
	 */
	@Column(name = "faultdesc", length = 250)
	public String getFaultDesc()
	{
		return this.faultDesc;
	}

	/**
	 * @return the fault field
	 */
	@Column(name = "faulty", columnDefinition="tinyint")
	public Boolean getFaulty()
	{
		return this.faulty;
	}

	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Column(name = "ponumber", length = 60)
	public String getPonumber()
	{
		return this.ponumber;
	}

	/**
	 * @return the schedule
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scheduleid")
	public Schedule getSchedule()
	{
		return this.schedule;
	}

	/**
	 * @return the support description
	 */
	@Column(name = "supportdesc", length = 250)
	public String getSupportDesc()
	{
		return this.supportDesc;
	}

	/**
	 * @return the supported field
	 */
	@Column(name = "supported", columnDefinition="tinyint")
	public Boolean getSupported()
	{
		return this.supported;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "turn", nullable = false)
	public ScheduleEquipmentTurn getTurn()
	{
		return this.turn;
	}

	@Transient
	public abstract boolean isScheduleEquipmentInstrument();

	public void setCaltype(CalibrationType caltype)
	{
		this.caltype = caltype;
	}

	public void setCommentDesc(String commentDesc)
	{
		this.commentDesc = commentDesc;
	}

	public void setFaultDesc(String faultDesc)
	{
		this.faultDesc = faultDesc;
	}

	public void setFaulty(Boolean faulty)
	{
		this.faulty = faulty;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setPonumber(String ponumber)
	{
		this.ponumber = ponumber;
	}

	/**
	 * @param schedule the schedule to set
	 */
	public void setSchedule(Schedule schedule)
	{
		this.schedule = schedule;
	}

	public void setSupportDesc(String supportDesc)
	{
		this.supportDesc = supportDesc;
	}

	public void setSupported(Boolean supported)
	{
		this.supported = supported;
	}

	public void setTurn(ScheduleEquipmentTurn turn)
	{
		this.turn = turn;
	}

}