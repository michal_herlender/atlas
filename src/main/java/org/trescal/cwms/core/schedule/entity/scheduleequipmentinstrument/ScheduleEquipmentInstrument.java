package org.trescal.cwms.core.schedule.entity.scheduleequipmentinstrument;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.schedule.entity.scheduleequipment.ScheduleEquipment;

@Entity
@DiscriminatorValue("instrument")
public class ScheduleEquipmentInstrument extends ScheduleEquipment
{
	private Instrument equipInst;

	/**
	 * @return the inst
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid")
	public Instrument getEquipInst()
	{
		return this.equipInst;
	}

	@Override
	@Transient
	public boolean isScheduleEquipmentInstrument()
	{
		return true;
	}

	public void setEquipInst(Instrument equipInst)
	{
		this.equipInst = equipInst;
	}

}