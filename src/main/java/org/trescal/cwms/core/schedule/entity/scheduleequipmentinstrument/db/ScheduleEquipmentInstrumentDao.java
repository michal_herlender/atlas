package org.trescal.cwms.core.schedule.entity.scheduleequipmentinstrument.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.schedule.entity.scheduleequipmentinstrument.ScheduleEquipmentInstrument;

public interface ScheduleEquipmentInstrumentDao extends BaseDao<ScheduleEquipmentInstrument, Integer> {}