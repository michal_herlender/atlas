package org.trescal.cwms.core.schedule.entity.repeatschedule;

public interface RepeatSchedulerService {
	void bookRepeatScheduleForNew(RepeatSchedule rs);
	void bookRepeatSchedules();
	int removeRepeatsForComingWeeks(RepeatSchedule rs);
}
