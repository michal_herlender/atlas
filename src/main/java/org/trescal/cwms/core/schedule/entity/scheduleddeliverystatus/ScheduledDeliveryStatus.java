package org.trescal.cwms.core.schedule.entity.scheduleddeliverystatus;

public enum ScheduledDeliveryStatus {

	PENDING("Pending"), // 0
	CANCELLED("Cancelled"), // 1 
	RESCHEDULED_AUTO("Rescheduled automatic"), // 2
	RESCHEDULED_MANUAL("Rescheduled manaul"), // 3
	COMPLETE("Complete"); // 4

	private String message;

	private ScheduledDeliveryStatus(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
