package org.trescal.cwms.core.schedule.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.ScheduledDelivery;
import org.trescal.cwms.core.schedule.entity.scheduleddelivery.db.ScheduledDeliveryService;
import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.schedule.entity.scheduletype.ScheduleType;
import org.trescal.cwms.core.schedule.form.EditScheduleForm;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.tools.DateTools;

@Controller
@IntranetController
public class ViewScheduleController {

	private static final Logger logger = LoggerFactory.getLogger(ViewScheduleController.class);
	public static final String FORM_NAME = "form";
	
	@Autowired
	private ScheduleService schServ;
	@Autowired
	private TransportOptionService transportOptionService;
	@Autowired
	private ScheduledDeliveryService scheduledDeliveryService;

	@ModelAttribute(FORM_NAME)
	protected EditScheduleForm formBackingObject(
			@RequestParam(value = "schid", required = false, defaultValue = "0") Integer schId) throws Exception {
		EditScheduleForm form = new EditScheduleForm();
		form.setSchedule(schServ.get(schId));
		form.setScheduleTypes(Arrays.asList(ScheduleType.values()));
		form.setScheduleStatuses(Arrays.asList(ScheduleStatus.values()));
		return form;
	}

	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	@RequestMapping(value = "/viewschedule.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(
			@Valid @ModelAttribute(FORM_NAME) EditScheduleForm form, BindingResult bindingResult) throws Exception {
		
		if(bindingResult.hasErrors()){
			return referenceData(form);
		}
		
		this.schServ.editSchedule(form);
		String tab = "&loadtab=editschedule-tab";
		return new ModelAndView(new RedirectView("viewschedule.htm?schid="+ form.getSchedule().getScheduleId() + tab));
	}

	@RequestMapping(value = "/viewschedule.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(
			 @ModelAttribute(FORM_NAME) EditScheduleForm form) throws Exception {
		logger.info("Loading ViewScheduleController referenceData model...");
		
		List<ScheduledDelivery> scheduledDeliveryList = this.scheduledDeliveryService.scheduledDeliveryFromScheduleAndOrDelivery(form.getSchedule().getScheduleId(), null);
		Map<String, Object> refData = new HashMap<String, Object>();
		refData.put("privateOnlyNotes", NoteType.SCHEDULENOTE.isPrivateOnly());
		refData.put("scheduleDeliveries", scheduledDeliveryList);
		refData.put("transportOptionsWithActiveMethod", this.transportOptionService.getTransportOptionsSchedulable(form.getSchedule().getOrganisation().getId()));
		
		return new ModelAndView("trescal/core/schedule/viewschedule", refData);
	}
}