package org.trescal.cwms.core.schedule.entity.scheduleequipmentinstrument.db;

import java.util.List;

import org.trescal.cwms.core.schedule.entity.scheduleequipmentinstrument.ScheduleEquipmentInstrument;

public interface ScheduleEquipmentInstrumentService
{

	/**
	 * Deletes the given {@link ScheduleEquipmentInstrument} from the database.
	 * 
	 * @param scheduleequipmentinstrument the
	 *        {@link ScheduleEquipmentInstrument} to delete.
	 */
	void deleteScheduleEquipmentInstrument(ScheduleEquipmentInstrument scheduleequipmentinstrument);

	/**
	 * Returns the {@link ScheduleEquipmentInstrument} entity with the given ID.
	 * 
	 * @param id the {@link ScheduleEquipmentInstrument} ID.
	 * @return the {@link ScheduleEquipmentInstrument}
	 */
	ScheduleEquipmentInstrument findScheduleEquipmentInstrument(int id);

	List<ScheduleEquipmentInstrument> getAllScheduleEquipmentInstruments();

	/**
	 * Inserts the given {@link ScheduleEquipmentInstrument} into the database.
	 * 
	 * @param scheduleequipmentinstrument the
	 *        {@link ScheduleEquipmentInstrument} to insert.
	 */
	void insertScheduleEquipmentInstrument(ScheduleEquipmentInstrument scheduleequipmentinstrument);

	void saveOrUpdateScheduleEquipmentInstrument(ScheduleEquipmentInstrument scheduleequipmentinstrument);

	/**
	 * Updates the given {@link ScheduleEquipmentInstrument} in the database.
	 * 
	 * @param scheduleequipmentinstrument the
	 *        {@link ScheduleEquipmentInstrument} to update.
	 */
	void updateScheduleEquipmentInstrument(ScheduleEquipmentInstrument scheduleequipmentinstrument);
}