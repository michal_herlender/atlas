package org.trescal.cwms.core.schedule.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumSet;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.schedule.dto.ScheduleDTO;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;
import org.trescal.cwms.core.schedule.entity.schedulestatus.ScheduleStatus;
import org.trescal.cwms.core.schedule.form.ScheduleSearchForm;
import org.trescal.cwms.core.schedule.form.ScheduleSearchFormValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY })
public class ScheduleSearchController {
	@Autowired
	private ScheduleService schServ;
	@Autowired
	private ScheduleSearchFormValidator validator;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private CompanyService compService;

	public static final String FORM_NAME = "form";

	@ModelAttribute(FORM_NAME)
	protected ScheduleSearchForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto)
			throws Exception {
		ScheduleSearchForm form = new ScheduleSearchForm();
		form.setNumOfNewWebSchedules(
				this.schServ.getSchedulesForDate(null, false, true, true, subdivDto.getKey(), companyDto.getKey()).size());
		form.setScheduleStatuses(Arrays.asList(ScheduleStatus.values()));
		form.setCoroles(EnumSet.complementOf(EnumSet.of(CompanyRole.NOTHING)));
		return form;
	}

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) throws Exception{
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}

	@RequestMapping(value = "/schedulesearch.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		return new ModelAndView("trescal/core/schedule/schedulesearch");
	}

	@RequestMapping(value = "/schedulesearch.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest req,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@Valid @ModelAttribute(FORM_NAME) ScheduleSearchForm form, BindingResult bindingResult) throws Exception {

		if (bindingResult.hasErrors()) {
			return referenceData(subdivDto);
		}

		Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
		Company allocatedCompany = this.compService.get(companyDto.getKey());

		if (req.getParameter("searchformsubmitted").equals("advancedsearch")) {
			PagedResultSet<ScheduleDTO> resultSet = this.schServ.searchSchedules(form, allocatedSubdiv,
					allocatedCompany, new PagedResultSet<ScheduleDTO>(form.getResultsPerPage(), form.getPageNo()));
			form.setRs(resultSet);
			return new ModelAndView("trescal/core/schedule/schedulesearchresults");
		}

		else if (req.getParameter("searchformsubmitted").equals("quicksearch")) {
			if (req.getParameter("quicksearch").equals("today")) {
				return new ModelAndView(new RedirectView("showschedules.htm?filter=today"));
			} else if (req.getParameter("quicksearch").equals("tomorrow")) {
				return new ModelAndView(new RedirectView("showschedules.htm?filter=tomorrow"));
			} else if (req.getParameter("quicksearch").equals("future")) {
				return new ModelAndView(new RedirectView("showschedules.htm?filter=future"));
			} else {
				return new ModelAndView(new RedirectView("showschedules.htm"));
			}
		} else if (req.getParameter("searchformsubmitted").equals("pendingweb")) {
			return new ModelAndView(new RedirectView("showschedules.htm?filter=pendingweb"));
		} else {
			return new ModelAndView(new RedirectView("showschedules.htm"));
		}
	}
}