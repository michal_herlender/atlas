package org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemand.db;

import java.util.Collection;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemand.JobItemDemand;

public interface JobItemDemandService extends BaseService<JobItemDemand, Integer> {
	
	boolean[] convertToBooleanArray(Collection<JobItemDemand> demands);
}