package org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public interface BatchCalibrationDao extends BaseDao<BatchCalibration, Integer> {
	
	List<BatchCalibration> getBatchCalibrationsWithIds(List<Integer> batchIds);
	
	List<JobItem> getDistinctItemsFromBatches(List<Integer> batchIds);
	
	List<BatchCalibration> getOtherCompleteBatchesOnJob(int jobId, int batchId);
}