package org.trescal.cwms.core.jobs.jobitem.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;


@Component
public class WorkRequirementValidator extends AbstractBeanValidator {

    @Autowired
    private CapabilityService capabilityService;

    public static final String ERROR_CODE_BLANK = "error.workrequirement.blank";
    public static final String ERROR_MESSAGE_BLANK = "A Procedure must be selected";

    public static final String ERROR_CODE_MISMATCH = "error.workrequirement.mismatch";
    public static final String ERROR_MESSAGE_MISMATCH = "The Work Instruction and Procedure do not match.  If you select a Work Instruction, the Procedure can be left blank because it will be auto-populated.";

    public static final String ERROR_CODE_PROC_MISMATCH = "error.workrequirement.procedure.mismatch";
    public static final String ERROR_MESSAGE_PROC_MISMATCH = "The Procedure and Subdivision do not match.";

	public static final String ERROR_CODE_NOT_EDITABLE = "error.workrequirement.noteditable";
	public static final String ERROR_MESSAGE_NOT_EDITABLE = "No edits are permitted because you do not have access to the Subdivision for the Work Requirement.";
	
	public static final String ERROR_CODE_VALUE_NOT_SELECTED = "error.value.notselected"; 

	@Override
	public boolean supports(Class<?> arg0) {
		return WorkRequirementForm.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		WorkRequirementForm form = (WorkRequirementForm) target;
		if (!form.isEditable()) {
            errors.reject(ERROR_CODE_NOT_EDITABLE, null, ERROR_MESSAGE_NOT_EDITABLE);
        }

        Capability capability = null;

        if (form.getProcedureId().length() > 0) {
            capability = capabilityService.get(Integer.valueOf(form.getProcedureId()));
        }

        // a procedure must be selected
        if (capability == null) {
            errors.reject(ERROR_CODE_BLANK, null, ERROR_MESSAGE_BLANK);
        }

        if (form.getServiceTypeId() == null) {
            errors.reject(ERROR_CODE_VALUE_NOT_SELECTED, null, ERROR_CODE_VALUE_NOT_SELECTED);
        }

        if (form.getCoid() == null) {
            errors.reject(ERROR_CODE_VALUE_NOT_SELECTED, null, ERROR_CODE_VALUE_NOT_SELECTED);
        }

        if (form.getSubdivId() == null) {
			errors.reject(ERROR_CODE_VALUE_NOT_SELECTED, null, ERROR_CODE_VALUE_NOT_SELECTED);
		}
		
	}
}
