package org.trescal.cwms.core.jobs.job.entity.poorigin;

import lombok.NoArgsConstructor;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("TP_QUOTE")
@NoArgsConstructor
public class POOriginTPQuote extends POOrigin<TPQuotation> {

    @Override
    @Transient
    public KindName getKindName() {
        return KindName.TP_QUOATATION;
    }

    public POOriginTPQuote(TPQuotation quotation) {
        super(quotation);
        setTpQuotation(quotation);
    }
}
