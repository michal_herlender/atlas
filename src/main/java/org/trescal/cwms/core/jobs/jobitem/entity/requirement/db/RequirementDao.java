package org.trescal.cwms.core.jobs.jobitem.entity.requirement.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitem.entity.requirement.Requirement;

public interface RequirementDao extends BaseDao<Requirement, Integer> {
	
	void deleteRequirementById(String id);
	
	List<Requirement> findRequirementByJobItem(int jobitemid);
	
	List<Requirement> searchRequirement(String requirement);
}