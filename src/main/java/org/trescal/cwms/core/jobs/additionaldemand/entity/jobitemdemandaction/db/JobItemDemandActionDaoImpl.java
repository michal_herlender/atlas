package org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemandaction.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemandaction.JobItemDemandAction;

@Repository
public class JobItemDemandActionDaoImpl extends BaseDaoImpl<JobItemDemandAction, Integer>
		implements JobItemDemandActionDao {

	@Override
	protected Class<JobItemDemandAction> getEntity() {
		return JobItemDemandAction.class;
	}
}