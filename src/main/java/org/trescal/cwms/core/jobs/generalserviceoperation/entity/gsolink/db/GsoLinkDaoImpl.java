package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink;

@Repository("GsoLinkDao")
public class GsoLinkDaoImpl extends BaseDaoImpl<GsoLink, Integer> implements GsoLinkDao {
protected Class<GsoLink> getEntity() {
     return GsoLink.class;
     }
 }
