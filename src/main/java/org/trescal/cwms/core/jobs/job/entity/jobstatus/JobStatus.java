package org.trescal.cwms.core.jobs.job.entity.jobstatus;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.system.entity.basestatus.BaseStatus;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "jobstatus")
public class JobStatus extends BaseStatus
{
	private List<Job> jobs;

	private boolean readyToInvoice;
	private boolean complete;

	/**
	 * Translation for name 
	 */
	private Set<Translation> nametranslations;

	/**
	 * Translation for description 
	 */
	private Set<Translation> descriptiontranslations;
	
	@OneToMany(mappedBy = "js", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<Job> getJobs()
	{
		return this.jobs;
	}

	@Column(name = "complete", nullable = false, columnDefinition="bit")
	public boolean isComplete()
	{
		return this.complete;
	}

	@Column(name = "readytoinvoice", nullable = false, columnDefinition="bit")
	public boolean isReadyToInvoice()
	{
		return this.readyToInvoice;
	}

	public void setComplete(boolean complete)
	{
		this.complete = complete;
	}

	public void setJobs(List<Job> jobs)
	{
		this.jobs = jobs;
	}

	public void setReadyToInvoice(boolean readyToInvoice)
	{
		this.readyToInvoice = readyToInvoice;
	}
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="jobstatusnametranslation", joinColumns=@JoinColumn(name="statusid"))
	public Set<Translation> getNametranslations() {
		return nametranslations;
	}

	public void setNametranslations(Set<Translation> nametranslations) {
		this.nametranslations = nametranslations;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="jobstatusdescriptiontranslation", joinColumns=@JoinColumn(name="statusid"))
	public Set<Translation> getDescriptiontranslations() {
		return descriptiontranslations;
	}

	public void setDescriptiontranslations(Set<Translation> descriptiontranslations) {
		this.descriptiontranslations = descriptiontranslations;
	}
}
