package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db.GeneralServiceOperationService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class JIGeneralServiceOperationController extends JobItemController {

	@Autowired
	private GeneralServiceOperationService gsoService;

	@PreAuthorize("hasAuthority('JI_GENERAL_SERVICE_OPERATION')")
	@RequestMapping(value = "/jigeneralserviceoperations.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@RequestParam(value = "jobitemid", required = true) Integer jobItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		Map<String, Object> refData = super.referenceData(jobItemId, contact, subdivDto.getKey(),username);
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		List<GeneralServiceOperation> gsoliste = this.gsoService.getByJobItemId(jobItemId);
		Map<Integer, Integer> gsoDocumentIds = new HashMap<>();
		for (GeneralServiceOperation gso : gsoliste) {
			GsoDocument gsoDoc = gso.getGsoDocuments().stream()
					.filter(e -> e.getLinks().stream().anyMatch(i -> i.getJi().equals(jobItem))).findFirst()
					.orElse(null);
			if (gsoDoc != null)
				gsoDocumentIds.put(gso.getId(), gsoDoc.getId());
		}
		refData.put("gsoliste", gsoliste);
		refData.put("gsodocliste", gsoDocumentIds);
		refData.put("jobItem", jobItem);
		return new ModelAndView("trescal/core/jobs/jobitem/jigeneralserviceoperation", refData);
	}
}