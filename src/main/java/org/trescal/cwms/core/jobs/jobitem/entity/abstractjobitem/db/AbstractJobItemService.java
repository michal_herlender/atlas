package org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.AbstractJobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitempo.AbstractJobItemPO;

public interface AbstractJobItemService<ItemType extends AbstractJobItem<? extends AbstractJobItemPO<ItemType>>>
		extends BaseService<ItemType, Integer> {

	Long countItemsForJobOnBPO(Integer jobId, Integer bpoId);
}