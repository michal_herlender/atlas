package org.trescal.cwms.core.jobs.certificate.entity.certificate;

import java.util.Comparator;

/**
 * This comparator orders certs, going from oldest to most recent for any
 * particular job item.
 * 
 * @author JamieV
 */
public class CertificateComparator implements Comparator<Certificate>
{
	public int compare(Certificate cert1, Certificate cert2)
	{
		if (!cert1.getCertno().equals(cert2.getCertno()))
		{
			return cert1.getCertno().compareTo(cert2.getCertno());
		}
		else
		{
			Integer i1 = cert1.getCertid();
			Integer i2 = cert2.getCertid();

			return i1.compareTo(i2);
		}
	}
}