package org.trescal.cwms.core.jobs.calibration.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.dto.StandardsUsageAnalysisDTO;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.db.StandardsUsageAnalysisService;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysisstatus.StandardsUsageAnalysisStatus;
import org.trescal.cwms.core.jobs.calibration.form.StandardsUsageAnalysisSearchForm;
import org.trescal.cwms.core.jobs.calibration.form.validator.StandardsUsageAnalysisSearchFormValidator;
import org.trescal.cwms.core.jobs.calibration.views.StandardsUsageAnalysisXlsxView;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;

@Controller
@IntranetController
public class StandardsUsageAnalysisSearchController {

	@Autowired
	private StandardsUsageAnalysisService standardsUsageAnalysisService;
	@Autowired
	private StandardsUsageAnalysisSearchFormValidator standardsUsageAnalysisSearchFormValidator;
	@Autowired
	StandardsUsageAnalysisXlsxView standardsUsageAnalysisXlsxView;

	public static final String VIEW_NAME = "trescal/core/jobs/calibration/standardsusageanalysissearch";
	public static final String RESULTS_NAME = "trescal/core/jobs/calibration/standardsusageanalysissearchresults";
	public static final String FORM_NAME = "form";

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.setValidator(standardsUsageAnalysisSearchFormValidator);
	}

	@ModelAttribute(FORM_NAME)
	protected StandardsUsageAnalysisSearchForm formBackingObject() throws Exception {
		StandardsUsageAnalysisSearchForm form = new StandardsUsageAnalysisSearchForm();
		form.setStandardsUsageAnalysisStatus(Arrays.asList(StandardsUsageAnalysisStatus.values()));
		form.setResultsPerPage(Constants.RESULTS_PER_PAGE);
		form.setPageNo(1);
		return form;
	}

	@RequestMapping(value = "/standardsusageanalysissearch.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute(FORM_NAME) StandardsUsageAnalysisSearchForm form) {
		return new ModelAndView(VIEW_NAME);
	}

	@RequestMapping(value = "/standardsusageanalysissearch.htm", method = RequestMethod.POST, params = "!export")
	protected ModelAndView onSubmit(@Validated @ModelAttribute(FORM_NAME) StandardsUsageAnalysisSearchForm form,
			BindingResult bindingResult, Locale locale) throws Exception {

		if (bindingResult.hasErrors()) {
			return referenceData(form);
		}
		PagedResultSet<StandardsUsageAnalysisDTO> rs = this.standardsUsageAnalysisService.sreachStandardsUsageAnalysis(
				form, new PagedResultSet<StandardsUsageAnalysisDTO>(form.getResultsPerPage(), form.getPageNo()),
				locale);
		form.setRs(rs);
		return new ModelAndView(RESULTS_NAME);
	}

	@RequestMapping(value = "/standardsusageanalysissearch.htm", method = RequestMethod.POST, params = "export")
	protected ModelAndView onExport(@ModelAttribute(FORM_NAME) StandardsUsageAnalysisSearchForm form, Locale locale)
			throws Exception {

		return new ModelAndView(standardsUsageAnalysisXlsxView, "standardsUsageAnalysisDTOs",
				this.standardsUsageAnalysisService.sreachStandardsUsageAnalysis(form,
						new PagedResultSet<StandardsUsageAnalysisDTO>(form.getResultsPerPage(), form.getPageNo()),
						locale).getResults());
	}

}
