package org.trescal.cwms.core.jobs.certificate.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.certificate.dto.CustomCertWrapper;
import org.trescal.cwms.core.jobs.certificate.dto.CustomGroupCertWrapper;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.form.CertManagementForm;
import org.trescal.cwms.core.jobs.certificate.form.CertManagementValidator;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class CertManagementController
{
	@Autowired
	private AuthenticationService authServ;
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private CertificateService certServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private JobService jobServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;
	@Autowired
	private CertManagementValidator validator;
	
	@InitBinder("formBackingObject")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
	
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception
	{
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
	}
	
	@ModelAttribute("form")
	protected CertManagementForm formBackingObject(@RequestParam(value="jobid", required=false, defaultValue="0") Integer jobId) throws Exception
	{
		Job job = this.jobServ.get(jobId);
		CertManagementForm form = new CertManagementForm();
		form.setJob(job);
		form.setCalDate(new Date());
		Map<Integer, CustomCertWrapper> customCerts = new HashMap<Integer, CustomCertWrapper>();
		for (JobItem ji : job.getItems()) {
			customCerts.put(ji.getJobItemId(), new CustomCertWrapper(ji));
			// for each cert link
			for (CertLink cl : ji.getCertLinks()) {
				// set the cert directory so we can check for PDFs
				cl.getCert().setDirectory(this.certServ.findCertificate(cl.getCert().getCertid()).getDirectory());
			}
		}
		form.setCustomCerts(customCerts);
		Map<Integer, CustomGroupCertWrapper> customGroupCerts = new HashMap<Integer, CustomGroupCertWrapper>();
		for (JobItemGroup g : job.getGroups()) {
			customGroupCerts.put(g.getId(), new CustomGroupCertWrapper(g));
		}
		form.setCustomGroupCerts(customGroupCerts);
		return form;
	}
	
	@RequestMapping(value="/certmanagement.htm", method=RequestMethod.POST)
	protected RedirectView onSubmit(@ModelAttribute("form") CertManagementForm form, 
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception
	{
		Contact currentContact = this.userService.get(username).getCon();
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Contact issuedBy = (form.getIssuerId() == null) ? currentContact : this.conServ.get(form.getIssuerId());
		for (JobItem ji : form.getJob().getItems()) {
			CustomCertWrapper ccw = form.getCustomCerts().get(ji.getJobItemId());
			if (ccw.isSelected()) this.certServ.publishCustomCert(ccw, form.getCalDate(), issuedBy, allocatedSubdiv);
		}
		for (JobItemGroup g : form.getJob().getGroups()) {
			CustomGroupCertWrapper gw = form.getCustomGroupCerts().get(g.getId());
			if (gw.isSelected()) this.certServ.publishCustomGroupCert(gw, form.getCalDate(), issuedBy, allocatedSubdiv);
		}
		return new RedirectView("certmanagement.htm?jobid=" + form.getJob().getJobid());
	}
	
	@RequestMapping(value="/certmanagement.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(
			@ModelAttribute("form") CertManagementForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception
	{
		Contact currentContact = this.userService.get(username).getCon();
		boolean isSiteOrHasPermission = form.getJob().getType().equals(JobType.SITE);
		if(!isSiteOrHasPermission) isSiteOrHasPermission=this.authServ.hasRight(currentContact, Permission.CERTIFICATE_CREATE, subdivDto.getKey());		
		boolean isAdminUser =this.authServ.hasRight(currentContact, Permission.CERTIFICATE_MANAGEMENT_ADMIN, subdivDto.getKey());
		Map<String, Object> refData = new HashMap<String, Object>();
		refData.put("calTypes", this.calTypeServ.getActiveCalTypes());
		refData.put("allowCertCreation", (isSiteOrHasPermission | isAdminUser));
		refData.put("isAdmin", isAdminUser);
		refData.put("businessContacts", this.conServ.getBusinessContacts(true));
		return new ModelAndView("trescal/core/jobs/certificate/certmanagement", refData);
	}
}