package org.trescal.cwms.core.jobs.generalserviceoperation.controller;

import java.beans.PropertyEditor;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db.GeneralServiceOperationService;
import org.trescal.cwms.core.jobs.generalserviceoperation.form.GeneralServiceOperationForm;
import org.trescal.cwms.core.jobs.generalserviceoperation.form.validator.GeneralServiceOperationValidator;
import org.trescal.cwms.core.jobs.jobitem.controller.JobItemController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class ResumeGeneralServiceOperationController extends JobItemController {
	private static final Logger logger = LoggerFactory.getLogger(
			org.trescal.cwms.core.jobs.generalserviceoperation.controller.ResumeGeneralServiceOperationController.class);
	private final String FORM_NAME = "form";

	@Autowired
	private GeneralServiceOperationService gsoServ;
	@Autowired
	private GeneralServiceOperationValidator validator;

	@InitBinder(FORM_NAME)
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(Date.class, (PropertyEditor) new MyCustomDateEditor(DateTools.df, true));
		binder.registerCustomEditor(byte[].class, (PropertyEditor) new ByteArrayMultipartFileEditor());
		binder.setValidator(validator);
	}

	@ModelAttribute(FORM_NAME)
	protected GeneralServiceOperationForm formBackingObject(
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") int jobitemid) throws Exception {
		GeneralServiceOperationForm form = new GeneralServiceOperationForm();

		JobItem ji = this.jobItemService.findJobItem(jobitemid);
		if (ji == null || jobitemid == 0) {
			logger.error("Jobitem with id " + jobitemid + " could not be found");
			throw new Exception("Unable to locate requested jobitem");
		}
		form.setJi(ji);
		if (!this.jobItemService.isReadyForGeneralServiceOperation(ji)) {
			logger.debug("Jobitem with id " + jobitemid + " was not ready for general service operation");
			return form;
		}

		return form;
	}

	@RequestMapping(value = { "/resumegso.htm" }, method = { RequestMethod.GET })
	public String onSubmit(RedirectAttributes redirectAttributes,
			@RequestParam(value = "jobitemid", required = true) Integer jobItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute("username") String username,
			@ModelAttribute("form") GeneralServiceOperationForm form, BindingResult bindingResult) throws Exception {

		Contact user = this.userService.get(username).getCon();
		GeneralServiceOperation gso = this.gsoServ.getOnHoldByJobItemId(jobItemId);
		
		form.setGso(gso);
		this.validator.validate(form, bindingResult, user);		
		if (bindingResult.hasErrors()) {
			return "trescal/core/jobs/generalserviceoperation/resumegeneralserviceoperation";
		}
			
		this.gsoServ.resumeGeneralServiceOperation(gso, user);

		// re-direct to complete general service operation screen
		return "redirect:completegso.htm?gsoid=" + gso.getId() + "&jobitemid=" + form.getJi().getJobItemId();
	}

}
