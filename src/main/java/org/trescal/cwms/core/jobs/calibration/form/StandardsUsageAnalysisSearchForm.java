package org.trescal.cwms.core.jobs.calibration.form;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.jobs.calibration.dto.StandardsUsageAnalysisDTO;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysisstatus.StandardsUsageAnalysisStatus;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StandardsUsageAnalysisSearchForm {

	private Integer instId;
	private String certno;
	private Date startDate;
	private Date finishDate;
	private StandardsUsageAnalysisStatus status;
	private List<StandardsUsageAnalysisStatus> standardsUsageAnalysisStatus;

	// results data
	private PagedResultSet<StandardsUsageAnalysisDTO> rs;
	private int pageNo;
	private int resultsPerPage;

}
