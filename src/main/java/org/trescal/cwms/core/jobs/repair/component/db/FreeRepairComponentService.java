package org.trescal.cwms.core.jobs.repair.component.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.repair.component.FreeRepairComponent;

public interface FreeRepairComponentService extends BaseService<FreeRepairComponent, Integer>
{
	
}