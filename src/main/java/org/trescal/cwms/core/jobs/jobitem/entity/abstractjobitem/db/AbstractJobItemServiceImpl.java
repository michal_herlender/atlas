package org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.db;

import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.AbstractJobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitempo.AbstractJobItemPO;

public abstract class AbstractJobItemServiceImpl<ItemType extends AbstractJobItem<? extends AbstractJobItemPO<ItemType>>>
		extends BaseServiceImpl<ItemType, Integer> implements AbstractJobItemService<ItemType> {

	protected abstract AbstractJobItemDao<ItemType> getBaseDao();

	@Override
	public Long countItemsForJobOnBPO(Integer jobId, Integer bpoId) {
		return getBaseDao().countItemsForJobOnBPO(jobId, bpoId);
	}
}