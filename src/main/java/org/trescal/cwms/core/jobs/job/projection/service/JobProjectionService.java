package org.trescal.cwms.core.jobs.job.projection.service;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;

public interface JobProjectionService {
	
	void loadJobItemsIntoJobs(Collection<JobItemProjectionDTO> jobItemDtos, Collection<JobProjectionDTO> jobDtos);
	
	/**
	 * Loads additional data into the provided jobDtos based on the parameters in the JobProjectionCommand provided.
	 * Returns these same JobProjectionDTOs together with other information in JobProjectionResult. 
	 */
	JobProjectionResult loadJobProjections(List<JobProjectionDTO> jobDtos, JobProjectionCommand command);
	
	/**
	 * Performs query of JobProjectionDTOs based on the specified jobItemDtos, 
	 * and then loads additional data based on the parameters in the JobProjectionCommand provided.
	 * Returns JobProjectionDTOs together with other information in JobProjectionResult. 
	 */
	JobProjectionResult getDTOsByJobItems(Collection<JobItemProjectionDTO> jobItemDtos, JobProjectionCommand command);
	
	/**
	 * Performs query of JobProjectionDTOs based on the specified jobExpenseItemDtos, 
	 * and then loads additional data based on the parameters in the JobProjectionCommand provided.
	 * Returns JobProjectionDTOs together with other information in JobProjectionResult. 
	 */
	JobProjectionResult getDTOsByJobExpenseItems(Collection<JobItemProjectionDTO> jobExpenseItemDtos, JobProjectionCommand command);
	
	/**
	 * Performs query of JobProjectionDTOs based on the specified collection of Integer jobids, 
	 * and then loads additional data based on the parameters in the JobProjectionCommand provided.
	 * Returns JobProjectionDTOs together with other information in JobProjectionResult. 
	 */
	JobProjectionResult getDTOsByJobIds(Collection<Integer> jobIds, JobProjectionCommand command);
	
	/**
	 * Convenience method to obtain and return a single JobProjectionDTO result, this method
	 * also loads additional data based on the parameters in the JobProjectionCommand provided.  
	 */
	JobProjectionDTO getDTOByJobId(Integer jobId, JobProjectionCommand command);
}
