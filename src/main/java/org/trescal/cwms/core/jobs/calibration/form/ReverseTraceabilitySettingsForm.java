package org.trescal.cwms.core.jobs.calibration.form;

import org.springframework.util.AutoPopulatingList;
import org.trescal.cwms.core.jobs.calibration.dto.ReverseTraceabilitySettingsDTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReverseTraceabilitySettingsForm {
	
	private Integer businessCompanyId;
	private AutoPopulatingList<ReverseTraceabilitySettingsDTO> reverseTraceabilitySettingsDtos;
	private ReverseTraceabilitySettingsDTO reverseTraceabilitySettingsDto;
	
	public ReverseTraceabilitySettingsForm() {
		this.reverseTraceabilitySettingsDtos = new AutoPopulatingList<>(ReverseTraceabilitySettingsDTO.class);
	}
	
	
}
