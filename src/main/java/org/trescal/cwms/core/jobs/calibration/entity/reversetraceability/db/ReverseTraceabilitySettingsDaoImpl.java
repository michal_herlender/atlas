package org.trescal.cwms.core.jobs.calibration.entity.reversetraceability.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.jobs.calibration.dto.ReverseTraceabilitySettingsDTO;
import org.trescal.cwms.core.jobs.calibration.entity.reversetraceability.ReverseTraceabilitySettings;
import org.trescal.cwms.core.jobs.calibration.entity.reversetraceability.ReverseTraceabilitySettings_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository("ReverseTraceabilitySettingsDao")
public class ReverseTraceabilitySettingsDaoImpl extends BaseDaoImpl<ReverseTraceabilitySettings, Integer>
	implements ReverseTraceabilitySettingsDao {

	@Override
	protected Class<ReverseTraceabilitySettings> getEntity() {
		return ReverseTraceabilitySettings.class;
	}

	@Override
	public List<ReverseTraceabilitySettingsDTO> getReverseTraceabilitySettingsFormSubdivIds(List<Integer> subdivIds) {
		return getResultList(cb -> {
			CriteriaQuery<ReverseTraceabilitySettingsDTO> cq = cb.createQuery(ReverseTraceabilitySettingsDTO.class);
			Root<ReverseTraceabilitySettings> root = cq.from(ReverseTraceabilitySettings.class);
			Join<ReverseTraceabilitySettings, Subdiv> subdiv = root.join(ReverseTraceabilitySettings_.businessSubdiv);
			cq.where(subdiv.get(Subdiv_.subdivid).in(subdivIds));
			cq.distinct(true);
			cq.select(cb.construct(ReverseTraceabilitySettingsDTO.class, root.get(ReverseTraceabilitySettings_.id),
					subdiv.get(Subdiv_.subdivid), subdiv.get(Subdiv_.subname),
					root.get(ReverseTraceabilitySettings_.reverseTraceability),
					root.get(ReverseTraceabilitySettings_.startDate)));
			return cq;
		});
	}

	@Override
	public Map<Integer, LocalDate> getSubdivsAndDateFromReverseTraceability() {
		return getResultList(cb -> {
			CriteriaQuery<ReverseTraceabilitySettingsDTO> cq = cb.createQuery(ReverseTraceabilitySettingsDTO.class);
			Root<ReverseTraceabilitySettings> root = cq.from(ReverseTraceabilitySettings.class);
			Join<ReverseTraceabilitySettings, Subdiv> subdiv = root.join(ReverseTraceabilitySettings_.businessSubdiv);
			cq.where(cb.isTrue(root.get(ReverseTraceabilitySettings_.reverseTraceability)));
			cq.distinct(true);
			cq.select(cb.construct(ReverseTraceabilitySettingsDTO.class, root.get(ReverseTraceabilitySettings_.id),
				subdiv.get(Subdiv_.subdivid), subdiv.get(Subdiv_.subname),
				root.get(ReverseTraceabilitySettings_.reverseTraceability),
				root.get(ReverseTraceabilitySettings_.startDate)));
			return cq;
		}).stream().collect(Collectors.toMap(ReverseTraceabilitySettingsDTO::getSubdivid,
				ReverseTraceabilitySettingsDTO::getStartDate));
	}
}
