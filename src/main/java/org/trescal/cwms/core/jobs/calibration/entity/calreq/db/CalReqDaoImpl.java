package org.trescal.cwms.core.jobs.calibration.entity.calreq.db;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CompanyModelCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.InstrumentCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.InstrumentCalReqDto;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.InstrumentCalReq_;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.JobItemCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.ModelCalReq;

@Repository("CalReqDao")
public class CalReqDaoImpl extends BaseDaoImpl<CalReq, Integer> implements CalReqDao {
	
	private static final Logger logger = LoggerFactory.getLogger(CalReqDaoImpl.class); 
	
	@Override
	protected Class<CalReq> getEntity() {
		return CalReq.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<JobItemCalReq> findAllCalReqsImmediatelyAffectingJobItem(int jobItemId) {
		Criteria criteria = this.getSession().createCriteria(JobItemCalReq.class);
		criteria.createCriteria("jobItem").add(Restrictions.idEq(jobItemId));
		return criteria.list();
	}
	
	@Override
	public CalReq find(Integer id) {
		Criteria criteria = this.getSession().createCriteria(CalReq.class);
		criteria.add(Restrictions.idEq(id));
		criteria.setFetchMode("pointSet", FetchMode.JOIN);
		criteria.setFetchMode("pointSet.points", FetchMode.JOIN);
		criteria.setFetchMode("pointSet.points.uom", FetchMode.JOIN);
		criteria.setFetchMode("pointSet.points.relatedUom", FetchMode.JOIN);
		criteria.setFetchMode("range", FetchMode.JOIN);
		criteria.setFetchMode("range.uom", FetchMode.JOIN);
		criteria.setFetchMode("range.relatedUom", FetchMode.JOIN);
		criteria.setFetchMode("lastModifiedBy", FetchMode.JOIN);
		return (CalReq) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public CalReq findCalReqsForCompanyAndModel(int compId, int modelId) {
		Criteria crit = this.getSession().createCriteria(CompanyModelCalReq.class);
		crit.createCriteria("comp").add(Restrictions.idEq(compId));
		crit.createCriteria("model").add(Restrictions.idEq(modelId));
		crit.add(Restrictions.eq("active", true));
		crit.setFetchMode("pointSet", FetchMode.JOIN);
		crit.setFetchMode("pointSet.points", FetchMode.JOIN);
		crit.setFetchMode("pointSet.points.uom", FetchMode.JOIN);
		crit.setFetchMode("pointSet.points.relatedUom", FetchMode.JOIN);
		crit.setFetchMode("range", FetchMode.JOIN);
		crit.setFetchMode("range.uom", FetchMode.JOIN);
		crit.setFetchMode("range.relatedUom", FetchMode.JOIN);
		crit.setFetchMode("lastModifiedBy", FetchMode.JOIN);
		List<CalReq> list = crit.list();
		return (list.size() > 0) ? list.get(0) : null;
	}
	
	@Override
	public InstrumentCalReq findCalReqsForInstrument(int plantId) {
		return getFirstResult(cb -> {
			CriteriaQuery<InstrumentCalReq> cq = cb.createQuery(InstrumentCalReq.class);
			Root<InstrumentCalReq> calReqRoot = cq.from(InstrumentCalReq.class);
			Join<InstrumentCalReq, Instrument> instrumentJoin = calReqRoot.join(InstrumentCalReq_.inst);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(calReqRoot.get(InstrumentCalReq_.active), true));
			clauses.getExpressions().add(cb.equal(instrumentJoin.get(Instrument_.plantid), plantId));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public CalReq findCalReqsForJobItem(int jobItemId) {
		Criteria crit = this.getSession().createCriteria(JobItemCalReq.class);
		crit.createCriteria("jobItem").add(Restrictions.idEq(jobItemId));
		crit.add(Restrictions.eq("active", true));
		List<CalReq> list = crit.list();
		return (list.size() > 0) ? list.get(0) : null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public CalReq findCalReqsForModel(int modelId) {
		Criteria crit = this.getSession().createCriteria(ModelCalReq.class);
		crit.createCriteria("model").add(Restrictions.idEq(modelId));
		crit.add(Restrictions.eq("active", true));
		List<CalReq> list = crit.list();
		return (list.size() > 0) ? list.get(0) : null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, CalReq> findCalReqsForMultipleCompaniesAndModels(Map<Integer, CalReq> map, Map<Integer, List<Integer>> modelToJobItemMap, Map<Integer, List<Integer>> compModelMap) {
		if ((compModelMap != null) && (compModelMap.size() > 0))
			for (int compId : compModelMap.keySet()) {
				List<Integer> modelIds = compModelMap.get(compId);
				if ((modelIds != null) && (modelIds.size() > 0)) {
					Criteria crit = this.getSession().createCriteria(CompanyModelCalReq.class);
					crit.createCriteria("comp").add(Restrictions.idEq(compId));
					crit.createCriteria("model").add(Restrictions.in("modelid", modelIds));
					crit.add(Restrictions.eq("active", true));
					List<CompanyModelCalReq> calReqs = crit.list();
					for (CompanyModelCalReq calReq : calReqs)
						for (Integer jobItemId : modelToJobItemMap.get(calReq.getModel().getModelid()))
							if (!map.containsKey(jobItemId)) map.put(jobItemId, calReq);
				}
			}
		return map;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, CalReq> findCalReqsForMultipleInstruments(Map<Integer, CalReq> map, Map<Integer, Integer> instToJobItemMap) {
		Set<Integer> plantIds = instToJobItemMap.keySet();
		if (plantIds.size() > 0) {
			Criteria crit = this.getSession().createCriteria(InstrumentCalReq.class);
			crit.createCriteria("inst").add(Restrictions.in("plantid", plantIds));
			crit.add(Restrictions.eq("active", true));
			List<InstrumentCalReq> calReqs = crit.list();
			for (InstrumentCalReq calReq : calReqs)
				if (!map.containsKey(instToJobItemMap.get(calReq.getInst().getPlantid())))
					map.put(instToJobItemMap.get(calReq.getInst().getPlantid()), calReq);
		}
		return map;
	}
	
	@SuppressWarnings("unchecked")
	public Map<Integer, CalReq> findCalReqsForMultipleJobItemModels(Map<Integer, CalReq> map, Map<Integer, List<Integer>> modelToJobItemMap) {
		Set<Integer> modelIds = modelToJobItemMap.keySet();
		if (modelIds.size() > 0) {
			Criteria crit = this.getSession().createCriteria(ModelCalReq.class);
			crit.createCriteria("model").add(Restrictions.in("modelid", modelIds));
			crit.add(Restrictions.eq("active", true));
			List<ModelCalReq> calReqs = crit.list();
			for (ModelCalReq calReq : calReqs)
				for (Integer jobItemId : modelToJobItemMap.get(calReq.getModel().getModelid()))
					if (!map.containsKey(jobItemId))
						map.put(jobItemId, calReq);
		}
		return map;
	}
	
	@SuppressWarnings("unchecked")
	public Map<Integer, CalReq> findCalReqsForMultipleJobItems(List<Integer> jobItemIds) {
		Criteria crit = this.getSession().createCriteria(JobItemCalReq.class);
		crit.createCriteria("jobItem").add(Restrictions.in("jobItemId", jobItemIds));
		crit.add(Restrictions.eq("active", true));
		List<JobItemCalReq> calReqs = crit.list();
		Map<Integer, CalReq> map = new HashMap<Integer, CalReq>();
		for (JobItemCalReq calReq : calReqs)
			map.put(calReq.getJobItem().getJobItemId(), calReq);
		return map;
	}
	
	/**
	 * Batches the query in chunks of 2000 instruments
	 * We could consider putting calreq in the SearchDTO also, but calreqs only needed for certain purposes (exports)
	 * This should have the same/similar performance, as only 1 query executed per 2000 instruments
	 */
	public Map<Integer, InstrumentCalReqDto> findInstrumentCalReqsForPlantIds(List<Integer> plantids) {
		Map<Integer, InstrumentCalReqDto> results = new HashMap<>();
        int fromIndex = 0;
        while (fromIndex < plantids.size()) {
            int toIndex = fromIndex + 1000 > plantids.size() ? plantids.size() : fromIndex + 1000;
            logger.debug("Batching calReq query from "+fromIndex+" to "+toIndex);
            List<Integer> subList = plantids.subList(fromIndex, toIndex);
            
            List<InstrumentCalReqDto> batchResults = findBatchInstrumentCalReqsForPlantIds(subList);
            batchResults.stream().forEach(dto -> results.put(dto.getPlantid(), dto));
            
            fromIndex = toIndex;
		}
		return results;
	}
	
	
	/**
	 * Note, thebatchPlantIds must be provided < 2100 at a time (SQL Server limitation)
	 * @param batchPlantids
	 * @return
	 */
	private List<InstrumentCalReqDto> findBatchInstrumentCalReqsForPlantIds(List<Integer> batchPlantids) {
		
		CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<InstrumentCalReqDto> cq = cb.createQuery(InstrumentCalReqDto.class);
		Root<InstrumentCalReq> calReq = cq.from(InstrumentCalReq.class);
		Join<InstrumentCalReq, Instrument> inst = calReq.join(InstrumentCalReq_.inst);
		
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(calReq.get(InstrumentCalReq_.active), true));
		clauses.getExpressions().add(inst.get(Instrument_.plantid).in(batchPlantids));
		cq.where(clauses);
		
		Selection<InstrumentCalReqDto> selection = cb.construct(InstrumentCalReqDto.class, 
				inst.get(Instrument_.plantid),
				calReq.get(InstrumentCalReq_.privateInstructions),
				calReq.get(InstrumentCalReq_.publicInstructions));
		cq.select(selection);
				
		return this.getEntityManager().createQuery(cq).getResultList();
	}

}