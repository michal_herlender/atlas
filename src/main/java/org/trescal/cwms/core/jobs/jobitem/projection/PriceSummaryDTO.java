package org.trescal.cwms.core.jobs.jobitem.projection;

import java.math.BigDecimal;

import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
/**
 * DTO object to hold the result of a price estimate calculation (see JobItemProjectionTools),
 * intended first for use on job item.
 * Intentionally doesn't hold an ID of the cost source entity (e.g. job costing) as not yet required, 
 * but just the CostSource enum and a revision (if applicable) for informational purposes.
 */
public class PriceSummaryDTO {

	private BigDecimal price; 
	private KeyValueIntegerString currency;
	private CostSource costSource;
	private Integer revision;	// Optional

	public PriceSummaryDTO(BigDecimal price, KeyValueIntegerString currency, CostSource costSource, Integer revision) {
		super();
		if (price == null) throw new IllegalArgumentException("price must not be null");
		if (currency == null) throw new IllegalArgumentException("currency must not be null");
		if (costSource == null) throw new IllegalArgumentException("costSource must not be null");
		this.price = price;
		this.currency = currency;
		this.costSource = costSource;
		this.revision = revision;
	}
}
