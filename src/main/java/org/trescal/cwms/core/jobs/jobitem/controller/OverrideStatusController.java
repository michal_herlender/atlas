package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.form.OverrideStatusForm;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.workstatus.WorkStatus;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Controller
@IntranetController
public class OverrideStatusController {

	@Autowired
	private ItemStateService itemStateService;
	@Autowired
	private JobItemService jobItemService;

	@ModelAttribute("form")
	public OverrideStatusForm formBackingObject() {
		return new OverrideStatusForm();
	}

	@RequestMapping(value = "/loadOverrideOverlay.htm", method = RequestMethod.GET)
	public String onRequest(@RequestParam(name = "jobItemId", required = true) Integer jobItemId, Model model,
			Locale locale) {
		List<KeyValueIntegerString> allTranslatedStatuses = itemStateService.getAllTranslatedItemStatuses(WorkStatus.class, false, locale);
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		model.addAttribute("allTranslatedStatuses", allTranslatedStatuses);
		model.addAttribute("jobItem", jobItem);
		return "trescal/core/jobs/jobitem/overrideoverlay";
	}

	@RequestMapping(value = "/loadOverrideOverlay.htm", method = RequestMethod.POST)
	public ModelAndView onSubmit(@RequestParam(name = "jobItemId", required = true) Integer jobItemId,
			@ModelAttribute("form") OverrideStatusForm form) {
		jobItemService.overrideStatusOfItems(form.getOverrideStatusId(), form.getOverrideItemIds(),
				form.getOverrideRemark());
		return new ModelAndView(new RedirectView("jiactions.htm?jobitemid=" + jobItemId));
	}
}