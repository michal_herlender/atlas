package org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.jobs.contractreview.entity.costs.ContractReviewLinkedCost;
import org.trescal.cwms.core.pricing.entity.costs.base.WithJobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.entity.costs.base.WithQuotationCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;

/**
 * Intersection entity that allows linking of a
 * {@link ContractReviewCalibrationCost} to one other cost source.
 */
@Entity
@Table(name = "contractreviewlinkedcalibrationcost")
public class ContractReviewLinkedCalibrationCost extends ContractReviewLinkedCost implements WithJobCostingCalibrationCost,WithQuotationCalibrationCost {

	private ContractReviewCalibrationCost calibrationCost;
	private QuotationCalibrationCost quotationCalibrationCost;
	private JobCostingCalibrationCost jobCostingCalibrationCost;

	public ContractReviewLinkedCalibrationCost() {
		super();
	}

	public ContractReviewLinkedCalibrationCost(ContractReviewCalibrationCost calibrationCost) {
		super();
		this.calibrationCost = calibrationCost;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "calcostid", nullable = false, unique = true)
	public ContractReviewCalibrationCost getCalibrationCost() {
		return this.calibrationCost;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobcostcalcostid")
	public JobCostingCalibrationCost getJobCostingCalibrationCost() {
		return this.jobCostingCalibrationCost;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quotecalcostid")
	public QuotationCalibrationCost getQuotationCalibrationCost() {
		return this.quotationCalibrationCost;
	}

	public void setCalibrationCost(ContractReviewCalibrationCost calibrationCost) {
		this.calibrationCost = calibrationCost;
	}

	public void setJobCostingCalibrationCost(JobCostingCalibrationCost jobCostingCalibrationCost) {
		this.jobCostingCalibrationCost = jobCostingCalibrationCost;
	}

	public void setQuotationCalibrationCost(QuotationCalibrationCost quotationCalibrationCost) {
		this.quotationCalibrationCost = quotationCalibrationCost;
	}
}