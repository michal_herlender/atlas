package org.trescal.cwms.core.jobs.jobitem.dto;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class JobItemActionForEmployeeDTO {

	private Integer jobId;
	private String jobNo;
	private Integer jobItemId;
	private Integer itemNo;
	private String activityDescription;
	private String remark;
	private LocalDateTime startStamp;
	private Integer timeSpent;

	public JobItemActionForEmployeeDTO() {
	}

	public JobItemActionForEmployeeDTO(Integer jobId, String jobNo, Integer jobItemId, Integer itemNo,
			String activityDescription, String remark, Date startStamp, Integer timeSpent) {
		this.jobId = jobId;
		this.jobNo = jobNo;
		this.jobItemId = jobItemId;
		this.itemNo = itemNo;
		this.activityDescription = activityDescription;
		this.remark = remark;
		this.startStamp = startStamp.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		this.timeSpent = timeSpent;
	}

	public Integer getJobId() {
		return jobId;
	}

	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}

	public String getJobNo() {
		return jobNo;
	}

	public void setJobNo(String jobNo) {
		this.jobNo = jobNo;
	}

	public Integer getJobItemId() {
		return jobItemId;
	}

	public void setJobItemId(Integer jobItemId) {
		this.jobItemId = jobItemId;
	}

	public Integer getItemNo() {
		return itemNo;
	}

	public void setItemNo(Integer itemNo) {
		this.itemNo = itemNo;
	}

	public String getActivityDescription() {
		return activityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public LocalDateTime getStartStamp() {
		return startStamp;
	}

	public void setStartStamp(LocalDateTime startStamp) {
		this.startStamp = startStamp;
	}

	public Integer getTimeSpent() {
		return timeSpent;
	}

	public void setTimeSpent(Integer timeSpent) {
		this.timeSpent = timeSpent;
	}

	public Duration getDuration() {
		return Duration.ofMinutes(timeSpent);
	}
}