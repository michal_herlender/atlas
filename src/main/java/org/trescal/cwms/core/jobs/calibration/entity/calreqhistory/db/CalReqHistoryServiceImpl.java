package org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.calibration.dto.CalReqHistoryDTO;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.InstrumentCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.JobItemCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.*;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("CalReqHistoryService")
public class CalReqHistoryServiceImpl implements CalReqHistoryService
{
	@Autowired
	private CalReqHistoryDao calReqHistoryDao;
	@Autowired
	private CalReqService calReqServ;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private JobItemService jiServ;

	public void deleteCalReqHistory(CalReqHistory calreqhistory)
	{
		this.calReqHistoryDao.remove(calreqhistory);
	}

	public void deleteCalReqHistoryImmediatelyAffectingJobItem(int jobItemId)
	{
		for (CalReqHistory crh : this.findFallbacksForJobItem(jobItemId))
		{
			for (CalReqEdit edit : this.findAllEditsForCalReq(crh.getOldCalReq()))
			{
				this.deleteCalReqHistory(edit);
			}

			this.deleteCalReqHistory(crh);
		}

		for (CalReqHistory crh : this.findOverridesForJobItem(jobItemId))
		{
			for (CalReqEdit edit : this.findAllEditsForCalReq(crh.getNewCalReq()))
			{
				this.deleteCalReqHistory(edit);
			}

			this.deleteCalReqHistory(crh);
		}
	}

	private ResultWrapper fetchCompleteCalReqHistory(Integer jobItemId, Integer plantId) {
		List<CalReqHistoryDTO> dtos = new ArrayList<>();

		List<CalReqHistory> fallsAndOvers = new ArrayList<>();
		CalReq current = null;

		// find every occasion that there has been a CalReq override or fallback
		// that effected the jobitem/instrument, and combine these into a single
		// list ordered by date
		if (jobItemId != null) {
			JobItem ji = this.jiServ.findJobItem(jobItemId);
			fallsAndOvers.addAll(this.findOverridesForInstrument(ji.getInst().getPlantid()));
			fallsAndOvers.addAll(this.findFallbacksForInstrument(ji.getInst().getPlantid()));
			fallsAndOvers.addAll(this.findOverridesForJobItem(jobItemId));
			fallsAndOvers.addAll(this.findFallbacksForJobItem(jobItemId));
			current = this.calReqServ.findCalReqsForJobItem(ji);
		} else if (plantId != null) {
			fallsAndOvers.addAll(this.findOverridesForInstrument(plantId));
			fallsAndOvers.addAll(this.findFallbacksForInstrument(plantId));
			current = this.calReqServ.findCalReqsForInstrument(this.instServ.get(plantId));
		}

		// combine these into a single list ordered by date
		fallsAndOvers.sort(new CalReqHistoryComparator());

		// if there are current CalReqs
		if (current != null) {
			// first check if these have been edited since the last
			// override/fallback
			Date from = (fallsAndOvers.size() < 1) ? null : fallsAndOvers.get(fallsAndOvers.size() - 1).getChangeOn();
			List<CalReqHistory> edits = this.findEditsForCalReqBetweenDates(current, from, new Date(), false);

			CalReqHistory firstEdit = null;
			// add all edits to the top of the history list
			for (CalReqHistory edit : edits)
			{
				dtos.add(new CalReqHistoryDTO(edit));
				firstEdit = edit;
			}

			// if the current reqs are CompanyModel ones
			if (!(current instanceof JobItemCalReq)
					&& !(current instanceof InstrumentCalReq))
			{
				CalReq oldReq = (firstEdit == null) ? current : firstEdit.getOldCalReq();
				// check if there have been any overrides that won't appear in
				// the main list of overrides or falls because they are not
				// specific to just this one jobitem/instrument
				if (oldReq != null)
				{
					Date to = (firstEdit == null) ? new Date() : firstEdit.getChangeOn();
					CalReqOverride over = (CalReqOverride) this.findHistoryForCalReqBetweenDates(oldReq, from, to, CalReqOverride.class, false);

					if (over != null)
					{
						dtos.add(new CalReqHistoryDTO(over));
					}
				}
			}
		}

		// then iterate over all overrides/fallbacks, at each stage checking if
		// the CalReqs were edited in between
		for (int i = fallsAndOvers.size() - 1; i >= 0; i--) {
			CalReqHistory h = fallsAndOvers.get(i);

			Date lastDateInList;
			// get the last date in the list so we can find edits in between
			lastDateInList = (dtos.size() > 0) ? dtos.get(dtos.size() - 1).getTime() : null;

			// if the action is a fallback but the new cal req is not active,
			// then the requirements fell back to have probably since been
			// de-activated!
			if ((h instanceof CalReqFallback)
				&& (h.getNewCalReq() != null)
				&& !h.getNewCalReq().isActive()
				&& !h.getNewCalReq().getClassKey().equalsIgnoreCase("instrument")
				&& !h.getNewCalReq().getClassKey().equalsIgnoreCase("jobitem")
				&& ((lastDateInList == null) || h.getNewCalReq().getDeactivatedOn().before(lastDateInList))) {
				List<CalReqHistory> edits = new ArrayList<>();

				// check for edits of the calreq fallen back to
				if ((lastDateInList != null) && (h.getOldCalReq() != null)) {
					edits = this.findEditsForCalReqBetweenDates(h.getNewCalReq(), h.getChangeOn(), lastDateInList, true);
				}

				// add all edits into the history list before anything else
				for (int j = edits.size(); j > 0; j--) {
					CalReqHistory edit = edits.get(j - 1);
					dtos.add(new CalReqHistoryDTO(edit));
				}

				// if there weren't any edits, add de-activation into history
				if (edits.size() < 1)
				{
					dtos.add(new CalReqHistoryDTO(h.getNewCalReq()));
				}
			}

			// if the CalReq changed from is not null (i.e. if there were
			// requirements on the jobitem/instrument before this
			// override/fallback)
			if (h.getOldCalReq() != null)
			{
				Date from = (i == 0) ? null : fallsAndOvers.get(i - 1).getChangeOn();

				// find all edits of this CalReq for the whole time it was
				// relevant to the jobitem/instrument
				List<CalReqHistory> edits = this.findEditsForCalReqBetweenDates(h.getOldCalReq(), from, h.getChangeOn(), false);

				// add the override/fallback into the history list
				dtos.add(new CalReqHistoryDTO(h));

				// add all edits into the history list after the
				// override/fallback
				for (CalReqHistory edit : edits)
				{
					dtos.add(new CalReqHistoryDTO(edit));
				}
			}
			// otherwise
			else
			{
				// just add this override
				dtos.add(new CalReqHistoryDTO(h));
			}
		}

		return new ResultWrapper(true, null, dtos, null);
	}

	public ResultWrapper fetchCompleteCalReqHistoryForInstrument(int plantId)
	{
		return this.fetchCompleteCalReqHistory(null, plantId);
	}

	public ResultWrapper fetchCompleteCalReqHistoryForJobItem(int jobItemId)
	{
		return this.fetchCompleteCalReqHistory(jobItemId, null);
	}

	public List<CalReqEdit> findAllEditsForCalReq(CalReq cr)
	{
		return this.calReqHistoryDao.findAllEditsForCalReq(cr);
	}

	public CalReqHistory findCalReqHistory(int id)
	{
		return this.calReqHistoryDao.find(id);
	}

	private List<CalReqHistory> findEditsForCalReqBetweenDates(CalReq cr, Date from, Date to, boolean future) {
		List<CalReqHistory> edits = new ArrayList<>();
		CalReqEdit edit = (CalReqEdit) this.findHistoryForCalReqBetweenDates(cr, from, to, CalReqEdit.class, future);
		CalReqHistory overOrFall = this.findOverrideOrFallbackForCalReqBetweenDates(cr, from, to);
		while ((edit != null) || (overOrFall != null)) {
			CalReqHistory h = (edit != null) ? edit : overOrFall;
			edits.add(h);

			CalReq next;
			if (future) {
				next = h.getNewCalReq();
			} else {
				next = h.getOldCalReq();
			}

			if (next == null)
			{
				edit = null;
				overOrFall = null;
			}
			else
			{
				edit = (CalReqEdit) this.findHistoryForCalReqBetweenDates(next, from, to, CalReqEdit.class, future);
				overOrFall = this.findOverrideOrFallbackForCalReqBetweenDates(next, from, to);
			}
		}
		return edits;
	}

	public List<CalReqFallback> findFallbacksForInstrument(int plantId)
	{
		return this.calReqHistoryDao.findFallbacksForInstrument(plantId);
	}

	public List<CalReqFallback> findFallbacksForJobItem(int jobItemId)
	{
		return this.calReqHistoryDao.findFallbacksForJobItem(jobItemId);
	}

	private CalReqHistory findHistoryForCalReqBetweenDates(CalReq cr, Date from, Date to, Class<? extends CalReqHistory> clazz, boolean future)
	{
		return this.calReqHistoryDao.findHistoryForCalReqBetweenDates(cr, from, to, clazz, future);
	}

	private CalReqHistory findOverrideOrFallbackForCalReqBetweenDates(CalReq cr, Date from, Date to)
	{
		return this.calReqHistoryDao.findOverrideOrFallbackForCalReqBetweenDates(cr, from, to);
	}

	public List<CalReqOverride> findOverridesForInstrument(int plantId)
	{
		return this.calReqHistoryDao.findOverridesForInstrument(plantId);
	}

	public List<CalReqOverride> findOverridesForJobItem(int jobItemId)
	{
		return this.calReqHistoryDao.findOverridesForJobItem(jobItemId);
	}

	public List<CalReqHistory> getAllCalReqHistorys()
	{
		return this.calReqHistoryDao.findAll();
	}

	public void insertCalReqHistory(CalReqHistory CalReqHistory)
	{
		this.calReqHistoryDao.persist(CalReqHistory);
	}

	public void updateCalReqHistory(CalReqHistory CalReqHistory) {
		this.calReqHistoryDao.merge(CalReqHistory);
	}
}