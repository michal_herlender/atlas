package org.trescal.cwms.core.jobs.job.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrument.dto.AddItemsValidDTO;
import org.trescal.cwms.core.instrument.dto.QuickAddItemsValidDTO;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.form.AddQuickItemsDTOValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.rest.common.component.RestErrorConverter;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@JsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class QuickAddInstrumentToJobController {

	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private JobService jService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private RestErrorConverter errorConverter;
	@Autowired
	private AddQuickItemsDTOValidator validator;

	@RequestMapping(value = "/quickAddItemsValid.json", method = RequestMethod.GET, produces = Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
	@ResponseBody
	public RestResultDTO<AddItemsValidDTO> quickAddItemsValid(@RequestParam(name = "plantid", required = true) int plantid,
			@RequestParam(name = "coid", required = true) int coid,
			@RequestParam(name = "jobid", required = true) int jobid, Model model, Locale locale,
			HttpServletResponse response, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			BindingResult bindingResult) {

		RestResultDTO<AddItemsValidDTO> dto = new RestResultDTO<>();
		AddItemsValidDTO results = new AddItemsValidDTO();
		results.setInstrument(new QuickAddItemsValidDTO());
		results.getInstrument().setPlantid(plantid);
		results.setCoid(coid);
		results.setJobId(jobid);
		validator.validate(results, bindingResult);

		if (bindingResult.hasErrors())
			return errorConverter.createResultDTO(bindingResult, locale);
		else {

			Job job = jService.get(jobid);

			if (job.getBookedInAddr() != null) {
				results.setDefAddrId(job.getBookedInAddr().getAddrid());
			} else
				results.setDefAddrId(0);

			results.setDefaultTurnAround(job.getDefaultTurn());

			results.setInstrument(instrumentService.isQuickAddItemsValid(subdivDto.getKey(), plantid, coid, jobid, locale));

			results.setServiceTypes(serviceTypeService.getDTOListWithShortNames(null, job.getType(), locale, false));

			if (job.getType().equals(JobType.SITE)) {
				results.setBookingInAddresses(this.addressService.getAllActiveSubdivAddressesKeyValue(
						job.getCon().getSub().getSubdivid(), AddressType.DELIVERY, false));
			} else {
				results.setBookingInAddresses(this.addressService.getActiveUserRoleAddressesKeyValue(username, false));
			}

			dto.addResult(results);
			dto.setSuccess(true);
			return dto;
		}
	}
}
