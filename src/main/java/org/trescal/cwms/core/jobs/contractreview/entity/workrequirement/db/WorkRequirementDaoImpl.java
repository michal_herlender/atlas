package org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;

@Repository("WorkRequirementDao")
public class WorkRequirementDaoImpl extends BaseDaoImpl<WorkRequirement, Integer> implements WorkRequirementDao {

	@Override
	protected Class<WorkRequirement> getEntity() {
		return WorkRequirement.class;
	}
}