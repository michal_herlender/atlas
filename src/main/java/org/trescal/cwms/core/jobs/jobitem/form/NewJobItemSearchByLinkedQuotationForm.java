package org.trescal.cwms.core.jobs.jobitem.form;

import lombok.Data;

@Data
public class NewJobItemSearchByLinkedQuotationForm implements NewJobItemBaseSearchForm {

    private Integer jobId;
    private Integer companyId;
    private Integer jobQuoteLinkId;
    private String action;


    private Integer resultsPerPage;
    private Integer instrumentsPageNo;
    private Integer companyModelsPageNo;
    private Integer allModelsPageNo;
}
