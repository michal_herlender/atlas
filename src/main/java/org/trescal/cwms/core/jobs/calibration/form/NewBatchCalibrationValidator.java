package org.trescal.cwms.core.jobs.calibration.form;

import org.jasypt.digest.StandardStringDigester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationToBatch;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.procedure.entity.defaultstandard.db.DefaultStandardService;
import org.trescal.cwms.core.tools.DigestConfigCreator;
import org.trescal.cwms.core.tools.PasswordTools;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class NewBatchCalibrationValidator extends AbstractBeanValidator {

	@Autowired
	private CalibrationProcessService calProServ;
	@Autowired
	private DigestConfigCreator configCreator;
	@Autowired
	private DefaultStandardService defStdServ;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(NewBatchCalibrationForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		NewBatchCalibrationForm form = (NewBatchCalibrationForm) target;
		super.validate(form, errors);

		CalibrationProcess process;
		if (form.getExistingBatch() != null) {
			process = form.getExistingBatch().getProcess();
		} else {
			process = this.calProServ.get(form.getCalProcessId());
		}

		/*
		 * BUSINESS VALIDATION
		 */
		// if auto printing, then password is required at this stage
		if (form.isAutoPrinting()) {
			// The user is trying to use an existing password
			if (form.getOwner().getEncryptedPassword() != null) {
				Contact con = form.getOwner();

				StandardStringDigester digester = new StandardStringDigester();
				digester.setConfig(this.configCreator.createDigesterConfig());

				if (!digester.matches(form.getPassword(), con.getEncryptedPassword())) {
					errors.rejectValue("password", "error.calibration.validator.wrongsignpassword");
				}
			}
			// the user is setting a new password
			else {
				if (!form.getPassword().equals(form.getConfirmPassword())) {
					errors.rejectValue("confirmPassword", "error.calibration.validator.confirmpassword");
				} else {
					if (PasswordTools.getPasswordStrength(form.getPassword()) < 3) {
						errors.rejectValue("password", "error.calibration.validator.passwordstrength");
					}
				}
			}
		}

		// if this is a new batch AND there is no date selected
		if ((form.getCalDate() == null) && (form.getExistingBatch() == null)) {
			errors.rejectValue("calDate", "error.calibration.validator.calibrationdateforbatchcalibration");
		}

		// Item Calibrations must have all necessary details
		for (CalibrationToBatch ctb : form.getItemCals()) {
			if (ctb.isToBatch()) {
				if (process.getApplicationComponent()) {
					if ((ctb.getJi().getCalType() == null) || (ctb.getProcId() == null)) {
						errors.rejectValue("itemCals", null, "Item " + ctb.getJi().getItemNo()
								+ " must have a Calibration Type and Procedure selected.");
					}
				} else {
					if ((ctb.getJi().getCalType() == null) || (ctb.getProcId() == null)) {
						errors.rejectValue("itemCals", null, "Item " + ctb.getJi().getItemNo()
								+ " must have both a Calibration Type and Procedure selected.");
					}
				}
			}
		}

		for (CalibrationToBatch ctb : form.getItemCals()) {
			//set the value to 0 to indicate no work instruction selection
			ctb.setWorkInstructionId(0);
			// if the cal is being batched and it has a proc and work
			// instruction selected
			if (ctb.isToBatch() && (ctb.getProcId() != null) && (ctb.getWorkInstructionId() != null)) {
				for (Instrument inst : ctb.getStandards()) {
					// if standards are not allowed to be out of calibration
					if (inst.isOutOfCalibration() && !form.isAllowUncalibratedStds()) {
						errors.rejectValue("itemCals", null,
							"Item " + ctb.getJi().getItemNo() + " cannot use standard with barcode "
								+ inst.getPlantid() + " because it is out of calibration.");
					}

					// if standard has surpassed cal interval
					if (inst.getCalExpiresAfterNumberOfUses() && (inst.getUsesSinceLastCalibration() >= inst
						.getPermittedNumberOfUses())) {
						errors.rejectValue("itemCals", null,
							"Item " + ctb.getJi().getItemNo() + " cannot use standard with barcode "
								+ inst.getPlantid() + " because it has been used for "
								+ inst.getUsesSinceLastCalibration()
								+ " calibrations since it was last calibrated.");
					}
				}

				// check all enforced stds are selected
				if (!this.defStdServ.enforcedAreCheckedForProcAndWI(ctb.getProcId(), ctb.getWorkInstructionId(),
						ctb.getStandardIds())) {
					errors.rejectValue("itemCals", null, "Item " + ctb.getJi().getItemNo()
							+ " has enforced standards for its procedure that have not been selected.");
				}
			}
		}

		if (!form.isInfoAcknowledged()) {
			errors.rejectValue("infoAcknowledged", "error.calibration.validator.readinstructionforthisjobconfirm");
		}
	}
}