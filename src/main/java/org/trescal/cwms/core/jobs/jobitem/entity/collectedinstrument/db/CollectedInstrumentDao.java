package org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitem.dto.CollectedInstrumentDto;
import org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.CollectedInstrument;

import java.util.List;

public interface CollectedInstrumentDao extends BaseDao<CollectedInstrument, Integer> {

    List<CollectedInstrument> getCollectedInstrumentsForCompany(int coid);

    List<CollectedInstrumentDto> getCollectedInstrumentsDtoForCompany(Integer coid);
}