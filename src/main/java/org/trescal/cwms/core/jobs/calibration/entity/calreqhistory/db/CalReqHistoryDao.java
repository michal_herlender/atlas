package org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.db;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqEdit;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqFallback;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqHistory;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqOverride;

public interface CalReqHistoryDao extends BaseDao<CalReqHistory, Integer> {
	
	List<CalReqEdit> findAllEditsForCalReq(CalReq cr);
	
	List<CalReqFallback> findFallbacksForInstrument(int plantId);
	
	List<CalReqFallback> findFallbacksForJobItem(int jobItemId);
	
	CalReqHistory findHistoryForCalReqBetweenDates(CalReq cr, Date from, Date to, Class<? extends CalReqHistory> clazz, boolean future);
	
	CalReqHistory findOverrideOrFallbackForCalReqBetweenDates(CalReq cr, Date from, Date to);
	
	List<CalReqOverride> findOverridesForInstrument(int plantId);
	
	List<CalReqOverride> findOverridesForJobItem(int jobItemId);
}