package org.trescal.cwms.core.jobs.job.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.db.DeliveryItemService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemaccessory.DeliveryItemAccessory;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.Accessory;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.GroupAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.db.AccessoryService;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.db.GroupAccessoryService;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.db.ItemAccessoryService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.db.JobItemGroupService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@IntranetController
public class AccessoryController {

	public static final String VIEW_NAME = "trescal/core/jobs/job/showgroupaccessoryfreetext";
	public static final String JOB_SHOWITEMACCESSORYFREETEXT = "trescal/core/jobs/job/showitemaccessoryfreetext";
	public static final String JOB_ITEM = "jobItem";
	public static final String MODEL_NAME = "group";
	@Autowired
	private AccessoryService accessoryService;
	@Autowired
	private DeliveryItemService deliveryItemService;
	@Autowired
	private GroupAccessoryService groupAccessoryService;
	@Autowired
	private ItemAccessoryService itemAccessoryService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private JobItemGroupService groupService;
	@Autowired
	private MessageSource messageSource;

	@GetMapping(value = "/additemaccessory.htm")
	public ModelAndView addItemAccessory(@RequestParam(name = "accessoryId") Integer accessoryId,
										 @RequestParam(name = "jobItemId") Integer jobItemId,
										 @RequestParam(name = "quantity", required = false, defaultValue = "1") Integer quantity) {
		Accessory accessory = accessoryService.get(accessoryId);
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		ItemAccessory itemAccessory = ItemAccessory.builder()
				.accessory(accessory)
				.ji(jobItem)
				.quantity(quantity)
				.build();
		jobItem.getAccessories().add(itemAccessory);
		jobItemService.merge(jobItem);
		return new ModelAndView("trescal/core/jobs/job/newitemaccessory", "itemAccessory", itemAccessory);
	}

	@GetMapping(value = "/addgroupaccessory.htm")
	public ModelAndView addGroupAccessory(@RequestParam(name = "accessoryId") Integer accessoryId,
										  @RequestParam(name = "groupId") Integer groupId,
										  @RequestParam(name = "quantity", required = false, defaultValue = "1") Integer quantity) {
		Accessory accessory = accessoryService.get(accessoryId);
		JobItemGroup group = groupService.findJobItemGroup(groupId);
		GroupAccessory groupAccessory = new GroupAccessory();
		groupAccessory.setAccessory(accessory);
		groupAccessory.setGroup(group);
		groupAccessory.setQuantity(quantity);
		group.getAccessories().add(groupAccessory);
		groupService.updateJobItemGroup(group);
		return new ModelAndView("trescal/core/jobs/job/newgroupaccessory", "groupAccessory", groupAccessory);
	}

	@GetMapping(value = "/showitemaccessories.htm")
	public ModelAndView showItemAccessories(@RequestParam(name = "jobItemId") Integer jobItemId) {
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		return new ModelAndView("trescal/core/jobs/job/showitemaccessories", JOB_ITEM, jobItem);
	}

	@GetMapping(value = "/showitemaccessoryfreetext.htm")
	public ModelAndView showItemAccessoryFreeText(@RequestParam(name = "jobItemId") Integer jobItemId) {
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		return new ModelAndView(JOB_SHOWITEMACCESSORYFREETEXT, JOB_ITEM, jobItem);
	}

	@GetMapping(value = "/showgroupaccessories.htm")
	public ModelAndView showGroupAccessories(@RequestParam(name = "groupId") Integer groupId) {
		JobItemGroup group = groupService.findJobItemGroup(groupId);
		return new ModelAndView("trescal/core/jobs/job/showgroupaccessories", MODEL_NAME, group);
	}

	@GetMapping(value = "/showgroupaccessoryfreetext.htm")
	public ModelAndView showGroupAccessoryFreeText(@RequestParam(name = "groupId") Integer groupId) {
		JobItemGroup group = groupService.findJobItemGroup(groupId);
		return new ModelAndView(VIEW_NAME, MODEL_NAME, group);
	}

	@GetMapping(value = "/updateitemaccessoryfreetext.htm")
	public ModelAndView updateItemAccessoryFreeText(@RequestParam(name = "jobItemId") Integer jobItemId,
													@RequestParam(name = "freeText") String freeText) {
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		jobItem.setAccessoryFreeText(freeText);
		jobItemService.updateJobItem(jobItem);
		return new ModelAndView(JOB_SHOWITEMACCESSORYFREETEXT, JOB_ITEM, jobItem);
	}

	@GetMapping(value = "/updategroupaccessoryfreetext.htm")
	public ModelAndView updateGroupAccessoryFreeText(@RequestParam(name = "groupId") Integer groupId,
													 @RequestParam(name = "freeText") String freeText) {
		JobItemGroup group = groupService.findJobItemGroup(groupId);
		group.setAccessoryFreeText(freeText);
		groupService.updateJobItemGroup(group);
		return new ModelAndView(VIEW_NAME, MODEL_NAME, group);
	}

	@GetMapping(value = "/removeitemaccessoryfreetext.htm")
	public ModelAndView removeItemAccessoryFreeText(@RequestParam(name = "jobItemId") Integer jobItemId) {
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		jobItem.setAccessoryFreeText(null);
		jobItemService.updateJobItem(jobItem);
		return new ModelAndView(JOB_SHOWITEMACCESSORYFREETEXT, JOB_ITEM, jobItem);
	}

	@GetMapping(value = "/removegroupaccessoryfreetext.htm")
	public ModelAndView removeGroupAccessoryFreeText(@RequestParam(name = "groupId") Integer groupId) {
		JobItemGroup group = groupService.findJobItemGroup(groupId);
		group.setAccessoryFreeText(null);
		groupService.updateJobItemGroup(group);
		return new ModelAndView(VIEW_NAME, MODEL_NAME, group);
	}

	@GetMapping(value = "/saveitemaccessory.json")
	@ResponseBody
	public void saveItemAccessory(@RequestParam(name = "accessoryId") Integer itemAccessoryId,
								  @RequestParam(name = "quantity", required = false, defaultValue = "1") Integer quantity) {
		ItemAccessory itemAccessory = itemAccessoryService.get(itemAccessoryId);
		itemAccessory.setQuantity(quantity);
		itemAccessoryService.update(itemAccessory);
	}

	@GetMapping(value = "/savegroupaccessory.json")
	@ResponseBody
	public void saveGroupAccessory(@RequestParam(name = "accessoryId") Integer groupAccessoryId,
								   @RequestParam(name = "quantity", required = false, defaultValue = "1") Integer quantity) {
		GroupAccessory groupAccessory = groupAccessoryService.get(groupAccessoryId);
		groupAccessory.setQuantity(quantity);
		groupAccessoryService.update(groupAccessory);
	}

	@GetMapping(value = "/removeitemaccessory.txt")
	@ResponseBody
	public String removeItemAccessory(@RequestParam(name = "itemAccessoryId") Integer itemAccessoryId) {
		ItemAccessory itemAccessory = itemAccessoryService.get(itemAccessoryId);
		itemAccessory.getJi().getAccessories().remove(itemAccessory);
		itemAccessoryService.delete(itemAccessory);
		return itemAccessory.getJi().getAccessories().isEmpty()
				? messageSource.getMessage("showitemaccessories.noaccessories", null, LocaleContextHolder.getLocale())
				: "";
	}

	@GetMapping(value = "/removegroupaccessory.txt")
	@ResponseBody
	public String removeGroupAccessory(@RequestParam(name = "groupId") Integer groupId,
									   @RequestParam(name = "groupAccessoryId") Integer groupAccessoryId) {
		JobItemGroup group = groupService.findJobItemGroup(groupId);
		GroupAccessory groupAccessory = group.getAccessories().stream()
				.filter(a -> a.getId().equals(groupAccessoryId))
				.findFirst()
				.orElse(null);
		group.getAccessories().remove(groupAccessory);
		groupService.updateJobItemGroup(group);
		return group.getAccessories().isEmpty()
				? messageSource.getMessage("showitemaccessories.noaccessories", null, LocaleContextHolder.getLocale()) : "";
	}

	@GetMapping(value = "/createaccessoryoverlay.htm")
	public ModelAndView createAccessoryOverlay(
			@RequestParam(name = "deliveryItemId") Integer deliveryItemId,
			@RequestParam(name = "jobItemId") Integer jobItemId) {
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		Map<String, Object> model = new HashMap<>();
		model.put(JOB_ITEM, jobItem);
		model.put("deliveryItemId", deliveryItemId);
		return new ModelAndView("trescal/core/jobs/job/accessoryoverlay", model);
	}

	@GetMapping(value = "/updatedeliveryitemaccessories.htm")
	public ModelAndView updateDeliveryItemAccessories(
			@RequestParam(name = "itemAccessoriesIds[]", required = false, defaultValue = "") List<Integer> itemAccessoriesIds,
			@RequestParam(name = "deliveryItemId") Integer deliveryItemId) {
		DeliveryItem deliveryItem = this.deliveryItemService.findDeliveryItem(deliveryItemId);
		if (deliveryItem != null) {
			// first remove any existing delivery item accessories
			deliveryItem.getAccessories().clear();
			deliveryItem = deliveryItemService.merge(deliveryItem);
			// now add each accessory selected
			for (Integer itemAccessoryId : itemAccessoriesIds) {
				// find first accessory
				ItemAccessory itemAccessory = itemAccessoryService.get(itemAccessoryId);
				// create new delivery item accessory
				DeliveryItemAccessory deliveryItemAccessory = new DeliveryItemAccessory();
				deliveryItemAccessory.setAccessory(itemAccessory.getAccessory());
				deliveryItemAccessory.setQty(itemAccessory.getQuantity());
				deliveryItemAccessory.setDeliveryItem(deliveryItem);
				deliveryItem.getAccessories().add(deliveryItemAccessory);
			}
			deliveryItem = deliveryItemService.merge(deliveryItem);
		}
		return new ModelAndView("trescal/core/jobs/job/deliveryItemAccessories", "deliveryItem", deliveryItem);
	}

	@GetMapping(value = "/updategroupfields.json")
	@ResponseBody
	public void updateGroupFields(@RequestParam(name = "groupId") Integer groupId,
								  @RequestParam(name = "calibrationGroup") Boolean calibrationGroup,
								  @RequestParam(name = "deliveryGroup") Boolean deliveryGroup) {
		JobItemGroup group = groupService.findJobItemGroup(groupId);
		group.setCalibrationGroup(calibrationGroup);
		group.setDeliveryGroup(deliveryGroup);
		groupService.updateJobItemGroup(group);
	}
}