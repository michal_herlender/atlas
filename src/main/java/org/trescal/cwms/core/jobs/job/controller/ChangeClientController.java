package org.trescal.cwms.core.jobs.job.controller;

import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.form.ChangeClientForm;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.db.OnBehalfItemService;

@Controller
@IntranetController
public class ChangeClientController {
	@Autowired
	private AddressService addServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private DeliveryService delServ;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private JobService jobServ;
	@Autowired
	private OnBehalfItemService onBehalfServ;
	@Autowired
	private JobItemService jiServ;

	@ModelAttribute("fbo")
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		ChangeClientForm fbo = new ChangeClientForm();

		int jobid = ServletRequestUtils.getIntParameter(request, "jobid", 0);
		fbo.setJob(this.jobServ.get(jobid));

		return fbo;
	}

	protected void onBind(HttpServletRequest request, ChangeClientForm fbo) {
		// Bind the checkbox fields properly as a fallback for when the
		// parameter value isn't passed.
		fbo.setIncDeliveries(Boolean.valueOf(ServletRequestUtils.getBooleanParameter(request, "incDeliveries", false)));
		fbo.setIncInstruments(
				Boolean.valueOf(ServletRequestUtils.getBooleanParameter(request, "incInstruments", false)));
	}

	@RequestMapping(value = "/changeclient.htm", method = RequestMethod.GET)
	protected String formView() {
		return "trescal/core/jobs/job/changeclient";
	}

	@RequestMapping(value = "/changeclient.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest request, @Valid @ModelAttribute("fbo") ChangeClientForm fbo,
			BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors())
			return new ModelAndView(formView());
		else {
			onBind(request, fbo);
			Job tempJob = fbo.getJob();

			// Re-get the same Job via JobService so that all fields are loaded
			// properly
			Job j = this.jobServ.get(tempJob.getJobid());

			int newContactId = ServletRequestUtils.getIntParameter(request, "personid", 0);
			int newReturnToId = ServletRequestUtils.getIntParameter(request, "addrid", 0);
			Contact newCon = this.conServ.get(newContactId);
			Address newAdd = this.addServ.get(newReturnToId);

			// change the booking in address in OnSite job case
			if (j.getType().equals(JobType.SITE)) {
				j.setBookedInAddr(newAdd);
			}

			j.setCon(newCon);
			j.setReturnTo(newAdd);

			Set<JobDelivery> dels = j.getDeliveries();
			Set<JobItem> items = j.getItems();

			if (fbo.isIncDeliveries() && (dels.size() > 0)) {
				Iterator<JobDelivery> delIt = dels.iterator();
				while (delIt.hasNext()) {
					Delivery tempDel = delIt.next();

					// only switchover client dels, not TP ones!
					if (tempDel.isClientDelivery()) {
						tempDel.setContact(newCon);
						tempDel.setAddress(newAdd);
						this.delServ.updateDelivery(tempDel);
					}
				}
			}

			if (items.size() > 0) {
				if (fbo.isIncInstruments()) {
					Iterator<JobItem> itemIt = items.iterator();
					while (itemIt.hasNext()) {
						JobItem tempItem = itemIt.next();
						Instrument tempInst = tempItem.getInst();
						tempInst.setCon(newCon);
						tempInst.setAdd(newAdd);
						tempInst.setComp(newCon.getSub().getComp());
						this.instServ.update(tempInst);
					}
				} else {
					for (JobItem jobItem : items) {
						Contact contact = jobItem.getInst().getCon();
						Address address = jobItem.getInst().getAdd();
						Company company = contact.getSub().getComp();
						OnBehalfItem oldOnBehalf = jobItem.getOnBehalf();
						// Can be removed, if we set orphanRemoval on JobItem.getOnBehalf
						if (oldOnBehalf != null && company.equals(oldOnBehalf.getCompany())
								&& company.equals(newCon.getSub().getComp())) {
							jobItem.setOnBehalf(null);
							onBehalfServ.delete(oldOnBehalf);
							jobItem = jiServ.mergeJobItem(jobItem);
						} else if (oldOnBehalf == null && !company.equals(newCon.getSub().getComp()))
							jiServ.linkOnBehalf(jobItem, contact, company, address);
					}
				}
			}

			this.jobServ.update(j);

			return new ModelAndView(new RedirectView("viewjob.htm?jobid=" + j.getJobid()));
		}
	}
}