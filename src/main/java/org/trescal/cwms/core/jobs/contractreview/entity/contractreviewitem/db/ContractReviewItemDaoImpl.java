package org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.db;

import org.eclipse.birt.report.model.elements.Translation;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.Contract_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview_;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem_;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.Capability_;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;

import javax.persistence.criteria.*;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Repository("ContractReviewItemDao")
public class ContractReviewItemDaoImpl extends BaseDaoImpl<ContractReviewItem, Integer>
    implements ContractReviewItemDao {

    @Override
    protected Class<ContractReviewItem> getEntity() {
        return ContractReviewItem.class;
    }

    @Override
    public List<Integer> getContractReviewedJobItemIds(int jobid) {
        return getResultList(cb -> {
            CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
            Root<ContractReviewItem> root = cq.from(ContractReviewItem.class);
            Join<ContractReviewItem, JobItem> jobitem = root.join(ContractReviewItem_.jobitem, JoinType.INNER);
            Join<JobItem, Job> job = jobitem.join(JobItem_.job, JoinType.INNER);

            cq.where(cb.equal(job.get(Job_.jobid), jobid));
			cq.select(jobitem.get(JobItem_.jobItemId));
			cq.distinct(true);
			return cq;
		});
	}

	@Override
	public List<JobItemProjectionDTO> getContractReviewItemsDTOs(Integer ContractReviewId, Locale locale) {
		return getResultList(cb -> {

			CriteriaQuery<JobItemProjectionDTO> cq = cb.createQuery(JobItemProjectionDTO.class);
			Root<ContractReviewItem> root = cq.from(ContractReviewItem.class);
			Join<ContractReviewItem, JobItem> jobItem = root.join(ContractReviewItem_.jobitem);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job, JoinType.INNER);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst, JoinType.INNER);

            Join<Instrument, InstrumentModel> instrumentModel = instrument.join(Instrument_.model, JoinType.INNER);

            Join<InstrumentModel, Translation> instrumentModelTranslation = instrumentModel
                .join(InstrumentModel_.NAME_TRANSLATIONS, JoinType.LEFT);
            instrumentModelTranslation.on(cb.equal(instrumentModelTranslation.get(Translation_.LOCALE), locale));

            Join<Instrument, Mfr> instrumentMfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
            Join<InstrumentModel, Description> instrumentModelDescription = instrumentModel
                .join(InstrumentModel_.description, JoinType.INNER);
            Join<JobItem, CalibrationType> calType = jobItem.join(JobItem_.calType, JoinType.LEFT);
            Join<JobItem, Capability> capability = jobItem.join(JobItem_.capability, JoinType.LEFT);
            Join<JobItem, ItemState> itemState = jobItem.join(JobItem_.state, JoinType.LEFT);
            Join<JobItem, OnBehalfItem> onBehalf = jobItem.join(JobItem_.onBehalf, JoinType.LEFT);
            Join<OnBehalfItem, Company> onBehalfCompany = onBehalf.join(OnBehalfItem_.company, JoinType.LEFT);

            Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType, JoinType.LEFT);
            Join<ServiceType, Translation> shortnameTranslation = serviceType.join(ServiceType_.SHORTNAME_TRANSLATION,
                JoinType.LEFT);
            shortnameTranslation.on(cb.equal(shortnameTranslation.get(Translation_.LOCALE), locale));

            Join<JobItem, Contract> contract = jobItem.join(JobItem_.contract, JoinType.LEFT);

			Subquery<Long> contactReviewItemsCount = cq.subquery(Long.class);
			Root<ContractReviewItem> criRoot = contactReviewItemsCount.from(ContractReviewItem.class);
			contactReviewItemsCount.where(cb.equal(criRoot.get(ContractReviewItem_.jobitem), root));
			contactReviewItemsCount.select(cb.count(criRoot));

			// Some fields (lastAction fields) only used by
			// NewSelectedJobItemsDao (for
			// now), hence nullLiteral
			CompoundSelection<JobItemProjectionDTO> selection = cb.construct(JobItemProjectionDTO.class,
                jobItem.get(JobItem_.jobItemId), job.get(Job_.jobid), jobItem.get(JobItem_.itemNo),

                instrument.get(Instrument_.plantid), instrument.get(Instrument_.plantno),
                instrument.get(Instrument_.serialno), instrument.get(Instrument_.customerDescription),
                instrument.get(Instrument_.calibrationStandard), instrument.get(Instrument_.scrapped),
                instrument.get(Instrument_.modelname), instrumentModel.get(InstrumentModel_.modelid),
                instrumentModelTranslation.get(Translation_.TRANSLATION),
                instrumentModel.get(InstrumentModel_.modelMfrType), instrumentMfr.get(Mfr_.genericMfr),
                instrumentMfr.get(Mfr_.name), instrumentModelDescription.get(Description_.typology),

                serviceType.get(ServiceType_.serviceTypeId), shortnameTranslation.get(Translation_.TRANSLATION),
                capability.get(Capability_.id), itemState.get(ItemState_.stateid),
                onBehalfCompany.get(Company_.coid), cb.nullLiteral(Integer.class), jobItem.get(JobItem_.turn),
                jobItem.get(JobItem_.dateComplete), jobItem.get(JobItem_.dateIn), jobItem.get(JobItem_.dueDate),
                jobItem.get(JobItem_.clientRef), contract.get(Contract_.id),
                cb.selectCase().when(cb.ge(contactReviewItemsCount.getSelection(), 1), true).otherwise(false),
                cb.nullLiteral(Integer.class), cb.nullLiteral(Date.class), cb.nullLiteral(String.class));

			cq.where(cb.equal(root.get(ContractReviewItem_.review).get(ContractReview_.id), ContractReviewId));

			cq.orderBy(cb.asc(jobItem.get(JobItem_.itemNo)));
			cq.select(selection);

			return cq;
		});
	}

	@Override
	public List<Integer> getJobItemsCouverdInContractReview(Integer ContractReviewId, Integer jobid) {
		return getResultList(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<ContractReviewItem> root = cq.from(ContractReviewItem.class);
			Join<ContractReviewItem, JobItem> jobitem = root.join(ContractReviewItem_.jobitem, JoinType.INNER);
			Join<JobItem, Job> job = jobitem.join(JobItem_.job, JoinType.INNER);

			cq.where(cb.equal(job.get(Job_.jobid), jobid),
					cb.equal(root.get(ContractReviewItem_.review).get(ContractReview_.id), ContractReviewId));
			cq.select(jobitem.get(JobItem_.itemNo));
			return cq;
		});
	}
}