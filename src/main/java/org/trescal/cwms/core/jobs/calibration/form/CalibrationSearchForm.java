package org.trescal.cwms.core.jobs.calibration.form;

import java.util.Date;

import org.trescal.cwms.core.jobs.calibration.dto.CalibrationDTO;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CalibrationSearchForm
{
	private String barcode;
	private Integer calProId;
	private Integer calTypeId;
	private Integer deptId;
	private String jobNo;
	private Integer modelid;
	private int pageNo;
	private Integer personid;
	private Integer procedureId;
	private String procedureReferenceAndName;
	private int resultsPerPage = 20;
	private PagedResultSet<CalibrationDTO> rs;
	private String standardBarcode;
	private Date startDate1;
	private Date startDate2;
	private boolean startDateBetween;
	private Integer statusId;

}