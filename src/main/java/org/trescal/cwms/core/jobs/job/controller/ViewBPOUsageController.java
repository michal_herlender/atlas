package org.trescal.cwms.core.jobs.job.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.dto.BPOPeriodicInvoiceDTO;
import org.trescal.cwms.core.jobs.job.dto.BPOUsageDTO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

@Controller
@IntranetController
public class ViewBPOUsageController {

	@Autowired
	private BPOService bpoService;
	@Value("${cwms.config.bpo.usage.periodicinvoice}")
	private boolean enablePeriodicInvoice;
	@Value("${cwms.config.bpo.usage.jobitemsonpilimit}")
	private Integer jobItemsOnPeriodicInvoiceLimit;

	public static final String REQUEST_URL = "/viewbpousage.htm";
	public static final String VIEW_NAME = "/trescal/core/jobs/bpousage";

	@RequestMapping(value = REQUEST_URL, method = RequestMethod.GET)
	public String referenceData(@RequestParam(name = "poid", required = true) int poid, Model model, Locale locale) {
		BPO bpo = this.bpoService.get(poid);
		model.addAttribute("bpo", bpo);
		List<BPOUsageDTO> bpoUsage = this.bpoService.getBPOUsage(bpo.getPoId(), locale);
		List<BPOUsageDTO> expenseItems = this.bpoService.getExpenseItemAmounts(poid, locale);
		BigDecimal spentTotal = bpoUsage.stream().map(BPOUsageDTO::getAmount).reduce(BigDecimal.valueOf(0, 2),
			BigDecimal::add);
		spentTotal = spentTotal.add(
			expenseItems.stream().map(BPOUsageDTO::getAmount).reduce(BigDecimal.valueOf(0, 2), BigDecimal::add));
		model.addAttribute("spentTotal", spentTotal);
		model.addAttribute("bpoUsage", bpoUsage);
		model.addAttribute("expenseItems", expenseItems);
		if (enablePeriodicInvoice) {
			List<BPOPeriodicInvoiceDTO> periodicInvoices = bpoService.getPeriodicInvoicesLinkedTo(poid, locale);
			BigDecimal periodicInvoiceSum = periodicInvoices.stream().map(pi -> pi.getPeriodicInvoiceAmount())
					.reduce(BigDecimal.valueOf(0, 2), BigDecimal::add);
			model.addAttribute("jobItemsOnPeriodicInvoiceLimit", jobItemsOnPeriodicInvoiceLimit);
			model.addAttribute("periodicInvoices", periodicInvoices);
			model.addAttribute("remainingTotal", bpo.getLimitAmount() == null ? null
					: bpo.getLimitAmount().subtract(spentTotal).subtract(periodicInvoiceSum));
		} else {
			model.addAttribute("periodicInvoices", Collections.emptyMap());
			model.addAttribute("periodicInvoiceSum", new BigDecimal("0.00"));
			model.addAttribute("remainingTotal",
					bpo.getLimitAmount() == null ? null : bpo.getLimitAmount().subtract(spentTotal));
		}
		return VIEW_NAME;
	}
}