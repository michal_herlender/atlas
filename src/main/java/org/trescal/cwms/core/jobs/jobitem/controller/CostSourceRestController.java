package org.trescal.cwms.core.jobs.jobitem.controller;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.entity.costs.dto.CostJobItemDto;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.db.JobCostingCalibrationCostService;
import org.trescal.cwms.core.pricing.lookup.PriceLookupRequirements;
import org.trescal.cwms.core.pricing.lookup.PriceLookupService;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupInput;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupInputFactory;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutput;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.trescal.cwms.core.tools.InstModelTools.instrumentModelNameViaTranslations;
import static org.trescal.cwms.core.tools.InstModelTools.instrumentModelNameWithTypology;
import static org.trescal.cwms.core.tools.TranslationUtils.getBestTranslation;

@RestController
@RequestMapping("costsource")
public class CostSourceRestController {

    @Autowired
    private CatalogPriceService catalogPriceService;
    @Autowired
    private JobCostingCalibrationCostService jobCostingCalibrationCostService;
    @Autowired
    private JobItemService jobItemService;
    @Autowired
    private PriceLookupService priceLookupService;
    @Autowired
    private QuotationItemService qiService;
    @Value("${cwms.config.quotation.resultsyearfilter}")
    private Integer quotationResultsYearFilter;

    @GetMapping("alternatives.json")
    CostSourceAlternativesOut alternatives(@RequestParam Integer jobItemId){
        val jobItem = jobItemService.findJobItem(jobItemId);
        val pricees = getPriceLookupOutputs(jobItem);
        val partitions = splitPricess(pricees);
        val catalogPrices = collectPriceCatalog(pricees);
        val clientCompanyId = jobItem.getJob().getCon().getSub().getComp().getCoid();
        val jobCostingOutputs =  this.jobCostingCalibrationCostService.findMatchingCosts(
            jobItem.getInst().getPlantid(), clientCompanyId, jobItem.getInst().getModel().getModelid(),
            jobItem.getServiceType().getCalibrationType().getCalTypeId(), 1, null)
            .stream().map(CostJobItemDto::fromJobCostingCalibrationCost).collect(Collectors.toList()) ;

        return CostSourceAlternativesOut.builder()
            .quotationRegDateAfter(getQuotationRegDateAfter())
            .summary(collectSummary(jobItem))
            .activeQuotationItems(partitions.get(false))
            .inactiveQuotationItems(partitions.get(true))
            .priceCatalogs(catalogPrices)
            .businessCurrencyCode(jobItem.getJob().getOrganisation().getComp().getCurrency().getCurrencyCode())
            .jobCosting(jobCostingOutputs)
            .build();
    }

    private List<PriceCatalogDto> collectPriceCatalog(Set<PriceLookupOutput> pricees) {
        return pricees.stream().filter(p->p.getCatalogPriceId() != null)
            .map(p -> Tuple.of(p,catalogPriceService.get(p.getCatalogPriceId())))
            .map(this::catalogPriceToDto)
            .collect(Collectors.toList());
    }

    private  PriceCatalogDto catalogPriceToDto(Tuple2<PriceLookupOutput,CatalogPrice> tuple2) {
        val p = tuple2._1;
        val catalog = tuple2._2;
        val locale = LocaleContextHolder.getLocale();
        return PriceCatalogDto.builder()
            .source(p.getQueryType().getMessage())
            .modelTypeName(getBestTranslation(catalog.getInstrumentModel().getModelType().getModelTypeNameTranslation()).orElse(""))
            .modelId(catalog.getInstrumentModel().getModelid())
            .modelName(instrumentModelNameWithTypology(catalog.getInstrumentModel(),locale))
            .serviceType(getBestTranslation(catalog.getServiceType().getShortnameTranslation()).orElse(""))
            .price(catalog.getFixedPrice())
            .id(catalog.getId())
            .build();
    }

    Map<Boolean, List<QuotationItemOutputDto>> splitPricess(Set<PriceLookupOutput> prices){
        return prices.stream().filter(p -> p.getQuotationItemId() != null)
            .map(p -> Tuple.of(p,qiService.findQuotationItem(p.getQuotationItemId())))
            .map(this::processPriceQuotationPair).collect(Collectors.partitioningBy(QuotationItemOutputDto::getIsOther));

    }

    private QuotationItemOutputDto processPriceQuotationPair(Tuple2<PriceLookupOutput, Quotationitem> tuple) {
        val today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
        val plo = tuple._1;
        val qi = tuple._2;
        return QuotationItemOutputDto.builder()
            .isOther(!(plo.getPreferredQuotation() ||
                (plo.getQuotationAccepted() && plo.getQuotationIssued() &&
                    plo.getQuotationExpiryDate() != null && plo.getQuotationExpiryDate().isAfter(today))))
            .prefered(plo.getPreferredQuotation())
            .statusName(getBestTranslation(qi.getQuotation().getQuotestatus().getNametranslations()).orElse(""))
            .source(plo.getQueryType().getMessage())
            .issued(plo.getQuotationIssued())
            .accepted(plo.getQuotationAccepted())
            .exiryDate(plo.getQuotationExpiryDate())
            .expired(qi.getQuotation().isExpired())
            .quoteId(qi.getQuotation().getId())
            .quoteNo(qi.getQuotation().getQno())
            .quotationVer(qi.getQuotation().getVer())
            .itemId(qi.getId())
            .itemNo(qi.getItemno())
            .price(plo.getQuotationCalCostTotalCost())
            .discountRate(plo.getQuotationCalCostDiscountRate())
            .discount(qi.getCalibrationCost().getDiscountValue())
            .finalPrice(qi.getCalibrationCost().getFinalCost())
            .currencyCode(qi.getQuotation().getCurrency().getCurrencyCode())
            .costId(plo.getQuotationCalCostId())
            .build();
    }

    private ItemSummary collectSummary(JobItem jobItem) {
        val inst = jobItem.getInst();
        val serviceType = jobItem.getServiceType();
        val contract = jobItem.getContract();
        return ItemSummary.builder()
            .companyName(jobItem.getJob().getCon().getSub().getComp().getConame())
            .plantId(inst.getPlantid())
            .modelId(inst.getModel().getModelid())
            .modelName(instrumentModelNameViaTranslations(inst,false, LocaleContextHolder.getLocale(), Locale.getDefault()) )
            .likedQuotes(collectLinkeqQuotes(jobItem.getJob().getLinkedQuotes()))
            .serviceTypeLabel(getBestTranslation(serviceType.getShortnameTranslation()).orElse("")+" - "+
                getBestTranslation(serviceType.getLongnameTranslation()).orElse(""))
            .contractNumber(contract  != null ? contract.getContractNumber():null)
            .contractQuotation(contract !=null && contract.getQuotation() != null?
                   quotationToLinkedQuotDto(contract.getQuotation()):null
                )
            .build();
    }

    private List<LinkedQuotDto> collectLinkeqQuotes(Set<JobQuoteLink> linkedQuotes) {
        return linkedQuotes.stream()
            .map(JobQuoteLink::getQuotation).map(this::quotationToLinkedQuotDto).collect(Collectors.toList());
    }

    private LinkedQuotDto quotationToLinkedQuotDto(Quotation q){
        val label = q.getQno()+" ver "+q.getVer();
        return LinkedQuotDto.of(q.getId(),label);
    }

    private SortedSet<PriceLookupOutput> getPriceLookupOutputs(JobItem jobItem) {
        Integer businessCompanyId = jobItem.getJob().getOrganisation().getComp().getCoid();
        Integer clientCompanyId = jobItem.getJob().getCon().getSub().getComp().getCoid();
        Integer serviceTypeId = jobItem.getServiceType().getServiceTypeId();
        Integer instrumentId = jobItem.getInst().getPlantid();
        List<Integer> includeQuotationIds = getQuotationIds(jobItem);

        PriceLookupRequirements reqs = new PriceLookupRequirements(businessCompanyId, clientCompanyId, true);
        reqs.setFirstMatchSearch(false);
        reqs.setIncludeQuotationIds(includeQuotationIds);
        reqs.setQuotationAccepted(null);
        reqs.setQuotationIssued(null);
        reqs.setQuotationExpiryDateAfter(null);
        reqs.setQuotationRegDateAfter(getQuotationRegDateAfter());

        PriceLookupInputFactory factory = new PriceLookupInputFactory(reqs);
        PriceLookupInput input = factory.addInstrument(serviceTypeId, instrumentId);

        reqs.addInput(input);
        this.priceLookupService.findPrices(reqs);
        return input.getOutputs();
    }

    private LocalDate getQuotationRegDateAfter() {
        return LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())
            .minusYears(quotationResultsYearFilter);
    }

    private List<Integer> getQuotationIds(JobItem jobItem) {
        val streamFromContract = jobItem.getContract() != null && jobItem.getContract().getQuotation() != null ?
            Stream.of(jobItem.getContract().getQuotation().getId()) : Stream.<Integer>empty();
        val streamIds = jobItem.getJob().getLinkedQuotes().stream().map(JobQuoteLink::getQuotation)
            .map(Quotation::getId);
        val stream = Stream.of(streamIds, streamFromContract).flatMap(Function.identity());
        return stream.collect(Collectors.toList());
    }

    @Data
    @AllArgsConstructor @NoArgsConstructor
    @Builder
    private static class PriceCatalogDto {

        private Integer id;
        private String source;
        private String modelTypeName;
        private Integer modelId;
        private String modelName;
        private String serviceType;
        private BigDecimal price;

    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    private static class CostSourceAlternativesOut{
        private LocalDate quotationRegDateAfter;
        private ItemSummary summary;
        private List<QuotationItemOutputDto> activeQuotationItems;
        private List<QuotationItemOutputDto> inactiveQuotationItems;
        private List<PriceCatalogDto> priceCatalogs;
        private String businessCurrencyCode;
        private List<CostJobItemDto> jobCosting;

    }
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    private static class ItemSummary {
        private  String companyName;
        private Integer plantId;
        private Integer modelId;
        private String serviceTypeLabel;
        private String modelName;
        private List<LinkedQuotDto> likedQuotes;
        private String contractNumber;
        private LinkedQuotDto contractQuotation;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    private  static class QuotationItemOutputDto {
        private Boolean isOther;
        private Boolean prefered;
        private Boolean accepted;
        private Boolean issued;
        private String statusName;
        private String source;
        private Boolean expired;
        private LocalDate exiryDate;
        private Integer quoteId;
        private String quoteNo;
        private Integer quotationVer;
        private Integer itemNo;
        private Integer itemId;
        private BigDecimal price;
        private BigDecimal discountRate;
        private BigDecimal discount;
        private BigDecimal finalPrice;
        private String currencyCode;
        private Integer costId;
    }
}



