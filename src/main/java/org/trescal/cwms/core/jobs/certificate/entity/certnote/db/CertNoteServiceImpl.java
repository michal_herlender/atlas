package org.trescal.cwms.core.jobs.certificate.entity.certnote.db;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.dwr.DWRService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certnote.CertNote;
import org.trescal.cwms.core.validation.BeanValidator;

@Service("certNoteService")
public class CertNoteServiceImpl implements CertNoteService
{
	@Autowired
	private CertNoteDao certNoteDao;
	@Autowired
	private BeanValidator beanValidator;
	@Autowired
	private CertificateService certServ;
	@Autowired
	private DWRService dwrServ;

	public void deleteCertNote(CertNote certnote)
	{
		this.certNoteDao.remove(certnote);
	}

	public void deleteCertNoteById(int id)
	{
		this.certNoteDao.remove(this.findCertNote(id));
	}

	public CertNote findCertNote(int id)
	{
		return this.certNoteDao.find(id);
	}

	public List<CertNote> getAllCertNotes()
	{
		return this.certNoteDao.findAll();
	}

	public void insertCertNote(CertNote CertNote)
	{
		this.certNoteDao.persist(CertNote);
	}

	public ResultWrapper insertCertNoteDWR(String note, int certId)
	{
		CertNote n = new CertNote();
		n.setActive(true);
		n.setCert(this.certServ.findCertificate(certId));
		n.setNote(note);
		n.setPublish(false);
		n.setSetBy(this.dwrServ.getCurrentUser());
		n.setSetOn(new Date());

		// create a new validator to validate this CertNote before
		// trying the insert
		BindException errors = new BindException(n, "certnote");
		this.beanValidator.validate(n, errors);

		// if validation passes then insert the CertNote
		if (!errors.hasErrors())
		{
			this.certNoteDao.persist(n);
		}

		return new ResultWrapper(errors, n);
	}

	public void saveOrUpdateCertNote(CertNote certnote)
	{
		this.certNoteDao.saveOrUpdate(certnote);
	}

	public void setCertNoteDao(CertNoteDao certNoteDao)
	{
		this.certNoteDao = certNoteDao;
	}

	public void setBeanValidator(BeanValidator beanValidator)
	{
		this.beanValidator = beanValidator;
	}

	public void setCertServ(CertificateService certServ)
	{
		this.certServ = certServ;
	}

	public void setDwrServ(DWRService dwrServ)
	{
		this.dwrServ = dwrServ;
	}

	public void updateCertNote(CertNote CertNote)
	{
		this.certNoteDao.update(CertNote);
	}

	public ResultWrapper updateCertNoteDWR(String note, int certNoteId)
	{
		CertNote n = this.findCertNote(certNoteId);
		n.setNote(note);
		n.setSetBy(this.dwrServ.getCurrentUser());

		// create a new validator to validate this CertNote before
		// trying the insert
		BindException errors = new BindException(n, "certnote");
		this.beanValidator.validate(n, errors);

		// if validation passes then insert the CertNote
		if (!errors.hasErrors())
		{
			this.certNoteDao.update(n);
		}

		return new ResultWrapper(errors, n);
	}
}