package org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.CallOffItem;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.dto.CallOffItemDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.form.JICallOffItemForm;

import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Interface for accessing and manipulating {@link CallOffItem} entities.
 */
public interface CallOffItemService extends BaseService<CallOffItem, Integer> {
	/**
	 * Adds the {@link CallOffItem}s with the given IDs to the given
	 * {@link Job} as {@link JobItem}s. The items will begin numbering at the
	 * given nextItemNo.
	 * 
	 * @param callOffItemIds the {@link CallOffItem} IDs
	 * @param job the {@link Job} to add the {@link CallOffItem}s to
	 * @param currentContact the {@link Contact} currently logged in
	 * @param nextItemNo the item number to begin at
	 * @return the next {@link JobItem} item number (after all items have been
	 *         added)
	 */
	int addCallOffItemsToNewJob(List<Integer> callOffItemIds, Job job, Contact currentContact, int nextItemNo);
	
	/**
	 * Creates new call off items, moved business logic here from Controller.
	 */
	void createCallOffItems(JICallOffItemForm form, Contact currentContact);
	
	/**
	 * Returns all {@link CallOffItem} entities stored in the database that are
	 * currently active. i.e. still suspended and not yet assigned to
	 * a new job.
	 *
	 * @return the {@link List} of active {@link CallOffItem} entities.
	 */
	List<CallOffItem> getAllActiveCallOffItems();

	Set<CallOffItemDTO> getAllActiveCallOffItems(Locale locale);

	CallOffItem getActiveForJobItem(int jobItemId);

	List<CallOffItemDTO> getCallOffItemsDtoForCompany(int coid, Boolean active);

	/**
	 * Returns all call off items owned by the {@link Company} with the given
	 * ID.
	 *
	 * @param coid    the {@link Company} ID.
	 * @param active  true returns only active items, false returns non-active
	 *                and null returns both active and inactive.
	 * @param session the HttpSession from which the allocated subdiv is obtained
	 * @return the {@link List} of {@link CallOffItem} entities.
	 */
	List<CallOffItem> getCallOffItemsForCompany(int coid, Boolean active);
	
	/**
	 * Returns a list of {@link CallOffItem} entities with the given IDs.
	 * 
	 * @param ids the list of {@link CallOffItem} IDs.
	 * @return the {@link List} of {@link CallOffItem} entities.
	 */
	List<CallOffItem> getItemsFromIds(List<Integer> ids);
	
	/**
	 * Reactivates the {@link CallOffItem} entities to their original job.
	 * 
	 * @param calloffitem the {@link CallOffItem} to reactivate on its original
	 *        job.
	 */
	void reactivateCallOffItems(List<Integer> ids, Contact contact);
	
	/**
	 * Reactivates the specified {@link CallOffItem} to its original
	 * job.  Mainly exposed for testing.
	 * 
	 * @param id the ID of the {@link CallOffItem} to reactivate on its original
	 *        job.
	 */
	void reactivateCallOffItem(Integer callOffItemId, Contact contact);
}