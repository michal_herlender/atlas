package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.jobitem.dto.ExpenseItemDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db.JobExpenseItemService;

@Controller
@JsonController
public class ExpenseItemController {

	@Autowired
	private JobExpenseItemService expenseItemService;

	@RequestMapping(value = "expenseitems.json", method = RequestMethod.GET)
	@ResponseBody
	public List<ExpenseItemDTO> getExpenseItemsForInvoice(
			@RequestParam(name = "invoiceid", required = true) Integer invoiceId) {
		return expenseItemService.getAvailableForInvoice(invoiceId);
	}
}