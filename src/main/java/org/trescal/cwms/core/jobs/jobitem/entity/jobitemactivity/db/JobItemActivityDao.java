package org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;

public interface JobItemActivityDao extends BaseDao<JobItemActivity, Integer> {

	List<JobItemActivity> getAllIncompleteActivityForContact(Contact contact);

	List<JobItem> getJobItemsWithSameState(int jobId, int stateId, int jobItemId);
}