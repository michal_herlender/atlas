package org.trescal.cwms.core.jobs.jobitem.dto;

import org.trescal.cwms.core.documents.images.ImagePropertyDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostLookUp;

/**
 * DTO object used to provide a more detailed snapshot about a particular
 * {@link JobItem}.
 * 
 * @author Richard
 */
public class DetailedJobItem
{
	private CostLookUp calCosts;
	private ImagePropertyDTO ipdto;
	private String fullModelName;
	private String calTypeLongName;
	private String calTypeShortName;
	private JobItem jobitem;

    public String getFullModelName() {
        return fullModelName;
    }

    public void setFullModelName(String fullModelName) {
        this.fullModelName = fullModelName;
    }

    public String getCalTypeLongName() {
        return calTypeLongName;
    }

    public void setCalTypeLongName(String calTypeLongName) {
        this.calTypeLongName = calTypeLongName;
    }

    public String getCalTypeShortName() {
        return calTypeShortName;
    }

    public void setCalTypeShortName(String calTypeShortName) {
        this.calTypeShortName = calTypeShortName;
    }

    public CostLookUp getCalCosts()
	{
		return this.calCosts;
	}

	public ImagePropertyDTO getIpdto()
	{
		return this.ipdto;
	}

	public JobItem getJobitem()
	{
		return this.jobitem;
	}

	public void setCalCosts(CostLookUp calCosts)
	{
		this.calCosts = calCosts;
	}

	public void setIpdto(ImagePropertyDTO ipdto)
	{
		this.ipdto = ipdto;
	}

	public void setJobitem(JobItem jobitem)
	{
		this.jobitem = jobitem;
	}
}
