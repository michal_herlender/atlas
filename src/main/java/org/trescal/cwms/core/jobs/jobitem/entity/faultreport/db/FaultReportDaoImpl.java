package org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.failurereport.dto.FailureReportDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;

@Repository("FaultReportDao")
public class FaultReportDaoImpl extends BaseDaoImpl<FaultReport, Integer> implements FaultReportDao {

	@Override
	protected Class<FaultReport> getEntity() {
		return FaultReport.class;
	}

	@Override
	public List<FaultReport> getFaultReportListByJobitemId(Integer jobItemId) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<FaultReport> cq = cb.createQuery(FaultReport.class);
		Root<FaultReport> root = cq.from(FaultReport.class);
		Join<FaultReport, JobItem> jobItemJoin = root.join(FaultReport_.jobItem);

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(jobItemJoin.get(JobItem_.jobItemId), jobItemId));

		cq.select(root);
		cq.where(clauses);

		return getEntityManager().createQuery(cq).getResultList();
	}

	@Override
	public FaultReport getLastFailureReportByJobitemId(Integer jobitemId, Boolean valid) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<FaultReport> cq = cb.createQuery(FaultReport.class);
		Root<FaultReport> root = cq.from(FaultReport.class);
		Join<FaultReport, JobItem> jobItemJoin = root.join(FaultReport_.jobItem);

		Predicate clauses = cb.conjunction();
		if (valid != null)
			clauses.getExpressions().add(cb.equal(root.get(FaultReport_.managerValidation), valid));
		clauses.getExpressions().add(cb.equal(jobItemJoin.get(JobItem_.jobItemId), jobitemId));

		cq.select(root);
		cq.where(clauses);
		cq.orderBy(cb.desc(root.get(FaultReport_.faultRepId)));

		return getEntityManager().createQuery(cq).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
	}

	@Override
	public List<FailureReportDTO> getAllFailureReportsByJobitemId(Integer jobitemId) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<FailureReportDTO> cq = cb.createQuery(FailureReportDTO.class);
		Root<FaultReport> root = cq.from(FaultReport.class);
		Join<FaultReport, JobItem> jobItemJoin = root.join(FaultReport_.jobItem.getName());
		Join<FaultReport, Contact> technicianJoin = root.join(FaultReport_.technician.getName(),JoinType.LEFT);
		Join<FaultReport, Contact> managerJoin = root.join(FaultReport_.managerValidationBy.getName(),JoinType.LEFT);

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(jobItemJoin.get(JobItem_.jobItemId.getName()), jobitemId));

		cq.select(cb.construct(FailureReportDTO.class, root.get(FaultReport_.faultRepId.getName()),
				root.get(FaultReport_.faultReportNumber.getName()),
				root.get(FaultReport_.issueDate.getName()),
				cb.concat(cb.concat(technicianJoin.get(Contact_.firstName.getName()), "  "),
						technicianJoin.get(Contact_.lastName.getName())),
				root.get(FaultReport_.managerValidationOn.getName()),
						cb.concat(cb.concat(managerJoin.get(Contact_.firstName.getName()), "  "),
								managerJoin.get(Contact_.lastName.getName())),
				root.get(FaultReport_.clientResponseNeeded.getName()),
				root.get(FaultReport_.clientApprovalOn.getName()),
				root.get(FaultReport_.approvalType.getName()),
				root.get(FaultReport_.finalOutcome.getName())
						));
		cq.where(clauses);
		cq.orderBy(cb.desc(root.get(FaultReport_.issueDate.getName())));
		
		return getEntityManager().createQuery(cq).getResultList();

	}

	@Override
	public FaultReport getFirstFailureReportByJobitemIdForDelivery(Integer jobitemId) {
		return this.getFirstResult(cb -> {
			CriteriaQuery<FaultReport> cq = cb.createQuery(FaultReport.class);
			Root<FaultReport> frRoot = cq.from(FaultReport.class);
			Join<FaultReport, JobItem> jiJoin = frRoot.join(FaultReport_.jobItem.getName());
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.and(cb.equal(frRoot.get(FaultReport_.requireDeliveryToClient.getName()), true),
					cb.equal(jiJoin.get(JobItem_.jobItemId.getName()), jobitemId)));
			cq.where(clauses);
			cq.orderBy(cb.asc(frRoot.get(FaultReport_.faultRepId)));
			return cq;
		}).orElse(null);
	}
}