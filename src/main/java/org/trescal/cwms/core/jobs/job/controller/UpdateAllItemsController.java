package org.trescal.cwms.core.jobs.job.controller;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentrange.InstrumentRange;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.db.ThreadService;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.db.ThreadTypeService;
import org.trescal.cwms.core.instrumentmodel.entity.range.RangeComparator;
import org.trescal.cwms.core.instrumentmodel.entity.uom.db.UoMService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.form.UpdateAllItemsForm;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.workinstruction.db.WorkInstructionService;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

@Controller
@IntranetController
public class UpdateAllItemsController {

    @Autowired
    private CalibrationTypeService calTypeServ;
    @Autowired
    private InstrumService instServ;
    @Autowired
    private JobItemService jiServ;
    @Autowired
    private JobService jobServ;
    @Autowired
    private CapabilityService procServ;
    @Autowired
    private ThreadService threadServ;
    @Autowired
    private ThreadTypeService threadTypeServ;
    @Autowired
    private UoMService uomServ;
    @Autowired
    private WorkInstructionService wiServ;

    @ModelAttribute("form")
    protected UpdateAllItemsForm formBackingObject(@RequestParam(value = "jobid", required = false, defaultValue = "0") Integer jobId) throws Exception {
		UpdateAllItemsForm form = new UpdateAllItemsForm();
		form.setJob(this.jobServ.get(jobId));
		return form;
	}
	
	@RequestMapping(value="/updateallitems.htm", method=RequestMethod.POST)
	protected RedirectView onSubmit(@ModelAttribute("form") UpdateAllItemsForm form) throws Exception
	{
		Job job = form.getJob();
		int count = 0;
		int rangeCount = 0;
		CalibrationType newCalType = null;
		// if updating the cal type for all items
		if (form.isUpdateCalType()) {
			newCalType = this.calTypeServ.find(form.getCalTypeId());
			// also update the job default cal type
			job.setDefaultCalType(newCalType);
			this.jobServ.update(job);
		}
		for (JobItem ji : job.getItems()) {
			// update thread size on instrument
			Integer threadId = form.getInstThreadIds().get(count);
			ji.getInst().setThread(threadId != null ? this.threadServ.get(threadId) : null);
			ji.getInst().setSerialno(form.getInstSerialNos().get(count));
			ji.getInst().setPlantno(form.getInstPlantNos().get(count));
			if (((ji.getInst().getRanges() != null) && (ji.getInst().getRanges().size() > 0))
					|| ((ji.getInst().getModel().getRanges() != null) && (ji.getInst().getModel().getRanges().size() > 0))) {
				// has range already
			}
			else {
				if (form.getInstStartRanges() != null) {
					if ((form.getInstStartRanges().get(rangeCount) != null)
							&& !form.getInstStartRanges().get(rangeCount).trim().isEmpty()
							&& NumberUtils.isNumber(form.getInstStartRanges().get(rangeCount))) {
						String start = form.getInstStartRanges().get(rangeCount);
						String end = form.getInstEndRanges().get(rangeCount);
						InstrumentRange ir = new InstrumentRange();
						ir.setStart(Double.parseDouble(start));
						if ((end != null) && !end.trim().isEmpty() && NumberUtils.isNumber(end)) {
							ir.setEnd(Double.parseDouble(end));
						}
						ir.setUom(this.uomServ.findUoM(form.getInstUomIds().get(rangeCount)));
						ir.setInstrument(ji.getInst());
						ji.getInst().setRanges(new TreeSet<InstrumentRange>(new RangeComparator()));
						ji.getInst().getRanges().add(ir);
					}
					rangeCount++;
				}
			}
			// persist instrument changes
			this.instServ.update(ji.getInst());
			// update procedure on job item
			Integer procId = form.getItemProcIds().get(count);
            ji.setCapability(procId != null ? this.procServ.get(procId) : null);
			// update work instruction on job item
			Integer wiId = form.getItemWorkInstIds().get(count);
			ji.setWorkInstruction(wiId != null ? this.wiServ.get(wiId) : null);
			// if updating the cal type for all items
			if (form.isUpdateCalType()) {
				ji.setServiceType(newCalType.getServiceType());
			}
			// persist job item changes
			this.jiServ.updateJobItem(ji);
			count++;
		}
		return new RedirectView("viewjob.htm?jobid=" + form.getJob().getJobid());
	}
	
	@RequestMapping(value="/updateallitems.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(HttpSession session) throws Exception
	{
		Map<String, Object> refData = new HashMap<String, Object>();
		refData.put("threadtypes", this.threadTypeServ.getAll());
		refData.put("uoms", this.uomServ.getAllUoMs());
		refData.put("caltypes", this.calTypeServ.getActiveCalTypes());
		return new ModelAndView("/trescal/core/jobs/job/updateallitems", refData);
	}
}