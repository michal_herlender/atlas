package org.trescal.cwms.core.jobs.calibration.entity.reversetraceability;

import lombok.Setter;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Setter
@Table(name = "reversetraceabilitysettings")
public class ReverseTraceabilitySettings {

	private Integer id;
	private Boolean reverseTraceability;
	private Subdiv businessSubdiv;
	private LocalDate startDate;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getId() {
		return id;
	}

	@NotNull
	@Column(name = "reversetraceability", columnDefinition = "tinyint", nullable = false)
	public Boolean getReverseTraceability() {
		return reverseTraceability;
	}

	@NotNull
	@OneToOne
	@JoinColumn(name = "subdivid", nullable = false)
	public Subdiv getBusinessSubdiv() {
		return businessSubdiv;
	}

	@NotNull
	@Column(name = "startdate", columnDefinition = "date", nullable = false)
	public LocalDate getStartDate() {
		return startDate;
	}

}
