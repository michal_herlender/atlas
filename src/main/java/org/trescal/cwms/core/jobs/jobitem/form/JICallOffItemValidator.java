package org.trescal.cwms.core.jobs.jobitem.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class JICallOffItemValidator extends AbstractBeanValidator
{
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(JICallOffItemForm.class);
	}
	
	public void validate(Object obj, Errors errors)
	{
		JICallOffItemForm form = (JICallOffItemForm) obj;

		super.validate(form, errors);
		boolean atLeastOne = false;
		for (Integer i : form.getJobItemIds().keySet())
		{
			if (form.getJobItemIds().get(i) != null)
			{
				atLeastOne = true;
			}
		}
		if (atLeastOne == false)
		{
			errors.reject("error.form.jobitemids", "At least one item must be selected to call off.");
		}
		for (Integer i : form.getReasons().keySet())
		{
			if (form.getReasons().get(i).length() > 1000)
			{
				errors.rejectValue("reasons[" + i + "]", "error.form.reasons", null, "The length of the 'reason' field can not exceed 1000 characters.");
			}
		}
	}
}