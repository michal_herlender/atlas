package org.trescal.cwms.core.jobs.certificate.entity.certnote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;

@Entity
@Table(name = "certnote")
public class CertNote extends Note
{
	public final static boolean privateOnly = true;
	
	private Certificate cert;
	
	/**
	 * @return the cert
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "certid")
	public Certificate getCert() {
		return this.cert;
	}
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.CERTNOTE;
	}
	
	/**
	 * @param cert the cert to set
	 */
	public void setCert(Certificate cert) {
		this.cert = cert;
	}
}