package org.trescal.cwms.core.jobs.certificate.entity.validation;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.EnumSet;

import static org.trescal.cwms.web.tools.validation.EnumFromString.getEnumFromString;

public enum CertificateValidationStatus {

	PENDING("certificatevalidation.status.pending"),
    ACCEPTED("certificatevalidation.status.accepted"),
    REJECTED("certificatevalidation.status.rejected");

    private String messageCode;

    private MessageSource messageSource;


    CertificateValidationStatus(String messageCode) {
        this.messageCode = messageCode;
    }

    @Component
    public static class CertificateValidationStatusMessageSourceInjector
    {
        @Autowired
        private MessageSource messageSource;

        @PostConstruct
        public void postConstruct() {
            for (CertificateValidationStatus status : EnumSet.allOf(CertificateValidationStatus.class))
                status.setMessageSource(messageSource);
        }
    }

    public String getName() {
        return messageSource.getMessage(messageCode, null, LocaleContextHolder.getLocale());
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /*Jackson will use this method to create enum from string received in JSON,
    creates nicer error message if received string isn't valid enum value also
    makes conversion case insensitive*/
    @JsonCreator
    public static CertificateValidationStatus fromValue(String value) {
        return getEnumFromString(CertificateValidationStatus.class, value);
    }
}
