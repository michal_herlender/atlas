package org.trescal.cwms.core.jobs.calibration.entity.callink.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalibrationActivityLink;

@Repository()
public class CalibrationActivityLinkDaoImpl extends BaseDaoImpl<CalibrationActivityLink, Integer>
		implements CalibrationActivityLinkDao {

	@Override
	protected Class<CalibrationActivityLink> getEntity() {
		return CalibrationActivityLink.class;
	}
}