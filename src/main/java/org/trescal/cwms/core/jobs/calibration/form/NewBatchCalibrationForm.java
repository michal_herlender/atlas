package org.trescal.cwms.core.jobs.calibration.form;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationToBatch;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class NewBatchCalibrationForm
{
	private boolean allowUncalibratedStds;
	private boolean autoPrinting;
	private Date calDate;
	private Integer calProcessId;
	private String confirmPassword;
	private BatchCalibration existingBatch;
	private boolean infoAcknowledged;
	private List<CalibrationToBatch> itemCals;
	private Job job;
	private boolean manualSelect;
	private Contact owner;
	private String password;

}