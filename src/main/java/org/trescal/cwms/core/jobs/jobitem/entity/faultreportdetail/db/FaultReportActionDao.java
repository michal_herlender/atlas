package org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportAction;

public interface FaultReportActionDao extends BaseDao<FaultReportAction, Integer> {

}
