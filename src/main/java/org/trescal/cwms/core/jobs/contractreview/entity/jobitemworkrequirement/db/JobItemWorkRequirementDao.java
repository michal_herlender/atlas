package org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public interface JobItemWorkRequirementDao extends BaseDao<JobItemWorkRequirement, Integer> {

	List<JobItemWorkRequirement> getForJobItemView(JobItem jobItem);
}