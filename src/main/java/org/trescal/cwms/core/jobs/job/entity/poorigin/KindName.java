package org.trescal.cwms.core.jobs.job.entity.poorigin;


public enum KindName {
    BLANK("purchaseorder.originBlank", "Blank"),
    JOB("purchaseorder.originJob", "Job"),
    COPY_SIMPLE("purchaseorder.originCopySimple", "Simple copy"),
    COPY_CANCEL("purchaseorder.originCopyCancel", "Copy/Cancel"),
    TP_QUOATATION("purchaseorder.originTPQuote", "TP quotation");

    public final String messageCode;
    public final String defaultMessage;

    KindName(String messageCode, String defaultMessage) {
        this.messageCode = messageCode;
        this.defaultMessage = defaultMessage;
    }
}
