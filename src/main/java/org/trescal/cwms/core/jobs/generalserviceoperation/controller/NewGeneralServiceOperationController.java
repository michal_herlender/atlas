package org.trescal.cwms.core.jobs.generalserviceoperation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db.GeneralServiceOperationService;
import org.trescal.cwms.core.jobs.generalserviceoperation.form.GeneralServiceOperationForm;
import org.trescal.cwms.core.jobs.generalserviceoperation.form.validator.GeneralServiceOperationValidator;
import org.trescal.cwms.core.jobs.jobitem.controller.JobItemController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.misc.tool.CustomLocalDateTimeEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class NewGeneralServiceOperationController extends JobItemController {
	private static final Logger logger = LoggerFactory.getLogger(
			org.trescal.cwms.core.jobs.generalserviceoperation.controller.NewGeneralServiceOperationController.class);
	public static final String ACTION_PERFORMED = "NEW";
	private final String FORM_NAME = "form";

	@Autowired
	private GeneralServiceOperationService gsoServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private GeneralServiceOperationValidator validator;

	@InitBinder(FORM_NAME)
	protected void initBinder(ServletRequestDataBinder binder) {
        binder.registerCustomEditor(Date.class, new MyCustomDateEditor(DateTools.dtf_ISO8601, true));
        binder.registerCustomEditor(LocalDateTime.class, new CustomLocalDateTimeEditor(DateTimeFormatter.ISO_DATE_TIME));
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        binder.setValidator(validator);
    }

	@ModelAttribute(FORM_NAME)
	protected GeneralServiceOperationForm formBackingObject(
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") int jobitemid) throws Exception {
		GeneralServiceOperationForm form = new GeneralServiceOperationForm();

		JobItem ji = this.jobItemService.findJobItem(jobitemid);
		if (ji == null || jobitemid == 0) {
			logger.error("Jobitem with id " + jobitemid + " could not be found");
			throw new Exception("Unable to locate requested jobitem");
		}
		form.setJi(ji);
		if (!this.jobItemService.isReadyForGeneralServiceOperation(ji)) {
			logger.debug("Jobitem with id " + jobitemid + " was not ready for general service operation");
			return form;
		}
		form.setJi(ji);
		form.setGso(new GeneralServiceOperation());
		form.setOtherJobItemsId(new ArrayList<>());
		form.setActionPerformed(ACTION_PERFORMED);

		return form;
	}

	@RequestMapping(value = {"/newgso.htm"}, method = {RequestMethod.GET})
	protected ModelAndView referenceData(@RequestParam(value = "jobitemid") Integer jobItemId,
										 @ModelAttribute("username") String username,
										 @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
										 @ModelAttribute(FORM_NAME) GeneralServiceOperationForm form) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		Map<String, Object> map = referenceData(jobItemId, contact, subdivDto.getKey(),username);

		List<JobItem> otherJis = this.jobItemService.getItemsOnJobInStateGroup(form.getJi().getJob().getJobid(),
			StateGroup.AWAITINGGENERALSERVICEOPERATION);
		// move current ji to first position
		int index = otherJis.indexOf(form.getJi());
		if (index != -1) {
			otherJis.remove(index);
			otherJis.add(0, form.getJi());
		}
		map.put("otherjobitems", otherJis);
		ServiceType serviceType = form.getJi().getNextWorkReq() != null
				? form.getJi().getNextWorkReq().getWorkRequirement().getServiceType()
				: null;
		if (serviceType != null)
			map.put("serviceType", serviceType);
		return new ModelAndView("trescal/core/jobs/generalserviceoperation/newgeneralserviceoperation", map);
	}

	@RequestMapping(value = {"/newgso.htm"}, method = {RequestMethod.POST})
	public ModelAndView onSubmit(@RequestParam(value = "jobitemid") Integer jobItemId,
								 @ModelAttribute("username") String username,
								 @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
								 @ModelAttribute(FORM_NAME) GeneralServiceOperationForm form, BindingResult bindingResult)
		throws Exception {

		Contact user = this.userService.get(username).getCon();
		Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());

		this.validator.validate(form, bindingResult, user);

		if (bindingResult.hasErrors())
			return referenceData(jobItemId, username, subdivDto, form);

		this.gsoServ.startAndSaveGeneralServiceOperation(form, user, allocatedSubdiv);

		return new ModelAndView(new RedirectView("jiactions.htm?jobitemid=" + form.getJi().getJobItemId()));
	}

}
