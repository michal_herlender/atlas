package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.db.CertLinkService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.form.JICalibrationForm;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class JICalibrationController extends JobItemController {
	private static final Logger logger = LoggerFactory.getLogger(JICalibrationController.class);

	@Autowired
	private CalibrationService calServ;
	@Autowired
	private CertificateService certServ;
	@Autowired
	private CertLinkService clServ;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;

	@ModelAttribute("units")
	public List<KeyValue<String, String>> initializeRecallIntervalUnits() {
		return IntervalUnit.getUnitsForSelect();
	}

	@ModelAttribute("form")
	protected JICalibrationForm formBackingObject(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@RequestParam(value = "jobitemid", required = true) Integer jobItemId) throws Exception {
		JICalibrationForm form = new JICalibrationForm();
		JobItem ji = this.jobItemService.findJobItem(jobItemId);
		if ((ji == null) || (jobItemId == 0)) {
			logger.error("Jobitem with id " + jobItemId + " could not be found");
			throw new Exception("Unable to locate requested jobitem");
		}
		form.setJi(ji);
		// reload job to get directory
		form.getJi().setJob(this.jobService.get(ji.getJob().getJobid()));
		// set all cert directories so that they can be opened from tooltips
		// and mouseovers on this page
		for (CertLink cl : form.getJi().getCertLinks()) {
			// sets the directory of the cert
			this.certServ.findCertificate(cl.getCert().getCertid());
		}
		return form;
	}

	@PreAuthorize("@hasJobItemAuthorityOnConnectedSubdiv.check('JI_CALIBRATION')")
	@RequestMapping(value = "/jicalibration.htm", method = RequestMethod.POST)
	protected RedirectView onSubmit(@ModelAttribute("form") JICalibrationForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Contact con = this.userService.get(username).getCon();
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Calibration cal = this.calServ.findCalibration(form.getCalId());
		IntervalUnit durationUnit = IntervalUnit.valueOf(form.getUnitCode());
		Certificate cert = this.certServ.createCertFromCal(con, cal, form.getDuration(), false,
				form.getCalLinkIdsToInclude(), false, allocatedSubdiv, durationUnit,
				CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE, null);
		// set template from calibration
		if ((cal.getCertTemplate() != null) && !cal.getCertTemplate().isEmpty())
			cert.setDocTemplateKey(cal.getCertTemplate());
		for (CertLink cl : cert.getLinks()) {
			Instrument inst = cl.getJobItem().getInst();
			// if we're in this loop we know there's at least one cert link,
			// so get most recent for instrument
			List<CertLink> certs = this.clServ.searchCertLink(inst.getPlantid(), true);
			CertLink mostRecent = certs.get(0);
			// if there's no last cal, this probably means the calibration had a
			// 'failure' outcome when it was previously done, but now as-found
			// results are needed so we can set this as the last cal and update
			// the recall date
			if (inst.getLastCal() == null)
				this.instServ.calculateRecallDateAfterCalibration(inst, cal, form.getDuration(), durationUnit,
						RecallRequirementType.FULL_CALIBRATION);
			else {
				Calibration lastCal = inst.getLastCal();
				if ((cert.getCal().getId() == lastCal.getId())
						&& (mostRecent.getLinkId().intValue() == cl.getLinkId().intValue()))
					// set recall date again if cal is still most recent
					this.instServ.calculateRecallDateAfterEditingCert(inst, cert);
			}
		}
		return new RedirectView("jicertificates.htm?jobitemid=" + form.getJi().getJobItemId());
	}

	@PreAuthorize("hasAuthority('JI_CALIBRATION')")
	@RequestMapping(value = "/jicalibration.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@RequestParam(value = "jobitemid", required = true) Integer jobItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		Map<String, Object> refData = super.referenceData(jobItemId, contact, subdivDto.getKey(),username);
		return new ModelAndView("trescal/core/jobs/jobitem/jicalibration", refData);
	}
}