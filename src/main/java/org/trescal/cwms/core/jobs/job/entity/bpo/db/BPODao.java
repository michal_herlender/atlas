package org.trescal.cwms.core.jobs.job.entity.bpo.db;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.job.dto.BPOKeyValue;
import org.trescal.cwms.core.jobs.job.dto.BPOPeriodicInvoiceDTO;
import org.trescal.cwms.core.jobs.job.dto.BPOUsageDTO;
import org.trescal.cwms.core.jobs.job.dto.BpoDTO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;

import java.util.List;
import java.util.Locale;

public interface BPODao extends AllocatedDao<Company, BPO, Integer> {

	BPO findDWRBPO(int id);

	List<BpoDTO> findAllForJob(Integer jobId, boolean activeOnly);

	List<BpoDTO> getAllByCompany(Company company, Boolean active, Company allocatedCompany);

	List<BpoDTO> getAllByContact(Contact contact, Boolean active, Company allocatedCompany);

	List<BPO> getAllByContactHierarchical(Contact contact, Company allocatedCompany);

	List<BpoDTO> getAllByContactHierarchicalNotOnJob(Integer personId, Integer allocatedCompanyId, Integer jobId);

	List<BpoDTO> getAllBySubdiv(Subdiv subdiv, Boolean active, Company allocatedCompany);

	List<BPO> getExpiringBPOs();

	List<BPOPeriodicInvoiceDTO> getPeriodicInvoicesLinkedTo(Integer bpoId);

	List<BPOUsageDTO> getJobItemsOnPeriodicInvoice(Integer invoiceId, Integer bpoId, Locale locale, Integer limit);

	/**
	 * List all job items with their most recent amount (invoice > job costing >
	 * contract review) linked to the given BPO and without "not invoiced" reason
	 *
	 * @param bpoId linked Blank Purchase Order
	 */
	List<BPOUsageDTO> getJobItemAmounts(Integer bpoId, Locale locale);
	
	List<BPOUsageDTO> getExpenseItemAmounts(Integer bpoId, Locale locale);
	
	List<Integer> getLinkedToJobsOnInvoice(Integer invoiceId);
	
	List<BPOKeyValue> getLinkedToJobItemsOnInvoice(Integer invoiceId);
}