package org.trescal.cwms.core.jobs.jobitem.form;

import org.jasypt.digest.StandardStringDigester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.tools.DigestConfigCreator;
import org.trescal.cwms.core.tools.PasswordTools;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class JICertificatesValidator extends AbstractBeanValidator
{
	@Autowired
	private ContactService contactService;
	@Autowired
	private DigestConfigCreator configCreator;
	
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(JICertificatesForm.class);
	}
	
	public void validate(Object obj, Errors errors)
	{
		JICertificatesForm form = (JICertificatesForm) obj;
		
		super.validate(form, errors);
		
		// BUSINESS VALIDATION
		if (form.getAction().equals("new")) {
			// Make sure Third Party details are present
			if (form.getSubdivid() == null) {
				errors.rejectValue("subdivid", "error.form.subdivid", null, "A Third Party company and subdivison must be selected for Third Party certificates.");
			}

			if ((form.getC().getThirdCertNo() == null) || form.getC().getThirdCertNo().trim().equals("")) {
				errors.rejectValue("c.thirdCertNo", "error.form.c.thirdCertNo", null, "A Third Party Certificate No. must be specified for Third Party certificates.");
			}
		}
		else if (form.getAction().equals("sign")) {
			// The user is trying to use an existing password
			if (form.isPwExists()) {
				Contact con = this.contactService.get(form.getCurrentContactId());
				StandardStringDigester digester = new StandardStringDigester();
				digester.setConfig(this.configCreator.createDigesterConfig());
				if (!digester.matches(form.getEncPassword(), con.getEncryptedPassword())) {
					errors.rejectValue("encPassword", "error.calibration.validator.wrongsignpassword");
				}
			}
			// The user is trying to set a new password
			if (!form.isPwExists()) {
				if (!form.getEncPassword().equals(form.getConfirmPassword())) {
					errors.rejectValue("confirmPassword", "error.calibration.validator.confirmpassword");
				}
				else {
					if (PasswordTools.getPasswordStrength(form.getEncPassword()) < 3) {
						errors.rejectValue("encPassword", "error.calibration.validator.passwordstrength");
					}
				}
			}
		}
	}
}