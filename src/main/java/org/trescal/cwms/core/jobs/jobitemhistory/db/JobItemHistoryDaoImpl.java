package org.trescal.cwms.core.jobs.jobitemhistory.db;

import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitemhistory.JobItemHistory;
import org.trescal.cwms.core.jobs.jobitemhistory.JobItemHistory_;

@Repository("JobItemHistoryDao")
public class JobItemHistoryDaoImpl extends BaseDaoImpl<JobItemHistory, Integer> implements JobItemHistoryDao {
	
	@Override
	protected Class<JobItemHistory> getEntity() {
		return JobItemHistory.class;
	}

	@Override
	public JobItemHistory getJobItemHistoryByChangeDate(Date changeDate) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<JobItemHistory> cq = cb.createQuery(JobItemHistory.class);
		Root<JobItemHistory> root = cq.from(JobItemHistory.class);
		
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(root.get(JobItemHistory_.changeDate), changeDate));
		cq.where(clauses);
		cq.select(root);
		List<JobItemHistory> list = getEntityManager().createQuery(cq).getResultList();
		return list != null && !list.isEmpty() ? list.stream().findFirst().get():null;
	}
}