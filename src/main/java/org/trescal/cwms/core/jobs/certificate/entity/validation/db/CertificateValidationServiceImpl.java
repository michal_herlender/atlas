package org.trescal.cwms.core.jobs.certificate.entity.validation.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidation;

@Service("CertificateValidationService")
public class CertificateValidationServiceImpl extends BaseServiceImpl<CertificateValidation, Integer> implements CertificateValidationService {

	@Autowired
	CertificateValidationDao certificateValidationDao;

	@Override
	protected BaseDao<CertificateValidation, Integer> getBaseDao() {
		return certificateValidationDao;
	}


}
