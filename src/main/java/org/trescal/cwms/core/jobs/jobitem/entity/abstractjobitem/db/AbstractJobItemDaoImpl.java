package org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.AbstractJobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.AbstractJobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitempo.AbstractJobItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitempo.AbstractJobItemPO_;

public abstract class AbstractJobItemDaoImpl<ItemType extends AbstractJobItem<? extends AbstractJobItemPO<ItemType>>>
		extends BaseDaoImpl<ItemType, Integer> implements AbstractJobItemDao<ItemType> {

	public Long countItemsForJobOnBPO(Integer jobId, Integer bpoId) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<ItemType> item = cq.from(getEntity());
			Join<ItemType, AbstractJobItemPO<ItemType>> itemPo = item.join(AbstractJobItem_.ITEM_POS);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(itemPo.get(AbstractJobItemPO_.bpo), bpoId));
			clauses.getExpressions().add(cb.equal(item.get(AbstractJobItem_.job), jobId));
			cq.where(clauses);
			cq.select(cb.count(itemPo));
			return cq;
		});
	}
}