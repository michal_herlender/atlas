package org.trescal.cwms.core.jobs.job.entity.jobquotelink.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.util.List;
import java.util.Set;

public interface JobQuoteLinkDao extends BaseDao<JobQuoteLink, Integer>
{
	List<JobQuoteLink> findJobQuoteLinks(int quoteid, int jobid);
	
	List<JobQuoteLink> findJobQuoteLinksResultset(int quoteid, Integer page, Integer resPerPage);
	
	Integer getCountJobQuoteLinks(int quoteid);
	
	Set<InstrumentModel> getModelIdsFromAllJobQuotes(int jobId);
	
	Set<InstrumentModel> getModelIdsFromJobQuote(int jobQuoteLinkId);

	PagedResultSet<Instrument> getInstrumentsFromJobQuote(PagedResultSet<Instrument> prs, Integer jobQuoteLinkId);

	PagedResultSet<Instrument> getInstrumentsFromAllJobQuotes(PagedResultSet<Instrument> prs, Integer jobId);
}