package org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.calibration.dto.StandardsUsageAnalysisDTO;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.StandardsUsageAnalysis;
import org.trescal.cwms.core.jobs.calibration.form.StandardsUsageAnalysisSearchForm;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

public interface StandardsUsageAnalysisDao extends BaseDao<StandardsUsageAnalysis, Integer> {

    PagedResultSet<StandardsUsageAnalysisDTO> sreachStandardsUsageAnalysis(StandardsUsageAnalysisSearchForm form,
                                                                           PagedResultSet<StandardsUsageAnalysisDTO> prs, Locale locale);

    StandardsUsageAnalysis getStandardsUsageAnalysisFromInstrument(Integer plantId);

    List<StandardsUsageAnalysisDTO> certificatesConnectedToMeasurementStandards(Integer businessSubdivId,
                                                                                LocalDate startDate, List<CalibrationVerificationStatus> calibrationStatus, Locale locale);

}
