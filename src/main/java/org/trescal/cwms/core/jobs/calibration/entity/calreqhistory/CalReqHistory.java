package org.trescal.cwms.core.jobs.calibration.entity.calreqhistory;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;

@Entity
@Table(name = "calreqhistory")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public class CalReqHistory
{
	private Contact changeBy;
	private Date changeOn;
	private int id;
	private CalReq newCalReq;
	private CalReq oldCalReq;

	@NotNull
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "changebyid", nullable = false)
	public Contact getChangeBy()
	{
		return this.changeBy;
	}

	@Column(name = "changeon")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getChangeOn()
	{
		return this.changeOn;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "newcalreqid")
	public CalReq getNewCalReq()
	{
		return this.newCalReq;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "oldcalreqid")
	public CalReq getOldCalReq()
	{
		return this.oldCalReq;
	}

	public void setChangeBy(Contact changeBy)
	{
		this.changeBy = changeBy;
	}

	public void setChangeOn(Date changeOn)
	{
		this.changeOn = changeOn;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setNewCalReq(CalReq newCalReq)
	{
		this.newCalReq = newCalReq;
	}

	public void setOldCalReq(CalReq oldCalReq)
	{
		this.oldCalReq = oldCalReq;
	}
}
