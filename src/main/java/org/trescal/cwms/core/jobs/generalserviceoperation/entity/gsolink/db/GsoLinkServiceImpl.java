package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink;

@Service("GsoLinkService")
public class GsoLinkServiceImpl extends BaseServiceImpl<GsoLink, Integer> implements GsoLinkService
{
  @Autowired
  private GsoLinkDao gsoLinkDao;
  
  protected BaseDao<GsoLink, Integer> getBaseDao() {
     return (BaseDao<GsoLink, Integer>)this.gsoLinkDao;
  }
  
}