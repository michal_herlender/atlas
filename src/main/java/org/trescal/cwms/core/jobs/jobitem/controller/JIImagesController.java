package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.form.JobItemForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class JIImagesController extends JobItemController {
	private static final Logger logger = LoggerFactory.getLogger(JIImagesController.class);

	@ModelAttribute("form")
	protected JobItemForm formBackingObject(
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") Integer jobitemid)
			throws Exception {
		JobItemForm form = new JobItemForm();
		JobItem ji = this.jobItemService.findJobItem(jobitemid);
		if ((ji == null) || (jobitemid == 0)) {
			logger.error("Jobitem with id " + jobitemid + " could not be found");
			throw new Exception("Unable to locate requested jobitem");
		}
		form.setJi(ji);
		return form;
	}

	@PreAuthorize("hasAuthority('JI_IMAGES')")
	@RequestMapping(value = "/jiimages.htm")
	protected ModelAndView handleRequest(@RequestParam(value = "jobitemid", required = true) Integer jobItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		Map<String, Object> refData = super.referenceData(jobItemId, contact, subdivDto.getKey(),username);
		return new ModelAndView("trescal/core/jobs/jobitem/jiimages", refData);
	}
}