package org.trescal.cwms.core.jobs.calibration.entity.point;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.tools.StringTools;

@MappedSuperclass
public abstract class Point extends Auditable
{
	private int id;
	private BigDecimal point;
	private BigDecimal relatedPoint;
	private UoM relatedUom;
	private UoM uom;

	@Transient
	/**
	 * Convenience property that returns the point with any trailing zeros
	 * removed.
	 */
	public String getFormattedPoint()
	{
		return StringTools.formatBigDecimalTrailingZeros(this.point);
	}

	@Transient
	/**
	 * Convenience property that returns the related point with any trailing zeros
	 * removed.
	 */
	public String getFormattedRelatedPoint()
	{
		return StringTools.formatBigDecimalTrailingZeros(this.relatedPoint);
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@Column(name = "point", nullable = false, precision = 19, scale = 4)
	public BigDecimal getPoint()
	{
		return this.point;
	}

	@Column(name = "relatedpoint", nullable = true, precision = 19, scale = 4)
	public BigDecimal getRelatedPoint()
	{
		return this.relatedPoint;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "relateduomid", nullable = true)
	public UoM getRelatedUom()
	{
		return this.relatedUom;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uomid", nullable = false)
	public UoM getUom()
	{
		return this.uom;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setPoint(BigDecimal point)
	{
		this.point = point;
	}

	public void setRelatedPoint(BigDecimal relatedPoint)
	{
		this.relatedPoint = relatedPoint;
	}

	public void setRelatedUom(UoM relatedUom)
	{
		this.relatedUom = relatedUom;
	}

	public void setUom(UoM uom)
	{
		this.uom = uom;
	}
}
