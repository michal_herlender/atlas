package org.trescal.cwms.core.jobs.job.projection.service;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.company.projection.SubdivProjectionDTO;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class JobProjectionResult {
	// Always provided
	private List<JobProjectionDTO> jobDtos;
	// Optionally provided, when loaded as options
	private Collection<ContactProjectionDTO> contactDtos;
	private Collection<SubdivProjectionDTO> businessSubdivDtos;
	private Collection<KeyValueIntegerString> transportOutDtos;
	// TODO transport options
}
