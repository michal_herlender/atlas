package org.trescal.cwms.core.jobs.job.dto;

import java.util.Date;

/**
 * Created by scottchamberlain on 12/04/2016.
 */
public class JobQuoteLinkDTO {

    private Integer quoteLinkId;
    private Integer quoteId;
    private String quoteQno;
    private String quoteCompany;
    private String quoteContact;
    private Integer quoteVersion;
    private Date quoteLinkedOn;
    private String quoteLinkedBy;

    public Integer getQuoteLinkId() {
        return quoteLinkId;
    }

    public void setQuoteLinkId(Integer quoteLinkId) {
        this.quoteLinkId = quoteLinkId;
    }

    public Integer getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(Integer quoteId) {
        this.quoteId = quoteId;
    }

    public String getQuoteQno() {
        return quoteQno;
    }

    public void setQuoteQno(String quoteQno) {
        this.quoteQno = quoteQno;
    }

    public Integer getQuoteVersion() {
        return quoteVersion;
    }

    public void setQuoteVersion(Integer quoteVersion) {
        this.quoteVersion = quoteVersion;
    }

    public Date getQuoteLinkedOn() {
        return quoteLinkedOn;
    }

    public void setQuoteLinkedOn(Date quoteLinkedOn) {
        this.quoteLinkedOn = quoteLinkedOn;
    }

    public String getQuoteLinkedBy() {
        return quoteLinkedBy;
    }

    public void setQuoteLinkedBy(String quoteLinkedBy) {
        this.quoteLinkedBy = quoteLinkedBy;
    }

	public String getQuoteCompany() {
		return quoteCompany;
	}

	public String getQuoteContact() {
		return quoteContact;
	}

	public void setQuoteCompany(String quoteCompany) {
		this.quoteCompany = quoteCompany;
	}

	public void setQuoteContact(String quoteContact) {
		this.quoteContact = quoteContact;
	}

}
