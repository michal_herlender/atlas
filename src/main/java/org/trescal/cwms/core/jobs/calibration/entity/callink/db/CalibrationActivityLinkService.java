package org.trescal.cwms.core.jobs.calibration.entity.callink.db;

import org.trescal.cwms.core.jobs.calibration.entity.callink.CalibrationActivityLink;

public interface CalibrationActivityLinkService {

	void persist(CalibrationActivityLink link);

}