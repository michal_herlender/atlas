package org.trescal.cwms.core.jobs.jobitem.entity.accessory.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitem.dto.ItemAccessoryDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory;

public interface ItemAccessoryDao extends BaseDao<ItemAccessory, Integer> {

	/**
	 * Count all accessories linked to some job item of this job.
	 * 
	 * @param jobId identifier of the job
	 * @return quantity of job item accessories linked to this job
	 */
	Integer countByJob(Integer jobId);

	/**
	 * Count all job items of the job, where accessory free text is not empty
	 * 
	 * @param jobId identifier of the job
	 * @return quantity of job items with non empty accessory free text
	 */
	Integer countFreeTextAccessoriesByJob(Integer jobId);

	/**
	 * Get all item accessories for a job. The accessory name is loaded in the given
	 * language.
	 * 
	 * @param jobId  identifier of the job
	 * @param locale locale to specify the language of accessory name
	 * @return dto of all item accessories of the job
	 */
	List<ItemAccessoryDTO> getAllByJob(Integer jobId, Locale locale);
}