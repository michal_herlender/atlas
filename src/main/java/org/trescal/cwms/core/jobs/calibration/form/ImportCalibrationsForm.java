package org.trescal.cwms.core.jobs.calibration.form;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.batch.config.AutoSubmitPolicyEnum;

import lombok.Data;

@Data
public class ImportCalibrationsForm {

	@NotNull
	private MultipartFile file;
	@NotNull
	private Integer subdivid;
	@NotNull
	private Integer exchangeFormatId;
	private AutoSubmitPolicyEnum autoSubmitPolicy;
	private Integer jobId;
	private boolean automaticContractReview;
	private boolean autoCreateJobitems;

}
