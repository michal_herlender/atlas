package org.trescal.cwms.core.jobs.calibration.dto;

import java.util.Map;

public class CalToCertify
{
	private Map<Integer, String> calLinks;
	private int defaultDuration;
	private String defaultDurationUnitCode;

	public Map<Integer, String> getCalLinks()
	{
		return this.calLinks;
	}

	public int getDefaultDuration()
	{
		return this.defaultDuration;
	}

	public void setCalLinks(Map<Integer, String> calLinks)
	{
		this.calLinks = calLinks;
	}

	public void setDefaultDuration(int defaultDuration)
	{
		this.defaultDuration = defaultDuration;
	}

	public String getDefaultDurationUnitCode() {
		return defaultDurationUnitCode;
	}

	public void setDefaultDurationUnitCode(String defaultDurationUnitCode) {
		this.defaultDurationUnitCode = defaultDurationUnitCode;
	}
	
	
}
