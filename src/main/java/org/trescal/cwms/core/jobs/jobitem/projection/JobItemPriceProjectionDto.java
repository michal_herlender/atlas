package org.trescal.cwms.core.jobs.jobitem.projection;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;

import lombok.Getter;
import lombok.Setter;

/**
 * This stores a projection of prices related to a job item
 * Is used directly for projections of ContractReview*Cost
 * with subclass projecting JobCosting*Cost, in future Invoice*Cost  
 */
@Getter @Setter
public class JobItemPriceProjectionDto {
	private Integer jobItemId;
	private List<PriceProjectionDto> prices;
	private PriceProjectionDto calPriceDto;
	private PriceProjectionDto adjPriceDto;
	private PriceProjectionDto repPriceDto;
	private PriceProjectionDto purPriceDto;
	
	public JobItemPriceProjectionDto(Integer jobItemId,
			// Calibration cost
			Integer calCostId, Boolean calActive, CostSource calCostSource,	BigDecimal calDiscountRate,	
			BigDecimal calDiscountValue, BigDecimal calFinalCost, BigDecimal calTotalCost,
			// Adjustment cost
			Integer adjCostId, Boolean adjActive, CostSource adjCostSource,	BigDecimal adjDiscountRate,	
			BigDecimal adjDiscountValue, BigDecimal adjFinalCost, BigDecimal adjTotalCost,
			// Repair cost
			Integer repCostId, Boolean repActive, CostSource repCostSource,	BigDecimal repDiscountRate,	
			BigDecimal repDiscountValue, BigDecimal repFinalCost, BigDecimal repTotalCost,
			// Purchase cost
			Integer purCostId, Boolean purActive, CostSource purCostSource,	BigDecimal purDiscountRate,	
			BigDecimal purDiscountValue, BigDecimal purFinalCost, BigDecimal purTotalCost) {
		this.jobItemId = jobItemId;
		this.prices = new ArrayList<>();
		if (calCostId != null) {
			this.calPriceDto = new PriceProjectionDto(calCostId, calActive, calCostSource, CostType.CALIBRATION,
					calDiscountRate, calDiscountValue, calFinalCost, calTotalCost);
			this.prices.add(this.calPriceDto);
		}
		if (adjCostId != null) {
			this.adjPriceDto = new PriceProjectionDto(adjCostId, adjActive, adjCostSource, CostType.ADJUSTMENT,
					adjDiscountRate, adjDiscountValue, adjFinalCost, adjTotalCost);
			this.prices.add(this.adjPriceDto);
		}
		if (repCostId != null) {
			this.repPriceDto = new PriceProjectionDto(repCostId, repActive, repCostSource, CostType.REPAIR,
					repDiscountRate, repDiscountValue, repFinalCost, repTotalCost);
			this.prices.add(this.repPriceDto);
		}
		if (purCostId != null) {
			this.purPriceDto = new PriceProjectionDto(purCostId, purActive, purCostSource, CostType.PURCHASE,
					purDiscountRate, purDiscountValue, purFinalCost, purTotalCost);
			this.prices.add(this.purPriceDto);
		}
	}
}
