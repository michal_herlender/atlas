package org.trescal.cwms.core.jobs.jobitem.entity.lookupsource;

public enum PointsLookupSource
{
	JOB, QUOTATION, INSTRUMENT;
}
