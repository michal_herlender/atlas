package org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db.JobItemWorkRequirementService;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.procedure.entity.workinstruction.db.WorkInstructionService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.workallocation.engineerallocation.db.EngineerAllocationService;

import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;

@Service("WorkRequirementService")
public class WorkRequirementServiceImpl extends BaseServiceImpl<WorkRequirement, Integer>
    implements WorkRequirementService {

    @Autowired
    private JobItemService jiServ;
    @Autowired
    private CapabilityService procServ;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private WorkInstructionService wiServ;
    @Autowired
    private JobItemWorkRequirementService jobItemWorkRequirementService;
    @Autowired
    private WorkRequirementDao workRequirementDao;
    @Autowired
    private EngineerAllocationService eaService;

    private final static Logger logger = LoggerFactory.getLogger(WorkRequirementService.class);

	@Override
	protected BaseDao<WorkRequirement, Integer> getBaseDao() {
		return workRequirementDao;
	}

	@Override
	public ResultWrapper ajaxDeleteWorkRequirement(int wrId, int jobItemId) {
		// get work requirement
		WorkRequirement workReq = this.get(wrId);
		// work requirement null?
		if (workReq != null) {
			// work requirement already completed?
			if (!workReq.getStatus().equals(WorkrequirementStatus.COMPLETE)
					&& !workReq.getStatus().equals(WorkrequirementStatus.CANCELLED)) {
				// get job item
				JobItem ji = this.jiServ.findJobItem(jobItemId);
				// job item null?
				if (ji != null) {
					// check this is not the job items next work requirement
					if ((ji.getNextWorkReq() != null) && (ji.getNextWorkReq().getId() != workReq.getId())) {
						// delete this work requirement
						this.delete(workReq);
						// return success
						return new ResultWrapper(true, "", null, null);
					} else {
						// return error message
						return new ResultWrapper(false,
								"Work requirement is the job items next work requirement and so cannot be deleted",
								null, null);
					}
				} else {
					// return error message
					return new ResultWrapper(false, "Job item for work requirement could not be found", null, null);
				}
			} else {
				// return error message
				return new ResultWrapper(false, "Work requirement is complete and so cannot be deleted", null, null);
			}
		} else {
			// return error message
			return new ResultWrapper(false, "Work requirement could not be found", null, null);
		}
	}

	@Override
	public void checkAllWorkRequirement(JobItem jobItem) {
		jobItem.getWorkRequirements().forEach(jiwr -> {
			WorkRequirement wr = jiwr.getWorkRequirement();
			wr.setStatus(WorkrequirementStatus.COMPLETE);
			merge(wr);
		});
	}

	@Override
	public void checkCurrentWorkRequirement(JobItem ji) {
		checkWorkRequirement(ji.getNextWorkReq());
	}

	@Override
	public void checkWorkRequirement(JobItemWorkRequirement jiwr) {
		if (jiwr != null) {
			JobItem ji = jiwr.getJobitem();
			WorkRequirement wr = jiwr.getWorkRequirement();
			Integer completedId = wr.getId();
			// set the current req to complete
			wr.setStatus(WorkrequirementStatus.COMPLETE);
			
			this.eaService.completeEngineerAllocation(ji);

			// check job item work requirements
			for (JobItemWorkRequirement w : ji.getWorkRequirements()) {
				// if there is still an incomplete/non cancelled req that isn't the
				// one just updated
				if (!w.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE)
						&& !w.getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED)
						&& (w.getId() != completedId.intValue())) {
					// set it as the next active
					ji.setNextWorkReq(w);
					// update proc
					this.jiServ.updateProcIfNecessary(ji, w);
					// Break loop, such that we only update the first work
					// requirement found
					break;
				}
			}
		}
	}

	@Override
	public void cancelCurrentWorkRequirement(JobItem ji) {
		// if item has current req
		if (ji.getNextWorkReq() != null) {
			WorkRequirement currReq = ji.getNextWorkReq().getWorkRequirement();
			Integer completedId = currReq.getId();
			// set the current req to complete
			currReq.setStatus(WorkrequirementStatus.CANCELLED);

			// check job item work requirements
			for (JobItemWorkRequirement wr : ji.getWorkRequirements()) {
				// if there is still an incomplete/non cancelled req that isn't the
				// one just updated
				if (!wr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE)
						&& !wr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED)
						&& (wr.getId() != completedId.intValue())) {
					// set it as the next active
					ji.setNextWorkReq(wr);
					// update proc
					this.jiServ.updateProcIfNecessary(ji, wr);
					// Break loop, such that we only update the first work
					// requirement found
					break;
				}
			}
		}
	}

	/**
	 * Indicates whether it's allowed to copy the work requirements from this job
	 * item (need at least one not-yet-complete requirement to copy from)
	 * 
	 * @param copyFrom
	 */
	@Override
	public boolean canCopyWorkRequirements(JobItem sourceJobItem) {
		boolean result = false;
		if (sourceJobItem.getNextWorkReq() != null) {
			if (!sourceJobItem.getNextWorkReq().getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE)
					&& !sourceJobItem.getNextWorkReq().getWorkRequirement().getStatus()
							.equals(WorkrequirementStatus.CANCELLED)) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * Copies uncompleted work requirements from source to destination, replacing
	 * any uncompleted work requirements on destination item
	 * 
	 * Validation (see JIContractReviewValidator) ensures that there is (a) at least
	 * one uncompleted work requirement to copy, and (b) that the "next work
	 * requirement" has not yet been completed.
	 * 
	 * This simplifies the copy option, by allowing the "next work requirement" and
	 * "calibration address" to be simultaneously copied, rather than calculated.
	 */
	@Override
	public void copyWorkRequirements(JobItem sourceJobItem, JobItem destJobItem) {
		// Recheck validation requirements (in case program code used without
		// bean validator)
		if (!canCopyWorkRequirements(sourceJobItem))
			throw new IllegalArgumentException(
					"NextWorkRequirement on sourceJobItem needs to be not-yet-completed to allow copy");

		// Delete uncompleted work requirements on destination
		Collection<JobItemWorkRequirement> jiwrToDelete = destJobItem.getWorkRequirements().stream()
				.filter(jiwr -> !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE)
						&& !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED))
				.collect(Collectors.toList());
		Collection<WorkRequirement> wrToDelete = jiwrToDelete.stream().map(jiwr -> jiwr.getWorkRequirement())
				.collect(Collectors.toList());
		destJobItem.getWorkRequirements().removeAll(jiwrToDelete);
		// Delete cascades to the JobItemWorkRequirement
		for (WorkRequirement jiwr : wrToDelete)
			this.delete(jiwr);

		// Copy uncompleted work requirements from source to destination
		sourceJobItem.getWorkRequirements().stream().map(sourceJiwr -> sourceJiwr.getWorkRequirement())
				.filter(sourceWr -> !sourceWr.getStatus().equals(WorkrequirementStatus.COMPLETE)
						&& !sourceWr.getStatus().equals(WorkrequirementStatus.CANCELLED))
				.forEach(sourceWr -> {
                    WorkRequirement wr = new WorkRequirement();
                    wr.setAddress(sourceWr.getAddress());
                    wr.setStatus(sourceWr.getStatus());
                    wr.setDepartment(sourceWr.getDepartment());
                    wr.setCapability(sourceWr.getCapability());
                    wr.setRequirement(sourceWr.getRequirement());
                    wr.setServiceType(sourceWr.getServiceType());
                    wr.setType(sourceWr.getType());
                    wr.setWorkInstruction(sourceWr.getWorkInstruction());

                    // New WR and JIWR are saved via JPA cascade from JobItem
                    JobItemWorkRequirement jiwr = new JobItemWorkRequirement();
                    jiwr.setJobitem(destJobItem);
                    jiwr.setWorkRequirement(wr);
                    wr.getJobItemWorkRequirements().add(jiwr);
                    destJobItem.getWorkRequirements().add(jiwr);

                    // Check if new WorkRequirement is to be the "next" one, if
                    // so set on destination
                    if (sourceJobItem.getNextWorkReq().getWorkRequirement().getId().equals(sourceWr.getId())) {
                        destJobItem.setNextWorkReq(jiwr);
                    }
                });
        // Set next calibration address / location and next capability
        destJobItem.setCalAddr(sourceJobItem.getCalAddr());
        destJobItem.setCalLoc(sourceJobItem.getCalLoc());
        destJobItem.setCapability(sourceJobItem.getCapability());

    }

	/**
	 * Used by external controller(s) to create individual work requirements on
	 * demand
	 */
	@Override
	public WorkRequirement createWorkRequirement(int jobItemId, int procedureId, int workInstructionId,
			int serviceTypeId, String requirementText) {

		JobItem ji = jiServ.findJobItem(jobItemId);

		this.deleteDefaultWorkRequirement(ji);

		JobItemWorkRequirement jiwr = new JobItemWorkRequirement();
		WorkRequirement wr = new WorkRequirement();
		jiwr.setJobitem(ji);
		jiwr.setWorkRequirement(wr);
		wr.setRequirement(requirementText);
		wr.setServiceType(this.serviceTypeService.get(serviceTypeId));
		wr.setStatus(WorkrequirementStatus.PENDING);

		if (workInstructionId != 0) {
			WorkInstruction workInstruction = this.wiServ.get(workInstructionId);
			wr.setWorkInstruction(workInstruction);
			// TODO if reactivating work instructions, enforce consistency with
			// procedure
		}
		if (procedureId != 0) {
            Capability capability = procServ.get(procedureId);
            wr.setCapability(capability);
            logger.debug("Set proc id " + procedureId + " on wr id " + wr.getId());
        }
		updateDepartmentAndAddress(wr, ji);

		jobItemWorkRequirementService.save(jiwr);

		ji.getWorkRequirements().add(jiwr);

		// If job item does not have a next work requirement, set it.
		if (ji.getNextWorkReq() == null) {
			ji.setNextWorkReq(jiwr);
			// also update proc
			this.jiServ.updateProcIfNecessary(ji, jiwr);
		}
		return wr;
	}

	/**
	 * Delete default work requirement without procedure, In case there is (only?)
	 * one
	 */
	private void deleteDefaultWorkRequirement(JobItem ji) {

		if (ji.getWorkRequirements().size() == 1) {

			JobItemWorkRequirement jiwr = ji.getWorkRequirements().stream().filter(wr -> {
                return wr.getWorkRequirement() == null
                    || (wr.getWorkRequirement() != null && wr.getWorkRequirement().getCapability() == null);
            }).findAny().orElse(null);

			if (jiwr != null) {
				ji.setNextWorkReq(null);
				ji.getWorkRequirements().remove(jiwr);
				jiwr.setJobitem(null);
				this.jobItemWorkRequirementService.delete(jiwr);
			}
		}
	}

	@Override
	public void updateDepartmentAndAddress(WorkRequirement wr, JobItem ji) {
        // Case 1 - Site job, no dept, address = current address of instrument
        if (JobType.SITE.equals(ji.getJob().getType())) {
            wr.setAddress(ji.getCurrentAddr());
            wr.setDepartment(null);
            wr.setType(WorkRequirementType.ONSITE);
        }
        // Case 2 - (Trescal laboratory), the dept/address should come from the
        // Procedure
        else if (wr.getCapability() != null && !WorkRequirementType.THIRD_PARTY.equals(wr.getCapability().getReqType())) {
            Department departmentMatch = null;
            int currentAddressId = ji.getCurrentAddr() != null ? ji.getCurrentAddr().getAddrid() : -1;
            int defaultAddressId = wr.getCapability().getDefAddress().getAddrid();
            // We want to use a department within the current address, if
            // service is
            // available
            for (CategorisedCapability cp : wr.getCapability().getCategories()) {
                int categoryAddrId = cp.getCategory().getDepartment().getAddress().getAddrid().intValue();
                if ((categoryAddrId == currentAddressId)
                    || (departmentMatch == null && categoryAddrId == defaultAddressId)) {
                    departmentMatch = cp.getCategory().getDepartment();
                }
            }
            wr.setDepartment(departmentMatch);
            wr.setAddress(departmentMatch != null ? departmentMatch.getAddress() : wr.getCapability().getDefAddress());
            wr.setType(wr.getCapability().getReqType());
        }
        // Case 3 - send to third party
        // Use the defaultAddress only if it has been initialized properly, e.g.
        // actual
        // supplier address
        // As of 2018-08-01 Spain Third Party procedures have not been updated
        // yet
        else if (wr.getCapability() != null && WorkRequirementType.THIRD_PARTY.equals(wr.getCapability().getReqType())) {
            Address defaultAddress = wr.getCapability().getDefAddress();
            if (CompanyRole.SUPPLIER.equals(defaultAddress.getSub().getComp().getCompanyRole())) {
                wr.setAddress(defaultAddress);
            } else {
                wr.setAddress(null);
            }
            wr.setDepartment(null);
            wr.setType(WorkRequirementType.THIRD_PARTY);
        }

        // Case 4 - no procedure, clear fields, assume inhouse calibration to be
        // confirmed at contract review
        else {
            wr.setDepartment(null);
            wr.setAddress(null);
            wr.setType(WorkRequirementType.CALIBRATION);
        }
    }

	@Override
	public WorkRequirement updateWorkRequirement(int wrId, int procedureId, int workInstructionId, int serviceTypeId,
			String requirementText) {
		WorkRequirement wr = this.get(wrId);
		wr.setServiceType(this.serviceTypeService.get(serviceTypeId));
		wr.setRequirement(requirementText);

        if (workInstructionId != 0) {
            WorkInstruction workInstruction = this.wiServ.get(workInstructionId);
            wr.setWorkInstruction(workInstruction);
//			wr.setCapability(workInstruction.getCapability());
            // TODO if reactivating work instructions, enforce consistency with
            // procedure
        } else {
            wr.setWorkInstruction(null);
        }
        if (procedureId != 0) {
            Capability capability = procServ.get(procedureId);
            wr.setCapability(capability);
            logger.debug("Set proc id " + procedureId + " on wr id " + wr.getId());
        } else {
            wr.setCapability(null);
        }

		// if the work requirement is for a job item,
		if (!wr.getJobItemWorkRequirements().isEmpty()) {
			JobItemWorkRequirement jiwr = wr.getJobItemWorkRequirements().iterator().next();
			JobItem ji = jiwr.getJobitem();
			updateDepartmentAndAddress(wr, ji);
			// if this is the item's next work req being edited, also update
			// proc
			if (ji.getNextWorkReq().getWorkRequirement().getId() == wr.getId()) {
				this.jiServ.updateProcIfNecessary(ji, jiwr);
			}
		}
		return wr;
	}

	@Override
	public WorkRequirement duplicate(WorkRequirement wr) {

        Capability p = wr.getCapability();
        Address ad = wr.getAddress();
        Department d = wr.getDepartment();
        ServiceType st = wr.getServiceType();
//		WorkInstruction wi = wr.getWorkInstruction();

        workRequirementDao.detach(wr);
        wr.setJobItemWorkRequirements(new HashSet<>());
        wr.setId(null);
        wr.setCapability(p);
        wr.setAddress(ad);
        wr.setDepartment(d);
        wr.setServiceType(st);
//		wr.setWorkInstruction(wi);
        wr.setStatus(WorkrequirementStatus.PENDING);

        return merge(wr);
    }

}