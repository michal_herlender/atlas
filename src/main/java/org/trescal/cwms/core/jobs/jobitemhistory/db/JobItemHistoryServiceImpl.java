package org.trescal.cwms.core.jobs.jobitemhistory.db;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.jobitemhistory.JobItemHistory;

@Service("JobItemHistoryService")
public class JobItemHistoryServiceImpl extends BaseServiceImpl<JobItemHistory, Integer> implements JobItemHistoryService
{
	@Autowired
	private JobItemHistoryDao jobItemHistoryDao;

	@Override
	public JobItemHistory getJobItemHistoryByChangeDate(Date changeDate) {
		return jobItemHistoryDao.getJobItemHistoryByChangeDate(changeDate);
	}

	@Override
	protected BaseDao<JobItemHistory, Integer> getBaseDao() {
		return jobItemHistoryDao;
	}
}