package org.trescal.cwms.core.jobs.certificate.entity;

public enum CertificateReplacementType
{
	LAB_CERT, THIRDPARTY_CERT, ONSITE_CERT;
}