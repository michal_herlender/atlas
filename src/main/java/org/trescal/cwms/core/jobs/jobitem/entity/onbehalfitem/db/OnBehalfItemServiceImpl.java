package org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.db;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.form.OnBehalfForm;
import org.trescal.cwms.core.login.entity.user.db.UserService;

@Service("OnBehalfItemService")
public class OnBehalfItemServiceImpl extends BaseServiceImpl<OnBehalfItem, Integer> implements OnBehalfItemService {

	@Autowired
	private OnBehalfItemDao onBehalfItemDao;
	@Autowired
	private UserService userService;
	@Autowired
	private AddressService addServ;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private JobItemService jobItemServ;

	@Override
	protected BaseDao<OnBehalfItem, Integer> getBaseDao() {
		return onBehalfItemDao;
	}

	@Override
	public OnBehalfItem getOnBehalfItemByJobitem(JobItem ji) {
		return this.onBehalfItemDao.getOnBehalfItemByJobitem(ji);
	}

	@Override
	public void updateOnBehalfItem(OnBehalfForm form, String username) {
		Set<JobItem> jobItems = form.getJob().getItems();
		// iterate over the selected items and create new OnBehalf items for
		// each one
		Contact currentContact = this.userService.get(username).getCon();
		Address address = form.getAddrid() == null ? null : this.addServ.get(form.getAddrid());
		Company company = this.compServ.get(form.getCoid());
		for (JobItem jobItem : jobItems) {
			if (form.getJobitemids().get(jobItem.getJobItemId())) {
				OnBehalfItem oldOnBehalf = jobItem.getOnBehalf();
				// Can be removed, if we set orphanRemoval on
				// JobItem.getOnBehalf
				if (oldOnBehalf != null
						&& (company != oldOnBehalf.getCompany() || address != oldOnBehalf.getAddress())) {
					jobItem.setOnBehalf(null);
					this.delete(oldOnBehalf);
					jobItem = jobItemServ.mergeJobItem(jobItem);
				}
				if (this.getOnBehalfItemByJobitem(jobItem) == null)
					jobItemServ.linkOnBehalf(jobItem, currentContact, company, address);
			}
		}

	}

}