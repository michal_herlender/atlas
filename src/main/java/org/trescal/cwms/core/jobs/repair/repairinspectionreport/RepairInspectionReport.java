package org.trescal.cwms.core.jobs.repair.repairinspectionreport;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperationComparator;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

@Entity
@Table(name = "repairinspectionreport")
public class RepairInspectionReport extends Allocated<Subdiv> implements ComponentEntity {

	public final static String repairInspectionReportIdentifierPrefix = "Repair Inspection Report  - ";
	
	private int rirId;
	private String rirNumber;
	private String clientDescription;
	private String clientInstructions;
	private String internalDescription;
	private String internalInspection;
	private String internalOperations;
	private Boolean manufacturerWarranty;
	private Boolean trescalWarranty;
	private String internalInstructions;
	private String externalInstructions;
	private Integer leadTime;
	private Date estimatedReturnDate;
	private Boolean completedByTechnician;
	private Date completedByTechnicianOn;
	private Contact technician;
	private Boolean validatedByManager;
	private Date validatedByManagerOn;
	private Contact manager;
	private Boolean reviewedByCsr;
	private Date reviewedByCsrOn;
	private Contact csr;
	private Set<FreeRepairOperation> freeRepairOperations;
	private RepairCompletionReport repairCompletionReport;
	private JobItem ji;
	private Date creationDate;
	private Contact createdBy;
	private File directory;
	private List<Email> sentEmails;
	private Boolean misuse;
	private String misuseComment;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getRirId() {
		return rirId;
	}

	public void setRirId(int rirId) {
		this.rirId = rirId;
	}

	@Column(name = "completedbytechnicianon", columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCompletedByTechnicianOn() {
		return completedByTechnicianOn;
	}

	public void setCompletedByTechnicianOn(Date completedOn) {
		this.completedByTechnicianOn = completedOn;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "technician", foreignKey = @ForeignKey(name = "repairinspectionreport_contact_FK"))
	public Contact getTechnician() {
		return technician;
	}

	public void setTechnician(Contact validatedBy) {
		this.technician = validatedBy;
	}
	
	@Column(name = "completedbytechnician")
	public Boolean getCompletedByTechnician() {
		return completedByTechnician;
	}
	
	@Column(name = "validatedbymanager")
	public Boolean getValidatedByManager() {
		return validatedByManager;
	}

	public void setValidatedByManager(Boolean validatedByManager) {
		this.validatedByManager = validatedByManager;
	}

	@Column(name = "validatedbymanageron", columnDefinition = "datetime")
	public Date getValidatedByManagerOn() {
		return validatedByManagerOn;
	}

	public void setValidatedByManagerOn(Date validatedByManagerOn) {
		this.validatedByManagerOn = validatedByManagerOn;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "manager", foreignKey = @ForeignKey(name = "repairinspectionreport_contact_manager_FK"))
	public Contact getManager() {
		return manager;
	}

	public void setManager(Contact manager) {
		this.manager = manager;
	}
	
	@Column(name = "reviewedbycsr")
	public Boolean getReviewedByCsr() {
		return reviewedByCsr;
	}

	public void setReviewedByCsr(Boolean reviewedByCsr) {
		this.reviewedByCsr = reviewedByCsr;
	}

	@Column(name = "reviewedbycsron", columnDefinition = "datetime")
	public Date getReviewedByCsrOn() {
		return reviewedByCsrOn;
	}

	public void setReviewedByCsrOn(Date reviewedByCsrOn) {
		this.reviewedByCsrOn = reviewedByCsrOn;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "csr", foreignKey = @ForeignKey(name = "repairinspectionreport_contact_csr_FK"))
	public Contact getCsr() {
		return csr;
	}

	public void setCsr(Contact csr) {
		this.csr = csr;
	}

	public void setCompletedByTechnician(Boolean completed) {
		this.completedByTechnician = completed;
	}

	@SortComparator(FreeRepairOperationComparator.class)
	@OneToMany(mappedBy = "repairInspectionReport", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	public Set<FreeRepairOperation> getFreeRepairOperations() {
		return freeRepairOperations;
	}

	public void setFreeRepairOperations(Set<FreeRepairOperation> freeRepairOperations) {
		this.freeRepairOperations = freeRepairOperations;
	}

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "repaircompletionreportid", foreignKey = @ForeignKey(name = "fk_rir_rcr"))
	public RepairCompletionReport getRepairCompletionReport() {
		return repairCompletionReport;
	}

	public void setRepairCompletionReport(RepairCompletionReport repairCompletionReport) {
		this.repairCompletionReport = repairCompletionReport;
	}

	@ManyToOne
	@JoinColumn(name = "jobitemid", foreignKey = @ForeignKey(name = "repairinspectionreport_jobitem_FK"))
	public JobItem getJi() {
		return ji;
	}

	public void setJi(JobItem ji) {
		this.ji = ji;
	}

	@Column(name = "clientdescription")
	public String getClientDescription() {
		return clientDescription;
	}

	public void setClientDescription(String clientDescription) {
		this.clientDescription = clientDescription;
	}

	@Column(name = "clientinstructions")
	public String getClientInstructions() {
		return clientInstructions;
	}

	public void setClientInstructions(String clientInstructions) {
		this.clientInstructions = clientInstructions;
	}

	@Column(name = "internaldescription")
	public String getInternalDescription() {
		return internalDescription;
	}

	public void setInternalDescription(String internalDescription) {
		this.internalDescription = internalDescription;
	}

	@Column(name = "internalinspection")
	public String getInternalInspection() {
		return internalInspection;
	}

	public void setInternalInspection(String internalInspection) {
		this.internalInspection = internalInspection;
	}

	@Column(name = "internaloperations")
	public String getInternalOperations() {
		return internalOperations;
	}

	public void setInternalOperations(String internalOperations) {
		this.internalOperations = internalOperations;
	}

	@Column(name = "manufacturerwarranty")
	public Boolean getManufacturerWarranty() {
		return manufacturerWarranty;
	}

	public void setManufacturerWarranty(Boolean manufacturerWarranty) {
		this.manufacturerWarranty = manufacturerWarranty;
	}

	@Column(name = "trescalwarranty")
	public Boolean getTrescalWarranty() {
		return trescalWarranty;
	}

	public void setTrescalWarranty(Boolean trescalWarranty) {
		this.trescalWarranty = trescalWarranty;
	}

	@Column(name = "internalinstructions")
	public String getInternalInstructions() {
		return internalInstructions;
	}

	public void setInternalInstructions(String internalInstructions) {
		this.internalInstructions = internalInstructions;
	}

	@Column(name = "externalinstructions")
	public String getExternalInstructions() {
		return externalInstructions;
	}

	public void setExternalInstructions(String externalInstructions) {
		this.externalInstructions = externalInstructions;
	}

	@Column(name = "leadtime")
	public Integer getLeadTime() {
		return leadTime;
	}

	public void setLeadTime(Integer leadTime) {
		this.leadTime = leadTime;
	}

	@Column(name = "estimatedreturndate", columnDefinition = "datetime2")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEstimatedReturnDate() {
		return estimatedReturnDate;
	}

	public void setEstimatedReturnDate(Date estimatedReturnDate) {
		this.estimatedReturnDate = estimatedReturnDate;
	}

	@Length(max = 30)
	@Column(name = "rirnumber", nullable = true, columnDefinition = "varchar(30)")
	public String getRirNumber() {
		return rirNumber;
	}
	
	public void setRirNumber(String rirNumber) {
		this.rirNumber = rirNumber;
	}

	@Column(name="creationdate", nullable=false, columnDefinition="datetime2")
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "createdby", foreignKey = @ForeignKey(name = "repairinspectionreport_contact_createdby_FK"))
	public Contact getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Contact createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "misuse")
	public Boolean getMisuse() {
		return misuse;
	}

	public void setMisuse(Boolean misuse) {
		this.misuse = misuse;
	}

	@Column(name = "misusecomment")
	public String getMisuseComment() {
		return misuseComment;
	}

	public void setMisuseComment(String misuseComment) {
		this.misuseComment = misuseComment;
	}

	@Override
	@Transient
	public Contact getDefaultContact() {
		// TODO Auto-generated method stub
		return this.getJi().getJob().getCon();
	}

	@Override
	@Transient
	public File getDirectory() {
		return this.directory;
	}

	@Override
	@Transient
	public String getIdentifier() {
		return RepairInspectionReport.repairInspectionReportIdentifierPrefix + this.getRirNumber();
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity() {
		return this.getJi().getJob();
	}

	@Override
	@Transient
	public List<Email> getSentEmails() {
		return this.sentEmails;
	}

	@Override
	@Transient
	public boolean isAccountsEmail() {
		return false;
	}

	@Override
	public void setDirectory(File file) {
		this.directory = file;
	}

	@Override
	public void setSentEmails(List<Email> emails) {
		this.sentEmails = emails;
	}
	
}