package org.trescal.cwms.core.jobs.repair.operation.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;

public interface FreeRepairOperationDao extends BaseDao<FreeRepairOperation, Integer> {

	void refresh(FreeRepairOperation fro);
	
}