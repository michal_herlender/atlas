package org.trescal.cwms.core.jobs.calibration.entity.callink.db;

import java.util.List;

import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;

public interface CalLinkService
{
	/**
	 * Deletes the given {@link CalLink} from the database.
	 * 
	 * @param callink the {@link CalLink} to delete.
	 */
	void deleteCalLink(CalLink callink);

	/**
	 * Returns the {@link CalLink} with the given ID.
	 * 
	 * @param id the {@link CalLink} ID.
	 * @return the {@link CalLink}
	 */
	CalLink findCalLink(int id);

	/**
	 * Returns a {@link List} of all {@link CalLink}s in the database.
	 * 
	 * @return the {@link List} of {@link CalLink}s.
	 */
	List<CalLink> getAllCalLinks();

	/**
	 * Inserts the given {@link CalLink} into the database.
	 * 
	 * @param callink the {@link CalLink} to insert.
	 */
	void insertCalLink(CalLink callink);

	/**
	 * Updates the given {@link CalLink} in the database if it already exists,
	 * otherwise inserts it.
	 * 
	 * @param callink the {@link CalLink} to update or insert.
	 */
	void saveOrUpdateCalLink(CalLink callink);

	/**
	 * Updates the given {@link CalLink} in the database.
	 * 
	 * @param callink the {@link CalLink} to update.
	 */
	void updateCalLink(CalLink callink);
}