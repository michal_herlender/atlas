package org.trescal.cwms.core.jobs.job.controller;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.login.entity.userpreferences.db.UserPreferencesService;
import org.trescal.cwms.core.system.Constants;

@Controller @JsonController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME})
public class JobPreferencesController {
	
	@Autowired
	private UserPreferencesService userPreferencesService;
	
	@RequestMapping(value="updateDefaultServiceTypeUsage.json", method = RequestMethod.POST)
	@ResponseBody
	public void update(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
					   @RequestBody UsageIn in) {
		this.userPreferencesService.updateDefaultServiceTypeUsage(username, in.getUsage());
	}
}

@Data
class UsageIn{
	private Boolean usage;
}
