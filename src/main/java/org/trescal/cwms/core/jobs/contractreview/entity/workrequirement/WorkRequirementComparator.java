package org.trescal.cwms.core.jobs.contractreview.entity.workrequirement;

import java.util.Comparator;

public class WorkRequirementComparator implements Comparator<WorkRequirement> {
	@Override
	public int compare(WorkRequirement o1, WorkRequirement o2) {
		return o1.getId().compareTo(o2.getId());
	}
}
