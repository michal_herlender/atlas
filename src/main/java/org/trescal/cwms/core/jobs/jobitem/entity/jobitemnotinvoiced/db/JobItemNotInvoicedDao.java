package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemNotInvoicedDTO;

public interface JobItemNotInvoicedDao extends BaseDao<JobItemNotInvoiced, Integer> {
	List<JobItemNotInvoicedDTO> getProjectionDTOs(Collection<Integer> invoiceIds);
}
