package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso;

import lombok.Setter;
import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocumentComparator;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLinkComparator;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsostatus.GeneralServiceOperationStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "generalserviceoperation")
@Setter
public class GeneralServiceOperation extends Allocated<Subdiv> {

    private int id;
    private Contact startedBy;
    private Date startedOn;
    private Contact completedBy;
    private Date completedOn;
    private GeneralServiceOperationStatus status;
    private Set<GsoLink> links;
    private Set<GsoDocument> gsoDocuments;
    private CalibrationType calType;
    private Capability capability;
    private Address address;
    private Boolean clientDecisionNeeded;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "startedby")
	public Contact getStartedBy() {
		return this.startedBy;
	}

	@Column(name = "startedon")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getStartedOn() {
		return this.startedOn;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "completedby")
	public Contact getCompletedBy() {
		return this.completedBy;
	}

	@Column(name = "completedon")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCompletedOn() {
		return this.completedOn;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "status")
	public GeneralServiceOperationStatus getStatus() {
	  return this.status;
	}
	
	@OneToMany(mappedBy = "gso", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(GsoLinkComparator.class)
	public Set<GsoLink> getLinks() {
		return this.links;
	}

	public ActionOutcome getJobItemOutcome(JobItem ji) {
		return this.links.stream().filter(l -> l.getOutcome() != null && ji.equals(l.getJi())).map(GsoLink::getOutcome).findAny().orElse(null);
	}

	public GsoDocument getJobItemGsoDocument(JobItem ji) {
		return this.gsoDocuments.stream().filter(d -> d.getLinks().stream().anyMatch(l -> l.getJi().equals(ji))).findFirst().orElse(null);
	}
	
	@OneToMany(mappedBy = "gso", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @SortComparator(GsoDocumentComparator.class)
    public Set<GsoDocument> getGsoDocuments() {
        return this.gsoDocuments;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "caltypeid")
    public CalibrationType getCalType() {
        return calType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "capabilityId")
    public Capability getCapability() {
        return capability;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "addressid")
    public Address getAddress() {
        return address;
    }

    @Column(name = "clientdecisionneeded")
    public Boolean getClientDecisionNeeded() {
        return clientDecisionNeeded;
	}
	
}