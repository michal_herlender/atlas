package org.trescal.cwms.core.jobs.jobitem.entity.accessory.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.GroupAccessory;

@Service("groupAccessoryService")
public class GroupAccessoryServiceImpl extends BaseServiceImpl<GroupAccessory, Integer>
		implements GroupAccessoryService {

	@Autowired
	private GroupAccessoryDao groupAccessoryDao;

	@Override
	protected BaseDao<GroupAccessory, Integer> getBaseDao() {
		return groupAccessoryDao;
	}

	@Override
	public Integer countByJob(Integer jobId) {
		return this.groupAccessoryDao.countByJob(jobId) + this.groupAccessoryDao.countFreeTextAccessoriesByJob(jobId);
	}
}