package org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.failurereport.dto.FailureReportDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;

public interface FaultReportDao extends BaseDao<FaultReport, Integer> {
	
	List<FaultReport> getFaultReportListByJobitemId(Integer jobItemId);

	FaultReport getLastFailureReportByJobitemId(Integer jobitemId, Boolean valid);

	List<FailureReportDTO> getAllFailureReportsByJobitemId(Integer jobitemId);
	
	FaultReport getFirstFailureReportByJobitemIdForDelivery(Integer jobitemId);
}