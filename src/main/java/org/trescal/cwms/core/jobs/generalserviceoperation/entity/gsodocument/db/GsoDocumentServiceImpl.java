package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;
import org.trescal.cwms.core.jobs.generalserviceoperation.form.GeneralServiceOperationForm;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tools.fileupload2.service.UploadService;

import java.io.File;
import java.io.IOException;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDateTime;

@Service("GsoDocumentService")
public class GsoDocumentServiceImpl extends BaseServiceImpl<GsoDocument, Integer> implements GsoDocumentService {
	@Autowired
	private GsoDocumentDao gsoDocumentDao;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private SystemComponentService scServ;
	@Autowired
	private UploadService uploadService;

	protected BaseDao<GsoDocument, Integer> getBaseDao() {
		return this.gsoDocumentDao;
	}

	@Override
	public void updateGsoDocument(GeneralServiceOperationForm form) {
		GsoDocument gsoDocument = this.get(form.getDocumentId());
	
		String fileName = gsoDocument.getDocumentNumber().concat(".pdf");
		String newFileName = form.getDocumentNumber().concat(".pdf");
		gsoDocument.setDirectory(this.compDirServ.getDirectory(this.scServ.findComponent(Component.GENERAL_SERVICE_OPERATION),
			form.getJi().getJob().getJobno()));
		File file = new File(gsoDocument.getDirectory().getAbsolutePath().concat(File.separator).concat(fileName));
		File newFile = new File(gsoDocument.getDirectory().getAbsolutePath().concat(File.separator).concat(newFileName));
		if (file.exists()) {
			if (form.getFile() != null && !form.getFile().isEmpty()) {
				if (file.delete()) {
					try {
						this.uploadService.saveFile(form.getFile().getInputStream(), gsoDocument.getDirectory(), newFileName);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else if (!fileName.equals(newFileName))
				file.renameTo(newFile);


		}
		gsoDocument.setDocumentDate(dateFromLocalDateTime(form.getDocumentDate()));
		//	gsoDocument.setDocumentNumber(form.getDocumentNumber());
		this.gsoDocumentDao.merge(gsoDocument);
	}
	
	@Override
	public GsoDocument findGsoDocumentByDocumentNumber(String documentNumber) {
		return this.gsoDocumentDao.findGsoDocumentByDocumentNumber(documentNumber);
	}
}