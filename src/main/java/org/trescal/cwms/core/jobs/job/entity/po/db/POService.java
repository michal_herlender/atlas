package org.trescal.cwms.core.jobs.job.entity.po.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.dto.ClientPurchaseOrderDTO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.projection.POProjectionDTO;
import org.trescal.cwms.core.logistics.dto.PoDTO;
import org.trescal.cwms.core.logistics.entity.asn.Asn;

import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.List;

/**
 * Interface for accessing and manipulating {@link PO} entities.
 */
public interface POService extends BaseService<PO, Integer> {

	void addPOsToJob(List<Integer> poIds, List<String> poNumbers, List<String> poComments, Job job, int defaultPO);

	PO createPO(Integer jobId, String poNumber, String comment, Integer allocatedCompanyId);
	
	/**
	 * this method deactivates a {@link PO}
	 * 
	 * @param poid the {@link PO} id.
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper deactivatePOById(int poid, HttpSession session);

	void deactivatePO(Integer poId, String userName);

	/**
	 * Deletes the {@link PO} with the given ID from the database.
	 * 
	 * @param poid the ID of the {@link PO} to delete.
	 */
	void deletePOById(int poid);

	/**
	 * Updates the 'poNumber' and 'comment' fields of the {@link PO} with the given
	 * ID.
	 * 
	 * @param poid     the ID of the {@link PO} to update.
	 * @param ponumber the new value for the poNumber field.
	 * @param comment  the new value for the comment field.
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper editPO(int jobid, int poid, Integer[] poJIs, String ponumber, String comment);

	/**
	 * Updates the {@link Job} identified by the given jobid to have the {@link PO}
	 * identified by the given poid as its default {@link PO}.
	 * 
	 * @param jobid the {@link Job} ID, not null.
	 * @param poid  the {@link PO} ID, not null.
	 */
	void updateJobPODefault(int jobid, int poid);

	List<PoDTO> getPOsByPrebooking(Integer prebookingdetailsid);

	void addPOsToPrebookingJob(List<String> poNumbers, List<String> poComments, Asn prebookingJob, int defaultPO,
			Company businessCompany);

	List<ClientPurchaseOrderDTO> getProjectionByJob(Integer jobId);

	List<POProjectionDTO> getPOsByJobIds(Collection<Integer> jobIds);

	List<PO> searchMatchingJobPOs(int jobId, String partPoNumber);

	PO getMatchingJobPO(int jobId, String poNumber);
}