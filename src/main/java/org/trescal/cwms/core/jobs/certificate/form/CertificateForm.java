package org.trescal.cwms.core.jobs.certificate.form;

import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateClass;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Getter @Setter
public class CertificateForm
{
	private Integer certId;
	private Integer calProcessId;
	@NotNull
	private Date calDate;
	@NotNull
	private Date certDate;
	@Min(0)
	private Integer duration;
	// For third party cert
	private Integer subdivid;
	private Integer coid;
	private String thirdCertNo;
	private IntervalUnit durationUnit;
	private CalibrationVerificationStatus calibrationVerificationStatus;
	private String remarks;
    private BigDecimal deviation;
    private CertificateClass certificateClass;
    private String deviationUnitsSymbol;
    private String acceptanceCriteria;

}
