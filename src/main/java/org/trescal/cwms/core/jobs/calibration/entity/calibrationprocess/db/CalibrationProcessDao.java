package org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationProcessKeyValue;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;

public interface CalibrationProcessDao extends BaseDao<CalibrationProcess, Integer> {

	CalibrationProcess findByName(String name);
	
	List<CalibrationProcessKeyValue> findAllKeyValue();
}