package org.trescal.cwms.core.jobs.jobitem.form.genericentityvalidator;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.core.validation.AbstractEntityValidator;

/**
 * Generic validator for validating {@link JobItemActivity} entities. Extends
 * {@link AbstractEntityValidator} to provide support for dwr based validation
 * which requires access to the resulting {@link BindException}.
 * 
 * @author jamiev
 */
public class JobItemActivityValidator extends AbstractBeanValidator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(JobItemActivity.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		JobItemActivity jia = (JobItemActivity) target;

		super.validate(jia,errors);

		// do the business validation
		if (jia.getTimeSpent() == -1)
		{
			errors.rejectValue("timeSpent", null, "Time Spent must be an integer.");
		}

	}
}
