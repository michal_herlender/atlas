package org.trescal.cwms.core.jobs.jobitem.form;

import java.util.Map;

import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class OnBehalfForm
{
	private Job job;
	private Map<Integer,Boolean> jobitemids;
	private Integer coid;
	private Integer addrid;
	private JobItem srcJobitem;

}