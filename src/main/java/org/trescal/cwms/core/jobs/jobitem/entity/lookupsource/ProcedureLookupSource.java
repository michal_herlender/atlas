package org.trescal.cwms.core.jobs.jobitem.entity.lookupsource;

public enum ProcedureLookupSource
{
	INSTRUMENT, JOB;
}
