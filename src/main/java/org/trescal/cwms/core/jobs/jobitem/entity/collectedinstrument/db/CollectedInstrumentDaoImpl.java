package org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.db;

import lombok.val;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.jobs.jobitem.dto.CollectedInstrumentDto;
import org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.CollectedInstrument;
import org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.CollectedInstrument_;

import javax.persistence.criteria.JoinType;
import java.util.List;

@Repository("CollectedInstrumentDao")
public class CollectedInstrumentDaoImpl extends BaseDaoImpl<CollectedInstrument, Integer> implements CollectedInstrumentDao {
	
	@Override
	protected Class<CollectedInstrument> getEntity() {
		return CollectedInstrument.class;
	}
	
	@Override
	public List<CollectedInstrument> findAll() {
		return getResultList(cb -> {
			val cq = cb.createQuery(CollectedInstrument.class);
			val ci = cq.from(CollectedInstrument.class);
			val instrument = ci.join(CollectedInstrument_.inst);
			val comp = instrument.join(Instrument_.con).join(Contact_.sub).join(Subdiv_.comp);
			cq.orderBy(cb.asc(comp.get(Company_.coname)), cb.asc(instrument.get(Instrument_.plantid)));
			return cq;
		});
	}

	public List<CollectedInstrument> getCollectedInstrumentsForCompany(int coid) {
		return getResultList(cb -> {
			val cq = cb.createQuery(CollectedInstrument.class);
			val collectedInstrument = cq.from(CollectedInstrument.class);
			val instrument = collectedInstrument.join(CollectedInstrument_.inst, JoinType.LEFT);
			val model = collectedInstrument.fetch(CollectedInstrument_.inst, JoinType.LEFT).fetch(Instrument_.model);
			val comp = instrument.join(Instrument_.con).join(Contact_.sub).join(Subdiv_.comp);
			model.fetch(InstrumentModel_.mfr);
			model.fetch(InstrumentModel_.description);
			cq.where(cb.equal(comp, coid));
			cq.orderBy(cb.asc(instrument.get(Instrument_.plantid)));
			return cq;
		});
	}

	@Override
	public List<CollectedInstrumentDto> getCollectedInstrumentsDtoForCompany(Integer coid) {
		return getResultList(cb -> {
			val cq = cb.createQuery(CollectedInstrumentDto.class);
			val collectedInstrument = cq.from(CollectedInstrument.class);
			val instrument = collectedInstrument.join(CollectedInstrument_.inst);
			val model = instrument.join(Instrument_.model);
			val comp = instrument.join(Instrument_.con).join(Contact_.sub).join(Subdiv_.comp);
			model.join(InstrumentModel_.mfr);
			model.join(InstrumentModel_.description);
			cq.where(cb.equal(comp, coid));
			cq.orderBy(cb.asc(instrument.get(Instrument_.plantid)));
			cq.select(cb.construct(CollectedInstrumentDto.class,
					collectedInstrument.get(CollectedInstrument_.id),
					instrument.get(Instrument_.plantid),
					model.join(InstrumentModel_.mfr).get(Mfr_.name),
					model.get(InstrumentModel_.model),
					model.join(InstrumentModel_.description).get(Description_.description))
			);
			return cq;
		});
	}
}