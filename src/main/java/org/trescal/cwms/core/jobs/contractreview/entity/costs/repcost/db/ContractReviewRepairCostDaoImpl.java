package org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost;

@Repository("ContractReviewRepairCostDao")
public class ContractReviewRepairCostDaoImpl extends BaseDaoImpl<ContractReviewRepairCost, Integer> implements ContractReviewRepairCostDao {
	
	@Override
	protected Class<ContractReviewRepairCost> getEntity() {
		return ContractReviewRepairCost.class;
	}
}