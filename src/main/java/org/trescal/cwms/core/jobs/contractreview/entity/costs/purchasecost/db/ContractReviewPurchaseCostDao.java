package org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost;

public interface ContractReviewPurchaseCostDao extends BaseDao<ContractReviewPurchaseCost, Integer> {}