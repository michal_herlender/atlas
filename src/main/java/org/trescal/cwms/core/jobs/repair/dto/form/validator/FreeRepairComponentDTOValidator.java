package org.trescal.cwms.core.jobs.repair.dto.form.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.repair.dto.FreeRepairComponentDTO;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class FreeRepairComponentDTOValidator extends AbstractBeanValidator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(FreeRepairComponentDTO.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);

		FreeRepairComponentDTO dto = (FreeRepairComponentDTO) target;

		if (StringUtils.isBlank(dto.getName())) {
			errors.rejectValue("name", "repairinspection.validator.fieldmandatory", "Mandatory field");
		}

		if (dto.getQuantity() == null)
			errors.rejectValue("quantity", "repairinspection.validator.fieldmandatory", "Mandatory field");

		if(dto.getStatus() == null)
			errors.rejectValue("status", "repairinspection.validator.fieldmandatory", "Mandatory field");
//		else if((!dto.getAddedAfterRiRValidation() && !dto.getStatus().isRirStatus()) ||
//				(dto.getAddedAfterRiRValidation() && dto.getStatus().isRirStatus()))
//			errors.rejectValue("status", "repairinspection.validator.invalidstatus", "Invalid status");
	}

}
