package org.trescal.cwms.core.jobs.jobitem.projection.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db.JobItemWorkRequirementService;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemViewWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemProjectionCommand.JI_LOAD_INSTRUMENTS;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

/**
 * Used by job item controllers 
 */
@Service
public class JobItemViewServiceImpl implements JobItemViewService {
	
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private JobItemProjectionService jipService;
	@Autowired
	private JobItemWorkRequirementService jiwrService;

	@Override
	public JobItemViewWrapper getViewWrapper(Integer jobItemId, Locale locale) {
		JobItemViewWrapper wrapper = new JobItemViewWrapper();
		JobItem jobItem = this.jobItemService.getForView(jobItemId);
		wrapper.setJobItem(jobItem);
//		List<JobItemDTO> itemsOnJob = jobItemDao.getJobItemDTOs(jobItem.getJob().getJobid(), locale);
//		wrapper.setItemsOnJob(itemsOnJob);
		// For now, we get these from job - in future use the job projection
		Integer jobId = jobItem.getJob().getJobid();
		Integer allocatedCompanyId = jobItem.getJob().getOrganisation().getComp().getId();
		JobItemProjectionCommand jipCommand = new JobItemProjectionCommand(locale, allocatedCompanyId, 
				JI_LOAD_INSTRUMENTS.class);
		List<JobItemProjectionDTO> itemsOnJob = this.jipService.getProjectionDTOsForJobIds(Collections.singletonList(jobId), jipCommand);
		wrapper.setItemsOnJob(itemsOnJob);
		wrapper.setPreviousItemId(getPreviousItemId(itemsOnJob, jobItem.getItemNo()));
		wrapper.setNextItemId(getNextItemId(itemsOnJob, jobItem.getItemNo()));
		wrapper.setWorkRequirements(this.jiwrService.getForJobItemView(jobItem));
		wrapper.setGenericModel(jobItem.getInst().getModel().getModelMfrType().equals(ModelMfrType.MFR_GENERIC));
		List<StateGroup> stateGroups = jobItem.getState().getGroupLinks().stream().map(StateGroupLink::getGroup)
				.collect(Collectors.toList());
		wrapper.setReadyForClientDelivery(stateGroups.contains(StateGroup.CLIENTDELIVERY));
		wrapper.setReadyForInternalDelivery(stateGroups.contains(StateGroup.INTERNALDELIVERY));
		wrapper.setReadyForInternalReturnDelivery(stateGroups.contains(StateGroup.INTERNAL_RETURN_DELIVERY));
		wrapper.setReadyForThirdPartyDelivery(stateGroups.contains(StateGroup.TPDELIVERY));
		wrapper.setAwaitingInternalPO(stateGroups.contains(StateGroup.AWAITINGINTERNALPO));
		wrapper.setReadyForCalibration(stateGroups.contains(StateGroup.AWAITINGCALIBRATION));
		wrapper.setReadyToResumeCalibration(stateGroups.contains(StateGroup.RESUMECALIBRATION));
		wrapper.setReadyForGeneralServiceOperation(stateGroups.contains(StateGroup.AWAITINGGENERALSERVICEOPERATION));
		wrapper.setReadyToResumeGeneralServiceOperation(stateGroups.contains(StateGroup.RESUMEGENERALSERVICEOPERATION));
		wrapper.setReadyForGeneralServiceOperationDocumentUpload(
				stateGroups.contains(StateGroup.AWAITING_GSO_DOCUMENT_UPLOAD));
		wrapper.setAwaitingClientReceiptConfirmation(stateGroups.contains(StateGroup.AWAITING_CLIENT_RECEIPT_CONFIRMATION));
		// Unnecessary???
//		translationService.getCorrectTranslation(jobItem.getInst().getModel().getNameTranslations(), locale);
//		translationService.getCorrectTranslation(jobItem.getState().getTranslations(), locale);
//		translationService.getCorrectTranslation(jobItem.getCalType().getServiceType().getShortnameTranslation(),
//				locale);
		return wrapper;
	}
	
	private Integer getPreviousItemId(List<JobItemProjectionDTO> itemsOnJob, int currentItemNo) {
		JobItemProjectionDTO previousJobItem = itemsOnJob.stream().filter(item -> item.getItemno() < currentItemNo)
				.max(Comparator.comparingInt(JobItemProjectionDTO::getItemno)).orElse(null);
		return previousJobItem == null ? null : previousJobItem.getJobItemId();
	}

	private Integer getNextItemId(List<JobItemProjectionDTO> itemsOnJob, int currentItemNo) {
		JobItemProjectionDTO nextJobItem = itemsOnJob.stream().filter(item -> item.getItemno() > currentItemNo)
				.min(Comparator.comparingInt(JobItemProjectionDTO::getItemno)).orElse(null);
		return nextJobItem == null ? null : nextJobItem.getJobItemId();
	}
}
