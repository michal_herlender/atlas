package org.trescal.cwms.core.jobs.job.entity.poorigin;

import lombok.NoArgsConstructor;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("copy_cancel")
@NoArgsConstructor
public class POOriginCancelCopy extends POOrigin<PurchaseOrder> {


    @Override
    @Transient
    public KindName getKindName() {
        return KindName.COPY_CANCEL;
    }

    public POOriginCancelCopy(PurchaseOrder kind) {
        super(kind);
        setPurchaseOrder(kind);
    }

}
