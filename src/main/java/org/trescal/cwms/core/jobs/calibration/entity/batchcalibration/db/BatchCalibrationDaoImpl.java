package org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.db;

import java.util.Arrays;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Repository("BatchCalibrationDao")
public class BatchCalibrationDaoImpl extends BaseDaoImpl<BatchCalibration, Integer> implements BatchCalibrationDao {
	
	@Override
	protected Class<BatchCalibration> getEntity() {
		return BatchCalibration.class;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BatchCalibration> getBatchCalibrationsWithIds(List<Integer> batchIds) {
		Criteria crit = this.getSession().createCriteria(BatchCalibration.class);
		crit.add(Restrictions.in("id", batchIds));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<JobItem> getDistinctItemsFromBatches(List<Integer> batchIds) {
		Criteria crit = this.getSession().createCriteria(JobItem.class);
		crit.createCriteria("calLinks").createCriteria("cal").createCriteria("batch").add(Restrictions.in("id", batchIds));
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<BatchCalibration> getOtherCompleteBatchesOnJob(int jobId, int batchId) {
		// get all other batches on job
		Criteria crit = this.getSession().createCriteria(BatchCalibration.class);
		crit.createCriteria("job").add(Restrictions.idEq(jobId));
		crit.add(Restrictions.ne("id", batchId));
		List<BatchCalibration> otherBatchesOnJob = crit.list();
		// get other batches on job that have incomplete cals
		crit = this.getSession().createCriteria(BatchCalibration.class);
		crit.createCriteria("job").add(Restrictions.idEq(jobId));
		crit.add(Restrictions.ne("id", batchId));
		crit.createCriteria("cals").createCriteria("status").add(Restrictions.in("name", Arrays.asList("Awaiting work", "On-going")));
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<BatchCalibration> incompleteBatches = crit.list();
		// remove the incomplete batches from the total list
		otherBatchesOnJob.removeAll(incompleteBatches);
		return otherBatchesOnJob;
	}
}