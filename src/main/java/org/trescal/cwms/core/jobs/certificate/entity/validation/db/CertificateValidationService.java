package org.trescal.cwms.core.jobs.certificate.entity.validation.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidation;

public interface CertificateValidationService extends BaseService<CertificateValidation,Integer> {

}
