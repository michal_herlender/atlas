package org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemForInstrumentTooltipDto;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobSortKey;
import org.trescal.cwms.core.jobs.jobitem.dto.ItemAtTP;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemFeedbackDto;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemForProspectiveInvoiceDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemInvoiceDto;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemOnClientPurchaseOrderDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemPlantillasDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.db.AbstractJobItemDao;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItemImportInfoDto;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.reports.dto.ItemsAtTPWrapper;
import org.trescal.cwms.core.vdi.projection.PositionProjection;
import org.trescal.cwms.core.workallocation.engineerallocation.form.AllocateWorkToEngineerSearchForm;
import org.trescal.cwms.core.workflow.dto.SelectedJobItemDTO;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;
import org.trescal.cwms.rest.tlm.dto.TLMJobItemDTO;

public interface JobItemDao extends AbstractJobItemDao<JobItem> {

	List<JobItemInvoiceDto> findAllCompleteJobItemsWithOnlyProformaInvoicesProjection(Integer allocatedCompanyId,
			Integer allocatedSubdivId, Company company, JobSortKey sortBy, String completeJob);

	List<JobItemInvoiceDto> findAllCompleteJobItemsWithoutInvoicesProjection(Integer allocatedCompanyId,
			Integer allocatedSubdivId, Company company, JobSortKey sortBy, String completeJob);

	JobItem findActiveJobItemFromBarcode(int plantid);

	JobItem findEagerJobItem(int jobItemId);

	JobItem findEagerPopulatedJobitem(int jobitemid);

	List<JobItem> findMostRecentJobItems(Integer plantid, Integer maxResults);

	JobItem findMostRecentJobItem(int plantid);
	
	JobItem findMostRecentActiveJobItem(int plantid);

	List<SelectedJobItemDTO> getActiveJobItems(List<Integer> procIds, List<Integer> stateIds,
			Boolean isAllocatedToEngineer, Subdiv subdiv, StateGroupLinkType sglType, Integer addressId);

	List<JobItem> getAllItems(List<Integer> ids);

	List<JobItem> getAllByState(ItemState state, Subdiv subdiv);

	List<JobItemOnClientPurchaseOrderDTO> getJobItemsOnClientPO(Integer poId, Integer jobId, Locale locale);

	List<JobItem> getAllJobItemsDWR(int jobid);

	List<JobItem> getAvailableTaskList(Contact contact);

	List<JobItem> getContactItemsOnJobInStateGroup(int personId, StateGroup group);

	Integer getCountActiveJobItemsWithProcs(List<Integer> procIds, Integer days, Boolean includeBusiness,
			List<StateGroup> stateGroups, Subdiv subdiv);

	Long getCountAllJobItemsForState(ItemState state, Subdiv subdiv, StateGroupLinkType groupType);

	Integer getCountJobItemsByDays(String days, int subdivId);

	Long getCountJobItemsForPlantid(Integer plantid);

	Long getCountJobItems(int jobid);

	List<ItemsAtTPWrapper> getItemsAtThirdParty(Subdiv subdiv);

	List<JobItem> getItemsOnJobInStateGroup(int jobId, StateGroup group);

	List<JobItem> getItemsRequiringChase(StateGroup group);

	List<JobItem> getJobItemsByIds(List<Integer> jobitemids);

	List<JobItem> getJobItemsByStatusAndProc(int jobid, int statusid, int procid, Integer excludejiid);

	List<JobItemProjectionDTO> getJobItemsFromJobs(List<String> jobnos, Locale locale);

	JobItem getMostRecentJobItemForInstrument(Instrument instrument);

	JobItemPlantillasDTO getPlantillasJobItem(int jobitemId, int allocatedAddressId);

	PagedResultSet<JobItemPlantillasDTO> getPlantillasJobItems(int allocatedCompanyId, int allocatedAddressId,
			Date lastModified, Integer clientAddressId, int businessSubdivId, int resultsPerPage, int currentPage);

	JobItem getJobItemByItemCode(String jobNo, Integer itemNo);

	List<JobItemDTO> getJobItemDTOs(Integer jobId, Locale locale);

	List<JobItemProjectionDTO> getJobItemProjectionDTOs(Collection<Integer> jobIds, Collection<Integer> jobItemIds);

	List<JobItemProjectionDTO> getJobItemProjectionDTOsByJobIds(Collection<Integer> jobIds, Locale locale);

	JobItem getForView(Integer jobItemId);

	JobItem findJobItem(String jobNumber, Integer jobItemNumber);

	PagedResultSet<TLMJobItemDTO> searchJobitems(boolean active, Integer plantId, String plantno, String serialno,
			String formerBarcode, Integer calibrationSubdiv, String jobitemno, int currentPage, int resultsPerPage,
			Locale locale);

	List<JobItemForProspectiveInvoiceDTO> getForProspectiveInvoice(Integer allocatedCompanyId, Integer companyId,
			Locale locale);

	Page<JobItemIdDto> getJobItemIdDto(String days);

	boolean stateHasGroupOfKeyName(ItemState state, StateGroup group);

	List<ItemAtTP> getAllItemsAtSpecificThirdParty(List<Integer> subdivids, int businessSubdivId, Locale locale);

	Optional<JobItem> findActiveJobItemFromBarcodeOrFormerBarcode(String barcode);

	List<JobItemImportInfoDto> getJobItemsInfo(Integer jobId);

	Map<Instrument, JobItem> findMostRecentJobItem(List<Integer> plantids);

	List<JobItemFeedbackDto> getAllJobItems(int jobid, Locale locale, Locale fallbackLocale);

	List<PositionProjection> getPositionProjectionForJob(Integer jobId);

	List<JobItemOnClientPurchaseOrderDTO> getJobItemsOnClientBPO(Integer poId, Integer jobId, Locale locale);

	Map<Integer, JobItemProjectionDTO> findMostRecentJobItemDTO(List<Integer> plantids);

    List<JobItemForInstrumentTooltipDto> findJobItemsForInstrumentTooltipByPlantId(Integer plantId);
    
    List<Integer> getInstrumentIdsFromJob(Integer jobid);
    
    JobItemProjectionDTO getJobItemProjectionDTO(Integer jobitemId,Locale locale);
    
    List<JobItem> getJobItemByDeliveryId(String delivryNo);

	List<SelectedJobItemDTO> getActiveJobItemsForWorkAllocation(List<Integer> procIds, List<Integer> stateIds,
																Boolean isAllocatedToEngineer, Subdiv subdiv, StateGroupLinkType sglType, Integer addressId, AllocateWorkToEngineerSearchForm form);
    
} 