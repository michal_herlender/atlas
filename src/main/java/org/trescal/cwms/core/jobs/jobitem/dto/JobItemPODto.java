package org.trescal.cwms.core.jobs.jobitem.dto;

import lombok.*;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JobItemPODto {
    private Integer jipoId;
    private Integer poId;
    private String poNumber;
    private String comment;

    public static JobItemPODto fromJobItemPpo(JobItemPO jipo) {
        val bpo = jipo.getBpo() != null? jipo.getBpo():jipo.getPo();
        return JobItemPODto.builder()
            .poId(bpo.getPoId())
            .jipoId(jipo.getId()).comment(bpo.getComment()).poNumber(bpo.getPoNumber())
            .build();
    }
}
