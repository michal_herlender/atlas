package org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.db;

import java.math.BigDecimal;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewLinkedRepairCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;

@Service
public class ContractReviewRepairCostServiceImpl implements ContractReviewRepairCostService {
	@Autowired
	private ContractReviewRepairCostDao ContractReviewRepairCostDao;

	/**
	 * Create empty zero valued cost
	 * 
	 * @return
	 */
	@Override
	public ContractReviewRepairCost createEmptyCost(JobItem jobItem, boolean active) {
		ContractReviewRepairCost cost = new ContractReviewRepairCost();
		cost.setActive(true);
		cost.setAutoSet(true);
		cost.setDiscountRate(new BigDecimal(0.00));
		cost.setDiscountValue(new BigDecimal(0.00));
		cost.setFinalCost(new BigDecimal(0.00));
		cost.setTotalCost(new BigDecimal(0.00));
		cost.setJobitem(jobItem);
		cost.setCostType(CostType.REPAIR);
		cost.setCostSrc(CostSource.MANUAL);
		cost.setActive(active);
		return cost;
	}

	@Override
	public ContractReviewRepairCost copyContractReviewCost(ContractReviewRepairCost old, JobItem copyToJobitem) {
		ContractReviewRepairCost newCost = null;

		if (old != null) {
			newCost = new ContractReviewRepairCost();

			BeanUtils.copyProperties(old, newCost);

			newCost.setCostid(null);
			newCost.setJobitem(copyToJobitem);
			newCost.setAutoSet(true);

			if (old.getLinkedCost() != null) {
				ContractReviewLinkedRepairCost oldLinkedCost = old.getLinkedCost();
				ContractReviewLinkedRepairCost newLinkedCost = new ContractReviewLinkedRepairCost();
				BeanUtils.copyProperties(oldLinkedCost, newLinkedCost);
				newLinkedCost.setId(0);
				newLinkedCost.setRepairCost(newCost);
				newCost.setLinkedCost(newLinkedCost);
			}
		}
		return newCost;
	}

	@Override
	public ContractReviewRepairCost findContractReviewRepairCost(int id) {
		return this.ContractReviewRepairCostDao.find(id);
	}
}