package org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.db;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.calibration.dto.StandardsUsageAnalysisDTO;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.StandardsUsageAnalysis;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysisstatus.StandardsUsageAnalysisStatus;
import org.trescal.cwms.core.jobs.calibration.form.StandardsUsageAnalysisSearchForm;
import org.trescal.cwms.core.jobs.calibration.form.UpdateStandardsUsageAnalysisForm;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_GenericReverseTraceabilityFailureDetected;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;

import java.io.File;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

@Service("StandardsUsageAnalysisService")
public class StandardsUsageAnalysisServiceImpl extends BaseServiceImpl<StandardsUsageAnalysis, Integer>
	implements StandardsUsageAnalysisService {

	@Autowired
	private StandardsUsageAnalysisDao standardsUsageAnalysisDao;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private ComponentDirectoryService componentDirectoryService;
	@Autowired
	private SystemComponentService scService;
	@Autowired
	private EmailContent_GenericReverseTraceabilityFailureDetected emailContent_GenericReverseTraceabilityFailureDetected;
	@Autowired
	private EmailService emailServ;
	@Value("${cwms.config.email.noreply}")
	private String noreplyAddress;

	@Override
	protected BaseDao<StandardsUsageAnalysis, Integer> getBaseDao() {
		return standardsUsageAnalysisDao;
	}

	@Override
	public PagedResultSet<StandardsUsageAnalysisDTO> sreachStandardsUsageAnalysis(StandardsUsageAnalysisSearchForm form,
			PagedResultSet<StandardsUsageAnalysisDTO> prs, Locale locale) {
		return this.standardsUsageAnalysisDao.sreachStandardsUsageAnalysis(form, prs, locale);
	}

	@Override
	public void updateStandardsUsageAnalysis(StandardsUsageAnalysis standardsUsageAnalysis) {
		this.merge(standardsUsageAnalysis);
	}

	@Override
	public void editStandardsUsageAnalysis(StandardsUsageAnalysis standardsUsageAnalysis,
			UpdateStandardsUsageAnalysisForm form) {

		// update status
		if (form.getStatus() != null) {
			standardsUsageAnalysis.setStatus(form.getStatus());
		}

		// update start date
		if (form.getStartDate() != null) {
			standardsUsageAnalysis.setStartDate(form.getStartDate());
		}

		// update finish date
		if (form.getFinishDate() != null) {
			standardsUsageAnalysis.setFinishDate(form.getFinishDate());
		}

		this.updateStandardsUsageAnalysis(standardsUsageAnalysis);
	}

	@Override
	public StandardsUsageAnalysis getStandardsUsageAnalysisFromInstrument(Integer plantId) {
		return this.standardsUsageAnalysisDao.getStandardsUsageAnalysisFromInstrument(plantId);
	}

	@Override
	public StandardsUsageAnalysis createStandardsUsageAnalysis(Integer plantId, Integer certificateid, LocalDate startDate, Date finishDate) {
		StandardsUsageAnalysis standardsUsageAnalysis = new StandardsUsageAnalysis();
		standardsUsageAnalysis.setInstrument(this.instrumentService.get(plantId));
		standardsUsageAnalysis.setCertificate(this.certificateService.findCertificate(certificateid));
		standardsUsageAnalysis.setStartDate(DateTools.dateFromLocalDate(startDate));
		standardsUsageAnalysis.setFinishDate(finishDate);
		standardsUsageAnalysis.setStatus(StandardsUsageAnalysisStatus.OPEN);
		this.updateStandardsUsageAnalysis(standardsUsageAnalysis);
		return standardsUsageAnalysis;
	}

	@Override
	public List<StandardsUsageAnalysisDTO> certificatesConnectedToMeasurementStandards(Integer businessSubdivId,
																					   LocalDate startDate, Locale locale) {
		List<CalibrationVerificationStatus> calibrationStatus = new ArrayList<>();
		calibrationStatus.add(CalibrationVerificationStatus.NOT_ACCEPTABLE);
		calibrationStatus.add(CalibrationVerificationStatus.PROBABLY_PASS);
		calibrationStatus.add(CalibrationVerificationStatus.PROBABLY_FAIL);
		return this.standardsUsageAnalysisDao.certificatesConnectedToMeasurementStandards(businessSubdivId,
			startDate, calibrationStatus, locale);
	}

	@Override
	public Boolean notifySubscribers(Set<String> subscriberEmails, Locale locale, List<StandardsUsageAnalysisDTO> dtos) {
		boolean emailSuccess = false;
		try {
			EmailContentDto emailDto = this.emailContent_GenericReverseTraceabilityFailureDetected.getContent(locale,
				new SimpleDateFormat("yyyyMMMdd_HHmm").format(new Date()), dtos);
			// send email
			for (String email : subscriberEmails) {
				emailSuccess = this.emailServ.sendBasicEmail(emailDto.getSubject(), noreplyAddress,
					email, emailDto.getBody());

			}
		} catch (MalformedURLException | URISyntaxException e) {
			e.printStackTrace();
		}
		return emailSuccess;
	}
	
	@Override
	public void storeFileGenerated(SXSSFWorkbook sxxfWorkbook, String identifier){
		File directory = this.componentDirectoryService.getDirectory(
				scService.findComponent(Component.STANDARDS_USAGE_ANALYSIS),identifier);
		File fileXlsx = new File(directory, "reversetraceabilityreport_"
				+ new SimpleDateFormat("yyyyMMMdd_HHmm").format(new Date()) + ".xlsx");
		try{
			FileOutputStream fileOut = new FileOutputStream(fileXlsx);
			sxxfWorkbook.write(fileOut);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
