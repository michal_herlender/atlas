package org.trescal.cwms.core.jobs.calibration.entity.calreq.db;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.InstrumentCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.InstrumentCalReqDto;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.JobItemCalReq;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;

public interface CalReqService
{
	ResultWrapper ajaxDeactivateCalReqs(int id, String typeCode, HttpSession session);

	CalReq ajaxFindCalReq(int id);

	void deactivateCalReqs(CalReq cr, HttpSession session);

	void deleteAllCalReqsImmediatelyAffectingJobItem(int jobItemId);

	void deleteCalReq(CalReq calreq);

	List<JobItemCalReq> findAllCalReqsImmediatelyAffectingJobItem(int jobItemId);

	CalReq findCalReq(int id);

	Map<Integer, CalReq> findCalReqsForAllItemsOnJob(int jobId);

	Map<Integer, CalReq> findCalReqsForQuotationItems(Quotation quotation, Collection<Quotationitem> items);

	CalReq findCalReqsForInstrument(Instrument i);

	CalReq findCalReqsForJobItem(JobItem ji);

	/**
	 * Primarily used when creating third party quote request from quotation item
	 * Finds related notes via instrument or model
	 * @param qi
	 * @return
	 */
	CalReq findCalReqsForQuotationItem(Quotationitem qi);

    InstrumentCalReq findInstrumentCalReqForInstrument(Instrument instrument);

	InstrumentCalReq findInstrumentCalReqForInstrument(Integer plantId);
	
	Map<Integer, InstrumentCalReqDto> findInstrumentCalReqsForPlantIds(List<Integer> plantids);

	List<CalReq> getAllCalReqs();

	Class<? extends CalReq> getClassFromString(String code);

	String getStringFromClass(Class<? extends CalReq> clazz);

	void insertCalReq(CalReq calreq);

	void updateCalReq(CalReq calreq);

}