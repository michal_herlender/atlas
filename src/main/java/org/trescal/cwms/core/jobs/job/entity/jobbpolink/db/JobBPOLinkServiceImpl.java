package org.trescal.cwms.core.jobs.job.entity.jobbpolink.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink;

@Service("JobBPOLinkService")
public class JobBPOLinkServiceImpl extends BaseServiceImpl<JobBPOLink, Integer> implements JobBPOLinkService {

	@Autowired
	private JobBPOLinkDao jobBpoLinkDao;
	
	@Override
	protected BaseDao<JobBPOLink, Integer> getBaseDao() {
		return jobBpoLinkDao;
	}

	@Override
	public JobBPOLink findByJobIdAndBpoId(Integer jobId, Integer bpoId) {
		return this.jobBpoLinkDao.findByJobIdAndBpoId(jobId, bpoId);
	}

}