package org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Entity
@Table(name = "contractreviewitem")
public class ContractReviewItem extends Auditable
{
	private int id;
	private JobItem jobitem;
	private ContractReview review;

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid", nullable = false)
	public JobItem getJobitem()
	{
		return this.jobitem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "reviewid", nullable = false)
	public ContractReview getReview()
	{
		return this.review;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setJobitem(JobItem jobitem)
	{
		this.jobitem = jobitem;
	}

	public void setReview(ContractReview review)
	{
		this.review = review;
	}
}
