package org.trescal.cwms.core.jobs.calibration.entity.standardused.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed;

public interface StandardUsedDao extends BaseDao<StandardUsed, Integer> {

	List<StandardUsed> getAllStandardsUsedOnCal(Integer calId);

	List<StandardUsed> getAllStandardsUsedOnJobItem(Integer jobItemId);

	Boolean standardsUsedOnJobItem(Integer jobItemId);
}