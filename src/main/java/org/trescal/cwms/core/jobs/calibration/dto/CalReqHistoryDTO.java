package org.trescal.cwms.core.jobs.calibration.dto;

import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqEdit;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqFallback;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqHistory;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqOverride;

import java.util.Date;

public class CalReqHistoryDTO {
    private String activity;
    private String changeBy;
    private int fromId;
    private Date time;
    private int toId;

    public CalReqHistoryDTO(CalReq inactiveCalReq) {
        this.changeBy = inactiveCalReq.getDeactivatedBy().getShortenedName();
        this.fromId = inactiveCalReq.getId();
        this.time = inactiveCalReq.getDeactivatedOn();
        this.toId = 0;

        this.activity = "deactivated the " + inactiveCalReq.getClassKey()
            + " requirements";
    }

	public CalReqHistoryDTO(CalReqHistory history) {
        this.changeBy = history.getChangeBy().getShortenedName();
        this.fromId = (history.getOldCalReq() != null) ? history.getOldCalReq().getId() : 0;
        this.time = history.getChangeOn();
        this.toId = (history.getNewCalReq() != null) ? history.getNewCalReq().getId() : 0;

        if (history instanceof CalReqOverride) {
            this.activity = "added overriding "
                + (history.getNewCalReq() != null ? history.getNewCalReq().getClassKey() : "") + " requirements";
        } else if (history instanceof CalReqFallback) {
            this.activity = "deactivated the "
					+ history.getOldCalReq().getClassKey() + " requirements";

			if (this.toId != 0)
			{
				this.activity = this.activity + " and fell back to "
						+ history.getNewCalReq().getClassKey()
						+ " requirements";
			}
		}
		else if (history instanceof CalReqEdit)
		{
            this.activity = "changed the "
                + (history.getNewCalReq() != null ? history.getNewCalReq().getClassKey() : "") + " requirements";
		}
		else
		{
			this.activity = "";
		}
	}

	public String getActivity()
	{
		return this.activity;
	}

    public String getChangeBy() {
        return this.changeBy;
    }

    public int getFromId() {
        return this.fromId;
    }

    public Date getTime() {
        return this.time;
    }

    public int getToId() {
        return this.toId;
    }

    public void setActivity(String activity)
	{
		this.activity = activity;
	}

    public void setChangeBy(String changeBy) {
        this.changeBy = changeBy;
    }

    public void setFromId(int fromId) {
        this.fromId = fromId;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public void setToId(int toId) {
        this.toId = toId;
    }
}
