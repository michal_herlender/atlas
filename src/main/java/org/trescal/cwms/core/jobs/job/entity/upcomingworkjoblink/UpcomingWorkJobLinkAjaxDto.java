package org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

@Data
@NoArgsConstructor
public class UpcomingWorkJobLinkAjaxDto {

    private Integer id;
    private Integer upcomingWorkId;
    private String jobNo;
    private Integer jobId;
    private boolean active;

    public UpcomingWorkJobLinkAjaxDto(Integer upcomingWorkId, String jobNo, boolean active){
        this.upcomingWorkId = upcomingWorkId;
        this.jobNo = jobNo;
        this.active = active;
    }
    public UpcomingWorkJobLinkAjaxDto(UpcomingWorkJobLink upcomingWorkJobLink){
        Job job = upcomingWorkJobLink.getJob();
        this.id = upcomingWorkJobLink.getId();
        this.upcomingWorkId = upcomingWorkJobLink.getUpcomingWork().getId();
        this.jobNo = job.getJobno();
        this.jobId = job.getJobid();
        this.active = upcomingWorkJobLink.getActive();
    }
}
