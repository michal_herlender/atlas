package org.trescal.cwms.core.jobs.jobitem.projection;

import java.math.BigDecimal;

import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
/**
 * Generic price entity that can be used for JobCosting*Cost, ContractReview*Cost, Invoice*Cost 
 * TODO consider adding some sort of "price entity type" if needed
 * Only necessary fields are added for now; others could be considered to add (currency?) 
 */
public class PriceProjectionDto {
	private Integer costId;
	private Boolean active;
	private CostSource costSource;
	private CostType costType;
	private BigDecimal discountRate;	// Percentage discount rate
	private BigDecimal discountValue; 	// Monetary discount rate
	private BigDecimal finalCost;		// Monetary cost amount after discount
	private BigDecimal totalCost;		// Monetary cost amount prior to to discount
	
	public PriceProjectionDto(Integer costId, Boolean active, CostSource costSource, CostType costType, 
			BigDecimal discountRate, BigDecimal discountValue, BigDecimal finalCost, BigDecimal totalCost) {
		super();
		this.costId = costId;
		this.active = active;
		this.costSource = costSource;
		this.costType = costType;
		this.discountRate = discountRate;
		this.discountValue = discountValue;
		this.finalCost = finalCost;
		this.totalCost = totalCost;
	}
}
