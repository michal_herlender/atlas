package org.trescal.cwms.core.jobs.job.entity.job.db;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.calibration.controller.ActiveInstrumentsJsonController.ActiveInstrumentDto;
import org.trescal.cwms.core.jobs.job.dto.JobKeyValue;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.form.JobSearchForm;
import org.trescal.cwms.core.jobs.job.form.OldJobSearchForm;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.vdi.projection.OrderProjection;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.web.home.dto.JobStats;

public interface JobDao extends AllocatedDao<Subdiv, Job, Integer> {

	Long countActiveByContactAndAllocatedSubdiv(Contact contact, Subdiv allocatedSubdiv);

	Long countByContact(Contact contact);

	Long countByContactAndAllocatedSubdiv(Contact contact, Subdiv allocatedSubdiv);

	Long countActiveBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv);

	Long countBySubdiv(Subdiv subdiv);

	Long countBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv);

	Long countActiveByCompanyAndAllocatedSubdiv(Company company, Subdiv allocatedSubdiv);

	Long countByCompany(Company company);

	Long countByCompanyAndAllocatedSubdiv(Company company, Subdiv allocatedSubdiv);

	List<Job> findAllJobsReadyToInvoice(int coid);

	Job findByJobNo(String jobno);

	List<JobKeyValue> findByPartialJobNo(String partialJobNo);

	Job findEagerJobForView(int id);

	Job getJobByExactJobNo(String jobNo);

	ResultWrapper getJobsAndItemsNotInStateGroupForAddress(List<StateGroup> stateGroups, int addrId);

	List<Job> getJobsById(Collection<Integer> ids);

	List<JobProjectionDTO> getJobProjectionDTOList(Collection<Integer> jobIds, boolean countJobItems, boolean countJobExpenseItems);

	JobStats getJobStats(int personId, int subdivId, int compId, int addrId);

	PagedResultSet<Job> queryJob(OldJobSearchForm jsf, PagedResultSet<Job> rs);
	
	void queryJobJPANew(JobSearchForm jsf, PagedResultSet<JobProjectionDTO> rs);

	List<Job> searchJob(String jobNo);

	List<Job> getActiveJobsBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv);

	List<ActiveInstrumentDto> findActiveJobsByPlantids(List<String> plantIds);

    Optional<OrderProjection> getOrderProjectionForJob(Integer jobId);
}