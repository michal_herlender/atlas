package org.trescal.cwms.core.jobs.repair.componentinventory.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.repair.componentinventory.ComponentInventory;

public interface ComponentInventoryDao extends BaseDao<ComponentInventory, Integer> {
	
}