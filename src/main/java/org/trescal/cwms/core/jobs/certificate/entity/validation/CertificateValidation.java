package org.trescal.cwms.core.jobs.certificate.entity.validation;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

@Entity
@Table(name = "certificatevalidation")
@Setter
public class CertificateValidation extends Auditable {

    private Certificate cert;
    private LocalDate datevalidated;
    private String note;
    private String responsible;
    private String document;
    private Contact validatedBy;
    private CertificateValidationStatus status;
    private int id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "certid")
    public Certificate getCert() {
        return this.cert;
    }

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return id;
    }

    @NotNull
    public LocalDate getDatevalidated() {
        return datevalidated;
    }

    @Length(max = 1000)
    @Column(name = "note", length = 1000)
    public String getNote() {
        return note;
    }

    @Length(max = 100)
    @Column(name = "responsible", length = 100)
    public String getResponsible() {
        return responsible;
    }

    @Length(max = 200)
    @Column(name = "document", length = 200)
    public String getDocument() {
        return document;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "personid", nullable = true)
    public Contact getValidatedBy() {
        return validatedBy;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "validationstatus")
    public CertificateValidationStatus getStatus() {
        return status;
    }
}