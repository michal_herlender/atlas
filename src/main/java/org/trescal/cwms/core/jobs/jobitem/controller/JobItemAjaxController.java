package org.trescal.cwms.core.jobs.jobitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemFeedbackDto;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;

import java.util.List;

@RestController
@RequestMapping("jobitem")
public class JobItemAjaxController {

    private final JobItemService jobItemService;

    @Autowired
    public JobItemAjaxController(JobItemService jobItemService, UserService userService) {
        this.jobItemService = jobItemService;
    }

    @GetMapping("allJobItems")
    List<JobItemFeedbackDto> getAllJobItemForJob(@RequestParam Integer jobId) {
        return jobItemService.getAllJobItemsByJobId(jobId);
    }
}
