package org.trescal.cwms.core.jobs.certificate.entity.instcertlink;

import java.io.File;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

@Entity
@Table(name = "instcertlink")
public class InstCertLink extends Auditable
{
	private Certificate cert;
	private int id;
	private Instrument inst;

	@Cascade(CascadeType.SAVE_UPDATE)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "certid")
	public Certificate getCert()
	{
		return this.cert;
	}

	/*
	 * CertLink and InstCertLink had the same copy & pasted implementation, 
	 * moved to Certificate (for now) GB 2016-02-15  
	 */
	@Deprecated
	@Transient
	public String getCertFilePath()
	{
		return this.cert.getCertFilePath();
	}
	
	@Deprecated
	@Transient
	public String getEncryptedCertFilePath()
	{
		return this.cert.getEncryptedCertFilePath();
	}

	/*
	 * CertLink and InstCertLink had the same copy & pasted implementation, 
	 * moved to Certificate (for now) GB 2016-02-15  
	 */
	@Deprecated
	@Transient
	public ArrayList<String> getCertFilePaths()
	{
		return this.cert.getCertFilePaths();
	}

	/*
	 * CertLink and InstCertLink had the same copy & pasted implementation, 
	 * moved to Certificate (for now) GB 2016-02-15  
	 */
	@Deprecated
	@Transient
	public ArrayList<File> getCertFiles()
	{
		return this.cert.getCertFiles();
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Cascade(CascadeType.SAVE_UPDATE)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid")
	public Instrument getInst()
	{
		return this.inst;
	}

	/*
	 * CertLink and InstCertLink had the same copy & pasted implementation, 
	 * moved to Certificate (for now) GB 2016-02-15  
	 */
	@Deprecated
	@Transient
	public boolean isHasCertFile()
	{
		return this.cert.getHasCertFile();
	}

	/*
	 * CertLink and InstCertLink had the same copy & pasted implementation, 
	 * moved to Certificate (for now) GB 2016-02-15  
	 */
	@Deprecated
	@Transient
	public boolean isHasWordFile()
	{
		return this.cert.hasWordFile();
	}

	public void setCert(Certificate cert)
	{
		this.cert = cert;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setInst(Instrument inst)
	{
		this.inst = inst;
	}
}
