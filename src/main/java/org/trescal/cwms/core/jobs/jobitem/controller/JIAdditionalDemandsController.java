package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemand.JobItemDemand;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemand;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemandStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.form.JIAdditionalDemandsForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class JIAdditionalDemandsController extends JobItemController {

	@Autowired
	private JobItemService jobItemService;

	@ModelAttribute("form")
	public JIAdditionalDemandsForm getAdditionalDemandForm(
			@RequestParam(value = "jobitemid", required = true) Integer jobItemId) {
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		JIAdditionalDemandsForm form = new JIAdditionalDemandsForm();
		Map<Integer, AdditionalDemandStatus> demandStatus = new HashMap<Integer, AdditionalDemandStatus>();
		Map<Integer, String> demandComment = new HashMap<Integer, String>();
		for (AdditionalDemand demand : AdditionalDemand.values()) {
			Optional<JobItemDemand> jobItemDemand = jobItem.getAdditionalDemands().stream()
					.filter(jid -> jid.getAdditionalDemand().equals(demand)).findAny();
			if (jobItemDemand.isPresent()) {
				demandStatus.put(demand.ordinal(), jobItemDemand.get().getAdditionalDemandStatus());
				demandComment.put(demand.ordinal(), jobItemDemand.get().getComment());
			} else {
				demandStatus.put(demand.ordinal(), AdditionalDemandStatus.UNDEFINED);
				demandComment.put(demand.ordinal(), "");
			}
		}
		form.setDemandStatus(demandStatus);
		form.setDemandComment(demandComment);
		return form;
	}

	@RequestMapping(value = "/jidemands.htm", method = RequestMethod.POST)
	public RedirectView onSubmit(@ModelAttribute("form") JIAdditionalDemandsForm form,
			@RequestParam(value = "jobitemid", required = true) Integer jobItemId) {
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		for (AdditionalDemand demand : AdditionalDemand.values()) {
			Optional<JobItemDemand> jobItemDemand = jobItem.getAdditionalDemands().stream()
					.filter(jid -> jid.getAdditionalDemand().equals(demand)).findAny();
			if (jobItemDemand.isPresent()) {
				jobItemDemand.get().setAdditionalDemandStatus(form.getDemandStatus().get(demand.ordinal()));
				jobItemDemand.get().setComment(form.getDemandComment().get(demand.ordinal()));
			} else {
				JobItemDemand jid = new JobItemDemand();
				jid.setJobItem(jobItem);
				jid.setAdditionalDemand(demand);
				jid.setAdditionalDemandStatus(form.getDemandStatus().get(demand.ordinal()));
				jid.setComment(form.getDemandComment().get(demand.ordinal()));
				jobItem.getAdditionalDemands().add(jid);
			}
		}
		jobItemService.updateAdditionalDemands(jobItem);
		return new RedirectView("jidemands.htm?jobitemid=" + jobItemId);
	}

	@RequestMapping(value = "/jidemands.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@RequestParam(value = "jobitemid", required = true) Integer jobItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		Map<String, Object> refData = super.referenceData(jobItemId, contact, subdivDto.getKey(),username);
		refData.put("additionalDemands", Arrays.asList(AdditionalDemand.values()));
		refData.put("additionalDemandStatus", Arrays.asList(AdditionalDemandStatus.values()));
		return new ModelAndView("trescal/core/jobs/jobitem/jidemands", refData);
	}
}