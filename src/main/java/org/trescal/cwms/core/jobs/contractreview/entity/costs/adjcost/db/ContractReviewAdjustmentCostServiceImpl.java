package org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.db;

import java.math.BigDecimal;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewLinkedAdjustmentCost;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;

@Service
public class ContractReviewAdjustmentCostServiceImpl implements ContractReviewAdjustmentCostService {
	@Autowired
	ContractReviewAdjustmentCostDao ContractReviewAdjustmentCostDao;

	/**
	 * Create empty zero valued cost
	 * 
	 * @return
	 */
	@Override
	public ContractReviewAdjustmentCost createEmptyCost(JobItem jobItem, boolean active) {
		ContractReviewAdjustmentCost cost = new ContractReviewAdjustmentCost();
		cost.setActive(true);
		cost.setAutoSet(true);
		cost.setDiscountRate(new BigDecimal(0.00));
		cost.setDiscountValue(new BigDecimal(0.00));
		cost.setFinalCost(new BigDecimal(0.00));
		cost.setTotalCost(new BigDecimal(0.00));
		cost.setCostSrc(CostSource.MANUAL);
		cost.setJobitem(jobItem);
		cost.setCostType(CostType.ADJUSTMENT);
		cost.setActive(active);
		return cost;
	}

	@Override
	public ContractReviewAdjustmentCost copyContractReviewCost(ContractReviewAdjustmentCost old,
			JobItem copyToJobitem) {
		ContractReviewAdjustmentCost newCost = null;

		if (old != null) {
			newCost = new ContractReviewAdjustmentCost();

			BeanUtils.copyProperties(old, newCost);

			newCost.setCostid(null);
			newCost.setJobitem(copyToJobitem);
			newCost.setAutoSet(true);

			if (old.getLinkedCost() != null) {
				ContractReviewLinkedAdjustmentCost oldLinkedCost = old.getLinkedCost();
				ContractReviewLinkedAdjustmentCost newLinkedCost = new ContractReviewLinkedAdjustmentCost();
				BeanUtils.copyProperties(oldLinkedCost, newLinkedCost);
				newLinkedCost.setId(0);
				newLinkedCost.setAdjustmentCost(newCost);
				newCost.setLinkedCost(newLinkedCost);
			}
		}
		return newCost;
	}

	@Override
	public ContractReviewAdjustmentCost findContractReviewAdjustmentCost(int id) {
		return this.ContractReviewAdjustmentCostDao.find(id);
	}

}