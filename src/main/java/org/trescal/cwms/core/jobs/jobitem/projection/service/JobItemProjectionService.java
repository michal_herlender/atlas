package org.trescal.cwms.core.jobs.jobitem.projection.service;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;

public interface JobItemProjectionService {
	
	List<JobItemProjectionDTO> getProjectionDTOsForJobIds(Collection<Integer> jobIds, JobItemProjectionCommand command);
	
	JobItemProjectionResult getProjectionDTOsForJobItemIds(Collection<Integer> jobItemIds, JobItemProjectionCommand command);
	
	JobItemProjectionResult loadJobItemProjections(List<JobItemProjectionDTO> jobItemDtos, JobItemProjectionCommand command);

    void loadAdditionalProjections(JobItemProjectionDTO jiDto, Integer allocatedCompanyId, Locale locale);
}
