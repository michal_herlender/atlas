package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.form.ReturnToAddressForm;
import org.trescal.cwms.core.system.Constants;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class ReturnToAddressController {

	@Autowired
	private JobItemService jobItemServ;
	@Autowired
	private JobService jobServ;

	@InitBinder("command")
	protected void initBinder(WebDataBinder binder) {
	}

	@ModelAttribute("command")
	protected ReturnToAddressForm formBackingObject(
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") Integer jobitemid,
			@RequestParam(value = "jobid", required = false, defaultValue = "0") Integer jobid) throws Exception {
		ReturnToAddressForm form = new ReturnToAddressForm();
		Map<Integer, Boolean> jobitemids = new HashMap<>();
		Job job = null;
		if (jobitemid != 0) {
			JobItem ji = this.jobItemServ.findJobItem(jobitemid);
			job = ji.getJob();
			form.setSrcJobitem(ji);
		} else if (jobid != 0) {
			job = this.jobServ.get(jobid);
		}
		if ((job == null) || ((jobitemid == 0) && (jobid == 0))) {
			throw new Exception("Could not find job");
		}
		form.setJob(job);
		job.getItems().stream().forEach(ji -> jobitemids.put(ji.getJobItemId(), false));
		if (form.getSrcJobitem() != null)
			jobitemids.put(form.getSrcJobitem().getJobItemId(), Boolean.TRUE);
		form.setJobitemids(jobitemids);
		return form;
	}

	@RequestMapping(value = "/returntoaddress.htm", method = RequestMethod.POST)
	protected String onSubmit(@ModelAttribute("command") ReturnToAddressForm form, BindingResult bindingResult,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {

		if (bindingResult.hasErrors())
			return referenceData();

		jobItemServ.updateReturnToAddress(form);

		if (form.getSrcJobitem() != null) {
			// do not use ji.getDefaultPage here: always go back to contract
			// review page as this is likely the page the user was linked from
			return "redirect:jicontractreview.htm?jobitemid=" + form.getSrcJobitem().getJobItemId();
		} else {
			return "redirect:returntoaddress.htm?jobid=" + form.getJob().getJobid();
		}
	}

	@RequestMapping(value = "/returntoaddress.htm", method = RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/jobs/jobitem/returntoaddress";
	}
}