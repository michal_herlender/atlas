package org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db.WorkRequirementService;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Service("JobItemWorkRequirementService")
public class JobItemWorkRequirementServiceImpl extends BaseServiceImpl<JobItemWorkRequirement, Integer>
		implements JobItemWorkRequirementService {

	@Autowired
	private WorkRequirementService workRequirementService;
	@Autowired
	private JobItemWorkRequirementDao baseDao;

	@Override
	public JobItemWorkRequirement findJobItemWorkRequirement(int id) {
		return this.get(id);
	}

	@Override
	public List<JobItemWorkRequirement> getForJobItemView(JobItem jobItem) {
		return baseDao.getForJobItemView(jobItem);
	}

	@Override
	public ResultWrapper toggleCompleteJobItemWorkRequirement(int jiwrId) {
		// get job item work requirement
		JobItemWorkRequirement jiwr = this.findJobItemWorkRequirement(jiwrId);
		// work requirement for this job item null?
		if (jiwr != null) {
			// job item work requirement already completed?
			if (jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE)) {
				// set work requirement status to PENDING
				jiwr.getWorkRequirement().setStatus(WorkrequirementStatus.PENDING);
			} else {
				// set work requirement status to COMPLETE
				jiwr.getWorkRequirement().setStatus(WorkrequirementStatus.COMPLETE);
			}
			// return success
			return new ResultWrapper(true, "", jiwr, null);
		} else {
			// return error message
			return new ResultWrapper(false, "Job item work requirement could not be found", null, null);
		}
	}

	@Override
	public JobItemWorkRequirement duplicate(JobItemWorkRequirement jiwr) {

		WorkRequirement wr = jiwr.getWorkRequirement();
		JobItem ji = jiwr.getJobitem();

		// baseDao.detach(jiwr);
		jiwr = new JobItemWorkRequirement();

		WorkRequirement newWr = workRequirementService.duplicate(wr);

		jiwr.setId(null);
		jiwr.setJobitem(ji);
		jiwr.setWorkRequirement(newWr);
		newWr.getJobItemWorkRequirements().add(jiwr);

		save(jiwr);

		return jiwr;
	}

	@Override
	protected BaseDao<JobItemWorkRequirement, Integer> getBaseDao() {
		return baseDao;
	}
}