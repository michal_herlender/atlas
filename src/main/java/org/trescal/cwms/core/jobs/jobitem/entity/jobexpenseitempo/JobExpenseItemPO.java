package org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitempo.AbstractJobItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;

import lombok.Setter;

@Setter
@Entity
@AssociationOverrides({
		@AssociationOverride(name = "item", joinColumns = @JoinColumn(name = "expenseitemid"), foreignKey = @ForeignKey(name = "FK_jobexpenseitempo_expenseitem")),
		@AssociationOverride(name = "lastModifiedBy", joinColumns = @JoinColumn(name = "lastModifiedBy"), foreignKey = @ForeignKey(name = "FK_jobexpenseitempo_lastmodifiedby")),
		@AssociationOverride(name = "po", joinColumns = @JoinColumn(name = "poid"), foreignKey = @ForeignKey(name = "FK_jobexpenseitempo_po")),
		@AssociationOverride(name = "bpo", joinColumns = @JoinColumn(name = "bpoid"), foreignKey = @ForeignKey(name = "FK_jobexpenseitempo_bpo")) })
@Table(name = "jobexpenseitempo", uniqueConstraints = @UniqueConstraint(name = "UK_jobexpenseitempo_expenseitemid", columnNames = {
		"expenseitemid" }))
public class JobExpenseItemPO extends AbstractJobItemPO<JobExpenseItem> {
}