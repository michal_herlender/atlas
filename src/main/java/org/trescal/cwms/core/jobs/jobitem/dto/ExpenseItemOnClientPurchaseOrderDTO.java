package org.trescal.cwms.core.jobs.jobitem.dto;

import lombok.Data;

@Data
public class ExpenseItemOnClientPurchaseOrderDTO {

	private Integer id;
	private Integer itemNo;
	private String modelName;
	private String comment;
	private Boolean onClientPO;
	private Boolean onAnyClientPO;

	public ExpenseItemOnClientPurchaseOrderDTO(Integer id, Integer itemNo, String modelName, String comment, Long onPo,
			Long onPos) {
		this.id = id;
		this.itemNo = itemNo;
		this.modelName = modelName;
		this.comment = comment;
		this.onClientPO = onPo > 0;
		this.onAnyClientPO = onPos > 0;
	}
}