package org.trescal.cwms.core.jobs.repair.validatedoperation;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationStateEnum;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.validatedcomponent.ValidatedFreeRepairComponent;

@Entity
@Table(name = "validatedfreerepairoperation")
public class ValidatedFreeRepairOperation extends Auditable {

	private int validatedOperationId;
	private Integer labourTime;
	private BigDecimal cost;
	private RepairOperationStateEnum validatedStatus;
	private FreeRepairOperation freeRepairOperation;
	private Set<ValidatedFreeRepairComponent> validatedFreeRepairComponents;
	private RepairCompletionReport repairCompletionReport;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getValidatedOperationId() {
		return validatedOperationId;
	}

	public void setValidatedOperationId(int validatedOperationId) {
		this.validatedOperationId = validatedOperationId;
	}

	@Column(name = "labourtime", nullable = false)
	public Integer getLabourTime() {
		return labourTime;
	}

	public void setLabourTime(Integer labourTime) {
		this.labourTime = labourTime;
	}

	@Column(name = "cost", nullable = true, precision = 10, scale = 2)
	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	
	@Column(name = "validatedstatus", nullable = true)
	@Enumerated(EnumType.STRING)
	public RepairOperationStateEnum getValidatedStatus() {
		return validatedStatus;
	}

	public void setValidatedStatus(RepairOperationStateEnum validatedStatus) {
		this.validatedStatus = validatedStatus;
	}

	@OneToOne
	@JoinColumn(name = "freerepairoperationid", foreignKey = @ForeignKey(name = "validatedfreerepairoperation_freerepairoperation_FK"))
	public FreeRepairOperation getFreeRepairOperation() {
		return freeRepairOperation;
	}

	public void setFreeRepairOperation(FreeRepairOperation freeRepairOperation) {
		this.freeRepairOperation = freeRepairOperation;
	}

	@OneToMany(mappedBy = "validatedFreeRepairOperation", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<ValidatedFreeRepairComponent> getValidatedFreeRepairComponents() {
		return validatedFreeRepairComponents;
	}

	public void setValidatedFreeRepairComponents(Set<ValidatedFreeRepairComponent> validatedFreeRepairComponents) {
		this.validatedFreeRepairComponents = validatedFreeRepairComponents;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "repaircompletionreportid", foreignKey = @ForeignKey(name = "validatedfreerepairoperation_repaircompletionreport_FK"))
	public RepairCompletionReport getRepairCompletionReport() {
		return repairCompletionReport;
	}

	public void setRepairCompletionReport(RepairCompletionReport repairCompletionReport) {
		this.repairCompletionReport = repairCompletionReport;
	}

}