package org.trescal.cwms.core.jobs.contractreview.entity.costs;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;


/**
 * Superclass that all linked contract review costs extends.
 * 
 * @author richard
 */
@MappedSuperclass
public abstract class ContractReviewLinkedCost
{
	private int id;

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	public void setId(int id)
	{
		this.id = id;
	}
}
