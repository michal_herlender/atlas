package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.account.entity.Ledgers;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction.TPInstruction;
import org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction.db.TPInstructionService;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.db.TPRequirementService;
import org.trescal.cwms.core.jobs.jobitem.form.JIThirdPartyForm;
import org.trescal.cwms.core.pricing.purchaseorder.dto.AjaxPurchaseOrderForm;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class JIThirdPartyController extends JobItemController {
	private final Logger logger = LoggerFactory.getLogger(JIFaultReportController.class);
	private static final String THIRD_PARTY_REQUIREMENTS = "Third party requirements recorded";

	@Value("#{props['cwms.config.quotation.resultsyearfilter']}")
	private Integer resultsYearFilter;
	@Autowired
	private ActionOutcomeService outcomeServ;
	@Autowired
	private DeliveryService delServ;
	@Autowired
	private ItemStateService itemStateService;
	@Autowired
	private TPInstructionService tpinServ;
	@Autowired
	private TPRequirementService tprServ;
	@Autowired
	private PurchaseOrderService purchaseOrderService;

	public void createNewTPRequirement(JIThirdPartyForm form, JobItem ji, Contact con) {
		TPRequirement tpr = new TPRequirement();
		tpr.setInvestigation(form.isInvestigation());
		tpr.setCalibration(form.isCalibration());
		tpr.setRepair(form.isRepair());
		tpr.setAdjustment(form.isAdjustment());
		tpr.setOutcome(this.outcomeServ.get(form.getActionOutcomeId()));
		tpr.setJi(ji);
		tpr.setCreated(new Date());
		tpr.setRecordedBy(con);
		this.tprServ.save(tpr);
	}

	@ModelAttribute("form")
	protected JIThirdPartyForm formBackingObject(HttpServletRequest request,
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") Integer jobitemid)
			throws Exception {
		JIThirdPartyForm form = new JIThirdPartyForm();
		JobItem ji = this.jobItemService.findJobItem(jobitemid);
		if ((ji == null) || (jobitemid == 0)) {
			logger.error("Jobitem with id " + jobitemid + " could not be found");
			throw new Exception("Unable to locate requested jobitem");
		}
		form.setJi(ji);
		form.setOtherItemsForTPReq(new ArrayList<Integer>());
		// set the comment type into the session for use by dwr
		HttpSession sess = request.getSession();
		sess.setAttribute("jitpinstructioncomment", PresetCommentType.THIRD_PARTY_INSTRUCTION);
		return form;
	}

	@ModelAttribute("purchaseOrderForm")
	protected AjaxPurchaseOrderForm getPurchaseOrderForm(HttpServletRequest request,
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") Integer jobitemid) {
		return this.purchaseOrderService.prepareAjaxPurchaseOrderForm(jobitemid, request);
	}

	@PreAuthorize("@hasJobItemAuthorityOnConnectedSubdiv.check('JI_THIRD_PARTY')")
	@RequestMapping(value = "/jithirdparty.htm", method = RequestMethod.POST)
	protected RedirectView onSubmit(@ModelAttribute("form") JIThirdPartyForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		JobItem ji = form.getJi();
		Contact con = this.userService.get(username).getCon();
		// create the tp req for the main job item
		this.createNewTPRequirement(form, ji, con);
		// has user supplied instructions?
		if ((form.getTpInstruction() != null) && (form.getTpInstruction() != "")) {
			// create new tpinstruction for job item
			TPInstruction tpin = new TPInstruction();
			// set values
			tpin.setActive(true);
			tpin.setJi(ji);
			tpin.setShowOnTPDelNote(true);
			tpin.setRecordedBy(con);
			tpin.setLastModified(new Date());
			tpin.setInstruction(form.getTpInstruction());
			// insert tp instruction
			this.tpinServ.save(tpin);
		}
		if (form.getOtherItemsForTPReq() != null) {
			// for each other item selected
			for (Integer jiId : form.getOtherItemsForTPReq()) {
				JobItem item = this.jobItemService.findJobItem(jiId);
				// create an identical tp req for the item
				this.createNewTPRequirement(form, item, con);
				// has user supplied instructions?
				if ((form.getTpInstruction() != null) && (form.getTpInstruction() != "")) {
					// create new tpinstruction for job item
					TPInstruction tpin = new TPInstruction();
					// set values
					tpin.setActive(true);
					tpin.setJi(item);
					tpin.setShowOnTPDelNote(true);
					tpin.setRecordedBy(con);
					tpin.setLastModified(new Date());
					tpin.setInstruction(form.getTpInstruction());
					// insert tp instruction
					this.tpinServ.save(tpin);
				}
			}
		}
		return new RedirectView("jithirdparty.htm?jobitemid=" + ji.getJobItemId());
	}

	@PreAuthorize("hasAuthority('JI_THIRD_PARTY')")
	@RequestMapping(value = "/jithirdparty.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@RequestParam(value = "jobitemid", required = true) Integer jobItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
        Contact contact = this.userService.get(username).getCon();
        JobItem jobItem = jobItemService.findJobItem(jobItemId);
        Map<String, Object> map = super.referenceData(jobItemId, contact, subdivDto.getKey(), username);
        // set up ledger list to be used on the page
        List<Ledgers> ledgers = Arrays.asList(Ledgers.PURCHASE_LEDGER, Ledgers.CAPITAL, Ledgers.UTILITY);
        map.put("ledgers", ledgers);
        map.put("resultsYearFilter", this.resultsYearFilter);
        ItemActivity activity = itemStateService.findItemActivityByName(THIRD_PARTY_REQUIREMENTS);
        map.put("actionoutcomes", this.outcomeServ.getActionOutcomesForActivity(activity));
        map.put("readyForRequirement", this.jobItemService.isReadyForTPRequirement(jobItem.getState()));
        map.put("deliveries", this.delServ.findByJobItem(jobItemId, DeliveryType.THIRDPARTY));
        List<JobItem> others = new ArrayList<JobItem>();
        for (JobItem item : jobItem.getJob().getItems()) {
            if ((item.getJobItemId() != jobItemId) && this.jobItemService.isReadyForTPRequirement(item.getState())) {
                others.add(item);
            }
        }
        map.put("others", others);
        return new ModelAndView("trescal/core/jobs/jobitem/jithirdparty", map);
    }
}