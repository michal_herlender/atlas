package org.trescal.cwms.core.jobs.repair.componentinventory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;

@Entity
@Table(name = "componentinventory")
public class ComponentInventory extends Auditable {

	private int componentInventoryId;
	private Subdiv businessSubdivId;
	private Company supplierCompanyId;
	private Component component;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getComponentInventoryId() {
		return componentInventoryId;
	}
	public void setComponentInventoryId(int componentInventoryId) {
		this.componentInventoryId = componentInventoryId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "businessSubdivId", foreignKey=@ForeignKey(name="componentinventory_subdiv_FK"))
	public Subdiv getBusinessSubdivId() {
		return businessSubdivId;
	}
	public void setBusinessSubdivId(Subdiv businessSubdivId) {
		this.businessSubdivId = businessSubdivId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "suppliercompanyid", foreignKey=@ForeignKey(name="componentinventory_company_FK"))
	public Company getSupplierCompanyId() {
		return supplierCompanyId;
	}
	public void setSupplierCompanyId(Company supplierCompanyId) {
		this.supplierCompanyId = supplierCompanyId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "componentid", foreignKey=@ForeignKey(name="componentinventory_component_FK"))
	public Component getComponent() {
		return component;
	}
	public void setComponent(Component component) {
		this.component = component;
	}
}