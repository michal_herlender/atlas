package org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.db;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.CalibrationPointSet;

@Repository("CalibrationPointSetDao")
public class CalibrationPointSetDaoImpl extends BaseDaoImpl<CalibrationPointSet, Integer> implements CalibrationPointSetDao {
	
	@Override
	protected Class<CalibrationPointSet> getEntity() {
		return CalibrationPointSet.class;
	}
	
	@Override
	public void deleteAll(List<CalibrationPointSet> calibrationpointset) {
		calibrationpointset.forEach(this::remove);
	}
}