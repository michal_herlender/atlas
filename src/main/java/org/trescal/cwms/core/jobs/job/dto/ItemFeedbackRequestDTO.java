package org.trescal.cwms.core.jobs.job.dto;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public class ItemFeedbackRequestDTO
{
	private ClientFeedbackRequestDTO cfrDTO;
	private JobItem ji;
	private String modelName;

	public ItemFeedbackRequestDTO()
	{

	}

	public ItemFeedbackRequestDTO(ClientFeedbackRequestDTO cfrDTO, JobItem ji)
	{
		this.cfrDTO = cfrDTO;
		this.ji = ji;
	}

	public ClientFeedbackRequestDTO getCfrDTO()
	{
		return this.cfrDTO;
	}

	public JobItem getJi()
	{
		return this.ji;
	}

	public void setCfrDTO(ClientFeedbackRequestDTO cfrDTO)
	{
		this.cfrDTO = cfrDTO;
	}

	public void setJi(JobItem ji)
	{
		this.ji = ji;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

}
