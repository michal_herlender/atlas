package org.trescal.cwms.core.jobs.repair.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationStateEnum;
import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationTypeEnum;

import lombok.Data;

@Data
public class FreeRepairOperationDTO {

	private Integer frOperationId;
	@NotNull
	private Integer position;
	@Size(min = 2, max = 100)
	private String name;
	@NotNull
	private RepairOperationTypeEnum type;
	@NotNull
	private Integer labourTimeMinutes;
	@NotNull
	private Integer labourTimeHours;
	private BigDecimal cost;
	@NotNull
	private RepairOperationStateEnum status;
	private RepairOperationStateEnum rirStatus;

	private Boolean addedAfterRiRValidation;
	private Integer repairInspectionReportId;
	private Integer validatedFreeRepairOperationId;
	private Integer coid;
	private String coName;
}
