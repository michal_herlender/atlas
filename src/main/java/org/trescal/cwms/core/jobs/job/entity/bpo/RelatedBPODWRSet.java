package org.trescal.cwms.core.jobs.job.entity.bpo;

import java.util.List;

import org.trescal.cwms.core.jobs.job.dto.DWRBpoDTO;

import lombok.Data;

/**
 * Marshalling object only for RelatedBPOSet usage with DWR until it could be replaced
 * see BPOServiceImpl::getAllBPOsRelatedToJob(...)
 * see searchBPO function in ContactSearchPlugin.js
 * <p>
 * Delete this class when contact search plugin is replaced...
 */
@Data
public class RelatedBPODWRSet {
    private String companyName;
    private String subdivName;
    private String contactName;

    private List<DWRBpoDTO> companyBPOs;
    private List<DWRBpoDTO> subdivBPOs;
    private List<DWRBpoDTO> contactBPOs;
    private String currencyERSymbol;
    private int size;

}
