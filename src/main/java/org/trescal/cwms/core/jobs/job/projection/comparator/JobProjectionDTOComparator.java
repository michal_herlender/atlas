package org.trescal.cwms.core.jobs.job.projection.comparator;

import java.util.Comparator;

import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;

/**
 * General purpose JobProjectionDTO comparator.
 * Compares by regDate, then jobno, then jobid.
 * All are mandatory fields in database
 *
 */
public class JobProjectionDTOComparator implements Comparator<JobProjectionDTO> {

	@Override
	public int compare(JobProjectionDTO dto1, JobProjectionDTO dto2) {
		int result = dto1.getRegDate().compareTo(dto2.getRegDate());
		if (result == 0) {
			result = dto1.getJobno().compareTo(dto2.getJobno());
			if (result == 0) {
				result = dto1.getJobid().compareTo(dto2.getJobid());
			}
		}
		
		return result;
	}

}
