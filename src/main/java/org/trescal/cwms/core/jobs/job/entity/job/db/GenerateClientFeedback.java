package org.trescal.cwms.core.jobs.job.entity.job.db;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.jobs.job.controller.GenerateClientFeedbackRequestIn;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.system.dto.EmailResultWrapper;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_ContractReviewFeedback;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.emailrecipient.RecipientType;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.form.EmailForm;

import io.vavr.control.Either;
import lombok.val;

@Service
public class GenerateClientFeedback {

    @Autowired
    private JobService jobService;
    @Autowired
    private BusinessDetailsService businessDetailsService;
    @Autowired
    private EmailContent_ContractReviewFeedback emailContentContractReviewFeedback;
    @Autowired
    private ContactService contactService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private MessageSource messages;
    @Autowired
    private SessionUtilsService sessionService;

    public Either<String, EmailResultWrapper> apply(GenerateClientFeedbackRequestIn in, Contact contact) {
        if (in.getItems().isEmpty() && StringUtils.isBlank(in.getBody()))
            return Either.left(messages.getMessage("error.contractreview.feedbackrequest.noitems",
                    null, "You must select one or more items", null));

        val job = Optional.ofNullable(jobService.get(in.getJobId())).<Either<String,Job>>map(Either::right)
                .orElseGet(() -> Either.left(messages.getMessage("error.job.notfound", null, "Could not find job", LocaleContextHolder.getLocale())));
        val businessDetails = this.businessDetailsService.getAllBusinessDetails(contact, null, null);

        Either<String, Email> maybeEmail = (StringUtils.isNotBlank(in.getBody()) && StringUtils.isNotBlank(in.getSubject()) ?
                Either.<String, Email.EmailBuilder>right(Email.builder().body(in.getBody()).subject(in.getSubject())) :
                job.map(j -> {
                            val content = emailContentContractReviewFeedback.getContent(j, in.getItems(), j.getCon().getLocale());
                            val body = StringUtils.isNotBlank(in.getBody()) ? in.getBody() : content.getBody();
                            val subject = StringUtils.isNotBlank(in.getSubject()) ? in.getSubject() : content.getSubject();
                            return Email.builder().body(body).subject(subject);
                        }
                ))
                .map(builder ->
                        builder.sentBy(contact).from(businessDetails.getSalesEmail())
                                .sentOn(new Date()).component(Component.JOB).entityId(in.getJobId()).build())
                .peek(email -> processContactsForFeedbackEmail(in, email));

        return in.getSend() ?
                maybeEmail.flatMap(email -> prepareAndSendFeedbackEmail(in.getJobId(), businessDetails, job, email)) :
                maybeEmail.map(email -> EmailResultWrapper.builder()
                        .email(email).body(email.getBody()).build());
    }

    private void processContactsForFeedbackEmail(GenerateClientFeedbackRequestIn in, Email email) {
        val recipientsFromEmails = emailRecipientCollectorFromEmails(email);
        val recipientsFromIds = emailRecipientCollectorFromContactIds(email);
        val currentContact = Optional.of(in.getIncludeSelfInCarbonCopy()).filter(c -> c).flatMap(c -> Optional.ofNullable(sessionService.getCurrentContact())).map(Stream::of).orElseGet(Stream::empty);

        val recipients = Stream.of(
                recipientsFromEmails.apply(RecipientType.EMAIL_TO).apply(in.getToEmails()),
                recipientsFromIds.apply(RecipientType.EMAIL_TO).apply(in.getToIds()),
                recipientsFromEmails.apply(RecipientType.EMAIL_CC).apply(in.getCcEmails()),
                recipientsFromIds.apply(RecipientType.EMAIL_CC).apply(in.getCcIds()),
                currentContact.map(c -> recipientFromContact(c, RecipientType.EMAIL_CC, email))
        ).flatMap(Function.identity()).collect(Collectors.toList());

        email.setRecipients(recipients);
    }


    Function<RecipientType, Function<List<String>, Stream<EmailRecipient>>> emailRecipientCollectorFromEmails(Email email) {
        return type -> list -> list.stream().map(address ->
                EmailRecipient.builder()
                        .email(email)
                        .emailAddress(address)
                        .type(type)
                        .build()
        );
    }

    Function<RecipientType, Function<List<Integer>, Stream<EmailRecipient>>> emailRecipientCollectorFromContactIds(Email email) {
        return type -> list -> list.stream().map(contactService::get)
                .filter(Objects::nonNull)
                .map(con -> recipientFromContact(con, type, email));
    }

    private EmailRecipient recipientFromContact(Contact contact, RecipientType type, Email email) {
        return EmailRecipient.builder()
                .recipientCon(contact)
                .emailAddress(contact.getEmail())
                .type(type)
                .email(email)
                .build();
    }

    private Either<String, EmailResultWrapper> prepareAndSendFeedbackEmail(Integer jobId, BusinessDetails businessDetails, Either<String, Job> job, Email email) {
        Either<String, EmailForm> emailForm = job.map(j -> EmailForm.builder()
                .email(email).component(Component.JOB).entityId(jobId).from(businessDetails.getSalesEmail())
                .recipients(email.getRecipients())
                .toCon(j.getCon()).toEmail(j.getCon().getEmail()).subject(email.getSubject()).body(email.getBody())
                .build()
        );

        return emailForm.filterOrElse(form -> emailService.sendEmailUsingForm(form, Collections.emptyList()),
                form -> "The feedback request email could not be sent")
                .map(form -> EmailResultWrapper.builder().send(true)
                        .email(form.getEmail()).body(form.getEmail().getBody()).build());
    }

}
