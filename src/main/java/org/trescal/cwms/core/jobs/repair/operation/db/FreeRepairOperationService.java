package org.trescal.cwms.core.jobs.repair.operation.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;

public interface FreeRepairOperationService extends BaseService<FreeRepairOperation, Integer>
{

	void refrech(FreeRepairOperation fro);
	
}