package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink;

public interface GsoLinkDao extends BaseDao<GsoLink, Integer> {
	
}
