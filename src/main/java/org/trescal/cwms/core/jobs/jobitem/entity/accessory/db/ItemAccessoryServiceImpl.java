package org.trescal.cwms.core.jobs.jobitem.entity.accessory.db;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.jobitem.dto.ItemAccessoryDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory;

@Service("itemAccessoryService")
public class ItemAccessoryServiceImpl extends BaseServiceImpl<ItemAccessory, Integer> implements ItemAccessoryService {

	@Autowired
	private ItemAccessoryDao itemAccessoryDao;

	@Override
	protected BaseDao<ItemAccessory, Integer> getBaseDao() {
		return itemAccessoryDao;
	}

	@Override
	public Integer countByJob(Integer jobId) {
		return this.itemAccessoryDao.countByJob(jobId) + this.itemAccessoryDao.countFreeTextAccessoriesByJob(jobId);
	}

	@Override
	public List<ItemAccessoryDTO> getAllByJob(Integer jobId, Locale locale) {
		return this.itemAccessoryDao.getAllByJob(jobId, locale);
	}
}