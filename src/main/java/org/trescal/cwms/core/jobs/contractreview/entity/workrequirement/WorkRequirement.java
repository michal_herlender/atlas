package org.trescal.cwms.core.jobs.contractreview.entity.workrequirement;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity representing a unit of work required for one or more {@link JobItem}s.
 *
 * 2018-06-20 (Galen Beck) - changes made as part of capability evolutions
 *
 * (a) Originally, the WorkRequirement would only exist on a JobItem, linked by
 * JobItemWorkRequirement, however it's now possible to define a set of
 * WorkRequirements that form a WorkProgram, to predefine a set of operations to
 * perform.
 * 
 * (b) Simplified design to remove WorkRequirementProcedure and set single
 * reference to procedure on WorkRequirement
 * 
 * (c) Added address (of entity performing the work) and service type reference
 */
@Entity
@Table(name = "workrequirement")
public class WorkRequirement extends Auditable {
    // only allow a single lab
    private Department department;
    private Integer id;
    private String requirement;
    private WorkRequirementType type;
    // only allow a single work instruction
    private WorkInstruction workInstruction;
    // 2018-06-20 - service type added, existing data defaults to job item
    // service type
    private ServiceType serviceType;
    // 2018-06-20 - changed to have a single (optional) procedure
    private Capability capability;
    // 2018-06-20 - indicates the Address of the entity (business, supplier)
    // performing the work
    private Address address;
    // There will be at most one JobItemWorkRequirement link, but @OneToMany
    // avoids auto-fetching
    private Set<JobItemWorkRequirement> jobItemWorkRequirements;
    // work requirement status with three possible values : PENDING, CANCELLED, COMPLETE
    private WorkrequirementStatus status;

    public WorkRequirement() {
        this.jobItemWorkRequirements = new HashSet<>();
    }

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deptid")
	public Department getDepartment() {
		return this.department;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getId() {
		return this.id;
	}

	@Length(max = 500)
	@Column(name = "requirement", length = 500)
	public String getRequirement() {
        return this.requirement;
    }

    // TODO consider @NotNull if all can be populated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "servicetypeid", nullable = true, foreignKey = @ForeignKey(name = "FK_workrequirement_servicetype"))
    public ServiceType getServiceType() {
        return serviceType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "capabilityId", foreignKey = @ForeignKey(name = "FK_workrequirement_procedure"))
    public Capability getCapability() {
        return capability;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "addressid", nullable = true, foreignKey = @ForeignKey(name = "FK_workrequirement_address"))
    public Address getAddress() {
        return address;
    }

    @NotNull
    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
	public WorkRequirementType getType() {
		return this.type;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "wiid")
	public WorkInstruction getWorkInstruction() {
		return this.workInstruction;
	}

	@Deprecated
	@Transient
	public boolean isComplete() {
		return WorkrequirementStatus.COMPLETE.equals(this.status);
	}

	@OneToMany(mappedBy = "workRequirement", cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<JobItemWorkRequirement> getJobItemWorkRequirements() {
		return jobItemWorkRequirements;
	}

	@NotNull
	@Enumerated(EnumType.ORDINAL)
	public WorkrequirementStatus getStatus() {
		return status;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public void setId(Integer id) {
		this.id = id;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public void setType(WorkRequirementType type) {
        this.type = type;
    }

    public void setWorkInstruction(WorkInstruction workInstruction) {
        this.workInstruction = workInstruction;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public void setCapability(Capability capability) {
        this.capability = capability;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setJobItemWorkRequirements(Set<JobItemWorkRequirement> jobItemWorkRequirements) {
        this.jobItemWorkRequirements = jobItemWorkRequirements;
    }

    public void setStatus(WorkrequirementStatus status) {
        this.status = status;
    }

}
