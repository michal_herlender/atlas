package org.trescal.cwms.core.jobs.job.entity.jobbpolink.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink;

public interface JobBPOLinkDao extends BaseDao<JobBPOLink, Integer> {
	JobBPOLink findByJobIdAndBpoId(Integer jobId, Integer bpoId);
}