package org.trescal.cwms.core.jobs.calibration.entity.callink;

import java.util.Comparator;

public class CalibrationToCalLinkComparator implements Comparator<CalLink>
{
	@Override
	public int compare(CalLink o1, CalLink o2)
	{
		if ((o1.isMainItem() && !o2.isMainItem())
				|| (!o1.isMainItem() && o2.isMainItem()))
		{
			if (o1.isMainItem())
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}
		if (compareByJobItemID(o1, o2) == 0) return compareByID(o1, o2);

		if (o1.getJi().getJob().getJobid() == o2.getJi().getJob().getJobid())
		{
			return ((Integer) o1.getJi().getItemNo()).compareTo(o2.getJi().getItemNo());
		}
		else
		{
			return ((Integer) o1.getJi().getJob().getJobid()).compareTo(o2.getJi().getJob().getJobid());
		}
	}
	private int compareByID(CalLink o1, CalLink o2) {
		return o1.getId() - o2.getId();
	}
	private int compareByJobItemID(CalLink o1, CalLink o2) {
		return o1.getJi().getJobItemId() - o2.getJi().getJobItemId();
	}
}
