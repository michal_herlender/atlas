package org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.CalibrationPoint;

@Service("CalibrationPointService")
public class CalibrationPointServiceImpl implements CalibrationPointService
{
	@Autowired
	CalibrationPointDao CalibrationPointDao;

	@Override
	public void deleteAll(Collection<CalibrationPoint> points)
	{
		this.CalibrationPointDao.deleteAll(points);
	}

	public void deleteCalibrationPoint(CalibrationPoint calibrationpoint)
	{
		this.CalibrationPointDao.remove(calibrationpoint);
	}

	public CalibrationPoint findCalibrationPoint(int id)
	{
		return this.CalibrationPointDao.find(id);
	}

	public List<CalibrationPoint> getAllCalibrationPoints()
	{
		return this.CalibrationPointDao.findAll();
	}

	public void insertCalibrationPoint(CalibrationPoint CalibrationPoint)
	{
		this.CalibrationPointDao.persist(CalibrationPoint);
	}

	public void saveOrUpdateCalibrationPoint(CalibrationPoint calibrationpoint)
	{
		this.CalibrationPointDao.saveOrUpdate(calibrationpoint);
	}

	public void setCalibrationPointDao(CalibrationPointDao CalibrationPointDao)
	{
		this.CalibrationPointDao = CalibrationPointDao;
	}

	@Override
	public List<String> trimCalPointsList(List<String> points)
	{
		List<String> finalPoints = new ArrayList<String>();
		if ((points != null) && !(points.size() < 1))
		{
			int lastVal = -1;
			boolean valueFound = false;

			// find the last entry with any value and trim all remaining
			for (int i = points.size() - 1; i >= 0; i--)
			{
				String point = points.get(i);
				if (!valueFound && point.trim().equals(""))
				{
					lastVal = i;
				}
				else
				{
					valueFound = true;
				}
			}

			if (valueFound)
			{
				finalPoints = points;
				if (lastVal > -1)
				{
					finalPoints = finalPoints.subList(0, lastVal);
				}
			}
		}
		return finalPoints;
	}

	public void updateCalibrationPoint(CalibrationPoint CalibrationPoint)
	{
		this.CalibrationPointDao.update(CalibrationPoint);
	}
}