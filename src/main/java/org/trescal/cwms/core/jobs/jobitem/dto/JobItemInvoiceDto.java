package org.trescal.cwms.core.jobs.jobitem.dto;

import java.time.LocalDate;

import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;

import lombok.Data;

@Data
public class JobItemInvoiceDto {

	// From Job Item
	private Integer jobItemId;
	private Integer jobItemNo;
	// From Job
	private LocalDate jobDateComplete;
	private Integer jobId;
	private String jobNo;
	// From Job -> Contact
	private String jobContactFirstName;
	private String jobContactLastName;
	// From Job -> Contact -> Subdiv -> Company
	private Integer companyId;
	private String companyName;
	private CompanyRole companyRole;
	// From Job -> Status
	private Boolean jobComplete;
	private String subdivName;
	// for standard jobs
	private LocalDate deliveryDate;
	private JobType jobType;

	// Default constructor needed for AliasToBeanTransformer in old Criteria
	// queries
	public JobItemInvoiceDto() {
		super();
	}

	// Used for CriteriaBuilder projection
	public JobItemInvoiceDto(Integer jobItemId, Integer jobItemNo, LocalDate jobDateComplete, Integer jobId,
			String jobNo, String jobContactFirstName, String jobContactLastName, Integer companyId, String companyName,
			CompanyRole companyRole, Boolean jobComplete, String subdivName, LocalDate deliveryDate, JobType type) {
		this.jobItemId = jobItemId;
		this.jobItemNo = jobItemNo;
		this.jobDateComplete = jobDateComplete;
		this.jobId = jobId;
		this.jobNo = jobNo;
		this.jobContactFirstName = jobContactFirstName;
		this.jobContactLastName = jobContactLastName;
		this.companyId = companyId;
		this.companyName = companyName;
		this.companyRole = companyRole;
		this.jobComplete = jobComplete;
		this.subdivName = subdivName;
		this.deliveryDate = deliveryDate;
		this.jobType = type;
	}
}