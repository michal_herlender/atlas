package org.trescal.cwms.core.jobs.calibration.entity.callink.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalibrationActivityLink;

@Service()
public class CalibrationActivityLinkServiceImpl implements CalibrationActivityLinkService {
	
	@Autowired
	CalibrationActivityLinkDao dao;
	
	@Override
	public void persist(CalibrationActivityLink link){
		dao.persist(link);
	}
	
}