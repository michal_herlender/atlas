package org.trescal.cwms.core.jobs.certificate.form;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class CertificateSearchValidator extends AbstractBeanValidator
{
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(CertificateSearchForm.class);
	}
	
	public void validate(Object obj, Errors errors)
	{
		CertificateSearchForm form = (CertificateSearchForm) obj;
		super.validate(form, errors);

		/**
		 * BUSINESS VALIDATION
		 */
		if (form.isIssueDateBetween())
		{
			// if first date is given
			if (form.getIssueDate1() != null)
			{
				// check second date is given
				if (form.getIssueDate2() == null)
				{
					errors.rejectValue("issueDate2", "issueDate2", null, "Second issue date has not been specified.");
				}
				else
				{
					// check second date is chronologically after the first date
					Calendar cal1 = new GregorianCalendar();
					cal1.setTime(form.getIssueDate1());
					Calendar cal2 = new GregorianCalendar();
					cal2.setTime(form.getIssueDate2());

					if (!cal2.after(cal1))
					{
						errors.rejectValue("issueDate2", "issueDate2", null, "Second issue date must be after the first issue date.");
					}
				}
			}
		}
		if (form.isCalDateBetween())
		{
			// if first date is given
			if (form.getCalDate1() != null)
			{
				// check second date is given
				if (form.getCalDate2() == null)
				{
					errors.rejectValue("calDate2", "calDate2", null, "Second calibration date has not been specified.");
				}
				else
				{
					// check second date is chronologically after the first date
					Calendar cal1 = new GregorianCalendar();
					cal1.setTime(form.getCalDate1());
					Calendar cal2 = new GregorianCalendar();
					cal2.setTime(form.getCalDate2());

					if (!cal2.after(cal1))
					{
						errors.rejectValue("calDate2", "calDate2", null, "Second calibration date must be after the first calibration date.");
					}
				}
			}
		}
		if ((form.getBc() != null))
		{
			if (!form.getBc().trim().isEmpty())
			{
				// check that it converts into a number
				if (!NumberTools.isAnInteger(form.getBc().trim()))
				{
					errors.rejectValue("bc", "bc", null, "The barcode given must be a number.");
				}
			}

		}
	}
}