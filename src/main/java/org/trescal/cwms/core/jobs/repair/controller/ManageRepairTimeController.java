package org.trescal.cwms.core.jobs.repair.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.jobs.repair.dto.RepairTimeDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairTimeForm;
import org.trescal.cwms.core.jobs.repair.dto.form.validator.RepairTimeValidator;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.db.RepairCompletionReportService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateTimeEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class ManageRepairTimeController {

	@Autowired
	private JobItemActionService jiActionService;
	@Autowired
	private UserService userService;
	@Autowired
	private RepairCompletionReportService rcrService;
	@Autowired
	private RepairTimeValidator validator;
	@Autowired
	private JobItemService jiService;
	
	@InitBinder("form")
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		binder.setValidator(validator);
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.dtf_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDateTime.class, new CustomLocalDateTimeEditor(DateTimeFormatter.ISO_DATE_TIME));
	}

	@ModelAttribute("form")
	protected RepairTimeForm repairTimeForm(@RequestParam(value = "rcrid") Integer rcrid,
											@RequestParam(value = "jobitemid") Integer jobitemid,
											@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName, Model model) {
		RepairTimeForm form = new RepairTimeForm();
		referenceData(form, rcrid, jobitemid, userName, model);
		form.setAction("ADD");

		return form;
	}
	
	private void referenceData(RepairTimeForm form, Integer rcrid, Integer jobitemid, String userName, Model model) {
		Contact contact = this.userService.get(userName).getCon();
		form.setTechnicianId(contact.getId());
		form.setTechnicianName(contact.getName());
		
		form.setRepairCompletionReportId(rcrid);
		form.setJobItemId(jobitemid);
		
		RepairCompletionReport rcr = this.rcrService.get(rcrid);
		form.setRepairCompletionReportIdentifier(rcr.getIdentifier());
		
		List<RepairTimeDTO> repairTimes = this.jiActionService.findTechnicianRepairTimeByRcr(jobitemid, rcr.getIdentifier(), null);
		this.rcrService.preparRepairTime(rcr, repairTimes, contact);
		form.setRepairTimes(repairTimes);
		model.addAttribute("actionPerformed", false);
	}

	@RequestMapping(value = "/managerepairtimeoverlay.htm", method = RequestMethod.GET)
	public String doGet(@ModelAttribute("form") RepairTimeForm form) {

		return "trescal/core/jobs/repair/managerepairtime";
	}
	
	@RequestMapping(value = "/managerepairtimeoverlay.htm", method = RequestMethod.POST)
	public String doPost(@Validated @ModelAttribute("form") RepairTimeForm form, BindingResult result,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName, Model model) {

		if(!result.hasErrors()) {
			// check if last jobitem action was not completed
			JobItem ji = this.jiService.findJobItem(form.getJobItemId());
			if(ji != null && ji.getActions().stream().anyMatch(a -> a.getEndStatus() == null))
				result.reject("repairtime.validator.lastactivitynotcompleted", new Object[]{ ji.getItemNo(), ji.getJob().getJobno() }, "Activity already in progress for item "+ji.getItemNo()+" : (Job "+ji.getJob().getJobno()+").");
		}
		// if everythings is ok
		if(!result.hasErrors()) {
			
			this.jiActionService.manageRepairTimeProgressActivities(form);
			
			referenceData(form, form.getRepairCompletionReportId(), form.getJobItemId(), userName, model);
			model.addAttribute("actionPerformed", true);
		}
		
		return "trescal/core/jobs/repair/managerepairtime";
	}

}
