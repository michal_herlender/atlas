package org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.job.dto.ClientPurchaseOrderItemDTO;
import org.trescal.cwms.core.jobs.job.projection.ClientPurchaseOrderProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.logistics.dto.PoDTO;

public interface JobItemPODao extends BaseDao<JobItemPO, Integer> {

	void deleteBPOLinksToJob(int bpoid, int jobid);

	List<JobItem> getItemsOnPO(int poId);

	List<JobItem> getItemsOnBPO(int bpoId, int jobId);

	List<ClientPurchaseOrderProjectionDTO> getProjectionDTOs(Collection<Integer> jobIds,
			Collection<Integer> jobItemIds);

	String getAllPOsByJobItem(int jobItemId);

	List<ClientPurchaseOrderItemDTO> getOnPO(Integer poId, Locale locale);
	
	List<ClientPurchaseOrderItemDTO> getOnBPO(Integer poId, Integer jobId, Locale locale);

	List<PoDTO> getValidPODtosForJobItem(Integer jobItemId);
	
	List<Integer> getJobitemIdsOnBPO(Integer bpoid, Integer jobId);
	
	JobItemPO getJobItemPo(Integer jobItemId, int poId, Integer jobId);
	
	
}