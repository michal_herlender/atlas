package org.trescal.cwms.core.jobs.job.entity.jobnote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;

@Entity
@Table(name = "jobnote")
public class JobNote extends Note
{
	private Job job;

	public final static boolean privateOnly = true;

	public JobNote()
	{
		super();
	}

	/**
	 * @return the job
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobid")
	public Job getJob()
	{
		return job;
	}

	@Override
	@Transient
	public NoteType getNoteType()
	{
		return NoteType.JOBNOTE;
	}
	
	/**
	 * @param job the job to set
	 */
	public void setJob(Job job)
	{
		this.job = job;
	}
}