package org.trescal.cwms.core.jobs.calibration.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@JsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE })
public class ActiveJobsByClientJsonController {

	@Autowired
	private SubdivService subdivService;
	@Autowired
	private JobService jobService;

	@RequestMapping(value = "/activeJobsByClientJsonController.json", method = RequestMethod.GET)
	@ResponseBody
	public List<KeyValue<Integer, String>> getAllAnalysisResults(@RequestParam(required = true) Integer clientSubdiv,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {

		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Subdiv subdiv = subdivService.get(clientSubdiv);
		return jobService.getActiveJobsBySubdivAndAllocatedSubdiv(subdiv, allocatedSubdiv).stream().filter(j -> {
			return j.getType().equals(JobType.SITE);
		}).map(j -> new KeyValue<>(j.getJobid(), j.getJobno())).collect(Collectors.toList());
	}

}
