package org.trescal.cwms.core.jobs.certificate.entity.certnote.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.certificate.entity.certnote.CertNote;

public interface CertNoteDao extends BaseDao<CertNote, Integer> {}