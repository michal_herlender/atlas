package org.trescal.cwms.core.jobs.jobitem.form;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.UserCertWrap;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;

public class JICertificatesForm extends JobItemForm
{
	private String action;
	private int actionForCertId;
	private Integer actionOutcomeId;
	private Certificate c;
	private int calProId;
	private int calTypeId;
	private List<CalibrationType> calTypes;
	private SystemComponent component;
	private String confirmPassword;
	private Integer currentContactId;
	private String encPassword;
	private MultipartFile file;
	private String note;
	private List<Integer> otherJobItems;
	private boolean procTrainingRecord;
	private boolean pwExists;
	private Integer subdivid;
	private String template;
	private List<UserCertWrap> userCertWraps;
	private String durationUnitId;
	private CalibrationVerificationStatus calibrationVerificationStatus;
    private BigDecimal deviation;
    private Integer supplementaryForId;
    private String supplementaryForComment;

	public String getAction()
	{
		return this.action;
	}

	public int getActionForCertId()
	{
		return this.actionForCertId;
	}

	public Integer getActionOutcomeId()
	{
		return this.actionOutcomeId;
	}

	/**
	 * @return the c
	 */
	@Valid
	public Certificate getC()
	{
		return this.c;
	}

	/**
	 * @return the calProId
	 */
	public int getCalProId()
	{
		return this.calProId;
	}

	/**
	 * @return the calTypeId
	 */
	public int getCalTypeId()
	{
		return this.calTypeId;
	}

	/**
	 * @return the calTypes
	 */
	public List<CalibrationType> getCalTypes()
	{
		return this.calTypes;
	}

	/**
	 * @return the component
	 */
	public SystemComponent getComponent()
	{
		return this.component;
	}

	public String getConfirmPassword()
	{
		return this.confirmPassword;
	}

	public Integer getCurrentContactId()
	{
		return this.currentContactId;
	}

	public String getEncPassword()
	{
		return this.encPassword;
	}

	public MultipartFile getFile()
	{
		return this.file;
	}

	/**
	 * @return the note
	 */
	public String getNote()
	{
		return this.note;
	}

	/**
	 * @return the otherJobItems
	 */
	public List<Integer> getOtherJobItems()
	{
		return this.otherJobItems;
	}

	/**
	 * @return the subdivid
	 */
	public Integer getSubdivid()
	{
		return this.subdivid;
	}

	public String getTemplate()
	{
		return this.template;
	}

	public List<UserCertWrap> getUserCertWraps()
	{
		return this.userCertWraps;
	}

	public boolean isProcTrainingRecord()
	{
		return this.procTrainingRecord;
	}

	public boolean isPwExists()
	{
		return this.pwExists;
	}

	public void setAction(String action)
	{
		this.action = action;
	}

	public void setActionForCertId(int actionForCertId)
	{
		this.actionForCertId = actionForCertId;
	}

	public void setActionOutcomeId(Integer actionOutcomeId)
	{
		this.actionOutcomeId = actionOutcomeId;
	}

	/**
	 * @param c the c to set
	 */
	public void setC(Certificate c)
	{
		this.c = c;
	}

	/**
	 * @param calProId the calProId to set
	 */
	public void setCalProId(int calProId)
	{
		this.calProId = calProId;
	}

	/**
	 * @param calTypeId the calTypeId to set
	 */
	public void setCalTypeId(int calTypeId)
	{
		this.calTypeId = calTypeId;
	}

	/**
	 * @param calTypes the calTypes to set
	 */
	public void setCalTypes(List<CalibrationType> calTypes)
	{
		this.calTypes = calTypes;
	}

	/**
	 * @param component the component to set
	 */
	public void setComponent(SystemComponent component)
	{
		this.component = component;
	}

	public void setConfirmPassword(String confirmPassword)
	{
		this.confirmPassword = confirmPassword;
	}

	public void setCurrentContactId(Integer currentContactId)
	{
		this.currentContactId = currentContactId;
	}

	public void setEncPassword(String encPassword)
	{
		this.encPassword = encPassword;
	}

	public void setFile(MultipartFile file)
	{
		this.file = file;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note)
	{
		this.note = note;
	}

	/**
	 * @param otherJobItems the otherJobItems to set
	 */
	public void setOtherJobItems(List<Integer> otherJobItems)
	{
		this.otherJobItems = otherJobItems;
	}

	public void setProcTrainingRecord(boolean procTrainingRecord)
	{
		this.procTrainingRecord = procTrainingRecord;
	}

	public void setPwExists(boolean pwExists)
	{
		this.pwExists = pwExists;
	}

	/**
	 * @param subdivid the subdivid to set
	 */
	public void setSubdivid(Integer subdivid)
	{
		this.subdivid = subdivid;
	}

	public void setTemplate(String template)
	{
		this.template = template;
	}

	public void setUserCertWraps(List<UserCertWrap> userCertWraps)
	{
		this.userCertWraps = userCertWraps;
	}

	public String getDurationUnitId() {
		return durationUnitId;
	}

	public void setDurationUnitId(String durationUnitId) {
		this.durationUnitId = durationUnitId;
	}

	public CalibrationVerificationStatus getCalibrationVerificationStatus() {
		return calibrationVerificationStatus;
	}

	public void setCalibrationVerificationStatus(CalibrationVerificationStatus calibrationVerificationStatus) {
		this.calibrationVerificationStatus = calibrationVerificationStatus;
	}

    public void setDeviation(BigDecimal deviation) {
        this.deviation = deviation;
    }

    public BigDecimal getDeviation() {
        return deviation;
    }

	public Integer getSupplementaryForId() {
		return supplementaryForId;
	}

	public void setSupplementaryForId(Integer supplementaryForId) {
		this.supplementaryForId = supplementaryForId;
	}

	public String getSupplementaryForComment() {
		return supplementaryForComment;
	}

	public void setSupplementaryForComment(String supplementaryForComment) {
		this.supplementaryForComment = supplementaryForComment;
	}
    
}