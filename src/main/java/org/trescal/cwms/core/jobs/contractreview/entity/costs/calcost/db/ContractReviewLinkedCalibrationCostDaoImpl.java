package org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewLinkedCalibrationCost;

@Repository
public class ContractReviewLinkedCalibrationCostDaoImpl extends BaseDaoImpl<ContractReviewLinkedCalibrationCost, Integer> implements ContractReviewLinkedCalibrationCostDao {
	
	@Override
	protected Class<ContractReviewLinkedCalibrationCost> getEntity() {
		return ContractReviewLinkedCalibrationCost.class;
	}
}