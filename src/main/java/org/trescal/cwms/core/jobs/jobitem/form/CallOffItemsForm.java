package org.trescal.cwms.core.jobs.jobitem.form;

import java.util.EnumSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public class CallOffItemsForm
{
	private Action action;
	private Integer coid;
	private Integer allocatedSubdivId;	// Needed for validation
	private List<Integer> itemIds;
	
	public enum Action {
		REACTIVATE_ITEMS("calloffitems.action.reactivate"), NEW_JOB("calloffitems.action.newjob");
		private String messageCode;
		private MessageSource messageSource;
		
		private Action(String messageCode) {
			this.messageCode = messageCode;
		}
		public void setMessageSource(MessageSource messageSource) {
			this.messageSource = messageSource;
		}
		public String getName() {
			return messageSource.getMessage(messageCode, null, LocaleContextHolder.getLocale());
		}
	}
	@Component
	public static class CallOffItemMessageSourceInjector
	{
		@Autowired
		private MessageSource messageSource;
		
		@PostConstruct
        public void postConstruct() {
            for (Action action : EnumSet.allOf(Action.class))
               action.setMessageSource(messageSource);
        }
	}

	/**
	 * @return the coid
	 */
	@NotNull
	public Integer getCoid()
	{
		return this.coid;
	}

	/**
	 * @return the itemIds
	 */
	public List<Integer> getItemIds()
	{
		return this.itemIds;
	}

	/**
	 * @param coid the coid to set
	 */
	public void setCoid(Integer coid)
	{
		this.coid = coid;
	}

	/**
	 * @param itemIds the itemIds to set
	 */
	public void setItemIds(List<Integer> itemIds)
	{
		this.itemIds = itemIds;
	}

	@NotNull(message="{error.value.notselected}")
	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	@NotNull
	public Integer getAllocatedSubdivId() {
		return allocatedSubdivId;
	}

	public void setAllocatedSubdivId(Integer allocatedSubdivId) {
		this.allocatedSubdivId = allocatedSubdivId;
	}
}