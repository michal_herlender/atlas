package org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.db;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;

public class ContractReviewItemServiceImpl implements ContractReviewItemService
{
	private ContractReviewItemDao contractReviewItemDao;

	public ContractReviewItem findContractReviewItem(int id)
	{
		return contractReviewItemDao.find(id);
	}

	public void insertContractReviewItem(ContractReviewItem ContractReviewItem)
	{
		contractReviewItemDao.persist(ContractReviewItem);
	}

	public void updateContractReviewItem(ContractReviewItem ContractReviewItem)
	{
		contractReviewItemDao.update(ContractReviewItem);
	}

	public List<ContractReviewItem> getAllContractReviewItems()
	{
		return contractReviewItemDao.findAll();
	}

	public void setContractReviewItemDao(ContractReviewItemDao ContractReviewItemDao)
	{
		this.contractReviewItemDao = ContractReviewItemDao;
	}

	public void deleteContractReviewItem(ContractReviewItem contractreviewitem)
	{
		this.contractReviewItemDao.remove(contractreviewitem);
	}

	public void saveOrUpdateContractReviewItem(ContractReviewItem contractreviewitem)
	{
		this.contractReviewItemDao.saveOrUpdate(contractreviewitem);
	}
		
	/**
	 * Returns an unordered Set of job item ids which have been contract reviewed, 
	 * for the given job. This is mainly intended for use in determining which page to link to 
	 * for each job item, as a replacement for the deprecated Job::getDefaultPage() method
	 * which causes lazy loading of the contract reviews.
	 */
	@Override
	public Set<Integer> getContractReviewedJobItemIds(int jobid) {
		List<Integer> idList = contractReviewItemDao.getContractReviewedJobItemIds(jobid);
		return new HashSet<>(idList);
	}

	@Override
	public List<JobItemProjectionDTO> getContractReviewItemsDTOs(Integer ContractReviewId, Locale locale) {
		return this.contractReviewItemDao.getContractReviewItemsDTOs(ContractReviewId, locale);
	}

	@Override
	public List<Integer> getJobItemsCouverdInContractReview(Integer ContractReviewId, Integer jobid) {
		return this.contractReviewItemDao.getJobItemsCouverdInContractReview(ContractReviewId, jobid);
	}

}