package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToSubdivDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation_;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink_;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsostatus.GeneralServiceOperationStatus;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsostatus.GeneralServiceOperationStatus_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement_;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement_;

@Repository("GeneralServiceOperationDao")
public class GeneralServiceOperationDaoImpl extends AllocatedToSubdivDaoImpl<GeneralServiceOperation, Integer> implements GeneralServiceOperationDao {
protected Class<GeneralServiceOperation> getEntity() {
     return GeneralServiceOperation.class;
     }

@Override
public List<GeneralServiceOperation> getByJobItemId(Integer jobitemid) {
	return getResultList(cb -> {
		CriteriaQuery<GeneralServiceOperation> cq = cb.createQuery(GeneralServiceOperation.class);
		Root<GeneralServiceOperation> root = cq.from(GeneralServiceOperation.class);
		Join<GeneralServiceOperation, GsoLink> linkJoin = root.join(GeneralServiceOperation_.links);
		Join<GsoLink, JobItem> jobitemJoin = linkJoin.join(GsoLink_.ji);
		
		cq.where(cb.equal(jobitemJoin.get(JobItem_.jobItemId), jobitemid));
		
		return cq;
	});
}

@Override
public GeneralServiceOperation getOnGoingByJobItemId(Integer jobitemid) {
	return getFirstResult(cb -> {
		CriteriaQuery<GeneralServiceOperation> cq = cb.createQuery(GeneralServiceOperation.class);
		Root<GeneralServiceOperation> root = cq.from(GeneralServiceOperation.class);
		Join<GeneralServiceOperation, GsoLink> gsoLinkJoin = root.join(GeneralServiceOperation_.links);
		Join<GsoLink, JobItem> jobItemJoin = gsoLinkJoin.join(GsoLink_.ji);
		Join<JobItem, JobItemWorkRequirement> jobItemWRJoin = jobItemJoin.join(JobItem_.nextWorkReq);
		Join<JobItemWorkRequirement, WorkRequirement> wRJoin = jobItemWRJoin.join(JobItemWorkRequirement_.workRequirement);
		Join<WorkRequirement, ServiceType> serviceTypeJoin = wRJoin.join(WorkRequirement_.serviceType);
		Join<ServiceType, CalibrationType> calTypeJoin = serviceTypeJoin.join(ServiceType_.calibrationType);
		Join<GeneralServiceOperation, GeneralServiceOperationStatus> statusJoin = root.join(GeneralServiceOperation_.status);
		
		Predicate andClause = cb.conjunction();
		andClause.getExpressions().add(cb.equal(jobItemJoin.get(JobItem_.jobItemId), jobitemid));
		andClause.getExpressions().add(cb.equal(statusJoin.get(GeneralServiceOperationStatus_.name), GeneralServiceOperationStatus.ON_GOING));
		andClause.getExpressions().add(cb.equal(root.get(GeneralServiceOperation_.calType), calTypeJoin));
		cq.where(andClause);
		cq.orderBy(cb.desc(root.get(GeneralServiceOperation_.id)));
		
		return cq;
	}).orElse(null);
}

@Override
public GeneralServiceOperation getOnHoldByJobItemId(Integer jobitemid) {
	return getFirstResult(cb -> {
		CriteriaQuery<GeneralServiceOperation> cq = cb.createQuery(GeneralServiceOperation.class);
		Root<GeneralServiceOperation> root = cq.from(GeneralServiceOperation.class);
		Join<GeneralServiceOperation, GsoLink> gsoLinkJoin = root.join(GeneralServiceOperation_.links);
		Join<GsoLink, JobItem> jobItemJoin = gsoLinkJoin.join(GsoLink_.ji);
		Join<JobItem, JobItemWorkRequirement> jobItemWRJoin = jobItemJoin.join(JobItem_.nextWorkReq);
		Join<JobItemWorkRequirement, WorkRequirement> wRJoin = jobItemWRJoin.join(JobItemWorkRequirement_.workRequirement);
		Join<WorkRequirement, ServiceType> serviceTypeJoin = wRJoin.join(WorkRequirement_.serviceType);
		Join<ServiceType, CalibrationType> calTypeJoin = serviceTypeJoin.join(ServiceType_.calibrationType);
		Join<GeneralServiceOperation, GeneralServiceOperationStatus> statusJoin = root.join(GeneralServiceOperation_.status);
		
		Predicate andClause = cb.conjunction();
		andClause.getExpressions().add(cb.equal(jobItemJoin.get(JobItem_.jobItemId), jobitemid));
		andClause.getExpressions().add(cb.equal(statusJoin.get(GeneralServiceOperationStatus_.name), GeneralServiceOperationStatus.ON_HOLD));
		andClause.getExpressions().add(cb.equal(root.get(GeneralServiceOperation_.calType), calTypeJoin));
		cq.where(andClause);
		
		return cq;
	}).orElse(null);
}

@Override
public GeneralServiceOperation getLastGSOByJobitemId(Integer jobitemid) {
	return getFirstResult(cb -> {
		CriteriaQuery<GeneralServiceOperation> cq = cb.createQuery(GeneralServiceOperation.class);
		Root<GeneralServiceOperation> root = cq.from(GeneralServiceOperation.class);
		Join<GeneralServiceOperation, GsoLink> linkJoin = root.join(GeneralServiceOperation_.links);
		Join<GsoLink, JobItem> jobitemJoin = linkJoin.join(GsoLink_.ji);
		
		cq.where(cb.equal(jobitemJoin.get(JobItem_.jobItemId), jobitemid));
		cq.orderBy(cb.desc(root.get(GeneralServiceOperation_.id)));
		
		return cq;
	}).orElse(null);
}

@Override
public GeneralServiceOperation getLastInstrumentGSO(Instrument instrument) {
	return this.getFirstResult(cb -> {
		CriteriaQuery<GeneralServiceOperation> cq = cb.createQuery(GeneralServiceOperation.class);
		Root<GeneralServiceOperation> root = cq.from(GeneralServiceOperation.class);
		Join<GeneralServiceOperation, GsoLink> gsoLink = root.join(GeneralServiceOperation_.links);
		Join<GsoLink, JobItem> jobItem = gsoLink.join(GsoLink_.ji);
		
		cq.where(cb.equal(jobItem.get(JobItem_.inst), instrument));
		cq.orderBy(cb.desc(root.get(GeneralServiceOperation_.completedOn)));
		
		return cq;
	}).orElse(null);
}
 }
