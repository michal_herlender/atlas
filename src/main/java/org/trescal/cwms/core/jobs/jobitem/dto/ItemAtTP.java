package org.trescal.cwms.core.jobs.jobitem.dto;

import java.time.ZonedDateTime;
import java.util.Date;

import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.tools.DateTools;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ItemAtTP {
	private String clientCompany;
	private Date dateIn;
	private int daysAway;
	private String itemDesc;
	private int itemNo;
	private int jobId;
	private int jobItemId;
	private String jobNo;
	private boolean overdue;
	private int poid;
	private String pono;
	private Date poDelDate;
	private Date poIssueDate;
	private Date sentOn;
	private String serialNo;
	private String telephone;
	private String mobile;
	private String name;
	private String email;
	private String subname;
	private String coname;
	private String companyrole;
	private String country;

	private String statusTranslation;

	public ItemAtTP(int jobItemId, String itemDesc, String statusTranslation, int itemNo, int jobId, String jobNo,
					ZonedDateTime dateIn,String subname,String coname,CompanyRole companyrole,String country, String clientCompany, String serialNo) {
		super();
		this.jobItemId = jobItemId;
		this.itemDesc = itemDesc;
		this.statusTranslation = statusTranslation;
		this.itemNo = itemNo;
		this.jobId = jobId;
		this.jobNo = jobNo;
		this.dateIn = DateTools.dateFromZonedDateTime(dateIn);
		this.clientCompany = clientCompany;
		this.serialNo = serialNo;
		this.subname = subname;
		this.coname = coname;
		this.companyrole = companyrole != null ? companyrole.getMessage() : "";
		this.country = country;
		
	}

}
