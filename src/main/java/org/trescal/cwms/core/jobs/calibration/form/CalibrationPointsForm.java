package org.trescal.cwms.core.jobs.calibration.form;

import java.util.List;

import javax.validation.Valid;

import org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.CalibrationPointSet;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.pointsettemplate.PointSetTemplate;

public class CalibrationPointsForm
{
	private String action;
	private CalReq cr;
	private CalReq editFrom;
	private List<Integer> metrics;
	private CalReq overrideFrom;
	private String page;
	private List<String> points;
	private CalibrationPointSet pointSet;
	private String rangeFrom;
	private String rangeRelatedPoint;
	private Integer rangeRelatedUom;
	private String rangeTo;
	private Integer rangeUom;
	private boolean relational;
	private boolean relationalRange;
	private List<Integer> relativeMetrics;
	private List<String> relativePoints;
	private boolean saveAsInstrumentDefault;
	private boolean saveAsTemplate;
	private PointSetTemplate template;

	public String getAction()
	{
		return this.action;
	}
	
	@Valid
	public CalReq getCr()
	{
		return this.cr;
	}

	public CalReq getEditFrom()
	{
		return this.editFrom;
	}

	public List<Integer> getMetrics()
	{
		return this.metrics;
	}

	public CalReq getOverrideFrom()
	{
		return this.overrideFrom;
	}

	public String getPage()
	{
		return this.page;
	}

	public List<String> getPoints()
	{
		return this.points;
	}

	public CalibrationPointSet getPointSet()
	{
		return this.pointSet;
	}

	public String getRangeFrom()
	{
		return this.rangeFrom;
	}

	public String getRangeRelatedPoint()
	{
		return this.rangeRelatedPoint;
	}

	public Integer getRangeRelatedUom()
	{
		return this.rangeRelatedUom;
	}

	public String getRangeTo()
	{
		return this.rangeTo;
	}

	public Integer getRangeUom()
	{
		return this.rangeUom;
	}

	public List<Integer> getRelativeMetrics()
	{
		return this.relativeMetrics;
	}

	public List<String> getRelativePoints()
	{
		return this.relativePoints;
	}

	public PointSetTemplate getTemplate()
	{
		return this.template;
	}

	public boolean isRelational()
	{
		return this.relational;
	}

	public boolean isRelationalRange()
	{
		return this.relationalRange;
	}

	public boolean isSaveAsInstrumentDefault()
	{
		return this.saveAsInstrumentDefault;
	}

	public boolean isSaveAsTemplate()
	{
		return this.saveAsTemplate;
	}

	public void setAction(String action)
	{
		this.action = action;
	}

	public void setCr(CalReq cr)
	{
		this.cr = cr;
	}

	public void setEditFrom(CalReq editFrom)
	{
		this.editFrom = editFrom;
	}

	public void setMetrics(List<Integer> metrics)
	{
		this.metrics = metrics;
	}

	public void setOverrideFrom(CalReq overrideFrom)
	{
		this.overrideFrom = overrideFrom;
	}

	public void setPage(String page)
	{
		this.page = page;
	}

	public void setPoints(List<String> points)
	{
		this.points = points;
	}

	public void setPointSet(CalibrationPointSet pointSet)
	{
		this.pointSet = pointSet;
	}

	public void setRangeFrom(String rangeFrom)
	{
		this.rangeFrom = rangeFrom;
	}

	public void setRangeRelatedPoint(String rangeRelatedPoint)
	{
		this.rangeRelatedPoint = rangeRelatedPoint;
	}

	public void setRangeRelatedUom(Integer rangeRelatedUom)
	{
		this.rangeRelatedUom = rangeRelatedUom;
	}

	public void setRangeTo(String rangeTo)
	{
		this.rangeTo = rangeTo;
	}

	public void setRangeUom(Integer rangeUom)
	{
		this.rangeUom = rangeUom;
	}

	public void setRelational(boolean relational)
	{
		this.relational = relational;
	}

	public void setRelationalRange(boolean relationalRange)
	{
		this.relationalRange = relationalRange;
	}

	public void setRelativeMetrics(List<Integer> relativeMetrics)
	{
		this.relativeMetrics = relativeMetrics;
	}

	public void setRelativePoints(List<String> relativePoints)
	{
		this.relativePoints = relativePoints;
	}

	public void setSaveAsInstrumentDefault(boolean saveAsInstrumentDefault)
	{
		this.saveAsInstrumentDefault = saveAsInstrumentDefault;
	}

	public void setSaveAsTemplate(boolean saveAsTemplate)
	{
		this.saveAsTemplate = saveAsTemplate;
	}

	public void setTemplate(PointSetTemplate template)
	{
		this.template = template;
	}

}
