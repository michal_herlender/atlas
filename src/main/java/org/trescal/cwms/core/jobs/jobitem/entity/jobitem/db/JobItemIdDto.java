package org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobItemIdDto {
	
	private Integer jobItemId;
	private Date dueDate;
	private String telephone;
}