package org.trescal.cwms.core.jobs.calibration.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.documents.excel.utils.ExcelFileReaderUtil;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.utils.ExchangeFormatGeneralValidator;
import org.trescal.cwms.core.jobs.calibration.form.ImportCalibrationsForm;
import org.trescal.cwms.core.jobs.calibration.form.validator.ImportCalibrationsFormValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV })
public class ImportOperationsController {

	public static final String FORM = "form";
	@Autowired
	private ExcelFileReaderUtil excelFileReaderUtil;
	@Autowired
	private ImportCalibrationsFormValidator validator;
	@Autowired
	private ExchangeFormatGeneralValidator efValidator;
	@Autowired
	private ExchangeFormatService efService;

	@ModelAttribute(FORM)
	protected ImportCalibrationsForm getImportCalibrationsForm() {
		return new ImportCalibrationsForm();
	}

	@RequestMapping(value = "/importoperations.htm", method = RequestMethod.GET)
	protected ModelAndView doGet() throws Exception {
		return new ModelAndView("trescal/core/jobs/calibration/importoperations");
	}

	@RequestMapping(value = "/importoperations.htm", method = RequestMethod.POST)
	public String submitForm(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(FORM) ImportCalibrationsForm form, BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) throws IOException {

		validator.validate(form, bindingResult, subdivDto.getKey());
		if (bindingResult.hasErrors())
			return "trescal/core/jobs/calibration/importoperations";

		ExchangeFormat ef = efService.get(form.getExchangeFormatId());
		List<LinkedCaseInsensitiveMap<String>> fileContent = excelFileReaderUtil
				.readExcelFile(form.getFile().getInputStream(), null, null);

		if (efValidator.validate(ef, fileContent, bindingResult).hasErrors()) {
			return "trescal/core/jobs/calibration/importoperations";
		}

		redirectAttributes.addFlashAttribute("fileContent", fileContent);
		redirectAttributes.addFlashAttribute("importCalibrationsForm", form);
		return "redirect:/importedoperationssynthesis.htm";
	}

}
