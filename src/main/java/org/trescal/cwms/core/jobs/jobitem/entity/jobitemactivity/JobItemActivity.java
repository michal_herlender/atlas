package org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity;

import lombok.Setter;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

import javax.persistence.*;

@Entity
@DiscriminatorValue("activity")
@Setter
public class JobItemActivity extends JobItemAction {

    private boolean complete;
    private Contact completedBy;
    private ActionOutcome outcome;

    /**
     * @return the completedBy
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "completedbyid")
    public Contact getCompletedBy() {
        return this.completedBy;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "actionoutcomeid")
    public ActionOutcome getOutcome() {
        return this.outcome;
    }

    /**
     * @return the complete
     */
    @Column(name = "complete", columnDefinition = "tinyint")
    public boolean isComplete() {
        return this.complete;
    }

    @Override
    @Transient
    public boolean isTransit() {
        return false;
    }
}