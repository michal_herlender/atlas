package org.trescal.cwms.core.jobs.job.entity.jobtypeservicetype;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

/**
 * Cross table indicating which ServiceType values are available for a JobType
 * @author Galen
 *
 */
@Entity
@Table(name="jobtypeservicetype", uniqueConstraints={@UniqueConstraint(columnNames={"servicetypeid","jobtypeid"})})
public class JobTypeServiceType {
	private int id;
	private ServiceType serviceType;
	private JobType jobType;
	private boolean defaultValue;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "servicetypeid", foreignKey=@ForeignKey(name="FK_jobtype_servicetype"))
	public ServiceType getServiceType() {
		return serviceType;
	}
	
	@NotNull
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "jobtypeid", nullable = false)
	public JobType getJobType() {
		return jobType;
	}

	@NotNull
	@Column(name = "defaultvalue", nullable=false, columnDefinition="bit default 0")
	public boolean isDefaultValue() {
		return defaultValue;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public void setServiceType(ServiceType calType) {
		this.serviceType = calType;
	}
	public void setJobType(JobType jobType) {
		this.jobType = jobType;
	}
	public void setDefaultValue(boolean defaultValue) {
		this.defaultValue = defaultValue;
	}

}
