package org.trescal.cwms.core.jobs.repair.component.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.repair.component.FreeRepairComponent;

public interface FreeRepairComponentDao extends BaseDao<FreeRepairComponent, Integer> {
	
}