package org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.db;

import java.util.Locale;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.dto.ContractReviewCalibrationCostDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;

@Repository("ContractReviewCalibrationCostDao")
public class ContractReviewCalibrationCostDaoImpl extends BaseDaoImpl<ContractReviewCalibrationCost, Integer> implements ContractReviewCalibrationCostDao {
	
	@Override
	protected Class<ContractReviewCalibrationCost> getEntity() {
		return ContractReviewCalibrationCost.class;
	}
	
	@Override
	public ContractReviewCalibrationCostDTO findContractReviewCalibrationCost(Integer id, Integer servicetypeid, Locale locale){
		return getFirstResult(cb ->{ 
			CriteriaQuery<ContractReviewCalibrationCostDTO> cq = cb.createQuery(ContractReviewCalibrationCostDTO.class);
			Root<ContractReviewCalibrationCost> root = cq.from(ContractReviewCalibrationCost.class); 
			Join<ContractReviewCalibrationCost, SupportedCurrency> supportedCurrency = root.join(ContractReviewCalibrationCost_.currency, JoinType.LEFT);
			Join<ContractReviewCalibrationCost, JobItem> jobitem = root.join(ContractReviewCalibrationCost_.jobitem);
			Join<JobItem, ServiceType> serviceType = jobitem.join(JobItem_.serviceType);
			Join<ServiceType, Translation> serviceTypeShortName = serviceType.join(ServiceType_.shortnameTranslation,
					JoinType.LEFT);
			serviceTypeShortName.on(cb.equal(serviceTypeShortName.get(Translation_.locale), locale));
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(ContractReviewCalibrationCost_.costid), id));
			clauses.getExpressions().add(cb.equal(serviceType.get(ServiceType_.serviceTypeId), servicetypeid));
			cq.where(clauses);
			cq.select(cb.construct(ContractReviewCalibrationCostDTO.class, 
					root.get(ContractReviewCalibrationCost_.costid),
					jobitem.get(JobItem_.jobItemId),
					jobitem.get(JobItem_.itemNo),
					serviceTypeShortName.get(Translation_.translation),
					supportedCurrency.get(SupportedCurrency_.currencyERSymbol),
					root.get(ContractReviewCalibrationCost_.finalCost)
					));
			return cq;
		}).orElse(null);
	}
}