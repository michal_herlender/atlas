package org.trescal.cwms.core.jobs.repair.dto.form.transformer;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.repair.component.FreeRepairComponent;
import org.trescal.cwms.core.jobs.repair.component.db.FreeRepairComponentService;
import org.trescal.cwms.core.jobs.repair.dto.FreeRepairComponentDTO;
import org.trescal.cwms.core.jobs.repair.dto.FreeRepairOperationDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairCompletionReportFormDTO;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperationComparator;
import org.trescal.cwms.core.jobs.repair.operation.db.FreeRepairOperationService;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.db.RepairCompletionReportService;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;
import org.trescal.cwms.core.jobs.repair.validatedcomponent.ValidatedFreeRepairComponent;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperation;
import org.trescal.cwms.core.tools.EntityDtoTransformer;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDateTime;
import static org.trescal.cwms.core.tools.DateTools.dateToLocalDateTime;

@Component
public class RepairCompletionReportFormDTOTransformer
	implements EntityDtoTransformer<RepairCompletionReport, RepairCompletionReportFormDTO> {

	@Autowired
	private CompanyService companyService;
	@Autowired
	private RepairCompletionReportService repairCompletionReportService;
	@Autowired
	private FreeRepairComponentService frcService;
	@Autowired
	private FreeRepairOperationService froService;
	@Autowired
	private RepairInspectionReportService repairInspectionReportService;
	@Autowired
	private JobItemService jobItemService;

	@Override
	public RepairCompletionReport transform(RepairCompletionReportFormDTO dto) {

		// we can only interact with an rcr if it is already created
		// (automatically)
		if (dto.getRcrId() != null) {

			RepairCompletionReport rcr = repairCompletionReportService.get(dto.getRcrId());

			rcr.setInternalCompletionComments(dto.getInternalCompletionComments());
			rcr.setExternalCompletionComments(dto.getExternalCompletionComments());
			rcr.setAdjustmentPerformed(dto.getAdjustmentPerformed());

			JobItem jobItem = jobItemService.findJobItem(dto.getJobItemId());
			// publish event of repair completion date changed
			if(dto.getRepairCompletionDate() != null) {
				this.repairCompletionReportService.publishRCRCompletionDateChangedEvent(jobItem, rcr,
					dateFromLocalDateTime(dto.getRepairCompletionDate()));
				rcr.setRepairCompletionDate(dateFromLocalDateTime(dto.getRepairCompletionDate()));
			}

			if (dto.getRirId() != null) {
				RepairInspectionReport rir = this.repairInspectionReportService.get(dto.getRirId());
				if (rir != null && rir.getMisuse() != dto.getMisuse()) {
					rir.setMisuse(dto.getMisuse());
					if (dto.getMisuse())
						rir.setMisuseComment(dto.getMisuseComment());
					else
						rir.setMisuseComment(null);
					this.repairInspectionReportService.merge(rir);
				}
			}

			// rir operations : change time, cost and price in VFRO
			dto.getRcrOperationsForm().stream()
					.filter(opDto -> !opDto.getAddedAfterRiRValidation() && StringUtils.isNoneBlank(opDto.getName()))
					.forEach(opDto -> {
						// get correspondant VFRO
						Optional<ValidatedFreeRepairOperation> vfroOpt = rcr
							.getValidatedFreeRepairOperations().stream().filter(vfroo -> vfroo
								.getFreeRepairOperation().getOperationId().equals(opDto.getFrOperationId()))
							.findFirst();
						vfroOpt.ifPresent(vfro -> {
							vfro.setCost(opDto.getCost());
							vfro.setLabourTime(opDto.getLabourTimeMinutes() + (opDto.getLabourTimeHours() * 60));
							vfro.getFreeRepairOperation().setOperationStatus(opDto.getStatus());
							vfro.getFreeRepairOperation().setName(opDto.getName());
							// remove unlinked components
						});
					});

			// rir components
			if (dto.getRcrComponentsForm() != null)
				dto.getRcrComponentsForm().stream().filter(
						compDto -> !compDto.getAddedAfterRiRValidation() && StringUtils.isNoneBlank(compDto.getName()))
						.forEach(compDto -> {
							// get correspondant VFRC
							ValidatedFreeRepairComponent vfrc = rcr.getValidatedFreeRepairOperations().stream()
								.flatMap(vfro -> vfro.getValidatedFreeRepairComponents().stream()).filter(vfrcc -> vfrcc.getFreeRepairComponent().getComponentId()
									.equals(compDto.getFrComponentId())).findFirst().orElse(null);
							if (vfrc == null) {
								FreeRepairComponent c = this.frcService.get(compDto.getFrComponentId());
								vfrc = new ValidatedFreeRepairComponent();
								vfrc.setFreeRepairComponent(c);
								if (c.getFreeRepairOperation().getValidatedFreeRepairOperation() != null) {
									vfrc.setValidatedFreeRepairOperation(
											c.getFreeRepairOperation().getValidatedFreeRepairOperation());
									c.getFreeRepairOperation().getValidatedFreeRepairOperation()
											.getValidatedFreeRepairComponents().add(vfrc);
								}
								c.setValidatedFreeRepairComponent(vfrc);
							}
							vfrc.setCost(compDto.getCost());
							vfrc.setQuantity(compDto.getQuantity());
							vfrc.getFreeRepairComponent().setStatus(compDto.getStatus());
							vfrc.getFreeRepairComponent().setName(compDto.getName());
						});

			// rcr operations : change time, cost and price in Operation itself
			Set<FreeRepairOperation> freeOps = new TreeSet<>(new FreeRepairOperationComparator());
			dto.getRcrOperationsForm().stream()
					.filter(opDto -> opDto.getAddedAfterRiRValidation() && StringUtils.isNoneBlank(opDto.getName()))
					.forEach(opDto -> {
						// get correspondant FreeRepairOperation, if null == new
						// operation added
						FreeRepairOperation fro = null;
						if (opDto.getFrOperationId() != null)
							fro = rcr.getFreeRepairOperations().stream().filter(fr -> fr.getOperationId().equals(opDto.getFrOperationId())).findFirst().orElse(null);
						if (fro == null) {
							// new
							fro = new FreeRepairOperation();
							fro.setAddedAfterRiRValidation(true);
							fro.setName(opDto.getName());
							fro.setOperationStatus(opDto.getStatus());
							fro.setOperationType(opDto.getType());
							fro.setRepairCompletionReport(rcr);
							if (opDto.getCoid() != null) {
								fro.setTpCompany(companyService.get(opDto.getCoid()));
							}
						}
						fro.setCost(opDto.getCost());
						fro.setPosition(opDto.getPosition());
						fro.setLabourTime(opDto.getLabourTimeMinutes() + (opDto.getLabourTimeHours() * 60));
						fro.setOperationStatus(opDto.getStatus());
						fro.setName(opDto.getName());
						this.froService.save(fro);

						freeOps.add(fro);
					});

			List<Integer> allSavedComponentsIds = new ArrayList<>();
			if (dto.getRcrComponentsForm() != null)
				allSavedComponentsIds
					.addAll(dto.getRcrComponentsForm().stream().map(FreeRepairComponentDTO::getFrComponentId)
						.filter(Objects::nonNull).collect(Collectors.toList()));

			Supplier<Stream<FreeRepairOperation>> allOperationS_temp = () -> Stream.concat(
					rcr.getRepairInspectionReports().stream().flatMap(rir -> rir.getFreeRepairOperations().stream()),
					freeOps.stream());

			// rcr components
			if (dto.getRcrComponentsForm() != null)
				dto.getRcrComponentsForm().stream().filter(
						compDto -> compDto.getAddedAfterRiRValidation() && StringUtils.isNoneBlank(compDto.getName()))
						.forEach(compDto -> {

							// look for operation
							Optional<FreeRepairOperation> froOpt = allOperationS_temp.get().filter(fr -> {
								if (compDto.getFreeRepairOperationId() != null && fr.getOperationId() != null)
									return fr.getOperationId().equals(compDto.getFreeRepairOperationId());
								else
									return compDto.getFreeRepairOperationName().equals(fr.getName());
							}).findFirst();

							final FreeRepairComponent frc;

							// new
							frc = new FreeRepairComponent();
							frc.setAddedAfterRiRValidation(true);
							frc.setName(compDto.getName());
							frc.setSource(compDto.getSource());
							frc.setStatus(compDto.getStatus());

							frc.setQuantity(compDto.getQuantity());
							frc.setCost(compDto.getCost());
							frc.setStatus(compDto.getStatus());
							frc.setName(compDto.getName());
							if (frc.getFreeRepairOperation() != null && frc.getFreeRepairOperation().getFreeRepairComponents() != null)
								frc.getFreeRepairOperation().getFreeRepairComponents().remove(frc);
							froOpt.ifPresent(frc::setFreeRepairOperation);
							this.frcService.save(frc);

							froOpt.ifPresent(fro -> {
								if (fro.getFreeRepairComponents() == null)
									fro.setFreeRepairComponents(new HashSet<>());
								fro.getFreeRepairComponents().add(frc);
							});
						});
			
			// remove deleted components on rcr operations
			freeOps.forEach(o -> {
				if(o.getFreeRepairComponents() != null) {
					List<FreeRepairComponent> cmps = o.getFreeRepairComponents().stream().filter(frcc -> frcc.getComponentId() == null || allSavedComponentsIds.contains(frcc.getComponentId())).collect(Collectors.toList());
					o.getFreeRepairComponents().clear();
					o.getFreeRepairComponents().addAll(cmps);
				}
			});
			
			// remove deleted components on rir operations
			rcr.getValidatedFreeRepairOperations().forEach(o -> {
				if(o.getFreeRepairOperation().getFreeRepairComponents() != null) {
					List<FreeRepairComponent> cmps = o.getFreeRepairOperation().getFreeRepairComponents().stream().filter(frcc -> frcc.getComponentId() == null || allSavedComponentsIds.contains(frcc.getComponentId())).collect(Collectors.toList());
					o.getFreeRepairOperation().getFreeRepairComponents().clear();
					o.getFreeRepairOperation().getFreeRepairComponents().addAll(cmps);
				}
			});
			
			rcr.getFreeRepairOperations().clear();
			rcr.getFreeRepairOperations().addAll(freeOps);

			return rcr;
		}
		return null;
	}

	@Override
	public RepairCompletionReportFormDTO transform(RepairCompletionReport entity) {

		RepairCompletionReportFormDTO dto = new RepairCompletionReportFormDTO();

		dto.setExternalCompletionComments(entity.getExternalCompletionComments());
		dto.setInternalCompletionComments(entity.getInternalCompletionComments());
		dto.setRcrId(entity.getRcrId());
		dto.setRcrOperationsForm(new ArrayList<>());
		dto.setRcrComponentsForm(new ArrayList<>());
		dto.setLastModified(entity.getLastModified());
		dto.setLastModifiedBy(entity.getLastModifiedBy().getName());
		dto.setAdjustmentPerformed(entity.getAdjustmentPerformed());
		dto.setRepairCompletionDate(entity.getRepairCompletionDate() != null ? dateToLocalDateTime(entity.getRepairCompletionDate()):null);
		if (entity.getValidated() != null && entity.getValidated()) {
			dto.setValidated(entity.getValidated());
			dto.setValidatedOn(entity.getValidatedOn());
			dto.setValidatedBy(entity.getValidatedBy().getName());
		}

		// rir operations
		entity.getValidatedFreeRepairOperations().forEach(vfro -> {
			FreeRepairOperationDTO froDto = new FreeRepairOperationDTO();

			froDto.setAddedAfterRiRValidation(false);
			froDto.setName(vfro.getFreeRepairOperation().getName());
			froDto.setCost(vfro.getCost());
			froDto.setLabourTimeMinutes(vfro.getLabourTime() % 60);
			froDto.setLabourTimeHours(vfro.getLabourTime() / 60);
			froDto.setPosition(vfro.getFreeRepairOperation().getPosition());
			if (vfro.getFreeRepairOperation().getTpCompany() != null) {
				froDto.setCoid(vfro.getFreeRepairOperation().getTpCompany().getCoid());
				froDto.setCoName(vfro.getFreeRepairOperation().getTpCompany().getConame());
			}
			froDto.setFrOperationId(vfro.getFreeRepairOperation().getOperationId());
			froDto.setStatus(vfro.getFreeRepairOperation().getOperationStatus());
			froDto.setRirStatus(vfro.getValidatedStatus());
			froDto.setType(vfro.getFreeRepairOperation().getOperationType());
			// add RcrOperationForm
			dto.getRcrOperationsForm().add(froDto);

			// components linked to rir operations
			if (vfro.getFreeRepairOperation().getFreeRepairComponents() != null)
				vfro.getFreeRepairOperation().getFreeRepairComponents().forEach(frc -> {
					FreeRepairComponentDTO frcDto = new FreeRepairComponentDTO();

					if (frc.getAddedAfterRiRValidation()) {
						frcDto.setCost(frc.getCost());
						frcDto.setQuantity(frc.getQuantity());
					} else {
						frcDto.setCost(frc.getValidatedFreeRepairComponent() != null
								? frc.getValidatedFreeRepairComponent().getCost()
								: frc.getCost());
						frcDto.setQuantity(frc.getValidatedFreeRepairComponent() != null
								? frc.getValidatedFreeRepairComponent().getQuantity()
								: frc.getQuantity());
						frcDto.setValidatedFreeRepairComponentId(frc.getValidatedFreeRepairComponent() != null
								? frc.getValidatedFreeRepairComponent().getValidatedComponentId()
								: null);
						frcDto.setRirStatus(frc.getValidatedFreeRepairComponent() != null
								? frc.getValidatedFreeRepairComponent().getValidatedStatus()
								:null);
					}

					if (frc.getComponent() != null)
						frcDto.setComponentId(frc.getComponent().getId());
					frcDto.setAddedAfterRiRValidation(frc.getAddedAfterRiRValidation());
					frcDto.setFrComponentId(frc.getComponentId());
					frcDto.setFreeRepairOperationName(vfro.getFreeRepairOperation().getName());
					frcDto.setFreeRepairOperationId(vfro.getFreeRepairOperation().getOperationId());
					frcDto.setName(frc.getName());
					frcDto.setSource(frc.getSource());
					frcDto.setStatus(frc.getStatus());

					// add RcrComponentsForm
					dto.getRcrComponentsForm().add(frcDto);
				});
		});

		// rcr operations
		if (entity.getFreeRepairOperations() != null)
			entity.getFreeRepairOperations().forEach(fro -> {
				FreeRepairOperationDTO froDto = new FreeRepairOperationDTO();

				froDto.setAddedAfterRiRValidation(true);
				froDto.setName(fro.getName());
				froDto.setCost(fro.getCost());
				froDto.setPosition(fro.getPosition());
				froDto.setLabourTimeMinutes(fro.getLabourTime() % 60);
				froDto.setLabourTimeHours(fro.getLabourTime() / 60);
				if (fro.getTpCompany() != null) {
					froDto.setCoid(fro.getTpCompany().getCoid());
					froDto.setCoName(fro.getTpCompany().getConame());
				}
				froDto.setFrOperationId(fro.getOperationId());
				froDto.setStatus(fro.getOperationStatus());
				froDto.setType(fro.getOperationType());

				// add RcrOperationForm
				dto.getRcrOperationsForm().add(froDto);

				// rcr : components
				if (fro.getFreeRepairComponents() != null)
					fro.getFreeRepairComponents().forEach(frc -> {
						FreeRepairComponentDTO frcDto = new FreeRepairComponentDTO();

						frcDto.setAddedAfterRiRValidation(true);
						if (frc.getComponent() != null)
							frcDto.setComponentId(frc.getComponent().getId());
						frcDto.setCost(frc.getCost());
						frcDto.setQuantity(frc.getQuantity());
						frcDto.setFrComponentId(frc.getComponentId());
						frcDto.setFreeRepairOperationName(fro.getName());
						frcDto.setFreeRepairOperationId(fro.getOperationId());
						frcDto.setName(frc.getName());
						frcDto.setSource(frc.getSource());
						frcDto.setStatus(frc.getStatus());
						if (frc.getValidatedFreeRepairComponent() != null)
							frcDto.setValidatedFreeRepairComponentId(
									frc.getValidatedFreeRepairComponent().getValidatedComponentId());
						// add RcrComponentsForm
						dto.getRcrComponentsForm().add(frcDto);
					});

			});

		return dto;
	}

}
