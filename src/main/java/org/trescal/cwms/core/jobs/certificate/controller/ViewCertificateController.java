package org.trescal.cwms.core.jobs.certificate.controller;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateClass;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certnote.CertNote;
import org.trescal.cwms.core.jobs.certificate.form.CertificateForm;
import org.trescal.cwms.core.jobs.certificate.form.ViewCertificateValidator;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CalibrationVerification;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CertificateClassDefault;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.Deviation;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@RequestMapping(value = "/viewcertificate.htm")
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class ViewCertificateController {
	@Autowired
	private CalibrationProcessService calProServ;
	@Autowired
	private CertificateService certServ;
	@Autowired
	private ViewCertificateValidator validator;
	@Autowired
	private UserService userService;
	@Autowired
	private CalibrationVerification calibrationVerificationSystemDefault;
	@Autowired
	private Deviation deviationSystemDefault;
	@Autowired
	private CertificateClassDefault certificateClassDefaultSystemDefault;
	@Autowired
	private AuthenticationService authenticationService;

	public static final String FORM_NAME = "form";
	
	@ModelAttribute("verificationStatuses")
	protected List<KeyValue<CalibrationVerificationStatus, String>> initializeVerificationStatuses() {
		return Arrays.stream(CalibrationVerificationStatus.values()).map(f -> new KeyValue<>(f, f.getName()))
				.collect(Collectors.toList());
	}

	@ModelAttribute("caneditverificationstatus")
	protected Boolean initializePermision(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		User user = this.userService.get(username);
		return authenticationService.hasRight(user.getCon(), Permission.VERIFICATION_STATUT_EDIT, subdivDto.getKey());
	}

	@ModelAttribute("hasverificationstatus")
	protected Boolean showVerificationStatus(@RequestParam(name = "certid", required = true) int certid) {
		Certificate certificate = certServ.findCertificate(certid);
		// Assuming instruments belonging to different clients won't ever be on the same
		// cert!
		Instrument instrument = certServ.getRelatedInstruments(certificate).get(0);
		return calibrationVerificationSystemDefault.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
	}

	@ModelAttribute("hasdeviation")
	protected Boolean showDeviation(@RequestParam(name = "certid", required = true) int certid) {
		Certificate certificate = certServ.findCertificate(certid);
		Instrument instrument = certServ.getRelatedInstruments(certificate).get(0);
		return deviationSystemDefault.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
	}

	@ModelAttribute("hasclass")
	protected Boolean showClass(@RequestParam(name = "certid", required = true) int certid) {
		Certificate certificate = certServ.findCertificate(certid);
		Instrument instrument = certServ.getRelatedInstruments(certificate).get(0);
		return certificateClassDefaultSystemDefault.getValueByScope(Scope.COMPANY, instrument.getComp(), null);
	}

	@ModelAttribute("certificateclasses")
	protected List<KeyValue<CertificateClass, String>> initializeCertificateClasses() {
		return Arrays.stream(CertificateClass.values()).sorted(Comparator.comparing(CertificateClass::getOrder))
				.map(c -> new KeyValue<>(c, c.getDescription())).collect(Collectors.toList());
	}

	@ModelAttribute("calprocesses")
	public List<CalibrationProcess> getCalProcesses() {
		return calProServ.getAll();
	}

	@ModelAttribute("units")
	public List<KeyValue<IntervalUnit, String>> initializeRecallIntervalUnits() {
		return Arrays.stream(IntervalUnit.values()).map(iu -> new KeyValue<IntervalUnit, String>(iu, iu.getName(0)))
				.collect(Collectors.toList());
	}

	@ModelAttribute("deviationunitsymbol")
	public String getDeviationUnitSymbol(@RequestParam(name = "certid", required = true) int certid) {
		// Assuming cert will be for only one instrument
		Certificate cert = this.certServ.findCertificate(certid);
		return cert.getLinks().stream().findFirst().flatMap(cl -> Optional.ofNullable(cl.getJobItem().getInst()))
				.flatMap(i -> Optional.ofNullable(i.getInstrumentComplementaryField()))
				.flatMap(f -> Optional.ofNullable(f.getDeviationUnits())).map(u -> u.getSymbol()).orElse("");
	}

	@ModelAttribute("privateOnlyNotes")
	public Boolean getPrivateOnly() {
		return CertNote.privateOnly;
	}

	@InitBinder(FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.setValidator(validator);
	}

	@ModelAttribute(FORM_NAME)
	public CertificateForm createForm(@RequestParam(name = "certid", required = true) int certid) {
		CertificateForm form = new CertificateForm();
		Certificate cert = this.certServ.findCertificate(certid);
		Instrument firstInstrument = this.certServ.getRelatedInstruments(cert).get(0);

		form.setCalProcessId(((cert.getCal() != null) && (cert.getCal().getCalProcess() != null))
				? cert.getCal().getCalProcess().getId()
				: null);
		form.setCalDate(cert.getCalDate());
		form.setCertDate(cert.getCertDate());
		form.setCertId(certid);
		form.setDuration(cert.getDuration());
		form.setDurationUnit(cert.getUnit());
		form.setRemarks(cert.getRemarks());
		if (cert.getThirdDiv() != null) {
			form.setSubdivid(cert.getThirdDiv().getId());
			form.setCoid(cert.getThirdDiv().getComp().getId());
		}
		form.setThirdCertNo(cert.getThirdCertNo());
		if (cert.getCalibrationVerificationStatus() != null) {
			form.setCalibrationVerificationStatus(cert.getCalibrationVerificationStatus());
		} else {
			form.setCalibrationVerificationStatus(CalibrationVerificationStatus.valueOf("NOT_APPLICABLE"));
		}
		if (cert.getDeviation() != null) {
			form.setDeviation(cert.getDeviation().stripTrailingZeros());
		}
		if (cert.getCertClass() != null) {
			form.setCertificateClass(cert.getCertClass());
		} else {
			cert.getLinks().stream().findFirst().flatMap(cl -> Optional.ofNullable(cl.getJobItem().getInst()))
					.flatMap(i -> Optional.ofNullable(i.getInstrumentComplementaryField()))
					.flatMap(f -> Optional.ofNullable(f.getCertificateClass()))
					.ifPresent(c -> form.setCertificateClass(c));
		}
		form.setDeviationUnitsSymbol(firstInstrument.getInstrumentComplementaryField() != null
				&& firstInstrument.getInstrumentComplementaryField().getDeviationUnits() != null
						? firstInstrument.getInstrumentComplementaryField().getDeviationUnits().getSymbol()
						: "");
		form.setAcceptanceCriteria(firstInstrument.getInstrumentComplementaryField() != null
				&& firstInstrument.getInstrumentComplementaryField().getCustomerAcceptanceCriteria() != null
						? firstInstrument.getInstrumentComplementaryField().getCustomerAcceptanceCriteria()
						: "");
		return form;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String displayForm(Model model, @RequestParam(name = "certid", required = true) int certid,
			@ModelAttribute(FORM_NAME) CertificateForm form) {
		Certificate cert = this.certServ.findCertificate(certid);
		model.addAttribute("cert", cert);
		return "trescal/core/jobs/certificate/viewcertificate";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String processForm(Model model, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@RequestParam(name = "certid", required = true) int certid,
			@Validated @ModelAttribute(FORM_NAME) CertificateForm form, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return displayForm(model, certid, form);
		}
		// edit certificate
		Certificate cert = this.certServ.editCertificate(certid, form, username, subdivDto.getKey());
		return "redirect:viewcertificate.htm?certid=" + cert.getCertid();
	}

}