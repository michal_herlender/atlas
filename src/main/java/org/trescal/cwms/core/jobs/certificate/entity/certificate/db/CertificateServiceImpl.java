package org.trescal.cwms.core.jobs.certificate.entity.certificate.db;

import io.vavr.API;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.batch.jobs.operationsimport.ImportedOperationItem;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.external.adveso.events.CertificateStatusChangedEvent;
import org.trescal.cwms.core.external.adveso.events.UploadCertificateEvent;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.InstrumentComplementaryField;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.InstrumentCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.certificate.dto.*;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateAction;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateReplacementType;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certaction.db.CertActionService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLinkComparator;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLinkJobComparator;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.db.CertLinkService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLinkComparator;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.db.InstCertLinkService;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidation;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidationStatus;
import org.trescal.cwms.core.jobs.certificate.form.CertificateForm;
import org.trescal.cwms.core.jobs.certificate.form.CertificateSearchForm;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db.JobItemWorkRequirementService;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db.WorkRequirementService;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_DelayedCertificate;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.*;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.rest.alligator.component.RestErrors;
import org.trescal.cwms.rest.common.dto.RestCommonCertificateDTO;
import org.trescal.cwms.rest.common.dto.output.RestFieldErrorDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static io.vavr.API.$;
import static io.vavr.API.Case;
import static io.vavr.Patterns.$Left;
import static io.vavr.Patterns.$Right;
import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDate;

@Service("CertificateService")
@Slf4j
public class CertificateServiceImpl implements CertificateService {
    @Value("#{props['cwms.admin.email']}")
	private String cwmsAdminEmail;

	@Autowired
	private CalibrationService calServ;
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private CertActionService certActionServ;
	@Autowired
	private CertificateDao certDao;
	@Autowired
	private CertLinkService certLinkServ;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private EmailContent_DelayedCertificate emailContentDelayedCertificate;
	@Autowired
	private EmailService emailServ;
    @Autowired
    private HookInterceptor_DelayCertificate interceptor_delayCertificate;
    @Autowired
    private HookInterceptor_SignCertificate interceptor_signCertificate;
    @Autowired
    private InstrumService instrumentService;
    @Autowired
    private JobItemService jiServ;
    @Autowired
    private JobService jobServ;
    @Autowired
    private NumerationService numerationService;
    @Autowired
    private CapabilityAuthorizationService procAccServ;
    @Autowired
    private UserService userService;
    @Autowired
    private SystemComponentService scServ;
    @Autowired
    private MessageSource messages;
    @Autowired
    private InstCertLinkService instCertLinkService;
    @Autowired
    private CalibrationTypeService calibrationTypeService;
    @Autowired
    private JobItemService jobItemService;
	@Autowired
	private HookInterceptor_CreateCertificate interceptor_CreateCertificate;
	@Autowired
	private ActionOutcomeService actionOutcomeService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private WorkRequirementService workRequirementService;
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;
	@Autowired
	protected SessionUtilsService sessionServ;
	@Autowired
	private JobItemWorkRequirementService jobItemWorkRequirementService;
	@Autowired
	private HookInterceptor_StartCertificateReplacement startCertificateReplacement_hook;
	@Autowired
	private HookInterceptor_StartThirdPartyCertificateReplacement startThirdPartyCertificateReplacement_hook;
	@Autowired
	private HookInterceptor_StartOnSiteCertificateReplacement startOnSiteCertificateReplacement_hook;
	@Value("#{props['cwms.config.filebrowser.maxRestFileUploadSize']}")
	private Integer fileSizeLimit;
	@Autowired
	private HookInterceptor_SignCertificate hook_SignCertificate;
	@Autowired
	private CalReqService calReqService;
	@Autowired
	private CalibrationProcessService calProServ;
	@Autowired
	private AuthenticationService authenticationService;

	@Override
	public ResultWrapper approveCertificate(int certid, String password, boolean isProcTrainingCert,
			HttpServletRequest req) {
		return new ResultWrapper(false, "This feature is deprecated in Atlas and targeted for removal");
	}

	@Override
	public Certificate createCertFromCal(Contact con, Calibration cal, Integer duration, boolean signCert,
			Subdiv allocatedSubdiv, IntervalUnit intervalUnit, CertStatusEnum defaultCertStatus, Date certDate) {
		List<Integer> allLinks = new ArrayList<>();
		for (CalLink cl : cal.getLinks())
			allLinks.add(cl.getId());
		return this.createCertFromCal(con, cal, duration, signCert, allLinks, false, allocatedSubdiv, intervalUnit,
				defaultCertStatus, certDate);
	}

	@Override
	public Certificate createThirdPartyCertFromCal(Subdiv allocatedSubdiv, Calibration cal, Contact registredBy,
			boolean signCert, Contact validatedBy, Instrument inst, String certNumber, Date certDate) {

		CertificateType type = CertificateType.CERT_THIRDPARTY;
		Certificate cert = new Certificate();
		cert.setCertno(this.findNewCertNo(type, allocatedSubdiv));
		cert.setCal(cal);
		cert.setServiceType(cal.getCalType().getServiceType());
		cert.setCertDate(certDate);
		cert.setCalDate(certDate);
		cert.setType(type);
		cert.setRegisteredBy(registredBy);
		cert.setDuration(inst.getCalFrequency());
		cert.setUnit(inst.getCalFrequencyUnit());
		cert.setThirdCertNo(certNumber);
        cal.getLinks().stream().findFirst().flatMap(cl -> Optional.of(cl.getJi().getInst()))
			.flatMap(i -> Optional.ofNullable(i.getInstrumentComplementaryField()))
			.flatMap(f -> Optional.ofNullable(f.getCertificateClass())).ifPresent(cert::setCertClass);
		cert.setCertPreviewed(true);

		if (signCert || !cal.getCalProcess().getSigningSupported()) {
			cert.setStatus(CertStatusEnum.SIGNED);
			cert.setSignedBy(validatedBy);
			cert.setApprovedBy(validatedBy);
		} else
			cert.setStatus(CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE);

		this.insertCertificate(cert, registredBy);
		this.createLinksForCert(cert, null, registredBy, cert.getCertDate());
		// set new cert into cal object
		if (cal.getCerts() == null)
			cal.setCerts(new ArrayList<>());
		cal.getCerts().add(cert);
		this.certDao.signCertificate(cert);
		return cert;

	}

	@Override
	public Certificate createCertFromCal(Contact con, Calibration cal, Integer duration, boolean signCert,
			List<Integer> calLinkIdsToInclude, boolean certPreviewed, Subdiv allocatedSubdiv, IntervalUnit intervalUnit,
			CertStatusEnum defaultCertStatus, Date certDate) {

		CertificateType type = CertificateType.CERT_INHOUSE;
		Certificate cert = new Certificate();
		cert.setCertno(this.findNewCertNo(type, allocatedSubdiv));
		cert.setCal(cal);
		cert.setServiceType(cal.getCalType().getServiceType());
		cert.setCertDate(certDate == null ? new Date() : certDate);
		cert.setType(type);
		cert.setRegisteredBy(con);
		cert.setDuration(duration);
		cert.setUnit(intervalUnit);
		cal.getLinks().stream().findFirst().flatMap(cl -> Optional.of(cl.getJi().getInst()))
			.flatMap(i -> Optional.ofNullable(i.getInstrumentComplementaryField()))
			.flatMap(f -> Optional.ofNullable(f.getCertificateClass())).ifPresent(cert::setCertClass);
		// has certificate been previewed by engineer?
		cert.setCertPreviewed(certPreviewed);
		// use the cal date from imported data to keep complete datetime
		// if is not null, else use the cal date from calibration
		cert.setCalDate(certDate == null ? dateFromLocalDate(cal.getCalDate()) : certDate);
		if (signCert || !cal.getCalProcess().getSigningSupported())
			this.signCertificate(cert, con);
		else
			cert.setStatus(defaultCertStatus); // TODO to be optimized : get default status and use it at first
		this.insertCertificate(cert, con);
		this.createLinksForCert(cert, calLinkIdsToInclude, con, cert.getCertDate());
		// set new cert into cal object
		if (cal.getCerts() == null)
			cal.setCerts(new ArrayList<>());
		cal.getCerts().add(cert);
		return cert;
	}

	@Override
	public ResultWrapper createCertificatePreview(Integer certId, Integer calId, String certTemplate, String tempDir,
			HttpSession session) {
		return new ResultWrapper(false, "This feature is deprecated in Atlas and targeted for removal");
	}

	@Override
	public void createLinksForCert(Certificate cert, Calibration cal, List<Integer> calLinkIdsToInclude,
			Contact contact, Date startDate) {
		for (CalLink calLink : cal.getLinks()) {
			if ((calLinkIdsToInclude == null) || calLinkIdsToInclude.contains(calLink.getId())) {
				JobItem ji = calLink.getJi();
				linkCertToJobItem(cert, ji, contact, startDate);
			}
		}
	}

	@Override
	public void linkCertToJobItem(Certificate cert, JobItem ji, Contact contact, Date startDate) {
		boolean alreadyLinked = false;
		if ((cert.getLinks() != null) && (cert.getLinks().size() > 0)) {
			for (CertLink link : cert.getLinks())
				if (link.getJobItem().getJobItemId() == ji.getJobItemId()) {
					alreadyLinked = true;
					break;
				}
		}
		if (!alreadyLinked) {
			CertLink cl = new CertLink();
			cl.setCert(cert);
			if (ji.getCertLinks() == null)
				ji.setCertLinks(new TreeSet<>(new CertLinkComparator()));
			if (cert.getLinks() == null)
				cert.setLinks(new TreeSet<>(new CertLinkJobComparator()));
			cl.setJobItem(ji);
			ji.getCertLinks().add(cl);
			cert.getLinks().add(cl);
			this.setCertificateDirectory(cert);
			this.certLinkServ.insertCertLink(cl, contact, startDate);
		}
	}

	@Override
	public void linkCertToInstrument(Certificate certificate, Instrument instrument) {
		if (instrument.getInstCertLinks().stream().noneMatch(l -> l.getCert().equals(certificate))) {
			InstCertLink instCertLink = new InstCertLink();
			instCertLink.setCert(certificate);
			instCertLink.setInst(instrument);
			instCertLinkService.insertInstCertLink(instCertLink);
			if (instrument.getInstCertLinks() == null)
				instrument.setInstCertLinks(new TreeSet<>(new InstCertLinkComparator()));
			if (certificate.getInstCertLinks() == null)
				certificate.setInstCertLinks(new TreeSet<>(new InstCertLinkComparator()));
			instrument.getInstCertLinks().add(instCertLink);
			certificate.getInstCertLinks().add(instCertLink);
		}
	}

	@Override
	public void linkCertToCal(Certificate cert, Calibration cal) {
		if (cert.getCal() == null) {
			cert.setCal(cal);
			// Indicate that certificate is now expected for this cal (for any linked job items)
			for (CalLink calLink : cal.getLinks()) {
				calLink.setCertIssued(true);
			}
		}
		if (cal.getCerts() == null) {
			cal.setCerts(new ArrayList<>());
		}
		// Just to prevent a duplicate link, check that cert not yet linked
		boolean alreadyLinked = false;
		for (Certificate linkedCert : cal.getCerts()) {
			if (linkedCert.getCertid() == cert.getCertid()) {
				alreadyLinked = true;
				break;
			}
		}
		if (!alreadyLinked)
			cal.getCerts().add(cert);
	}

	@Override
	public void createLinksForCert(Certificate cert, List<Integer> calLinkIdsToInclude, Contact contact,
			Date startDate) {
		Calibration cal = cert.getCal();
		this.createLinksForCert(cert, cal, calLinkIdsToInclude, contact, startDate);
	}

	@Override
	public void createOrUpdateReservedCertificates(JobItem ji, Contact contact, Date certDate) {
		// Check if reserved inhouse certificates already exist connected to job
		// item,
		// if so update them. Note: certificates are not deleted, this must be
		// done manually
		int existingReservedCertCount = 0;
		for (CertLink certLink : ji.getCertLinks()) {
			if (certLink.getCert().getType().equals(CertificateType.CERT_INHOUSE)
					&& certLink.getCert().getStatus().equals(CertStatusEnum.RESERVED)) {
				Certificate certificate = certLink.getCert();
				certificate.setCalDate(certDate);
				certificate.setCertDate(certDate);
				this.updateCertificate(certificate);
				existingReservedCertCount++;
			}
		}
		for (JobItemWorkRequirement jiwr : ji.getWorkRequirements()) {
			WorkRequirement wr = jiwr.getWorkRequirement();
			if ((wr.getType().equals(WorkRequirementType.CALIBRATION)
					|| wr.getType().equals(WorkRequirementType.ONSITE))
					&& !wr.getStatus().equals(WorkrequirementStatus.COMPLETE)
					&& !wr.getStatus().equals(WorkrequirementStatus.CANCELLED)) {
				if (existingReservedCertCount > 0) {
					existingReservedCertCount--;
				} else {
					// Need to create new reserved certificate for this work
					// requirement
					createReservedCertificate(jiwr, contact, certDate);
				}
			}
		}
	}

	private void createReservedCertificate(JobItemWorkRequirement jiwr, Contact contact, Date certDate) {
        ServiceType serviceType = jiwr.getWorkRequirement().getServiceType();
        Subdiv businessSubdiv = jiwr.getWorkRequirement().getCapability().getOrganisation();

        Certificate certificate = new Certificate();
        // Note, no Calibration is created until cal occurs, just Certificate
        Optional.of(jiwr.getJobitem().getInst())
            .flatMap(i -> Optional.ofNullable(i.getInstrumentComplementaryField()))
            .flatMap(f -> Optional.ofNullable(f.getCertificateClass())).ifPresent(certificate::setCertClass);
        certificate.setCalDate(certDate);
        certificate.setServiceType(serviceType);
        certificate.setCertDate(certDate);
        certificate.setCertno(this.findNewCertNo(CertificateType.CERT_INHOUSE, businessSubdiv));
		certificate.setDuration(jiwr.getJobitem().getInst().getCalFrequency());
		certificate.setUnit(jiwr.getJobitem().getInst().getCalFrequencyUnit());
		certificate.setRegisteredBy(contact);
		certificate.setStatus(CertStatusEnum.RESERVED);
		certificate.setType(CertificateType.CERT_INHOUSE);
		this.insertCertificate(certificate, contact);
		// We can't call the regular createLinksForCert(...) method because
		// there is no cal yet
		// So, we just link to the job item
		linkCertToJobItem(certificate, jiwr.getJobitem(), contact, certDate);
	}

	@Override
	public Certificate createSupplementaryCert(int certId, Contact currentUser,
			Date startDate) {
		Certificate originalCert = this.findCertificate(certId);
		if (originalCert.getCal() == null)
			throw new UnsupportedOperationException("Cert id "+certId+" has no calibration attached, which is first required to reissue certificate");
		Subdiv businessSubdiv = originalCert.getCal().getOrganisation();
		Certificate newCert = new Certificate();
		if (startDate == null)
			newCert.setCertDate(new Date());
		else
			newCert.setCertDate(startDate);
		newCert.setCertno(this.findNewCertNo(originalCert.getType(), businessSubdiv));
		newCert.setStatus(CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE);
		newCert.setRegisteredBy(currentUser);
		newCert.setSupplementaryFor(originalCert);
		// set all properties shared with the original certificate
		newCert.setType(originalCert.getType());
		newCert.setCal(originalCert.getCal());
		newCert.setServiceType(originalCert.getServiceType());
		newCert.setCalDate(originalCert.getCalDate());
		newCert.setDuration(originalCert.getDuration());
		newCert.setUnit(originalCert.getUnit());
		newCert.setDocTemplateKey(originalCert.getDocTemplateKey());
		newCert.setCalibrationVerificationStatus(originalCert.getCalibrationVerificationStatus());
		this.insertCertificate(newCert, currentUser);
		this.createLinksForCert(newCert, null, currentUser, startDate);

		originalCert.getLinks().forEach(cl -> {
			CertificateStatusChangedEvent eventOnNewCert = new CertificateStatusChangedEvent(this, cl.getJobItem(),
					newCert, null, newCert.getStatus());
			applicationEventPublisher.publishEvent(eventOnNewCert);
		});

		return newCert;
	}

	@Override
	public void delayCertificate(Certificate cert) {
		// call method to trigger hook for status update (do not remove or
		// change)
        this.certDao.delayCertificate(cert);
        this.interceptor_delayCertificate.recordAction(cert);

        // e-mail engineer (CC lab manager)
        // set from address and subject
        String fromAddr = this.cwmsAdminEmail;
        String ccAddr = "";
        // get department under which procedure is categorised and
        // find any
        // managers to cc in email
        for (CategorisedCapability cat : cert.getCal().getCapability().getCategories()) {
            for (DepartmentMember dm : cat.getCategory().getDepartment().getMembers()) {
                if (dm.getDepartmentRole().isManage()) {
                    if ((dm.getContact().getEmail() != null) && !dm.getContact().getEmail().equals("")) {
                        ccAddr = dm.getContact().getEmail();
                    }
                }
            }
        }
        // create array for to list
        ArrayList<String> toList = new ArrayList<>();
        // find engineer
        Contact engineer = cert.getCal().getCompletedBy();
        // add calibration completed by contact
        toList.add(engineer.getEmail());

        EmailContentDto contentDto = this.emailContentDelayedCertificate.getContent(cert, engineer.getLocale());

        // send email
		this.emailServ.sendAdvancedEmail(contentDto.getSubject(), fromAddr, toList, Collections.singletonList(ccAddr), Collections.emptyList(), Collections.emptyList(),
				Collections.emptyList(), contentDto.getBody(), true, false);

	}

	@Override
	public void deleteCertificate(Certificate cert, Contact con) {
		cert.setStatus(CertStatusEnum.DELETED);
		certActionServ.logCertAction(cert, CertificateAction.DELETED, con);
	}

	@Override
	public Certificate findCertificate(int id) {
		Certificate cert = this.certDao.find(id);
		// 2018-12-12 GB : Prevent directory lookup on repeated gets.
		if (cert != null)
			if (cert.getDirectory() == null)
				this.setCertificateDirectory(cert);
		return cert;
	}

	@Override
	public Certificate findCertificateByCertNo(String certno) {
		Certificate cert = null;
		if (!StringUtils.isBlank(certno)) {
			cert = this.certDao.findCertificateByCertNo(certno);
			if (cert != null && cert.getStatus().equals(CertStatusEnum.DELETED)) {
				cert = null;
			}
		}
		return cert;
	}

	@Override
	public String findNewCertNo(CertificateType type, Subdiv allocatedSubdiv) {
		String certno = "";
		switch (type) {
		case CERT_INHOUSE:
			certno = numerationService.generateNumber(NumerationType.CERTIFICATE, allocatedSubdiv.getComp(),
					allocatedSubdiv);
			break;
		case CERT_THIRDPARTY:
			certno = numerationService.generateNumber(NumerationType.TP_CERTIFICATE, allocatedSubdiv.getComp(),
					allocatedSubdiv);
			break;
		case CERT_CLIENT:
			certno = numerationService.generateNumber(NumerationType.CLIENT_CERTIFICATE, allocatedSubdiv.getComp(),
					allocatedSubdiv);
			break;
		}
		return certno;
	}

	@Override
	public Certificate findWebSafeCertificate(int id) {
		return this.findCertificate(id);
	}

	@Override
	public List<Certificate> getAllCertificatesAwaitingSignature() {
		return this.certDao.getAllCertificatesAwaitingSignature();
	}

	@Override
	public Certificate getReservedCertificate(JobItem jobItem) {
		log.debug("Checking for reserved certificate on jobitem id " + jobItem.getJobItemId());
		Certificate certificate = null;
		if (CollectionUtils.isNotEmpty(jobItem.getCertLinks()))
			for (CertLink certLink : jobItem.getCertLinks()) {
				if (certLink.getCert().getType().equals(CertificateType.CERT_INHOUSE)
						&& certLink.getCert().getStatus().equals(CertStatusEnum.RESERVED)) {
					certificate = certLink.getCert();
					break;
				}
			}
		if (certificate != null)
			log.debug("Returning reserved certificate " + certificate.getCertno());
		else
			log.debug("No reserved certificate found");
		return certificate;
	}

	@Override
	public String getReservedCertificateNumber(Integer jobItemId) {
		return this.certDao.getReservedCertificateNumber(jobItemId);
	}

	@Override
	public void insertCertificate(Certificate c, Contact currentUser) {
		this.certDao.persist(c);
		// after successful insertion, log the action
		this.certActionServ.logCertAction(c, CertificateAction.ISSUED, currentUser);
	}

	@Override
	public void publishCustomCert(CustomCertWrapper ccw, Date calDate, Contact issuedBy,
								  Subdiv allocatedSubdiv) {
		CalibrationType calType = this.calTypeServ.find(ccw.getCalTypeId());
		CertificateType certType = CertificateType.CERT_INHOUSE;
		Certificate cert = new Certificate();
		cert.setCalDate(calDate);
		cert.setServiceType(calType.getServiceType());
		cert.setCertDate(new Date());
		cert.setCertno(this.findNewCertNo(certType, allocatedSubdiv));
		cert.setDuration(ccw.getDuration());
		// TODO custom certs need interval units passed, if reactivated
		cert.setUnit(IntervalUnit.MONTH);
		cert.setRegisteredBy(issuedBy);
		cert.setType(certType);
		if (ccw.getCalId() != null) {
			Calibration cal = this.calServ.findCalibration(ccw.getCalId());
			cert.setCal(cal);
			if (!cal.getCalProcess().getSigningSupported())
				this.signCertificate(cert, issuedBy);
			else
				cert.setStatus(CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE);
			this.insertCertificate(cert, issuedBy);
			this.createLinksForCert(cert, null, issuedBy, calDate);
			// set new cert into cal object
			if (cal.getCerts() == null)
				cal.setCerts(new ArrayList<>());
			cal.getCerts().add(cert);
		} else {
			// think about signing?
			cert.setStatus(CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE);
			this.insertCertificate(cert, issuedBy);
			CertLink cl = new CertLink();
			cl.setCert(cert);
			JobItem ji = this.jiServ.findJobItem(ccw.getJobItemId());
			if (ji.getCertLinks() == null)
				ji.setCertLinks(new TreeSet<>(new CertLinkComparator()));
			if (cert.getLinks() == null)
				cert.setLinks(new TreeSet<>(new CertLinkJobComparator()));
			cl.setJobItem(ji);
			ji.getCertLinks().add(cl);
			cert.getLinks().add(cl);
			this.certLinkServ.insertCertLink(cl);
		}
	}

	@Override
	public void publishCustomGroupCert(CustomGroupCertWrapper gw, Date calDate, Contact issuedBy,
									   Subdiv allocatedSubdiv) {
		CalibrationType calType = this.calTypeServ.find(gw.getCalTypeId());
		CertificateType certType = CertificateType.CERT_INHOUSE;
		Certificate cert = new Certificate();
		cert.setCalDate(calDate);
		cert.setServiceType(calType.getServiceType());
		cert.setCertDate(new Date());
		cert.setCertno(this.findNewCertNo(certType, allocatedSubdiv));
		cert.setDuration(gw.getDuration());
		// TODO custom certs need interval units passed, if reactivated
		cert.setUnit(IntervalUnit.MONTH);
		cert.setRegisteredBy(issuedBy);
		cert.setType(certType);
		// think about signing?
		cert.setStatus(CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE);
		this.insertCertificate(cert, issuedBy);
		for (Integer jobItemId : gw.getJobItemIds()) {
			CertLink cl = new CertLink();
			cl.setCert(cert);
			JobItem ji = this.jiServ.findJobItem(jobItemId);
			if (ji.getCertLinks() == null)
				ji.setCertLinks(new TreeSet<>(new CertLinkComparator()));
			if (cert.getLinks() == null)
				cert.setLinks(new TreeSet<>(new CertLinkJobComparator()));
			cl.setJobItem(ji);
			ji.getCertLinks().add(cl);
			cert.getLinks().add(cl);
			this.certLinkServ.insertCertLink(cl);
		}
	}

	@Override
	public ResultWrapper linkCertToCal(int certid, int calid, HttpSession session) {
		Locale locale = LocaleContextHolder.getLocale();
		// first find certificate
		Certificate cert = this.findCertificate(certid);
		// certificate null?
		if (cert != null) {
			// find calibration
			Calibration cal = this.calServ.findCalibration(calid);
			// calibration null?
			if (cal != null) {
				// check cert does not already have cal
				if ((cert.getCal() == null) && (cal.getCerts().size() == 0)) {
					// set cal on certificate
					cert.setCal(cal);
					// update certificate
					this.updateCertificate(cert);
					// add certificate to calibration
					cal.getCerts().add(cert);
					// update calibration
					this.calServ.updateCalibration(cal);
					// return successful result
					return new ResultWrapper(true, "", cal.getCalProcess().getProcess(), null);
				} else {
					// return un-successful wrapper
					return new ResultWrapper(false, this.messages.getMessage("error.certificate.linkwithcalibration",
							null, "A problem occured and the certificate and calibration could not be linked", locale),
							null, null);
				}
			} else {
				// return un-successful wrapper
				return new ResultWrapper(false, this.messages.getMessage("error.certificate.calibrationnotfound", null,
						"The calibration could not be found", locale), null, null);
			}
		} else {
			// return un-successful wrapper
			return new ResultWrapper(false, this.messages.getMessage("error.certificate.notfound", null,
					"The certificate could not be found", locale), null, null);
		}
	}

	@Override
	public ResultWrapper markCertificateAsPreviewed(int certid, HttpSession session) {
		Locale locale = LocaleContextHolder.getLocale();
		// find the certificate
		Certificate cert = this.findCertificate(certid);
		// certificate null?
		if (cert != null) {
			// set as previewed
			cert.setCertPreviewed(true);
			// return successful wrapper
			return new ResultWrapper(true, "", cert, null);
		} else {
			// return un-successful wrapper
			return new ResultWrapper(false, this.messages.getMessage("error.certificate.notfound", null,
					"The certificate could not be found", locale), null, null);
		}
	}

	@Override
	public void resetCertificate(int certid, Contact currentUser) {
		Certificate cert = this.findCertificate(certid);
		// set status back to unsigned
		if ((cert.getCal() == null) || cert.getCal().getCalProcess().getSigningSupported()) {
			cert.setStatus(CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE);
			cert.setSignedBy(null);
			cert.setApprovedBy(null);
		} else
			cert.setStatus(CertStatusEnum.SIGNING_NOT_SUPPORTED);
		// delete PDF
		// if cert pdf file exists
		if (cert.getHasCertFile()) {
			// delete the pdf
			File certPDF = new File(cert.getCertFilePath());
			certPDF.delete();
		}
		// after successful reset, log action
		this.certActionServ.logCertAction(cert, CertificateAction.RESET, currentUser);
	}

	public void searchCertificatesJPA(CertificateSearchForm form, PagedResultSet<CertificateSearchWrapper> prs,
			boolean includeDeletedAndCancelledCertificates) {
		this.certDao.searchCertificatesJPA(form, prs, includeDeletedAndCancelledCertificates);
		setCertificateDirectories(prs);
	}

	private void setCertificateDirectories(PagedResultSet<CertificateSearchWrapper> prs) {
		// set the directory for the certs being returned.
		// this allows checking for the cert pdf file
		for (CertificateSearchWrapper certWrap : prs.getResults()) {
			Certificate cert = certWrap.getCert();
			this.setCertificateDirectory(cert);
		}
	}

	/**
	 * Checks whether certificate file exists, in expected location
	 */
	@Override
	public boolean getCertificateFileExists(Certificate certificate) {
		setCertificateDirectory(certificate);
		File directory = certificate.getDirectory();

		String fileName = certificate.getCertno() + ".PDF";
		File file = new File(directory.getAbsolutePath().concat(File.separator).concat(fileName));
		return file.exists();
	}

	/**
	 * Note, this will intentionally throw FileNotFoundException, if the file
	 * doesn't exist. Recommend to use getCertificateFileExists() first to check
	 * file existence
	 */
	@Override
	public String readCertificateFileIntoBase64Format(Certificate certificate) {

		setCertificateDirectory(certificate);
		File directory = certificate.getDirectory();

		String fileName = certificate.getCertno() + ".PDF";
		File file = new File(directory.getAbsolutePath().concat(File.separator).concat(fileName));

		try (FileInputStream fis = new FileInputStream(file)) {
			// 20MB max
			if(file.length() > fileSizeLimit){
				return RestErrors.CODE_FILE_SIZE;
			} else {
				byte[] fileData = new byte[(int) file.length()];
				IOUtils.read(fis, fileData);
				fis.close();

				// to base64
				return com.lowagie.text.pdf.codec.Base64.encodeBytes(fileData);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void setCertificateDirectory(Certificate cert) {
		if (cert != null) {
			Integer firstInstLinkPlantId = null;
			if (cert.getInstCertLinks() != null && cert.getInstCertLinks().size() > 0) 
				firstInstLinkPlantId = cert.getInstCertLinks().iterator().next().getInst().getPlantid();
				
			String firstJobitemLinkJobNo = null;
			if (cert.getLinks() != null && cert.getLinks().size() > 0) 
				firstJobitemLinkJobNo = cert.getLinks().iterator().next().getJobItem().getJob().getJobno();
				
			cert.setDirectory(this.getCertificateDirectory(cert.getType(), firstInstLinkPlantId, firstJobitemLinkJobNo));
		}
	}
	
	@Override
	public File getCertificateDirectory(CertificateType certType, Integer firstInstLinkPlantId, String firstJobitemLinkJobNo) {
		File certDirectory = null;
		switch (certType) {
			case CERT_CLIENT:
				if (firstInstLinkPlantId != null) {
					certDirectory = this.compDirServ.getDirectory(this.scServ.findComponent(Component.CLIENT_CERTIFICATE),
									String.valueOf(firstInstLinkPlantId));
				}
				break;
			case CERT_THIRDPARTY:
				if (firstInstLinkPlantId != null) {
					certDirectory = this.compDirServ.getDirectory(this.scServ.findComponent(Component.CERTIFICATE),
							String.valueOf(firstInstLinkPlantId));
				}
				// Note, third party cert may have either job link or inst link,
				// evaluate both cases (intentionally, no break)
			default:
				if (firstJobitemLinkJobNo != null) {
					certDirectory = this.compDirServ.getDirectory(this.scServ.findComponent(Component.CERTIFICATE),
							firstJobitemLinkJobNo);
				}
				break;
			}
		return certDirectory;
	}

	@Override
	public void signCertificate(Certificate certificate, Contact con) {
		Calibration cal = certificate.getCal();

		// if certificate has no linked cal (probably a site cert), just allow
		// the user to sign because there is no procedure to check against
		if (cal == null) {
			certificate.setStatus(CertStatusEnum.SIGNED);
			certificate.setSignedBy(con);

			// only fire hook if the certificate is not being signed
			// immediately upon creation
			if (certificate.getLinks() != null) {
				this.certDao.signCertificate(certificate);
				HookInterceptor_SignCertificate.Input input = new HookInterceptor_SignCertificate.Input(certificate,
						con);
				this.interceptor_signCertificate.recordAction(input);
			}
		} else {
			if (cal.getCalProcess().getSigningSupported()) {
                val accStat = this.procAccServ.authorizedFor(cal.getCapability().getId(),
                    cal.getCalType().getCalTypeId(), con.getPersonid());

                API.Match(accStat)
                    .of(Case($Right($(AccreditationLevel.APPROVE)), () -> {
                            certificate.setStatus(CertStatusEnum.SIGNED);
                            certificate.setSignedBy(con);
                            certificate.setApprovedBy(con);
                            return certificate;
                        }),
                        Case($Right($(AccreditationLevel.SIGN)), () -> {
                            certificate.setStatus(CertStatusEnum.SIGNED_AWAITING_APPROVAL);
                            return certificate;
                        }),
                        Case($Right($(AccreditationLevel.PERFORM)), () -> {
                            certificate.setStatus(CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE);
                            return certificate;
                        }),
                        Case($Left($()), unused -> {
                            certificate.setStatus(CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE);
                            return certificate;
                        })
                    );
                accStat.peek(l -> {
                    if (certificate.getLinks() != null) {
                        this.certDao.signCertificate(certificate);
                        HookInterceptor_SignCertificate.Input input = new HookInterceptor_SignCertificate.Input(
                            certificate, con);
                        this.interceptor_signCertificate.recordAction(input);
                    }
                });
            } else {
				certificate.setStatus(CertStatusEnum.SIGNING_NOT_SUPPORTED);
			}
		}
	}

	@Override
	public ResultWrapper unIssueCertificate(int certid, HttpSession session) {
		Locale locale = LocaleContextHolder.getLocale();
		try {
			Certificate cert = this.findCertificate(certid);

			if (cert == null) {
				return new ResultWrapper(false, this.messages.getMessage("error.certificate.notfound", null,
						"The certificate could not be found", locale));
			}

			this.deleteCertificate(cert, sessionServ.getCurrentContact());

			return new ResultWrapper(true, null);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResultWrapper(false,
					this.messages.getMessage("error.certificate.deleting", null,
							"There was an error deleting this certificate. Please ensure the database is still clean!",
							locale));
		}
	}

	@Override
	public void updateCertificate(Certificate c) {
		this.certDao.merge(c);
	}

	@Override
	public boolean wordFileExistsInCertificateDirectory(int certId) {
		Certificate cert = this.findCertificate(certId);
		return cert.hasWordFile();

	}

	@Override
	public Certificate getMostRecentCertForInstrument(Instrument instrument, Date calibratedBefore) {

		List<CertLink> jobCertLinks = certLinkServ.searchCertLink(instrument.getPlantid(), false);
		Set<InstCertLink> instrumentCertLinks = instrument.getInstCertLinks();

		Comparator<Certificate> comparator = Comparator.comparing(Certificate::getCalDate);
		comparator = comparator.thenComparing(Certificate::getCertid);

		List<Certificate> allCerts = new ArrayList<>();
		if (jobCertLinks != null && jobCertLinks.size() > 0) {
			allCerts = jobCertLinks.stream().map(CertLink::getCert).collect(Collectors.toList());
		}
		if (instrumentCertLinks != null && instrumentCertLinks.size() > 0) {
			allCerts.addAll(instrumentCertLinks.stream().map(InstCertLink::getCert).collect(Collectors.toList()));
		}

		List<Certificate> filteredCerts = allCerts.stream().filter(c -> (c.getStatus().equals(CertStatusEnum.SIGNED)
				|| c.getStatus().equals(CertStatusEnum.SIGNING_NOT_SUPPORTED))).collect(Collectors.toList());

		List<Certificate> dateRestrictedCerts = filteredCerts;
		if (calibratedBefore != null) {
			dateRestrictedCerts = filteredCerts.stream().filter(c -> !c.getCalDate().after(calibratedBefore))
					.collect(Collectors.toList());
		}

		if (dateRestrictedCerts.size() > 0) {
			return filteredCerts.stream().max(comparator).orElse(null);
		} else {
			return null;
		}

	}

	@Override
	public List<Instrument> getRelatedInstruments(Certificate certificate) {
		if (certificate.getInstCertLinks() != null && certificate.getInstCertLinks().size() > 0) {
			return certificate.getInstCertLinks().stream().map(InstCertLink::getInst).collect(Collectors.toList());
		} else if (certificate.getLinks() != null && certificate.getLinks().size() > 0) {
			return certificate.getLinks().stream().map(l -> l.getJobItem().getInst()).collect(Collectors.toList());
		} else {
			return null;
		}
	}

	@Override
	public PagedResultSet<Certificate> getAllCertificatesByRelatedInstrument(Integer plantid, Integer resultsPerPage,
			Integer currentPage) {
		return certDao.getCertificatesByRelatedInstrument(plantid, resultsPerPage, currentPage);
	}

	@Override
	public Certificate createCommonCertificate(RestCommonCertificateDTO inputDTO, String fileExtension) {

		Instrument instrument = this.instrumentService.get(inputDTO.getPlantId());
		Contact contact = this.contactService.getByHrId(inputDTO.getHrid());

		JobItem jobItem = null;
		if (inputDTO.getJobItemId() != null) {
			jobItem = jobItemService.findJobItem(inputDTO.getJobItemId());
		}

		JobItemWorkRequirement jiwr = null;
		if (inputDTO.getWorkRequirementId() != null)
			jiwr = jobItemWorkRequirementService.get(inputDTO.getWorkRequirementId());

		/*
		 * allocatedSubdiv used to generate CertificateNumber 1) if jobitem is set we
		 * use it to get the allocatedSubdiv 2) else if calibrationId is set we use it
		 * to get the Calibration and then get the organisation (subdivision) 3) else if
		 * instrument is set we get the defaultBusinessContact from instrument subdivion
		 * if is set if not from instrument company and then get the
		 * allocatedSubdivision from this defaultBusinessContact
		 */
		Subdiv allocatedSubdiv = null;
		if (jobItem != null)
			allocatedSubdiv = jobItem.getJob().getOrganisation();
		else if (inputDTO.getCalibrationId() != null) {
			Calibration cal = calServ.findCalibration(inputDTO.getCalibrationId());
			if (cal != null)
				allocatedSubdiv = cal.getOrganisation();
		} else if (instrument != null) {
			Contact defaultBusinessContact = instrument.getCon().getSub().getComp().getDefaultBusinessContact();
			allocatedSubdiv = defaultBusinessContact.getSub();
		} else {
			throw new IllegalArgumentException(
					"Primary business subdivision cannot be determined for instrument, configuration or data migration error");
		}

		String certNo = this.findNewCertNo(inputDTO.getCertificateType(), allocatedSubdiv);

		Certificate certificate = new Certificate();
		certificate.setCertno(certNo);

		// type (restricted by validator - only CERT_THIRDPARTY or CERT_CLIENT)
		certificate.setType(inputDTO.getCertificateType());

		// set dates
		certificate.setCalDate(inputDTO.getCalibrationDate());
		certificate.setCertDate(inputDTO.getCertificateDate());

		// calibration type (automatically set via setting service type)
		if (inputDTO.getCalTypeId() != null) {
			CalibrationType calibrationType = calibrationTypeService.find(inputDTO.getCalTypeId());
			certificate.setServiceType(calibrationType.getServiceType());
		} else if (jiwr != null) {
			certificate.setServiceType(jiwr.getWorkRequirement().getServiceType());
		} else if (jobItem != null) {
			certificate.setServiceType(jobItem.getServiceType());
		}

		// third party subdivision
		if (inputDTO.getThirdDivId() != null) {
			Subdiv subdiv = subdivService.get(inputDTO.getThirdDivId());
			certificate.setThirdDiv(subdiv);
		}

		// third party Certificate Number
		certificate.setThirdCertNo(inputDTO.getThirdCertificateNumber());

		// remarks
		if (StringUtils.isNotBlank(inputDTO.getRemarks())) {
			certificate.setRemarks(inputDTO.getRemarks());
		}

		// registered by
		if (contact != null)
			certificate.setRegisteredBy(contact);

		// cal verification status
		if (inputDTO.getCalVerificationStatus() != null) {
			certificate.setCalibrationVerificationStatus(inputDTO.getCalVerificationStatus());
		}

		// duration
		if (inputDTO.getDuration() != null) {
			certificate.setDuration(inputDTO.getDuration());
		} else {
			certificate.setDuration(instrument.getCalFrequency());
		}

		// interval unit
		if (inputDTO.getIntervalUnit() != null) {
			certificate.setUnit(inputDTO.getIntervalUnit());
		} else {
			certificate.setUnit(instrument.getCalFrequencyUnit());
		}

		// status (always SIGNING_NOT_SUPPORTED)
		certificate.setStatus(CertStatusEnum.SIGNED);

		// remarks
		if (inputDTO.getRemarks() != null) {
			certificate.setRemarks(inputDTO.getRemarks());
		}

		if (inputDTO.getAdjustment() != null)
			certificate.setAdjustment(inputDTO.getAdjustment());
		if (inputDTO.getRepair() != null)
			certificate.setRepair(inputDTO.getRepair());
		if (inputDTO.getRestriction() != null)
			certificate.setRestriction(inputDTO.getRestriction());
		if (inputDTO.getOptimization() != null)
			certificate.setOptimization(inputDTO.getOptimization());

		if (jiwr != null)
			workRequirementService.checkWorkRequirement(jiwr);

		// create certlink if jobitem exists
		CertLink certLink = null;
		if (jobItem != null) {
			certLink = new CertLink();
			certLink.setCert(certificate);
			certLink.setJobItem(jobItem);
			certificate.setLinks(new TreeSet<>(new CertLinkJobComparator()));
			certificate.getLinks().add(certLink);
			if (jobItem.getCertLinks() == null) {
				jobItem.setCertLinks(new TreeSet<>(new CertLinkJobComparator()));
			}
			jobItem.getCertLinks().add(certLink);

			if (inputDTO.getThirdPartyWorkOutcome() != null) {
				// set third party work outcome
				ActionOutcome ao = actionOutcomeService.getByTypeAndValue(ActionOutcomeType.POST_TP_FURTHER_WORK,
						inputDTO.getThirdPartyWorkOutcome());

				certificate.setThirdOutcome(ao);
			}

		} else {
			// cert links, essential to determine directory and user access to
			// certificate
			InstCertLink instCertLink = new InstCertLink();
			instCertLink.setCert(certificate);
			instCertLink.setInst(instrument);
			certificate.setInstCertLinks(new HashSet<>());
			certificate.getInstCertLinks().add(instCertLink);
		}

		// decode file
		byte[] fileData = Base64.getDecoder().decode(inputDTO.getFile());

		// setting to upload directory for the certificate file
		setCertificateDirectory(certificate);
		File directory = certificate.getDirectory();

		// write file
		boolean fileExist = false;
		try {
			fileExist = writeFile(directory, certificate.getCertno() + fileExtension, fileData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		certDao.persist(certificate);

		// check current WR if outcome is successful and and type of certificate is
		// third party
		if (CertificateType.CERT_THIRDPARTY.equals(certificate.getType()) && certificate.getThirdOutcome() != null
				&& certificate.getThirdOutcome().isPositiveOutcome()
				&& jobItem != null && jobItemService.stateHasGroupOfKeyName(jobItem.getState(), StateGroup.AWAITING_TP_CERTIFICATE)) {
			workRequirementService.checkCurrentWorkRequirement(jobItem);
		}

		Certificate suppCertificate = null;
		if (inputDTO.getSupplementaryForId() != null)
			suppCertificate = findCertificate(inputDTO.getSupplementaryForId());
		boolean isReplaced;
		if (suppCertificate != null) {
			suppCertificate.setStatus(CertStatusEnum.REPLACED);
			certificate.setSupplementaryFor(suppCertificate);
			isReplaced = true;
		}
		else isReplaced = fileExist;

		// send notifications to Adveso
		if (jobItem != null) {	

			CertificateStatusChangedEvent createEvent = new CertificateStatusChangedEvent(this, jobItem, certificate,
					null, certificate.getStatus());
			applicationEventPublisher.publishEvent(createEvent);

			// Adveso needs this !!!
			CertificateStatusChangedEvent updateEvent = new CertificateStatusChangedEvent(this, jobItem, certificate,
					certificate.getStatus(), certificate.getStatus());
			applicationEventPublisher.publishEvent(updateEvent);

			UploadCertificateEvent uploadCertEvent = new UploadCertificateEvent(this, certificate, isReplaced, jobItem);
			applicationEventPublisher.publishEvent(uploadCertEvent);

		}

		if (certLink != null) {
			HookInterceptor_CreateCertificate.Input hookInput = new HookInterceptor_CreateCertificate.Input(certLink,
					contact, inputDTO.getTimeSpent(), inputDTO.getStartDate(), null);
			interceptor_CreateCertificate.recordAction(hookInput);
		}

		return certificate;
	}

	@Override
	public Certificate commonRestUpdateCertificate(RestCommonCertificateDTO inputDTO, String fileExtension) {

		Certificate certificate = findCertificate(inputDTO.getCertId());

		// calibration date
		if (inputDTO.getCalibrationDate() != null) {
			certificate.setCalDate(inputDTO.getCalibrationDate());
		}

		// calibration type
		if (inputDTO.getCalTypeId() != null) {
			CalibrationType calibrationType = calibrationTypeService.find(inputDTO.getCalTypeId());
			certificate.setServiceType(calibrationType.getServiceType());
		}

		// certificate date
		if (inputDTO.getCertificateDate() != null) {
			certificate.setCertDate(inputDTO.getCertificateDate());
		}

		// duration
		if (inputDTO.getDuration() != null) {
			certificate.setDuration(inputDTO.getDuration());
		}

		// interval unit
		if (inputDTO.getIntervalUnit() != null) {
			certificate.setUnit(inputDTO.getIntervalUnit());
		}

		// status
		if (StringUtils.isNotBlank(inputDTO.getCertificateStatus())) {
			certificate.setStatus(CertStatusEnum.valueOf(inputDTO.getCertificateStatus()));
		}

		// third certificate number
		if (StringUtils.isNotBlank(inputDTO.getThirdCertificateNumber())) {
			certificate.setThirdCertNo(inputDTO.getThirdCertificateNumber());
		}

		// remarks
		if (inputDTO.getRemarks() != null) {
			certificate.setRemarks(inputDTO.getRemarks());
		}

		if (inputDTO.getAdjustment() != null)
			certificate.setAdjustment(inputDTO.getAdjustment());
		if (inputDTO.getRepair() != null)
			certificate.setRepair(inputDTO.getRepair());
		if (inputDTO.getRestriction() != null)
			certificate.setRestriction(inputDTO.getRestriction());
		if (inputDTO.getOptimization() != null)
			certificate.setOptimization(inputDTO.getOptimization());

		if (inputDTO.getWorkRequirementId() != null) {
			JobItemWorkRequirement jiwr = jobItemWorkRequirementService.get(inputDTO.getWorkRequirementId());
			if (jiwr == null)
				throw new IllegalArgumentException(
						"Primary business subdivision cannot be determined for instrument, configuration or data migration error");
			workRequirementService.checkWorkRequirement(jiwr);

		}

		// file
		if (StringUtils.isNotBlank(inputDTO.getFile())) {
			// setting to upload directory for the certificate file
			setCertificateDirectory(certificate);
			File directory = certificate.getDirectory();

			// decode file
			byte[] fileData = Base64.getDecoder().decode(inputDTO.getFile());

			// write file
			boolean fileExist = false;
			try {
				fileExist = writeFile(directory, certificate.getCertno() + fileExtension, fileData);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Certificate suppCertificate = null;
			if (inputDTO.getSupplementaryForId() != null)
				suppCertificate = findCertificate(inputDTO.getCertId());
			boolean isReplaced;
			if (suppCertificate != null)
				isReplaced = true;
			else isReplaced = fileExist;

			// send event for upload certificate
			if (inputDTO.getJobItemId() != null) {
				JobItem jobItem = jobItemService.findJobItem(inputDTO.getJobItemId());
				if (jobItem != null) {
					UploadCertificateEvent uploadCertEvent = new UploadCertificateEvent(this, certificate, isReplaced,
							jobItem);
					applicationEventPublisher.publishEvent(uploadCertEvent);
				}
			}
		}

		certDao.merge(certificate);
		return certificate;
	}

	private boolean writeFile(File directory, String fileName, byte[] fileData) throws Exception {
		File file = new File(directory.getAbsolutePath().concat(File.separator).concat(fileName));

		boolean fileExist = file.exists();

		// Try resource close method - JDK 7
		FileOutputStream os = new FileOutputStream(file);
		IOUtils.write(fileData, os);
		os.flush();
		os.close();
		// TODO
		return fileExist;
	}

	@Override
	public List<Certificate> getCertificateListByJobitemId(Integer jobitemid) {
		return this.certDao.getCertificateListByJobitemId(jobitemid);
	}

	@Override
	public Date getLastTpCertDate(Integer jobitemid) {

		Certificate tpCert = this.getCertificateListByJobitemId(jobitemid).stream()
				.filter(c -> c.getType().equals(CertificateType.CERT_THIRDPARTY)).max(Comparator.comparing(Certificate::getCertDate)).orElse(null);

		if (tpCert != null)
			return tpCert.getCertDate();

		return null;
	}

	@Override
	public Certificate getMostRecentCertificateForJobItem(JobItem jobItem) {
		return this.certDao.getMostRecentCertificateForJobItem(jobItem.getJobItemId());
	}

	@Override
	public boolean uploadFile(MultipartFile file, Certificate cert, Contact contact) throws IOException {

		if (file != null) {
			String name = file.getOriginalFilename();
			if (Objects.nonNull(name) && !Objects.equals(name, "")) {
                // get name without extension
                String nameWithoutExt;
                // get extension
                String ext = name.substring(name.lastIndexOf("."));
                // rename file to cert no
                nameWithoutExt = cert.getCertno();

                if (name.lastIndexOf('\\') > -1) {
                    name = name.substring(name.lastIndexOf('\\'));
                    nameWithoutExt = name.substring(0, name.lastIndexOf("."));
				}

				file.transferTo(new File(cert.getDirectory().getAbsolutePath().concat(File.separator)
						.concat(nameWithoutExt).concat(ext)));

				if (cert.getType().equals(CertificateType.CERT_THIRDPARTY)) {
					// log the upload action
					this.certActionServ.logCertAction(cert, CertificateAction.TPUPLOAD, contact);
				}

				return ext.equalsIgnoreCase(".pdf");
			}
		}

		return false;
	}

	@Override
	public List<Certificate> getCertificatesForInstrument(Instrument instrument) {
		List<Certificate> certs = this.certDao.getJobCertsForInstrument(instrument);
		certs.addAll(this.certDao.getNonJobCertsForInstrument(instrument));
		return certs.stream().sorted(Comparator.comparing(Certificate::getCalDate)).collect(Collectors.toList());
	}

	@Override
	public Certificate getLatestCertificatesForInstrument(Instrument instrument) {
		Optional<Certificate> cert = Optional.of(getCertificatesForInstrument(instrument).stream()
				.max(Comparator.comparing(Certificate::getCalDate)).orElseGet(Certificate::new));
		return cert.get();
	}

	@Override
	public List<Certificate> getByThirdCertNoAndPlantId(String thirdCertificateNumber, Integer plantId) {
		return certDao.getByThirdCertNoAndPlantId(thirdCertificateNumber, plantId);
	}

	@Override
	public Certificate importCertificate(ImportedOperationItem dto, Contact crc, JobItem jobitem,
			Contact certificateValidatedBy, Calibration cal) {
		// create certificate or get reserved
		Certificate certificate = this
				.getByThirdCertNoAndPlantId(dto.getDocumentNumber(), jobitem.getInst().getPlantid()).stream().findAny()
				.orElse(null);
		if (certificate != null) {
			certificate.setStatus(CertStatusEnum.SIGNED);
			certificate.setCertDate(dto.getOperationDate());
			certificate.setSignedBy(certificateValidatedBy);
			this.linkCertToCal(certificate, cal);
			List<Integer> allLinks = new ArrayList<>();
			for (CalLink cl : cal.getLinks())
				allLinks.add(cl.getId());
			this.createLinksForCert(certificate, cal, allLinks, certificateValidatedBy, dto.getOperationDate());

		} else {
			// create certificate
			Certificate newCertificate = new Certificate();
			newCertificate.setCertno(this.findNewCertNo(CertificateType.CERT_INHOUSE, crc.getSub()));
			newCertificate.setCal(cal);
			newCertificate.setServiceType(cal.getCalType().getServiceType());
			newCertificate.setCertDate(dto.getOperationDate());
			newCertificate.setType(CertificateType.CERT_INHOUSE);
			newCertificate.setRegisteredBy(crc);
			newCertificate.setThirdCertNo(dto.getDocumentNumber());
			newCertificate.setDuration(jobitem.getInst().getCalFrequency());
			newCertificate.setUnit(jobitem.getInst().getCalFrequencyUnit());
			newCertificate.setSignedBy(certificateValidatedBy);
			newCertificate.setStatus(CertStatusEnum.SIGNED);
			cal.getLinks().stream().findFirst().flatMap(cl -> Optional.of(cl.getJi().getInst()))
				.flatMap(i -> Optional.ofNullable(i.getInstrumentComplementaryField()))
				.flatMap(f -> Optional.ofNullable(f.getCertificateClass()))
				.ifPresent(newCertificate::setCertClass);
			newCertificate.setCalDate(dto.getOperationDate());
			List<Integer> allLinks = new ArrayList<>();
			for (CalLink cl : cal.getLinks())
				allLinks.add(cl.getId());
			this.createLinksForCert(newCertificate, allLinks, certificateValidatedBy, newCertificate.getCertDate());
			// set new cert into cal object
			if (cal.getCerts() == null) {
				cal.setCerts(new ArrayList<>());
			}
			cal.getCerts().add(newCertificate);
			this.insertCertificate(newCertificate, certificateValidatedBy);
			return newCertificate;
		}
		return certificate;
	}

	@Override
	public Certificate setStatus(Certificate certificate, String statusName) {
		certificate.setStatus(CertStatusEnum.valueOf(statusName));
		return certificate;
	}

	@Override
	public RestResultDTO<String> uploadOnSiteCertificate(Integer jobid, List<MultipartFile> multipartFiles,
			Locale locale, Contact user) throws IOException {

		RestResultDTO<String> res = new RestResultDTO<>();

		if (multipartFiles.isEmpty()) {
			// files list is empty
			return new RestResultDTO<>(false, this.messages.getMessage(RestErrors.CODE_CERTIFICATES_EMPTY_LIST, null,
					RestErrors.MESSAGE_CERTIFICATES_EMPTY_LIST, locale));
		} else {

			Job job = this.jobServ.get(jobid);

			// iterate on files
			for (MultipartFile multipartFile : multipartFiles) {

				// get the current multiPartFile and the complete FileName
				// complete file name with extension (may include path)
				String compFileName = multipartFile.getOriginalFilename();
				// file name with extension
				String fileName = FilenameUtils.getName(compFileName);
				// file name without extension
				String fileBaseName = FilenameUtils.getBaseName(compFileName);

				// check if it's a PDF or WORD file
				if (!"PDF".equalsIgnoreCase(FilenameUtils.getExtension(compFileName))
						&& !"DOC".equalsIgnoreCase(FilenameUtils.getExtension(compFileName))
						&& !"DOCX".equalsIgnoreCase(FilenameUtils.getExtension(compFileName))) {
					res.addError(new RestFieldErrorDTO(fileName,
							this.messages.getMessage(RestErrors.CODE_FILE_NAME_NOT_VALIDE, new Object[]{fileName},
									RestErrors.MESSAGE_FILE_NAME_NOT_VALIDE, locale)));
				}

				List<Certificate> certificates = this.getByThirdCertNoAndJobId(fileBaseName, job.getJobid());

				if (certificates.isEmpty()) {
					// certificate not found
					res.addError(new RestFieldErrorDTO(fileName,
							this.messages.getMessage(RestErrors.CODE_CERTIFICATE_NOT_FOUND,
									new Object[]{fileBaseName}, RestErrors.MESSAGE_CERTIFICATE_NOT_FOUND, locale)));
				} else {
					String fileExtension = FilenameUtils.getExtension(fileName);
					importCertificateFile(job.getJobno(), fileExtension, multipartFile.getBytes(), certificates);
				}
			}
		}
		if (res.getErrors().isEmpty()) {
			res.setSuccess(true);
			res.setMessage(this.messages.getMessage("certificate.alluploadedsuccessfuly", new Object[] {}, locale));
		} else {
			res.setSuccess(false);
			res.setMessage(this.messages.getMessage("certificate.upload.haserrors", new Object[] {}, locale));
		}

		return res;
	}

	@Override
	public void importCertificateFile(String jobNo, String fileExtension, byte[] bytes,
			List<Certificate> certificates) {
		for (Certificate certificate : certificates) {
			SystemComponent sc = this.scServ.findComponent(Component.CERTIFICATE);
			File directory = this.compDirServ.getDirectory(sc, jobNo);
			boolean fileExist = false;
			try {
				fileExist = writeFile(directory, certificate.getCertno() + "." + fileExtension, bytes);
			} catch (Exception e) {
				e.printStackTrace();
			}

			for (CertLink cl : certificate.getLinks()) {
				// send event for upload certificate
				UploadCertificateEvent uploadCertEvent = new UploadCertificateEvent(this, certificate, fileExist,
						cl.getJobItem());
				applicationEventPublisher.publishEvent(uploadCertEvent);
			}
		}
	}

    @Override
    public List<CertificateForInstrumentTooltipDto> findCertificatesForInstrumentTooltipByPlantId(Integer plantId) {
        return certDao.findCertificatesForInstrumentTooltipByPlantId(plantId);
    }

    @Override
	public List<Certificate> getByThirdCertNoAndJobId(String fileBaseName, int jobid) {
		return certDao.getByThirdCertNoAndJobId(fileBaseName, jobid);
	}

	@Override
	public List<File> certificateFilesForNumbers(String jobNumber, List<String> certNumbers) {
		val directory = this.compDirServ.getDirectory(this.scServ.findComponent(Component.CERTIFICATE), jobNumber);
		if (directory == null || !directory.exists() || !directory.isDirectory())
			return Collections.emptyList();

		val fileArray = directory.listFiles(
				file -> !file.isDirectory() && certNumbers.contains(FilenameUtils.getBaseName(file.getName())));
		if (fileArray == null)
			return Collections.emptyList();
		return Arrays.asList(fileArray);
	}

	@Override
	public List<CertificateProjectionDTO> getCertificatesForJobItemIds(Collection<Integer> jobItemIds) {
		List<CertificateProjectionDTO> result = Collections.emptyList();
		if (!jobItemIds.isEmpty()) {
			result = this.certDao.getCertificatesForJobItemIds(jobItemIds);
		}
		return result;
	}
	
	@Override
	public Integer getSubdivIdFromCalibration(int certid) {
		return this.certDao.getSubdivIdFromCalibration(certid);
	}
	
	@Override
	public ResultWrapper certificateReplacement(Integer jobItemId, Integer certificateId, Locale locale, CertificateReplacementType certType) {
		// 1) Validation part
		JobItem ji = this.jobItemService.findJobItem(jobItemId);
		Certificate cert = this.findCertificate(certificateId);
		if(ji == null) {
			String msg = this.messages.getMessage(RestErrors.CODE_JOB_ITEM_NOT_FOUND, new Object[] { jobItemId },
					RestErrors.MESSAGE_JOB_ITEM_NOT_FOUND, locale);
			return new ResultWrapper(false, msg);
		}
		
		if(cert == null || cert.getStatus().equals(CertStatusEnum.DELETED)) {
			String msg = this.messages.getMessage("certificatereplacement.validation.certnotfound", null,
					"Certificate not found", locale);
			return new ResultWrapper(false, msg);
		}
		
		if(!cert.getHasCertFile()) {
			String msg = this.messages.getMessage("certificatereplacement.validation.certnofile", null,
					"Certificate has no file", locale);
			return new ResultWrapper(false, msg);
		}

		if ((CertificateReplacementType.LAB_CERT.equals(certType) && !this.jobItemService.stateHasGroupOfKeyName(ji.getState(), StateGroup.START_LAB_CERTIFICATE_REPLACEMENT)) ||
				(CertificateReplacementType.THIRDPARTY_CERT.equals(certType) && !this.jobItemService.stateHasGroupOfKeyName(ji.getState(), StateGroup.START_THIRDPARTY_CERTIFICATE_REPLACEMENT)) ||
				(CertificateReplacementType.ONSITE_CERT.equals(certType) && !this.jobItemService.stateHasGroupOfKeyName(ji.getState(), StateGroup.START_ONSITE_CERTIFICATE_REPLACEMENT))) {
			String msg = this.messages.getMessage("certificatereplacement.validation.itemnotready", null,
				"JobItem not in the right status to perform certificate replacement", locale);
			return new ResultWrapper(false, msg);
		}

		// 2) Perform certificate replacement action
		// get the target work requirement
		List<JobItemWorkRequirement> jiwrs = ji.getWorkRequirements().stream()
				.filter(wr -> (wr.getWorkRequirement().getCapability() == null || cert.getCal() == null
						|| wr.getWorkRequirement().getCapability().getId() == cert.getCal().getCapability().getId())
						&& (wr.getWorkRequirement().getWorkInstruction() == null || cert.getCal() == null
						|| wr.getWorkRequirement().getWorkInstruction().getId().equals(cert.getCal().getWorkInstruction().getId()))
						&& wr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE))
				.collect(Collectors.toList());

		// if we found one work requirement
		if (jiwrs.size() == 1) {
			this.performCertificateReplacement(ji, jiwrs.get(0).getWorkRequirement(), cert, certType);
			return new ResultWrapper(true, null);
		} else if(jiwrs.size() == 0) {
			// no jiwrs found
			return new ResultWrapper(false, "WR Inconsistent", null);
		} else {
			List<Integer> jiWrIds = jiwrs.stream().map(JobItemWorkRequirement::getId).collect(Collectors.toList());
			return new ResultWrapper(false, "Multiple WR", jiWrIds, null);
		}

	}
	
	@Override
	public JobItemWorkRequirement performCertificateReplacement(JobItem ji, WorkRequirement wr, Certificate cert, CertificateReplacementType certType) {
		JobItemWorkRequirement jiwr = new JobItemWorkRequirement();

		WorkRequirement newWr = this.workRequirementService.duplicate(wr);

		jiwr.setId(null);
		jiwr.setJobitem(ji);
		jiwr.setWorkRequirement(newWr);
		newWr.getJobItemWorkRequirements().add(jiwr);
		ji.setNextWorkReq(jiwr);
		
		this.jobItemWorkRequirementService.save(jiwr);
		
		switch(certType) {
		case LAB_CERT : HookInterceptor_StartCertificateReplacement.Input inputLab = new HookInterceptor_StartCertificateReplacement.Input(cert, ji, null, null);
						startCertificateReplacement_hook.recordAction(inputLab);
						break;
		case THIRDPARTY_CERT : 	HookInterceptor_StartThirdPartyCertificateReplacement.Input inputTP = new HookInterceptor_StartThirdPartyCertificateReplacement.Input(cert, ji, null, null);
								startThirdPartyCertificateReplacement_hook.recordAction(inputTP);
								break;
		case ONSITE_CERT : 	HookInterceptor_StartOnSiteCertificateReplacement.Input inputOnsite = new HookInterceptor_StartOnSiteCertificateReplacement.Input(cert, ji, null, null);
							startOnSiteCertificateReplacement_hook.recordAction(inputOnsite);
							break;
		default:
			break;
		}
		
		// set certificate as to_be_replaced
		cert.setStatus(CertStatusEnum.TO_BE_REPLACED);
		this.updateCertificate(cert);
		
		return jiwr;
	}

	@Override
	public void searchCertificatesProjectionJPA(CertificateSearchForm form,
			PagedResultSet<CertificateSearchWrapper> prs, boolean includeDeletedAndCancelledCertificates, Locale locale) {
		this.certDao.searchCertificatesProjectionJPA(form, prs, includeDeletedAndCancelledCertificates);
		if(prs.getResults() != null) {
			List<Integer> certificateIds = prs.getResults().stream().filter(c -> c.getCertDto() != null)
					.map(c -> c.getCertDto().getCertId()).collect(Collectors.toList());
				
			List<WebCertificateJobItemLinkProjectionDTO> jobitemLinkDtos = this.certDao.searchCertificateJobItemLinksProjectionJPA(certificateIds, locale);

			List<WebCertificateInstrumentLinkProjectionDTO> instrumentLinkDtos = this.certDao.searchCertificateInstrumentLinksProjectionJPA(certificateIds, locale);
			//link the jobitemlinks and instrumentlinks to certificatesearchwrapper result
			prs.getResults().forEach(csw -> {
				// set jobitemlinks
				csw.getCertDto().setJobitemLinkDtos(jobitemLinkDtos.stream()
						.filter(jl -> jl.getCertId().equals(csw.getCertDto().getCertId()))
						.collect(Collectors.toList()));

				// set instrumentlinks
				csw.getCertDto().setInstrumentLinkDtos(instrumentLinkDtos.stream()
						.filter(il -> il.getCertId().equals(csw.getCertDto().getCertId()))
						.collect(Collectors.toList()));

				// set Certificate Encrypted File Path
				Integer firstInstLinkPlantId = CollectionUtils.isNotEmpty(csw.getCertDto().getInstrumentLinkDtos()) ? csw.getCertDto().getInstrumentLinkDtos().get(0).getPlantid() : null;
				String firstJobitemLinkJobNo = CollectionUtils.isNotEmpty(csw.getCertDto().getJobitemLinkDtos()) ? csw.getCertDto().getJobitemLinkDtos().get(0).getJobNo() : null;
				File certDirectory = this.getCertificateDirectory(csw.getCertDto().getType(), firstInstLinkPlantId, firstJobitemLinkJobNo);
				csw.getCertDto().setCertEncryptedFilePath(certDirectory);
			});
		
		}
	}

	@Override
	 public void signRestCertificate(Certificate certificate, Contact contact, Date issueDate, String thirdCertNo,
				Integer timeSpent, Date startDate) {
		certificate.getCal().getLinks().stream().map(CalLink::getJi).forEach(ji -> {
			CertificateStatusChangedEvent eventOnNewCert = new CertificateStatusChangedEvent(this, ji, certificate,
				certificate.getStatus(), CertStatusEnum.SIGNED);
			applicationEventPublisher.publishEvent(eventOnNewCert);
		});
		certificate.setStatus(CertStatusEnum.SIGNED);
		certificate.setSignedBy(contact);
		certificate.setCertDate(issueDate != null ? issueDate : new Date());
		if (thirdCertNo != null)
			certificate.setThirdCertNo(thirdCertNo);
		this.updateCertificate(certificate);
		this.certActionServ.logCertAction(certificate, CertificateAction.SIGNEDANDAPPROVED, contact);

		HookInterceptor_SignCertificate.Input input = new HookInterceptor_SignCertificate.Input(certificate, contact,
				timeSpent, startDate);
		hook_SignCertificate.recordAction(input);
	}
	
	@Override
	public void resetCertificateValidation(Certificate certificate, Contact contact) {
		// Adds an additional "pending" certificate validation record 
		
		CertificateValidation pendingValidation = new CertificateValidation();
		pendingValidation.setCert(certificate);
		pendingValidation.setDatevalidated(LocalDate.now());
		pendingValidation.setStatus(CertificateValidationStatus.PENDING);
		pendingValidation.setValidatedBy(contact);
		
		// This adds the new validation record to the certificate.
		certificate.setValidation(pendingValidation);
	}

	@Override
	public List<String> getRelatedInstrumentsInstructions(Integer certificateId) {
		Certificate certificate = this.findCertificate(certificateId);
		List<Instrument> instruments = this.getRelatedInstruments(certificate);
		if (instruments != null && !instruments.isEmpty()) {
			return instruments.stream().map(
					i -> Optional.ofNullable(calReqService.findCalReqsForInstrument(i)).orElse(new InstrumentCalReq()))
					.map(c -> Optional.ofNullable(c.getPublicInstructions()).orElse("")).collect(Collectors.toList());
		} else
			return null;
	}

	@Override
	public List<String> getRelatedInstrumentsAcceptanceCriteria(Integer certificateId) {
		Certificate certificate = this.findCertificate(certificateId);
		List<Instrument> instruments = this.getRelatedInstruments(certificate);
		if (instruments != null && !instruments.isEmpty()) {
			return instruments.stream()
					.map(i -> Optional.ofNullable(i.getInstrumentComplementaryField())
							.orElse(new InstrumentComplementaryField()))
					.map(cf -> Optional.ofNullable(cf.getCustomerAcceptanceCriteria()).orElse(""))
					.collect(Collectors.toList());
		} else
			return null;
	}

	@Override
	public String getRelatedInstrumentDeviation(Integer certificateId) {
		Certificate certificate = this.findCertificate(certificateId);
		Instrument instrument = this.getRelatedInstruments(certificate).get(0);
		String deviationUnits = instrument.getInstrumentComplementaryField() != null
				&& instrument.getInstrumentComplementaryField().getDeviationUnits() != null
						? instrument.getInstrumentComplementaryField().getDeviationUnits().getSymbol()
						: "";
		return certificate.getDeviation() != null ? certificate.getDeviation().toPlainString() + deviationUnits : "";
	}
	
	@Override
	public Certificate editCertificate( Integer certid, CertificateForm form, String username, Integer allocatedSubdivId){
		User user = this.userService.get(username);
		Certificate cert = this.findCertificate(certid);

		// Determine if anything impacting recall date has changed
		boolean calDateChanged = !DateTools.isSameDay(cert.getCalDate(), form.getCalDate());
		boolean durationChanged = (cert.getDuration() != null && !form.getDuration().equals(cert.getDuration()))
				|| (cert.getDuration() == null && form.getDuration() != null);
		boolean unitChanged = (cert.getUnit() != null && !form.getDurationUnit().equals(cert.getUnit()))
				|| (cert.getUnit() == null && form.getDurationUnit() != null);

		cert.setThirdDiv(form.getSubdivid() == null ? null : this.subdivService.get(form.getSubdivid()));
		cert.setThirdCertNo(form.getThirdCertNo());
		cert.setCertDate(form.getCertDate());
		cert.setCalDate(form.getCalDate());
		cert.setDuration(form.getDuration());
		cert.setUnit(form.getDurationUnit());
		cert.setRemarks(form.getRemarks());

		if (authenticationService.hasRight(user.getCon(), Permission.VERIFICATION_STATUT_EDIT, allocatedSubdivId)) {
			cert.setCalibrationVerificationStatus(form.getCalibrationVerificationStatus());
		}

		cert.setDeviation(form.getDeviation());
		cert.setCertClass(form.getCertificateClass());

		// if cal process is populated and has been changed
        if ((form.getCalProcessId() != null) && (cert.getCal() != null)
            && (!Objects.equals(cert.getCal().getCalProcess().getId(), form.getCalProcessId()))) {
            Calibration cal = cert.getCal();
            cal.setCalProcess(this.calProServ.get(form.getCalProcessId()));
            this.calServ.updateCalibration(cal);
        }

		// if anything potentially impacting recall date has been changed
		if (calDateChanged || durationChanged || unitChanged) {
			for (CertLink cl : cert.getLinks()) {
				Instrument inst = cl.getJobItem().getInst();

				// if we're in this loop we know there's at least one cert link,
				// so get most recent for instrument
				List<CertLink> certs = this.certLinkServ.searchCertLink(inst.getPlantid(), true);
				CertLink mostRecent = certs.get(0);

				Date mostRecentClientCert = null;
				if ((inst.getInstCertLinks() != null) && (inst.getInstCertLinks().size() > 0)) {
                    TreeSet<InstCertLink> icls = new TreeSet<>(new InstCertLinkComparator());
					icls.addAll(inst.getInstCertLinks());
					InstCertLink icl = icls.last();
					mostRecentClientCert = icl.getCert().getCalDate();
				}

				if (cert.getType().equals(CertificateType.CERT_INHOUSE)) {
					if ((cert.getCal() != null)) {
						Calibration lastCal = inst.getLastCal();

						if ((lastCal != null) && (cert.getCal().getId() == lastCal.getId())
								&& (mostRecent.getLinkId().intValue() == cl.getLinkId().intValue())
								&& ((mostRecentClientCert == null) || mostRecentClientCert.before(cert.getCalDate()))) {
							// set recall date again if cal is still most recent
							this.instrumentService.calculateRecallDateAfterEditingCert(inst, cert);
						}
					}
				} else if (cert.getType().equals(CertificateType.CERT_THIRDPARTY)) {
					if ((mostRecent.getLinkId().intValue() == cl.getLinkId().intValue())
							&& ((mostRecentClientCert == null) || mostRecentClientCert.before(cert.getCalDate()))) {
						// set recall date again if tp cert is most recent
						this.instrumentService.calculateRecallDateAfterTPCert(inst, cert);
					}
				}
			}
		}
		this.updateCertificate(cert);
		
		return cert;
	}

}