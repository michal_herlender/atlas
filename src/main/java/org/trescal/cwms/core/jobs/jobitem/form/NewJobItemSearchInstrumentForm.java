package org.trescal.cwms.core.jobs.jobitem.form;

import lombok.Data;
import org.trescal.cwms.core.jobs.jobitem.entity.enums.NewJobItemSearchOrderByEnum;

@Data
public class NewJobItemSearchInstrumentForm implements NewJobItemBaseSearchForm {

    private Integer coid;
    private Integer groupId;
    private Integer jobId;


    private Integer domainId;
    private Integer familyId;
    private Integer descId;
    private Integer mfrId;
    private Integer salesCatId;
    private String model;
    private NewJobItemSearchOrderByEnum orderBy;
    private String action;
    private Boolean excludeQuarantined = true;

    private Integer barcode;
    private String formerBarcode;
    private String serialNo;
    private String plantNo;
    private String customerDescription;


    private Integer resultsPerPage;
    private Integer instrumentsPageNo;
    private Integer companyModelsPageNo;
    private Integer allModelsPageNo;


}
