package org.trescal.cwms.core.jobs.calibration.dto;

import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

public class MultiCompleteCalItem
{
	private CalLink calLink;
	private ActionOutcome outcome;
	private Integer outcomeId;
	private int timeSpent;

	public MultiCompleteCalItem(CalLink calLink, ActionOutcome outcome, int timeSpent)
	{
		this.calLink = calLink;
		this.timeSpent = timeSpent;
		this.outcome = outcome;
		this.outcomeId = (outcome == null) ? null : outcome.getId();
	}

	public CalLink getCalLink()
	{
		return this.calLink;
	}

	public ActionOutcome getOutcome()
	{
		return this.outcome;
	}

	public Integer getOutcomeId()
	{
		return this.outcomeId;
	}

	public int getTimeSpent()
	{
		return this.timeSpent;
	}

	public void setCalLink(CalLink calLink)
	{
		this.calLink = calLink;
	}

	public void setOutcome(ActionOutcome outcome)
	{
		this.outcome = outcome;
	}

	public void setOutcomeId(Integer outcomeId)
	{
		this.outcomeId = outcomeId;
	}

	public void setTimeSpent(int timeSpent)
	{
		this.timeSpent = timeSpent;
	}
}