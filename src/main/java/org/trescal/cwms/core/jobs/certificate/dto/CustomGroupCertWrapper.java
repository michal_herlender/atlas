package org.trescal.cwms.core.jobs.certificate.dto;

import java.util.ArrayList;
import java.util.List;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;

public class CustomGroupCertWrapper
{
	private int calTypeId;
	private int duration;
	private int groupId;
	private List<Integer> jobItemIds;
	private boolean selected;

	public CustomGroupCertWrapper(JobItemGroup group)
	{
		this.calTypeId = (group.getJob().getDefaultCalType() != null) ? group.getJob().getDefaultCalType().getCalTypeId() : 1;
		this.duration = 12;
		this.selected = false;
		this.jobItemIds = new ArrayList<Integer>();

		if (group.isCalibrationGroup())
		{
			for (JobItem ji : group.getItems())
			{
				this.jobItemIds.add(ji.getJobItemId());
			}
		}
	}

	public int getCalTypeId()
	{
		return this.calTypeId;
	}

	public int getDuration()
	{
		return this.duration;
	}

	public int getGroupId()
	{
		return this.groupId;
	}

	public List<Integer> getJobItemIds()
	{
		return this.jobItemIds;
	}

	public boolean isSelected()
	{
		return this.selected;
	}

	public void setCalTypeId(int calTypeId)
	{
		this.calTypeId = calTypeId;
	}

	public void setDuration(int duration)
	{
		this.duration = duration;
	}

	public void setGroupId(int groupId)
	{
		this.groupId = groupId;
	}

	public void setJobItemIds(List<Integer> jobItemIds)
	{
		this.jobItemIds = jobItemIds;
	}

	public void setSelected(boolean selected)
	{
		this.selected = selected;
	}

}
