package org.trescal.cwms.core.jobs.job.views;

/**
 * Static message key / default text definitions for job exports
 *
 */
public class JobExportLabelDefinitions {
	
	public static String CODE_DEFAULT_SHEET_NAME = "report.defaultsheetname";
	public static String TEXT_DEFAULT_SHEET_NAME = "Sheet 1";
	
	public static String CODE_JOB_NO = "jobno";
	public static String TEXT_JOB_NO = "Job No";

	public static String CODE_ITEM_NO = "itemno";
	public static String TEXT_ITEM_NO= "Item No";

	public static String CODE_BARCODE = "barcode";
	public static String TEXT_BARCODE= "Barcode";

	public static String CODE_PLANTNO = "plantno";
	public static String TEXT_PLANTNO= "Plant No";

	public static String CODE_SERIALNO = "serialno";
	public static String TEXT_SERIALNO= "Serial No";

	public static String CODE_INSTRUMENT_MODEL_NAME = "instmodelname";
	public static String TEXT_INSTRUMENT_MODEL_NAME= "Instrument Model Name";

	public static String CODE_CUSTOMER_DESCRIPTION = "customerdescription";
	public static String TEXT_CUSTOMER_DESCRIPTION= "Customer Description";

	public static String CODE_SERVICE_TYPE = "servicetype";
	public static String TEXT_SERVICE_TYPE= "Service Type";

	// Intended for "Status - Job Item"
	public static String CODE_STATUS = "status";
	public static String TEXT_STATUS= "Status";

	// Intended for "Status - Job Item"
	public static String CODE_JOB_ITEM = "vm_global.jobitem";
	public static String TEXT_JOB_ITEM = "Job Item";

	// Intended for "Status - Remark" 
	public static String CODE_REMARK = "workflow.remark";
	public static String TEXT_REMARK = "Remark";

	// Intended for "Status - Last Updated" 
	public static String CODE_LAST_UPDATED = "jithirdparty.lastupdated";
	public static String TEXT_LAST_UPDATED = "Last Updated";

	public static String CODE_CAPABILITY_REF = "searchprocedureform.procedurereference";
	public static String TEXT_CAPABILITY_REF = "Capability Reference";

	public static String CODE_CONTRACT_REVIEW_BY = "workflow.contractreviewby";
	public static String TEXT_CONTRACT_REVIEW_BY = "Contract review by";
	
	public static String CODE_PICKUP_DATE = "addjob.pickupdate";
	public static String TEXT_PICKUP_DATE = "Pickup Date";

	public static String CODE_RECEIPT_DATE = "addjob.receiptdate";
	public static String TEXT_RECEIPT_DATE = "Receipt Date";
	
	public static String CODE_DATE_IN = "datein";
	public static String TEXT_DATE_IN = "Date In";

	public static String CODE_DUE_DATE = "duedate";
	public static String TEXT_DUE_DATE= "Due Date";

	public static String CODE_TURNAROUND = "jicontractreview.turnaround";
	public static String TEXT_TURNAROUND = "Turnaround";

	public static String CODE_COMPLETION_DATE = "docs.completiondate";
	public static String TEXT_COMPLETION_DATE = "Completion Date";

	// Meant for "Client Ref - Job" and "Client Ref - Job Item"
	public static String CODE_CLIENT_REF = "clientref";
	public static String TEXT_CLIENT_REF = "Client Ref";
	
	public static String CODE_CONTRACT_NO="contract.number";
	public static String TEXT_CONTRACT_NO ="Contract Number";

	public static String CODE_JOB = "job";
	public static String TEXT_JOB = "Job";

	public static String CODE_CLIENT_COMPANY = "clientcompany";
	public static String TEXT_CLIENT_COMPANY = "Client Company";

	// e.g "Client Subdivision"
	public static String CODE_SUBDIVISION = "subdivision";
	public static String TEXT_SUBDIVISION = "Subdivision";

	// e.g "Client Contact"
	public static String CODE_CONTACT = "contact";
	public static String TEXT_CONTACT = "Contact";

	public static String CODE_ON_BEHALF_COMPANY = "viewcertificate.onbehcomp";
	public static String TEXT_ON_BEHALF_COMPANY = "On-behalf company";
}
