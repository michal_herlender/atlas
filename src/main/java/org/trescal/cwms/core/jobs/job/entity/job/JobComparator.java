package org.trescal.cwms.core.jobs.job.entity.job;

import java.util.Comparator;

/**
 * Inner comparator class for sorting {@link Job}s when returning collections
 * of jobs, each with multiple items
 * 
 * @author jamiev
 */
public class JobComparator implements Comparator<Job>
{
	public int compare(Job job, Job anotherJob)
	{
		String id1 = (job).getJobno();
		String id2 = (anotherJob).getJobno();

		return id1.compareTo(id2);
	}
}