package org.trescal.cwms.core.jobs.calibration.form.validator;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.trescal.cwms.batch.jobs.operationsimport.ImportedOperationItem;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.form.ImportedOperationSynthesisForm;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class ImportedOperationsSynthesisFormValidator extends AbstractBeanValidator {

	@Autowired
	private SubdivService subdivService;
	@Autowired
	private CalibrationService calibrationService;
	@Autowired
	private ImportedOperationItemValidator rowValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(ImportedOperationSynthesisForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		super.validate(target, errors);

		ImportedOperationSynthesisForm form = (ImportedOperationSynthesisForm) target;
		Integer coid = subdivService.get(form.getSubdivid()).getComp().getCoid();

		// get groups
		List<List<ImportedOperationItem>> groups = this.calibrationService.groupImportedOperations(form.getRows());
		// only checked groups
		List<List<ImportedOperationItem>> checkedGroups = groups.stream().filter(i -> i.get(0).isChecked())
				.collect(Collectors.toList());

		for (List<ImportedOperationItem> group : checkedGroups) {

			// check all rows inside checked group
			group.forEach(row -> row.setChecked(true));

			for (ImportedOperationItem row : group) {
				int rowIndex = form.getRows().indexOf(row);
				ImportedOperationItem nextRow = rowIndex + 1 < form.getRows().size() ? form.getRows().get(rowIndex + 1)
						: null;
				errors.pushNestedPath("rows[" + rowIndex + "]");
				ValidationUtils.invokeValidator(this.rowValidator, row, errors, coid, nextRow);

				boolean duplicatedWR = form.getRows().stream().anyMatch(dto -> {
					return form.getRows().indexOf(dto) != form.getRows().indexOf(row) && dto.getJiWrId() != null
							&& dto.getJiWrId().equals(row.getJiWrId());
				});
				if (duplicatedWR)
					errors.rejectValue("jiWrId", "importedcalibrationssynthesis.duplicatedworkrequirement");

				errors.popNestedPath();
			}
		}

	}

}
