package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;

public interface GeneralServiceOperationDao extends AllocatedDao<Subdiv, GeneralServiceOperation, Integer> {
	
	List<GeneralServiceOperation> getByJobItemId(Integer jobitemid);
	
	GeneralServiceOperation getOnGoingByJobItemId(Integer jobitemid);
	
	GeneralServiceOperation getOnHoldByJobItemId(Integer jobitemid);
	
	GeneralServiceOperation getLastGSOByJobitemId(Integer jobitemid);
	
	GeneralServiceOperation getLastInstrumentGSO(Instrument instrument);
	
}
