package org.trescal.cwms.core.jobs.job.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class EditJobFormValidator extends AbstractBeanValidator {

	@Override
	public boolean supports(Class<?> clazz) {
		return EditJobForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		EditJobForm form = (EditJobForm) target;
		if (JobType.SITE.equals(form.getJobType())) {
			super.validate(target, errors, EditJobForm.OnsiteJobType.class);
		}
		else {
			super.validate(target, errors, EditJobForm.InhouseJobType.class);
		}
	}
}
