package org.trescal.cwms.core.jobs.jobitem.entity.requirement;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Entity representing a {@link JobItem}'s calibration requirement.
 * 
 * @author Richard
 */
@Entity
@Table(name = "requirement")
public class Requirement extends Auditable
{
	private Date created;
	private Contact createdBy;
	private boolean deleted;
	private Contact deletedBy;
	private Date deletedOn;
	private Integer id;
	private JobItem item;
	private String label;
	private String requirement;

	@NotNull
	@Column(name = "created", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreated()
	{
		return this.created;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", nullable = false)
	public Contact getCreatedBy()
	{
		return this.createdBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deletebyid")
	public Contact getDeletedBy()
	{
		return this.deletedBy;
	}

	@Column(name = "deletedon", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDeletedOn()
	{
		return this.deletedOn;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "reqid", nullable = false)
	@Type(type = "int")
	public Integer getId()
	{
		return this.id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid", nullable = false)
	public JobItem getItem()
	{
		return this.item;
	}

	@Length(min = 0, max = 50)
	@Column(name = "label", length = 50, nullable = true, unique = false)
	public String getLabel()
	{
		return this.label;
	}

	@NotNull
	@NotEmpty
	@Length(max = 2000)
	@Column(name = "requirement", length = 2000, columnDefinition="nvarchar(2000)")
	public String getRequirement()
	{
		return this.requirement;
	}

	@NotNull
	@Column(name = "deleted", nullable = false, columnDefinition="tinyint")
	public boolean isDeleted()
	{
		return this.deleted;
	}

	public void setCreated()
	{
		this.created = new Timestamp(new Date().getTime());
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public void setCreatedBy(Contact createdBy)
	{
		this.createdBy = createdBy;
	}

	public void setDeleted(boolean deleted)
	{
		this.deleted = deleted;
	}

	public void setDeletedBy(Contact deletedBy)
	{
		this.deletedBy = deletedBy;
	}

	public void setDeletedOn(Date deletedOn)
	{
		this.deletedOn = deletedOn;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public void setItem(JobItem item)
	{
		this.item = item;
	}

	public void setRequirement(String requirement)
	{
		this.requirement = requirement;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}