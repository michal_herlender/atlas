package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.form.JobItemForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@RequestMapping(value = "/jiinstfiles.htm")
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class JiInstrumentFilesController extends JobItemController {

	private static final Logger logger = LoggerFactory.getLogger(JiInstrumentFilesController.class);

	@Autowired
	SystemComponentService systemComponentService;

	@Autowired
	FileBrowserService fileBrowserService;

	@ModelAttribute("form")
	protected JobItemForm formBackingObject(
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") Integer jobitemid)
			throws Exception {
		JobItemForm form = new JobItemForm();
		JobItem ji = this.jobItemService.findJobItem(jobitemid);
		if ((ji == null) || (jobitemid == 0)) {
			logger.error("Jobitem with id " + jobitemid + " could not be found");
			throw new Exception("Unable to locate requested jobitem");
		}
		form.setJi(ji);
		return form;
	}

	@ModelAttribute("instrument")
	public Instrument populateInstrument(
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") Integer jobitemid) {
		JobItem jobItem = jobItemService.findJobItem(jobitemid);
		return jobItem.getInst();
	}

	@ModelAttribute("systemcomponent")
	public SystemComponent populateSystemComponent() {
		return systemComponentService.findComponent(Component.INSTRUMENT);
	}

	@ModelAttribute("rootfiles")
	public FileBrowserWrapper populateRootFiles(
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") Integer jobitemid) {
		JobItem jobItem = jobItemService.findJobItem(jobitemid);
		return (FileBrowserWrapper) fileBrowserService
				.getFilesForComponentRoot(Component.INSTRUMENT, jobItem.getInst().getPlantid(), new ArrayList<String>());
	}
	
	@ModelAttribute("filesName")
	public List<String> populateFilesName(
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") Integer jobitemid) {
		return fileBrowserService.getFilesName(populateRootFiles(jobitemid));
	}

	@PreAuthorize("hasAuthority('JI_INST_FILES')")
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView displayPage(@RequestParam(value = "jobitemid", required = true) Integer jobItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		Map<String, Object> refData = super.referenceData(jobItemId, contact, subdivDto.getKey(),username);
		return new ModelAndView("trescal/core/jobs/jobitem/jiinstrumentfiles", refData);
	}
}