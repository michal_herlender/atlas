package org.trescal.cwms.core.jobs.jobitem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CollectedInstrumentDto {
    private Integer id;
    private Integer plantId;
    private String mfr;
    private String model;
    private String description;
}
