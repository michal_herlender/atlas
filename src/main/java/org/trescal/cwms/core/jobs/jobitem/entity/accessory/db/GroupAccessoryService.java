package org.trescal.cwms.core.jobs.jobitem.entity.accessory.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.GroupAccessory;

public interface GroupAccessoryService extends BaseService<GroupAccessory, Integer> {

	/**
	 * Count all accessories linked to a item group of this job. Accessory free
	 * texts are counted as one accessory.
	 * 
	 * @param jobId identifier of the job
	 * @return quantity of group accessories linked to this job
	 */
	Integer countByJob(Integer jobId);
}