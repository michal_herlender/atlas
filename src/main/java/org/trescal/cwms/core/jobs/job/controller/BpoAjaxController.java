package org.trescal.cwms.core.jobs.job.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.jobs.job.dto.BpoDTO;
import org.trescal.cwms.core.jobs.job.entity.bpo.RelatedBPOSet;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;


@RestController
@RequestMapping("bpo")
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class BpoAjaxController {

    @Autowired
    BPOService bpoService;

    @PostMapping("save.json")
    @Secured({"BPO_FOR_COMPANY_CREATE", "COMPANY_BPO_EDIT"})
    BpoDTO save(@RequestBody BpoDTO inBpo, @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompay) {
        return bpoService.saveBpoDto(inBpo, allocatedCompay);
    }

    @GetMapping("get.json")
    BpoDTO get(@RequestParam Integer poId) {
        return new BpoDTO(bpoService.get(poId));
    }

    @GetMapping("relatedToContact.json")
    RelatedBPOSet getBPOsRelatedToContact(@RequestParam Integer personId, @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompany) {
        return bpoService.getAllBPOsRelatedToContact(personId, allocatedCompany);
    }
}
