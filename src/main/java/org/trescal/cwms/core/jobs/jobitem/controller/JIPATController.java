package org.trescal.cwms.core.jobs.jobitem.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemand;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemandStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.pat.PAT;
import org.trescal.cwms.core.jobs.jobitem.entity.pat.db.PATService;
import org.trescal.cwms.core.jobs.jobitem.form.JIPATForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Map;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class JIPATController extends JobItemController {
	private static final Logger logger = LoggerFactory.getLogger(JIPATController.class);

	@Autowired
	private PATService patServ;

	@ModelAttribute("form")
	protected JIPATForm formBackingObject(
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") Integer jobitemid)
			throws Exception {
		JIPATForm form = new JIPATForm();
		JobItem ji = this.jobItemService.findJobItem(jobitemid);
		if ((ji == null) || (jobitemid == 0)) {
			logger.error("Jobitem with id " + jobitemid + " could not be found");
			throw new Exception("Unable to locate requested jobitem");
		}
		form.setJi(ji);
		// set up new PAT that can be submitted
		PAT pat = new PAT();
		// fill in defaults for PAT results
		pat.setJi(ji);
		pat.setTestDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		pat.setPlugOk(null);
		pat.setFuseOk(true);
		pat.setCableOk(null);
		pat.setCaseOk(true);
		pat.setSafe(true);
		pat.setBondingOk(true);
		pat.setBondingReading(new BigDecimal("0.00"));
		pat.setInsulationOk(true);
		pat.setInsulationReading(new BigDecimal("0.00"));
		// set PAT onto form
		form.setPat(pat);
		return form;
	}

	@PreAuthorize("hasAuthority('JI_PAT')")
	@RequestMapping(value = "/jipat.htm", method = RequestMethod.POST)
	protected RedirectView onSubmit(@ModelAttribute("form") JIPATForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		// load current session into PAT before validation
		Contact contact = this.userService.get(username).getCon();
		form.getPat().setTestedBy(contact);
		JobItem ji = form.getJi();
		PAT pat = form.getPat();
		// persist PAT
		this.patServ.insertPAT(pat);
		jobItemService.mergeJobItem(ji);
		// redirect to same page
		return new RedirectView("jipat.htm?jobitemid=" + ji.getJobItemId());
	}

	@PreAuthorize("hasAuthority('JI_PAT')")
	@RequestMapping(value = "/jipat.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@RequestParam(value = "jobitemid") Integer jobItemId,
										 @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
										 @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		Map<String, Object> refData = super.referenceData(jobItemId, contact, subdivDto.getKey(),username);
		refData.put("additionalDemands", Arrays.asList(AdditionalDemand.values()));
		refData.put("additionalDemandStatus", Arrays.asList(AdditionalDemandStatus.values()));
		return new ModelAndView("trescal/core/jobs/jobitem/jipat", refData);
	}
}