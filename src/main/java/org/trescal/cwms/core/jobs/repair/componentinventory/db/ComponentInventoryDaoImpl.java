package org.trescal.cwms.core.jobs.repair.componentinventory.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.repair.componentinventory.ComponentInventory;

@Repository("ComponentInventoryDao")
public class ComponentInventoryDaoImpl extends BaseDaoImpl<ComponentInventory, Integer> implements ComponentInventoryDao {
	
	@Override
	protected Class<ComponentInventory> getEntity() {
		return ComponentInventory.class;
	}

}