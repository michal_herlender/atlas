package org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemActionForEmployeeDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemsActionWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.JobItemTransit;
import org.trescal.cwms.core.jobs.jobitem.form.genericentityvalidator.EditJobItemActionValidator;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemActionProjectionDTO;
import org.trescal.cwms.core.jobs.repair.dto.RepairTimeDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairTimeForm;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.db.RepairCompletionReportService;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperation;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_RecordProductionRepairTime;
import org.trescal.cwms.core.workflow.automation.antech.jobinterceptor.JobItemActionChange;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDateTime;
import static org.trescal.cwms.core.tools.TranslationUtils.getBestTranslation;

@Service("JobItemActionService")
public class JobItemActionServiceImpl extends BaseServiceImpl<JobItemAction, Integer> implements JobItemActionService {
	@Autowired
	private AuthenticationService authServ;
	@Autowired
	private EditJobItemActionValidator editValidator;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private JobItemActionDao jobItemActionDao;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private UserService userService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private RepairCompletionReportService rcrService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private HookInterceptor_RecordProductionRepairTime hookInterceptor_RecordProductionRepairTime;

	// Redefined here to enable AOP to trigger (not possible with inherited methods
	// or interface)
	@Override
	@JobItemActionChange
	public void save(JobItemAction object) {
		super.save(object);
	}
	
	@Override
	public void saveWithoutInterceptor(JobItemAction object) {
		super.save(object);
	}

	// Redefined here to enable AOP to trigger (not possible with inherited methods)
	@Override
	@JobItemActionChange
	public JobItemAction merge(JobItemAction object) {
		return super.merge(object);
	}

	// Redefined here to enable AOP to trigger (not possible with inherited methods)

	/**
	 * 2019-08-23 Changed to allow deletion (for all except first action)
	 * (a) by Permission.JI_ACTIVITY_OVERRIDE regardless of whether contact is set on job item action, or
	 * (b) if contact is same as the person that recorded the last action
	 *  
	 * Note, since 2019-03-21 we've had issues with startedBy being null on some transits (undiagnosed issue)
	 * hence the need to change
	 * 
	 * int jobItemId : job item that we'd like to delete last action for
	 * Contact contact : contact that we're checking privileges for   
	 * int allocatedSubdivId : subdiv that we are checking privileges for
	 */
	public Boolean canDeleteLastAction(int jobItemId, Contact contact, int allocatedSubdivId) {
		JobItem jobItem = jiServ.findJobItem(jobItemId);
		// get most recent active action for job item
		JobItemAction jia = this.findLastActiveActionForItem(jobItem);
		boolean result = false;
		if (jia != null && 
				jia.getStartStatus() != null) {
			// Must exist, and not be the first action to be deletable
			if (jia.getStartedBy() != null && 
					jia.getStartedBy().getId().equals(contact.getId())) {
				result = true;
			}
			else if (this.authServ.hasRight(contact, Permission.JI_ACTIVITY_OVERRIDE, allocatedSubdivId)) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * 2019-08-23 Removed servlet request / session references and  
	 * Note, not called externally by DWR.  
	 * Future refactoring should consider to split out all messages to validator  
	 */
	public ResultWrapper deleteJobItemActionById(int jiaId, String username, int allocatedSubdivId) {
		ResultWrapper resWrap = new ResultWrapper();

		Contact contact = this.userService.get(username).getCon();
		JobItemAction jia = this.jobItemActionDao.find(jiaId);
		JobItem ji = this.jiServ.findEagerJobItem(jia.getJobItem().getJobItemId());
		JobItemAction lastJia = this.findLastActiveActionForItem(ji);
		JobItemAction penultimateJia = this.findPenultimateActiveActionForItem(ji);

		if (lastJia.getId().intValue() == jia.getId().intValue()) {

			Boolean permission = this.canDeleteLastAction(ji.getJobItemId(), contact, allocatedSubdivId);

			if (permission) {
				// if the item is currently mid-way through a calibration
				if (this.jiServ.stateHasGroupOfKeyName(ji.getState(), StateGroup.CALIBRATION)) {
					resWrap.setSuccess(false);
					resWrap.setMessage(
						"You cannot delete a calibration activity from this page. Please go into the on-going calibration and click the 'Cancel calibration' button.");
					return resWrap;
				} else if (this.jiServ.stateHasGroupOfKeyName(ji.getState(), StateGroup.GENERALSERVICEOPERATION)) {
					resWrap.setSuccess(false);
					Locale locale = LocaleContextHolder.getLocale();
					resWrap.setMessage(this.messageSource.getMessage("generalserviceoperation.errors.deleteactivity", null, "You cannot delete a general service operation activity by this way. Please click on Complete Activity action and click the 'Cancel General service operation' button.", locale));
					return resWrap;
				}

				// if the activity is not yet complete, completely erase it from
				// the db
				if (lastJia instanceof JobItemActivity) {
					JobItemActivity lastAct = (JobItemActivity) lastJia;
					if (!lastAct.isComplete()) {
						// set the item status to what it was before the
						// activity began
						ji.setState(lastAct.getStartStatus());

						// remove the activity from the item's collection
						ji.getActions().remove(lastAct);

						// delete the incomplete activity
						this.delete(lastAct);

						HashMap<String, Object> map = new HashMap<>();
						map.put("state", ji.getState());
						resWrap.setResults(map);
						resWrap.setSuccess(true);
						return resWrap;
					}
				}

				jia.setDeleted(true);
				// SHOULD I CAST THIS AND THEN SET AS COMPLETE?
				// jia.setComplete(true);
				jia.setDeletedBy(contact);
				jia.setDeleteStamp(new Date());
				// Triggers job item action interceptor
				this.merge(jia);

				if (penultimateJia != null)
					ji.setState(penultimateJia.getEndStatus());
				// Triggers job item action interceptor
				this.merge(jia);

				// if the activity is a transit activity, try to set the current
				// address of the item back to what it was before the activity
				if (jia instanceof JobItemTransit) {
					JobItemTransit jit = (JobItemTransit) jia;
					ItemState state = jit.getStartStatus();

					// if the transit was a despatch or third party transit
					if (this.jiServ.stateHasGroupOfKeyName(state, StateGroup.DESPATCH)
						|| this.jiServ.stateHasGroupOfKeyName(state, StateGroup.TPTRANSIT)) {
							ji.setCurrentAddr(jit.getFromAddr());
					}
					// else if the transit was a receipt
					else if (this.jiServ.stateHasGroupOfKeyName(state, StateGroup.RECEIVE)) {
						ji.setCurrentAddr(null);
					}
				}

				// No need to update job item, it's a JPA managed entity
				// this.jiServ.updateJobItem(ji);

				HashMap<String, Object> map = new HashMap<>();
				map.put("jia", jia);
				map.put("state", ji.getState());
				resWrap.setResults(map);
				resWrap.setSuccess(true);
			} else {
				resWrap.setSuccess(false);
				resWrap.setMessage("There was an error attempting to delete the last activity on item " + ji.getItemNo()
					+ " on Job " + ji.getJob().getJobno());
			}
		} else {
			resWrap.setSuccess(false);
			resWrap.setMessage("This is no longer the last activity on item " + ji.getItemNo() + " on Job "
					+ ji.getJob().getJobno());
		}

		return resWrap;
	}

	public ResultWrapper editAction(int jobItemActionId, String timeSpent, String remark, HttpSession session) {
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact contact = this.userService.getEagerLoad(username).getCon();

		// find the activity
		JobItemAction jia = this.get(jobItemActionId);

		// check integer
		if (!NumberTools.isAnInteger(timeSpent)) {
			return new ResultWrapper(false, "Time spent must be an integer", null, null);
		}

		// copy properties to fake bean
		JobItemActivity temp = new JobItemActivity();
		BeanUtils.copyProperties(jia, temp);

		// set the new fields
		temp.setTimeSpent(Integer.parseInt(timeSpent));
		temp.setRemark(remark);

		// validate
		BindException errors = new BindException(temp, "temp");
		this.editValidator.validate(temp, errors);

		// check authorization
		if (temp.getCompletedBy().getPersonid() != contact.getPersonid()) {
            errors.rejectValue("completedBy", "Wrong User Exception",
                "Only " + temp.getCompletedBy().getName() + " may edit this activity.");
        }

		// if validation passes
		if (!errors.hasErrors()) {
			// copy properties back and update
			BeanUtils.copyProperties(temp, jia);
			this.merge(jia);
		}

		// return results
		return new ResultWrapper(errors, jia);
	}

	public JobItemAction findLastActiveActionForItem(JobItem jobItem) {
		return jobItem.getActions().stream().filter(action -> !action.isDeleted()).min((a1, a2) -> a2.getId() - a1.getId()).orElse(null);
	}

	public JobItemAction findPenultimateActiveActionForItem(JobItem jobItem) {
		return jobItem.getActions().stream().filter(action -> !action.isDeleted())
				.sorted((a1, a2) -> a2.getId() - a1.getId()).skip(1).findFirst().orElse(null);
	}

	@Override
	public List<JobItemActionForEmployeeDTO> findNotTimeRelated(Contact employee, LocalDate start, LocalDate end) {
		return jobItemActionDao.findNotTimeRelated(employee, start, end);
	}

	@Override
	public List<JobItemActionForEmployeeDTO> findTimeRelated(Contact employee, LocalDate start, LocalDate end) {
		return jobItemActionDao.findTimeRelated(employee, start, end);
	}
	
	@Override
	public List<JobItemActionProjectionDTO> getProjectionDTOsForJobItem(Integer jobItemId, Boolean deleted, Locale locale) {
		if (jobItemId == null) throw new IllegalArgumentException("jobItemId must not be null or 0");
		if (jobItemId == 0) throw new IllegalArgumentException("jobItemId must not be 0");
		if (locale == null) throw new IllegalArgumentException("locale must not be null");
		return jobItemActionDao.getProjectionDTOsForJobItem(jobItemId, deleted, locale);
	}

	@Override
	public Integer summarizeTimeRelated(Contact employee, LocalDate start, LocalDate end) {
		return jobItemActionDao.summarizeTimeRelated(employee, start, end);
	}

	@Override
	public Integer summarizeNotTimeRelated(Contact employee, LocalDate start, LocalDate end) {
		return jobItemActionDao.summarizeNotTimeRelated(employee, start, end);
	}

	public ResultWrapper webGetJobItemActions(int jobItemId) {
		JobItem jobItem = jiServ.findJobItem(jobItemId);
		List<JobItemAction> actions = jobItem.getActions().stream().filter(action -> !action.isDeleted())
			.sorted(Comparator.comparingInt(JobItemAction::getId)).collect(Collectors.toList());
		List<JobItemsActionWrapper> result = new ArrayList<>();
		if (actions.size() > 0) {
			for (JobItemAction ji : actions) {
				if (!ji.getActivity().isOverride()) {
					JobItemsActionWrapper jia = new JobItemsActionWrapper();
					jia.setActivityDesc(ji.getActivityDesc());
					if (ji.getActivity() != null && ji.getActivity().getTranslations() != null
						&& ji.getActivity().getTranslations().size() > 0) {
                        // description of Activity --> localized
                        jia.setActivityDesc(getBestTranslation(ji.getActivity().getTranslations()).orElse("")
                        );
                    }
					if (ji.getEndStatus() == null)
						jia.setEndStatus("-- Unknown Status --");
					else {
						jia.setEndStatus(
                            getBestTranslation(ji.getEndStatus().getTranslations()).orElse("")
						);
					}

					if (!ji.isTransit()) {
						if (ji instanceof JobItemActivity) {
							JobItemActivity j = (JobItemActivity) ji;
							if (j.getCompletedBy() != null) {
								jia.setName(j.getCompletedBy().getName());
							}
						}
					} else {
						if (ji.getStartedBy() != null) {
							jia.setName(ji.getStartedBy().getName());
						}
					}

					jia.setEndStamp(ji.getEndStamp());

					result.add(jia);
				}
			}
		}
		return new ResultWrapper(true, "", result, null);
	}

	@Override
	protected BaseDao<JobItemAction, Integer> getBaseDao() {
		return this.jobItemActionDao;
	}

	@Override
	public List<RepairTimeDTO> findTechnicianRepairTimeByRcr(Integer jiId, String rcrIdentifier, Contact contact) {
		return this.jobItemActionDao.findTechnicianRepairTimeByRcr(jiId, rcrIdentifier, contact);
	}

	@Override
	public void editRepairTimeProgressActivities(RepairTimeForm form) {
		if(form.getRepairTimes() != null) {
			// edit only the current technicien repair time
			List<RepairTimeDTO> dtos = form.getRepairTimes().stream().filter(rt -> rt.getTechnicianId() == null || Objects.equals(form.getTechnicianId(), rt.getTechnicianId())).collect(Collectors.toList());
			for (RepairTimeDTO dto : dtos) {
				JobItemAction action = this.get(dto.getJobItemActionId());
				if (action != null) {
					action.setStartStamp(dateFromLocalDateTime(dto.getStartDate()));
					Calendar endDate = Calendar.getInstance();
					endDate.setTime(dateFromLocalDateTime(dto.getStartDate()));
					endDate.add(Calendar.MINUTE, dto.getTimespentMinute() + 60 * dto.getTimespentHour());
					action.setEndStamp(endDate.getTime());
					action.setTimeSpent(dto.getTimespentMinute() + 60 * dto.getTimespentHour());
					String[] oldRemarkSplited = action.getRemark().split("<br/>");
					if (oldRemarkSplited.length == 2)
						action.setRemark(action.getRemark().concat("<br/>").concat(dto.getTechnicianComment()));
					else
						action.setRemark(oldRemarkSplited[0].concat("<br/>").concat(oldRemarkSplited[1]).concat("<br/>").concat(dto.getTechnicianComment()));
					this.merge(action);
				} else if (dto.getStartDate() != null && (dto.getTimespentHour() != 0 || dto.getTimespentMinute() != 0)) {
					JobItem ji = this.jiServ.get(form.getJobItemId());
					RepairCompletionReport rcr = this.rcrService.get(form.getRepairCompletionReportId());
					Contact contact = this.contactService.get(form.getTechnicianId());
					Integer timespent = dto.getTimespentMinute() + (dto.getTimespentHour() * 60);
					HookInterceptor_RecordProductionRepairTime.Input input = new HookInterceptor_RecordProductionRepairTime.Input(ji, rcr.getIdentifier(), timespent, contact, dateFromLocalDateTime(dto.getStartDate()), dto.getTechnicianComment(), dto.getOperationName());
					hookInterceptor_RecordProductionRepairTime.recordAction(input);
				}
			}
			// update rcr operations times
			RepairCompletionReport rcr = this.rcrService.get(form.getRepairCompletionReportId());
			List<RepairTimeDTO> repairTimes = this.jobItemActionDao.findTechnicianRepairTimeByRcr(form.getJobItemId(), rcr.getIdentifier(), null);
			for (ValidatedFreeRepairOperation vfro : rcr.getValidatedFreeRepairOperations()) {
				List<RepairTimeDTO> rtsDto = repairTimes.stream().filter(rt -> {
					String[] commentSplited = rt.getTechnicianComposedComment().split("<br/>");
					String operationName = commentSplited[1];
					return vfro.getFreeRepairOperation().getName().equals(operationName);
				}).collect(Collectors.toList());

				if (!rtsDto.isEmpty()) {
					int totalLaborTime = 0;
					for (RepairTimeDTO e : rtsDto)
						totalLaborTime += (e.getTimespentHour() * 60) + e.getTimespentMinute();
					if (totalLaborTime > 0)
						vfro.setLabourTime(totalLaborTime);
				}
			}
			
			for (FreeRepairOperation fro : rcr.getFreeRepairOperations()) {
				List<RepairTimeDTO> rtsDto = repairTimes.stream().filter(rt -> {
					String[] commentSplited = rt.getTechnicianComposedComment().split("<br/>");
					String operationName = commentSplited[1];
					return fro.getName().equals(operationName);
				}).collect(Collectors.toList());

				if (!rtsDto.isEmpty()) {
					int totalLaborTime = 0;
					for (RepairTimeDTO e : rtsDto)
						totalLaborTime += (e.getTimespentHour() * 60) + e.getTimespentMinute();
					if (totalLaborTime > 0)
						fro.setLabourTime(totalLaborTime);
				}
			}
			this.rcrService.merge(rcr);
		}
	}

	@Override
	public void manageRepairTimeProgressActivities(RepairTimeForm form) {
		if(form.getAction().equals("EDIT"))
			this.editRepairTimeProgressActivities(form);
		else if(form.getAction().equals("DELETE") && form.getItemToDeleteId() != null)
		{
			JobItemAction jia = this.get(form.getItemToDeleteId());
			if(jia != null) {
				RepairCompletionReport rcr = this.rcrService.get(form.getRepairCompletionReportId());
				String[] remarkSplited = jia.getRemark().split("<br/>");
				rcr.getValidatedFreeRepairOperations().stream().filter(o -> o.getFreeRepairOperation().getName().equals(remarkSplited[1])).forEach(o -> o.setLabourTime(o.getLabourTime()-jia.getTimeSpent()));
				rcr.getFreeRepairOperations().stream().filter(o -> o.getName().equals(remarkSplited[1])).forEach(o -> o.setLabourTime(o.getLabourTime()-jia.getTimeSpent()));
				this.rcrService.merge(rcr);
				this.delete(jia);
			}
		}
	}

	@Override
	public List<Integer> findNotProcessedActionsIdForAdveso() {
		return this.jobItemActionDao.findNotProcessedActionsIdForAdveso();
	}

}