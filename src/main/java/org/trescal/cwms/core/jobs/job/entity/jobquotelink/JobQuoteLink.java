package org.trescal.cwms.core.jobs.job.entity.jobquotelink;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Represents the link between a {@link Job} and a {@link Quotation}.
 * 
 * @author richard
 */
@Entity
@Table(name = "jobquotelink", uniqueConstraints = { @UniqueConstraint(columnNames = { "jobid", "quoteid" }) })
public class JobQuoteLink extends Auditable
{
	private int id;
	private Job job;
	private Contact linkedBy;
	private Date linkedOn;
	private Quotation quotation;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobid", nullable = false)
	public Job getJob()
	{
		return this.job;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "linkedby", nullable = false)
	public Contact getLinkedBy()
	{
		return this.linkedBy;
	}

	@NotNull
	@Column(name = "linkedon", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLinkedOn()
	{
		return this.linkedOn;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quoteid", nullable = false)
	public Quotation getQuotation()
	{
		return this.quotation;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setJob(Job job)
	{
		this.job = job;
	}

	public void setLinkedBy(Contact linkedBy)
	{
		this.linkedBy = linkedBy;
	}

	public void setLinkedOn(Date linkedOn)
	{
		this.linkedOn = linkedOn;
	}

	public void setQuotation(Quotation quotation)
	{
		this.quotation = quotation;
	}
}