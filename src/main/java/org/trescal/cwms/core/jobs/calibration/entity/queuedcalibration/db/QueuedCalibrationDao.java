package org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.QueuedCalibration;

public interface QueuedCalibrationDao extends BaseDao<QueuedCalibration, Integer> {
	
	List<QueuedCalibration> getAccreditedActiveQueuedCalibrations(int personId);
	
	List<QueuedCalibration> getAccreditedActiveQueuedCalibrationsForBatches(List<Integer> batchIds, int personId);
	
	List<QueuedCalibration> getActiveQueuedCalibrations();
	
	List<QueuedCalibration> getActiveQueuedCalibrationsForBatches(List<Integer> batchIds);
	
	List<QueuedCalibration> getActiveQueuedCalibrationsOrderedByJob();
	
	List<QueuedCalibration> getQueuedCalibrationsForReprint();
	
	List<QueuedCalibration> getQueuedCalibrationsToPrintNow();
	
	List<QueuedCalibration> getQueuedCalibrationsWithIds(List<Integer> ids);
}