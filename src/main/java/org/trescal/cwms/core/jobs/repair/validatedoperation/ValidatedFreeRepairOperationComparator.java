package org.trescal.cwms.core.jobs.repair.validatedoperation;

import java.util.Comparator;

public class ValidatedFreeRepairOperationComparator implements Comparator<ValidatedFreeRepairOperation> {

	@Override
	public int compare(ValidatedFreeRepairOperation o1, ValidatedFreeRepairOperation o2) {
		return o1.getFreeRepairOperation().getPosition() == o2.getFreeRepairOperation().getPosition()
				? (o1.getValidatedOperationId() - o2.getValidatedOperationId())
				: o1.getFreeRepairOperation().getPosition() - o2.getFreeRepairOperation().getPosition();
	}

}
