package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@MappedSuperclass
public abstract class AbstractJobItemNotInvoiced {

	private LocalDate date;
	private Contact contact;
	private NotInvoicedReason reason;
	private String comments;
	private Invoice periodicInvoice;

	@NotNull
    @Column(name = "date", nullable = false, columnDefinition = "date")
    public LocalDate getDate() {
		return date;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contactid", nullable = false)
	public Contact getContact() {
		return contact;
	}

	@NotNull
	@Enumerated(EnumType.ORDINAL)
	public NotInvoicedReason getReason() {
		return reason;
	}

	@NotNull
	@Column(name = "comments", nullable = false, columnDefinition = "nvarchar(200)")
	public String getComments() {
		return comments;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "periodicinvoiceid")
	public Invoice getPeriodicInvoice() {
		return periodicInvoice;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public void setReason(NotInvoicedReason reason) {
		this.reason = reason;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public void setPeriodicInvoice(Invoice periodicInvoice) {
		this.periodicInvoice = periodicInvoice;
	}
}