package org.trescal.cwms.core.jobs.jobitem.entity.defect;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;

@Entity
@Table(name = "defect")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public abstract class Defect extends Auditable
{
	private String desc;
	private int id;

	@Column(name = "description", length=500)
	@Length(max = 500)
	public String getDesc()
	{
		return this.desc;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	public void setDesc(String desc)
	{
		this.desc = desc;
	}

	public void setId(int id)
	{
		this.id = id;
	}
}
