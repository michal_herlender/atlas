package org.trescal.cwms.core.jobs.jobitem.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JobItemActivityRequestDTO
{
    private Integer jobItemId;

    private Integer activityId;

}
