package org.trescal.cwms.core.jobs.jobitem.controller;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.db.JobCostingCalibrationCostService;
import org.trescal.cwms.core.pricing.lookup.PriceLookupRequirements;
import org.trescal.cwms.core.pricing.lookup.PriceLookupService;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupInput;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupInputFactory;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutput;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutputComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;

import java.time.LocalDate;
import java.util.*;

/**
 * Replacement for DWR "Alternative Costs" overlay
 * Provides model and forwards to JSP overlay 
 * Created 2019-09-26
 *
 */
@Controller @IntranetController
public class CostSourceController {
	@Autowired
	private CatalogPriceService catalogPriceService;
	@Autowired
	private JobCostingCalibrationCostService jobCostingCalibrationCostService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private PriceLookupService priceLookupService;
	@Autowired
	private QuotationItemService qiService;
	@Value("${cwms.config.quotation.resultsyearfilter}")
	private Integer quotationResultsYearFilter;
	
	@RequestMapping(value="jobitemcostsource.htm", method=RequestMethod.GET)
	public String handleRequest(Model model,
			@RequestParam(name="jobitemid") Integer jobitemid) {

		JobItem jobItem = this.jobItemService.findJobItem(jobitemid);
		// Outputs are sorted by preference
		SortedSet<PriceLookupOutput> outputs = getPriceLookupOutputs(jobItem);
		Map<PriceLookupOutput, Quotationitem> quotationItemSelectableOutputs = new TreeMap<>(new PriceLookupOutputComparator());
		Map<PriceLookupOutput, Quotationitem> quotationItemOtherOutputs = new TreeMap<>(new PriceLookupOutputComparator());
		Map<PriceLookupOutput, CatalogPrice> catalogPriceOutputs = new TreeMap<>(new PriceLookupOutputComparator());
		// For now, use entities and refactor to DTOs if needed for performance
		LocalDate today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		for (PriceLookupOutput plo : outputs) {
			if (plo.getQuotationItemId() != null) {
				Quotationitem qi = this.qiService.findQuotationItem(plo.getQuotationItemId());
				if (plo.getPreferredQuotation() ||
					(plo.getQuotationAccepted() && plo.getQuotationIssued() &&
						plo.getQuotationExpiryDate() != null && plo.getQuotationExpiryDate().isAfter(today))) {
					quotationItemSelectableOutputs.put(plo, qi);
				} else {
					quotationItemOtherOutputs.put(plo, qi);
				}
			}
			else if (plo.getCatalogPriceId() != null) {
				CatalogPrice cp = this.catalogPriceService.get(plo.getCatalogPriceId());
				catalogPriceOutputs.put(plo, cp);
			}
		}

		int clientCompanyId = jobItem.getJob().getCon().getSub().getComp().getCoid();
		val jobCostingOutputs = this.jobCostingCalibrationCostService.findMatchingCosts(
			jobItem.getInst().getPlantid(), clientCompanyId, jobItem.getInst().getModel().getModelid(), jobItem.getServiceType().getCalibrationType().getCalTypeId(), 1, null);
		
		model.addAttribute("businessCurrency", jobItem.getJob().getOrganisation().getComp().getCurrency());
		model.addAttribute("quotationItemSelectableOutputs", quotationItemSelectableOutputs);
		model.addAttribute("quotationItemOtherOutputs", quotationItemOtherOutputs);
		model.addAttribute("quotationRegDateAfter", getQuotationRegDateAfter());
		model.addAttribute("jobCostingOutputs", jobCostingOutputs);
		model.addAttribute("catalogPriceOutputs", catalogPriceOutputs);
		model.addAttribute("jobitem", jobItem);

		return "trescal/core/jobs/jobitem/costsourceoverlay";
	}

	private LocalDate getQuotationRegDateAfter() {
		return LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).minusYears(quotationResultsYearFilter);
	}

	private SortedSet<PriceLookupOutput> getPriceLookupOutputs(JobItem jobItem) {
		Integer businessCompanyId = jobItem.getJob().getOrganisation().getComp().getCoid();
		Integer clientCompanyId = jobItem.getJob().getCon().getSub().getComp().getCoid();
		Integer serviceTypeId = jobItem.getServiceType().getServiceTypeId();
		Integer instrumentId = jobItem.getInst().getPlantid();
		List<Integer> includeQuotationIds = getQuotationIds(jobItem);

		PriceLookupRequirements reqs = new PriceLookupRequirements(businessCompanyId, clientCompanyId, true);
		reqs.setFirstMatchSearch(false);
		reqs.setIncludeQuotationIds(includeQuotationIds);
		reqs.setQuotationAccepted(null);
		reqs.setQuotationIssued(null);
		reqs.setQuotationExpiryDateAfter(null);
		reqs.setQuotationRegDateAfter(getQuotationRegDateAfter());
		
		PriceLookupInputFactory factory = new PriceLookupInputFactory(reqs);
		PriceLookupInput input = factory.addInstrument(serviceTypeId, instrumentId);
		
		reqs.addInput(input);
		this.priceLookupService.findPrices(reqs);
		return input.getOutputs();
	}
	
	private List<Integer> getQuotationIds(JobItem jobItem) {
		List<Integer> includeQuotationIds = new ArrayList<>();
		jobItem.getJob().getLinkedQuotes().forEach(lq -> includeQuotationIds.add(lq.getQuotation().getId()));
		if (jobItem.getContract() != null && jobItem.getContract().getQuotation() != null) {
			includeQuotationIds.add(jobItem.getContract().getQuotation().getId());
		}
		return includeQuotationIds;
	}
}