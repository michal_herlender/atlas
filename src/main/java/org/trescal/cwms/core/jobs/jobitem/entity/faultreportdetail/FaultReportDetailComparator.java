package org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail;

import java.util.Comparator;

public class FaultReportDetailComparator implements Comparator<FaultReportDetail>
{
	@Override
	public int compare(FaultReportDetail o1, FaultReportDetail o2)
	{
		if ((o1.getId() == 0) && (o2.getId() == 0))
		{
			return 1;
		}
		else
		{
			return ((Integer) o1.getId()).compareTo(o2.getId());
		}
	}
}
