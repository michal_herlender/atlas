package org.trescal.cwms.core.jobs.calibration.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.calibration.dto.NewCalibrationStartedDTO;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.calibration.form.NewCalibrationForm;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@JsonController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class StartCalibrationToolController {
	
	@Autowired
	private CalibrationProcessService calProcessService;
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private UserService userService;
	
	private String fillToolLink(String toolLink, JobItem jobItem, Contact contact) {
		certificateService.createOrUpdateReservedCertificates(jobItem, contact, new Date());
		Certificate certificate = certificateService.getReservedCertificate(jobItem);
		return toolLink.replace("{0}", certificate.getCertno());
	}

	@RequestMapping(value = "/startcalibrationtool.json", method = RequestMethod.POST)
	@ResponseBody
	public NewCalibrationStartedDTO onToolSubmit(
			@RequestParam(name = "issuenow", required = false, defaultValue = "false") Boolean issueNow,
			@RequestParam(name = "signnow", required = false, defaultValue = "false") Boolean signNow,
			@RequestParam(value = "jobItemId", required = true) Integer jobItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute("form") NewCalibrationForm form, BindingResult bindingResult) throws Exception {
		CalibrationProcess calProcess = calProcessService.get(form.getCalProcessId());
		Contact contact = userService.get(username).getCon();
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		// TODO - discuss / determine - should the calibration to be started here, or leave it up to the calibration tool?
		String redirectURL = "jicalibration.htm?jobitemid="+jobItemId;
		return new NewCalibrationStartedDTO(redirectURL, fillToolLink(calProcess.getToolLink(), jobItem, contact));
	}
}