package org.trescal.cwms.core.jobs.job.entity.jobnote.db;

import java.util.List;

import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobnote.JobNote;

/**
 * Interface for accessing and manipulating {@link JobNote} entities.
 * 
 * @author jamiev
 */
public interface JobNoteService
{
	/**
	 * Deletes the given {@link JobNote} from the database.
	 * 
	 * @param jobnote the {@link JobNote} to delete.
	 */
	void deleteJobNote(JobNote jobnote);

	/**
	 * Deletes the {@link JobNote} with the given ID from the database.
	 * 
	 * @param id the ID of the {@link JobNote} to delete.
	 */
	void deleteJobNoteById(int id);

	/**
	 * Returns the {@link JobNote} with the given ID.
	 * 
	 * @param id the {@link JobNote} ID.
	 * @return the {@link JobNote}
	 */
	JobNote findJobNote(int id);

	/**
	 * Returns all job notes for the {@link Job} with the given ID that are
	 * currently active.
	 * 
	 * @param jobid the ID of the {@link Job} to get notes for.
	 * @return the {@link List} of {@link JobNote} entities.
	 */
	List<JobNote> getActiveJobNotesForJob(int jobid);

	/**
	 * Returns all {@link JobNote} entities stored in the database.
	 * 
	 * @return the {@link List} of {@link JobNote} entities.
	 */
	List<JobNote> getAllJobNotes();

	/**
	 * Inserts the given {@link JobNote} into the database.
	 * 
	 * @param jobnote the {@link JobNote} to insert.
	 */
	void insertJobNote(JobNote jobnote);

	/**
	 * Inserts the given {@link JobNote} if it is not currently stored in the
	 * database, and otherwise updates the existing {@link JobNote}.
	 * 
	 * @param jobnote the {@link JobNote} to insert or update.
	 */
	void saveOrUpdateJobNote(JobNote jobnote);

	/**
	 * Updates the given {@link JobNote} in the database.
	 * 
	 * @param jobnote the {@link JobNote} to update.
	 */
	void updateJobNote(JobNote jobnote);
}