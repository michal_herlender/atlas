package org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup_;

@Repository("JobItemGroupDao")
public class JobItemGroupDaoImpl extends BaseDaoImpl<JobItemGroup, Integer> implements JobItemGroupDao {
	@Override
	protected Class<JobItemGroup> getEntity() {
		return JobItemGroup.class;
	}

	@SuppressWarnings("unchecked")
	public List<JobItemGroup> getAll(int jobId) {
		Criteria crit = getSession().createCriteria(JobItemGroup.class);
		crit.createCriteria("job").add(Restrictions.eq("jobid", jobId));
		return crit.list();
	}

	public JobItemGroup getGroupWithItems(int groupId) {
		Criteria criteria = getSession().createCriteria(JobItemGroup.class);
		criteria.add(Restrictions.idEq(groupId));
		criteria.setFetchMode("items", FetchMode.JOIN);
		criteria.setFetchMode("items.inst", FetchMode.JOIN);
		criteria.setFetchMode("items.inst.mfr", FetchMode.JOIN);
		criteria.setFetchMode("items.inst.model", FetchMode.JOIN);
		criteria.setFetchMode("items.inst.model.modelMfrType", FetchMode.JOIN);
		criteria.setFetchMode("items.inst.model.mfr", FetchMode.JOIN);
		criteria.setFetchMode("items.inst.model.description", FetchMode.JOIN);
		criteria.setFetchMode("items.calType", FetchMode.JOIN);
		return (JobItemGroup) criteria.uniqueResult();
	}

	@Override
	public List<Integer> getJobitemsByGroupId(int groupId) {
		return getResultList(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<JobItem> root = cq.from(JobItem.class);
			cq.where(cb.equal(root.get(JobItem_.group), groupId));
			cq.select(root.get(JobItem_.jobItemId));
			cq.distinct(true);
			return cq;
		});
	}

	@Override
	public List<Integer> getGroupIdsByJobitemIds(List<Integer> jobitemIds) {
		// TODO Auto-generated method stub
		return getResultList(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<JobItem> root = cq.from(JobItem.class);
			Join<JobItem, JobItemGroup> jobitemGroup = root.join(JobItem_.group);
			cq.where(root.get(JobItem_.jobItemId).in(jobitemIds));
			cq.select(jobitemGroup.get(JobItemGroup_.id));
			cq.distinct(true);
			return cq;
		});
	}
}