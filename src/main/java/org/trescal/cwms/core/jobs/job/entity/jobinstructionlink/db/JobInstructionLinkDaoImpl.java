package org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.db;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.JobInstructionLink;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.JobInstructionLink_;

@Repository
public class JobInstructionLinkDaoImpl extends BaseDaoImpl<JobInstructionLink, Integer> implements JobInstructionLinkDao {

	@Override
	protected Class<JobInstructionLink> getEntity() {
		return JobInstructionLink.class;
	}
	
	public List<JobInstructionLink> getAllForJob(Job job) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<JobInstructionLink> cq = cb.createQuery(JobInstructionLink.class);

		Root<JobInstructionLink> root = cq.from(JobInstructionLink.class);

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(root.get(JobInstructionLink_.job), job));

		cq.where(clauses);
		cq.select(root);

		return getEntityManager().createQuery(cq).getResultList();
	}
}