package org.trescal.cwms.core.jobs.job.entity.job;

import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.SortNatural;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryComparator;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.instrument.entity.checkout.CheckOut;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReviewComparator;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.JobInstructionLink;
import org.trescal.cwms.core.jobs.job.entity.jobnote.JobNote;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLinkQuoteComparator;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.poorigin.OriginKind;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItemComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;
import org.trescal.cwms.core.logistics.entity.jobcourier.JobCourierLink;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLinkComparator;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCostingComparator;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLink;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.supportedcurrency.MultiCurrencySupport;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.timekeeping.entity.TimeSheetEntry;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name = "job")
@Setter
public class Job extends Allocated<Subdiv> implements NoteAwareEntity, ComponentEntity, MultiCurrencySupport, OriginKind {

    private Set<BatchCalibration> batchCals;
    private Address bookedInAddr;
    private Location bookedInLoc;
    private String clientRef;
    private Contact con;
    private Set<ContractReview> contractReviews;
    private Contact createdBy;
    // define multi-currency support here
    protected SupportedCurrency currency;
	private CalibrationType defaultCalType;
	private PO defaultPO;
	private Integer defaultTurn = 0;
	private Set<JobDelivery> deliveries;
	private File directory;
	private BigDecimal estCarriageOut;
	private List<JobItemGroup> groups;
	private TransportOption inOption;
	private Set<JobInstructionLink> insts;
	private Set<InvoiceJobLink> invoiceLinks;
	private Set<JobItem> items;
	private SortedSet<JobExpenseItem> expenseItems;
	private Set<JobCosting> jobCostings;
	private int jobid;
	private String jobno;
	private JobStatus js;
	private Set<JobQuoteLink> linkedQuotes;
	private Set<JobNote> notes;
	private List<PO> POs;
	protected BigDecimal rate;
	private List<InstructionLink<?>> relatedInstructionLinks;
	private TransportOption returnOption;
	private Address returnTo;
	private Location returnToLoc;
	private List<Email> sentEmails;
	private Set<TPQuoteRequest> tpQuoteRequests;
	private JobType type;
	private List<TimeSheetEntry> timeSheetEntries;
	private Set<JobBPOLink> bpoLinks;

	private ZonedDateTime pickupDate;
	private LocalDate regDate;
	private ZonedDateTime receiptDate;
	private LocalDate agreedDelDate;
	private LocalDate dateComplete;
	private Boolean overrideDateInOnJobItems;

	/**
	 * Data for courier
	 */
	private JobCourierLink jobCourier;
	private Integer numberOfPackages;
	private String packageType;
	private String storageArea;
	
	private List<CheckOut> checkOuts;
	

	public Job() {
		this.linkedQuotes = new TreeSet<>(new JobQuoteLinkQuoteComparator());
	}

	/**
	 * Append a set of items to the pre-existing set
	 */
	public void addJobItems(Set<JobItem> items) {
		this.items.addAll(items);
	}

	@Column(name = "agreeddeldate", columnDefinition = "date")
	@DateTimeFormat(iso = ISO.DATE)
	public LocalDate getAgreedDelDate() {
		return this.agreedDelDate;
	}

	@Transient
	@Override
	public BigDecimal getBaseFinalCost() {
		return null;
	}

	@Transient
	@Override
	public String getBaseFinalCostFormatted() {
		return null;
	}

	@OneToMany(mappedBy = "job", fetch = FetchType.LAZY)
	@OrderBy("beginTime")
	public Set<BatchCalibration> getBatchCals() {
		return this.batchCals;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bookedinaddrid")
	public Address getBookedInAddr() {
		return this.bookedInAddr;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bookedinlocid")
	public Location getBookedInLoc() {
		return this.bookedInLoc;
	}
	
	@Transient
	public BPO getBpo() {
		return this.bpoLinks != null ? this.bpoLinks.stream().filter(JobBPOLink::isDefaultForJob).map(JobBPOLink::getBpo).findFirst().orElse(null):null;
	}


	/**
	 * @return the clientRef
	 */
	@Length(max = 30)
	@Column(name = "clientref", length = 30)
	public String getClientRef() {
		return this.clientRef;
	}

	/**
	 * @return the con
	 */
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid")
	public Contact getCon() {
		return this.con;
	}

	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	@OneToMany(mappedBy = "job", fetch = FetchType.LAZY)
	@SortComparator(ContractReviewComparator.class)
	public Set<ContractReview> getContractReviews() {
		return this.contractReviews;
	}

	/**
	 * @return the createdBy
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "createdby")
	public Contact getCreatedBy() {
		return this.createdBy;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currencyid", nullable = false)
	public SupportedCurrency getCurrency() {
		return this.currency;
	}

	/**
	 * @return the dateComplete
	 */
	@Column(name = "datecomplete", columnDefinition = "date")
	public LocalDate getDateComplete() {
		return this.dateComplete;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "defaultcaltypeid", nullable = false)
	public CalibrationType getDefaultCalType() {
		return this.defaultCalType;
	}

	@Override
	@Transient
	public Contact getDefaultContact() {
		return this.con;
	}

	/**
	 * @return the defaultPO
	 */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "defaultpoid")
	public PO getDefaultPO() {
		return this.defaultPO;
	}

	@Column(name = "defaultturn")
	@Type(type = "int")
	public Integer getDefaultTurn() {
		return this.defaultTurn;
	}

	/**
	 * @return the deliveries
	 */
	@OneToMany(mappedBy = "job", fetch = FetchType.LAZY)
	@SortComparator(DeliveryComparator.class)
	public Set<JobDelivery> getDeliveries() {
		return this.deliveries;
	}

	@Transient
	public File getDirectory() {
		return this.directory;
	}

	@NotNull
	@Column(name = "estcarout", nullable = false, precision = 10, scale = 2)
	public BigDecimal getEstCarriageOut() {
		return this.estCarriageOut;
	}


	@OneToMany(mappedBy = "job", fetch = FetchType.LAZY)
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	public List<JobItemGroup> getGroups() {
		return this.groups;
	}

	@Override
	@Transient
	public String getIdentifier() {
		return this.jobno;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inoptionid")
	public TransportOption getInOption() {
		return this.inOption;
	}

	/**
	 * @return the insts
	 */
	@OneToMany(mappedBy = "job", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<JobInstructionLink> getInsts() {
		return this.insts;
	}

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = "job", orphanRemoval = true)
	@SortComparator(InvoiceJobLinkComparator.class)
	public Set<InvoiceJobLink> getInvoiceLinks() {
		return this.invoiceLinks;
	}

	/**
	 * @return the items
	 */
	@OneToMany(mappedBy = "job", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(JobItemComparator.class)
	public Set<JobItem> getItems() {
		return this.items;
	}

	@OneToMany(mappedBy = "job", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	@SortNatural
	public SortedSet<JobExpenseItem> getExpenseItems() {
		return expenseItems;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "job")
	@SortComparator(JobCostingComparator.class)
	public Set<JobCosting> getJobCostings() {
		return this.jobCostings;
	}

	/**
	 * @return the jobid
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "jobid", nullable = false, unique = true)
	@Type(type = "int")
	public int getJobid() {
		return this.jobid;
	}

	/**
	 * @return the jobno
	 */
	@NotNull
	@Length(min = 1, max = 30)
	@Column(name = "jobno", length = 30, unique = true)
	public String getJobno() {
		return this.jobno;
	}

	/**
	 * @return the js add notnull constraint to database (maybe with deprecated
	 *         field cleanup)
	 */
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid")
	public JobStatus getJs() {
		return this.js;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "job")
	@SortComparator(JobQuoteLinkQuoteComparator.class)
	public Set<JobQuoteLink> getLinkedQuotes() {
		return this.linkedQuotes;
	}

	/**
	 * @return the notes
	 */
	@OneToMany(mappedBy = "job", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(NoteComparator.class)
	public Set<JobNote> getNotes() {
		return this.notes;
	}

	@Transient
	public ComponentEntity getParentEntity() {
		return null;
	}

	/**
	 * @return the POs
	 */
	@OneToMany(mappedBy = "job", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public List<PO> getPOs() {
		return this.POs;
	}

	@Transient
	public Integer getPrivateActiveNoteCount() {
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount() {
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount() {
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount() {
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}

	@NotNull
	@Column(name = "rate", nullable = false, precision = 10, scale = 2)
	public BigDecimal getRate() {
		return this.rate;
	}

	/**
	 * @return the regDate
	 */
	@Column(name = "regdate", columnDefinition = "date")
	public LocalDate getRegDate() {
		return this.regDate;
	}

	/**
	 * @return the relatedInstructions
	 */
	@Transient
	public List<InstructionLink<?>> getRelatedInstructionLinks() {
		return this.relatedInstructionLinks;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "returnoptionid")
	public TransportOption getReturnOption() {
		return this.returnOption;
	}

	/**
	 * @return the returnTo
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "returnto")
	public Address getReturnTo() {
		return this.returnTo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "returntoloc")
	public Location getReturnToLoc() {
		return this.returnToLoc;
	}

	@Override
	@Transient
	public List<Email> getSentEmails() {
		return this.sentEmails;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "requestFromJob")
	@SortComparator(TPQuoteRequest.TPQuoteRequestComparator.class)
	public Set<TPQuoteRequest> getTpQuoteRequests() {
		return this.tpQuoteRequests;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "typeid", nullable = false)
	public JobType getType() {
		return this.type;
	}

	@Override
	@Transient
	public boolean isAccountsEmail() {
		return false;
	}


	/**
	 * Sets regDate to date of creation
	 */
	public void setRegDate() {
		this.regDate = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
	}

	@Override
	public String toString() {
		return "Job " + this.jobno;
	}

	@OneToMany(mappedBy = "job", fetch = FetchType.LAZY)
	public List<TimeSheetEntry> getTimeSheetEntries() {
		return timeSheetEntries;
	}

	public void setTimeSheetEntries(List<TimeSheetEntry> timeSheetEntries) {
		this.timeSheetEntries = timeSheetEntries;
	}

	@OneToOne(mappedBy = "job", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public JobCourierLink getJobCourier() {
		return jobCourier;
	}

	@Column(name = "receiptdate", columnDefinition = "datetime")
	public ZonedDateTime getReceiptDate() {
		return receiptDate;
	}

	@NotNull
	@Column(name = "overridedateinonjobitems", nullable = false)
	public Boolean getOverrideDateInOnJobItems() {
		return overrideDateInOnJobItems;
	}

	@Column(name = "pickupdate", columnDefinition = "datetime")
	public ZonedDateTime getPickupDate() {
		return pickupDate;
	}

	@Column(name = "numberofpackages")
	public Integer getNumberOfPackages() {
		return numberOfPackages;
	}

	@Column(name = "packagetype")
	public String getPackageType() {
		return packageType;
	}

	@Column(name = "storagearea")
	public String getStorageArea() {
		return storageArea;
	}
	
	/**
	 * @return the BPO links
	 */
	@OneToMany(mappedBy = "job", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public Set<JobBPOLink> getBpoLinks() {
		return this.bpoLinks;
	}

	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "job")
	public List<CheckOut> getCheckOuts() {
		return checkOuts;
	}
	
	
	@Transient
	public long getContractInstructionsSize() {
		if(this.items.isEmpty())
			return 0;
		else
			return this.items.stream().filter(ji -> ji.getContract() != null && ji.getContract().getInstructions() != null)
					.map(JobItem::getContract).map(Contract::getInstructions).distinct().count();
	}
	
	@Transient
	public List<Contract> getContracts() {
		return this.items.stream().map(JobItem::getContract).filter(Objects::nonNull).sorted(Comparator.comparing(Contract::getId)).distinct().collect(Collectors.toList());
	}
	
	@Transient
	public long getItemsCountByContract(Contract contract) {
		return this.items.stream().filter(ji -> ji.getContract() != null && ji.getContract().getId().equals(contract.getId())).count();
	}
	
	@Transient
	public List<JobItem> getContractsItems() {
		return this.items.stream().filter(ji -> ji.getContract() != null).collect(Collectors.toList());
	}
	
	@Transient
	public String getItemsId() {
		return this.items.stream().map(JobItem::getJobItemId).map(Object::toString)
                .collect(Collectors.joining(","));
	}
}