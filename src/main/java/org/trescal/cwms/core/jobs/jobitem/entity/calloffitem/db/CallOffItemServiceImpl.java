package org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.db;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db.WorkRequirementService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.CallOffItem;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.dto.CallOffItemDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItemComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.db.JobItemActivityService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.NotInvoicedReason;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db.JobItemNotInvoicedService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.form.JICallOffItemForm;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.Discount;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.rest.adveso.service.AdvesoEventService;

import java.time.LocalDate;
import java.util.*;

@Service("CallOffItemService")
public class CallOffItemServiceImpl extends BaseServiceImpl<CallOffItem, Integer> implements CallOffItemService {
	@Autowired
	private AdvesoEventService advesoEventService;
	@Autowired
	private CallOffItemDao callOffItemDao;
	@Autowired
	private Discount discount;
	@Autowired
	private ItemStateService itemStateServ;
	@Autowired
	private JobItemActivityService jiaServ;
	@Autowired
	private JobItemNotInvoicedService jiNotInvoicedService;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private WorkRequirementService workRequirementService;

	@Override
	protected BaseDao<CallOffItem, Integer> getBaseDao() {
		return callOffItemDao;
	}

	@Override
	public int addCallOffItemsToNewJob(List<Integer> callOffItemIds, Job job, Contact currentContact, int nextItemNo) {
		// look for discount defaults for this contact/sub/company or system
		// default
		Double calDiscount = discount.parseValue(
				discount.getValueHierarchical(Scope.CONTACT, job.getCon(), job.getOrganisation().getComp()).getValue());

		if (!callOffItemIds.isEmpty()) {
			CallOffItem coffi = this.get(callOffItemIds.stream().findFirst().get());
			Job oldJob = coffi.getOffItem().getJob();

			if(oldJob.getBpoLinks() != null) {
				if(job.getBpoLinks() == null)
					job.setBpoLinks(new HashSet<>());
				job.getBpoLinks().addAll(oldJob.getBpoLinks());
			}
			
//			job.getPOs().clear();
			job.getPOs().addAll(coffi.getOffItem().getJob().getPOs());
		}

		for (Integer id : callOffItemIds) {
			CallOffItem coi = this.get(id);
			JobItem ji = new JobItem();
			JobItem oldItem = this.jiServ.findJobItem(coi.getOffItem().getJobItemId());
			ji.setItemNo(nextItemNo);
			nextItemNo++;
			if (jiServ.getDateIn(job) != null)
				ji.setDateIn(jiServ.getDateIn(job));
			else
				ji.setDateIn(oldItem.getDateIn());
			ji.setDueDate(oldItem.getDueDate());
			ji.setAgreedDelDate(oldItem.getAgreedDelDate());
			ji.setTurn(oldItem.getTurn());
			ji.setReqCleaning(oldItem.getReqCleaning());
			ji.setServiceType(oldItem.getServiceType());
			ji.setInst(oldItem.getInst());
			ji.setJob(job);

			if (oldItem.getClientRef() != null)
				ji.setClientRef(oldItem.getClientRef());

			// 2018-01-22 GB : Previously used job defaults, changed to
			// retain
			// addr/loc from job item
			ji.setCurrentAddr(oldItem.getBookedInAddr());
			ji.setCurrentLoc(oldItem.getBookedInLoc());
			ji.setBookedInAddr(oldItem.getBookedInAddr());
			ji.setBookedInLoc(oldItem.getBookedInLoc());
			ji.setAccessories(new HashSet<>());
			if (!StringUtils.isEmpty(oldItem.getAccessoryFreeText())) {
				ji.setAccessoryFreeText(oldItem.getAccessoryFreeText());
			}
			// if the job has a default PO or BPO, add the item to it
			if (ji.getItemPOs() == null) {
				ji.setItemPOs(new HashSet<>());
			}
			if (job.getBpo() != null) {
				JobItemPO jipo = new JobItemPO();
				jipo.setItem(ji);
				jipo.setBpo(job.getBpo());
				ji.getItemPOs().add(jipo);
			}else if (job.getDefaultPO() != null) {
				JobItemPO jipo = new JobItemPO();
				jipo.setItem(ji);
				jipo.setPo(job.getDefaultPO());
				ji.getItemPOs().add(jipo);
			}
			
			if(!oldItem.getItemPOs().isEmpty()) {
				ji.getItemPOs().addAll(oldItem.getItemPOs());
			}

			// 2019-05-23 added work requirement copy when possible; sets proc,
			// wi, cal address too
			if (this.workRequirementService.canCopyWorkRequirements(oldItem)) {
				this.workRequirementService.copyWorkRequirements(oldItem, ji);
			}
			if (oldItem.getOnBehalf() != null) {
				OnBehalfItem oldOnBehalf = oldItem.getOnBehalf();
				OnBehalfItem newOnBehalf = new OnBehalfItem();
				newOnBehalf.setAddress(oldOnBehalf.getAddress());
				newOnBehalf.setCompany(oldOnBehalf.getCompany());
				newOnBehalf.setJobitem(ji);
				newOnBehalf.setSetBy(oldOnBehalf.getSetBy());
				newOnBehalf.setSetOn(oldOnBehalf.getSetOn());

				ji.setOnBehalf(newOnBehalf);
			}
			if (oldItem.getAccessories() != null)
				for (ItemAccessory oldAccessory : oldItem.getAccessories()) {
					ItemAccessory newAccessory = ItemAccessory.builder()
							.accessory(oldAccessory.getAccessory())
							.quantity(oldAccessory.getQuantity())
							.ji(ji).build();
					ji.getAccessories().add(newAccessory);
				}
			// this will be overridden by the status set in any active hooks
			ji.setState(null);
			this.jiServ.insertJobItem(ji, currentContact, calDiscount, null);
			coi.setActive(false);
			coi.setOnDate(new Date());
			coi.setOnItem(ji);
			// create activity for the old item
			JobItemActivity jia = new JobItemActivity();
			jia.setCreatedOn(new Date());
			jia.setActivity(this.itemStateServ.findItemActivityByName("Assigned to new Job"));
			jia.setActivityDesc("Assigned to new Job");
			jia.setEndStamp(new Date());
			jia.setRemark("Moved to Job <a href='viewjob.htm?jobid=" + job.getJobid() + "'>" + job.getJobno()
					+ "</a> as item <a href='jiactions.htm?jobitemid=" + ji.getJobItemId() + "'>" + ji.getItemNo()
					+ "</a>.");
			jia.setStartStamp(new Date());
			jia.setTimeSpent(0);
			jia.setComplete(true);
			jia.setCompletedBy(currentContact);
			jia.setEndStatus(this.itemStateServ.findItemStatusByName("No longer active on this job"));
			jia.setJobItem(oldItem);
			jia.setStartedBy(currentContact);
			jia.setStartStatus((ItemStatus) oldItem.getState());
			this.jiaServ.save(jia);
			if (ji.getActions().size() == 1) {
				// add remark to first activity of the new item
				ji.getActions().forEach(action -> action.setRemark("Created from Item <a href='jiactions.htm?jobitemid=" + oldItem.getJobItemId()
					+ "'>" + oldItem.getItemNo() + "</a> on Job <a href='viewjob.htm?jobid="
					+ oldItem.getJob().getJobid() + "'>" + oldItem.getJob().getJobno() + "</a>."));
			}
			// set status for old item
			oldItem.setState(this.itemStateServ.findItemStatusByName("No longer active on this job"));
			// add to set of items on job
			if (job.getItems() == null)
				job.setItems(new TreeSet<>(new JobItemComparator()));
			job.getItems().add(ji);
		}
		return nextItemNo;
	}

	private void createCallOffItem(Integer jobitemId, String reason, Contact currentContact) {
		if (jobitemId != null) {
			JobItem jobItem = this.jiServ.findJobItem(jobitemId);
			// get current item status (persisted in activity)
			ItemStatus currentStatus = (ItemStatus) jobItem.getState();
			jobItem.setState(this.itemStateServ.findItemStatusByName("Called Off"));
			if (jobItem.getNotInvoiced() == null) {
				JobItemNotInvoiced jiNotInvoiced = new JobItemNotInvoiced();
				jiNotInvoiced.setComments(reason);
				jiNotInvoiced.setContact(currentContact);
				jiNotInvoiced.setDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
				jiNotInvoiced.setJobItem(jobItem);
				jiNotInvoiced.setReason(NotInvoicedReason.CALLED_OFF);
				this.jiNotInvoicedService.save(jiNotInvoiced);
			}

			// publish jobitem calloff item event
			advesoEventService.publishJobItemCallOffItemEvent(jobItem);
			// create as active call off item
			CallOffItem coi = new CallOffItem();
			coi.setOrganisation(jobItem.getJob().getOrganisation());
			coi.setActive(true);
			coi.setCalledOffBy(currentContact);
			coi.setOffDate(new Date());
			coi.setOffItem(jobItem);
			coi.setReason(reason);
			this.save(coi);
			// add activity to item as log
			JobItemActivity jia = new JobItemActivity();
			jia.setCreatedOn(new Date());
			jia.setActivity(this.itemStateServ.findItemActivityByName("Item called off"));
			jia.setActivityDesc("Item called off");
			jia.setEndStamp(new Date());
			jia.setRemark("Check <a href='calloffitems.htm' class='mainlink'>Called-Off item list</a>");
			jia.setStartStamp(new Date());
			jia.setTimeSpent(0);
			jia.setComplete(true);
			jia.setCompletedBy(currentContact);
			jia.setEndStatus((ItemStatus) jobItem.getState());
			jia.setJobItem(jobItem);
			jia.setStartedBy(currentContact);
			jia.setStartStatus(currentStatus);
			this.jiaServ.save(jia);
		}
	}

	public void createCallOffItems(JICallOffItemForm form, Contact currentContact) {
		Collection<Integer> keys = form.getJobItemIds().keySet();
		Map<Integer, Integer> itemIds = form.getJobItemIds();
		Map<Integer, String> reasons = form.getReasons();
		for (Integer key : keys) {
			Integer jobitemId = itemIds.get(key);
			String reason = reasons.get(key);
			if (jobitemId != null) {
				createCallOffItem(jobitemId, reason, currentContact);
			}
		}
	}

	@Override
	public List<CallOffItem> getAllActiveCallOffItems() {
		return this.callOffItemDao.getAllActive();
	}

    @Override
    public Set<CallOffItemDTO> getAllActiveCallOffItems(Locale locale) {
        return this.callOffItemDao.getAllActiveCallOffItems(locale);
    }

    @Override
    public CallOffItem getActiveForJobItem(int jobItemId) {
        return this.callOffItemDao.getActiveForJobItem(jobItemId);
    }

    @Override
    public List<CallOffItemDTO> getCallOffItemsDtoForCompany(int coid, Boolean active) {
        return this.callOffItemDao.getAllForCompanyDto(coid, active);

    }

    @Override
    public List<CallOffItem> getCallOffItemsForCompany(int coid, Boolean active) {
        return this.callOffItemDao.getAllForCompany(coid, active);
    }

    @Override
    public List<CallOffItem> getItemsFromIds(List<Integer> ids) {
        return this.callOffItemDao.getAll(ids);
    }

	@Override
	public void reactivateCallOffItem(Integer calloffItemId, Contact contact) {
		CallOffItem calloffitem = this.get(calloffItemId);
		if (calloffitem.isActive()) {
			JobItem ji = calloffitem.getOffItem();
			// get current status (should be 'called-off')
			ItemStatus currentStatus = (ItemStatus) ji.getState();
			// set status of Job Item (stream naturally sorted by ID)
			ji.getActions().stream()
				.filter(action -> !action.isDeleted() && action.getEndStatus().equals(currentStatus)).findFirst().map(JobItemAction::getStartStatus).ifPresent(ji::setState);
			// If not-invoiced reason was for being called off, we can remove
			// the reason
			if (ji.getNotInvoiced() != null && ji.getNotInvoiced().getReason().equals(NotInvoicedReason.CALLED_OFF)) {
				JobItemNotInvoiced jiNotInvoiced = ji.getNotInvoiced();
				ji.setNotInvoiced(null);
				this.jiNotInvoicedService.delete(jiNotInvoiced);
			}

			// De-activate call off item instance
			calloffitem.setOnDate(new Date());
			calloffitem.setOnItem(ji);
			calloffitem.setActive(false);
			// Add activity to Job Item as log
			JobItemActivity jia = new JobItemActivity();
			jia.setCreatedOn(new Date());
			jia.setActivity(this.itemStateServ.findItemActivityByName("Re-activated on job after call off"));
			jia.setActivityDesc(jia.getActivity().getDescription());
			jia.setEndStamp(new Date());
			jia.setRemark("");
			jia.setStartStamp(new Date());
			jia.setTimeSpent(0);
			jia.setComplete(true);
			jia.setCompletedBy(contact);
			jia.setEndStatus((ItemStatus) ji.getState());
			jia.setJobItem(ji);
			jia.setStartedBy(contact);
			jia.setStartStatus(currentStatus);
			this.jiaServ.save(jia);
		}
	}

	@Override
	public void reactivateCallOffItems(List<Integer> callOffItems, Contact contact) {
		for (Integer id : callOffItems) {
			this.reactivateCallOffItem(id, contact);
		}
	}
}