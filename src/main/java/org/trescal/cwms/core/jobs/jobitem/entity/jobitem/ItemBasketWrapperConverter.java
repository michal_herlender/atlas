package org.trescal.cwms.core.jobs.jobitem.entity.jobitem;

import com.fasterxml.jackson.databind.util.StdConverter;
import lombok.val;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.tools.InstModelTools;

import java.util.Collection;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Stream;

class ItemBasketWrapperConverter extends StdConverter<ItemBasketWrapper, BasketItemDto> {
    @Override
    public BasketItemDto convert(ItemBasketWrapper ibw) {

        val inst = Optional.ofNullable(ibw.getInst());
        val model = Optional.ofNullable(ibw.getModel());
        val wi = inst
            .map(Instrument::getWorkInstructions).map(Collection::stream)
            .flatMap(Stream::findFirst);
        return BasketItemDto.builder()
            .instId(inst.map(Instrument::getPlantid).orElse(null))
            .modelId(model.map(InstrumentModel::getModelid).orElseGet(() -> inst.map(Instrument::getModel).map(InstrumentModel::getModelid).orElse(null)))
            .part(ibw.getPart())
            .existingGroupId(ibw.getExistingGroupId())
            .temporalGroupId(ibw.getTempGroupId())
            .baseItemId(ibw.getBaseItemId())
            .quotationItem(ibw.getQuotationItemDto())
            .modelName(model.map(m -> InstModelTools.instrumentModelNameWithTypology(m, LocaleContextHolder.getLocale())).orElseGet(() -> inst.map(i -> InstModelTools.instrumentModelNameViaTranslations(i, false, LocaleContextHolder.getLocale(), Locale.getDefault())).orElse(null)
            ))
            .workInstructionTitle(wi.map(WorkInstruction::getTitle).orElse(null))
            .workInstructionId(wi.map(WorkInstruction::getId).orElse(null))
            .serialNo(inst.map(Instrument::getSerialno).orElse(null))
            .plantNo(inst.map(Instrument::getPlantno).orElse(null))
            .calFrequency(inst.map(Instrument::getCalFrequency).orElse(null))
            .calFrequencyUnit(inst.map(Instrument::getCalFrequencyUnit).orElse(null))
            .build();
    }
}
