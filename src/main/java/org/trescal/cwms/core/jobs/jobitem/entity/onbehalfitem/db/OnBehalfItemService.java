package org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.form.OnBehalfForm;

public interface OnBehalfItemService extends BaseService<OnBehalfItem, Integer> {
	
	public OnBehalfItem getOnBehalfItemByJobitem(JobItem ji);
	public void updateOnBehalfItem(OnBehalfForm form,String username);
	
	
}