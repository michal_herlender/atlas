package org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemandaction.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemandaction.JobItemDemandAction;

public interface JobItemDemandActionService extends BaseService<JobItemDemandAction, Integer> {
}