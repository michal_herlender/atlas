package org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction;

import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "jobitemaction")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@Setter
public abstract class JobItemAction extends Auditable {

    private ItemActivity activity;
    private String activityDesc;
    private boolean deleted;
    private Contact deletedBy;
    private Date deleteStamp;
    private Date endStamp;
    private ItemStatus endStatus;
    private Integer id;
    private JobItem jobItem;
    private String remark;
    private Contact startedBy;
    private Date startStamp;
    private ItemStatus startStatus;
    private Integer timeSpent;
    private Date createdOn;
    private List<AdvesoJobItemActivity> advesoJobItemActivities;

    /**
     * @return the activity
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "activityid")
    public ItemActivity getActivity() {
        return this.activity;
    }

    /**
     * @return the activityDesc
     */
    @Column(name = "activitydesc", length = 1000)
    @Length(max = 1000)
    public String getActivityDesc() {
        return this.activityDesc;
    }

    /**
     * @return the deletedBy
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "deletedbyid")
    public Contact getDeletedBy() {
        return this.deletedBy;
    }

    /**
     * @return the deleteStamp
     */
    @Column(name = "deletestamp", columnDefinition = "datetime")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getDeleteStamp() {
        return this.deleteStamp;
    }

    /**
     * @return the endStamp
     */
    @Column(name = "endstamp", columnDefinition = "datetime")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndStamp() {
        return this.endStamp;
    }

    /**
     * @return the endStatus
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "endstatusid")
    public ItemStatus getEndStatus() {
        return this.endStatus;
    }

    /**
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @Type(type = "int")
    public Integer getId() {
        return this.id;
    }

    /**
     * @return the jobItem
     */
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "jobitemid")
    public JobItem getJobItem() {
        return this.jobItem;
    }

    /**
     * @return the remark
     */
    @Column(name = "remark", length = 1000)
    @Length(max = 1000)
    public String getRemark() {
        return this.remark;
    }

    /**
     * @return the startedBy
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "startedbyid")
    public Contact getStartedBy() {
        return this.startedBy;
    }

    /**
     * @return the startStamp
     */
    @Column(name = "startstamp", columnDefinition = "datetime")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getStartStamp() {
        return this.startStamp;
    }

    /**
     * @return the startStatus
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "startstatusid")
    public ItemStatus getStartStatus() {
        return this.startStatus;
    }

    /**
     * @return the timeSpent
     */
    @Column(name = "timespent")
    @Type(type = "int")
    public Integer getTimeSpent() {
        return this.timeSpent;
    }

    /**
     * @return the deleted
     */
    @Column(name = "deleted", columnDefinition = "tinyint")
    public boolean isDeleted() {
        return this.deleted;
    }

    @Column(name = "createdon")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getCreatedOn() {
        return createdOn;
    }

    @OneToMany(mappedBy = "jobItemAction", fetch = FetchType.LAZY)
    public List<AdvesoJobItemActivity> getAdvesoJobItemActivities() {
        return this.advesoJobItemActivities;
    }

    @Transient
    public abstract boolean isTransit();
}