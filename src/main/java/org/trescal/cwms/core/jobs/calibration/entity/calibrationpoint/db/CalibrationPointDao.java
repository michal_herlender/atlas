package org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.db;

import java.util.Collection;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.CalibrationPoint;

public interface CalibrationPointDao extends BaseDao<CalibrationPoint, Integer> {
	
	void deleteAll(Collection<CalibrationPoint> points);
}