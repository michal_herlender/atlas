package org.trescal.cwms.core.jobs.jobitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db.ServiceCapabilityService;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db.WorkRequirementService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.form.WorkRequirementCatalogForm;
import org.trescal.cwms.core.jobs.jobitem.form.WorkRequirementCatalogValidator;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.utils.ProcedureMatch;
import org.trescal.cwms.core.procedure.utils.ProcedureMatchResults;
import org.trescal.cwms.core.procedure.utils.ProcedureMatchType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Controller that performs lookup / research of capabilities for work
 * requirements mainly based on instrument properties and subfamily, using
 * Procedure / ServiceCapability records as reference "catalogs" to recommend
 * where to service an instrument
 *
 * Supplements WorkRequirementController for now; the functionality may be
 * merged later
 *
 * @author Galen
 *
 */
@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class WorkRequirementCatalogController {
    @Autowired
    private JobItemService jobItemService;
    @Autowired
    private CapabilityService capabilityService;
    @Autowired
    private ServiceCapabilityService serviceCapabilityService;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private WorkRequirementService wrService;
    @Autowired
    private WorkRequirementCatalogValidator validator;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("form")
	public WorkRequirementCatalogForm formBackingObject(Locale locale,
														@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
														@RequestParam(name = "jobItemId") Integer jobItemId,
														@RequestParam(name = "serviceTypeId", required = false) Integer serviceTypeId) {
		JobItem jobItem = this.jobItemService.findJobItem(jobItemId);
		Subdiv subdiv = this.subdivService.get(subdivDto.getKey());

		WorkRequirementCatalogForm form = new WorkRequirementCatalogForm();
		form.setAction(WorkRequirementCatalogForm.ACTION_SEARCH);
		form.setAddCount(1);
		form.setCoid(subdiv.getComp().getCoid());

		form.setIncludeAllResults(false);
		form.setIncludeInstrumentModel(false);
		form.setIncludeGenericCapabilities(false);
		form.setJobItemId(jobItemId);
		form.setSelectSubdivision(false);
		if (serviceTypeId != null)
			form.setServiceTypeId(serviceTypeId);
		else
			form.setServiceTypeId(jobItem.getCalType().getServiceType().getServiceTypeId());
		form.setSubdivid(subdiv.getSubdivid());
		form.setSubfamilyId(jobItem.getInst().getModel().getDescription().getId());
		form.setSubfamilyName(this.translationService
				.getCorrectTranslation(jobItem.getInst().getModel().getDescription().getTranslations(), locale));
		return form;
	}

	@RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "workrequirementlookup.htm")
	public String handleRequest(Model model,
								@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
								@Validated @ModelAttribute("form") WorkRequirementCatalogForm form, BindingResult bindingResult,
								@RequestParam(name = "jobItemId") Integer jobItemId) {
		if (form.getAction().equals(WorkRequirementCatalogForm.ACTION_CREATE) && !bindingResult.hasErrors()) {
			performCreate(model, form);
			return "trescal/core/jobs/jobitem/workrequirementsuccess";
		} else {
			performSearch(model, form, subdivDto.getKey());
			return "trescal/core/jobs/jobitem/workrequirementcatalog";
		}
	}

	private void performCreate(Model model, WorkRequirementCatalogForm form) {
		WorkRequirement wr = null;
		for (int i = 0; i < form.getAddCount(); i++) {
			wr = this.wrService.createWorkRequirement(form.getJobItemId(), form.getProcId(), 0, form.getServiceTypeId(),
					form.getRequirementText());
		}
		model.addAttribute("wr", wr);
	}

	private void performSearch(Model model, WorkRequirementCatalogForm form, Integer allocatedSubdivId) {
		Locale locale = LocaleContextHolder.getLocale();
		JobItem jobItem = this.jobItemService.findJobItem(form.getJobItemId());
		// Note, selected subdivision may be business or supplier, used for search
        Subdiv selectedSubdiv = this.subdivService.get(form.getSubdivid());
        // Allocated subdiv is "home subdivision" for search (e.g. determining country)
        Subdiv businessSubdiv = this.subdivService.get(allocatedSubdivId);

        model.addAttribute("selectedSubdiv", selectedSubdiv);
        model.addAttribute("instrument", jobItem.getInst());
        model.addAttribute("subFamily", jobItem.getInst().getModel().getDescription());

        ServiceType serviceType = this.serviceTypeService.get(form.getServiceTypeId());

        Map<ProcedureMatchType, Set<ProcedureMatch>> resultMap;
        List<Capability> procResults;
        ProcedureMatchResults matchResults;
        if (form.getIncludeGenericCapabilities()) {
            procResults = this.capabilityService.searchCapabilityBySubFamilyIdAndSubdivId(null, true,
                form.getSubdivid());
            matchResults = new ProcedureMatchResults(businessSubdiv, serviceType);
            procResults.forEach(matchResults::addMatch);
        } else {
            procResults = this.capabilityService.searchCapabilityBySubFamilyIdAndSubdivId(form.getSubfamilyId(),
                false, null);
            matchResults = new ProcedureMatchResults(businessSubdiv, serviceType,
                form.getIncludeAllResults());
            procResults.forEach(matchResults::addMatch);

            if (form.getIncludeInstrumentModel()) {
                Integer modelId = jobItem.getInst().getModel().getModelid();
                List<ServiceCapability> scResults = this.serviceCapabilityService.find(modelId, null, null, null);
                // TODO need to exclude duplicate matches already in filtered matches
                scResults.forEach(matchResults::addMatch);
            }
		}
		resultMap = matchResults.getResultMap();

		model.addAttribute("resultMap", resultMap);
		model.addAttribute("matchTypes", ProcedureMatchType.values());
		model.addAttribute("serviceTypes",
			serviceTypeService.getDTOList(DomainType.INSTRUMENTMODEL, jobItem.getJob().getType(), locale, false));
		model.addAttribute("serviceType", serviceType);

	}
}