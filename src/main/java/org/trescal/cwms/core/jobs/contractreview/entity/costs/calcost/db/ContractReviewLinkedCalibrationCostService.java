package org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewLinkedCalibrationCost;

public interface ContractReviewLinkedCalibrationCostService extends BaseService<ContractReviewLinkedCalibrationCost, Integer> {}