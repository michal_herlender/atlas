package org.trescal.cwms.core.jobs.jobitem.form;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JIContractReviewForm extends JobItemForm {
	private Integer actionOutcomeId;
	@Valid
	private ContractReviewAdjustmentCost adjustmentCost;
	private boolean bulkUpdate;
	@Valid
	private ContractReviewCalibrationCost calibrationCost;
	private Integer calType;
	private Set<CalibrationType> calTypes;
	private Date certDate;
	private boolean copyBypassCosting;
	private boolean copyCalType;
	private boolean copyContract;
	private boolean copyCleaning;
	private boolean copyCosts;
	private boolean copyOnBehalf;
	private boolean copyOutcome;
	private boolean copyRequirements;
	private boolean copyReserveCertificate;
	private boolean copyTransportIn;
	private boolean copyTransportOut;
	private boolean copyTurnaround;
    private boolean copyWorkRequirements;
    private Integer inMethodId;
    private List<Integer> jobItemIds;
    @Valid
    private ContractReviewPurchaseCost purchaseCost;
    @Valid
    private ContractReviewRepairCost repairCost;
    private boolean reserveCertificate;
    @Min(value = 1, message = ERROR_CODE_TRANSPORT_OUT)
    private Integer returnMethodId;
    @Size(max = 500)
    private String reviewComments;
    private boolean saveComments;
    private String submit;
    private Integer contractId;
    private String clientReference;
    private boolean copyClientReference;
    private boolean[] demands;
    private LocalDate agreedDelDate;
    private Integer turnaround;
    private String dueDateComment;
	private String agreedDelDateComment;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private LocalDateTime clientReceiptDate;
	private BigDecimal calTotalCost;

	private Date currentDate = new Date();
	public static final String ERROR_CODE_TRANSPORT_OUT = "{error.contractreview.transportout}";

	public Integer getActionOutcomeId() {
		return this.actionOutcomeId;
	}

}