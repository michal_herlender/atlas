package org.trescal.cwms.core.jobs.contractreview.controller;

import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.db.ContractReviewCalibrationCostService;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.dto.CostJobItemDto;

@RestController
@RequestMapping("contractreview")
public class ContractreviewRestController {

    @Autowired private ContractReviewCalibrationCostService contractReviewCalibrationCostService;

    @PutMapping("resetLinkedCost.json")
    Either<String, CostSource> resetLinkedCost(@RequestParam Integer costId){
        return contractReviewCalibrationCostService.resetLinkedCostByCostId(costId);
    }

    @PutMapping("updateCostSource.json")
    Either<String, CostJobItemDto> updateCostSource(@RequestParam Integer jobItemId, @RequestParam Integer costId, @ RequestParam CostSource costSource){
        return contractReviewCalibrationCostService.updateCostSourceE(jobItemId,costId,costSource);
    }
}
