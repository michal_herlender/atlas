package org.trescal.cwms.core.jobs.job.entity.job.db;

import lombok.val;
import org.apache.commons.lang3.tuple.Triple;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToSubdivDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption_;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrDao;
import org.trescal.cwms.core.jobs.calibration.controller.ActiveInstrumentsJsonController.ActiveInstrumentDto;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost_;
import org.trescal.cwms.core.jobs.job.dto.JobKeyValue;
import org.trescal.cwms.core.jobs.job.dto.JobsForAddressWrapper;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink_;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus_;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.po.PO_;
import org.trescal.cwms.core.jobs.job.form.JobSearchForm;
import org.trescal.cwms.core.jobs.job.form.OldJobSearchForm;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem_;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.Capability_;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.StringJpaUtils;
import org.trescal.cwms.core.vdi.projection.OrderProjection;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.web.home.dto.JobStats;
import org.trescal.cwms.web.jobs.jobs.form.OldWebJobSearchForm;
import org.trescal.cwms.web.jobs.jobs.form.WebJobSearchForm;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Repository("JobDao")
public class JobDaoImpl extends AllocatedToSubdivDaoImpl<Job, Integer> implements JobDao {

	@Autowired
	private MfrDao mfrDao;

	@Override
	protected Class<Job> getEntity() {
		return Job.class;
	}


	@Override
	public Long countActiveByContactAndAllocatedSubdiv(Contact contact, Subdiv allocatedSubdiv) {
		return getSingleResult(cb -> {
			val cq = cb.createQuery(Long.class);
			val job = cq.from(Job.class);
			cq.select(cb.count(job));
			cq.where(cb.and(
					cb.equal(job.get(Job_.organisation), allocatedSubdiv),
					cb.equal(job.get(Job_.con), contact),
					cb.isFalse(job.join(Job_.js).get(JobStatus_.complete))
			));
			return cq;
		});
	}

	@Override
	public Long countByContact(Contact contact) {
	    return getSingleResult(cb -> {
	    	val cq = cb.createQuery(Long.class);
	    	val job = cq.from(Job.class);
	    	cq.select(cb.count(job));
	    	cq.where(cb.equal(job.get(Job_.con),contact));
	    	return cq;
		});
	}

	@Override
	public Long countByContactAndAllocatedSubdiv(Contact contact, Subdiv allocatedSubdiv) {
		return getSingleResult(cb -> {
			val cq = cb.createQuery(Long.class);
			val job = cq.from(Job.class);
			cq.select(cb.count(job));
			cq.where(cb.and(
					cb.equal(job.get(Job_.organisation),allocatedSubdiv),
					cb.equal(job.get(Job_.con),contact)
			));
			return cq;
		});
	}

	@Override
	public Long countActiveBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv) {

	    return getSingleResult(cb -> {
		val cq = cb.createQuery(Long.class);
		val job = cq.from(Job.class);
		cq.select(cb.count(job));
		cq.where(cb.and(
				cb.equal(job.get(Job_.organisation),allocatedSubdiv),
				cb.equal(job.join(Job_.con).get(Contact_.sub),subdiv),
				cb.isFalse(job.join(Job_.js).get(JobStatus_.complete))
		));
		return cq;
	});
	}

	@Override
	public Long countBySubdiv(Subdiv subdiv) {
		return getSingleResult(cb -> {
			val cq = cb.createQuery(Long.class);
			val job = cq.from(Job.class);
			cq.select(cb.count(job));
			cq.where(
					cb.equal(job.join(Job_.con).get(Contact_.sub),subdiv)
			);
			return cq;
		});
	}

	@Override
	public Long countBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv) {
		return getSingleResult(cb -> {
			val cq = cb.createQuery(Long.class);
			val job = cq.from(Job.class);
			cq.select(cb.count(job));
			cq.where(cb.and(
					cb.equal(job.get(Job_.organisation),allocatedSubdiv),
					cb.equal(job.join(Job_.con).get(Contact_.sub),subdiv)
			));
			return cq;
		});
	}

	@Override
	public Long countActiveByCompanyAndAllocatedSubdiv(Company company, Subdiv allocatedSubdiv) {
		return getSingleResult(cb -> {
			val cq = cb.createQuery(Long.class);
			val job = cq.from(Job.class);
			cq.select(cb.count(job));
			cq.where(cb.and(
					cb.equal(job.get(Job_.organisation),allocatedSubdiv),
					cb.equal(job.join(Job_.con).join(Contact_.sub).get(Subdiv_.comp),company),
					cb.isFalse(job.join(Job_.js).get(JobStatus_.complete))
			));
			return cq;
		});
	}

	@Override
	public Long countByCompany(Company company) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<Job> job = cq.from(Job.class);
			Join<Job, Contact> contact = job.join(Job_.con);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			cq.where(cb.equal(subdiv.get(Subdiv_.comp), company));
			cq.distinct(true);
			cq.select(cb.count(job));
			return cq;
		});
	}

	@Override
	public Long countByCompanyAndAllocatedSubdiv(Company company, Subdiv allocatedSubdiv) {
		return getSingleResult(cb -> {
			val cq = cb.createQuery(Long.class);
			val job = cq.from(Job.class);
			cq.select(cb.count(job));
			cq.where(cb.and(
					cb.equal(job.get(Job_.organisation),allocatedSubdiv),
					cb.equal(job.join(Job_.con).join(Contact_.sub).get(Subdiv_.comp),company)
			));
			return cq;
		});
	}

	@Override
	public List<Job> findAllJobsReadyToInvoice(int coid) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Job.class);
			val job = cq.from(Job.class);
			val con = job.fetch(Job_.con);
			con.fetch(Contact_.sub);
			cq.where(cb.and(
					cb.equal(job.get(Job_.con).get(Contact_.sub).get(Subdiv_.comp),coid),
					cb.isTrue(job.join(Job_.js).get(JobStatus_.readyToInvoice))
					));
			con.fetch(Contact_.user, javax.persistence.criteria.JoinType.LEFT);
			con.fetch(Contact_.userPreferences, javax.persistence.criteria.JoinType.LEFT);
			return cq;
		});
	}

	@Override
	public Job findByJobNo(String jobno) {
		return getFirstResult(cb -> {
			CriteriaQuery<Job> cq = cb.createQuery(Job.class);
			Root<Job> root = cq.from(Job.class);
			cq.where(cb.equal(root.get(Job_.jobno), jobno));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<JobKeyValue> findByPartialJobNo(String partialJobNo) {
		return getResultList(cb -> {
			CriteriaQuery<JobKeyValue> cq = cb.createQuery(JobKeyValue.class);
			Root<Job> root = cq.from(Job.class);
			cq.where(cb.like(cb.lower(root.get(Job_.jobno)), "%" + partialJobNo.toLowerCase() + "%"));
			cq.orderBy(cb.desc(root.get(Job_.regDate)));
			cq.select(cb.construct(JobKeyValue.class, root.get(Job_.jobid), root.get(Job_.jobno)));
			return cq;
		}, 0, Constants.RESULTS_PER_PAGE);
	}

	@Override
	public Job findEagerJobForView(int jobid) {
		return getFirstResult(cb -> {
			CriteriaQuery<Job> cq = cb.createQuery(Job.class);
			Root<Job> root = cq.from(Job.class);

			Fetch<Job, JobItem> jobItems = root.fetch(Job_.items, javax.persistence.criteria.JoinType.LEFT);
			Fetch<JobItem, JobCostingItem> jobCostingItem = jobItems.fetch(JobItem_.jobCostingItems,
					javax.persistence.criteria.JoinType.LEFT);

			// TODO replace with job costing summary query
			Fetch<JobCostingItem, JobCostingAdjustmentCost> jcAdjustmentCost = jobCostingItem
					.fetch(JobCostingItem_.adjustmentCost, javax.persistence.criteria.JoinType.LEFT);
			jcAdjustmentCost.fetch(JobCostingAdjustmentCost_.linkedCost, javax.persistence.criteria.JoinType.LEFT);
			Fetch<JobCostingItem, JobCostingCalibrationCost> jcCalibrationCost = jobCostingItem
					.fetch(JobCostingItem_.calibrationCost, javax.persistence.criteria.JoinType.LEFT);
			jcCalibrationCost.fetch(JobCostingCalibrationCost_.linkedCost, javax.persistence.criteria.JoinType.LEFT);
			Fetch<JobCostingItem, JobCostingPurchaseCost> jcPurchaseCost = jobCostingItem
					.fetch(JobCostingItem_.purchaseCost, javax.persistence.criteria.JoinType.LEFT);
			jcPurchaseCost.fetch(JobCostingPurchaseCost_.linkedCost, javax.persistence.criteria.JoinType.LEFT);
			Fetch<JobCostingItem, JobCostingRepairCost> jcRepairCost = jobCostingItem.fetch(JobCostingItem_.repairCost,
					javax.persistence.criteria.JoinType.LEFT);
			jcRepairCost.fetch(JobCostingRepairCost_.linkedCost, javax.persistence.criteria.JoinType.LEFT);

			jobItems.fetch(JobItem_.itemPOs, javax.persistence.criteria.JoinType.LEFT);
			jobItems.fetch(JobItem_.images, javax.persistence.criteria.JoinType.LEFT);
			jobItems.fetch(JobItem_.invoiceItems, javax.persistence.criteria.JoinType.LEFT);
			jobItems.fetch(JobItem_.notInvoiced, javax.persistence.criteria.JoinType.LEFT);
			jobItems.fetch(JobItem_.onBehalf, javax.persistence.criteria.JoinType.LEFT);
			jobItems.fetch(JobItem_.state, javax.persistence.criteria.JoinType.LEFT);

			Fetch<JobItem, ContractReviewAdjustmentCost> crAdjustmentCost = jobItems.fetch(JobItem_.adjustmentCost,
					javax.persistence.criteria.JoinType.LEFT);
			crAdjustmentCost.fetch(ContractReviewAdjustmentCost_.linkedCost, javax.persistence.criteria.JoinType.LEFT);
			Fetch<JobItem, ContractReviewCalibrationCost> crCalibrationCost = jobItems.fetch(JobItem_.calibrationCost,
					javax.persistence.criteria.JoinType.LEFT);
			crCalibrationCost.fetch(ContractReviewCalibrationCost_.linkedCost,
					javax.persistence.criteria.JoinType.LEFT);
			Fetch<JobItem, ContractReviewPurchaseCost> crPurchaseCost = jobItems.fetch(JobItem_.purchaseCost,
					javax.persistence.criteria.JoinType.LEFT);
			crPurchaseCost.fetch(ContractReviewPurchaseCost_.linkedCost, javax.persistence.criteria.JoinType.LEFT);
			Fetch<JobItem, ContractReviewRepairCost> crRepairCost = jobItems.fetch(JobItem_.repairCost,
					javax.persistence.criteria.JoinType.LEFT);
			crRepairCost.fetch(ContractReviewRepairCost_.linkedCost, javax.persistence.criteria.JoinType.LEFT);

			Fetch<JobItem, Instrument> instruments = jobItems.fetch(JobItem_.inst,
					javax.persistence.criteria.JoinType.LEFT);

			Fetch<Instrument, InstrumentModel> model = instruments.fetch(Instrument_.model,
					javax.persistence.criteria.JoinType.LEFT);
			model.fetch(InstrumentModel_.description, javax.persistence.criteria.JoinType.LEFT);
			model.fetch(InstrumentModel_.modelType, javax.persistence.criteria.JoinType.LEFT);


			cq.where(cb.equal(root.get(Job_.jobid), jobid));
			cq.distinct(true);
			cq.select(root);
			return cq;
		}).orElse(null);
	}

	@Override
	public Job getJobByExactJobNo(String jobNo) {
	    return getFirstResult(cb -> {
	    	val cq = cb.createQuery(Job.class);
	    	val job = cq.from(Job.class);
	    	cq.where(cb.equal(job.get(Job_.jobno),jobNo));
	    	return cq;
				}).orElse(null);
	}

	@Override
	public ResultWrapper getJobsAndItemsNotInStateGroupForAddress(List<StateGroup> stateGroups, int addrId) {
		LocalDate dateNow = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		val dateLater = dateNow.plusDays(7);

		val groupLink = stateGroups.stream().map(StateGroup::getId).collect(Collectors.toList());
		List<JobsForAddressWrapper> list = this.getResultList(cb -> {
			val cq = cb.createQuery(JobsForAddressWrapper.class);
			val job = cq.from(Job.class);
			val returnTo = job.join(Job_.returnTo);
			val item = job.join(Job_.items);
			val inst = item.join(JobItem_.inst);
			val itemState = item.join(JobItem_.state);
			val status = job.join(Job_.js);
			cq.select(cb.construct(JobsForAddressWrapper.class,
					job.get(Job_.jobid),job.get(Job_.jobno),item.get(JobItem_.itemNo),item.get(JobItem_.dueDate),inst,itemState.get(ItemState_.description)
					));
			val subquery = cq.subquery(Integer.class);
			val subJob = subquery.from(Job.class);
			subquery.select(subJob.get(Job_.jobid))
					.where(cb.and(
							cb.equal(subJob.get(Job_.returnTo),addrId),
							cb.isFalse(subJob.join(Job_.js).get(JobStatus_.complete)),
							subJob.join(Job_.items).join(JobItem_.state).join(ItemState_.groupLinks).in(groupLink)
					));

			cq.where(cb.and(
					cb.equal(returnTo,addrId),
					cb.between(item.get(JobItem_.dueDate),dateNow,dateLater),
					cb.isFalse(status.get(JobStatus_.complete)),
					job.in(subquery)
			));
			return cq;
		});

		return new ResultWrapper(true, "", list, null);
	}

	@Override
	public List<Job> getJobsById(Collection<Integer> ids) {
	    return getResultList(cb -> {
	    	val cq = cb.createQuery(Job.class);
	    	val job = cq.from(Job.class);
	    	cq.where(job.in(ids));
	    	return cq;
		});
	}


	public List<Job> getJobsForSearchAndExport(Collection<Integer> ids) {
		if (ids == null || ids.isEmpty()) {
			return Collections.emptyList();
		} else {
			return getResultList(cb -> {
				CriteriaQuery<Job> cq = cb.createQuery(Job.class);
				Root<Job> root = cq.from(Job.class);
				Fetch<Job, JobItem> jobItems = root.fetch(Job_.items, javax.persistence.criteria.JoinType.LEFT);
				jobItems.fetch(JobItem_.notInvoiced, javax.persistence.criteria.JoinType.LEFT);
				jobItems.fetch(JobItem_.onBehalf, javax.persistence.criteria.JoinType.LEFT);
				jobItems.fetch(JobItem_.state, javax.persistence.criteria.JoinType.LEFT);

				Fetch<JobItem, ContractReviewAdjustmentCost> crAdjustmentCost = jobItems.fetch(JobItem_.adjustmentCost,
						javax.persistence.criteria.JoinType.LEFT);
				crAdjustmentCost.fetch(ContractReviewAdjustmentCost_.linkedCost,
						javax.persistence.criteria.JoinType.LEFT);
				Fetch<JobItem, ContractReviewCalibrationCost> crCalibrationCost = jobItems
						.fetch(JobItem_.calibrationCost, javax.persistence.criteria.JoinType.LEFT);
				crCalibrationCost.fetch(ContractReviewCalibrationCost_.linkedCost,
						javax.persistence.criteria.JoinType.LEFT);
				Fetch<JobItem, ContractReviewPurchaseCost> crPurchaseCost = jobItems.fetch(JobItem_.purchaseCost,
						javax.persistence.criteria.JoinType.LEFT);
				crPurchaseCost.fetch(ContractReviewPurchaseCost_.linkedCost, javax.persistence.criteria.JoinType.LEFT);
				Fetch<JobItem, ContractReviewRepairCost> crRepairCost = jobItems.fetch(JobItem_.repairCost,
						javax.persistence.criteria.JoinType.LEFT);
				crRepairCost.fetch(ContractReviewRepairCost_.linkedCost, javax.persistence.criteria.JoinType.LEFT);

				Fetch<JobItem, Instrument> instruments = jobItems.fetch(JobItem_.inst,
						javax.persistence.criteria.JoinType.LEFT);

				Fetch<Instrument, InstrumentModel> model = instruments.fetch(Instrument_.model,
						javax.persistence.criteria.JoinType.LEFT);
				model.fetch(InstrumentModel_.description, javax.persistence.criteria.JoinType.LEFT);
				model.fetch(InstrumentModel_.modelType, javax.persistence.criteria.JoinType.LEFT);

				cq.where(root.get(Job_.jobid).in(ids));
				cq.orderBy(cb.desc(root.get(Job_.regDate)), cb.desc(root.get(Job_.jobno)));
				cq.distinct(true);
				cq.select(root);

				return cq;
			});
		}
	}
	
	@Override 
	public List<JobProjectionDTO> getJobProjectionDTOList(Collection<Integer> jobIds, boolean countJobItems, boolean countJobExpenseItems) {
		if (jobIds == null || jobIds.isEmpty())
			throw new IllegalArgumentException("At least one job id must be specified!");
		
		return this.getResultList(cb -> {
			CriteriaQuery<JobProjectionDTO> cq = cb.createQuery(JobProjectionDTO.class);
			Root<Job> root = cq.from(Job.class);
			Join<Job, Contact> contact = root.join(Job_.con, javax.persistence.criteria.JoinType.INNER);
			Join<Job, SupportedCurrency> currency = root.join(Job_.currency, javax.persistence.criteria.JoinType.INNER);
			Join<Job, JobBPOLink> jobBpoLink = root.join(Job_.bpoLinks, javax.persistence.criteria.JoinType.LEFT);
			jobBpoLink.on(cb.isTrue(jobBpoLink.get(JobBPOLink_.defaultForJob)));
			Join<JobBPOLink, BPO> jobBpo = jobBpoLink.join(JobBPOLink_.bpo, javax.persistence.criteria.JoinType.LEFT);
			Join<Job, PO> defaultPo = root.join(Job_.defaultPO, javax.persistence.criteria.JoinType.LEFT);
			Join<Job, JobStatus> jobStatus = root.join(Job_.js, javax.persistence.criteria.JoinType.INNER);
			Join<Job, TransportOption> transportOut = root.join(Job_.returnOption, javax.persistence.criteria.JoinType.LEFT);
			Join<Job, Subdiv> organisation = root.join(Job_.organisation.getName(), javax.persistence.criteria.JoinType.INNER);

			Selection<Long> jiCountSelection = cb.nullLiteral(Long.class);
			if (countJobItems) {
				Subquery<Long> jiCountSq = cq.subquery(Long.class);
				Root<JobItem> jiCount = jiCountSq.from(JobItem.class);
				jiCountSq.where(cb.equal(jiCount.get(JobItem_.job), root));
				jiCountSq.select(cb.count(jiCount));
				jiCountSelection = jiCountSq.getSelection();
			}
			
			Selection<Long> jiExpenseCountSelection = cb.nullLiteral(Long.class);
			if (countJobExpenseItems) {
				Subquery<Long> jiExpenseCountSq = cq.subquery(Long.class);
				Root<JobExpenseItem> jiExpenseCount = jiExpenseCountSq.from(JobExpenseItem.class);
				jiExpenseCountSq.where(cb.equal(jiExpenseCount.get(JobExpenseItem_.job), root));
				jiExpenseCountSq.select(cb.count(jiExpenseCount));
				jiExpenseCountSelection = jiExpenseCountSq.getSelection();
			}


			cq.where(root.get(Job_.jobid).in(jobIds));
			CompoundSelection<JobProjectionDTO> selection = cb.construct(JobProjectionDTO.class, 
					root.get(Job_.jobid),
					root.get(Job_.jobno),
					jobBpo.get(BPO_.poId),
					contact.get(Contact_.personid),
					currency.get(SupportedCurrency_.currencyId),
					defaultPo.get(PO_.poId),
					jobStatus.get(JobStatus_.statusid),
					organisation.get(Subdiv_.subdivid),
					transportOut.get(TransportOption_.id),
					root.get(Job_.clientRef),
					root.get(Job_.dateComplete),
					root.get(Job_.receiptDate),
					root.get(Job_.regDate),
					root.get(Job_.type),
					jiCountSelection,
					jiExpenseCountSelection
				);
			cq.select(selection);
			return cq;	
		});
	}


	public JobStats getJobStats(int personId, int subdivId, int compId, int addrId) {
		return getSingleResult(cb -> {
			val cq = cb.createQuery(JobStats.class);
			val jobitem  = cq.from(JobItem.class);
			val job = jobitem.join(JobItem_.job);
			val status = job.join(Job_.js);
			val con = job.join(Job_.con);
			val sub = con.join(Contact_.sub);
			val returnTo = job.join(Job_.returnTo);
			cq.where(cb.and(cb.isFalse(status.get(JobStatus_.complete)),cb.equal(sub.get(Subdiv_.comp),compId)));
			cq.select(cb.construct(JobStats.class,
					cb.count(jobitem),
                    conditionalCount(cb,subdivId,sub),
					conditionalCount(cb,personId,con),
					conditionalCount(cb,addrId,returnTo),
                    cb.countDistinct(job),
					conditionalCount(cb,subdivId,sub,true),
					conditionalCount(cb,personId,con,true),
					conditionalCount(cb,addrId,returnTo,true)
					));
			return cq;
		});
	}
	private Expression<Long> conditionalCount(CriteriaBuilder cb, Integer id, Expression<?> exp) {
		return conditionalCount(cb,id,exp,false);
	}
	private Expression<Long> conditionalCount(CriteriaBuilder cb, Integer id, Expression<?> exp, Boolean distinct) {
		val one = cb.literal(1L);
		Function<Expression<Long>,Expression<Long>> operation = distinct? cb::countDistinct:cb::count;
		return operation.apply(
				cb.<Long>selectCase().when(cb.equal(exp,id),one).otherwise(cb.nullLiteral(Long.class)));
	}

	/**
	 * Internal function that parses a {@link JobSearchForm} and returns a
	 * {@link Criteria} object based on the fields within it.
	 * 
	 * @param jsf the {@link JobSearchForm} to build the {@link Criteria} from, not
	 *            null.
	 * @return a {@link Criteria}.
	 */
	@SuppressWarnings("unchecked")
	private Criteria getQueryJobBasicCriteria(OldJobSearchForm jsf) {
		Criteria jobCriteria = this.getSession().createCriteria(Job.class);
		Criteria conCriteria = null;
		Criteria subdivCriteria = null;
		Criteria instCrit = null;
		Criteria itemCrit = null;
		Criteria returnToCrit = jobCriteria.createCriteria("returnTo");
		// return to
		if ((jsf.getReturnTo() != null) && !jsf.getReturnTo().equals("")) {
			returnToCrit.add(Restrictions.ilike("town", jsf.getReturnTo(), MatchMode.ANYWHERE));
		}
		// job no
		if ((jsf.getJobno() != null) && !jsf.getJobno().equals("")) {
			jobCriteria.add(Restrictions.ilike("jobno", jsf.getJobno(), MatchMode.ANYWHERE));
		}
		// job type
		if ((jsf.getJobType() != null) && !jsf.getJobType().equals(JobType.UNDEFINED)) {
			jobCriteria.add(Restrictions.eq("type", jsf.getJobType()));
		}
		// plant no / serial no
		if (((jsf.getPlantno() != null) && !jsf.getPlantno().equals(""))
				|| ((jsf.getSerialno() != null) && !jsf.getSerialno().equals(""))
				|| ((jsf.getPlantid() != null) && !jsf.getPlantid().equals(""))) {
			if (itemCrit == null) {
				itemCrit = jobCriteria.createCriteria("items", JoinType.LEFT_OUTER_JOIN);
			}

			instCrit = itemCrit.createCriteria("inst");

			if ((jsf.getPlantid() != null) && !jsf.getPlantid().equals("")) {
				if (NumberTools.isAnInteger(jsf.getPlantid())) {
					Disjunction or = Restrictions.disjunction();
					or.add(Restrictions.idEq(Integer.parseInt(jsf.getPlantid())));
					or.add(Restrictions.eq("formerBarCode", jsf.getPlantid()));
					instCrit.add(or);
				} else {
					instCrit.add(Restrictions.eq("formerBarCode", jsf.getPlantid()));
				}
			} else {
				instCrit.add(Restrictions.ilike("plantno", jsf.getPlantno(), MatchMode.ANYWHERE));
				instCrit.add(Restrictions.ilike("serialno", jsf.getSerialno(), MatchMode.ANYWHERE));
			}
		}
		// client reference
		if ((jsf.getClientRef() != null) && !jsf.getClientRef().equals("")) {
			jobCriteria.add(Restrictions.ilike("clientRef", jsf.getClientRef(), MatchMode.ANYWHERE));
		}
		// company
		// Added the onbehalf company
		if ((jsf.getCoid() != null) && !jsf.getCoid().equals("")) {
			conCriteria = jobCriteria.createCriteria("con");
			subdivCriteria = conCriteria.createCriteria("sub");

			if (itemCrit == null) {
				itemCrit = jobCriteria.createCriteria("items", JoinType.LEFT_OUTER_JOIN);
			}

			Criteria onbehalfCriteria = itemCrit.createCriteria("onBehalf", JoinType.LEFT_OUTER_JOIN);
			subdivCriteria.createAlias("comp", "ownerCompany");
			onbehalfCriteria.createAlias("company", "onBehalfCompany", JoinType.LEFT_OUTER_JOIN);
			Disjunction or = Restrictions.disjunction();
			or.add(Restrictions.eq("ownerCompany.coid", Integer.parseInt(jsf.getCoid())));
			or.add(Restrictions.eq("onBehalfCompany.coid", Integer.parseInt(jsf.getCoid())));
			jobCriteria.add(or);
		}
		if (jsf.getSubdivid() != null && jsf.getSubdivid() != 0) {
			if (conCriteria == null)
				conCriteria = jobCriteria.createCriteria("con");
			if (subdivCriteria == null)
				subdivCriteria = conCriteria.createCriteria("sub");
			subdivCriteria.add(Restrictions.idEq(jsf.getSubdivid()));
		}
		// business company
		if ((jsf.getOrgId() != null) && !jsf.getOrgId().equals("")) {
			jobCriteria.add(this.getSubdivCrit(Integer.parseInt(jsf.getOrgId())));
		}
		// web search criteria
		if (jsf instanceof OldWebJobSearchForm) {
			OldWebJobSearchForm webForm = (OldWebJobSearchForm) jsf;
			if (webForm.getSubdivid() != null) {
				if (conCriteria == null) {
					conCriteria = jobCriteria.createCriteria("con");
				}
				if (subdivCriteria == null) {
					subdivCriteria = conCriteria.createCriteria("sub");
				}
				subdivCriteria.add(Restrictions.eq("subdivid", webForm.getSubdivid()));
			}
			if (webForm.getAddrid() != null) {
				returnToCrit.add(Restrictions.idEq(webForm.getAddrid()));
			}
		}
		// contact
		if ((jsf.getPersonid() != null) && (!jsf.getPersonid().equals(""))) {
			if (conCriteria == null) {
				conCriteria = jobCriteria.createCriteria("con");
			}
			conCriteria.add(Restrictions.eq("personid", Integer.parseInt(jsf.getPersonid())));
		}
		// subdiv (contact wins)
		else if (jsf.getSubdivid() != null && jsf.getSubdivid() != 0) {
			if (conCriteria == null)
				conCriteria = jobCriteria.createCriteria("con");
			if (subdivCriteria == null)
				subdivCriteria = conCriteria.createCriteria("sub");
			subdivCriteria.add(Restrictions.eq("subdivid", jsf.getSubdivid()));
		}
		// client purchase order no
		if ((jsf.getPurOrder() != null) && !jsf.getPurOrder().trim().equals("")) {
			// get distinct list matching bpo ids
			Criteria bpoCrit = this.getSession().createCriteria(BPO.class)
					.add(Restrictions.ilike("poNumber", jsf.getPurOrder().trim(), MatchMode.ANYWHERE));
			bpoCrit.setProjection(
					Projections.distinct(Projections.projectionList().add(Projections.id()).add(Projections.id())));

			// build a comma separated list of bpo ids
			String m = "";
			for (Object[] o : (List<Object[]>) bpoCrit.list()) {
				if (!m.equals("")) {
					m = m + ",";
				}
				m = m + o[0];
			}

			// if no BPOs match the po number, add -1 to the list of IDs to stop
			// the
			// criteria from throwing an exception with empty "in ()" clause
			if (m.trim().isEmpty()) {
				m = "-1";
			}

			// get matching POs
			Criteria poCrit = this.getSession().createCriteria(PO.class)
					.add(Restrictions.ilike("poNumber", jsf.getPurOrder().trim(), MatchMode.ANYWHERE));
			List<PO> pos = poCrit.list();
			Set<Integer> jobIds = new HashSet<>();

			// get the list of jobids from the POs
			for (PO p : pos) {
				jobIds.add(p.getJob().getJobid());
			}

			// if no POs match the po number, add -1 to the list of job ids to
			// stop
			// the criteria from throwing an exception with empty "in ()" clause
			if (jobIds.size() < 1) {
				jobIds.add(-1);
			}

			// add both criteria in an OR expression
			jobCriteria.add(Restrictions.or(Restrictions.sqlRestriction("({alias}.bpoid in (" + m + "))"),
					Restrictions.in("jobid", jobIds)));
		}
		// job active
		if ((jsf.getActive() != null) && jsf.getActive()) {
			Criteria activeCriteria = jobCriteria.createCriteria("js");
			activeCriteria.add(Restrictions.eq("complete", false));
		}
		// manufacturer
		if (((jsf.getMfr() != null) && !jsf.getMfr().trim().equals(""))
				|| ((jsf.getModel() != null) && !jsf.getModel().trim().equals(""))
				|| ((jsf.getDesc() != null) && !jsf.getDesc().trim().equals(""))
				|| ((jsf.getModelid() != null) && !jsf.getModelid().equals(0))
				|| ((jsf.getMfrid() != null) && !jsf.getMfrid().equals(0))
				|| ((jsf.getDescid() != null) && !jsf.getDescid().equals(0))) {
			if (instCrit == null) {
				if (itemCrit == null) {
					itemCrit = jobCriteria.createCriteria("items");
				}

				instCrit = itemCrit.createCriteria("inst");
			}

			Criteria modelCrit = instCrit.createCriteria("model");
			Criteria mfrCrit = modelCrit.createCriteria("mfr");

			// add model filtering
			if ((jsf.getModelid() != null) && !jsf.getModelid().equals(0)) {
				modelCrit.add(Restrictions.idEq(jsf.getModelid()));
			} else if ((jsf.getModel() != null) && !jsf.getModel().trim().equals("")) {
				modelCrit.add(Restrictions.ilike("model", jsf.getModel(), MatchMode.ANYWHERE));
			}

			// add description filtering
			if (jsf.getDescid() != null) {
				modelCrit.createCriteria("description").add(Restrictions.idEq(jsf.getDescid()));
			} else if ((jsf.getDesc() != null) && !jsf.getDesc().trim().equals("")) {
				modelCrit.createCriteria("description")
						.add(Restrictions.ilike("description", jsf.getDesc(), MatchMode.ANYWHERE));
			}

			// add mfr filtering
			if (jsf.getMfrid() != null) {
				// this will find all instruments with a model whose mfr
				// matches the
				// given id
				instCrit.add(Restrictions
						.sqlRestriction("({alias}.mfrid = " + jsf.getMfrid() + " or {alias}.mfrid is null)"));

				// this will find all instruments whose instrument
				// specific mfr
				// matches the given id
				// and whose model type mfr is set to be generic
				mfrCrit.add(Restrictions.sqlRestriction("({alias}.mfrid = " + jsf.getMfrid() + " or ({alias}.mfrid != "
						+ jsf.getMfrid() + " and {alias}.genericmfr = 1))"));
			} else if ((jsf.getMfr() != null) && !jsf.getMfr().trim().equals("")) {
				this.mfrDao.addMfrCriteria(instCrit, mfrCrit, jsf.getMfr());
			}
		}
		// procedure
		if ((jsf.getProcId() != null) && (jsf.getProcId() != 0)) {
			if (itemCrit == null) {
				itemCrit = jobCriteria.createCriteria("items");
			}

			itemCrit.createCriteria("proc").add(Restrictions.idEq(jsf.getProcId()));
		}
		// booked in date
		if (jsf.getBookedInDate1() != null) {
			if (jsf.isBookedInDateBetween()) {
				jobCriteria.add(Restrictions.between("regDate", jsf.getBookedInDate1(),
						jsf.getBookedInDate2()));
			} else {
				jobCriteria.add(Restrictions.eq("regDate", jsf.getBookedInDate1()));
			}
		}
		// completed date
		if (jsf.getCompletedDate1() != null) {
			if (jsf.isCompletedDateBetween()) {
				jobCriteria.add(Restrictions.between("dateComplete", jsf.getCompletedDate1(),
						jsf.getCompletedDate2()));
			} else {
				jobCriteria.add(Restrictions.eq("dateComplete", jsf.getCompletedDate1()));
			}
		}

		return jobCriteria;
	}

	@Override
	@Deprecated
	public PagedResultSet<Job> queryJob(OldJobSearchForm jsf, PagedResultSet<Job> rs) {
		// prepare criteria
		Criteria jobCriteria = this.getQueryJobBasicCriteria(jsf);

		// get count of distinct objects returned
		int count = ((Long) jobCriteria.setProjection(Projections.countDistinct("jobid")).uniqueResult()).intValue();
		rs.setResultsCount(count);

		// prepare order as list
		List<Order> ordering = Arrays.asList(Order.desc("regDate"), Order.desc("jobno"));

		// reset projection to return only the necessary fields: any columns
		// that are used in the order by clause to prevent SQL Server
		// complaining, followed by the job id (this MUST be last). if a 'null'
		// projection is set and all fields are returned, one of the joins to
		// the child associations (e.g. job items and/or instruments) can cause
		// the results to be non-distinct (hibernate issue)
		jobCriteria.setProjection(Projections.distinct(Projections.projectionList().add(Projections.property("regDate"))
				.add(Projections.property("jobno")).add(Projections.property("jobid"))));
		jobCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		// reapply any odering
		for (Order order : ordering) {
			jobCriteria.addOrder(order);
		}

		// set the pagination
		jobCriteria.setFirstResult(rs.getStartResultsFrom());
		jobCriteria.setMaxResults(rs.getResultsPerPage());

		// projection will cause .list() to return a list of the IDs (the last
		// field specified in the projection list) so get these and convert them
		// to proper job objects using same ordering as specified above
		@SuppressWarnings("unchecked")
		List<Integer> jobIds = jobCriteria.list();
		List<Job> jobs = this.getJobsForSearchAndExport(jobIds);
		// List<Job> jobs = this.getJobsByIdInSpecificOrder(jobIds, ordering);

		// get page of results
		rs.setResults(jobs);

		return rs;
	}
	
	@Override
	public void queryJobJPANew(JobSearchForm jsf, PagedResultSet<JobProjectionDTO> rs) {
		this.completePagedResultSet(rs, JobProjectionDTO.class, cb -> cq -> {
			Root<Job> root = cq.from(Job.class);
			Join<Job, Subdiv> allocatedSubdiv = root.join(Job_.organisation.getName());
			Join<Subdiv, Company> allocatedCompany = allocatedSubdiv.join(Subdiv_.comp);
			Join<Job, Contact> jobContact = root.join(Job_.con, javax.persistence.criteria.JoinType.INNER);
			Join<Job, JobBPOLink> jobBpoLink = root.join(Job_.bpoLinks, javax.persistence.criteria.JoinType.LEFT);
			Join<JobBPOLink, BPO> jobBpo = jobBpoLink.join(JobBPOLink_.bpo, javax.persistence.criteria.JoinType.LEFT);
			jobBpoLink.on(cb.isTrue(jobBpoLink.get(JobBPOLink_.defaultForJob)));
			Join<Job, PO> defaultPo = root.join(Job_.defaultPO, javax.persistence.criteria.JoinType.LEFT);
			Join<Job, JobStatus> jobStatus = root.join(Job_.js, javax.persistence.criteria.JoinType.INNER);
			Predicate clauses = cb.conjunction();
			
			
			Selection<Long> jiExpenseCountSelection = cb.nullLiteral(Long.class);
				Subquery<Long> jiExpenseCountSq = cq.subquery(Long.class);
				Root<JobExpenseItem> jiExpenseCount = jiExpenseCountSq.from(JobExpenseItem.class);
				jiExpenseCountSq.where(cb.equal(jiExpenseCount.get(JobExpenseItem_.job), root));
				jiExpenseCountSq.select(cb.count(jiExpenseCount));
				jiExpenseCountSelection = jiExpenseCountSq.getSelection();
			


			boolean searchReturnToTown = jsf.getReturnTo() != null && !jsf.getReturnTo().isEmpty();
			Integer searchWebAddressId = null;
			// TODO likely merge "WebSearchForm" form into "JobSearchForm", note subdivid is
			// overridden as int there!
			if (jsf instanceof WebJobSearchForm) {
				WebJobSearchForm webForm = (WebJobSearchForm) jsf;
				if (webForm.getAddrid() != null) {
					searchWebAddressId = webForm.getAddrid();
				}
				throw new UnsupportedOperationException("WebJobSearchForm not yet supported, resolve double subdivid");
			}
			if (searchReturnToTown || searchWebAddressId != null) {
				Join<Job, Address> returnTo = root.join(Job_.returnTo, javax.persistence.criteria.JoinType.LEFT);
				clauses.getExpressions().add(cb.like(returnTo.get(Address_.town), '%' + jsf.getReturnTo() + '%'));
				if (searchWebAddressId != null) {
					clauses.getExpressions().add(cb.equal(returnTo.get(Address_.addrid), searchWebAddressId));
				}
			}
			if ((jsf.getJobno() != null) && !jsf.getJobno().isEmpty()) {
				clauses.getExpressions().add(cb.like(root.get(Job_.jobno), '%' + jsf.getJobno() + '%'));
			}
			if ((jsf.getJobType() != null) && !jsf.getJobType().equals(JobType.UNDEFINED)) {
				clauses.getExpressions().add(cb.equal(root.get(Job_.type), jsf.getJobType()));
			}

			boolean searchPlantId = jsf.getPlantid() != null && !jsf.getPlantid().isEmpty()
					&& NumberTools.isAnInteger(jsf.getPlantid());
			boolean searchPlantNo = jsf.getPlantno() != null && !jsf.getPlantno().isEmpty();
			boolean searchSerialNo = jsf.getSerialno() != null && !jsf.getSerialno().isEmpty();

			Join<Job, JobItem> jobitems = null;
			Join<JobItem, Instrument> instrument = null;

			// TODO / comfirm add a "formerBarCode" or "barcode" combination search; for now
			// omitted.
			if (searchPlantId || searchPlantNo || searchSerialNo) {
				jobitems = root.join(Job_.items, javax.persistence.criteria.JoinType.LEFT);
				instrument = jobitems.join(JobItem_.inst, javax.persistence.criteria.JoinType.INNER);
				if (searchPlantId) {
					Integer plantId = Integer.valueOf(jsf.getPlantid());
					// Previously would search former barcode + plant id if integer
					clauses.getExpressions().add(cb.equal(instrument.get(Instrument_.plantid), plantId));
				}
				if (searchPlantNo) {
					clauses.getExpressions()
							.add(cb.like(instrument.get(Instrument_.plantno), '%' + jsf.getPlantno() + '%'));
				}
				if (searchSerialNo) {
					clauses.getExpressions()
							.add(cb.like(instrument.get(Instrument_.serialno), '%' + jsf.getSerialno() + '%'));
				}
			}
			if ((jsf.getClientRef() != null) && !jsf.getClientRef().equals("")) {
				clauses.getExpressions().add(cb.like(root.get(Job_.clientRef), '%' + jsf.getClientRef() + '%'));
			}
			// TODO change coid to Integer once deprecated job search removed
			boolean searchCompany = jsf.getCoid() != null && !jsf.getCoid().equals("")
					&& NumberTools.isAnInteger(jsf.getCoid());
			boolean searchSubdiv = jsf.getSubdivid() != null && jsf.getSubdivid() != 0;
			boolean searchContact = jsf.getPersonid() != null && !jsf.getPersonid().equals("")
					&& NumberTools.isAnInteger(jsf.getPersonid());

			if (searchCompany || searchSubdiv || searchContact) {
				Join<Contact, Subdiv> jobSubdiv = jobContact.join(Contact_.sub,
						javax.persistence.criteria.JoinType.INNER);
				if (searchContact) {
					Integer personid = Integer.valueOf(jsf.getPersonid());
					clauses.getExpressions().add(cb.equal(jobContact.get(Contact_.personid), personid));
				} else if (searchSubdiv) {
					clauses.getExpressions().add(cb.equal(jobSubdiv.get(Subdiv_.subdivid), jsf.getSubdivid()));
				} else {
					if (jobitems == null) {
						jobitems = root.join(Job_.items, javax.persistence.criteria.JoinType.LEFT);
					}
					Join<JobItem, OnBehalfItem> onBehalf = jobitems.join(JobItem_.onBehalf,
							javax.persistence.criteria.JoinType.LEFT);
					Join<OnBehalfItem, Company> onBehalfCompany = onBehalf.join(OnBehalfItem_.company,
							javax.persistence.criteria.JoinType.LEFT);
					Join<Subdiv, Company> jobCompany = jobSubdiv.join(Subdiv_.comp,
							javax.persistence.criteria.JoinType.INNER);
					Predicate orCompany = cb.disjunction();
					orCompany.getExpressions().add(cb.equal(jobCompany.get(Company_.coid), jsf.getCoid()));
					orCompany.getExpressions().add(cb.equal(onBehalfCompany.get(Company_.coid), jsf.getCoid()));
					clauses.getExpressions().add(orCompany);
				}
			}
			// TODO change orgid to integer after deprecated query removed
			if ((jsf.getOrgId() != null) && !jsf.getOrgId().equals("")) {
				Integer allocatedSubdivId = Integer.valueOf(jsf.getOrgId());
				Join<Job, Subdiv> jobBusinessSubdiv = root.join(Job_.organisation.getName(),
						javax.persistence.criteria.JoinType.INNER);
				clauses.getExpressions().add(cb.equal(jobBusinessSubdiv.get(Subdiv_.subdivid), allocatedSubdivId));
			}
			if ((jsf.getPurOrder() != null) && !jsf.getPurOrder().trim().equals("")) {
				// Note, continuing with previous implementation which searched job pos / bpos
				// only
				Predicate orPoNumber = cb.disjunction();

				Join<Job, JobBPOLink> jobBpoLink2 = root.join(Job_.bpoLinks, javax.persistence.criteria.JoinType.LEFT);
				Join<JobBPOLink, BPO> bpo = jobBpoLink2.join(JobBPOLink_.bpo, javax.persistence.criteria.JoinType.LEFT);
				orPoNumber.getExpressions().add(cb.like(bpo.get(BPO_.poNumber), '%' + jsf.getPurOrder() + '%'));

				Join<Job, PO> po = root.join(Job_.defaultPO, javax.persistence.criteria.JoinType.LEFT);
				orPoNumber.getExpressions().add(cb.like(po.get(PO_.poNumber), '%' + jsf.getPurOrder() + '%'));

				clauses.getExpressions().add(orPoNumber);
			}
			// TODO it would be nice to be able to search for complete = true/false/any
			if ((jsf.getActive() != null) && jsf.getActive()) {
				clauses.getExpressions().add(cb.equal(jobStatus.get(JobStatus_.complete), false));
			}
			boolean searchMfrName = jsf.getMfr() != null && !jsf.getMfr().trim().isEmpty();
			boolean searchMfrId = jsf.getMfrid() != null && !jsf.getMfrid().equals(0);
			boolean searchModelName = jsf.getModel() != null && !jsf.getModel().trim().isEmpty();
			boolean searchModelId = jsf.getModelid() != null && !jsf.getModelid().equals(0);
			boolean searchSubfamilyName = jsf.getDesc() != null && !jsf.getDesc().trim().isEmpty();
			boolean searchSubfamilyId = jsf.getDescid() != null && !jsf.getDescid().equals(0);

			boolean searchMfr = searchMfrName || searchMfrId;
			boolean searchModel = searchModelName || searchModelId;
			boolean searchSubfamily = searchSubfamilyName || searchSubfamilyId;
			boolean searchProcId = (jsf.getProcId() != null) && (jsf.getProcId() != 0);
			boolean searchBookedInDate = jsf.getBookedInDate1() != null;
			boolean searchCompletedDate = jsf.getCompletedDate1() != null;

			if (jobitems == null && searchMfr || searchModel || searchSubfamily || searchProcId || searchBookedInDate
					|| searchCompletedDate) {
				jobitems = root.join(Job_.items, javax.persistence.criteria.JoinType.LEFT);
			}
			if (searchMfr || searchModel || searchSubfamily) {
				if (instrument == null) {
					instrument = jobitems.join(JobItem_.inst, javax.persistence.criteria.JoinType.INNER);
				}

				Join<Instrument, InstrumentModel> instmodel = instrument.join(Instrument_.model,
						javax.persistence.criteria.JoinType.LEFT);
				if (searchSubfamily) {
					Join<InstrumentModel, Description> description = instmodel.join(InstrumentModel_.description);
					if (searchSubfamilyId) {
						clauses.getExpressions().add(cb.equal(description.get(Description_.id), jsf.getDescid()));
					} else if (searchSubfamilyName) {
						// Note, this searches default language only (like old implementation)
						clauses.getExpressions()
								.add(cb.like(description.get(Description_.description), "%" + jsf.getDesc() + "%"));
					}
				}

				if (searchModelId) {
					clauses.getExpressions().add(cb.equal(instmodel.get(InstrumentModel_.modelid), jsf.getModelid()));
				} else if (searchModelName) {
					// Former implementation did not search model name on instrument, it is searched
					// now...
					Predicate modelClauses = cb.disjunction();
					modelClauses.getExpressions()
							.add(cb.like(instmodel.get(InstrumentModel_.model), "%" + jsf.getModel() + "%"));
					modelClauses.getExpressions()
							.add(cb.like(instrument.get(Instrument_.modelname), "%" + jsf.getModel() + "%"));
					clauses.getExpressions().add(modelClauses);
				}

				if (searchMfr) {
					Join<InstrumentModel, Mfr> modelMfr = instmodel.join(InstrumentModel_.mfr,
							javax.persistence.criteria.JoinType.LEFT);
					Join<Instrument, Mfr> instrumentMfr = instrument.join(Instrument_.mfr,
							javax.persistence.criteria.JoinType.LEFT);
					Predicate mfrClauses = cb.disjunction();
					if (searchMfrId) {
						mfrClauses.getExpressions().add(cb.equal(modelMfr.get(Mfr_.mfrid), jsf.getMfrid()));
						mfrClauses.getExpressions().add(cb.equal(instrumentMfr.get(Mfr_.mfrid), jsf.getMfrid()));
					} else if (searchMfrName) {
						mfrClauses.getExpressions().add(cb.like(modelMfr.get(Mfr_.name), "%" + jsf.getMfr() + "%"));
						mfrClauses.getExpressions()
								.add(cb.like(instrumentMfr.get(Mfr_.name), "%" + jsf.getMfr() + "%"));

					}
					clauses.getExpressions().add(mfrClauses);
				}
			}
			if (searchProcId) {
                // Currently searches main job item proc, test/tune performance if adding WR
                // search
                Join<JobItem, Capability> capability = jobitems.join(JobItem_.capability,
                    javax.persistence.criteria.JoinType.LEFT);
                clauses.getExpressions().add(cb.equal(capability.get(Capability_.id), jsf.getProcId()));
            }
			// Date searches kept on job, not practical on job items (datetime info)
			if (searchBookedInDate) {
				if (jsf.isBookedInDateBetween() && jsf.getBookedInDate2() != null) {
					clauses.getExpressions().add(
							cb.between(root.get(Job_.regDate), jsf.getBookedInDate1(), jsf.getBookedInDate2()));
				} else {
					clauses.getExpressions().add(cb.equal(root.get(Job_.regDate), jsf.getBookedInDate1()));
				}
			}
			if (searchCompletedDate) {
				if (jsf.isCompletedDateBetween() && jsf.getCompletedDate2() != null) {
					clauses.getExpressions().add(cb.between(root.get(Job_.dateComplete),
							jsf.getCompletedDate1(), jsf.getCompletedDate2()));
				} else {
					clauses.getExpressions()
							.add(cb.equal(root.get(Job_.dateComplete), jsf.getCompletedDate1()));
				}
			}
			cq.where(clauses);
			cq.distinct(true);

			CompoundSelection<JobProjectionDTO> selection = cb.construct(JobProjectionDTO.class, root.get(Job_.jobid),
					root.get(Job_.jobno), jobBpo.get(BPO_.poId), jobBpo.get(BPO_.poNumber), jobContact.get(Contact_.personid), 
					defaultPo.get(PO_.poId), defaultPo.get(PO_.poNumber),  jobStatus.get(JobStatus_.statusid),
					allocatedSubdiv.get(Subdiv_.subdivid) ,allocatedCompany.get(Company_.coid),  root.get(Job_.clientRef),
					root.get(Job_.dateComplete), root.get(Job_.receiptDate), root.get(Job_.pickupDate), 
					root.get(Job_.regDate), root.get(Job_.type.getName()), jiExpenseCountSelection);
			List<javax.persistence.criteria.Order> order = new ArrayList<>();
			order.add(cb.desc(root.get(Job_.regDate)));
			order.add(cb.desc(root.get(Job_.jobno)));
			order.add(cb.desc(root.get(Job_.jobid)));

			return Triple.of(root, selection, order);
		}, true);

	}

	@Override
	public List<Job> searchJob(String jobNo) {
	    return getResultList(cb -> {
	    	val cq = cb.createQuery(Job.class);
	    	val job = cq.from(Job.class);
	    	cq.where(StringJpaUtils.ilike(cb,job.get(Job_.jobno),jobNo,MatchMode.START));
	    	cq.orderBy(cb.desc(job.get(Job_.jobno)));
	    	return cq;
		});
	}

	@Override
	public List<Job> getActiveJobsBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Job> cq = cb.createQuery(Job.class);

		Root<Job> root = cq.from(Job.class);
		Join<Job, JobStatus> jobStatusJoin = root.join(Job_.js.getName());
		Join<Job, Contact> contactJoin = root.join(Job_.con.getName());

		cq.select(root);
		cq.where(cb.and(cb.equal(root.get(Job_.organisation), allocatedSubdiv),
				cb.equal(jobStatusJoin.get(JobStatus_.complete), false),
				cb.equal(contactJoin.get(Contact_.sub), subdiv)));

		return getEntityManager().createQuery(cq).getResultList();
	}

	@Override
	public List<ActiveInstrumentDto> findActiveJobsByPlantids(List<String> plantIds) {
		return getResultList(cb -> {
			CriteriaQuery<ActiveInstrumentDto> cq = cb.createQuery(ActiveInstrumentDto.class);
			Root<Job> root = cq.from(Job.class);
			Join<Job, JobItem> jobitem = root.join(Job_.items);
			Join<JobItem, ItemState> state = jobitem.join(JobItem_.state);
			Join<JobItem, Instrument> inst = jobitem.join(JobItem_.inst);

			Predicate clauses = cb.conjunction();

			clauses.getExpressions().add(cb.equal(state.get(ItemState_.active), true));
			clauses.getExpressions().add(inst.get(Instrument_.plantid).in(plantIds));
			cq.where(clauses);

			cq.select(cb.construct(ActiveInstrumentDto.class, 
					inst.get(Instrument_.plantid),
					inst.get(Instrument_.plantno),
					inst.get(Instrument_.serialno),
					jobitem.get(JobItem_.itemNo),
					jobitem.get(JobItem_.jobItemId),
					root.get(Job_.jobno),
					root.get(Job_.jobid)));
			cq.distinct(true);
			return cq;
		});
	}

	@Override
	public Optional<OrderProjection> getOrderProjectionForJob(Integer jobId) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(OrderProjection.class);
			val job = cq.from(Job.class);
			val contact = job.join(Job_.con);
			val contactCompany = contact.join(Contact_.sub).join(Subdiv_.comp);
			val contactCompanyAddress = contactCompany.join(Company_.legalAddress);
			val jobOrganisation = job.<Job,Subdiv>join(Job_.organisation.getName());
			val jobOrganisationCompany = jobOrganisation.join(Subdiv_.comp);
			val jobOrganisationAddress = jobOrganisationCompany.join(Company_.legalAddress);
			cq.where(cb.equal(job, jobId));
			cq.select(cb.construct(OrderProjection.class,
					job.get(Job_.jobid),
					job.get(Job_.jobno),
					contactCompany.get(Company_.coid),
					contactCompany.get(Company_.coname),
				contactCompanyAddress.get(Address_.addr1),
				contactCompanyAddress.get(Address_.addr2),
				contactCompanyAddress.get(Address_.addr3),
				contactCompanyAddress.get(Address_.town),
				contactCompanyAddress.join(Address_.country).get(Country_.countryCode),
				StringJpaUtils.trimAndConcatWithWhitespace(contact.get(Contact_.firstName), contact.get(Contact_.lastName)).apply(cb),
				contactCompanyAddress.get(Address_.postcode),
				contact.get(Contact_.telephone),
				contact.get(Contact_.fax),
				contact.get(Contact_.email),
				job.get(Job_.regDate).as(LocalDate.class),
				jobOrganisation.get(Subdiv_.subname),
				jobOrganisationCompany.get(Company_.coname),
				jobOrganisationAddress.get(Address_.addr1),
				jobOrganisationAddress.get(Address_.addr2),
				jobOrganisationAddress.get(Address_.addr3),
				jobOrganisationAddress.get(Address_.town),
				jobOrganisationAddress.join(Address_.country).get(Country_.countryCode),
				jobOrganisationAddress.get(Address_.postcode),
				job.get(Job_.jobno)
			));
			return cq;
		});
	}
}