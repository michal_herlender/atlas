package org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.CalibrationPoint;

public interface CalibrationPointService
{
	void deleteAll(Collection<CalibrationPoint> points);

	void deleteCalibrationPoint(CalibrationPoint calibrationpoint);

	CalibrationPoint findCalibrationPoint(int id);

	List<CalibrationPoint> getAllCalibrationPoints();

	void insertCalibrationPoint(CalibrationPoint calibrationpoint);

	void saveOrUpdateCalibrationPoint(CalibrationPoint calibrationpoint);

	/**
	 * Takes a {@link String} list of points readings and removes any
	 * blank/empty points entries from the list. Method works by checking for
	 * the last text entry and trimming all entries after this. Any empty values
	 * found between non-empty values are preserved.
	 * 
	 * @param points list of {@link String} points to be trimmed.
	 * @return trimmed list of points.
	 */
	List<String> trimCalPointsList(List<String> points);

	void updateCalibrationPoint(CalibrationPoint calibrationpoint);
}