package org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationProcessKeyValue;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;

@Service
public class CalibrationProcessServiceImpl extends BaseServiceImpl<CalibrationProcess, Integer>
		implements CalibrationProcessService {

	@Autowired
	private CalibrationProcessDao calibrationProcessDao;

	@Override
	protected BaseDao<CalibrationProcess, Integer> getBaseDao() {
		return calibrationProcessDao;
	}

	@Override
	public CalibrationProcess findByName(String name) {
		return this.calibrationProcessDao.findByName(name);
	}

	@Override
	public List<CalibrationProcessKeyValue> findAllKeyValue() {
		return this.calibrationProcessDao.findAllKeyValue();
	}
}