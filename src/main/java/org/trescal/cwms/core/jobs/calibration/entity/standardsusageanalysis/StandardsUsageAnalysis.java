package org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysisstatus.StandardsUsageAnalysisStatus;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

import lombok.Setter;

@Entity
@Setter
@Table(name = "standardsusageanalysis")
public class StandardsUsageAnalysis implements ComponentEntity {

	private Integer id;
	private Date startDate;
	private Date finishDate;
	private Instrument instrument;
	private StandardsUsageAnalysisStatus status;
	private Certificate certificate;
	private File directory;
	private List<Email> sentEmails;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getId() {
		return id;
	}

	@Column(name = "startdate", columnDefinition = "date", nullable = true)
	public Date getStartDate() {
		return startDate;
	}

	@NotNull
	@Column(name = "finishdate", columnDefinition = "date", nullable = false)
	public Date getFinishDate() {
		return finishDate;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "instid", foreignKey = @ForeignKey(name = "FK_standardsusageanalysis_instrument"))
	public Instrument getInstrument() {
		return instrument;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "statusid", nullable = false)
	public StandardsUsageAnalysisStatus getStatus() {
		return status;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="certificateid", foreignKey = @ForeignKey(name ="FK_standardsusageanalysis_certificate"))
	public Certificate getCertificate() {
		return certificate;
	}

	@Override
	@Transient
	public Contact getDefaultContact()
	{
		return this.getInstrument().getDefaultContact();
	}
	
	@Override
	@Transient
	public File getDirectory()
	{
		return this.directory;
	}
	
	@Override
	@Transient
	public ComponentEntity getParentEntity()
	{
		return this.getInstrument();
	}

	@Override
	@Transient
	public List<Email> getSentEmails()
	{
		return this.sentEmails;
	}

	@Override
	@Transient
	public boolean isAccountsEmail() {
		return false;
	}

	@Override
	@Transient
	public String getIdentifier() {
		return String.valueOf(instrument.getPlantid());
	}

	@Override
	public void setDirectory(File file)
	{
		this.directory = file;
	}
	
	@Override
	public void setSentEmails(List<Email> emails)
	{
		this.sentEmails = emails;
	}	
}
