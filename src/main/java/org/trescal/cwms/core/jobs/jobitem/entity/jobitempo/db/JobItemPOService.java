package org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.dto.ClientPurchaseOrderItemDTO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.projection.ClientPurchaseOrderProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.logistics.dto.PoDTO;

/**
 * Interface for accessing and manipulating {@link JobItemPO} entities.
 * 
 * @author jamiev
 */
public interface JobItemPOService
{
	ResultWrapper assignJobItemsToPO(Integer[] jobItemIds, int poId);

	/**
	 * Effectively removes any {@link JobItem} entities on the {@link Job} with
	 * the given ID from the {@link BPO} attached to the {@link Job} entity.
	 * This means that the {@link BPO} can be safely removed from the
	 * {@link Job} entity or de-activated.
	 * 
	 * @param jobid the {@link Job} ID.
	 */
	void deleteBPOLinksToJob(int bpoid, int jobid);

	/**
	 * Deletes the given {@link JobItemPO} from the database.
	 * 
	 * @param jobitempo the {@link JobItemPO} to delete.
	 */
	void deleteJobItemPO(JobItemPO jobitempo);

	/**
	 * Deletes the {@link JobItemPO} with the given ID from the database.
	 * 
	 * @param id the ID of the {@link JobItemPO} to delete.
	 */
	void deleteJobItemPOById(int id);

	/**
	 * Returns the {@link JobItemPO} entity with the given ID.
	 * 
	 * @param id the {@link JobItemPO} ID.
	 * @return the {@link JobItemPO}
	 */
	JobItemPO findJobItemPO(int id);

	/**
	 * Returns all {@link JobItemPO} entities stored in the database.
	 * 
	 * @return the {@link List} of {@link JobItemPO} entities.
	 */
	List<JobItemPO> getAllJobItemPOs();
	
	List<ClientPurchaseOrderProjectionDTO> getProjectionDTOs(Collection<Integer> jobIds, Collection<Integer> jobItemIds);

	/**
	 * Returns all {@link JobItem} entities on the {@link BPO} with the given
	 * ID.
	 * 
	 * @param bpoId the {@link BPO} ID.
	 * @param jobId the {@link Job} ID.
	 * @return the {@link List} of {@link JobItem} entities.
	 */
	List<JobItemWrapper> getItemsOnBPO(int bpoId, int jobId);

	/**
	 * Returns all {@link JobItem} entities on the {@link PO} with the given ID.
	 * 
	 * @param poId the {@link PO} ID.
	 * @return the {@link List} of {@link JobItem} entities.
	 */
	List<JobItemWrapper> getItemsOnPO(int poId);

	/**
	 * Returns a projection list of POs for the job items on the 
	 * indicated Jobs / Job Items as indicated by ID.
	 * If both lists are specified, then OR clause.  
	 */

    List<PoDTO> getValidPODtosForJobItem(Integer jobItemId);

	/**
	 * Inserts the given {@link JobItemPO} into the database.
	 * 
	 * @param jobitempo the {@link JobItemPO} to insert.
	 */
	void insertJobItemPO(JobItemPO jobitempo);

	/**
	 * Inserts a {@link JobItemPO} into the database for the {@link JobItem}
	 * with the given ID and the {@link BPO} attached to the item's {@link Job}
	 * entity.
	 * 
	 * @param jobitemid the {@link JobItem} ID.
	 * @return the newly created {@link JobItemPO}.
	 */
	JobItemPO insertJobItemPOForBPO(int jobitemid);

	/**
	 * Inserts a {@link JobItemPO} into the database with the {@link JobItem}
	 * and {@link PO} entities associated with the given IDs.
	 * 
	 * @param jobitemid the {@link JobItem} ID.
	 * @param poid the {@link PO} ID.
	 * @return the newly created {@link JobItemPO} ID.
	 */
	JobItemPO insertJobItemPOWithIds(int jobitemid, int poid);

	/**
	 * get POs and BPOs using job item
	 * 
	 * @param jobItemId the {@link JobItem} ID.
	 */
	String getAllPOsByJobItem(int jobItemId);
	
	
	void assignItemsToPO(List<Integer> jobItemIds,List<Integer> jobServiceIds, Integer poId, Integer jobId);
	
	List<ClientPurchaseOrderItemDTO> getOnPO(Integer poId);
	
	List<ClientPurchaseOrderItemDTO> getOnBPO(Integer poId, Integer jobId);
	
	ResultWrapper assignJIandJobServicestoBPO(List<Integer> jobItemIds, List<Integer> jobServicesIds, int poId, Integer jobId);
	
	ResultWrapper unassignJIandJobServicestoBPO(Integer jobItemId, Integer jobServicesId, int poId, Integer jobId);
	
	List<Integer> getJobitemIdsOnBPO(Integer bpoid, Integer jobId);
	
	JobItemPO getJobItemPo(Integer jobItemId, int poId, Integer jobId);
	
	
}