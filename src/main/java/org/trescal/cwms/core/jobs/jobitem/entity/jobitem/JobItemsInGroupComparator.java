package org.trescal.cwms.core.jobs.jobitem.entity.jobitem;

import java.util.Comparator;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;

/**
 * Inner comparator class for sorting {@link JobItem}s when returning
 * collections of {@link JobItem}s in {@link JobItemGroup}s
 * 
 * @author jamiev
 * Refactored 2014-1-12 GB to be failsafe for test data that does not contain a complete reference chain.
 * Sort order - (a) default model types, (b) modules, (c) by item number
 */
public class JobItemsInGroupComparator implements Comparator<JobItem>
{
	public int compare(JobItem ji1, JobItem ji2)
	{
		InstrumentModelType modelType1 = resolveInstModelType(ji1);
		InstrumentModelType modelType2 = resolveInstModelType(ji2);
		
		if ((modelType1 == null) ||	
			(modelType2 == null) ||
			(modelType1.getInstModelTypeId() == modelType2.getInstModelTypeId())) 
		{
			// compare by itemNos (primary ID)
			Integer itemNo1 = ji1.getItemNo();
			Integer itemNo2 = ji2.getItemNo();
			return itemNo1.compareTo(itemNo2);
		}
		if (modelType1.getDefaultModelType() && !modelType2.getDefaultModelType())
		{
			return 1;
		}
		if (modelType2.getDefaultModelType() && !modelType1.getDefaultModelType() )
		{
			return -1;
		}
		if (modelType1.getModules() && !modelType2.getModules())
		{
			return -1;
		}
		if (modelType2.getModules() && !modelType1.getModules())
		{
			return 1;
		}
		// compare itemNos
		Integer itemNo1 = ji1.getItemNo();
		Integer itemNo2 = ji2.getItemNo();
		return itemNo1.compareTo(itemNo2);
	}
	// Returns instrument model type, or null if path cannot be navigated
	private InstrumentModelType resolveInstModelType(JobItem item) {
		if (item.getInst() == null) return null;
		if (item.getInst().getModel() == null) return null;
		return item.getInst().getModel().getModelType();
	}
}