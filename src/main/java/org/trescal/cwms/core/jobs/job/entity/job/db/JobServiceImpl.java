package org.trescal.cwms.core.jobs.job.entity.job.db;

import io.vavr.control.Either;
import lombok.val;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courier.db.CourierService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.filenamingservice.JobFileNamingService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.db.NotificationSystemFieldChangeModelService;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.external.adveso.events.JobItemActionCreateOrUpdateEvent;
import org.trescal.cwms.core.instrument.entity.instrument.PossibleInstrumentDTO;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModelComparator;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db.ServiceCapabilityService;
import org.trescal.cwms.core.jobs.calibration.controller.ActiveInstrumentsJsonController.ActiveInstrumentDto;
import org.trescal.cwms.core.jobs.job.controller.GenerateClientFeedbackRequestIn;
import org.trescal.cwms.core.jobs.job.dto.JobKeyValue;
import org.trescal.cwms.core.jobs.job.dto.JobSearchResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.abstractpo.AbstractPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink;
import org.trescal.cwms.core.jobs.job.entity.jobnote.JobNote;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.db.JobStatusService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.po.db.POService;
import org.trescal.cwms.core.jobs.job.form.EditJobForm;
import org.trescal.cwms.core.jobs.job.form.JobSearchForm;
import org.trescal.cwms.core.jobs.job.form.OldJobSearchForm;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.ItemBasketWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItemComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.dao.AsnService;
import org.trescal.cwms.core.logistics.entity.asnitems.AsnItemServiceTypeSource;
import org.trescal.cwms.core.logistics.entity.jobcourier.JobCourierLink;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;
import org.trescal.cwms.core.logistics.form.JobFinalSynthesisDTOForm;
import org.trescal.cwms.core.logistics.form.PrebookingJobForm;
import org.trescal.cwms.core.logistics.utils.RecordTypeEnum;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.EmailResultWrapper;
import org.trescal.cwms.core.system.entity.basestatus.BaseStatus;
import org.trescal.cwms.core.system.entity.basestatus.db.BaseStatusService;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.deletedcomponent.DeletedComponent;
import org.trescal.cwms.core.system.entity.deletedcomponent.db.DeletedComponentService;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.status.Status;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.Turnaround;
import org.trescal.cwms.core.system.entity.template.db.TemplateService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.validation.BeanValidator;
import org.trescal.cwms.core.vdi.projection.OrderProjection;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.web.home.dto.JobStats;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static org.trescal.cwms.core.tools.DateTools.localDateTimeToZonedDateTime;

@Service("jobService")
public class JobServiceImpl extends BaseServiceImpl<Job, Integer> implements JobService {
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private ContactService contactServ;
	@Autowired
	private DeletedComponentService delCompServ;
	@Autowired
	private DocumentService docServ;
	@Autowired
	private POService poService;
	@Autowired
	private JobDao jobDao;
	@Autowired
	private JobStatusService jsServ;
	@Autowired
	private MessageSource messages;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SystemComponentService scServ;
	@Autowired
	private TemplateService templateServ;
	@Autowired
	private UserService userService;
	@Autowired
	private BeanValidator validator;
	@Autowired
	private Turnaround turnaround;
	@Autowired
	private AsnService asnService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private LocationService locationService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private BaseStatusService statusService;
	@Autowired
	private BPOService bpoServ;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private JobService jobServ;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private SupportedCurrencyService currencyServ;
	@Autowired
	private TransportOptionService transportOptionService;
	@Autowired
	private CalibrationTypeService calibrationTypeService;
	@Autowired
	private CourierService courierServ;
	@Autowired
	private ServiceCapabilityService serviceCapabilityService;
	@Autowired
	private JobItemActionService jobItemActionService;
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;
	@Autowired
	private NotificationSystemFieldChangeModelService notificationSystemFieldChangeModelService;
	@Autowired
	private LinkToAdveso linkToAdvesoSettings;
	@Autowired
	private JobDataRepository jobRepository;
	@Autowired
	GenerateClientFeedback generateClientFeedback;
	@Autowired
	private BaseStatusService baseStatusService;

	public static final Logger logger = LoggerFactory.getLogger(JobServiceImpl.class);

	@Override
	protected BaseDao<Job, Integer> getBaseDao() {
		return jobDao;
	}

	public void modifyJobStatus(Job job) {
		JobStatus jobStatus = baseStatusService.findDefaultStatus(JobStatus.class);
		job.setJs(jobStatus);
		jobServ.merge(job);
	}

	private JobSearchResultWrapper wrap(Job job, Company allocatedCompany) {
		JobSearchResultWrapper w = new JobSearchResultWrapper();
		w.setCompany(job.getCon().getSub().getComp().getConame());
		w.setCompanyOnstop(false);
		w.setContact(job.getCon().getName());
		w.setContactActive(true);
		w.setDatein(job.getRegDate());
		w.setJobid(job.getJobid());
		w.setJobno(job.getJobno());
		w.setPersonid(job.getCon().getPersonid());
		w.setCoid(job.getCon().getSub().getComp().getCoid());
		w.setClientref(job.getClientRef());
		if ((job.getPOs() != null) && (job.getPOs().size() > 0))
			w.setPo(job.getPOs().get(0).getPoNumber());
		Subdiv subdiv = job.getCon().getSub();
		Company company = subdiv.getComp();
		CompanySettingsForAllocatedCompany companySettings = companySettingsService.getByCompany(company,
				allocatedCompany);
		if ((companySettings != null && !companySettings.isActive()) || !subdiv.isActive()
				|| !job.getCon().isActive()) {
			w.setContactActive(false);
		}
		if (companySettings != null)
			w.setCompanyOnstop(companySettings.isOnStop());
		return w;
	}

	@Override
	public PagedResultSet<JobSearchResultWrapper> ajaxQueryJob(OldJobSearchForm jsf, int allocatedSubdivId,
			int resPerPage, int pageNo) {
		Company allocatedCompany = subdivService.get(allocatedSubdivId).getComp();
		return this.queryJobOld(jsf, new PagedResultSet<>(resPerPage, pageNo)).apply(

				job -> wrap(job, allocatedCompany));

	}

	@Override
	public PagedResultSet<JobSearchResultWrapper> ajaxQueryJobByCoid(int coid, int allocatedSubdivId, int resPerPage,
			int pageNo) {
		OldJobSearchForm jsf = new OldJobSearchForm();
		jsf.setCoid(String.valueOf(coid));
		return this.ajaxQueryJob(jsf, allocatedSubdivId, resPerPage, pageNo);
	}

	@Override
	public Long countActiveByContactAndAllocatedSubdiv(Contact contact, Subdiv allocatedSubdiv) {
		return this.jobDao.countActiveByContactAndAllocatedSubdiv(contact, allocatedSubdiv);
	}

	@Override
	public Long countByContact(Contact contact) {
		return this.jobDao.countByContact(contact);
	}

	@Override
	public Long countByContactAndAllocatedSubdiv(Contact contact, Subdiv allocatedSubdiv) {
		return this.jobDao.countByContactAndAllocatedSubdiv(contact, allocatedSubdiv);
	}

	@Override
	public Long countActiveBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv) {
		return jobDao.countActiveBySubdivAndAllocatedSubdiv(subdiv, allocatedSubdiv);
	}

	@Override
	public Long countBySubdiv(Subdiv subdiv) {
		return jobDao.countBySubdiv(subdiv);
	}

	@Override
	public Long countBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv) {
		return jobDao.countBySubdivAndAllocatedSubdiv(subdiv, allocatedSubdiv);
	}

	@Override
	public Long countActiveByCompanyAndAllocatedSubdiv(Company company, Subdiv allocatedSubdiv) {
		return jobDao.countActiveByCompanyAndAllocatedSubdiv(company, allocatedSubdiv);
	}

	@Override
	public Long countByCompany(Company company) {
		return jobDao.countByCompany(company);
	}

	@Override
	public Long countByCompanyAndAllocatedSubdiv(Company company, Subdiv allocatedSubdiv) {
		return jobDao.countByCompanyAndAllocatedSubdiv(company, allocatedSubdiv);
	}

	@Override
	public void deleteJobById(int id) {
		this.jobDao.remove(this.get(id));
	}

	@Override
	public ResultWrapper deleteJobWithChecks(int jobid, String reason, HttpSession session) {
		// validate job exists
		Job job = this.get(jobid);
		if (job == null) {
			return new ResultWrapper(false,
					this.messages.getMessage("error.job.notfound", null, "Could not find the job", null));
		} else {
			int items = job.getItems().size();
			int dels = job.getDeliveries().size();
			int costings = job.getJobCostings().size();
			int invs = job.getInvoiceLinks().size();

			if ((items <= 0) && (invs <= 0) && (costings <= 0) && (dels <= 0)) {
				String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
				Contact contact = this.userService.getEagerLoad(username).getCon();
				// create deletion object
				DeletedComponent dj = new DeletedComponent();
				dj.setComponent(Component.JOB);
				dj.setContact(contact);
				dj.setDate(new Date());
				dj.setRefNo(job.getJobno());
				dj.setReason(reason);

				// validate deletion object
				BindException errors = new BindException(dj, "dj");
				this.validator.validate(dj, errors);

				// if validation passes then delete the job
				if (!errors.hasErrors()) {

					// remove the job reference from the asn is exists
					Asn asn = asnService.findAsnByJobId(job.getJobid());
					if (asn != null) {
						asn.setJob(null);
						asnService.merge(asn);
					}

					// delete the job
					this.delete(job);

					// persist the deletedjob
					this.delCompServ.insertDeletedComponent(dj);
				}

				// set deletedjob into the results
				return new ResultWrapper(errors, dj);
			} else {
				String error = "";

				error = (items > 0) ? error + " " + items + " job items," : error;
				error = (invs > 0) ? error + " " + invs + " invoices," : error;
				error = (costings > 0) ? error + " " + costings + " costings," : error;
				error = (dels > 0) ? error + " " + dels + " deliveries," : error;

				error = "Job " + job.getJobno() + " cannot be deleted because it has" + error;

				return new ResultWrapper(false, error, null, null);
			}
		}
	}

	/**
	 * Edits a single job based on the specified (pre-validated) data contained in
	 * form
	 */
	@Override
	public void editJob(EditJobForm form, Contact editedBy) {
		Job job = this.jobServ.get(form.getJobid());

		Address bookedInAddress;
		Location bookedInLocation;
		if (JobType.SITE.equals(form.getJobType())) {
			bookedInAddress = this.addressService.getReferenceOrNull(form.getBookedInClientAddressId());
			bookedInLocation = this.locationService.getReferenceOrNull(form.getBookedInClientLocationId());
		} else {
			bookedInAddress = this.addressService.getReferenceOrNull(form.getBookedInBusinessAddressId());
			bookedInLocation = this.locationService.getReferenceOrNull(form.getBookedInBusinessLocationId());
		}
		Address returnToAddress = this.addressService.getReferenceOrNull(form.getReturnToAddressId());
		Location returnToLocation = this.locationService.getReferenceOrNull(form.getReturnToLocationId());

		TransportOption inOption = this.transportOptionService.getReferenceOrNull(form.getTransportInId());
		TransportOption returnOption = this.transportOptionService.getReferenceOrNull(form.getTransportOutId());

		Contact clientContact = this.contactServ.getReferenceOrNull(form.getJobContactId());

		this.currencyServ.setCurrencyFromForm(job, form.getCurrencyCode(), job.getCon().getSub().getComp());

		job.setBookedInAddr(bookedInAddress);
		job.setBookedInLoc(bookedInLocation);
		job.setReturnTo(returnToAddress);
		job.setReturnToLoc(returnToLocation);
		job.setInOption(inOption);
		job.setReturnOption(returnOption);
		job.setCon(clientContact);
		job.setCreatedBy(this.contactServ.get(form.getBusinessSubdivContactId()));
		job.setAgreedDelDate(form.getAgreedDeliveryDate());
		job.setClientRef(form.getClientReference());
		job.setReceiptDate(localDateTimeToZonedDateTime(form.getReceiptDate()));
		job.setPickupDate(localDateTimeToZonedDateTime(form.getPickupDate()));
		job.setOverrideDateInOnJobItems(form.getOverrideDateInOnJobItems());

		for (JobItem jobItem : job.getItems()) {

			if (form.getOverrideDateInOnJobItems()) {
				if (form.getPickupDate() != null)
					jobItem.setDateIn(localDateTimeToZonedDateTime(form.getPickupDate()));
				else
					jobItem.setDateIn(localDateTimeToZonedDateTime(form.getReceiptDate()));
			}

			val oldDueDate = jobItem.getDueDate();
			// recalculate datein or set it from agreedDelDate if available but only if
			// there is no agreedDelDate on jobitem
			if (jobItem.getAgreedDelDate() == null) {
				if (job.getAgreedDelDate() != null)
					jobItem.setDueDate(job.getAgreedDelDate());
				else
					jobItem.calculateAndSetDueDate();

				// if dueDate is changed register notification
				if (!oldDueDate.equals(jobItem.getDueDate())) {
					Company businessCompany = jobItem.getJob().getOrganisation().getComp();
					Subdiv jobSubdiv = jobItem.getJob().getCon().getSub();
					boolean isLinkedToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
							jobSubdiv.getSubdivid(), businessCompany.getCoid(), new Date());
					if (isLinkedToAdveso) {
						notificationSystemFieldChangeModelService.saveAndPublishNotificationFieldChange(jobItem,
								JobItem_.dueDate.getName(), NotificationSystemFieldTypeEnum.Date,
								jobItem.getDueDate().format(DateTimeFormatter.ISO_DATE),
								oldDueDate.format(DateTimeFormatter.ISO_DATE));
					}
				}
			}
		}
		if (!job.getType().equals(form.getJobType())) {
			job.setType(form.getJobType());
			job.setDefaultCalType(this.calTypeServ.getDefaultCalType(form.getJobType(), job.getOrganisation()));
		}
		if (form.getCopyTransportIn() || form.getCopyTransportOut()) {
			// Job items only needed here, are all fetched at once
			for (JobItem ji : job.getItems()) {
				if (form.getCopyTransportIn()) {
					ji.setInOption(inOption);
				}
				if (form.getCopyTransportOut()) {
					ji.setReturnOption(returnOption);
				}
			}
		}

	}

	@Override
	public List<Job> findAllJobsReadyToInvoice(int coid) {
		return this.jobDao.findAllJobsReadyToInvoice(coid);
	}

	@Override
	public Job findByJobNo(String jobno) {
		return this.jobDao.findByJobNo(jobno);
	}

	@Override
	public List<JobKeyValue> findByPartialJobNo(String partialJobNo) {
		return jobDao.findByPartialJobNo(partialJobNo);
	}

	public Job findEagerJobForView(int id) {
		Job job = this.jobDao.findEagerJobForView(id);
		if (job != null && job.getDirectory() == null) {
			job.setDirectory(getJobPath(job));
		}
		return job;
	}

	@Override
	public Job get(Integer id) {
		Job j = this.jobDao.find(id);
		// 2018-12-12 GB : Prevent directory lookup on repeated gets.
		if (j != null && j.getDirectory() == null) {
			j.setDirectory(getJobPath(j));
		}

		return j;
	}

	@Override
	public Job findWebSafeJob(int jobid) {
		// call existing method to re-use code
		return this.get(jobid);
	}

	@Override
	public Either<String, EmailResultWrapper> generateClientFeedback(GenerateClientFeedbackRequestIn in,
			Contact contact) {
		return generateClientFeedback.apply(in, contact);
	}

	@Override
	public Optional<OrderProjection> getOrderProjectionForJob(Integer jobId) {
		return jobDao.getOrderProjectionForJob(jobId);
	}

	@Override
	public List<JobProjectionDTO> getJobProjectionDTOList(Collection<Integer> jobIds, boolean countJobItems, boolean countJobExpenseItems) {
		List<JobProjectionDTO> result = Collections.emptyList();
		if (!jobIds.isEmpty()) {
			result = this.jobDao.getJobProjectionDTOList(jobIds, countJobItems, countJobExpenseItems);
		}
	
		return result;
	}
	
/*	@Override
	public List<JobProjectionDTO> getJobProjectionDTOExpenseList(Collection<Integer> jobIds, boolean countJobExpenseItems) {
		List<JobProjectionDTO> result = Collections.emptyList();
		if (!jobIds.isEmpty()) {
			result = this.jobDao.getJobProjectionDTOList(jobIds, countJobExpenseItems);
		}
		return result;
	}*/

	@Override
	public Set<InstrumentModel> getDistinctModels(Job job) {
		Set<InstrumentModel> models = new TreeSet<>(new InstrumentModelComparator());
		for (JobItem ji : job.getItems()) {
			models.add(ji.getInst().getModel());
		}
		return models;
	}

	@Override
	public Job getJobByExactJobNo(String jobNo) {
		return this.jobDao.getJobByExactJobNo(jobNo);
	}

/*
	private List<Integer> getJobItemIds(Set<JobItem> jobitems) {
		ArrayList<Integer> ids = new ArrayList<>();
		if (jobitems != null) {
			for (JobItem jobitem : jobitems) {
				ids.add(jobitem.getJobItemId());
			}
		}
		return ids;
	}*/

	@Override
	public File getJobPath(Job job) {
		return (this.compDirServ.getDirectory(this.scServ.findComponent(Component.JOB), job.getJobno(), null));
	}

	@Override
	public ResultWrapper getJobsAndItemsNotInStateGroupForAddress(List<StateGroup> stateGroups, int addrId) {
		return this.jobDao.getJobsAndItemsNotInStateGroupForAddress(stateGroups, addrId);
	}

	@Override
	public List<Job> getJobsById(Collection<Integer> ids) {
		return this.jobDao.getJobsById(ids);
	}

	@Override
	public JobStats getJobStats(int personId, int subdivId, int compId, int addrId) {
		return this.jobDao.getJobStats(personId, subdivId, compId, addrId);
	}

	@Override
	public AbstractPO getMatchingJobPO(int jobId, String poNumber) {
		Job job = this.get(jobId);

		// check if the job has a BPO that matches the poNumber
		if (job.getBpo() != null) {
			if (job.getBpo().getPoNumber().equals(poNumber)) {
				// if so return the BPO
				return job.getBpo();
			}
		}

		// else check if the job has a PO that matches the poNumber
		return this.poService.getMatchingJobPO(jobId, poNumber);
	}

	@Override
	public Integer insertJob(Job j, int subdivId) {
		Subdiv subdiv = subdivService.get(subdivId);
		// look up default cal type
		j.setDefaultCalType(this.calTypeServ.getDefaultCalType(j.getType(), subdiv));
		j.setDefaultTurn(this.turnaround.getTurnaround(j.getCon(), subdiv));
		j.setDirectory((this.compDirServ.getDirectory(this.scServ.findComponent(Component.JOB), j.getJobno())));
		this.jobDao.persist(j);
		return j.getJobid();
	}

	@Override
	public void mergeJob(Job j) {
		this.jobDao.merge(j);
	}

	@Override
	public ResultWrapper noInvoiceRequired(int jobId) {
		Job j = this.get(jobId);

		if (j == null) {
			return new ResultWrapper(false, "Job could not be found");
		}

		if (j.getJs().isReadyToInvoice()) {
			j.setJs(this.jsServ.findCompleteStatus());
			j.setDateComplete(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			this.merge(j);
			return new ResultWrapper(true, null, j, null);
		} else {
			return new ResultWrapper(false, "This job is not currently ready for invoicing");
		}
	}

	@Override
	public ResultWrapper generateJobSheet(int jobid, HttpSession session) {
		logger.debug("generateJobSheet locale: " + LocaleContextHolder.getLocale());
		Job job = this.get(jobid);
		if (job == null) {
			return new ResultWrapper(false,
					this.messages.getMessage("error.job.notfound", null, "Unable to find job", null));
		} else {
			// now generate the job sheet
			Document doc = this.generateJobSheetInternal(job, job.getReturnTo(), job.getEstCarriageOut(), session);

			return new ResultWrapper(doc.isExists(), "", doc, null);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Document generateJobSheetInternal(Job job, Address address, BigDecimal carriage, HttpSession session) {
		logger.debug("Entered printJobSheet() call");
		Map<String, Object> model = new HashMap<>();
		List<String> sessionNewFileList = (List<String>) session.getAttribute(Constants.HTTPSESS_NEW_FILE_LIST);
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		// Contact not returned by service so eager load not needed
		Contact contact = this.userService.get(username).getCon();
		Company client = job.getCon().getSub().getComp();
		Company business = job.getOrganisation().getComp();

		// Parameters for Birt
		model.put("jobid", job.getJobid());
		model.put("jobno", job.getJobno());
		model.put("coid", business.getCoid());
		model.put("subdivid", job.getOrganisation().getSubdivid());
		model.put("customerid", client.getCoid());
		model.put("contactid", job.getCon().getPersonid());
		String locale = contact.getLocale() != null ? contact.getLocale().toString()
				: LocaleContextHolder.getLocale().toString();
		model.put("ReportLocale", locale);
		logger.debug("BIRT parameter jobid : " + job.getJobid());
		logger.debug("BIRT parameter jobno : " + job.getJobno());
		logger.debug("BIRT parameter coid : " + business.getCoid());
		logger.debug("BIRT parameter subdivid : " + job.getOrganisation().getSubdivid());
		logger.debug("BIRT parameter customerid : " + client.getCoid());
		logger.debug("BIRT parameter contactid : " + job.getCon().getPersonid());
		logger.debug("BIRT parameter ReportLocale : " + locale);

		// create new file naming service
		JobFileNamingService jfns = new JobFileNamingService(job, true);
		// delete existing files
		jfns.deleteExistingJobSheets();

		// create the document
		logger.debug("Before call to create job sheet document");
		Document doc = this.docServ.createBirtSQLDocument(model, Component.JOB, "html",
				this.templateServ.findByName("Job Sheet"), jfns, "", sessionNewFileList,
				job.getOrganisation().getComp());
		logger.debug("After call to create job sheet document");

		return doc;
	}

	@Override
	public PagedResultSet<Job> queryJobOld(OldJobSearchForm jsf, PagedResultSet<Job> rs) {
		return this.jobDao.queryJob(jsf, rs);
	}

	@Override
	public List<Job> searchJob(String jobno) {
		return this.jobDao.searchJob(jobno);
	}

	@Override
	public List<AbstractPO> searchMatchingJobPOs(int jobId, String partPoNumber) {
		List<AbstractPO> results = new ArrayList<>();
		Job job = this.get(jobId);
		// check if the job has a BPO that matches this part string
		if (job.getBpo() != null) {
			if (job.getBpo().getPoNumber().contains(partPoNumber)) {
				// if so add the BPO to the list
				results.add(job.getBpo());
			}
		}
		// also check if the job has POs that match the part string
		List<PO> pos = this.poService.searchMatchingJobPOs(jobId, partPoNumber);
		// add matching pos to results
		results.addAll(pos);
		return results;
	}

	@Override
	public void updateDefaultCalType(int jobid, int caltypeid) {
		Job j = this.get(jobid);
		j.setDefaultCalType(this.calTypeServ.find(caltypeid));
		this.merge(j);
	}

	@Override
	public void updateDefaultTurn(int jobid, int turn) {
		Job j = this.get(jobid);
		j.setDefaultTurn(turn);
		this.merge(j);
	}

	@Override
	public List<Job> getActiveJobsBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv) {
		return this.jobDao.getActiveJobsBySubdivAndAllocatedSubdiv(subdiv, allocatedSubdiv);
	}

	@Override
	public Job createJobFromPrebookingOrFile(JobFinalSynthesisDTOForm jobFinalSynthDTOForm, Contact addedBy,
			Subdiv allocatedSubdiv) {

		Job j = new Job();
		PrebookingJobForm pbjf = jobFinalSynthDTOForm.getPbjf();
		j.setType(pbjf.getJobType());
		j.setDefaultCalType(this.calTypeServ.getDefaultCalType(j.getType(), allocatedSubdiv));
		// set contact and address
		j.setReturnTo(this.addressService.get(pbjf.getAddrid()));
		if (pbjf.getLocid() != null) {
			Location loc = this.locationService.get(pbjf.getLocid());
			if (loc != null)
				j.setReturnToLoc(loc);
		}
		j.setCon(this.contactService.get(pbjf.getPersonid()));

		// set basic job info
		j.setClientRef(pbjf.getClientref());
		j.setRegDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		j.setCreatedBy(addedBy);
		j.setOrganisation(allocatedSubdiv);
		j.setJs((JobStatus) this.statusService.findDefaultStatus(JobStatus.class));
		if (j.getType().equals(JobType.SITE)) {
			j.setBookedInAddr(j.getReturnTo());
			j.setBookedInLoc(j.getReturnToLoc());
		} else {
			j.setBookedInAddr(
					(pbjf.getBookedInAddrId() == null) ? null : this.addressService.get(pbjf.getBookedInAddrId()));
			j.setBookedInLoc(
					(pbjf.getBookedInLocId() == null) ? null : this.locationService.get(pbjf.getBookedInLocId()));
		}
		j.setEstCarriageOut((pbjf.getEstCarriageOut() != null) ? pbjf.getEstCarriageOut() : new BigDecimal("0.00"));
		j.setReturnOption(this.transportOptionService.getTransportOptionOut(j.getReturnTo(), allocatedSubdiv));
		j.setInOption(this.transportOptionService.getTransportOptionIn(j.getReturnTo(), allocatedSubdiv));
		// add BPO link
		if (pbjf.getBpo() != null) {
			JobBPOLink bpoLink = new JobBPOLink();
			bpoLink.setJob(j);
			bpoLink.setBpo(this.bpoServ.get(pbjf.getBpo()));
			bpoLink.setDefaultForJob(true);
			bpoLink.setCreatedOn(new Date());
			j.setBpoLinks(new HashSet<>());
			j.getBpoLinks().add(bpoLink);
		}
		// either set the currency and rate from the company default or
		// from the other selected currency
		if (pbjf.getCurrencyCode().equals(Constants.COMPANY_DEFAULT_CURRENCY)) {
			j.setCurrency(j.getCon().getSub().getComp().getCurrency());
			j.setRate(j.getCon().getSub().getComp().getRate());
		} else {
			j.setCurrency(this.currencyServ.findCurrencyByCode(pbjf.getCurrencyCode()));
			j.setRate(j.getCurrency().getDefaultRate());
		}
		String jobno = this.numerationService.generateNumber(NumerationType.JOB, j.getOrganisation().getComp(),
				j.getOrganisation());
		j.setJobno(jobno);
		TransportOption transportIn = transportOptionService.get(pbjf.getTransportIn());
		TransportOption transportOut = transportOptionService.get(pbjf.getTransportOut());
		j.setInOption(transportIn);
		j.setReturnOption(transportOut);

		if (pbjf.getReceiptDate() != null)
			j.setReceiptDate(DateTools.dateToZonedDateTime(pbjf.getReceiptDate()));
		if (pbjf.getPickupDate() != null)
			j.setPickupDate(DateTools.dateToZonedDateTime(pbjf.getPickupDate()));

		j.setOverrideDateInOnJobItems(
				pbjf.getOverrideDateInOnJobItems() != null ? pbjf.getOverrideDateInOnJobItems() : false);

		/* POs */
		List<String> distinctPoListFromFile = jobFinalSynthDTOForm.getIdentifiedJobItemsFinalSynthDTO().stream()
				.map(AsnItemFinalSynthesisDTO::getPoNumber).filter(StringUtils::isNotBlank).distinct()
				.collect(Collectors.toList());
		List<PO> jobPoList = new ArrayList<>();
		if (pbjf.getRecordType().equals(RecordTypeEnum.PREBOOKING)) {
			// load already created POs
			Asn asn = asnService.get(pbjf.getAsnId());
			jobPoList.addAll(asn.getPrebookingJobDetails().getPoList());
			if (j.getPickupDate() == null) {
				j.setPickupDate(DateTools.dateToZonedDateTime(asn.getPrebookingJobDetails().getPickupDate()));
			}
		}

		// add POs from form
		if (CollectionUtils.isNotEmpty(pbjf.getPoNumberList()))
			pbjf.getPoNumberList().forEach(e -> {
				if (distinctPoListFromFile.stream().noneMatch(s -> s.equals(e)))
					distinctPoListFromFile.add(e);
			});

		pbjf.setPoNumberList(new ArrayList<>());
		pbjf.getPoNumberList().addAll(distinctPoListFromFile);
		// add POs from the form to the list
		for (String poNumber : pbjf.getPoNumberList()) {
			// create the new PO if the it number doesn't already exist
			// jobPoList
			boolean exists = jobPoList.stream().anyMatch(po -> po.getPoNumber().equalsIgnoreCase(poNumber));
			if (!exists) {
				PO po = new PO();
				po.setPoNumber(poNumber);
				po.setActive(true);
				if (pbjf.getPonumber().contains(poNumber))
					po.setComment(pbjf.getPoCommentList().get(pbjf.getPonumber().indexOf(poNumber)));
				if (poNumber.equals(pbjf.getPonumber()))
					po.setDefaultForJob(j);
				po.setOrganisation(allocatedSubdiv.getComp());
				jobPoList.add(po);
			}
		}

		// add courier data
		if (pbjf.getCarrier() != null) {
			JobCourierLink jcl = new JobCourierLink();
			Courier courier = courierServ.get(pbjf.getCarrier());
			jcl.setCourier(courier);
			jcl.setJob(j);
			jcl.setTrackingNumber(pbjf.getTrackingNumber());
			if (pbjf.getNumberOfPackages() != null)
				j.setNumberOfPackages(pbjf.getNumberOfPackages());
			j.setPackageType(pbjf.getPackageType());
			j.setStorageArea(pbjf.getStorageArea());
		}

		if (StringUtils.isNotEmpty(jobFinalSynthDTOForm.getPbjf().getComments())) {
			// add comments if any, as a jobnote
			JobNote jobNote = new JobNote();
			jobNote.setActive(true);
			jobNote.setJob(j);
			jobNote.setPublish(false);
			jobNote.setSetBy(addedBy);
			jobNote.setSetOn(new Date());
			jobNote.setNote(jobFinalSynthDTOForm.getPbjf().getComments());
			if (j.getNotes() == null)
				j.setNotes(new TreeSet<>(new NoteComparator()));
			j.getNotes().add(jobNote);
		}

		// persist job
		this.jobServ.insertJob(j, allocatedSubdiv.getSubdivid());

		Set<JobItem> jibitems = this.jiServ.createJobItems(j, jobFinalSynthDTOForm.getIdentifiedJobItemsFinalSynthDTO(),
				addedBy, pbjf.getAsnId());

		j.setItems(new TreeSet<>(new JobItemComparator()));
		j.getItems().addAll(jibitems);

		// link POs to the job
		jobPoList.forEach(po -> po.setJob(j));
		j.setPOs(jobPoList);

		this.merge(j);
		// used to check if the subdiv is linked to adveso
		Company businessCompany = j.getOrganisation().getComp();
		j.getItems().forEach(jjii -> jjii.getActions().forEach(jjiia -> {
			jobItemActionService.saveWithoutInterceptor(jjiia);

			// check if the subdiv is linked to adveso
			boolean isLinkedToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
					jjii.getJob().getCon().getSub().getSubdivid(), businessCompany.getCoid(), new Date());
			// adveso event
			if (isLinkedToAdveso) {
				JobItemActionCreateOrUpdateEvent createEvent = new JobItemActionCreateOrUpdateEvent(this, jjiia, false);
				applicationEventPublisher.publishEvent(createEvent);
			}
		}));

		// update prebooking
		if (pbjf.getAsnId() != null) {
			Asn asn = asnService.get(pbjf.getAsnId());
			if (asn != null) {
				asn.setJob(j);
				asn.setJobType(j.getType());
				asnService.merge(asn);
			}
		}

		return j;
	}

	public List<Integer> initializeProcsIdsAndServiceTypeIds(List<ItemBasketWrapper> itemsBasketWrapper, Job job,
			List<Integer> calTypeIds, List<Integer> serviceTypeIds) {
		List<Integer> myList = new ArrayList<>();
		for (int i = 0; i < itemsBasketWrapper.size(); i++) {
			if (calTypeIds.get(i) != null) {
				ItemBasketWrapper ibw = itemsBasketWrapper.get(i);

				InstrumentModel instModel = ibw.getInst().getModel();
				CalibrationType calType = calibrationTypeService.find(calTypeIds.get(i));
				ServiceCapability serviceCapability = this.serviceCapabilityService.getCalService(instModel, calType,
						job.getOrganisation());

				serviceTypeIds.add(calType.getServiceType().getServiceTypeId());

				if (serviceCapability != null)
                    myList.add(serviceCapability.getCapability().getId());
				else
					myList.add(null);
			} else
				myList.add(null);

		}
		return myList;
	}

	@Override
	public void initializeJobItemsServiceTypes(AsnItemFinalSynthesisDTO dto, Map<Integer, String> servicesType,
			Integer serviceTypeId) {
		if (dto.getServicetypeid() != null) {
			if (servicesType.containsKey(dto.getServicetypeid()))
				dto.setServiceTypeSource(AsnItemServiceTypeSource.FILE);
		} else {
			PossibleInstrumentDTO poInst = dto.getPossibleInstrument();
			if (poInst != null) {

				if (poInst.getQuotationServiceTypeId() != null
						&& servicesType.containsKey(poInst.getQuotationServiceTypeId())) {
					dto.setServicetypeid(poInst.getQuotationServiceTypeId());
					dto.setServiceTypeSource(AsnItemServiceTypeSource.QUOTATION);
				} else {
					if (poInst.getJiServiceTypeId() != null && servicesType.containsKey(poInst.getJiServiceTypeId())) {
						dto.setServicetypeid(poInst.getJiServiceTypeId());
						dto.setServiceTypeSource(AsnItemServiceTypeSource.JOBITEM);
					} else if (poInst.getInstrumentServiceTypeId() != null
							&& servicesType.containsKey(poInst.getInstrumentServiceTypeId())) {
						dto.setServicetypeid(poInst.getInstrumentServiceTypeId());
						dto.setServiceTypeSource(AsnItemServiceTypeSource.INSTRUMENT);
					} else if (poInst.getInstrumentModelServiceTypeId() != null
							&& servicesType.containsKey(poInst.getInstrumentModelServiceTypeId())) {
						dto.setServicetypeid(poInst.getInstrumentModelServiceTypeId());
						dto.setServiceTypeSource(AsnItemServiceTypeSource.INSTRUMENT_MODEL);
					}
				}
			}
			if (dto.getServiceTypeSource() == null) {
				dto.setServicetypeid(serviceTypeId);
				dto.setServiceTypeSource(AsnItemServiceTypeSource.BUSINESS_COMPANY_SETTING);
			}

		}
	}

	@Override
	public JobItem createNewRepairJobAndJobItem(JobItem oldJobItem, Date frApprovalOn, Contact con) {
		Job newJob = new Job();
		Job oldJob = oldJobItem.getJob();
		newJob.setType(oldJob.getType());
		newJob.setDefaultCalType(oldJob.getDefaultCalType());
		// set contact and address
		newJob.setReturnTo(oldJob.getReturnTo());
		newJob.setReturnToLoc(oldJob.getReturnToLoc());
		newJob.setCon(oldJob.getCon());
		// set basic job info
		newJob.setClientRef(oldJob.getClientRef());
		newJob.setRegDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		newJob.setCreatedBy(con);
		newJob.setOrganisation(oldJob.getOrganisation());
		newJob.setJs((JobStatus) this.statusService.findDefaultStatus(JobStatus.class));
		newJob.setBookedInAddr(oldJob.getBookedInAddr());
		newJob.setBookedInLoc(oldJob.getBookedInLoc());
		newJob.setOverrideDateInOnJobItems(oldJob.getOverrideDateInOnJobItems());

		newJob.setEstCarriageOut(oldJob.getEstCarriageOut());
		newJob.setReturnOption(oldJob.getReturnOption());
		newJob.setInOption(oldJob.getInOption());
		// add BPO links
		if (oldJob.getBpoLinks() != null) {
			newJob.setBpoLinks(new HashSet<>());
			newJob.getBpoLinks().addAll(oldJob.getBpoLinks());
		}

		newJob.setCurrency(oldJob.getCurrency());
		newJob.setRate(oldJob.getRate());

		String jobno = this.numerationService.generateNumber(NumerationType.JOB, newJob.getOrganisation().getComp(),
				newJob.getOrganisation());
		newJob.setJobno(jobno);
		newJob.setInOption(oldJob.getInOption());
		newJob.setReturnOption(oldJob.getReturnOption());
		// Job ReceiptDate initialized with the date and time of the customer's
		// reply on Failure Report
		if (frApprovalOn != null)
			newJob.setReceiptDate(DateTools.dateToZonedDateTime(frApprovalOn));
		else
			newJob.setReceiptDate(DateTools.dateToZonedDateTime(new Date()));
		// add courier data
		if (oldJob.getJobCourier() != null) {
			JobCourierLink jcl = new JobCourierLink();
			jcl.setCourier(oldJob.getJobCourier().getCourier());
			jcl.setJob(newJob);
			jcl.setTrackingNumber(oldJob.getJobCourier().getTrackingNumber());
		}
		newJob.setPackageType(oldJob.getPackageType());
		newJob.setStorageArea(oldJob.getStorageArea());
		this.insertJob(newJob, newJob.getCreatedBy().getSub().getSubdivid());
		JobItem rji = this.jiServ.createNewRepairJobItem(newJob, oldJobItem, con);
		newJob.setItems(new TreeSet<>(new JobItemComparator()));
		newJob.getItems().add(rji);
		this.merge(newJob);
		return rji;
	}

	/**
	 * add instruments to a job already exist using prebooking
	 */
	@Override
	public Job addItemsToJobAlreadyExist(Job job, JobFinalSynthesisDTOForm jobFinalSynthDTOForm, Contact addedBy,
			Integer asnId) {

		PrebookingJobForm pbjf = jobFinalSynthDTOForm.getPbjf();

		// add BPO links
		if (pbjf.getBpo() != null) {
			JobBPOLink bpoLink = new JobBPOLink();
			bpoLink.setBpo(this.bpoServ.get(pbjf.getBpo()));
			bpoLink.setJob(job);
			bpoLink.setDefaultForJob(true);
			bpoLink.setCreatedOn(new Date());
			if (job.getBpoLinks() == null)
				job.setBpoLinks(new HashSet<>());
			job.getBpoLinks().add(bpoLink);
		}

		// POs
		List<String> distinctPoListFromFile = jobFinalSynthDTOForm.getIdentifiedJobItemsFinalSynthDTO().stream()
				.map(AsnItemFinalSynthesisDTO::getPoNumber).filter(StringUtils::isNotBlank).distinct()
				.collect(Collectors.toList());
		// load already created POs
		Asn asn = asnService.get(pbjf.getAsnId());
		List<PO> jobPoList = new ArrayList<>(asn.getPrebookingJobDetails().getPoList());

		// add POs from form
		if (CollectionUtils.isNotEmpty(pbjf.getPoNumberList()))
			pbjf.getPoNumberList().forEach(e -> {
				if (distinctPoListFromFile.stream().noneMatch(s -> s.equals(e)))
					distinctPoListFromFile.add(e);
			});

		pbjf.setPoNumberList(new ArrayList<>());
		pbjf.getPoNumberList().addAll(distinctPoListFromFile);
		// add POs from the form to the list
		for (String poNumber : pbjf.getPoNumberList()) {
			// create the new PO if the it number doesn't already exist
			// jobPoList
			boolean exists = jobPoList.stream().anyMatch(po -> po.getPoNumber().equalsIgnoreCase(poNumber));
			if (!exists) {
				PO po = new PO();
				po.setPoNumber(poNumber);
				po.setActive(true);
				if (pbjf.getPonumber().contains(poNumber))
					po.setComment(pbjf.getPoCommentList().get(pbjf.getPonumber().indexOf(poNumber)));
				if (poNumber.equals(pbjf.getPonumber()))
					po.setDefaultForJob(job);
				po.setOrganisation(job.getOrganisation().getComp());
				jobPoList.add(po);
			}
		}

		if (StringUtils.isNotEmpty(jobFinalSynthDTOForm.getPbjf().getComments())) {
			// add comments if any, as a jobnote
			JobNote jobNote = new JobNote();
			jobNote.setActive(true);
			jobNote.setJob(job);
			jobNote.setPublish(false);
			jobNote.setSetBy(addedBy);
			jobNote.setSetOn(new Date());
			jobNote.setNote(jobFinalSynthDTOForm.getPbjf().getComments());
			if (job.getNotes() == null)
				job.setNotes(new TreeSet<>(new NoteComparator()));
			job.getNotes().add(jobNote);
		}

		// create Job Items
		Set<JobItem> jibitems = this.jiServ.createJobItems(job,
				jobFinalSynthDTOForm.getIdentifiedJobItemsFinalSynthDTO(), addedBy, asnId);

		job.setItems(new TreeSet<>(new JobItemComparator()));
		job.getItems().addAll(jibitems);

		// link POs to the job
		jobPoList.forEach(po -> po.setJob(job));
		job.getPOs().addAll(jobPoList);

		// used to check if the subdiv is linked to adveso
		Company businessCompany = job.getOrganisation().getComp();
		jibitems.forEach(jjii -> jjii.getActions().forEach(jjiia -> {
			jobItemActionService.saveWithoutInterceptor(jjiia);

			// check if the subdiv is linked to adveso
			boolean isLinkedToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
					jjii.getJob().getCon().getSub().getSubdivid(), businessCompany.getCoid(), new Date());
			// adveso event
			if (isLinkedToAdveso) {
				JobItemActionCreateOrUpdateEvent createEvent = new JobItemActionCreateOrUpdateEvent(this, jjiia, false);
				applicationEventPublisher.publishEvent(createEvent);
			}
		}));

		// update prebooking
		if (asnId != null) {
			asn.setJob(job);
			asn.setJobType(job.getType());
			asnService.merge(asn);
		}
		return job;
	}

	@Override
	public Optional<Job> findJobByJobNumber(String jobNo) {
		return jobRepository.findByJobno(jobNo);
	}

	@Override
	public List<ActiveInstrumentDto> findActiveJobsByPlantids(List<String> plantIds) {
		return jobDao.findActiveJobsByPlantids(plantIds);
	}

	@Override
	public String getJobNo(Integer jobId) {
		return get(jobId).getJobno();
	}

	@Override
	public void queryJobJPANew(JobSearchForm jsf, Locale locale, PagedResultSet<JobProjectionDTO> rs) {
		this.jobDao.queryJobJPANew(jsf, rs);
	}
}