package org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.CriteriaQueryGenerator;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.company.entity.country.Country_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.Contract_;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery_;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem_;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.TransportMethod_;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration_;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess_;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink_;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate_;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview_;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost_;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement_;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement_;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobSortKey;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus_;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.po.PO_;
import org.trescal.cwms.core.jobs.jobitem.dto.*;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.db.AbstractJobItemDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItemImportInfoDto;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnote.JobItemNote_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.JobItemTransit;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.JobItemTransit_;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.requirement.Requirement_;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.invoice.entity.costs.calcost.InvoiceCalibrationCost_;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice_;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem_;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem_;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter_;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.Capability_;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization_;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.tools.OptionalsUtils;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.reports.dto.ItemsAtTPWrapper;
import org.trescal.cwms.core.vdi.projection.PositionProjection;
import org.trescal.cwms.core.workallocation.engineerallocation.form.AllocateWorkToEngineerSearchForm;
import org.trescal.cwms.core.workflow.dto.SelectedJobItemDTO;
import org.trescal.cwms.core.workflow.entity.StateGroupType;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation_;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus_;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink_;
import org.trescal.cwms.rest.tlm.dto.TLMJobItemDTO;

import javax.persistence.EntityGraph;
import javax.persistence.Subgraph;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.metamodel.SingularAttribute;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.trescal.cwms.core.tools.StringJpaUtils.trimAndConcatWithWhitespace;

@Repository("JobItemDao")
@Slf4j
public class JobItemDaoImpl extends AbstractJobItemDaoImpl<JobItem> implements JobItemDao {

    private final Clock clock;

    public JobItemDaoImpl(Clock clock) {
        this.clock = clock;
    }

    @Override
    protected Class<JobItem> getEntity() {
        return JobItem.class;
    }

    private Predicate procIdPredicate(CriteriaBuilder cb, Path<JobItem> root, List<Integer> procIds) {

        if (procIds == null)
            return cb.isNull(root.get(JobItem_.capability));
        Path<Capability> procedure = root.get(JobItem_.capability);
        In<Integer> inClause = cb.in(procedure.get(Capability_.id));
        procIds.forEach(inClause::value);
        return inClause;

    }

	private Predicate activeJobItemDaysPredicate(Integer days, CriteriaBuilder cb, Path<LocalDate> dueDate) {
		if (days == null)
			return cb.and();
		return calucalteDaysPredicate(days.toString(), cb, dueDate);
	}

	private Predicate businessRolePredicate(Boolean includeBusiness, CriteriaBuilder cb, Root<JobItem> root) {
		if (includeBusiness == null)
			return cb.and();
		Predicate role = cb.equal(
				root.get(JobItem_.job).get(Job_.con).get(Contact_.sub).get(Subdiv_.comp).get(Company_.companyRole),
				CompanyRole.BUSINESS);
		return includeBusiness ? role : role.not();
	}

	private Predicate groupLinkPredicate(StateGroupLinkType linkType, CriteriaBuilder cb,
			Join<ItemState, StateGroupLink> groupLink) {
		return cb.equal(groupLink.get(StateGroupLink_.type), linkType);
	}

	private Join<ItemState, StateGroupLink> stateGroupLinkJoin(Root<JobItem> root) {
		Join<JobItem, ItemState> state = root.join(JobItem_.state, JoinType.INNER);
		return state.join(ItemState_.groupLinks);
	}

	private Predicate allocatedSubdivPredicate(Subdiv subdiv, CriteriaBuilder cb, Root<JobItem> root,
			Join<ItemState, StateGroupLink> groupLink) {
		return cb.and(cb.equal(root.get(JobItem_.job).get(Job_.organisation), subdiv),
				groupLinkPredicate(StateGroupLinkType.ALLOCATED_SUBDIV, cb, groupLink));
	}

	private Predicate currentSubdivPredicate(Subdiv subdiv, CriteriaBuilder cb, Root<JobItem> root,
			Join<ItemState, StateGroupLink> groupLink) {
		return cb.and(cb.equal(root.get(JobItem_.currentAddr).get(Address_.sub), subdiv),
				groupLinkPredicate(StateGroupLinkType.CURRENT_SUBDIV, cb, groupLink));
	}

	private Predicate stateGroupsPredicate(List<StateGroup> stateGroups, CriteriaBuilder cb, Root<JobItem> root,
			Join<ItemState, StateGroupLink> groupLink) {
		if (stateGroups != null && !stateGroups.isEmpty()) {
			In<Integer> inclause = cb.in(groupLink.get(StateGroupLink_.groupId));
			stateGroups.stream().map(StateGroup::getId).forEach(inclause::value);
			return inclause;
		}
		return cb.isTrue(root.get(JobItem_.state).get(ItemState_.active));
	}

	private CriteriaQueryGenerator<JobItem> buildActiveJobItemsWithProcsQuery(List<Integer> procIds, Integer days,
			Boolean includeBusiness, List<StateGroup> stateGroups, Subdiv subdiv) {

		return cb -> cq -> {
			Root<JobItem> root = cq.from(JobItem.class);
			Join<ItemState, StateGroupLink> groupLink = stateGroupLinkJoin(root);
			Predicate predicates = cb.and(procIdPredicate(cb, root, procIds),
					activeJobItemDaysPredicate(days, cb, root.get(JobItem_.dueDate)),
					businessRolePredicate(includeBusiness, cb, root),
					cb.or(allocatedSubdivPredicate(subdiv, cb, root, groupLink),
							currentSubdivPredicate(subdiv, cb, root, groupLink)),
					stateGroupsPredicate(stateGroups, cb, root, groupLink));
			cq.where(predicates);
			return Triple.of(root, null, null);
		};
	}

	protected CriteriaQueryGenerator<JobItem> buildJobItemByDateCriteriaBuilder(String days, int subdivId) {
		return cb -> cq -> {
			Root<JobItem> root = cq.from(JobItem.class);
			Join<JobItem, ItemState> state = root.join(JobItem_.state);
			// Hack. Because Static schema generator cannot correctly infer type
			// of the
			// organization
			Join<JobItem, Job> job = root.join(JobItem_.job);
			Join<Job, Subdiv> organisation = job.join(Job_.organisation.getName());
			Join<JobItem, Address> address = root.join(JobItem_.currentAddr, JoinType.LEFT);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(state.get(ItemStatus_.active)));
			clauses.getExpressions().add(cb.or(cb.equal(address.get(Address_.sub), subdivId),
					cb.equal(organisation.get(Subdiv_.subdivid), subdivId)));
			cq.where(clauses);
			clauses.getExpressions().add(calucalteDaysPredicate(days, cb, root.get(JobItem_.dueDate)));
			return Triple.of(root, null, null);
		};
	}

	private Predicate calucalteDaysPredicate(String days, CriteriaBuilder cb, Path<LocalDate> dueDate) {
        val currentTime = LocalDate.now(clock.withZone(LocaleContextHolder.getTimeZone().toZoneId()));
		switch (days) {
		case "5":
			return cb.greaterThanOrEqualTo(dueDate, currentTime.plusDays(5));
		case "0":
			return cb.lessThan(dueDate, currentTime);
		case "un":
			return cb.isNull(dueDate);
		default:
			return cb.equal(dueDate, currentTime.plusDays(Integer.parseInt(days)));
		}
	}

	@Override
	public JobItem findActiveJobItemFromBarcode(int plantid) {
		return getFirstResult(cb -> {
			CriteriaQuery<JobItem> cq = cb.createQuery(JobItem.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Instrument> inst = jobItem.join(JobItem_.inst);
			Join<JobItem, ItemState> state = jobItem.join(JobItem_.state);
			Predicate clauses = cb.and(cb.equal(inst.get(Instrument_.plantid), plantid),
					cb.isTrue(state.get(ItemState_.active)));
			cq.where(clauses);
			cq.orderBy(cb.desc(jobItem.get(JobItem_.jobItemId)));
			return cq;
		}).orElse(null);
	}

	@Override
	public Optional<JobItem> findActiveJobItemFromBarcodeOrFormerBarcode(String barcode) {
		if (barcode == null || barcode.isEmpty())
			return Optional.empty();
		return getFirstResult(cb -> {
			val cq = cb.createQuery(JobItem.class);
			val jobitem = cq.from(JobItem.class);
			val inst = jobitem.join(JobItem_.inst);
			jobitem.fetch(JobItem_.inst);
			jobitem.fetch(JobItem_.actions);
			cq.orderBy(cb.desc(jobitem.get(JobItem_.jobItemId)));
			cq.where(
					cb.and(cb
							.or(Stream
									.of(parseInt(barcode).map(id -> cb.equal(inst, id)),
											Optional.of(barcode)
													.map(b -> cb.equal(inst.get(Instrument_.formerBarCode), b)))
									.flatMap(OptionalsUtils::optionalToStream).toArray(Predicate[]::new)),
							cb.isTrue(jobitem.join(JobItem_.state).get(ItemState_.active))));
			return cq;
		});
	}

	private Optional<Integer> parseInt(String input) {
		return Optional.ofNullable(input).filter(NumberTools::isAnInteger).map(Integer::parseInt);
	}

	@Override
	public List<JobItemInvoiceDto> findAllCompleteJobItemsWithoutInvoicesProjection(Integer allocatedCompanyId,
			Integer allocatedSubdivId, Company company, JobSortKey sortBy, String completeJob) {
		if (allocatedCompanyId == null && allocatedSubdivId == null) {
			throw new IllegalArgumentException(
					"At least one of allocatedCompanyId or allocatedSubdivId must be specified, but both were null");
		}

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<JobItemInvoiceDto> cq = cb.createQuery(JobItemInvoiceDto.class);
		Root<JobItem> jobItem = cq.from(JobItem.class);
		Join<JobItem, Job> job = jobItem.join(JobItem_.job);
		Join<Job, Subdiv> allocatedSubdiv = job.join(Job_.organisation.getName()); // No
																					// generics
		Join<JobItem, ItemState> state = jobItem.join(JobItem_.state);
		Join<Job, Contact> contact = job.join(Job_.con);
		Join<Contact, Subdiv> clientSubdiv = contact.join(Contact_.sub);
		Join<Subdiv, Company> clientCompany = clientSubdiv.join(Subdiv_.comp);
		Join<Job, JobStatus> jobStatus = job.join(Job_.js);
		Join<JobItem, JobItemNotInvoiced> notInvoiced = jobItem.join(JobItem_.notInvoiced,
				javax.persistence.criteria.JoinType.LEFT);

		Subquery<Integer> alreadyInvoiced = cq.subquery(Integer.class);
		Root<JobItem> ji = alreadyInvoiced.from(JobItem.class);
		Join<JobItem, InvoiceItem> invItem = ji.join(JobItem_.invoiceItems, javax.persistence.criteria.JoinType.INNER);
		Join<InvoiceItem, Invoice> inv = invItem.join(InvoiceItem_.invoice);
		Join<Invoice, InvoiceType> invType = inv.join(Invoice_.type);
		alreadyInvoiced.select(ji.get(JobItem_.jobItemId));
		alreadyInvoiced.where(cb.notEqual(invType.get(InvoiceType_.name), "Internal"));

		Predicate clauses = cb.conjunction();
		if (allocatedSubdivId != null) {
			// Restrict to allocated subdiv if specified, otherwise business
			// company wide search
			clauses.getExpressions().add(cb.equal(allocatedSubdiv.get(Subdiv_.subdivid), allocatedSubdivId));
		} else {
			Join<Subdiv, Company> allocatedCompany = allocatedSubdiv.join(Subdiv_.comp);
			clauses.getExpressions().add(cb.equal(allocatedCompany.get(Company_.coid), allocatedCompanyId));
		}
		// select only job items with completed job
		if (completeJob.equals("YES")) {
			clauses.getExpressions().add(cb.isTrue(jobStatus.get(JobStatus_.complete)));
			// select only job items with incomplete job
		} else if (completeJob.equals("NO")) {
			clauses.getExpressions().add(cb.isFalse(jobStatus.get(JobStatus_.complete)));
		}
		clauses.getExpressions().add(cb.equal(state.get(ItemState_.active), false));
		clauses.getExpressions().add(cb.not(cb.in(jobItem.get(JobItem_.jobItemId)).value(alreadyInvoiced)));
		clauses.getExpressions().add(cb.isNull(notInvoiced.get(JobItemNotInvoiced_.id)));

		if (company != null)
			clauses.getExpressions().add(cb.equal(clientCompany, company));
		cq.select(cb.construct(JobItemInvoiceDto.class, jobItem.get(JobItem_.jobItemId), jobItem.get(JobItem_.itemNo),
				job.get(Job_.dateComplete), job.get(Job_.jobid), job.get(Job_.jobno), contact.get(Contact_.firstName),
				contact.get(Contact_.lastName), clientCompany.get(Company_.coid), clientCompany.get(Company_.coname),
				clientCompany.get(Company_.companyRole), jobStatus.get(JobStatus_.complete),
				allocatedSubdiv.get(Subdiv_.subname), cb.function("getDeliveryDateFromLastCreatedClientDelivery",
						LocalDate.class, jobItem.get(JobItem_.jobItemId)),
				job.get(Job_.type)));
		cq.where(clauses);
		buildSort(sortBy, cb, cq, jobItem, job, clientSubdiv, clientCompany);
		TypedQuery<JobItemInvoiceDto> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}

	private void buildSort(JobSortKey sortBy, CriteriaBuilder cb, CriteriaQuery<JobItemInvoiceDto> cq,
			Root<JobItem> jobItem, Join<JobItem, Job> job, Join<Contact, Subdiv> clientSubdiv,
			Join<Subdiv, Company> clientCompany) {
		switch (sortBy) {
		case COMPANY:
			cq.orderBy(
					Arrays.asList(cb.asc(clientCompany.get(Company_.coname)), cb.asc(clientSubdiv.get(Subdiv_.subname)),
							cb.asc(job.get(Job_.jobid)), cb.asc(jobItem.get(JobItem_.itemNo))));
			break;
		case DATECOMPLETE:
			cq.orderBy(Arrays.asList(cb.asc(job.get(Job_.dateComplete)), cb.asc(job.get(Job_.jobid)),
					cb.asc(jobItem.get(JobItem_.itemNo))));
			break;
		case JOB:
			cq.orderBy(Arrays.asList(cb.asc(job.get(Job_.jobid)), cb.asc(jobItem.get(JobItem_.itemNo))));
			break;
		default:
			break;
		}
	}

	private Predicate subDivPredicate(Integer allocatedCompanyId, Integer allocatedSubdivId, CriteriaBuilder cb,
			Root<JobItem> root) {
		// Hack due to lack of generic in Metamodel schema
		Join<Job, Subdiv> subdiv = root.join(JobItem_.job).join(Job_.organisation.getName());
		if (allocatedSubdivId != null)
			return cb.equal(subdiv.get(Subdiv_.subdivid), allocatedSubdivId);
		return cb.equal(subdiv.get(Subdiv_.comp).get(Company_.coid), allocatedCompanyId);
	}

	private Predicate restrictCompanyPredicate(Company company, CriteriaBuilder cb, Root<JobItem> root) {
		if (company == null)
			return cb.and();
		Join<JobItem, Job> jobJoin = root.join(JobItem_.job, JoinType.INNER);
		Join<Contact, Subdiv> subJoin = jobJoin.join(Job_.con, JoinType.LEFT).join(Contact_.sub, JoinType.LEFT);
		return cb.equal(subJoin.get(Subdiv_.comp), company);
	}

	private Predicate proformaOrRegularPredicate(Boolean isRegular, CriteriaBuilder cb, Root<JobItem> root) {
		Join<Invoice, InvoiceType> join = root.join(JobItem_.invoiceItems, JoinType.INNER)
				.join(InvoiceItem_.invoice, JoinType.INNER).join(Invoice_.type);
		Path<Boolean> addToAccountPath = join.get(InvoiceType_.addToAccount);
		if (isRegular)
			return cb.and(cb.isTrue(addToAccountPath), cb.notEqual(join.get(InvoiceType_.name), "Internal"));

		return cb.isFalse(addToAccountPath);
	}

	private Predicate notInvoicedisNullPredicate(CriteriaBuilder cb, Root<JobItem> root) {
		return cb.isNull(root.join(JobItem_.notInvoiced, JoinType.LEFT));
	}

	private Subquery<Integer> createJobItemanvoiceCiteriaSubquery(Integer allocatedCompanyId, Integer allocatedSubdivId,
			Company company, Boolean isRegular, CriteriaBuilder cb, CriteriaQuery<JobItemInvoiceDto> cq) {
		Subquery<Integer> sub = cq.subquery(Integer.class);
		if (allocatedCompanyId == null && allocatedSubdivId == null) {
			throw new IllegalArgumentException(
					"At least one of allocatedCompanyId or allocatedSubdivId must be specified, but both were null");
		}
		Root<JobItem> root = sub.from(JobItem.class);
		Predicate clause = cb.and(cb.isFalse(root.join(JobItem_.state).get(ItemState_.active)),
				subDivPredicate(allocatedCompanyId, allocatedSubdivId, cb, root),
				restrictCompanyPredicate(company, cb, root), proformaOrRegularPredicate(isRegular, cb, root),
				notInvoicedisNullPredicate(cb, root));
		sub.where(clause);
		sub.select(root.get(JobItem_.jobItemId));
		return sub;
	}

	public List<JobItemInvoiceDto> findAllCompleteJobItemsWithOnlyProformaInvoicesProjection(Integer allocatedCompanyId,
			Integer allocatedSubdivId, Company company, JobSortKey sortBy, String completeJob) {
		return getResultList(cb -> {
			CriteriaQuery<JobItemInvoiceDto> cq = cb.createQuery(JobItemInvoiceDto.class);
			Root<JobItem> root = cq.from(JobItem.class);
			Join<JobItem, Job> job = root.join(JobItem_.job);
			Join<Job, JobStatus> js = job.join(Job_.js);
			Predicate clauses = cb.and(
					cb.in(root.get(JobItem_.jobItemId))
							.value(createJobItemanvoiceCiteriaSubquery(allocatedCompanyId, allocatedSubdivId, company,
									false, cb, cq)),
					cb.in(root.get(JobItem_.jobItemId)).value(createJobItemanvoiceCiteriaSubquery(allocatedCompanyId,
							allocatedSubdivId, company, true, cb, cq)).not());
			if (completeJob.equals("YES")) {
				clauses.getExpressions().add(cb.isTrue(js.get(JobStatus_.complete)));
			} else if (completeJob.equals("NO")) {
				clauses.getExpressions().add(cb.isFalse(js.get(JobStatus_.complete)));
			}

			cq.where(clauses);
			Join<JobItem, Job> jobJoin = root.join(JobItem_.job);
			// Hack due to lack of support for generic in Metamodel schema
			Join<Job, Subdiv> organizationJoin = jobJoin.join(Job_.organisation.getName());
			Join<Job, Contact> contactJoin = jobJoin.join(Job_.con, JoinType.LEFT);
			Join<Contact, Subdiv> clientSubdivJoin = contactJoin.join(Contact_.sub, JoinType.LEFT);
			Join<Subdiv, Company> companyJoin = clientSubdivJoin.join(Subdiv_.comp, JoinType.LEFT);
			buildSort(sortBy, cb, cq, root, jobJoin, clientSubdivJoin, companyJoin);
			cq.select(cb.construct(JobItemInvoiceDto.class, root.get(JobItem_.jobItemId), root.get(JobItem_.itemNo),
					jobJoin.get(Job_.dateComplete), jobJoin.get(Job_.jobid), jobJoin.get(Job_.jobno),
					contactJoin.get(Contact_.firstName), contactJoin.get(Contact_.lastName),
					companyJoin.get(Company_.coid), companyJoin.get(Company_.coname),
					companyJoin.get(Company_.companyRole), jobJoin.join(Job_.js).get(JobStatus_.complete),
					organizationJoin.get(Subdiv_.subname), root.get(JobItem_.agreedDelDate), jobJoin.get(Job_.type)));
			return cq;
		});
	}

	@Override
	public JobItem findEagerJobItem(int id) {
		EntityGraph<JobItem> eg = getEntityManager().createEntityGraph(JobItem.class);
		eg.addSubgraph(JobItem_.state.getName());
		Subgraph<Job> sg = eg.addSubgraph(JobItem_.job.getName());
		// sg.addSubgraph(Job_.bpoLinks, JobBPOLink.class);
		sg.addSubgraph(Job_.items.getName());

		return getSingleResult(cb -> {
			CriteriaQuery<JobItem> cq = cb.createQuery(JobItem.class);
			Root<JobItem> root = cq.from(JobItem.class);
			cq.where(cb.equal(root.get(JobItem_.jobItemId), id));
			return cq;
		}, eg);
	}

	private void unfoldInstrumentSubgraph(Fetch<?, Instrument> instrumentSq) {
		instrumentSq.fetch(Instrument_.mfr, JoinType.LEFT);
		val modelSq = instrumentSq.fetch(Instrument_.model, JoinType.LEFT);
		modelSq.fetch(InstrumentModel_.mfr, JoinType.LEFT);
		modelSq.fetch(InstrumentModel_.description, JoinType.LEFT);
	}

	@Override
	public JobItem findEagerPopulatedJobitem(int jobitemid) {
		return getSingleResult(cb -> {
			CriteriaQuery<JobItem> cq = cb.createQuery(JobItem.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			cq.where(cb.equal(jobItem.get(JobItem_.jobItemId), jobitemid));
			unfoldInstrumentSubgraph(jobItem.fetch(JobItem_.inst));

			val actionSq = jobItem.fetch(JobItem_.actions, JoinType.LEFT);
			actionSq.fetch(JobItemAction_.startedBy, JoinType.LEFT);
			actionSq.fetch(JobItemAction_.startStatus, JoinType.LEFT);
			actionSq.fetch(JobItemAction_.endStatus, JoinType.LEFT);
			jobItem.fetch(JobItem_.state, JoinType.LEFT);
			jobItem.fetch(JobItem_.images, JoinType.LEFT);
			val reqSq = jobItem.fetch(JobItem_.req, JoinType.LEFT);
			reqSq.fetch(Requirement_.createdBy, JoinType.LEFT);
			val jobSq = jobItem.fetch(JobItem_.job, JoinType.LEFT);
			val itemsSq = jobSq.fetch(Job_.items, JoinType.LEFT);
			unfoldInstrumentSubgraph(itemsSq.fetch(JobItem_.inst, JoinType.LEFT));
			jobSq.fetch(Job_.currency, JoinType.LEFT);
			val faultSq = jobItem.fetch(JobItem_.failureReports, JoinType.LEFT);
            faultSq.fetch(FaultReport_.actions, JoinType.LEFT);
            faultSq.fetch(FaultReport_.descriptions, JoinType.LEFT);
            val reviewItemSq = jobItem.fetch(JobItem_.contactReviewItems, JoinType.LEFT);
            val reviewSq = reviewItemSq.fetch(ContractReviewItem_.review, JoinType.LEFT);
            reviewSq.fetch(ContractReview_.reviewBy, JoinType.LEFT);

            val callinkSq = jobItem.fetch(JobItem_.calLinks, JoinType.LEFT);
            val calSq = callinkSq.fetch(CalLink_.cal, JoinType.LEFT);
            calSq.fetch(Calibration_.calType, JoinType.LEFT);
            calSq.fetch(Calibration_.calProcess, JoinType.LEFT);
            calSq.fetch(Calibration_.startedBy, JoinType.LEFT);
            calSq.fetch(Calibration_.capability, JoinType.LEFT);
            calSq.fetch(Calibration_.status, JoinType.LEFT);

            jobItem.fetch(JobItem_.calibrationCost, JoinType.LEFT);
            jobItem.fetch(JobItem_.adjustmentCost, JoinType.LEFT);
            jobItem.fetch(JobItem_.repairCost, JoinType.LEFT);
            jobItem.fetch(JobItem_.purchaseCost, JoinType.LEFT);
            jobItem.fetch(JobItem_.workRequirements, JoinType.LEFT)
                .fetch(JobItemWorkRequirement_.workRequirement, JoinType.LEFT)
                .fetch(WorkRequirement_.department, JoinType.LEFT);

            return cq;
		});
	}

	private Function<CriteriaBuilder, CriteriaQuery<JobItem>> getJobItemFunction(int plantid) {
		return cb -> {
			CriteriaQuery<JobItem> cq = cb.createQuery(JobItem.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
			cq.where(cb.equal(instrument.get(Instrument_.plantid), plantid));
			List<javax.persistence.criteria.Order> orders = new ArrayList<>();
			orders.add(cb.desc(jobItem.get(JobItem_.dateIn)));
			orders.add(cb.desc(jobItem.get(JobItem_.jobItemId)));
			cq.orderBy(orders);
			return cq;
		};
	}

	@Override
	public JobItem findMostRecentJobItem(int plantid) {
		return getFirstResult(getJobItemFunction(plantid)).orElse(null);
	}

	public Map<Instrument, JobItem> findMostRecentJobItem(List<Integer> plantids) {
		return getResultList(cb -> {
			CriteriaQuery<JobItem> cq = cb.createQuery(JobItem.class);
			Root<JobItem> root = cq.from(JobItem.class);

			Subquery<Integer> sq = cq.subquery(Integer.class);
			Root<JobItem> jobItem = sq.from(JobItem.class);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
			sq.where(instrument.get(Instrument_.plantid).in(plantids));
			sq.groupBy(instrument.get(Instrument_.plantid));
			sq.select(cb.max(jobItem.get(JobItem_.jobItemId)));

			root.fetch(JobItem_.jobCostingItems, JoinType.LEFT);
			root.fetch(JobItem_.calibrationCost, JoinType.LEFT);
			root.fetch(JobItem_.calType, JoinType.LEFT);
			cq.where(root.get(JobItem_.jobItemId).in(sq.getSelection()));
			cq.distinct(true);

			return cq;
		}).stream().collect(Collectors.toMap(JobItem::getInst, i -> i));

	}

	@Override
	public Map<Integer, JobItemProjectionDTO> findMostRecentJobItemDTO(List<Integer> plantids) {

		return getResultList(cb -> {

			CriteriaQuery<JobItemProjectionDTO> cq = cb.createQuery(JobItemProjectionDTO.class);
			Root<JobItem> root = cq.from(JobItem.class);
			Join<JobItem, Job> job = root.join(JobItem_.job, JoinType.INNER);
			Join<Job, SupportedCurrency> currency = job.join(Job_.currency, javax.persistence.criteria.JoinType.INNER);
			Join<JobItem, Instrument> instrumentJoin = root.join(JobItem_.inst);
			Join<JobItem, JobCostingItem> jobCostingItems = root.join(JobItem_.jobCostingItems, JoinType.LEFT);
			Join<JobCostingItem, JobCosting> jobCosting = jobCostingItems.join(JobCostingItem_.jobCosting,
					JoinType.LEFT);
			Join<JobItem, CalibrationType> calType = root.join(JobItem_.calType, JoinType.LEFT);
			Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType, JoinType.LEFT);
			Subquery<Integer> sq = cq.subquery(Integer.class);
			Root<JobItem> jobItem = sq.from(JobItem.class);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
			sq.where(instrument.get(Instrument_.plantid).in(plantids));
			sq.groupBy(instrument.get(Instrument_.plantid));
			sq.select(cb.max(jobItem.get(JobItem_.jobItemId)));

			cq.where(root.get(JobItem_.jobItemId).in(sq.getSelection()));

			cq.select(cb.construct(JobItemProjectionDTO.class, root.get(JobItem_.jobItemId), job.get(Job_.jobid),
					root.get(JobItem_.itemNo), instrumentJoin.get(Instrument_.plantid),
					serviceType.get(ServiceType_.serviceTypeId), jobCostingItems.get(JobCostingItem_.id),
					jobCostingItems.get(JobCostingItem_.itemno), jobCostingItems.get(JobCostingItem_.totalCost),
					jobCostingItems.get(JobCostingItem_.finalCost), jobCosting.get(JobCosting_.id),
					jobCosting.get(JobCosting_.ver), currency.get(SupportedCurrency_.currencyId)));

			cq.distinct(true);
			return cq;

		}).stream().collect(Collectors.toMap(JobItemProjectionDTO::getPlantid, i -> i, (i1, i2) -> {
			log.debug(">>>>>duplicated key>>>>>" + i1);
			return i1;
		}));
	}

	@Override
	public List<JobItemForInstrumentTooltipDto> findJobItemsForInstrumentTooltipByPlantId(Integer plantId) {
		return getResultList(cb -> {
			val cq = cb.createQuery(JobItemForInstrumentTooltipDto.class);
			val jobItem = cq.from(JobItem.class);
			val job = jobItem.join(JobItem_.job);
			val serviceType = jobItem.join(JobItem_.calType, JoinType.LEFT).join(CalibrationType_.serviceType,
					JoinType.LEFT);
			val calibrationCost = jobItem.join(JobItem_.calibrationCost, JoinType.LEFT);

			cq.where(cb.equal(jobItem.get(JobItem_.inst), plantId));

			cq.select(cb.construct(JobItemForInstrumentTooltipDto.class, job.get(Job_.jobno), job.get(Job_.jobid),
					jobItem.get(JobItem_.itemNo), jobItem.get(JobItem_.jobItemId),
					joinTranslation(cb, serviceType, ServiceType_.shortnameTranslation,
							LocaleContextHolder.getLocale()),
					jobItem.get(JobItem_.turn), jobItem.get(JobItem_.dateIn), jobItem.get(JobItem_.dateComplete),
					calibrationCost.get(ContractReviewCalibrationCost_.finalCost), jobCostingIdSubquery(cb, cq, job),
					job.join(Job_.currency).get(SupportedCurrency_.currencyCode),
					jobCostingItemCostSubquery(cb, cq, jobItem, JobCostingItem_.calibrationCost),
					invoiceItemCalibrationCostSubquery(cb, cq, jobItem),
					jobCostingItemCostSubquery(cb, cq, jobItem, JobCostingItem_.repairCost)));
			return cq;
		});
	}

	private Subquery<BigDecimal> invoiceItemCalibrationCostSubquery(CriteriaBuilder cb,
			CriteriaQuery<JobItemForInstrumentTooltipDto> cq, From<?, JobItem> jobItem) {
		val sq = cq.subquery(BigDecimal.class);
		val invoiceItem = sq.from(InvoiceItem.class);
		sq.where(cb.equal(invoiceItem.get(InvoiceItem_.jobItem), jobItem),
				cb.equal(invoiceItem.join(InvoiceItem_.invoice).join(Invoice_.type).get(InvoiceType_.name),
						"Calibration"),
				cb.equal(invoiceItem, invoiceItemMaxIdForJobItemAndTypeCalibrationSubquery(cb, cq, jobItem)));
		sq.select(invoiceItem.join(InvoiceItem_.calibrationCost).get(InvoiceCalibrationCost_.finalCost));
		return sq;

	}

	private Subquery<Integer> invoiceItemMaxIdForJobItemAndTypeCalibrationSubquery(CriteriaBuilder cb,
			CriteriaQuery<JobItemForInstrumentTooltipDto> cq, From<?, JobItem> jobItem) {
		val sq = cq.subquery(Integer.class);
		val invoiceItem = sq.from(InvoiceItem.class);
		sq.select(cb.max(invoiceItem.get(InvoiceItem_.id)));
		sq.where(cb.equal(invoiceItem.get(InvoiceItem_.jobItem), jobItem), cb.equal(
				invoiceItem.join(InvoiceItem_.invoice).join(Invoice_.type).get(InvoiceType_.name), "Calibration"));
		return sq;
	}

	private Subquery<BigDecimal> jobCostingItemCostSubquery(CriteriaBuilder cb,
			CriteriaQuery<JobItemForInstrumentTooltipDto> cq, From<?, JobItem> jobItem,
			SingularAttribute<JobCostingItem, ? extends Cost> cost) {
		val sq = cq.subquery(BigDecimal.class);
		val jobCostingItem = sq.from(JobCostingItem.class);
		sq.where(cb.equal(jobCostingItem.get(JobCostingItem_.jobItem), jobItem),
				cb.equal(jobCostingItem, jobCostingItemMaxIdForJobItem(cb, cq, jobItem)));
		sq.select(jobCostingItem.join(cost, JoinType.LEFT).get(JobCostingRepairCost_.finalCost));
		return sq;
	}

	private Subquery<Integer> jobCostingItemMaxIdForJobItem(CriteriaBuilder cb,
			CriteriaQuery<JobItemForInstrumentTooltipDto> cq, From<?, JobItem> jobItem) {
		val sq = cq.subquery(Integer.class);
		val jobCostingItem = sq.from(JobCostingItem.class);
		sq.select(cb.max(jobCostingItem.get(JobCostingItem_.id)));
		sq.where(cb.equal(jobCostingItem.get(JobCostingItem_.jobItem), jobItem));
		return sq;
	}

	private Subquery<Integer> jobCostingIdSubquery(CriteriaBuilder cb, CriteriaQuery<JobItemForInstrumentTooltipDto> cq,
			From<?, Job> job) {
		val sq = cq.subquery(Integer.class);
		val jobCosting = sq.from(JobCosting.class);
		sq.where(cb.equal(jobCosting.get(JobCosting_.job), job));
		sq.select(cb.max(jobCosting.get(JobCosting_.id)));
		return sq;
	}

	@Override
	public List<JobItem> findMostRecentJobItems(Integer plantid, Integer maxResults) {
		if (plantid == null)
			throw new IllegalArgumentException("plantid must be specified");
		if (maxResults == null)
			throw new IllegalArgumentException("maxResults must be specified");
		return getResultList(getJobItemFunction(plantid), 0, maxResults);
	}

	@Override
	public List<SelectedJobItemDTO> getActiveJobItems(List<Integer> procIds, List<Integer> stateIds,
			Boolean isAllocatedToEngineer, Subdiv subdiv, StateGroupLinkType sglType, Integer addressId) {
		return getActiveJobItems(procIds, stateIds, null, null, false, subdiv, sglType, addressId);
	}

	private List<SelectedJobItemDTO> getActiveJobItems(List<Integer> procIds, List<Integer> stateIds, Integer days,
			Boolean includeBusiness, Boolean isAllocatedToEngineer, Subdiv subdiv, StateGroupLinkType sglType,
			Integer addressId) {
		Locale locale = LocaleContextHolder.getLocale();
		return getResultList(cb -> {
			CriteriaQuery<SelectedJobItemDTO> cq = cb.createQuery(SelectedJobItemDTO.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			Join<Job, Subdiv> allocatedSubdiv = job.join("organisation");
			Join<Job, JobStatus> jobStatus = job.join(Job_.js, javax.persistence.criteria.JoinType.LEFT);
			Join<JobStatus, Translation> jobStatusName = jobStatus.join(JobStatus_.nametranslations,
					javax.persistence.criteria.JoinType.LEFT);
			jobStatusName.on(cb.equal(jobStatusName.get(Translation_.locale), locale));
            Join<Job, Contact> clientContact = job.join(Job_.con, javax.persistence.criteria.JoinType.LEFT);
            Join<Contact, Subdiv> clientSubdiv = clientContact.join(Contact_.sub);
            Join<Subdiv, Company> clientCompany = clientSubdiv.join(Subdiv_.comp);
            Join<Job, TransportOption> transportOutOption = job.join(Job_.returnOption,
                javax.persistence.criteria.JoinType.LEFT);
            Join<TransportOption, TransportMethod> transportOutMethod = transportOutOption.join(TransportOption_.method,
                javax.persistence.criteria.JoinType.LEFT);
            Join<TransportMethod, Translation> transportOutName = transportOutMethod
                .join(TransportMethod_.methodTranslation, javax.persistence.criteria.JoinType.LEFT);
            transportOutName.on(cb.equal(transportOutName.get(Translation_.locale), locale));
            Join<TransportOption, Subdiv> transportOutSubdiv = transportOutOption.join(TransportOption_.sub,
                javax.persistence.criteria.JoinType.LEFT);
            Join<JobItem, Capability> procedure = jobItem.join(JobItem_.capability, javax.persistence.criteria.JoinType.LEFT);
            Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst,
                javax.persistence.criteria.JoinType.LEFT);
            Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model);
            Join<InstrumentModel, Translation> modelName = model.join(InstrumentModel_.nameTranslations,
                javax.persistence.criteria.JoinType.LEFT);
            modelName.on(cb.equal(modelName.get(Translation_.locale), locale));
            Join<JobItem, CalibrationType> calType = jobItem.join(JobItem_.calType,
                javax.persistence.criteria.JoinType.LEFT);
            Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType,
                javax.persistence.criteria.JoinType.LEFT);
            Join<ServiceType, Translation> serviceTypeShortName = serviceType.join(ServiceType_.shortnameTranslation,
                javax.persistence.criteria.JoinType.LEFT);
			serviceTypeShortName.on(cb.equal(serviceTypeShortName.get(Translation_.locale), locale));
			Join<JobItem, Contact> contractReviewBy = jobItem.join(JobItem_.lastContractReviewBy,
					javax.persistence.criteria.JoinType.LEFT);
			Join<JobItem, ItemState> itemState = jobItem.join(JobItem_.state, javax.persistence.criteria.JoinType.LEFT);
			Expression<String> itemStateName = joinTranslation(cb, itemState, ItemState_.translations, locale);
			Join<ItemState, StateGroupLink> stateGroupLink = itemState.join(ItemState_.groupLinks,
					javax.persistence.criteria.JoinType.LEFT);
			Join<JobItem, JobItemAction> action = jobItem.join(JobItem_.actions,
					javax.persistence.criteria.JoinType.LEFT);
			Subquery<Integer> actionSubQuery = cq.subquery(Integer.class);
			Root<JobItemAction> sqActions = actionSubQuery.from(JobItemAction.class);
			actionSubQuery.where(cb.equal(sqActions.get(JobItemAction_.jobItem), jobItem));
			actionSubQuery.select(cb.max(sqActions.get(JobItemAction_.id)));
			action.on(cb.equal(action.get(JobItemAction_.id), actionSubQuery));
			Join<JobItem, Address> currentAddress = jobItem.join(JobItem_.currentAddr,
					javax.persistence.criteria.JoinType.LEFT);
			Join<JobItem, Address> calAddress = jobItem.join(JobItem_.calAddr,
					javax.persistence.criteria.JoinType.LEFT);
			Subquery<Long> jiCountSq = cq.subquery(Long.class);
			Root<JobItem> jiCount = jiCountSq.from(JobItem.class);
			jiCountSq.where(cb.equal(jiCount.get(JobItem_.job), job));
            jiCountSq.select(cb.count(jiCount));
            cq.select(cb.construct(SelectedJobItemDTO.class, jobItem.get(JobItem_.jobItemId),
                jobItem.get(JobItem_.itemNo), jiCountSq.getSelection(), jobItem.get(JobItem_.dueDate),
                jobItem.get(JobItem_.turn), job.get(Job_.jobid), job.get(Job_.jobno),
                jobStatusName.get(Translation_.translation), job.get(Job_.regDate), job.get(Job_.type),
                clientContact.get(Contact_.personid), clientContact.get(Contact_.firstName),
                clientContact.get(Contact_.lastName), clientContact.get(Contact_.active),
                clientSubdiv.get(Subdiv_.subdivid), clientSubdiv.get(Subdiv_.subname),
                clientSubdiv.get(Subdiv_.active), clientCompany.get(Company_.coid),
                clientCompany.get(Company_.coname), transportOutOption.get(TransportOption_.id),
                transportOutName.get(Translation_.translation), transportOutSubdiv.get(Subdiv_.subdivCode),
                procedure.get(Capability_.id), procedure.get(Capability_.reference), procedure.get(Capability_.name),
                instrument.get(Instrument_.plantid), instrument.get(Instrument_.plantno),
                instrument.get(Instrument_.serialno), model.get(InstrumentModel_.modelid),
                modelName.get(Translation_.translation), serviceTypeShortName.get(Translation_.translation),
                serviceType.get(ServiceType_.displayColour), serviceType.get(ServiceType_.displayColourFastTrack),
                contractReviewBy.get(Contact_.personid), contractReviewBy.get(Contact_.firstName),
                contractReviewBy.get(Contact_.lastName), itemState.get(ItemState_.stateid), itemStateName,
                action.get(JobItemAction_.endStamp), action.get(JobItemAction_.remark),
                action.get(JobItemAction_.id), allocatedSubdiv.get(Subdiv_.subdivid),
                allocatedSubdiv.get(Subdiv_.subdivCode)));
            cq.distinct(true);
			Predicate clauses = cb.conjunction();
			Predicate allocatedSubdivPredicate = cb.equal(allocatedSubdiv, subdiv);
			Predicate currentSubdivPredicate = cb.equal(currentAddress.get(Address_.sub), subdiv);
			Predicate allocatedNotCurrentSubdivPredicate = cb.and(cb.equal(allocatedSubdiv, subdiv),
					cb.notEqual(currentAddress.get(Address_.sub), subdiv));
			Predicate calibrationSubdivPredicate = cb.disjunction();
			if (stateIds != null) {
				List<Integer> depGroups = StateGroup.getAllStateGroupsOfType(StateGroupType.DEPARTMENT).stream()
						.map(StateGroup::getId).collect(Collectors.toList());
				clauses.getExpressions().add(stateGroupLink.get(StateGroupLink_.groupId).in(depGroups));
				allocatedSubdivPredicate = cb.and(allocatedSubdivPredicate,
						cb.equal(stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.ALLOCATED_SUBDIV));
				currentSubdivPredicate = cb.and(currentSubdivPredicate,
						cb.equal(stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.CURRENT_SUBDIV));
				allocatedNotCurrentSubdivPredicate = cb.and(allocatedNotCurrentSubdivPredicate, cb.equal(
						stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.ALLOCATED_NOTCURRENT_SUBDIV));
				calibrationSubdivPredicate = cb.and(cb.equal(calAddress.get(Address_.sub), subdiv),
						cb.equal(stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.CALIBRATION_SUBDIV));
			}
			if (sglType == null) {
				Predicate sglDisjunction = cb.disjunction();
				sglDisjunction.getExpressions().add(allocatedSubdivPredicate);
				sglDisjunction.getExpressions().add(currentSubdivPredicate);
				sglDisjunction.getExpressions().add(allocatedNotCurrentSubdivPredicate);
				sglDisjunction.getExpressions().add(calibrationSubdivPredicate);
				clauses.getExpressions().add(sglDisjunction);
			} else if (sglType == StateGroupLinkType.ALLOCATED_SUBDIV) {
				clauses.getExpressions().add(allocatedSubdivPredicate);
			} else if (sglType == StateGroupLinkType.CURRENT_SUBDIV) {
				clauses.getExpressions().add(currentSubdivPredicate);
			} else if (sglType == StateGroupLinkType.ALLOCATED_NOTCURRENT_SUBDIV) {
				clauses.getExpressions().add(allocatedNotCurrentSubdivPredicate);
			} else if (sglType == StateGroupLinkType.CALIBRATION_SUBDIV) {
				clauses.getExpressions().add(calibrationSubdivPredicate);
			}
			if (procIds != null)
				if (procIds.isEmpty())
                    clauses.getExpressions().add(cb.isNull(procedure.get(Capability_.id)));
				else
                    clauses.getExpressions().add(procedure.get(Capability_.id).in(procIds));
			if (stateIds != null) {
				if (stateIds.isEmpty())
					clauses.getExpressions().add(cb.isNull(itemState.get(ItemState_.stateid)));
				else
					clauses.getExpressions().add(itemState.get(ItemState_.stateid).in(stateIds));
			} else
				clauses.getExpressions().add(cb.isTrue(itemState.get(ItemState_.active)));
			if (days != null) {
				if (days <= 0) {
                    LocalDate localDate = LocalDate.now(clock.withZone(LocaleContextHolder.getTimeZone().toZoneId()));
					clauses.getExpressions().add(cb.lessThanOrEqualTo(jobItem.get(JobItem_.dueDate), localDate));
				} else if (days >= 5) {
                    LocalDate localDate = LocalDate.now(clock.withZone(LocaleContextHolder.getTimeZone().toZoneId())).plusDays(5);
					clauses.getExpressions().add(cb.greaterThanOrEqualTo(jobItem.get(JobItem_.dueDate), localDate));
				} else {
                    LocalDate localDate = LocalDate.now(clock.withZone(LocaleContextHolder.getTimeZone().toZoneId()))
                            .plusDays(days);
                    Date date = DateTools.dateFromLocalDate(localDate);
                    clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.dueDate), date));
                }
			}
			if (includeBusiness != null) {
				if (includeBusiness)
					clauses.getExpressions()
							.add(cb.equal(clientCompany.get(Company_.companyRole), CompanyRole.BUSINESS));
				else
					clauses.getExpressions()
							.add(cb.notEqual(clientCompany.get(Company_.companyRole), CompanyRole.BUSINESS));
			}
			if (isAllocatedToEngineer != null) {
				Subquery<Long> engineerAllocationSq = cq.subquery(Long.class);
				Root<EngineerAllocation> engineerAllocation = engineerAllocationSq.from(EngineerAllocation.class);
				Join<EngineerAllocation, JobItem> allocatedItem = engineerAllocation
						.join(EngineerAllocation_.itemAllocated);
				engineerAllocationSq.where(cb.equal(allocatedItem, jobItem));
				engineerAllocationSq.select(cb.count(engineerAllocation));
				if (isAllocatedToEngineer)
					clauses.getExpressions().add(cb.notEqual(engineerAllocationSq, 0L));
				else
					clauses.getExpressions().add(cb.equal(engineerAllocationSq, 0L));
			}
			if (addressId != null)
				clauses.getExpressions().add(cb.equal(currentAddress.get(Address_.addrid), addressId));
			cq.where(clauses);
			cq.orderBy(cb.asc(job.get(Job_.jobno)), cb.asc(jobItem.get(JobItem_.itemNo)));
			return cq;
		});
	}

	@Override
	public List<JobItem> getAllItems(List<Integer> ids) {
		return ids == null || ids.isEmpty() ? new ArrayList<>() : getResultList(cb -> {
			CriteriaQuery<JobItem> cq = cb.createQuery(JobItem.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			cq.where(jobItem.get(JobItem_.jobItemId).in(ids));
			return cq;
		});
	}

	@Override
	public List<JobItem> getAllByState(ItemState state, Subdiv subdiv) {
		return getResultList(cb -> {
			CriteriaQuery<JobItem> cq = cb.createQuery(JobItem.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Address> currentAddress = jobItem.join(JobItem_.currentAddr);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.state), state));
			clauses.getExpressions().add(cb.equal(currentAddress.get(Address_.sub), subdiv));
			cq.where(clauses);
			cq.distinct(true);
			return cq;
		});
	}

	@Override
	public List<JobItemOnClientPurchaseOrderDTO> getJobItemsOnClientPO(Integer poId, Integer jobId, Locale locale) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<JobItemOnClientPurchaseOrderDTO> query = cb.createQuery(JobItemOnClientPurchaseOrderDTO.class);
		Root<JobItem> jobItem = query.from(JobItem.class);
		Path<Job> job = jobItem.get(JobItem_.job);
		Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
		Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model);
		Join<InstrumentModel, Translation> modelName = model.join(InstrumentModel_.nameTranslations,
				javax.persistence.criteria.JoinType.LEFT);
		modelName.on(cb.equal(modelName.get(Translation_.locale), locale));
		Join<JobItem, CalibrationType> calType = jobItem.join(JobItem_.calType,
				javax.persistence.criteria.JoinType.LEFT);
		Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType);
		Join<ServiceType, Translation> serviceTypeName = serviceType.join(ServiceType_.shortnameTranslation);
		serviceTypeName.on(cb.equal(serviceTypeName.get(Translation_.locale), locale));
		Join<ServiceType, Translation> serviceTypeLongName = serviceType.join(ServiceType_.longnameTranslation);
		serviceTypeLongName.on(cb.equal(serviceTypeLongName.get(Translation_.locale), locale));
		Subquery<Long> sq = query.subquery(Long.class);
		Root<JobItemPO> sqJiPo = sq.from(JobItemPO.class);
		Join<JobItemPO, PO> sqpo = sqJiPo.join(JobItemPO_.po);
		Predicate sqClauses = cb.conjunction();
		sqClauses.getExpressions().add(cb.equal(sqpo.get(PO_.poId), poId));
		sqClauses.getExpressions().add(cb.equal(sqJiPo.get(JobItemPO_.item), jobItem));
		sq.where(sqClauses);
		sq.select(cb.count(sqJiPo));
		Subquery<Long> sq2 = query.subquery(Long.class);
		Root<JobItemPO> sq2po = sq2.from(JobItemPO.class);
		sq2.where(cb.equal(sq2po.get(JobItemPO_.item), jobItem));
		sq2.select(cb.count(sq2po));
		query.where(cb.equal(job.get(Job_.jobid), jobId));
		query.select(cb.construct(JobItemOnClientPurchaseOrderDTO.class, jobItem.get(JobItem_.jobItemId),
				jobItem.get(JobItem_.itemNo), modelName.get(Translation_.translation),
				instrument.get(Instrument_.serialno), instrument.get(Instrument_.plantno),
				serviceTypeName.get(Translation_.translation), serviceTypeLongName.get(Translation_.translation),
				serviceType.get(ServiceType_.displayColour), sq.getSelection(), sq2.getSelection()));
		TypedQuery<JobItemOnClientPurchaseOrderDTO> tq = getEntityManager().createQuery(query);
		return tq.getResultList();
	}

	@Override
	public List<JobItemOnClientPurchaseOrderDTO> getJobItemsOnClientBPO(Integer poId, Integer jobId, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<JobItemOnClientPurchaseOrderDTO> query = cb
					.createQuery(JobItemOnClientPurchaseOrderDTO.class);
			Root<JobItem> jobItem = query.from(JobItem.class);
			Path<Job> job = jobItem.get(JobItem_.job);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
			Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model);
			Join<InstrumentModel, Translation> modelName = model.join(InstrumentModel_.nameTranslations,
					javax.persistence.criteria.JoinType.LEFT);
			modelName.on(cb.equal(modelName.get(Translation_.locale), locale));
			Join<JobItem, CalibrationType> calType = jobItem.join(JobItem_.calType,
					javax.persistence.criteria.JoinType.LEFT);
			Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType);
			Join<ServiceType, Translation> serviceTypeName = serviceType.join(ServiceType_.shortnameTranslation);
			serviceTypeName.on(cb.equal(serviceTypeName.get(Translation_.locale), locale));
			Join<ServiceType, Translation> serviceTypeLongName = serviceType.join(ServiceType_.longnameTranslation);
			serviceTypeLongName.on(cb.equal(serviceTypeLongName.get(Translation_.locale), locale));
			Subquery<Long> sq = query.subquery(Long.class);
			Root<JobItemPO> sqJiBpo = sq.from(JobItemPO.class);
			Join<JobItemPO, BPO> sqbpo = sqJiBpo.join(JobItemPO_.bpo);
			Predicate sqClauses = cb.conjunction();
			sqClauses.getExpressions().add(cb.equal(sqbpo.get(BPO_.poId), poId));
			sqClauses.getExpressions().add(cb.equal(sqJiBpo.get(JobItemPO_.item), jobItem));
			sq.where(sqClauses);
			sq.select(cb.count(sqJiBpo));
			Subquery<Long> sq2 = query.subquery(Long.class);
			Root<JobItemPO> sq2po = sq2.from(JobItemPO.class);
			sq2.where(cb.equal(sq2po.get(JobItemPO_.item), jobItem));
			sq2.select(cb.count(sq2po));
			query.where(cb.equal(job.get(Job_.jobid), jobId));
			query.orderBy(cb.asc(jobItem.get(JobItem_.jobItemId)));
			query.select(cb.construct(JobItemOnClientPurchaseOrderDTO.class, jobItem.get(JobItem_.jobItemId),
					jobItem.get(JobItem_.itemNo), modelName.get(Translation_.translation),
					instrument.get(Instrument_.serialno), instrument.get(Instrument_.plantno),
					serviceTypeName.get(Translation_.translation), serviceTypeLongName.get(Translation_.translation),
					serviceType.get(ServiceType_.displayColour), sq.getSelection(), sq2.getSelection()));

			return query;
		});
	}

	public List<JobItemFeedbackDto> getAllJobItems(int jobId, Locale mainLocale, Locale fallbackLocale) {
		return getResultList(cb -> {
			val cq = cb.createQuery(JobItemFeedbackDto.class);
			val jobItem = cq.from(JobItem.class);
			val job = jobItem.join(JobItem_.job);
			val instrument = jobItem.join(JobItem_.inst);
			val model = instrument.join(Instrument_.model);

			val mainModelName = model.join(InstrumentModel_.nameTranslations, JoinType.LEFT);
			mainModelName.on(cb.equal(mainModelName.get(Translation_.locale), mainLocale));
			val fallbackModelName = model.join(InstrumentModel_.nameTranslations, JoinType.LEFT);
			fallbackModelName.on(cb.equal(fallbackModelName.get(Translation_.locale), fallbackLocale));

			val calType = jobItem.join(JobItem_.calType, JoinType.LEFT);
			val serviceType = calType.join(CalibrationType_.serviceType, JoinType.LEFT);
			val mainCalName = serviceType.join(ServiceType_.shortnameTranslation);
			mainCalName.on(cb.equal(mainCalName.get(Translation_.locale), mainLocale));
			val fallbackCalName = serviceType.join(ServiceType_.shortnameTranslation);
			fallbackCalName.on(cb.equal(fallbackCalName.get(Translation_.locale), fallbackLocale));

			val contact = job.join(Job_.con);
			val fullName = trimAndConcatWithWhitespace(contact.get(Contact_.firstName), contact.get(Contact_.lastName))
					.apply(cb);
			cq.where(cb.equal(job, jobId));
			cq.select(cb.construct(JobItemFeedbackDto.class, jobItem.get(JobItem_.jobItemId),
					jobItem.get(JobItem_.itemNo), instrument.get(Instrument_.serialno),
					instrument.get(Instrument_.plantno), serviceType.get(ServiceType_.displayColour), fullName,
					contact.get(Contact_.email), contact.get(Contact_.personid),
					cb.selectCase().when(cb.isNotNull(mainModelName), mainModelName.get(Translation_.translation))
							.otherwise(fallbackModelName.get(Translation_.translation)),
					cb.selectCase().when(cb.isNotNull(mainCalName), mainCalName.get(Translation_.translation))
							.otherwise(fallbackCalName.get(Translation_.translation))));
			return cq;
		});
	}

	@Override
	public List<JobItem> getAllJobItemsDWR(int jobid) {
		return getResultList(cb -> {
			val cq = cb.createQuery(JobItem.class);
			val jobItem = cq.from(JobItem.class);
			val job = jobItem.fetch(JobItem_.job);
			jobItem.fetch(JobItem_.state, JoinType.LEFT);
			cq.orderBy(cb.asc(jobItem.get(JobItem_.itemNo)));
			cq.where(cb.equal(jobItem.get(JobItem_.job), jobid));
			val con = job.fetch(Job_.con, JoinType.LEFT);
			con.fetch(Contact_.user);
			con.fetch(Contact_.userPreferences, JoinType.LEFT);
			val inst = jobItem.fetch(JobItem_.inst);
			inst.fetch(Instrument_.mfr, JoinType.LEFT);
			val instModel = inst.fetch(Instrument_.model);
			instModel.fetch(InstrumentModel_.mfr, JoinType.LEFT);
			instModel.fetch(InstrumentModel_.description, JoinType.LEFT);
			jobItem.fetch(JobItem_.calType, JoinType.LEFT);
			jobItem.fetch(JobItem_.itemPOs, JoinType.LEFT).fetch(JobItemPO_.po, JoinType.LEFT);
			cq.distinct(true);
			return cq;
		});
	}

	@Override
	public List<JobItem> getAvailableTaskList(Contact contact) {
		return getResultList(cb -> {
            CriteriaQuery<JobItem> cq = cb.createQuery(JobItem.class);
            Root<JobItem> jobItem = cq.from(JobItem.class);
            Join<JobItem, Job> job = jobItem.join(JobItem_.job);
            Join<JobItem, Capability> procedure = jobItem.join(JobItem_.capability);
            Join<Capability, CapabilityAuthorization> authorization = procedure.join(Capability_.authorizations);
            Join<CapabilityAuthorization, CalibrationType> procCalType = authorization
                .join(CapabilityAuthorization_.caltype);
            Join<JobItem, ItemState> state = jobItem.join(JobItem_.state);
            Join<ItemState, StateGroupLink> stateGroupLink = state.join(ItemState_.groupLinks);
            Join<JobItem, CalibrationType> itemCalType = jobItem.join(JobItem_.calType);
            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.equal(procCalType, itemCalType));
            clauses.getExpressions().add(cb.or(
                cb.equal(stateGroupLink.get(StateGroupLink_.groupId), StateGroup.AWAITINGCALIBRATION.getId()),
                cb.equal(stateGroupLink.get(StateGroupLink_.groupId), StateGroup.RESUMECALIBRATION.getId())));
            clauses.getExpressions()
                .add(cb.equal(authorization.get(CapabilityAuthorization_.authorizationFor), contact));
            clauses.getExpressions().add(
                cb.notEqual(authorization.get(CapabilityAuthorization_.accredLevel), AccreditationLevel.REMOVED));
            cq.where(clauses);
            cq.orderBy(cb.asc(jobItem.get(JobItem_.dueDate)), cb.asc(job.get(Job_.jobno)),
                cb.asc(jobItem.get(JobItem_.itemNo)));
            return cq;
        });
	}

	@Override
	public List<JobItem> getContactItemsOnJobInStateGroup(int personId, StateGroup group) {
		return getResultList(cb -> {
			val cq = cb.createQuery(JobItem.class);
			val jobItem = cq.from(JobItem.class);
			val job = jobItem.join(JobItem_.job);
			val itemState = jobItem.join(JobItem_.state);
			val groupLinks = itemState.join(ItemState_.groupLinks, JoinType.LEFT);
			cq.where(cb.and(cb.equal(job.get(Job_.con), personId),
					cb.equal(groupLinks.get(StateGroupLink_.groupId), group.getId())));
			cq.orderBy(cb.asc(job.get(Job_.jobno)), cb.asc(jobItem.get(JobItem_.itemNo)));
			return cq;
		});
	}

	@Override
	public Integer getCountActiveJobItemsWithProcs(List<Integer> procIds, Integer days, Boolean includeBusiness,
			List<StateGroup> stateGroups, Subdiv subdiv) {
		return getCount(buildActiveJobItemsWithProcsQuery(procIds, days, includeBusiness, stateGroups, subdiv));
	}

	@Override
	public Long getCountAllJobItemsForState(ItemState state, Subdiv subdiv, StateGroupLinkType groupType) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.state), state));
			if (groupType == StateGroupLinkType.ALLOCATED_COMPANY || groupType == StateGroupLinkType.ALLOCATED_SUBDIV
					|| groupType == StateGroupLinkType.ALLOCATED_NOTCURRENT_SUBDIV) {
				Join<JobItem, Job> job = jobItem.join(JobItem_.job);
				if (groupType == StateGroupLinkType.ALLOCATED_COMPANY) {
					Join<Job, Subdiv> allocatedSubdiv = job.join(Job_.organisation.getName());
					clauses.getExpressions().add(cb.equal(allocatedSubdiv.get(Subdiv_.comp), subdiv.getComp()));
				} else
					clauses.getExpressions().add(cb.equal(job.get(Job_.organisation), subdiv));
			}
			if (groupType == StateGroupLinkType.CURRENT_COMPANY || groupType == StateGroupLinkType.CURRENT_SUBDIV
					|| groupType == StateGroupLinkType.ALLOCATED_NOTCURRENT_SUBDIV) {
				Join<JobItem, Address> currentAddress = jobItem.join(JobItem_.currentAddr,
						javax.persistence.criteria.JoinType.LEFT);
				if (groupType == StateGroupLinkType.CURRENT_COMPANY) {
					Join<Address, Subdiv> currentSubdiv = currentAddress.join(Address_.sub);
					clauses.getExpressions().add(cb.equal(currentSubdiv.get(Subdiv_.comp), subdiv.getComp()));
				} else if (groupType == StateGroupLinkType.CURRENT_SUBDIV)
					clauses.getExpressions().add(cb.equal(currentAddress.get(Address_.sub), subdiv));
				else
					clauses.getExpressions().add(cb.notEqual(currentAddress.get(Address_.sub), subdiv));
			}
			if (groupType == StateGroupLinkType.CALIBRATION_SUBDIV) {
				Join<JobItem, Address> calibrationAddress = jobItem.join(JobItem_.calAddr,
						javax.persistence.criteria.JoinType.LEFT);
				clauses.getExpressions().add(cb.equal(calibrationAddress.get(Address_.sub), subdiv));
			}
			cq.where(clauses);
			cq.select(cb.count(jobItem));
			return cq;
		});
	}

	@Override
	public Long getCountJobItemsForPlantid(Integer plantid) {
		if (plantid == null)
			throw new IllegalArgumentException("plantid mjst be specified");
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<JobItem> root = cq.from(JobItem.class);
			Join<JobItem, Instrument> instrument = root.join(JobItem_.inst);
			cq.where(cb.equal(instrument.get(Instrument_.plantid), plantid));
			cq.select(cb.count(root));
			return cq;
		});
	}

	@Override
	public Long getCountJobItems(int jobid) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<JobItem> root = cq.from(JobItem.class);
		Join<JobItem, Job> job = root.join(JobItem_.job);
		cq.where(cb.equal(job.get(Job_.jobid), jobid));
		cq.select(cb.count(root));
		return getEntityManager().createQuery(cq).getSingleResult();
	}

	public Page<JobItemIdDto> getJobItemIdDto(String days) {
		return new PageImpl<>(getResultList(cb -> {
			CriteriaQuery<JobItemIdDto> cq = cb.createQuery(JobItemIdDto.class);
			Root<JobItem> root = cq.from(JobItem.class);
			cq.select(cb.construct(JobItemIdDto.class, root.get(JobItem_.jobItemId), root.get(JobItem_.dueDate),
					root.get(JobItem_.currentAddr).get(Address_.telephone)));
			return cq;
		}, 0, 10));

	}

	@Override
	public Integer getCountJobItemsByDays(String days, int subdivId) {
		return getCount(buildJobItemByDateCriteriaBuilder(days, subdivId));
	}

	@Override
	public List<ItemAtTP> getAllItemsAtSpecificThirdParty(List<Integer> subdivids, int businessSubdivId, Locale locale) {

		return getResultList(cb -> {
			CriteriaQuery<ItemAtTP> cq = cb.createQuery(ItemAtTP.class);
			Root<JobItem> ji = cq.from(JobItem.class);
			Join<JobItem, Address> address = ji.join(JobItem_.currentAddr);
			Join<Address, Subdiv> sub = address.join(Address_.sub);
			Join<JobItem, ItemState> state = ji.join(JobItem_.state);
			Join<Subdiv, Company> company = sub.join(Subdiv_.comp);
			Join<Company, Country> country = company.join(Company_.country);
			Join<ItemState, Translation> statusTranslation = state.join(ItemState_.translations,
					javax.persistence.criteria.JoinType.LEFT);
			statusTranslation.on(cb.equal(statusTranslation.get(Translation_.locale), locale));

			Join<JobItem, Job> job = ji.join(JobItem_.job);

			Join<JobItem, Instrument> instrument = ji.join(JobItem_.inst);
			Join<Instrument, InstrumentModel> instModel = instrument.join(Instrument_.model);
			Join<InstrumentModel, Translation> modelName = instModel.join(InstrumentModel_.nameTranslations,
					javax.persistence.criteria.JoinType.LEFT);
			modelName.on(cb.equal(modelName.get(Translation_.locale), locale));

			Join<Job, Contact> contact = job.join(Job_.con);
			Join<Contact, Subdiv> subJob = contact.join(Contact_.sub);
			Join<Subdiv, Company> companyClient = subJob.join(Subdiv_.comp);

			Predicate clauses = cb.conjunction();
			if(CollectionUtils.isNotEmpty(subdivids)) {
				clauses.getExpressions().add(sub.get(Subdiv_.subdivid).in(subdivids));
			}
			
			clauses.getExpressions().add(cb.isTrue(state.get(ItemState_.active)));
			clauses.getExpressions().add(cb.equal(job.get(Job_.organisation), businessSubdivId));
			clauses.getExpressions().add(cb.notEqual(company.get(Company_.companyRole), CompanyRole.CLIENT));

			cq.select(cb.construct(ItemAtTP.class, ji.get(JobItem_.jobItemId), modelName.get(Translation_.translation),
					statusTranslation.get(Translation_.translation), ji.get(JobItem_.itemNo), job.get(Job_.jobid),
					job.get(Job_.jobno), ji.get(JobItem_.dateIn),sub.get(Subdiv_.subname), company.get(Company_.coname),
					company.get(Company_.companyRole), country.get(Country_.country),companyClient.get(Company_.coname),
					instrument.get(Instrument_.serialno)));

			cq.where(clauses);

			return cq;
		});
	}

	@Override
	public List<ItemsAtTPWrapper> getItemsAtThirdParty(Subdiv subdiv) {
		return getResultList(cb -> {
			val cq = cb.createQuery(ItemsAtTPWrapper.class);
			val jobItem = cq.from(JobItem.class);
			val address = jobItem.join(JobItem_.currentAddr);
			val sub = address.join(Address_.sub);
			val comp = sub.join(Subdiv_.comp);
			val country = comp.join(Company_.country);
			cq.where(cb.and(cb.notEqual(sub, subdiv),
					cb.equal(jobItem.join(JobItem_.job).get(Job_.organisation), subdiv),
					cb.isTrue(jobItem.join(JobItem_.state).get(ItemState_.active)),
					cb.notEqual(comp.get(Company_.companyRole), CompanyRole.CLIENT)));
			cq.select(cb.construct(ItemsAtTPWrapper.class, comp.get(Company_.coid), comp.get(Company_.coname),
					sub.get(Subdiv_.subdivid), sub.get(Subdiv_.subname), comp.get(Company_.companyRole),
					country.get(Country_.countryCode), cb.countDistinct(jobItem), country.get(Country_.country)));
			cq.groupBy(comp.get(Company_.coid), comp.get(Company_.coname), sub.get(Subdiv_.subdivid),
					sub.get(Subdiv_.subname), comp.get(Company_.companyRole), country.get(Country_.countryCode),
					country.get(Country_.country));
			cq.orderBy(cb.asc(comp.get(Company_.coname)), cb.asc(sub.get(Subdiv_.subname)));
			return cq;
		});
	}

	@Override
	public List<JobItem> getItemsOnJobInStateGroup(int jobId, StateGroup group) {
		return getResultList(cb -> {
			val cq = cb.createQuery(JobItem.class);
			val jobItem = cq.from(JobItem.class);
			cq.where(cb.and(cb.equal(jobItem.get(JobItem_.job), jobId),
					cb.equal(jobItem.join(JobItem_.state).join(ItemState_.groupLinks).get(StateGroupLink_.groupId),
							group.getId())));
			cq.orderBy(cb.asc(jobItem.get(JobItem_.itemNo)));
			return cq;
		});
	}

	@Override
	public List<JobItem> getItemsRequiringChase(StateGroup group) {
		return getResultList(cb -> {
			val cq = cb.createQuery(JobItem.class);
			val jobItem = cq.from(JobItem.class);
			val job = jobItem.join(JobItem_.job);
			val con = job.join(Job_.con);
			cq.where(cb
					.and(cb.equal(jobItem.join(JobItem_.state).join(ItemState_.groupLinks).get(StateGroupLink_.groupId),
							group.getId())));
			cq.orderBy(cb.asc(con.get(Contact_.lastName)), cb.asc(con.get(Contact_.firstName)),
					cb.asc(job.get(Job_.jobno)), cb.asc(jobItem.get(JobItem_.itemNo)));
			return cq;
		});
	}

	/**
	 * Intended to be called with either jobIds and/or jobItemIds with at least
	 * one id present in one of the arguments
	 *
	 * @param jobIds
	 *            (may be null or empty)
	 * @param jobItemIds
	 *            (may be null or empty)
	 */
	@Override
	public List<JobItemProjectionDTO> getJobItemProjectionDTOs(Collection<Integer> jobIds,
			Collection<Integer> jobItemIds) {
		boolean jobQuery = jobIds != null && !jobIds.isEmpty();
		boolean jobItemQuery = jobItemIds != null && !jobItemIds.isEmpty();
		if (!jobQuery && !jobItemQuery)
			throw new IllegalArgumentException("At least one job item id or job id must be specified!");
		return getResultList(cb -> {
            CriteriaQuery<JobItemProjectionDTO> cq = cb.createQuery(JobItemProjectionDTO.class);
            Root<JobItem> root = cq.from(JobItem.class);
            Join<JobItem, Job> job = root.join(JobItem_.job, JoinType.INNER);
            Join<JobItem, Instrument> instrument = root.join(JobItem_.inst, JoinType.INNER);
            Join<JobItem, CalibrationType> calType = root.join(JobItem_.calType, JoinType.LEFT);
            Join<JobItem, Capability> capability = root.join(JobItem_.capability, JoinType.LEFT);
            Join<JobItem, ItemState> itemState = root.join(JobItem_.state, JoinType.LEFT);
            Join<JobItem, OnBehalfItem> onBehalf = root.join(JobItem_.onBehalf, JoinType.LEFT);
            Join<OnBehalfItem, Company> onBehalfCompany = onBehalf.join(OnBehalfItem_.company, JoinType.LEFT);
            Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType, JoinType.LEFT);
            // Some fields (contractReviewById) only used by
            // NewSelectedJobItemsDao, hence
            // nullLiteral
            CompoundSelection<JobItemProjectionDTO> selection = cb.construct(JobItemProjectionDTO.class,
                root.get(JobItem_.jobItemId), job.get(Job_.jobid), root.get(JobItem_.itemNo),
                instrument.get(Instrument_.plantid), serviceType.get(ServiceType_.serviceTypeId),
                capability.get(Capability_.id), itemState.get(ItemState_.stateid),
                onBehalfCompany.get(Company_.coid), cb.nullLiteral(Integer.class), root.get(JobItem_.turn),
                root.get(JobItem_.dateComplete), root.get(JobItem_.dateIn), root.get(JobItem_.dueDate),
                root.get(JobItem_.clientRef));
			cq.select(selection);
			Predicate clauses = cb.disjunction();
			if (jobQuery)
				clauses.getExpressions().add(job.get(Job_.jobid).in(jobIds));
			if (jobItemQuery)
				clauses.getExpressions().add(root.get(JobItem_.jobItemId).in(jobItemIds));
			cq.where(clauses);
			cq.orderBy(cb.asc(root.get(JobItem_.itemNo)));
			return cq;
		});
	}

	@Override
	public List<JobItemProjectionDTO> getJobItemProjectionDTOsByJobIds(Collection<Integer> jobIds, Locale locale) {
		boolean jobQuery = jobIds != null && !jobIds.isEmpty();
		if (!jobQuery)
			throw new IllegalArgumentException("At least one job id must be specified!");
		return getResultList(cb -> {
			CriteriaQuery<JobItemProjectionDTO> cq = cb.createQuery(JobItemProjectionDTO.class);
			Root<JobItem> root = cq.from(JobItem.class);
			Join<JobItem, Job> job = root.join(JobItem_.job, JoinType.INNER);
            Join<JobItem, Instrument> instrument = root.join(JobItem_.inst, JoinType.INNER);
            Join<Instrument, InstrumentModel> instrumentModel = instrument.join(Instrument_.model, JoinType.INNER);

            Join<InstrumentModel, Translation> instrumentModelTranslation = instrumentModel
                .join(InstrumentModel_.nameTranslations, JoinType.LEFT);
            instrumentModelTranslation.on(cb.equal(instrumentModelTranslation.get(Translation_.locale), locale));

            Join<Instrument, Mfr> instrumentMfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
            Join<InstrumentModel, Description> instrumentModelDescription = instrumentModel
                .join(InstrumentModel_.description, JoinType.INNER);
            Join<JobItem, CalibrationType> calType = root.join(JobItem_.calType, JoinType.LEFT);
            Join<JobItem, Capability> capability = root.join(JobItem_.capability, JoinType.LEFT);
            Join<JobItem, ItemState> itemState = root.join(JobItem_.state, JoinType.LEFT);
            Join<JobItem, OnBehalfItem> onBehalf = root.join(JobItem_.onBehalf, JoinType.LEFT);
            Join<OnBehalfItem, Company> onBehalfCompany = onBehalf.join(OnBehalfItem_.company, JoinType.LEFT);
            Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType, JoinType.LEFT);
            Join<JobItem, Contract> contract = root.join(JobItem_.contract, JoinType.LEFT);

            Subquery<Long> contactReviewItemsCount = cq.subquery(Long.class);
            Root<ContractReviewItem> criRoot = contactReviewItemsCount.from(ContractReviewItem.class);
            contactReviewItemsCount.where(cb.equal(criRoot.get(ContractReviewItem_.jobitem), root));
            contactReviewItemsCount.select(cb.count(criRoot));

			// Some fields (lastAction fields) only used by
			// NewSelectedJobItemsDao (for
			// now), hence nullLiteral
            CompoundSelection<JobItemProjectionDTO> selection = cb.construct(JobItemProjectionDTO.class,
                root.get(JobItem_.jobItemId), job.get(Job_.jobid), root.get(JobItem_.itemNo),
                instrument.get(Instrument_.plantid), instrument.get(Instrument_.plantno),
                instrument.get(Instrument_.serialno), instrument.get(Instrument_.customerDescription),
                instrument.get(Instrument_.calibrationStandard), instrument.get(Instrument_.scrapped),
                instrument.get(Instrument_.modelname), instrumentModel.get(InstrumentModel_.modelid),
                instrumentModelTranslation.get(Translation_.translation),
                instrumentModel.get(InstrumentModel_.modelMfrType), instrumentMfr.get(Mfr_.genericMfr),
                instrumentMfr.get(Mfr_.name), instrumentModelDescription.get(Description_.typology),

                serviceType.get(ServiceType_.serviceTypeId), cb.nullLiteral(String.class),
                capability.get(Capability_.id), itemState.get(ItemState_.stateid),
                onBehalfCompany.get(Company_.coid), cb.nullLiteral(Integer.class), root.get(JobItem_.turn),
                root.get(JobItem_.dateComplete), root.get(JobItem_.dateIn), root.get(JobItem_.dueDate),
                root.get(JobItem_.clientRef), contract.get(Contract_.id),
                cb.selectCase().when(cb.ge(contactReviewItemsCount.getSelection(), 1), true).otherwise(false),
                cb.nullLiteral(Integer.class), cb.nullLiteral(Date.class), cb.nullLiteral(String.class));
			cq.where(job.get(Job_.jobid).in(jobIds));
			cq.orderBy(cb.asc(root.get(JobItem_.itemNo)));
			cq.select(selection);
			return cq;
		});
	}

	/**
	 * Method is ONLY used for invoice generation, so eagerly gets entities used
	 * for invoicing, and to prevent lazy loading on onBehalf / notInvoiced
	 * Ultimately, this can/should be scrapped when invoicing can be fully done
	 * from projections GB 2019-06-27
	 */
	@Override
	public List<JobItem> getJobItemsByIds(List<Integer> jobitemids) {
		List<JobItem> result;
		if (jobitemids == null || jobitemids.isEmpty()) {
			result = Collections.emptyList();
		} else {
			result = getResultList(cb -> {
				CriteriaQuery<JobItem> cq = cb.createQuery(JobItem.class);
				Root<JobItem> root = cq.from(JobItem.class);
				// @OneToOne relationships used in invoice generation
				Fetch<JobItem, ContractReviewCalibrationCost> crCalCost = root.fetch(JobItem_.calibrationCost,
						JoinType.LEFT);
				Fetch<JobItem, ContractReviewRepairCost> crRepCost = root.fetch(JobItem_.repairCost, JoinType.LEFT);
				Fetch<JobItem, ContractReviewAdjustmentCost> crAdjCost = root.fetch(JobItem_.adjustmentCost,
						JoinType.LEFT);
				Fetch<JobItem, ContractReviewPurchaseCost> crPurCost = root.fetch(JobItem_.purchaseCost, JoinType.LEFT);
				// Go one level deeper into this hierarchy
				crCalCost.fetch(ContractReviewCalibrationCost_.linkedCost, JoinType.LEFT);
				crRepCost.fetch(ContractReviewRepairCost_.linkedCost, JoinType.LEFT);
				crAdjCost.fetch(ContractReviewAdjustmentCost_.linkedCost, JoinType.LEFT);
				crPurCost.fetch(ContractReviewPurchaseCost_.linkedCost, JoinType.LEFT);
				root.fetch(JobItem_.notInvoiced, JoinType.LEFT);
				// @OneToOne relationships not used in invoice generation, but
				// prevent lazy loading
				root.fetch(JobItem_.onBehalf, JoinType.LEFT);
				// @ManyToOne relationships used in invoice generation
				root.fetch(JobItem_.job, JoinType.INNER);
				root.fetch(JobItem_.inst, JoinType.INNER);
				cq.orderBy(cb.asc(root.get(JobItem_.jobItemId)));
				cq.where(root.get(JobItem_.jobItemId).in(jobitemids));
				return cq;
			});
		}
		return result;
	}

	@Override
	public List<JobItem> getJobItemsByStatusAndProc(int jobid, int statusid, int procid, Integer excludejiid) {
		return getResultList(cb -> {
            val cq = cb.createQuery(JobItem.class);
            val jobItem = cq.from(JobItem.class);
            jobItem.fetch(JobItem_.job);
            jobItem.fetch(JobItem_.state);
            val proc = jobItem.join(JobItem_.nextWorkReq).join(JobItemWorkRequirement_.workRequirement)
                .join(WorkRequirement_.capability);
            cq.where(cb
                .and(Stream
                    .of(Optional.of(cb.equal(jobItem.get(JobItem_.job), jobid)),
                        Optional.of(cb.equal(jobItem.get(JobItem_.state), statusid)),
                        Optional.of(cb.equal(proc, procid)),
                        Optional.ofNullable(excludejiid)
                            .map(ei -> cb.notEqual(jobItem.get(JobItem_.job), ei)))
                    .flatMap(OptionalsUtils::optionalToStream).toArray(Predicate[]::new)));
            return cq;
        });
	}

	@Override
	public List<JobItemProjectionDTO> getJobItemsFromJobs(List<String> jobnos, Locale locale) {
		return getResultList(cb -> {
			val cq = cb.createQuery(JobItemProjectionDTO.class);
			val jobItem = cq.from(JobItem.class);
			val job = jobItem.join(JobItem_.job);
			val instrument = jobItem.join(JobItem_.inst);
			val instrumentModel = instrument.join(Instrument_.model, JoinType.LEFT);
			val instrumentModelTranslation = instrumentModel.join(InstrumentModel_.nameTranslations, JoinType.LEFT);
			instrumentModelTranslation.on(cb.equal(instrumentModelTranslation.get(Translation_.locale), locale));
			val instrumentMfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
			val instrumentModelDescription = instrumentModel.join(InstrumentModel_.description, JoinType.INNER);
			cq.where(job.get(Job_.jobno).in(jobnos));
			cq.select(cb.construct(JobItemProjectionDTO.class, jobItem.get(JobItem_.jobItemId),
					jobItem.get(JobItem_.itemNo), job.get(Job_.jobno), instrument.get(Instrument_.plantid),
					instrument.get(Instrument_.plantno), instrument.get(Instrument_.serialno),
					instrument.get(Instrument_.customerDescription), instrument.get(Instrument_.calibrationStandard),
					instrument.get(Instrument_.scrapped), instrument.get(Instrument_.modelname),
					instrumentModel.get(InstrumentModel_.modelid),
					instrumentModelTranslation.get(Translation_.translation),
					instrumentModel.get(InstrumentModel_.modelMfrType), instrumentMfr.get(Mfr_.genericMfr),
					instrumentMfr.get(Mfr_.name), instrumentModelDescription.get(Description_.typology)));
			cq.orderBy(cb.asc(job.get(Job_.jobid)), cb.asc(job.get(Job_.jobno)),cb.asc(jobItem.get(JobItem_.itemNo)));
			return cq;
		});
	}

	@Override
	public JobItem getMostRecentJobItemForInstrument(Instrument instrument) {
		return getFirstResult(cb -> {
			CriteriaQuery<JobItem> cq = cb.createQuery(JobItem.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			cq.where(cb.equal(jobItem.get(JobItem_.inst), instrument));
			cq.orderBy(cb.desc(jobItem.get(JobItem_.dateIn)), cb.desc(jobItem.get(JobItem_.jobItemId)));
			return cq;
		}).orElse(null);
	}

	private CriteriaQuery<Long> getPlantillasJPACriteriaCount(int allocatedCompanyId, int allocatedAddressId,
			Date lastModified, Integer clientAddressId, int businessSubdivId) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		assemblePlantillasJPACriteria(cq, allocatedCompanyId, 0, allocatedAddressId, lastModified, clientAddressId,
				businessSubdivId, Long.class);
		return cq;
	}

	private CriteriaQuery<JobItemPlantillasDTO> getPlantillasJPACriteriaDTO(int allocatedCompanyId, int jobItemId,
			int allocatedAddressId, Date lastModified, Integer clientAddressId, int businessSubdivId) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<JobItemPlantillasDTO> cq = cb.createQuery(JobItemPlantillasDTO.class);
		assemblePlantillasJPACriteria(cq, allocatedCompanyId, jobItemId, allocatedAddressId, lastModified,
				clientAddressId, businessSubdivId,  JobItemPlantillasDTO.class);
		return cq;
	}

	/**
	 * Builds CriteriaQuery for the specified criteria.
	 *
	 * @param allocatedCompanyId
	 *            - 0 if not used, otherwise restricts results to job items
	 *            synchronized for allocated company
	 * @param allocatedAddressId
	 *            - Address ID (normally business company) to be used for "last
	 *            transit in date" information (must be provided)
	 * @param jobItemId
	 *            - 0 if not used, otherwise returns just this job item
	 * @param lastModified
	 *            - null if not used, otherwise return records modified after
	 *            this date
	 * @param clientAddressId
	 *            - null if not used, otherwise returns records just for this
	 *            client address id
	 * @param resultClass
	 *            - either Long for count or JobItemPlantillasDTO for criteria
	 */
	private <T> void assemblePlantillasJPACriteria(CriteriaQuery<T> cq, int allocatedCompanyId, int jobItemId,
			int allocatedAddressId, Date lastModified, Integer clientAddressId, int businessSubdivId, Class<T> resultClass) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		Root<JobItem> jobItem = cq.from(JobItem.class);
		Join<JobItem, Job> job = jobItem.join(JobItem_.job, javax.persistence.criteria.JoinType.INNER);
		Join<Job, Address> returnToAddress = job.join(Job_.returnTo, javax.persistence.criteria.JoinType.INNER);
		Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst, javax.persistence.criteria.JoinType.LEFT);
		Join<JobItem, ItemState> state = jobItem.join(JobItem_.state, javax.persistence.criteria.JoinType.LEFT);
		Join<JobItem, CalibrationType> calType = jobItem.join(JobItem_.calType,
				javax.persistence.criteria.JoinType.LEFT);
		Join<JobItem, ContractReviewCalibrationCost> calibrationCost = jobItem.join(JobItem_.calibrationCost,
				javax.persistence.criteria.JoinType.LEFT);
		Join<Job, SupportedCurrency> currency = job.join(Job_.currency, javax.persistence.criteria.JoinType.LEFT);
		Join<JobItem, OnBehalfItem> onBehalf = jobItem.join(JobItem_.onBehalf,
				javax.persistence.criteria.JoinType.LEFT);
		Join<OnBehalfItem, Address> onBehalfAddress = onBehalf.join(OnBehalfItem_.address,
				javax.persistence.criteria.JoinType.LEFT);
		Join<OnBehalfItem, Company> onBehalfCompany = onBehalf.join(OnBehalfItem_.company,
				javax.persistence.criteria.JoinType.LEFT);

		Predicate conjunction = cb.conjunction();

		if (businessSubdivId != 0) {
			Join<Job, Subdiv> subdiv = job.join(Job_.organisation.getName(), javax.persistence.criteria.JoinType.INNER);
			conjunction.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), businessSubdivId));
		}
		if (allocatedCompanyId != 0) {
			Join<Job, Contact> contact = job.join(Job_.con, javax.persistence.criteria.JoinType.INNER);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub, javax.persistence.criteria.JoinType.INNER);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp, javax.persistence.criteria.JoinType.INNER);
			Join<Company, CompanySettingsForAllocatedCompany> settings = company
					.join(Company_.settingsForAllocatedCompanies, javax.persistence.criteria.JoinType.INNER);
			Join<CompanySettingsForAllocatedCompany, Company> businessCompany = settings
					.join(CompanySettingsForAllocatedCompany_.organisation.getName());
			conjunction.getExpressions().add(cb.equal(businessCompany.get(Company_.coid), allocatedCompanyId));
			conjunction.getExpressions()
					.add(cb.equal(settings.get(CompanySettingsForAllocatedCompany_.syncToPlantillas), true));
		}
		if (jobItemId != 0) {
			conjunction.getExpressions().add(cb.equal(jobItem.get(JobItem_.jobItemId), jobItemId));
		}
		if (lastModified != null) {
			Predicate disjunction = cb.disjunction();
			disjunction.getExpressions().add(cb.greaterThan(jobItem.get(JobItem_.lastModified), lastModified));
			disjunction.getExpressions().add(cb.greaterThan(job.get(Job_.lastModified), lastModified));
			conjunction.getExpressions().add(disjunction);
		}
		if (clientAddressId != null) {
			conjunction.getExpressions().add(cb.equal(returnToAddress.get(Address_.addrid), clientAddressId));
		}

		Join<JobItem, JobItemAction> actions = jobItem.join(JobItem_.actions, javax.persistence.criteria.JoinType.LEFT);

		Subquery<Integer> sq = cq.subquery(Integer.class);
		Root<JobItemTransit> sqJobItemTransit = sq.from(JobItemTransit.class);
		Join<JobItemTransit, Address> sqToAddr = sqJobItemTransit.join(JobItemTransit_.toAddr,
				javax.persistence.criteria.JoinType.INNER);
		sqToAddr.on();

		Predicate sqConjunction = cb.conjunction();
		sqConjunction.getExpressions().add(cb.equal(sqToAddr.get(Address_.addrid), allocatedAddressId));
		sqConjunction.getExpressions().add(cb.equal(sqJobItemTransit.get(JobItemTransit_.jobItem), jobItem));

		sq.where(sqConjunction);
		sq.select(cb.greatest(sqJobItemTransit.get(JobItemTransit_.id)));

		actions.on(cb.in(actions.get(JobItemAction_.id)).value(sq));

		cq.where(conjunction);

		if (Long.class.equals(resultClass)) {
			cq.multiselect(cb.count(jobItem));
		} else if (JobItemPlantillasDTO.class.equals(resultClass)) {
			cq.select(cb.construct(resultClass, jobItem.get(JobItem_.lastModified), job.get(Job_.lastModified),
					returnToAddress.get(Address_.addrid), instrument.get(Instrument_.plantid),
					jobItem.get(JobItem_.jobItemId), job.get(Job_.jobid), job.get(Job_.jobno),
					jobItem.get(JobItem_.itemNo), state.get(ItemState_.stateid), job.get(Job_.type),
					calType.get(CalibrationType_.accreditationSpecific), jobItem.get(JobItem_.dateIn),
					actions.get(JobItemAction_.endStamp), calibrationCost.get(ContractReviewCalibrationCost_.finalCost),
					currency.get(SupportedCurrency_.currencyCode), onBehalfAddress.get(Address_.addrid),
					onBehalfCompany.get(Company_.coid)));
			cq.orderBy(cb.asc(jobItem.get(JobItem_.jobItemId)));
		} else {
			throw new IllegalArgumentException();
		}
	}

	@Override
	public JobItemPlantillasDTO getPlantillasJobItem(int jobItemId, int allocatedAddressId) {
		CriteriaQuery<JobItemPlantillasDTO> cq = getPlantillasJPACriteriaDTO(0, jobItemId, allocatedAddressId, null,
				null, 0);
		TypedQuery<JobItemPlantillasDTO> queryDTO = getEntityManager().createQuery(cq);
		List<JobItemPlantillasDTO> results = queryDTO.getResultList();
		return results.isEmpty() ? null : results.get(0);
	}

	@Override
	public PagedResultSet<JobItemPlantillasDTO> getPlantillasJobItems(int allocatedCompanyId, int allocatedAddressId,
			Date lastModified, Integer clientAddressId, int businessSubdivId, int resultsPerPage, int currentPage) {
		CriteriaQuery<Long> cqCount = getPlantillasJPACriteriaCount(allocatedCompanyId, allocatedAddressId,
				lastModified, clientAddressId, businessSubdivId);
		TypedQuery<Long> queryCount = getEntityManager().createQuery(cqCount);
		int count = queryCount.getSingleResult().intValue();

		CriteriaQuery<JobItemPlantillasDTO> cqDTO = getPlantillasJPACriteriaDTO(allocatedCompanyId, 0,
				allocatedAddressId, lastModified, clientAddressId, businessSubdivId);
		TypedQuery<JobItemPlantillasDTO> queryDTO = getEntityManager().createQuery(cqDTO);
		PagedResultSet<JobItemPlantillasDTO> prs = new PagedResultSet<>(resultsPerPage, currentPage);
		log.debug(String.valueOf(count));
		prs.setResultsCount(count);
		queryDTO.setFirstResult(prs.getStartResultsFrom());
		queryDTO.setMaxResults(prs.getResultsPerPage());
		prs.setResults(queryDTO.getResultList());
		return prs;
	}

	@Override
	public JobItem getJobItemByItemCode(String jobNo, Integer itemNo) {
		return getFirstResult(cb -> {
			CriteriaQuery<JobItem> cq = cb.createQuery(JobItem.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			cq.where(cb.and(cb.equal(jobItem.get(JobItem_.itemNo), itemNo), cb.equal(job.get(Job_.jobno), jobNo)));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<JobItemDTO> getJobItemDTOs(Integer jobId, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<JobItemDTO> cq = cb.createQuery(JobItemDTO.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
			Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model);
			Join<InstrumentModel, Translation> modelName = model.join(InstrumentModel_.nameTranslations,
					javax.persistence.criteria.JoinType.LEFT);
			modelName.on(cb.equal(modelName.get(Translation_.locale), locale));
			cq.where(cb.equal(job.get(Job_.jobid), jobId));
			cq.orderBy(cb.asc(jobItem.get(JobItem_.itemNo)));
			cq.select(cb.construct(JobItemDTO.class, jobItem.get(JobItem_.jobItemId), jobItem.get(JobItem_.itemNo),
					modelName.get(Translation_.translation)));
			return cq;
		});
	}

	@Override
	public JobItem getForView(Integer jobItemId) {
		return getSingleResult(cb -> {
			CriteriaQuery<JobItem> cq = cb.createQuery(JobItem.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			jobItem.fetch(JobItem_.job);
			Fetch<JobItem, Instrument> instrument = jobItem.fetch(JobItem_.inst);
			Fetch<Instrument, InstrumentModel> instrumentModel = instrument.fetch(Instrument_.model);
			instrumentModel.fetch(InstrumentModel_.mfr, javax.persistence.criteria.JoinType.LEFT);
			instrument.fetch(Instrument_.mfr, javax.persistence.criteria.JoinType.LEFT);
			Fetch<JobItem, CalibrationType> calType = jobItem.fetch(JobItem_.calType);
			calType.fetch(CalibrationType_.serviceType);
			Fetch<JobItem, ItemState> state = jobItem.fetch(JobItem_.state);
			state.fetch(ItemState_.groupLinks, javax.persistence.criteria.JoinType.LEFT);
			jobItem.fetch(JobItem_.group, javax.persistence.criteria.JoinType.LEFT);
			cq.where(cb.equal(jobItem.get(JobItem_.jobItemId), jobItemId));
			return cq;
		});
	}

	@Override
	public JobItem findJobItem(String jobNumber, Integer jobItemNumber) {
		return getFirstResult(cb -> {
			CriteriaQuery<JobItem> cq = cb.createQuery(JobItem.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			cq.where(cb.equal(jobItem.get(JobItem_.itemNo), jobItemNumber));
			cq.where(cb.equal(job.get(Job_.jobno), jobNumber));
			return cq;
		}).orElse(null);
	}

	@Override
	public PagedResultSet<TLMJobItemDTO> searchJobitems(boolean active, Integer plantId, String plantno,
			String serialno, String formerBarcode, Integer calibrationSubdiv, String jobitemno, int currentPage,
			int resultsPerPage, Locale locale) {

		PagedResultSet<TLMJobItemDTO> result = new PagedResultSet<>(resultsPerPage, currentPage);
		super.completePagedResultSet(result, TLMJobItemDTO.class, cb -> cq -> {
			Root<JobItem> jobItemRoot = cq.from(JobItem.class);
			Join<JobItem, Instrument> instrumentJoin = jobItemRoot.join(JobItem_.inst);
			Join<Instrument, InstrumentModel> instrumentModelJoin = instrumentJoin.join(Instrument_.model);
			Join<JobItem, Job> jobJoin = jobItemRoot.join(JobItem_.job);
			Join<Job, Contact> contactJoin = jobJoin.join(Job_.con);
			Join<Contact, Subdiv> subdivJoin = contactJoin.join(Contact_.sub);
			Join<Subdiv, Company> companyJoin = subdivJoin.join(Subdiv_.comp);
			Join<JobItem, ItemState> itemStatus = jobItemRoot.join(JobItem_.state);
			Join<ItemState, Translation> joinTranslation = itemStatus.join(ItemState_.translations,
					javax.persistence.criteria.JoinType.LEFT);
			joinTranslation.on(cb.equal(joinTranslation.get(Translation_.locale.getName()), locale));

			// select
			Selection<TLMJobItemDTO> selection = cb
					.construct(TLMJobItemDTO.class, jobItemRoot.get(JobItem_.jobItemId.getName()),
							cb.concat(cb.concat(jobJoin.get(Job_.jobno.getName()), "."),
									jobItemRoot.get(JobItem_.itemNo.getName()).as(String.class)),
							instrumentJoin.get(Instrument_.plantid.getName()),
							instrumentJoin.get(Instrument_.plantno.getName()),
							instrumentJoin.get(Instrument_.serialno.getName()),
							instrumentJoin.get(Instrument_.formerBarCode.getName()),
							jobItemRoot.get(JobItem_.state)
									.get(ItemState_.stateid.getName()),
							joinTranslation
									.get(Translation_.translation.getName()),
							instrumentJoin.get(Instrument_.customerDescription.getName()),
							instrumentModelJoin.get(
									InstrumentModel_.modelid.getName()),
							cb.selectCase()
									.when(cb.equal(instrumentModelJoin.get(InstrumentModel_.modelMfrType.getName()),
											ModelMfrType.MFR_GENERIC),
											instrumentJoin.get(Instrument_.modelname.getName()))
									.otherwise(
											instrumentModelJoin.get(InstrumentModel_.model.getName())),
							cb.selectCase()
									.when(cb.equal(instrumentModelJoin.get(InstrumentModel_.modelMfrType.getName()),
											ModelMfrType.MFR_GENERIC),
											instrumentJoin.get(Instrument_.mfr.getName()).get(Mfr_.name.getName()))
									.otherwise(instrumentModelJoin.get(InstrumentModel_.mfr.getName())
											.get(Mfr_.name.getName())),
							instrumentModelJoin.get(InstrumentModel_.description.getName())
									.get(Description_.description.getName()),
							companyJoin.get(Company_.coname.getName()), subdivJoin.get(Subdiv_.subname.getName()));

			// where
			Predicate clauses = cb.conjunction();
			Predicate activePredicate = cb.equal(jobItemRoot.get(JobItem_.state).get(ItemState_.active), active);
			clauses.getExpressions().add(activePredicate);

			if (plantId != null) {
				Predicate plantIdPredicate = cb.equal(instrumentJoin.get(Instrument_.plantid), plantId);
				clauses.getExpressions().add(plantIdPredicate);
			}

			if (plantno != null) {
				Predicate plantnoPredicate = cb.equal(instrumentJoin.get(Instrument_.plantno), plantno);
				clauses.getExpressions().add(plantnoPredicate);
			}

			if (serialno != null) {
				Predicate serialnoPredicate = cb.equal(instrumentJoin.get(Instrument_.serialno), serialno);
				clauses.getExpressions().add(serialnoPredicate);
			}

			if (formerBarcode != null) {
				Predicate formerBarcodePredicate = cb.equal(instrumentJoin.get(Instrument_.formerBarCode),
						formerBarcode);
				clauses.getExpressions().add(formerBarcodePredicate);
			}

			if (calibrationSubdiv != null) {
				Predicate calibrationSubdivPredicate = cb.equal(
						jobItemRoot.get(JobItem_.calAddr).get(Address_.sub).get(Subdiv_.subdivid), calibrationSubdiv);
				clauses.getExpressions().add(calibrationSubdivPredicate);
			}

			if (jobitemno != null) {
				Predicate jobitemnoPredicate = cb.equal(cb.concat(cb.concat(jobJoin.get(Job_.jobno.getName()), "."),
						jobItemRoot.get(JobItem_.itemNo.getName()).as(String.class)), jobitemno);
				clauses.getExpressions().add(jobitemnoPredicate);
			}

			cq.where(clauses);

			// order by
			List<javax.persistence.criteria.Order> order = new ArrayList<>();
			order.add(cb.desc(jobItemRoot.get(JobItem_.jobItemId)));

			return Triple.of(jobItemRoot, selection, order);
		});
		return result;
	}

	@Override
	public List<JobItemForProspectiveInvoiceDTO> getForProspectiveInvoice(Integer allocatedCompanyId, Integer companyId,
			Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<JobItemForProspectiveInvoiceDTO> cq = cb.createQuery(JobItemForProspectiveInvoiceDTO.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, ItemState> itemState = jobItem.join(JobItem_.state);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
			Subquery<String> jobItemPoSq = cq.subquery(String.class);
			Root<JobItemPO> jobItemPo = jobItemPoSq.from(JobItemPO.class);
			Join<JobItemPO, PO> po = jobItemPo.join(JobItemPO_.po, JoinType.LEFT);
			Join<JobItemPO, BPO> bpo = jobItemPo.join(JobItemPO_.bpo, JoinType.LEFT);
			jobItemPoSq.where(cb.equal(jobItemPo.get(JobItemPO_.item), jobItem));
			jobItemPoSq.select(stringAggregation(cb, cb.coalesce(bpo.get(BPO_.poNumber), po.get(PO_.poNumber)), ", "));
			Join<Job, JobStatus> jobStatus = job.join(Job_.js);
			Expression<String> jobStatusTranslation = joinTranslation(cb, jobStatus, JobStatus_.nametranslations,
					locale);
			Join<Job, Subdiv> allocatedSubdiv = job.join("organisation");
			Join<Job, Contact> contact = job.join(Job_.con);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			Subquery<Long> itemCountSq = cq.subquery(Long.class);
			Root<JobItem> itemCount = itemCountSq.from(JobItem.class);
			itemCountSq.where(cb.equal(itemCount.get(JobItem_.job), job));
			itemCountSq.select(cb.count(itemCount));
			Subquery<InvoiceItem> alreadyInvoiced = cq.subquery(InvoiceItem.class);
			Root<InvoiceItem> invoiceItem = alreadyInvoiced.from(InvoiceItem.class);
			Join<InvoiceItem, Invoice> invoice = invoiceItem.join(InvoiceItem_.invoice);
			Join<Invoice, InvoiceType> invoiceType = invoice.join(Invoice_.type);
			Predicate alreadyInvoicedClauses = cb.conjunction();
			alreadyInvoicedClauses.getExpressions().add(cb.equal(invoiceItem.get(InvoiceItem_.jobItem), jobItem));
			alreadyInvoicedClauses.getExpressions().add(cb.notEqual(invoiceType.get(InvoiceType_.name), "Internal"));
			alreadyInvoicedClauses.getExpressions().add(cb.isTrue(invoiceType.get(InvoiceType_.addToAccount)));
			alreadyInvoiced.where(alreadyInvoicedClauses);
			alreadyInvoiced.select(invoiceItem);
			Join<JobItem, JobItemNotInvoiced> notInvoiced = jobItem.join(JobItem_.notInvoiced, JoinType.LEFT);
			Subquery<Integer> latestJobCostingSq = cq.subquery(Integer.class);
			Root<JobCostingItem> jobCostingItem = latestJobCostingSq.from(JobCostingItem.class);
			latestJobCostingSq.where(cb.equal(jobCostingItem.get(JobCostingItem_.jobItem), jobItem));
			latestJobCostingSq.select(cb.max(jobCostingItem.get(JobCostingItem_.id)));
			Join<JobItem, JobCostingItem> latestJobCostingItem = jobItem.join(JobItem_.jobCostingItems, JoinType.LEFT);
			latestJobCostingItem
					.on(cb.equal(latestJobCostingItem.get(JobCostingItem_.id), latestJobCostingSq.getSelection()));
			Join<JobCostingItem, JobCosting> jobCosting = latestJobCostingItem.join(JobCostingItem_.jobCosting,
					JoinType.LEFT);
			Join<JobCosting, SupportedCurrency> costingCurrency = jobCosting.join(JobCosting_.currency);
			Join<JobCostingItem, JobCostingCalibrationCost> calibrationPrice = latestJobCostingItem
					.join(JobCostingItem_.calibrationCost, JoinType.LEFT);
			Join<JobCostingItem, JobCostingRepairCost> repairPrice = latestJobCostingItem
					.join(JobCostingItem_.repairCost, JoinType.LEFT);
			Join<JobCostingItem, JobCostingAdjustmentCost> adjustmentPrice = latestJobCostingItem
					.join(JobCostingItem_.adjustmentCost, JoinType.LEFT);
			Join<JobCostingItem, JobCostingPurchaseCost> purchasePrice = latestJobCostingItem
					.join(JobCostingItem_.purchaseCost, JoinType.LEFT);
			Subquery<Integer> latestContractReviewSq = cq.subquery(Integer.class);
			Root<ContractReviewItem> contractReviewItem = latestContractReviewSq.from(ContractReviewItem.class);
			latestContractReviewSq.where(cb.equal(contractReviewItem.get(ContractReviewItem_.jobitem), jobItem));
			latestContractReviewSq.select(cb.max(contractReviewItem.get(ContractReviewItem_.id)));
			Root<ContractReviewItem> latestContractReviewItem = cq.from(ContractReviewItem.class);
			Subquery<Long> proFormaInvoices = cq.subquery(Long.class);
			Root<InvoiceItem> proFormaItem = proFormaInvoices.from(InvoiceItem.class);
			Join<InvoiceItem, Invoice> proFormaInvoice = proFormaItem.join(InvoiceItem_.invoice);
			Join<Invoice, InvoiceType> proFormaType = proFormaInvoice.join(Invoice_.type);
			proFormaType.on(cb.isFalse(proFormaType.get(InvoiceType_.addToAccount)));
			proFormaInvoices.where(cb.equal(proFormaItem.get(InvoiceItem_.jobItem), jobItem));
			proFormaInvoices.select(cb.count(proFormaItem));
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(allocatedSubdiv.get(Subdiv_.comp), allocatedCompanyId));
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.comp), companyId));
			clauses.getExpressions().add(cb.not(cb.exists(alreadyInvoiced)));
			clauses.getExpressions().add(cb.isFalse(itemState.get(ItemState_.active)));
			clauses.getExpressions().add(cb.isNull(notInvoiced));
			clauses.getExpressions().add(cb.equal(latestContractReviewItem.get(ContractReviewItem_.id),
					latestContractReviewSq.getSelection()));
			cq.where(clauses);
			cq.select(cb.construct(JobItemForProspectiveInvoiceDTO.class, jobItem.get(JobItem_.jobItemId),
					jobItem.get(JobItem_.itemNo), job.get(Job_.jobid), job.get(Job_.jobno), jobStatusTranslation,
					itemCountSq.getSelection(), subdiv.get(Subdiv_.subdivid), subdiv.get(Subdiv_.subname),
					jobItemPoSq.getSelection(), latestJobCostingItem.get(JobCostingItem_.id),
					jobCosting.get(JobCosting_.regdate), costingCurrency.get(SupportedCurrency_.currencyERSymbol),
					calibrationPrice.get(JobCostingCalibrationCost_.finalCost),
					repairPrice.get(JobCostingRepairCost_.finalCost),
					adjustmentPrice.get(JobCostingAdjustmentCost_.finalCost),
					purchasePrice.get(JobCostingPurchaseCost_.finalCost), proFormaInvoices.getSelection(),
					instrument.get(Instrument_.plantno)));
			return cq;
		});
	}

	@Override
	public boolean stateHasGroupOfKeyName(ItemState state, StateGroup group) {
		return getFirstResult(cb -> {
            CriteriaQuery<StateGroupLink> cq = cb.createQuery(StateGroupLink.class);
            Root<StateGroupLink> stateGroupLinkRoot = cq.from(StateGroupLink.class);
            Join<StateGroupLink, ItemState> itemStateJoin = stateGroupLinkRoot.join(StateGroupLink_.state);
            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.equal(stateGroupLinkRoot.get(StateGroupLink_.groupId), group.getId()));
            clauses.getExpressions().add(cb.equal(itemStateJoin.get(ItemState_.stateid), state.getStateid()));
            cq.where(clauses);
            return cq;
        }).isPresent();

	}

	@Override
	public List<JobItemImportInfoDto> getJobItemsInfo(Integer jobId) {

		return getResultList(cb -> {

			CriteriaQuery<JobItemImportInfoDto> cq = cb.createQuery(JobItemImportInfoDto.class);
			Root<JobItem> root = cq.from(JobItem.class);
			Join<JobItem, Job> job = root.join(JobItem_.job);
			Join<JobItem, Instrument> inst = root.join(JobItem_.inst);
			Join<JobItem, JobItemWorkRequirement> jiwr = root.join(JobItem_.workRequirements);
			Join<JobItemWorkRequirement, WorkRequirement> wr = jiwr.join(JobItemWorkRequirement_.workRequirement);
			Join<WorkRequirement, ServiceType> st = wr.join(WorkRequirement_.serviceType);

			Subquery<Integer> latestContractReviewSq = cq.subquery(Integer.class);
			Root<ContractReviewItem> contractReviewItem = latestContractReviewSq.from(ContractReviewItem.class);
			latestContractReviewSq
					.where(cb.equal(contractReviewItem.get(ContractReviewItem_.jobitem), root.get(JobItem_.jobItemId)));
			latestContractReviewSq.select(cb.max(contractReviewItem.get(ContractReviewItem_.id)));

			cq.where(cb.and(cb.equal(job.get(Job_.jobid), jobId),
					cb.equal(wr.get(WorkRequirement_.status), WorkrequirementStatus.PENDING.ordinal())));

            cq.select(cb.construct(JobItemImportInfoDto.class, inst.get(Instrument_.plantid),
                root.get(JobItem_.jobItemId), jiwr.get(JobItemWorkRequirement_.id),
                wr.get(WorkRequirement_.capability).get(Capability_.id), st.get(ServiceType_.serviceTypeId),
                cb.selectCase().when(latestContractReviewSq.isNull(), false).otherwise(true)));

			return cq;
		});
	}

	private Expression<String> fullName(CriteriaBuilder cb, Path<Contact> contact) {
		return trimAndConcatWithWhitespace(contact.get(Contact_.firstName), contact.get(Contact_.lastName)).apply(cb);
	}

	@Override
	public List<PositionProjection> getPositionProjectionForJob(Integer jobId) {
		return getResultList(cb -> {
            val cq = cb.createQuery(PositionProjection.class);
            val jobItem = cq.from(JobItem.class);
            val job = jobItem.join(JobItem_.job);
            cq.where(cb.equal(jobItem.get(JobItem_.job), jobId));
            val inst = jobItem.join(JobItem_.inst);
            val lastCal = inst.join(Instrument_.lastCal, JoinType.LEFT);
            val mfr = inst.join(Instrument_.mfr, JoinType.LEFT);
            val model = inst.join(Instrument_.model);
            val modelDescription = joinTranslation(cb, model.join(InstrumentModel_.description),
                Description_.translations, LocaleContextHolder.getLocale());
            val proc = jobItem.join(JobItem_.workRequirements, JoinType.LEFT)
                .join(JobItemWorkRequirement_.workRequirement, JoinType.LEFT)
                .join(WorkRequirement_.capability, JoinType.LEFT);
            val cal = jobItem.join(JobItem_.calLinks, JoinType.LEFT).join(CalLink_.cal, JoinType.LEFT);
            val cert = cal.join(Calibration_.certs, JoinType.LEFT);
            val registeredBy = cert.join(Certificate_.registeredBy, JoinType.LEFT);
            val registeredByName = fullName(cb, registeredBy);
            cq.select(cb.construct(PositionProjection.class, cb.literal(jobId), inst.get(Instrument_.plantno),
                proc.get(Capability_.id), proc.get(Capability_.reqType),
                trimAndConcatWithWhitespace(proc.get(Capability_.name), proc.get(Capability_.description)).apply(cb),
                inst.get(Instrument_.plantid), inst.get(Instrument_.formerBarCode), inst.get(Instrument_.serialno),
                inst.get(Instrument_.modelname), modelDescription, inst.get(Instrument_.calFrequency),
                inst.get(Instrument_.calFrequencyUnit), lastCal.get(Calibration_.calDate),
                inst.get(Instrument_.nextCalDueDate), mfr.get(Mfr_.name), job.get(Job_.regDate).as(LocalDate.class),
                cal.get(Calibration_.id), cert.get(Certificate_.certid), cert.get(Certificate_.certDate),
                cert.get(Certificate_.certno), cert.get(Certificate_.remarks), cal.get(Calibration_.calDate),
                cal.join(Calibration_.calProcess, JoinType.LEFT).get(CalibrationProcess_.description),
                jobItem.join(JobItem_.notes, JoinType.LEFT).get(JobItemNote_.note), registeredByName,
                cert.get(Certificate_.calibrationVerificationStatus), cert.get(Certificate_.restriction)));
			return cq;
		});
	}

	@Override
	public JobItem findMostRecentActiveJobItem(int plantid) {
		return getFirstResult(cb -> {
			CriteriaQuery<JobItem> cq = cb.createQuery(JobItem.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
			cq.where(cb.and(cb.equal(instrument.get(Instrument_.plantid), plantid),
					cb.isNull(jobItem.get(JobItem_.dateComplete))));
			List<javax.persistence.criteria.Order> orders = new ArrayList<>();
			orders.add(cb.desc(jobItem.get(JobItem_.dateIn)));
			orders.add(cb.desc(jobItem.get(JobItem_.jobItemId)));
			cq.orderBy(orders);
			return cq;
		}).orElse(null);
	}

	@Override
	public List<Integer> getInstrumentIdsFromJob(Integer jobid) {
		return getResultList(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			cq.where(cb.equal(job.get(Job_.jobid), jobid));
			cq.select(instrument.get(Instrument_.plantid));
			cq.distinct(true);
			return cq;
		});
	}

	@Override
	public JobItemProjectionDTO getJobItemProjectionDTO(Integer jobitemId, Locale locale) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(JobItemProjectionDTO.class);
			val jobItem = cq.from(JobItem.class);
			val job = jobItem.join(JobItem_.job);
			val instrument = jobItem.join(JobItem_.inst);
			val instrumentModel = instrument.join(Instrument_.model, JoinType.LEFT);
			val instrumentModelTranslation = instrumentModel.join(InstrumentModel_.nameTranslations, JoinType.LEFT);
			instrumentModelTranslation.on(cb.equal(instrumentModelTranslation.get(Translation_.locale), locale));
			val instrumentMfr = instrument.join(Instrument_.mfr, JoinType.LEFT);
			val instrumentModelDescription = instrumentModel.join(InstrumentModel_.description, JoinType.INNER);
            cq.where(cb.equal(jobItem.get(JobItem_.jobItemId), jobitemId));
            cq.select(cb.construct(JobItemProjectionDTO.class, jobItem.get(JobItem_.jobItemId),
                jobItem.get(JobItem_.itemNo), job.get(Job_.jobno),
                job.get(Job_.jobid), job.get(Job_.type), instrument.get(Instrument_.plantid),
                jobItem.get(JobItem_.capability).get(Capability_.id), instrument.get(Instrument_.plantno),
                instrument.get(Instrument_.serialno), instrument.get(Instrument_.customerDescription),
                instrument.get(Instrument_.calibrationStandard), instrument.get(Instrument_.scrapped),
                instrument.get(Instrument_.modelname), instrumentModel.get(InstrumentModel_.modelid),
                instrumentModelTranslation.get(Translation_.translation),
                instrumentModel.get(InstrumentModel_.modelMfrType), instrumentMfr.get(Mfr_.genericMfr),
                instrumentMfr.get(Mfr_.name), instrumentModelDescription.get(Description_.typology),
                jobItem.get(JobItem_.serviceType).get(ServiceType_.calibrationType).get(CalibrationType_.calTypeId)));

			return cq;
		}).orElse(null);
	}

	@Override
	public List<JobItem> getJobItemByDeliveryId(String deliveryNo) {
		return getResultList(cb -> {
			val cq = cb.createQuery(JobItem.class);
			val jdi = cq.from(JobDeliveryItem.class);
			cq.where(cb.equal(jdi.get(JobDeliveryItem_.delivery).get(Delivery_.deliveryno), deliveryNo));
			return cq.select(jdi.get(JobDeliveryItem_.jobitem));
		});
	}

	@Override
	public List<SelectedJobItemDTO> getActiveJobItemsForWorkAllocation(List<Integer> procIds, List<Integer> stateIds, Boolean isAllocatedToEngineer,
																	   Subdiv subdiv, StateGroupLinkType sglType, Integer addressId, AllocateWorkToEngineerSearchForm form) {
		Locale locale = LocaleContextHolder.getLocale();
		return getResultList(cb -> {
			CriteriaQuery<SelectedJobItemDTO> cq = cb.createQuery(SelectedJobItemDTO.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			Join<Job, Subdiv> allocatedSubdiv = job.join("organisation");
			Join<Job, JobStatus> jobStatus = job.join(Job_.js, javax.persistence.criteria.JoinType.LEFT);
			Join<JobStatus, Translation> jobStatusName = jobStatus.join(JobStatus_.nametranslations,
					javax.persistence.criteria.JoinType.LEFT);
			jobStatusName.on(cb.equal(jobStatusName.get(Translation_.locale), locale));
			Join<Job, Contact> clientContact = job.join(Job_.con, javax.persistence.criteria.JoinType.LEFT);
			Join<Contact, Subdiv> clientSubdiv = clientContact.join(Contact_.sub);
			Join<Subdiv, Company> clientCompany = clientSubdiv.join(Subdiv_.comp);
			Join<Job, TransportOption> transportOutOption = job.join(Job_.returnOption,
					javax.persistence.criteria.JoinType.LEFT);
			Join<TransportOption, TransportMethod> transportOutMethod = transportOutOption.join(TransportOption_.method,
					javax.persistence.criteria.JoinType.LEFT);
			Join<TransportMethod, Translation> transportOutName = transportOutMethod
					.join(TransportMethod_.methodTranslation, javax.persistence.criteria.JoinType.LEFT);
			transportOutName.on(cb.equal(transportOutName.get(Translation_.locale), locale));
			Join<TransportOption, Subdiv> transportOutSubdiv = transportOutOption.join(TransportOption_.sub,
					javax.persistence.criteria.JoinType.LEFT);
			Join<JobItem, Capability> procedure = jobItem.join(JobItem_.capability, javax.persistence.criteria.JoinType.LEFT);
			Join<Capability, CapabilityFilter> capabilityFilter = procedure.join(Capability_.capabilityFilters,javax.persistence.criteria.JoinType.LEFT);

			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst,
					javax.persistence.criteria.JoinType.LEFT);
			Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model);
			Join<InstrumentModel, Translation> modelName = model.join(InstrumentModel_.nameTranslations,
					javax.persistence.criteria.JoinType.LEFT);
			modelName.on(cb.equal(modelName.get(Translation_.locale), locale));
			Join<InstrumentModel,Description> desc = model.join(InstrumentModel_.description);
			Join<Description,Translation> descTrasnlation = desc.join(Description_.translations,javax.persistence.criteria.JoinType.LEFT);
			descTrasnlation.on(cb.equal(descTrasnlation.get(Translation_.locale),locale));
			Join<JobItem, CalibrationType> calType = jobItem.join(JobItem_.calType,
					javax.persistence.criteria.JoinType.LEFT);
			Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType,
					javax.persistence.criteria.JoinType.LEFT);
			Join<ServiceType, Translation> serviceTypeShortName = serviceType.join(ServiceType_.shortnameTranslation,
					javax.persistence.criteria.JoinType.LEFT);
			serviceTypeShortName.on(cb.equal(serviceTypeShortName.get(Translation_.locale), locale));
			Join<JobItem, Contact> contractReviewBy = jobItem.join(JobItem_.lastContractReviewBy,
					javax.persistence.criteria.JoinType.LEFT);
			Join<JobItem, ItemState> itemState = jobItem.join(JobItem_.state, javax.persistence.criteria.JoinType.LEFT);
			Expression<String> itemStateName = joinTranslation(cb, itemState, ItemState_.translations, locale);
			Join<ItemState, StateGroupLink> stateGroupLink = itemState.join(ItemState_.groupLinks,
					javax.persistence.criteria.JoinType.LEFT);
			Join<JobItem, JobItemAction> action = jobItem.join(JobItem_.actions,
					javax.persistence.criteria.JoinType.LEFT);
			Subquery<Integer> actionSubQuery = cq.subquery(Integer.class);
			Root<JobItemAction> sqActions = actionSubQuery.from(JobItemAction.class);
			actionSubQuery.where(cb.equal(sqActions.get(JobItemAction_.jobItem), jobItem));
			actionSubQuery.select(cb.max(sqActions.get(JobItemAction_.id)));
			action.on(cb.equal(action.get(JobItemAction_.id), actionSubQuery));
			Join<JobItem, Address> currentAddress = jobItem.join(JobItem_.currentAddr,
					javax.persistence.criteria.JoinType.LEFT);
			Join<JobItem, Address> calAddress = jobItem.join(JobItem_.calAddr,
					javax.persistence.criteria.JoinType.LEFT);
			Subquery<Long> jiCountSq = cq.subquery(Long.class);
			Root<JobItem> jiCount = jiCountSq.from(JobItem.class);
			jiCountSq.where(cb.equal(jiCount.get(JobItem_.job), job));
			jiCountSq.select(cb.count(jiCount));
			cq.select(cb.construct(SelectedJobItemDTO.class, jobItem.get(JobItem_.jobItemId),
					jobItem.get(JobItem_.itemNo), jiCountSq.getSelection(), jobItem.get(JobItem_.dueDate),
					jobItem.get(JobItem_.turn), job.get(Job_.jobid), job.get(Job_.jobno),
					jobStatusName.get(Translation_.translation), job.get(Job_.regDate), job.get(Job_.type),
					clientContact.get(Contact_.personid), clientContact.get(Contact_.firstName),
					clientContact.get(Contact_.lastName), clientContact.get(Contact_.active),
					clientSubdiv.get(Subdiv_.subdivid), clientSubdiv.get(Subdiv_.subname),
					clientSubdiv.get(Subdiv_.active), clientCompany.get(Company_.coid),
					clientCompany.get(Company_.coname), transportOutOption.get(TransportOption_.id),
					transportOutName.get(Translation_.translation), transportOutSubdiv.get(Subdiv_.subdivCode),
					procedure.get(Capability_.id), procedure.get(Capability_.reference), procedure.get(Capability_.name),
					instrument.get(Instrument_.plantid), instrument.get(Instrument_.plantno),
					instrument.get(Instrument_.serialno), model.get(InstrumentModel_.modelid),
					modelName.get(Translation_.translation), serviceTypeShortName.get(Translation_.translation),
					serviceType.get(ServiceType_.displayColour), serviceType.get(ServiceType_.displayColourFastTrack),
					contractReviewBy.get(Contact_.personid), contractReviewBy.get(Contact_.firstName),
					contractReviewBy.get(Contact_.lastName), itemState.get(ItemState_.stateid), itemStateName,
					action.get(JobItemAction_.endStamp), action.get(JobItemAction_.remark),
					action.get(JobItemAction_.id), allocatedSubdiv.get(Subdiv_.subdivid),
					allocatedSubdiv.get(Subdiv_.subdivCode),capabilityFilter.get(CapabilityFilter_.standardTime)));
			cq.distinct(true);
			Predicate clauses = cb.conjunction();
			Predicate allocatedSubdivPredicate = cb.equal(allocatedSubdiv, subdiv);
			Predicate currentSubdivPredicate = cb.equal(currentAddress.get(Address_.sub), subdiv);
			Predicate allocatedNotCurrentSubdivPredicate = cb.and(cb.equal(allocatedSubdiv, subdiv),
					cb.notEqual(currentAddress.get(Address_.sub), subdiv));
			Predicate calibrationSubdivPredicate = cb.disjunction();
			if (stateIds != null) {
				List<Integer> depGroups = StateGroup.getAllStateGroupsOfType(StateGroupType.DEPARTMENT).stream()
						.map(StateGroup::getId).collect(Collectors.toList());
				clauses.getExpressions().add(stateGroupLink.get(StateGroupLink_.groupId).in(depGroups));
				allocatedSubdivPredicate = cb.and(allocatedSubdivPredicate,
						cb.equal(stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.ALLOCATED_SUBDIV));
				currentSubdivPredicate = cb.and(currentSubdivPredicate,
						cb.equal(stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.CURRENT_SUBDIV));
				allocatedNotCurrentSubdivPredicate = cb.and(allocatedNotCurrentSubdivPredicate, cb.equal(
						stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.ALLOCATED_NOTCURRENT_SUBDIV));
				calibrationSubdivPredicate = cb.and(cb.equal(calAddress.get(Address_.sub), subdiv),
						cb.equal(stateGroupLink.get(StateGroupLink_.type), StateGroupLinkType.CALIBRATION_SUBDIV));
			}
			if (sglType == null) {
				Predicate sglDisjunction = cb.disjunction();
				sglDisjunction.getExpressions().add(allocatedSubdivPredicate);
				sglDisjunction.getExpressions().add(currentSubdivPredicate);
				sglDisjunction.getExpressions().add(allocatedNotCurrentSubdivPredicate);
				sglDisjunction.getExpressions().add(calibrationSubdivPredicate);
				clauses.getExpressions().add(sglDisjunction);
			} 
			if (procIds != null)
				if (procIds.isEmpty())
					clauses.getExpressions().add(cb.isNull(procedure.get(Capability_.id)));
				else
					clauses.getExpressions().add(procedure.get(Capability_.id).in(procIds));
			if (stateIds != null) {
				if (stateIds.isEmpty())
					clauses.getExpressions().add(cb.isNull(itemState.get(ItemState_.stateid)));
				else
					clauses.getExpressions().add(itemState.get(ItemState_.stateid).in(stateIds));
			} else
				clauses.getExpressions().add(cb.isTrue(itemState.get(ItemState_.active)));


			if (isAllocatedToEngineer != null) {
				Subquery<Long> engineerAllocationSq = cq.subquery(Long.class);
				Root<EngineerAllocation> engineerAllocation = engineerAllocationSq.from(EngineerAllocation.class);
				Join<EngineerAllocation, JobItem> allocatedItem = engineerAllocation
						.join(EngineerAllocation_.itemAllocated);
				engineerAllocationSq.where(cb.equal(allocatedItem, jobItem));
				engineerAllocationSq.select(cb.count(engineerAllocation));
				if (isAllocatedToEngineer)
					clauses.getExpressions().add(cb.notEqual(engineerAllocationSq, 0L));
				else
					clauses.getExpressions().add(cb.equal(engineerAllocationSq, 0L));
			}

			if(form.getClientCompanyId()!=null){
				clauses.getExpressions().add(cb.equal(clientCompany.get(Company_.coid),form.getClientCompanyId()));
			}
			if(form.getClientSubdivId()!=null){
				clauses.getExpressions().add(cb.equal(clientSubdiv.get(Subdiv_.subdivid),form.getClientSubdivId()));
			}
			if(form.getServiceTypeId()!=null){
				clauses.getExpressions().add(cb.equal(serviceType.get(ServiceType_.serviceTypeId),form.getServiceTypeId()));
			}
			if(form.getJobtype()!=null){
				clauses.getExpressions().add(cb.equal(job.get(Job_.type),form.getJobtype()));
			}

			if(StringUtils.isNotBlank(form.getDesc())){
				clauses.getExpressions().add(cb.equal(desc.get(Description_.description),form.getDesc()));
			}
			if(form.getRangeDueDate()==true){
				clauses.getExpressions().add(cb.between(jobItem.get(JobItem_.dueDate),form.getAweDueDate1(),form.getAwedueDate2()));
			}else{
				if(form.getAweDueDate1()!=null)
					clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.dueDate),form.getAweDueDate1()));
			}

			cq.where(clauses);
			cq.orderBy(cb.asc(job.get(Job_.jobno)), cb.asc(jobItem.get(JobItem_.itemNo)));
			return cq;
		}).stream().limit(100).collect(Collectors.toList());
	}

}