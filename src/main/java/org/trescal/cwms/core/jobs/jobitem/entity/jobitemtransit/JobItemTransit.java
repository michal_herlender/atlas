package org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit;

import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;

import javax.persistence.*;
import java.util.Date;

@Entity
@DiscriminatorValue("transit")
public class JobItemTransit extends JobItemAction
{
	private Address fromAddr;
	private Location fromLoc;
	private Address toAddr;
	private Location toLoc;
	private Date transitDate;


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fromaddrid")
	public Address getFromAddr()
	{
		return this.fromAddr;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fromlocid")
	public Location getFromLoc()
	{
		return this.fromLoc;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "toaddrid")
	public Address getToAddr()
	{
		return this.toAddr;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tolocid")
	public Location getToLoc()
	{
		return this.toLoc;
	}

	@Column(name = "transitdate", columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getTransitDate()
	{
		return this.transitDate;
	}

	@Override
	@Transient
	public boolean isTransit()
	{
		return true;
	}

	public void setFromAddr(Address fromAddr)
	{
		this.fromAddr = fromAddr;
	}

	public void setFromLoc(Location fromLoc)
	{
		this.fromLoc = fromLoc;
	}

	public void setToAddr(Address toAddr)
	{
		this.toAddr = toAddr;
	}

	public void setToLoc(Location toLoc)
	{
		this.toLoc = toLoc;
	}

	public void setTransitDate(Date transitDate)
	{
		this.transitDate = transitDate;
	}
}