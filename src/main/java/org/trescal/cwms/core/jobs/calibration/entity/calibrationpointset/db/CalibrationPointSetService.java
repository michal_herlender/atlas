package org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.db;

import java.util.List;

import org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.CalibrationPointSet;

public interface CalibrationPointSetService
{
	void deleteCalibrationPointSet(CalibrationPointSet calibrationpointset);

	CalibrationPointSet findCalibrationPointSet(int id);

	List<CalibrationPointSet> getAllCalibrationPointSets();

	void insertCalibrationPointSet(CalibrationPointSet calibrationpointset);

	void saveOrUpdateCalibrationPointSet(CalibrationPointSet calibrationpointset);

	void updateCalibrationPointSet(CalibrationPointSet calibrationpointset);
}