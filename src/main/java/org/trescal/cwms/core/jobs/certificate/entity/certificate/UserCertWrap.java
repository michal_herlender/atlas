package org.trescal.cwms.core.jobs.certificate.entity.certificate;

import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.tools.labelprinting.certificate.CertificateLabelSettings;

public class UserCertWrap
{
	private Boolean accredited;
	private CertLink cl;
	private CertificateLabelSettings labelSettings;
	private AccreditationLevel level;
	// Removed boolean includeRecallDateOnLabel 2016-05-19 GB; replaced with label settings

	public UserCertWrap()
	{

	}

	public UserCertWrap(CertLink certLink, Boolean accredited, AccreditationLevel level, CertificateLabelSettings labelSettings)
	{
		this.cl = certLink;
		this.accredited = accredited;
		this.level = level;
		this.labelSettings = labelSettings;
	}

	public Boolean getAccredited()
	{
		return this.accredited;
	}

	public CertLink getCl()
	{
		return this.cl;
	}

	public AccreditationLevel getLevel()
	{
		return this.level;
	}

	public void setAccredited(Boolean accredited)
	{
		this.accredited = accredited;
	}

	public void setCert(CertLink cl)
	{
		this.cl = cl;
	}

	public void setLevel(AccreditationLevel level)
	{
		this.level = level;
	}

	public CertificateLabelSettings getLabelSettings() {
		return labelSettings;
	}

	public void setLabelSettings(CertificateLabelSettings labelSettings) {
		this.labelSettings = labelSettings;
	}
}