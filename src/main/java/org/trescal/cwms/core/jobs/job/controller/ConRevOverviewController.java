package org.trescal.cwms.core.jobs.job.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.StringTools;

@Controller @IntranetController
public class ConRevOverviewController
{
	@Value("#{props['cwms.config.jobitem.fast_track_turn']}")
	private int fastTrackTurn;
	@Autowired
	private CalReqService calReqServ;
	@Autowired
	private JobService jobServ;
	
	@ModelAttribute("job")
	protected Job formBackingObject(@RequestParam(value="jobid", required=false, defaultValue="0") Integer jobid) throws ServletException
	{
		Job job = this.jobServ.get(jobid);
		return job;
	}
	
	private Map<Integer, String> getCalReqsText(Map<Integer, CalReq> calReqs) {
		Map<Integer, String> result = new HashMap<>();
		for(Integer jobItemId : calReqs.keySet()) {
			CalReq calReq = calReqs.get(jobItemId);
			result.put(jobItemId, StringTools.convertCalReqsToSingleString(calReq));
		}
		return result;
	}
	
	@RequestMapping(value="/croverview.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute("job") Job job) throws Exception
	{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(Constants.FAST_TRACK_TURN, this.fastTrackTurn);
		Map<Integer, CalReq> calReqs = this.calReqServ.findCalReqsForAllItemsOnJob(job.getJobid());
		// TODO calReqs can be removed when croverview.vm conversion to croverview.jsp complete
		// calReqs used by VM, calReqsText can be used by JSP (replaces StringTool call)
		map.put("calReqs", calReqs);
		map.put("calReqsText", getCalReqsText(calReqs));
		return new ModelAndView("trescal/core/jobs/job/croverview", map);
	}
}