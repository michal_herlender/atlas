package org.trescal.cwms.core.jobs.calibration.entity.standardused;

import java.util.Collection;
import java.util.Date;

import org.trescal.cwms.core.jobs.calibration.dto.CalibrationDTO;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Builder
public class StandardUsedModel {
	
	private Integer plantId;
	private Date startCalDate;
	private Date endCalDate;
	private Collection<CalibrationDTO> calibrationDTOs;
	
}
