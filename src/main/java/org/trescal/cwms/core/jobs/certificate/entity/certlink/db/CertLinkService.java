package org.trescal.cwms.core.jobs.certificate.entity.certlink.db;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;

public interface CertLinkService extends BaseService<CertLink, Integer> {

	CertLink findCertLink(Integer linkId);

	void insertCertLink(CertLink cl);

	/**
	 * Returns the total number of certlinks associated with the instrument
	 * 
	 * @param plantid
	 * @return
	 */
	Long getCertLinkCount(Integer plantid);

	/**
	 * Provides the most recent maxResults certlinks for the specified plantid This
	 * avoids loading all results, in the case of 100s of jobs / certlinks for one
	 * plantid
	 * 
	 * @param plantid
	 * @param maxResults
	 * @return
	 */
	List<CertLink> searchByPlantId(Integer plantid, Integer maxResults);

	/**
	 * Returns a list of {@link CertLink} entities that cover the {@link Instrument}
	 * entity identified by the given id.
	 * 
	 * 2019-07-15 deprecated for performance reasons; hundreds of jobs for some
	 * instruments. use scalable design and querying techniques instead.
	 * 
	 */
	@Deprecated
	List<CertLink> searchCertLink(Integer plantId, boolean includeDeletedAndCancelledCertificates);

	void insertCertLink(CertLink cl, Contact contact, Date startDate);
}