package org.trescal.cwms.core.jobs.certificate.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class CertificateForInstrumentTooltipDto {

    private String certificateNo;
    private String jobNo;
    private Integer jobId;
    private Integer itemNo;
    private Integer jobItemId;
    private Date calibrationDate;
    private Date certificateDate;
}
