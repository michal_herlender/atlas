package org.trescal.cwms.core.jobs.calibration.entity.templatepoint;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.jobs.calibration.entity.point.Point;
import org.trescal.cwms.core.jobs.calibration.entity.pointsettemplate.PointSetTemplate;

@Entity
@Table(name = "templatepoint")
public class TemplatePoint extends Point
{
	private PointSetTemplate template;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tempid", nullable = false)
	public PointSetTemplate getTemplate()
	{
		return this.template;
	}

	public void setTemplate(PointSetTemplate template)
	{
		this.template = template;
	}
}
