package org.trescal.cwms.core.jobs.jobitem.form;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public class JobItemForm
{
	private JobItem ji;

	public JobItem getJi()
	{
		return this.ji;
	}

	public void setJi(JobItem ji)
	{
		this.ji = ji;
	}
}