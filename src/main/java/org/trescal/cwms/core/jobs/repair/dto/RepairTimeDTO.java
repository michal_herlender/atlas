package org.trescal.cwms.core.jobs.repair.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.trescal.cwms.core.tools.DateTools;

import java.time.LocalDateTime;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public class RepairTimeDTO {

	private int jobItemActionId;
	private Integer timespentHour;
	private Integer timespentMinute;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private LocalDateTime startDate;
	private Integer technicianId;
	private String technicianName;
	private String technicianComment;
	private String technicianComposedComment;
	private Integer operationId;
	private String operationName;
	private Integer operationRirTimespentHour;
	private Integer operationRirTimespentMinute;
	
	public RepairTimeDTO(int id, Integer timespent, Date createdOn, String technicianComment, Integer technicianId, String technicianFisrtName, String technicianLastName) {
		super();
		this.jobItemActionId = id;
		this.timespentHour = timespent / 60;
		this.timespentMinute = timespent % 60;
		this.startDate = DateTools.dateToLocalDateTime(createdOn);
		this.technicianComposedComment = technicianComment;
		String[] commentSplit = technicianComment.split("<br/>");
		if (commentSplit.length > 2)
			this.technicianComment = commentSplit[2];
		this.technicianId = technicianId;
		this.technicianName = technicianFisrtName + " " + technicianLastName;
	}

	
}