package org.trescal.cwms.core.jobs.jobitem.form;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.db.ContractReviewService;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import javax.validation.ConstraintViolation;
import java.util.Set;

@Component
public class JIContractReviewValidator extends AbstractBeanValidator {
    @Autowired
    private JobItemService jobItemService;
    @Autowired
    private ContractReviewService contractRevServ;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(JIContractReviewForm.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        super.validate(target, errors);
        JIContractReviewForm form = (JIContractReviewForm) target;
		
		// prevent user to perform a Contract review on inactive JOBITEM
		if(form.getSubmit().equals("contract review") && form.getJi() != null && form.getJi().getState() != null && !form.getJi().getState().getActive())
			errors.reject("error.contractreview.jobitem.inactive", new Object[]{}, "You can not perform a ContractReview on inactive JOBITEM");
		
		if (form.isBulkUpdate()) {
			
			// If copying to other job items, ensure that either the "copy transport out option" 
			// is selected or that all selected other job items already have a transport out option
			if (!form.isCopyTransportOut()) {
				for (int jobitemid : form.getJobItemIds()) {
					JobItem jobItem = this.jobItemService.findJobItem(jobitemid);
					if (jobItem.getReturnOption() == null) {
						errors.reject("error.contractreview.transportout.jobitem", new Object[]{jobItem.getItemNo()}, "Job item {0} does not have a transport out option selected.");
					}
				}
			}
			
			// If copying work requirements, ensure that 
			// the job item has a "next work requirement" which is not yet completed
			// (inherently verifies at least one uncompleted/non cancelled work requirement to copy)
			if (form.isCopyWorkRequirements()) {
				if (form.getJi().getNextWorkReq() == null ||
						form.getJi().getNextWorkReq().getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE) ||
						form.getJi().getNextWorkReq().getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED)) {
					errors.reject("error.contractreview.nextworkrequirement", null, "To copy work requirements, the job item must have an uncompleted or non cancelled next work requirement.");
				}
			}
		}

		// only validate presetcomments field if we are trying to save it
		if (form.isSaveComments()) {
			// create a dummy ContractReview object and validate it's comment
			// field
			PresetComment comment = new PresetComment();
			comment.setType(PresetCommentType.CONTRACT_REVIEW);
			comment.setComment(form.getReviewComments());
			Set<ConstraintViolation<PresetComment>> violations = super.getConstraintViolations(comment);
			for (ConstraintViolation<PresetComment> violation : violations) {
				if (violation.getPropertyPath().toString().equals("comment"))
					errors.rejectValue("saveComments", null, violation.getMessage());
			}
		}
		// if reserving a certificate, ensure that the date is entered
		if (form.isReserveCertificate()) {
			if (form.getCertDate() == null) {
				errors.rejectValue("certDate", "error.contractreview.certdate", "A certificate date must be entered when reserving a certificate");
			}
			checkWorkRequirements(form.getJi(), errors);
			// A procedure needs to be selected for each work requirement, if reserving certificates
			if (form.isCopyReserveCertificate()) {
				for (Integer jobitemid : form.getJobItemIds()) {
					JobItem jobItem = this.jobItemService.findJobItem(jobitemid);
					checkWorkRequirements(jobItem, errors);
				}
			}
		}
		
		if (form.getSubmit().equalsIgnoreCase("update")){
			if(StringUtils.isNoneBlank(form.getReviewComments()) && form.getJi()!=null){
				if(this.contractRevServ.getMostRecentContractReviewForJobitem(form.getJi())==null)
				{
					errors.rejectValue("reviewComments", "error.contractreview.contractreviewmustbepreformed", "A first contract review must be performed to save comments.");
				}
			}
		}
		
		// Don't allow the deletion of the turnaround
		if (form.getTurnaround() == null){
			errors.rejectValue("turnaround", "error.contractreview.turnaround", "The value must be greater than or equal to 1");	
		}
		
	}
	
	/**
	 * If reserving certificates for a job item, all the work requirements need to have a procedure selected
	 * Job items always have at least one work requirement  
	 * @param jobItem
	 * @param errors
	 */
	private void checkWorkRequirements(JobItem jobItem, Errors errors) {
		boolean missingProcedure = false;
		for (JobItemWorkRequirement jiwr : jobItem.getWorkRequirements()) {
            if (jiwr.getWorkRequirement().getCapability() == null)
                missingProcedure = true;
        }
		if (missingProcedure) {
			errors.reject("error.contractreview.procedure.jobitem", new Object[]{jobItem.getItemNo()}, "Job item {0} requires procedure selection on its work requirements");
		}
	}
}