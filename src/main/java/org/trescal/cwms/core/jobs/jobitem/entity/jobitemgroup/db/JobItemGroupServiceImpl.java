package org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.db;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;

@Service("JobItemGroupService")
public class JobItemGroupServiceImpl implements JobItemGroupService
{
	@Autowired
	private JobItemGroupDao jobItemGroupDao;
	@Autowired
	private JobService jobServ;
	
	public ResultWrapper ajaxGetGroupWithItems(int groupId)
	{
		JobItemGroup jig = this.jobItemGroupDao.getGroupWithItems(groupId);

		if (jig != null)
		{
			return new ResultWrapper(true, "", jig, null);
		}
		else
		{
			return new ResultWrapper(false, "The job item group could not be found", null, null);
		}
	}

	public ResultWrapper ajaxUpdateGroupFields(int groupId, Boolean withCase, Boolean calibration, Boolean delivery)
	{
		JobItemGroup group = this.findJobItemGroup(groupId);

		if (group == null)
		{
			return new ResultWrapper(false, "Group could not be found");
		}
		if (calibration != null)
		{
			group.setCalibrationGroup(calibration);
		}
		if (delivery != null)
		{
			group.setDeliveryGroup(delivery);
		}

		this.updateJobItemGroup(group);

		return new ResultWrapper(true, null, group, null);
	}

	public ResultWrapper deleteGroupFromJob(int groupId)
	{
		try
		{
			// Eager fetch so jobitems can be completely returned in DWR result
			JobItemGroup g = this.getGroupWithItems(groupId);
			// create new arraylist of job items to pass back
			ArrayList<JobItem> jobitems = new ArrayList<JobItem>();

			if (g.getItems().size() > 0)
			{
				for (JobItem ji : g.getItems())
				{
					// should persist automatically
					ji.setGroup(null);
					// add job item to arraylist
					jobitems.add(ji);
				}
			}

			this.deleteJobItemGroup(g);

			return new ResultWrapper(true, null, jobitems, null);
		}
		catch (Exception e)
		{
			return new ResultWrapper(false, e.getMessage(), null, null);
		}
	}

	public void deleteJobItemGroup(JobItemGroup jobitemgroup)
	{
		this.jobItemGroupDao.remove(jobitemgroup);
	}

	public JobItemGroup findJobItemGroup(int id)
	{
		return this.jobItemGroupDao.find(id);
	}

	public List<JobItemGroup> getAllGroupsForJob(int jobId)
	{
		return this.jobItemGroupDao.getAll(jobId);
	}

	public List<JobItemGroup> getAllJobItemGroups()
	{
		return this.jobItemGroupDao.findAll();
	}

	public JobItemGroup getGroupWithItems(int groupId)
	{
		return this.jobItemGroupDao.getGroupWithItems(groupId);
	}

	public void insertJobItemGroup(JobItemGroup JobItemGroup)
	{
		this.jobItemGroupDao.persist(JobItemGroup);
	}

	public ResultWrapper newGroupForJob(int jobId)
	{
		Job j = this.jobServ.get(jobId);
		JobItemGroup g = new JobItemGroup();

		if (j != null)
		{
			g.setJob(j);
			this.insertJobItemGroup(g);
			return new ResultWrapper(true, null, g, null);
		}

		return new ResultWrapper(false, "Could not create new Group for Job.", null, null);
	}

	public void saveOrUpdateJobItemGroup(JobItemGroup jobitemgroup)
	{
		this.jobItemGroupDao.saveOrUpdate(jobitemgroup);
	}

	public void setJobItemGroupDao(JobItemGroupDao jobItemGroupDao)
	{
		this.jobItemGroupDao = jobItemGroupDao;
	}

	public void setJobServ(JobService jobServ)
	{
		this.jobServ = jobServ;
	}

	public void updateJobItemGroup(JobItemGroup JobItemGroup)
	{
		this.jobItemGroupDao.update(JobItemGroup);
	}

	@Override
	public List<Integer> getJobitemsByGroupId(int groupId) {
		return this.jobItemGroupDao.getJobitemsByGroupId(groupId);
	}

	@Override
	public List<Integer> getGroupIdsByJobitemIds(List<Integer> jobitemIds) {
		return this.jobItemGroupDao.getGroupIdsByJobitemIds(jobitemIds);
	}
}