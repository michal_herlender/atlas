package org.trescal.cwms.core.jobs.jobitemhistory;

import lombok.Setter;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "jobitemhistory")
@Setter
public class JobItemHistory {

	private Contact changeBy;
	private Date changeDate;
	private int id;
	private JobItem jobItem;
	private String comment;
	private boolean valueUpdated;
	private String fieldname;
	private LocalDate oldValue;
	private LocalDate newValue;

	public JobItemHistory() {
	}

	public JobItemHistory(Contact changeBy, Date changeDate, JobItem jobItem, String comment, boolean valueUpdated,
						  LocalDate oldValue, LocalDate newValue, String fieldname) {
		super();
		this.changeBy = changeBy;
		this.changeDate = changeDate;
		this.jobItem = jobItem;
		this.comment = comment;
		this.valueUpdated = valueUpdated;
		this.oldValue = oldValue;
		this.newValue = newValue;
		this.fieldname = fieldname;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "changeby", nullable = false, foreignKey = @ForeignKey(name = "FK_jobitemhistory_changeby"))
	public Contact getChangeBy() {
		return this.changeBy;
	}

	@NotNull
	@Column(name = "changedate", nullable = false, columnDefinition = "datetime2")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getChangeDate() {
		return this.changeDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid", nullable = false, foreignKey = @ForeignKey(name = "FK_jobitemhistory_jobitemid"))
	public JobItem getJobItem() {
		return this.jobItem;
	}

	@Size(max = 1000)
	@Column(name = "comment", columnDefinition = "nvarchar(1000)")
	public String getComment() {
		return this.comment;
	}

	@Column(name = "newvalue", columnDefinition = "date")
	public LocalDate getNewValue() {
		return this.newValue;
	}

	@Column(name = "oldvalue", columnDefinition = "date")
	public LocalDate getOldValue() {
		return this.oldValue;
	}

	@Column(name = "valueupdated", nullable = false, columnDefinition = "tinyint")
	public boolean isValueUpdated() {
		return this.valueUpdated;
	}

	@Size(max = 32)
	@Column(name = "fieldname", columnDefinition = "varchar(32)")
	public String getFieldname() {
		return fieldname;
	}

}