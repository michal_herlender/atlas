package org.trescal.cwms.core.jobs.certificate.entity.certaction.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.certificate.entity.certaction.CertAction;

public interface CertActionDao extends BaseDao<CertAction, Integer> {}