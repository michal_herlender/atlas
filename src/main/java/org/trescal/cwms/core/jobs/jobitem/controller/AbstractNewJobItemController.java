package org.trescal.cwms.core.jobs.jobitem.controller;

import lombok.val;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db.ServiceCapabilityService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.ItemBasketWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.form.AbstractNewJobItemForm;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;


public abstract class AbstractNewJobItemController {
	@Autowired
	protected InstrumService instServ;
	@Autowired
	protected InstrumentModelService modelServ;
	@Autowired
	protected JobService jobServ;
    @Autowired
    protected JobItemService jiServ;
    @Autowired
    protected MessageSource messageSource;
    @Autowired
    protected QuotationItemService qiService;
    @Autowired
    protected ServiceCapabilityService serviceCapabilityService;
    @Autowired
    protected TranslationService translationService;
    @Autowired
    protected CalibrationTypeService calibrationTypeService;
    @Autowired
    protected CapabilityService capabilityService;

    public final static Logger logger = LoggerFactory.getLogger(AbstractNewJobItemController.class);

    /**
     * Establishes the recommended service type for the instrument or instrument
     * model in the basket
     */
    protected Pair<Integer, Integer> getServiceTypeId(Job job, Quotationitem qItem, Instrument inst, Boolean isCompatible, boolean defaultServiceTypeUsage) {
        Pair<Integer, Integer> resultServiceType;

		// If defaultServiceTypeUsage is true (user preference) we use the job default service type only.

		// Priority 1 - linked quotation item
		if (!defaultServiceTypeUsage && qItem != null) {
			resultServiceType = Pair.of(1, qItem.getServiceType().getServiceTypeId());
		}
		// Priority 2 - instrument default service type
		else if (!defaultServiceTypeUsage && (inst != null) && (inst.getDefaultServiceType() != null)
			&& isCompatible) {
			resultServiceType = Pair.of(2, inst.getDefaultServiceType().getServiceTypeId());
		}
		// Priority 3 - job default service type (will never be null)
		else {
			resultServiceType = Pair.of(3, job.getDefaultCalType().getServiceType().getServiceTypeId());
		}
		return resultServiceType;
	}

	private Pair<Integer, Integer> getServiceTypeId(Job job, ItemBasketWrapper basketItem, boolean defaultServiceTypeUsage) {
		return getServiceTypeId(job, basketItem.getQuotationItem(), basketItem.getInst(), basketItem.getIsInstrumentCalTypeCompatibleWithJobType(), defaultServiceTypeUsage);
	}

	public ArrayList<ItemBasketWrapper> initializeBasketItems(Model model, AbstractNewJobItemForm form, Job job, boolean defaultServiceTypeUsage) {
		Locale locale = LocaleContextHolder.getLocale();
		// create new basket wrapper list
		ArrayList<ItemBasketWrapper> basketItems = new ArrayList<>();
		// get all the lists of basket item details
		if (form.getBasketTypes().size() > 0) {
			List<Integer> ids = form.getBasketIds();
			List<Integer> tempGroupIds = form.getBasketTempGroupIds();
			List<Integer> existingGroupIds = form.getBasketExistingGroupIds();
			List<String> types = form.getBasketTypes();
			List<String> parts = form.getBasketParts();
			List<Integer> baseItemIds = form.getBasketBaseItemIds();
			List<Integer> quotationItemIds = form.getBasketQuotationItemIds();
			List<Integer> serviceTypeIds = form.getServiceTypeIds();
			List<Integer> serviceTypeSources = form.getServiceTypeSources();
			// create ref data list of barcodes in basket
			List<Integer> barcodesInBasket = new ArrayList<>();
			// bind from lists into object wrappers
			for (int i = 0; i < form.getBasketTypes().size(); i++) {
				ItemBasketWrapper basketItem = new ItemBasketWrapper();
				logger.trace("basketIds[" + i + "]:" + ObjectUtils.toString(ids.get(i)));
				logger.trace("basketTempGroupIds[" + i + "]:" + ObjectUtils.toString(tempGroupIds.get(i)));
				logger.trace("basketExistingGroupIds[" + i + "]:" + ObjectUtils.toString(existingGroupIds.get(i)));
				logger.trace("basketTypes[" + i + "]:" + ObjectUtils.toString(types.get(i)));
				logger.trace("basketParts[" + i + "]:" + ObjectUtils.toString(parts.get(i)));
				logger.trace("basketBaseItemIds[" + i + "]:" + ObjectUtils.toString(baseItemIds.get(i)));
				logger.trace("basketQuotationItemIds[" + i + "]:" + ObjectUtils.toString(quotationItemIds.get(i)));
				String type = types.get(i);
				int id = ids.get(i);
				Quotationitem qItem = null;
				if (quotationItemIds.get(i) != 0) {
					qItem = this.qiService.findQuotationItem(quotationItemIds.get(i));
					basketItem.setQuotationItem(qItem);
				}
				basketItem.setQuotationItemDto(getDto(qItem, locale));

				if (type.equals("instrument")) {
					Instrument inst = this.instServ.get(id);
					basketItem.setInst(inst);
					barcodesInBasket.add(id);
					if (inst.getDefaultServiceType() != null) {
						// If defaultServiceTypeUsage = true, we allow only the default job type
						Boolean isCompatible = defaultServiceTypeUsage ?
							inst.getDefaultServiceType().getServiceTypeId() == job.getDefaultCalType().getServiceType().getServiceTypeId()
							: calibrationTypeService.isCompatibleWithJobType(
                            inst.getDefaultServiceType().getCalibrationType(), job.getType());
                        basketItem.setIsInstrumentCalTypeCompatibleWithJobType(isCompatible);
                    }
                } else if (type.equals("model")) {
                    basketItem.setModel(this.modelServ.findInstrumentModel(id));
                }
                // We look up the service type id in initialization
                // - we could consider to put in basket in future, but we should
                // really first refactor javascript-to-html basket handling
                Pair<Integer, Integer> serviceTypeResult = getServiceTypeId(job, basketItem, defaultServiceTypeUsage);
                Integer serviceTypeSource = serviceTypeResult.getLeft();
                Integer serviceTypeId = serviceTypeResult.getRight();
                serviceTypeIds.add(serviceTypeId);
                serviceTypeSources.add(serviceTypeSource);
                logger.info("serviceTypeIds[" + i + "]:" + serviceTypeId);
                logger.info("serviceTypeIds.size():" + serviceTypeIds.size());
                logger.info("serviceTypeSources[" + i + "]:" + serviceTypeSource);
                logger.info("serviceTypeSources.size():" + serviceTypeSources.size());

                // Lookup the default procedure using capability service type filters
                val proc = basketItem.getInst() != null ? this.capabilityService.getActiveCapabilityUsingCapabilityFilter
                    (basketItem.getInst().getModel().getDescription().getId(), serviceTypeId, job.getOrganisation().getSubdivid()) : Optional.<Capability>empty();

                if (proc.isPresent()) {
                    basketItem.setProc(proc.get());
                } else {
                    // Lookup the default procedure and work instruction for the
                    // instrument or model (in future reference on quotation item)
                    // Note we no longer use the default procedure on instrument -
                    // may not be correct, target removal
                    Integer instModelId = basketItem.getInst() == null ? basketItem.getModel().getModelid()
                        : basketItem.getInst().getModel().getModelid();
                    List<ServiceCapability> capabilities = this.serviceCapabilityService.find(instModelId, serviceTypeId,
                        null, job.getOrganisation().getSubdivid());
                    if (!capabilities.isEmpty()) {
                        // Otherwise null; consider putting id / name into form from
                        // projection
                        ServiceCapability capability = capabilities.get(0);
                        basketItem.setProc(capability.getCapability());
                        basketItem.setWi(capability.getWorkInstruction());
                    }
				}

				// If no type specified, the item has been deleted from the
				// basket (order/index maintained)
				basketItem.setExistingGroupId(existingGroupIds.get(i));
				basketItem.setTempGroupId(tempGroupIds.get(i));
				basketItem.setPart(parts.get(i));
				JobItem baseItem = (baseItemIds.get(i) == null) ? null : this.jiServ.findJobItem(baseItemIds.get(i));
				basketItem.setBaseItem(baseItem);
				basketItem.setBaseItemId(baseItem == null ? 0 : baseItemIds.get(i));
				basketItems.add(basketItem);
			}
			// set barcodes in basket into ref data
			model.addAttribute("barcodesInBasket", barcodesInBasket);
		}
		model.addAttribute("basketItems", basketItems);

		return basketItems;
	}

	/*
	 * Prepares a Quotationitem for formatting/display; if null passed then the
	 * "Not Linked" DTO is returned
	 */
	public KeyValue<Integer, String> getDto(Quotationitem qItem, Locale locale) {
		KeyValue<Integer, String> result;
		if (qItem != null) {
			String value = qItem.getQuotation().getQno() +
					"." +
					qItem.getItemno() +
					" - " +
					translationService.getCorrectTranslation(qItem.getServiceType().getShortnameTranslation(), locale) +
					" - " +
					qItem.getReferenceNo() +
					" - " +
					qItem.getQuotation().getCurrency().getCurrencySymbol() +
					" " +
					qItem.getFinalCost();
			result = new KeyValue<>(qItem.getId(), value);
		} else {
			result = new KeyValue<>(0, messageSource.getMessage("notlinked", null, locale));
		}
		return result;
	}
}
