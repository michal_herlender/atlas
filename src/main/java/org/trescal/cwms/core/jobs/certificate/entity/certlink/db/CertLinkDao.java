package org.trescal.cwms.core.jobs.certificate.entity.certlink.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;

import java.util.List;

public interface CertLinkDao extends BaseDao<CertLink, Integer> {

	/**
	 * Returns the total number of certlinks associated with the instrument
	 */
	Long getCertLinkCount(Integer plantid);

	/**
	 * Provides the most recent maxResults certlinks for the specified plantid
	 * This avoids loading all results, in the case of 100s of jobs / certlinks for one plantid
	 * 
	 */
	List<CertLink> searchByPlantId(Integer plantid, Integer maxResults);

	/**
	 * 2019-07-15 deprecated for performance reasons; hundreds of jobs for some instruments.
	 * use scalable design and querying techniques to limit results instead.
	 */
	@Deprecated
	List<CertLink> searchCertLink(Integer plantId, boolean includeDeletedAndCancelledCertificates);	
}