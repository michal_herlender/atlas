package org.trescal.cwms.core.jobs.job.entity.job.db;

import org.springframework.data.repository.CrudRepository;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

import java.util.Optional;

public interface JobDataRepository extends CrudRepository<Job, Integer> {

    Optional<Job> findByJobno(String jobNo);
}
