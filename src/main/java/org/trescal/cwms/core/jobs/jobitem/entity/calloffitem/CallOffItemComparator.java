package org.trescal.cwms.core.jobs.jobitem.entity.calloffitem;

import java.util.Comparator;

/**
 * Comparator by company name (client company of job), then job number, then item number, then call off item id
 * @author Galen Beck
 *
 */
public class CallOffItemComparator implements Comparator<CallOffItem> {

	@Override
	public int compare(CallOffItem callOffItem0, CallOffItem callOffItem1) {
		String name0 = callOffItem0.getOffItem().getJob().getCon().getSub().getComp().getConame();
		String name1 = callOffItem1.getOffItem().getJob().getCon().getSub().getComp().getConame();
		int result = name0.compareTo(name1);
		if (result == 0) {
			String jobNo0 = callOffItem0.getOffItem().getJob().getJobno(); 
			String jobNo1 = callOffItem1.getOffItem().getJob().getJobno();
			result = jobNo0.compareTo(jobNo1);
			if (result == 0) {
				int itemNo0 = callOffItem0.getOffItem().getItemNo();
				int itemNo1 = callOffItem1.getOffItem().getItemNo();
				result = itemNo0 - itemNo1;
				if (result == 0) {
					result = callOffItem0.getId() - callOffItem1.getId();
				}
			}
		}
		return result;
	}

}
