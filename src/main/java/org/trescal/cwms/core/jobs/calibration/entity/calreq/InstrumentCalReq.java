package org.trescal.cwms.core.jobs.calibration.entity.calreq;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.system.Constants;

@Entity
@DiscriminatorValue("instrument")
public class InstrumentCalReq extends CalReq
{
	private Instrument inst;
	
	@Transient
	@Override
	public String getClassKey()
	{
		return Constants.CALREQ_MAP_INSTRUMENT;
	}

	@Override
	@Transient
	public int getGenericId()
	{
		return (this.inst == null) ? 0 : this.inst.getPlantid();
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid", nullable = true)
	public Instrument getInst()
	{
		return this.inst;
	}

	@Transient
	@Override
	public String getSource()
	{
		return "calreq.instcalreqsource"; 
	}
	
	@Transient
	@Override
	public String getSourceParameter()
	{
		return "" + this.inst.getPlantid();
	}

	public void setInst(Instrument inst)
	{
		this.inst = inst;
	}
}