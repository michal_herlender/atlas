package org.trescal.cwms.core.jobs.repair.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairCompletionReportFormDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairInspectionReportFormDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.transformer.RepairCompletionReportFormDTOTransformer;
import org.trescal.cwms.core.jobs.repair.dto.form.transformer.RepairInspectionReportFormTransformer;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.db.RepairCompletionReportService;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class RepairReportsController {

	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private RepairInspectionReportService rirService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private RepairInspectionReportFormTransformer rirTransformer;
	@Autowired
	private RepairCompletionReportFormDTOTransformer rcrTransformer;
	@Autowired
	private RepairCompletionReportService repairCompletionReportService;

	@ModelAttribute("ji")
	protected JobItem ji(Model model, @RequestParam(value = "jobitemid", required = true) Integer jobItemId) {
		JobItem ji = jobItemService.findJobItem(jobItemId);

		boolean showRcrTab = ji.getRepairInspectionReports() != null && ji.getRepairInspectionReports().stream().anyMatch(rir -> rir.getReviewedByCsr() != null && rir.getReviewedByCsr());
		model.addAttribute("disableRcrTab", !showRcrTab);
		return ji;
	}

    @RequestMapping(value = "/repairreports.htm", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView doGet(Model model, @RequestParam(value = "jobitemid") Integer jobItemId,
                              @RequestParam(name = "rirId", required = false) Integer rirId,
                              @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {

        // try to find rir either from jobitem or rirId
        RepairInspectionReport rir;
        if (rirId != null)
            rir = rirService.get(rirId);
        else
            rir = rirService.getRirByJobItemIdAndSubdivId(jobItemId, subdivDto.getKey());

		if (rir != null) {
			RepairInspectionReportFormDTO rirForm = rirTransformer.transform(rir);
			rirForm.setJobItemId(jobItemId);
			model.addAttribute("rirForm", rirForm);
			if (rir.getRepairCompletionReport() != null) {
				RepairCompletionReport rcr = repairCompletionReportService
						.get(rir.getRepairCompletionReport().getRcrId());
				RepairCompletionReportFormDTO rcrForm = rcrTransformer.transform(rcr);
				rcrForm.setJobItemId(jobItemId);
				model.addAttribute("rcrForm", rcrForm);
			}
		}

		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		model.addAttribute("businessSubdiv", rirId != null ? rirService.get(rirId).getOrganisation() : allocatedSubdiv);
		return new ModelAndView("trescal/core/jobs/repair/repairreports");
	}

}
