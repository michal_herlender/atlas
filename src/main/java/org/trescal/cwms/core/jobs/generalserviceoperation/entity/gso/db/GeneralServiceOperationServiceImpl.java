package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocumentComparator;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.GsoDocumentLink;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.GsoDocumentLinkComparator;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLinkComparator;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsostatus.GeneralServiceOperationStatus;
import org.trescal.cwms.core.jobs.generalserviceoperation.form.GeneralServiceOperationForm;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.db.JobItemActivityService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tools.fileupload2.service.UploadService;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_CompleteGeneralServiceOperation;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_StartGeneralServiceOperation;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_UploadGeneralServiceOperationDocument;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDateTime;

@Service("GeneralServiceOperationService")
public class GeneralServiceOperationServiceImpl extends BaseServiceImpl<GeneralServiceOperation, Integer>
    implements GeneralServiceOperationService {

    @Autowired
    private GeneralServiceOperationDao generalServiceOperationDao;
    @Autowired
    private StatusService statusServ;
    @Autowired
    private HookInterceptor_StartGeneralServiceOperation hookInterceptor_startGeneralServiceOperation;
    @Autowired
    private NumerationService numerationService;
    @Autowired
    private ActionOutcomeService aoService;
    @Autowired
    private HookInterceptor_CompleteGeneralServiceOperation hookInterceptor_CompleteGSO;
    @Autowired
    private ComponentDirectoryService compDirServ;
    @Autowired
	private SystemComponentService systemCompServ;
	@Autowired
	private UploadService uploadService;
	@Autowired
	private HookInterceptor_UploadGeneralServiceOperationDocument hookInterceptor_UploadGSODocument;
	@Autowired
	private JobItemService jiService;
	@Autowired
	private JobItemActivityService jiaServ;
	@Autowired
	private InstrumService instServ;

	protected BaseDao<GeneralServiceOperation, Integer> getBaseDao() {
		return this.generalServiceOperationDao;
	}

	public void startAndSaveGeneralServiceOperation(GeneralServiceOperationForm form, Contact startedBy,
			Subdiv allocatedSubdiv) {

        GeneralServiceOperation gso = new GeneralServiceOperation();
        gso.setOrganisation(allocatedSubdiv);
        // initial status ON-GOING
        gso.setStatus((GeneralServiceOperationStatus) this.statusServ.findStatusByName("On-going",
            GeneralServiceOperationStatus.class));
        if (form.getStartedOn() == null)
            gso.setStartedOn(new Date());
        else
            gso.setStartedOn(dateFromLocalDateTime(form.getStartedOn()));
        gso.setStartedBy(startedBy);
        // get calibration type and procedure from jobitem workrequirement
        CalibrationType calType = form.getJi().getNextWorkReq() != null
            ? form.getJi().getNextWorkReq().getWorkRequirement().getServiceType().getCalibrationType()
            : null;
        gso.setCalType(calType);
        Capability capability = form.getJi().getNextWorkReq() != null
            ? form.getJi().getNextWorkReq().getWorkRequirement().getCapability()
            : null;
        gso.setCapability(capability);
        gso.setAddress(form.getJi().getCurrentAddr());
        // gsolink initialization
        GsoLink gsolink = new GsoLink();
        gsolink.setGso(gso);
        gsolink.setJi(form.getJi());
        Set<GsoLink> links = new TreeSet<>(new GsoLinkComparator());
        links.add(gsolink);
        // remove all null IDs from OtherJobitemsId List
        form.getOtherJobItemsId().removeAll(Collections.singleton(null));
        for (Integer jiId : form.getOtherJobItemsId()) {
            JobItem ji = this.jiService.findJobItem(jiId);
            gsolink = new GsoLink();
            gsolink.setGso(gso);
            gsolink.setJi(ji);
            links.add(gsolink);
        }
        gso.setLinks(links);
        this.generalServiceOperationDao.persist(gso);

		// invoke the startGeneralServiceOperation Hook for creating the 'General service operation' activity
		// on the linked Jobitems
		HookInterceptor_StartGeneralServiceOperation.Input input = new HookInterceptor_StartGeneralServiceOperation.Input(
				gso, false, gso.getStartedBy(), gso.getStartedOn(), gso.getStartedOn());
		this.hookInterceptor_startGeneralServiceOperation.recordAction(input);
	}

	@Override
	public List<GeneralServiceOperation> getByJobItemId(Integer jobitemid) {
		return this.generalServiceOperationDao.getByJobItemId(jobitemid);
	}

	@Override
	public void completeGeneralServiceOperation(GeneralServiceOperationForm form, Contact completedBy) {
		GeneralServiceOperation gso = this.get(form.getGso().getId());
		// get first selected outcome to check if it's an ON-HOLD outcome or Not
		// because when the user select an ON-HOLD outcome it become applicable for all items
		Integer firstOutcomeLinkId = form.getLinkedOutcomes().keySet().iterator().next();
		ActionOutcome actionOutcome = this.aoService.get(form.getLinkedOutcomes().get(firstOutcomeLinkId));
		String status = actionOutcome.getGenericValue().name().equals("ON_HOLD") ? "On hold" : "Complete";
		gso.setStatus((GeneralServiceOperationStatus) this.statusServ.findStatusByName(status,
			GeneralServiceOperationStatus.class));
		gso.setCompletedOn(dateFromLocalDateTime(form.getCompletedOn()));
		gso.setCompletedBy(completedBy);
		gso.setClientDecisionNeeded(form.getClientDecisionNeeded());
		// update GSO links outcome and timespent
		for (Integer linkId : form.getLinkedOutcomes().keySet()) {
			Optional<GsoLink> currentLinkOpt = gso.getLinks().stream().filter(e -> e.getId() == linkId).findFirst();
			currentLinkOpt.ifPresent(currentLink -> {
				ActionOutcome ao = this.aoService.get(form.getLinkedOutcomes().get(linkId));
				currentLink.setOutcome(ao);
				currentLink.setTimeSpent(form.getLinkedTimesSpent().get(linkId));
				// calculate the nextCalDueDate if the outcome is positive
				if (ao.isPositiveOutcome()) {
					// set recall date
					this.instServ.calculateRecallDateAfterGSO(currentLink.getJi().getInst(), gso, form.getDuration(), form.getIntervalUnitId(),
						currentLink.getJi().getServiceType().getCalibrationType().getRecallRequirementType());
				}
			});
		}
		this.generalServiceOperationDao.merge(gso);
		// invoking hook CompleteGeneralServiceOperation for completing the General service operation activity
		HookInterceptor_CompleteGeneralServiceOperation.Input input = new HookInterceptor_CompleteGeneralServiceOperation.Input(
				gso, form.getRemark());
		hookInterceptor_CompleteGSO.recordAction(input);
	}

	@Override
	public void cancelGeneralServiceOperation(GeneralServiceOperationForm form, Contact cancelleddBy) {
		GeneralServiceOperation gso = this.get(form.getGso().getId());
		gso.setStatus((GeneralServiceOperationStatus) this.statusServ
			.findStatusByName(GeneralServiceOperationStatus.CANCELLED, GeneralServiceOperationStatus.class));
		gso.setCompletedOn(dateFromLocalDateTime(form.getCompletedOn()));
		gso.setCompletedBy(cancelleddBy);
		this.generalServiceOperationDao.merge(gso);
		// erase incomplete GSO activity
		this.eraseIncompleteGsoActivity(gso, cancelleddBy);
	}

	@Override
	public void eraseIncompleteGsoActivity(GeneralServiceOperation gso, Contact erasedBy) {
		// for all items on the general service operation being deleted
		for (GsoLink link : gso.getLinks()) {
			JobItem ji = link.getJi();

			// see if there is an incomplete activity
			JobItemActivity jia = this.jiaServ.getIncompleteActivityForItem(ji);

			// if there is an incomplete activity
			if (jia != null) {
				// check if the incomplete activity is a general service operation
				boolean isGsoActivity = false;
				for (StateGroupLink sgl : jia.getActivity().getGroupLinks()) {
					if (sgl.getGroup().equals(StateGroup.GENERALSERVICEOPERATION)) {
						isGsoActivity = true;
						break;
					}
				}

				// if the incomplete activity is a general service operation activity
				if (isGsoActivity) {
					// set the item status to what it was before the general service operation
					// began
					ji.setState(jia.getStartStatus());

					// remove the activity from the item's collection
					ji.getActions().remove(jia);

					// delete the incomplete activity
					this.jiaServ.delete(jia);

					// while the item is still at general service operation on hold statuses, keep
					// deleting actions as this will otherwise block progression
					while (this.jiService.isReadyToResumeGeneralServiceOperation(ji)) {
						// get all actions ordered properly
						TreeSet<JobItemAction> actions = new TreeSet<>(new JobItemActionComparator());
						actions.addAll(ji.getActions());

						JobItemActivity lastNonDeletedActivity = null;

						// loop backwards to find last non-deleted action
						for (JobItemAction action : actions.descendingSet()) {
							if (!action.isDeleted()) {
								lastNonDeletedActivity = (action instanceof JobItemActivity) ? (JobItemActivity) action
									: null;
								break;
							}
						}

						if (lastNonDeletedActivity != null) {
							// set the activity as deleted (this way any long
							// remarks entered will still technically be
							// available by viewing deleted actions)
							lastNonDeletedActivity.setDeleted(true);
							lastNonDeletedActivity.setDeletedBy(erasedBy);
							lastNonDeletedActivity.setDeleteStamp(new Date());

							// persist the update
							this.jiaServ.merge(lastNonDeletedActivity);

							// set the item status to what it was before the
							// start general service opertion
							ji.setState(lastNonDeletedActivity.getStartStatus());
						}
					}
				}
			}
		}
	}

	@Override
	public GeneralServiceOperation getOnGoingByJobItemId(Integer jobitemid) {
		return this.generalServiceOperationDao.getOnGoingByJobItemId(jobitemid);
	}

	@Override
	public GeneralServiceOperation getOnHoldByJobItemId(Integer jobitemid) {
		return this.generalServiceOperationDao.getOnHoldByJobItemId(jobitemid);
	}

	@Override
	public void uploadGeneralServiceOperationDocument(GeneralServiceOperationForm form, Subdiv allocatedSubdiv) {
		GeneralServiceOperation gso = this.get(form.getGso().getId());
		GsoDocument gsoDoc = new GsoDocument();
		gsoDoc.setDocumentDate(dateFromLocalDateTime(form.getDocumentDate()));
		// generate the GSO document number
		String gsoDocumentNumber = numerationService.generateNumber(NumerationType.GENERAL_SERVICE_OPERATION,
			allocatedSubdiv.getComp(), allocatedSubdiv);
		gsoDoc.setDocumentNumber(gsoDocumentNumber);
		gsoDoc.setGso(gso);
		// GsoDocuments initialization
		if (gso.getGsoDocuments() == null)
			gso.setGsoDocuments(new TreeSet<>(new GsoDocumentComparator()));
		gso.getGsoDocuments().add(gsoDoc);
		// GSO document links initialization
		GsoDocumentLink gsoDocLink = new GsoDocumentLink();
		gsoDocLink.setGsoDocument(gsoDoc);
		gsoDocLink.setJi(form.getJi());
		Set<GsoDocumentLink> gsoDocLinks = new TreeSet<>(new GsoDocumentLinkComparator());
		gsoDocLinks.add(gsoDocLink);
		if (form.getOtherJobItemsId() != null) {
			form.getOtherJobItemsId().removeAll(Collections.singleton(null));
			for (Integer jiId : form.getOtherJobItemsId()) {
				JobItem ji = this.jiService.findJobItem(jiId);
				gsoDocLink = new GsoDocumentLink();
				gsoDocLink.setGsoDocument(gsoDoc);
				gsoDocLink.setJi(ji);
				gsoDocLinks.add(gsoDocLink);
			}
		}
		gsoDoc.setLinks(gsoDocLinks);
		this.merge(gso);

		// save GsoDocument file 
		SystemComponent comp = this.systemCompServ.findComponent(Component.GENERAL_SERVICE_OPERATION);
		JobItem firstJi = gso.getLinks().iterator().next().getJi();
		File toDirectory = this.compDirServ.getDirectory(comp, firstJi.getJob().getJobno());

		String fileName = form.getFile().getOriginalFilename();
		String newName = gsoDoc.getIdentifier();
		int extChar = fileName.lastIndexOf(".");
		newName = newName.concat(fileName.substring(extChar));
		try {
			this.uploadService.saveFile(form.getFile().getInputStream(), toDirectory, newName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// invoking hook UploadGeneralServiceOperationDocument to create the 'Upload GSO Document' activity
		HookInterceptor_UploadGeneralServiceOperationDocument.Input input = new HookInterceptor_UploadGeneralServiceOperationDocument.Input(
			gsoDoc, newName);
		hookInterceptor_UploadGSODocument.recordAction(input);
	}

	@Override
	public void resumeGeneralServiceOperation(GeneralServiceOperation gso, Contact user) {
		// updating the status to ON-GOING
		gso.setStatus((GeneralServiceOperationStatus) this.statusServ.findStatusByName("On-going",
				GeneralServiceOperationStatus.class));
		
		// set the outcome and timespent to the default value (NULL)
		for (GsoLink gsoLink : gso.getLinks()) {
			gsoLink.setOutcome(null);
			gsoLink.setTimeSpent(null);
		}
		this.merge(gso);

		// invoking the hook StartGSO to restart the on-hold GSO
		HookInterceptor_StartGeneralServiceOperation.Input input = new HookInterceptor_StartGeneralServiceOperation.Input(
				gso, true, null, null, null);
		this.hookInterceptor_startGeneralServiceOperation.recordAction(input);
	}

	@Override
	public GeneralServiceOperation getLastGSOByJobitemId(Integer jobitemid) {
		return this.generalServiceOperationDao.getLastGSOByJobitemId(jobitemid);
	}

	@Override
	public GeneralServiceOperation getLastInstrumentGSO(Instrument instrument) {
		return this.generalServiceOperationDao.getLastInstrumentGSO(instrument);
	}

}