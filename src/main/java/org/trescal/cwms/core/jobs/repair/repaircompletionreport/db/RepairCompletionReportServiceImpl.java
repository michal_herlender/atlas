package org.trescal.cwms.core.jobs.repair.repaircompletionreport.db;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.birt.repaircompletionreport.RepairInspectionReportDataSet;
import org.trescal.cwms.core.documents.filenamingservice.FileNamingService;
import org.trescal.cwms.core.documents.filenamingservice.RepairInspectionReportFileNamingService;
import org.trescal.cwms.core.external.adveso.events.RCRCompletionDateChangedEvent;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.repair.dto.RepairTimeDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairCompletionReportFormDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.transformer.RepairCompletionReportFormDTOTransformer;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.operation.db.FreeRepairOperationService;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperation;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tools.fileupload2.service.UploadService;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_AutoEngineerInspectionAfterTP;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_UpdateRCR;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_ValidateRCR;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_ValidateRCR.Input;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.spring.model.KeyValue;

@Service("RepairCompletionReportService")
public class RepairCompletionReportServiceImpl extends BaseServiceImpl<RepairCompletionReport, Integer>
		implements RepairCompletionReportService {

	@Autowired
	private RepairCompletionReportDao dao;
	@Autowired
	private RepairCompletionReportFormDTOTransformer transformer;
	@Autowired
	private HookInterceptor_ValidateRCR hookInterceptor_ValidateRCR;
	@Autowired
	private FreeRepairOperationService freeRepairOperationService;
	@Autowired
	private UserService userService;
	@Autowired
	private DocumentService documentService;
	@Autowired
	private SystemComponentService scService;
	@Autowired
	private ComponentDirectoryService compDirService;
	@Autowired
	private UploadService uploadService;
	@Autowired
	private JobItemService jiService;
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;
	@Autowired
	private LinkToAdveso linkToAdvesoSettings;
	@Autowired
	private HookInterceptor_AutoEngineerInspectionAfterTP hookInterceptor_AutoEngineerInspectionAfterTP;
	@Autowired
	private HookInterceptor_UpdateRCR hookInterceptor_UpdateRCR;
	@Autowired
	private CertificateService certificateService;
	

	private static final Logger logger = LoggerFactory.getLogger(RepairCompletionReportServiceImpl.class);

	@Override
	protected BaseDao<RepairCompletionReport, Integer> getBaseDao() {
		return dao;
	}

	@Override
	public RepairCompletionReport updateRcr(RepairCompletionReportFormDTO form, Integer allocatedSubdivId) {
		RepairCompletionReport rcr = transformer.transform(form);
		this.merge(rcr);
		//if TP rirFile exist upload it
		JobItem ji = this.jiService.findJobItem(form.getJobItemId());
		if(ji != null) {
			rcr.setDirectory(this.getRepairCompletionReportDirectory(ji.getJob().getJobno()));
			File file = new File(rcr.getRepairCompletionReportAppendixFilePath());
			if(file.exists() && form.getRcrAppendixFile() != null && !form.getRcrAppendixFile().isEmpty())
				file.delete();
			
			if(form.getRcrAppendixFile() != null && !form.getRcrAppendixFile().isEmpty()) {
				try {
						this.uploadService.saveFile(form.getRcrAppendixFile().getInputStream(), rcr.getDirectory(), rcr.getRepairCompletionReportAppendixFileName());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}					
			}
		}
		return rcr;
	}

	@Override
	public void refresh(RepairCompletionReport rcr) {
		dao.refresh(rcr);
		// refresh operations
		rcr.getFreeRepairOperations().forEach(o -> {
			freeRepairOperationService.refrech(o);
		});
		rcr.getValidatedFreeRepairOperations().forEach(vo -> {
			freeRepairOperationService.refrech(vo.getFreeRepairOperation());
		});
	}

	@Override
	public void validate(RepairCompletionReportFormDTO form, String username, Locale locale) {
		RepairCompletionReport rcr = transformer.transform(form);
		rcr.setValidated(true);
		Contact contact = this.userService.get(username).getCon();
		logger.info("Contact for username : " + username + " found id " + (contact == null ? "null" : contact.getId()));

		rcr.setValidatedBy(contact);
		rcr.setValidatedOn(new Date());

		Input input = new Input(rcr, form.getTimeSpent());
		hookInterceptor_ValidateRCR.recordAction(input);
		this.save(rcr);
		// automatic generation of document after rcr validation
		generateRcrDocument(rcr, locale);
	}
	
	private void generateRcrDocument(RepairCompletionReport rcr, Locale locale) {
		RepairInspectionReport rir = rcr.getRepairInspectionReports().stream().findFirst().get();
		String jobNumber = rir.getJi().getJob().getJobno();

		Component component = Component.REPAIR_COMPLETION_REPORT;
		BaseDocumentType documentType = BaseDocumentType.REPAIR_COMPLETION_REPORT;
		
		SystemComponent sc = this.scService.findComponent(component);
		String directory = this.compDirService.getDirectory(sc, jobNumber).getAbsolutePath();
		FileNamingService fnService = new RepairInspectionReportFileNamingService(rir, true, directory);
		
		Map<String,Object> additionalParameters = new HashMap<>();
		additionalParameters.put(RepairInspectionReportDataSet.PARAMETER_COMPLETION_REPORT, true);
		
		documentService.createBirtDocument(rir.getRirId(), documentType, locale,
				component, fnService, additionalParameters);
	}

	@Override
	public RepairCompletionReport getByJobitem(Integer jobitemid) {
		return dao.getByJobitem(jobitemid);
	}

	@Override
	public RepairCompletionReport getValidatedRcrByJobItem(int jobItemId) {
		return dao.getValidatedRcrByJobItem(jobItemId);
	}

	@Override
	public File getRepairCompletionReportDirectory(String jobNo) {
		SystemComponent comp = this.scService.findComponent(Component.REPAIR_INSPECTION_REPORT);
		return this.compDirService.getDirectory(comp, jobNo);
	}

	@Override
	public void publishRCRCompletionDateChangedEvent(JobItem jobItem, RepairCompletionReport rcr, Date newRepairCompletionDate) {
		boolean islinkToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
				jobItem.getJob().getCon().getSub().getSubdivid(),
				jobItem.getJob().getOrganisation().getComp().getCoid(),  new Date());

		if (islinkToAdveso) {
			RCRCompletionDateChangedEvent completionDateChangedEvent = new RCRCompletionDateChangedEvent(this, rcr,
					jobItem, rcr.getRepairCompletionDate(), newRepairCompletionDate);
			applicationEventPublisher.publishEvent(completionDateChangedEvent);
		}
	}

	@Override
	public void saveRepairCompletionReport(RepairCompletionReportFormDTO form, KeyValue<Integer, String> subdivDto) {
		RepairCompletionReport rcr = this.updateRcr(form, subdivDto.getKey());

		// if current ji item is awaiting rcr update
		JobItem ji = jiService.findJobItem(form.getJobItemId());
		if (jiService.stateHasGroupOfKeyName(ji.getState(), StateGroup.AWAITINGRCRUPDATE)) {
			hookInterceptor_UpdateRCR.recordAction(rcr);

			// if tp work perform engineer inspection automatically
			Certificate lastCert = this.certificateService.getMostRecentCertificateForJobItem(ji);
			if (lastCert != null && StringUtils.isNotEmpty(lastCert.getThirdCertNo())
					&& ji.getNextWorkReq().getWorkRequirement().getType().equals(WorkRequirementType.THIRD_PARTY)) {
				HookInterceptor_AutoEngineerInspectionAfterTP.Input input = new HookInterceptor_AutoEngineerInspectionAfterTP.Input(
						ji);
				hookInterceptor_AutoEngineerInspectionAfterTP.recordAction(input);
			}
		}
	}

	@Override
	public void preparRepairTime(RepairCompletionReport rcr, List<RepairTimeDTO> repairTimes, Contact contact) {
		List<RepairTimeDTO> newRepairTimes = new ArrayList<RepairTimeDTO>();
		for (ValidatedFreeRepairOperation vfro : rcr.getValidatedFreeRepairOperations()) {
			List<RepairTimeDTO> rtDtos = repairTimes.stream().filter(rt -> {
				String[] commentSplited = rt.getTechnicianComposedComment().split("<br/>");
				String operationName = commentSplited[1];
				return vfro.getFreeRepairOperation().getName().equals(operationName);
			}).collect(Collectors.toList());
			
			if(rtDtos == null || rtDtos.isEmpty())
			{
				RepairTimeDTO rtDto = createNewEmptyRepairTimeDTO(vfro.getFreeRepairOperation().getOperationId(), vfro.getFreeRepairOperation().getName(), vfro.getFreeRepairOperation().getLabourTime());
				newRepairTimes.add(rtDto);
			}
			else {
				
				rtDtos.forEach(rtDto -> {
					rtDto.setOperationId(vfro.getFreeRepairOperation().getOperationId());
					rtDto.setOperationName(vfro.getFreeRepairOperation().getName());
					rtDto.setOperationRirTimespentHour(Integer.valueOf(vfro.getFreeRepairOperation().getLabourTime()/60));
					rtDto.setOperationRirTimespentMinute(vfro.getFreeRepairOperation().getLabourTime()%60);
				});
				if(rtDtos.stream().allMatch(rt -> !rt.getTechnicianId().equals(contact.getId()))){
					RepairTimeDTO rtDto = createNewEmptyRepairTimeDTO(vfro.getFreeRepairOperation().getOperationId(), vfro.getFreeRepairOperation().getName(), vfro.getFreeRepairOperation().getLabourTime());				
					newRepairTimes.add(rtDto);
				}
			}
		}
		
		for (FreeRepairOperation fro : rcr.getFreeRepairOperations()) {
			List<RepairTimeDTO> rtDtos = repairTimes.stream().filter(rt -> {
				String[] commentSplited = rt.getTechnicianComposedComment().split("<br/>");
				String operationName = commentSplited[1];
				return fro.getName().equals(operationName);
			}).collect(Collectors.toList());
			
			if(rtDtos == null || rtDtos.isEmpty())
			{
				RepairTimeDTO rtDto = createNewEmptyRepairTimeDTO(fro.getOperationId(), fro.getName(), fro.getLabourTime());
				newRepairTimes.add(rtDto);
			}
			else {	
				rtDtos.forEach(rtDto -> {
					rtDto.setOperationId(fro.getOperationId());
					rtDto.setOperationName(fro.getName());
					rtDto.setOperationRirTimespentHour(Integer.valueOf(fro.getLabourTime()/60));
					rtDto.setOperationRirTimespentMinute(fro.getLabourTime()%60);
				});
				if(rtDtos.stream().allMatch(rt -> !rt.getTechnicianId().equals(contact.getId()))){
					RepairTimeDTO rtDto = createNewEmptyRepairTimeDTO(fro.getOperationId(), fro.getName(), fro.getLabourTime());
					newRepairTimes.add(rtDto);
				}
			}
			
		}
		repairTimes.addAll(newRepairTimes);
	}
	
	private RepairTimeDTO createNewEmptyRepairTimeDTO(Integer operationId, String operationName, Integer operationLabourTime){
		RepairTimeDTO rtDto = new RepairTimeDTO();
		rtDto.setTimespentMinute(0);
		rtDto.setTimespentHour(0);
		rtDto.setOperationId(operationId);
		rtDto.setOperationName(operationName);
		rtDto.setOperationRirTimespentHour(Integer.valueOf(operationLabourTime/60));
		rtDto.setOperationRirTimespentMinute(operationLabourTime%60);
		return rtDto;
	}
}