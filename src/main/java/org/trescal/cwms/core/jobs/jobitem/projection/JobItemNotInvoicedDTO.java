package org.trescal.cwms.core.jobs.jobitem.projection;

import java.time.LocalDate;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.NotInvoicedReason;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class JobItemNotInvoicedDTO extends NotInvoicedProjectionDTO {
	private Integer notInvoicedId;
	private Integer jobItemId;
	
	// Field initialized after initial query
	private JobItemProjectionDTO jobItem;
	
	// Constructor used by JPA projection
	public JobItemNotInvoicedDTO(LocalDate date, Integer contactId, NotInvoicedReason reason, String comments,
			Integer invoiceId, Integer notInvoicedId, Integer jobItemId) {
		super(date, contactId, reason, comments, invoiceId);
		this.notInvoicedId = notInvoicedId;
		this.jobItemId = jobItemId;
	}
}
