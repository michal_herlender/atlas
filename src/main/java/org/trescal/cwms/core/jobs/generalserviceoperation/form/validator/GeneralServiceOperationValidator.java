package org.trescal.cwms.core.jobs.generalserviceoperation.form.validator;


import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.generalserviceoperation.controller.CompleteGeneralServiceOperationController;
import org.trescal.cwms.core.jobs.generalserviceoperation.controller.EditGsoDocumentController;
import org.trescal.cwms.core.jobs.generalserviceoperation.controller.NewGeneralServiceOperationController;
import org.trescal.cwms.core.jobs.generalserviceoperation.controller.UploadGeneralServiceOperationDocumentController;
import org.trescal.cwms.core.jobs.generalserviceoperation.form.GeneralServiceOperationForm;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Service
public class GeneralServiceOperationValidator extends AbstractBeanValidator {

    @Autowired
    private CapabilityAuthorizationService procAccServ;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(GeneralServiceOperationForm.class);
    }

    @Override
    public void validate(Object target, Errors errors, Object... validationHints) {

        GeneralServiceOperationForm gsof = (GeneralServiceOperationForm) target;
		super.validate(target, errors, validationHints);

		if (gsof.getActionPerformed() != null && gsof.getActionPerformed().equals(NewGeneralServiceOperationController.ACTION_PERFORMED) && gsof.getStartedOn() == null) {
			errors.rejectValue("startedOn", "generalserviceoperation.validation.startedon", "A Started Date must be selected.");
		}
		
		if (gsof.getActionPerformed() != null && gsof.getActionPerformed().equals(CompleteGeneralServiceOperationController.ACTION_PERFORMED)) {
			if (gsof.getCompletedOn() == null) 
				errors.rejectValue("completedOn", "generalserviceoperation.validation.completedon", "A Completed Date must be selected.");
			
			for (Integer tsKey : gsof.getLinkedTimesSpent().keySet()) {
				if (gsof.getLinkedTimesSpent().get(tsKey) == null) 
					errors.rejectValue("linkedTimesSpent["+tsKey+"]", "generalserviceoperation.validation.timespent", "A spent time must be selected.");
			}
					
		}

        if (gsof.getActionPerformed() != null && (gsof.getActionPerformed().equals(UploadGeneralServiceOperationDocumentController.ACTION_PERFORMED) ||
            gsof.getActionPerformed().equals(EditGsoDocumentController.ACTION_PERFORMED))) {
            if (gsof.getActionPerformed().equals(UploadGeneralServiceOperationDocumentController.ACTION_PERFORMED) &&
                (gsof.getFile() == null || gsof.getFile().isEmpty()))
                errors.rejectValue("file", "generalserviceoperation.validation.file", "A file must be selected.");

            if (gsof.getDocumentDate() == null)
                errors.rejectValue("documentDate", "generalserviceoperation.validation.documentdate", "A document date must be selected.");

        }

        Capability proc = gsof.getGso().getCapability() != null ? gsof.getGso().getCapability() : gsof.getJi().getNextWorkReq().getWorkRequirement().getCapability();
        CalibrationType calType = gsof.getGso().getCalType() != null ? gsof.getGso().getCalType() : gsof.getJi().getNextWorkReq().getWorkRequirement().getServiceType().getCalibrationType();
        Contact user = (Contact) validationHints[0];
        if (proc != null && calType != null && user != null) {
            val isNotAuthorized = this.procAccServ.authorizedFor(proc.getId(), calType.getCalTypeId(), user.getPersonid()).isLeft();
            if (isNotAuthorized)
                errors.rejectValue("gso.startedBy", "generalserviceoperation.rest.errors.notaccredited",
                    new Object[]{user.getPersonid()},
                    "The user is not accredited to perform this action for {0}");
        }
    }

}
