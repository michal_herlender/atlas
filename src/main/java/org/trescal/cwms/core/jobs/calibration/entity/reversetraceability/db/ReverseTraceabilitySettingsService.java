package org.trescal.cwms.core.jobs.calibration.entity.reversetraceability.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.calibration.dto.ReverseTraceabilitySettingsDTO;
import org.trescal.cwms.core.jobs.calibration.entity.reversetraceability.ReverseTraceabilitySettings;
import org.trescal.cwms.core.jobs.calibration.form.ReverseTraceabilitySettingsForm;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface ReverseTraceabilitySettingsService extends BaseService<ReverseTraceabilitySettings, Integer> {

	List<ReverseTraceabilitySettingsDTO> getReverseTraceabilitySettingsFormSubdivIds(List<Integer> subdivIds);

	void updateReverseTraceabilitySettings(ReverseTraceabilitySettings reverseTraceabilitySettings);

	void enableReverseTraceabilitySettings(ReverseTraceabilitySettingsDTO dto);

	void editReverseTraceabilitySettings(ReverseTraceabilitySettingsForm form);

	/**
	 * Finding the business subdivisions which have reverse traceability enabled
	 * and the start date from which reverse traceability is enabled
	 */
	Map<Integer, LocalDate> getSubdivsAndDateFromReverseTraceability();
}
