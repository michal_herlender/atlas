package org.trescal.cwms.core.jobs.repair.dto.form;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.trescal.cwms.core.jobs.jobitem.form.JobItemForm;
import org.trescal.cwms.core.jobs.repair.dto.FreeRepairComponentDTO;
import org.trescal.cwms.core.jobs.repair.dto.FreeRepairOperationDTO;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class RepairInspectionReportFormDTO extends JobItemForm implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer rirId;
	private String rirNumber;
	private String clientDescription;
	private String clientInstructions;
	private String internalDescription;
	private String internalInspection;
	private String internalOperations;
	private Boolean manufacturerWarranty;
	private Boolean trescalWarranty;
	private String internalInstructions;
	private String externalInstructions;
	@NotNull
	@Size(min = 1)
	private List<FreeRepairOperationDTO> operationsForm;
	private List<FreeRepairComponentDTO> componentsForm;
	private Integer leadTime;
	private Date estimatedReturnDate;
	private Integer jobItemId;
	private Boolean completedByTechnician;
	private Date completedByTechnicianOn;
	private String technician;
	private Boolean validatedByManager;
	private Date validatedByManagerOn;
	private String manager;
	private Boolean reviewedByCsr;
	private Date reviewedByCsrOn;
	private String csr;
	private Date lastModified;
	private Date creationDate;
	private String createdBy;
	private Integer timeSpent;
	private Boolean misuse;
	private String misuseComment;
	private Boolean isAccredited;
	
}
