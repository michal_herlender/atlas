package org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public interface WorkRequirementService extends BaseService<WorkRequirement, Integer> {

	/**
	 * this method deletes a {@link WorkRequirement} so long as it is not the job
	 * items next work instruction and the work requirement has not been completed.
	 * 
	 * @param wrId      id of the {@link WorkRequirement} to be deleted.
	 * @param jobItemId id of the {@link Job} item the {@link WorkRequirement} is
	 *                  linked to
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper ajaxDeleteWorkRequirement(int wrId, int jobItemId);

	/**
	 * Indicates whether it's allowed to copy the work requirements from this job
	 * item (need at least one not-yet-complete requirement to copy from)
	 * 
	 * @param copyFrom
	 */
	boolean canCopyWorkRequirements(JobItem sourceJobItem);

	void copyWorkRequirements(JobItem sourceJobItem, JobItem destJobItem);

	WorkRequirement createWorkRequirement(int jobItemId, int procedureId, int workInstructionId, int serviceTypeId,
			String requirementText);

	/**
	 * Utility method to resolve department, address, and type for the
	 * WorkRequirement
	 * 
	 * @param WorkRequirement (may not be null) JobItem ji (may have null
	 *                        currentAddr, e.g. in transit)
	 * @return department
	 */
	void updateDepartmentAndAddress(WorkRequirement wr, JobItem ji);

	WorkRequirement updateWorkRequirement(int wrId, int procedureId, int workInstructionId, int serviceTypeId,
			String requirementText);

	void checkCurrentWorkRequirement(JobItem ji);

	void cancelCurrentWorkRequirement(JobItem ji);

	WorkRequirement duplicate(WorkRequirement workRequirement);

	void checkAllWorkRequirement(JobItem jobItem);

	void checkWorkRequirement(JobItemWorkRequirement jiwr);
}