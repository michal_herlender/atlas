package org.trescal.cwms.core.jobs.repair.component;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;
import org.trescal.cwms.core.jobs.repair.component.enums.RepairComponentSourceEnum;
import org.trescal.cwms.core.jobs.repair.component.enums.RepairComponentStateEnum;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.validatedcomponent.ValidatedFreeRepairComponent;

@Entity
@Table(name = "freerepaircomponent")
public class FreeRepairComponent {

	private Integer componentId;
	private String name;
	private Integer quantity;
	private BigDecimal cost;
	private RepairComponentStateEnum status;
	private RepairComponentSourceEnum source;
	private Boolean addedAfterRiRValidation;
	private FreeRepairOperation freeRepairOperation;
	private ValidatedFreeRepairComponent validatedFreeRepairComponent;
	private Component component;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "quantity", nullable = true)
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name = "cost", nullable = true)
	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	public RepairComponentStateEnum getStatus() {
		return status;
	}

	public void setStatus(RepairComponentStateEnum status) {
		this.status = status;
	}

	@Column(name = "addedafterrirvalidation")
	public Boolean getAddedAfterRiRValidation() {
		return addedAfterRiRValidation;
	}

	@Column(name = "source")
	@Enumerated(EnumType.STRING)
	public RepairComponentSourceEnum getSource() {
		return source;
	}

	public void setSource(RepairComponentSourceEnum source) {
		this.source = source;
	}

	public void setAddedAfterRiRValidation(Boolean addedAfterRiRValidation) {
		this.addedAfterRiRValidation = addedAfterRiRValidation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "freerepairoperationid", foreignKey = @ForeignKey(name = "freerepaircomponent_freerepairoperation_FK"))
	public FreeRepairOperation getFreeRepairOperation() {
		return freeRepairOperation;
	}

	public void setFreeRepairOperation(FreeRepairOperation freeRepairOperation) {
		this.freeRepairOperation = freeRepairOperation;
	}

	@OneToOne(mappedBy = "freeRepairComponent", cascade = CascadeType.ALL)
	public ValidatedFreeRepairComponent getValidatedFreeRepairComponent() {
		return validatedFreeRepairComponent;
	}

	public void setValidatedFreeRepairComponent(ValidatedFreeRepairComponent validatedFreeRepairComponent) {
		this.validatedFreeRepairComponent = validatedFreeRepairComponent;
	}

	@OneToOne()
	@JoinColumn(name = "componentid", foreignKey = @ForeignKey(name = "freerepaircomponent_component_FK"))
	public Component getComponent() {
		return component;
	}

	public void setComponent(Component component) {
		this.component = component;
	}

}