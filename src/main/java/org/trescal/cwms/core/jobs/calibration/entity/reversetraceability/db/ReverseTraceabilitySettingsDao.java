package org.trescal.cwms.core.jobs.calibration.entity.reversetraceability.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.calibration.dto.ReverseTraceabilitySettingsDTO;
import org.trescal.cwms.core.jobs.calibration.entity.reversetraceability.ReverseTraceabilitySettings;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface ReverseTraceabilitySettingsDao extends BaseDao<ReverseTraceabilitySettings, Integer> {

	List<ReverseTraceabilitySettingsDTO> getReverseTraceabilitySettingsFormSubdivIds(List<Integer> subdivIds);

	Map<Integer, LocalDate> getSubdivsAndDateFromReverseTraceability();
}
