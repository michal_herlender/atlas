package org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;

@Entity
@Table(name = "collectedinstrument")
public class CollectedInstrument extends Auditable
{
	private Contact collectedBy;

	private Date collectStamp;

	private int id;

	private Instrument inst;
	private InstrumentModel model;
	private Integer numItems;
	private Schedule schedule;

	// private Instrument ins;

	public CollectedInstrument()
	{
	}

	public CollectedInstrument(Schedule schedule, InstrumentModel model, Instrument ins, Integer numItems)
	{
		this.schedule = schedule;
		this.model = model;
		// this.ins = ins;
		this.numItems = numItems;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "collectedby")
	public Contact getCollectedBy()
	{
		return this.collectedBy;
	}

	@Column(name = "collectstamp")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCollectStamp()
	{
		return this.collectStamp;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	/*
	 * public Instrument getIns() { return this.ins; }
	 */

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid")
	public Instrument getInst()
	{
		return this.inst;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelid")
	public InstrumentModel getModel()
	{
		return this.model;
	}

	@Column(name = "numitems")
	@Type(type = "int")
	public Integer getNumItems()
	{
		return this.numItems;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scheduleid")
	public Schedule getSchedule()
	{
		return this.schedule;
	}

	public void setCollectedBy(Contact collectedBy)
	{
		this.collectedBy = collectedBy;
	}

	public void setCollectStamp(Date collectStamp)
	{
		this.collectStamp = collectStamp;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	/*
	 * public void setIns(Instrument ins) { this.ins = ins; }
	 */

	public void setInst(Instrument inst)
	{
		this.inst = inst;
	}

	public void setModel(InstrumentModel model)
	{
		this.model = model;
	}

	public void setNumItems(Integer numItems)
	{
		this.numItems = numItems;
	}

	public void setSchedule(Schedule schedule)
	{
		this.schedule = schedule;
	}

}