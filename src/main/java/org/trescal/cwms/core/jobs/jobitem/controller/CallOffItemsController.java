package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.CallOffItem;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.CallOffItemDTOComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.db.CallOffItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.dto.CallOffItemDTO;
import org.trescal.cwms.core.jobs.jobitem.form.CallOffItemsForm;
import org.trescal.cwms.core.jobs.jobitem.form.CallOffItemsValidator;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_SUBDIV,
		Constants.SESSION_ATTRIBUTE_USERNAME })
public class CallOffItemsController {

	@Autowired
	private CallOffItemsValidator validator;
	@Autowired
	private CallOffItemService callOffItemService;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private UserService userService;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("form")
	public CallOffItemsForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		CallOffItemsForm form = new CallOffItemsForm();
		form = new CallOffItemsForm();
		form.setAllocatedSubdivId(subdivDto.getKey());
		return form;
	}

	@RequestMapping(value = "/calloffitems.htm", method = RequestMethod.GET)
	public String referenceData(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto, Locale locale) {

		Company allocatedCompany = this.companyService.get(companyDto.getKey());

		Set<CallOffItemDTO> items = callOffItemService.getAllActiveCallOffItems(locale);
		Map<Integer, Map<Integer, Set<CallOffItemDTO>>> itemsMap = getItemsMap(items);

		Set<Integer> companyIds = itemsMap.keySet();
		List<CompanySettingsForAllocatedCompany> settings = this.companySettingsService.getAll(companyIds,
				allocatedCompany);
		model.addAttribute("companyActive", getSettingsActive(companyIds, settings));
		model.addAttribute("companyOnStop", getSettingsOnStop(companyIds, settings));
		model.addAttribute("itemsMap", itemsMap);
		model.addAttribute("itemsCount", items.size());
		model.addAttribute("companyCount", companyIds.size());
		model.addAttribute("actions", CallOffItemsForm.Action.values());

		return "trescal/core/jobs/jobitem/calloffitems";
	}

	@RequestMapping(value = "/calloffitems.htm", method = RequestMethod.POST)
	public String onSubmit(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@Valid @ModelAttribute("form") CallOffItemsForm form, BindingResult bindingResult, Locale locale)
			throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData(model, companyDto, locale);
		}
		if (form.getAction().equals(CallOffItemsForm.Action.NEW_JOB)) {
			int coid = form.getCoid();
			String itemsParam = "";
			if (form.getItemIds() != null) {
				for (Integer i : form.getItemIds())
					itemsParam = itemsParam + i + ",";
				itemsParam = itemsParam.substring(0, (itemsParam.length() - 1));
			}
			return "redirect:addjob.htm?calloffcoid=" + coid + "&items=" + itemsParam;
		} else if (form.getAction().equals(CallOffItemsForm.Action.REACTIVATE_ITEMS)) {
			Contact contact = this.userService.get(username).getCon();
			// All the items will be on the same job for reactivation, and at
			// least one must be selected (validation constraints)
			CallOffItem firstItem = this.callOffItemService.get(form.getItemIds().get(0));
			int jobid = firstItem.getOffItem().getJob().getJobid();
			callOffItemService.reactivateCallOffItems(form.getItemIds(), contact);
			return "redirect:viewjob.htm?jobid=" + jobid;
		} else {
			throw new RuntimeException("Invalid action " + form.getAction());
		}
	}

	private Map<Integer, Boolean> getSettingsActive(Set<Integer> coids,
			List<CompanySettingsForAllocatedCompany> settings) {
		Map<Integer, Boolean> result = new HashMap<>();
		coids.stream().forEach(coid -> result.put(coid, false));
		settings.stream().forEach(setting -> result.put(setting.getCompany().getCoid(), setting.isActive()));
		return result;
	}

	private Map<Integer, Boolean> getSettingsOnStop(Set<Integer> coids,
			List<CompanySettingsForAllocatedCompany> settings) {
		Map<Integer, Boolean> result = new HashMap<>();
		coids.stream().forEach(coid -> result.put(coid, false));
		settings.stream().forEach(setting -> result.put(setting.getCompany().getCoid(), setting.isOnStop()));
		return result;
	}

	private Map<Integer, Map<Integer, Set<CallOffItemDTO>>> getItemsMap(Set<CallOffItemDTO> items) {

		Map<Integer, Map<Integer, Set<CallOffItemDTO>>> companyMap = new LinkedHashMap<>();

		List<CallOffItemDTO> sortedItems = items.stream().sorted(new CallOffItemDTOComparator())
				.collect(Collectors.toList());

		for (CallOffItemDTO callOffItem : sortedItems) {

			Integer coid = callOffItem.getCoid();
			Integer subdivid = callOffItem.getSubdivId();
			if (!companyMap.containsKey(coid)) {
				companyMap.put(coid, new TreeMap<>());
			}
			Map<Integer, Set<CallOffItemDTO>> subdivMap = companyMap.get(coid);

			if (!subdivMap.containsKey(subdivid)) {
				subdivMap.put(subdivid, new TreeSet<CallOffItemDTO>(new CallOffItemDTOComparator()));
			}
			subdivMap.get(subdivid).add(callOffItem);
		}

		return companyMap;
	}

}
