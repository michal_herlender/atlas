package org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Entity
@Table(name="onbehalfitem")
public class OnBehalfItem extends Auditable
{
	private Address address;
	private Company company;
	private int id;
	private JobItem jobitem;

	private Contact setBy;
	private Date setOn;

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "addressid", unique = false, nullable = true)
	public Address getAddress()
	{
		return this.address;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "coid", unique = false, nullable = false)
	public Company getCompany()
	{
		return this.company;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid", nullable = false, unique=true)
	public JobItem getJobitem()
	{
		return this.jobitem;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "setby", nullable = false)
	public Contact getSetBy()
	{
		return this.setBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "seton", nullable = false)
	public Date getSetOn()
	{
		return this.setOn;
	}

	public void setAddress(Address address)
	{
		this.address = address;
	}

	public void setCompany(Company company)
	{
		this.company = company;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setJobitem(JobItem jobitem)
	{
		this.jobitem = jobitem;
	}

	public void setSetBy(Contact setBy)
	{
		this.setBy = setBy;
	}

	public void setSetOn(Date setOn)
	{
		this.setOn = setOn;
	}
}