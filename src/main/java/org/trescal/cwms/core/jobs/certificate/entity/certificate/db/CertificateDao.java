package org.trescal.cwms.core.jobs.certificate.entity.certificate.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.dto.CertificateForInstrumentTooltipDto;
import org.trescal.cwms.core.jobs.certificate.dto.CertificateProjectionDTO;
import org.trescal.cwms.core.jobs.certificate.dto.CertificateSearchWrapper;
import org.trescal.cwms.core.jobs.certificate.dto.WebCertificateInstrumentLinkProjectionDTO;
import org.trescal.cwms.core.jobs.certificate.dto.WebCertificateJobItemLinkProjectionDTO;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.form.CertificateSearchForm;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface CertificateDao extends BaseDao<Certificate, Integer> {

	void delayCertificate(Certificate c);

	Certificate findCertificateByCertNo(String certno);

	List<Certificate> getAllCertificatesAwaitingSignature();

	List<Certificate> getAllCertsForJob(Job job);

	String getReservedCertificateNumber(Integer jobItemId);

	void searchCertificatesJPA(CertificateSearchForm form, PagedResultSet<CertificateSearchWrapper> prs,
			boolean includeDeletedAndCancelledCertificates);

	void signCertificate(Certificate cert);

	PagedResultSet<Certificate> getCertificatesByRelatedInstrument(Integer plantid, Integer resultsPerPage,
			Integer currentPage);

	List<Certificate> getCertificateListByJobitemId(Integer jobitemid);

	List<Certificate> getJobCertsForInstrument(Instrument instrument);

	List<Certificate> getNonJobCertsForInstrument(Instrument instrument);

	Certificate getByThirdCertNo(String thirdCertificateNumber);

	List<Certificate> getByThirdCertNoAndPlantId(String thirdCertificateNumber, Integer plantId);

	Certificate getMostRecentCertificateForJobItem(Integer jobItemId);

	List<Certificate> getByThirdCertNoAndJobId(String thirdCertificateNumber, int jobid);

	List<CertificateProjectionDTO> getCertificatesForJobItemIds(Collection<Integer> jobItemIds);

	Integer getSubdivIdFromCalibration(int certid);

	List<CertificateForInstrumentTooltipDto> findCertificatesForInstrumentTooltipByPlantId(Integer plantId);

	void searchCertificatesProjectionJPA(CertificateSearchForm form, PagedResultSet<CertificateSearchWrapper> prs,
			boolean includeDeletedAndCancelledCertificates);

	List<WebCertificateJobItemLinkProjectionDTO> searchCertificateJobItemLinksProjectionJPA(List<Integer> certificateIds, Locale locale);

	List<WebCertificateInstrumentLinkProjectionDTO> searchCertificateInstrumentLinksProjectionJPA(List<Integer> certificateIds, Locale locale);

}