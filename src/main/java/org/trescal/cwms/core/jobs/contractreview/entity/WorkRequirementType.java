package org.trescal.cwms.core.jobs.contractreview.entity;

public enum WorkRequirementType {
	
	ADJUSTMENT("ADJ"),
	CALIBRATION("CAL"),
	REPAIR("REP"),
	THIRD_PARTY("TP"),
	ONSITE("SIT"),
	GENERAL_SERVICE_OPERATION("GSO");

	private String shortname;

	WorkRequirementType(String shortname) {
		this.shortname = shortname;
	}

	public String getShortname() {
		return this.shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

}
