package org.trescal.cwms.core.jobs.job.entity.po.db;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.dto.ClientPurchaseOrderDTO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.projection.POProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.db.JobItemPOService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.logistics.dto.PoDTO;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.system.Constants;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service("POService")
public class POServiceImpl extends BaseServiceImpl<PO, Integer> implements POService {

	@Autowired
	private PODao pODao;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private JobItemPOService jipoServ;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private JobService jobServ;
	@Autowired
	private UserService userService;

	@Override
	protected BaseDao<PO, Integer> getBaseDao() {
		return pODao;
	}

	@Override
	public PO createPO(Integer jobId, String poNumber, String comment, Integer allocatedCompanyId) {
		Job job = jobServ.get(jobId);
		Company allocatedCompany = companyService.get(allocatedCompanyId);
		PO po = new PO();
		po.setActive(true);
		po.setJob(job);
		po.setPoNumber(poNumber);
		po.setComment(comment);
		po.setOrganisation(allocatedCompany);
		this.save(po);
		return po;
	}

	@Override
	public ResultWrapper deactivatePOById(int poid, HttpSession session) {
		// get the purchase order
		PO po = this.get(poid);
		// purchase order found?
		if (po != null) {
			if (po.getJob().getDefaultPO() != null) {
				if (po.getPoId() == po.getJob().getDefaultPO().getPoId()) {
					Job j = this.jobServ.get(po.getJob().getJobid());
					j.setDefaultPO(null);
					this.jobServ.merge(j);
				}
			}
			// get the current contact
			String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
			Contact currentcon = this.userService.getEagerLoad(username).getCon();
			// current contact found?
			if (currentcon != null) {
				// remove all jipo's
				for (JobItemPO jipo : po.getJobItemPOs()) {
					// remove jipo from job item
					jipo.getItem().getItemPOs().remove(jipo);
					// remove all job item purchase orders
					this.jipoServ.deleteJobItemPO(jipo);
				}
				po.setJobItemPOs(null);
				// deactivate the purchase order
				po.setActive(false);
				po.setDeactivatedBy(currentcon);
				po.setDeactivatedTime(new Date());
				// return successful result
				return new ResultWrapper(true, "", po, null);
			} else {
				return new ResultWrapper(false, "The current contact could not be found", null, null);
			}
		} else {
			return new ResultWrapper(false, "Job item purchase order could not be found", null, null);
		}
	}

	@Override
	public void deactivatePO(Integer poId, String userName) {
		Contact contact = userService.get(userName).getCon();
		PO po = this.get(poId);
		if (po.equals(po.getJob().getDefaultPO()))
			po.getJob().setDefaultPO(null);
		// Remove all linked items
		po.getJobItemPOs().clear();
		po.getExpenseItemPOs().clear();
		po.setActive(false);
		po.setDeactivatedBy(contact);
		po.setDeactivatedTime(new Date());
		this.merge(po);
	}

	public void deletePOById(int poid) {
		PO po = this.get(poid);
		if (po.getJob().getDefaultPO() != null) {
			if (po.getPoId() == po.getJob().getDefaultPO().getPoId()) {
				Job j = this.jobServ.get(po.getJob().getJobid());
				j.setDefaultPO(null);
				this.jobServ.merge(j);
			}
		}
		this.delete(po);
	}

	public ResultWrapper editPO(int jobid, int poid, Integer[] poJIs, String ponumber, String comment) {
		// get po entity
		PO poToEdit = this.get(poid);
		// po null?
		if (poToEdit != null) {
			// update job items assigned to po
			ResultWrapper result = this.jipoServ.assignJobItemsToPO(poJIs, poid);
			// job item update successful?
			if (result.isSuccess()) {
				// update po number and comment
				poToEdit.setPoNumber(ponumber);
				poToEdit.setComment(comment);
				// update purchase order
				poToEdit = this.merge(poToEdit);
				// return successful update
				return new ResultWrapper(true, "", poToEdit, null);
			} else {
				// return un-successful update
				return new ResultWrapper(false, "Purchase order job items could not be updated", null, null);
			}
		} else {
			// return un-successful update
			return new ResultWrapper(false, "Purchase order could not be found", null, null);
		}
	}

	@Override
	public void addPOsToJob(List<Integer> poIds, List<String> poNumbers, List<String> poComments, Job job,
			int defaultPO) {
		int i = 1;
		if (job.getPOs() == null)
			job.setPOs(new ArrayList<>());
		if (CollectionUtils.isNotEmpty(poNumbers)) {
			for (String s : poNumbers) {
				PO po;
				if (poIds == null || poIds.size() < i || poIds.get(i - 1).equals(0)) {
					po = new PO();
					po.setActive(true);
					po.setPoNumber(s);
					if (poComments != null && poComments.size() >= i)
						po.setComment(poComments.get(i - 1));
					po.setJob(job);
					po.setOrganisation(job.getOrganisation().getComp());

				} else {
					po = pODao.find(poIds.get(i - 1));
					po.setJob(job);
					// po.setPrebooking(null);
				}
				job.getPOs().add(po);
				if (i == defaultPO) {
					job.setDefaultPO(po);
					po.setDefaultForJob(job);
					po.setDefaultForPrebooking(null);
				}
				i++;
			}

		}
		jobServ.merge(job);
	}

	public void updateJobPODefault(int jobid, int poid) {
		Job j = this.jobServ.get(jobid);
		PO po = this.get(poid);

		if (po.getJob().getJobid() == j.getJobid()) {
			j.setDefaultPO(po);
			this.jobServ.merge(j);
		}
	}

	@Override
	public void addPOsToPrebookingJob(List<String> poNumbers, List<String> poComments, Asn prebookingJob, int defaultPO,
			Company businessCompany) {
		int i = 1;
		if (prebookingJob.getPrebookingJobDetails().getPoList() == null)
			prebookingJob.getPrebookingJobDetails().setPoList(new ArrayList<PO>());
		if (poNumbers != null) {
			for (String s : poNumbers) {
				PO po = new PO();
				po.setActive(true);
				po.setPoNumber(s);
				if (poComments != null && !poComments.isEmpty())
					po.setComment(poComments.get(i - 1));
				po.setPrebooking(prebookingJob.getPrebookingJobDetails());
				po.setOrganisation(businessCompany);
				prebookingJob.getPrebookingJobDetails().getPoList().add(po);
				if (i == defaultPO) {
					prebookingJob.getPrebookingJobDetails().setDefaultPO(po);
					po.setDefaultForPrebooking(prebookingJob.getPrebookingJobDetails());
				}
				i++;
			}
		}

	}

	@Override
	public List<PoDTO> getPOsByPrebooking(Integer prebookingdetailsid) {
		return pODao.getPOsByPrebooking(prebookingdetailsid);
	}

	@Override
	public List<ClientPurchaseOrderDTO> getProjectionByJob(Integer jobId) {
		return pODao.getProjectionByJob(jobId);
	}

	@Override
	public List<POProjectionDTO> getPOsByJobIds(Collection<Integer> jobIds) {
		return this.pODao.getPOsByJobIds(jobIds);
	}

	@Override
	public List<PO> searchMatchingJobPOs(int jobId, String partPoNumber) {
		return pODao.searchMatchingJobPOs(jobId, partPoNumber);
	}

	@Override
	public PO getMatchingJobPO(int jobId, String poNumber) {
		return pODao.getMatchingJobPO(jobId, poNumber);
	}
}