package org.trescal.cwms.core.jobs.repair.dto;

public class ComponentJsonDto {

	private Integer id;
	private String displayName;
	private String name;

	public ComponentJsonDto(Integer id, String displayName, String name) {
		this.id = id;
		this.displayName = displayName;
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
