package org.trescal.cwms.core.jobs.calibration.form.validator;

import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.jobs.calibration.form.ImportCalibrationsForm;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class ImportCalibrationsFormValidator extends AbstractBeanValidator {

	@Autowired
	private JobService jobService;
	@Autowired
	private SubdivService subdivService;

	// 50MB
	private static final long MAX_IN_BYTES = 52428800;
	private static final String EXCEL_EXTENSION_1 = "xlsx";
	private static final String EXCEL_EXTENSION_2 = "xls";

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(ImportCalibrationsForm.class);
	}

	@Override
	public void validate(Object target, Errors errors, Object... validationHints) {

		ImportCalibrationsForm form = (ImportCalibrationsForm) target;
		super.validate(form, errors);

		Subdiv subdiv = subdivService.get(form.getSubdivid());
		Integer allocatedSubdivId = (Integer) validationHints[0];
		Subdiv allocatedSubdiv = subdivService.get(allocatedSubdivId);

		// check if job exists
		if (jobService.countActiveBySubdivAndAllocatedSubdiv(subdiv, allocatedSubdiv) == 0) {
			errors.rejectValue("jobId", "calibrationimportation.notonsitejobselected");
		}

		MultipartFile file = form.getFile();
		// validate file
		if (file == null || file.isEmpty()) {
			errors.rejectValue("file", "error.instrument.upload.file.empty");
		} else if (!EXCEL_EXTENSION_1.equalsIgnoreCase((FilenameUtils.getExtension(file.getOriginalFilename())))
				&& !EXCEL_EXTENSION_2.equalsIgnoreCase((FilenameUtils.getExtension(file.getOriginalFilename())))) {
			errors.rejectValue("file", "error.instrument.upload.file.notexcel");
			return;
		} else {
			// try to open the file
			try {
				// For .xlsx
				if (POIXMLDocument.hasOOXMLHeader(file.getInputStream())) {
					XSSFWorkbook wb = new XSSFWorkbook(file.getInputStream());
				}
				// For .xls
				else if (POIFSFileSystem.hasPOIFSHeader(file.getInputStream())) {
					HSSFWorkbook wb = new HSSFWorkbook(file.getInputStream());
				} else {
					errors.rejectValue("file", "error.instrument.upload.file.cannotbeopened");
				}
			} catch (IOException e) {
				e.printStackTrace();
				errors.rejectValue("file", "error.instrument.upload.file.cannotbeopened");
			}
		}

		// check max file size
		if (file.getSize() > MAX_IN_BYTES) {
			errors.rejectValue("file", "error.instrument.upload.file.maxsizeexceeded");
		}

	}

}
