package org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

import javax.persistence.*;

@Entity
@Table(name = "tpinstruction")
public class TPInstruction extends Auditable {
	private boolean active;
	private int id;
	private String instruction;
	private JobItem ji;
	private Contact recordedBy;
	private Boolean showOnTPDelNote;

	public TPInstruction()
	{
		this.active = true;
	}

	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	/**
	 * @return the instruction
	 */
	@Length(max = 1000)
	@Column(name = "instruction", length = 1000)
	public String getInstruction()
	{
		return this.instruction;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid")
	public JobItem getJi()
	{
		return this.ji;
	}

	/**
	 * @return the recordedBy
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "recordedby")
	public Contact getRecordedBy()
	{
		return this.recordedBy;
	}

	@Column(name = "active", columnDefinition="tinyint")
	public boolean isActive()
	{
		return this.active;
	}

	@Column(name = "showontpdelnote", columnDefinition="tinyint")
	public Boolean isShowOnTPDelNote()
	{
		return this.showOnTPDelNote;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @param instruction the instruction to set
	 */
	public void setInstruction(String instruction)
	{
		this.instruction = instruction;
	}

	public void setJi(JobItem ji)
	{
		this.ji = ji;
	}


	/**
	 * @param recordedBy the recordedBy to set
	 */
	public void setRecordedBy(Contact recordedBy)
	{
		this.recordedBy = recordedBy;
	}

	public void setShowOnTPDelNote(Boolean showOnTPDelNote)
	{
		this.showOnTPDelNote = showOnTPDelNote;
	}
}