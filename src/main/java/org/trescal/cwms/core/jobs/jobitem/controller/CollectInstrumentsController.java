package org.trescal.cwms.core.jobs.jobitem.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.db.CollectedInstrumentService;
import org.trescal.cwms.core.jobs.jobitem.form.CollectInstrumentsForm;
import org.trescal.cwms.core.jobs.jobitem.form.CollectInstrumentsValidator;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class CollectInstrumentsController
{
	@Autowired
	private CollectedInstrumentService ciServ;
	@Autowired
	private CollectInstrumentsValidator validator;
	@Autowired
	private UserService userService;
	
	@InitBinder("form")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
	
	@ModelAttribute("form")
	protected CollectInstrumentsForm formBackingObject() throws Exception
	{
		return new CollectInstrumentsForm();
	}
	
	@RequestMapping(value="/collectinstruments.htm", method=RequestMethod.POST)
	protected RedirectView onSubmit(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@Valid @ModelAttribute("form") CollectInstrumentsForm form, BindingResult result) throws Exception
	{
		Contact contact = this.userService.get(username).getCon();
		this.ciServ.collectInstruments(form.getPlantIds(), contact);
		return new RedirectView("viewcollectedinstruments.htm");
	}
	
	@RequestMapping(value="/collectinstruments.htm", method=RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/jobs/jobitem/collectinstruments";
	}
}