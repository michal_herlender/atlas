package org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.JobInstructionLink;

public interface JobInstructionLinkDao extends BaseDao<JobInstructionLink, Integer>
{
	List<JobInstructionLink> getAllForJob(Job job);
}
