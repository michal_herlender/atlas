package org.trescal.cwms.core.jobs.job.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientPurchaseOrderItemDTO {

	private Integer itemNo;
	private String instrument;
	private String serialNo;
	private String plantNo;
	private String serviceType;
}