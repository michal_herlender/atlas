package org.trescal.cwms.core.jobs.repair.operation;

import java.util.Comparator;

public class FreeRepairOperationComparator implements Comparator<FreeRepairOperation> {

	@Override
	public int compare(FreeRepairOperation o1, FreeRepairOperation o2) {
		return o1.getPosition() - o2.getPosition();
	}

}
