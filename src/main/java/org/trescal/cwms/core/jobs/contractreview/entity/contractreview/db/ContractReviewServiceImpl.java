package org.trescal.cwms.core.jobs.contractreview.entity.contractreview.db;

import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.db.ContractService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.external.adveso.entity.notificationsystemfieldchangemodel.db.NotificationSystemFieldChangeModelService;
import org.trescal.cwms.core.external.adveso.enums.NotificationSystemFieldTypeEnum;
import org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemand.JobItemDemand;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemand;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemandStatus;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItemComparator;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db.WorkRequirementService;
import org.trescal.cwms.core.jobs.contractreview.projection.ContractReviewProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.entity.requirement.Requirement;
import org.trescal.cwms.core.jobs.jobitem.entity.requirement.RequirementComparator;
import org.trescal.cwms.core.jobs.jobitem.form.JIContractReviewForm;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemPriceProjectionDto;
import org.trescal.cwms.core.jobs.jobitemhistory.JobItemHistory;
import org.trescal.cwms.core.jobs.jobitemhistory.db.JobItemHistoryService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.presetcomment.db.PresetCommentService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_PerformContractReview;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.spring.model.KeyValue;

import lombok.val;

@Service("ContractReviewService")
public class ContractReviewServiceImpl extends BaseServiceImpl<ContractReview, Integer>
	implements ContractReviewService {

	private static final Logger logger = LoggerFactory.getLogger(ContractReviewServiceImpl.class);

	@Value("#{props['cwms.config.costs.invoices.roundupnearest50']}")
	private boolean roundUpFinalCosts;
	@Autowired
	private ContractReviewDao contractReviewDao;
	@Autowired
	private HookInterceptor_PerformContractReview interceptor_performContractReview;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private ActionOutcomeService actOutServ;
	@Autowired
	private PresetCommentService presetCommentServ;
	@Autowired
	private UserService userService;
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private LinkToAdveso linkToAdvesoSettings;
	@Autowired
	private TransportOptionService transOptServ;
	@Autowired
	private ContractService contractService;
	@Autowired
	private NotificationSystemFieldChangeModelService notificationSystemFieldChangeModelService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ContractReviewService contractRevServ;
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private WorkRequirementService workRequirementService;
	@Autowired
	private JobItemHistoryService jobItemHistoryService;
	@Override
	protected BaseDao<ContractReview, Integer> getBaseDao() {
		return contractReviewDao;
	}

	@Override
	public void saveAndInsertContractReviewActivity(ContractReview contractReview, Integer timeSpent) {
		saveAndInsertContractReviewActivity(contractReview, timeSpent, null, null, null);
	}

	@Override
	public void saveAndInsertContractReviewActivity(ContractReview contractReview, Integer timeSpent, Date startDate,
			Date endDate, Contact contact) {

		// Save
		contractReviewDao.persist(contractReview);

		// set latest contract review on job items
		contractReview.getItems().forEach(cri -> cri.getJobitem().setLastContractReviewBy(contractReview.getReviewBy()));

		HookInterceptor_PerformContractReview.Input input = new HookInterceptor_PerformContractReview.Input(
				contractReview, timeSpent, startDate, endDate, contact);
		interceptor_performContractReview.recordAction(input);
	}

	@Override
	public List<ContractReview> getEagerContractReviewsForJob(int jobid) {
		return contractReviewDao.getEagerContractReviewsForJob(jobid);
	}

	@Override
	public ContractReview getMostRecentContractReviewForJobitem(JobItem jobItem) {
		return this.contractReviewDao.getMostRecentContractReviewforJobItem(jobItem);
	}

	@Override
	public List<JobItemPriceProjectionDto> getPriceProjectionDTOs(Collection<Integer> jobItemIds) {
		List<JobItemPriceProjectionDto> result = Collections.emptyList();
		if (!jobItemIds.isEmpty()) {
			result = this.contractReviewDao.getPriceProjectionDTOs(jobItemIds);
		}
		return result;
	}

	@Override
	public List<ContractReviewProjectionDTO> getProjectionDTOsForJobItem(Integer jobItemId) {
		return this.contractReviewDao.getProjectionDTOsForJobItem(jobItemId);
	}

	@Override
	public JobItem contractReview(JIContractReviewForm form, KeyValue<Integer, String> subdivDto, String username) {
		Contact contact = this.userService.get(username).getCon();
		// always save this jobitem first of all
		JobItem ji = form.getJi();
		CalibrationType ct = this.calTypeServ.find(form.getCalType());
		ji.setServiceType(ct.getServiceType());
		ji.setReturnOption(this.transOptServ.get(form.getReturnMethodId()));
		ji.setInOption(this.transOptServ.get(form.getInMethodId()));
		if (form.getContractId() != null && form.getContractId() > 0) {
			Contract contract = contractService.get(form.getContractId());
			contractService.linkJobItemToContract(ji, contract);
		} else
			ji.setContract(null);
		if (form.getActionOutcomeId() != null) {
			ji.setConRevOutcome(this.actOutServ.get(form.getActionOutcomeId()));
		}
		List<JobItemDemand> jobItemDemands = ji.getAdditionalDemands();
		for (AdditionalDemand demand : AdditionalDemand.values()) {
			Optional<JobItemDemand> jobItemDemand = jobItemDemands.stream()
					.filter(jid -> jid.getAdditionalDemand().equals(demand)).findFirst();
			if (jobItemDemand.isPresent()) {
				if (!jobItemDemand.get().getAdditionalDemandStatus().equals(AdditionalDemandStatus.DONE))
					jobItemDemand.get().setAdditionalDemandStatus(
							form.getDemands()[demand.ordinal()] ? AdditionalDemandStatus.REQUIRED
									: AdditionalDemandStatus.NOT_REQUIRED);
			} else {
				JobItemDemand jid = new JobItemDemand();
				jid.setJobItem(ji);
				jid.setAdditionalDemand(demand);
				jid.setAdditionalDemandStatus(form.getDemands()[demand.ordinal()] ? AdditionalDemandStatus.REQUIRED
						: AdditionalDemandStatus.NOT_REQUIRED);
				jobItemDemands.add(jid);
			}
		}
		// client receipt date
		if (form.getClientReceiptDate() != null)
			ji.setClientReceiptDate(
				DateTools.localDateTimeToZonedDateTime(form.getClientReceiptDate()));
		// client ref
		ji.setClientRef(form.getClientReference());
		// update default service type on instrument
		ji.getInst().setDefaultServiceType(ji.getServiceType());
		// perform any cost updates (calculation of totals, discounts etc)
		CostCalculator.updateSingleCost(ji.getAdjustmentCost(), this.roundUpFinalCosts);
		CostCalculator.updateSingleCost(ji.getCalibrationCost(), this.roundUpFinalCosts);
		CostCalculator.updateSingleCost(ji.getRepairCost(), this.roundUpFinalCosts);
		CostCalculator.updateSingleCost(ji.getPurchaseCost(), this.roundUpFinalCosts);

		Company BusinessCompany = ji.getJob().getOrganisation().getComp();
		Subdiv jobSubdiv = ji.getJob().getCon().getSub();
		boolean isLinkedToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(jobSubdiv.getSubdivid(),
			BusinessCompany.getCoid(), new Date());

		// if either of the old value or the new agreedDelDate is null (XOR), or the
		// actual value is
		// changed
		boolean agreedDelDateChanged = (ji.getAgreedDelDate() == null ^ form.getAgreedDelDate() == null)
			|| (ji.getAgreedDelDate() != null && form.getAgreedDelDate() != null
			&& !ji.getAgreedDelDate().equals(form.getAgreedDelDate()));
		// if the turn around is changed
		boolean turnaroundChanged = !ji.getTurn().equals(form.getTurnaround());

		val oldDueDate = ji.getDueDate();

		if (agreedDelDateChanged) {

			if (form.getAgreedDelDate() != null) {

				// notify change
				if (isLinkedToAdveso) {
					notificationSystemFieldChangeModelService.saveAndPublishNotificationFieldChange(ji, JobItem_.agreedDelDate.getName(),
						NotificationSystemFieldTypeEnum.Date,
						form.getAgreedDelDate().format(DateTimeFormatter.ISO_DATE), ji.getDueDate().format(DateTimeFormatter.ISO_DATE));
				}

				JobItemHistory jih = new JobItemHistory(contact, new Date(), ji, form.getAgreedDelDateComment(), true,
						ji.getDueDate(), form.getAgreedDelDate(), JobItem_.agreedDelDate.getName());
				jobItemHistoryService.save(jih);

				ji.setAgreedDelDate(form.getAgreedDelDate());
				ji.setDueDate(form.getAgreedDelDate());

			} else {
				ji.setAgreedDelDate(null);
				if (ji.getJob().getAgreedDelDate() != null) {
					ji.setDueDate(ji.getJob().getAgreedDelDate());
				} else {
					ji.calculateAndSetDueDate();
				}
			}
		}

		if (turnaroundChanged) {
			ji.setTurn(form.getTurnaround());
			if (ji.getAgreedDelDate() == null && ji.getJob().getAgreedDelDate() == null) {
				ji.calculateAndSetDueDate();
			}
		}

		// notify duedate change
		if (isLinkedToAdveso && !ji.getDueDate().equals(oldDueDate)) {
			notificationSystemFieldChangeModelService.saveAndPublishNotificationFieldChange(ji, JobItem_.dueDate.getName(),
				NotificationSystemFieldTypeEnum.Date,
				ji.getDueDate().format(DateTimeFormatter.ISO_DATE), oldDueDate.format(DateTimeFormatter.ISO_DATE));
		}

		// call merge to make this jobitem the persistent entity (and no longer
		// stale)
		ji = this.jobItemService.mergeJobItem(ji);
		if (form.isReserveCertificate()) {
			this.certificateService.createOrUpdateReservedCertificates(ji, contact, form.getCertDate());
		}
		if (form.getSubmit().trim().equalsIgnoreCase("update")) {

			if (StringUtils.isNoneBlank(form.getReviewComments())) {
				ContractReview review = this.contractRevServ.getMostRecentContractReviewForJobitem(ji);
				if (review != null) {
					review.setComments(form.getReviewComments());
					this.contractRevServ.merge(review);
				}
			}
			if (form.isBulkUpdate()) {
				if ((form.getJobItemIds() != null) && !form.getJobItemIds().isEmpty()) {
					this.copyToItem(ji, form, contact, false);
				}
			}
		} else if (form.getSubmit().trim().equalsIgnoreCase("contract review")) {
			// create the contract review
			ContractReview review = new ContractReview();
			review.setComments(form.getReviewComments());
			review.setJob(ji.getJob());
			review.setReviewBy(contact);
			review.setReviewDate(new Date());
			review.setItems(new TreeSet<>(new ContractReviewItemComparator()));
			// add this item to the review
			this.addContractReviewItem(ji, review);
			if (form.isBulkUpdate()) {
				if ((form.getJobItemIds() != null) && (form.getJobItemIds().size() > 0)) {
					logger.debug("Updating " + form.getJobItemIds() + " other selected job items");
					// perform any copying requested
					List<JobItem> items = this.copyToItem(ji, form, contact, true);
					// create any contract reviews requested
					for (JobItem item : items) {
						this.addContractReviewItem(item, review);
						logger.debug("Added contract review to job item with ID " + item.getJobItemId());
					}
				}
			}
			this.saveAndInsertContractReviewActivity(review, null);
		}
		// if user has requested the comment be saved then save it
		if (form.isSaveComments() && form.getReviewComments() != null && form.getReviewComments().trim().length() > 0) {
			PresetComment comment = new PresetComment();
			Subdiv subdiv = this.subdivService.get(subdivDto.getKey());
			comment.setOrganisation(subdiv);
			comment.setType(PresetCommentType.CONTRACT_REVIEW);
			comment.setComment(form.getReviewComments());
			comment.setSeton(new Date());
			comment.setSetby(contact);
			this.presetCommentServ.save(comment);
		}

		return ji;
	}

	/**
	 * Creates a new {@link ContractReviewItem} entity for the given {@link JobItem}
	 * and adds it to the given {@link ContractReview} entitiy.
	 *
	 * @param ji     the {@link JobItem} entity to create a {@link ContractReview}
	 *               for.
	 * @param review the {@link ContractReview} entity to add the item to.
	 */
	private void addContractReviewItem(JobItem ji, ContractReview review) {
		ContractReviewItem crItem = new ContractReviewItem();
		crItem.setJobitem(ji);
		crItem.setReview(review);
		review.getItems().add(crItem);
	}

	/**
	 * Copies an onbehlf client from the copyFrom {@link JobItem} to the copyTo
	 * {@link JobItem}. If copyFrom has no {@link OnBehalfItem} then copyTo's is
	 * nulled and removed.
	 *
	 * @param copyFrom the {@link JobItem} to copy from.
	 * @param copyTo   the {@link JobItem} to copy to.
	 */
	private void copyOnBehalf(JobItem copyFrom, JobItem copyTo, Contact contact) {
		// only try and copy / create if on-behalf exists for the copyFrom item
		if (copyFrom.getOnBehalf() != null) {
			OnBehalfItem from = copyFrom.getOnBehalf();
			OnBehalfItem to = copyTo.getOnBehalf() == null ? new OnBehalfItem() : copyTo.getOnBehalf();
			to.setAddress(from.getAddress());
			to.setCompany(from.getCompany());
			to.setJobitem(copyTo);
			to.setSetBy(contact);
			to.setSetOn(new Date());
			copyTo.setOnBehalf(to);
		} else {
			copyTo.setOnBehalf(null);
		}
		jobItemService.mergeJobItem(copyTo);
	}

	/**
	 * Copies the selected properties from one {@link JobItem} to a number of
	 * others.
	 *
	 * @param copyFrom          the {@link JobItem} to copy the properties from.
	 * @param form              {@link JIContractReviewForm} object containing list
	 *                          of other {@link JobItem} entities + selected
	 *                          properties to copy.
	 * @param contact           the {@link Contact} performing this update.
	 * @param alwaysCopyOutcome whether to always copy outcome (e.g. for contract
	 *                          review)
	 * @return {@link List} of {@link JobItem} entities that had properties copied
	 *         to them.
	 */
	private List<JobItem> copyToItem(JobItem copyFrom, JIContractReviewForm form, Contact contact,
			boolean alwaysCopyOutcome) {
		List<JobItem> otherItems = this.jobItemService.getAllItems(form.getJobItemIds());
		Contract contract = form.getContractId() == 0 ? null : contractService.get(form.getContractId());
		for (JobItem item : otherItems) {
			if (form.isCopyCalType()) {
				item.setServiceType(copyFrom.getServiceType());
				// update default service type on instrument
				item.getInst().setDefaultServiceType(copyFrom.getServiceType());
			}
			if (form.isCopyCleaning())
				item.setReqCleaning(copyFrom.getReqCleaning());
			if (form.isCopyBypassCosting())
				item.setApprovedToBypassCosting(copyFrom.getApprovedToBypassCosting());
			if (form.isCopyTurnaround()) {
				int turn = item.getTurn();
				item.setTurn(copyFrom.getTurn());
				// if turnaround was changed
				if (turn != copyFrom.getTurn()) {
					// re-calculate due date after turnaround has changed
					// if the action is an update add notification
					val oldDueDate = item.getDueDate();
					item.calculateAndSetDueDate();
					Company businessCompany = item.getJob().getOrganisation().getComp();
					Subdiv jobSubdiv = item.getJob().getCon().getSub();
					boolean isLinkedToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
						jobSubdiv.getSubdivid(), businessCompany.getCoid(), new Date());
					if (oldDueDate != null && item.getDueDate().compareTo(oldDueDate) != 0) {
						JobItemHistory jih = new JobItemHistory(contact, new Date(), item, null, true,
							item.getDueDate(), form.getAgreedDelDate(), JobItem_.agreedDelDate.getName());
						jobItemHistoryService.save(jih);
						if (isLinkedToAdveso)
							notificationSystemFieldChangeModelService.saveAndPublishNotificationFieldChange(item, JobItem_.dueDate.getName(),
								NotificationSystemFieldTypeEnum.Date,
								item.getDueDate().format(DateTimeFormatter.ISO_DATE), oldDueDate.format(DateTimeFormatter.ISO_DATE));
					}

				}
			}
			if (form.isCopyRequirements()) {
				for (Requirement r : copyFrom.getReq()) {
					if (!r.isDeleted()) {
						Requirement newReq = new Requirement();
						newReq.setCreated(new Date());
						newReq.setCreatedBy(contact);
						newReq.setDeleted(false);
						newReq.setItem(item);
						newReq.setLabel(r.getLabel());
						newReq.setRequirement(r.getRequirement());
						if (item.getReq() == null)
							item.setReq(new TreeSet<>(new RequirementComparator()));
						item.getReq().add(newReq);
					}
				}
			}
			if (form.isCopyOnBehalf())
				this.copyOnBehalf(copyFrom, item, contact);
			if (form.isCopyTransportIn())
				item.setInOption(copyFrom.getInOption());
			if (form.isCopyTransportOut())
				item.setReturnOption(copyFrom.getReturnOption());
			if (form.isCopyOutcome() || alwaysCopyOutcome)
				item.setConRevOutcome(copyFrom.getConRevOutcome());
			if (form.isCopyCosts())
				this.jobItemService.copyContractReviewCosts(copyFrom, item);
			if (form.isCopyReserveCertificate() && form.isReserveCertificate())
				this.certificateService.createOrUpdateReservedCertificates(item, contact, form.getCertDate());
			if (form.isCopyClientReference())
				item.setClientRef(copyFrom.getClientRef());
			if (form.isCopyWorkRequirements())
				this.workRequirementService.copyWorkRequirements(copyFrom, item);
			this.jobItemService.mergeJobItem(item);
		}
		if (form.isCopyContract())
			if (contract == null)
				otherItems.forEach(jobItem -> contractService.linkJobItemToContract(jobItem, null));
			else
				contractService.updateJobItemsContract(otherItems, contract);
		return otherItems;
	}

	@Override
	public List<ContractReviewProjectionDTO> getCrProjectionDTOsyjob(Integer jobid) {
		return this.contractReviewDao.getCrProjectionDTOsyjob(jobid);
	}

	@Override
	public Long CountCryjob(Integer jobid) {
		return this.contractReviewDao.CountCryjob(jobid);
	}

}