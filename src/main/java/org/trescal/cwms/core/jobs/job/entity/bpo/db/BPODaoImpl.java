package org.trescal.cwms.core.jobs.job.entity.bpo.db;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost_;
import org.trescal.cwms.core.jobs.job.dto.BPOKeyValue;
import org.trescal.cwms.core.jobs.job.dto.BPOPeriodicInvoiceDTO;
import org.trescal.cwms.core.jobs.job.dto.BPOUsageDTO;
import org.trescal.cwms.core.jobs.job.dto.BpoDTO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink_;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO_;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice_;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem_;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem_;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.StringJpaUtils;

import lombok.val;

@Repository("BPODao")
public class BPODaoImpl extends AllocatedToCompanyDaoImpl<BPO, Integer> implements BPODao {

	@Override
	protected Class<BPO> getEntity() {
		return BPO.class;
	}

	public BPO findDWRBPO(int id) {
		return getFirstResult(cb -> {
			CriteriaQuery<BPO> cq = cb.createQuery(BPO.class);
			Root<BPO> bpo = cq.from(BPO.class);
			bpo.fetch(BPO_.company, JoinType.LEFT);
			bpo.fetch(BPO_.subdiv, JoinType.LEFT);
			bpo.fetch(BPO_.contact, JoinType.LEFT);
			cq.where(cb.equal(bpo, id));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<BpoDTO> findAllForJob(Integer jobId, boolean activeOnly) {
		return getResultList(cb -> {
			CriteriaQuery<BpoDTO> cq = cb.createQuery(BpoDTO.class);
			Root<JobBPOLink> jobBpoLink = cq.from(JobBPOLink.class);
			Join<JobBPOLink, Job> job = jobBpoLink.join(JobBPOLink_.job);
			Join<Job, Contact> jobContact = job.join(Job_.con);
			Join<Contact, Subdiv> jobSubdiv = jobContact.join(Contact_.sub);
			Join<Job, Subdiv> allocatedSubdiv = job.join(Job_.organisation.getName());
			Join<JobBPOLink, BPO> bpo = jobBpoLink.join(JobBPOLink_.bpo);
			Subquery<Long> jobItemCountSq = cq.subquery(Long.class);
			Root<JobItemPO> jobItemPO = jobItemCountSq.from(JobItemPO.class);
			Join<JobItemPO, JobItem> jobItem = jobItemPO.join(JobItemPO_.ITEM);
			Predicate jiSqClauses = cb.conjunction();
			jiSqClauses.getExpressions().add(cb.equal(jobItemPO.get(JobItemPO_.bpo), bpo));
			jiSqClauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.job), jobId));
			jobItemCountSq.where(jiSqClauses);
			jobItemCountSq.select(cb.count(jobItemPO.get(JobItemPO_.id)));
			Subquery<Long> expenseItemCountSq = cq.subquery(Long.class);
			Root<JobExpenseItemPO> expenseItemPO = expenseItemCountSq.from(JobExpenseItemPO.class);
			Join<JobExpenseItemPO, JobExpenseItem> expenseItem = expenseItemPO.join(JobExpenseItemPO_.ITEM);
			Predicate eiSqClauses = cb.conjunction();
			eiSqClauses.getExpressions().add(cb.equal(expenseItemPO.get(JobExpenseItemPO_.bpo), bpo));
			eiSqClauses.getExpressions().add(cb.equal(expenseItem.get(JobExpenseItem_.job), jobId));
			expenseItemCountSq.where(eiSqClauses);
			expenseItemCountSq.select(cb.count(expenseItemPO.get(JobExpenseItemPO_.id)));
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(job.get(Job_.jobid), jobId));
			clauses.getExpressions().add(cb.equal(bpo.get(BPO_.organisation), allocatedSubdiv.get(Subdiv_.comp)));
			Predicate customerLink = cb.disjunction();
			customerLink.getExpressions().add(cb.equal(bpo.get(BPO_.company), jobSubdiv.get(Subdiv_.comp)));
			customerLink.getExpressions().add(cb.equal(bpo.get(BPO_.subdiv), jobSubdiv));
			customerLink.getExpressions().add(cb.equal(bpo.get(BPO_.contact), jobContact));
			clauses.getExpressions().add(customerLink);
			if (activeOnly)
				clauses.getExpressions().add(cb.isTrue(bpo.get(BPO_.active)));
			cq.where(clauses);
			cq.select(cb.construct(BpoDTO.class, bpo.get(BPO_.poId), bpo.get(BPO_.poNumber), bpo.get(BPO_.comment),
					bpo.get(BPO_.durationFrom), bpo.get(BPO_.durationTo), bpo.get(BPO_.limitAmount),
					cb.selectCase().when(cb.isNull(bpo.get(BPO_.contact)), cb.literal(false))
							.otherwise(cb.literal(true)),
					cb.selectCase().when(cb.isNull(bpo.get(BPO_.subdiv)), cb.literal(false))
							.otherwise(cb.literal(true)),
					cb.selectCase().when(cb.isNull(bpo.get(BPO_.company)), cb.literal(false))
							.otherwise(cb.literal(true)),
					cb.selectCase().when(cb.isTrue(jobBpoLink.get(JobBPOLink_.defaultForJob)), cb.literal(true))
							.otherwise(cb.literal(false)),
					bpo.get(BPO_.active), jobItemCountSq.getSelection(), expenseItemCountSq.getSelection()))
					.distinct(true);
			return cq;
		});
	}

	@Override
	public List<BpoDTO> getAllByCompany(Company company, Boolean active, Company allocatedCompany) {
		return getResultList(cb -> {
			CriteriaQuery<BpoDTO> cq = cb.createQuery(BpoDTO.class);
			Root<BPO> bpo = cq.from(BPO.class);
			val comp = bpo.join(BPO_.company);
			//Dirty hack as the Hibernate<=5.4.30 does not recognize cb.literal(Scope.COMPANY) as a constructor paramter
			val scope = cb.<Scope>selectCase().when(cb.isNotNull(comp), cb.literal(Scope.COMPANY))
				.otherwise(cb.nullLiteral(Scope.class));
			val currency = comp.join(Company_.currency);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(bpo.get(BPO_.company), company));
			clauses.getExpressions().add(cb.equal(bpo.get(BPO_.organisation), allocatedCompany));
			if (active != null)
				clauses.getExpressions().add(cb.equal(bpo.get(BPO_.active), active));
			cq.where(clauses);
			cq.orderBy(cb.asc(bpo.get(BPO_.poId)));
			cq.select(cb.construct(BpoDTO.class,
				bpo.get(BPO_.poId), bpo.get(BPO_.poNumber), bpo.get(BPO_.comment),
				bpo.get(BPO_.durationFrom), bpo.get(BPO_.durationTo), bpo.get(BPO_.limitAmount),
				comp.get(Company_.coname), cb.literal(""), cb.literal(""),
				cb.literal(false), cb.literal(0), cb.literal(0), bpo.get(BPO_.active),
				currency.get(SupportedCurrency_.currencyERSymbol),
				currency.get(SupportedCurrency_.currencyCode), scope, comp.get(Company_.coid))
			);
			return cq;
		});
	}

	public List<BpoDTO> getAllByContact(Contact contact, Boolean active, Company allocatedCompany) {
		return getResultList(cb -> {
			CriteriaQuery<BpoDTO> cq = cb.createQuery(BpoDTO.class);
			Root<BPO> bpo = cq.from(BPO.class);
			val con = bpo.join(BPO_.contact);
			//Dirty hack as the Hibernate<=5.4.30 does not recognize cb.literal(Scope.COMPANY) as a constructor paramter
			val scope = cb.<Scope>selectCase().when(cb.isNotNull(con), cb.literal(Scope.CONTACT))
				.otherwise(cb.nullLiteral(Scope.class));
			val currency = con.join(Contact_.sub).join(Subdiv_.comp).join(Company_.currency);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(bpo.get(BPO_.contact), contact));
			clauses.getExpressions().add(cb.equal(bpo.get(BPO_.organisation), allocatedCompany));
			if (active != null)
				clauses.getExpressions().add(cb.equal(bpo.get(BPO_.active), active));
			cq.where(clauses);
			cq.orderBy(cb.asc(bpo.get(BPO_.poId)));
			cq.select(cb.construct(BpoDTO.class,
				bpo.get(BPO_.poId), bpo.get(BPO_.poNumber), bpo.get(BPO_.comment),
				bpo.get(BPO_.durationFrom), bpo.get(BPO_.durationTo), bpo.get(BPO_.limitAmount),
				cb.nullLiteral(String.class), cb.nullLiteral(String.class),
				StringJpaUtils.trimAndConcatWithWhitespace(con.get(Contact_.firstName), con.get(Contact_.lastName)).apply(cb),

				cb.literal(false), cb.literal(0), cb.literal(0), bpo.get(BPO_.active),
				currency.get(SupportedCurrency_.currencyERSymbol), currency.get(SupportedCurrency_.currencyCode),
				scope, con.get(Contact_.personid))
			);
			return cq;
		});
	}

	@Override
	public List<BPO> getAllByContactHierarchical(Contact contact, Company allocatedCompany) {
		return getResultList(cb -> {
			CriteriaQuery<BPO> cq = cb.createQuery(BPO.class);
			Root<BPO> bpo = cq.from(BPO.class);
			Predicate clauses = cb.conjunction();
			Predicate orgLevelDisjunction = cb.disjunction();
			orgLevelDisjunction.getExpressions().add(cb.equal(bpo.get(BPO_.contact), contact));
			orgLevelDisjunction.getExpressions().add(cb.equal(bpo.get(BPO_.subdiv), contact.getSub()));
			orgLevelDisjunction.getExpressions().add(cb.equal(bpo.get(BPO_.company), contact.getSub().getComp()));
			clauses.getExpressions().add(orgLevelDisjunction);
			clauses.getExpressions().add(cb.equal(bpo.get(BPO_.organisation), allocatedCompany));
			clauses.getExpressions().add(cb.isTrue(bpo.get(BPO_.active)));
			cq.where(clauses);
			cq.orderBy(cb.asc(bpo.get(BPO_.poId)));
			return cq;
		});
	}

	@Override
	public List<BpoDTO> getAllByContactHierarchicalNotOnJob(Integer personId, Integer allocatedCompanyId,
			Integer jobId) {
		return getResultList(cb -> {
			CriteriaQuery<BpoDTO> cq = cb.createQuery(BpoDTO.class);
			Root<BPO> bpo = cq.from(BPO.class);
			Join<BPO, JobBPOLink> jobLink = bpo.join(BPO_.jobLinks, JoinType.LEFT);
			jobLink.on(cb.equal(jobLink.get(JobBPOLink_.job), jobId));
			Root<Contact> contact = cq.from(Contact.class);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(contact.get(Contact_.personid), personId));
			Predicate orgLevelDisjunction = cb.disjunction();
			orgLevelDisjunction.getExpressions().add(cb.equal(bpo.get(BPO_.contact), contact));
			orgLevelDisjunction.getExpressions().add(cb.equal(bpo.get(BPO_.subdiv), subdiv));
			orgLevelDisjunction.getExpressions().add(cb.equal(bpo.get(BPO_.company), subdiv.get(Subdiv_.comp)));
			clauses.getExpressions().add(orgLevelDisjunction);
			clauses.getExpressions().add(cb.equal(bpo.get(BPO_.organisation), allocatedCompanyId));
			clauses.getExpressions().add(cb.isTrue(bpo.get(BPO_.active)));
			clauses.getExpressions().add(cb.isNull(jobLink.get(JobBPOLink_.id)));
			cq.where(clauses);
			cq.orderBy(cb.asc(bpo.get(BPO_.poId)));
			cq.select(cb.construct(BpoDTO.class, bpo.get(BPO_.poId), bpo.get(BPO_.poNumber), bpo.get(BPO_.comment),
					bpo.get(BPO_.durationFrom), bpo.get(BPO_.durationTo), bpo.get(BPO_.limitAmount),
					cb.selectCase().when(cb.isNull(bpo.get(BPO_.contact)), cb.literal(false))
							.otherwise(cb.literal(true)),
					cb.selectCase().when(cb.isNull(bpo.get(BPO_.subdiv)), cb.literal(false))
							.otherwise(cb.literal(true)),
					cb.selectCase().when(cb.isNull(bpo.get(BPO_.company)), cb.literal(false)).otherwise(
							cb.literal(true)),
					cb.literal(false), bpo.get(BPO_.active), cb.literal(0L), cb.literal(0L))).distinct(true);
			return cq;
		});
	}

	@Override
	public List<BpoDTO> getAllBySubdiv(Subdiv subdiv, Boolean active, Company allocatedCompany) {
		return getResultList(cb -> {
			CriteriaQuery<BpoDTO> cq = cb.createQuery(BpoDTO.class);
			Root<BPO> bpo = cq.from(BPO.class);
			val sub = bpo.join(BPO_.subdiv);
			//Dirty hack as the Hibernate<=5.4.30 does not recognize cb.literal(Scope.COMPANY) as a constructor paramter
			val scope = cb.<Scope>selectCase().when(cb.isNotNull(sub), cb.literal(Scope.SUBDIV))
				.otherwise(cb.nullLiteral(Scope.class));
			val currency = sub.get(Subdiv_.comp).get(Company_.currency);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(bpo.get(BPO_.subdiv), subdiv));
			clauses.getExpressions().add(cb.equal(bpo.get(BPO_.organisation), allocatedCompany));
			if (active != null)
				clauses.getExpressions().add(cb.equal(bpo.get(BPO_.active), active));
			cq.where(clauses);
			cq.orderBy(cb.asc(bpo.get(BPO_.poId)));
			cq.select(cb.construct(BpoDTO.class,
				bpo.get(BPO_.poId), bpo.get(BPO_.poNumber), bpo.get(BPO_.comment),
				bpo.get(BPO_.durationFrom), bpo.get(BPO_.durationTo), bpo.get(BPO_.limitAmount),
				cb.nullLiteral(String.class), sub.get(Subdiv_.subname), cb.nullLiteral(String.class),
				cb.literal(false), cb.literal(0), cb.literal(0),
				bpo.get(BPO_.active),
				currency.get(SupportedCurrency_.currencyERSymbol),
				currency.get(SupportedCurrency_.currencyCode), scope,
				sub.get(Subdiv_.subdivid)
				)
			);
			return cq;
		});
	}

	@Override
	public List<BPO> getExpiringBPOs() {
		return getResultList(cb -> {
			CriteriaQuery<BPO> cq = cb.createQuery(BPO.class);
			Root<BPO> bpo = cq.from(BPO.class);
			bpo.fetch(BPO_.company);
			bpo.fetch(BPO_.subdiv);
			bpo.fetch(BPO_.contact);
			Predicate clauses = cb.conjunction();
			Calendar cal = new GregorianCalendar();
			cal.add(Calendar.DATE, 14);
			Date dateTo = cal.getTime();
			clauses.getExpressions().add(cb.between(bpo.get(BPO_.durationTo), new Date(), dateTo));
			clauses.getExpressions().add(cb.isTrue(bpo.get(BPO_.active)));
			clauses.getExpressions().add(cb.isFalse(bpo.get(BPO_.expirationWarningSent)));
			cq.where(clauses);
			cq.orderBy(cb.desc(bpo.get(BPO_.durationTo)));
			return cq;
		});
	}

	@Override
	public List<BPOPeriodicInvoiceDTO> getPeriodicInvoicesLinkedTo(Integer bpoId) {
		return getResultList(cb -> {
			CriteriaQuery<BPOPeriodicInvoiceDTO> cq = cb.createQuery(BPOPeriodicInvoiceDTO.class);
			Root<JobItemPO> jobItemPO = cq.from(JobItemPO.class);
			Join<JobItemPO, JobItem> linkedJobItem = jobItemPO.join(JobItemPO_.ITEM);
			Join<JobItem, JobItemNotInvoiced> linkedNotInvoiced = linkedJobItem.join(JobItem_.notInvoiced);
			Join<JobItemNotInvoiced, Invoice> periodicInvoice = linkedNotInvoiced
					.join(JobItemNotInvoiced_.periodicInvoice);
			cq.where(cb.equal(jobItemPO.get(JobItemPO_.bpo), bpoId));
			cq.distinct(true);
			cq.select(cb.construct(BPOPeriodicInvoiceDTO.class, periodicInvoice.get(Invoice_.id),
					periodicInvoice.get(Invoice_.invno), periodicInvoice.get(Invoice_.totalCost)));
			return cq;
		});
	}

	@Override
	public List<BPOUsageDTO> getJobItemsOnPeriodicInvoice(Integer invoiceId, Integer bpoId, Locale locale,
			Integer limit) {
		return getResultList(cb -> {
			CriteriaQuery<BPOUsageDTO> cq = cb.createQuery(BPOUsageDTO.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, JobItemNotInvoiced> notInvoiced = jobItem.join(JobItem_.notInvoiced);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			Join<Job, JobStatus> jobStatus = job.join(Job_.js, JoinType.LEFT);
			Join<JobStatus, Translation> jobStatusName = jobStatus.join(JobStatus_.nametranslations, JoinType.LEFT);
			jobStatusName.on(cb.equal(jobStatusName.get(Translation_.locale), locale));
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst, JoinType.LEFT);
			Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model, JoinType.LEFT);
			Join<InstrumentModel, Translation> modelName = model.join(InstrumentModel_.nameTranslations, JoinType.LEFT);
			modelName.on(cb.equal(modelName.get(Translation_.locale), locale));
			Join<JobItem, CalibrationType> calType = jobItem.join(JobItem_.calType, JoinType.LEFT);
			Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType, JoinType.LEFT);
			Join<ServiceType, Translation> serviceTypeShortName = serviceType.join(ServiceType_.shortnameTranslation,
					JoinType.LEFT);
			serviceTypeShortName.on(cb.equal(serviceTypeShortName.get(Translation_.locale), locale));
			// subquery to count items on job
			Subquery<Long> itemCountSq = cq.subquery(Long.class);
			Root<JobItem> itemCount = itemCountSq.from(JobItem.class);
			itemCountSq.where(cb.equal(itemCount.get(JobItem_.job), job));
			itemCountSq.select(cb.count(itemCount));
			// subquery to find price for latest job costing item
			Subquery<BigDecimal> jobCostingItemCostSq = cq.subquery(BigDecimal.class);
			Root<JobCostingItem> latestJobCostingItem = jobCostingItemCostSq.from(JobCostingItem.class);
			Subquery<Integer> latestJobCostingItemSq = jobCostingItemCostSq.subquery(Integer.class);
			Root<JobCostingItem> jobCostingItem = latestJobCostingItemSq.from(JobCostingItem.class);
			latestJobCostingItemSq.where(cb.equal(jobCostingItem.get(JobCostingItem_.jobItem), jobItem));
			latestJobCostingItemSq.select(cb.max(jobCostingItem.get(JobCostingItem_.id)));
			jobCostingItemCostSq.where(
					cb.equal(latestJobCostingItem.get(JobCostingItem_.id), latestJobCostingItemSq.getSelection()));
			jobCostingItemCostSq.select(latestJobCostingItem.get(JobCostingItem_.totalCost));
			// subqueries to find contract review price
			Subquery<BigDecimal> adjustmentCostSq = cq.subquery(BigDecimal.class);
			Root<ContractReviewAdjustmentCost> adjustmentCost = adjustmentCostSq
					.from(ContractReviewAdjustmentCost.class);
			adjustmentCostSq.where(cb.equal(adjustmentCost, jobItem.get(JobItem_.adjustmentCost)));
			adjustmentCostSq.select(adjustmentCost.get(ContractReviewAdjustmentCost_.totalCost));
			Subquery<BigDecimal> calibrationCostSq = cq.subquery(BigDecimal.class);
			Root<ContractReviewCalibrationCost> calibrationCost = calibrationCostSq
					.from(ContractReviewCalibrationCost.class);
			calibrationCostSq.where(cb.equal(calibrationCost, jobItem.get(JobItem_.calibrationCost)));
			calibrationCostSq.select(calibrationCost.get(ContractReviewCalibrationCost_.totalCost));
			Subquery<BigDecimal> repairCostSq = cq.subquery(BigDecimal.class);
			Root<ContractReviewRepairCost> repairCost = repairCostSq.from(ContractReviewRepairCost.class);
			repairCostSq.where(cb.equal(repairCost, jobItem.get(JobItem_.repairCost)));
			repairCostSq.select(repairCost.get(ContractReviewRepairCost_.totalCost));
			Subquery<BigDecimal> purchaseCostSq = cq.subquery(BigDecimal.class);
			Root<ContractReviewPurchaseCost> purchaseCost = purchaseCostSq.from(ContractReviewPurchaseCost.class);
			purchaseCostSq.where(cb.equal(purchaseCost, jobItem.get(JobItem_.purchaseCost)));
			purchaseCostSq.select(purchaseCost.get(ContractReviewPurchaseCost_.totalCost));
			// subquery to figure out whether the item is linked to BPO
			Subquery<Long> itemOnBPOSq = cq.subquery(Long.class);
			Root<JobItemPO> itemOnBPORoot = itemOnBPOSq.from(JobItemPO.class);
			itemOnBPOSq.where(cb.and(cb.equal(itemOnBPORoot.get(JobItemPO_.item), jobItem),
					cb.equal(itemOnBPORoot.get(JobItemPO_.bpo), bpoId)));
			itemOnBPOSq.select(cb.count(itemOnBPORoot));
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(notInvoiced.get(JobItemNotInvoiced_.periodicInvoice), invoiceId));
			cq.where(clauses);
			cq.orderBy(cb.asc(job.get(Job_.jobno)), cb.asc(jobItem.get(JobItem_.itemNo)));
			cq.select(cb.construct(BPOUsageDTO.class, job.get(Job_.jobid), job.get(Job_.jobno),
					jobItem.get(JobItem_.dateIn), jobItem.get(JobItem_.dateComplete),
					jobStatusName.get(Translation_.translation), jobItem.get(JobItem_.jobItemId),
					jobItem.get(JobItem_.itemNo), itemCountSq.getSelection(), modelName.get(Translation_.translation),
					serviceTypeShortName.get(Translation_.translation),
					cb.coalesce(jobCostingItemCostSq.getSelection(), cb.sum(cb.sum(
							cb.sum(cb.coalesce(adjustmentCostSq.getSelection(), cb.literal(BigDecimal.valueOf(0, 2))),
									cb.coalesce(calibrationCostSq.getSelection(),
											cb.literal(BigDecimal.valueOf(0, 2)))),
							cb.coalesce(repairCostSq.getSelection(), cb.literal(BigDecimal.valueOf(0, 2)))),
							cb.coalesce(purchaseCostSq.getSelection(), cb.literal(BigDecimal.valueOf(0, 2))))),
					itemOnBPOSq.getSelection()));
			return cq;
		}, 0, limit);
	}

	@Override
	public List<BPOUsageDTO> getJobItemAmounts(Integer bpoId, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<BPOUsageDTO> cq = cb.createQuery(BPOUsageDTO.class);
			Root<JobItemPO> jobItemPO = cq.from(JobItemPO.class);
			Join<JobItemPO, JobItem> jobItem = jobItemPO.join(JobItemPO_.ITEM);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			Join<Job, JobStatus> jobStatus = job.join(Job_.js, JoinType.LEFT);
			Join<JobStatus, Translation> jobStatusName = jobStatus.join(JobStatus_.nametranslations, JoinType.LEFT);
			jobStatusName.on(cb.equal(jobStatusName.get(Translation_.locale), locale));
			Join<JobItem, JobItemNotInvoiced> notInvoiced = jobItem.join(JobItem_.notInvoiced, JoinType.LEFT);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst, JoinType.LEFT);
			Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model, JoinType.LEFT);
			Join<InstrumentModel, Translation> modelName = model.join(InstrumentModel_.nameTranslations, JoinType.LEFT);
			modelName.on(cb.equal(modelName.get(Translation_.locale), locale));
			Join<JobItem, CalibrationType> calType = jobItem.join(JobItem_.calType, JoinType.LEFT);
			Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType, JoinType.LEFT);
			Join<ServiceType, Translation> serviceTypeShortName = serviceType.join(ServiceType_.shortnameTranslation,
					JoinType.LEFT);
			serviceTypeShortName.on(cb.equal(serviceTypeShortName.get(Translation_.locale), locale));
			// subquery to count items on job
			Subquery<Long> itemCountSq = cq.subquery(Long.class);
			Root<JobItem> itemCount = itemCountSq.from(JobItem.class);
			itemCountSq.where(cb.equal(itemCount.get(JobItem_.job), job));
			itemCountSq.select(cb.count(itemCount));
			// subquery to find price for latest invoice item
			Subquery<BigDecimal> invoiceItemCostSq = cq.subquery(BigDecimal.class);
			Root<InvoiceItem> latestInvoiceItem = invoiceItemCostSq.from(InvoiceItem.class);
			Subquery<Integer> latestInvoiceItemSq = invoiceItemCostSq.subquery(Integer.class);
			Root<InvoiceItem> invoiceItem = latestInvoiceItemSq.from(InvoiceItem.class);
			latestInvoiceItemSq.where(cb.equal(invoiceItem.get(InvoiceItem_.jobItem), jobItem));
			latestInvoiceItemSq.select(cb.max(invoiceItem.get(InvoiceItem_.id)));
			invoiceItemCostSq
					.where(cb.equal(latestInvoiceItem.get(InvoiceItem_.id), latestInvoiceItemSq.getSelection()));
			invoiceItemCostSq.select(latestInvoiceItem.get(InvoiceItem_.totalCost));
			// subquery to find price for latest job costing item
			Subquery<BigDecimal> jobCostingItemCostSq = cq.subquery(BigDecimal.class);
			Root<JobCostingItem> latestJobCostingItem = jobCostingItemCostSq.from(JobCostingItem.class);
			Subquery<Integer> latestJobCostingItemSq = jobCostingItemCostSq.subquery(Integer.class);
			Root<JobCostingItem> jobCostingItem = latestJobCostingItemSq.from(JobCostingItem.class);
			latestJobCostingItemSq.where(cb.equal(jobCostingItem.get(JobCostingItem_.jobItem), jobItem));
			latestJobCostingItemSq.select(cb.max(jobCostingItem.get(JobCostingItem_.id)));
			jobCostingItemCostSq.where(
					cb.equal(latestJobCostingItem.get(JobCostingItem_.id), latestJobCostingItemSq.getSelection()));
			jobCostingItemCostSq.select(latestJobCostingItem.get(JobCostingItem_.totalCost));
			// subqueries to find contract review price
			Subquery<BigDecimal> adjustmentCostSq = cq.subquery(BigDecimal.class);
			Root<ContractReviewAdjustmentCost> adjustmentCost = adjustmentCostSq
					.from(ContractReviewAdjustmentCost.class);
			adjustmentCostSq.where(cb.equal(adjustmentCost, jobItem.get(JobItem_.adjustmentCost)));
			adjustmentCostSq.select(adjustmentCost.get(ContractReviewAdjustmentCost_.totalCost));
			Subquery<BigDecimal> calibrationCostSq = cq.subquery(BigDecimal.class);
			Root<ContractReviewCalibrationCost> calibrationCost = calibrationCostSq
					.from(ContractReviewCalibrationCost.class);
			calibrationCostSq.where(cb.equal(calibrationCost, jobItem.get(JobItem_.calibrationCost)));
			calibrationCostSq.select(calibrationCost.get(ContractReviewCalibrationCost_.totalCost));
			Subquery<BigDecimal> repairCostSq = cq.subquery(BigDecimal.class);
			Root<ContractReviewRepairCost> repairCost = repairCostSq.from(ContractReviewRepairCost.class);
			repairCostSq.where(cb.equal(repairCost, jobItem.get(JobItem_.repairCost)));
			repairCostSq.select(repairCost.get(ContractReviewRepairCost_.totalCost));
			Subquery<BigDecimal> purchaseCostSq = cq.subquery(BigDecimal.class);
			Root<ContractReviewPurchaseCost> purchaseCost = purchaseCostSq.from(ContractReviewPurchaseCost.class);
			purchaseCostSq.where(cb.equal(purchaseCost, jobItem.get(JobItem_.purchaseCost)));
			purchaseCostSq.select(purchaseCost.get(ContractReviewPurchaseCost_.totalCost));
			cq.where(cb.and(cb.equal(jobItemPO.get(JobItemPO_.bpo), bpoId), cb.isNull(notInvoiced)));
			cq.orderBy(cb.asc(job.get(Job_.jobno)), cb.asc(jobItem.get(JobItem_.itemNo)));
			cq.select(
				cb.construct(BPOUsageDTO.class, job.get(Job_.jobid), job.get(Job_.jobno),
					jobItem.get(JobItem_.dateIn), jobItem.get(JobItem_.dateComplete),
					jobStatusName.get(Translation_.translation), jobItem.get(JobItem_.jobItemId),
					jobItem.get(JobItem_.itemNo), itemCountSq
						.getSelection(),
					modelName.get(Translation_.translation), serviceTypeShortName.get(Translation_.translation),
					cb.coalesce(invoiceItemCostSq.getSelection(),
						cb.coalesce(jobCostingItemCostSq.getSelection(), cb.sum(
											cb.sum(cb.sum(
													cb.coalesce(adjustmentCostSq.getSelection(),
															cb.literal(BigDecimal.valueOf(0, 2))),
													cb.coalesce(calibrationCostSq.getSelection(),
															cb.literal(BigDecimal.valueOf(0, 2)))),
													cb.coalesce(repairCostSq.getSelection(),
															cb.literal(BigDecimal.valueOf(0, 2)))),
											cb.coalesce(purchaseCostSq.getSelection(),
													cb.literal(BigDecimal.valueOf(0, 2))))))));
			return cq;
		});
	}

	@Override
	public List<BPOUsageDTO> getExpenseItemAmounts(Integer bpoId, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<BPOUsageDTO> cq = cb.createQuery(BPOUsageDTO.class);
			Root<JobExpenseItemPO> expenseItemPO = cq.from(JobExpenseItemPO.class);
			Join<JobExpenseItemPO, JobExpenseItem> expenseItem = expenseItemPO.join(JobExpenseItemPO_.ITEM);
			Join<JobExpenseItem, Job> job = expenseItem.join(JobExpenseItem_.job);
			Join<Job, JobStatus> jobStatus = job.join(Job_.js);
			Expression<String> jobStatusName = joinTranslation(cb, jobStatus, JobStatus_.nametranslations, locale);
			Subquery<Long> itemCountSq = cq.subquery(Long.class);
			Root<JobExpenseItem> items = itemCountSq.from(JobExpenseItem.class);
			itemCountSq.where(cb.equal(items.get(JobExpenseItem_.job), job));
			itemCountSq.select(cb.count(items));
			Join<JobExpenseItem, InstrumentModel> model = expenseItem.join(JobExpenseItem_.model);
			Expression<String> modelName = joinTranslation(cb, model, InstrumentModel_.nameTranslations, locale);
			cq.where(cb.equal(expenseItemPO.get(JobExpenseItemPO_.bpo), bpoId));
			cq.select(cb.construct(BPOUsageDTO.class, job.get(Job_.jobid), job.get(Job_.jobno),
					expenseItem.get(JobExpenseItem_.date), jobStatusName, expenseItem.get(JobExpenseItem_.id),
					expenseItem.get(JobExpenseItem_.itemNo), itemCountSq.getSelection(), modelName,
					expenseItem.get(JobExpenseItem_.comment), expenseItem.get(JobExpenseItem_.singlePrice),
					expenseItem.get(JobExpenseItem_.quantity)));
			return cq;
		});
	}

	@Override
	public List<Integer> getLinkedToJobsOnInvoice(Integer invoiceId) {
		return getResultList(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<InvoiceJobLink> jobLink = cq.from(InvoiceJobLink.class);
			Join<InvoiceJobLink, Job> job = jobLink.join(InvoiceJobLink_.job);
			Join<Job, JobBPOLink> bpoLink = job.join(Job_.bpoLinks);
			Join<JobBPOLink, BPO> bpo = bpoLink.join(JobBPOLink_.bpo);
			cq.where(cb.equal(jobLink.get(InvoiceJobLink_.invoice), invoiceId));
			cq.distinct(true);
			cq.select(bpo.get(BPO_.poId));
			return cq;
		});
	}
	
	@Override
	public List<BPOKeyValue> getLinkedToJobItemsOnInvoice(Integer invoiceId) {
		return getResultList(cb -> {
			CriteriaQuery<BPOKeyValue> cq = cb.createQuery(BPOKeyValue.class);
			Root<InvoiceJobLink> jobLink = cq.from(InvoiceJobLink.class);
			Join<InvoiceJobLink, Job> job = jobLink.join(InvoiceJobLink_.job);
			Join<Job, JobItem> jobItem = job.join(Job_.items);
			Join<JobItem, JobItemPO> itemPo = jobItem.join(JobItem_.ITEM_POS);
			Join<JobItemPO, BPO> bpo = itemPo.join(JobItemPO_.bpo);
			cq.where(cb.equal(jobLink.get(InvoiceJobLink_.invoice), invoiceId));
			cq.distinct(true);
			cq.select(cb.construct(BPOKeyValue.class, bpo.get(BPO_.poId), bpo.get(BPO_.poNumber)));
			return cq;
		});
	}
}