package org.trescal.cwms.core.jobs.jobitem.form;

import org.trescal.cwms.core.jobs.jobitem.entity.pat.PAT;

public class JIPATForm extends JobItemForm {

	private PAT pat;

	public PAT getPat() {
		return this.pat;
	}

	public void setPat(PAT pat) {
		this.pat = pat;
	}
}