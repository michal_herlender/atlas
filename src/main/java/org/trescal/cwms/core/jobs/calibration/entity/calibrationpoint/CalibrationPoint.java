package org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.CalibrationPointSet;
import org.trescal.cwms.core.jobs.calibration.entity.point.Point;

@Entity
@Table(name = "calibrationpoint")
public class CalibrationPoint extends Point
{
	private CalibrationPointSet pointSet;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "psid", nullable = false)
	public CalibrationPointSet getPointSet()
	{
		return this.pointSet;
	}

	public void setPointSet(CalibrationPointSet pointSet)
	{
		this.pointSet = pointSet;
	}
}
