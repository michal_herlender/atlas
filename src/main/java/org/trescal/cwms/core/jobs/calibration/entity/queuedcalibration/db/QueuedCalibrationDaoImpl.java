package org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.db;

import lombok.val;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration_;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink_;
import org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.QueuedCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.QueuedCalibration_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.procedure.entity.procedure.Capability_;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization_;

import java.util.List;

@Repository("QueuedCalibrationDao")
public class QueuedCalibrationDaoImpl extends BaseDaoImpl<QueuedCalibration, Integer> implements QueuedCalibrationDao {
	
	@Override
	protected Class<QueuedCalibration> getEntity() {
		return QueuedCalibration.class;
	}
	
	public List<QueuedCalibration> getAccreditedActiveQueuedCalibrations(int personId) {
        // Gets all jobitems that have a procedure that the contact with the
        // given personid is accredited to perform at the caltype that the
        // jobitem is currently assigned to. Using HQL as joins seem too
        // complicated for criteria's to cope with.
        return getResultList(cb -> {
            val cq = cb.createQuery(QueuedCalibration.class);
            val queuedCalibartion = cq.from(QueuedCalibration.class);
            val calibration = queuedCalibartion.join(QueuedCalibration_.queuedCal);
            val authorization = calibration
                .join(Calibration_.capability).join(Capability_.authorizations);
            val calibrationType1 = authorization.join(CapabilityAuthorization_.caltype);
            val calibrationType2 = calibration.join(Calibration_.calType);
            cq.where(cb.equal(calibrationType1, calibrationType2), cb.isTrue(queuedCalibartion.get(QueuedCalibration_.active)),
                cb.equal(authorization.get(CapabilityAuthorization_.authorizationFor), personId)
            );
            return cq;
        });
    }
	
	public List<QueuedCalibration> getAccreditedActiveQueuedCalibrationsForBatches(List<Integer> batchIds, int personId) {
        return getResultList(cb -> {
            val cq = cb.createQuery(QueuedCalibration.class);
            val queuedCalibartion = cq.from(QueuedCalibration.class);
            val calibration = queuedCalibartion.join(QueuedCalibration_.queuedCal);
            val authorization = calibration
                .join(Calibration_.capability).join(Capability_.authorizations);
            val calibrationType1 = authorization.join(CapabilityAuthorization_.caltype);
            val calibrationType2 = calibration.join(Calibration_.calType);
            cq.where(cb.equal(calibrationType1, calibrationType2), cb.isTrue(queuedCalibartion.get(QueuedCalibration_.active)),
                cb.equal(authorization.get(CapabilityAuthorization_.authorizationFor), personId),
                calibration.join(Calibration_.batch).in(batchIds),
                cb.equal(authorization.get(CapabilityAuthorization_.accredLevel), AccreditationLevel.APPROVE)
            );
            cq.orderBy(cb.asc(queuedCalibartion.get(QueuedCalibration_.timeQueued)));
            return cq;
        });
    }
	
	public List<QueuedCalibration> getActiveQueuedCalibrations() {
        return getResultList(cb -> {
            val cq = cb.createQuery(QueuedCalibration.class);
            val queuedCalibration = cq.from(QueuedCalibration.class);
            cq.where(cb.isTrue(queuedCalibration.get(QueuedCalibration_.active)));
            cq.orderBy(cb.asc(queuedCalibration.get(QueuedCalibration_.timeQueued)));
            return cq;
        });
    }
	
	public List<QueuedCalibration> getActiveQueuedCalibrationsForBatches(List<Integer> batchIds) {
        return getResultList(cb -> {
            val cq = cb.createQuery(QueuedCalibration.class);
            val queuedCalibration = cq.from(QueuedCalibration.class);
            cq.where(cb.isTrue(queuedCalibration.get(QueuedCalibration_.active)),
                queuedCalibration.join(QueuedCalibration_.queuedCal).get(Calibration_.batch).in(batchIds)
            );
            cq.orderBy(cb.asc(queuedCalibration.get(QueuedCalibration_.timeQueued)));
            return cq;
        });
    }
	
	public List<QueuedCalibration> getActiveQueuedCalibrationsOrderedByJob() {
        return getResultList(cb -> {
            val cq = cb.createQuery(QueuedCalibration.class);
            val queuedCalibration = cq.from(QueuedCalibration.class);
            val jobItem = queuedCalibration.join(QueuedCalibration_.queuedCal)
                .join(Calibration_.links).join(CalLink_.ji);
            cq.where(cb.isTrue(queuedCalibration.get(QueuedCalibration_.active)));
            cq.orderBy(cb.asc(jobItem.join(JobItem_.job)),
                cb.asc(jobItem.get(JobItem_.jobItemId))
            );
            return cq;
        });
    }
	
	public List<QueuedCalibration> getQueuedCalibrationsForReprint() {
        return getResultList(cb -> {
            val cq = cb.createQuery(QueuedCalibration.class);
            val queuedCalibration = cq.from(QueuedCalibration.class);
            cq.where(cb.isTrue(queuedCalibration.get(QueuedCalibration_.reprint)));
            val jobItem = queuedCalibration.join(QueuedCalibration_.queuedCal)
                .join(Calibration_.links).join(CalLink_.ji);
            cq.orderBy(cb.asc(jobItem.join(JobItem_.job)),
                cb.asc(jobItem.get(JobItem_.jobItemId))
            );
            return cq;
        });
    }
	
	public List<QueuedCalibration> getQueuedCalibrationsToPrintNow() {
        return getResultList(cb -> {
            val cq = cb.createQuery(QueuedCalibration.class);
            val queuedCalibration = cq.from(QueuedCalibration.class);
            cq.where(cb.isTrue(queuedCalibration.get(QueuedCalibration_.printNow)));
            val jobItem = queuedCalibration.join(QueuedCalibration_.queuedCal)
                .join(Calibration_.links).join(CalLink_.ji);
            cq.orderBy(cb.asc(jobItem.join(JobItem_.job)),
                cb.asc(jobItem.get(JobItem_.jobItemId))
            );
            return cq;
        });
    }
	
	public List<QueuedCalibration> getQueuedCalibrationsWithIds(List<Integer> ids) {
        return getResultList(cb -> {
            val cq = cb.createQuery(QueuedCalibration.class);
            val queuedCalibration = cq.from(QueuedCalibration.class);
            cq.where(queuedCalibration.in(ids));
            return cq;
        });
    }
}