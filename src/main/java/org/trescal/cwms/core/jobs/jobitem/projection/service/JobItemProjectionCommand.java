package org.trescal.cwms.core.jobs.jobitem.projection.service;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import lombok.Getter;
import lombok.Setter;

/**
 * Provides indication of which data to load
 * as input for JobItemProjectionService
 */
@Getter
@Setter
public class JobItemProjectionCommand {
    // Mandatory values set in constructor
    private Locale locale;
    private Integer allocatedCompanyId;
    // Optional values set via options (default false)
    private Boolean loadInstruments;
    private Boolean loadServiceTypes;
    private Boolean loadItemStates;
    private Boolean loadOnBehalfCompany;
    private Boolean loadCapabilities;
    private Boolean loadContractReviewers;
    private Boolean loadContractReviewPrice;
    private Boolean loadContracts;
    private Boolean loadCertificates;
    private Boolean loadClientPOs;
    private Boolean loadCostingItems;
    private Boolean loadSupplierPOs;

    public interface JI_LOAD_INSTRUMENTS {
    }

    public interface JI_LOAD_SERVICE_TYPES {
    }

    public interface JI_LOAD_ITEM_STATES {
    }

    public interface JI_LOAD_ON_BEHALF {
    }

    public interface JI_LOAD_CAPABILITIES {
    }

    public interface JI_LOAD_CONTRACT_REVIEWERS {
    }

    public interface JI_LOAD_CONTRACT_REVIEW_PRICE {
    }

    public interface JI_LOAD_CONTRACT {
    }

    public interface JI_LOAD_CERTIFICATES {
    }

    public interface JI_LOAD_CLIENT_POs {
    }

    public interface JI_LOAD_COSTING_ITEMS {
    }

    public interface JI_LOAD_SUPPLIER_POS {
    }

    public JobItemProjectionCommand(Locale locale, Integer allocatedCompanyId,
                                    Class<?>... options) {
        if (locale == null) throw new IllegalArgumentException("locale must not be null");
        if (allocatedCompanyId == null) throw new IllegalArgumentException("allocatedCompanyId must not be null");
        this.locale = locale;
        this.allocatedCompanyId = allocatedCompanyId;
        List<Class<?>> args = Arrays.asList(options);
        this.loadInstruments = args.contains(JI_LOAD_INSTRUMENTS.class);
        this.loadServiceTypes = args.contains(JI_LOAD_SERVICE_TYPES.class);
        this.loadItemStates = args.contains(JI_LOAD_ITEM_STATES.class);
        this.loadOnBehalfCompany = args.contains(JI_LOAD_ON_BEHALF.class);
        this.loadCapabilities = args.contains(JI_LOAD_CAPABILITIES.class);
        this.loadContractReviewers = args.contains(JI_LOAD_CONTRACT_REVIEWERS.class);
        this.loadContractReviewPrice = args.contains(JI_LOAD_CONTRACT_REVIEW_PRICE.class);
        this.loadContracts = args.contains(JI_LOAD_CONTRACT.class);
        this.loadCertificates = args.contains(JI_LOAD_CERTIFICATES.class);
        this.loadClientPOs = args.contains(JI_LOAD_CLIENT_POs.class);
        this.loadCostingItems = args.contains(JI_LOAD_CLIENT_POs.class);
        this.loadSupplierPOs = args.contains(JI_LOAD_SUPPLIER_POS.class);
    }
}