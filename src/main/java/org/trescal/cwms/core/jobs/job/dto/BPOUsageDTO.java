package org.trescal.cwms.core.jobs.job.dto;

import lombok.Data;
import org.trescal.cwms.core.tools.DateTools;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Date;

@Data
public class BPOUsageDTO {

	private Integer jobId;
	private String jobNo;
	private ZonedDateTime regDate;
	private Date dateComplete;
	private String jobStatus;
	private Integer jobItemId;
	private Integer expenseItemId;
	private Integer itemNo;
	private Long itemCount;
	private String modelName;
	private String serviceType;
	private String comment;
	private BigDecimal amount;
	private Boolean itemOnBPO;

	public BPOUsageDTO(Integer jobId, String jobNo, ZonedDateTime regDate, Date dateComplete, String jobStatus,
					   Integer jobItemId, Integer itemNo, Long itemCount, String modelName, String serviceType,
					   BigDecimal amount) {
		super();
		this.jobId = jobId;
		this.jobNo = jobNo;
		this.regDate = regDate;
		this.dateComplete = dateComplete;
		this.jobStatus = jobStatus;
		this.jobItemId = jobItemId;
		this.itemNo = itemNo;
		this.itemCount = itemCount;
		this.modelName = modelName;
		this.serviceType = serviceType;
		this.amount = amount;
		this.setItemOnBPO(true);
	}

	public BPOUsageDTO(Integer jobId, String jobNo, LocalDate date, String jobStatus, Integer expenseItemId,
			Integer itemNo, Long itemCount, String modelName, String comment, BigDecimal singlePrice,
			Integer quantity) {
		super();
		this.jobId = jobId;
		this.jobNo = jobNo;
		this.dateComplete = DateTools.dateFromLocalDate(date);
		this.jobStatus = jobStatus;
		this.expenseItemId = expenseItemId;
		this.itemNo = itemNo;
		this.itemCount = itemCount;
		this.modelName = modelName;
		this.comment = comment;
		this.amount = singlePrice.multiply(BigDecimal.valueOf(quantity));
		this.setItemOnBPO(true);
	}

	public BPOUsageDTO(Integer jobId, String jobNo, ZonedDateTime regDate, Date dateComplete, String jobStatus,
					   Integer jobItemId, Integer itemNo, Long itemCount, String modelName, String serviceType, BigDecimal amount,
					   Long itemOnBPO) {
		super();
		this.jobId = jobId;
		this.jobNo = jobNo;
		this.regDate = regDate;
		this.dateComplete = dateComplete;
		this.jobStatus = jobStatus;
		this.jobItemId = jobItemId;
		this.itemNo = itemNo;
		this.itemCount = itemCount;
		this.modelName = modelName;
		this.serviceType = serviceType;
		this.amount = amount;
		this.itemOnBPO = itemOnBPO > 0L;
	}
}