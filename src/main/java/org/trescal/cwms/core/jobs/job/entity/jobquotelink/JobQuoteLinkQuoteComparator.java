package org.trescal.cwms.core.jobs.job.entity.jobquotelink;

import java.util.Comparator;

import org.trescal.cwms.core.quotation.entity.quotation.Quotation;

/**
 * Implementation of {@link Comparator} that sorts a collection of
 * {@link JobQuoteLink} entities by their respective {@link Quotation}.
 * 
 * @author richard
 */
public class JobQuoteLinkQuoteComparator implements Comparator<JobQuoteLink>
{
	@Override
	public int compare(JobQuoteLink o1, JobQuoteLink o2)
	{
		if ((o1.getQuotation() == null) || (o2.getQuotation() == null))
		{
			return o1.getQuotation() != null ? 1 : -1;
		}
		else
		{
			if (o1.getQuotation().getId() == o2.getQuotation().getId())
			{
				return 1;
			}
			else
			{
				return ((Integer) o1.getQuotation().getId()).compareTo(o2.getQuotation().getId());
			}
		}
	}
}
