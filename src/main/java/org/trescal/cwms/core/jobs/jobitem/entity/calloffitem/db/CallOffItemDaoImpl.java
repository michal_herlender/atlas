package org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.db;

import lombok.val;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToSubdivDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.CallOffItem;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.CallOffItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.dto.CallOffItemDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;

import javax.persistence.criteria.*;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@Repository("CallOffItemDao")
public class CallOffItemDaoImpl extends AllocatedToSubdivDaoImpl<CallOffItem, Integer> implements CallOffItemDao {
    @Override
    protected Class<CallOffItem> getEntity() {
        return CallOffItem.class;
    }

    public CallOffItem getActiveForJobItem(int jobItemId)
	{
		return getFirstResult(cb -> {
			CriteriaQuery<CallOffItem> cq = cb.createQuery(CallOffItem.class);
			Root<CallOffItem> root = cq.from(CallOffItem.class);
			cq.where(cb.equal(root.get(CallOffItem_.offItem), jobItemId));
			return cq;
		}).orElse(null);
	}
	
	
	@Override
	public Set<CallOffItemDTO> getAllActiveCallOffItems(Locale locale){
		
		List<CallOffItemDTO> callOffItems =  this.getResultList(cb-> {
			CriteriaQuery<CallOffItemDTO> cq = cb.createQuery(CallOffItemDTO.class);
			Root<CallOffItem> callOffItem = cq.from(CallOffItem.class);
			
			Join<CallOffItem, JobItem> offItem =  callOffItem.join(CallOffItem_.offItem);
			Join<JobItem, Job> job = offItem.join(JobItem_.job);
		
			Join<Job, Contact> contact = job.join(Job_.con);
			Join<Contact, Subdiv> sub = contact.join(Contact_.sub);
			Join<Subdiv, Company> comp = sub.join(Subdiv_.comp);
			
			Join<JobItem, Address> currentAddress = offItem.join(JobItem_.currentAddr, JoinType.LEFT);
			Join<Address, Subdiv> subdiv = currentAddress.join(Address_.sub, JoinType.LEFT);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp, JoinType.LEFT);
			
			Join<JobItem, Instrument> inst = offItem.join(JobItem_.inst, JoinType.LEFT);
			Join<Instrument, InstrumentModel> model = inst.join(Instrument_.model, JoinType.LEFT);
			Join<InstrumentModel, Translation> modelName = model.join(InstrumentModel_.nameTranslations, JoinType.LEFT);
			modelName.on(cb.equal(modelName.get(Translation_.locale), locale));
			
			// sub query to count items on job
			Subquery<Long> itemCountSq = cq.subquery(Long.class);
			Root<JobItem> itemCount = itemCountSq.from(JobItem.class);
			itemCountSq.where(cb.equal(itemCount.get(JobItem_.job), job));
			itemCountSq.select(cb.count(itemCount));
			// sub query to count contract review items on job item
			Subquery<Long> contractReviewItemCountSq = cq.subquery(Long.class);
			Root<ContractReviewItem> contractReviewItemCount = contractReviewItemCountSq.from(ContractReviewItem.class);
			contractReviewItemCountSq.where(cb.equal(contractReviewItemCount.get(ContractReviewItem_.jobitem), offItem));
			contractReviewItemCountSq.select(cb.count(contractReviewItemCount));
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(callOffItem.get(CallOffItem_.active), true));
			cq.where(clauses);
			
			CompoundSelection<CallOffItemDTO> selection = cb.construct(CallOffItemDTO.class, 
					callOffItem.get(CallOffItem_.id),
					sub.get(Subdiv_.subdivid),
					comp.get(Company_.coid),
					job.get(Job_.jobid),
					job.get(Job_.jobno),
					offItem.get(JobItem_.jobItemId),
					company.get(Company_.companyCode),
					comp.get(Company_.coname),
					sub.get(Subdiv_.subname),
					sub.get(Subdiv_.active),
					subdiv.get(Subdiv_.subdivCode),
					currentAddress.get(Address_.town),
					callOffItem.get(CallOffItem_.calledOffBy).get(Contact_.firstName),
					callOffItem.get(CallOffItem_.calledOffBy).get(Contact_.lastName),
					callOffItem.get(CallOffItem_.offDate),
					callOffItem.get(CallOffItem_.reason),
					inst.get(Instrument_.plantid),
					modelName.get(Translation_.translation),
					offItem.get(JobItem_.itemNo),
					contractReviewItemCountSq.getSelection(),
					itemCountSq.getSelection());
			
			cq.select(selection);
			return cq;
		});

        return new HashSet<>(callOffItems);
	}
	
	public List<CallOffItem> getAllActive() {
        return getResultList(cb -> {
            val cq = cb.createQuery(CallOffItem.class);
            val callOffItem = cq.from(CallOffItem.class);
            val item = callOffItem.join(CallOffItem_.offItem);
            val job = item.join(JobItem_.job);
            val comp = job.join(Job_.con).join(Contact_.sub).join(Subdiv_.comp);
            cq.orderBy(cb.desc(comp.get(Company_.coname)),
                    cb.desc(job.get(Job_.jobno)), cb.asc(item.get(JobItem_.itemNo))
            );
            return cq;
        });
    }

    @Override
    public List<CallOffItemDTO> getAllForCompanyDto(int coid, Boolean active) {

        return getResultList(cb -> {
            val cq = cb.createQuery(CallOffItemDTO.class);
            val callOffItem = cq.from(CallOffItem.class);
            val item = callOffItem.join(CallOffItem_.offItem);
            val job = item.join(JobItem_.job);
            val sub = job.join(Job_.con).join(Contact_.sub);
            val comp = sub.join(Subdiv_.comp);
            val currentAddress = item.join(JobItem_.currentAddr, JoinType.LEFT);
            val subdiv = currentAddress.join(Address_.sub, JoinType.LEFT);
            val company = subdiv.join(Subdiv_.comp, JoinType.LEFT);
            if (active != null)
                cq.where(cb.equal(callOffItem.get(CallOffItem_.active), active), cb.equal(comp, coid));
            else
                cq.where(cb.equal(comp, coid));

            val inst = item.join(JobItem_.inst);
            val model = inst.join(Instrument_.model);
            // sub query to count items on job
            Subquery<Long> itemCountSq = cq.subquery(Long.class);
            Root<JobItem> itemCount = itemCountSq.from(JobItem.class);
            itemCountSq.where(cb.equal(itemCount.get(JobItem_.job), job));
            itemCountSq.select(cb.count(itemCount));
            // sub query to count contract review items on job item
            Subquery<Long> contractReviewItemCountSq = cq.subquery(Long.class);
            Root<ContractReviewItem> contractReviewItemCount = contractReviewItemCountSq.from(ContractReviewItem.class);
            contractReviewItemCountSq.where(cb.equal(contractReviewItemCount.get(ContractReviewItem_.jobitem), item));
            contractReviewItemCountSq.select(cb.count(contractReviewItemCount));

            cq.select(cb.construct(CallOffItemDTO.class,
                    callOffItem.get(CallOffItem_.id),
                    sub.get(Subdiv_.subdivid),
                    comp.get(Company_.coid),
                    job.get(Job_.jobid),
                    job.get(Job_.jobno),
                    item.get(JobItem_.jobItemId),
                    company.get(Company_.companyCode),
                    comp.get(Company_.coname),
                    sub.get(Subdiv_.subname),
                    sub.get(Subdiv_.active),
                    subdiv.get(Subdiv_.subdivCode),
                    currentAddress.get(Address_.town),
                    callOffItem.get(CallOffItem_.calledOffBy).get(Contact_.firstName),
                    callOffItem.get(CallOffItem_.calledOffBy).get(Contact_.lastName),
                    callOffItem.get(CallOffItem_.offDate),
                    callOffItem.get(CallOffItem_.reason),
                    inst.get(Instrument_.plantid),
                    joinTranslation(cb, model, InstrumentModel_.nameTranslations, LocaleContextHolder.getLocale()),
                    item.get(JobItem_.itemNo),
                    contractReviewItemCountSq.getSelection(),
                    itemCountSq.getSelection()));
            cq.orderBy(cb.desc(job.get(Job_.jobno)), cb.asc(item.get(JobItem_.itemNo)));
            return cq;
        });
    }

    public List<CallOffItem> getAllForCompany(int coid, Boolean active) {
	    /*

	    return getResultList(cb -> {
	    	val cq = cb.createQuery(CallOffItem.class);
	    	val callOffItem = cq.from(CallOffItem.class);
	    	val item = callOffItem.join(CallOffItem_.offItem);
	    	val job = item.join(JobItem_.job);
	    	val comp = job.join(Job_.con).join(Contact_.sub).join(Subdiv_.comp);
	    	if(active!=null)
	    		cq.where(cb.equal(callOffItem.get(CallOffItem_.active),active),cb.equal(comp,coid));
	    	else
	    		cq.where(cb.equal(comp,coid));

	    	cq.orderBy(cb.desc(job.get(Job_.jobno)),cb.asc(item.get(JobItem_.itemNo)));
	    	return  cq;
		});
	     */
        Criteria crit = getSession().createCriteria(CallOffItem.class);
        Criteria itemCrit = crit.createCriteria("offItem");
        Criteria jobCrit = itemCrit.createCriteria("job");
        Criteria conCrit = jobCrit.createCriteria("con");
        Criteria subCrit = conCrit.createCriteria("sub");
        Criteria compCrit = subCrit.createCriteria("comp");
        crit.setFetchMode("offItem", FetchMode.JOIN);
        crit.setFetchMode("offItem.inst", FetchMode.JOIN);
        crit.setFetchMode("offItem.inst.model", FetchMode.JOIN);
        crit.setFetchMode("offItem.inst.model.mfr", FetchMode.JOIN);
        crit.setFetchMode("offItem.inst.model.description", FetchMode.JOIN);
        crit.setFetchMode("offItem.inst.con", FetchMode.JOIN);
        crit.setFetchMode("offItem.inst.con.sub", FetchMode.JOIN);
        if (active != null) crit.add(Restrictions.eq("active", active));
        compCrit.add(Restrictions.idEq(coid));
        jobCrit.addOrder(org.hibernate.criterion.Order.desc("jobno"));
        itemCrit.addOrder(org.hibernate.criterion.Order.asc("itemNo"));

        return (List<CallOffItem>) crit.list();
    }
	
	public List<CallOffItem> getAll(List<Integer> ids) {
        return getResultList(cb -> {
            val cq = cb.createQuery(CallOffItem.class);
            cq.where(cq.from(CallOffItem.class).in(ids));
            return cq;
        });
    }
}