package org.trescal.cwms.core.jobs.certificate.entity.certnote.db;

import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.certificate.entity.certnote.CertNote;

public interface CertNoteService
{
	void deleteCertNote(CertNote certnote);

	void deleteCertNoteById(int id);

	CertNote findCertNote(int id);

	List<CertNote> getAllCertNotes();

	void insertCertNote(CertNote certnote);

	ResultWrapper insertCertNoteDWR(String note, int certId);

	void saveOrUpdateCertNote(CertNote certnote);

	void updateCertNote(CertNote certnote);

	ResultWrapper updateCertNoteDWR(String note, int certNoteId);
}