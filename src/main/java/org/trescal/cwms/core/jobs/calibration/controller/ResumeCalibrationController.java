package org.trescal.cwms.core.jobs.calibration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.form.ResumeCalibrationForm;
import org.trescal.cwms.core.jobs.calibration.form.ResumeCalibrationValidator;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.status.db.StatusService;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME })
public class ResumeCalibrationController {
	@Autowired
	private CalibrationService calServ;
	@Autowired
	private UserService userService;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private ResumeCalibrationValidator validator;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("form")
	protected ResumeCalibrationForm formBackingObject(@RequestParam("jobitemid") int jobItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		Calibration cal = this.calServ.findOnHoldCalibrationForItem(jobItemId);

		ResumeCalibrationForm form = new ResumeCalibrationForm();
		form.setJobItemId(jobItemId);
		form.setContactId(this.userService.get(username).getCon().getPersonid());
		if (cal != null)
			form.setCalibrationId(cal.getId());

		return form;
	}

	@RequestMapping(value = "/resumecalibration.htm")
	protected String resumeCalibration(@Validated @ModelAttribute("form") ResumeCalibrationForm form,
			BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return "trescal/core/jobs/calibration/resumecalibration";
		}
		Calibration cal = this.calServ.findCalibration(form.getCalibrationId());
		// resume the calibration (adds another Pre-calibration activity)
		this.calServ.resumeCalibration(cal, null, null, null, (CalibrationStatus) this.statusServ.findStatusByName(CalibrationStatus.ON_GOING,
				CalibrationStatus.class));

		// re-direct to complete calibration screen
		return "redirect:completecalibration.htm?openproc=true&calid=" + form.getCalibrationId() + "&jobitemid="
				+ form.getJobItemId();
	}
}