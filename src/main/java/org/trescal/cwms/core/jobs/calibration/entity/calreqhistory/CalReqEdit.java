package org.trescal.cwms.core.jobs.calibration.entity.calreqhistory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("edit")
public class CalReqEdit extends CalReqHistory
{

}