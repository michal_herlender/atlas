package org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum WorkrequirementStatus {
	PENDING("vm_global.pendingworkrequirement"),				    // 0
	COMPLETE("vm_global.workrequirementcompleted"), 				// 1
	CANCELLED("vm_global.workrequirementcancelled");				// 2

	
	private String messageCode;
	private MessageSource messageSource;
	
	private WorkrequirementStatus(String messageCode) {
		this.messageCode = messageCode;
	}
	
	@Component
	public static class WorkrequirementStatusMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
			for (WorkrequirementStatus status: EnumSet.allOf(WorkrequirementStatus.class)) {
				status.setMessageSource(messages);
			}
		}
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getMessageCode() {
		return messageCode;
	}
	
	public String getMessage() {
		Locale locale = LocaleContextHolder.getLocale();
		return this.messageSource.getMessage(messageCode, null, this.toString(), locale);
	}
}
