package org.trescal.cwms.core.jobs.jobitem.form;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;

public class JIActivitiesForm extends JobItemForm
{
	/**
	 * Indicates whether the most recent activity on the Job Item can be deleted
	 * by the current user. true = permission granted, false = permission
	 * denied, null = no activities for this job item
	 */
	private Boolean canDeleteLastActivity;
	private Date date;
	private Integer endStatId;
	private List<JobItemActivity> existingActivities;
	private Integer fromAddrId;
	private Integer fromLocId;
	private List<Integer> holdItemIds;
	private String holdRemark;
	private Integer holdStatusId;
	private Integer hour;
	private Integer lastActionId;
	private Integer lastActivityId;
	private String message;
	private Integer minutes;
	private String mode;
	private String other;
	private List<Integer> overrideItemIds;
	private String overrideRemark;
	private Integer overrideStatusId;
	private Integer plusHours;
	private String remark;
	private String submitAction;
	private Integer timeSpent;
	private Integer toAddrId;
	private Integer toLocId;
	private Integer transMethodId;

	/**
	 * @return the canDeleteLastActivity
	 */
	public Boolean getCanDeleteLastActivity()
	{
		return this.canDeleteLastActivity;
	}

	public Date getDate()
	{
		return this.date;
	}

	public Integer getEndStatId()
	{
		return this.endStatId;
	}

	/**
	 * @return the existingActivities
	 */
	public List<JobItemActivity> getExistingActivities()
	{
		return this.existingActivities;
	}

	public Integer getFromAddrId()
	{
		return this.fromAddrId;
	}

	public Integer getFromLocId()
	{
		return this.fromLocId;
	}

	public List<Integer> getHoldItemIds()
	{
		return this.holdItemIds;
	}

	public String getHoldRemark()
	{
		return this.holdRemark;
	}

	public Integer getHoldStatusId()
	{
		return this.holdStatusId;
	}

	public Integer getHour()
	{
		return this.hour;
	}

	public Integer getLastActionId()
	{
		return this.lastActionId;
	}

	public Integer getLastActivityId()
	{
		return this.lastActivityId;
	}

	public String getMessage()
	{
		return this.message;
	}

	public Integer getMinutes()
	{
		return this.minutes;
	}

	public String getMode()
	{
		return this.mode;
	}

	public String getOther()
	{
		return this.other;
	}

	public List<Integer> getOverrideItemIds()
	{
		return this.overrideItemIds;
	}

	public String getOverrideRemark()
	{
		return this.overrideRemark;
	}

	public Integer getOverrideStatusId()
	{
		return this.overrideStatusId;
	}

	public Integer getPlusHours()
	{
		return this.plusHours;
	}

	public String getRemark()
	{
		return this.remark;
	}

	public String getSubmitAction()
	{
		return this.submitAction;
	}

	public Integer getTimeSpent()
	{
		return this.timeSpent;
	}

	public Integer getToAddrId()
	{
		return this.toAddrId;
	}

	public Integer getToLocId()
	{
		return this.toLocId;
	}

	public Integer getTransMethodId()
	{
		return this.transMethodId;
	}

	/**
	 * @param canDeleteLastActivity the canDeleteLastActivity to set
	 */
	public void setCanDeleteLastActivity(Boolean canDeleteLastActivity)
	{
		this.canDeleteLastActivity = canDeleteLastActivity;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public void setEndStatId(Integer endStatId)
	{
		this.endStatId = endStatId;
	}

	/**
	 * @param existingActivities the existingActivities to set
	 */
	public void setExistingActivities(List<JobItemActivity> existingActivities)
	{
		this.existingActivities = existingActivities;
	}

	public void setFromAddrId(Integer fromAddrId)
	{
		this.fromAddrId = fromAddrId;
	}

	public void setFromLocId(Integer fromLocId)
	{
		this.fromLocId = fromLocId;
	}

	public void setHoldItemIds(List<Integer> holdItemIds)
	{
		this.holdItemIds = holdItemIds;
	}

	public void setHoldRemark(String holdRemark)
	{
		this.holdRemark = holdRemark;
	}

	public void setHoldStatusId(Integer holdStatusId)
	{
		this.holdStatusId = holdStatusId;
	}

	public void setHour(Integer hour)
	{
		this.hour = hour;
	}

	public void setLastActionId(Integer lastActionId)
	{
		this.lastActionId = lastActionId;
	}

	public void setLastActivityId(Integer lastActivityId)
	{
		this.lastActivityId = lastActivityId;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public void setMinutes(Integer minutes)
	{
		this.minutes = minutes;
	}

	public void setMode(String mode)
	{
		this.mode = mode;
	}

	public void setOther(String other)
	{
		this.other = other;
	}

	public void setOverrideItemIds(List<Integer> overrideItemIds)
	{
		this.overrideItemIds = overrideItemIds;
	}

	public void setOverrideRemark(String overrideRemark)
	{
		this.overrideRemark = overrideRemark;
	}

	public void setOverrideStatusId(Integer overrideStatusId)
	{
		this.overrideStatusId = overrideStatusId;
	}

	public void setPlusHours(Integer plusHours)
	{
		this.plusHours = plusHours;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public void setSubmitAction(String submitAction)
	{
		this.submitAction = submitAction;
	}

	public void setTimeSpent(Integer timeSpent)
	{
		this.timeSpent = timeSpent;
	}

	public void setToAddrId(Integer toAddrId)
	{
		this.toAddrId = toAddrId;
	}

	public void setToLocId(Integer toLocId)
	{
		this.toLocId = toLocId;
	}

	public void setTransMethodId(Integer transMethodId)
	{
		this.transMethodId = transMethodId;
	}
}