package org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costs.base.WithCostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.RepCost;

@Entity
@DiscriminatorValue("contract")
public class ContractReviewRepairCost extends RepCost implements WithCostSource<ContractReviewLinkedRepairCost>
{
	private JobItem jobitem;
	private ContractReviewLinkedRepairCost linkedCost;
	private CostSource costSrc;

	@Enumerated(EnumType.STRING)
	@Column(name = "costsource")
	public CostSource getCostSrc()
	{
		return this.costSrc;
	}

	@OneToOne(mappedBy = "repairCost")
	public JobItem getJobitem()
	{
		return this.jobitem;
	}

	@OneToOne(mappedBy = "repairCost", cascade = CascadeType.ALL)
	public ContractReviewLinkedRepairCost getLinkedCost()
	{
		return this.linkedCost;
	}

	@Override
	@Transient
	public Cost getLinkedCostSrc()
	{
		return null;
	}

	public void setCostSrc(CostSource costSrc)
	{
		this.costSrc = costSrc;
	}

	public void setJobitem(JobItem jobitem)
	{
		this.jobitem = jobitem;
	}

	public void setLinkedCost(ContractReviewLinkedRepairCost linkedCost)
	{
		this.linkedCost = linkedCost;
	}
}
