package org.trescal.cwms.core.jobs.generalserviceoperation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.db.GsoDocumentService;
import org.trescal.cwms.core.jobs.generalserviceoperation.form.GeneralServiceOperationForm;
import org.trescal.cwms.core.jobs.generalserviceoperation.form.validator.GeneralServiceOperationValidator;
import org.trescal.cwms.core.jobs.jobitem.controller.JobItemController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.misc.tool.CustomLocalDateTimeEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

import static org.trescal.cwms.core.tools.DateTools.dateToLocalDateTime;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class EditGsoDocumentController extends JobItemController {
    private static final Logger logger = LoggerFactory.getLogger(
        org.trescal.cwms.core.jobs.generalserviceoperation.controller.EditGsoDocumentController.class);
    public static final String ACTION_PERFORMED = "EDIT_GSODOCUMENT";
    private final String FORM_NAME = "form";

    @Autowired
    private GsoDocumentService gsoDocumentService;
    @Autowired
    private GeneralServiceOperationValidator validator;
    @Autowired
    private ComponentDirectoryService compDirServ;
    @Autowired
    private SystemComponentService scServ;

    @InitBinder(FORM_NAME)
    protected void initBinder(ServletRequestDataBinder binder) throws Exception {
        binder.registerCustomEditor(LocalDateTime.class, new CustomLocalDateTimeEditor(DateTimeFormatter.ISO_DATE_TIME));
        binder.registerCustomEditor(Date.class, new MyCustomDateEditor(DateTools.dtf_activities, true));
        binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
        binder.setValidator(validator);
    }

    @ModelAttribute(FORM_NAME)
    protected GeneralServiceOperationForm formBackingObject(
        @RequestParam(value = "jobitemid") int jobitemid,
        @RequestParam(value = "gsodocid") Integer gsodocid) throws Exception {
        JobItem ji = this.jobItemService.findJobItem(jobitemid);
        if (ji == null || jobitemid == 0) {
            logger.error("Jobitem with id " + jobitemid + " could not be found");
            throw new Exception("Unable to locate requested jobitem");
        }
        GsoDocument gsoDocument = gsoDocumentService.get(gsodocid);
        if (gsoDocument == null) {
            logger.error("General service operation Document with id " + gsodocid + " could not be found");
            throw new Exception("Unable to locate requested General service operation Document");
        }
        GeneralServiceOperationForm form = new GeneralServiceOperationForm();
        form.setGsoDocument(gsoDocument);
        form.setDocumentId(gsoDocument.getId());
        form.setDocumentDate(dateToLocalDateTime(gsoDocument.getDocumentDate()));
        form.setDocumentNumber(gsoDocument.getDocumentNumber());
        form.setStartedOn(dateToLocalDateTime(gsoDocument.getGso().getStartedOn()));
        form.setStartedByName(gsoDocument.getGso().getStartedBy().getName());
        form.setGso(gsoDocument.getGso());
        form.setActionPerformed(ACTION_PERFORMED);
        form.setCompletedOn(dateToLocalDateTime(gsoDocument.getGso().getCompletedOn()));
        form.setCompletedByName(gsoDocument.getGso().getCompletedBy().getName());
        form.setJi(ji);

        return form;
    }

    @RequestMapping(value = {"/editgsodocument.htm"}, method = {RequestMethod.GET})
    protected ModelAndView editGsoDocumentGet(@RequestParam(value = "jobitemid") Integer jobItemId,
                                              @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                                              @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                              @ModelAttribute("form") GeneralServiceOperationForm form) throws Exception {
        Contact contact = this.userService.get(username).getCon();
        Map<String, Object> map = referenceData(jobItemId, contact, subdivDto.getKey(), username);
        form.getGsoDocument().setDirectory(this.compDirServ.getDirectory(this.scServ.findComponent(Component.GENERAL_SERVICE_OPERATION),
            form.getJi().getJob().getJobno()));

        return new ModelAndView("trescal/core/jobs/generalserviceoperation/uploadgsodocument", map);
    }

    @RequestMapping(value = {"/editgsodocument.htm"}, method = {RequestMethod.POST})
    protected ModelAndView editGsoDocumentPost(@RequestParam(value = "jobitemid") Integer jobItemId,
                                               @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                                               @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                               @ModelAttribute("form") GeneralServiceOperationForm form, BindingResult bindingResult)
        throws Exception {
        Contact contact = this.userService.get(username).getCon();
        this.validator.validate(form, bindingResult, contact);
        if (bindingResult.hasErrors())
            return editGsoDocumentGet(jobItemId, username, subdivDto, form);
        this.gsoDocumentService.updateGsoDocument(form);
        return new ModelAndView(new RedirectView("jigeneralserviceoperations.htm?jobitemid=" + form.getJi().getJobItemId()));
    }

}
