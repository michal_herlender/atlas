package org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.db;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.dto.ClientPurchaseOrderItemDTO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.db.JobBPOLinkService;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.po.db.POService;
import org.trescal.cwms.core.jobs.job.projection.ClientPurchaseOrderProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db.JobExpenseItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.db.JobExpenseItemPOService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.logistics.dto.PoDTO;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_AddItemPO;

import lombok.val;

@Service("JobItemPOService")
public class JobItemPOServiceImpl implements JobItemPOService {

	@Autowired
	private JobItemPODao jobItemPODao;
	@Autowired
	private JobItemService jobItemServ;
	@Autowired
	private JobExpenseItemService jobExpenseItemService;
	@Autowired
	private JobService jobServ;
	@Autowired
	private POService poServ;
	@Autowired
	private BPOService bpoServ;
	@Autowired
	private HookInterceptor_AddItemPO interceptor_addItemPO;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private JobBPOLinkService jobBpoLinkService;
	@Autowired
	private JobExpenseItemPOService jobexpenseitemposervice;

	public void assignItemsToPO(List<Integer> jobItemIds, List<Integer> jobServiceIds, Integer poId, Integer jobId) {
		PO po = poServ.get(poId);
		// Remove unselected job items
		po.getJobItemPOs().removeIf(jiPO -> !jobItemIds.contains(jiPO.getItem().getJobItemId()));
		// Already existing job items
		List<Integer> existingItemIds = po.getJobItemPOs().stream().map(jiPO -> jiPO.getItem().getJobItemId())
				.collect(Collectors.toList());
		// Add new selected items
		for (Integer id : jobItemIds)
			if (!existingItemIds.contains(id)) {
				JobItemPO jiPO = new JobItemPO();
				jiPO.setItem(jobItemServ.get(id));
				jiPO.setPo(po);
				this.insertJobItemPO(jiPO);
			}
		// Remove unselected job services
		po.getExpenseItemPOs().removeIf(eiPO -> !jobServiceIds.contains(eiPO.getItem().getId()));
		// Already existing job services
		existingItemIds = po.getExpenseItemPOs().stream().map(eiPO -> eiPO.getItem().getId())
				.collect(Collectors.toList());
		// Add new selected services
		for (Integer id : jobServiceIds)
			if (!existingItemIds.contains(id)) {
				JobExpenseItemPO eiPO = new JobExpenseItemPO();
				eiPO.setItem(jobExpenseItemService.get(id));
				eiPO.setPo(po);
				this.jobexpenseitemposervice.save(eiPO);
			}
		poServ.merge(po);
	}

	@Override
	public ResultWrapper assignJobItemsToPO(Integer[] jobItemIds, int poId) {
		try {
			List<Integer> jobItemIdList = new ArrayList<>(Arrays.asList(jobItemIds));
			Map<Integer, JobItemPO> alreadyOnPO = new HashMap<>();
			// get po to edit items for
			PO po = this.poServ.get(poId);
			// populate map of job items already on the PO
			for (JobItemPO jipo : po.getJobItemPOs())
				alreadyOnPO.put(jipo.getItem().getJobItemId(), jipo);
			// for each of the checked job items
			for (int i : jobItemIds) {
				// if the item isn't already on the po
				if (!alreadyOnPO.containsKey(i)) {
					// assign it to po
					JobItemPO jipo = new JobItemPO();
					jipo.setItem(this.jobItemServ.findJobItem(i));
					jipo.setPo(po);

					this.insertJobItemPO(jipo);
					po.getJobItemPOs().add(jipo);
				}
			}
			// for each of the items on the po
			for (Integer i : alreadyOnPO.keySet()) {
				// if the item has been unchecked
				if (!jobItemIdList.contains(i)) {
					// remove it from po
					po.getJobItemPOs().remove(alreadyOnPO.get(i));
					this.deleteJobItemPO(alreadyOnPO.get(i));
				}
			}
			return new ResultWrapper(true, null, po, null);
		} catch (Exception e) {
			return new ResultWrapper(false, e.getMessage());
		}
	}

	@Override
	public void deleteBPOLinksToJob(int bpoid, int jobid) {
		// delete all linked jobitempos
		this.jobItemPODao.deleteBPOLinksToJob(bpoid, jobid);
		// delete link between Job and BPO
		Job j = this.jobServ.get(jobid);
		val targetLinkOpt = j.getBpoLinks().stream().filter(b -> b.getBpo().getPoId() == bpoid).findFirst();
		targetLinkOpt.ifPresent(targetLink -> {
			BPO bpo = targetLink.getBpo();
			bpo.getExpenseItemPOs().removeIf(jeiPo -> jeiPo.getItem().getJob().getJobid() == jobid);
			j.getBpoLinks().remove(targetLink);
			bpo.getJobLinks().remove(targetLink);
			this.jobBpoLinkService.delete(targetLink);
		});
	}

	@Override
	public void deleteJobItemPO(JobItemPO jobitempo) {
		this.jobItemPODao.remove(jobitempo);
	}

	@Override
	public void deleteJobItemPOById(int id) {
		this.jobItemPODao.remove(this.findJobItemPO(id));
	}

	@Override
	public JobItemPO findJobItemPO(int id) {
		return this.jobItemPODao.find(id);
	}

	@Override
	public List<JobItemPO> getAllJobItemPOs() {
		return this.jobItemPODao.findAll();
	}

	@Override
	public List<ClientPurchaseOrderProjectionDTO> getProjectionDTOs(Collection<Integer> jobIds,
			Collection<Integer> jobItemIds) {
		List<ClientPurchaseOrderProjectionDTO> result = Collections.emptyList();
		if ((jobItemIds != null && !jobItemIds.isEmpty()) || (jobIds != null && !jobIds.isEmpty())) {
			result = this.jobItemPODao.getProjectionDTOs(jobIds, jobItemIds);
		}
		return result;
	}

	@Override
	public List<JobItemWrapper> getItemsOnBPO(int bpoId, int jobId) {
		List<JobItemWrapper> wrappers = new ArrayList<>();
		List<JobItem> jobItems = this.jobItemPODao.getItemsOnBPO(bpoId, jobId);
		for (JobItem jobItem : jobItems) {
			String calTypeShortName = jobItem.getCalType().getServiceType().getShortName();
			String instrumentModelName = InstModelTools.modelNameViaTranslations(jobItem.getInst().getModel(),
					LocaleContextHolder.getLocale(), supportedLocaleService.getPrimaryLocale());
			JobItemWrapper wrapper = new JobItemWrapper(jobItem, instrumentModelName, calTypeShortName);
			wrappers.add(wrapper);
		}
		return wrappers;
	}

	@Override
	public List<JobItemWrapper> getItemsOnPO(int poId) {
		List<JobItemWrapper> wrappers = new ArrayList<>();
		List<JobItem> jobItems = this.jobItemPODao.getItemsOnPO(poId);
		for (JobItem jobItem : jobItems) {
			String calTypeShortName = jobItem.getCalType().getServiceType().getShortName();
			String instrumentModelName = InstModelTools.modelNameViaTranslations(jobItem.getInst().getModel(),
					LocaleContextHolder.getLocale(), supportedLocaleService.getPrimaryLocale());
			JobItemWrapper wrapper = new JobItemWrapper(jobItem, instrumentModelName, calTypeShortName);
			wrappers.add(wrapper);
		}
		return wrappers;
	}

	@Override
	public List<PoDTO> getValidPODtosForJobItem(Integer jobItemId) {
		return jobItemPODao.getValidPODtosForJobItem(jobItemId);
	}

	@Override
	public void insertJobItemPO(JobItemPO jipo) {
		this.jobItemPODao.persist(jipo);
		this.interceptor_addItemPO.recordAction(jipo);
	}

	@Override
	public JobItemPO insertJobItemPOForBPO(int jobitemid) {
		System.out.println("insertJobItemPOForBPO");
		JobItemPO jipo = new JobItemPO();
		JobItem ji = this.jobItemServ.findEagerJobItem(jobitemid);
		jipo.setItem(ji);
		jipo.setBpo(ji.getJob().getBpo());

		this.insertJobItemPO(jipo);

		return jipo;
	}

	@Override
	public JobItemPO insertJobItemPOWithIds(int jobitemid, int poid) {
		JobItemPO jipo = new JobItemPO();
		jipo.setItem(this.jobItemServ.findJobItem(jobitemid));
		jipo.setPo(this.poServ.get(poid));
		this.insertJobItemPO(jipo);
		return jipo;
	}

	@Override
	public String getAllPOsByJobItem(int jobItemId) {
		return this.jobItemPODao.getAllPOsByJobItem(jobItemId);
	}

	@Override
	public List<ClientPurchaseOrderItemDTO> getOnPO(Integer poId) {
		Locale locale = LocaleContextHolder.getLocale();
		return this.jobItemPODao.getOnPO(poId, locale);
	}

	@Override
	public List<ClientPurchaseOrderItemDTO> getOnBPO(Integer poId, Integer jobId) {
		Locale locale = LocaleContextHolder.getLocale();
		return this.jobItemPODao.getOnBPO(poId, jobId, locale);
	}

	@Override
	public ResultWrapper assignJIandJobServicestoBPO(List<Integer> jobItemIds, List<Integer> jobServicesIds, int poId,
			Integer jobId) {

		try {
			BPO bpo = this.bpoServ.get(poId);

			List<Integer> ids = this.getJobitemIdsOnBPO(poId, jobId);

			for (int jobitemId : jobItemIds) {
				if (!ids.contains(jobitemId)) {
					JobItemPO jipo = new JobItemPO();
					jipo.setItem(this.jobItemServ.findJobItem(jobitemId));
					jipo.setBpo(bpo);
					this.insertJobItemPO(jipo);
				}
			}

			List<Integer> jobExpenseIds = this.jobexpenseitemposervice.getJobServicesIdsOnBPO(poId, jobId);
			for (int jobServiceId : jobServicesIds) {
				if (!jobExpenseIds.contains(jobServiceId)) {
					JobExpenseItemPO itemPO = new JobExpenseItemPO();
					itemPO.setBpo(bpo);
					itemPO.setItem(jobExpenseItemService.get(jobServiceId));
					this.jobexpenseitemposervice.save(itemPO);
				}
			}

			return new ResultWrapper(true, null, null, null);
		} catch (Exception e) {
			return new ResultWrapper(false, e.getMessage());
		}

	}

	@Override
	public ResultWrapper unassignJIandJobServicestoBPO(Integer jobItemId, Integer jobServicesId, int poId,
			Integer jobId) {

		try {
			BPO bpo = this.bpoServ.get(poId);
			if (jobItemId != null) {
				JobItemPO jiPO = this.getJobItemPo(jobItemId, poId, jobId);
				if (jiPO != null) {
					this.deleteJobItemPO(jiPO);
				}
			}

			if (jobServicesId != null) {
				JobExpenseItemPO expenseitem = this.jobexpenseitemposervice.getJobExpenseItemPO(jobServicesId, poId,
						jobId);
				if (expenseitem != null) {
					this.jobexpenseitemposervice.delete(expenseitem);
				}

			}

			return new ResultWrapper(true, null, bpo, null);
		} catch (Exception e) {
			return new ResultWrapper(false, e.getMessage());
		}

	}

	@Override
	public List<Integer> getJobitemIdsOnBPO(Integer bpoid, Integer jobId) {
		return this.jobItemPODao.getJobitemIdsOnBPO(bpoid, jobId);
	}

	@Override
	public JobItemPO getJobItemPo(Integer jobItemId, int poId, Integer jobId) {
		return this.jobItemPODao.getJobItemPo(jobItemId, poId, jobId);
	}
}