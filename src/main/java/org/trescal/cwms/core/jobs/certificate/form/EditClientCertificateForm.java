package org.trescal.cwms.core.jobs.certificate.form;

import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter @Setter
public class EditClientCertificateForm
{
	private Integer serviceTypeId;
	private Certificate cert;
	private MultipartFile file;
	private Instrument inst;
	private boolean persisted;
	private Integer subdivid;
	private boolean updateNextCalDueDate;
	private String durationUnitId;
	private CalibrationVerificationStatus calVerificationStatus;
    private BigDecimal deviation;
}
