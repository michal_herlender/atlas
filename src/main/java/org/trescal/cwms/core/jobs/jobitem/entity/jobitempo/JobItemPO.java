package org.trescal.cwms.core.jobs.jobitem.entity.jobitempo;

import javax.persistence.AssociationOverride;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitempo.AbstractJobItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

import lombok.Setter;

@Setter
@Entity
@AssociationOverride(name = "item", joinColumns = @JoinColumn(name = "jobitemid"))
@Table(name = "jobitempo", uniqueConstraints = @UniqueConstraint(columnNames = { "jobitemid", "bpoid", "poid" }))
public class JobItemPO extends AbstractJobItemPO<JobItem> {
}