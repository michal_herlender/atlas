package org.trescal.cwms.core.jobs.certificate.entity.instcertlink.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;

public interface InstCertLinkDao extends BaseDao<InstCertLink, Integer> {
	Long getInstCertLinkCount(Integer plantid);
	List<InstCertLink> searchByPlantId(Integer plantid, Integer maxResults);
}