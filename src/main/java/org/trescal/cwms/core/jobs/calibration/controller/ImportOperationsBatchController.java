package org.trescal.cwms.core.jobs.calibration.controller;

import org.apache.commons.io.FilenameUtils;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.batch.config.AutoSubmitPolicyEnum;
import org.trescal.cwms.batch.config.entities.backgroundtask.BackgroundTask;
import org.trescal.cwms.batch.config.entities.backgroundtask.BackgroundTaskService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db.ExchangeFormatFileService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatzipfile.OperationsImportZipFile;
import org.trescal.cwms.core.jobs.calibration.form.ImportCalibrationsForm;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class ImportOperationsBatchController {

    public static final String FORM = "form";
    @Autowired
    private BackgroundTaskService bgTaskService;
    @Autowired
	private UserService userService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private ExchangeFormatFileService efFileService;

	@ModelAttribute(FORM)
	protected ImportCalibrationsForm getImportCalibrationsForm() {
		return new ImportCalibrationsForm();
	}

	@ModelAttribute("autoSubmitPolicies")
	protected List<KeyValue<String, String>> getAutoSubmitPolicies() {
		return Arrays.asList(AutoSubmitPolicyEnum.values()).stream().map(v -> new KeyValue<>(v.name(), v.getValue()))
				.collect(Collectors.toList());
	}

	@GetMapping(value = "/importoperationsbatch.htm")
	public ModelAndView doGet() {
		return new ModelAndView("trescal/core/jobs/calibration/importoperationsbatch");
	}

	@PostMapping(value = "/importoperationsbatch.json", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResultWrapper submitForm(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(FORM) ImportCalibrationsForm form, BindingResult bindingResult, Locale locale)
			throws IOException {

		ResultWrapper rw = new ResultWrapper();
		rw.setErrorList(new ArrayList<>());


        User user = userService.get(username);
        BackgroundTask bgTask;

		try {
            ExchangeFormatFile efFile;
			if (FilenameUtils.getExtension(form.getFile().getOriginalFilename()).equalsIgnoreCase("zip"))
				efFile = new OperationsImportZipFile();
			else
				efFile = new ExchangeFormatFile();

			efFile = efFileService.createEfFile(user.getCon(), form.getFile(), efFile);

			bgTask = bgTaskService.submitOperationsImportBgTask(form, user.getCon().getId(), user.getCon().getId(),
					subdivDto.getKey(), efFile, locale.toString());
			rw.setSuccess(true);
			rw.setMessage(messageSource.getMessage("importcalibrations.bgtasksubmitted",
					new Object[] { bgTask.getId() }, null, locale));
			rw.setResults(bgTask.getId());
		} catch (JobExecutionAlreadyRunningException e) {
			rw.setSuccess(false);
			rw.setMessage(
					messageSource.getMessage("importcalibrations.jobalreadyrunning", new Object[] {}, null, locale));
		} catch (JobRestartException e) {
			rw.setSuccess(false);
			rw.setMessage(
					messageSource.getMessage("importcalibrations.jobrestarterror", new Object[] {}, null, locale));
		} catch (JobInstanceAlreadyCompleteException e) {
			rw.setSuccess(false);
			rw.setMessage(
					messageSource.getMessage("importcalibrations.jobalreadycompleted", new Object[] {}, null, locale));
		} catch (JobParametersInvalidException e) {
			rw.setSuccess(false);
			rw.setMessage(
					messageSource.getMessage("importcalibrations.invalidjobparametres", new Object[] {}, null, locale));
		}

		return rw;
	}

}
