package org.trescal.cwms.core.jobs.certificate.entity;

import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

/**
 * Enum identifying the three main types of {@link Certificate}, those that are
 * in-house certificates (generic), those that are third party certificates and
 * those that are not related to jobs on the system.
 * 
 * @author JamieV
 */
public enum CertificateType
{
	CERT_CLIENT, CERT_INHOUSE, CERT_THIRDPARTY;
}