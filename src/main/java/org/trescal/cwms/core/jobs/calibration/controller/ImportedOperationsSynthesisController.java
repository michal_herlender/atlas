package org.trescal.cwms.core.jobs.calibration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.AutoPopulatingList;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.batch.jobs.operationsimport.ImportedOperationItem;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.calibration.form.ImportCalibrationsForm;
import org.trescal.cwms.core.jobs.calibration.form.ImportedOperationSynthesisForm;
import org.trescal.cwms.core.jobs.calibration.form.validator.ImportedOperationsSynthesisFormValidator;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchForm;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.spring.helpers.IntegerElementFactory;
import org.trescal.cwms.spring.model.KeyValue;

import javax.validation.Valid;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME,
	Constants.SESSION_ATTRIBUTE_COMPANY})
public class ImportedOperationsSynthesisController {

	private static final String TLM_IMPORTED = "TLM IMPORTED";
	public static final String FORM_NAME = "form";
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private CalibrationService calibrationService;
	@Autowired
	private JobService jobService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private UserService userService;
	@Autowired
	private ImportedOperationsSynthesisFormValidator validator;
	@Autowired
	private ExchangeFormatService exchangeFormatServ;
	@Autowired
	private CalibrationProcessService calibrationProcessService;
	@Autowired
	private ServiceTypeService serviceTypeService;

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.dtf, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}

	@ModelAttribute(FORM_NAME)
	protected ImportedOperationSynthesisForm formBackingObject(
			@ModelAttribute("importCalibrationsForm") ImportCalibrationsForm importCalibrationsForm,
			@ModelAttribute("fileContent") ArrayList<LinkedCaseInsensitiveMap<String>> fileContent,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Model model,
			Locale locale) throws ParseException {
		ImportedOperationSynthesisForm form = new ImportedOperationSynthesisForm();

		// read content into dto
		if (importCalibrationsForm.getSubdivid() != null && importCalibrationsForm.getExchangeFormatId() != null) {

			form.setExchangeFormatId(importCalibrationsForm.getExchangeFormatId());
			form.setRows(calibrationService.convertFileContentToImportedCalibrationDTO(form.getExchangeFormatId(),
					fileContent, importCalibrationsForm.getSubdivid()));

			this.calibrationService.groupImportedOperations(form.getRows());

			form.setSubdivid(importCalibrationsForm.getSubdivid());
			Subdiv subdiv = subdivService.get(importCalibrationsForm.getSubdivid());
			Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
			List<Job> jobs = jobService.getActiveJobsBySubdivAndAllocatedSubdiv(subdiv, allocatedSubdiv).stream()
					.filter(j -> j.getType().equals(JobType.SITE)).collect(Collectors.toList());
			// get active jobs, jobitems and non completed workrequirements
			Map<Job, Map<JobItem, List<JobItemWorkRequirement>>> joJiWr = new LinkedHashMap<>();
			Map<Integer, Boolean> jobitemContractReviewed = new HashMap<>();
			for (Job job : jobs) {
				Map<JobItem, List<JobItemWorkRequirement>> jiWr = new LinkedHashMap<>();
				for (JobItem jobItem : job.getItems()) {
					// get non completed/cancelled wr
					List<JobItemWorkRequirement> wrs = jobItem.getWorkRequirements().stream().filter(
							jiwr -> !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE)
									&& !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED))
							.collect(Collectors.toList());
					jiWr.put(jobItem, wrs);

					// whether it's contract reviewed
					boolean isAwaitingContractReview = jobItemService.stateHasGroupOfKeyName(jobItem.getState(),
							StateGroup.AWAITING_CONTRACT_REVIEW);
					if (!jobItem.getContactReviewItems().isEmpty() && !isAwaitingContractReview)
						jobitemContractReviewed.put(jobItem.getJobItemId(), true);
					else
						jobitemContractReviewed.put(jobItem.getJobItemId(), false);
				}
				joJiWr.put(job, jiWr);
			}
			ExchangeFormat ef = exchangeFormatServ.get(form.getExchangeFormatId());
			// get tableHeader
			Map<String, ExchangeFormatFieldNameEnum> columnsNameMapping = exchangeFormatServ.getColumnNamesMapping(ef);

			model.addAttribute("tableHeader", columnsNameMapping);
			model.addAttribute("subdiv", subdiv);
			model.addAttribute("joJiWr", joJiWr);
			model.addAttribute("jobitemContractReviewed", jobitemContractReviewed);
		}

		model.addAttribute("serviceTypes", serviceTypeService.getDTOListWithShortNames(null, null, locale, false));

		return form;
	}

	@RequestMapping(value = "/importedoperationssynthesis.htm", method = RequestMethod.GET)
	protected ModelAndView doGet(Model model) {
		return new ModelAndView("trescal/core/jobs/calibration/importedoperationssynthesis");
	}

	@RequestMapping(value = { "/importedoperationssynthesis.htm" }, params = { "!save", "newanalysis",
			"!createmissingjobitems" }, method = RequestMethod.POST)
	protected ModelAndView newanalysis(@Valid @ModelAttribute(FORM_NAME) ImportedOperationSynthesisForm form,
			BindingResult result, Model model) {
		return new ModelAndView("trescal/core/jobs/calibration/importedoperationssynthesis");
	}

	@RequestMapping(value = { "/importedoperationssynthesis.htm" }, params = { "save", "!newanalysis",
			"!createmissingjobitems" }, method = RequestMethod.POST)
	protected ModelAndView submit(@Valid @ModelAttribute(FORM_NAME) ImportedOperationSynthesisForm form,
			BindingResult result, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Model model,
			RedirectAttributes redirectAttributes, Locale locale) throws Exception {

		List<List<ImportedOperationItem>> groups = this.calibrationService.groupImportedOperations(form.getRows());

		// only checked groups
		List<List<ImportedOperationItem>> checkedGroups = groups.stream().filter(i -> i.get(0).isChecked())
				.collect(Collectors.toList());

		// if checked groups have errors
		boolean checkedRowsHasErrors = checkedGroups.stream().flatMap(Collection::stream).anyMatch(row -> {
			int currentIndex = form.getRows().indexOf(row);
			return result.getFieldErrors().stream()
					.anyMatch(fr -> fr.getField().contains("rows[" + currentIndex + "]"));
		});

		if (checkedRowsHasErrors)
			return newanalysis(form, result, model);

		Contact contact = this.userService.get(username).getCon();
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());

		// import operations
		calibrationService.importOnsiteOperations(checkedGroups, allocatedSubdiv.getSubdivid(), contact.getPersonid(),
				locale, calibrationProcessService.findByName(TLM_IMPORTED));

		Job job = this.jobService.get(form.getJobId());
		redirectAttributes.addFlashAttribute("job", job);
		return new ModelAndView(new RedirectView("importoperations.htm"));
	}

	@RequestMapping(value = { "/importedoperationssynthesis.htm" }, params = { "createmissingjobitems", "!save",
			"!newanalysis" }, method = RequestMethod.POST)
	protected String createMissingJobitems(@Valid @ModelAttribute(FORM_NAME) ImportedOperationSynthesisForm form,
			BindingResult result, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Model model,
			RedirectAttributes redirectAttributes, Locale locale) {

		Contact loggedInUser = this.userService.get(username).getCon();
		Job job = jobService.get(form.getJobId());

		Date dateIn;
		if (jobItemService.getDateIn(job) != null)
			dateIn = DateTools.dateFromZonedDateTime(jobItemService.getDateIn(job));
		else
			dateIn = new Date();

		NewJobItemSearchForm njisf = new NewJobItemSearchForm();
		// initilize to prevent null pointer exceptions
		njisf.setBasketTempGroupIds(new AutoPopulatingList<>(i -> 0));
		njisf.setBasketExistingGroupIds(new AutoPopulatingList<>(new IntegerElementFactory(0)));
		njisf.setBasketParts(new AutoPopulatingList<>(String.class));
		njisf.setBasketBaseItemIds(new AutoPopulatingList<>(new IntegerElementFactory(0)));
		njisf.setBasketTypes(new AutoPopulatingList<>(String.class));
		njisf.setBasketIds(new AutoPopulatingList<>(new IntegerElementFactory(0)));
		njisf.setBasketQuotationItemIds(new AutoPopulatingList<>(new IntegerElementFactory(0)));
		njisf.setServiceTypeIds(new AutoPopulatingList<>(new IntegerElementFactory(0)));
		njisf.setServiceTypeSources(new AutoPopulatingList<>(new IntegerElementFactory(0)));
		njisf.setBookingInAddrId(job.getBookedInAddr().getAddrid());
		njisf.setDatesIn(new AutoPopulatingList<>(i -> dateIn));
		njisf.setBookInByContactIds(new AutoPopulatingList<>(new IntegerElementFactory(0)));

		// create missing jobitems : instrument detected but no jobitem selected
		List<ImportedOperationItem> selectedRows = form.getRows().stream().filter(ImportedOperationItem::isChecked)
				.collect(Collectors.toList());
		for (ImportedOperationItem row : selectedRows) {
			this.calibrationService.detectInstrumentImportedCalibration(row, form.getSubdivid());
			if (row.getInstrument() != null && row.getJobitemId() == null
					&& !njisf.getBasketIds().contains(row.getInstrument().getPlantid())) {
				njisf.getBasketIds().add(row.getInstrument().getPlantid());
				njisf.getBasketTypes().add("instrument");
				if (row.getServiceTypeId() != null)
					njisf.getServiceTypeIds().add(row.getServiceTypeId());
				// date in of the jobitem to be created
				njisf.getDatesIn().add(row.getOperationDate());
				// book in by user id
				Contact operationBy = null;
				if (row.getOperationByHrId() != null) {
					operationBy = contactService.getByHrId(row.getOperationByHrId());
				}
				if (operationBy != null)
					njisf.getBookInByContactIds().add(operationBy.getPersonid());
				else
					njisf.getBookInByContactIds().add(loggedInUser.getPersonid());
			}
		}

		redirectAttributes.addFlashAttribute("flashBaseItemIds", njisf.getBasketBaseItemIds());
		redirectAttributes.addFlashAttribute("flashExistingGroupIds", njisf.getBasketExistingGroupIds());
		redirectAttributes.addFlashAttribute("flashIds", njisf.getBasketIds());
		redirectAttributes.addFlashAttribute("flashParts", njisf.getBasketParts());
		redirectAttributes.addFlashAttribute("flashTempGroupIds", njisf.getBasketTempGroupIds());
		redirectAttributes.addFlashAttribute("flashTypes", njisf.getBasketTypes());
		redirectAttributes.addFlashAttribute("flashQuotationItemIds", njisf.getBasketQuotationItemIds());
		redirectAttributes.addFlashAttribute("flashServiceTypeIds", njisf.getServiceTypeIds());
		redirectAttributes.addFlashAttribute("flashServiceTypeSources", njisf.getServiceTypeSources());
		redirectAttributes.addFlashAttribute("flashDatesIn", njisf.getDatesIn());
		redirectAttributes.addFlashAttribute("flashBookInByContactIds", njisf.getBookInByContactIds());

		return "redirect:addnewitemstojob.htm?jobid=" + form.getJobId() + "&bookingInAddrId="
				+ job.getBookedInAddr().getAddrid();

	}

}
