package org.trescal.cwms.core.jobs.jobitem.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;

@Component
public class OnBehalfValidator implements Validator
{
	@Autowired
	private CompanyService compServ;
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.isAssignableFrom(OnBehalfForm.class);
	}
	
	@Override
	public void validate(Object target, Errors errors)
	{
		OnBehalfForm form = (OnBehalfForm) target;

		// check the company
		if (form.getCoid() == null)
		{
			errors.rejectValue("coid", "error.onbehalf.nocompany", null, "A company must be selected");
		}
		else
		{
			Company co = this.compServ.get(form.getCoid());
			if (co == null)
			{
				errors.rejectValue("coid", "error.onbehalf.nocompany", null, "A company must be selected");
			}
			else if (!co.getCompanyRole().equals(CompanyRole.CLIENT)
					&& !co.getCompanyRole().equals(CompanyRole.BUSINESS)
					&& !co.getCompanyRole().equals(CompanyRole.PROSPECT))
			{
				errors.rejectValue("coid", "error.onbehalf.invalidcompany", null, "A valid company must be selected");
			}
		}
	}
}