package org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;

public interface OnBehalfItemDao extends BaseDao<OnBehalfItem, Integer> {
	
	public OnBehalfItem getOnBehalfItemByJobitem(JobItem ji);
}