package org.trescal.cwms.core.jobs.calibration.form;

import java.util.List;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.dto.MultiCompleteCal;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

public class MultiCompleteCalibrationForm
{
	private int action;
	private Boolean issueNow;
	private Job job;
	private List<MultiCompleteCal> onGoingCals;
	private List<MultiCompleteCal> onHoldCals;
	private String remark;
	private Contact currentContact;

	public int getAction()
	{
		return this.action;
	}

	public Boolean getIssueNow()
	{
		return this.issueNow;
	}

	public Job getJob()
	{
		return this.job;
	}

	public List<MultiCompleteCal> getOnGoingCals()
	{
		return this.onGoingCals;
	}

	public List<MultiCompleteCal> getOnHoldCals()
	{
		return this.onHoldCals;
	}

	public String getRemark()
	{
		return this.remark;
	}

	public void setAction(int action)
	{
		this.action = action;
	}

	public void setIssueNow(Boolean issueNow)
	{
		this.issueNow = issueNow;
	}

	public void setJob(Job job)
	{
		this.job = job;
	}

	public void setOnGoingCals(List<MultiCompleteCal> onGoingCals)
	{
		this.onGoingCals = onGoingCals;
	}

	public void setOnHoldCals(List<MultiCompleteCal> onHoldCals)
	{
		this.onHoldCals = onHoldCals;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public Contact getCurrentContact() {
		return currentContact;
	}

	public void setCurrentContact(Contact currentContact) {
		this.currentContact = currentContact;
	}
}