package org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitempo;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.po.PO;

import lombok.Setter;

@Setter
@MappedSuperclass
public abstract class AbstractJobItemPO<ItemType> extends Versioned {

	private Integer id;
	private ItemType item;
	private BPO bpo;
	private PO po;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	@ManyToOne
	public ItemType getItem() {
		return item;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bpoid")
	public BPO getBpo() {
		return bpo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "poid")
	public PO getPo() {
		return po;
	}
}