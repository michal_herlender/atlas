package org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemActionForEmployeeDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemActionProjectionDTO;
import org.trescal.cwms.core.jobs.repair.dto.RepairTimeDTO;

public interface JobItemActionDao extends BaseDao<JobItemAction, Integer> {

	List<JobItemActionForEmployeeDTO> findNotTimeRelated(Contact employee, LocalDate start, LocalDate end);

	List<JobItemActionForEmployeeDTO> findTimeRelated(Contact employee, LocalDate start, LocalDate end);

	List<JobItemAction> findForJobItemView(JobItem jobItem);
	
	List<JobItemActionProjectionDTO> getProjectionDTOsForJobItem(Integer jobItemId, Boolean deleted, Locale locale);

	Integer summarizeTimeRelated(Contact employee, LocalDate start, LocalDate end);

	Integer summarizeNotTimeRelated(Contact employee, LocalDate start, LocalDate end);
	
	List<RepairTimeDTO> findTechnicianRepairTimeByRcr(Integer jiId, String rcrIdentifier, Contact contact);
	
	List<Integer> findNotProcessedActionsIdForAdveso();
}