package org.trescal.cwms.core.jobs.certificate.entity.certaction;

import java.util.Comparator;

public class CertActionComparator implements Comparator<CertAction>
{
	@Override
	public int compare(CertAction ca1, CertAction ca2)
	{
		if (ca1.getCert().getCertid() == ca2.getCert().getCertid())
		{
			if (ca1.getActionOn().compareTo(ca2.getActionOn()) == 0)
			{
				Integer c1 = ca1.getId();
				Integer c2 = ca2.getId();

				return c1.compareTo(c2);
			}
			else
			{
				return ca1.getActionOn().compareTo(ca2.getActionOn());
			}
		}
		else
		{
			Integer c1 = ca1.getCert().getCertid();
			Integer c2 = ca2.getCert().getCertid();

			return c1.compareTo(c2);
		}
	}
}
