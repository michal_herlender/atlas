package org.trescal.cwms.core.jobs.jobitem.form;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HoldStatusForm {

	private String submitAction;
	private Integer holdStatusId;
	private String holdRemark;
	private List<Integer> holdItemIds;

}