package org.trescal.cwms.core.jobs.calibration.entity.calreq;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.Constants;

@Entity
@DiscriminatorValue("jobitem")
public class JobItemCalReq extends CalReq
{
	private JobItem jobItem;

	@Transient
	@Override
	public String getClassKey()
	{
		return Constants.CALREQ_MAP_JOBITEM;
	}

	@Override
	@Transient
	public int getGenericId()
	{
		return (this.jobItem == null) ? 0 : this.jobItem.getJobItemId();
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid", nullable = true)
	public JobItem getJobItem()
	{
		return this.jobItem;
	}

	@Transient
	@Override
	public String getSource()
	{
		return "calreq.jobitemreqsource";
	}
	
	@Transient
	@Override
	public String getSourceParameter()
	{
		return "";
	}

	public void setJobItem(JobItem jobItem)
	{
		this.jobItem = jobItem;
	}
}