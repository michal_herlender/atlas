package org.trescal.cwms.core.jobs.certificate.entity.certlink;

import java.util.Comparator;
import java.util.Date;

/**
 * This comparator orders certs, going from oldest to most recent for any
 * particular job item.
 * 
 * @author JamieV
 */
public class CertLinkComparator implements Comparator<CertLink>
{
	public int compare(CertLink certLink, CertLink anotherCertLink)
	{
		Integer jobitemid1 = certLink.getJobItem().getJobItemId();
		Integer jobitemid2 = anotherCertLink.getJobItem().getJobItemId();

		if (jobitemid1.intValue() == jobitemid2.intValue())
		{
			Date d1 = certLink.getCert().getCertDate();
			Date d2 = anotherCertLink.getCert().getCertDate();

			if (d1.equals(d2))
			{
				int certid1 = certLink.getCert().getCertid();
				int certid2 = anotherCertLink.getCert().getCertid();

				return ((Integer) certid1).compareTo(certid2);
			}
			else
			{
				return d1.compareTo(d2);
			}
		}
		else
		{
			return jobitemid1.compareTo(jobitemid2);
		}
	}
}