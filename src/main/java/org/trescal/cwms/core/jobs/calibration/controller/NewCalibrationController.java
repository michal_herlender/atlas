package org.trescal.cwms.core.jobs.calibration.controller;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalibrationToCalLinkComparator;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.db.StandardUsedService;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.jobs.calibration.form.NewCalibrationForm;
import org.trescal.cwms.core.jobs.calibration.form.NewCalibrationValidator;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.controller.JobItemController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnote.JobItemNote;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandard;
import org.trescal.cwms.core.procedure.entity.defaultstandard.db.DefaultStandardService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.procedure.entity.workinstruction.db.WorkInstructionService;
import org.trescal.cwms.core.procedure.entity.workinstructionstandard.WorkInstructionStandard;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.InstructionDTO;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionEntity;
import org.trescal.cwms.core.system.entity.instructionlink.db.InstructionLinkService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.FileTools;
import org.trescal.cwms.spring.model.KeyValue;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class NewCalibrationController extends JobItemController {
    private static final Logger logger = LoggerFactory.getLogger(NewCalibrationController.class);

    @Value("${cwms.config.calibration.allow_uncalibrated_standards}")
    private boolean allowUncalibratedStds;
    @Value("${cwms.config.certificate.password.password_required}")
    private boolean passwordRequired;
    @Value("${cwms.config.calibrations.startmultiple}")
    private boolean startMultipleCalibrations;
    @Autowired
    private CalibrationProcessService calProServ;
    @Autowired
    private CalibrationService calServ;
    @Autowired
    private CertificateService certServ;
    @Autowired
    private ContactService conServ;
    @Autowired
    private DefaultStandardService defStandServ;
    @Autowired
    private InstructionLinkService instructionLinkServ;
    @Autowired
    private InstrumService instrumServ;
    @Autowired
    private CapabilityService procServ;
    @Autowired
    private StandardUsedService stdUsedServ;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private WorkInstructionService wiServ;
    @Autowired
    private NewCalibrationValidator validator;

    @InitBinder
    protected void initBinder(ServletRequestDataBinder binder) {
        binder.registerCustomEditor(Date.class, new MyCustomDateEditor(DateTools.df_ISO8601, true));
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
	}

	@ModelAttribute("form")
	protected NewCalibrationForm formBackingObject(
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") int jobitemid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		NewCalibrationForm form = new NewCalibrationForm();
		form.setStartFromTool(false);
		// create new calibration object
		Calibration cal = new Calibration();
		// initialise fields to prevent null pointer exceptions
		cal.setCerts(new ArrayList<>());
		JobItem ji = this.jobItemService.findJobItem(jobitemid);
		if ((ji == null) || (jobitemid == 0)) {
			logger.error("Jobitem with id " + jobitemid + " could not be found");
			throw new Exception("Unable to locate requested jobitem");
		}
		form.setJi(ji);
		if (!this.jobItemService.isReadyForCalibration(ji)) {
			logger.debug("Jobitem with id " + jobitemid + " was not ready for calibration");
		} else {
			Set<CalLink> calLinks = new HashSet<>();
			if (ji.getGroup() != null) {
				for (JobItem itemInGroup : ji.getGroup().getItems()) {
					CalLink calLink = new CalLink();
					calLink.setCal(cal);
					calLink.setJi(itemInGroup);
					calLinks.add(calLink);
				}
			} else {
				CalLink calLink = new CalLink();
				calLink.setCal(cal);
				calLink.setJi(ji);
				calLinks.add(calLink);
			}
			cal.setLinks(calLinks);
			// set today's date as cal date by default
			cal.setCalDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			cal.setCalAddress(ji.getCalAddr());

            Certificate reservedCertificate = this.certServ.getReservedCertificate(ji);
            form.setCalDate(reservedCertificate == null ? LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()) : DateTools.dateToLocalDate(reservedCertificate.getCalDate()));
            form.setCalDateFromReservedCertificate(
                reservedCertificate == null ? null : reservedCertificate.getCertno());
            form.setPasswordReq(this.passwordRequired);
            form.setAllowUncalibratedStds(this.allowUncalibratedStds);
            form.setProcId(0);
            form.setCalProcessId(0);
            form.setWorkInstructionId(0);
            // pre-populate the defaults of the procedure
            Capability proc = null;
            if ((form.getJi().getNextWorkReq() != null)
                && (form.getJi().getNextWorkReq().getWorkRequirement().getCapability() != null)) {
                proc = form.getJi().getNextWorkReq().getWorkRequirement().getCapability();
            } else if (form.getJi().getCapability() != null) {
                proc = form.getJi().getCapability();
            }
            // pre-populate the defaults of the work instruction
            WorkInstruction wi = null;
            if ((form.getJi().getNextWorkReq() != null)
                && (form.getJi().getNextWorkReq().getWorkRequirement().getWorkInstruction() != null)) {
                wi = form.getJi().getNextWorkReq().getWorkRequirement().getWorkInstruction();
            } else if (form.getJi().getWorkInstruction() != null) {
                wi = form.getJi().getWorkInstruction();
            }
            if (proc != null) {
                form.setProcId(proc.getId());
                cal.setCapability(proc);
                if (proc.getCalibrationProcess() != null) {
                    form.setCalProcessId(proc.getCalibrationProcess().getId());
                    if ((proc.getCalibrationProcess().getApplicationComponent())
                        && (form.getWorkInstructionId() == 0)) {
                        if (proc.getDefWorkInstruction() != null) {
                            form.setWorkInstructionId(proc.getDefWorkInstruction().getId());
                        }
                    }
                }
            }
			if (wi != null) {
				form.setWorkInstructionId(wi.getId());
				cal.setWorkInstruction(wi);
			}
			form.setCal(cal);
			// form.setTimeSpents(30);
			form.setDuration(form.getJi().getInst().getCalFrequency());
			boolean pwExists = false;
			if (contact.getEncryptedPassword() != null) {
				if (!contact.getEncryptedPassword().equals("")) {
					pwExists = true;
				}
			}
			form.setPwExists(pwExists);
			form.getJi().getJob().setRelatedInstructionLinks(this.instructionLinkServ.getAllForJob(ji.getJob()));
			// set jobitem contract instructions
			if(ji.getContract() != null && CollectionUtils.isNotEmpty(ji.getContract().getInstructions())) {
				form.setContractInstructions(new ArrayList<>());
				for (Instruction instruction : ji.getContract().getInstructions()) {
					InstructionDTO instructionDto = new InstructionDTO(instruction);
					instructionDto.setLinkedToType(InstructionEntity.CONTRACT);
					instructionDto.setLinkedToName(ji.getContract().getContractNumber());
					form.getContractInstructions().add(instructionDto);
				}
			}
		}
		return form;
	}

	public boolean isMainItemForGroupCal(JobItem ji) {
		// check if job item is a base unit
		return ji.getInst().getModel().getModelType().getModules();
	}

	protected void onBind(boolean issueNow, boolean signNow, NewCalibrationForm form, Contact contact) {
		List<DefaultStandard> errorProcStds = new ArrayList<>();
		if (form.getProcStdsOnScreen() != null) {
			for (Integer i : form.getProcStdsOnScreen()) {
				errorProcStds.add(this.defStandServ.findDefaultStandard(i));
			}
		}
		form.setErrorProcStds(errorProcStds);
		List<Instrument> errorAdditionalStds = new ArrayList<>();
		if (form.getAddStdsOnScreen() != null) {
			for (Integer i : form.getAddStdsOnScreen()) {
				errorAdditionalStds.add(this.instrumServ.get(i));
			}
		}
		form.setErrorAdditionalStds(errorAdditionalStds);
		Set<Integer> set = new HashSet<>();
		if (form.getInstrums() != null) {
			set.addAll(Arrays.asList(form.getInstrums()));
		}
		set.remove(0);
		form.setStdIds(set);
		Integer[] stds = set.toArray(new Integer[0]);
		form.setInstrums(stds);
		Calibration cal = form.getCal();
		if (form.getCalProcessId() != null) {
			cal.setCalProcess(this.calProServ.get(form.getCalProcessId()));
		}
		if (form.getJi() != null) {
			cal.setCalType(form.getJi().getNextWorkReq().getWorkRequirement().getServiceType().getCalibrationType());
		}
		if (form.getProcId() != null) {
            cal.setCapability(this.procServ.get(form.getProcId()));
		}
		if (form.getWorkInstructionId() != null) {
			WorkInstruction wi = this.wiServ.get(form.getWorkInstructionId());
			if (wi != null) {
				cal.setWorkInstruction(wi);
			}
		}
		cal.setStartedBy(contact);
		cal.setCalDate(form.getCalDate());
		// Keep cal date consistent between calibration and any reserved
		// certificate
		Certificate reservedCertificate = this.certServ.getReservedCertificate(form.getJi());
		if (reservedCertificate != null)
			reservedCertificate.setCalDate(DateTools.dateFromLocalDate(form.getCalDate()));
		// MOVED TO THE ONSUBMIT
		if (form.getItemsToCalibrate() == null) {
			form.setItemsToCalibrate(new ArrayList<>());
		}
		form.setIssueNow(issueNow);
		form.setSignNow(signNow);
	}

	@RequestMapping(value = "/newcalibration.htm", method = RequestMethod.POST)
	public ModelAndView onSubmit(@RequestParam(value = "jobitemid") Integer jobItemId,
								 @RequestParam(name = "issuenow", required = false, defaultValue = "false") boolean issueNow,
								 @RequestParam(name = "signnow", required = false, defaultValue = "false") boolean signNow,
								 @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
								 @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
								 @ModelAttribute("form") NewCalibrationForm form, BindingResult bindingResult,
								 RedirectAttributes redirectAttributes) throws Exception {
		Contact user = this.userService.get(username).getCon();
		onBind(issueNow, signNow, form, user);
		// if this is a group calibration - moved away from onBind
		Set<CalLink> calLinks = new TreeSet<>(new CalibrationToCalLinkComparator());
		if (form.isGroupCal()) {
			// make a cal link for each checked jobitem
			for (Integer itemId : form.getItemsToCalibrate()) {
				JobItem ji = this.jobItemService.findJobItem(itemId);
				CalLink calLink = new CalLink();
				calLink.setJi(ji);
				calLink.setCal(form.getCal());
				calLink.setMainItem(this.isMainItemForGroupCal(ji));
				calLinks.add(calLink);
			}
		} else {
			// otherwise, just link the main jobitem
			CalLink calLink = new CalLink();
			calLink.setJi(form.getJi());
			calLink.setCal(form.getCal());
			calLink.setMainItem(true);
			calLinks.add(calLink);
		}
		form.getCal().setLinks(calLinks);
		// Performing manual validation due to onBind() method containing
		// initialization/setters for several cal process details
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Calibration cal = form.getCal();
		if (cal != null)
			cal.setOrganisation(allocatedSubdiv);
		validator.validate(form, bindingResult);
		if (bindingResult.hasErrors())
			return referenceData(jobItemId, issueNow, signNow, username, subdivDto, form);
		if (form.getInstrums() != null)
			this.stdUsedServ.createStandardsUsed(form.getInstrums(), cal);
		this.calServ.insertAndStartCalibration(cal, user, null, null);
		if (form.getStartFromTool()) {
			Certificate reservedCertificate = this.certServ.getReservedCertificate(form.getJi());
			reservedCertificate.setCal(cal);
			this.certServ.updateCertificate(reservedCertificate);
		}
		if (cal != null && !cal.getCalProcess().getApplicationComponent()) {
			if (form.getIssueNow()) {
				// complete calibration
				this.calServ.completeCalibration(cal, user, form.getTimeSpents(), null, null, true, null, null, null);
				// create cert object
				Certificate cert = this.certServ.createCertFromCal(user, cal, form.getDuration(), form.getSignNow(),
					allocatedSubdiv, IntervalUnit.MONTH, CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE, null);
				// if signing now
				if (form.getSignNow()) {
					if (!form.isPwExists()) {
						this.conServ.setEncryptedPassword(user.getPersonid(), form.getEncPassword());
					}
					// if a file has been uploaded then copy it into
					// the certificate directory
					MultipartFile file = form.getFile();
					if (file != null) {
						// reload cert to set directory
						cert = this.certServ.findCertificate(cert.getCertid());
						String name = file.getOriginalFilename();
						if (!name.equals("")) {
							// get name without extension
							String nameWithoutExt;
							// get extension
							String ext = name.substring(name.lastIndexOf("."));
							// rename file to cert no
							nameWithoutExt = cert.getCertno();
							if (name.lastIndexOf('\\') > -1) {
								name = name.substring(name.lastIndexOf('\\'));
								nameWithoutExt = name.substring(0, name.lastIndexOf("."));
							}
							file.transferTo(new File(cert.getDirectory().getAbsolutePath().concat(File.separator)
								.concat(nameWithoutExt).concat(ext)));
						}
					}
				}
				redirectAttributes.addAttribute("jobitemid", form.getJi().getJobItemId());
				return new ModelAndView(
					new RedirectView("jicertificates.htm?jobitemid=" + form.getJi().getJobItemId()));
			}
		}

		redirectAttributes.addAttribute("jobitemid", form.getJi().getJobItemId())
			.addAttribute("calid", cal != null ? cal.getId() : 0)
			.addAttribute("openproc", true);
		return new ModelAndView(new RedirectView("completecalibration.htm"));
	}

	@RequestMapping(value = "/newcalibration.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@RequestParam(value = "jobitemid") Integer jobItemId,
										 @RequestParam(name = "issuenow", required = false, defaultValue = "false") boolean issueNow,
										 @RequestParam(name = "signnow", required = false, defaultValue = "false") boolean signNow,
										 @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
										 @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
										 @ModelAttribute("form") NewCalibrationForm form) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		Map<String, Object> map = super.referenceData(jobItemId, contact, subdivDto.getKey(),username);
		if (this.jobItemService.isReadyForCalibration(form.getJi())) {
            onBind(issueNow, signNow, form, contact);
            // set the different note types (previously into the session)
            map.put("jobitemnote", JobItemNote.class);
            map.put("procs", this.procServ.getAll());
            map.put("processes", this.calProServ.getAll());
            map.put("classes", CalibrationClass.values());
            Job j = this.jobService.get(form.getJi().getJob().getJobid());
            map.put("jobfiles",
                FileTools.getFileListForDir(
                    j.getDirectory().getAbsolutePath().concat(File.separator).concat("Certificates"),
                    Collections.singletonList(".doc")));
            map.put("procstandards",
                this.defStandServ.getStandardsForProcedure(form.getCal().getCapability().getId(), null));
            if (form.getCal().getWorkInstruction() != null) {
                map.put("wistandards", this.defStandServ
                    .getStandardsForWorkInstruction(form.getCal().getWorkInstruction().getId(), null));
            } else {
                map.put("wistandards", new ArrayList<WorkInstructionStandard>());
            }
            map.put("workinstructions", this.wiServ.getAll());
            // get all other items at same state for option to start multiple
            // calibrations
            map.put("optionalMultiStartItems",
                this.jobItemService.getJobItemsByStatusAndProc(form.getJi().getJob().getJobid(),
                    form.getJi().getState().getStateid(), form.getCal().getCapability().getId(),
                    form.getJi().getJobItemId()));
            map.put("startMultipleCalibrations", this.startMultipleCalibrations);
		}
		return new ModelAndView("trescal/core/jobs/calibration/newcalibration", map);
	}
}