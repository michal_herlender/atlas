package org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.db;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.jobs.calibration.dto.StandardsUsageAnalysisDTO;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration_;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.StandardsUsageAnalysis;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.StandardsUsageAnalysis_;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed_;
import org.trescal.cwms.core.jobs.calibration.form.StandardsUsageAnalysisSearchForm;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate_;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.PagedResultSet;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Repository("StandardsUsageAnalysisDao")
public class StandardsUsageAnalysisDaoImpl extends BaseDaoImpl<StandardsUsageAnalysis, Integer>
	implements StandardsUsageAnalysisDao {

	@Override
	protected Class<StandardsUsageAnalysis> getEntity() {
		return StandardsUsageAnalysis.class;
	}

	@Override
	public PagedResultSet<StandardsUsageAnalysisDTO> sreachStandardsUsageAnalysis(StandardsUsageAnalysisSearchForm form,
			PagedResultSet<StandardsUsageAnalysisDTO> prs, Locale locale) {
		completePagedResultSet(prs, StandardsUsageAnalysisDTO.class, cb -> cq -> {
			Root<StandardsUsageAnalysis> root = cq.from(StandardsUsageAnalysis.class);
			Join<StandardsUsageAnalysis, Instrument> inst = root.join(StandardsUsageAnalysis_.instrument);
			Join<StandardsUsageAnalysis, Certificate> certif = root.join(StandardsUsageAnalysis_.certificate);
			Join<Instrument, InstrumentModel> model = inst.join(Instrument_.model);
			Join<InstrumentModel, Translation> modelName = model.join(InstrumentModel_.nameTranslations,
					javax.persistence.criteria.JoinType.LEFT);
			modelName.on(cb.equal(modelName.get(Translation_.locale), locale));
			Join<Instrument, Mfr> instMfr = inst.join(Instrument_.mfr, JoinType.LEFT);
			Join<InstrumentModel, Mfr> modelMfr = model.join(InstrumentModel_.mfr, JoinType.LEFT);

			Predicate clauses = cb.conjunction();
			if (form.getInstId() != null) {
				clauses.getExpressions().add(cb.equal(inst.get(Instrument_.plantid), form.getInstId()));
			}

			if(form.getCertno() != null && !form.getCertno().trim().isEmpty()){
				String likeParam = "%" + form.getCertno().trim() + "%";
				clauses.getExpressions().add(cb.like(certif.get(Certificate_.certno), likeParam));
			}

			if (form.getStatus() != null) {
				clauses.getExpressions().add(cb.equal(root.get(StandardsUsageAnalysis_.status), form.getStatus()));
			}

			if (form.getStartDate() != null) {
				cb.greaterThanOrEqualTo(root.get(StandardsUsageAnalysis_.startDate), form.getStartDate());
			}

			if (form.getFinishDate() != null) {
				cb.lessThanOrEqualTo(root.get(StandardsUsageAnalysis_.finishDate), form.getFinishDate());
			}

			cq.where(clauses);
			cq.distinct(true);
			CompoundSelection<StandardsUsageAnalysisDTO> selection = cb.construct(StandardsUsageAnalysisDTO.class,
					root.get(StandardsUsageAnalysis_.id), inst.get(Instrument_.plantid), 
					certif.get(Certificate_.certid), certif.get(Certificate_.certno),
					root.get(StandardsUsageAnalysis_.startDate), root.get(StandardsUsageAnalysis_.finishDate),
					root.get(StandardsUsageAnalysis_.status), inst.get(Instrument_.plantno),
					inst.get(Instrument_.serialno), modelName.get(Translation_.translation),
					cb.coalesce(modelMfr.get(Mfr_.name), instMfr.get(Mfr_.name)),
					cb.coalesce(model.get(InstrumentModel_.model), inst.get(Instrument_.modelname)));

			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(StandardsUsageAnalysis_.startDate)));

			return Triple.of(root, selection, order);
		});
		return prs;
	}

	@Override
	public StandardsUsageAnalysis getStandardsUsageAnalysisFromInstrument(Integer plantId) {
		return getFirstResult(cb -> {
			CriteriaQuery<StandardsUsageAnalysis> cq = cb.createQuery(StandardsUsageAnalysis.class);
			Root<StandardsUsageAnalysis> root = cq.from(StandardsUsageAnalysis.class);
			Join<StandardsUsageAnalysis, Instrument> instrument = root.join(StandardsUsageAnalysis_.instrument);
			cq.where(cb.equal(instrument.get(Instrument_.plantid), plantId));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<StandardsUsageAnalysisDTO> certificatesConnectedToMeasurementStandards(Integer businessSubdivId, LocalDate startDate,
																					   List<CalibrationVerificationStatus> calibrationStatus, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<StandardsUsageAnalysisDTO> cq = cb.createQuery(StandardsUsageAnalysisDTO.class);
			Root<StandardUsed> standardUsed = cq.from(StandardUsed.class);
			Join<StandardUsed, Instrument> instrument = standardUsed.join(StandardUsed_.inst);
			Join<Instrument, Company> company = instrument.join(Instrument_.comp);
			Join<Company, Contact> contact = company.join(Company_.defaultBusinessContact);
			Join<Contact, Subdiv> businessSubdiv = contact.join(Contact_.sub);
			Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model);
			Expression<String> modelName = joinTranslation(cb, model, InstrumentModel_.nameTranslations, locale);
			Join<Instrument, JobItem> jobitem = instrument.join(Instrument_.jobItems);
			Join<JobItem, CertLink> certLinks = jobitem.join(JobItem_.certLinks, JoinType.LEFT);
			Join<CertLink, Certificate> certificate = certLinks.join(CertLink_.cert, JoinType.LEFT);
			Join<Certificate, Calibration> calibration = certificate.join(Certificate_.cal);
			
			Subquery<Integer> standardsSubQuery = cq.subquery(Integer.class);
			Root<StandardsUsageAnalysis> sqStandards = standardsSubQuery.from(StandardsUsageAnalysis.class);
			Join<StandardsUsageAnalysis, Certificate> standardsAlreadyCreatedForCertificate = sqStandards.join(StandardsUsageAnalysis_.certificate);
			standardsSubQuery.where(cb.equal(sqStandards.get(StandardsUsageAnalysis_.instrument), instrument));
			standardsSubQuery.select(cb.max(standardsAlreadyCreatedForCertificate.get(Certificate_.certid)));
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(businessSubdiv.get(Subdiv_.subdivid), businessSubdivId));
			clauses.getExpressions().add(cb.greaterThanOrEqualTo(calibration.get(Calibration_.calDate), startDate));
			clauses.getExpressions().add(certificate.get(Certificate_.calibrationVerificationStatus).in(calibrationStatus));
			clauses.getExpressions().add(standardsSubQuery.isNull());
			cq.where(clauses);
			cq.distinct(true);
			cq.select(cb.construct(StandardsUsageAnalysisDTO.class, 
					certificate.get(Certificate_.certid),
					certificate.get(Certificate_.certno),
					certificate.get(Certificate_.thirdCertNo),
					instrument.get(Instrument_.plantid),
					modelName,
					calibration.get(Calibration_.calDate),
					certificate.get(Certificate_.calibrationVerificationStatus)));
			return cq;
    	});
    }

}
