package org.trescal.cwms.core.jobs.jobitem.entity.defect;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Entity
@DiscriminatorValue("item")
public class ItemDefect extends Defect
{
	private boolean calSealBroken;
	private JobItem ji;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid")
	public JobItem getJi()
	{
		return this.ji;
	}

	@Column(name = "calsealbroken", columnDefinition="bit")
	public boolean isCalSealBroken()
	{
		return this.calSealBroken;
	}

	public void setCalSealBroken(boolean calSealBroken)
	{
		this.calSealBroken = calSealBroken;
	}

	public void setJi(JobItem ji)
	{
		this.ji = ji;
	}
}
