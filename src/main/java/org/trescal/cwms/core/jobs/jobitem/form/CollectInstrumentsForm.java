package org.trescal.cwms.core.jobs.jobitem.form;

import java.util.List;

public class CollectInstrumentsForm
{
	List<Integer> plantIds;

	public List<Integer> getPlantIds()
	{
		return this.plantIds;
	}

	public void setPlantIds(List<Integer> plantIds)
	{
		this.plantIds = plantIds;
	}
}