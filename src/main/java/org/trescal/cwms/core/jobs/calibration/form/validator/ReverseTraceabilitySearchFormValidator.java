package org.trescal.cwms.core.jobs.calibration.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.calibration.form.ReverseTraceabilitySearchForm;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class ReverseTraceabilitySearchFormValidator extends AbstractBeanValidator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(ReverseTraceabilitySearchForm.class);
	}
	
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			ReverseTraceabilitySearchForm form = (ReverseTraceabilitySearchForm) target;
			// date validation : the end date must be after the start date
			if(form.getStartCalDate() != null && form.getEndCalDate() != null && form.getStartCalDate().after(form.getEndCalDate())) {
				errors.rejectValue("startCalDate", "reversetraceabilitysearch.error.date", null,
						"The end date must be after the start date.");
			}
		}
	}
	
}
