package org.trescal.cwms.core.jobs.contractreview.entity.contractreview.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview;
import org.trescal.cwms.core.jobs.contractreview.projection.ContractReviewProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemPriceProjectionDto;

public interface ContractReviewDao extends BaseDao<ContractReview, Integer> {
	
	ContractReview getMostRecentContractReviewforJobItem(JobItem jobitem);
	List<ContractReview> getEagerContractReviewsForJob(int jobid);
	List<ContractReviewProjectionDTO> getProjectionDTOsForJobItem(Integer jobItemId);
	List<JobItemPriceProjectionDto> getPriceProjectionDTOs(Collection<Integer> jobItemIds);
	List<ContractReviewProjectionDTO> getCrProjectionDTOsyjob(Integer jobid);
	Long CountCryjob(Integer jobid);
	
	
}