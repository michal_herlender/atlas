package org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public interface JobItemWorkRequirementService extends BaseService<JobItemWorkRequirement, Integer> {

	JobItemWorkRequirement findJobItemWorkRequirement(int id);

	List<JobItemWorkRequirement> getForJobItemView(JobItem jobItem);

	ResultWrapper toggleCompleteJobItemWorkRequirement(int jiwrId);

	JobItemWorkRequirement duplicate(JobItemWorkRequirement jiwr);
}