package org.trescal.cwms.core.jobs.job.projection.service;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class JobProjectionCommand {
	// Mandatory values set in constructor
	private Locale locale;
	private Integer allocatedCompanyId;
	// Optional values set via options (default false)
	private Boolean loadContact;
	private Boolean loadCurrency;
	private Boolean loadOrganisation;
	private Boolean loadStatus;
	private Boolean loadTransportOut;
	private Boolean loadClientPOs;
	private Boolean countJobItems;
	private Boolean countJobExpenseItems; //Modification SK
	
	public static interface JOB_LOAD_CONTACT {};
	public static interface JOB_LOAD_CURRENCY {};
	public static interface JOB_LOAD_ORGANISATION {};
	public static interface JOB_LOAD_STATUS {};
	public static interface JOB_LOAD_TRANSPORT_OUT {};
	public static interface JOB_LOAD_CLIENT_POS {};
	public static interface JOB_COUNT_ITEMS {};
	public static interface JOB_EXPENSE_ITEMS {};
	
	public JobProjectionCommand(Locale locale, Integer allocatedCompanyId, 
			Class<?>... options) {
		if (locale == null) throw new IllegalArgumentException("locale must not be null");
		if (allocatedCompanyId == null) throw new IllegalArgumentException("allocatedCompanyId must not be null");
		this.locale = locale;
		this.allocatedCompanyId = allocatedCompanyId;
		List<Class<?>> args = Arrays.asList(options);
		this.loadContact = args.contains(JOB_LOAD_CONTACT.class);
		this.loadCurrency = args.contains(JOB_LOAD_CURRENCY.class);
		this.loadOrganisation = args.contains(JOB_LOAD_ORGANISATION.class); 
		this.loadStatus = args.contains(JOB_LOAD_STATUS.class); 
		this.loadTransportOut = args.contains(JOB_LOAD_TRANSPORT_OUT.class);
		this.loadClientPOs = args.contains(JOB_LOAD_CLIENT_POS.class);
		
		this.countJobItems = args.contains(JOB_COUNT_ITEMS.class);
		this.countJobExpenseItems = args.contains(JOB_EXPENSE_ITEMS.class);
	}

}
