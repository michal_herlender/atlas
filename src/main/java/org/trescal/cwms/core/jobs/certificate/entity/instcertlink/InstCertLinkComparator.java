package org.trescal.cwms.core.jobs.certificate.entity.instcertlink;

import java.util.Comparator;
import java.util.Date;

public class InstCertLinkComparator implements Comparator<InstCertLink>
{
	@Override
	public int compare(InstCertLink icl1, InstCertLink icl2)
	{
		int inst1 = icl1.getInst().getPlantid();
		int inst2 = icl2.getInst().getPlantid();

		if (inst1 == inst2)
		{
			Date d1 = icl1.getCert().getCalDate();
			Date d2 = icl2.getCert().getCalDate();

			if (d1.compareTo(d2) == 0)
			{
				return Integer.valueOf(icl1.getId()).compareTo(icl2.getId());
			}
			else
			{
				return d1.compareTo(d2);
			}
		}
		else
		{
			return Integer.valueOf(inst1).compareTo(inst2);
		}
	}
}
