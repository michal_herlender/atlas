package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument;

import java.io.File;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.GsoDocumentLink;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.GsoDocumentLinkComparator;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.tools.EncryptionTools;
import org.trescal.cwms.core.tools.supportedlocale.LocaleFilter;

@Entity
@Table(name = "gsodocument")
public class GsoDocument extends Auditable implements ComponentEntity {

	private Logger logger = LoggerFactory.getLogger(LocaleFilter.class);
	private int id;
	private String documentNumber;
	private Date documentDate;
	private File directory;
	private List<Email> sentEmails;
	private Set<GsoDocumentLink> links;
	private GeneralServiceOperation gso;
	private GsoDocument supplementaryFor;
	
	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@Length(max = 60)
	@Column(name = "documentnumber", nullable = true, columnDefinition = "varchar(60)")
	public String getDocumentNumber() {
		return documentNumber;
	}
	
	@Column(name = "documentdate")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDocumentDate() {
		return this.documentDate;
	}

	@OneToMany(mappedBy = "gsoDocument", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(GsoDocumentLinkComparator.class)
	public Set<GsoDocumentLink> getLinks() {
		return this.links;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "gsoid", nullable = false)
	public GeneralServiceOperation getGso() {
		return this.gso;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "supplementaryforid")
	public GsoDocument getSupplementaryFor() {
		return this.supplementaryFor;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}

	public void setLinks(Set<GsoDocumentLink> links) {
		this.links = links;
	}

	public void setGso(GeneralServiceOperation gso) {
		this.gso = gso;
	}

	public void setSupplementaryFor(GsoDocument supplementaryFor) {
		this.supplementaryFor = supplementaryFor;
	}

	@Override
	@Transient
	public Contact getDefaultContact() {
		return null;
	}

	@Transient
	@Override
	public File getDirectory() {
		return this.directory;
	}

	@Override
	@Transient
	public List<Email> getSentEmails() {
		return this.sentEmails;
	}

	@Override
	@Transient
	public boolean isAccountsEmail() {
		return false;
	}

	@Override
	public void setDirectory(File file) {
		this.directory = file;
	}

	@Override
	public void setSentEmails(List<Email> emails) {
		this.sentEmails = emails;
	}

	@Override
	@Transient
	public String getIdentifier() {
		return this.documentNumber;
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity() {
		if (this.getLinks() != null) {
			if (this.getLinks().size() > 0) {
				return this.getLinks().iterator().next().getJi().getJob();
			}
		}

		return null;
	}
	
	@Transient
	public String getEncryptedGsoDocumentFilePath() {
		try {
			return URLEncoder.encode(EncryptionTools.encrypt(this.getGsoDocumentFilePath()), "UTF-8");
		} catch (Exception ex) {
			logger.error("Error encrypting filePath", ex);
			return "";
		}
	}

	@Transient
	public String getGsoDocumentFilePath() {
		String fileName = this.documentNumber.concat(".pdf");
		return this.directory.getAbsolutePath().concat(File.separator).concat(fileName);
	}
	
	@Transient
	public String getGsoDocumentFileName() {
		return this.documentNumber.concat(".pdf");
	}
}