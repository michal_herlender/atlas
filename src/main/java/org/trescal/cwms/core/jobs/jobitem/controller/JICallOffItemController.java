package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.db.CallOffItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.db.JobItemActivityService;
import org.trescal.cwms.core.jobs.jobitem.form.JICallOffItemForm;
import org.trescal.cwms.core.jobs.jobitem.form.JICallOffItemValidator;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.db.StateGroupLinkService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class JICallOffItemController extends JobItemController {
	private static final Logger logger = LoggerFactory.getLogger(JIContractReviewController.class);

	@Autowired
	private CallOffItemService coiServ;
	@Autowired
	private JobItemActivityService jiaServ;
	@Autowired
	private JICallOffItemValidator validator;
	@Autowired
	private StateGroupLinkService sglService;
	@Autowired
	private UserService userService;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("form")
	protected JICallOffItemForm formBackingObject(@RequestParam(value = "jobitemid", required = true) Integer jobItemId)
			throws Exception {
		JICallOffItemForm form = new JICallOffItemForm();
		JobItem ji = this.jobItemService.findEagerJobItem(jobItemId);
		if ((ji == null) || (jobItemId == 0)) {
			logger.error("Jobitem with id " + jobItemId + " could not be found");
			throw new Exception("Unable to locate requested jobitem");
		}
		form.setJi(ji);
		if (this.sglService.containsStateGroup(ji.getState().getGroupLinks(), StateGroup.CALLEDOFF)) {
			form.setCallOffItem(this.coiServ.getActiveForJobItem(ji.getJobItemId()));
		}
		if (this.jiaServ.getIncompleteActivityForItem(ji) == null) {
			form.setIncompleteActivity(false);
		} else {
			form.setIncompleteActivity(true);
		}
		Map<Integer, Integer> jobItemIds = new HashMap<Integer, Integer>();
		Map<Integer, String> reasons = new HashMap<Integer, String>();
		for (JobItem i : form.getJi().getJob().getItems()) {
			jobItemIds.put(i.getJobItemId(), null);
			reasons.put(i.getJobItemId(), "");
		}
		form.setJobItemIds(jobItemIds);
		form.setReasons(reasons);
		return form;
	}

	@PreAuthorize("@hasJobItemAuthorityOnConnectedSubdiv.check('JI_CALLOFF_ITEM')")
	@RequestMapping(value = "/jicalloffitem.htm", method = RequestMethod.POST)
	protected String onSubmit(Model model, @RequestParam(value = "jobitemid", required = true) Integer jobItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@Valid @ModelAttribute("form") JICallOffItemForm form, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData(model, username, subdivDto, jobItemId);
		}
		Contact currentContact = userService.get(username).getCon();
		this.coiServ.createCallOffItems(form, currentContact);
		return "redirect:calloffitems.htm";
	}

	@PreAuthorize("hasAuthority('JI_CALLOFF_ITEM')")
	@RequestMapping(value = "/jicalloffitem.htm", method = RequestMethod.GET)
	protected String referenceData(Model model, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@RequestParam(value = "jobitemid", required = true) Integer jobItemId) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		Map<String, Object> refData = super.referenceData(jobItemId, contact, subdivDto.getKey(),username);
		model.addAllAttributes(refData);
		model.addAttribute("selectableItems", getSelectableItems(jobItem.getJob()));
		return "trescal/core/jobs/jobitem/jicalloffitem";
	}

	/*
	 * Arraylist indicating whether job item is selectable as a call off item
	 * (Should be active status, and not already called off)
	 */
	protected List<Boolean> getSelectableItems(Job job) {
		ArrayList<Boolean> result = new ArrayList<>();
		for (JobItem ji : job.getItems()) {
			boolean jiResult = ji.getState().getActive()
					&& !sglService.containsStateGroup(ji.getState().getGroupLinks(), StateGroup.CALLEDOFF);
			result.add(jiResult);
		}
		return result;
	}
}