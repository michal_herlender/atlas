package org.trescal.cwms.core.jobs.calibration.entity.standardused;

import java.util.Comparator;

public class StandardUsedComparator implements Comparator<StandardUsed>
{
	@Override
	public int compare(StandardUsed o1, StandardUsed o2)
	{
		if ((o1.getCalibration() == null) && (o2.getCalibration() == null))
		{
			return 1;
		}
		else if ((o1.getCalibration() == null) && (o2.getCalibration() != null))
		{
			return -1;
		}
		else if ((o1.getCalibration() != null) && (o2.getCalibration() == null))
		{
			return 1;
		}
		else
		{
			if ((o1.getCalibration().getCompleteTime() != null)
					&& (o2.getCalibration().getCompleteTime() != null))
			{
				if (o2.getCalibration().getCompleteTime().compareTo(o1.getCalibration().getCompleteTime()) != 0)
				{
					return o2.getCalibration().getCompleteTime().compareTo(o1.getCalibration().getCompleteTime());
				}
				else
				{
					return ((Integer) o1.getId()).compareTo(o2.getId());
				}
			}
			else
			{
				if (o1.getCalibration().getId() == o2.getCalibration().getId())
				{
					if (o1.getId() == o2.getId())
					{
						return ((Integer) o1.getInst().getPlantid()).compareTo(o2.getInst().getPlantid());
					}
					else
					{
						return ((Integer) o1.getId()).compareTo(o2.getId());
					}
				}
				else
				{
					return ((Integer) o1.getId()).compareTo(o2.getId());
				}
			}
		}
	}
}
