package org.trescal.cwms.core.jobs.calibration.dto;

import java.time.LocalDate;
import java.util.Date;

import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysisstatus.StandardsUsageAnalysisStatus;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class StandardsUsageAnalysisDTO {

	private Integer standardsUsageAnalysisId;
	private Integer instId;
	private Date startDate;
	private Date finishDate;
	private StandardsUsageAnalysisStatus status;
	private String plantNo;
	private String serialNo;
	private String instrumentModelName;
	private String brand;
	private String modelNo;
	private Integer certid;
	private String certno;
	private String certExternalno;
	private CalibrationVerificationStatus calibrationVerificationstatus;
	private LocalDate calDate;

	public StandardsUsageAnalysisDTO(Integer standardsUsageAnalysisId, Integer instId, Integer certid, String certno,
			Date startDate, Date finishDate, StandardsUsageAnalysisStatus status, String plantNo, String serialNo,
			String instrumentModelName, String brand, String modelNo) {
		super();
		this.standardsUsageAnalysisId = standardsUsageAnalysisId;
		this.instId = instId;
		this.certid = certid;
		this.certno = certno;
		this.startDate = startDate;
		this.finishDate = finishDate;
		this.status = status;
		this.plantNo = plantNo;
		this.serialNo = serialNo;
		this.instrumentModelName = instrumentModelName;
		this.brand = brand;
		this.modelNo = modelNo;
	}

	public StandardsUsageAnalysisDTO(Integer certid, String certno, String certExternalno, Integer instId,
			String instrumentModelName, LocalDate calDate, CalibrationVerificationStatus calibrationVerificationstatus) {
		super();
		this.certid = certid;
		this.certno = certno;
		this.certExternalno = certExternalno;
		this.instId = instId;
		this.instrumentModelName = instrumentModelName;
		this.calDate = calDate;
		this.calibrationVerificationstatus = calibrationVerificationstatus;
	}

}
