package org.trescal.cwms.core.jobs.jobitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemActivityRequestDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemActivityResponseDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.db.JobItemActivityService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.translation.Translation;

import java.time.ZoneId;
import java.util.Locale;
import java.util.Optional;

@RestController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME})
public class JobItemActivityController {

    private JobItemActivityService jobItemActivityService;

    private UserService userService;

    public JobItemActivityController(JobItemActivityService jobItemActivityService,UserService userService) {
        this.jobItemActivityService = jobItemActivityService;
        this.userService = userService;
    }

    @PostMapping("/create_incomplete_activity")
    @ResponseBody
    public JobItemActivityResponseDTO createIncompleteActivity (@RequestBody JobItemActivityRequestDTO jobItemActivityRequestDTO,
                                                                @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                                                                Locale locale)
    {
        Contact contact =  userService.get(username).getCon();

        JobItemActivity incompleteAction = jobItemActivityService.createIncompleteAction(
                jobItemActivityRequestDTO.getActivityId(),
                jobItemActivityRequestDTO.getJobItemId()
                , contact);

        Optional<String> activityTranslation = incompleteAction.getActivity().getTranslations()
                .stream()
                .filter(t -> locale.equals(t.getLocale()))
                .map(Translation::getTranslation)
                .findAny();

        Optional<String> startStatusTranslation = incompleteAction.getStartStatus().getTranslations()
            .stream()
            .filter(t -> locale.equals(t.getLocale()))
            .map(Translation::getTranslation)
            .findAny();

        JobItemActivityResponseDTO jobItemActivityResponseDTO = JobItemActivityResponseDTO.builder()
                .activityId(incompleteAction.getActivity().getStateid())
                .jobItemId(incompleteAction.getJobItem().getJobItemId())
                .description(activityTranslation.orElse(""))
                .startStatus(startStatusTranslation.orElse(""))
                .startedBy(contact.getName())
                .startStamp(incompleteAction.getStartStamp().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                .build();

        return jobItemActivityResponseDTO;
    }
}
