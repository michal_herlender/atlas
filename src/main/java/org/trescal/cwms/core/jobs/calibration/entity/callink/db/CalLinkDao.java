package org.trescal.cwms.core.jobs.calibration.entity.callink.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;

public interface CalLinkDao extends BaseDao<CalLink, Integer> {}