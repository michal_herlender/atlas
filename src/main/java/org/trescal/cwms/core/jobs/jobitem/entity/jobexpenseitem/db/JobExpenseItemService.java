package org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.dto.ExpenseItemDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.ExpenseItemOnClientPurchaseOrderDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.InvoiceableItemsByCompanyDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.db.AbstractJobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;

public interface JobExpenseItemService extends AbstractJobItemService<JobExpenseItem> {

	JobExpenseItem copy(JobExpenseItem expenseItem);

	void copyItemsToNewJob(List<Integer> expenseItemIds, Job newJob);

	List<InvoiceableItemsByCompanyDTO> getInvoiceable(Integer allocatedCompanyId, Integer allocatedSubdivId);

	/**
	 * @param clientCompany      client company
	 * @param allocatedCompanyId job owning business company
	 * @param allocatedSubdivId  job owning business subdivision
	 * @param completeJob        status of job owning (complete, not complete, or
	 *                           both(by default))
	 * 
	 *                           either allocatedCompanyId or allocatedSubdivId must
	 *                           be specified
	 * 
	 * @return list of all {@link JobExpenseItem}s for a client, which are not
	 *         already invoiced
	 */
	List<JobExpenseItem> getInvoiceable(Company clientCompany, Integer allocatedCompanyId, Integer allocatedSubdivId,
			String completeJob);

	List<ExpenseItemOnClientPurchaseOrderDTO> getExpenseItemsOnClientBPO(Integer poId, Integer jobId);

	List<ExpenseItemOnClientPurchaseOrderDTO> getExpenseItemsOnClientPO(Integer poId, Integer jobId);

	List<ExpenseItemDTO> getAvailableForInvoice(Integer invoiceId);
}