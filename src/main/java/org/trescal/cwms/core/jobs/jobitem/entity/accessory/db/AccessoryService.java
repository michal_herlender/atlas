package org.trescal.cwms.core.jobs.jobitem.entity.accessory.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.Accessory;

public interface AccessoryService extends BaseService<Accessory, Integer> {

	/**
	 * Count all accessories either linked to a job item or to a job item group of
	 * this job. Accessory free texts are counted as one accessory.
	 * 
	 * @param jobId identifier of the job
	 * @return quantity of accessories linked to this job
	 */
	Integer countByJob(Integer jobId);
}