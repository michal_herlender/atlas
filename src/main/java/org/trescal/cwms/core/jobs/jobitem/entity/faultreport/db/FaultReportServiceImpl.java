package org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.batch.jobs.operationsimport.ImportedOperationItem;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.filenamingservice.FailureReportFileNamingService;
import org.trescal.cwms.core.failurereport.dto.FailureReportDTO;
import org.trescal.cwms.core.failurereport.enums.FailureReportFinalOutcome;
import org.trescal.cwms.core.failurereport.enums.FailureReportRecommendationsEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportSourceEnum;
import org.trescal.cwms.core.failurereport.enums.FaultReportVersionEnum;
import org.trescal.cwms.core.failurereport.form.FailureReportForm;
import org.trescal.cwms.core.failurereport.form.FailureReportForm.FaultReportFormAction;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db.WorkRequirementService;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.MultiCurrencyUtils;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.*;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_FailureReportClientResponse.Input;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_NoOperationPerformed;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.rest.adveso.dto.AdvesoFailureReportClientResponseInputDTO;
import org.trescal.cwms.rest.tlm.dto.TLMFailureReportInputDTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Service("FaultReportService")
public class FaultReportServiceImpl implements FaultReportService {

	private static final String ON_HOLD_FOR_ONSITE_REPAIR_AND_TP_FR_OUTCOMES = "On hold - Site job, Repair or Third party issue";
	@Autowired
	private ContactService contactService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private JobService jobService;
	@Autowired
	private FaultReportDao faultReportDao;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private HookInterceptor_CreateFaultReport interceptor_createFaultReport;
	@Autowired
	private HookInterceptor_UpdateFaultReport interceptor_updateFaultReport;
	@Autowired
	private HookInterceptor_SelectFailureReportFinalOutcome hookInterceptor_SelectFailureReportFinalOutcome;
	@Autowired
	private HookInterceptor_FailureReportClientResponse hookInterceptor_FailureReportClientResponse;
	@Autowired
	private ComponentDirectoryService compDirService;
	@Autowired
	private SystemComponentService scService;
	@Autowired
	private DocumentService documentService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private HookInterceptor_MakeFRAvailableForExternalManegementSystem hook_MakeFRAvailableForExternalSysteme;
	@Autowired
	private LinkToAdveso linkToAdvesoSettings;
	@Autowired
	private WorkRequirementService workRequirementService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private HookInterceptor_PlaceOnHold hookInterceptor_PlaceOnHold;
	@Autowired
	private HookInterceptor_SendFailureReportToClient hookInterceptor_SendFailureReportToClient;
	@Autowired
	private ItemStateService itemStateService;
	@Autowired
	private CalibrationService calibrationService;
	@Autowired
	private ActionOutcomeService actionOutcomeServ;

	public void deleteFaultReport(FaultReport faultreport) {
		this.faultReportDao.remove(faultreport);
	}

	public FaultReport findFaultReport(int id) {
		return this.faultReportDao.find(id);
	}

	public List<FaultReport> getAllFaultReports() {
		return this.faultReportDao.findAll();
	}

	@Override
	public FaultReport createFailureReportForOnsiteOperationsImportation(ImportedOperationItem dto,
			JobItemWorkRequirement jiwr, Subdiv allocatedSubdiv, boolean isFrWithoutPriorCal) {

		JobItem jobItem = jiwr.getJobitem();

		FaultReport fr = new FaultReport();

		fr.setStates(dto.getFrStates());
		fr.setIssueDate(dto.getOperationDate());
		fr.setOtherState(dto.getFrOtherState());

		// defaulted settings, since coming from onsite calibrations
		// importation
		fr.setSource(FailureReportSourceEnum.TLM);
		fr.setVersion(FaultReportVersionEnum.FAILURE_REPORT);
		fr.setManagerValidation(true);
		fr.setAwaitingFinalisation(true);
		fr.setOperationInLaboratory(false);
		fr.setNoCostingRequired(true);

		if (dto.getFrSubdivisionId() != null)
			fr.setOrganisation(subdivService.get(dto.getFrSubdivisionId()));
		else
			fr.setOrganisation(allocatedSubdiv);

		fr.setJobItem(jobItem);
		fr.setTechnician(dto.getOperationBy());
		fr.setTechnicianComments(dto.getFrTechnicianComment());
		fr.setManagerComments(dto.getFrValidationComment());
		fr.setManagerValidationBy(dto.getOperationValidatedBy());
		fr.setManagerValidationOn(dto.getOperationValidatedOn());
		fr.setClientResponseNeeded(dto.getFrClientResponseNeeded());
		fr.setSendToClient(dto.getFrSendToClient());
		fr.setDispensationComments(dto.getFrDispensationComment());
		if (StringUtils.isNotBlank(fr.getDispensationComments()))
			fr.setDispensation(true);
		fr.setRecommendations(dto.getFrRecommendations());
		fr.setEstDeliveryTime(dto.getFrEstimatedDeliveryTime());
		fr.setClientAlias(dto.getFrClientAlias());

		fr.setClientApproval(dto.getFrClientDecision());
		fr.setClientApprovalOn(dto.getFrClientApprovalOn());
		fr.setClientComments(dto.getFrClientComments());

		if (dto.getFrOperationByTrescal() == null)
			fr.setOperationByTrescal(true);
		else
			fr.setOperationByTrescal(dto.getFrOperationByTrescal());

		ServiceType nextServiceType = null;
		// there is a calibration next
		if (dto.getNextServiceTypeId() != null) {
			nextServiceType = serviceTypeService.get(dto.getNextServiceTypeId());
		}

		// deduce only if we didn't receive the finaloutcome
		if (dto.getFrFinalOutcome() == null) {
			fr.setFinalOutcome(deduceFrOutcomeFromRecommendation(fr.getOperationByTrescal(), fr.getRecommendations(),
					nextServiceType, dto.getFrClientResponseNeeded(), dto.getFrClientDecision(), true));
		}
		boolean isAdjustmentOutcome = fr.getRecommendations().contains(FailureReportRecommendationsEnum.ADJUSTMENT);
		boolean isRestriction = (fr.getRecommendations().contains(FailureReportRecommendationsEnum.RESTRICTION)
				|| fr.getRecommendations().contains(FailureReportRecommendationsEnum.OTHER));
		this.completeWrDependingOnFrOutcome(jiwr, fr.getFinalOutcome(), isAdjustmentOutcome, isRestriction,
				isFrWithoutPriorCal);

		if (fr.getFinalOutcome() != null && BooleanUtils.isFalse(fr.getClientResponseNeeded()))
			fr.setOutcomePreselected(true);

		jobItem.getFailureReports().add(fr);
		String frNumber = numerationService.generateNumber(NumerationType.FAULT_REPORT, fr.getOrganisation().getComp(),
				fr.getOrganisation());
		fr.setFaultReportNumber(frNumber);

		this.faultReportDao.persist(fr);

		return fr;
	}

	/**
	 * According to DB table see Ticket DEV-1455 (new table in comments) byTrescal:
	 * either by Trescal or a TP recs: list of recommendations of the FR
	 * expectedServiceType: the expected service type of the following operation,
	 * either the service type of the following line in case of the import mode, or
	 * the service type of the current WR in cas of connected mode
	 * clientResponseNeeded: wether the client response was/is needed.
	 * clientDecision: client decision on the recommendations, either true or false
	 * importMode: true if it's the import mode, false if it's the connected
	 * mode/web services.
	 */
	@Override
	public FailureReportFinalOutcome deduceFrOutcomeFromRecommendation(boolean byTrescal,
			List<FailureReportRecommendationsEnum> recs, ServiceType expectedServiceType, Boolean clientResponseNeeded,
			Boolean clientDecision, boolean importMode) {

		// if client response is/was needed
		if (BooleanUtils.isTrue(clientResponseNeeded)) {
			// if we're still expecting the client response
			if (clientDecision == null)
				return null;
			else if (!clientDecision && importMode && expectedServiceType == null)
				// if the client has refused our proposal in the imported mode and there are no
				// other operations ahead to import
				return FailureReportFinalOutcome.RETURN_TO_CLIENT;
		}

		// REPAIR
		if (recs.contains(FailureReportRecommendationsEnum.REPAIR)) {
			if (byTrescal)
				return FailureReportFinalOutcome.INTERNAL_REPAIR;
			else
				return FailureReportFinalOutcome.THIRD_PARTY_REPAIR;
		} else {
			// SCRAP
			if (recs.contains(FailureReportRecommendationsEnum.SCRAPPING_PROPOSAL))
				return FailureReportFinalOutcome.SCRAP;
			else {
				// CALIBRATION
				if (recs.contains(FailureReportRecommendationsEnum.CALIBRATION_WITH_JUDGMENT)) {
					if (byTrescal)
						return FailureReportFinalOutcome.INTERNAL_CALIBRATION_WITH_JUDGMENT;
					else
						return FailureReportFinalOutcome.THIRD_PARTY_CALIBRATION_WITH_JUDGMENT;
				} else if (recs.contains(FailureReportRecommendationsEnum.CALIBRATION_WITHOUT_JUDGMENT)) {
					if (byTrescal)
						return FailureReportFinalOutcome.INTERNAL_CALIBRATION_WITHOUT_JUDGMENT;
					else
						return FailureReportFinalOutcome.THIRD_PARTY_CALIBRATION_WITHOUT_JUDGMENT;
				} else if (recs.contains(FailureReportRecommendationsEnum.OTHER)) {
					// OTHER
					if (expectedServiceType != null) {
						if (expectedServiceType.getCalibrationType().getCalibrationWithJudgement()) {
							if (byTrescal)
								return FailureReportFinalOutcome.INTERNAL_CALIBRATION_WITH_JUDGMENT;
							else
								return FailureReportFinalOutcome.THIRD_PARTY_CALIBRATION_WITH_JUDGMENT;
						} else {
							if (byTrescal)
								return FailureReportFinalOutcome.INTERNAL_CALIBRATION_WITHOUT_JUDGMENT;
							else
								return FailureReportFinalOutcome.THIRD_PARTY_CALIBRATION_WITHOUT_JUDGMENT;
						}
					} else
						return FailureReportFinalOutcome.RETURN_TO_CLIENT;
				} else if (recs.contains(FailureReportRecommendationsEnum.ADJUSTMENT) && expectedServiceType != null) {
					// ADJUSTMENT
					if (expectedServiceType.getCalibrationType().getCalibrationWithJudgement()) {
						if (byTrescal)
							return FailureReportFinalOutcome.INTERNAL_CALIBRATION_WITH_JUDGMENT;
						else
							return FailureReportFinalOutcome.THIRD_PARTY_CALIBRATION_WITH_JUDGMENT;
					} else {
						if (byTrescal)
							return FailureReportFinalOutcome.INTERNAL_CALIBRATION_WITHOUT_JUDGMENT;
						else
							return FailureReportFinalOutcome.THIRD_PARTY_CALIBRATION_WITHOUT_JUDGMENT;
					}
				}
			}
		}

		// could not deduce, WF will go to 'awaiting selection of failure report
		// outcome' if it reached the step of the final outcome of the FR.
		return null;
	}

	private void completeWrDependingOnFrOutcome(JobItemWorkRequirement jiwr, FailureReportFinalOutcome finaloutcome,
			boolean isAdjustment, boolean isRestriction, boolean isFrWithoutPriorCal) {
		if (finaloutcome == null)
			return;

		JobItem jobItem = jiwr.getJobitem();

		if (finaloutcome.equals(FailureReportFinalOutcome.RETURN_TO_CLIENT)
				|| finaloutcome.equals(FailureReportFinalOutcome.SCRAP))
			this.workRequirementService.checkAllWorkRequirement(jobItem);

		switch (finaloutcome) {
			case INTERNAL_REPAIR:
			case THIRD_PARTY_REPAIR:
			case THIRD_PARTY_CALIBRATION_WITH_JUDGMENT:
			case THIRD_PARTY_CALIBRATION_WITHOUT_JUDGMENT:
				if (!isFrWithoutPriorCal || isWRincompatibleWithFrOutcome(finaloutcome, jiwr.getWorkRequirement()))
					this.workRequirementService.checkCurrentWorkRequirement(jobItem);
				break;
			case INTERNAL_CALIBRATION_WITH_JUDGMENT:
			case INTERNAL_CALIBRATION_WITHOUT_JUDGMENT:
				if (!isAdjustment && !isRestriction
					&& (!isFrWithoutPriorCal || isWRincompatibleWithFrOutcome(finaloutcome, jiwr.getWorkRequirement())))
					this.workRequirementService.checkCurrentWorkRequirement(jobItem);
				break;
			default:
				break;
		}
	}

	private boolean isWRincompatibleWithFrOutcome(FailureReportFinalOutcome finaloutcome, WorkRequirement wr) {
		switch (finaloutcome) {
			case INTERNAL_REPAIR:
				if (wr.getServiceType().getRepair() && wr.getType().equals(WorkRequirementType.REPAIR))
					return false;
				break;
			case THIRD_PARTY_REPAIR:
				if (wr.getServiceType().getRepair() && wr.getType().equals(WorkRequirementType.THIRD_PARTY))
					return false;
				break;
			case THIRD_PARTY_CALIBRATION_WITH_JUDGMENT:
				if (wr.getServiceType().getCalibrationType().getCalibrationWithJudgement()
					&& wr.getType().equals(WorkRequirementType.THIRD_PARTY))
					return false;
			break;
		case THIRD_PARTY_CALIBRATION_WITHOUT_JUDGMENT:
			if (!wr.getServiceType().getCalibrationType().getCalibrationWithJudgement()
				&& wr.getType().equals(WorkRequirementType.THIRD_PARTY))
				return false;
			break;
		case INTERNAL_CALIBRATION_WITH_JUDGMENT:
			if (wr.getServiceType().getCalibrationType().getCalibrationWithJudgement()
				&& wr.getType().equals(WorkRequirementType.CALIBRATION))
				return false;
			break;
			case INTERNAL_CALIBRATION_WITHOUT_JUDGMENT:
				if (!wr.getServiceType().getCalibrationType().getCalibrationWithJudgement()
					&& wr.getType().equals(WorkRequirementType.CALIBRATION))
					return false;
				break;
			default:
				break;
		}
		return true;
	}

	@Override
	public FaultReport createFailureReportForTLMInterface(TLMFailureReportInputDTO dto) {

		JobItem ji = jobItemService.findJobItem(dto.getJobItemId());
		JobItemWorkRequirement jiwr = ji.getNextWorkReq();

		FaultReport fr = new FaultReport();

		Contact manager = null;

		Contact technician = contactService.getByHrId(dto.getTechnicianHrid());

		if (dto.getManagerHrid() != null)
			manager = contactService.getByHrId(dto.getManagerHrid());

		fr.setJobItem(ji);
		fr.setStates(dto.getStates());
		fr.setIssueDate(dto.getIssueDate());
		fr.setOtherState(dto.getOtherState());
		fr.setTechnician(technician);
		fr.setTechnicianComments(dto.getTechnicianComments());
		fr.setTrescalRef(dto.getTrescalRef());
		fr.setAwaitingFinalisation(true);

		fr.setNoCostingRequired(dto.getNoCostingRequired());
		fr.setVersion(FaultReportVersionEnum.FAILURE_REPORT);
		if (fr.getValueInBusinessCompanyCurrency() != null) {
			Company businessCompany = technician.getSub().getComp();
			fr.setCost(MultiCurrencyUtils.convertCurrencyValue(fr.getValueInBusinessCompanyCurrency(),
					businessCompany.getCurrency(), ji.getJob().getCurrency()));
		} else {
			fr.setCost(new BigDecimal(0));
		}
		fr.setEstAdjustTime(dto.getEstAdjustTime());
		fr.setEstRepairTime(dto.getEstRepairTime());
		fr.setEstDeliveryTime(dto.getEstDeliveryTime());
		fr.setNoCostingRequired(dto.getNoCostingRequired());
		fr.setOutcomePreselected(dto.getOutcomePreselected());
		fr.setDispensationComments(dto.getDispensationComments());
		if (StringUtils.isNotBlank(dto.getDispensationComments()))
			fr.setDispensation(true);
		fr.setSource(FailureReportSourceEnum.TLM);
		fr.setClientAlias(dto.getClientAlias());

		fr.setManagerComments(dto.getManagerComments());
		fr.setManagerValidation(dto.getManagerValidation());
		fr.setManagerValidationBy(manager);
		fr.setManagerValidationOn(dto.getManagerValidationOn());

		// TODO: to be revised when new FR version is deployed on TLM
		if (dto.getOperationByTrescal() == null)
			fr.setOperationByTrescal(true);
		else
			fr.setOperationByTrescal(dto.getOperationByTrescal());

		if (dto.getOperationInLaboratory() == null) {
			fr.setOperationInLaboratory(!ji.getJob().getType().equals(JobType.SITE));
		} else {
			fr.setOperationInLaboratory(dto.getOperationInLaboratory());
		}
		fr.setRecommendations(dto.getRecommendations());

		fr.setSendToClient(dto.getSendToClient());
		fr.setClientResponseNeeded(dto.getClientResponseNeeded());
		fr.setClientApproval(dto.getClientApproval());
		fr.setClientApprovalOn(dto.getClientApprovalOn());
		fr.setClientComments(dto.getClientComments());

		if (dto.getSubdivId() != null)
			fr.setOrganisation(subdivService.get(dto.getSubdivId()));
		else
			fr.setOrganisation(ji.getJob().getOrganisation());

		// create 'no operation performed' activities if the current status is 'awaiting
		// calibration'
		boolean isFrWithoutPriorCal = false;
		if (jobItemService.isReadyForCalibration(ji)) {
			isFrWithoutPriorCal = true;
			ActionOutcome failureReportRequired = actionOutcomeServ.getByTypeAndValue(
					ActionOutcomeType.NO_OPERATION_PERFORMED,
					ActionOutcomeValue_NoOperationPerformed.FAILURE_REPORT_REQUIRED);
			calibrationService.noOperationPerformed(dto.getJobItemId(), null, failureReportRequired.getId(), null,
					null);
		}

		// if the outcome is preselected deduce the final outcome
		if (BooleanUtils.isTrue(fr.getOutcomePreselected())) {

			// default to 'by Trescal' : DB 17/12/19
			if (fr.getOperationByTrescal() == null)
				fr.setOperationByTrescal(true);

			fr.setFinalOutcome(deduceFrOutcomeFromRecommendation(fr.getOperationByTrescal(), fr.getRecommendations(),
					ji.getNextWorkReq().getWorkRequirement().getServiceType(), fr.getClientResponseNeeded(),
					fr.getClientApproval(), false));

			if (!jobItemService.isReadyForCalibration(ji)) {
				// complete work requirement depending on the fr outcome
				boolean isAdjustmentOutcome = fr.getRecommendations()
						.contains(FailureReportRecommendationsEnum.ADJUSTMENT);
				boolean isRestriction = (fr.getRecommendations().contains(FailureReportRecommendationsEnum.RESTRICTION)
						|| fr.getRecommendations().contains(FailureReportRecommendationsEnum.OTHER));
				this.completeWrDependingOnFrOutcome(jiwr, fr.getFinalOutcome(), isAdjustmentOutcome, isRestriction,
						isFrWithoutPriorCal);
			}
		}

		ji.getFailureReports().add(fr);
		String frNumber = numerationService.generateNumber(NumerationType.FAULT_REPORT, fr.getOrganisation().getComp(),
				fr.getOrganisation());
		fr.setFaultReportNumber(frNumber);

		// create new Repair Job if Failure Report Final Outcome is Repair, and only if
		// it is not a site job
		String jiActivityRemark = null;
		if (!ji.getJob().getType().equals(JobType.SITE)
				&& (FailureReportFinalOutcome.INTERNAL_REPAIR.equals(fr.getFinalOutcome())
						|| FailureReportFinalOutcome.THIRD_PARTY_REPAIR.equals(fr.getFinalOutcome()))) {
			// create new repair job/jobitem from old job/jobitem
			JobItem newjobItem = this.jobService.createNewRepairJobAndJobItem(fr.getJobItem(), fr.getClientApprovalOn(),
				fr.getManagerValidationBy());
			Locale messageLocale = newjobItem.getJob().getOrganisation().getComp().getDocumentLanguage();
			jiActivityRemark = messageSource.getMessage(
				"jiactions.newrepairji", new Object[]{"<a href='jiactions.htm?jobitemid="
					+ newjobItem.getJobItemId() + "'><u>" + newjobItem.getItemCode() + "</u></a>"},
				messageLocale);
		}

		this.faultReportDao.persist(fr);

		// insert fault report
		this.createFailureReportActivites(fr, dto.getSpentTime(), dto.getRemarks(), jiActivityRemark);

		return fr;
	}

	@Override
	public void createOrUpdateFailureReport(FailureReportForm form, Contact contact, Subdiv allocatedSubdiv,
			boolean isFrWithoutPriorCal) {
		FaultReport fr;
		JobItem jobItem = this.jobItemService.findJobItem(form.getJobItemId());
		// If performing creation, set technician field
		if (FaultReportFormAction.NEW == form.getAction()) {

			fr = new FaultReport();
			fr.setFaultReportNumber(numerationService.generateNumber(NumerationType.FAULT_REPORT,
				allocatedSubdiv.getComp(), allocatedSubdiv));
			fr.setBusinessCompanyCurrency(allocatedSubdiv.getComp().getCurrency());
			fr.setIssueDate(new Date());
			fr.setNoCostingRequired(false);
			fr.setOutcomePreselected(false);
			fr.setRecommendations(new ArrayList<>());
			fr.setStates(new ArrayList<>());
			fr.setVersion(FaultReportVersionEnum.FAILURE_REPORT);
			fr.setAwaitingFinalisation(true);
			fr.setSource(FailureReportSourceEnum.CWMS);
			fr.setTechnician(contact);
			fr.setOrganisation(allocatedSubdiv);
			fr.setJobItem(jobItem);

			jobItem.getFailureReports().add(fr);
		} else {
			fr = this.findFaultReport(form.getFaultRepId());
		}

		if (FaultReportFormAction.EDIT_FULL.equals(form.getAction())
				|| FaultReportFormAction.EDIT_TECHNICIAN_MANAGER.equals(form.getAction())
				|| FaultReportFormAction.NEW.equals(form.getAction())
				|| FaultReportFormAction.VALIDATE.equals(form.getAction())) {
			fr.setClientResponseNeeded(form.getClientResponseNeeded());
			fr.setDispensationComments(form.getDispensationComments());
			fr.setTechnicianComments(form.getTechnicianComments());
			fr.setEstAdjustTime(form.getEstAdjustTime());
			fr.setEstRepairTime(form.getEstRepairTime());
			fr.setEstDeliveryTime(form.getEstDeliveryTime());
			fr.setOperationByTrescal(form.getOperationByTrescal());
			fr.setOperationInLaboratory(form.getOperationInLaboratory());
			fr.setOtherState(form.getOtherState());
			fr.getRecommendations().clear();
			if (form.getRecommendations() != null)
				fr.getRecommendations().addAll(form.getRecommendations());
			fr.setManagerComments(form.getManagerComments());
			fr.setSendToClient(form.getSendToClient());
			fr.getStates().clear();
			fr.getStates().addAll(form.getStates());
			fr.setValueInBusinessCompanyCurrency(form.getValueInBusinessCompanyCurrency());
			if (fr.getValueInBusinessCompanyCurrency() != null) {
				Company businessCompany = allocatedSubdiv.getComp();
				fr.setCost(MultiCurrencyUtils.convertCurrencyValue(fr.getValueInBusinessCompanyCurrency(),
						businessCompany.getCurrency(), fr.getJobItem().getJob().getCurrency()));
			} else {
				fr.setCost(new BigDecimal(0));
			}
			fr.setDispensation(form.getDispensation());
		}

		// Only update client response information when it's actually been shown
		// and entered by user
		if (FaultReportFormAction.EDIT_CLIENT_RESPONSE.equals(form.getAction())
				|| FaultReportFormAction.EDIT_FULL.equals(form.getAction())) {
			fr.setApprovalType(form.getClientApprovalType());
			fr.setClientApproval(form.getClientApproval());
			fr.setClientApprovalBy(form.getPersonid() != null ? this.contactService.get(form.getPersonid()) : null);
			fr.setClientApprovalOn(DateTools.dateFromLocalDateTime(form.getClientApprovalOn()));
			fr.setClientComments(form.getClientComments());
			fr.setClientOutcome(form.getClientOutcome());
			fr.setClientAlias(form.getClientAlias());

			// if actually data is entered
			if (form.getClientApproval() != null) {
				// fire hook :Client Response Received
				hookInterceptor_FailureReportClientResponse.recordAction(new Input(fr, new Date(), 0, null));
			}
		}

		// If performing validation, update manager information
		if (FaultReportFormAction.VALIDATE.equals(form.getAction())) {
			fr.setAwaitingFinalisation(false);
			fr.setManagerValidation(true);
			fr.setManagerValidationBy(contact);
			fr.setManagerValidationOn(new Date());
		}

		// final outcome
		boolean awaitingSelectionOfFinalOutcome = jobItemService.stateHasGroupOfKeyName(fr.getJobItem().getState(),
				StateGroup.AWAITING_SELECTION_OF_FR_OUTCOME);
		if (awaitingSelectionOfFinalOutcome && form.getFinalOutcome() != null) {

			fr.setFinalOutcome(form.getFinalOutcome());

			boolean isAdjustmentOutcome = fr.getRecommendations().contains(FailureReportRecommendationsEnum.ADJUSTMENT);
			boolean isRestriction = (fr.getRecommendations().contains(FailureReportRecommendationsEnum.RESTRICTION)
					|| fr.getRecommendations().contains(FailureReportRecommendationsEnum.OTHER));
			this.completeWrDependingOnFrOutcome(jobItem.getNextWorkReq(), form.getFinalOutcome(), isAdjustmentOutcome,
					isRestriction, isFrWithoutPriorCal);

			String jiActivityRemark = null;
			if (!jobItem.getJob().getType().equals(JobType.SITE)
					&& (FailureReportFinalOutcome.INTERNAL_REPAIR.equals(form.getFinalOutcome())
							|| FailureReportFinalOutcome.THIRD_PARTY_REPAIR.equals(form.getFinalOutcome()))) {
				// create new repair job/jobitem from old job/jobitem
				JobItem newjobItem = this.jobService.createNewRepairJobAndJobItem(fr.getJobItem(),
					fr.getClientApprovalOn(), contact);
				Locale messageLocale = newjobItem.getJob().getOrganisation().getComp().getDocumentLanguage();
				jiActivityRemark = messageSource.getMessage(
					"jiactions.newrepairji", new Object[]{"<a href='jiactions.htm?jobitemid="
						+ newjobItem.getJobItemId() + "'><u>" + newjobItem.getItemCode() + "</u></a>"},
					messageLocale);
			}

			org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_SelectFailureReportFinalOutcome.Input input = new org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_SelectFailureReportFinalOutcome.Input(
					fr.getJobItem(), jiActivityRemark, new Date(), 5, null);
			hookInterceptor_SelectFailureReportFinalOutcome.recordAction(input);

		}

		if (form.getFaultRepId() == 0) {
			this.faultReportDao.persist(fr);
			this.interceptor_createFaultReport.recordAction(new HookInterceptor_CreateFaultReport.Input(fr));
		} else {
			// this.faultReportDao.update(fr);
			this.interceptor_updateFaultReport.recordAction(new HookInterceptor_UpdateFaultReport.Input(fr));
			if ((fr.getRequireDeliveryToClient() == null || !fr.getRequireDeliveryToClient())
					&& FaultReportFormAction.VALIDATE.equals(form.getAction())) {
				// if we have to send it to the
				// client and also we're linked to Adveso
				// automatically register the activity 'available to external
				// management system'
				boolean isLinkedToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
						jobItem.getJob().getCon().getSub().getSubdivid(),
						jobItem.getJob().getOrganisation().getComp().getCoid(),  new Date());
				boolean makeFRAvailable = BooleanUtils.isTrue(fr.getSendToClient()) && isLinkedToAdveso;
				if (makeFRAvailable) {
					HookInterceptor_MakeFRAvailableForExternalManegementSystem.Input input = new HookInterceptor_MakeFRAvailableForExternalManegementSystem.Input(
							fr, fr.getTechnician(), fr.getManagerValidationOn(), 0, null);
					hook_MakeFRAvailableForExternalSysteme.recordAction(input);
				}
			}
		}

	}

	@Override
	public void createFailureReportActivites(FaultReport fr, Integer timeSpent, String remark,
			String validationRemark) {

		this.interceptor_createFaultReport.recordAction(new HookInterceptor_CreateFaultReport.Input(fr, timeSpent,
			remark, fr.getTechnician(), fr.getIssueDate(), fr.getIssueDate()));

		// temporary modify these for the WR to work
		fr.setAwaitingFinalisation(false);
		Boolean clientApprovalOldValue = null;
		if (fr.getClientApproval() != null) {
			clientApprovalOldValue = fr.getClientApproval();
			fr.setClientApproval(null);
		}

		/* validation */
		this.interceptor_updateFaultReport.recordAction(new HookInterceptor_UpdateFaultReport.Input(fr, 0,
			validationRemark, fr.getManagerValidationBy(), fr.getManagerValidationOn(), fr.getManagerValidationOn()));

		// is subdiv linked to adveso ?
		boolean isLinkedToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(
			fr.getJobItem().getJob().getCon().getSub().getSubdivid(),
			fr.getTechnician().getSub().getComp().getCoid(), new Date());

		// send/make-available to Client ?
		boolean makeFRAvailable = BooleanUtils.isTrue(fr.getSendToClient()) && isLinkedToAdveso;

		// if the outcome is preselected and we have to send it to the
		// client and also we're linked to Adveso
		// automatically register the activity 'available to external
		// management system'
		if ((fr.getRequireDeliveryToClient() == null || !fr.getRequireDeliveryToClient()) && makeFRAvailable) {
			HookInterceptor_MakeFRAvailableForExternalManegementSystem.Input input = new HookInterceptor_MakeFRAvailableForExternalManegementSystem.Input(
					fr, fr.getManagerValidationBy(), fr.getManagerValidationOn(), 0, validationRemark);
			hook_MakeFRAvailableForExternalSysteme.recordAction(input);
		}

		fr.setClientApproval(clientApprovalOldValue);

		if (fr.getClientResponseNeeded() && fr.getClientApproval() != null) {

			org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_SendFailureReportToClient.Input input = new org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_SendFailureReportToClient.Input(
					fr, fr.getManagerValidationOn(), 0, fr.getManagerValidationBy());
			hookInterceptor_SendFailureReportToClient.recordAction(input);

			fr.setOutcomePreselected(true);

			Input clientResponseInput = new Input(fr, fr.getManagerValidationOn(), 0, fr.getManagerValidationBy());
			hookInterceptor_FailureReportClientResponse.recordAction(clientResponseInput);
		}

		if (fr.getFinalOutcome() != null) {
			org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_SelectFailureReportFinalOutcome.Input input = new org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_SelectFailureReportFinalOutcome.Input(
					fr.getJobItem(), null, fr.getManagerValidationOn(), 0, fr.getManagerValidationBy());
			hookInterceptor_SelectFailureReportFinalOutcome.recordAction(input);
		}

		if (fr.getJobItem().getJob().getType().equals(JobType.SITE)
				&& (FailureReportFinalOutcome.INTERNAL_REPAIR.equals(fr.getFinalOutcome())
						|| FailureReportFinalOutcome.THIRD_PARTY_CALIBRATION_WITH_JUDGMENT.equals(fr.getFinalOutcome())
						|| FailureReportFinalOutcome.THIRD_PARTY_CALIBRATION_WITHOUT_JUDGMENT
								.equals(fr.getFinalOutcome())
						|| FailureReportFinalOutcome.THIRD_PARTY_REPAIR.equals(fr.getFinalOutcome()))) {
			ItemStatus onHoldStatus = itemStateService
					.findItemStatusByName(ON_HOLD_FOR_ONSITE_REPAIR_AND_TP_FR_OUTCOMES);
			org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_PlaceOnHold.Input input = new org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_PlaceOnHold.Input(
					fr.getJobItem(), onHoldStatus, null);
			hookInterceptor_PlaceOnHold.recordAction(input);
		}

	}

	/**
	 * @deprecated Use merge in the right way0
	 */
	public void updateFaultReport(FaultReport faultReport) {
		this.faultReportDao.update(faultReport);
	}

	@Override
	public List<FaultReport> getFaultReportListByJobitemId(Integer jobItemId) {
		return faultReportDao.getFaultReportListByJobitemId(jobItemId);
	}

	@Override
	public FaultReport getLastFailureReportByJobitemId(Integer jobitemId, Boolean valid) {
		return faultReportDao.getLastFailureReportByJobitemId(jobitemId, valid);
	}

	@Override
	public List<FailureReportDTO> getAllFailureReportsByJobitemId(Integer jobitemId) {
		return faultReportDao.getAllFailureReportsByJobitemId(jobitemId);
	}

	@Override
	public void automaticFaultReportGeneration(FaultReport fr, boolean withClientReponse) {
		// FR Document Generation
		String jobNumber = fr.getJobItem().getJob().getJobno();

		String directory = this.compDirService
			.getDirectory(
				this.scService.findComponent(
					org.trescal.cwms.core.system.entity.systemcomponent.Component.FAILURE_REPORT),
				jobNumber)
			.getAbsolutePath();

		FailureReportFileNamingService fnService = new FailureReportFileNamingService(fr, directory, withClientReponse);

		Locale locale;
		if (fr.getClientApprovalBy() != null)
			locale = fr.getClientApprovalBy().getLocale();
		else if (fr.getManagerValidationBy() != null)
			locale = fr.getManagerValidationBy().getLocale();
		else
			locale = new Locale("en", "GB");

		documentService.createBirtDocument(fr.getFaultRepId(), BaseDocumentType.FAILURE_REPORT_2014, locale,
			org.trescal.cwms.core.system.entity.systemcomponent.Component.FAILURE_REPORT, fnService);

	}

	@Override
	public void setFailureReportClientResponseFromAdveso(FaultReport fr,
			AdvesoFailureReportClientResponseInputDTO inputDTO) {
		fr.setClientApproval(inputDTO.getClientApproval());
		fr.setClientAlias(inputDTO.getClientApprovalBy());
		fr.setClientApprovalOn(inputDTO.getClientApprovalOn());
		fr.setClientComments(inputDTO.getClientComments());
		fr.setClientOutcome(inputDTO.getClientOutcome());

		this.updateFaultReport(fr);
		hookInterceptor_FailureReportClientResponse
				.recordAction(new Input(fr, inputDTO.getClientApprovalOn(), inputDTO.getTimeSpent(), null));
	}

	@Override
	public FaultReport getFirstFailureReportByJobitemIdForDelivery(Integer jobitemId) {
		return this.faultReportDao.getFirstFailureReportByJobitemIdForDelivery(jobitemId);
	}

	@Override
	public void deliverInformationalFR(JobItem ji, JobItemActivity a) {
		if (jobItemService.stateHasGroupOfKeyName(a.getActivity(), StateGroup.FR_REQUIREDELIVERY)) {
			FaultReport fr = this.getFirstFailureReportByJobitemIdForDelivery(ji.getJobItemId());
			fr.setRequireDeliveryToClient(false);
			faultReportDao.merge(fr);
			this.interceptor_updateFaultReport.recordAction(new HookInterceptor_UpdateFaultReport.Input(fr));
			Locale messageLocale = ji.getJob().getOrganisation().getComp().getDocumentLanguage();
			if (StringUtils.isNotBlank(a.getRemark()))
				a.setRemark(
					a.getRemark()
						.concat("<br/>"
							+ this.messageSource.getMessage("jifaultreport.faultreport", null,
							"Failure Report : ", messageLocale)
							+ " : " + fr.getFaultReportNumber()));
			else
				a.setRemark(this.messageSource.getMessage("jifaultreport.faultreport", null, "Failure Report : ",
					messageLocale) + " : " + fr.getFaultReportNumber());
		}
	}

	@Override
	public void setInformationalFRToBeSent(FaultReport fr) {
		fr.setRequireDeliveryToClient(true);
		this.faultReportDao.merge(fr);
	}

}