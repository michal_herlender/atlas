package org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.JobItemTransit;

@Repository("JobItemTransitDao")
public class JobItemTransitDaoImpl extends BaseDaoImpl<JobItemTransit, Integer> implements JobItemTransitDao {
	
	@Override
	protected Class<JobItemTransit> getEntity() {
		return JobItemTransit.class;
	}

}