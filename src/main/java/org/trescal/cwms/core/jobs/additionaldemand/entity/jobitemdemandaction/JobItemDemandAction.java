package org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemandaction;

import java.time.Duration;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemand;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemandStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

/**
 * 2020-07-07 : Via group discussion today we determined that this entity could be commented out   
 * as it was not created, but made to anticipate a future demand of recording time against AdditionalDemand
 * operations.  If needed in the future it could be reactivated and the SchemaUpdateTool run to generate SQL DDL scripts.
 */
//@Entity
//@Table(name = "jobitemdemandaction")
public class JobItemDemandAction {

	private Integer id;
	private JobItem jobItem;
	private AdditionalDemand demand;
	private AdditionalDemandStatus previousStatus;
	private AdditionalDemandStatus newStatus;
	private LocalDateTime recordTime;
	private Duration timeSpent;
	private Contact contact;
	private String comment;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "jobItemId", nullable = false, foreignKey = @ForeignKey(name = "FK_jobitemdemandaction_jobitem"))
	public JobItem getJobItem() {
		return jobItem;
	}

	public void setJobItem(JobItem jobItem) {
		this.jobItem = jobItem;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "demand")
	public AdditionalDemand getDemand() {
		return demand;
	}

	public void setDemand(AdditionalDemand demand) {
		this.demand = demand;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "previousStatus")
	public AdditionalDemandStatus getPreviousStatus() {
		return previousStatus;
	}

	public void setPreviousStatus(AdditionalDemandStatus previousStatus) {
		this.previousStatus = previousStatus;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "newStatus")
	public AdditionalDemandStatus getNewStatus() {
		return newStatus;
	}

	public void setNewStatus(AdditionalDemandStatus newStatus) {
		this.newStatus = newStatus;
	}

	@DateTimeFormat(iso = ISO.DATE_TIME)
	@Column(name = "recordTime")
	public LocalDateTime getRecordTime() {
		return recordTime;
	}

	public void setRecordTime(LocalDateTime recordTime) {
		this.recordTime = recordTime;
	}

	@NotNull
	@Column(name = "timeSpent", nullable = false)
	public Duration getTimeSpent() {
		return timeSpent;
	}

	public void setTimeSpent(Duration timeSpent) {
		this.timeSpent = timeSpent;
	}

	@ManyToOne
	@JoinColumn(name = "personId", foreignKey = @ForeignKey(name = "FK_jobitemdemandaction_contact"))
	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	@Column(name = "comment")
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}