package org.trescal.cwms.core.jobs.job.entity.jobinstructionlink;

import java.util.List;
import java.util.TreeMap;

import org.trescal.cwms.core.system.entity.instruction.Instruction;

public class RelatedInstructionSet
{
	private TreeMap<String, List<Instruction>> instructionMap;
	private int size;

	public RelatedInstructionSet()
	{}

	/**
	 * @return the instructionMap
	 */
	public TreeMap<String, List<Instruction>> getInstructionMap()
	{
		return this.instructionMap;
	}

	/**
	 * @return the size
	 */
	public int getSize()
	{
		return this.size;
	}

	/**
	 * @param instructionMap the instructionMap to set
	 */
	public void setInstructionMap(TreeMap<String, List<Instruction>> instructionMap)
	{
		this.instructionMap = instructionMap;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(int size)
	{
		this.size = size;
	}

}