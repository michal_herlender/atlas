package org.trescal.cwms.core.jobs.job.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courier.db.CourierService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.documents.excel.utils.ExcelFileReaderUtil;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db.ExchangeFormatFileService;
import org.trescal.cwms.core.exchangeformat.utils.ExchangeFormatGeneralValidator;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.entity.po.db.POService;
import org.trescal.cwms.core.jobs.job.form.AddJobForm;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.db.CallOffItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.db.CollectedInstrumentService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db.JobExpenseItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.logistics.dto.AsnDTO;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.dao.AsnService;
import org.trescal.cwms.core.logistics.form.PrebookingJobForm;
import org.trescal.cwms.core.logistics.form.validator.AddJobFormValidator;
import org.trescal.cwms.core.logistics.utils.RecordTypeEnum;
import org.trescal.cwms.core.logistics.utils.SourceAPIEnum;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateTimeEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.basestatus.db.BaseStatusService;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap.SimpleEntry;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDateTime;

@Controller
@IntranetController
@SessionAttributes(value = {Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_SUBDIV,
	Constants.SESSION_ATTRIBUTE_USERNAME})
public class AddJobController {
	private Integer scannedBarcode;

	private static final String JFS_VIEW = "jobfinalsynthesis.htm";

	@Value("#{props['cwms.config.address.use_locations']}")
	private boolean useLocations;
	@Autowired
	private AddressService addrServ;
	@Autowired
	private BaseStatusService statusServ;
	@Autowired
	private BPOService bpoServ;
	@Autowired
	private CollectedInstrumentService ciServ;
	@Autowired
	private CallOffItemService coiServ;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private ContactService contServ;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private JobExpenseItemService expenseItemService;
	@Autowired
	private JobService jobServ;
	@Autowired
	private LocationService locServ;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private POService poServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SupportedCurrencyService currencyServ;
	@Autowired
	private TransportOptionService transportOptionService;
	@Autowired
	private UserService userService;
	// START
	@Autowired
	private ServletContext servletContext;
	@Autowired
	private AsnService asnService;
	@Autowired
	private CourierService courierService;
	@Autowired
	private AddJobFormValidator validator;
	@Autowired
	private ExcelFileReaderUtil excelFileReaderUtil;
	@Autowired
	private ExchangeFormatService efService;
	@Autowired
	private ExchangeFormatFileService efFileService;
	@Autowired
	private ExchangeFormatGeneralValidator efValidator;

	@InitBinder("ajf")
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		binder.setValidator(validator);
		val dateTimeEditor = new CustomDateEditor(DateTools.ldtf_ISO8601, true);
		binder.registerCustomEditor(Date.class, dateTimeEditor);
		binder.registerCustomEditor(LocalDateTime.class, new CustomLocalDateTimeEditor(DateTimeFormatter.ISO_DATE_TIME));
	}

	@ModelAttribute("ajf")
	protected AddJobForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@RequestParam(value = "basedonjobid", required = false, defaultValue = "0") Integer basedOnId,
			@RequestParam(value = "calloffcoid", required = false, defaultValue = "0") Integer callOffItemComp,
			@RequestParam(value = "collectedcoid", required = false, defaultValue = "0") Integer collectedComp,
			@RequestParam(value = "plantid", required = false, defaultValue = "0") Integer barcode,
			@RequestParam(value = "items", required = false) String itemStr,
			@RequestParam(value = "insts", required = false) String instStr, Model model) {
		AddJobForm ajf = new AddJobForm();
		Contact currentCon = this.userService.get(username).getCon();
		Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
		// If current contact's subdiv is same, can use his/her preferences if
		// available
		if (currentCon.getSub().getId().intValue() == allocatedSubdiv.getId().intValue()) {
			if (currentCon.getDefAddress() != null) {
				ajf.setBookedInAddrId(currentCon.getDefAddress().getAddrid());
			}
			if (currentCon.getDefLocation() != null)
				ajf.setBookedInLocId(currentCon.getDefLocation().getLocationid());
		}
		// Otherwise default booking in address should come from subdiv
		if ((ajf.getBookedInAddrId() == null) && (allocatedSubdiv.getDefaultAddress() != null)) {
			ajf.setBookedInAddrId(allocatedSubdiv.getDefaultAddress().getAddrid());
		}

		ajf.setEstCarriageOut(new BigDecimal("0.00"));
		if (basedOnId != 0) {
			Job basedOnJob = this.jobServ.get(basedOnId);
			ajf.setCoid(basedOnJob.getCon().getSub().getComp().getCoid());
			ajf.setSubdivid(basedOnJob.getCon().getSub().getSubdivid());
			ajf.setAddrid(basedOnJob.getReturnTo() != null ? basedOnJob.getReturnTo().getAddrid() : null);
			ajf.setPersonid(basedOnJob.getCon().getPersonid());
			ajf.setLocid(basedOnJob.getReturnToLoc() != null ? basedOnJob.getReturnToLoc().getLocationid() : null);
			ajf.setBasedOnJob(basedOnJob);
			ajf.setCreateJobFrom("job");
		} else if (callOffItemComp != 0) {
			if (itemStr != null && !itemStr.isEmpty()) {
				String[] itemIdStrs = itemStr.split(",");
				List<Integer> ids = new ArrayList<>();
				int i = 0;
				while (i < itemIdStrs.length) {
					ids.add(Integer.parseInt(itemIdStrs[i]));
					i++;
				}
				ajf.setCallOffItems(this.coiServ.getItemsFromIds(ids));
			}
			ajf.setCoid(this.compServ.get(callOffItemComp).getCoid());
			ajf.setCreateJobFrom("calloffitems");
		} else if (collectedComp != 0) {
			ajf.setCoid(this.compServ.get(collectedComp).getCoid());
			ajf.setCreateJobFrom("collectedinsts");
		} else if (barcode != 0) {
			Instrument inst = this.instServ.get(barcode);
			if (inst == null)
				throw new RuntimeException("Instrument with barcode " + barcode + " could not be found");
			this.scannedBarcode = barcode;
			model.addAttribute("scannedBarcode", barcode);
			ajf.setCoid(inst.getCon().getSub().getComp().getCoid());
			ajf.setSubdivid(inst.getCon().getSub().getSubdivid());
			ajf.setPersonid(inst.getCon().getPersonid());
			ajf.setAddrid((inst.getCon().getDefAddress() != null) ? inst.getCon().getDefAddress().getAddrid()
					: inst.getAdd().getAddrid());
			ajf.setCreateJobFrom("barcode");
		} else {
			ajf.setCreateJobFrom("scratch");
		}
		// set default currency from business company
		SupportedCurrency cur = allocatedSubdiv.getComp().getCurrency();
		ajf.setCurrencyCode(cur.getCurrencyCode());
		ajf.setJob(new Job());
		if (currentCon.getUserPreferences() != null) {
			ajf.setJobType(currentCon.getUserPreferences().getDefaultJobType());
		} else {
			ajf.setJobType(JobType.STANDARD);
		}
		ajf.setOverrideDateInOnJobItems(true);
		return ajf;
	}

	@ModelAttribute("transportOptions")
	protected List<TransportOption> setTransportInOptionForBusinessSubdiv(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Model model) {
		return transportOptionService.getTransportOptionsFromSubdiv(subdivDto.getKey());
	}

	@ModelAttribute("couriers")
	protected List<Courier> setCouriers(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		List<Courier> list = new ArrayList<>();
		list.add(new Courier());
		list.addAll(courierService.getCouriersByBusinessCompany(companyDto.getKey()));
		return list;
	}

	protected void onBind(AddJobForm form) throws Exception {
		if (form.getSelectedCallOffItems() == null) {
			form.setSelectedCallOffItems(new ArrayList<>());
		}
		if (form.getSelectedCollectedInsts() == null) {
			form.setSelectedCollectedInsts(new ArrayList<>());
		}
	}

	@ModelAttribute("listasn")
	public List<AsnDTO> getAsn(@RequestParam(name = "jobid", required = false) Integer jobid,
			@RequestParam(name = "plant_id", required = false) Integer plantid,
			@RequestParam(name = "plant_no", required = false) String plantno) throws Exception {

		if (jobid != null) {
			List<AsnDTO> listAsn = this.asnService.findAsn(jobServ.get(jobid).getCon().getSub().getSubdivid(), plantid,
					plantno);

			// set the size of asnItems
			for (AsnDTO asndto : listAsn) {
				asndto.setItemsSize(this.asnService.asnItemsSize(asndto.getId()));
			}

			// return the list of available prebookings
			return listAsn;
		}

		return null;
	}

	protected void onBindAndValidate(HttpServletRequest request, AddJobForm ajf) throws Exception {
		List<String> poNumberList = new ArrayList<>();
		List<String> poCommentList = new ArrayList<>();
		List<Integer> poIdList = new ArrayList<>();
		// POs data
		int i = 1;
		while (ServletRequestUtils.getStringParameter(request, "polist[" + i + "]") != null) {
			poNumberList.add(ServletRequestUtils.getStringParameter(request, "polist[" + i + "]"));
			poCommentList.add(ServletRequestUtils.getStringParameter(request, "pocomlist[" + i + "]"));
			poIdList.add(ServletRequestUtils.getIntParameter(request, "poidlist[" + i + "]"));
			i++;
		}
		ajf.setPoNumberList(poNumberList);
		ajf.setPoCommentList(poCommentList);
		ajf.setPoIdList(poIdList);
	}

	@RequestMapping(value = "/additemsfromprebooking.htm", method = RequestMethod.GET)
	public ModelAndView additemsfp(Model model, @ModelAttribute("jobid") String jobid) {
		model.addAttribute("job", jobServ.get(Integer.parseInt(jobid)));
		return new ModelAndView("trescal/core/jobs/jobitem/additemsfromprebooking");
	}

	@RequestMapping(value = "/addjob.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, @ModelAttribute("ajf") AddJobForm form)
			throws Exception {

		HashMap<String, Object> refData = new HashMap<>();
		List<Address> addresses = new ArrayList<>();
		List<Location> locations = new ArrayList<>();
		User currentUser = userService.get(username);
		Set<Subdiv> userSubdivs = subdivService.getBusinessCompanySubdivsForUser(currentUser);
		for (Subdiv subdiv : userSubdivs) {
			List<Address> subdivAddresses = subdiv.getAddresses().stream().filter(Address::getActive)
					.collect(Collectors.toList());
			addresses.addAll(subdivAddresses);
			for (Address address : subdivAddresses)
				locations.addAll(address.getLocations());
		}
		refData.put("businessAddresses", addresses);
		refData.put("businessLocations", locations);
		refData.put("useLocations", this.useLocations);
		refData.put("currencyList", this.currencyServ.getAllSupportedCurrenciesWithExRates());
		refData.put("jobTypes", JobType.getActiveJobTypes());
		refData.put("currentDate", LocalDateTime.now(LocaleContextHolder.getTimeZone().toZoneId()));
		val prefills = collectPrefillsIds(form);
		refData.put("prefills", prefills);
		return new ModelAndView("trescal/core/jobs/job/addjob", refData);
	}

	private Map<String, Integer> collectPrefillsIds(AddJobForm form) {
		return Stream.of(
				checkAndCrateEntry("coid", form.getCoid()),
				checkAndCrateEntry("subdivId", form.getSubdivid()),
				checkAndCrateEntry("personId", form.getPersonid()),
				checkAndCrateEntry("addressId", form.getAddrid()),
				checkAndCrateEntry("locationId", form.getLocid())
		).filter(Optional::isPresent).map(Optional::get).collect(Collectors.toMap(Entry::getKey, Entry::getValue));
	}

	private Optional<SimpleEntry<String, Integer>> checkAndCrateEntry(String key, Integer value) {
		return value != null ? Optional.of(new SimpleEntry<>(key, value)) : Optional.empty();
	}

	@RequestMapping(value = "/addjob.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest request,
									@RequestParam(name = "defaultpo", required = false, defaultValue = "0") Integer defaultPO,
									@RequestParam(name = "defaultbpo", required = false, defaultValue = "0") Integer defaultBPO,
									@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
									@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
									@Valid @ModelAttribute("ajf") AddJobForm ajf, BindingResult bindingResult,
									final RedirectAttributes redirectAttributes) throws Exception {
		if (bindingResult.hasErrors())
			return referenceData(username, ajf);
		else {
			onBind(ajf);
			onBindAndValidate(request, ajf);

			String createJobFrom = ajf.getCreateJobFrom();
			Contact contact = this.userService.get(username).getCon();
			Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());

			Job job = ajf.getJob();
			job.setType(ajf.getJobType());
			// set contact and address
			if (!createJobFrom.equals("job")) {
				job.setReturnTo(this.addrServ.get(ajf.getAddrid()));
				if (ajf.getLocid() != null) {
					Location loc = this.locServ.get(ajf.getLocid());
					if (loc != null)
						job.setReturnToLoc(loc);
				}
				job.setCon(this.contServ.get(ajf.getPersonid()));
			} else {
				// reload these so as not to throw session error
				job.setReturnTo(this.addrServ.get(ajf.getBasedOnJob().getReturnTo().getAddrid()));
				if (ajf.getBasedOnJob().getReturnToLoc() != null) {
					Location loc = this.locServ.get(ajf.getBasedOnJob().getReturnToLoc().getLocationid());
					if (loc != null)
						job.setReturnToLoc(loc);
				}
				job.setCon(this.contServ.get(ajf.getBasedOnJob().getCon().getPersonid()));
			}
			// set basic job info
			job.setClientRef(ajf.getClientref());
			job.setEstCarriageOut(new BigDecimal("0.00"));
			job.setRegDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			job.setCreatedBy(contact);
			job.setOrganisation(this.subdivService.get(subdivDto.getKey()));
			job.setJs( this.statusServ.findDefaultStatus(JobStatus.class));
			if (job.getType().equals(JobType.SITE)) {
				job.setBookedInAddr(job.getReturnTo());
				job.setBookedInLoc(job.getReturnToLoc());
			} else {
				job.setBookedInAddr(
					(ajf.getBookedInAddrId() == null) ? null : this.addrServ.get(ajf.getBookedInAddrId()));
				job.setBookedInLoc((ajf.getBookedInLocId() == null) ? null : this.locServ.get(ajf.getBookedInLocId()));
			}
			job.setReturnOption(this.transportOptionService.getTransportOptionOut(job.getReturnTo(), allocatedSubdiv));
			job.setInOption(this.transportOptionService.getTransportOptionIn(job.getReturnTo(), allocatedSubdiv));
			// set ReceiptDate & getPickupDate
			job.setOverrideDateInOnJobItems(ajf.getOverrideDateInOnJobItems());
			if (ajf.getReceiptDate() != null)
				job.setReceiptDate(DateTools.localDateTimeToZonedDateTime(ajf.getReceiptDate()));
			if (ajf.getPickupDate() != null)
				job.setPickupDate(DateTools.localDateTimeToZonedDateTime(ajf.getPickupDate()));
			// add BPO link
			int bpoid = ServletRequestUtils.getIntParameter(request, "bpo", 0);
			if (bpoid != 0) {
				BPO bpo = this.bpoServ.get(bpoid);
				JobBPOLink bpoLink = new JobBPOLink();
				bpoLink.setBpo(bpo);
				bpoLink.setJob(job);
				bpoLink.setDefaultForJob(true);
				bpoLink.setCreatedOn(new Date());
				job.setBpoLinks(new HashSet<>());
				job.getBpoLinks().add(bpoLink);
			}
			// either set the currency and rate from the company default or
			// from
			// the
			// other selected currency
			if (ajf.getCurrencyCode().equals(Constants.COMPANY_DEFAULT_CURRENCY)) {
				job.setCurrency(job.getCon().getSub().getComp().getCurrency());
				job.setRate(job.getCon().getSub().getComp().getRate());
			} else {
				job.setCurrency(this.currencyServ.findCurrencyByCode(ajf.getCurrencyCode()));
				job.setRate(job.getCurrency().getDefaultRate());
			}
			String jobno = this.numerationService.generateNumber(NumerationType.JOB, job.getOrganisation().getComp(),
					job.getOrganisation());
			job.setJobno(jobno);
			// delivery details
			prepareDeliveryDetails(job, ajf);
			// persist job
			Integer jobId = this.jobServ.insertJob(job, subdivDto.getKey());
			// load job again, because it will be updated on adding POs
			// (default
			// PO) -> optimistic locking
			job = jobServ.get(jobId);
			// add POs
			this.poServ.addPOsToJob(ajf.getPoIdList(), ajf.getPoNumberList(), ajf.getPoCommentList(), job, defaultPO);
			// add job items for any collected insts or call off items
			// selected
			int nextItemNo = 1;
			nextItemNo = this.ciServ.addCollectedInstrumentsToJob(ajf.getSelectedCollectedInsts(), job, contact,
					nextItemNo);
			nextItemNo = this.coiServ.addCallOffItemsToNewJob(ajf.getSelectedCallOffItems(), job, contact, nextItemNo);
			this.jiServ.copyItemsToNewJob(ajf.getOldJobItemIds(), job, contact, nextItemNo);
			this.expenseItemService.copyItemsToNewJob(ajf.getOldJobServiceIds(), job);
			// if adding a job from a barcode, add parameters to tell
			// viewjob
			// page
			String addParams = (createJobFrom.equals("barcode"))
					? "&loadtab=quickitems-tab&quickplantid=" + this.scannedBarcode
					: "";
			return new ModelAndView(new RedirectView("viewjob.htm?jobid=" + job.getJobid() + addParams));
		}
	}

	private void prepareDeliveryDetails(Job j, AddJobForm ajf) {
		j.setPackageType(ajf.getPackageType());
		j.setNumberOfPackages(ajf.getNumberOfPackages());
		j.setStorageArea(ajf.getStorageArea());
	}

	@RequestMapping(value = "/addjobfp.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmitJobFP(HttpServletRequest request,
			@RequestParam(name = "defaultpo", required = false, defaultValue = "0") Integer defaultPO,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@RequestParam(name = "jobid", required = false) Integer jobid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@Valid @ModelAttribute("ajf") AddJobForm ajf, BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) throws Exception {

		boolean addItemsToJobAlreadyCreated = jobid != null;
		if (bindingResult.hasErrors())
			return referenceData(username, ajf);
		else {
			onBind(ajf);
			onBindAndValidate(request, ajf);
			// if Record_Type is 'PREBOOKING' prepare data for
			// JobFinalSynthesis
			// get the temporary Directory
			Asn asn = asnService.get(ajf.getAsnId());

			List<LinkedCaseInsensitiveMap<String>> fileContent;
			// PrebookingJobForm initialization from AddJobForm
			PrebookingJobForm pbjf = prebookingJobFormInit(ajf);
			// get File Content
			if (asn.getSourceApi().equals(SourceAPIEnum.MOBILE_APP)) {
				fileContent = new ObjectMapper().readValue(asn.getItemsFromJson(),
						new TypeReference<List<LinkedCaseInsensitiveMap<String>>>() {
						});
			} else {
				File tempDir = (File) servletContext.getAttribute(ServletContext.TEMPDIR);
				File file = efFileService.getFile(asn.getExchangeFormatFile(), tempDir);
				fileContent = excelFileReaderUtil.readExcelFile(new FileInputStream(file),
						asn.getExchangeFormat().getSheetName(), asn.getExchangeFormat().getLinesToSkip());
			}

			// validate FileContent with ExchangeFormat
			if (efValidator.validate(asn.getExchangeFormat(), fileContent, bindingResult).hasErrors()) {
				return referenceData(username, ajf);
			}

			// set defaultPO
			pbjf.setDefaultPO(defaultPO);
			pbjf.setAsnId(ajf.getAsnId());

			// add BPO
			int bpoid = ServletRequestUtils.getIntParameter(request, "bpo", 0);
			if (bpoid != 0)
				pbjf.setBpo(bpoid);
			if (addItemsToJobAlreadyCreated) {
				pbjf.setRecordType(RecordTypeEnum.PREBOOKING);
				pbjf.setCoid(ajf.getCoid());
				Job currentJob = this.jobServ.get(jobid);
				pbjf.setJobType(currentJob.getType());
			}

			redirectAttributes.addFlashAttribute("pbjf", pbjf);
			redirectAttributes.addFlashAttribute("fileContent", fileContent);
			redirectAttributes.addFlashAttribute("jobId", jobid);
			redirectAttributes.addFlashAttribute("addItemsToJobAlreadyCreated", addItemsToJobAlreadyCreated);

			return new ModelAndView(new RedirectView(JFS_VIEW));
		}
	}

	@RequestMapping(value = "/addjobff.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmitJobFF(HttpServletRequest request,
			@RequestParam(name = "defaultpo", required = false, defaultValue = "0") Integer defaultPO,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@Valid @ModelAttribute("ajf") AddJobForm ajf, BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) throws Exception {

		if (bindingResult.hasErrors())
			return referenceData(username, ajf);
		else {

			ExchangeFormat ef = efService.get(ajf.getExchangeFormat());

			// get File Content
			List<LinkedCaseInsensitiveMap<String>> fileContent = excelFileReaderUtil
					.readExcelFile(ajf.getUploadFile().getInputStream(), ef.getSheetName(), ef.getLinesToSkip());

			// Validate the FileContent with Exchange format
			if (efValidator.validate(ef, fileContent, bindingResult).hasErrors()) {
				return referenceData(username, ajf);
			}

			onBind(ajf);
			onBindAndValidate(request, ajf);

			// PrebookingJobForm initialization
			PrebookingJobForm pbjf = prebookingJobFormInit(ajf);
			pbjf.setDefaultPO(defaultPO);
			pbjf.setAsnId(0);

			// add BPO
			int bpoid = ServletRequestUtils.getIntParameter(request, "bpo", 0);
			if (bpoid != 0)
				pbjf.setBpo(bpoid);

			redirectAttributes.addFlashAttribute("pbjf", pbjf);
			redirectAttributes.addFlashAttribute("fileContent", fileContent);
			redirectAttributes.addFlashAttribute("addItemsToJobAlreadyCreated", false);

			return new ModelAndView(new RedirectView(JFS_VIEW));
		}
	}

	private PrebookingJobForm prebookingJobFormInit(AddJobForm ajf) {
		PrebookingJobForm pbjf = new PrebookingJobForm();

		pbjf.setExchangeFormat(ajf.getExchangeFormat());
		pbjf.setRecordType(ajf.getRecordType());
		pbjf.setCoid(ajf.getCoid());
		pbjf.setAddrid(ajf.getAddrid());
		pbjf.setLocid(ajf.getLocid());
		pbjf.setPersonid(ajf.getPersonid());
		pbjf.setJobType(ajf.getJobType());
		pbjf.setClientref(ajf.getClientref());
		pbjf.setBookedInAddrId(ajf.getBookedInAddrId());
		pbjf.setBookedInLocId(ajf.getBookedInLocId());
		pbjf.setCurrencyCode(ajf.getCurrencyCode());
		pbjf.setEstCarriageOut(ajf.getEstCarriageOut());
		pbjf.setPonumber(ajf.getPonumber());
		pbjf.setPoCommentList(ajf.getPoCommentList());
		pbjf.setPoNumberList(ajf.getPoNumberList());
		pbjf.setPoIdList(ajf.getPoIdList());
		if (ajf.getReceiptDate() != null)
			pbjf.setReceiptDate(dateFromLocalDateTime(ajf.getReceiptDate()));
		if (ajf.getPickupDate() != null)
			pbjf.setPickupDate(dateFromLocalDateTime(ajf.getPickupDate()));
		pbjf.setOverrideDateInOnJobItems(ajf.getOverrideDateInOnJobItems());
		pbjf.setTransportIn(ajf.getTransportIn());
		pbjf.setCarrier(ajf.getCarrier());
		pbjf.setTrackingNumber(ajf.getTrackingNumber());
		pbjf.setTransportOut(ajf.getTransportOut());
		pbjf.setNumberOfPackages(ajf.getNumberOfPackages());
		pbjf.setPackageType(ajf.getPackageType());
		pbjf.setStorageArea(ajf.getStorageArea());

		return pbjf;
	}

	public File convert(MultipartFile file) {
		File convFile = new File(file.getOriginalFilename());
		try {
			convFile.createNewFile();
			FileOutputStream fos = new FileOutputStream(convFile);
			fos.write(file.getBytes());
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return convFile;
	}
}