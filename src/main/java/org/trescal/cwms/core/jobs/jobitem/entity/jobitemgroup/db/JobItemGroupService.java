package org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.db;

import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;

public interface JobItemGroupService
{
	ResultWrapper ajaxGetGroupWithItems(int groupId);

	ResultWrapper ajaxUpdateGroupFields(int groupId, Boolean withCase, Boolean calibration, Boolean delivery);

	ResultWrapper deleteGroupFromJob(int groupId);

	void deleteJobItemGroup(JobItemGroup jobitemgroup);

	JobItemGroup findJobItemGroup(int id);

	List<JobItemGroup> getAllGroupsForJob(int jobId);

	List<JobItemGroup> getAllJobItemGroups();

	JobItemGroup getGroupWithItems(int groupId);

	void insertJobItemGroup(JobItemGroup jobitemgroup);

	ResultWrapper newGroupForJob(int jobId);

	void saveOrUpdateJobItemGroup(JobItemGroup jobitemgroup);

	void updateJobItemGroup(JobItemGroup jobitemgroup);
	
	List<Integer> getJobitemsByGroupId(int groupId);
	
	List<Integer> getGroupIdsByJobitemIds(List<Integer> jobitemIds);
}