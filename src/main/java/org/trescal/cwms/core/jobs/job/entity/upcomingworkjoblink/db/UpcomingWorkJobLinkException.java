package org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.db;

public class UpcomingWorkJobLinkException extends Exception {
    public UpcomingWorkJobLinkException(String message){
        super(message);
    }
}
