package org.trescal.cwms.core.jobs.calibration.entity.pointsettemplate.db;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.calibration.entity.pointsettemplate.PointSetTemplate;

@Repository("PointSetTemplateDao")
public class PointSetTemplateDaoImpl extends BaseDaoImpl<PointSetTemplate, Integer> implements PointSetTemplateDao {
	
	@Override
	protected Class<PointSetTemplate> getEntity() {
		return PointSetTemplate.class;
	}
	
	@Override
	public PointSetTemplate findPointSetTemplateWithPoints(int id) {
		Criteria criteria = getSession().createCriteria(PointSetTemplate.class);
		criteria.add(Restrictions.idEq(id));
		criteria.setFetchMode("points", org.hibernate.FetchMode.JOIN);
		criteria.setFetchMode("points.uom", org.hibernate.FetchMode.JOIN);
		return (PointSetTemplate) criteria.uniqueResult();
	}
}