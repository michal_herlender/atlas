package org.trescal.cwms.core.jobs.jobitem.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class CustomForm {
	private Integer coid;
	private List<CustomItem> customItems;

	public List<CustomItem> getCustomItems(){
		log.info("custom items getter");
		return this.customItems;};
}
