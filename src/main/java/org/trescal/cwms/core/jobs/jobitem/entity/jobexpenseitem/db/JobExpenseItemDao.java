package org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.jobs.jobitem.dto.ExpenseItemDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.ExpenseItemOnClientPurchaseOrderDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.InvoiceableItemsByCompanyDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.db.AbstractJobItemDao;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;

public interface JobExpenseItemDao extends AbstractJobItemDao<JobExpenseItem> {

	List<InvoiceableItemsByCompanyDTO> getInvoiceable(Integer allocatedCompanyId, Integer allocatedSubdivId);

	List<JobExpenseItem> getInvoiceable(Company clientCompany, Integer allocatedCompanyId, Integer allocatedSubdivId,
			String completeJob);

	List<ExpenseItemOnClientPurchaseOrderDTO> getExpenseItemsOnClientBPO(Integer poId, Integer jobId, Locale locale);

	List<ExpenseItemOnClientPurchaseOrderDTO> getExpenseItemsOnClientPO(Integer poId, Integer jobId, Locale locale);
	
	List<ExpenseItemDTO> getAvailableForInvoice(Integer invoiceId, Locale locale);
}