package org.trescal.cwms.core.jobs.jobitem.form.genericentityvalidator;

import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportDetail;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

public class FaultReportDetailValidator extends AbstractBeanValidator
{

	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(FaultReportDetail.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		FaultReportDetail frdetail = (FaultReportDetail) target;

		super.validate(frdetail, errors);

		// check fault report is not null
		if (frdetail.getFaultRep() == null)
		{
			errors.rejectValue("faultRep", null, "Could not find fault report");
		}

		// check contact is not null
		if (frdetail.getRecordedBy() == null)
		{
			errors.rejectValue("recordedBy", null, "Could not find the current contact");
		}

	}
}
