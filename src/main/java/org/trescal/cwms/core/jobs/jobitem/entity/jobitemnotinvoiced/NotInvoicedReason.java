package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum NotInvoicedReason {
    UNDEFINED("notinvoicedreason.undefined"),                    // 0
    CONTRACT("notinvoicedreason.contract"),                    // 1
    INTERNAL_JOB("notinvoicedreason.internaljob"),                // 2
    NO_WORK_PERFORMED("notinvoicedreason.noworkperformed"),        // 3
    WARRANTY("notinvoicedreason.warranty"),                        // 4
    OTHER("notinvoicedreason.other"),                            // 5
    INTER_SUBDIVISION("notinvoicedreason.intersubdivision"),    // 6
    QUALITY_REWORK("notinvoicedreason.qualityrework"),            // 7
    CALLED_OFF("notinvoicedreason.calledoff");                    // 8

    private final String messageCode;
    private MessageSource messageSource;

    NotInvoicedReason(String messageCode) {
        this.messageCode = messageCode;
    }

    @Component
    public static class NotInvoicedReasonMessageSourceInjector {
        @Autowired
        private ReloadableResourceBundleMessageSource messages;

        @PostConstruct
        public void postConstruct() {
            for (NotInvoicedReason reason : EnumSet.allOf(NotInvoicedReason.class)) {
                reason.setMessageSource(messages);
            }
        }
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public String getMessage() {
        Locale locale = LocaleContextHolder.getLocale();
        return this.messageSource.getMessage(messageCode, null, this.toString(), locale);
    }

    public String getName() {
        return name();
    }
}
