package org.trescal.cwms.core.jobs.calibration.entity.calreq;

import javax.persistence.*;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.system.Constants;

import lombok.Setter;

@Entity
@DiscriminatorValue("companymodel")
@Setter
public class CompanyModelCalReq extends CalReq {

	private Company comp;
	private InstrumentModel model;

	@Transient
	@Override
	public String getClassKey() {
		return Constants.CALREQ_MAP_COMPANYMODEL;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "compid")
	public Company getComp() {
		return this.comp;
	}

	@Override
	@Transient
	public int getGenericId() {
		return (this.model == null) ? 0 : this.model.getModelid();
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelid", referencedColumnName = "modelid")
	public InstrumentModel getModel() {
		return this.model;
	}

	/*
	 * GB 2016-02-06 : Previously, this would only return message key if model+comp
	 * were not null; changed to return in all cases (to help identify inconsistent
	 * data in system)
	 */
	@Transient
	@Override
	public String getSource() {
		return "calreq.companymodelcalreqsource";
	}
	
	@Transient
	@Override
	public String getSourceParameter() {
		if ((this.comp != null) && (this.model != null)) {
			return this.model.getDefinitiveModel() + ", " + this.comp.getConame();
		}
		return "";
	}

}