package org.trescal.cwms.core.jobs.jobitem.entity.jobitem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Immutable
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "activeclientjobitems")
public class ActiveClientJobItem {


    @Id
    @Column(name = "jobitemid")
    private Integer jobItemId;

    @Column(name = "duedate")
    private LocalDate dueDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "jobitemid")
    private JobItem jobItem;
}
