package org.trescal.cwms.core.jobs.contractreview.entity.contractreview;

import java.util.Comparator;

/**
 * Implementation of {@link Comparator} that sorts {@link ContractReview}
 * entities by date, descending.
 * 
 * @author richard
 */
public class ContractReviewComparator implements Comparator<ContractReview>
{
	@Override
	public int compare(ContractReview o1, ContractReview o2)
	{
		if (o2.getReviewDate().compareTo(o1.getReviewDate()) == 1)
		{
			return o2.getReviewDate().compareTo(o1.getReviewDate());
		}
		else if (o2.getId() != o1.getId())
		{
			return ((Integer) o2.getId()).compareTo(o1.getId());
		}
		else
		{
			return 1;
		}
	}
}
