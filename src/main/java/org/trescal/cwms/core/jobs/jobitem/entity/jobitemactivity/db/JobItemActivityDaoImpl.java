package org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity_;

@Repository("JobItemActivityDao")
public class JobItemActivityDaoImpl extends BaseDaoImpl<JobItemActivity, Integer> implements JobItemActivityDao {

	@Override
	protected Class<JobItemActivity> getEntity() {
		return JobItemActivity.class;
	}

	@Override
	public List<JobItemActivity> getAllIncompleteActivityForContact(Contact contact) {
		return getResultList(cb -> {
			CriteriaQuery<JobItemActivity> cq = cb.createQuery(JobItemActivity.class);
			Root<JobItemActivity> root = cq.from(JobItemActivity.class);
			Join<JobItemActivity, JobItem> jobItem = root.join(JobItemActivity_.jobItem);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isFalse(root.get(JobItemActivity_.complete)));
			clauses.getExpressions().add(cb.equal(root.get(JobItemActivity_.startedBy), contact));
			cq.where(clauses);
			cq.orderBy(cb.asc(job.get(Job_.jobid)), cb.asc(jobItem.get(JobItem_.itemNo)));
			return cq;
		});
	}

	@SuppressWarnings("unchecked")
	public List<JobItem> getJobItemsWithSameState(int jobId, int stateId, int jobItemId) {
		Criteria criteria = getSession().createCriteria(JobItem.class);
		Criteria jobCrit = criteria.createCriteria("job");
		jobCrit.add(Restrictions.idEq(jobId));
		Criteria stateCrit = criteria.createCriteria("state");
		stateCrit.add(Restrictions.idEq(stateId));
		criteria.add(Restrictions.ne("jobItemId", jobItemId));
		criteria.addOrder(Order.asc("itemNo"));
		criteria.setFetchMode("inst", FetchMode.JOIN);
		criteria.setFetchMode("inst.mfr", FetchMode.JOIN);
		criteria.setFetchMode("inst.mfr.name", FetchMode.JOIN);
		criteria.setFetchMode("inst.model", FetchMode.JOIN);
		criteria.setFetchMode("inst.model.mfr", FetchMode.JOIN);
		criteria.setFetchMode("inst.model.mfr.name", FetchMode.JOIN);
		criteria.setFetchMode("inst.model.model", FetchMode.JOIN);
		criteria.setFetchMode("inst.model.description", FetchMode.JOIN);
		criteria.setFetchMode("inst.model.description.description", FetchMode.JOIN);
		return criteria.list();
	}
}