package org.trescal.cwms.core.jobs.certificate.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class ViewCertificateValidator extends AbstractBeanValidator
{
	@Autowired
	private CertificateService certificateService; 
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return CertificateForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors)
	{
        CertificateForm form = (CertificateForm) obj;
        super.validate(form, errors);

        Certificate cert = this.certificateService.findCertificate(form.getCertId());

        errors.rejectValue("subdivid", "error.certificate.thirdpartysubdiv");

        // Make sure Third Party details are present if a TP Cert
        if (cert.getType().equals(CertificateType.CERT_THIRDPARTY)
                && ((form.getThirdCertNo() == null) || form.getThirdCertNo().trim().equals(""))) {
            errors.rejectValue("thirdCertNo", "error.certificate.thirdpartynumber");
        }
        if (cert.getType().equals(CertificateType.CERT_THIRDPARTY)
                && ((form.getSubdivid() == null) || (form.getSubdivid().intValue() == 0))) {
			errors.rejectValue("subdivid", "error.certificate.thirdpartysubdiv");
		}
	}
}