package org.trescal.cwms.core.jobs.certificate.form;

import java.util.Date;
import java.util.Map;

import org.trescal.cwms.core.jobs.certificate.dto.CustomCertWrapper;
import org.trescal.cwms.core.jobs.certificate.dto.CustomGroupCertWrapper;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CertManagementForm
{
	private Date calDate;
	private Map<Integer, CustomCertWrapper> customCerts;
	private Map<Integer, CustomGroupCertWrapper> customGroupCerts;
	private Integer issuerId;
	private Job job;

}