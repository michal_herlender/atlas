package org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem;

import lombok.Setter;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitempo.AbstractJobItemPO;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Setter
@MappedSuperclass
public abstract class AbstractJobItem<ItemPOType extends AbstractJobItemPO<?>> extends Versioned {

	private Job job;
	private ItemState state;
	private String clientRef;
	private Set<ItemPOType> itemPOs;

	@Transient
	public abstract JobItemType getJobItemType();

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "jobid")
    public Job getJob() {
		return this.job;
	}

    /**
     * @return the state
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "stateid")
    public ItemState getState() {
        return this.state;
    }

    @Size(min = 0, max = 30)
    @Column(name = "clientref", length = 30)
    public String getClientRef() {
        return clientRef;
    }

    @OneToMany(mappedBy = "item", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<ItemPOType> getItemPOs() {
        return this.itemPOs;
    }

    @Transient
	public abstract Set<InvoiceItem> getInvoiceItems();

	@Transient
	public Boolean getActive() {
		return this.state.getActive();
	}
}