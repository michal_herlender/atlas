package org.trescal.cwms.core.jobs.jobitem.form;

import java.util.Map;

import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemandStatus;

public class JIAdditionalDemandsForm {

	private Map<Integer, AdditionalDemandStatus> demandStatus;
	private Map<Integer, String> demandComment;

	public Map<Integer, AdditionalDemandStatus> getDemandStatus() {
		return demandStatus;
	}

	public void setDemandStatus(Map<Integer, AdditionalDemandStatus> demandStatus) {
		this.demandStatus = demandStatus;
	}

	public Map<Integer, String> getDemandComment() {
		return demandComment;
	}

	public void setDemandComment(Map<Integer, String> demandComment) {
		this.demandComment = demandComment;
	}
}