package org.trescal.cwms.core.jobs.jobitem.projection;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.company.projection.CompanyProjectionDTO;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItemDTO;
import org.trescal.cwms.core.instrument.dto.InstrumentProjectionDTO;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.jobs.certificate.dto.CertificateProjectionDTO;
import org.trescal.cwms.core.jobs.contractreview.projection.ContractReviewProjectionDTO;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.projection.ClientPurchaseOrderProjectionDTO;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.pricing.jobcost.projection.JobCostingItemProjectionDTO;
import org.trescal.cwms.core.pricing.purchaseorder.projection.PurchaseOrderItemProjectionDTO;
import org.trescal.cwms.core.procedure.dto.CapabilityProjectionDTO;
import org.trescal.cwms.core.system.entity.servicetype.ServiceTypeDto;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created for job search results.
 * 
 * Intended for more generic projection usage where we can selectively load
 * relationships.
 */
@Getter
@Setter
public class JobItemProjectionDTO {
	// Below result fields always populated by query
	private Integer jobItemId;
	private Integer jobid;
	private String jobno;
	private Integer itemno;
	private Integer plantid;
	private Integer serviceTypeId;
	private String serviceTypeShortNameTranslation;
	private Integer capabilityId;
	private Integer stateId;
	private Integer onBehalfCompanyId;
	private Integer contractReviewById;
	private Integer turn;
	private Date dateComplete;
	private ZonedDateTime dateIn;
	private LocalDate dueDate;
	private String clientRef;
	private String defaultPage;
	private String procReference;
	private Integer calTypeId;
	// Optional fields populated by some queries
	private Integer contractId;
	private Integer currencyId;
	private KeyValueIntegerString currency;
	// private Boolean hasContractReview;
	private JobItemActionProjectionDTO lastAction;

	// Optional single valued relationships
	private ServiceTypeDto serviceType;
	private CapabilityProjectionDTO capability; // aka "Procedure"
	private KeyValueIntegerString state; // with translation
	private KeyValueIntegerString contract; // with value as contract number
	private InstrumentProjectionDTO instrument;
	private JobProjectionDTO job;
	private CompanyProjectionDTO onBehalfCompany;
	private ContactProjectionDTO contractReviewBy;
	private JobItemPriceProjectionDto priceContractReview;
	private PriceSummaryDTO estimatedPrice; // Calculated from CR/JC prices
	// Optional multi-valued relationships
	private List<DeliveryItemDTO> deliveryItems;
	private List<JobItemActionProjectionDTO> actions;
	private List<ContractReviewProjectionDTO> contractReviews;
	private List<ClientPurchaseOrderProjectionDTO> jobItemPos;
	private List<CertificateProjectionDTO> certificates;
	private List<JobCostingItemProjectionDTO> costingItems;
	private List<PurchaseOrderItemProjectionDTO> supplierPos;

	/**
	 * Constructor 1 - used when no instrument information is supplied
	 * <p>
	 * Explicit constructor required (not via Lombok) as not all fields loaded
	 */
	public JobItemProjectionDTO(Integer jobItemId, Integer jobid, Integer itemno, Integer plantid,
			Integer serviceTypeId, Integer capabilityId, Integer stateId, Integer onBehalfCompanyId,
			Integer contractReviewById, Integer turn, Date dateComplete, ZonedDateTime dateIn, LocalDate dueDate,
			String clientRef) {
		super();
		this.jobItemId = jobItemId;
		this.jobid = jobid;
		this.itemno = itemno;
		this.plantid = plantid;
		this.serviceTypeId = serviceTypeId;
		this.capabilityId = capabilityId;
		this.stateId = stateId;
		this.onBehalfCompanyId = onBehalfCompanyId;
		this.contractReviewById = contractReviewById;
		this.turn = turn;
		this.dateComplete = dateComplete;
		this.dateIn = dateIn;
		this.dueDate = dueDate;
		this.clientRef = clientRef;
	}

	/**
	 * Constructor 2 - used when Instrument information is included at same time
	 * - and optionally latest job item action comment (e.g. for use with
	 * selected job items) -
	 */
	public JobItemProjectionDTO(Integer jobItemId, Integer jobid, Integer itemno, Integer plantid, String plantno,
			String serialno, String customerDescription, Boolean calibrationStandard, Boolean scrapped,
			String modelname, Integer modelid, String model, ModelMfrType modelMfrType, Boolean genericMfr,
			String mfrName, Integer typology, Integer serviceTypeId, String serviceTypeShortNameTranslation,
			Integer capabilityId, Integer stateId, Integer onBehalfCompanyId, Integer contractReviewById, Integer turn,
			Date dateComplete, ZonedDateTime dateIn, LocalDate dueDate, String clientRef, Integer contractId,
			Boolean hasContractReview, Integer lastActionId, Date lastActionEndStamp, String lastActionRemark) {
		super();
		this.jobItemId = jobItemId;
		this.jobid = jobid;
		this.itemno = itemno;
		this.plantid = plantid;
		this.instrument = new InstrumentProjectionDTO(plantid, plantno, serialno, customerDescription,
				calibrationStandard, scrapped, modelid, model, modelMfrType, typology, genericMfr, mfrName, modelname);
		this.serviceTypeId = serviceTypeId;
		this.serviceTypeShortNameTranslation = serviceTypeShortNameTranslation;
		this.capabilityId = capabilityId;
		this.stateId = stateId;
		this.onBehalfCompanyId = onBehalfCompanyId;
		this.contractReviewById = contractReviewById;
		this.turn = turn;
		this.dateComplete = dateComplete;
		this.dateIn = dateIn;
		this.dueDate = dueDate;
		this.clientRef = clientRef;
		this.contractId = contractId;

		// TODO : refactor this into view
		if (hasContractReview != null && hasContractReview)
			this.defaultPage = "jiactions.htm";
		else
			this.defaultPage = "jicontractreview.htm";

		if (lastActionId != null) {
			// Note - only some info populated
			this.lastAction = new JobItemActionProjectionDTO(lastActionId, jobItemId, lastActionEndStamp,
					lastActionRemark);
		}
	}

	/**
	 * Constructor is used by JobItemDaoImpl::getJobItemsFromJobs(); target to
	 * consolidate with others
	 */
	public JobItemProjectionDTO(Integer jobItemId, Integer itemno, String jobno, Integer plantid, String plantno,
			String serialno, String customerDescription, Boolean calibrationStandard, Boolean scrapped,
			String modelname, Integer modelid, String model, ModelMfrType modelMfrType, Boolean genericMfr,
			String mfrName, Integer typology) {
		super();
		this.jobItemId = jobItemId;
		this.itemno = itemno;
		this.jobno = jobno;
		this.plantid = plantid;
		this.instrument = new InstrumentProjectionDTO(plantid, plantno, serialno, customerDescription,
				calibrationStandard, scrapped, modelid, model, modelMfrType, typology, genericMfr, mfrName, modelname);

	}

	public JobItemProjectionDTO(Integer jobItemId, Integer jobid, Integer itemno, Integer plantid,
			Integer serviceTypeId, Integer jobCostingItemId, Integer jobCostingItemno, BigDecimal totalCost,
			BigDecimal finalCost, Integer jobCostingId, Integer jobCostingVersion, Integer currencyId) {
		super();
		this.jobItemId = jobItemId;
		this.jobid = jobid;
		this.itemno = itemno;
		this.plantid = plantid;
		this.serviceTypeId = serviceTypeId;
		this.currencyId = currencyId;
		this.costingItems = new ArrayList<>();
		if (jobCostingItemId != null && jobCostingItemno != null && totalCost != null && finalCost != null) {
			this.costingItems.add(new JobCostingItemProjectionDTO(jobCostingItemId, jobCostingItemno, totalCost,
					finalCost, jobItemId, jobCostingId, jobCostingVersion));
		}
	}

	public JobItemProjectionDTO(Integer jobItemId, Integer itemno, String jobno, Integer jobid, JobType jobtype,
			Integer plantid, Integer capabilityId, String plantno, String serialno, String customerDescription,
			Boolean calibrationStandard, Boolean scrapped, String modelname, Integer modelid, String model,
			ModelMfrType modelMfrType, Boolean genericMfr, String mfrName, Integer typology, Integer calTypeId) {
		super();
		this.jobItemId = jobItemId;
		this.itemno = itemno;
		this.jobno = jobno;
		this.plantid = plantid;
		this.capabilityId = capabilityId;
		this.calTypeId = calTypeId;
		this.instrument = new InstrumentProjectionDTO(plantid, plantno, serialno, customerDescription,
				calibrationStandard, scrapped, modelid, model, modelMfrType, typology, genericMfr, mfrName, modelname);
		this.job = new JobProjectionDTO(jobid, jobno, jobtype);

	}

}