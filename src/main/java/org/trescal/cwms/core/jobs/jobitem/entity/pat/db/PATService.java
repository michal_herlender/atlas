package org.trescal.cwms.core.jobs.jobitem.entity.pat.db;

import java.util.List;

import org.trescal.cwms.core.jobs.jobitem.entity.pat.PAT;

public interface PATService
{
	void deletePAT(PAT pat);

	PAT findPAT(int id);

	List<PAT> getAllPATs();

	void insertPAT(PAT pat);

	void updatePAT(PAT pat);
}