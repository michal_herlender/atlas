package org.trescal.cwms.core.jobs.jobitem.dto;

import lombok.Data;

@Data
public class JobItemDTO {

	private Integer id;
	private Integer itemNo;
	private String modelName;

	public JobItemDTO() {
	}

	public JobItemDTO(Integer id, Integer itemNo, String modelName) {
		this.id = id;
		this.itemNo = itemNo;
		this.modelName = modelName;
	}

}