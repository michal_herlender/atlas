package org.trescal.cwms.core.jobs.calibration.entity.callink.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;

@Service("CalLinkService")
public class CalLinkServiceImpl implements CalLinkService
{
	@Autowired
	CalLinkDao calLinkDao;

	public CalLink findCalLink(int id)
	{
		return calLinkDao.find(id);
	}

	public void insertCalLink(CalLink CalLink)
	{
		calLinkDao.persist(CalLink);
	}

	public void updateCalLink(CalLink CalLink)
	{
		calLinkDao.update(CalLink);
	}

	public List<CalLink> getAllCalLinks()
	{
		return calLinkDao.findAll();
	}

	public void setCalLinkDao(CalLinkDao calLinkDao)
	{
		this.calLinkDao = calLinkDao;
	}

	public void deleteCalLink(CalLink callink)
	{
		this.calLinkDao.remove(callink);
	}

	public void saveOrUpdateCalLink(CalLink callink)
	{
		this.calLinkDao.saveOrUpdate(callink);
	}
}