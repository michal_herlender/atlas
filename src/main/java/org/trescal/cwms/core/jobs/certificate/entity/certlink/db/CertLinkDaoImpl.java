package org.trescal.cwms.core.jobs.certificate.entity.certlink.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate_;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink_;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;

@Repository("CertLinkDao")
public class CertLinkDaoImpl extends BaseDaoImpl<CertLink, Integer> implements CertLinkDao {

	@Override
	protected Class<CertLink> getEntity() {
		return CertLink.class;
	}

	public Long getCertLinkCount(Integer plantId) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<CertLink> root = cq.from(CertLink.class);
			Join<CertLink, JobItem> jobItem = root.join(CertLink_.jobItem);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
			cq.where(cb.equal(instrument.get(Instrument_.plantid), plantId));
			cq.select(cb.count(root));
			return cq;
		});
	}

	@Override
	@Deprecated
	public List<CertLink> searchCertLink(Integer plantId, boolean includeDeletedAndCancelledCertificates) {
		return getResultList(cb -> {
			CriteriaQuery<CertLink> cq = cb.createQuery(CertLink.class);
			Root<CertLink> certLink = cq.from(CertLink.class);
			Join<CertLink, JobItem> jobItem = certLink.join(CertLink_.jobItem);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
			Join<CertLink, Certificate> certificate = certLink.join(CertLink_.cert);

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(instrument.get(Instrument_.plantid), plantId));
			if (!includeDeletedAndCancelledCertificates){
				clauses.getExpressions().add(cb.notEqual(certificate.get(Certificate_.status), CertStatusEnum.DELETED));
				clauses.getExpressions().add(cb.notEqual(certificate.get(Certificate_.status), CertStatusEnum.CANCELLED));
			}
			cq.where(clauses);

			cq.orderBy(cb.desc(certificate.get(Certificate_.calDate)), cb.desc(certificate.get(Certificate_.certno)));
			return cq;
		});
	}

	@Override
	public List<CertLink> searchByPlantId(Integer plantid, Integer maxResults) {
		if (plantid == null)
			throw new IllegalArgumentException("plantid must be specified");
		if (maxResults == null)
			throw new IllegalArgumentException("maxResults must be specified");
		return getResultList(cb -> {
			CriteriaQuery<CertLink> cq = cb.createQuery(CertLink.class);
			Root<CertLink> root = cq.from(CertLink.class);
			Join<CertLink, JobItem> jobItem = root.join(CertLink_.jobItem);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
			Join<CertLink, Certificate> certificate = root.join(CertLink_.cert);
			cq.where(cb.equal(instrument.get(Instrument_.plantid), plantid));
			cq.orderBy(cb.desc(certificate.get(Certificate_.calDate)), cb.desc(certificate.get(Certificate_.certno)),
					cb.desc(root.get(CertLink_.linkId)));
			return cq;
		}, 0, maxResults);
	}
}