package org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.db;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.QueuedCalibration;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

public interface QueuedCalibrationService
{
	void ajaxReprintCertificates(int[] certIds, HttpSession session);

	ResultWrapper checkAccreditedForBatchCertificates(int batchId, int personId);

	/**
	 * Deletes the given {@link QueuedCalibration} from the database.
	 * 
	 * @param queuedcalibration the {@link QueuedCalibration} to delete.
	 */
	void deleteQueuedCalibration(QueuedCalibration queuedcalibration);

	/**
	 * Returns the {@link QueuedCalibration} with the given ID.
	 * 
	 * @param id the {@link QueuedCalibration} ID.
	 * @return the {@link QueuedCalibration}
	 */
	QueuedCalibration findQueuedCalibration(int id);

	List<QueuedCalibration> getAccreditedActiveQueuedCalibrations(int personId);

	List<QueuedCalibration> getAccreditedActiveQueuedCalibrationsForBatches(List<Integer> batchIds, int personId);

	/**
	 * Returns a {@link List} of all {@link QueuedCalibration}s that are
	 * currently set as active (i.e. where the {@link Calibration} associated
	 * has not yet had it's {@link Certificate} generated). The {@link List} is
	 * returned in order of the time the {@link Calibration} was queued, so that
	 * {@link Calibration}s that have been queued for the longest time have
	 * their {@link Certificate} generated first.
	 * 
	 * @return {@link List} of {@link QueuedCalibration}s.
	 */
	List<QueuedCalibration> getActiveQueuedCalibrations();

	/**
	 * Returns a {@link List} of all {@link QueuedCalibration}s that are
	 * currently set as active for the {@link BatchCalibration}s with the given
	 * IDs.
	 * 
	 * @param batchIds the {@link BatchCalibration} IDs.
	 * @return {@link List} of {@link QueuedCalibration}s.
	 */
	List<QueuedCalibration> getActiveQueuedCalibrationsForBatches(List<Integer> batchIds);

	/**
	 * See getActiveQueuedCalibrations(). Orders the results by {@link Job}
	 * instead of time queued.
	 * 
	 * @return {@link List} of {@link QueuedCalibration}s.
	 */
	List<QueuedCalibration> getActiveQueuedCalibrationsOrderedByJob();

	/**
	 * Returns a {@link List} of all {@link QueuedCalibration}s in the database.
	 * 
	 * @return the {@link List} of {@link QueuedCalibration}s.
	 */
	List<QueuedCalibration> getAllQueuedCalibrations();

	List<QueuedCalibration> getQueuedCalibrationsForReprint();

	List<QueuedCalibration> getQueuedCalibrationsToPrintNow();

	/**
	 * Returns a {@link List} of {@link QueuedCalibration}s that have IDs
	 * matching those in the given {@link List} of IDs.
	 * 
	 * @param ids the {@link List} of IDs.
	 * @return the {@link List} of {@link QueuedCalibration}s.
	 */
	List<QueuedCalibration> getQueuedCalibrationsWithIds(List<Integer> ids);

	/**
	 * Inserts the given {@link QueuedCalibration} into the database.
	 * 
	 * @param queuedcalibration the {@link QueuedCalibration} to insert.
	 */
	void insertQueuedCalibration(QueuedCalibration queuedcalibration);

	void printActiveQueuedCalibrationsForBatches(List<Integer> batchIds, Contact printedBy, HashMap<Integer, Integer> map);

	void printSelectedQueuedCalibrations(List<QueuedCalibration> queuedCals, HttpSession session);

	/**
	 * Creates and persists a new active {@link QueuedCalibration} object for
	 * the given {@link Calibration} and records the current time as the time
	 * queued.
	 * 
	 * @param cal the {@link Calibration} to queue.
	 */
	void queueCalibration(Calibration cal);

	void reprintCertificates(List<Certificate> certs, Contact printedBy);
	/**
	 * Updates the given {@link QueuedCalibration} in the database if it already
	 * exists, otherwise inserts it.
	 * 
	 * @param queuedcalibration the {@link QueuedCalibration} to update or
	 *        insert.
	 */
	void saveOrUpdateQueuedCalibration(QueuedCalibration queuedcalibration);

	/**
	 * Finds the {@link QueuedCalibration} with the given ID and sets it as
	 * inactive, also setting the time completed field to the current time.
	 * 
	 * @param queuedCalibrationId the {@link QueuedCalibration} ID.
	 */
	void setQueuedCalibrationAsCompleted(QueuedCalibration qc);

	/**
	 * Updates the given {@link QueuedCalibration} in the database.
	 * 
	 * @param queuedcalibration the {@link QueuedCalibration} to update.
	 */
	void updateQueuedCalibration(QueuedCalibration queuedcalibration);
}