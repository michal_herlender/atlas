package org.trescal.cwms.core.jobs.job.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;

@Controller
@IntranetController
public class DeleteJobController {
	@Autowired
	private JobService jobService;
	@Autowired
	private JobItemService jobItemService;

	@RequestMapping("/deletejob.htm")
	public String handleRequest(@RequestParam(value = "jobid", required = true) Integer jobid) {
		Job job = this.jobService.get(jobid);
		if (job.getItems().size() > 0) {
			return "redirect:viewjob.htm?jobid=" + job.getJobid();
		} else {
			this.jobService.deleteJobById(jobid);
			return "redirect:home.htm";
		}
	}

	@RequestMapping("deletejobitem.htm")
	public String deleteJobItem(RedirectAttributes redirectAttributes,
			@RequestParam(name = "deleteItemId", required = true) Integer jobItemId) {
		Integer jobId = this.jobItemService.findJobItem(jobItemId).getJob().getJobid();
		ResultWrapper resWrap = this.jobItemService.deleteJobItemWithChecks(jobItemId);
		redirectAttributes.addFlashAttribute("message", resWrap.getMessage());
		return "redirect:viewjob.htm?jobid=" + jobId;
	}
}