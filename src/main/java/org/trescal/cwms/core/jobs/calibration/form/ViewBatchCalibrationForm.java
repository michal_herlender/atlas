package org.trescal.cwms.core.jobs.calibration.form;

import java.io.File;
import java.util.List;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.calibration.dto.AccreditedCalibrationWrapper;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.calibration.enums.BatchCalibrationInvoiceType;

public class ViewBatchCalibrationForm
{
	private boolean autoPrinting;
	private BatchCalibration batchCal;
	private boolean batchRequiresCourier;
	private List<AccreditedCalibrationWrapper> calWrappers;
	private boolean canBeDeleted;
	private ResultWrapper canCertify;
	private Integer cdtId;
	private boolean certify;
	private String confirmPassword;
	private String consignmentNo;
	private Contact currentContact;
	private boolean despatch;
	private boolean includeRecallDateOnLabels;
	private boolean invoice;
	private String invoiceitems;
	private BatchCalibrationInvoiceType invType;
	private BatchCalibrationInvoiceType[] invTypes;
	private File jobCertDirectory;
	private boolean jobSheet;
	private boolean labels;
	private String password;
	private List<Integer> selectedBatchIds;

	public BatchCalibration getBatchCal()
	{
		return this.batchCal;
	}

	public List<AccreditedCalibrationWrapper> getCalWrappers()
	{
		return this.calWrappers;
	}

	public ResultWrapper getCanCertify()
	{
		return this.canCertify;
	}

	public Integer getCdtId()
	{
		return this.cdtId;
	}

	public String getConfirmPassword()
	{
		return this.confirmPassword;
	}

	public String getConsignmentNo()
	{
		return this.consignmentNo;
	}

	public Contact getCurrentContact()
	{
		return this.currentContact;
	}

	public String getInvoiceitems()
	{
		return this.invoiceitems;
	}

	public BatchCalibrationInvoiceType getInvType()
	{
		return this.invType;
	}

	public BatchCalibrationInvoiceType[] getInvTypes()
	{
		return this.invTypes;
	}

	public File getJobCertDirectory()
	{
		return this.jobCertDirectory;
	}

	public String getPassword()
	{
		return this.password;
	}

	public List<Integer> getSelectedBatchIds()
	{
		return this.selectedBatchIds;
	}

	public boolean isAutoPrinting()
	{
		return this.autoPrinting;
	}

	public boolean isBatchRequiresCourier()
	{
		return this.batchRequiresCourier;
	}

	public boolean isCanBeDeleted()
	{
		return this.canBeDeleted;
	}

	public boolean isCertify()
	{
		return this.certify;
	}

	public boolean isDespatch()
	{
		return this.despatch;
	}

	public boolean isIncludeRecallDateOnLabels()
	{
		return this.includeRecallDateOnLabels;
	}

	public boolean isInvoice()
	{
		return this.invoice;
	}

	public boolean isJobSheet()
	{
		return this.jobSheet;
	}

	public boolean isLabels()
	{
		return this.labels;
	}

	public void setAutoPrinting(boolean autoPrinting)
	{
		this.autoPrinting = autoPrinting;
	}

	public void setBatchCal(BatchCalibration batchCal)
	{
		this.batchCal = batchCal;
	}

	public void setBatchRequiresCourier(boolean batchRequiresCourier)
	{
		this.batchRequiresCourier = batchRequiresCourier;
	}

	public void setCalWrappers(List<AccreditedCalibrationWrapper> calWrappers)
	{
		this.calWrappers = calWrappers;
	}

	public void setCanBeDeleted(boolean canBeDeleted)
	{
		this.canBeDeleted = canBeDeleted;
	}

	public void setCanCertify(ResultWrapper canCertify)
	{
		this.canCertify = canCertify;
	}

	public void setCdtId(Integer cdtId)
	{
		this.cdtId = cdtId;
	}

	public void setCertify(boolean certify)
	{
		this.certify = certify;
	}

	public void setConfirmPassword(String confirmPassword)
	{
		this.confirmPassword = confirmPassword;
	}

	public void setConsignmentNo(String consignmentNo)
	{
		this.consignmentNo = consignmentNo;
	}

	public void setCurrentContact(Contact currentContact)
	{
		this.currentContact = currentContact;
	}

	public void setDespatch(boolean despatch)
	{
		this.despatch = despatch;
	}

	public void setIncludeRecallDateOnLabels(boolean includeRecallDateOnLabels)
	{
		this.includeRecallDateOnLabels = includeRecallDateOnLabels;
	}

	public void setInvoice(boolean invoice)
	{
		this.invoice = invoice;
	}

	public void setInvoiceitems(String invoiceitems)
	{
		this.invoiceitems = invoiceitems;
	}

	public void setInvType(BatchCalibrationInvoiceType invType)
	{
		this.invType = invType;
	}

	public void setInvTypes(BatchCalibrationInvoiceType[] invTypes)
	{
		this.invTypes = invTypes;
	}

	public void setJobCertDirectory(File jobCertDirectory)
	{
		this.jobCertDirectory = jobCertDirectory;
	}

	public void setJobSheet(boolean jobSheet)
	{
		this.jobSheet = jobSheet;
	}

	public void setLabels(boolean labels)
	{
		this.labels = labels;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public void setSelectedBatchIds(List<Integer> selectedBatchIds)
	{
		this.selectedBatchIds = selectedBatchIds;
	}
}