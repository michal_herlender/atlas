package org.trescal.cwms.core.jobs.jobitem.entity.pat.db;

import java.util.List;

import org.trescal.cwms.core.jobs.jobitem.entity.pat.PAT;

public class PATServiceImpl implements PATService
{
	private PATDao patDao;

	public void deletePAT(PAT pat)
	{
		this.patDao.remove(pat);
	}

	public PAT findPAT(int id)
	{
		return this.patDao.find(id);
	}

	public List<PAT> getAllPATs()
	{
		return this.patDao.findAll();
	}

	public void insertPAT(PAT PAT)
	{
		this.patDao.persist(PAT);
	}

	public void setPatDao(PATDao patDao)
	{
		this.patDao = patDao;
	}

	public void updatePAT(PAT PAT)
	{
		this.patDao.update(PAT);
	}
}