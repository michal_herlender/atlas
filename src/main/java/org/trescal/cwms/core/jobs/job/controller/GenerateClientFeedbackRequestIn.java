package org.trescal.cwms.core.jobs.job.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.jobs.job.dto.ClientFeedbackRequestDTO;

import java.util.Collections;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public  class GenerateClientFeedbackRequestIn {
    private List<ClientFeedbackRequestDTO> items = Collections.emptyList();
    private List<Integer> toIds = Collections.emptyList();
    private List<String> toEmails = Collections.emptyList();
    private List<Integer> ccIds = Collections.emptyList();
    private List<String> ccEmails = Collections.emptyList();
    private Integer jobId;
    private String body;
    private String subject;
    private Boolean send = false;
    private Boolean includeSelfInCarbonCopy = false;
}
