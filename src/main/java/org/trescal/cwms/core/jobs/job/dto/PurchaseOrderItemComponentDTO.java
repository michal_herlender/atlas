package org.trescal.cwms.core.jobs.job.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PurchaseOrderItemComponentDTO {

	private List<ClientPurchaseOrderItemDTO> jobItems;
	private List<ClientPurchaseOrderExpenseItemDTO> expenseItems;
}