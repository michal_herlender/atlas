package org.trescal.cwms.core.jobs.repair.dto.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.repair.dto.FreeRepairOperationDTO;
import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationTypeEnum;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class FreeRepairOperationDTOValidator extends AbstractBeanValidator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(FreeRepairOperationDTO.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		FreeRepairOperationDTO dto = (FreeRepairOperationDTO) target;
		super.validate(dto, errors);

		if (dto.getType() != null
				&& (dto.getType().equals(RepairOperationTypeEnum.EXTERNAL)
						|| dto.getType().equals(RepairOperationTypeEnum.EXTERNAL_WITH_CALIBRATION))
				&& (dto.getCoid() == null || dto.getCoid() == 0))
			errors.rejectValue("coid", "repairinspection.validator.fieldmandatory", "Mandatory field");
		
		if(dto.getStatus() == null)
			errors.rejectValue("status", "repairinspection.validator.mandatory", "Mandatory field");

		if(((dto.getLabourTimeHours() != null && dto.getLabourTimeHours() < 1) && 
			(dto.getLabourTimeMinutes() != null && dto.getLabourTimeMinutes() < 1)) || 
			(dto.getLabourTimeHours() != null && dto.getLabourTimeHours() > 99))
				errors.rejectValue("labourTimeHours", "repairinspection.validator.invalidtime", "Invalid Time (0<H<100 and 0<M<60)");
			if(((dto.getLabourTimeHours() != null && dto.getLabourTimeHours() < 1) && 
				(dto.getLabourTimeMinutes() != null && dto.getLabourTimeMinutes() < 1)) || 
				(dto.getLabourTimeMinutes() != null && dto.getLabourTimeMinutes() > 59))
				errors.rejectValue("labourTimeMinutes", "repairinspection.validator.invalidtime", "Invalid Time (0<H<100 and 0<M<60)");
	}

}
