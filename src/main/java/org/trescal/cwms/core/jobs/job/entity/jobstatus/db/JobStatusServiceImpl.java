package org.trescal.cwms.core.jobs.job.entity.jobstatus.db;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public class JobStatusServiceImpl implements JobStatusService
{
	private JobStatusDao jobStatusDao;

	public void deleteJobStatus(JobStatus jobstatus)
	{
		this.jobStatusDao.remove(jobstatus);
	}

	@Override
	public JobStatus findCompleteStatus()
	{
		return this.jobStatusDao.findCompleteStatus();
	}

	public JobStatus findJobStatus(int id)
	{
		return this.jobStatusDao.find(id);
	}

	@Override
	public JobStatus findReadyToInvoiceStatus()
	{
		return this.jobStatusDao.findReadyToInvoiceStatus();
	}
	
	@Override
	public List<KeyValueIntegerString> getKeyValueList(Collection<Integer> jobStatusIds, Locale locale) {
		List<KeyValueIntegerString> result = Collections.emptyList();
		if (!jobStatusIds.isEmpty()) {
			result = this.jobStatusDao.getKeyValueList(jobStatusIds, locale);
		}
		return result;
	}

	public List<JobStatus> getAllJobStatuss()
	{
		return this.jobStatusDao.findAll();
	}

	public void insertJobStatus(JobStatus JobStatus)
	{
		this.jobStatusDao.persist(JobStatus);
	}

	public void saveOrUpdateJobStatus(JobStatus jobstatus)
	{
		this.jobStatusDao.saveOrUpdate(jobstatus);
	}

	public void setJobStatusDao(JobStatusDao jobStatusDao)
	{
		this.jobStatusDao = jobStatusDao;
	}

	public void updateJobStatus(JobStatus JobStatus)
	{
		this.jobStatusDao.update(JobStatus);
	}
}