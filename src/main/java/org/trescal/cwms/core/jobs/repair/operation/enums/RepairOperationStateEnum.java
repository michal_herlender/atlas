package org.trescal.cwms.core.jobs.repair.operation.enums;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum RepairOperationStateEnum {
	AWAITING_EXECUTION("repairreport.operation.status.awaitingexecution",true,false),
	EXECUTED("repairreport.operation.status.executed",true,true),
	IGNORED("repairreport.operation.status.ignored",false,true);

	private String messageCode;
	private boolean rirStatus;
	private boolean rcrStatus;
	private ReloadableResourceBundleMessageSource messageSource;

	private RepairOperationStateEnum(String messageCode, boolean rirStatus, boolean rcrStatus) {
		this.messageCode = messageCode;
		this.rirStatus = rirStatus;
		this.rcrStatus = rcrStatus;
	}

	@Component
	private static class MessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (RepairOperationStateEnum e : EnumSet.allOf(RepairOperationStateEnum.class))
				e.messageSource = messageSource;
		}
	}

	public String getMessage() {
		return messageSource.getMessage(messageCode, null, this.toString(), LocaleContextHolder.getLocale());
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	
	public String getValue() {
		Locale locale = LocaleContextHolder.getLocale();
		if (messageSource != null) {
			return messageSource.getMessage(this.messageCode, null, this.name(), locale);
		}
		return this.toString();
	}

	public boolean isRirStatus() {
		return rirStatus;
	}

	public boolean isRcrStatus() {
		return rcrStatus;
	}
}
