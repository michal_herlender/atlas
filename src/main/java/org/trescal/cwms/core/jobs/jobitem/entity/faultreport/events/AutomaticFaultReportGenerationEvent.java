package org.trescal.cwms.core.jobs.jobitem.entity.faultreport.events;

import org.springframework.context.ApplicationEvent;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;

public class AutomaticFaultReportGenerationEvent extends ApplicationEvent {

	private static final long serialVersionUID = 1L;

	private FaultReport faultReport;
	private boolean withClientReponse;

	public AutomaticFaultReportGenerationEvent(Object source, FaultReport faultReport, boolean withClientReponse) {
		super(source);
		this.faultReport = faultReport;
		this.withClientReponse = withClientReponse;
	}

	public FaultReport getFaultReport() {
		return faultReport;
	}

	public boolean isWithClientReponse() {
		return withClientReponse;
	}
}
