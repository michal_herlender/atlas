package org.trescal.cwms.core.jobs.job.entity.jobbpolink.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink;

public interface JobBPOLinkService extends BaseService<JobBPOLink, Integer> {

	JobBPOLink findByJobIdAndBpoId(Integer jobId, Integer bpoId);
}