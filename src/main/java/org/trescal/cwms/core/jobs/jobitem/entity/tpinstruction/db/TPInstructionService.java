package org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction.db;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.deliverynote.form.TPInstructionDTO;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction.TPInstruction;

public interface TPInstructionService extends BaseService<TPInstruction, Integer>
{
	ResultWrapper deactivateTPInstruction(int instructionId, HttpSession session);

	ResultWrapper insertTPInstruction(int jobItemId, String instruction, HttpSession session);

	ResultWrapper updateTPInstruction(int instructionId, String instruction);

	ResultWrapper updateTPInstructionShowOnTPDN(int instructionId, boolean showOnTPDN);
	
	/**
	 * Get all tpInstructions for a job.
	 * 
	 * @param jobId  identifier of the job
	 */
	List<TPInstructionDTO> getAllByJob(Integer jobId);
}