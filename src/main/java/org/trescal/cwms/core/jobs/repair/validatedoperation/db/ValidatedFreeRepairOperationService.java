package org.trescal.cwms.core.jobs.repair.validatedoperation.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperation;

public interface ValidatedFreeRepairOperationService extends BaseService<ValidatedFreeRepairOperation, Integer>
{
	
}