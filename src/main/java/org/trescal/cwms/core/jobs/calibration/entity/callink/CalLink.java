package org.trescal.cwms.core.jobs.calibration.entity.callink;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

@Entity
@Table(name = "callink", uniqueConstraints = { @UniqueConstraint(columnNames = { "calid", "jobitemid" }) })
public class CalLink extends Auditable {
	private Calibration cal;
	private boolean certIssued;
	private int id;
	private JobItem ji;
	private boolean mainItem;
	private ActionOutcome outcome;
	private Integer timeSpent;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "calid", nullable = false)
	public Calibration getCal() {
		return this.cal;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "jobitemid", nullable = false)
	public JobItem getJi() {
		return this.ji;
	}

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "actionoutcomeid")
	public ActionOutcome getOutcome() {
		return this.outcome;
	}

	@Column(name = "timespent")
	public Integer getTimeSpent() {
		return this.timeSpent;
	}

	@NotNull
	@Column(name = "certissued", nullable = false, columnDefinition = "bit")
	public boolean isCertIssued() {
		return this.certIssued;
	}

	@Column(name = "mainitem", columnDefinition = "bit")
	public boolean isMainItem() {
		return this.mainItem;
	}

	public void setCal(Calibration cal) {
		this.cal = cal;
	}

	public void setCertIssued(boolean certIssued) {
		this.certIssued = certIssued;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setJi(JobItem ji) {
		this.ji = ji;
	}

	public void setMainItem(boolean mainItem) {
		this.mainItem = mainItem;
	}

	public void setOutcome(ActionOutcome outcome) {
		this.outcome = outcome;
	}

	public void setTimeSpent(Integer timeSpent) {
		this.timeSpent = timeSpent;
	}

}