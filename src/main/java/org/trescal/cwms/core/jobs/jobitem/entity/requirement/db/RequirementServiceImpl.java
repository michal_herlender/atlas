package org.trescal.cwms.core.jobs.jobitem.entity.requirement.db;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.dwr.DWRService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.requirement.Requirement;
import org.trescal.cwms.core.jobs.jobitem.entity.requirement.RequirementComparator;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.presetcomment.db.PresetCommentService;
import org.trescal.cwms.core.validation.BeanValidator;

@Service("requirementService")
public class RequirementServiceImpl implements RequirementService
{
	@Autowired
	private DWRService dwrServ;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private JobItemService jobitemServ;
	@Autowired
	private MessageSource messages;
	@Autowired
	private PresetCommentService presetCommentServ;
	@Autowired
	private RequirementDao reqDao;
	@Autowired
	private BeanValidator reqValidator;

	public void deleteRequirement(Requirement r)
	{
		this.reqDao.remove(r);
	}

	public void deleteRequirementById(String id)
	{
		this.reqDao.deleteRequirementById(id);
	}

	public Requirement findRequirement(Integer id)
	{
		return this.reqDao.find(id);
	}

	public Set<Requirement> findRequirementByJobItem(int jobitemid)
	{
		List<Requirement> list = this.reqDao.findRequirementByJobItem(jobitemid);
		TreeSet<Requirement> reqs = new TreeSet<Requirement>(new RequirementComparator());
		if (list != null)
		{
			reqs.addAll(list);
		}
		return reqs;
	}

	public List<Requirement> getAllRequirements()
	{
		return this.reqDao.findAll();
	}

	@Override
	public ResultWrapper insert(String requirement, int jobitemid, boolean save, boolean saveInstrumentReq, int plantid)
	{
		JobItem jobitem = this.jobitemServ.findJobItem(jobitemid);
		if (jobitem == null)
		{
			return new ResultWrapper(false, "No matching jobitem could be found.", null, null);
		}
		else
		{
			Requirement req = new Requirement();
			req.setCreated(new Date());
			req.setCreatedBy(this.dwrServ.getCurrentUser());
			req.setItem(jobitem);
			req.setRequirement(requirement);

			BindException errors = new BindException(req, "requirement");
			this.reqValidator.validate(req, errors);

			if (!errors.hasErrors())
			{
				this.reqDao.persist(req);
				if (save) {
					PresetComment comment = new PresetComment();
					comment.setType(PresetCommentType.JOB_ITEM_REQUIREMENT);
					comment.setComment(req.getRequirement());
					comment.setSeton(new Date());
					comment.setSetby(this.dwrServ.getCurrentUser());
					this.presetCommentServ.save(comment);
				}
				if (saveInstrumentReq) {
					Instrument inst = this.instServ.get(plantid);
					inst.setRequirement(requirement);
					this.instServ.update(inst);
				}
			}
			return new ResultWrapper(errors, req);
		}
	}
	
	public void insertRequirement(Requirement r)
	{
		this.reqDao.persist(r);
	}

	@Override
	public void saveOrUpdateAll(List<Requirement> requirements)
	{
		this.reqDao.saveOrUpdateAll(requirements);
	}

	public List<Requirement> searchRequirement(String requirement)
	{
		return this.reqDao.searchRequirement(requirement);
	}

	public void setDwrServ(DWRService dwrServ)
	{
		this.dwrServ = dwrServ;
	}

	public void setInstServ(InstrumService instServ)
	{
		this.instServ = instServ;
	}

	public void setJobitemServ(JobItemService jobitemServ)
	{
		this.jobitemServ = jobitemServ;
	}

	public void setMessages(MessageSource messages)
	{
		this.messages = messages;
	}

	public void setPresetCommentServ(PresetCommentService presetCommentServ)
	{
		this.presetCommentServ = presetCommentServ;
	}

	/**
	 * @param reqDao the reqDao to set
	 */
	public void setReqDao(RequirementDao reqDao)
	{
		this.reqDao = reqDao;
	}

	public void setReqValidator(BeanValidator reqValidator)
	{
		this.reqValidator = reqValidator;
	}

	@Override
	public ResultWrapper update(int id, String requirement, boolean saveComment, boolean saveInstrumentReq, int plantid)
	{
		Requirement req = this.findRequirement(id);
		if (req == null)
		{
			return new ResultWrapper(false, this.messages.getMessage("error.requirement.notfound", null, "The requested requirement could not be found", null));
		}
		else
		{
			req.setRequirement(requirement);
			req.setCreatedBy(this.dwrServ.getCurrentUser());
			req.setCreated(new Date());

			BindException errors = new BindException(req, "requirement");
			this.reqValidator.validate(req, errors);
			
			if (!errors.hasErrors()) {
				this.reqDao.persist(req);
				if (saveComment) {
					PresetComment comment = new PresetComment();
					comment.setType(PresetCommentType.JOB_ITEM_REQUIREMENT);
					comment.setComment(req.getRequirement());
					comment.setSeton(new Date());
					comment.setSetby(this.dwrServ.getCurrentUser());
					this.presetCommentServ.save(comment);
				}
				if (saveInstrumentReq) {
					Instrument inst = this.instServ.get(plantid);
					if (inst != null) {
						inst.setRequirement(requirement);
						this.instServ.update(inst);
					}
				}
			}
			return new ResultWrapper(errors, req);
		}
	}

	public void updateRequirement(Requirement r)
	{
		this.reqDao.update(r);
	}

	@Override
	public ResultWrapper updateRequirementStatus(int reqId, boolean deleted)
	{
		Requirement req = this.findRequirement(reqId);
		if (req == null)
		{
			return new ResultWrapper(false, "The requirement could not be found.", null, null);
		}
		else
		{
			req.setDeleted(deleted);
			req.setDeletedBy(deleted ? this.dwrServ.getCurrentUser() : null);
			req.setDeletedOn(deleted ? new Date() : null);
			this.updateRequirement(req);
			return new ResultWrapper(true, "", req, null);
		}
	}
}