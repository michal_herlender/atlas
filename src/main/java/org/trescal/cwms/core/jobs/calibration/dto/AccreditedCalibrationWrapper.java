package org.trescal.cwms.core.jobs.calibration.dto;

import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;

public class AccreditedCalibrationWrapper
{
	private boolean accredited;
	private Calibration cal;
	private boolean wordFileExists;

	public AccreditedCalibrationWrapper(Calibration cal, boolean accredited, boolean wordFileExists)
	{
		this.cal = cal;
		this.accredited = accredited;
		this.wordFileExists = wordFileExists;
	}

	public Calibration getCal()
	{
		return this.cal;
	}

	public boolean isAccredited()
	{
		return this.accredited;
	}

	public boolean isWordFileExists()
	{
		return this.wordFileExists;
	}

	public void setAccredited(boolean accredited)
	{
		this.accredited = accredited;
	}

	public void setCal(Calibration cal)
	{
		this.cal = cal;
	}

	public void setWordFileExists(boolean wordFileExists)
	{
		this.wordFileExists = wordFileExists;
	}
}