package org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork;

import lombok.Setter;

@Setter
@Entity
@Table(name="upcomingworkjoblink")
@AllArgsConstructor
@NoArgsConstructor
public class UpcomingWorkJobLink {
	private int id;
	private Boolean active;
	private Job job;
	private UpcomingWork upcomingWork;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@Type(type = "int")
	public int getId() {
		return id;
	}
	
	@NotNull
	@Column(name = "active", nullable=false, columnDefinition="bit")
	public Boolean getActive() {
		return active;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobid", nullable=false, foreignKey=@ForeignKey(name="FK_upcomingworkjoblink_jobid"))
	public Job getJob() {
		return job;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "upcomingworkid", nullable=false, foreignKey=@ForeignKey(name="FK_upcomingworkjoblink_upcomingworkid"))
	public UpcomingWork getUpcomingWork() {
		return upcomingWork;
	}
}