package org.trescal.cwms.core.jobs.jobitem.dto;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Date;

@Getter
@Setter
public class JobItemPlantillasDTO {
	public JobItemPlantillasDTO(Date lastModifiedJobItem, Date lastModifiedJob, Integer addrid, Integer plantid,
								Integer jobitemid, Integer jobid, String jobno, Integer jobitemno, Integer stateid, JobType jobType,
								Boolean accredited, ZonedDateTime dateIn, Date lastTransitDateIn, BigDecimal calCost, String currencyCode,
								Integer onBehalfOfAddrId, Integer onBehalfOfCoId) {
		super();
		this.lastModifiedJobItem = lastModifiedJobItem;
		this.lastModifiedJob = lastModifiedJob;
		this.addrid = addrid;
		this.plantid = plantid;
		this.jobitemid = jobitemid;
		this.jobid = jobid;
		this.jobno = jobno;
		this.jobitemno = jobitemno;
		this.stateid = stateid;
		this.jobType = jobType;
		this.accredited = accredited;
		this.dateIn = dateIn;
		this.lastTransitDateIn = lastTransitDateIn;
		this.calCost = calCost;
		this.currencyCode = currencyCode;
		this.onBehalfOfAddrId = onBehalfOfAddrId;
		this.onBehalfOfCoId = onBehalfOfCoId;
	}
	private Date lastModifiedJobItem;
	private Date lastModifiedJob;
	private Integer addrid;            // Client address we are returning the job to
	private Integer plantid;
	private Integer jobitemid;
	private Integer jobid;
	private String jobno;
	private Integer jobitemno;        // Number within the job
	private Integer stateid;        // Relating to job item state
	private JobType jobType;
	private Boolean accredited;
	private ZonedDateTime dateIn;            // Date job item created
	private Date lastTransitDateIn;    // Last date of job item transit into in local facility (if any)
	private BigDecimal calCost;        // Final Cost of the ContractReviewCalibrationCost
	private String currencyCode;    // Currency of the ContractReviewCalibrationCost
	private Integer onBehalfOfAddrId;
	private Integer onBehalfOfCoId;
}
