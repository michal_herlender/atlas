package org.trescal.cwms.core.jobs.calibration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationDTO;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.form.CalibrationSearchForm;
import org.trescal.cwms.core.jobs.calibration.form.CalibrationSearchValidator;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class CalibrationSearchController {
    @Autowired
    private CalibrationProcessService calProServ;
    @Autowired
    private CalibrationService calServ;
    @Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private DepartmentService deptServ;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private CalibrationSearchValidator validator;
	
	public static final String FORM_NAME = "form";

	@ModelAttribute(FORM_NAME)
	protected CalibrationSearchForm formBackingObject()
	{
		return new CalibrationSearchForm();
	}
	
	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.setValidator(validator);
	}
	
	@RequestMapping(value="/calsearch.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(
			@RequestParam(value="mid", required=false, defaultValue="") Integer mid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(FORM_NAME) CalibrationSearchForm form, BindingResult bindingResult, Locale locale ) throws Exception
	{
		if (bindingResult.hasErrors()) return referenceData(subdivDto, form, bindingResult);
		// you have to lookup and set the modelid separately as if it's set from
		// a url spring will break and try and double submit it
		if (mid != null) form.setModelid(mid);
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		PagedResultSet<CalibrationDTO> rs = this.calServ.searchCalibrations(form, new PagedResultSet<CalibrationDTO>(form.getResultsPerPage() == 0 ? Constants.RESULTS_PER_PAGE : 
			form.getResultsPerPage(), form.getPageNo() == 0 ? 1 : form.getPageNo()), allocatedSubdiv, locale);
		form.setRs(rs);
		return new ModelAndView("trescal/core/jobs/calibration/calsearchresults", FORM_NAME, form);
	}
	
	@RequestMapping(value="/calsearch.htm", params="forced=forced", method=RequestMethod.GET)
	public ModelAndView forceSubmit(
			@RequestParam(value="mid", required=false, defaultValue="null") Integer mid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(FORM_NAME) CalibrationSearchForm form, BindingResult bindingResult, Locale locale) throws Exception
	{
		if (bindingResult.hasErrors()) return referenceData(subdivDto, form, bindingResult);
		else return onSubmit(mid, subdivDto, form, bindingResult, locale);
	}
	
	@RequestMapping(value="/calsearch.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(FORM_NAME) CalibrationSearchForm form, BindingResult bindingResult) throws Exception
	{
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Map<String, Object> refData = new HashMap<String, Object>();
		refData.put("calstatuses", this.statusServ.getAllStatuss(CalibrationStatus.class));
		refData.put("caltypes", this.calTypeServ.getCalTypes());
        refData.put("calprocesses", this.calProServ.getAll());
        refData.put("depts", this.deptServ.getAllCapabilityEnabledSubdivDepartments(allocatedSubdiv));
		return new ModelAndView("trescal/core/jobs/calibration/calsearch", refData);
	}
}