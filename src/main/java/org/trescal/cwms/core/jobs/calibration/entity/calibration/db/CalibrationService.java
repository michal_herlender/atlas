package org.trescal.cwms.core.jobs.calibration.entity.calibration.db;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.trescal.cwms.batch.jobs.operationsimport.ImportedOperationItem;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.calibration.dto.CalToCertify;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationDTO;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.jobs.calibration.form.CalibrationSearchForm;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;

public interface CalibrationService {

	void ajaxStartCalibration(int calid, int startedById);

	/**
	 * Sets the status of the {@link Calibration} with the given ID to Cancelled.
	 *
	 * @param id the {@link Calibration} ID.
	 */
	void cancelCalibration(int id, Contact contact);

	/**
	 * Updates the batch positions of {@link Calibration}s in the
	 * {@link BatchCalibration} with the given ID by finding the {@link Calibration}
	 * at the given old position, setting it to the new position and
	 * incrementing/decrementing all the batch positions of the {@link Calibration}s
	 * in-between as appropriate.
	 * 
	 * @param batchId the {@link BatchCalibration} ID.
	 * @param oldPos  the position of the {@link Calibration} to move.
	 * @param newPos  the position to move the {@link Calibration} to.
	 */
	void changeCalibrationPositionInBatch(int batchId, Integer oldPos, Integer newPos);

	/**
	 * Checks the strength of a given password.
	 * 
	 * @param password the password to check the strength of.
	 * @return an Integer ranging from 0-5 (0 being weakest, 5 being strongest)
	 *         representing the strength of the given password.
	 */
	Integer checkPasswordStrength(String password);

	ResultWrapper completeBatchedCal(int calId, HttpSession session);

	void completeCalibration(Calibration cal, Contact completedBy, Map<Integer, Integer> timeSpents,
			Map<Integer, Integer> calLinkOutcomes, String remark, boolean certIssued, List<Integer> includeOnCert,
			Integer certDuration, IntervalUnit intervalUnit);

	ResultWrapper completeCalibrationAsUTC(int calId, int completedById, int actionOutcomeId, Integer timeSpent,
			HttpSession session);

	ResultWrapper completeCalibrationAsUTCMultipleOutcomes(int calId, int completedById,
			Map<Integer, Integer> linkedOutcomes, Map<Integer, Integer> timeSpents, String remark);

	void decrementStandardCounters(Calibration cal, Date beforeDate);

	/**
	 * Deletes the given {@link Calibration} from the database.
	 * 
	 * @param calibration the {@link Calibration} to delete.
	 */
	void deleteCalibration(Calibration calibration, Contact contact);

	/**
	 * Removes the {@link JobItemActivity} that will have been added when the given
	 * {@link Calibration} was started. e.g. an incomplete 'Pre-calibration'
	 * activity
	 * 
	 * @param cal the {@link Calibration} to erase the {@link JobItemActivity} for
	 */
	void eraseIncompleteCalibrationActivity(Calibration cal, Contact contact);

	/**
	 * Returns the {@link Calibration} in the {@link BatchCalibration} with the
	 * given ID and with the given position in the {@link BatchCalibration}
	 * 
	 * @return the {@link Calibration} or null if there is no match
	 */
	Calibration findBatchCalibrationByPosition(int batchId, int position);

	/**
	 * Returns the {@link Calibration} with the given ID.
	 * 
	 * @param id the {@link Calibration} ID.
	 * @return the {@link Calibration} entity.
	 */
	Calibration findCalibration(int id);

	CalToCertify findCalToIssueCert(int calId, int plantId);

	/**
	 * Returns the {@link Calibration} with the given ID.
	 * 
	 * @param id the {@link Calibration} ID.
	 * @return the {@link Calibration} entity.
	 */
	Calibration findEagerCalibration(int id);

	Calibration findOnHoldCalibrationForItem(int jobItemId);

	/**
	 * Returns all {@link Calibration} entities currently stored in the database.
	 * 
	 * @return the {@link List} of {@link Calibration} entities.
	 */
	List<Calibration> getAllCalibrations();

	List<Calibration> getAllCalibrationsForJobAtStatus(int jobId, String statusName);

	List<Calibration> getAllCalibrationsForJobAtStatusUsingProc(int jobId, String statusName, int procid);

	/**
	 * this method retrieves a list of all calibrations performed when the supplied
	 * id is one of the standards used for the calibration.
	 * 
	 * @param plantid    id of instrument standard
	 * @param page       page number of results required
	 * @param resPerPage results to be displayed per request
	 * @return {@link List} {@link Calibration}
	 */
	List<Calibration> getAllCalibrationsPerformedWithInstrumentStandard(int plantid, Integer page, Integer resPerPage);

	/**
	 * Returns a list of all ongoing {@link Calibration}s that have been started by
	 * the given {@link Contact}. Ordered by the start time.
	 * 
	 * @param personid the id of the {@link Contact} who started the
	 *                 {@link Calibration}s.
	 * @return list of {@link Calibration} entities.
	 */
	List<Calibration> getAllOngoingCalibrationsByContact(int personid);

	/**
	 * Returns the amount of minutes (rounded to nearest upper 5) since the
	 * {@link Calibration} with the given ID was created.
	 * 
	 * @param calid the {@link Calibration} ID
	 * @return amount of minutes since it began
	 */
	Integer getMinutesFromCalibrationStart(int calid);

	/**
	 * Inserts the given {@link Calibration} into the database.
	 * 
	 * @param calibration the {@link Calibration} to insert.
	 */
	void insertCalibration(Calibration calibration);

	ResultWrapper publishCertForBatchedCal(int calId, HttpSession session);

	void placeCalibrationOnHold(Calibration cal, Contact completedBy, Map<Integer, Integer> timeSpents,
			Map<Integer, Integer> calLinkOutcomes, String remark);

	/**
	 * Returns all {@link Calibration} results (as a {@link PagedResultSet}) of a
	 * complex query customised by the user on the given
	 * {@link CalibrationSearchForm} object.
	 * 
	 * @param form the {@link CalibrationSearchForm} holding the query criteria.
	 * @param prs  the {@link PagedResultSet} before the query.
	 * @return the {@link PagedResultSet} after the query.
	 */
	PagedResultSet<CalibrationDTO> searchCalibrations(CalibrationSearchForm form, PagedResultSet<CalibrationDTO> prs,
			Subdiv allocatedSubdiv, Locale locale);

	/**
	 * Sets the CWMS procedure with the given xindiceKey to complete, so that a
	 * certificate can be generated for the {@link Calibration}.
	 * 
	 * @param xindiceKey the CWMS procedure xindiceKey.
	 */
	void setProcedureComplete(String xindiceKey);

	/**
	 * Sets the {@link Calibration} as started by updating the status and recording
	 * the current time. Also records the {@link Contact} that started the
	 * {@link Calibration}
	 * 
	 * @param cal       the {@link Calibration}
	 * @param startedBy the {@link Contact} beginning the calibration
	 */
	void startCalibration(Calibration cal, Contact startedBy, Date startDate, String remark);

	/**
	 * Updates the given {@link Calibration} in the database.
	 * 
	 * @param calibration the {@link Calibration} entity.
	 */
	void updateCalibration(Calibration calibration);

	Calibration findLastCompletedCalibrationForJobitem(int jobitemid);

	Calibration findLastCalibrationForJobitem(Integer jobitemid);

	void resumeCalibration(Calibration cal, Contact contact, Date startDate, Date endDate,
						   CalibrationStatus onGoingStatus);

	List<ImportedOperationItem> convertFileContentToImportedCalibrationDTO(Integer exchangeFormatId,
																		   ArrayList<LinkedCaseInsensitiveMap<String>> fileContent, Integer subdivid) throws ParseException;

	ImportedOperationItem detectInstrumentImportedCalibration(ImportedOperationItem dto, Integer subdivid);

	List<List<ImportedOperationItem>> groupImportedOperations(List<ImportedOperationItem> dtos);

	ItemActivity getCalibrationTypeItemActivity(boolean onSite, boolean preCal);

	Calibration createCalibrationForTLM(Subdiv businessSubdiv, List<JobItemWorkRequirement> jobItemWorkRequirements,
			CalibrationClass calClass, CalibrationProcess calProcess, Date startDate, Date calDate);

	List<Calibration> importOnsiteOperations(List<List<ImportedOperationItem>> groups, Integer allocatedSubdivId,
                                             Integer crcId, Locale locale, CalibrationProcess calProcess) throws Exception;

	Calibration registerCalibrationTime(Calibration calibration, Date startDate, Integer timeSpent, Date endDate,
			Contact technician, String remarks, CalibrationStatus onHoldCalStatus, CalibrationStatus onGoingCalStatus,
			ItemActivity postOnSiteCalibrationActivity, ItemActivity preOnsiteCalibrationActivity);

	void automaticContractReview(Contact crc, Locale locale, JobItem ji, Date startDate, Date endDate, String comment);

	void noOperationPerformed(Integer jobItemId, String remark, Integer aoid, Date startDate, Integer time);

	void insertAndStartCalibration(Calibration calibration, Contact user, Date startDate, String remark);

	void completeCalibrationForTLM(Calibration calibration, Contact contact, Date calibrationEndDate,
								   Date calibrationDate, ActionOutcome actionOutcome, LocalDate nextCalDate, boolean certificateIssued);

	Pair<Calibration, Certificate> startCalibrationForTLM(List<JobItemWorkRequirement> jobItemWorkRequirements,
			Contact startedBy, CalibrationProcess calProcess, CalibrationClass calClass,
			String certificateSupplementaryFor, Date startDate, Date calDate, String comment);
	
	PagedResultSet<CalibrationDTO> getCalibrationsPerformed(Integer plantId, Date startCalDate,
			Date endCalDate, Integer allocatedCompanyId, Integer allocatedSubdivId, Locale locale, PagedResultSet<CalibrationDTO> prs);

	List<CalibrationDTO> getCalibrationsPerformed_(Integer plantId, Date startCalDate, Date endCalDate);
}