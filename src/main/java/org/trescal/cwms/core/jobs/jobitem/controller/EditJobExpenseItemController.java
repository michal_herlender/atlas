package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db.JobExpenseItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;

@Controller
@RequestMapping("/editjobexpenseitem.htm")
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class EditJobExpenseItemController {

	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private JobExpenseItemService jobExpenseItemService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private SubdivService subdivService;

	@ModelAttribute("expenseItem")
	public JobExpenseItem getJobExpenseItem(@RequestParam(name = "id", required = true) Integer id) {
		return jobExpenseItemService.get(id);
	}

	@GetMapping
	public String onRequest(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto, Locale locale,
			Model model) {
		List<ServiceType> serviceTypes = serviceTypeService.getAllByDomainType(DomainType.SERVICE);
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		model.addAttribute("models", instrumentModelService.getAll(DomainType.SERVICE, locale, serviceTypes.get(0),
				allocatedSubdiv.getComp()));
		return "trescal/core/jobs/jobitem/jobexpenseitem";
	}

	@PostMapping
	public String onSubmit(@Validated @ModelAttribute("expenseItem") JobExpenseItem expenseItem,
			BindingResult bindingResult) {
		return "";
	}
}