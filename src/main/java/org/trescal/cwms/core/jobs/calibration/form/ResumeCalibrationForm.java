package org.trescal.cwms.core.jobs.calibration.form;

/*
 * Basic form to follow standard validation approach for controller
 */
public class ResumeCalibrationForm {
	private Integer jobItemId;
	private Integer calibrationId;
	private Integer contactId;
	
	public Integer getJobItemId() {
		return jobItemId;
	}
	public Integer getCalibrationId() {
		return calibrationId;
	}
	public Integer getContactId() {
		return contactId;
	}
	public void setJobItemId(Integer jobItemId) {
		this.jobItemId = jobItemId;
	}
	public void setCalibrationId(Integer calibrationId) {
		this.calibrationId = calibrationId;
	}
	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}
}
