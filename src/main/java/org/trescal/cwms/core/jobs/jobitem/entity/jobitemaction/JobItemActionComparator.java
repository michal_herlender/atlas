package org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction;

import java.util.Comparator;

/**
 * Inner comparator class for sorting JobItems when returning collections of
 * jobs, each with multiple items
 * 
 * @author jamiev
 */
public class JobItemActionComparator implements Comparator<JobItemAction> {
	// this is only used when making a treeset from the actions, it cannot be
	// used in the job item bean because new actions may need to be added to the
	// set before they have an id

	public int compare(JobItemAction jia1, JobItemAction jia2) {
		// check for null AND for 0 here, because the ID is an Integer (so can
		// be null) but is also sometimes intentionally set to 0 to stop
		// validators complaining, like in the method newCompletedActivity() in
		// JobItemActivityService!

		if (((jia1.getId() != null) && (jia1.getId() != 0)) && ((jia2.getId() != null) && (jia2.getId() != 0))) {
			return jia1.getId().compareTo(jia2.getId());
		} else {
			return jia1.getStartStamp().compareTo(jia2.getStartStamp());
		}
	}
}