package org.trescal.cwms.core.jobs.job.form;

import java.util.List;

import org.trescal.cwms.core.jobs.job.entity.job.Job;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UpdateAllItemsForm
{
	private Integer calTypeId;
	private List<String> instEndRanges;
	private List<String> instPlantNos;
	private List<String> instSerialNos;
	private List<String> instStartRanges;
	private List<Integer> instThreadIds;
	private List<Integer> instUomIds;
	private List<Integer> itemProcIds;
	private List<Integer> itemWorkInstIds;
	private Job job;
	private boolean updateCalType;

}
