package org.trescal.cwms.core.jobs.calibration.dto;

public class NewCalibrationStartedDTO {

	private String redirect;
	private String toolLink;

	public NewCalibrationStartedDTO() {
	}

	public NewCalibrationStartedDTO(String redirect, String toolLink) {
		this.redirect = redirect;
		this.toolLink = toolLink;
	}

	public String getRedirect() {
		return redirect;
	}

	public void setRedirect(String redirect) {
		this.redirect = redirect;
	}

	public String getToolLink() {
		return toolLink;
	}

	public void setToolLink(String toolLink) {
		this.toolLink = toolLink;
	}
}