package org.trescal.cwms.core.jobs.calibration.entity.calibration.comparators;

import java.util.Comparator;

import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

/**
 * sorts calibrations on a batch by job item no
 * 
 * @author stuarth
 */
public class CalibrationJobItemNoComparator implements Comparator<Calibration>
{
	public int compare(Calibration cal1, Calibration cal2)
	{
		if ((cal1.getLinks().size() > 0) && (cal2.getLinks().size() > 0))
		{
			JobItem ji1 = cal1.getLinks().iterator().next().getJi();
			JobItem ji2 = cal2.getLinks().iterator().next().getJi();

			return ((Integer) ji1.getItemNo()).compareTo(ji2.getItemNo());
		}
		else
		{
			if ((cal1.getLinks().size() == 0) && (cal2.getLinks().size() == 0))
			{
				return 0;
			}
			else if (cal1.getLinks().size() == 0)
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}
	}
}