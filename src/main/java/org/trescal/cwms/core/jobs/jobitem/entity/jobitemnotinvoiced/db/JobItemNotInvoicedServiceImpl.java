package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.NotInvoicedReason;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemNotInvoicedDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class JobItemNotInvoicedServiceImpl extends BaseServiceImpl<JobItemNotInvoiced, Integer>
	implements JobItemNotInvoicedService {

	@Autowired
	private JobItemNotInvoicedDao jiniDao;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private InvoiceService invoiceService;

	@Override
	protected BaseDao<JobItemNotInvoiced, Integer> getBaseDao() {
		return jiniDao;
	}

	/*
	 * Updates the existing JobItemNotInvoiced entity attached to a JobItem or
	 * creates a new one if it doesn't exist.
	 */
	@Override
	public JobItemNotInvoiced recordNotInvoiced(JobItem jobItem, NotInvoicedReason reason, String comments,
			int invoiceId, Contact contact) {
		JobItemNotInvoiced jobItemNotInvoiced = jobItem.getNotInvoiced();
		boolean changed = false;
		if (jobItemNotInvoiced == null) {
			jobItemNotInvoiced = new JobItemNotInvoiced();
			changed = true;
		}
		if (comments != null && !comments.equals(jobItemNotInvoiced.getComments())) {
			jobItemNotInvoiced.setComments(comments);
			changed = true;
		}
		if (!reason.equals(jobItemNotInvoiced.getReason())) {
			jobItemNotInvoiced.setReason(reason);
			changed = true;
		}
		int currentinvoiceId = jobItemNotInvoiced.getPeriodicInvoice() == null ? 0
			: jobItemNotInvoiced.getPeriodicInvoice().getId();
		if ((invoiceId == 0) && (currentinvoiceId != 0)) {
			jobItemNotInvoiced.setPeriodicInvoice(null);
			changed = true;
		} else if ((invoiceId != 0) && (currentinvoiceId != invoiceId)) {
			Invoice invoice = this.invoiceService.findInvoice(invoiceId);
			jobItemNotInvoiced.setPeriodicInvoice(invoice);
			changed = true;
		}

		if (changed) {
			jobItemNotInvoiced.setContact(contact);
			jobItemNotInvoiced.setDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			jobItemNotInvoiced.setJobItem(jobItem);
			this.jiniDao.persist(jobItemNotInvoiced);
			jobItem.setNotInvoiced(jobItemNotInvoiced);
		}
		return jobItemNotInvoiced;
	}

	@Override
	public void createNotInvoicedItems(Integer invoiceId, List<Integer> jobitemids, NotInvoicedReason reason,
			String comments, Contact contact) {
		Invoice invoice = null;
		LocalDate today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		if (invoiceId != null && invoiceId > 0)
			invoice = invoiceService.findInvoice(invoiceId);
		// Using eager fetching, to check whether record already created
		List<JobItem> jobItems = this.jobItemService.getJobItemsByIds(jobitemids);

		for (JobItem jobItem : jobItems) {
			boolean newNotInvoiced = false;
			JobItemNotInvoiced jobItemNotInvoiced = jobItem.getNotInvoiced();
			if (jobItemNotInvoiced == null) {
				jobItemNotInvoiced = new JobItemNotInvoiced();
				jobItemNotInvoiced.setJobItem(jobItem);
				newNotInvoiced = true;
			}
			jobItemNotInvoiced.setComments(comments);
			jobItemNotInvoiced.setContact(contact);
			jobItemNotInvoiced.setDate(today);
			jobItemNotInvoiced.setPeriodicInvoice(invoice);
			jobItemNotInvoiced.setReason(reason);
			if (newNotInvoiced) {
				this.save(jobItemNotInvoiced);
				jobItem.setNotInvoiced(jobItemNotInvoiced);
			}
		}
	}

	@Override
	public List<JobItemNotInvoicedDTO> getProjectionDTOs(Collection<Integer> invoiceIds) {
		List<JobItemNotInvoicedDTO> result = Collections.emptyList();
		if ((invoiceIds != null) && !invoiceIds.isEmpty()) {
			result = this.jiniDao.getProjectionDTOs(invoiceIds);
		}
		return result;
	}

	@Override
	public void unlinkJobItemFromPeriodicInvoice(Integer invoiceId, List<Integer> items, Locale locale) {
		List<JobItemNotInvoicedDTO> result;
		if (invoiceId != null) {

			result = this.jiniDao.getProjectionDTOs(Collections.singleton(invoiceId));

			List<Integer> jobItemsNotInvoicedIds = result.stream().map(JobItemNotInvoicedDTO::getJobItemId)
				.collect(Collectors.toList());

			for (Integer id : jobItemsNotInvoicedIds) {
				if (!items.contains(id)) {

					JobItem ji = this.jobItemService.findJobItem(id);

					JobItemNotInvoiced jobItemNotInvoiced = ji.getNotInvoiced();
					if (jobItemNotInvoiced != null) {

						ji.setNotInvoiced(null);
						this.delete(this.get(jobItemNotInvoiced.getId()));
					}
				}
			}

		}
	}

}