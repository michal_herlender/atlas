package org.trescal.cwms.core.jobs.calibration.entity.calreq;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.CalibrationPointSet;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationrange.CalibrationRange;

//@MappedSuperclass
@Entity
@Table(name = "calreq")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public abstract class CalReq extends Versioned
{
	private boolean active;
	private Contact deactivatedBy;
	private Date deactivatedOn;
	private int id;
	private CalibrationPointSet pointSet;
	private String privateInstructions;
	private String publicInstructions;
	private boolean publish;
	private CalibrationRange range;
	private String translatedSource;
	
	public CalReq()
	{
		this.active = true;
		this.publish = true;
	}

	@Transient
	public abstract String getClassKey();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deactivatedbyid", nullable = true)
	public Contact getDeactivatedBy()
	{
		return this.deactivatedBy;
	}

	@Column(name = "deactivatedon", columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDeactivatedOn()
	{
		return this.deactivatedOn;
	}

	@Transient
	public int getGenericId()
	{
		return 0;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Transient
	public Date getLastModifiedOnDate()
	{
		return this.getLastModified();
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pointsetid", nullable = true)
	public CalibrationPointSet getPointSet()
	{
		return this.pointSet;
	}

	@Length(max = 2000, message = "Private instructions must be at most 2000 characters long")
	@Column(name = "instructions", columnDefinition="nvarchar(2000)")
	public String getPrivateInstructions()
	{
		return this.privateInstructions;
	}

	@Length(max = 2000, message = "Public instructions must be at most 2000 characters long")
	@Column(name = "publicinstructions", columnDefinition="nvarchar(2000)")
	public String getPublicInstructions()
	{
		return this.publicInstructions;
	}

	/**
	 * 2021-02-22 moved field from old "rangeid" (modelrange) to new "calibrationrangeid" (calibrationrange)
	 * Constraint is removed on rangeid to modelrange to enable migration
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "calibrationrangeid", nullable = true, foreignKey=@ForeignKey(name="FK_calreq_calibrationrange"))
	public CalibrationRange getRange()
	{
		return this.range;
	}

	@Transient
	public abstract String getSource();
	
	@Transient
	public String getTranslatedSource() {
		return translatedSource; 
	}
	
	@Transient
	public abstract String getSourceParameter();
	
	@Transient
	public String getType()
	{
		if ((this.id == 0) || (this.pointSet != null))
		{
			return "pointset";
		}
		else if (this.range != null)
		{
			return "range";
		}
		else
		{
			return "textonly";
		}
	}

	@Column(name = "active", nullable=true, columnDefinition="tinyint")
	public boolean isActive()
	{
		return this.active;
	}

	@Column(name = "publish", nullable = false, unique = false, columnDefinition="bit")
	public boolean isPublish()
	{
		return this.publish;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public void setDeactivatedBy(Contact deactivatedBy)
	{
		this.deactivatedBy = deactivatedBy;
	}

	public void setDeactivatedOn(Date deactivatedOn)
	{
		this.deactivatedOn = deactivatedOn;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setPointSet(CalibrationPointSet pointSet)
	{
		this.pointSet = pointSet;
	}

	public void setPrivateInstructions(String privateInstructions)
	{
		this.privateInstructions = privateInstructions;
	}

	public void setPublicInstructions(String publicInstructions)
	{
		this.publicInstructions = publicInstructions;
	}

	public void setPublish(boolean publish)
	{
		this.publish = publish;
	}

	public void setRange(CalibrationRange range)
	{
		this.range = range;
	}
	
	public void setTranslatedSource(String translatedSource) {
		this.translatedSource = translatedSource;
	}
}