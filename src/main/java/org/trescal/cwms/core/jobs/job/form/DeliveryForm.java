package org.trescal.cwms.core.jobs.job.form;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DeliveryForm {
	

	public DeliveryForm() {
	}
	
	public DeliveryForm(Integer numberOfPackages, String packageType, String storageArea, Integer jobId) {
		super();
		this.numberOfPackages = numberOfPackages;
		this.packageType = packageType;
		this.storageArea = storageArea;
		this.jobId = jobId;
	}
	private  Integer  numberOfPackages;
	private  String packageType;
	private  String storageArea;
	
	private  Integer jobId;
	
}
