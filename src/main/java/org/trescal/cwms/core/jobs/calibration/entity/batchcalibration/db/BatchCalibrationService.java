package org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.db;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.enums.BatchCalibrationInvoiceType;
import org.trescal.cwms.core.jobs.calibration.form.NewBatchCalibrationForm;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.login.entity.user.User;

import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.List;

public interface BatchCalibrationService
{
	BatchCalibration addCalibrationsToBatch(NewBatchCalibrationForm form, Subdiv allocatedSubdiv);

	/**
	 * Checks the items that are to be invoiced for the {@link BatchCalibration}
	 * (s) and if the system setting allows only one invoice per item, warns the
	 * user of any items that have already been invoiced.
	 * 
	 * @param batchIds the IDs of the {@link BatchCalibration}s to invoice
	 * @param invType the {@link BatchCalibrationInvoiceType} being used
	 * @return the {@link ResultWrapper}
	 */
	ResultWrapper ajaxCheckItemsToInvoice(Integer[] batchIds, BatchCalibrationInvoiceType invType);

	boolean batchCalibrationCanBeDeleted(BatchCalibration batchCal);

	boolean batchHasStartedCalibrations(BatchCalibration batchCal);

	/**
	 * Creates a {@link BatchCalibration} using the details from the given
	 * {@link NewBatchCalibrationForm} and persists it in the database
	 * 
	 * @param form the {@link NewBatchCalibrationForm}
	 * @return the newly-created {@link BatchCalibration}
	 */
	BatchCalibration createBatchCalibration(NewBatchCalibrationForm form, Subdiv allocatedSubdiv);
	
	/**
	 * Deletes the given {@link BatchCalibration} from the database.
	 * 
	 * @param batchcalibration the {@link BatchCalibration} to delete.
	 */
	void deleteBatchCalibration(BatchCalibration batchcalibration);

	ResultWrapper deleteBatchCalibrationWithChecks(int id);

	ResultWrapper deleteCalibrationFromBatch(int calId, HttpSession session);

	void despatchBatchCalibrations(List<Integer> batchIds, CourierDespatchType cdt, 
			String consignmentNo, Contact creator, Subdiv allocatedSubdiv, List<String> sessionNewFileList) throws Exception;

	/**
	 * Returns the {@link BatchCalibration} with the given ID.
	 * 
	 * @param id the {@link BatchCalibration} ID.
	 * @return the {@link BatchCalibration}
	 */
	BatchCalibration findBatchCalibration(int id);

	/**
	 * Returns a {@link List} of all {@link BatchCalibration}s in the database.
	 * 
	 * @return the {@link List} of {@link BatchCalibration}s
	 */
	List<BatchCalibration> getAllBatchCalibrations();

	/**
	 * Returns a list of incomplete {@link Calibration} in the
	 * {@link BatchCalibration} with the given ID. This method returns all
	 * {@link Calibration}s that are Awaiting Work.
	 * 
	 * @param batchId the {@link BatchCalibration} ID.
	 * @param currentContactId the ID of the logged-in {@link Contact}
	 * @return list {@link Calibration}
	 */
	List<Calibration> getAllNewCalibrationsInBatch(int batchId, int currentContactId);

	List<BatchCalibration> getBatchCalibrationsWithIds(List<Integer> batchIds);

	List<JobItem> getDistinctItemsFromBatches(List<Integer> batchIds);

	List<JobItem> getItemsAlreadyInvoiced(List<Integer> batchIds, BatchCalibrationInvoiceType invType);
	
	/**
	 * Returns the next incomplete {@link Calibration} in the
	 * {@link BatchCalibration} with the given ID. This method prioritises
	 * On-going {@link Calibration}s and only if there are no On-going ones will
	 * it return the next {@link Calibration} that is Awaiting Work.
	 * 
	 * @param batchId the {@link BatchCalibration} ID.
	 * @param currentContactId the ID of the logged-in {@link Contact}
	 * @return the next incomplete {@link Calibration}
	 */
	Calibration getNextIncompleteCalibrationInBatch(int batchId, int currentContactId);

	List<BatchCalibration> getOtherCompleteBatchesOnJob(int jobId, int batchId);

	/**
	 * Inserts the given {@link BatchCalibration} into the database.
	 * 
	 * @param batchcalibration the {@link BatchCalibration} to insert.
	 */
	void insertBatchCalibration(BatchCalibration batchcalibration);

	void mergeBatchCalibration(BatchCalibration batchcalibration);

	/**
	 * this method orders the batch calibration by serial number if boolean
	 * passed is true, else the calibrations are ordered by the default of job
	 * item number.
	 * 
	 * @param bySerial indicates if we wish to order by serial no
	 * @param batchId id of the batch calibration to be re-ordered
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper orderCalibrationsOnBatch(boolean bySerial, int batchId);

	HashSet<Integer> removeItemsNotCalibratedFromInvoice(HashSet<Integer> jobitems);

	/**
	 * Updates the given {@link BatchCalibration} in the database if it already
	 * exists, otherwise inserts it.
	 *
	 * @param batchcalibration the {@link BatchCalibration} to insert or update.
	 */
	void saveOrUpdateBatchCalibration(BatchCalibration batchcalibration);

	/**
	 * Sets the boolean 'chooseNext' attribute of the {@link BatchCalibration}
	 * with the given ID to the value given. This indicates whether or not the
	 * {@link User} should be taken back to the {@link BatchCalibration} page to
	 * manually select the next {@link Calibration} to perform between
	 * {@link Calibration}s or not.
	 * 
	 * @param batchId the {@link BatchCalibration} ID.
	 * @param chooseNext the new value of the 'chooseNext' attribute.
	 */
	void setChooseNext(int batchId, boolean chooseNext);

	/**
	 * Updates the given {@link BatchCalibration} in the database.
	 * 
	 * @param batchcalibration the {@link BatchCalibration} to update.
	 */
	void updateBatchCalibration(BatchCalibration batchcalibration);
}