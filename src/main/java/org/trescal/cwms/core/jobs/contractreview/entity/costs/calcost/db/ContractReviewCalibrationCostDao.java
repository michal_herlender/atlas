package org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.db;

import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.dto.ContractReviewCalibrationCostDTO;

public interface ContractReviewCalibrationCostDao extends BaseDao<ContractReviewCalibrationCost, Integer> {
	
	ContractReviewCalibrationCostDTO findContractReviewCalibrationCost(Integer id, Integer servicetypeid, Locale locale);
	
}