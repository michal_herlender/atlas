package org.trescal.cwms.core.jobs.job.entity.poorigin;

import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("blank")
public class POOriginBlank extends POOrigin<PurchaseOrder> {

    public POOriginBlank() {
        super(null);
    }

    @Override
    @Transient
    public KindName getKindName() {
        return KindName.BLANK;
    }


}
