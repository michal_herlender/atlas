package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobExpenseItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.NotInvoicedReason;
import org.trescal.cwms.core.jobs.jobitem.projection.JobExpenseItemNotInvoicedDTO;

public interface ExpenseItemNotInvoicedService extends BaseService<JobExpenseItemNotInvoiced, Integer> {

	void recordNotInvoiced(JobExpenseItem item, NotInvoicedReason reason, String comments, Integer invoiceId,
			Contact contact);

	void createNotInvoicedItems(Integer invoiceId, List<Integer> expenseItemIds, NotInvoicedReason reason,
			String comments, Contact contact);
	
	List<JobExpenseItemNotInvoicedDTO> getProjectionDTOs(Collection<Integer> invoiceIds, Locale locale);
	
	void unlinkJobExpenseItemFromPeriodicInvoice(Integer invoiceId,List<Integer> expenseItemIds, Locale locale);
}