package org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemand.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemand.JobItemDemand;

@Repository
public class JobItemDemandDaoImpl extends BaseDaoImpl<JobItemDemand, Integer> implements JobItemDemandDao {

	@Override
	protected Class<JobItemDemand> getEntity() {
		return JobItemDemand.class;
	}
}