package org.trescal.cwms.core.jobs.job.entity.jobnote.db;

import java.util.List;

import org.trescal.cwms.core.jobs.job.entity.jobnote.JobNote;

public class JobNoteServiceImpl implements JobNoteService
{
	JobNoteDao JobNoteDao;

	public void deleteJobNote(JobNote JobNote)
	{
		this.JobNoteDao.remove(JobNote);
	}

	public void deleteJobNoteById(int id)
	{
		this.JobNoteDao.remove(this.JobNoteDao.find(id));
	}

	public JobNote findJobNote(int id)
	{
		return this.JobNoteDao.find(id);
	}

	public List<JobNote> getActiveJobNotesForJob(int jobid)
	{
		return this.JobNoteDao.getActiveJobNotesForJob(jobid);
	}

	public List<JobNote> getAllJobNotes()
	{
		return this.JobNoteDao.findAll();
	}

	public void insertJobNote(JobNote JobNote)
	{
		this.JobNoteDao.persist(JobNote);
	}

	public void saveOrUpdateJobNote(JobNote JobNote)
	{
		this.JobNoteDao.saveOrUpdate(JobNote);
	}

	public void setJobNoteDao(JobNoteDao JobNoteDao)
	{
		this.JobNoteDao = JobNoteDao;
	}

	public void updateJobNote(JobNote JobNote)
	{
		this.JobNoteDao.update(JobNote);
	}
}