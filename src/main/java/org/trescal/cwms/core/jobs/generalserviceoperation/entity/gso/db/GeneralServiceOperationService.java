package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.form.GeneralServiceOperationForm;

public interface GeneralServiceOperationService extends BaseService<GeneralServiceOperation, Integer> {
	
  void startAndSaveGeneralServiceOperation(GeneralServiceOperationForm form, Contact startedBy, Subdiv allocatedSubdiv);
  
  List<GeneralServiceOperation> getByJobItemId(Integer jobitemid);
  
  GeneralServiceOperation getOnGoingByJobItemId(Integer jobitem);
  
  GeneralServiceOperation getOnHoldByJobItemId(Integer jobitem);
  
  void completeGeneralServiceOperation(GeneralServiceOperationForm form, Contact completedBy);
  
  void cancelGeneralServiceOperation(GeneralServiceOperationForm form, Contact cancelededBy);
  
  void uploadGeneralServiceOperationDocument(GeneralServiceOperationForm form, Subdiv allocatedSubdiv);

  void resumeGeneralServiceOperation(GeneralServiceOperation gso, Contact resumedBy);

  GeneralServiceOperation getLastGSOByJobitemId(Integer jobitemid);
  
  void eraseIncompleteGsoActivity(GeneralServiceOperation gso, Contact erasedBy);
  
  GeneralServiceOperation getLastInstrumentGSO(Instrument instrument);
}
