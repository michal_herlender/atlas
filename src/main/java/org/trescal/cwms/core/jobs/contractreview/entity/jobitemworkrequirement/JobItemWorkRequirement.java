package org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

/**
 * Entity joining a {@link JobItem} to a specific {@link WorkRequirement}.
 * 
 * @author Richard
 */
@Entity
@Table(name = "jobitemworkrequirement", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "jobitemid", "reqid" }) })
public class JobItemWorkRequirement extends Auditable {
	private Integer id;
	private JobItem jobitem;
	private WorkRequirement workRequirement;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getId() {
		return this.id;
	}

	// Cascade managed by job item
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid", nullable = false)
	public JobItem getJobitem() {
		return this.jobitem;
	}
	
	// Cascade managed by JobItemWorkRequirement (allows auto persist of WorkRequirement)
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "reqid", nullable = false)
	public WorkRequirement getWorkRequirement() {
		return this.workRequirement;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public void setJobitem(JobItem jobitem) {
		this.jobitem = jobitem;
	}

	public void setWorkRequirement(WorkRequirement workRequirement) {
		this.workRequirement = workRequirement;
	}
	
}
