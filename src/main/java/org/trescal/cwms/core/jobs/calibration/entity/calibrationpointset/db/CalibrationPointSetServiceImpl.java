package org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.CalibrationPointSet;

@Service("CalibrationPointSetService")
public class CalibrationPointSetServiceImpl implements CalibrationPointSetService
{
	@Autowired
	private CalibrationPointSetDao CalibrationPointSetDao;
	
	public void deleteCalibrationPointSet(CalibrationPointSet calibrationpointset)
	{
		this.CalibrationPointSetDao.remove(calibrationpointset);
	}

	public CalibrationPointSet findCalibrationPointSet(int id)
	{
		return this.CalibrationPointSetDao.find(id);
	}

	public List<CalibrationPointSet> getAllCalibrationPointSets()
	{
		return this.CalibrationPointSetDao.findAll();
	}

	public void insertCalibrationPointSet(CalibrationPointSet CalibrationPointSet)
	{
		this.CalibrationPointSetDao.persist(CalibrationPointSet);
	}

	public void saveOrUpdateCalibrationPointSet(CalibrationPointSet calibrationpointset)
	{
		this.CalibrationPointSetDao.saveOrUpdate(calibrationpointset);
	}

	public void setCalibrationPointSetDao(CalibrationPointSetDao CalibrationPointSetDao)
	{
		this.CalibrationPointSetDao = CalibrationPointSetDao;
	}

	public void updateCalibrationPointSet(CalibrationPointSet CalibrationPointSet)
	{
		this.CalibrationPointSetDao.update(CalibrationPointSet);
	}
}