package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink;

import java.util.Comparator;

public class GsoLinkComparator implements Comparator<GsoLink>
{
	@Override
	public int compare(GsoLink o1, GsoLink o2)
	{
		if (compareByJobItemID(o1, o2) == 0) return compareByID(o1, o2);

		if (o1.getJi().getJob().getJobid() == o2.getJi().getJob().getJobid())
		{
			return ((Integer) o1.getJi().getItemNo()).compareTo(o2.getJi().getItemNo());
		}
		else
		{
			return ((Integer) o1.getJi().getJob().getJobid()).compareTo(o2.getJi().getJob().getJobid());
		}
	}
	private int compareByID(GsoLink o1, GsoLink o2) {
		return o1.getId() - o2.getId();
	}
	private int compareByJobItemID(GsoLink o1, GsoLink o2) {
		return o1.getJi().getJobItemId() - o2.getJi().getJobItemId();
	}
}
