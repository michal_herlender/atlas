package org.trescal.cwms.core.jobs.job.dto;

import java.time.LocalDate;

public class JobSearchResultWrapper {
    private String clientref;
    private int coid;
    private String company;
    private boolean companyOnstop;
    private String contact;
    private boolean contactActive;
    private LocalDate dateIn;
    private Integer jobid;
    private String jobno;
    private int personid;
    private String po;

    public String getClientref() {
        return this.clientref;
    }

	public int getCoid()
	{
		return this.coid;
	}

    public String getCompany() {
        return this.company;
    }

    public String getContact() {
        return this.contact;
    }

    public LocalDate getDatein() {
        return this.dateIn;
    }

    public Integer getJobid() {
        return this.jobid;
    }

    public String getJobno()
	{
		return this.jobno;
	}

	public int getPersonid()
	{
		return this.personid;
	}

	public String getPo()
	{
		return this.po;
	}

	public boolean isCompanyOnstop()
	{
		return this.companyOnstop;
	}

	public boolean isContactActive()
	{
		return this.contactActive;
	}

	public void setClientref(String clientref)
	{
		this.clientref = clientref;
	}

	public void setCoid(int coid)
	{
		this.coid = coid;
	}

	public void setCompany(String company)
	{
		this.company = company;
	}

	public void setCompanyOnstop(boolean companyOnstop)
	{
		this.companyOnstop = companyOnstop;
	}

    public void setContact(String contact) {
        this.contact = contact;
    }

    public void setContactActive(boolean contactActive) {
        this.contactActive = contactActive;
    }

    public void setDatein(LocalDate datein) {
        this.dateIn = datein;
    }

    public void setJobid(Integer jobid) {
        this.jobid = jobid;
    }

    public void setJobno(String jobno)
	{
		this.jobno = jobno;
	}

	public void setPersonid(int personid)
	{
		this.personid = personid;
	}

	public void setPo(String po)
	{
		this.po = po;
	}

}
