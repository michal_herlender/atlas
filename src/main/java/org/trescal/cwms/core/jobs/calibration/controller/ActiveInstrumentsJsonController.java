package org.trescal.cwms.core.jobs.calibration.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.system.Constants;

import lombok.AllArgsConstructor;
import lombok.Data;

@Controller
@JsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE })
public class ActiveInstrumentsJsonController {
	
	@Autowired
	private JobService jobService;

	/**
	 * @param : plantIds list of instruments plant ids
	 * @return : map of plant id and active job id if it exists
	 */
	@RequestMapping(value = "/activeInstrumentsJsonController.json", method = RequestMethod.POST)
	@ResponseBody
	public List<ActiveInstrumentDto> getJobs(@RequestBody List<String> plantIds) {
		return jobService.findActiveJobsByPlantids(plantIds);
	}
	
	@Data
	@AllArgsConstructor
	public static class ActiveInstrumentDto {
		private Integer plantId;
		private String plantNo;
		private String serialNo;
		private Integer jobitemNo;
		private Integer jobitemId;
		private String jobNo;
		private Integer jobId;
	}

}
