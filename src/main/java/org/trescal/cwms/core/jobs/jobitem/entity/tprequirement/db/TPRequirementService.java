package org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.db;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirementDTO;

public interface TPRequirementService extends BaseService<TPRequirement, Integer>
{
	ResultWrapper ajaxUpdateTPRequirement(int id, boolean inv, boolean cal, boolean rep, boolean adj, HttpSession session);
	List<TPRequirementDTO> getProjectionDTOsForIds(Collection<Integer> requirementIds);
}