package org.trescal.cwms.core.jobs.jobitem.dto;

import java.util.List;

import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;

import lombok.Getter;
import lombok.Setter;

/**
 * Used by job item controllers 
 */
@Getter @Setter
public class JobItemViewWrapper {

	private JobItem jobItem;
//	private List<JobItemDTO> itemsOnJob;
	private List<JobItemProjectionDTO> itemsOnJob;
	private Integer previousItemId;
	private Integer nextItemId;
	private List<JobItemWorkRequirement> workRequirements;
	private boolean genericModel;
	private boolean readyForClientDelivery;
	private boolean readyForInternalDelivery;
	private boolean readyForInternalReturnDelivery;
	private boolean readyForThirdPartyDelivery;
	private boolean awaitingInternalPO;
	private boolean readyForCalibration;
	private boolean readyToResumeCalibration;
	private boolean readyForGeneralServiceOperation;
	private boolean readyToResumeGeneralServiceOperation;
	private boolean readyForGeneralServiceOperationDocumentUpload;
	private boolean awaitingClientReceiptConfirmation;
	
}