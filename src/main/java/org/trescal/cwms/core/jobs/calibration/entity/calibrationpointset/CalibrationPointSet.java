package org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.SortComparator;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.CalibrationPoint;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.PointComparator;
import org.trescal.cwms.core.jobs.calibration.entity.pointset.PointSet;

/**
 * 2018-02-04 GB : Formerly had empty used references to Instrument and JobItem; removed.
 * This entity is just referenced by CalReq in the current design.
 *
 */
@Entity
@Table(name = "calibrationpointset")
public class CalibrationPointSet extends PointSet
{
	private Set<CalibrationPoint> points;

	@OneToMany(mappedBy = "pointSet", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(PointComparator.class)
	public Set<CalibrationPoint> getPoints()
	{
		return this.points;
	}

	@Transient
	public boolean isRelational()
	{
		if ((this.points != null) && (this.points.size() > 0))
		{
			for (CalibrationPoint point : this.points)
			{
				if (point.getRelatedPoint() != null)
				{
					return true;
				}
			}
		}

		return false;
	}

	public void setPoints(Set<CalibrationPoint> points)
	{
		this.points = points;
	}
}
