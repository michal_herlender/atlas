package org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost;

@Repository("ContractReviewAdjustmentCostDao")
public class ContractReviewAdjustmentCostDaoImpl extends BaseDaoImpl<ContractReviewAdjustmentCost, Integer> implements ContractReviewAdjustmentCostDao {
	
	@Override
	protected Class<ContractReviewAdjustmentCost> getEntity() {
		return ContractReviewAdjustmentCost.class;
	}
}