package org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationProcessKeyValue;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;

public interface CalibrationProcessService extends BaseService<CalibrationProcess, Integer> {

	CalibrationProcess findByName(String name);

	List<CalibrationProcessKeyValue> findAllKeyValue();
}