package org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.dto;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ContractReviewCalibrationCostDTO {
	
	private Integer id;
	private Integer jobItemId;
	private Integer itmeno;
	private String serviceType;
	private String currencyERSymbol;
	private BigDecimal finalCost;
	
	public ContractReviewCalibrationCostDTO(Integer id, Integer jobItemId, Integer itmeno, String serviceType,
			String currencyERSymbol, BigDecimal finalCost) {
		super();
		this.id = id;
		this.jobItemId = jobItemId;
		this.itmeno = itmeno;
		this.serviceType = serviceType;
		this.currencyERSymbol = currencyERSymbol;
		this.finalCost = finalCost;
	}


}
