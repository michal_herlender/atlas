package org.trescal.cwms.core.jobs.jobitem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ItemAccessoryDTO {

	private Integer itemAccessoryId;
	private Integer jobItemId;
	private Integer accessoryId;
	private String accessoryName;
	private Integer quantity;
}