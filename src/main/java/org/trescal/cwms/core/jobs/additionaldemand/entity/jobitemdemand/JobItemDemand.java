package org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemand;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemand;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemandStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Entity
@Table(name = "jobitemdemand", uniqueConstraints = @UniqueConstraint(columnNames = { "jobItemId",
		"additionalDemand" }, name = "UK_jobitemdemand_jobitem_additionaldemand"))
public class JobItemDemand {

	private Integer id;
	private JobItem jobItem;
	private AdditionalDemand additionalDemand;
	private AdditionalDemandStatus additionalDemandStatus;
	private String comment;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "jobItemId", nullable = false, foreignKey = @ForeignKey(name = "FK_jobitemdemand_jobitem"))
	public JobItem getJobItem() {
		return jobItem;
	}

	public void setJobItem(JobItem jobItem) {
		this.jobItem = jobItem;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "additionalDemand", nullable = false)
	public AdditionalDemand getAdditionalDemand() {
		return additionalDemand;
	}

	public void setAdditionalDemand(AdditionalDemand additionalDemand) {
		this.additionalDemand = additionalDemand;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "demandStatus", nullable = false)
	public AdditionalDemandStatus getAdditionalDemandStatus() {
		return additionalDemandStatus;
	}

	public void setAdditionalDemandStatus(AdditionalDemandStatus additionalDemandStatus) {
		this.additionalDemandStatus = additionalDemandStatus;
	}

	@Column(name = "comment")
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}