package org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "faultreportinstruction")
public class FaultReportInstruction extends FaultReportDetail
{
	private Boolean showOnTPDelNote;

	@NotNull
	@Column(name = "showontpdelnote", nullable=false, columnDefinition="bit")
	public Boolean isShowOnTPDelNote()
	{
		return this.showOnTPDelNote;
	}

	public void setShowOnTPDelNote(Boolean showOnTPDelNote)
	{
		this.showOnTPDelNote = showOnTPDelNote;
	}
}