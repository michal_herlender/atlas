package org.trescal.cwms.core.jobs.calibration.entity.standardused.db;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumentPercentageService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsedComparator;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsedDto;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@Service("StandardUsedService")
public class StandardUsedServiceImpl extends BaseServiceImpl<StandardUsed, Integer> implements StandardUsedService {

	@Autowired
	private CalibrationService calServ;
	@Autowired
	private InstrumService instServ;
    @Autowired
    private StandardUsedDao standardUsedDao;
    @Autowired
    private SupportedLocaleService localeService;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private CertificateService certServ;
    @Autowired
    private InstrumentPercentageService instrumentPercentageService;

    public static String MESSAGE_CODE_UNKNOWN = "unknown";

    @Override
    protected BaseDao<StandardUsed, Integer> getBaseDao() {
        return standardUsedDao;
    }

    public ResultWrapper ajaxAddStandardUsedOnCalibration(int plantId, int calId) {
        ResultWrapper rw = this.instServ.findInstrumAsStandard(plantId);
		if (!rw.isSuccess() || (rw.getResults() == null)) {
			return rw;
		}
		Calibration cal = this.calServ.findCalibration(calId);
		if (cal == null) {
			return new ResultWrapper(false,
					"There was an error adding the standard barcode " + plantId + " to the given calibration.");
		}
		Instrument inst = (Instrument) rw.getResults();
		for (StandardUsed su : cal.getStandardsUsed()) {
			if (su.getInst().getPlantid() == plantId) {
				return new ResultWrapper(false,
						"The standard barcode " + plantId + " is already recorded on the given calibration.");
			}
		}
		StandardUsed su = new StandardUsed();
		su.setCalibration(cal);
		su.setInst(inst);
		// store data as a snapshot for how long this instrument had left in
		// cal at the time of being used for the calibration
        su.setNextCalDueDate(inst.getNextCalDueDate());
        su.setCalPercentage(instrumentPercentageService.totalCalibrationPercentage(inst.getNextCalDueDate(), inst.getCalFrequencyUnit(), inst.getCalFrequency()));
        this.save(su);
		StandardUsedDto dto = new StandardUsedDto(su.getId(), inst.getPlantid(), inst.getSerialno(), 
				inst.getPlantno(), inst.getDefinitiveInstrument());
		return new ResultWrapper(true, null, dto, null);
	}

	public ResultWrapper ajaxDeleteStandardUsedOnCalibration(int suId) {
		StandardUsed su = this.get(suId);
		if (su != null) {
			this.delete(su);
			return new ResultWrapper(true, null);
		} else {
			return new ResultWrapper(false,
					"Could not find the specified standard record, it may have already been deleted.");
		}
	}

	@Override
	public String ajaxGetStandardsUsedAsTabbedString(int calId) {

		// find all standards used on the calibration
		List<StandardUsed> stds = this.getAllStandardsUsedOnCal(calId);

		StringBuilder str = new StringBuilder();

		// iterate over each standard used and add the tab-separated data,
		// adding a new line character at the end of each instrument
		for (StandardUsed std : stds) {
			String plantNo = std.getInst().getPlantno();
			String desc = InstModelTools.modelNameViaTranslations(std.getInst().getModel(),
				LocaleContextHolder.getLocale(), localeService.getPrimaryLocale());
			String recallDate = (std.getNextCalDueDate() != null) ? std.getNextCalDueDate().format(DateTimeFormatter.ISO_DATE) : "";
			str.append(plantNo).append("\t").append(desc).append("\t").append(recallDate).append("\n");
		}

		// add empty line at the end so that if these are copied into a table on
		// a Word document, it is obvious where the last line is
		str.append("\n\n");

		return str.toString();
	}

	public List<StandardUsed> ajaxGetStandardsUsedForCal(int calId) {
		return this.getAllStandardsUsedOnCal(calId);
	}

	public List<StandardUsed> createStandardsUsed(Integer[] instrums, Calibration cal) {
		List<StandardUsed> stds = new ArrayList<>();

		if (cal.getStandardsUsed() == null) {
			cal.setStandardsUsed(new TreeSet<>(new StandardUsedComparator()));
		}

		for (Integer i : instrums) {
			Instrument inst = this.instServ.get(i);

			StandardUsed su = new StandardUsed();
			su.setInst(inst);
			su.setCalibration(cal);

			// store data as a snapshot for how long this instrument had left in
			// cal at the time of being used for the calibration
            su.setNextCalDueDate(inst.getNextCalDueDate());
            su.setCalPercentage(instrumentPercentageService.totalCalibrationPercentage(inst.getNextCalDueDate(), inst.getCalFrequencyUnit(), inst.getCalFrequency()));

			cal.getStandardsUsed().add(su);
			// this.insertStandardUsed(su);
			stds.add(su);
		}

		return stds;
	}

	public List<StandardUsed> getAllStandardsUsedOnCal(int calId) {
		return this.standardUsedDao.getAllStandardsUsedOnCal(calId);
	}

	public void setCalServ(CalibrationService calServ) {
		this.calServ = calServ;
	}

	public void setInstServ(InstrumService instServ) {
		this.instServ = instServ;
	}

	public void setStandardUsedDao(StandardUsedDao standardUsedDao) {
		this.standardUsedDao = standardUsedDao;
	}

	@Override
	public List<StandardUsedDto> getStandardsUsedForJobItemAjax(Integer jobItemId) {
		List<StandardUsed> standardsUsed = standardUsedDao.getAllStandardsUsedOnJobItem(jobItemId);
		List<StandardUsedDto> dtos = new ArrayList<>();
		for (StandardUsed standardUsed : standardsUsed) {
			StandardUsedDto dto = new StandardUsedDto();
			dto.setPlantId(standardUsed.getInst().getPlantid());
			dto.setPlantNumber(standardUsed.getInst().getPlantno());
			dto.setSerialNumber(standardUsed.getInst().getSerialno());
			dto.setTranslatedModelName(translationService.getCorrectTranslation(
					standardUsed.getInst().getModel().getNameTranslations(), LocaleContextHolder.getLocale()));
			if (standardUsed.getNextCalDueDate() != null) {
				LocalDate calDateAtTimeOfUse = standardUsed.getNextCalDueDate();
				dto.setCalDueDateAtTimeOfUse(
					calDateAtTimeOfUse.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT))
				);
				dto.setInCalAtTimeOfUse(
					standardUsed.getCalibration().getCalDate().isBefore(standardUsed.getNextCalDueDate()));
			} else {
				dto.setCalDueDateAtTimeOfUse(messageSource.getMessage(MESSAGE_CODE_UNKNOWN, null, "Unknown",
					LocaleContextHolder.getLocale()));
				dto.setInCalAtTimeOfUse(false);
			}
			LocalDate dateAtTimeOfUse = standardUsed.getCalibration().getCalDate();
			dto.setDateOfUse(dateAtTimeOfUse.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
			Certificate cert = certServ.getMostRecentCertForInstrument(standardUsed.getInst(),
				dateFromLocalDate(standardUsed.getCalibration().getCalDate()));
			if (cert != null) {
				dto.setCertId(cert.getCertid());
				certServ.setCertificateDirectory(cert);
				dto.setFileAvailable(cert.getHasCertFile());
			}
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public Boolean showStandardsUsedButtonForJobItem(JobItem jobItem) {
		return standardUsedDao.standardsUsedOnJobItem(jobItem.getJobItemId());
	}
}