package org.trescal.cwms.core.jobs.repair.dto.form;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.jobs.repair.dto.RepairTimeDTO;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class RepairTimeForm {

	private String action;
	private Integer itemToDeleteId;
	private Integer jobItemId;
	private Integer repairCompletionReportId;
	private String repairCompletionReportIdentifier;
	private Integer technicianId;
	private String technicianName;
	private Date currentDate = new Date();
	private List<RepairTimeDTO> repairTimes;

}