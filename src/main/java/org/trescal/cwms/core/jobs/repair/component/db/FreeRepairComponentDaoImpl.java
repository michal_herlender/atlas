package org.trescal.cwms.core.jobs.repair.component.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.repair.component.FreeRepairComponent;

@Repository("FreeRepairComponentDao")
public class FreeRepairComponentDaoImpl extends BaseDaoImpl<FreeRepairComponent, Integer> implements FreeRepairComponentDao {
	
	@Override
	protected Class<FreeRepairComponent> getEntity() {
		return FreeRepairComponent.class;
	}

}