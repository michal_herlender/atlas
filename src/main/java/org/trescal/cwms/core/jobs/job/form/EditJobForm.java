package org.trescal.cwms.core.jobs.job.form;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;

import lombok.Data;

@Data
public class EditJobForm {

	@NotNull
	private Integer jobid;
	@NotNull(message = "{error.value.notselected}")
	private JobType jobType;
	@NotNull
	@Min(value = 1, message = "{error.value.notselected}")
	private Integer jobContactId;
	// Mandatory selection for regular jobs
	@NotNull(groups = { InhouseJobType.class })
	@Min(value = 1, message = "{error.value.notselected}", groups = { InhouseJobType.class })
	private Integer bookedInBusinessAddressId;
	private Integer bookedInBusinessLocationId;
    // Mandatory selection for onsite jobs
    @NotNull(groups = {OnsiteJobType.class})
    @Min(value = 1, message = "{error.value.notselected}", groups = {OnsiteJobType.class})
    private Integer bookedInClientAddressId;
    private Integer bookedInClientLocationId;
    @NotNull
    @Min(value = 1, message = "{error.value.notselected}")
    private Integer returnToAddressId;
    private Integer returnToLocationId;
    @NotNull(message = "{error.value.notselected}")
    private LocalDateTime receiptDate;
    private LocalDateTime pickupDate;
    @NotNull
    private Boolean overrideDateInOnJobItems;
    // Optional
    private LocalDate agreedDeliveryDate;
    // Optional
    @Length(max = 30)
    private String clientReference;
    @NotNull(message = "{error.value.notselected}")
    private String currencyCode;
    private Integer transportInId;
	private Boolean copyTransportIn;
	private Integer transportOutId;
	private Boolean copyTransportOut;
	private BigDecimal estCarriageOut;
	private Integer businessSubdivContactId;
	private  Date currentDate = new Date();

	public interface OnsiteJobType {
	}

	public interface InhouseJobType {
	}

}