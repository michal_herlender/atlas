package org.trescal.cwms.core.jobs.jobitem.form;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class WorkRequirementCatalogForm {
	
	public static final String ACTION_CREATE = "create";
	public static final String ACTION_SEARCH = "search";
	
	private String action;
	private Integer jobItemId; 
	private Integer serviceTypeId;
	// Selections used for subdivision
	private Boolean selectSubdivision;
	private Integer coid;
	private Integer subdivid;
	// Selections used for search only
	private Boolean includeAllResults;
	private Boolean includeInstrumentModel;
	private Boolean includeGenericCapabilities;
	private Integer subfamilyId;
	private String subfamilyName;
	// Selections used for create only
	private Integer addCount;
	private Integer procId;
	private String requirementText;
	
	public static interface CreateGroup {};
	public static interface SearchGroupSubfamily {};

	@NotNull
	public String getAction() {
		return action;
	}

	@NotNull
	public Integer getJobItemId() {
		return jobItemId;
	}

	@NotNull
	@Min(value=1, message="{error.value.notselected}")
	public Integer getServiceTypeId() {
		return serviceTypeId;
	}

	@NotNull
	public Boolean getSelectSubdivision() {
		return selectSubdivision;
	}

	@NotNull
	@Min(value=1, message="{error.value.notselected}")
	public Integer getCoid() {
		return coid;
	}

	@NotNull
	@Min(value=1, message="{error.value.notselected}")
	public Integer getSubdivid() {
		return subdivid;
	}

	@NotNull
	public Boolean getIncludeAllResults() {
		return includeAllResults;
	}

	@NotNull
	public Boolean getIncludeInstrumentModel() {
		return includeInstrumentModel;
	}

	@NotNull
	public Boolean getIncludeGenericCapabilities() {
		return includeGenericCapabilities;
	}

	/**
	 * Subfamily must be specified, unless "generic capability search" is selected 
	 */
	@NotNull
	@Min(value=1, message="{error.value.notselected}", groups={SearchGroupSubfamily.class})
	public Integer getSubfamilyId() {
		return subfamilyId;
	}

	public String getSubfamilyName() {
		return subfamilyName;
	}

	@NotNull(message="{error.value.notselected}", groups={CreateGroup.class})
	@Min(value=1, groups={CreateGroup.class})
	@Max(value=99, groups={CreateGroup.class})
	public Integer getAddCount() {
		return addCount;
	}

	@NotNull(message="{error.value.notselected}", groups={CreateGroup.class})
	public Integer getProcId() {
		return procId;
	}

	@NotNull(groups={CreateGroup.class})
	@Length(min=0, max=500, groups={CreateGroup.class})
	public String getRequirementText() {
		return requirementText;
	}

	public void setServiceTypeId(Integer serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	public void setIncludeAllResults(Boolean includeAllResults) {
		this.includeAllResults = includeAllResults;
	}

	public void setIncludeInstrumentModel(Boolean includeInstrumentModel) {
		this.includeInstrumentModel = includeInstrumentModel;
	}

	public void setAddCount(Integer addCount) {
		this.addCount = addCount;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setJobItemId(Integer jobItemId) {
		this.jobItemId = jobItemId;
	}

	public void setSelectSubdivision(Boolean selectSubdivision) {
		this.selectSubdivision = selectSubdivision;
	}

	public void setCoid(Integer companyId) {
		this.coid = companyId;
	}

	public void setSubdivid(Integer subdivId) {
		this.subdivid = subdivId;
	}

	public void setProcId(Integer procId) {
		this.procId = procId;
	}

	public void setRequirementText(String requirementText) {
		this.requirementText = requirementText;
	}

	public void setIncludeGenericCapabilities(Boolean includeGenericCapabilities) {
		this.includeGenericCapabilities = includeGenericCapabilities;
	}

	public void setSubfamilyId(Integer subfamilyId) {
		this.subfamilyId = subfamilyId;
	}

	public void setSubfamilyName(String subfamilyName) {
		this.subfamilyName = subfamilyName;
	}

}
