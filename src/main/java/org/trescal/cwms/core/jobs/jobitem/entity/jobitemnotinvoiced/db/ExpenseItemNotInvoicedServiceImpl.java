package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db.JobExpenseItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobExpenseItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.NotInvoicedReason;
import org.trescal.cwms.core.jobs.jobitem.projection.JobExpenseItemNotInvoicedDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class ExpenseItemNotInvoicedServiceImpl extends BaseServiceImpl<JobExpenseItemNotInvoiced, Integer>
	implements ExpenseItemNotInvoicedService {

	@Autowired
	private ExpenseItemNotInvoicedDao expenseItemNotInvoicedDao;
	@Autowired
	private JobExpenseItemService jobExpenseItemService;
	@Autowired
	private InvoiceService invoiceService;

	@Override
	protected BaseDao<JobExpenseItemNotInvoiced, Integer> getBaseDao() {
		return expenseItemNotInvoicedDao;
	}

	@Override
	public void recordNotInvoiced(JobExpenseItem item, NotInvoicedReason reason, String comments, Integer invoiceId,
			Contact contact) {
		if (item.getNotInvoiced() == null || !item.getNotInvoiced().getComments().equals(comments)
			|| item.getNotInvoiced().getReason() != reason
			|| item.getNotInvoiced().getPeriodicInvoice() == null && invoiceId != null && invoiceId != 0
			|| item.getNotInvoiced().getPeriodicInvoice() != null && (invoiceId == null || invoiceId == 0)) {
			JobExpenseItemNotInvoiced itemNotInvoiced = item.getNotInvoiced();
			boolean newItem = itemNotInvoiced == null;
			if (newItem)
				itemNotInvoiced = new JobExpenseItemNotInvoiced();
			itemNotInvoiced.setReason(reason);
			itemNotInvoiced.setComments(comments);
			if (invoiceId != null && invoiceId != 0)
				itemNotInvoiced.setPeriodicInvoice(invoiceService.findInvoice(invoiceId));
			itemNotInvoiced.setContact(contact);
			itemNotInvoiced.setDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			itemNotInvoiced.setExpenseItem(item);
			item.setNotInvoiced(itemNotInvoiced);
			// merge is not possible for a new itemNotInvoiced here, because of
			// a Hibernate bug (HHH-13063)
			if (newItem)
				this.save(itemNotInvoiced);
			else
				this.merge(itemNotInvoiced);
		}
	}

	@Override
	public void createNotInvoicedItems(Integer invoiceId, List<Integer> expenseItemIds, NotInvoicedReason reason,
			String comments, Contact contact) {
		Invoice invoice = null;
		LocalDate today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		if (invoiceId != null && invoiceId > 0)
			invoice = invoiceService.findInvoice(invoiceId);
		// Using eager fetching, to check whether record already created
		if (expenseItemIds != null && !expenseItemIds.isEmpty()) {
			List<JobExpenseItem> jobExpenseItems = expenseItemIds.stream().map(jobExpenseItemService::get)
				.collect(Collectors.toList());
			for (JobExpenseItem jobExpenseItem : jobExpenseItems) {
				boolean newNotInvoiced = false;
				JobExpenseItemNotInvoiced expenseItemNotInvoiced = jobExpenseItem.getNotInvoiced();
				if (expenseItemNotInvoiced == null) {
					expenseItemNotInvoiced = new JobExpenseItemNotInvoiced();
					expenseItemNotInvoiced.setExpenseItem(jobExpenseItem);
					newNotInvoiced = true;
				}
				expenseItemNotInvoiced.setComments(comments);
				expenseItemNotInvoiced.setContact(contact);
				expenseItemNotInvoiced.setDate(today);
				expenseItemNotInvoiced.setPeriodicInvoice(invoice);
				expenseItemNotInvoiced.setReason(reason);
				if (newNotInvoiced) {
					this.save(expenseItemNotInvoiced);
					jobExpenseItem.setNotInvoiced(expenseItemNotInvoiced);
				}
			}
		}
	}

	@Override
	public List<JobExpenseItemNotInvoicedDTO> getProjectionDTOs(Collection<Integer> invoiceIds, Locale locale) {
		List<JobExpenseItemNotInvoicedDTO> result = Collections.emptyList();
		if (invoiceIds != null && !invoiceIds.isEmpty()) {
			result = this.expenseItemNotInvoicedDao.getProjectionDTOs(invoiceIds, locale);
		}
		return result;
	}

	@Override
	public void unlinkJobExpenseItemFromPeriodicInvoice(Integer invoiceId, List<Integer> expenseItemIds,
			Locale locale) {
		List<JobExpenseItemNotInvoicedDTO> result;
		if (invoiceId != null) {
			result = this.expenseItemNotInvoicedDao.getProjectionDTOs(Collections.singleton(invoiceId), locale);
			List<Integer> expenseItemsNotInvoicedIds = result.stream().map(jeini -> jeini.getExpenseItem().getId())
				.collect(Collectors.toList());

			for (Integer id : expenseItemsNotInvoicedIds) {
				if (!expenseItemIds.contains(id)) {
					this.delete(this.get(id));
				}
			}
		}
	}
}