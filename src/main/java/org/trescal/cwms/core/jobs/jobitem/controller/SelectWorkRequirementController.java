package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateReplacementType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db.JobItemWorkRequirementService;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db.WorkRequirementService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV,
		Constants.HTTPSESS_NEW_FILE_LIST })
public class SelectWorkRequirementController {
	
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private WorkRequirementService wrService;
	@Autowired
	private JobItemService jiService;
	@Autowired
	private JobItemWorkRequirementService jobItemWorkRequirementService;
	
	private void referenceData(String jiWrIds, Integer certId, Integer jobitemId, Model model) {
		String[] jiWrIdsTab = jiWrIds.split(",");
		List<WorkRequirement> wrs = new ArrayList<>();
		if(!jiWrIds.isEmpty()){
			for (String id : jiWrIdsTab) {
				JobItemWorkRequirement jiWr = this.jobItemWorkRequirementService.get(Integer.valueOf(id));
				if(jiWr != null) 
					wrs.add(jiWr.getWorkRequirement());
			}
		}
		Certificate cert = this.certificateService.findCertificate(certId);
		model.addAttribute("jobitemid", jobitemId);
		model.addAttribute("certificateid", cert.getCertid());
		model.addAttribute("certificateno", cert.getCertno());
		model.addAttribute("workrequirements", wrs);
	}

	@RequestMapping(value = "/selectworkrequirement.htm", method = RequestMethod.GET)
	public String doGet(@RequestParam("jiwrids") String jiWrIds, @RequestParam("certificateid") Integer certificateId
			, @RequestParam("jobitemid") Integer jobitemId, Model model) {
		referenceData(jiWrIds, certificateId, jobitemId, model);
		return "trescal/core/jobs/jobitem/selectworkrequirement";
	}
	
	@RequestMapping(value = "/selectworkrequirement.htm", method = RequestMethod.POST)
	public ResultWrapper doPost(@RequestParam("wrid") Integer wrId, @RequestParam("certificateid") Integer certificateId,
			@RequestParam("jobitemid") Integer jobitemId) {

		WorkRequirement wr = this.wrService.get(wrId);
		JobItem ji = this.jiService.findJobItem(jobitemId);
		Certificate cert = this.certificateService.findCertificate(certificateId);
		CertificateReplacementType certType = ji.getJob().getType().equals(JobType.STANDARD) ? CertificateReplacementType.LAB_CERT :
			  CertificateReplacementType.ONSITE_CERT;
		if(wr != null){
			this.certificateService.performCertificateReplacement(ji, wr, cert, certType);
			return new ResultWrapper(true, null);
		}
		
		return new ResultWrapper(false, null);
	}
}