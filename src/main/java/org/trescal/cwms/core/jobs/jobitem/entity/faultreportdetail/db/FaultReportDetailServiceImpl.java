package org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.db;

import java.util.Date;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportAction;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportDescription;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportDetail;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportInstruction;
import org.trescal.cwms.core.jobs.jobitem.form.genericentityvalidator.FaultReportDetailValidator;
import org.trescal.cwms.core.tools.AuthenticationService;

@Service("FaultReportDetailService")
public class FaultReportDetailServiceImpl implements FaultReportDetailService
{
	@Autowired
	private AuthenticationService authServ;
	@Autowired
	private ContactService contServ;
	@Autowired
	private FaultReportActionDao actionDao;
	@Autowired
	private FaultReportDescriptionDao descriptionDao;
	@Autowired
	private FaultReportInstructionDao instructionDao;
	@Autowired
	private FaultReportService frServ;
	@Autowired
	private FaultReportDetailValidator validator;

	public static final String TYPE_ACTION = "action";
	public static final String TYPE_DESCRIPTION = "description";
	public static final String TYPE_INSTRUCTION = "instruction"; 
	
	public ResultWrapper ajaxFindFaultReportDetail(String type, int id)
	{
		FaultReportDetail frd = this.getFaultReportDetail(type, id);
		return new ResultWrapper(true, null, frd, null);
	}

	public ResultWrapper deactivateFaultReportDetail(String type, int faultRepDetailId, int personId,int subdivId)
	{
		FaultReportDetail frd = this.getFaultReportDetail(type, faultRepDetailId);

		// allow anybody with authority over the original author to edit his/her
		// comments
		if (this.authServ.allowPrivileges(personId, frd.getRecordedBy().getPersonid(),subdivId))
		{
			frd.setActive(false);
			
			this.updateFaultReportDetail(type, frd);

			return new ResultWrapper(true, null, frd, null);
		}
		else
		{
			return new ResultWrapper(false, "You do not have permission to de-activate "
					+ frd.getRecordedBy().getName()
					+ "'s fault report comments");
		}
	}

	public ResultWrapper editFaultReportDetail(String type, int faultRepDetailId, int personId, String value)
	{
		FaultReportDetail frd = this.getFaultReportDetail(type, faultRepDetailId);

		// only allow the original author to edit his/her comments, unless this
		// is a fault report instruction (in which case anyone can edit)
		if ((frd.getRecordedBy().getPersonid() == personId)
				|| TYPE_INSTRUCTION.equalsIgnoreCase(type))
		{
			try
			{
				FaultReportDetail temp = frd.getClass().newInstance();
				BeanUtils.copyProperties(frd, temp);

				temp.setDescription(value);
				temp.setLastModified(new Date());

				if (TYPE_INSTRUCTION.equalsIgnoreCase(type))
				{
					Contact contact = this.contServ.get(personId);
					temp.setRecordedBy(contact);
				}
				
				BindException errors = new BindException(temp, "frdetail");
				this.validator.validate(temp, errors);

				if (!errors.hasErrors())
				{
					BeanUtils.copyProperties(temp, frd);
					this.updateFaultReportDetail(type, frd);
				}

				return new ResultWrapper(errors, frd);
			}
			catch (Exception e)
			{
				return new ResultWrapper(false, "An error occurred when attempting to edit this fault report comment");
			}

		}
		else
		{
			return new ResultWrapper(false, "Only "
					+ frd.getRecordedBy().getName()
					+ " may edit his/her fault report comments");
		}
	}
	
	// Created for compatibility with existing code / DWR syntax
	private FaultReportDetail getFaultReportDetail(String type, int id) {
		if (type == null) {
			throw new IllegalArgumentException("Type was null");
		}
		else if (TYPE_DESCRIPTION.equalsIgnoreCase(type)) {
			return this.descriptionDao.find(id);
		}
		else if (TYPE_ACTION.equalsIgnoreCase(type)) {
			return this.actionDao.find(id);
		}
		else if (TYPE_INSTRUCTION.equalsIgnoreCase(type)) {
			return this.instructionDao.find(id);
		}
		else {
			throw new IllegalArgumentException(type);
		}
	}
	
	// Created for compatibility with existing code / DWR syntax
	private void updateFaultReportDetail(String type, FaultReportDetail frd) {
		if (type == null) {
			throw new IllegalArgumentException("Type was null");
		}
		else if (TYPE_DESCRIPTION.equalsIgnoreCase(type)) {
			this.descriptionDao.update((FaultReportDescription) frd);
		}
		else if (TYPE_ACTION.equalsIgnoreCase(type)) {
			this.actionDao.update((FaultReportAction) frd);
		}
		else if (TYPE_INSTRUCTION.equalsIgnoreCase(type)) {
			this.instructionDao.update((FaultReportInstruction) frd);
		}
		else {
			throw new IllegalArgumentException(type);
		}
	}
	
	@Override
	public ResultWrapper insertFaultReportAction(int faultRepId, int personId, String value)
	{
		FaultReport faultRep = this.frServ.findFaultReport(faultRepId);
		Contact contact = this.contServ.get(personId);

		FaultReportAction frAction = new FaultReportAction();
		frAction.setFaultRep(faultRep);
		frAction.setRecordedBy(contact);
		frAction.setLastModified(new Date());
		frAction.setDescription(value);
		
		BindException errors = new BindException(frAction, "frdetail");
		this.validator.validate(frAction, null);

		if (!errors.hasErrors())
		{
			this.actionDao.persist(frAction);
		}

		return new ResultWrapper(errors, frAction);
	}

	@Override
	public ResultWrapper insertFaultReportDescription(int faultRepId, int personId, String value)
	{
		FaultReport faultRep = this.frServ.findFaultReport(faultRepId);
		Contact contact = this.contServ.get(personId);

		FaultReportDescription frDesc = new FaultReportDescription();
		frDesc.setFaultRep(faultRep);
		frDesc.setRecordedBy(contact);
		frDesc.setLastModified(new Date());
		frDesc.setDescription(value);

		BindException errors = new BindException(frDesc, "frdetail");
		this.validator.validate(frDesc, null);

		if (!errors.hasErrors())
		{
			this.descriptionDao.persist(frDesc);
		}

		return new ResultWrapper(errors, frDesc);
	}

	@Override
	public ResultWrapper insertFaultReportInstruction(int faultRepId, int personId, String value)
	{
		FaultReport faultRep = this.frServ.findFaultReport(faultRepId);
		Contact contact = this.contServ.get(personId);

		FaultReportInstruction frInst = new FaultReportInstruction();
		frInst.setFaultRep(faultRep);
		frInst.setRecordedBy(contact);
		frInst.setLastModified(new Date());
		frInst.setDescription(value);
		frInst.setShowOnTPDelNote(true);

		BindException errors = new BindException(frInst, "frdetail");
		this.validator.validate(frInst, null);

		if (!errors.hasErrors())
		{
			this.instructionDao.persist(frInst);
		}

		return new ResultWrapper(errors, frInst);
	}

	public ResultWrapper updateFaultReportInstructionShowOnTPDN(int faultRepInstId, boolean showOnTPDN)
	{
		FaultReportInstruction frInst = this.instructionDao.find(faultRepInstId);

		if (frInst != null)
		{
			frInst.setShowOnTPDelNote(showOnTPDN);
			this.instructionDao.persist(frInst);

			return new ResultWrapper(true, "", frInst, null);
		}
		else
		{
			return new ResultWrapper(false, "The fault report instruction could not be found", null, null);
		}
	}
}