package org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.db;

import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;

public interface ContractReviewPurchaseCostService
{
	/**
	 * Create empty zero valued cost 
	 * @return
	 */
	ContractReviewPurchaseCost createEmptyCost(JobItem jobItem, boolean active);

	/**
	 * Creates a copy of the {@link Cost} and assigns it to the given
	 * {@link JobItem}. The cost must be a {@link ContractReview} {@link Cost}.
	 * 
	 * @param oldCost the {@link Cost} to copy from.
	 * @param copyToJobitem the {@link JobItem} to copy into.
	 * @return the new {@link Cost}.
	 */
	ContractReviewPurchaseCost copyContractReviewCost(ContractReviewPurchaseCost oldCost, JobItem copyToJobitem);

	ContractReviewPurchaseCost findContractReviewPurchaseCost(int id);
}