package org.trescal.cwms.core.jobs.job.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PurchaseOrderComponentDTO {

	private List<ClientPurchaseOrderDTO> pos;
	private Integer jobDefaultPO;
	private List<BpoDTO> bpos;
	private Integer jobDefaultBPO;
	private List<BpoDTO> availableBPOs;

	// User Rights
	private boolean createPO;
	private boolean addRemoveItems;
	private boolean editPO;
	private boolean deactivatePO;
	private boolean removeBPO;
}