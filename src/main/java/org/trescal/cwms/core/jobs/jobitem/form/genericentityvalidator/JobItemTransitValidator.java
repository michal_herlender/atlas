package org.trescal.cwms.core.jobs.jobitem.form.genericentityvalidator;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.JobItemTransit;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.core.validation.AbstractEntityValidator;

/**
 * Generic validator for validating {@link JobItemTransit} entities. Extends
 * {@link AbstractEntityValidator} to provide support for dwr based validation
 * which requires access to the resulting {@link BindException}.
 * 
 * @author jamiev
 */
public class JobItemTransitValidator extends AbstractBeanValidator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(JobItemTransit.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		JobItemTransit jit = (JobItemTransit) target;

		super.validate(jit, errors);

		// don't allow 'despatched' if item has not been received (i.e. if
		// status is still 'in transit')
		if ((jit.getJobItem().getState().getStateid() == 10)
				&& (jit.getActivity().getStateid() == 1))
		{
			errors.rejectValue("activity", null, "Cannot despatch an item whilst it is still in transit.");
		}

	}
}
