package org.trescal.cwms.core.jobs.jobitem.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.deliverynote.entity.transportmethod.db.TransportMethodService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.db.JobItemActivityService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.db.JobItemTransitService;
import org.trescal.cwms.core.jobs.jobitem.form.JIActivitiesForm;
import org.trescal.cwms.core.jobs.jobitem.form.JIActivitiesValidator;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.db.RepairCompletionReportService;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.core.workflow.dto.ItemStateDTO;
import org.trescal.cwms.core.workflow.entity.holdstatus.HoldStatus;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.nextactivity.NextActivity;
import org.trescal.cwms.core.workflow.entity.nextactivity.db.NextActivityService;
import org.trescal.cwms.core.workflow.entity.outcomestatus.db.OutcomeStatusService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.db.StateGroupLinkService;
import org.trescal.cwms.core.workflow.entity.workstatus.WorkStatus;
import org.trescal.cwms.spring.model.KeyValue;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class JIActivitiesController extends JobItemController {
	private static final Logger logger = LoggerFactory.getLogger(JIActivitiesController.class);

	@Value("#{props['cwms.config.calibrations.completemultiple']}")
	private String completeMultipleCalibrations;
	@Autowired
	private JobItemActionService actionServ;
	@Autowired
	private CalibrationService calServ;
	@Autowired
	private ItemStateService itemStateServ;
	@Autowired
	private JobItemActivityService jiaServ;
	@Autowired
	private JobItemTransitService jobItemTransitService;
	@Autowired
	private NextActivityService nextActServ;
	@Autowired
	private OutcomeStatusService outcomeStatusServ;
	@Autowired
	private TransportMethodService transMethServ;
	@Autowired
	private JIActivitiesValidator validator;
	@Autowired
	private AuthenticationService authServ;
	@Autowired
	private RepairInspectionReportService rirService;
	@Autowired
	private RepairCompletionReportService rcrService;
	@Autowired
	private StateGroupLinkService stateGroupLinkService;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
	}

	@ModelAttribute("form")
	protected JIActivitiesForm formBackingObject(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@RequestParam(value = "jobitemid") Integer jobitemid, @ModelAttribute("message") String message)
			throws Exception {
		JIActivitiesForm form = new JIActivitiesForm();
		JobItem ji = this.jobItemService.findJobItem(jobitemid);
		if ((ji == null) || (jobitemid == 0)) {
			logger.error("Jobitem with id " + jobitemid + " could not be found");
			throw new Exception("Unable to locate requested jobitem");
		}
		form.setJi(ji);
		Contact con = userService.get(username).getCon();
		form.setCanDeleteLastActivity(
				this.actionServ.canDeleteLastAction(form.getJi().getJobItemId(), con, subdivDto.getKey()));
		// Set the last action here (in FBO) so that if the user tries to
		// delete the last activity, this will be the one deleted - i.e. no
		// new activities added by other users in other sessions will be
		// deleted!
		JobItemAction lastActiveAction = this.actionServ.findLastActiveActionForItem(form.getJi());
		form.setLastActionId(lastActiveAction != null ? lastActiveAction.getId() : 0);
		JobItemActivity incompleteAct = this.jiaServ.getIncompleteActivityForItem(ji);
		if (incompleteAct != null) {
			form.setTimeSpent(DateTools.getMinutesDifference(incompleteAct.getStartStamp(), new Date()));
		} else {
			form.setTimeSpent(5);
		}
		form.setMessage(message);
		return form;
	}

	@PreAuthorize("@hasJobItemAuthorityOnConnectedSubdiv.check('JI_ACTION')")
	@RequestMapping(value = "/jiactions.htm", method = RequestMethod.POST, params = { "nooperationperformed" })
	protected ModelAndView onSubmitNoActionRequired(@RequestParam(value = "jobitemid") Integer jobItemId,
			@RequestParam(value = "aoid") Integer aoid,
			@RequestParam(value = "activityremark", required = false) String activityremark) {
		this.calServ.noOperationPerformed(jobItemId, activityremark, aoid, null, null);
		return new ModelAndView(new RedirectView("jiactions.htm?jobitemid=" + jobItemId));
	}

	@PreAuthorize("@hasJobItemAuthorityOnConnectedSubdiv.check('JI_ACTION')")
	@RequestMapping(value = "/jiactions.htm", method = RequestMethod.POST, params = { "!nooperationperformed" })
	protected ModelAndView onSubmit(@RequestParam(value = "jobitemid") Integer jobItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@Valid @ModelAttribute("form") JIActivitiesForm form, BindingResult result, Locale locale,
			RedirectAttributes redirectAttributes) throws Exception {
		if (result.hasErrors())
			return referenceData(jobItemId, username, subdivDto, locale);
		else {
			if (form.getOverrideItemIds() == null)
				form.setOverrideItemIds(new ArrayList<>());
			JobItem ji = form.getJi();
			switch (form.getSubmitAction()) {
			case "delete":
				ResultWrapper resWrap = this.actionServ.deleteJobItemActionById(form.getLastActionId(), username,
						subdivDto.getKey());
				redirectAttributes.addFlashAttribute("message", resWrap.getMessage());
				updateRirAfterDeletion(resWrap.isSuccess(), form.getLastActionId(), subdivDto.getKey());
				break;
			/*
			 * TRANSIT STUFF COMMENTED OUT UNTIL FULLY IMPLEMENTED AND UPDATED
			 * Uncommented for development evaluation 2016-01-27 GB (separated
			 * into method)
			 */
			case "override":
				this.jobItemService.overrideStatusOfItems(form.getOverrideStatusId(), form.getOverrideItemIds(),
						form.getOverrideRemark());
				break;
			case "onhold":
				this.jobItemService.placeItemsOnHold(form.getHoldStatusId(), form.getHoldItemIds(),
						form.getHoldRemark());
				break;
			case "offhold":
				this.jobItemService.takeItemsOffHold(form.getHoldItemIds(), form.getHoldRemark());
				break;
			case "receive":
				// HARDCODED - 90
				this.jobItemTransitService.receiveInTransitItem(ji.getJobItemId(), null, 90, username);
				break;
			default:
				throw new Exception("No submit action detected.");
			}
			return new ModelAndView(new RedirectView("jiactions.htm?jobitemid=" + ji.getJobItemId()));
		}
	}

	@PreAuthorize("hasAuthority('JI_ACTION')")
	@RequestMapping(value = "/jiactions.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@RequestParam(value = "jobitemid") Integer jobItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Locale locale)
			throws Exception {
		Contact contact = this.userService.get(username).getCon();
		Map<String, Object> refData = super.referenceData(jobItemId, contact, subdivDto.getKey(), username);
		JobItem ji = jobItemService.findJobItem(jobItemId);
		// Disabled transit activities, commented out due to poor performance GB
		// 2016-06-08
		refData.put("transportmethods", this.transMethServ.getAllTransportMethods());
		refData.put("isintransit", this.jobItemService.isInTransit(ji.getJobItemId()));
		refData.put("allProgressActivities", this.itemStateServ.getAllProgressActivities(false));
		refData.put("allstatuses", this.itemStateServ.getAllItemStatuses(WorkStatus.class, false));
		refData.put("holdstatuses", this.itemStateServ.getAllItemStatuses(HoldStatus.class, false));
		JobItemActivity incompleteAct = this.jiaServ.getIncompleteActivityForItem(ji);
		if (incompleteAct != null) {
            refData.put("incompleteAct", true);
            refData.put("incompleteActId", incompleteAct.getId());
            refData.put("allowedOutcomes", this.outcomeStatusServ
                .getOutcomeStatusesForActivity(incompleteAct.getActivity().getStateid(), false));
            refData.put("actionOutcomes", incompleteAct.getActivity().getActionOutcomes());
            // get list of items available to multi complete
            refData.put("optionalMultiCompleteItems",
                ji.getCapability() == null ? new ArrayList<Calibration>()
                    : this.calServ.getAllCalibrationsForJobAtStatusUsingProc(ji.getJob().getJobid(), "On-going",
                    ji.getCapability().getId()));
            refData.put("completeMultipleCalibrations", this.completeMultipleCalibrations);
        } else {
			refData.put("incompleteAct", false);
			List<NextActivity> nextActs = this.nextActServ.getNextActivitiesForStatus(ji.getState().getStateid(), true);
			refData.put("allowedActivities", nextActs);
			if (nextActs.size() > 0) {
				refData.put("allowedOutcomes", this.outcomeStatusServ
						.getOutcomeStatusesForActivity(nextActs.get(0).getActivity().getStateid(), false));
				refData.put("actionOutcomes", nextActs.get(0).getActivity().getActionOutcomes());
			}
		}
		User currentUser = userService.get(username);
		if (currentUser != null)
			refData.put("canOverride",
					authServ.hasRight(currentUser.getCon(), Permission.JI_ACTIVITY_OVERRIDE, subdivDto.getKey()));
		// get record repair time stateGroup linked activities
		List<ItemStateDTO> rrtItemStates = this.stateGroupLinkService
				.getLinkedItemStates(StateGroup.RECORD_REPAIR_TIME_BY_TECHNICIAN, locale);
		if (rrtItemStates != null)
			refData.put("rrtItemStates", rrtItemStates.stream().map(ItemStateDTO::getId).collect(Collectors.toList()));
		return new ModelAndView("trescal/core/jobs/jobitem/jiactions", refData);
	}

	private void updateRirAfterDeletion(boolean success, int lastActionId, int allocatedSubdiv) {
		if (success) {
			JobItemAction lastAction = this.actionServ.get(lastActionId);
			if (lastAction != null) {
				if (jobItemService.stateHasGroupOfKeyName(lastAction.getActivity(),
						StateGroup.RIR_TECHNICIAN_VALIDATION)) {
					RepairInspectionReport rir = rirService.getValidatedRirByJobItemAndValidationDate(
							lastAction.getJobItem().getJobItemId(), allocatedSubdiv, true);
					if (rir != null) {
						rir.setCompletedByTechnician(false);
						rir.setCompletedByTechnicianOn(null);
						rir.setTechnician(null);
						rirService.merge(rir);
					}
				} else if (jobItemService.stateHasGroupOfKeyName(lastAction.getActivity(),
						StateGroup.RIR_MANAGER_VALIDATION)) {
					RepairInspectionReport rir = rirService.getValidatedRirByJobItemAndValidationDate(
							lastAction.getJobItem().getJobItemId(), allocatedSubdiv, true);
					if (rir != null) {
						rir.setValidatedByManager(false);
						rir.setValidatedByManagerOn(null);
						rir.setManager(null);
						rirService.merge(rir);
					}
				} else if (jobItemService.stateHasGroupOfKeyName(lastAction.getActivity(), StateGroup.RIR_CSR_REVIEW)) {
					RepairInspectionReport rir = rirService.getValidatedRirByJobItemAndValidationDate(
							lastAction.getJobItem().getJobItemId(), allocatedSubdiv, false);
					if (rir != null) {
						rir.setReviewedByCsr(false);
						rir.setReviewedByCsrOn(null);
						rir.setCsr(null);
						rirService.merge(rir);
					}
				} else if (jobItemService.stateHasGroupOfKeyName(lastAction.getActivity(), StateGroup.RCR_VALIDATION)) {
					RepairCompletionReport rcr = rcrService
							.getValidatedRcrByJobItem(lastAction.getJobItem().getJobItemId());
					if (rcr != null) {
						rcr.setValidated(false);
						rcrService.merge(rcr);
					}
				}
			}
		}
	}
}