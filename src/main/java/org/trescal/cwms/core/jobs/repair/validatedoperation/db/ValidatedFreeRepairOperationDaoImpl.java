package org.trescal.cwms.core.jobs.repair.validatedoperation.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperation;

@Repository("ValidatedFreeRepairOperationDao")
public class ValidatedFreeRepairOperationDaoImpl extends BaseDaoImpl<ValidatedFreeRepairOperation, Integer> implements ValidatedFreeRepairOperationDao {
	
	@Override
	protected Class<ValidatedFreeRepairOperation> getEntity() {
		return ValidatedFreeRepairOperation.class;
	}

}