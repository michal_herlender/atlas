package org.trescal.cwms.core.jobs.jobitem.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.CallOffItem;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.db.CallOffItemService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class CallOffItemsValidator extends AbstractBeanValidator
{
	@Autowired
	private CallOffItemService callOffItemService;
	
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(CallOffItemsForm.class);
	}
	
	public void validate(Object obj, Errors errors)
	{
		CallOffItemsForm form = (CallOffItemsForm) obj;

		super.validate(form, errors);

		if (form.getItemIds() == null || form.getItemIds().isEmpty()) {
			errors.reject("calloffitems.error.noselection", "At least one item must be selected");
		}
		if (!errors.hasErrors()) {
			// Fields present, continue with detailed validation
			// For re-activation, 
			// (a) the job items need to be on the same subdiv as the logged in subdiv
			// (b) the job items should all be on the same job for UI forwarding (TODO - discuss)
			if (form.getAction().equals(CallOffItemsForm.Action.REACTIVATE_ITEMS)) {
				boolean otherSubdiv = false;
				for (Integer callOffItemId : form.getItemIds()) {
					CallOffItem callOffItem = this.callOffItemService.get(callOffItemId);
					if (callOffItem.getOffItem().getJob().getOrganisation().getSubdivid() != form.getAllocatedSubdivId()) {
						otherSubdiv = true;
					}
				}
				if (otherSubdiv) {
					errors.reject("calloffitems.error.othersubdiv", "Call off items may only be re-activated if the job belongs to your current logged-in subdivision");
				}
			}
			// TODO (debating) for a new job, the job items should belong to the same client contact?
		}		
	}
}