package org.trescal.cwms.core.jobs.jobitem.projection;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.trescal.cwms.core.jobs.job.projection.ClientPurchaseOrderProjectionDTO;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.system.entity.servicetype.ServiceTypeDto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class JobExpenseItemProjectionDTO {
	// Below result fields always populated by query
	private Integer id;
	private Integer itemNo;
	private Integer serviceTypeId;
	private Integer modelId;
	private String instrumentModelTranslation;		// From instrumentmodel (translated)
	private BigDecimal singlePrice;
	private Integer quantity;
	private String clientRef;
	private String comment;
	private LocalDate date;
	private Boolean invoiceable;
	private Integer jobId;

	// Optionally initialized after creation
	private ServiceTypeDto serviceType;
	private JobProjectionDTO job;
	// Optional multi-valued relationships
	private List<ClientPurchaseOrderProjectionDTO> expenseItemPos;
	
	/**
	 * Constructor for JPA queries
	 */
	public JobExpenseItemProjectionDTO(Integer id, Integer itemNo, Integer serviceTypeId, Integer modelId, String instrumentModelTranslation, 
			BigDecimal singlePrice, Integer quantity, String clientRef, String comment, LocalDate date, Boolean invoiceable, Integer jobId) {
		super();
		this.id = id;
		this.itemNo = itemNo;
		this.serviceTypeId = serviceTypeId;
		this.modelId = modelId;
		this.instrumentModelTranslation = instrumentModelTranslation;
		this.singlePrice = singlePrice;
		this.quantity = quantity;
		this.clientRef = clientRef;
		this.comment = comment;
		this.date = date;
		this.invoiceable = invoiceable;
		this.jobId = jobId;
	}
	
	public BigDecimal getTotalPrice() {
		return getSinglePrice().multiply(BigDecimal.valueOf(getQuantity()));
	}
}
