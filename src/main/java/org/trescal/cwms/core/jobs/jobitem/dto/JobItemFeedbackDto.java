package org.trescal.cwms.core.jobs.jobitem.dto;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class JobItemFeedbackDto {
    Integer jobItemId;
    Integer  itemNo;
    String serialNo;
    String plantNo;
    String displayColor;
    String name;
    String email;
    Integer personId;
    String modelName;
    String calTypeShortName;
}
