package org.trescal.cwms.core.jobs.repair.repairinspectionreport.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairInspectionReportFormDTO;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.spring.model.KeyValue;

public interface RepairInspectionReportService extends BaseService<RepairInspectionReport, Integer> {
	RepairInspectionReport validateByTechnician(RepairInspectionReportFormDTO form, String username, Integer subdivId);

	void refresh(RepairInspectionReport rir);

	void validateByManager(RepairInspectionReportFormDTO form, String username, Integer subdivId);

	RepairInspectionReport getRirByJobItemIdAndSubdivId(Integer jobitemid, Integer subdivId);

	List<RepairInspectionReport> getAllRirByJobItemId(int jobItemId);
	
	RepairInspectionReport getLatestValidatedRir(int jobItemId);
	
	RepairInspectionReport getValidatedRirByJobItemAndValidationDate(int jobItemId,int allocatedSubdiv, boolean completedByTechnician);
	
	void saveRepairInspection(RepairInspectionReportFormDTO form, String username,
			KeyValue<Integer, String> subdivDto);
	
	Boolean isAccreditedToPerformRirInitialization(JobItemWorkRequirement jiWorkRequirement, Contact currentContact);
	
	Boolean isAccreditedToPerformRirTechCompletion(JobItemWorkRequirement jiWorkRequirement, Contact currentContact);
	
	Boolean isAccreditedToPerformRirManagerValidation(JobItemWorkRequirement jiWorkRequirement, Contact currentContact);

	void reviewByCsr(RepairInspectionReportFormDTO form, String username);

}