package org.trescal.cwms.core.jobs.jobitem.form;

import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class WorkRequirementForm {
	private String action;
	private boolean editable;				// Only able to edit if subdiv is available to us
	private int jobItemId;
	private int wrId;
	private Integer modelId;
	private String procedureId;				// Comes from dropdown control so can be blank
	private String procedureReferenceAndName;
	private Integer coid;
	private Integer subdivId;
	private String workInstructionId;			// Comes from dropdown control so can be blank
	private String workInstructionTitle;
	private Integer serviceTypeId;
	private String requirementText;
	private String type;


}
