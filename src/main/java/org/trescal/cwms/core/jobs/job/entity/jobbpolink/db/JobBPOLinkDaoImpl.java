package org.trescal.cwms.core.jobs.job.entity.jobbpolink.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink_;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;

@Repository("JobBPOLinkDao")
public class JobBPOLinkDaoImpl extends BaseDaoImpl<JobBPOLink, Integer> implements JobBPOLinkDao {

	@Override
	protected Class<JobBPOLink> getEntity() {
		return JobBPOLink.class;
	}

	@Override
	public JobBPOLink findByJobIdAndBpoId(Integer jobId, Integer bpoId) {
		return getFirstResult(cb -> {
			CriteriaQuery<JobBPOLink> q = cb.createQuery(JobBPOLink.class);
			Root<JobBPOLink> root = q.from(JobBPOLink.class);
			Join<JobBPOLink, Job> jobJoin = root.join(JobBPOLink_.job);
			Join<JobBPOLink, BPO> bpoJoin = root.join(JobBPOLink_.bpo);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(jobJoin.get(Job_.jobid), jobId));
			clauses.getExpressions().add(cb.equal(bpoJoin.get(BPO_.poId), bpoId));
			q.where(clauses);
			
			return q;
		}).orElse(null);
	}
}