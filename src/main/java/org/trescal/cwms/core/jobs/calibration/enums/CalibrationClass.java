package org.trescal.cwms.core.jobs.calibration.enums;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

/**
 * @author JamieV
 */
public enum CalibrationClass
{
	AS_FOUND("calibrationclass.asfound", "As Found"), 
	AS_LEFT("calibrationclass.asleft", "As Left"), 
	AS_FOUND_AND_AS_LEFT("calibrationclass.asfoundandasleft", "As Found and As Left");

	private String messageCode;
	private String defaultDesc;
	private MessageSource messageSource;
		
	@Component
	public static class CalibrationClassMessageSourceInjector {
		@Autowired
		private MessageSource messageSource;
		
		@PostConstruct
        public void postConstruct() {
            for (CalibrationClass instance : EnumSet.allOf(CalibrationClass.class))
            	instance.setMessageSource(messageSource);
        }
	}

	private CalibrationClass(String messageCode, String defaultDesc)
	{
		this.messageCode = messageCode;
		this.defaultDesc = defaultDesc;
	}

	public String getDesc()
	{
		Locale locale = LocaleContextHolder.getLocale();
		return messageSource.getMessage(messageCode, null, defaultDesc, locale);
	}
	
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
}
