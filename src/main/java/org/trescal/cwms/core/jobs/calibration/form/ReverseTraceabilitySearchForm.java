package org.trescal.cwms.core.jobs.calibration.form;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.jobs.calibration.dto.CalibrationDTO;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReverseTraceabilitySearchForm {

	@NotNull
	private Integer plantId;
	private Date startCalDate;
	private Date endCalDate;
	private Integer businessSubdivId;
	private Integer businessCompanyId;
	
	// results data
	private PagedResultSet<CalibrationDTO> rs;
	private int pageNo;
	private int resultsPerPage;
	private int resultsTotal;
}
