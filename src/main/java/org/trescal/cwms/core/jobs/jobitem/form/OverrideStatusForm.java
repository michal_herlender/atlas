package org.trescal.cwms.core.jobs.jobitem.form;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class OverrideStatusForm {

	private Integer overrideStatusId;
	private String overrideRemark;
	private List<Integer> overrideItemIds;

}