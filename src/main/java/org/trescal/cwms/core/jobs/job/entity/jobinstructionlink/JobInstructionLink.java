package org.trescal.cwms.core.jobs.job.entity.jobinstructionlink;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionEntity;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLink;

/**
 * An instruction recorded for a {@link Job}.
 */
@Entity
@Table(name = "jobinstructionlink", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "jobid", "instructionid" }) })
public class JobInstructionLink extends Versioned implements InstructionLink<Job> {

	private int id;
	private Job job;
	private Instruction instruction;

	@Transient
	public InstructionEntity getInstructionEntity() {
		return InstructionEntity.JOB;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobid")
	public Job getJob() {
		return this.job;
	}

	@Override
	@Transient
	public Job getEntity() {
		return job;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "instructionid")
	@Cascade(value = CascadeType.ALL)
	@Valid
	public Instruction getInstruction() {
		return instruction;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public void setInstruction(Instruction instruction) {
		this.instruction = instruction;
	}

	/*
	 * All Job Instructions are global, so setting organization (if called) has no
	 * impact
	 */
	@Override
	public void setOrganisation(Company organisation) {
		return;
	}

	/*
	 * All Job Instructions are global
	 */
	@Override
	@Transient
	public Company getOrganisation() {
		return null;
	}
}