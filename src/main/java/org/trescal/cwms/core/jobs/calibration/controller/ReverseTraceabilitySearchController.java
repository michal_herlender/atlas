package org.trescal.cwms.core.jobs.calibration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationDTO;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsedModel;
import org.trescal.cwms.core.jobs.calibration.form.ReverseTraceabilitySearchForm;
import org.trescal.cwms.core.jobs.calibration.form.validator.ReverseTraceabilitySearchFormValidator;
import org.trescal.cwms.core.jobs.calibration.views.ReverseTraceabilityModelGenerator;
import org.trescal.cwms.core.jobs.calibration.views.ReverseTraceabilityXlsxView;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY})
public class ReverseTraceabilitySearchController {

	@Autowired
	private CalibrationService calibrationService;
	@Autowired
	private ReverseTraceabilitySearchFormValidator reverseTraceabilitySearchFormValidator;
	@Autowired
	private ReverseTraceabilityModelGenerator standardUsedModelGenerator;
	@Autowired
	private ReverseTraceabilityXlsxView standardUsedXlsxView;

	public static final String VIEW_NAME = "trescal/core/jobs/calibration/reversetraceabilitysearch";
	public static final String RESULTS_NAME = "trescal/core/jobs/calibration/reversetraceabilitysearchresults";
	public static final String FORM_NAME = "form";

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.setValidator(reverseTraceabilitySearchFormValidator);
	}

	@ModelAttribute(FORM_NAME)
	protected ReverseTraceabilitySearchForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		ReverseTraceabilitySearchForm form = new ReverseTraceabilitySearchForm();
		form.setBusinessCompanyId(companyDto.getKey());
		form.setBusinessSubdivId(subdivDto.getKey());
		form.setResultsPerPage(Constants.RESULTS_PER_PAGE);
		form.setPageNo(1);
		return form;
	}

	@RequestMapping(value = "/reversetraceabilitysearch.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute(FORM_NAME) ReverseTraceabilitySearchForm form) {
		return new ModelAndView(VIEW_NAME);
	}

	@RequestMapping(value = "/reversetraceabilitysearch.htm", method = RequestMethod.POST, params = "!export")
	protected ModelAndView onSubmit(Locale locale,
			@Validated @ModelAttribute(FORM_NAME) ReverseTraceabilitySearchForm form, BindingResult bindingResult)
			throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData(form);
		}
		PagedResultSet<CalibrationDTO> rs = this.calibrationService.getCalibrationsPerformed(form.getPlantId(),
			form.getStartCalDate(), form.getEndCalDate(), form.getBusinessCompanyId(), form.getBusinessSubdivId(),
			locale, new PagedResultSet<>(form.getResultsPerPage(), form.getPageNo()));
		form.setResultsTotal(rs.getResultsCount());
		form.setRs(rs);
		return new ModelAndView(RESULTS_NAME);
	}

	@RequestMapping(value = "/reversetraceabilitysearch.htm", method = RequestMethod.POST, params = "export")
	protected ModelAndView onExport(@ModelAttribute(FORM_NAME) ReverseTraceabilitySearchForm form, Locale locale)
			throws Exception {
		StandardUsedModel model = this.standardUsedModelGenerator.getModel(form.getPlantId(), form.getStartCalDate(),
				form.getEndCalDate(), form.getBusinessCompanyId(), form.getBusinessSubdivId(), form.getResultsTotal(),
				locale);
		Map<String, Object> modelMap = new HashMap<>();
		modelMap.put("model", model);
		return new ModelAndView(standardUsedXlsxView, modelMap);
	}
}
