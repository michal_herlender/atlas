package org.trescal.cwms.core.jobs.job.entity.jobnote.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.job.entity.jobnote.JobNote;

public interface JobNoteDao extends BaseDao<JobNote, Integer> {
	
	List<JobNote> getActiveJobNotesForJob(int jobid);
}