package org.trescal.cwms.core.jobs.repair.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrumentmodel.entity.component.db.ComponentService;
import org.trescal.cwms.core.jobs.repair.dto.ComponentJsonDto;

@Controller
@JsonController
public class ComponentJSONController {

	@Autowired
	private ComponentService componentService;

	public static int MAX_RESULTS = 10;

	@RequestMapping(value = "/searchComponent.json", method = RequestMethod.GET)
	@ResponseBody
	public List<ComponentJsonDto> searchComponent(@RequestParam(name = "keyword", required = true) String keyword) {
		return componentService.searchComponents(keyword, MAX_RESULTS);
	}

}
