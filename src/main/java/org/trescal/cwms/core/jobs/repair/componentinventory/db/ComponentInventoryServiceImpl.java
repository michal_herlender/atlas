package org.trescal.cwms.core.jobs.repair.componentinventory.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.repair.componentinventory.ComponentInventory;

@Service("ComponentInventoryService")
public class ComponentInventoryServiceImpl extends BaseServiceImpl<ComponentInventory, Integer> implements ComponentInventoryService
{
	@Autowired
	private ComponentInventoryDao componentInventoryDao;

	@Override
	protected BaseDao<ComponentInventory, Integer> getBaseDao() {
		return componentInventoryDao;
	}
}