package org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemActionForEmployeeDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemActionProjectionDTO;
import org.trescal.cwms.core.jobs.repair.dto.RepairTimeDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairTimeForm;

public interface JobItemActionService extends BaseService<JobItemAction, Integer>
{
	/**
	 * Indicates whether or not the {@link User} retrieved from the
	 * {@link Contact} with the given ID has permission to delete the last
	 * {@link JobItemAction} on the {@link JobItem} entity.
	 * 
	 * @param jobItemId the {@link JobItem} ID.
	 * @param currentContactId the ID of the {@link Contact} to test
	 * @return true if the {@link User} has permission to delete the last
	 *         {@link JobItemAction} on the {@link JobItem}, false if permission
	 *         is not granted and null if the {@link JobItem} has no
	 *         {@link JobItemAction} objects.
	 */
	Boolean canDeleteLastAction(int jobItemId, Contact contact, int allocatedSubdivId);

	/**
	 * Validates whether the {@link User} can delete the {@link JobItemAction}
	 * with the given ID, checks that it is the last {@link JobItemAction} on
	 * the {@link JobItem} and if permission is granted, sets the
	 * {@link JobItemAction} to 'deleted'.
	 * 
	 * @param jobItemId the {@link JobItemAction} ID.
	 * @return a {@link ResultWrapper}
	 */
	ResultWrapper deleteJobItemActionById(int jiaId, String username, int allocatedSubdivId);

	ResultWrapper editAction(int jobItemActionId, String timeSpent, String remark, HttpSession session);

	/**
	 * Returns the last (i.e. latest) {@link JobItemAction} that is still active
	 * (i.e. not deleted) for the {@link JobItem}.
	 * 
	 * @param jobItem the {@link JobItem}.
	 * @return the {@link JobItemAction}, or null if there are none active
	 */
	JobItemAction findLastActiveActionForItem(JobItem jobItem);

	/**
	 * Returns the penultimate {@link JobItemAction} that is still active for
	 * the {@link JobItem}.
	 * 
	 * @param jobItem the {@link JobItem}.
	 * @return the {@link JobItemAction}, or null if there is only one active
	 */
	JobItemAction findPenultimateActiveActionForItem(JobItem jobItem);
	
	/**
	 * Returns all {@link JobItemAction}s of an employee between start and end date
	 */
	List<JobItemActionForEmployeeDTO> findNotTimeRelated(Contact employee, LocalDate start, LocalDate end);
	
	/**
	 * Returns all time related {@link JobItemAction}s of an employee between start and end date
	 */
	List<JobItemActionForEmployeeDTO> findTimeRelated(Contact employee, LocalDate start, LocalDate end);
	
	/**
	 * Returns sum of all time related {@link JobItemAction}s done by employee between start and end date
	 */
	Integer summarizeTimeRelated(Contact employee, LocalDate start, LocalDate end);
	
	/**
	 * Returns sum of all not time related {@link JobItemAction}s done by employee between start and end date
	 */
	Integer summarizeNotTimeRelated(Contact employee, LocalDate start, LocalDate end);
	
	/**
	 * Returns all {@link JobItemAction} entities currently stored for a job
	 * item.
	 * 
	 * @param jobItemId the id of the job item we require {@link JobItemAction}
	 *        's for
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper webGetJobItemActions(int jobItemId);

	void saveWithoutInterceptor(JobItemAction jia);
	
	/**
	 * Returns a projection list of actions for the specific job item. 
	 * @param jobItemId
	 * @param deleted query parameter for deleted actions (null to return all)
	 * @param locale to load details in (e.g. state description Key/Value pairs)
	 * @return
	 */
	List<JobItemActionProjectionDTO> getProjectionDTOsForJobItem(Integer jobItemId, Boolean deleted, Locale locale);
	
	List<RepairTimeDTO> findTechnicianRepairTimeByRcr(Integer jiId, String rcrIdentifier, Contact contact);
	
	void editRepairTimeProgressActivities(RepairTimeForm form);
	
	void manageRepairTimeProgressActivities(RepairTimeForm form);
	
	List<Integer> findNotProcessedActionsIdForAdveso();
}