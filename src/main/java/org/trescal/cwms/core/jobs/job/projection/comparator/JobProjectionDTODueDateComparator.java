package org.trescal.cwms.core.jobs.job.projection.comparator;

import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;

import java.time.LocalDate;
import java.util.Comparator;

/**
 * JobProjectionDTO comparator intended for use with dashboard displays,
 * where the JobItemProjectionDTOs have also been populated.
 * <p>
 * Sorts: by oldest due date of items contained within JobProjectionDTO,
 * then jobno, then jobid.
 * All are mandatory fields in database but possible to have empty jobitems.
 */
public class JobProjectionDTODueDateComparator implements Comparator<JobProjectionDTO> {
	private final Comparator<LocalDate> dateComparator;

	public JobProjectionDTODueDateComparator() {
		this.dateComparator = Comparator.nullsFirst(Comparator.naturalOrder());
	}

	@Override
	public int compare(JobProjectionDTO dto1, JobProjectionDTO dto2) {
		LocalDate dueDate1 = getOldestDueDate(dto1);
		LocalDate dueDate2 = getOldestDueDate(dto2);

		int result = dateComparator.compare(dueDate1, dueDate2);
		if (result == 0) {
			result = dto1.getJobno().compareTo(dto2.getJobno());
			if (result == 0) {
				result = dto1.getJobid().compareTo(dto2.getJobid());
			}
		}

		return result;
	}

	private LocalDate getOldestDueDate(JobProjectionDTO jobDto) {
		LocalDate result = null;
		// JobDTO with no items won't have item list populated
		if (jobDto.getJobitems() != null) {
			result = jobDto.getJobitems().stream()
				.map(JobItemProjectionDTO::getDueDate)
				.sorted()
				.findFirst()
				.orElse(null);
		}
		return result;
	}

}
