package org.trescal.cwms.core.jobs.certificate.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.certificate.dto.CertificateSearchWrapper;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.certificate.form.CertificateSearchForm;
import org.trescal.cwms.core.jobs.certificate.form.CertificateSearchValidator;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;

@Controller
@IntranetController
@RequestMapping(value = "/certsearch.htm")
public class CertificateSearchController {
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private CertificateService certServ;
	@Autowired
	private CertificateSearchValidator validator;

	public static final String FORM_NAME = "certsearchform";

	@ModelAttribute("certstatuses")
	public CertStatusEnum[] getCertStatuses() {
		return CertStatusEnum.values();
	}

	@ModelAttribute("caltypes")
	public Object getCalTypes() {
		return calTypeServ.getCalTypes();
	}

	@ModelAttribute(FORM_NAME)
	public CertificateSearchForm makeCertSearchForm() throws Exception {
		return new CertificateSearchForm();
	}

	@InitBinder(FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.setValidator(validator);
	}

	@RequestMapping(method = RequestMethod.GET)
	public String formView() {
		return "trescal/core/jobs/certificate/certsearch";
	}

	@RequestMapping(method = RequestMethod.GET, params = "forced=forced")
	public String forcedSubmit(@ModelAttribute(FORM_NAME) CertificateSearchForm certSearchForm,
			@RequestParam(value = "mid", required = false, defaultValue = "") Integer mid) throws Exception {
		// Form validation not required, will be empty for forced search
		certSearchForm.setModelid(mid);
		PagedResultSet<CertificateSearchWrapper> rs = new PagedResultSet<CertificateSearchWrapper>(
				certSearchForm.getResultsPerPage(), certSearchForm.getPageNo());
		this.certServ.searchCertificatesJPA(certSearchForm, rs, true);
		certSearchForm.setRs(rs);
		return "trescal/core/jobs/certificate/certsearchresults";
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView onSubmit(@Validated @ModelAttribute(FORM_NAME) CertificateSearchForm certSearchForm,
			BindingResult result) throws Exception {

		if (result.hasErrors()) {
			return new ModelAndView("trescal/core/jobs/certificate/certsearch", "certsearchform", certSearchForm);
		} else {
			PagedResultSet<CertificateSearchWrapper> rs = new PagedResultSet<CertificateSearchWrapper>(
					certSearchForm.getResultsPerPage(), certSearchForm.getPageNo());
			this.certServ.searchCertificatesJPA(certSearchForm, rs, true);
			certSearchForm.setRs(rs);
			return new ModelAndView("trescal/core/jobs/certificate/certsearchresults", "certsearchform",
					certSearchForm);
		}
	}
}
