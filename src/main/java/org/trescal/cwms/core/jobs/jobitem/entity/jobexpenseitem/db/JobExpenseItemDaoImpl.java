package org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db;

import lombok.val;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus_;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.po.PO_;
import org.trescal.cwms.core.jobs.jobitem.dto.ExpenseItemDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.ExpenseItemOnClientPurchaseOrderDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.InvoiceableItemsByCompanyDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.db.AbstractJobItemDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobExpenseItemNotInvoiced;
import org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.GenericPricingEntity_;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice_;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem_;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink;
import org.trescal.cwms.core.pricing.invoice.entity.invoicejoblink.InvoiceJobLink_;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceType_;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem_;

import javax.persistence.criteria.*;
import java.util.List;
import java.util.Locale;

@Repository
public class JobExpenseItemDaoImpl extends AbstractJobItemDaoImpl<JobExpenseItem> implements JobExpenseItemDao {

	@Override
	protected Class<JobExpenseItem> getEntity() {
		return JobExpenseItem.class;
	}

	@Override
	public List<InvoiceableItemsByCompanyDTO> getInvoiceable(Integer allocatedCompanyId, Integer allocatedSubdivId) {
		if (allocatedCompanyId == null && allocatedSubdivId == null)
			throw new IllegalArgumentException(
					"Either allocatedCompanyId or allocatedSubdivId must be specified but both were null");
		return getResultList(cb -> {
			CriteriaQuery<InvoiceableItemsByCompanyDTO> cq = cb.createQuery(InvoiceableItemsByCompanyDTO.class);
			Root<JobExpenseItem> expenseItem = cq.from(JobExpenseItem.class);
			Join<JobExpenseItem, JobExpenseItemNotInvoiced> notInvoiced = expenseItem.join(JobExpenseItem_.notInvoiced,
					JoinType.LEFT);
			Join<JobExpenseItem, Job> job = expenseItem.join(JobExpenseItem_.job);
			Join<Job, Subdiv> allocatedSubdiv = job.join(Job_.organisation.getName());
			Join<Job, Contact> contact = job.join(Job_.con);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(expenseItem.get(JobExpenseItem_.invoiceable)));
			clauses.getExpressions().add(cb.isEmpty(expenseItem.get(JobExpenseItem_.invoiceItems)));
			if (allocatedSubdivId != null) {
				clauses.getExpressions().add(cb.equal(allocatedSubdiv.get(Subdiv_.subdivid), allocatedSubdivId));
			} else {
				Join<Subdiv, Company> allocatedCompany = allocatedSubdiv.join(Subdiv_.comp);
				clauses.getExpressions().add(cb.equal(allocatedCompany.get(Company_.coid), allocatedCompanyId));
			}
			clauses.getExpressions().add(cb.isNull(notInvoiced));
			cq.where(clauses);
			cq.groupBy(company.get(Company_.coid), company.get(Company_.coname), company.get(Company_.companyRole));
			cq.select(cb.construct(InvoiceableItemsByCompanyDTO.class, company.get(Company_.coid),
					company.get(Company_.coname), company.get(Company_.companyRole), cb.count(expenseItem)));
			return cq;
		});
	}

	@Override
	public List<JobExpenseItem> getInvoiceable(Company clientCompany, Integer allocatedCompanyId,
			Integer allocatedSubdivId, String completeJob) {

		if (allocatedCompanyId == null && allocatedSubdivId == null)
			throw new IllegalArgumentException(
					"Either allocatedCompanyId or allocatedSubdivId must be specified but both were null");

		return getResultList(cb -> {
			CriteriaQuery<JobExpenseItem> cq = cb.createQuery(JobExpenseItem.class);
			Root<JobExpenseItem> expenseItem = cq.from(JobExpenseItem.class);
			Join<JobExpenseItem, JobExpenseItemNotInvoiced> notInvoiced = expenseItem.join(JobExpenseItem_.notInvoiced,
                JoinType.LEFT);
            Join<JobExpenseItem, InvoiceItem> invoiceItem = expenseItem.join(JobExpenseItem_.invoiceItems,
                JoinType.LEFT);
            Join<InvoiceItem, Invoice> invoice = invoiceItem.join(InvoiceItem_.invoice, JoinType.LEFT);
            Join<Invoice, InvoiceType> invoiceType = invoice.join(Invoice_.type, JoinType.LEFT);
            invoiceType.on(cb.or(cb.isTrue(invoiceType.get(InvoiceType_.addToAccount)),
                cb.equal(invoiceType.get(InvoiceType_.name), "Internal")));

            Join<JobExpenseItem, Job> job = expenseItem.join(JobExpenseItem_.job, JoinType.LEFT);

            // TODO: fetch would be nice to prevent lazy loading, but doubles the results, if there is more than one costing item -> Hibernate bug?
            // expenseItem.fetch(JobExpenseItem_.costingItems, JoinType.LEFT).fetch(JobCostingExpenseItem_.jobCosting, JoinType.LEFT).fetch(GenericPricingEntity_.currency, JoinType.LEFT);

            Join<Job, JobStatus> js = job.join(Job_.js);
            Join<Job, Contact> contact = job.join(Job_.con, JoinType.LEFT);
            Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub, JoinType.LEFT);
            Join<Job, Subdiv> allocatedSubdiv = job.join(Job_.organisation.getName(), JoinType.LEFT);

            val subquery = expenseInvoicesCountSubquery(cb, cq, expenseItem);

            Predicate clauses = cb.and(
                cb.equal(subdiv.get(Subdiv_.comp), clientCompany),
				allocatedSubdivId != null ? cb.equal(allocatedSubdiv, allocatedSubdivId) :
					cb.equal(allocatedSubdiv.get(Subdiv_.comp), allocatedCompanyId),
				cb.isTrue(expenseItem.get(JobExpenseItem_.invoiceable)),
				cb.isNull(notInvoiced),
				cb.isNull(invoiceType),
				cb.equal(subquery.getSelection(), 0)
			);


			// select only job expenses with completed job
			if (completeJob.equals("YES")) {
				clauses.getExpressions().add(cb.isTrue(js.get(JobStatus_.complete)));
			}
			// select only job expenses with incomplete job
			else if (completeJob.equals("NO")) {
				clauses.getExpressions().add(cb.isFalse(js.get(JobStatus_.complete)));
			}

			cq.where(clauses);
			cq.orderBy(cb.asc(expenseItem.get(JobExpenseItem_.id)));
			return cq;
		});
	}

	private Subquery<Long> expenseInvoicesCountSubquery(CriteriaBuilder cb, CriteriaQuery<JobExpenseItem> cq, Root<JobExpenseItem> expenseItem) {
		Subquery<Long> expenseInvoices = cq.subquery(Long.class);
		Root<InvoiceItem> expenseInvoiceItem = expenseInvoices.from(InvoiceItem.class);
		Join<InvoiceItem, Invoice> expenseInvoice = expenseInvoiceItem.join(InvoiceItem_.invoice);
		Join<Invoice, InvoiceType> expenseInvoiceType = expenseInvoice.join(Invoice_.type);
		Predicate expenseInvoicesClauses = cb.conjunction();
		expenseInvoicesClauses.getExpressions()
			.add(cb.notEqual(expenseInvoiceType.get(InvoiceType_.name), "Internal"));
		expenseInvoicesClauses.getExpressions().add(cb.isTrue(expenseInvoiceType.get(InvoiceType_.addToAccount)));
		expenseInvoicesClauses.getExpressions()
			.add(cb.equal(expenseInvoiceItem.get(InvoiceItem_.expenseItem), expenseItem));
		expenseInvoices.where(expenseInvoicesClauses);
		expenseInvoices.select(cb.count(expenseInvoiceItem));
		return expenseInvoices;
	}

	@Override
	public List<ExpenseItemOnClientPurchaseOrderDTO> getExpenseItemsOnClientBPO(Integer poId, Integer jobId,
																				Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<ExpenseItemOnClientPurchaseOrderDTO> query = cb
				.createQuery(ExpenseItemOnClientPurchaseOrderDTO.class);
			Root<JobExpenseItem> expenseItem = query.from(JobExpenseItem.class);
			Join<JobExpenseItem, InstrumentModel> model = expenseItem.join(JobExpenseItem_.model);
			Expression<String> modelName = joinTranslation(cb, model, InstrumentModel_.nameTranslations, locale);
			Subquery<Long> sq = query.subquery(Long.class);
			Root<JobExpenseItemPO> sqJeiBpo = sq.from(JobExpenseItemPO.class);
			Join<JobExpenseItemPO, BPO> sqbpo = sqJeiBpo.join(JobExpenseItemPO_.bpo);
			Predicate sqClauses = cb.conjunction();
			sqClauses.getExpressions().add(cb.equal(sqbpo.get(BPO_.poId), poId));
			sqClauses.getExpressions().add(cb.equal(sqJeiBpo.get(JobExpenseItemPO_.item), expenseItem));
			sq.where(sqClauses);
			sq.select(cb.count(sqJeiBpo));
			Subquery<Long> sq2 = query.subquery(Long.class);
			Root<JobExpenseItemPO> sq2po = sq2.from(JobExpenseItemPO.class);
			sq2.where(cb.equal(sq2po.get(JobExpenseItemPO_.item), expenseItem));
			sq2.select(cb.count(sq2po));
			query.where(cb.equal(expenseItem.get(JobExpenseItem_.job), jobId));
			query.orderBy(cb.asc(expenseItem.get(JobExpenseItem_.id)));
			query.select(cb.construct(ExpenseItemOnClientPurchaseOrderDTO.class, expenseItem.get(JobExpenseItem_.id),
					expenseItem.get(JobExpenseItem_.itemNo), modelName, expenseItem.get(JobExpenseItem_.comment),
					sq.getSelection(), sq2.getSelection()));
			return query;
		});
	}

	@Override
	public List<ExpenseItemOnClientPurchaseOrderDTO> getExpenseItemsOnClientPO(Integer poId, Integer jobId,
			Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<ExpenseItemOnClientPurchaseOrderDTO> query = cb
					.createQuery(ExpenseItemOnClientPurchaseOrderDTO.class);
			Root<JobExpenseItem> expenseItem = query.from(JobExpenseItem.class);
			Join<JobExpenseItem, InstrumentModel> model = expenseItem.join(JobExpenseItem_.model);
			Expression<String> modelName = joinTranslation(cb, model, InstrumentModel_.nameTranslations, locale);
			Subquery<Long> sq = query.subquery(Long.class);
			Root<JobExpenseItemPO> sqJeiPo = sq.from(JobExpenseItemPO.class);
			Join<JobExpenseItemPO, PO> sqPo = sqJeiPo.join(JobExpenseItemPO_.po);
			Predicate sqClauses = cb.conjunction();
			sqClauses.getExpressions().add(cb.equal(sqPo.get(PO_.poId), poId));
			sqClauses.getExpressions().add(cb.equal(sqJeiPo.get(JobExpenseItemPO_.item), expenseItem));
			sq.where(sqClauses);
			sq.select(cb.count(sqJeiPo));
			Subquery<Long> sq2 = query.subquery(Long.class);
			Root<JobExpenseItemPO> sq2po = sq2.from(JobExpenseItemPO.class);
			sq2.where(cb.equal(sq2po.get(JobExpenseItemPO_.item), expenseItem));
			sq2.select(cb.count(sq2po));
			query.where(cb.equal(expenseItem.get(JobExpenseItem_.job), jobId));
			query.select(cb.construct(ExpenseItemOnClientPurchaseOrderDTO.class, expenseItem.get(JobExpenseItem_.id),
					expenseItem.get(JobExpenseItem_.itemNo), modelName, expenseItem.get(JobExpenseItem_.comment),
					sq.getSelection(), sq2.getSelection()));
			return query;
		});
	}

	@Override
	public List<ExpenseItemDTO> getAvailableForInvoice(Integer invoiceId, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<ExpenseItemDTO> cq = cb.createQuery(ExpenseItemDTO.class);
			Root<InvoiceJobLink> jobLink = cq.from(InvoiceJobLink.class);
			Join<InvoiceJobLink, Job> job = jobLink.join(InvoiceJobLink_.job);
			Join<Job, JobExpenseItem> expenseItem = job.join(Job_.expenseItems);
			Join<JobExpenseItem, InstrumentModel> model = expenseItem.join(JobExpenseItem_.model);
			Expression<String> modelName = joinTranslation(cb, model, InstrumentModel_.nameTranslations, locale);
			cq.where(cb.equal(jobLink.get(InvoiceJobLink_.invoice), invoiceId));
			cq.select(cb.construct(ExpenseItemDTO.class, expenseItem.get(JobExpenseItem_.id), job.get(Job_.jobid),
					job.get(Job_.jobno), expenseItem.get(JobExpenseItem_.itemNo), modelName,
					expenseItem.get(JobExpenseItem_.comment)));
			return cq;
		});
	}
}