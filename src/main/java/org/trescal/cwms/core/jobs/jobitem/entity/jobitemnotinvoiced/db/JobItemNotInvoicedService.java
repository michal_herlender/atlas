package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.NotInvoicedReason;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemNotInvoicedDTO;

public interface JobItemNotInvoicedService extends BaseService<JobItemNotInvoiced, Integer> {
	/***
	 * Creates a JobItemNotInvoiced entry or updates if changes are necessary
	 * @param jobItem : The job item to be associated with
	 * @param reason : NotInvoicedReason
	 * @param comments : new comments to use
	 * @param invoiceId : id of invoice, or 0 if no invoice
	 * @param contact : the contact performing the change
	 * @return
	 */
	JobItemNotInvoiced recordNotInvoiced(JobItem jobItem, NotInvoicedReason reason, String comments, int invoiceId, Contact contact);
	
	void createNotInvoicedItems(Integer invoiceId, List<Integer> jobitemids, NotInvoicedReason reason, String comments, Contact contact);
	
	List<JobItemNotInvoicedDTO> getProjectionDTOs(Collection<Integer> invoiceIds);
	
	void unlinkJobItemFromPeriodicInvoice(Integer invoiceId,List<Integer> items, Locale locale);
}
