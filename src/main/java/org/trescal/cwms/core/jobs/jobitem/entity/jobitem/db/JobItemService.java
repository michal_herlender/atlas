package org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db;

import org.springframework.data.domain.Page;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobSortKey;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.form.AddQuickItemsForm.AddQuickItemsRowDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.*;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.db.AbstractJobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.ItemBasketWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItemImportInfoDto;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.JobItemTransit;
import org.trescal.cwms.core.jobs.jobitem.form.ReturnToAddressForm;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.reports.dto.ItemsAtTPWrapper;
import org.trescal.cwms.core.vdi.projection.PositionProjection;
import org.trescal.cwms.core.workallocation.engineerallocation.form.AllocateWorkToEngineerSearchForm;
import org.trescal.cwms.core.workflow.dto.SelectedJobDTO;
import org.trescal.cwms.core.workflow.dto.SelectedJobItemDTO;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.holdstatus.HoldStatus;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;
import org.trescal.cwms.rest.tlm.dto.TLMJobItemDTO;
import org.trescal.cwms.spring.model.KeyValue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * Interface for accessing and manipulating {@link JobItem} entities.
 */
public interface JobItemService extends AbstractJobItemService<JobItem> {

	/**
	 * Iterates through the given {@link ItemBasketWrapper} objects, and if these
	 * are existing {@link Instrument}s, adds it to the {@link Job} as a new
	 * {@link JobItem} using {@link Job} and {@link Instrument} default settings
	 * where possible.
	 * 
	 * @param basketItems             the {@link ItemBasketWrapper} objects.
     * @param job                     the {@link Job} to add the {@link JobItem}s
     *                                to.
     * @param bookedInBy              the {@link Contact} booking in the
     *                                {@link JobItem}s.
     * @param bookingInAddr           the {@link Address} to book them in at.
     * @param defaultServiceTypeUsage if true, restrict service type to just the
     *                                default service type of the job
     */
    void addInstrumentsStraightToJob(List<ItemBasketWrapper> basketItems, Job job,
                                     Contact bookedInBy, Address bookingInAddr, boolean defaultServiceTypeUsage);

	void addRequirementsFromQuoteNotes(JobItem jobItem, Quotationitem qitem, Contact contact);

	ResultWrapper ajaxExportItemDataToLabview(int jobItemId);

	void ajaxReceivePaymentForItems(List<String> jobItemIds);

	ResultWrapper ajaxSetJobItemChaseOptions(int jobItemId, boolean chaseActive, boolean neverChase);

	ResultWrapper ajaxSetJobItemNextWorkRequirement(int jobItemId, int wrId);

	LocalDate calculateNextTransitDate(JobItem ji);

	/**
	 * Changes the location of any active {@link JobItem}s with the
	 * {@link Instrument} with the given ID to the location with the given ID.
	 *
	 * @param plantid the {@link Instrument} ID.
	 * @param locId   the {@link Location} ID.
	 */
	void changeItemLocation(int plantid, int locId);

	ChangeJobItemGroupWrapper changeJobItemGroup(int jobItemId, int groupId);

	/**
	 * Changes the {@link Instrument} of the {@link JobItem} with the given ID to
	 * the {@link Instrument} matching the given barcode.
	 * 
	 * @param jobitemid the {@link JobItem} ID.
	 * @param barcode   the {@link Instrument} ID.
	 * @return the {@link ResultWrapper}
	 */
	ResultWrapper changeJobItemInst(int jobitemid, String barcode,int personid);

	void cleanItem(JobItem ji, int timeSpent, String remark);

    /**
     * Copies the current set of contract review {@link Cost}s from the given source
     * {@link JobItem} into the destination {@link JobItem}. It is up to the callee
     * to persist the changes.
     *
     * @param jiSrc  the source {@link JobItem} to copy costs from.
     * @param jiDest the {@link JobItem} to copy costs to.
     */
    void copyContractReviewCosts(JobItem jiSrc, JobItem jiDest);

    void copyItemsToNewJob(List<Integer> oldJobItemIds, Job job, Contact currentContact, int nextItemNo);

    /**
     * Deletes the given {@link JobItem} from the database.
     *
     * @param ji the {@link JobItem} to delete.
     */
    void deleteJobItem(JobItem ji);

    /**
     * Checks that the {@link JobItem} with the given ID is valid to be deleted
	 * (i.e. it has no associated {@link Certificate}s or {@link Calibration}s),
	 * before deleting it from the database.
	 * 
	 * @param id the {@link JobItem} ID.
	 */
	ResultWrapper deleteJobItemWithChecks(int id);

	ResultWrapper despatchItem(int barcode);

	boolean exportItemDataToLabview(JobItem ji);

	/**
	 * Finds the active {@link JobItem} with the given {@link Instrument} barcode.
	 * If there is more than one active {@link JobItem} with the given
	 * {@link Instrument} barcode, the one with the highest ID is returned. If there
	 * are no matching {@link JobItem}s, null is returned.
	 * 
	 * @param plantid the {@link Instrument} ID.
	 * @return the (last) matching {@link JobItem}, or null if there are no matches.
	 */
	JobItem findActiveJobItemFromBarcode(int plantid);

	/**
	 * Returns the {@link JobItem} with the given ID, with certain additional fields
	 * loaded eagerly.
	 * 
	 * @param jobItemId the {@link JobItem} ID.
	 * @return the {@link JobItem}
	 */
	JobItem findEagerJobItem(int jobItemId);

	/**
	 * Returns {@link JobItem} with the given id with number fields pre-populated.
	 * Intended for AJAX calls, resulting object is heavy as is the operation to
	 * load it very fetch-intensive.
	 * 
	 * @param jobitemid the id of the {@link JobItem}.
	 * @return the {@link JobItem}.
	 */
	JobItem findEagerPopulatedJobitem(int jobitemid);

	/**
	 * Returns the {@link JobItem} with the given ID.
	 * 
	 * @param id the {@link JobItem} ID.
	 * @return the {@link JobItem}
	 */
	JobItem findJobItem(int id);

	/**
	 * Returns the {@link JobItem} with the given JobItemNumber.
	 * 
	 * @param jobItemNumber the {@link JobItem} JobItemNumber.
	 * @return the {@link JobItem}
	 */
	JobItem findJobItem(String jobItemNumber);

	/**
	 * Only used by one DWR call, consider future removal
	 */
	JobItem findEagerJobItemFromBarcode(int plantid);

	/**
	 * Returns the most recent {@link JobItem} for the {@link Instrument} identified
	 * by the given plantid, or null if the {@link Instrument} has never appeared on
	 * a {@link Job}.
	 * 
	 * @param plantid the barcode of the {@link Instrument}.
	 * @return the {@link JobItem} or null.
	 */
	JobItem findMostRecentJobItem(int plantid);
	
	JobItem findMostRecentActiveJobItem(int plantid);

	List<JobItem> findMostRecentJobItems(Integer plantid, Integer maxResults);

	List<JobItemInvoiceDto> findAllCompleteJobItemsWithoutInvoicesProjection(Integer allocatedCompanyId,
			Integer allocatedSubdivId, Company company, JobSortKey sortKey, String completeJob);

	List<JobItemInvoiceDto> findAllCompleteJobItemsWithOnlyProformaInvoicesProjection(Integer allocatedCompanyId,
			Integer allocatedSubdivId, Company company, JobSortKey sortKey, String completeJob);

	SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>> getActiveJobItemsForDeptReadyForWork(int deptId);

	/**
	 * Returns a {@link List} of {@link JobItem} entities that have ids matching
	 * those in the list parameter.
	 * 
	 * @param ids list of {@link JobItem} ids.
	 * @return list of {@link JobItem} entities.
	 */
	List<JobItem> getAllItems(List<Integer> ids);

	List<JobItem> getAllByState(ItemState state, Subdiv subdiv);

	/**
	 * Returns all {@link JobItem} entities stored in the database.
	 * 
	 * @return the {@link List} of {@link JobItem} entities.
	 */
	List<JobItem> getAllJobItems();

	List<JobItemOnClientPurchaseOrderDTO> getJobItemsOnClientPO(Integer poId, Integer jobId);

	/**
	 * Intended to be called with either jobIds and/or jobItemIds with at least one
	 * id present in one of the arguments
	 *
	 * @param jobIds     (may be null or empty)
	 * @param jobItemIds (may be null or empty)
	 */
	List<JobItemProjectionDTO> getJobItemProjectionDTOs(Collection<Integer> jobIds, Collection<Integer> jobItemIds);

	List<JobItemProjectionDTO> getJobItemProjectionDTOsByJobIds(Collection<Integer> jobIds, Locale locale);

	/**
	 * Returns a list of all {@link JobItem} entities that the given {@link Contact}
	 * is accredited to perform.
	 * 
	 * @param contact the {@link Contact} who the work can be performed by.
	 * @return list of {@link JobItem} entities.
	 */
	List<JobItem> getAvailableTaskList(Contact contact);

	List<JobItem> getContactItemsOnJobInStateGroup(int personId, StateGroup group);

	Integer getCountActiveJobItemsWithProcs(List<Integer> procIds, Integer days, Boolean includeBusiness,
			boolean labGroupsOnly, Subdiv subdiv);

	/**
	 * Returns all {@link JobItem} entities on the system with a {@link ItemState}
	 * matching the given Status ID.
	 * 
	 * @param subdivId the ID of the current {@link JobItem}'s {@link Job}
	 *                 allocation to subdiv.
	 * @return the {@link List} of {@link JobItem} entities.
	 */
	Long getCountAllJobItemsForState(ItemState state, int subdivId, StateGroupLinkType groupType);

	Integer getCountJobItemsByDate(String days, int subdivId);

	Long getCountJobItems(int jobid);

	Long getCountJobItemsForPlantid(Integer plantid);

	ActionOutcome getDefaultConRevOutcomeForItem(JobItem ji);

	/**
	 * Populates a {@link DetailedJobItem} dto with information about the given
	 * {@link JobItem}. Tests if the given id matches a real {@link JobItem}.
	 * 
	 * @param jobitemid the id of the {@link JobItem}.
	 * @return {@link DetailedJobItem} or null if no jobitem found.
	 */
	DetailedJobItem getDetailedJobItem(int jobitemid);

	JobItem getForView(Integer jobItemId);

    List<ItemsAtTPWrapper> getItemsAtThirdParty(Subdiv subdiv);

    List<JobItem> getItemsOnJobInStateGroup(int jobId, StateGroup group);

    Map<Contact, List<JobItem>> getItemsRequiringClientChase();

    Map<Contact, List<JobItem>> getItemsRequiringSupplierChase();

    List<JobItem> getJobItemsByIds(List<Integer> jobitemids);

    /**
     * Returns all {@link JobItem} entities on the {@link Job} with the matching
     * {@link ItemState} and {@link Capability}.
     *
     * @param jobid       id of the job which items should belong to
     * @param statusid    id of the status we want items for
     * @param procid      id of the procedure we want items for
     * @param excludejiid id of job item to be excluded (null if no exclusions)
     * @return {@link List} {@link JobItem}
     */
    List<JobItem> getJobItemsByStatusAndProc(int jobid, int statusid, int procid, Integer excludejiid);

    /**
     * Returns a list of {@link JobItemProjectionDTO} entities that belong to
     * {@link Job}s with the given jobnos.
     *
     * @param jobnos list of {@link Job} jobno.
     * @return list of matching {@link JobItem} entites sorted by {@link Job},
     * {@link JobItem} itemno.
     */
	List<JobItemProjectionDTO> getJobItemsFromJobs(List<String> jobnos, Locale locale);

	/**
	 * Returns a list of all {@link JobItem} entities belonging to the {@link Job}
	 * with the given id that have no associated {@link JobCostingItem} entities.
	 * 
	 * @param job the id of the {@link Job}.
	 * @return set of matching {@link JobItem} entities.
	 */
	Set<JobItem> getJobItemsWithNoCostings(int job);

	JobItem getMostRecentJobItemForInstrument(Instrument instrument);

	JobItemPlantillasDTO getPlantillasJobItem(int jobitemId, int allocatedAddressId);

	PagedResultSet<JobItemPlantillasDTO> getPlantillasJobItems(int allocatedCompanyId, int allocatedAddressId,
			Date lastModified, Integer clientAddressId, int businessSubdivId, int resultsPerPage, int currentPage);

	JobItemTransit getPreviousTransitActionForItem(JobItem ji);

	List<JobItem> getRemainingItemsOnJobInStateGroup(BatchCalibration batchCal, StateGroup group);

	/**
	 * Returns all {@link JobItem} entities that are on the {@link Job} with which
	 * the given {@link Delivery} is associated, but not currently assigned to the
	 * {@link Delivery} entity.
	 * 
	 * @param delivery the {@link Delivery} to get remaining items for.
	 * @return the {@link List} of {@link JobItem} entities.
	 */
	List<JobItem> getRemainingJobItemsForDelivery(JobDelivery delivery);

	/**
	 * Inserts the given {@link JobItem} into the database.
	 * 
	 * @param ji                           the {@link JobItem} to insert.
	 * @param currentContact               the {@link Contact} inserting the
	 *                                     {@link JobItem}.
	 * @param calDiscountFromSystemDefault previously looked up system default for
	 *                                     calibration discount (used for
	 *                                     non-quotation pricing)
	 */
	void insertJobItem(JobItem ji, Contact currentContact, Double calDiscountFromSystemDefault,
			String contractReviewComment);

	/**
	 * return the list of job items ids
	 * 
	 * @param job        the id of the {@link Job}
	 * @param rows       {@link AddQuickItemsRowDTO}
	 * @param bookedInAt {@link Address}
	 * @param contact    {@link Contact}
	 * 
	 */
	List<Integer> insertQuickItems(Job job, List<AddQuickItemsRowDTO> rows, Address bookedInAt, Contact contact);

	/**
	 * Returns whether or not the {@link JobItem} with the given ID is currently
	 * marked as In Transit.
	 * 
	 * @param jobItemId the {@link JobItem} ID.
	 * @return true if the item is in transit, false otherwise.
	 */
	boolean isInTransit(int jobItemId);

	boolean isInCalibration(JobItem ji);

	boolean isReadyForCalibration(JobItem ji);

    boolean isReadyForClientDelivery(JobItem ji, Integer businessSubdivId);

    boolean isReadyForInternalDelivery(JobItem ji, Integer businessSubdivId);

    boolean isReadyForInternalReturnDelivery(JobItem ji, Integer businessSubdivId);

    boolean isAwaitingInternalPO(JobItem ji);

    boolean isReadyForTPDelivery(JobItem ji, Integer businessSubdivId);

    boolean isReadyForTPRequirement(ItemState state);

    boolean isReadyToResumeCalibration(JobItem ji);

    boolean isReadyForGeneralServiceOperation(JobItem ji);

    boolean isReadyToResumeGeneralServiceOperation(JobItem ji);

    boolean isReadyForGeneralServiceOperationDocumentUpload(JobItem ji);

    boolean itemCanBeDespatched(JobItem ji);

    boolean itemCanBeReceived(JobItem ji);

    boolean itemHasBeenCalibrated(JobItem ji);

    void linkOnBehalf(JobItem jobItem, Contact setBy, Company onBehalfCompany, Address onBehalfAddress);

    /**
     * Function that calculates the {@link Address} (and {@link Location}) of the
     * given {@link JobItem}'s calibration. The function checks the
     * {@link Capability} of the {@link JobItem} and calculates the most appropriate
     * {@link Address} to send the item to that supports the {@link Capability}.
     *
     * @param ji the {@link JobItem}
     */
    void lookUpCalAddress(JobItem ji);

    JobItem mergeJobItem(JobItem ji);

    void moveItemToBottomOfJob(JobItem ji);

    /**
     * Sets the {@link ItemState} of the {@link JobItem}s with the given IDs to the
     * {@link ItemStatus} with the given ID.
	 * 
	 * @param statusId   the {@link ItemStatus} ID
	 * @param jobItemIds the {@link List} of {@link JobItem} IDs
	 * @param remark     the remark to add to the {@link JobItemActivity}
	 */
	void overrideStatusOfItems(int statusId, List<Integer> jobItemIds, String remark);

	/**
	 * Sets the {@link ItemState} of the {@link JobItem}s with the given IDs to the
	 * {@link HoldStatus} with the given ID.
	 * 
	 * @param statusId   the {@link ItemStatus} ID
	 * @param jobItemIds the {@link List} of {@link JobItem} IDs
	 * @param remark     the remark to add to the {@link JobItemActivity}
	 */
	void placeItemsOnHold(int statusId, List<Integer> jobItemIds, String remark);

	ResultWrapper receiveItem(int barcode, int addrId, Integer locId);

	void receivePaymentForItems(List<Integer> jobItemIds);

	boolean stateHasGroupOfKeyName(ItemState state, StateGroup group);

	void takeItemsOffHold(List<Integer> jobItemIds, String remark);

    /**
     * Updates the given {@link JobItem} in the database.
     *
     * @param ji the {@link JobItem} to update.
     * @deprecated JPA saves managed entities automatically!
     */
    @Deprecated
    void updateJobItem(JobItem ji);

    void updateProcIfNecessary(JobItem ji, JobItemWorkRequirement jiwr);

    /**
     * Checks if the currently logged-in {@link Contact} is accredited to perform
     * the default {@link Capability} for the given {@link JobItem}.
     *
     * @param con the currently logged-in {@link Contact}
     * @param ji  the {@link JobItem} to check the {@link Capability} accreditation
     *            for
     * @return true if the user is accredited to perform the default
     * {@link Capability} for the {@link JobItem}, false if the user is not
     * accredited, and null if there is no default {@link Capability}
     */
    Boolean userAccreditedForItemProc(Contact con, JobItem ji);

    JobItem getJobItemByItemCode(String itemCode);

    List<KeyValue<Integer, String>> getJobItemContractReviewCosts(JobItem ji);

    PagedResultSet<TLMJobItemDTO> searchJobitems(boolean active, Integer plantId, String plantno, String serialno,
                                                 String formerBarcode, Integer calibrationSubdiv, String jobno, int currentPage, int resultsPerPage,
                                                 Locale localeFromRequest);

	List<JobItemForProspectiveInvoiceDTO> getForProspectiveInvoice(Integer allocatedCompanyId, Integer companyId);

	Set<JobItem> createJobItems(Job job, List<AsnItemFinalSynthesisDTO> identifiedJobItemsList, Contact addedBy,
			Integer asnId);

	void setDefaultCosts(JobItem ji, BigDecimal calibrationDiscount);

	void updateAdditionalDemands(JobItem jobItem);

	JobItem createNewRepairJobItem(Job j, JobItem oldJi, Contact con);

	void setNextWorkRequirement(JobItem ji, JobItemWorkRequirement workReq);

	Page<JobItemIdDto> getJobItemIdDto(String days);

	Optional<JobItem> findActiveJobItemFromBarcodeOrFormerBarcode(String barcode);

	List<ItemAtTP> getAllItemsAtSpecificThirdParty(List<Integer> subdivids, int businessSubdivId, Locale locale);

	List<JobItemImportInfoDto> getJobItemsInfo(Integer jobId);

    Map<Instrument, JobItem> findMostRecentJobItem(List<Integer> plantids);

    Map<Integer, JobItemProjectionDTO> findMostRecentJobItemDTO(List<Integer> plantids);

    List<JobItem> createJobItems(Integer jobId, Map<Integer, Integer> plantIdsAndDefaultSt, Integer bookedInByContactId,
                                 Integer bookingInAddrId);

    List<JobItemFeedbackDto> getAllJobItemsByJobId(Integer jobId);

    /**
     * Inserts new {@link JobItem} entities using the details from the basket items.
     *
     * @param procIds       the {@link Capability} IDs for the new {@link JobItem}s.
     * @param itemPOs       the {@link PO} strings to match for the new
     *                      {@link JobItem}s.
     * @param basketItems   the {@link ItemBasketWrapper}s containing the
     *                      {@link Instrument}s and other details for the new
     *                      {@link JobItem} s.
     * @param job           the {@link Job} to add the {@link JobItem}s to.
     * @param bookingInAddr the {@link Address} to book in the {@link JobItem}s at.
     * @param datesIn       the Dates in of the jobitems, will be used also in book
     *                      in hook
     * @return the IDs of the newly created {@link JobItem}s.
     */
    List<Integer> createJobItems(List<Integer> procIds, List<Integer> workInstIds, List<String> itemPOs,
                                 List<ItemBasketWrapper> basketItems, Job job, List<Integer> defaultServiceTypeIds,
                                 List<Integer> bookedInByContactIds, Address bookingInAddr, List<Date> datesIn);

    List<PositionProjection> getPositionProjectionForJob(Integer jobId);

    ZonedDateTime getDateIn(Job job);

    List<JobItemOnClientPurchaseOrderDTO> getJobItemsOnClientBPO(Integer poId, Integer jobId);

    void updateReturnToAddress(ReturnToAddressForm form);

    List<JobItemForInstrumentTooltipDto> findJobItemsForInstrumentTooltipByPlantId(Integer plantId);

    List<Integer> getInstrumentIdsFromJob(Integer jobid);

    JobItemProjectionDTO getJobItemProjectionDTO(Integer jobitemId,Locale locale);
    
    ResultWrapper scanOutItem(JobItem ji,Address add);
    
    List<JobItem> getJobItemByDeliveryId(String delivryNo);
    	
    ResultWrapper scanInDeliveryItems(JobItem ji, int addrId, Integer locId);
    
    ResultWrapper scanInItem(int barcode, int addrId, Integer locId);

	List<SelectedJobItemDTO> getActiveJobItems(List<Integer> procIds, List<Integer> stateIds,
											   Boolean isAllocatedToEngineer, Subdiv subdiv, StateGroupLinkType sglType, Integer addressId);

	SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>> getActiveJobItemsReadyForAllocation(Subdiv subdiv, AllocateWorkToEngineerSearchForm form);

	List<SelectedJobItemDTO> getActiveJobItemsForWorkAllocation(List<Integer> procIds, List<Integer> stateIds,
																Boolean isAllocatedToEngineer, Subdiv subdiv, StateGroupLinkType sglType, Integer addressId, AllocateWorkToEngineerSearchForm form);

	void modifyJobStatus(Job job);

}