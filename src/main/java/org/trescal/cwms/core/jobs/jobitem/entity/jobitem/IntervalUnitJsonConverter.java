package org.trescal.cwms.core.jobs.jobitem.entity.jobitem;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.io.IOException;

public class IntervalUnitJsonConverter extends StdSerializer<IntervalUnit> {

    public IntervalUnitJsonConverter() {
        super(IntervalUnit.class);
    }

    @Override
    public void serialize(IntervalUnit intervalUnit, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("singular", intervalUnit.getSingularName(LocaleContextHolder.getLocale()));
        jsonGenerator.writeStringField("plural", intervalUnit.getName(2, LocaleContextHolder.getLocale()));
        jsonGenerator.writeEndObject();
    }
}
