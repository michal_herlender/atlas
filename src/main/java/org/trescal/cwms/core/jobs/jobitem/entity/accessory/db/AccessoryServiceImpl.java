package org.trescal.cwms.core.jobs.jobitem.entity.accessory.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.Accessory;

@Service("AccessoryService")
public class AccessoryServiceImpl extends BaseServiceImpl<Accessory, Integer> implements AccessoryService {

	@Autowired
	private AccessoryDao accessoryDao;
	@Autowired
	private ItemAccessoryService itemAccessoryService;
	@Autowired
	private GroupAccessoryService groupAccessoryService;

	@Override
	protected BaseDao<Accessory, Integer> getBaseDao() {
		return accessoryDao;
	}

	@Override
	public Integer countByJob(Integer jobId) {
		Integer result = 0;
		// Add all job item accessories
		result += itemAccessoryService.countByJob(jobId);
		// Add all group accessories
		result += groupAccessoryService.countByJob(jobId);
		return result;
	}
}