package org.trescal.cwms.core.jobs.calibration.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.range.Range;
import org.trescal.cwms.core.instrumentmodel.entity.range.db.RangeService;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.instrumentmodel.entity.uom.db.UoMService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.CalibrationPoint;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.PointComparator;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.db.CalibrationPointService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.CalibrationPointSet;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.db.CalibrationPointSetService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationrange.CalibrationRange;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CompanyModelCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.InstrumentCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.JobItemCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.ModelCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqEdit;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqOverride;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.db.CalReqHistoryService;
import org.trescal.cwms.core.jobs.calibration.entity.pointsettemplate.PointSetTemplate;
import org.trescal.cwms.core.jobs.calibration.entity.pointsettemplate.db.PointSetTemplateService;
import org.trescal.cwms.core.jobs.calibration.entity.templatepoint.TemplatePoint;
import org.trescal.cwms.core.jobs.calibration.form.CalibrationPointsForm;
import org.trescal.cwms.core.jobs.calibration.form.CalibrationPointsValidator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;

/**
 * Controller used to manage the creation and editing of
 * {@link CalibrationPointSet}s. After each save all current
 * {@link CalibrationPoint}s are deleted and updated values reinserted into the
 * database.
 */
@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class CalibrationPointsController {
	@Autowired
	private CalibrationPointService calPointServ;
	@Autowired
	private CalibrationPointSetService calPointSetServ;
	@Autowired
	private CalReqHistoryService calReqHistoryServ;
	@Autowired
	private CalReqService calReqServ;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private InstrumService instrumServ;
	@Autowired
	private JobItemService jobItemServ;
	@Autowired
	private InstrumentModelService modelServ;
	@Autowired
	private PointSetTemplateService pointSetTempServ;
	@Autowired
	private RangeService rangeServ;
	// Auto-initialized the first time used, neither threadsafe nor necessary!
	private HashMap<Integer, UoM> uoms;
	@Autowired
	private UoMService uomServ;
	@Autowired
	private CalibrationPointsValidator validator;
	@Autowired
	private UserService userService;

	private void addEmptyPointSetToForm(CalibrationPointsForm form, int amountOfPoints) {
		List<String> points = new ArrayList<String>();
		List<Integer> uoms = new ArrayList<Integer>();
		List<String> relativePoints = new ArrayList<String>();
		List<Integer> relativeUoms = new ArrayList<Integer>();
		for (int i = 0; i < amountOfPoints; i++) {
			points.add("");
			uoms.add(null);
			relativePoints.add("");
			relativeUoms.add(null);
		}
		form.setMetrics(uoms);
		form.setPoints(points);
		form.setRelativeMetrics(relativeUoms);
		form.setRelativePoints(relativePoints);
	}

	private void copyExistingPointSetToForm(CalibrationPointsForm form, Set<CalibrationPoint> pointSet) {
		List<String> points = new ArrayList<String>();
		List<Integer> uoms = new ArrayList<Integer>();
		List<String> relativePoints = new ArrayList<String>();
		List<Integer> relativeUoms = new ArrayList<Integer>();
		for (CalibrationPoint point : pointSet) {
			points.add(point.getFormattedPoint());
			uoms.add(point.getUom().getId());
			relativePoints.add((point.getRelatedPoint() == null) ? "" : point.getFormattedRelatedPoint());
			relativeUoms.add((point.getRelatedUom() == null) ? null : point.getRelatedUom().getId());
		}
		form.setMetrics(uoms);
		form.setPoints(points);
		form.setRelativeMetrics(relativeUoms);
		form.setRelativePoints(relativePoints);
	}

	private void copyExistingRangeToForm(CalibrationPointsForm form, CalibrationRange range) {
		form.setRangeFrom(range.getStart().toString());
		form.setRangeTo(range.getEnd().toString());
		form.setRangeUom(range.getUom().getId());

		if (range.isRelational()) {
			form.setRangeRelatedPoint(range.getRelatedPoint().toPlainString());
			form.setRangeRelatedUom(range.getRelatedUom().getId());
			form.setRelationalRange(true);
		} else {
			form.setRelationalRange(false);
		}
	}

	@InitBinder("command")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("command")
	protected CalibrationPointsForm formBackingObject(
			@RequestParam(name = "calreqid", required = false, defaultValue = "0") Integer calReqId,
			@RequestParam(name = "type", required = false, defaultValue = "") String type,
			@RequestParam(name = "convert", required = false, defaultValue = "false") Boolean convert,
			@RequestParam(name = "page", required = false, defaultValue = "") String page,
			@RequestParam(name = "jobitemid", required = false, defaultValue = "0") Integer jobItemId,
			@RequestParam(name = "plantid", required = false, defaultValue = "0") Integer plantId,
			@RequestParam(name = "compid", required = false, defaultValue = "0") Integer compId,
			@RequestParam(name = "modelid", required = false, defaultValue = "0") Integer modelId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		CalibrationPointsForm form = new CalibrationPointsForm();
		Contact contact = userService.get(username).getCon();
		Class<? extends CalReq> clazz = this.calReqServ.getClassFromString(type);
		if (type.trim().isEmpty() || (clazz == null))
			throw new Exception("Unable to determine calibration requirement type");
		else {
			CalReq cr = null;
			CalReq convertFrom = null;
			if (calReqId != 0)
				cr = this.calReqServ.findCalReq(calReqId);
			if (cr != null) {
				convertFrom = cr;
				cr = null;
				if (convert)
					form.setOverrideFrom(convertFrom);
				else
					form.setEditFrom(convertFrom);
			}
			if (cr == null)
				if (jobItemId != 0) {
					JobItemCalReq jicr = new JobItemCalReq();
					jicr.setJobItem(this.jobItemServ.findJobItem(jobItemId));
					cr = jicr;
				} else if (plantId != 0) {
					InstrumentCalReq icr = new InstrumentCalReq();
					icr.setInst(this.instrumServ.get(plantId));
					cr = icr;
				} else if ((compId != 0) && (modelId != 0)) {
					CompanyModelCalReq cmcr = new CompanyModelCalReq();
					cmcr.setComp(this.compServ.get(compId));
					cmcr.setModel(this.modelServ.findInstrumentModel(modelId));
					cr = cmcr;
				} else if (modelId != 0) {
					ModelCalReq mcr = new ModelCalReq();
					mcr.setModel(this.modelServ.findInstrumentModel(modelId));
					cr = mcr;
				} else if (form.getEditFrom() != null) {
					CalReq newCr = form.getEditFrom().getClass().newInstance();
					BeanUtils.copyProperties(form.getEditFrom(), newCr);
					newCr.setActive(true);
					newCr.setPointSet(null);
					newCr.setRange(null);
					newCr.setPrivateInstructions(null);
					newCr.setId(0);
					cr = newCr;
				} else
					throw new Exception("Not enough parameters to create new calibration requirements");

			form.setCr(cr);
			CalibrationPointSet pointSet = (cr.getPointSet() == null) ? new CalibrationPointSet() : cr.getPointSet();
			form.setPointSet(pointSet);
			form.setSaveAsInstrumentDefault(false);
			form.setSaveAsTemplate(false);
			// create an empty pointsettemplate that *can* be used to save a
			// pointsettemplate
			PointSetTemplate pst = new PointSetTemplate();
			pst.setSetOn(new Date());
			pst.setSetBy(contact);
			pst.setTitle("");
			pst.setDescription("");
			form.setTemplate(pst);
			// copy each point and metric into the fbo lists - makes validation
			// easier
			if (form.getPointSet() != null && form.getPointSet().getPoints() != null) {
				this.copyExistingPointSetToForm(form, form.getPointSet().getPoints());
			} else if (convertFrom != null && convertFrom.getPointSet() != null
					&& convertFrom.getPointSet().getPoints() != null) {
				this.copyExistingPointSetToForm(form, convertFrom.getPointSet().getPoints());
			} else {
				// insert 9 empty points to start
				this.addEmptyPointSetToForm(form, 9);
			}

			if (cr.getRange() != null) {
				this.copyExistingRangeToForm(form, cr.getRange());
			} else if ((convertFrom != null) && (convertFrom.getRange() != null)) {
				this.copyExistingRangeToForm(form, convertFrom.getRange());
			}
			if (convertFrom != null && convertFrom.getPrivateInstructions() != null) {
				cr.setPrivateInstructions(convertFrom.getPrivateInstructions());
			}
			form.setPage(page);
			form.setAction((convertFrom != null) ? convertFrom.getType() : cr.getType());
			form.setRelational(((convertFrom != null) && (convertFrom.getPointSet() != null))
					? convertFrom.getPointSet().isRelational()
					: form.getPointSet().isRelational());
			return form;
		}
	}

	/**
	 * Retains a lookup hashmap of {@link UoM} entities to save repeated loads of
	 * the same {@link UoM} entity.
	 * 
	 * @param id
	 * @return
	 */
	private UoM getUoM(int id) {
		if (this.uoms == null) {
			this.uoms = new HashMap<Integer, UoM>();
		}

		if (this.uoms.containsKey(id)) {
			return this.uoms.get(id);
		} else {
			// only query the db if the UoM has not already been loaded
			UoM uom = this.uomServ.findUoM(id);
			this.uoms.put(id, uom);
			return uom;
		}
	}

	@RequestMapping(value = "/calibrationpoints.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(HttpSession session,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@Validated @ModelAttribute("command") CalibrationPointsForm form, BindingResult bindingResult)
			throws Exception {
		if (bindingResult.hasErrors())
			return referenceData(session);
		Contact contact = userService.get(username).getCon();
		CalibrationPointSet ps = form.getPointSet();
		CalReq cr = form.getCr();

		// delete any current points belonging to the CalibrationPointsSet
		if ((ps.getPoints() != null) && !ps.getPoints().isEmpty()) {
			this.calPointServ.deleteAll(ps.getPoints());
			ps.setPoints(new TreeSet<>(new PointComparator()));
		}

		Range unusedRange = cr.getRange();
		cr.setRange(null);

		// if using pointset instead of range, delete any orphaned ranges
		if (unusedRange != null) {
			if (cr.getId() != 0) {
				this.calReqServ.updateCalReq(cr);
			}

			this.rangeServ.deleteRange(unusedRange);
		}

		// get the current set of points
		List<String> points = this.calPointServ.trimCalPointsList(form.getPoints());

		if (form.getAction().trim().equalsIgnoreCase("pointset") && !points.isEmpty()) {
			ps.setPoints(new TreeSet<>(new PointComparator()));

			int count = 0;
			for (String point : points) {
				CalibrationPoint aPoint = new CalibrationPoint();
				aPoint.setPoint(new BigDecimal(point));
				aPoint.setUom(this.getUoM(form.getMetrics().get(count)));

				if (form.isRelational()) {
					String rp = form.getRelativePoints().get(count);
					Integer ru = form.getRelativeMetrics().get(count);
					aPoint.setRelatedPoint(((rp != null) && !rp.trim().isEmpty()) ? new BigDecimal(rp) : null);
					aPoint.setRelatedUom((ru != null) ? this.getUoM(ru) : null);
				}

				aPoint.setPointSet(ps);
				ps.getPoints().add(aPoint);
				count++;
			}

			this.calPointSetServ.saveOrUpdateCalibrationPointSet(ps);

			if (form.isSaveAsTemplate()) {
				// add the points into a PointSetTemplate
				PointSetTemplate temp = form.getTemplate();
				temp.setPoints(new TreeSet<>(new PointComparator()));
				for (CalibrationPoint point : ps.getPoints()) {
					TemplatePoint p = new TemplatePoint();
					p.setPoint(point.getPoint());
					p.setUom(point.getUom());
					p.setRelatedPoint(point.getRelatedPoint());
					p.setRelatedUom(point.getRelatedUom());
					p.setTemplate(temp);
					temp.getPoints().add(p);
				}
				this.pointSetTempServ.insertPointSetTemplate(temp);
			}

			cr.setPointSet(ps);

			if (cr.getId() != 0) {
				this.calReqServ.updateCalReq(cr);
			}
		} else if (form.getAction().trim().equalsIgnoreCase("range")) {
			CalibrationRange range = new CalibrationRange();
			range.setStart(Double.parseDouble(form.getRangeFrom()));
			range.setEnd(Double.parseDouble(form.getRangeTo()));
			range.setUom(this.getUoM(form.getRangeUom()));

			if (form.isRelationalRange()) {
				String rp = form.getRangeRelatedPoint();
				Integer ru = form.getRangeRelatedUom();
				range.setRelatedPoint(((rp != null) && !rp.trim().isEmpty()) ? new BigDecimal(rp) : null);
				range.setRelatedUom((ru != null) ? this.getUoM(ru) : null);
			}

			// insert range
			this.rangeServ.insertRange(range);

			cr.setRange(range);
			cr.setPointSet(null);

			if (cr.getId() != 0) {
				this.calReqServ.updateCalReq(cr);
			}

			// delete unused and empty pointset if persisted
			if (ps.getId() != 0) {
				this.calPointSetServ.deleteCalibrationPointSet(ps);
			}
		} else {
			cr.setPointSet(null);

			if (cr.getId() != 0) {
				this.calReqServ.updateCalReq(cr);
			}

			// delete unused and empty pointset if persisted
			if (ps.getId() != 0) {
				this.calPointSetServ.deleteCalibrationPointSet(ps);
			}
		}

		if (cr.getId() == 0) {
			this.calReqServ.insertCalReq(cr);

			if (form.getEditFrom() != null) {
				CalReqEdit e = new CalReqEdit();
				e.setChangeBy(contact);
				e.setChangeOn(new Date());
				e.setNewCalReq(cr);
				e.setOldCalReq(form.getEditFrom());

				this.calReqServ.deactivateCalReqs(form.getEditFrom(), session);

				this.calReqHistoryServ.insertCalReqHistory(e);
			} else {
				CalReqOverride o = new CalReqOverride();
				o.setChangeBy(contact);
				o.setChangeOn(new Date());
				o.setNewCalReq(cr);
				o.setOldCalReq(form.getOverrideFrom());
				o.setOverrideOnInst((cr instanceof InstrumentCalReq) ? ((InstrumentCalReq) cr).getInst() : null);
				o.setOverrideOnJobItem((cr instanceof JobItemCalReq) ? ((JobItemCalReq) cr).getJobItem() : null);

				this.calReqHistoryServ.insertCalReqHistory(o);
			}
		} else {
			this.calReqServ.updateCalReq(cr);
		}

		return new ModelAndView("trescal/core/jobs/calibration/calibrationpointssuccess", "form", form);
	}

	@RequestMapping(value = "/calibrationpoints.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(HttpSession session) throws Exception {
		Map<String, Object> refData = new HashMap<String, Object>();
		refData.put("uom", this.uomServ.getAllUoMs());
		refData.put("templates", this.pointSetTempServ.getAllPointSetTemplates());
		// set the different note/comment types into the session for use by
		// dwr
		session.setAttribute("calibrationrequirement", PresetCommentType.CALIBRATION_REQUIREMENT);
		return new ModelAndView("trescal/core/jobs/calibration/calibrationpoints", refData);
	}
}