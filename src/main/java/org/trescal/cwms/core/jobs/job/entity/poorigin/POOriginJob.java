package org.trescal.cwms.core.jobs.job.entity.poorigin;

import lombok.NoArgsConstructor;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("job")
@NoArgsConstructor
public class POOriginJob extends POOrigin<Job> {

    @Override
    @Transient
    public KindName getKindName() {
        return KindName.JOB;
    }

    public POOriginJob(Job kind) {
        super(kind);
        this.setJob(kind);
    }

}
