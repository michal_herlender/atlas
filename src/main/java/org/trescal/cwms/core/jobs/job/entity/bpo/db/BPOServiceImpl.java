package org.trescal.cwms.core.jobs.job.entity.bpo.db;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.jobs.job.dto.BPOPeriodicInvoiceDTO;
import org.trescal.cwms.core.jobs.job.dto.BPOUsageDTO;
import org.trescal.cwms.core.jobs.job.dto.BpoDTO;
import org.trescal.cwms.core.jobs.job.dto.DWRBpoDTO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.RelatedBPODWRSet;
import org.trescal.cwms.core.jobs.job.entity.bpo.RelatedBPOSet;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.db.JobBPOLinkService;
import org.trescal.cwms.core.jobs.job.form.genericentityvalidator.BPOValidator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpSession;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Trescal
 */
@Service("BPOService")
public class BPOServiceImpl extends BaseServiceImpl<BPO, Integer> implements BPOService {

	@Autowired
	private BPODao bpoDao;
	// TODO : discuss / evaluate validation for BPO creation. 
	@Autowired
	private BPOValidator bpoValidator;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private JobService jobServ;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private SubdivService subServ;
	@Value("${cwms.config.bpo.usage.jobitemsonpilimit}")
	private Integer jobItemsOnPeriodicInvoiceLimit;
	@Autowired
	private JobBPOLinkService jobBpoLinkService;

	@Override
	protected BaseDao<BPO, Integer> getBaseDao() {
		return bpoDao;
	}

	@Override
	public BpoDTO addBPOToJob(int jobid, int bpoid) {
		JobBPOLink existingLink = this.jobBpoLinkService.findByJobIdAndBpoId(jobid, bpoid);
		BpoDTO result;
		Integer addItemCount = 0;
		if (existingLink == null) {
			BPO bpo = this.get(bpoid);
			Job job = this.jobServ.get(jobid);
			existingLink = new JobBPOLink();
			existingLink.setBpo(bpo);
			existingLink.setJob(job);
			existingLink.setDefaultForJob(false);
			existingLink.setCreatedOn(new Date());
			this.jobBpoLinkService.save(existingLink);
			if (job.getBpoLinks() == null)
				job.setBpoLinks(new HashSet<>());
			job.getBpoLinks().add(existingLink);

		}

		result = new BpoDTO(existingLink.getBpo());
		result.setAddedItems(addItemCount);

		return result;
	}

	@Override
	public BpoDTO addBPOToJobItem(Integer jobItemId, Integer bpoId) {
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		BPO bpo = this.get(bpoId);
		// remove linked BPO(s)
		jobItem.getItemPOs().removeIf(jipo -> jipo.getBpo() != null);
		// assign new BPO
		JobItemPO jobItemPO = new JobItemPO();
		jobItemPO.setItem(jobItem);
		jobItemPO.setBpo(bpo);
		jobItem.getItemPOs().add(jobItemPO);
		jobItemService.mergeJobItem(jobItem);
		return new BpoDTO(bpo);
	}

	@Override
	public BPO get(Integer id) {
		return this.bpoDao.find(id);
	}

	@Override
	public Map<Scope, List<BpoDTO>> findAllForJob(Integer jobId, boolean activeOnly) {
		return this.bpoDao.findAllForJob(jobId, activeOnly).stream().collect(Collectors.groupingBy(BpoDTO::getScope));
	}

	@Override
	public List<BPO> getAllByContactHierarchical(Contact contact, Company allocatedCompany) {
		return bpoDao.getAllByContactHierarchical(contact, allocatedCompany);
	}

	@Override
	public List<BpoDTO> getAllByContactHierarchicalNotOnJob(Integer personId, Integer allocatedCompanyId,
			Integer jobId) {
		return bpoDao.getAllByContactHierarchicalNotOnJob(personId, allocatedCompanyId, jobId);
	}

	@Override
	public List<BpoDTO> getAllByCompany(Company company, Boolean active, Company allocatedCompany) {
		return this.bpoDao.getAllByCompany(company, active, allocatedCompany);
	}

	@Override
	public List<BpoDTO> getAllByContact(Contact contact, Boolean active, Company allocatedCompany) {
		return this.bpoDao.getAllByContact(contact, active, allocatedCompany);
	}

	@Override
	public List<BpoDTO> getAllBySubdiv(Subdiv subdiv, Boolean active, Company allocatedCompany) {
		return this.bpoDao.getAllBySubdiv(subdiv, active, allocatedCompany);
	}

	@Override
	public List<BPOPeriodicInvoiceDTO> getPeriodicInvoicesLinkedTo(Integer bpoId, Locale locale) {
		List<BPOPeriodicInvoiceDTO> periodicInvoices = bpoDao.getPeriodicInvoicesLinkedTo(bpoId);
		periodicInvoices.forEach(pi -> pi.setJobItems(bpoDao.getJobItemsOnPeriodicInvoice(pi.getPeriodicInvoiceId(), bpoId, locale,
			jobItemsOnPeriodicInvoiceLimit)));
		return periodicInvoices;
	}

	@Override
	public List<BPOUsageDTO> getBPOUsage(Integer bpoId, Locale locale) {
		return bpoDao.getJobItemAmounts(bpoId, locale);
	}

	@Override
	public List<BPOUsageDTO> getExpenseItemAmounts(Integer bpoId, Locale locale) {
		return bpoDao.getExpenseItemAmounts(bpoId, locale);
	}

	@Override
	public List<BPO> getExpiringBPOs() {
		return this.bpoDao.getExpiringBPOs();
	}

	@Override
	public RelatedBPOSet getAllBPOsRelatedToContact(int personid, KeyValue<Integer, String> allocatedCompanyKV) {
		Company allocatedCompany = this.compServ.get(allocatedCompanyKV.getKey());
		Contact c = this.conServ.get(personid);
		RelatedBPOSet rbs = new RelatedBPOSet();
		int size = 0;
		List<BpoDTO> tempList = this.getAllByCompany(c.getSub().getComp(), true, allocatedCompany);
		rbs.setCompanyName(c.getSub().getComp().getConame());
		rbs.setSubdivName(c.getSub().getSubname());
		rbs.setContactName(c.getName());
		rbs.setCompanyBPOs(tempList);
		size = size + tempList.size();
		tempList = this.getAllBySubdiv(c.getSub(), true, allocatedCompany);
		rbs.setSubdivBPOs(tempList);
		size = size + tempList.size();
		tempList = this.getAllByContact(c, true, allocatedCompany);
		rbs.setContactBPOs(tempList);
		size = size + tempList.size();
		SupportedCurrency currency = c.getSub().getComp().getCurrency();
		rbs.setCurrencyERSymbol(currency.getCurrencyERSymbol());
		rbs.setSize(size);
		return rbs;
	}

	@Override
	public RelatedBPODWRSet getAllBPOsRelatedToJob(int personid, HttpSession session) {
		@SuppressWarnings("unchecked")
		KeyValue<Integer, String> companyKeyValue = (KeyValue<Integer, String>) session
			.getAttribute(Constants.SESSION_ATTRIBUTE_COMPANY);
		Company allocatedCompany = this.compServ.get(companyKeyValue.getKey());
		Contact c = this.conServ.get(personid);
		RelatedBPODWRSet rbs = new RelatedBPODWRSet();
		rbs.setCompanyName(c.getSub().getComp().getConame());
		rbs.setSubdivName(c.getSub().getSubname());
		rbs.setContactName(c.getName());

		List<BpoDTO> companyList = this.getAllByCompany(c.getSub().getComp(), true, allocatedCompany);
		List<BpoDTO> subdivList = this.getAllBySubdiv(c.getSub(), true, allocatedCompany);
		List<BpoDTO> contactList = this.getAllByContact(c, true, allocatedCompany);

		Locale locale = LocaleContextHolder.getLocale();
		DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(locale); 
		
		rbs.setCompanyBPOs(convertDTOs(companyList, formatter));
		rbs.setSubdivBPOs(convertDTOs(subdivList, formatter));
		rbs.setContactBPOs(convertDTOs(contactList, formatter));

		SupportedCurrency currency = c.getSub().getComp().getCurrency();
		rbs.setCurrencyERSymbol(currency.getCurrencyERSymbol());
		rbs.setSize(companyList.size() + subdivList.size() + contactList.size());
		return rbs;
	}
	
	private List<DWRBpoDTO> convertDTOs(List<BpoDTO> sourceList, DateTimeFormatter formatter) {
		return sourceList.stream().map(bpoDTO -> new DWRBpoDTO(bpoDTO, formatter)).collect(Collectors.toList());
	}

	@Override
	public void removeItemFromBPO(Integer jobItemId, Integer bpoId) {
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		BPO bpo = this.get(bpoId);
		jobItem.getItemPOs().removeIf(jipo -> jipo.getBpo() != null && jipo.getBpo().equals(bpo));
		jobItemService.mergeJobItem(jobItem);
	}

	@Override
	public void updateJobBPODefault(int jobid, int poid) {
		Job j = this.jobServ.get(jobid);

		JobBPOLink currentDefaultBpoLink = j.getBpoLinks().stream().filter(JobBPOLink::isDefaultForJob).findFirst()
			.orElse(null);
		if (currentDefaultBpoLink != null) {
			currentDefaultBpoLink.setDefaultForJob(false);
			this.jobBpoLinkService.merge(currentDefaultBpoLink);
		}

		if (currentDefaultBpoLink == null || currentDefaultBpoLink.getBpo().getPoId() != poid) {
			JobBPOLink newDefaultBpoLink = j.getBpoLinks().stream().filter(e -> e.getBpo().getPoId() == poid)
				.findFirst().orElse(null);
			if (newDefaultBpoLink != null) {
				newDefaultBpoLink.setDefaultForJob(true);
				this.jobBpoLinkService.merge(newDefaultBpoLink);
			}
		}
	}

	@Override
	public BpoDTO saveBpoDto(BpoDTO inBpo, KeyValue<Integer, String> allocatedCompay) {
		return new BpoDTO(merge(dtoToData(inBpo, allocatedCompay)));
	}
	
	@Override
	public List<Integer> getLinkedToJobsOnInvoice(Integer invoiceId){
		return this.bpoDao.getLinkedToJobsOnInvoice(invoiceId);
	}

	private BPO dtoToData(BpoDTO in, KeyValue<Integer, String> allocatedCompay) {
		val bpo = in.getPoId() == null ? new BPO() : bpoDao.find(in.getPoId());
		bpo.setLimitAmount(in.getLimitAmount());
		bpo.setComment(in.getComment());
		bpo.setPoNumber(in.getPoNumber());
		bpo.setDurationFrom(in.getDurationFrom());
		bpo.setDurationTo(in.getDurationTo());
		bpo.setOrganisation(compServ.get(allocatedCompay.getKey()));
		bpo.setActive(in.getActive());
		switch (in.getScope()) {
			case COMPANY:
				bpo.setCompany(compServ.get(in.getUnitId()));
				break;
			case SUBDIV:
				bpo.setSubdiv(subServ.get(in.getUnitId()));
				break;
			case CONTACT:
				bpo.setContact(conServ.get(in.getUnitId()));
				break;
		}

		return bpo;
	}
}