package org.trescal.cwms.core.jobs.jobitem.form;

import java.util.Map;

import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.jobitem.entity.enums.NewJobItemSearchOrderByEnum;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class NewJobItemSearchForm extends AbstractNewJobItemForm {
	private String action;
	private Integer basketNextTempGroup;
	private Integer bookingInAddrId;
	private boolean contractReviewItems;
	private Integer jobQuoteLinkId;
	private NewJobItemSearchOrderByEnum orderby;
	private Integer pageDistinctModels;
	private Integer pageInstruments;
	private Integer pageModels;
	private Integer resultsPerPage;
	private InstrumentAndModelSearchForm<InstrumentModel> searchForm;
	private String tabSelected;
	private Map<Integer, Integer> instQItemIds; // Map of Instrument plantids to
												// default linked QuotationItem
												// ids
	private Map<Integer, Integer> modelQItemIds; // Map of InstrumentModel
													// modelids to default
													// linked QuotationItem ids
	private Map<Integer, Integer> distinctQItemIds; // Map of InstrumentModel
													// modelids to default
													// linked QuotationItem ids

}