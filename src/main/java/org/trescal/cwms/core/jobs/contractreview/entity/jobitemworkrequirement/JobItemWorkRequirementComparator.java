package org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement;

import java.util.Comparator;

/**
 * Comparator is not used in Hibernate, just in services that require work requirement comparison for 
 * already persisted work requirements. 
 */
public class JobItemWorkRequirementComparator implements Comparator<JobItemWorkRequirement> {
	@Override
	public int compare(JobItemWorkRequirement o1, JobItemWorkRequirement o2) {
		if (o1.getWorkRequirement().getId().compareTo(o2.getWorkRequirement().getId()) != 0) {
			return o1.getWorkRequirement().getId().compareTo(o2.getWorkRequirement().getId());
		} else {
			return ((Integer) o1.getJobitem().getJobItemId()).compareTo(o2.getJobitem().getJobItemId());
		}

	}
}
