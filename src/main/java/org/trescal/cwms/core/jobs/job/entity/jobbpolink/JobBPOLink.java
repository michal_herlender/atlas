package org.trescal.cwms.core.jobs.job.entity.jobbpolink;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

import lombok.Setter;

@Entity
@Table(name = "jobbpolink")
@Setter
public class JobBPOLink extends Auditable
{
	private int id;
	private BPO bpo;
	private Job job;
	private boolean defaultForJob;
	private Date createdOn;
	
	/**
	 * @return the BPO
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "bpoid")
	public BPO getBpo()
	{
		return this.bpo;
	}
	
	/**
	 * @return the Job
	 */
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "jobid")
	public Job getJob()
	{
		return this.job;
	}

	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId()
	{
		return this.id;
	}

	@Column(name = "defaultforjob")
	public boolean isDefaultForJob() {
		return defaultForJob;
	}
	
	@NotNull
	@Column(name = "createdon", columnDefinition = "datetime2")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreatedOn() {
		return this.createdOn;
	}
}
