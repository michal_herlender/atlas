package org.trescal.cwms.core.jobs.repair.component.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.repair.component.FreeRepairComponent;

@Service("FreeRepairComponentService")
public class FreeRepairComponentServiceImpl extends BaseServiceImpl<FreeRepairComponent, Integer> implements FreeRepairComponentService
{
	@Autowired
	private FreeRepairComponentDao freeRepairComponentDao;

	@Override
	protected BaseDao<FreeRepairComponent, Integer> getBaseDao() {
		return freeRepairComponentDao;
	}
}