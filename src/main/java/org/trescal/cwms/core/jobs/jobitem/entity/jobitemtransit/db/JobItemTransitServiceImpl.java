package org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.db;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.JobItemTransit;
import org.trescal.cwms.core.jobs.jobitem.form.genericentityvalidator.JobItemTransitValidator;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.workflow.automation.antech.jobinterceptor.JobItemActionChange;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

import java.util.Date;

@Service("JobItemTransitService")
@Slf4j
public class JobItemTransitServiceImpl extends BaseServiceImpl<JobItemTransit, Integer> implements JobItemTransitService
{
	@Autowired
	private JobItemActionService actionServ;
	@Autowired
	private AddressService addrServ;
	@Autowired
	private ItemStateService itemStateServ;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private JobItemTransitValidator jitValidator;
	@Autowired
	private JobItemTransitDao jobItemTransitDao;
	@Autowired
	private LocationService locServ;
	@Autowired
	private UserService userService;

	// Redefined here to enable AOP to trigger (not possible with inherited methods or interface) 
	@Override
	@JobItemActionChange
	public void save(JobItemTransit object) {
		super.save(object);
	}

	// Redefined here to enable AOP to trigger (not possible with inherited methods) 
	@Override
	@JobItemActionChange
	public JobItemTransit merge(JobItemTransit object) {
		return super.merge(object);
	}

	public ResultWrapper receiveInTransitItem(int jobItemId, String remark, int endStatId, String username)
	{
		JobItem ji = this.jiServ.findJobItem(jobItemId);

		/*
		 * HARDCODED STATUS - in transit 10...
		 */
		if (ji.getState().getStateid() != 10)
		{
			return new ResultWrapper(false, "not in transit", null, null);
		}

		JobItemAction action = this.actionServ.findLastActiveActionForItem(ji);
		JobItemTransit transit;
		if (action instanceof JobItemTransit)
		{
			transit = this.get(action.getId());
		}
		else
		{
			return new ResultWrapper(false, "no transit action on item", null, null);
		}

		Integer fromLocId = (transit.getFromLoc() == null) ? null : transit.getFromLoc().getLocationid();
		Integer toLocId = (transit.getToLoc() == null) ? null : transit.getToLoc().getLocationid();
		/*
		 * HARDCODED ACTIVITY - 2 received
		 */
		return this.recordTransit(jobItemId, remark, endStatId, transit.getFromAddr().getAddrid(),
				fromLocId, transit.getToAddr().getAddrid(), toLocId, username);
	}

	private ResultWrapper recordTransit(int jobItemId, String remark, int endStatId, int fromAddrId,
										Integer fromLocId, int toAddrId, Integer toLocId, String username)
	{
		JobItem ji = this.jiServ.findJobItem(jobItemId);

		if (ji.getState().isStatus())
		{
			JobItemTransit jit = new JobItemTransit();
			jit.setCreatedOn(new Date());
			jit.setActivity(this.itemStateServ.findItemActivity(2));
			jit.setActivityDesc(jit.getActivity().getDescription());
			jit.setFromAddr(this.addrServ.get(fromAddrId));
			jit.setToAddr(this.addrServ.get(toAddrId));

			Location fLoc = (fromLocId == null) ? null : this.locServ.get(fromLocId);
			jit.setFromLoc(fLoc);
			Location tLoc = (toLocId == null) ? null : this.locServ.get(toLocId);
			jit.setToLoc(tLoc);

			Contact contact = userService.get(username).getCon();
			jit.setStartedBy(contact);
			jit.setStartStamp(new Date());
			jit.setStartStatus((ItemStatus) ji.getState());
			// jit.setStepNo(ji.getNextStepNo());
			jit.setTimeSpent(5);
			jit.setDeleted(false);
			jit.setEndStamp(new Date());
			jit.setJobItem(ji);
			jit.setRemark(remark);

			// if "Despatched" set to "In Transit"
			int endStatusId = (jit.getActivity().getStateid() == 1) ? 10 : endStatId;
			jit.setEndStatus(this.itemStateServ.findItemStatus(endStatusId));


			// create a new validator to validate this JobItemTransit before
			// trying the insert
			BindException errors = new BindException(jit, "jobitemtransit");
			this.jitValidator.validate(jit, errors);

			// if validation passes then insert the JobItemTransit
			if (!errors.hasErrors())
			{
				ji.setState(jit.getEndStatus());
				this.jiServ.merge(ji);
				jit.setJobItem(ji);

				this.save(jit);
			}

			return new ResultWrapper(errors, jit);
		}
		else
		{
			// cannot transport when state is an activity
			return new ResultWrapper(false, "Cannot create a transit action whilst the item has an incomplete activity.", null, null);
		}
	}

	@Override
	protected BaseDao<JobItemTransit, Integer> getBaseDao() {
		return jobItemTransitDao;
	}
}