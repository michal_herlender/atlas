package org.trescal.cwms.core.jobs.contractreview.entity.contractreview.db;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview;
import org.trescal.cwms.core.jobs.contractreview.projection.ContractReviewProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.form.JIContractReviewForm;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemPriceProjectionDto;
import org.trescal.cwms.spring.model.KeyValue;

public interface ContractReviewService extends BaseService<ContractReview, Integer> {

	void saveAndInsertContractReviewActivity(ContractReview contractReview, Integer timeSpent);

	ContractReview getMostRecentContractReviewForJobitem(JobItem jobItem);

	List<ContractReview> getEagerContractReviewsForJob(int jobid);

	List<ContractReviewProjectionDTO> getProjectionDTOsForJobItem(Integer jobItemId);

	List<JobItemPriceProjectionDto> getPriceProjectionDTOs(Collection<Integer> jobItemIds);

	JobItem contractReview(JIContractReviewForm form, KeyValue<Integer, String> subdivDto, String username);

	void saveAndInsertContractReviewActivity(ContractReview contractReview, Integer timeSpent, Date startDate,
			Date endDate, Contact contact);
	
	List<ContractReviewProjectionDTO> getCrProjectionDTOsyjob(Integer jobid);
	Long CountCryjob(Integer jobid);
}