package org.trescal.cwms.core.jobs.calibration.entity.reversetraceability;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.mailgroupmember.db.MailGroupMemberService;
import org.trescal.cwms.core.company.entity.mailgrouptype.MailGroupType;
import org.trescal.cwms.core.jobs.calibration.dto.StandardsUsageAnalysisDTO;
import org.trescal.cwms.core.jobs.calibration.entity.reversetraceability.db.ReverseTraceabilitySettingsService;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.StandardsUsageAnalysis;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.db.StandardsUsageAnalysisService;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsedModel;
import org.trescal.cwms.core.jobs.calibration.views.ReverseTraceabilityModelGenerator;
import org.trescal.cwms.core.jobs.calibration.views.ReverseTraceabilityXlsxView;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;
import org.trescal.cwms.core.tools.DateTools;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

@Component("ReverseTraceabilityMailerTarget")
public class ReverseTraceabilityMailer {

	@Autowired
	private StandardsUsageAnalysisService standardsUsageAnalysisService;
	@Autowired
	private ReverseTraceabilityModelGenerator reverseTraceabilityModelGenerator;
	@Autowired
	private ReverseTraceabilitySettingsService reverseTraceabilitySettingsService;
	@Autowired
	private ReverseTraceabilityXlsxView reverseTraceabilityXlsxView;
	@Autowired
	private MailGroupMemberService mailGroupMemberService;
	@Autowired
	private ScheduledTaskService schTaskServ;

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	public void findfailures() throws IOException {
		Locale locale = LocaleContextHolder.getLocale();
		if (this.schTaskServ.scheduledTaskIsActive(this.getClass().getName(), Thread.currentThread().getStackTrace())) {
			// business subdivisions which have reverse traceability enabled
			Map<Integer, LocalDate> subdivs = this.reverseTraceabilitySettingsService
				.getSubdivsAndDateFromReverseTraceability();
			Boolean emailSuccess = false;
			int failuresSize = 0;
			for (Integer subdivid : subdivs.keySet()) {
				List<StandardsUsageAnalysisDTO> failureCertificates = this.standardsUsageAnalysisService
					.certificatesConnectedToMeasurementStandards(subdivid, subdivs.get(subdivid), locale);
				if (!failureCertificates.isEmpty()) {
					for (StandardsUsageAnalysisDTO dto : failureCertificates) {
						StandardsUsageAnalysis standardsUsageAnalysis = this.standardsUsageAnalysisService
							.createStandardsUsageAnalysis(dto.getInstId(), dto.getCertid(), dto.getCalDate(),
								new Date());
						// generate a reverse traceability report
						SXSSFWorkbook sxxfWorkbook = new SXSSFWorkbook();
						StandardUsedModel model = this.reverseTraceabilityModelGenerator.getModel(dto.getInstId(),
								DateTools.dateFromLocalDate(dto.getCalDate()), new Date(), null, null, null, null);
						this.reverseTraceabilityXlsxView.generateExcelFileForReverseTraceability(sxxfWorkbook, model);
						// store the excel file generated in files tab of the
						// standards
						// usage analysis
						this.standardsUsageAnalysisService.storeFileGenerated(sxxfWorkbook,
								standardsUsageAnalysis.getIdentifier());
					}
					// get a group of contacts that receive a reverse traceability
					// report for a business subdivision
					Set<String> subscribersEmails = this.mailGroupMemberService
							.getMembersEmailsOfGroupForSubdiv(MailGroupType.REVERSE_TRACEABILITY, subdivid);
					// send mail
					emailSuccess = this.standardsUsageAnalysisService.notifySubscribers(subscribersEmails, locale,
							failureCertificates);
					failuresSize = failuresSize + failureCertificates.size();
				}
			}

			String message = failuresSize + " failures found";
			message = message + ((emailSuccess) ? ", notification sent to all subscribers" : "");
			// report results of task run
			this.schTaskServ.reportOnScheduledTaskRun(this.getClass().getName(), Thread.currentThread().getStackTrace(),
					true, message);
		} else {
			this.logger.info(
					"Generate Reverse Traceability Report not running: scheduled task cannot be found or is turned off");
		}

	}

}
