package org.trescal.cwms.core.jobs.calibration.entity.standardused.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsedDto;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public interface StandardUsedService extends BaseService<StandardUsed, Integer> {

	ResultWrapper ajaxAddStandardUsedOnCalibration(int plantId, int calId);

	ResultWrapper ajaxDeleteStandardUsedOnCalibration(int suId);

	String ajaxGetStandardsUsedAsTabbedString(int calId);

	List<StandardUsed> ajaxGetStandardsUsedForCal(int calId);

	List<StandardUsed> createStandardsUsed(Integer[] instrums, Calibration cal);

	List<StandardUsed> getAllStandardsUsedOnCal(int calId);

	List<StandardUsedDto> getStandardsUsedForJobItemAjax(Integer jobItemId);

	Boolean showStandardsUsedButtonForJobItem(JobItem jobItem);
}