package org.trescal.cwms.core.jobs.additionaldemand.enums;

public enum AdditionalDemandStatus {
	UNDEFINED("additionalDemand.undefined", false), NOT_REQUIRED("additionalDemand.notrequired", true),
	REQUIRED("additionalDemand.required", false), DONE("additionalDemand.done", true);

	private String messageCode;
	private boolean completed;

	private AdditionalDemandStatus(String messageCode, boolean completed) {
		this.messageCode = messageCode;
		this.completed = completed;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public boolean isCompleted() {
		return completed;
	}
}