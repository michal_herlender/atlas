package org.trescal.cwms.core.jobs.repair.repaircompletionreport;

import java.io.File;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperationComparator;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.enums.AdjustmentPerformedEnum;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperationComparator;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.tools.EncryptionTools;
import org.trescal.cwms.core.tools.supportedlocale.LocaleFilter;

import lombok.Setter;

@Entity
@Table(name = "repaircompletionreport")
@Setter
public class RepairCompletionReport extends Auditable implements ComponentEntity {

	public final static String repairCompletionReportIdentifierPrefix = "Repair Completion Report  - ";
	public final static String repairCompletionReportAppendixIdentifierPrefix = "Repair Completion Report Appendix  - ";
	private Logger logger = LoggerFactory.getLogger(LocaleFilter.class);

	private int rcrId;
	private String rcrNumber;
	private String internalCompletionComments;
	private String externalCompletionComments;
	private Set<RepairInspectionReport> repairInspectionReports;
	/* From RIR */
	private Set<ValidatedFreeRepairOperation> validatedFreeRepairOperations;
	/* Added in RCR */
	private Set<FreeRepairOperation> freeRepairOperations;
	private Contact validatedBy;
	private Boolean validated;
	private Date validatedOn;
	private File directory;
	private List<Email> sentEmails;
	private AdjustmentPerformedEnum adjustmentPerformed;
	private Date repairCompletionDate;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getRcrId() {
		return rcrId;
	}

	@Column(name = "internalCompletionComments")
	public String getInternalCompletionComments() {
		return internalCompletionComments;
	}

	@Column(name = "externalCompletionComments")
	public String getExternalCompletionComments() {
		return externalCompletionComments;
	}

	@SortComparator(ValidatedFreeRepairOperationComparator.class)
	@OneToMany(mappedBy = "repairCompletionReport", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	public Set<ValidatedFreeRepairOperation> getValidatedFreeRepairOperations() {
		return validatedFreeRepairOperations;
	}

	@OneToMany(mappedBy = "repairCompletionReport", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	public Set<RepairInspectionReport> getRepairInspectionReports() {
		return repairInspectionReports;
	}

	public void setRepairInspectionReports(Set<RepairInspectionReport> repairInspectionReports) {
		this.repairInspectionReports = repairInspectionReports;
	}

	@SortComparator(FreeRepairOperationComparator.class)
	@OneToMany(mappedBy = "repairCompletionReport", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	public Set<FreeRepairOperation> getFreeRepairOperations() {
		return freeRepairOperations;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "validatedby", foreignKey = @ForeignKey(name = "repaircompletionreport_validatedby_FK"))
	public Contact getValidatedBy() {
		return validatedBy;
	}

	@Column(name = "validated")
	public Boolean getValidated() {
		return validated;
	}

	@Column(name = "validatedon", columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getValidatedOn() {
		return validatedOn;
	}

	@Column(name = "repaircompletiondate")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getRepairCompletionDate() {
		return repairCompletionDate;
	}

	@Column(name = "adjustmentperformed")
	@Enumerated(EnumType.STRING)
	public AdjustmentPerformedEnum getAdjustmentPerformed() {
		return adjustmentPerformed;
	}

	@Override
	@Transient
	public Contact getDefaultContact() {
		// TODO Auto-generated method stub
		RepairInspectionReport rir = this.repairInspectionReports.stream().findFirst().get();
		return rir.getJi().getJob().getCon();
	}

	@Override
	@Transient
	public File getDirectory() {
		return this.directory;
	}

	@Override
	@Transient
	public String getIdentifier() {
		return RepairCompletionReport.repairCompletionReportIdentifierPrefix + this.getRcrNumber();
	}

	@Transient
	public String getAppendixIdentifier() {
		return RepairCompletionReport.repairCompletionReportAppendixIdentifierPrefix + this.getRcrNumber();
	}

	@Transient
	public String getRepairCompletionReportAppendixFilePath() {
		String fileName = this.getAppendixIdentifier().concat(".pdf");
		return this.directory.getAbsolutePath().concat(File.separator).concat(fileName);
	}

	@Transient
	public String getEncryptedRepairCompletionReportAppendixFilePath() {
		try {
			return URLEncoder.encode(EncryptionTools.encrypt(this.getRepairCompletionReportAppendixFilePath()),
					"UTF-8");
		} catch (Exception ex) {
			logger.error("Error encrypting filePath", ex);
			return "";
		}
	}

	@Transient
	public String getRepairCompletionReportAppendixFileName() {
		return this.getAppendixIdentifier().concat(".pdf");
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity() {
		RepairInspectionReport rir = this.repairInspectionReports.stream().findFirst().get();
		return rir.getJi().getJob();
	}

	@Override
	@Transient
	public List<Email> getSentEmails() {
		return this.sentEmails;
	}

	@Override
	@Transient
	public boolean isAccountsEmail() {
		return false;
	}

	@Override
	public void setDirectory(File file) {
		this.directory = file;
	}

	@Override
	public void setSentEmails(List<Email> emails) {
		this.sentEmails = emails;
	}

	@Length(max = 30)
	@Column(name = "rcrnumber", nullable = true, columnDefinition = "varchar(30)")
	public String getRcrNumber() {
		return rcrNumber;
	}

	@Override
	public void setLastModified(Date lastModified) {
		// TODO Auto-generated method stub
		super.setLastModified(lastModified);
	}

	@Override
	public void setLastModifiedBy(Contact lastModifiedBy) {
		// TODO Auto-generated method stub
		super.setLastModifiedBy(lastModifiedBy);
	}

}