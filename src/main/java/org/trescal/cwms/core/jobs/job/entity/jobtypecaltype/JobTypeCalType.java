package org.trescal.cwms.core.jobs.job.entity.jobtypecaltype;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

/**
 * Cross table indicating which CalibrationType values are available for each JobType, 
 * and which CalibrationType should be used as the default value for a JobType.  
 * @author Galen
 *
 */

@Entity
@Table(name="jobtypecaltype", uniqueConstraints={@UniqueConstraint(columnNames={"caltypeid","jobtypeid"})})
public class JobTypeCalType {
	private int id;
	private CalibrationType calType;
	private JobType jobType;
	private boolean defaultValue;
	
	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "caltypeid")
	public CalibrationType getCalType()
	{
		return this.calType;
	}
	
	@NotNull
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "jobtypeid", nullable = false)
	public JobType getJobType()
	{
		return this.jobType;
	}
	
	@NotNull
	@Column(name = "defaultvalue", nullable=false, columnDefinition="bit default 0")
	public boolean getDefaultValue() {
		return this.defaultValue;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCalType(CalibrationType calType) {
		this.calType = calType;
	}

	public void setJobType(JobType jobType) {
		this.jobType = jobType;
	}

	public void setDefaultValue(boolean defaultValue) {
		this.defaultValue = defaultValue;
	}
	
}
