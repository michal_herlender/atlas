package org.trescal.cwms.core.jobs.jobitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.documents.images.image.db.ImageService;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnote.JobItemNote;
import org.trescal.cwms.core.jobs.jobitem.projection.service.JobItemViewService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.StringTools;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A superclass for all Job Item Controllers to handle data common across all
 * controllers
 */
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME })
public abstract class JobItemController {

	private static final String NO_OPERATION_PERFORMED_ACTIVITY_NAME = "No operation performed";
	@Value("#{props['cwms.config.jobitem.fast_track_turn']}")
	private int fastTrackTurn;
	@Autowired
	private CalReqService calReqService;
	@Autowired
	private ContactService contactService;
	@Autowired
	protected DepartmentService departmentService;
	@Autowired
	private ImageService imageService;
	@Autowired
	protected JobItemService jobItemService;
	@Autowired
	private JobItemViewService jobItemViewService;
	@Autowired
	protected JobService jobService;
	@Autowired
	protected UserService userService;
	@Autowired
	protected ItemStateService itemStateService;

	@ModelAttribute(Constants.FAST_TRACK_TURN)
	public int fastTrackTurn() {
		return fastTrackTurn;
	}

	@ModelAttribute("privateOnlyNotes")
	public boolean privateOnlyNotes() {
		return JobItemNote.privateOnly;
	}

	/**
	 * Value passed via referenceData call so that POSTs do not cause additional
	 * DB queries
	 */
	private boolean getItemAtCalibrationLocation(JobItem jobItem) {
		return jobItem.getCurrentAddr() == null || jobItem.getCalAddr() == null
				|| jobItem.getCurrentAddr().getAddrid().equals(jobItem.getCalAddr().getAddrid());
	}

	/**
	 * Provides reference data required in all Job Item Controllers
	 * 
	 * @param subdivId
	 *            - the id of the curently logged in subdiv (not necessarily the
	 *            job's subdiv!)
	 */
	protected Map<String, Object> referenceData(Integer jobItemId, Contact contact, Integer subdivId, String username) throws Exception {
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		Locale locale = LocaleContextHolder.getLocale();
		Contact con = this.userService.get(username).getCon();
		Map<String, Object> refData = new HashMap<>();
		refData.put("jobItem", jobItem);
		refData.put("contact", contact);
		refData.put("viewWrapper", this.jobItemViewService.getViewWrapper(jobItemId, locale));
		refData.put("itemAtCalibrationLocation", getItemAtCalibrationLocation(jobItem));
		refData.put("jobFolderLink", this.jobService.get(jobItem.getJob().getJobid()).getDirectory());
		refData.put("accreditedForItemProc", this.jobItemService.userAccreditedForItemProc(contact, jobItem));
		refData.put("requirementtypes", WorkRequirementType.values());
		CalReq calReq = this.calReqService.findCalReqsForJobItem(jobItem);
		refData.put("calReqs", calReq);
		refData.put("calReqString", calReq == null ? null : StringTools.convertCalReqsToSingleString(calReq));
		// as a backup load all instrument model images
		refData.put("jobItemImage", this.imageService.getMostRecentInstrumentImage(jobItem.getInst().getPlantid()));
		refData.put("instModelImage",
				this.imageService.getMostRecentInstrumentModelImage(jobItem.getInst().getModel().getModelid()));
		refData.put("canEditAllocation",
				this.contactService.contactManagesDepartmentOrIsAdmin(contact, DepartmentType.LABORATORY, subdivId));
		// get id of activity 'no operation performed'
		refData.put("noOperationPerformedActivityId",
				itemStateService.findItemActivityByName(NO_OPERATION_PERFORMED_ACTIVITY_NAME).getStateid());
		refData.put("allowedToChangeJobitemInstru", con.getUser().getAuthorities().contains(Permission.JI_CHANGE_INSTRUMENT));
		return refData;
	}


}