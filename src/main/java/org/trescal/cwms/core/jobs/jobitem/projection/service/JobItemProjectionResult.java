package org.trescal.cwms.core.jobs.jobitem.projection.service;

import java.util.List;

import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.instrument.dto.InstrumentProjectionDTO;
import org.trescal.cwms.core.jobs.certificate.dto.CertificateProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.procedure.dto.CapabilityProjectionDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import lombok.Getter;
import lombok.Setter;

/**
 * Provides results of job item projection
 * (main results provided as DTO, but also maps of other DTOs 
 *  which can be useful to the caller in filtering, etc...)
 *
 */
@Getter @Setter
public class JobItemProjectionResult {
	// Always provided
	private List<JobItemProjectionDTO> jiDtos;
	// Optionally provided, if loaded as options
	private List<KeyValueIntegerString> itemStates;
	private List<KeyValueIntegerString> serviceTypes;
	private List<CapabilityProjectionDTO> capabilities;
	private List<ContactProjectionDTO> contractReviewers;
	private List<CertificateProjectionDTO> certificates;
	private List<InstrumentProjectionDTO> instruments;
}
