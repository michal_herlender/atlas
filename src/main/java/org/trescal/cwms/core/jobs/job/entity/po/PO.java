package org.trescal.cwms.core.jobs.job.entity.po;

import lombok.Setter;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.abstractpo.AbstractPO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitempo.InvoiceItemPO;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "po")
@Setter
public class PO extends AbstractPO {

	private boolean active;
	private Contact deactivatedBy;
	private Date deactivatedTime;
	private Set<InvoiceItemPO> invItemPOs;
	private Job job;
	private Job defaultForJob;
	private List<JobItemPO> jobItemPOs;
	List<JobExpenseItemPO> expenseItemPOs;
	private PrebookingJobDetails prebooking;
	private PrebookingJobDetails defaultForPrebooking;

	public PO() {
		super();
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deactivatedby")
	public Contact getDeactivatedBy() {
		return this.deactivatedBy;
	}

	@Column(name = "deactivatedtime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDeactivatedTime() {
		return this.deactivatedTime;
	}

	@OneToMany(mappedBy = "jobPO", cascade = CascadeType.ALL)
	public Set<InvoiceItemPO> getInvItemPOs() {
		return this.invItemPOs;
	}

	/**
	 * @return the job
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobid", nullable = true)
	public Job getJob() {
		return this.job;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "prebookingdetailsid", foreignKey = @ForeignKey(name = "FK_prebookingjobdetails_id"))
	public PrebookingJobDetails getPrebooking() {
		return prebooking;
	}

	/**
	 * @return the jobItemPOs
	 */
	@OneToMany(mappedBy = "po", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<JobItemPO> getJobItemPOs() {
		return this.jobItemPOs;
	}

	/**
	 * @return the expenseItemPOs
	 */
	@OneToMany(mappedBy = "po", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<JobExpenseItemPO> getExpenseItemPOs() {
		return this.expenseItemPOs;
	}

	@Column(name = "active", columnDefinition = "tinyint")
	public boolean isActive() {
		return this.active;
	}

	@Override
	@Transient
	public boolean isBPO() {
		return false;
	}

	@OneToOne(mappedBy = "defaultPO", fetch = FetchType.LAZY)
	public Job getDefaultForJob() {
		return defaultForJob;
	}

	@OneToOne(mappedBy = "defaultPO", fetch = FetchType.LAZY)
	public PrebookingJobDetails getDefaultForPrebooking() {
		return defaultForPrebooking;
	}
}