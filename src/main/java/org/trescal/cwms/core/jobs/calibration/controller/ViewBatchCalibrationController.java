package org.trescal.cwms.core.jobs.calibration.controller;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.db.CourierDespatchTypeService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.dto.AccreditedCalibrationWrapper;
import org.trescal.cwms.core.jobs.calibration.dto.OtherCompleteBatchWrapper;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.db.BatchCalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.QueuedCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.db.QueuedCalibrationService;
import org.trescal.cwms.core.jobs.calibration.enums.BatchCalibrationInvoiceType;
import org.trescal.cwms.core.jobs.calibration.form.ViewBatchCalibrationForm;
import org.trescal.cwms.core.jobs.calibration.form.ViewBatchCalibrationValidator;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.BatchInvoice;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CalibrationLabels;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.spring.model.KeyValue;

import java.io.File;
import java.util.*;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV,
    Constants.HTTPSESS_NEW_FILE_LIST})
public class ViewBatchCalibrationController {
    @Value("#{props['cwms.config.certificate.batch.automatic_printing']}")
    private boolean autoPrinting;
    @Autowired
    private BatchCalibrationService batchCalServ;
    @Autowired
    private CourierDespatchTypeService cdtServ;
    @Autowired
    private CertificateService certServ;
    @Autowired
    private ContactService conServ;
    @Autowired
    private JobService jobServ;
    @Value("#{props['cwms.config.printing.certificate.signing.logged_in_user']}")
    private boolean loggedInUserSigns;
    @Autowired
    private CapabilityAuthorizationService procAccServ;
    @Autowired
    private QueuedCalibrationService queuedCalServ;
    @Autowired
	private SubdivService subdivService;
	@Autowired
	private ViewBatchCalibrationValidator validator;
	@Autowired
	private UserService userService;
	@Autowired
	private BatchInvoice batchInvoice;
	@Autowired
	private CalibrationLabels calibrationLabels;
	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("form")
	protected ViewBatchCalibrationForm formBackingObject(
			@RequestParam(value = "batchid", required = false, defaultValue = "0") Integer batchCalId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		// get request parameters
		BatchCalibration batchCal = this.batchCalServ.findBatchCalibration(batchCalId);

		// throw exception here if no batch is found
		if ((batchCal == null) || (batchCalId == 0)) {
			throw new Exception("Cannot find the requested batch calibration");
        }
        // create command object
        ViewBatchCalibrationForm form = new ViewBatchCalibrationForm();

        // get currently logged-in contact
        Contact currentContact = this.userService.get(username).getCon();
        form.setCurrentContact(currentContact);
        int currentContactId = currentContact.getPersonid();

        // check accreditations for each calibration in batch
        List<AccreditedCalibrationWrapper> calWrappers = new ArrayList<>();
        for (Calibration cal : batchCal.getCals()) {
            int procId = cal.getCapability().getId();
            int calTypeId = cal.getCalType().getCalTypeId();
            val accStat = this.procAccServ.authorizedFor(procId, calTypeId, currentContactId).isRight();
            boolean hasWordFile = false;
            for (Certificate cert : cal.getCerts()) {
                if (this.certServ.wordFileExistsInCertificateDirectory(cert.getCertid())) {
                    hasWordFile = true;
                    break;
                }
			}
            calWrappers.add(new AccreditedCalibrationWrapper(cal, accStat, hasWordFile));
		}

		// get certificates folder for the batch
		Job job = this.jobServ.get(batchCal.getJob().getJobid());
		String certDirPath = job.getDirectory().getAbsolutePath().concat(File.separator).concat("Certificates");
		File certDir = new File(certDirPath);

		// set FBO fields
		form.setBatchCal(batchCal);
        form.setCalWrappers(calWrappers);
        form.setAutoPrinting(this.autoPrinting);
        form.setCanBeDeleted(this.batchCalServ.batchCalibrationCanBeDeleted(batchCal));
        form.setInvType(BatchCalibrationInvoiceType.ALL_BATCH_ITEMS);
        form.setInvTypes(BatchCalibrationInvoiceType.values());
        form.setCanCertify(this.queuedCalServ.checkAccreditedForBatchCertificates(batchCal.getId(), currentContactId));
        form.setJobCertDirectory(certDir);
        form.setBatchRequiresCourier(this.isCourierRequired(batchCal));

        // determine whether to print recall date on labels (use system default if reactivating)
        form.setIncludeRecallDateOnLabels(false);

        return form;
    }

	public int getAmountOfMissingFiles(BatchCalibration batchCal) {
		int missingFiles = 0;

		if (!batchCal.getProcess().getApplicationComponent()) {
			for (Calibration cal : batchCal.getCals()) {
				for (Certificate cert : cal.getCerts()) {
					if (!this.certServ.wordFileExistsInCertificateDirectory(cert.getCertid())) {
						missingFiles++;
					}
				}
			}
		}

		return missingFiles;
	}

    public boolean isCourierRequired(BatchCalibration batch) {
        if (batch != null && batch.getJob() != null && batch.getJob().getReturnOption() != null
            && batch.getJob().getReturnOption().getMethod() != null) {
            String transport = batch.getJob().getReturnOption().getMethod().getMethodText();
            return transport.equalsIgnoreCase("courier");
        }
        return false;
    }

    protected void onBind(ViewBatchCalibrationForm form) {
        // get all selected batch ids
        List<Integer> selectedBatchIds = form.getSelectedBatchIds();
        // if there is no selected batches
        if (selectedBatchIds == null)
            // create an empty list
            selectedBatchIds = new ArrayList<>();
        // add the batch being viewed into the list
        selectedBatchIds.add(form.getBatchCal().getId());
        // set the list back into the form
        form.setSelectedBatchIds(selectedBatchIds);
	}

	@RequestMapping(value = "/viewbatchcalibration.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(
			@RequestParam(value = "doctype", required = false, defaultValue = "html") String docType,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList,
			@ModelAttribute("form") ViewBatchCalibrationForm form, BindingResult bindingResult) throws Exception {
        onBind(form);
        if (bindingResult.hasErrors())
            return referenceData(form, bindingResult, subdivDto);
        Contact currentContact = this.userService.get(username).getCon();
        Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
        BatchCalibration batch = form.getBatchCal();
        List<Integer> batchIds = form.getSelectedBatchIds();
        // load up other batches being completed so their timestamps can be
        // updated
        List<BatchCalibration> selectedBatches = new ArrayList<>();
        for (Integer batchId : batchIds)
            selectedBatches.add(this.batchCalServ.findBatchCalibration(batchId));
        // if manually printing
        if (!form.isAutoPrinting())
            // if new password has been set
            if (form.getCurrentContact().getEncryptedPassword() == null)
                // set password to current contact
                this.conServ.setEncryptedPassword(form.getCurrentContact().getPersonid(), form.getPassword());
        if (form.isCertify()) {
            // if logged in user doesn't sign, use the batch owner
            Contact signatory = (this.loggedInUserSigns) ? form.getCurrentContact() : batch.getOwner();
            // create a map to store queued cal ids and cert ids
            HashMap<Integer, Integer> map = new HashMap<>();
            // get the calibrations requiring certs from the batch
            List<QueuedCalibration> queuedCals = this.queuedCalServ.getActiveQueuedCalibrationsForBatches(batchIds);
            // for each calibration requiring a cert
            for (QueuedCalibration qc : queuedCals) {
                Certificate cert;
                // if there isn't a cert already
                if (qc.getQueuedCal().getCerts().size() < 1) {
                    // create the cert object in the database - duration was
                    // already hard coded to 12 so have also hard coded the
                    // interval unit to months
                    cert = this.certServ.createCertFromCal(signatory, qc.getQueuedCal(), 12, true, allocatedSubdiv,
                        IntervalUnit.MONTH, CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE, null);
                }
                // otherwise
                else {
					// get the cert that has already been created
					cert = qc.getQueuedCal().getCerts().get(0);
					// if cert has not yet been signed
					if (!cert.getStatus().equals(CertStatusEnum.SIGNED))
						// sign the cert
						this.certServ.signCertificate(cert, signatory);
				}
				// add queued cal and cert id to map
				map.put(qc.getId(), cert.getCertid());
			}
			// then actually send them all to be generated/printed
			this.queuedCalServ.printActiveQueuedCalibrationsForBatches(batchIds, form.getCurrentContact(), map);
			// update times of selected batches
			for (BatchCalibration bc : selectedBatches)
				bc.setLastCertify(new Date());
		}
		if (form.isDespatch()) {
			CourierDespatchType cdt = null;
            // if there has been a courier despatch type selected
            if (form.getCdtId() != null)
                // load up the courier despatch type
                cdt = this.cdtServ.get(form.getCdtId());
            this.batchCalServ.despatchBatchCalibrations(batchIds, cdt, form.getConsignmentNo(), currentContact,
                allocatedSubdiv, sessionNewFileList);
            // update times of selected batches
            for (BatchCalibration bc : selectedBatches)
                bc.setLastDespatch(new Date());
        }
        // no longer supported
        if (form.isLabels()) {
            // update times of selected batches
            for (BatchCalibration bc : selectedBatches)
                bc.setLastLabels(new Date());
        }
        /*
         * Job sheet printing removed for various reasons. If ever uncommented, must
         * provide options to choose an invoice address and attempt to forecast a job
         * completion date!
         */
        /*
         * if (form.isJobSheet()) { // reload the job - stop lazy loading problems Job
         * job = this.jobServ.findJob(batch.getJob().getJobid()); Document doc =
         * this.jobServ.printJobSheet(job, null, null); if (doc.isExists()) { Printer
         * printer = this.printServ.getDefaultPrinterForContact(this.sessionServ
         * .getCurrentContact().getPersonid());
         * this.printJobServ.printFile(doc.getFileName(), printer, 1); } }
         */
        // update/merge each of the selected batches
        for (BatchCalibration bc : selectedBatches)
            this.batchCalServ.mergeBatchCalibration(bc);
		return new ModelAndView(new RedirectView("viewjob.htm?jobid=" + batch.getJob().getJobid()));
	}

    @RequestMapping(value = "/viewbatchcalibration.htm", method = RequestMethod.GET)
    protected ModelAndView referenceData(@ModelAttribute("form") ViewBatchCalibrationForm form,
                                         BindingResult bindingResult,
                                         @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
        Map<String, Object> refData = new HashMap<>();

        BatchCalibration batch = form.getBatchCal();

        List<BatchCalibration> otherBatches = this.batchCalServ.getOtherCompleteBatchesOnJob(batch.getJob().getJobid(),
            batch.getId());
        List<OtherCompleteBatchWrapper> otherBatchWraps = new ArrayList<>();

        for (BatchCalibration otherBatch : otherBatches) {
            int missingFiles = this.getAmountOfMissingFiles(otherBatch);
            otherBatchWraps.add(new OtherCompleteBatchWrapper(otherBatch, missingFiles));
        }
        refData.put("otherCompleteBatches", otherBatchWraps);
        refData.put("missingFiles", this.getAmountOfMissingFiles(batch));
        refData.put("courierDespatchTypes", this.cdtServ.getAllSorted());
        // if company is business company OR if invoicing has been disabled as
        // company setting, don't allow invoicing from batch screen
		Company comp = batch.getJob().getCon().getSub().getComp();
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		refData.put("allowInvoicing", comp.getCompanyRole() != CompanyRole.BUSINESS && batchInvoice.parseValue(
				batchInvoice.getValueHierarchical(Scope.COMPANY, comp, allocatedSubdiv.getComp()).getValue()));
		// get company default setting for cal labels
		refData.put("allowLabelPrinting", calibrationLabels.parseValue(
				calibrationLabels.getValueHierarchical(Scope.COMPANY, comp, allocatedSubdiv.getComp()).getValue()));
		return new ModelAndView("trescal/core/jobs/calibration/viewbatchcalibration", refData);
	}
}