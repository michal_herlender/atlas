package org.trescal.cwms.core.jobs.calibration.entity.calibration;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalibrationActivityLink;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalibrationToCalLinkComparator;
import org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.QueuedCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsedComparator;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "calibration")
public class Calibration extends Allocated<Subdiv> {

	private BatchCalibration batch;
    private Integer batchPosition;
    private CalibrationClass calClass;
    private LocalDate calDate;
    private CalibrationProcess calProcess;
    private CalibrationType calType;
    private List<Certificate> certs;
    private String certTemplate;
    private Contact completedBy;
    private Date completeTime;
    private int id;
    private Set<CalLink> links;
    private Capability capability;
    private Set<QueuedCalibration> queued;
    private Set<StandardUsed> standardsUsed;
    private Contact startedBy;
    private Date startTime;
    private CalibrationStatus status;
    private WorkInstruction workInstruction;
    private Address calAddress;
    private String xindiceKey;
    private List<CalibrationActivityLink> jobitemActivityLinks;

    public Calibration() {
		batchPosition = 0;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "batchid")
	public BatchCalibration getBatch() {
		return this.batch;
	}

	@Column(name = "batchposition")
	public Integer getBatchPosition() {
		return this.batchPosition;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "caladdressid")
	public Address getCalAddress() {
		return calAddress;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "calclass", nullable = false)
	public CalibrationClass getCalClass() {
		return this.calClass;
	}

	@NotNull
    @Column(name = "caldate", nullable = false, columnDefinition = "date")
    public LocalDate getCalDate() {
		return this.calDate;
	}

	/**
	 * @return the calProcess
	 */
	@NotNull(message = "A calibration process must be selected")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "calprocessid")
	public CalibrationProcess getCalProcess() {
		return this.calProcess;
	}

	/**
	 * @return the calType
	 */
	@NotNull(message = "A calibration type must be selected")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "caltypeid")
	public CalibrationType getCalType() {
		return this.calType;
	}

	/**
	 * @return the certs
	 */
	@OneToMany(mappedBy = "cal")
	public List<Certificate> getCerts() {
		return this.certs;
	}

	public void setCalDate(LocalDate calDate) {
		this.calDate = calDate;
	}

	/**
	 * @return the completedBy
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "completedby")
	public Contact getCompletedBy() {
		return this.completedBy;
	}

	/**
	 * @return the completeTime
	 */
	@Column(name = "completetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCompleteTime() {
		return this.completeTime;
	}


	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
    public int getId() {
        return this.id;
    }

    @OneToMany(mappedBy = "cal", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @SortComparator(CalibrationToCalLinkComparator.class)
    public Set<CalLink> getLinks() {
        return this.links;
    }

    /**
     * @return the proc
     */
    @NotNull(message = "A procedure must be selected")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "capabilityId")
    public Capability getCapability() {
        return this.capability;
    }

    @OneToMany(mappedBy = "queuedCal")
    public Set<QueuedCalibration> getQueued() {
        return this.queued;
    }

    /**
     * @return the standardsUsed
     */
    @OneToMany(mappedBy = "calibration", cascade = CascadeType.ALL)
	@SortComparator(StandardUsedComparator.class)
	public Set<StandardUsed> getStandardsUsed() {
		return this.standardsUsed;
	}

	/**
	 * @return the startedBy
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "startedby")
	public Contact getStartedBy() {
		return this.startedBy;
	}

	/**
	 * @return the startTime
	 */
	@Column(name = "starttime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getStartTime() {
		return this.startTime;
	}

	/**
	 * @return the status
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "calibrationstatus")
	public CalibrationStatus getStatus() {
		return this.status;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "workInstructionId", foreignKey = @ForeignKey(name = "FK_calibration_workinstruction"))
	public WorkInstruction getWorkInstruction() {
		return this.workInstruction;
	}

	@Length(max = 100)
	@Column(name = "certtemplate", length = 100)
	public String getCertTemplate() {
		return this.certTemplate;
	}

	@OneToMany(mappedBy = "calibration", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<CalibrationActivityLink> getJobitemActivityLinks() {
		return jobitemActivityLinks;
	}

	public void setBatch(BatchCalibration batch) {
		this.batch = batch;
	}

	public void setBatchPosition(Integer batchPosition) {
		this.batchPosition = batchPosition;
	}

	public void setCalClass(CalibrationClass calClass) {
		this.calClass = calClass;
	}

	/**
	 * @return the xindiceKey
	 */
	@Length(max = 100)
	@Column(name = "xindicekey", length = 100)
	public String getXindiceKey() {
		return this.xindiceKey;
	}

	/**
	 * @param calProcess the calProcess to set
	 */
	public void setCalProcess(CalibrationProcess calProcess) {
		this.calProcess = calProcess;
	}

	/**
	 * @param calType the calType to set
	 */
	public void setCalType(CalibrationType calType) {
		this.calType = calType;
	}

	/**
	 * @param certs the certs to set
	 */
	public void setCerts(List<Certificate> certs) {
		this.certs = certs;
	}

	public void setCertTemplate(String certTemplate) {
		this.certTemplate = certTemplate;
	}

	/**
	 * @param completedBy the completedBy to set
	 */
	public void setCompletedBy(Contact completedBy) {
		this.completedBy = completedBy;
	}

	/**
	 * @param completeTime the completeTime to set
	 */
	public void setCompleteTime(Date completeTime) {
		this.completeTime = completeTime;
	}

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    public void setLinks(Set<CalLink> links) {
        this.links = links;
    }

    /**
     * @param capability the proc to set
     */
    public void setCapability(Capability capability) {
        this.capability = capability;
    }

    public void setQueued(Set<QueuedCalibration> queued) {
        this.queued = queued;
    }

    /**
     * @param standardsUsed the standardsUsed to set
     */
    public void setStandardsUsed(Set<StandardUsed> standardsUsed) {
		this.standardsUsed = standardsUsed;
	}

	/**
	 * @param startedBy the startedBy to set
	 */
	public void setStartedBy(Contact startedBy) {
		this.startedBy = startedBy;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(CalibrationStatus status) {
		this.status = status;
	}

	public void setWorkInstruction(WorkInstruction workInstruction) {
		this.workInstruction = workInstruction;
	}

	/**
	 * @param xindiceKey the xindiceKey to set
	 */
	public void setXindiceKey(String xindiceKey) {
		this.xindiceKey = xindiceKey;
	}

	public void setCalAddress(Address calAddress) {
		this.calAddress = calAddress;
	}

	public void setJobitemActivityLinks(List<CalibrationActivityLink> jobitemActivityLinks) {
		this.jobitemActivityLinks = jobitemActivityLinks;
	}
}