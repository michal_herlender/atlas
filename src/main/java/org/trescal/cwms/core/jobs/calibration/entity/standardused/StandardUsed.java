package org.trescal.cwms.core.jobs.calibration.entity.standardused;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "standardused")
public class StandardUsed extends Auditable {
    private Calibration calibration;
    private Integer calPercentage;
    private int id;
    private Instrument inst;
    private LocalDate nextCalDueDate;

    /**
     * @return the calibration
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "calibration")
    public Calibration getCalibration() {
        return this.calibration;
    }

	@Column(name = "calpercentage")
	public Integer getCalPercentage()
	{
		return this.calPercentage;
	}

	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
    }

    /**
     * @return the inst
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "inst")
    public Instrument getInst() {
        return this.inst;
    }

    @Column(name = "recalldate", columnDefinition = "date")
    public LocalDate getNextCalDueDate() {
        return this.nextCalDueDate;
    }

    /**
     * @param calibration the calibration to set
     */
    public void setCalibration(Calibration calibration) {
        this.calibration = calibration;
	}

	public void setCalPercentage(Integer calPercentage)
	{
		this.calPercentage = calPercentage;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
        this.id = id;
    }

    /**
     * @param inst the inst to set
     */
    public void setInst(Instrument inst) {
        this.inst = inst;
    }

    public void setNextCalDueDate(LocalDate nextCalDueDate) {
        this.nextCalDueDate = nextCalDueDate;
	}
}