package org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem_;

@Repository("OnBehalfItemDao")
public class OnBehalfItemDaoImpl extends BaseDaoImpl<OnBehalfItem, Integer> implements OnBehalfItemDao {
	@Override
	protected Class<OnBehalfItem> getEntity() {
		return OnBehalfItem.class;
	}

	@Override
	public OnBehalfItem getOnBehalfItemByJobitem(JobItem ji) {
		return getFirstResult(cb -> {
			CriteriaQuery<OnBehalfItem> cq = cb.createQuery(OnBehalfItem.class);
			Root<OnBehalfItem> onBehalfItem = cq.from(OnBehalfItem.class);
			cq.where(cb.equal(onBehalfItem.get(OnBehalfItem_.jobitem), ji));
			return cq;
		}).orElse(null);
	}
}