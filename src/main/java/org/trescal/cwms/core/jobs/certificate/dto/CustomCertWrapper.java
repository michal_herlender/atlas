package org.trescal.cwms.core.jobs.certificate.dto;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public class CustomCertWrapper
{
	private Integer calId;
	private int calTypeId;
	private int duration;
	private int jobItemId;
	private boolean selected;

	public CustomCertWrapper(JobItem ji)
	{
		this.jobItemId = ji.getJobItemId();
		this.selected = false;
		this.duration = 12;
		this.calTypeId = ji.getCalType().getCalTypeId();
	}

	public Integer getCalId()
	{
		return this.calId;
	}

	public int getCalTypeId()
	{
		return this.calTypeId;
	}

	public int getDuration()
	{
		return this.duration;
	}

	public int getJobItemId()
	{
		return this.jobItemId;
	}

	public boolean isSelected()
	{
		return this.selected;
	}

	public void setCalId(Integer calId)
	{
		this.calId = calId;
	}

	public void setCalTypeId(int calTypeId)
	{
		this.calTypeId = calTypeId;
	}

	public void setDuration(int duration)
	{
		this.duration = duration;
	}

	public void setJobItemId(int jobItemId)
	{
		this.jobItemId = jobItemId;
	}

	public void setSelected(boolean selected)
	{
		this.selected = selected;
	}

}