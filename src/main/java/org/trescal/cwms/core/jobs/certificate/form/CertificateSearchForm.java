package org.trescal.cwms.core.jobs.certificate.form;

import lombok.Data;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.certificate.dto.CertificateSearchWrapper;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.util.Date;

@Data
public class CertificateSearchForm {
	private String bc;
	private Date calDate1;
	private Date calDate2;
	private boolean calDateBetween;
	private Integer calTypeId;
	private String certNo;
	private Integer coid;
	private String desc;
	private Integer descid;
	private Date issueDate1;
	private Date issueDate2;
	private boolean issueDateBetween;
	private String jobNo;
	private String mfr;
	private Integer mfrid;
	private String model;
	private int pageNo;
	private Integer personid;
	private String plantNo;
	private int resultsPerPage = 20;
	private PagedResultSet<CertificateSearchWrapper> rs;
	private String serialNo;
	private CertStatusEnum status;
	private String tpCertNo;
	private Integer tpId;
	private Integer modelid;
	private CalibrationVerificationStatus calibrationVerificationStatus;
	private Date jobItemDateComplete1;
	private Date jobItemDateComplete2;
	private boolean jobItemCompletionDateBetween;

	public boolean hasModelSearch() {
		boolean result = false;
		// model (by id or name)
		if ((modelid != null) && (modelid != 0)) {
			result = true;
		} else if ((model != null) && !model.trim().isEmpty()) {
			result = true;
		}
		return result;
	}

	public boolean hasSubfamilySearch() {
		boolean result = false;
		// subfamily (by id or name)
		if ((descid != null) && (descid != 0)) {
			result = true;
		} else if ((desc != null) && !desc.trim().isEmpty()) {
			result = true;
		}
		return result;
	}

	public boolean hasMfrSearch() {
		boolean result = false;
		// mfr (by id or name)
		if ((mfrid != null) && (mfrid != 0)) {
			result = true;
		} else if ((mfr != null) && !mfr.trim().isEmpty()) {
			result = true;
		}
		return result;
	}

	public boolean hasInstrumentSearch() {
        boolean result = (bc != null) && !bc.trim().isEmpty();
        // barcode = plant id
        // serial no
		if ((serialNo != null) && !serialNo.trim().isEmpty()) {
			result = true;
		}
		// plant no
		if ((plantNo != null) && !plantNo.trim().isEmpty()) {
			result = true;
		}

		return result;
	}

	public boolean hasJobSearch() {
		return jobNo != null && !jobNo.trim().isEmpty();
	}
	
	public boolean hasJobitemSearch() {
		return jobItemDateComplete1 != null;
	}

	public boolean hasCompanySearch() {
		return coid != null;
	}

	public boolean hasSubdivSearch() {
		return false;
	}

	public boolean hasAddressSearch() {
		return false;
	}

	public boolean hasContactSearch() {
		return personid != null;
	}

}