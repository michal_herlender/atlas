package org.trescal.cwms.core.jobs.calibration.entity.calreqhistory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Entity
@DiscriminatorValue("override")
public class CalReqOverride extends CalReqHistory
{
	private Instrument overrideOnInst;
	private JobItem overrideOnJobItem;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "overrideoninst")
	public Instrument getOverrideOnInst()
	{
		return this.overrideOnInst;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "overrideonji")
	public JobItem getOverrideOnJobItem()
	{
		return this.overrideOnJobItem;
	}

	public void setOverrideOnInst(Instrument overrideOnInst)
	{
		this.overrideOnInst = overrideOnInst;
	}

	public void setOverrideOnJobItem(JobItem overrideOnJobItem)
	{
		this.overrideOnJobItem = overrideOnJobItem;
	}
}
