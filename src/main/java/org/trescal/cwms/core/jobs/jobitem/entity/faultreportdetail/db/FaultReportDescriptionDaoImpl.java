package org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportDescription;

@Repository
public class FaultReportDescriptionDaoImpl extends BaseDaoImpl<FaultReportDescription, Integer> implements FaultReportDescriptionDao {

	@Override
	protected Class<FaultReportDescription> getEntity() {
		return FaultReportDescription.class;
	}


}
