package org.trescal.cwms.core.jobs.repair.component.enums;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum RepairComponentStateEnum {
	TO_ORDER("repairreport.component.status.toorder",true),
	IN_STOCK("repairreport.component.status.instock",true),
	USED("repairreport.component.status.used",false),
	IGNORED("repairreport.component.status.ignored",false);

	private String messageCode;
	private ReloadableResourceBundleMessageSource messageSource;
	private boolean rirStatus;

	private RepairComponentStateEnum(String messageCode, boolean rirStatus) {
		this.messageCode = messageCode;
		this.rirStatus = rirStatus;
	}

	@Component
	private static class MessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (RepairComponentStateEnum e : EnumSet.allOf(RepairComponentStateEnum.class))
				e.messageSource = messageSource;
		}
	}

	public String getMessage() {
		return messageSource.getMessage(messageCode, null, this.toString(), LocaleContextHolder.getLocale());
	}

	public String getMessageCode() {
		return messageCode;
	}
	
	public String getValue() {
		Locale locale = LocaleContextHolder.getLocale();
		if (messageSource != null) {
			return messageSource.getMessage(this.messageCode, null, this.name(), locale);
		}
		return this.toString();
	}

	public boolean isRirStatus() {
		return rirStatus;
	}
}
