package org.trescal.cwms.core.jobs.jobitem.projection.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.projection.CompanyProjectionDTO;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.contract.entity.contract.db.ContractService;
import org.trescal.cwms.core.instrument.dto.InstrumentProjectionDTO;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.certificate.dto.CertificateProjectionDTO;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.db.ContractReviewService;
import org.trescal.cwms.core.jobs.job.projection.ClientPurchaseOrderProjectionDTO;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionCommand;
import org.trescal.cwms.core.jobs.job.projection.service.JobProjectionService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.db.JobItemPOService;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemPriceProjectionDto;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.db.JobCostingItemService;
import org.trescal.cwms.core.pricing.jobcost.projection.JobCostingItemProjectionDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.db.PurchaseOrderJobItemService;
import org.trescal.cwms.core.pricing.purchaseorder.projection.PurchaseOrderItemProjectionDTO;
import org.trescal.cwms.core.procedure.dto.CapabilityProjectionDTO;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceTypeDto;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import java.util.*;
import java.util.stream.Collectors;

@Service @Slf4j
public class JobItemProjectionServiceImpl implements JobItemProjectionService {
	@Autowired
	private CompanyService companyService;
	@Autowired
	private ContractService contractService;
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private ContactService contactService;
    @Autowired
    private ContractReviewService contractReviewService;
    @Autowired
    private InstrumService instrumServ;
    @Autowired
    private ItemStateService itemStateService;
    @Autowired
    private JobCostingItemService jobCostingItemService;
    @Autowired
    private JobItemService jobItemService;
    @Autowired
    private JobItemPOService jiPoService;
    @Autowired
    private CapabilityService capabilityService;
    @Autowired
    private PurchaseOrderJobItemService pojiService;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private JobItemActionService jobItemActionService;
    @Autowired
    private JobProjectionService jobProjectionService;

    @Override
    public List<JobItemProjectionDTO> getProjectionDTOsForJobIds(Collection<Integer> jobIds, JobItemProjectionCommand command) {
        List<JobItemProjectionDTO> jobItemDtos = Collections.emptyList();
		if (!jobIds.isEmpty()) {
			jobItemDtos = this.jobItemService.getJobItemProjectionDTOs(jobIds, null);
			loadJobItemProjections(jobItemDtos, command);
		}
		return jobItemDtos;
	}

	@Override
	public JobItemProjectionResult getProjectionDTOsForJobItemIds(Collection<Integer> jobItemIds, JobItemProjectionCommand command) {
		if (log.isDebugEnabled()) log.debug("jobItemIds.size() : "+jobItemIds.size());
		List<JobItemProjectionDTO> jobItemDtos = Collections.emptyList();
		if (!jobItemIds.isEmpty()) {
			jobItemDtos = this.jobItemService.getJobItemProjectionDTOs(null, jobItemIds);
		}
		JobItemProjectionResult result = loadJobItemProjections(jobItemDtos, command);
		if (log.isDebugEnabled()) log.debug("jobItemDtos.size() : "+jobItemDtos.size());
		return result;
	}

	@Override
	public JobItemProjectionResult loadJobItemProjections(List<JobItemProjectionDTO> jiDtos, JobItemProjectionCommand command) {
		JobItemProjectionResult result = new JobItemProjectionResult();
		result.setJiDtos(jiDtos);
		if (command.getLoadInstruments())
			result.setInstruments(loadInstruments(jiDtos, command));

		if (command.getLoadServiceTypes())
			result.setServiceTypes(loadServiceTypes(jiDtos, command));

		if (command.getLoadItemStates())
			result.setItemStates(loadItemStates(jiDtos, command));

		if (command.getLoadOnBehalfCompany())
			loadOnBehalfCompanies(jiDtos, command);

		if (command.getLoadCapabilities())
			result.setCapabilities(loadCapabilities(jiDtos));
		
		if (command.getLoadContractReviewers())
			result.setContractReviewers(loadContractReviewers(jiDtos, command));
		
		if (command.getLoadContracts())
			loadContracts(jiDtos);

		if (command.getLoadContractReviewPrice() ||
				command.getLoadCertificates() ||
				command.getLoadClientPOs() ||
				command.getLoadCostingItems()) {
			Map<Integer, JobItemProjectionDTO> mapJobItems = jiDtos.stream()
					.collect(Collectors.toMap(JobItemProjectionDTO::getJobItemId, jiDto -> jiDto));

			if (command.getLoadContractReviewPrice())
				loadContractReviewPrices(mapJobItems);
			
			if (command.getLoadCertificates())
				result.setCertificates(loadCertificates(mapJobItems));
			
			if (command.getLoadClientPOs())
				loadClientPOs(mapJobItems);
			
			if (command.getLoadCostingItems())
				loadCostingItems(mapJobItems);
			
			if (command.getLoadSupplierPOs())
				loadSupplierPOs(mapJobItems);
		}
			
		return result;
	}

	private List<InstrumentProjectionDTO> loadInstruments(List<JobItemProjectionDTO> jobItemDtos, JobItemProjectionCommand command) {
		Locale locale = command.getLocale(); 
				
		// Supports the case of an instrument having multiple job items in list
		MultiValuedMap<Integer, JobItemProjectionDTO> mapJobItemByPlantId = new HashSetValuedHashMap<>();
		jobItemDtos.forEach(jiDto -> mapJobItemByPlantId.put(jiDto.getPlantid(), jiDto));
		Set<Integer> plantIds = mapJobItemByPlantId.keySet();
		List<InstrumentProjectionDTO> instDtos = this.instrumServ.getInstrumentProjectionDTOs(locale, plantIds);
		instDtos.forEach(
				instDto -> mapJobItemByPlantId.get(instDto.getPlantid()).forEach(
					jiDto -> jiDto.setInstrument(instDto)));
		return instDtos;
	}

	private List<KeyValueIntegerString> loadServiceTypes(List<JobItemProjectionDTO> jobItemDtos, JobItemProjectionCommand command) {
		Locale locale = command.getLocale();
		
		Set<Integer> serviceTypeIds = jobItemDtos.stream().filter(jiDto -> jiDto.getServiceTypeId() != null)
				.map(JobItemProjectionDTO::getServiceTypeId).collect(Collectors.toSet());
		Map<Integer, ServiceTypeDto> serviceTypes = this.serviceTypeService
				.getDTOs(serviceTypeIds, locale).stream()
				.collect(Collectors.toMap(ServiceTypeDto::getId, dto -> dto));
		jobItemDtos.stream().filter(jiDto -> jiDto.getServiceTypeId() != null)
				.forEach(jiDto -> jiDto.setServiceType(serviceTypes.get(jiDto.getServiceTypeId())));
		List<KeyValueIntegerString> serviceTypesList = new ArrayList<>();
		serviceTypes.forEach((k,v)->serviceTypesList.add(new KeyValueIntegerString(k, v.getShortName())));
		return serviceTypesList;
	}

	private List<KeyValueIntegerString> loadItemStates(List<JobItemProjectionDTO> jobItemDtos, JobItemProjectionCommand command) {
		Locale locale = command.getLocale();
		
		Set<Integer> stateIds = jobItemDtos.stream().filter(jiDto -> jiDto.getStateId() != null)
				.map(JobItemProjectionDTO::getStateId).collect(Collectors.toSet());
		List<KeyValueIntegerString> itemStates = this.itemStateService.getDTOList(stateIds, locale);
		Map<Integer, KeyValueIntegerString> stateMap = itemStates.stream()
				.collect(Collectors.toMap(KeyValue::getKey, dto -> dto));
		jobItemDtos.stream().filter(jiDto -> jiDto.getStateId() != null)
				.forEach(jiDto -> jiDto.setState(stateMap.get(jiDto.getStateId())));
		return itemStates;
	}
	
	private void loadContracts(List<JobItemProjectionDTO> jobItemDtos) {
		MultiValuedMap<Integer, JobItemProjectionDTO> mapJobItemByContract = new HashSetValuedHashMap<>();
		jobItemDtos.stream()
			.filter(jiDto -> jiDto.getContractId() != null)
			.forEach(jiDto -> mapJobItemByContract.put(jiDto.getContractId(), jiDto));
		Set<Integer> contractIds =  mapJobItemByContract.keySet();
		if (log.isDebugEnabled()) log.debug("Loading DTOs for "+contractIds.size()+ " contracts");
		
		List<KeyValueIntegerString> contracts = this.contractService.getKeyValueList(contractIds);
		for (KeyValueIntegerString contractDto : contracts) {
			for (JobItemProjectionDTO jiDto : mapJobItemByContract.get(contractDto.getKey())) {
				jiDto.setContract(contractDto);
			}
		}
		
	}

	private void loadOnBehalfCompanies(List<JobItemProjectionDTO> jobItemDtos, JobItemProjectionCommand command) {
		Integer allocatedCompanyId = command.getAllocatedCompanyId();
		
		Set<Integer> companyIds = jobItemDtos.stream().filter(jiDto -> jiDto.getOnBehalfCompanyId() != null)
				.map(JobItemProjectionDTO::getOnBehalfCompanyId).collect(Collectors.toSet());
		if (log.isDebugEnabled()) log.debug("Loading DTOs for "+companyIds.size()+ " on behalf of companies");
		
		Map<Integer, CompanyProjectionDTO> companyMap = this.companyService.getDTOList(companyIds, allocatedCompanyId)
				.stream().collect(Collectors.toMap(CompanyProjectionDTO::getCoid, dto -> dto));
		jobItemDtos.stream().filter(jiDto -> jiDto.getOnBehalfCompanyId() != null)
				.forEach(jiDto -> jiDto.setOnBehalfCompany(companyMap.get(jiDto.getOnBehalfCompanyId())));
	}

	private List<CapabilityProjectionDTO> loadCapabilities(List<JobItemProjectionDTO> jobItemDtos) {
        MultiValuedMap<Integer, JobItemProjectionDTO> mapJobItemByCapability = new HashSetValuedHashMap<>();
        jobItemDtos.stream()
            .filter(jiDto -> jiDto.getCapabilityId() != null)
            .forEach(jiDto -> mapJobItemByCapability.put(jiDto.getCapabilityId(), jiDto));
        Set<Integer> capabilityIds = mapJobItemByCapability.keySet();

        List<CapabilityProjectionDTO> capabilities = this.capabilityService.getCapabilityProjectionDtos(capabilityIds);
        for (CapabilityProjectionDTO capability : capabilities) {
            for (JobItemProjectionDTO jiDto : mapJobItemByCapability.get(capability.getProcId())) {
                jiDto.setCapability(capability);
            }
        }
        return capabilities;
    }
	
	private List<ContactProjectionDTO> loadContractReviewers(List<JobItemProjectionDTO> jobItemDtos, JobItemProjectionCommand command) {
		MultiValuedMap<Integer, JobItemProjectionDTO> mapJobItemByReviewer = new HashSetValuedHashMap<>();
		jobItemDtos.stream()
			.filter(jiDto -> jiDto.getContractReviewById() != null)
			.forEach(jiDto -> mapJobItemByReviewer.put(jiDto.getContractReviewById(), jiDto));
		Set<Integer> personIds = mapJobItemByReviewer.keySet();
		List<ContactProjectionDTO> contactDtos = this.contactService.getContactProjectionDTOs(personIds, command.getAllocatedCompanyId());
		for (ContactProjectionDTO contactDto : contactDtos) {
			for (JobItemProjectionDTO jiDto : mapJobItemByReviewer.get(contactDto.getPersonid())) {
				jiDto.setContractReviewBy(contactDto);
			}
		}
		return contactDtos;
	}
	
	private void loadContractReviewPrices(Map<Integer, JobItemProjectionDTO> mapJobItems) {
		Collection<Integer> jobItemIds = mapJobItems.keySet();
		List<JobItemPriceProjectionDto> priceDtos = this.contractReviewService.getPriceProjectionDTOs(jobItemIds);
		for (JobItemPriceProjectionDto priceDto : priceDtos) {
			JobItemProjectionDTO jiDto = mapJobItems.get(priceDto.getJobItemId());
			jiDto.setPriceContractReview(priceDto);
		}
		
		
	}
	
	private List<CertificateProjectionDTO> loadCertificates(Map<Integer, JobItemProjectionDTO> mapJobItems) {
		Collection<Integer> jobItemIds = mapJobItems.keySet();
		List<CertificateProjectionDTO> certDtos = this.certificateService.getCertificatesForJobItemIds(jobItemIds);
		for (CertificateProjectionDTO certDto : certDtos) {
			JobItemProjectionDTO jiDto = mapJobItems.get(certDto.getJobItemId());
			certDto.setJobItemDto(jiDto);
			if (jiDto.getCertificates() == null)
				jiDto.setCertificates(new ArrayList<>());
			jiDto.getCertificates().add(certDto);
		}
		return certDtos;
	}
	
	private void loadClientPOs(Map<Integer, JobItemProjectionDTO> mapJobItems) {
		Collection<Integer> jobItemIds = mapJobItems.keySet();
		List<ClientPurchaseOrderProjectionDTO> jobItemPos = this.jiPoService.getProjectionDTOs(null, jobItemIds);

		if (log.isDebugEnabled()) log.debug("Found "+jobItemPos.size()+ " jobItemPos");
		for (ClientPurchaseOrderProjectionDTO poDto : jobItemPos) {
			JobItemProjectionDTO jiDto = mapJobItems.get(poDto.getItemId());
			if (jiDto == null) {
				log.error("No jiDto found for jobItemId "+poDto.getItemId());
				log.error("jobItemId in original set: "+jobItemIds.contains(poDto.getItemId()));
			}
			else {
				if (jiDto.getJobItemPos() == null)
					jiDto.setJobItemPos(new ArrayList<>());
			jiDto.getJobItemPos().add(poDto);
			}
		}
		
	}
	
	private void loadCostingItems(Map<Integer, JobItemProjectionDTO> mapJobItems) {
		Collection<Integer> jobItemIds = mapJobItems.keySet();
		List<JobCostingItemProjectionDTO> ciDtos = this.jobCostingItemService.getProjectionDTOsForJobItems(jobItemIds);
		
		if (log.isDebugEnabled()) log.debug("Found "+ciDtos.size()+ " costingItems");
		for (JobCostingItemProjectionDTO ciDto : ciDtos) {
			JobItemProjectionDTO jiDto = mapJobItems.get(ciDto.getJobItemId());
			if (jiDto.getCostingItems() == null)
				jiDto.setCostingItems(new ArrayList<>());
			jiDto.getCostingItems().add(ciDto);
		}
		
	}
	
	private void loadSupplierPOs(Map<Integer, JobItemProjectionDTO> mapJobItems) {
		Collection<Integer> jobItemIds = mapJobItems.keySet();
		List<PurchaseOrderItemProjectionDTO> poiDtos = this.pojiService.getProjectionDTOsForJobItems(jobItemIds);
		
		if (log.isDebugEnabled()) log.debug("Found "+poiDtos.size()+ " supplier po items");
		for (PurchaseOrderItemProjectionDTO poiDto : poiDtos) {
			JobItemProjectionDTO jiDto = mapJobItems.get(poiDto.getJobItemId());
			if (jiDto.getSupplierPos() == null)
				jiDto.setSupplierPos(new ArrayList<>());
			jiDto.getSupplierPos().add(poiDto);
		}
	}

	@Override
	public void loadAdditionalProjections(JobItemProjectionDTO jiDto, Integer allocatedCompanyId, Locale locale) {
		Integer jobItemId = jiDto.getJobItemId();

		JobProjectionCommand jobCommand = new JobProjectionCommand(locale, allocatedCompanyId, JobProjectionCommand.JOB_LOAD_CONTACT.class, JobProjectionCommand.JOB_LOAD_CURRENCY.class, JobProjectionCommand.JOB_LOAD_STATUS.class);
		JobProjectionDTO jobDto = this.jobProjectionService.getDTOByJobId(jiDto.getJobid(), jobCommand);

		jiDto.setJob(jobDto);
		// Exclude deleted actions from overlay display
		jiDto.setActions(this.jobItemActionService.getProjectionDTOsForJobItem(jobItemId, false, locale));
		jiDto.setContractReviews(this.contractReviewService.getProjectionDTOsForJobItem(jobItemId));
	}

}
