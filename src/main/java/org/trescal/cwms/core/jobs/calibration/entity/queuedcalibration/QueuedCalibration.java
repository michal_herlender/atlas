package org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "queuedcalibration")
public class QueuedCalibration {
    private boolean active;
    private Integer certId;
    private int id;
    private Contact printedBy;
    private boolean printNow;
    private Calibration queuedCal;
    private boolean reprint;
    private Date timeCompleted;
    private Date timeQueued;

    @Column(name = "certid")
    public Integer getCertId() {
        return this.certId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "printedbyid")
    public Contact getPrintedBy() {
        return this.printedBy;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "queuedcalid")
    public Calibration getQueuedCal() {
        return this.queuedCal;
    }

    @Column(name = "timecompleted", columnDefinition = "datetime")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getTimeCompleted() {
        return this.timeCompleted;
    }

    @Column(name = "timequeued", columnDefinition = "datetime")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getTimeQueued() {
        return this.timeQueued;
    }

    @Column(name = "active", nullable = false, columnDefinition = "tinyint")
    public boolean isActive() {
        return this.active;
    }

	@Column(name = "printnow", nullable = false, columnDefinition="tinyint")
	public boolean isPrintNow()
	{
		return this.printNow;
	}

	@Column(name = "reprint", nullable = false, columnDefinition="tinyint")
	public boolean isReprint()
	{
		return this.reprint;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public void setCertId(Integer certId)
	{
		this.certId = certId;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setPrintedBy(Contact printedBy)
	{
		this.printedBy = printedBy;
	}

	public void setPrintNow(boolean printNow)
	{
		this.printNow = printNow;
	}

	public void setQueuedCal(Calibration queuedCal)
	{
		this.queuedCal = queuedCal;
	}

	public void setReprint(boolean reprint)
	{
		this.reprint = reprint;
	}

	public void setTimeCompleted(Date timeCompleted)
	{
		this.timeCompleted = timeCompleted;
	}

	public void setTimeQueued(Date timeQueued)
	{
		this.timeQueued = timeQueued;
	}
}