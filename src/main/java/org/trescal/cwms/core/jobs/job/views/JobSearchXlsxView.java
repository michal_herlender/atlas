package org.trescal.cwms.core.jobs.job.views;

import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.tools.reports.view.XlsxViewDecorator;

/**
 * Replacement for job export (JobItemsXlsxView) with reorganized fields / less
 * data loading
 * 
 * This is still an entity based job item export, as it relies on the job search
 * results.
 * 
 * Note, it's used in both the internal ERP and customer portal job results!
 * 
 * History: 2019-05-05 : Created class - Galen Beck
 * History: 2020-04-23 : Updated class - Mohammed Amnay
 *
 */
@Component
public class JobSearchXlsxView extends AbstractJobItemXlsxStreamingView {

	@Autowired
	private LocaleResolver localeResolver;
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SXSSFWorkbook streamingWorkbook = (SXSSFWorkbook) workbook;
		Sheet sheet = workbook.createSheet("Data");
		XlsxViewDecorator decorator = new XlsxViewDecorator(sheet, streamingWorkbook, true, null);
		@SuppressWarnings("unchecked")
		Collection<JobProjectionDTO> jobs = (Collection<JobProjectionDTO>) model.get("jobs");
		Locale locale = this.localeResolver.resolveLocale(request);

		Map<Integer, Integer> columnWidthMap = writeLabels(decorator, locale, false);
		writeResults(decorator, jobs, false);
		decorator.autoSizeColumns();
		resizeSelectedColumns(sheet, columnWidthMap);
		response.setHeader("Content-Disposition", "attachment; filename=\"jobsearch_export.xlsx\"");
	}

}
