package org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.system.entity.status.Status;

@Entity
@DiscriminatorValue("calibration")
public class CalibrationStatus extends Status
{
	public static final String AWAITING_WORK = "Awating work";
	public static final String ON_GOING = "On-going";
	public static final String CANCELLED = "Cancelled";
	public static final String COMPLETE = "Complete";
	public static final String UNABLE_TO_CALIBRATE = "Unable to calibrate";
	public static final String ON_HOLD = "On hold";
	
	private List<Calibration> calibrations;

	/**
	 * @return the calibrations
	 */
	@OneToMany(mappedBy = "status", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public List<Calibration> getCalibrations()
	{
		return this.calibrations;
	}

	/**
	 * @param calibrations the calibrations to set
	 */
	public void setCalibrations(List<Calibration> calibrations)
	{
		this.calibrations = calibrations;
	}
}