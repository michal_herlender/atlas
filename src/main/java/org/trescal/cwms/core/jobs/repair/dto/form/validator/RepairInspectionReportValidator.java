package org.trescal.cwms.core.jobs.repair.dto.form.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.trescal.cwms.core.jobs.repair.dto.FreeRepairComponentDTO;
import org.trescal.cwms.core.jobs.repair.dto.FreeRepairOperationDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairInspectionReportFormDTO;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import java.lang.reflect.Method;
import java.util.Iterator;

@Component
public class RepairInspectionReportValidator extends AbstractBeanValidator {

	@Autowired
	private FreeRepairOperationDTOValidator operationValidator;
	@Autowired
	private FreeRepairComponentDTOValidator componentValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(RepairInspectionReportFormDTO.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		RepairInspectionReportFormDTO rirForm = (RepairInspectionReportFormDTO) target;
		super.validate(target, errors);

		/* General validation */
		if (StringUtils.isBlank(rirForm.getClientDescription()))
			errors.rejectValue("clientDescription", "repairinspection.validator.mandatory", "Mandatory field");

		if (StringUtils.isBlank(rirForm.getInternalInspection()))
			errors.rejectValue("internalInspection", "repairinspection.validator.mandatory", "Mandatory field");

		if (rirForm.getLeadTime() == null)
			errors.rejectValue("leadTime", "repairinspection.validator.mandatory", "Mandatory field");

		if (rirForm.getEstimatedReturnDate() == null)
			errors.rejectValue("estimatedReturnDate", "repairinspection.validator.mandatory", "Mandatory field");
		
		if (rirForm.getMisuse() == null)
			errors.rejectValue("misuse", "repairinspection.validator.mandatory", "Mandatory field");

		// remove deleted ops from list, and validate
		int i = 0;
		if (rirForm.getOperationsForm() != null && !rirForm.getOperationsForm().isEmpty())
			for (Iterator<FreeRepairOperationDTO> iterator = rirForm.getOperationsForm().iterator(); iterator
					.hasNext();) {
				FreeRepairOperationDTO op = (FreeRepairOperationDTO) iterator.next();
				if (allFieldsAreNull(op))
					iterator.remove();
				else {
					try {
						errors.pushNestedPath("operationsForm[" + i + "]");
						ValidationUtils.invokeValidator(this.operationValidator, op, errors);

						// check the cost
						if (rirForm.getCompletedByTechnician() != null && rirForm.getCompletedByTechnician()
								&& op.getCost() == null)
							errors.rejectValue("cost", "repairinspection.validator.mandatory", "Mandatory field");

						i++;
					} finally {
						errors.popNestedPath();
					}
				}
			}

		// remove deleted components from list, and validate
		i = 0;
		if (rirForm.getComponentsForm() != null && !rirForm.getComponentsForm().isEmpty())
			for (Iterator<FreeRepairComponentDTO> iterator = rirForm.getComponentsForm().iterator(); iterator
					.hasNext();) {
				FreeRepairComponentDTO comp = (FreeRepairComponentDTO) iterator.next();
				if (allFieldsAreNull(comp))
					iterator.remove();
				else {
					try {
						errors.pushNestedPath("componentsForm[" + i + "]");
						ValidationUtils.invokeValidator(this.componentValidator, comp, errors);

						// check the cost
						if (rirForm.getCompletedByTechnician() != null && rirForm.getCompletedByTechnician()
								&& comp.getCost() == null)
							errors.rejectValue("cost", "repairinspection.validator.mandatory", "Mandatory field");

						i++;
					} finally {
						errors.popNestedPath();
					}
				}
			}
	}

	/**
	 * checks if all the getters return null
	 */
	public boolean allFieldsAreNull(Object o) {
		try {
			for (Method m : o.getClass().getDeclaredMethods())
				if (m.getName().startsWith("get") && m.invoke(o) != null)
					return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

}
