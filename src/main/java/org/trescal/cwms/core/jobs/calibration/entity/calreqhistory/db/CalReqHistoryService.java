package org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.db;

import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqEdit;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqHistory;

public interface CalReqHistoryService
{
	void deleteCalReqHistory(CalReqHistory calreqhistory);

	void deleteCalReqHistoryImmediatelyAffectingJobItem(int jobItemId);

	ResultWrapper fetchCompleteCalReqHistoryForInstrument(int plantId);

	ResultWrapper fetchCompleteCalReqHistoryForJobItem(int jobItemId);

	List<CalReqEdit> findAllEditsForCalReq(CalReq cr);

	CalReqHistory findCalReqHistory(int id);

	List<CalReqHistory> getAllCalReqHistorys();

	void insertCalReqHistory(CalReqHistory calreqhistory);

	void updateCalReqHistory(CalReqHistory calreqhistory);
}