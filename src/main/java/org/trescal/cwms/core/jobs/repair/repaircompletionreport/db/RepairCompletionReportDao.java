package org.trescal.cwms.core.jobs.repair.repaircompletionreport.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;

public interface RepairCompletionReportDao extends BaseDao<RepairCompletionReport, Integer> {

	void refresh(RepairCompletionReport rcr);

	RepairCompletionReport getByJobitem(Integer jobitemid);
	
	RepairCompletionReport getValidatedRcrByJobItem(int jobItemId);
}