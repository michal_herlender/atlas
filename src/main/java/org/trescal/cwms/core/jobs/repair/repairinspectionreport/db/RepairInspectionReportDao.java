package org.trescal.cwms.core.jobs.repair.repairinspectionreport.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;

public interface RepairInspectionReportDao extends BaseDao<RepairInspectionReport, Integer> {
	RepairInspectionReport getRirByJobItemIdAndSubdivId(Integer jobitemid, Integer subdivid);

	void refrech(RepairInspectionReport rir);

	List<RepairInspectionReport> getAllRirByJobItemId(int jobItemId);
	
	RepairInspectionReport getLatestValidatedRir(int jobItemId);
	
	RepairInspectionReport getValidatedRirByJobItemAndValidationDate(int jobItemId, int allocatedSubdiv, boolean completedByTechnician);
}