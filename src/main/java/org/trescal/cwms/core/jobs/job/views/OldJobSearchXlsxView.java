package org.trescal.cwms.core.jobs.job.views;

import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.reports.view.XlsxViewDecorator;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

/**
 * Replacement for job export (JobItemsXlsxView) with reorganized fields / less
 * data loading
 * 
 * This is still an entity based job item export, as it relies on the job search
 * results.
 * 
 * Note, it's used in both the internal ERP and customer portal job results!
 * 
 * History: 2019-05-05 : Created class - Galen Beck
 *
 */
@Component
public class OldJobSearchXlsxView extends AbstractXlsxStreamingView {

	@Autowired
	private LocaleResolver localeResolver;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private TranslationService translationService;

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SXSSFWorkbook streamingWorkbook = (SXSSFWorkbook) workbook;
		Sheet sheet = workbook.createSheet("Data");
		XlsxViewDecorator decorator = new XlsxViewDecorator(sheet, streamingWorkbook, true, null);
		Locale locale = this.localeResolver.resolveLocale(request);
		@SuppressWarnings("unchecked")
		Set<Job> jobs = (Set<Job>) model.get("jobs");

		writeLabels(decorator, locale);
		writeResults(decorator, locale, jobs);
		decorator.autoSizeColumns();
		response.setHeader("Content-Disposition", "attachment; filename=\"jobsearch_export.xlsx\"");
	}

	private void writeLabels(XlsxViewDecorator decorator, Locale locale) {
		int rowIndex = 0;
		int colIndex = 0;
		CellStyle boldStyle = decorator.getStyleHolder().getHeaderStyle();

		String labelJobNo = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_JOB_NO, null,
				JobExportLabelDefinitions.TEXT_JOB_NO, locale);
		decorator.createCell(rowIndex, colIndex++, labelJobNo, boldStyle);

		String labelItemNo = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_ITEM_NO, null,
				JobExportLabelDefinitions.TEXT_ITEM_NO, locale);
		decorator.createCell(rowIndex, colIndex++, labelItemNo, boldStyle);

		String labelBarcode = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_BARCODE, null,
				JobExportLabelDefinitions.TEXT_BARCODE, locale);
		decorator.createCell(rowIndex, colIndex++, labelBarcode, boldStyle);

		String labelPlantNo = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_PLANTNO, null,
				JobExportLabelDefinitions.TEXT_PLANTNO, locale);
		decorator.createCell(rowIndex, colIndex++, labelPlantNo, boldStyle);

		String labelSerialNo = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_SERIALNO, null,
				JobExportLabelDefinitions.TEXT_SERIALNO, locale);
		decorator.createCell(rowIndex, colIndex++, labelSerialNo, boldStyle);

		String labelInstModelName = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_INSTRUMENT_MODEL_NAME,
				null, JobExportLabelDefinitions.TEXT_INSTRUMENT_MODEL_NAME, locale);
		decorator.createCell(rowIndex, colIndex++, labelInstModelName, boldStyle);

		String labelCustomerDescription = this.messageSource.getMessage(
				JobExportLabelDefinitions.CODE_CUSTOMER_DESCRIPTION, null,
				JobExportLabelDefinitions.TEXT_CUSTOMER_DESCRIPTION, locale);
		decorator.createCell(rowIndex, colIndex++, labelCustomerDescription, boldStyle);

		String labelServiceType = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_SERVICE_TYPE, null,
				JobExportLabelDefinitions.TEXT_SERVICE_TYPE, locale);
		decorator.createCell(rowIndex, colIndex++, labelServiceType, boldStyle);

		String labelStatus = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_STATUS, null,
				JobExportLabelDefinitions.TEXT_STATUS, locale);
		String labelJobItem = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_JOB_ITEM, null,
				JobExportLabelDefinitions.TEXT_JOB_ITEM, locale);
		decorator.createCell(rowIndex, colIndex++, labelStatus + " - " + labelJobItem, boldStyle);

		String labelPickupDate = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_PICKUP_DATE, null,
				JobExportLabelDefinitions.TEXT_PICKUP_DATE, locale);
		decorator.createCell(rowIndex, colIndex++, labelPickupDate, boldStyle);

		String labelReceiptDate = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_RECEIPT_DATE, null,
				JobExportLabelDefinitions.TEXT_RECEIPT_DATE, locale);
		decorator.createCell(rowIndex, colIndex++, labelReceiptDate, boldStyle);

		String labelDateIn = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_DATE_IN, null,
				JobExportLabelDefinitions.TEXT_DATE_IN, locale);
		decorator.createCell(rowIndex, colIndex++, labelDateIn, boldStyle);

		String labelDueDate = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_DUE_DATE, null,
				JobExportLabelDefinitions.TEXT_DUE_DATE, locale);
		decorator.createCell(rowIndex, colIndex++, labelDueDate, boldStyle);

		String labelTurnaround = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_TURNAROUND, null,
				JobExportLabelDefinitions.TEXT_TURNAROUND, locale);
		decorator.createCell(rowIndex, colIndex++, labelTurnaround, boldStyle);

		String labelCompletionDate = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_COMPLETION_DATE, null,
				JobExportLabelDefinitions.TEXT_COMPLETION_DATE, locale);
		decorator.createCell(rowIndex, colIndex++, labelCompletionDate, boldStyle);

		String labelClientRef = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_CLIENT_REF, null,
				JobExportLabelDefinitions.TEXT_CLIENT_REF, locale);
		String labelJob = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_JOB, null,
				JobExportLabelDefinitions.TEXT_JOB, locale);

		decorator.createCell(rowIndex, colIndex++, labelClientRef + " - " + labelJob, boldStyle);
		decorator.createCell(rowIndex, colIndex++, labelClientRef + " - " + labelJobItem, boldStyle);
		
		String labelContractNo = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_CONTRACT_NO, null,
				JobExportLabelDefinitions.TEXT_CONTRACT_NO, locale);
		decorator.createCell(rowIndex, colIndex++, labelContractNo, boldStyle);

		String labelClientCompany = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_CLIENT_COMPANY, null,
				JobExportLabelDefinitions.TEXT_CLIENT_COMPANY, locale);
		decorator.createCell(rowIndex, colIndex++, labelClientCompany, boldStyle);

		String labelSubdivision = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_SUBDIVISION, null,
				JobExportLabelDefinitions.TEXT_SUBDIVISION, locale);
		decorator.createCell(rowIndex, colIndex++, labelSubdivision, boldStyle);

		String labelContact = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_CONTACT, null,
				JobExportLabelDefinitions.TEXT_CONTACT, locale);
		decorator.createCell(rowIndex, colIndex++, labelContact, boldStyle);

		String labelOnBehalfOf = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_ON_BEHALF_COMPANY, null,
				JobExportLabelDefinitions.TEXT_ON_BEHALF_COMPANY, locale);
		decorator.createCell(rowIndex, colIndex++, labelOnBehalfOf, boldStyle);
	}

	private void writeResults(XlsxViewDecorator decorator, Locale locale, Set<Job> jobs) {
		int rowIndex = 1;

		Locale fallbackLocale = this.supportedLocaleService.getPrimaryLocale();
		CellStyle styleDate = decorator.getStyleHolder().getDateStyle();
		CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();
		CellStyle styleText = decorator.getStyleHolder().getTextStyle();

		for (Job job : jobs) {
			for (JobItem jobItem : job.getItems()) {
				int colIndex = 0;

				decorator.createCell(rowIndex, colIndex++, job.getJobno(), styleText);
				decorator.createCell(rowIndex, colIndex++, jobItem.getItemNo(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, jobItem.getInst().getPlantid(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, jobItem.getInst().getPlantno(), styleText);
				decorator.createCell(rowIndex, colIndex++, jobItem.getInst().getSerialno(), styleText);

				String instModelName = InstModelTools.instrumentModelNameViaTranslations(jobItem.getInst(), false,
						locale, fallbackLocale);
				decorator.createCell(rowIndex, colIndex++, instModelName, styleText);
				decorator.createCell(rowIndex, colIndex++, jobItem.getInst().getCustomerDescription(), styleText);
				decorator.createCell(rowIndex, colIndex++, getServiceType(jobItem, locale), styleText);
				decorator.createCell(rowIndex, colIndex++, getStatus(jobItem, locale), styleText);
				decorator.createCell(rowIndex, colIndex++, DateTools.dateFromZonedDateTime(job.getPickupDate()), styleDate);
				decorator.createCell(rowIndex, colIndex++, DateTools.dateFromZonedDateTime(job.getReceiptDate()), styleDate);
				decorator.createCell(rowIndex, colIndex++, job.getRegDate(), styleDate);
				decorator.createCell(rowIndex, colIndex++, jobItem.getDueDate(), styleDate);
				decorator.createCell(rowIndex, colIndex++, jobItem.getTurn(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, jobItem.getDateComplete(), styleDate);
				decorator.createCell(rowIndex, colIndex++, job.getClientRef(), styleDate);
				decorator.createCell(rowIndex, colIndex++, jobItem.getClientRef(), styleDate);
				decorator.createCell(rowIndex, colIndex++,((jobItem.getContract() != null ? jobItem.getContract().getContractNumber() : "")), styleDate);
				decorator.createCell(rowIndex, colIndex++, job.getCon().getSub().getComp().getConame(), styleDate);
				decorator.createCell(rowIndex, colIndex++, job.getCon().getSub().getSubname(), styleDate);
				decorator.createCell(rowIndex, colIndex++, job.getCon().getName(), styleDate);
				decorator.createCell(rowIndex, colIndex++, getOnBehalfOfCompany(jobItem), styleDate);

				rowIndex++;
			}
		}
	}

	private String getServiceType(JobItem jobItem, Locale locale) {
		String serviceType = null;
		if (jobItem.getCalType() != null) {
			serviceType = this.translationService
					.getCorrectTranslation(jobItem.getCalType().getServiceType().getShortnameTranslation(), locale);
		}
		return serviceType;
	}

	private String getStatus(JobItem jobItem, Locale locale) {
		String status = null;
		if (jobItem.getState() != null) {
			status = this.translationService.getCorrectTranslation(jobItem.getState().getTranslations(), locale);
		}
		return status;
	}

	private String getOnBehalfOfCompany(JobItem jobItem) {
		String result = null;
		if (jobItem.getOnBehalf() != null) {
			result = jobItem.getOnBehalf().getCompany().getConame();
		}
		return result;
	}
}
