package org.trescal.cwms.core.jobs.repair.operation.enums;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum RepairOperationTypeEnum {
	INTERNAL("repairoperation.type.internal"),
	EXTERNAL("repairoperation.type.external"),
	EXTERNAL_WITH_CALIBRATION("repairoperation.type.externalwithcal");

	private ReloadableResourceBundleMessageSource messages;
	private String messageCode;

	private RepairOperationTypeEnum(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getValue() {
		Locale locale = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.name(), locale);
		}
		return this.toString();
	}

	@Component
	public static class RepairOperationTypeMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;

		@PostConstruct
		public void postConstruct() {
			for (RepairOperationTypeEnum rote : EnumSet.allOf(RepairOperationTypeEnum.class))
				rote.setMessageSource(messages);
		}
	}

	public void setMessageSource(ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}
}
