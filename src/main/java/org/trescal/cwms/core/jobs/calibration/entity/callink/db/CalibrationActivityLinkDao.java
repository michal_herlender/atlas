package org.trescal.cwms.core.jobs.calibration.entity.callink.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalibrationActivityLink;

public interface CalibrationActivityLinkDao extends BaseDao<CalibrationActivityLink, Integer> {}