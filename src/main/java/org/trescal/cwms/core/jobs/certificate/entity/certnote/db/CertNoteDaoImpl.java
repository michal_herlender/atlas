package org.trescal.cwms.core.jobs.certificate.entity.certnote.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.certificate.entity.certnote.CertNote;

@Repository("CertNoteDao")
public class CertNoteDaoImpl extends BaseDaoImpl<CertNote, Integer> implements CertNoteDao {
	
	@Override
	protected Class<CertNote> getEntity() {
		return CertNote.class;
	}
}