package org.trescal.cwms.core.jobs.jobitem.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Builder
@Getter
public class JobItemActivityResponseDTO {

	private Integer activityId;

	private Integer jobItemId;

	private String startStatus;
	
	private String description;

	private String startedBy;

	@JsonFormat(pattern = "yyyy-MM-dd['T'HH:mm:ss.SSS][XXX]")
	private LocalDateTime startStamp;

}
