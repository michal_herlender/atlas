package org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.JobInstructionLink;

public interface JobInstructionLinkService extends
		BaseService<JobInstructionLink, Integer>
{
	List<JobInstructionLink> getAllForJob(Job job);
}