package org.trescal.cwms.core.jobs.job.entity.bpo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;
import org.trescal.cwms.core.tools.DateTools;

import java.util.Date;
import java.util.List;

/**
 * Task that e-mails out a simple reminder when the {@link BPO} for a company,
 * subdivision or contact is close to expiring.
 * 
 * TODO May need to be a Service for transaction demarcation if resurrected
 * 
 * @author jamiev
 */
@Component("BPOExpirationMailerTarget")
public class BPOExpirationMailer
{
	@Autowired
	private BPOService bpoServ;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private ScheduledTaskService schTaskServ;
	@Value("${cwms.config.bpo.expiration_mailto}")
	private String toAddress;

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public void findExpiringBPOs()
	{
		if (this.schTaskServ.scheduledTaskIsActive(this.getClass().getName(), Thread.currentThread().getStackTrace()))
		{
			boolean emailSuccess = false;

			List<BPO> BPOs = this.bpoServ.getExpiringBPOs();

			if (BPOs.size() > 0)
			{
				String mailText = "The following BPOs are due to expire shortly:\n\n\n";

				for (BPO bpo : BPOs) {
					mailText = mailText + bpo.getPoNumber() + " - ";

					if (bpo.getComment().trim().length() > 0) {
						mailText = mailText + bpo.getComment() + " - ";
					}

					mailText = mailText + DateTools.df.format(bpo.getDurationTo()) + " - ";

					if (bpo.getCompany() == null) {
						if (bpo.getSubdiv() == null) {
							mailText = mailText + bpo.getContact().getName();
							mailText = mailText + " ("
								+ bpo.getContact().getSub().getSubname();
							mailText = mailText
								+ ", "
									+ bpo.getContact().getSub().getComp().getConame()
									+ ")\n";
						}
						else
						{
							mailText = mailText + bpo.getSubdiv().getSubname()
									+ " ("
									+ bpo.getSubdiv().getComp().getConame()
									+ ")\n";
						}
					}
					else
					{
						mailText = mailText + bpo.getCompany().getConame()
								+ "\n";
					}

					bpo.setExpirationWarningSent(true);
					bpo.setExpirationWarningDate(new Date());
					this.bpoServ.update(bpo);
				}

				emailSuccess = this.emailServ.sendBasicEmail("Expiring BPOs", "bpo@antech.org.uk", this.toAddress, mailText);
			}

			// report results of task run
			String message = BPOs.size() + " expiring BPOs found";
			message = message
					+ ((emailSuccess) ? ", notification sent to "
							+ this.toAddress : "");
			this.schTaskServ.reportOnScheduledTaskRun(this.getClass().getName(), Thread.currentThread().getStackTrace(), true, message);
		}
		else
		{
			this.logger.info("BPO Expiration Mailer not running: scheduled task cannot be found or is turned off");
		}
	}
}