package org.trescal.cwms.core.jobs.calibration.views;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
import org.trescal.cwms.core.jobs.calibration.dto.StandardsUsageAnalysisDTO;
import org.trescal.cwms.core.tools.reports.view.XlsxCellStyleHolder;
import org.trescal.cwms.core.tools.reports.view.XlsxViewDecorator;

@Component
public class StandardsUsageAnalysisXlsxView extends AbstractXlsxStreamingView {

	private static final boolean automaticAutosize = true;

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		SXSSFWorkbook sxxfWorkbook = (SXSSFWorkbook) workbook;
		XlsxCellStyleHolder styleHolder = new XlsxCellStyleHolder(workbook);
		// create style for date cells
		DataFormat dateFormat = workbook.createDataFormat();
		CellStyle dateStyle = workbook.createCellStyle();
		dateStyle.setDataFormat(dateFormat.getFormat("d/m/yyyy"));
		// create new Excel sheet
		Sheet sheetStandardsUsageAnalysis = workbook.createSheet("Standards Usage");

		XlsxViewDecorator decoratorStandardsUsageAnalysis = new XlsxViewDecorator(sheetStandardsUsageAnalysis,
				sxxfWorkbook, automaticAutosize, styleHolder);

		createLabelsStandardsUsageAnalysis(decoratorStandardsUsageAnalysis);

		response.setHeader("Content-Disposition", "attachment; filename=\"standardsusagesearch_export.xlsx\"");

		populateStandardsUsageAnalysis(decoratorStandardsUsageAnalysis, model, dateStyle);

		decoratorStandardsUsageAnalysis.autoSizeColumns();
	}

	private void createLabelsStandardsUsageAnalysis(XlsxViewDecorator decorator) {
		int colIndex = 0;
		int rowIndex = 0;
		CellStyle styleHeader = decorator.getStyleHolder().getHeaderStyle();
		decorator.createCell(rowIndex, colIndex++, "Barcode", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Certificate Number", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Status", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Start Date", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "End Date", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Plant Number", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Serial Number", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Instrument Model Name", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Brand", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Model Number", styleHeader);

	}

	private void populateStandardsUsageAnalysis(XlsxViewDecorator decorator, Map<String, Object> model,
			CellStyle styleDate) {

		@SuppressWarnings("unchecked")
		List<StandardsUsageAnalysisDTO> dtos = (List<StandardsUsageAnalysisDTO>) model
				.get("standardsUsageAnalysisDTOs");
		CellStyle styleText = decorator.getStyleHolder().getTextStyle();
		CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();

		int rowIndex = 1;
		for (StandardsUsageAnalysisDTO dto : dtos) {
			int colIndex = 0;
			decorator.createCell(rowIndex, colIndex++, dto.getInstId(), styleInteger);
			decorator.createCell(rowIndex, colIndex++, dto.getCertno(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getStatus().getStatus(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getStartDate(), styleDate);
			decorator.createCell(rowIndex, colIndex++, dto.getFinishDate(), styleDate);
			decorator.createCell(rowIndex, colIndex++, dto.getPlantNo(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getSerialNo(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getInstrumentModelName(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getBrand(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getModelNo(), styleText);
			rowIndex++;
		}

	}

}
