package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;
import org.trescal.cwms.core.jobs.generalserviceoperation.form.GeneralServiceOperationForm;

public interface GsoDocumentService extends BaseService<GsoDocument, Integer> {
	
	void updateGsoDocument(GeneralServiceOperationForm form);
	
	GsoDocument findGsoDocumentByDocumentNumber(String documentNumber);
	
}
