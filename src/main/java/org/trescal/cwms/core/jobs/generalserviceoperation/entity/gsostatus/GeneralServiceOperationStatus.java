package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsostatus;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.system.entity.status.Status;

@Entity
@DiscriminatorValue("generalserviceoperation")
public class GeneralServiceOperationStatus extends Status {

	public static final String ON_GOING = "On-going";

	public static final String CANCELLED = "Cancelled";

	public static final String COMPLETE = "Complete";

	public static final String ON_HOLD = "On hold";

	private List<GeneralServiceOperation> generalServiceOperations;

	@OneToMany(mappedBy = "status", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	public List<GeneralServiceOperation> getGeneralServiceOperations() {
		return this.generalServiceOperations;
	}

	public void setGeneralServiceOperations(List<GeneralServiceOperation> generalServiceOperations) {
		this.generalServiceOperations = generalServiceOperations;
	}
}
