package org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemandaction.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemandaction.JobItemDemandAction;

@Service
public class JobItemDemandActionServiceImpl extends BaseServiceImpl<JobItemDemandAction, Integer>
		implements JobItemDemandActionService {

	@Autowired
	private JobItemDemandActionDao jobItemDemandActionDao;

	@Override
	protected BaseDao<JobItemDemandAction, Integer> getBaseDao() {
		return jobItemDemandActionDao;
	}
}