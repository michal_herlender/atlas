package org.trescal.cwms.core.jobs.calibration.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.StandardsUsageAnalysis;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.db.StandardsUsageAnalysisService;
import org.trescal.cwms.core.jobs.calibration.form.UpdateStandardsUsageAnalysisForm;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.tools.DateTools;

@Controller
@IntranetController
public class EditStandardsUsageAnalysisController {

	@Autowired
	private StandardsUsageAnalysisService standardsUsageAnalysisService;
	private static final String JFS_VIEW = "viewstandardsusageanalysis.htm?id=";
	public static final String FORM_NAME = "form";

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
	}

	@ModelAttribute(FORM_NAME)
	protected UpdateStandardsUsageAnalysisForm formBackingObject() throws Exception {
		UpdateStandardsUsageAnalysisForm form = new UpdateStandardsUsageAnalysisForm();
		return form;
	}

	@RequestMapping(value = "/updatestandardsusage.htm", method = RequestMethod.POST)
	protected ModelAndView onEditSubmit(Model model, @ModelAttribute("form") UpdateStandardsUsageAnalysisForm form) {

		StandardsUsageAnalysis standardsUsageAnalysis = standardsUsageAnalysisService
				.get(form.getStandardsUsageAnalysisId());
		if (standardsUsageAnalysis != null) {
			this.standardsUsageAnalysisService.editStandardsUsageAnalysis(standardsUsageAnalysis, form);
		}
		return new ModelAndView(new RedirectView(JFS_VIEW + standardsUsageAnalysis.getId() + "&loadtab=update-tab"));
	}
}
