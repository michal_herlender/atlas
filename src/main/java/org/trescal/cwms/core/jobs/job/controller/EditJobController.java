package org.trescal.cwms.core.jobs.job.controller;

import static org.trescal.cwms.core.tools.DateTools.localDateTimeFromZonedDateTime;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.form.EditJobForm;
import org.trescal.cwms.core.jobs.job.form.EditJobFormValidator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.CustomLocalDateTimeEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrencyFormatter;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_CONTACT })
public class EditJobController {

	@Autowired
	private AddressService addressService;
	@Autowired
	private ContactService conServ;
	@Autowired
	private LocationService locationService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private JobService jobService;
	@Autowired
	private SupportedCurrencyFormatter currencyFormatter;
	@Autowired
	private TransportOptionService transportOptionService;
	@Autowired
	private EditJobFormValidator validator;
	@Autowired
	private UserService userService;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) throws Exception {
		binder.setValidator(validator);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.registerCustomEditor(LocalDateTime.class,
				new CustomLocalDateTimeEditor(DateTimeFormatter.ISO_DATE_TIME));
	}

	@ModelAttribute("form")
	public EditJobForm formBackingObject(@RequestParam(value = "jobid") Integer jobid) {
		Job job = this.jobService.get(jobid);

		EditJobForm form = new EditJobForm();
		form.setAgreedDeliveryDate(job.getAgreedDelDate());
		if (JobType.SITE.equals(job.getType())) {
			// Site job booked in at client address
			// (and optionally one of its locations)
			form.setBookedInBusinessAddressId(job.getOrganisation().getDefaultAddress().getAddrid());
			form.setBookedInBusinessLocationId(0);
			form.setBookedInClientAddressId(job.getBookedInAddr().getAddrid());
			form.setBookedInClientLocationId(job.getBookedInLoc() == null ? 0 : job.getBookedInLoc().getLocationid());
		} else {
			// Inhouse/Laboratory job booked in at business address
			// (and optionally one of its locations)
			form.setBookedInBusinessAddressId(job.getBookedInAddr().getAddrid());
			form.setBookedInBusinessLocationId(job.getBookedInLoc() == null ? 0 : job.getBookedInLoc().getLocationid());
			if (job.getCon().getDefAddress() != null)
				form.setBookedInClientAddressId(job.getCon().getDefAddress().getAddrid());
			form.setBookedInClientLocationId(0);
		}
		form.setClientReference(job.getClientRef());
		form.setCopyTransportIn(true);
		form.setCopyTransportOut(true);
		form.setCurrencyCode(""); // Maintain existing rate on job by default
		form.setEstCarriageOut(job.getEstCarriageOut());
		form.setJobContactId(job.getCon().getId());
		form.setJobid(job.getJobid());
		form.setJobType(job.getType());
		form.setOverrideDateInOnJobItems(job.getOverrideDateInOnJobItems());
		form.setReturnToAddressId(job.getReturnTo() != null ? job.getReturnTo().getAddrid() : 0);
		form.setReturnToLocationId(job.getReturnToLoc() != null ? job.getReturnToLoc().getLocationid() : 0);
		form.setTransportInId(job.getInOption() != null ? job.getInOption().getId() : 0);
		form.setTransportOutId(job.getReturnOption() != null ? job.getReturnOption().getId() : 0);
		form.setBusinessSubdivContactId(job.getCreatedBy().getId());

		return form;
	}

	@PreAuthorize("@hasJobAuthorityOnConnectedSubdiv.check('JOB_EDIT')")
	@RequestMapping(value = "editjob.htm", method = RequestMethod.GET)
	public String referenceData(Model model, Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("form") EditJobForm form) {
		Job job = this.jobService.get(form.getJobid());
		form.setPickupDate(localDateTimeFromZonedDateTime(job.getPickupDate()));
		form.setReceiptDate(localDateTimeFromZonedDateTime(job.getReceiptDate()));
		ContactKeyValue clientContactDto = new ContactKeyValue(job.getCon().getPersonid(), job.getCon().getName());
		int businessSubdivId = job.getOrganisation().getId();
		int clientSubdivId = job.getCon().getSub().getId();
		int clientCompanyId = job.getCon().getSub().getComp().getId();

		// load a list of contacts to source job. Add existing sourced-by to list, as
		// value may not be active!

		ContactKeyValue trescalContact = new ContactKeyValue(job.getCreatedBy().getId(), job.getCreatedBy().getName());
		Contact currentContact = this.userService.get(username).getCon();
		ContactKeyValue loggedUser = new ContactKeyValue(currentContact.getId(), currentContact.getName());
		List<ContactKeyValue> businessSubdivContacts = this.conServ.getContactDtoList(null,
				job.getOrganisation().getSubdivid(), true, trescalContact, loggedUser);

		List<KeyValue<Integer, String>> businessInAddresses = this.addressService
				.getActiveUserRoleAddressesKeyValue(username, true);
		List<Location> businessInLocations = this.locationService
				.getAllAddressLocations(form.getBookedInBusinessAddressId(), true);

		List<KeyValue<Integer, String>> clientAddresses = this.addressService
				.getAllActiveSubdivAddressesKeyValue(clientSubdivId, AddressType.DELIVERY, true);
		List<Location> clientInLocations = form.getBookedInClientAddressId() == null ? null
				: this.locationService.getAllAddressLocations(form.getBookedInClientAddressId(), true);
		List<Location> clientOutLocations = this.locationService.getAllAddressLocations(form.getReturnToAddressId(),
				true);
		List<ContactKeyValue> clientContacts = this.conServ.getContactDtoList(null, clientSubdivId, true,
				clientContactDto);

		model.addAttribute("businessAddresses", businessInAddresses);
		model.addAttribute("businessInLocations", businessInLocations);
		model.addAttribute("clientAddresses", clientAddresses);
		model.addAttribute("clientInLocations", clientInLocations);
		model.addAttribute("returnAddresses", clientAddresses);
		model.addAttribute("returnToLocations", clientOutLocations);
		model.addAttribute("clientContacts", clientContacts);
		model.addAttribute("jobItemCount", this.jobItemService.getCountJobItems(job.getJobid()));
		model.addAttribute("estCarriageOutEditable", job.getEstCarriageOut().compareTo(BigDecimal.ZERO) != 0);

		model.addAttribute("jobTypes", JobType.getActiveJobTypes());
		model.addAttribute("supportedCurrencies",
				this.currencyFormatter.getCompanyCurrencyDTOs(clientCompanyId, job, locale));
		model.addAttribute("transportOptions",
				this.transportOptionService.getTransportOptionsFromSubdiv(businessSubdivId));
		model.addAttribute("job", job);
		model.addAttribute("businessSubdivContacts", businessSubdivContacts);

		return "trescal/core/jobs/job/editjob";
	}

	@PreAuthorize("@hasJobAuthorityOnConnectedSubdiv.check('JOB_EDIT')")
	@RequestMapping(value = "editjob.htm", method = RequestMethod.POST)
	public String onSubmit(Model model, Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_CONTACT) Contact currentUser,
			@Valid @ModelAttribute("form") EditJobForm form, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData(model, locale, username, form);
		}
		jobService.editJob(form, currentUser);
		return "redirect:viewjob.htm?jobid=" + form.getJobid();
	}
}
