package org.trescal.cwms.core.jobs.job.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.system.enums.Scope;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class BpoDTO {

	private Integer poId;
	private String poNumber;
	private String comment;
	private Date durationFrom;
	private Date durationTo;
	private BigDecimal limitAmount;
	private String companyName;
	private String subdivName;
	private String contactName;
	private Boolean defaultForJob;
	private Integer addedItems;
	private Integer addedExpenseItems;
	private Boolean active;
	private String currencySymbol;
	private String currencyCode;
	private Scope scope;
	private Integer unitId;

	public BpoDTO(Integer poId, String poNumber, String comment, Date durationFrom, Date durationTo,
				  BigDecimal limitAmount, Boolean contactBPO, Boolean subdivBPO, Boolean companyBPO, Boolean defaultForJob,
				  Boolean active, Long addedItems, Long addedExpenseItems) {
		super();
		this.poId = poId;
		this.poNumber = poNumber;
		this.comment = comment;
		this.durationFrom = durationFrom;
		this.durationTo = durationTo;
		this.limitAmount = limitAmount;
		this.scope = contactBPO ? Scope.CONTACT : subdivBPO ? Scope.SUBDIV : companyBPO ? Scope.COMPANY : null;
		this.defaultForJob = defaultForJob;
		this.active = active;
		this.addedItems = addedItems.intValue();
		this.addedExpenseItems = addedExpenseItems.intValue();
	}

	public BpoDTO(Integer poId, String poNumber, String comment) {
		super();
		this.poId = poId;
		this.poNumber = poNumber;
		this.comment = comment;
	}

	public BpoDTO(BPO bpo) {
		this.poId = bpo.getPoId();
		this.poNumber = bpo.getPoNumber();
		this.comment = bpo.getComment();
		this.durationFrom = bpo.getDurationFrom();
		this.durationTo = bpo.getDurationTo();
		this.limitAmount = bpo.getLimitAmount();
		this.active = bpo.isActive();
		if (bpo.getCompany() != null) {
			this.companyName = bpo.getCompany().getConame();
			this.scope = Scope.COMPANY;
			this.unitId = bpo.getCompany().getCoid();
			this.currencyCode = bpo.getCompany().getCurrency().getCurrencyCode();
			this.currencySymbol = bpo.getCompany().getCurrency().getCurrencySymbol();
		}
		if (bpo.getSubdiv() != null) {
			this.subdivName = bpo.getSubdiv().getSubname();
			this.scope = Scope.SUBDIV;
			this.unitId = bpo.getSubdiv().getId();
			this.currencyCode = bpo.getSubdiv().getComp().getCurrency().getCurrencyCode();
			this.currencySymbol = bpo.getSubdiv().getComp().getCurrency().getCurrencySymbol();
		}
		if (bpo.getContact() != null) {
			this.contactName = bpo.getContact().getName();
			this.scope = Scope.CONTACT;
			this.unitId = bpo.getContact().getId();
			this.currencyCode = bpo.getContact().getSub().getComp().getCurrency().getCurrencyCode();
			this.currencySymbol = bpo.getContact().getSub().getComp().getCurrency().getCurrencySymbol();
		}
		this.addedItems = 0;
	}
}