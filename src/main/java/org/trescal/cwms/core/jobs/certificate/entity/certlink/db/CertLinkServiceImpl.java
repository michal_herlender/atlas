package org.trescal.cwms.core.jobs.certificate.entity.certlink.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_CreateCertificate;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_CreateCertificate.Input;

import java.util.Date;
import java.util.List;

@Service("CertLinkService")
public class CertLinkServiceImpl extends BaseServiceImpl<CertLink, Integer> implements CertLinkService {

	@Autowired
	private CertLinkDao clDao;
	@Autowired
	private HookInterceptor_CreateCertificate interceptor_createCertificate;

	@Override
	protected BaseDao<CertLink, Integer> getBaseDao() {
		return clDao;
	}

	@Override
	public CertLink findCertLink(Integer linkId) {
		return this.clDao.find(linkId);
	}

	@Override
	public void insertCertLink(CertLink cl) {
		this.clDao.persist(cl);
		interceptor_createCertificate
				.recordAction(new Input(cl, HookInterceptor_CreateCertificate.CERTIFICATE_DEFAULT_TIME_SPENT));
	}

	@Override
	public void insertCertLink(CertLink cl, Contact contact, Date startDate) {
		this.clDao.persist(cl);
		interceptor_createCertificate.recordAction(new Input(cl, contact,
				HookInterceptor_CreateCertificate.CERTIFICATE_DEFAULT_TIME_SPENT, startDate, null));
	}
	
	@Override
	public List<CertLink> searchByPlantId(Integer plantid, Integer maxResults) {
		return this.clDao.searchByPlantId(plantid, maxResults);
	}

	@Override
	public List<CertLink> searchCertLink(Integer plantId, boolean includeDeletedAndCancelledCertificates) {
		return this.clDao.searchCertLink(plantId, includeDeletedAndCancelledCertificates);
	}
	
	/**
	 * Returns the total number of certlinks associated with the instrument 
	 */
	@Override
	public Long getCertLinkCount(Integer plantid) {
		return this.clDao.getCertLinkCount(plantid);
	}
}