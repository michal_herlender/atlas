package org.trescal.cwms.core.jobs.calibration.entity.callink;

import java.util.Comparator;

public class JobItemToCalLinkComparator implements Comparator<CalLink>
{
	@Override
	public int compare(CalLink o1, CalLink o2)
	{
		return ((Integer) o1.getCal().getId()).compareTo(o2.getCal().getId());
	}
}