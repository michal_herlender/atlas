package org.trescal.cwms.core.jobs.repair.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.birt.repaircompletionreport.RepairInspectionReportDataSet;
import org.trescal.cwms.core.documents.filenamingservice.FileNamingService;
import org.trescal.cwms.core.documents.filenamingservice.RepairInspectionReportFileNamingService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;

@Controller
@IntranetController
@SessionAttributes(Constants.HTTPSESS_NEW_FILE_LIST)
public class RepairInspectionReportDocumentController {

	@Autowired
	private ComponentDirectoryService compDirService;
	@Autowired
	private DocumentService documentService;
	@Autowired
	private RepairInspectionReportService rirService;
	@Autowired
	private SystemComponentService scService;

	@RequestMapping(path = "rirdocument.htm")
	public String generateDocument(Locale locale,
			@ModelAttribute(name = Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList,
			@RequestParam(name = "ririd", required = true) Integer rirId,
			@RequestParam(name= "completionReport", required=false, defaultValue="false") Boolean completionReport) throws Exception {

		RepairInspectionReport rir = rirService.get(rirId);
		String jobNumber = rir.getJi().getJob().getJobno();
		Integer jobid = rir.getJi().getJob().getJobid();

		Component component = null;
		BaseDocumentType documentType = null;
		if (completionReport) {
			component = Component.REPAIR_COMPLETION_REPORT; 
			documentType = BaseDocumentType.REPAIR_COMPLETION_REPORT; 
		}
		else {
			component = Component.REPAIR_INSPECTION_REPORT; 
			documentType = BaseDocumentType.REPAIR_EVALUATION_REPORT; 
		}
		String directory = getDirectory(component, jobNumber);
		FileNamingService fnService = new RepairInspectionReportFileNamingService(rir, completionReport, directory);
		
		Map<String,Object> additionalParameters = new HashMap<>();
		additionalParameters.put(RepairInspectionReportDataSet.PARAMETER_COMPLETION_REPORT, completionReport);
		
		Document doc = documentService.createBirtDocument(rirId, documentType, locale,
				component, fnService, additionalParameters);
		documentService.addFileNameToSession(doc, sessionNewFileList);

		return "redirect:viewjob.htm?jobid=" + jobid + "&loadtab=jobfiles-tab";
	}

	private String getDirectory(Component component, String jobNumber) {
		SystemComponent sc = this.scService.findComponent(component);
		String directory = this.compDirService.getDirectory(sc, jobNumber).getAbsolutePath();
		return directory;
	}
}