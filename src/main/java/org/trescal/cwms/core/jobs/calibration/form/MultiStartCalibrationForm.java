package org.trescal.cwms.core.jobs.calibration.form;

import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.calibration.dto.MultiStartCal;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandard;

public class MultiStartCalibrationForm
{
	private Contact currentContact;
	private Integer[] addStdsOnScreen;
	private List<MultiStartCal> cals;
	private List<Instrument> errorAdditionalStds;
	private List<DefaultStandard> errorProcStds;
	private Integer[] instrums;
	private JobItem ji;
	private Job job;
	private Integer[] procStdsOnScreen;
	private Set<Integer> stdIds;

	public Integer[] getAddStdsOnScreen()
	{
		return this.addStdsOnScreen;
	}

	public List<MultiStartCal> getCals()
	{
		return this.cals;
	}

	public List<Instrument> getErrorAdditionalStds()
	{
		return this.errorAdditionalStds;
	}

	public List<DefaultStandard> getErrorProcStds()
	{
		return this.errorProcStds;
	}

	public Integer[] getInstrums()
	{
		return this.instrums;
	}

	public JobItem getJi()
	{
		return this.ji;
	}

	public Job getJob()
	{
		return this.job;
	}

	public Integer[] getProcStdsOnScreen()
	{
		return this.procStdsOnScreen;
	}

	public Set<Integer> getStdIds()
	{
		return this.stdIds;
	}

	public void setAddStdsOnScreen(Integer[] addStdsOnScreen)
	{
		this.addStdsOnScreen = addStdsOnScreen;
	}

	public void setCals(List<MultiStartCal> cals)
	{
		this.cals = cals;
	}

	public void setErrorAdditionalStds(List<Instrument> errorAdditionalStds)
	{
		this.errorAdditionalStds = errorAdditionalStds;
	}

	public void setErrorProcStds(List<DefaultStandard> errorProcStds)
	{
		this.errorProcStds = errorProcStds;
	}

	public void setInstrums(Integer[] instrums)
	{
		this.instrums = instrums;
	}

	public void setJi(JobItem ji)
	{
		this.ji = ji;
	}

	public void setJob(Job job)
	{
		this.job = job;
	}

	public void setProcStdsOnScreen(Integer[] procStdsOnScreen)
	{
		this.procStdsOnScreen = procStdsOnScreen;
	}

	public void setStdIds(Set<Integer> stdIds)
	{
		this.stdIds = stdIds;
	}

	public Contact getCurrentContact() {
		return currentContact;
	}

	public void setCurrentContact(Contact currentContact) {
		this.currentContact = currentContact;
	}
}