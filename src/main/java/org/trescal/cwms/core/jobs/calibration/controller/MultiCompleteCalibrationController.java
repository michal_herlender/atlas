package org.trescal.cwms.core.jobs.calibration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.calibration.dto.MultiCompleteCal;
import org.trescal.cwms.core.jobs.calibration.dto.MultiCompleteCalItem;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.form.MultiCompleteCalibrationForm;
import org.trescal.cwms.core.jobs.calibration.form.MultiCompleteCalibrationValidator;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.recall.entity.recallrule.RecallRuleInterval;
import org.trescal.cwms.core.recall.entity.recallrule.db.RecallRuleService;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class MultiCompleteCalibrationController {
    @Autowired
    private ActionOutcomeService aoServ;
    @Autowired
    private CalibrationService calServ;
    @Autowired
    private CertificateService certServ;
    @Autowired
    private ItemStateService itemStateServ;
    @Autowired
    private JobItemService jiServ;
    @Autowired
    private JobService jobServ;
    @Autowired
    private RecallRuleService recallRuleServ;
    @Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private MultiCompleteCalibrationValidator validator;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("form")
	protected Object formBackingObject(HttpSession session,
			@RequestParam(name = "jobid", required = true) Integer jobId,
			@RequestParam(name = "jiid", required = false, defaultValue = "0") Integer jiId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		Contact currentContact = this.userService.get(username).getCon();

		// get request parameters (currently, UI always passes job item id)
		Job job = this.jobServ.get(jobId);
		JobItem jItem = jiId == 0 ? null : this.jiServ.findJobItem(jiId);

		// set the preset comment type into the session
		session.setAttribute("completecalibrationcomment", PresetCommentType.COMPLETE_CALIBRATION);

		MultiCompleteCalibrationForm form = new MultiCompleteCalibrationForm();
		form.setCurrentContact(currentContact);
		form.setJob(job);
		// Currently appears to be no functionality in UI to cancel or resume
		// multiple
		form.setAction(Constants.FORM_COMPLETE);

		// initialise list of calibrations
		List<Calibration> cals = new ArrayList<Calibration>();
		// job item passed?
		if (jItem != null) {
            cals.addAll(
                this.calServ.getAllCalibrationsForJobAtStatusUsingProc(jobId, "On-going", jItem.getCapability().getId()));
        } else {
			cals.addAll(this.calServ.getAllCalibrationsForJobAtStatus(job.getJobid(), "On-going"));
		}

		List<MultiCompleteCal> onGoingCals = new ArrayList<MultiCompleteCal>();
		for (Calibration cal : cals) {
			Instrument inst = cal.getLinks().iterator().next().getJi().getInst();
			RecallRequirementType requirementType = cal.getLinks().iterator().next().getJi().getCalType()
					.getRecallRequirementType();
			RecallRuleInterval duration = this.recallRuleServ.getCurrentRecallInterval(inst, requirementType, null);

			MultiCompleteCal mCal = new MultiCompleteCal(cal, duration);
			int timeSpent = this.calServ.getMinutesFromCalibrationStart(cal.getId());

			for (CalLink cl : cal.getLinks()) {
				MultiCompleteCalItem mCalItem = new MultiCompleteCalItem(cl, null, timeSpent);
				mCal.getItems().add(mCalItem);
			}

			onGoingCals.add(mCal);
		}
		form.setOnGoingCals(onGoingCals);

		List<MultiCompleteCal> onHoldCals = new ArrayList<MultiCompleteCal>();
		for (Calibration cal : this.calServ.getAllCalibrationsForJobAtStatus(job.getJobid(), "On hold")) {
			MultiCompleteCal mCal = new MultiCompleteCal(cal, new RecallRuleInterval(0, IntervalUnit.MONTH));
			onHoldCals.add(mCal);
		}
		form.setOnHoldCals(onHoldCals);

		return form;
	}

	protected void onBind(MultiCompleteCalibrationForm form) throws Exception {
		for (MultiCompleteCal mCal : form.getOnGoingCals()) {
			for (MultiCompleteCalItem mCalItem : mCal.getItems()) {
				// set ids to objects as part of binding process
				mCalItem.setOutcome(
						(mCalItem.getOutcomeId() == null) ? null : this.aoServ.get(mCalItem.getOutcomeId()));
			}
		}
	}

	@RequestMapping(value = "/multicomplete.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest request, HttpSession session,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@Valid @ModelAttribute("form") MultiCompleteCalibrationForm form, BindingResult bindingResult)
			throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData(form);
		}
		onBind(form);
		// get the current contact
		Contact contact = this.userService.get(username).getCon();
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		if (form.getAction() == Constants.FORM_COMPLETE) {
			for (MultiCompleteCal mCal : form.getOnGoingCals()) {
				if (mCal.isSelected()) {
					// put data in format that existing methods can interpret
					Map<Integer, Integer> timeSpents = new HashMap<Integer, Integer>();
					Map<Integer, Integer> outcomes = new HashMap<Integer, Integer>();
					List<Integer> includeOnCert = new ArrayList<Integer>();
					for (MultiCompleteCalItem mCalItem : mCal.getItems()) {
						timeSpents.put(mCalItem.getCalLink().getId(), mCalItem.getTimeSpent());
						outcomes.put(mCalItem.getCalLink().getId(), mCalItem.getOutcome().getId());
						includeOnCert.add(mCalItem.getCalLink().getId());
					}

					boolean failedCal = true;
					boolean allItemsPos = true;
					boolean onHold = false;
					calResult: for (MultiCompleteCalItem mCalItem : mCal.getItems()) {
						ActionOutcome ao = mCalItem.getOutcome();
						if (ao.getDescription().contains("on hold")) {
							onHold = true;
							failedCal = false;
							break calResult;
						} else if (ao.isPositiveOutcome()) {
							failedCal = false;
							break calResult;
						} else {
							allItemsPos = false;
						}
					}

					// IF ALL ITEMS FAILED TO CALIBRATE
					if (failedCal) {
						this.calServ.completeCalibrationAsUTCMultipleOutcomes(mCal.getCal().getId(),
								contact.getPersonid(), outcomes, timeSpents, form.getRemark());
					}
					// IF ON HOLD BUTTON PRESSED
					else if (onHold) {
						this.calServ.placeCalibrationOnHold(mCal.getCal(), contact, timeSpents, outcomes,
								form.getRemark());
					}
					// IF AT LEAST ONE ITEM SUCCESSFULLY CALIBRATED
					else {
						RecallRequirementType requirementType = mCal.getCal().getLinks().iterator().next().getJi()
								.getCalType().getRecallRequirementType();
						// complete calibration
						this.calServ.completeCalibration(mCal.getCal(), contact, timeSpents, outcomes, form.getRemark(),
								false, null, mCal.getDuration().getInterval(), mCal.getDuration().getUnit());
						// if new cert is being issued and all items have
						// positive outcome
						if ((form.getIssueNow() != null) && (form.getIssueNow() == true) && (allItemsPos)
								&& requirementType.equals(RecallRequirementType.FULL_CALIBRATION)) {
							// create certificate
							this.certServ.createCertFromCal(contact, mCal.getCal(), mCal.getDuration().getInterval(),
									false, includeOnCert, false, allocatedSubdiv, mCal.getDuration().getUnit(),
									CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE, null);
						}
					}
				}
			}
		} else if (form.getAction() == Constants.FORM_CANCEL) {
			for (MultiCompleteCal mCal : form.getOnGoingCals()) {
				if (mCal.isSelected()) {
					this.calServ.cancelCalibration(mCal.getCal().getId(), contact);
				}
			}
		} else if (form.getAction() == Constants.FORM_RESUME) {
			for (MultiCompleteCal mCal : form.getOnHoldCals()) {
				if (mCal.isSelected()) {
					this.calServ.resumeCalibration(mCal.getCal(), contact, null, null,
							(CalibrationStatus) this.statusServ.findStatusByName(CalibrationStatus.ON_GOING,
									CalibrationStatus.class));
				}
			}
		}

		return new ModelAndView(new RedirectView("viewjob.htm?jobid=" + form.getJob().getJobid()));
	}

	@RequestMapping(value = "/multicomplete.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute("form") MultiCompleteCalibrationForm form) throws Exception {
		onBind(form);
		ItemActivity activity = this.itemStateServ.findItemActivityByName("Pre-calibration");

		Map<String, Object> refData = new HashMap<String, Object>();
		refData.put("actionoutcomes", aoServ.getActionOutcomesForActivity(activity));
		return new ModelAndView("trescal/core/jobs/calibration/multicomplete", refData);
	}
}