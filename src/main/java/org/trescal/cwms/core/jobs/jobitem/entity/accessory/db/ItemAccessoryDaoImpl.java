package org.trescal.cwms.core.jobs.jobitem.entity.accessory.db;

import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.dto.ItemAccessoryDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.Accessory;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.Accessory_;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;

@Repository("itemAccessoryDao")
public class ItemAccessoryDaoImpl extends BaseDaoImpl<ItemAccessory, Integer> implements ItemAccessoryDao {

	@Override
	protected Class<ItemAccessory> getEntity() {
		return ItemAccessory.class;
	}

	@Override
	public Integer countByJob(Integer jobId) {
		Long result = getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, ItemAccessory> accessory = jobItem.join(JobItem_.accessories);
			cq.where(cb.equal(jobItem.get(JobItem_.job), jobId));
			cq.select(cb.sumAsLong(accessory.get(ItemAccessory_.quantity)));
			return cq;
		});
		// empty sum is null, not as usual 0
		return result == null ? 0 : result.intValue();
	}

	@Override
	public Integer countFreeTextAccessoriesByJob(Integer jobId) {
		return getCount(cb -> cq -> {
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.job), jobId));
			clauses.getExpressions().add(cb.isNotNull(jobItem.get(JobItem_.accessoryFreeText)));
			clauses.getExpressions().add(cb.notEqual(jobItem.get(JobItem_.accessoryFreeText), ""));
			cq.where(clauses);
			return Triple.of(jobItem.get(JobItem_.jobItemId), null, null);
		});
	}

	@Override
	public List<ItemAccessoryDTO> getAllByJob(Integer jobId, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<ItemAccessoryDTO> cq = cb.createQuery(ItemAccessoryDTO.class);
			Root<ItemAccessory> itemAccessory = cq.from(ItemAccessory.class);
			Join<ItemAccessory, JobItem> jobItem = itemAccessory.join(ItemAccessory_.ji);
			Join<ItemAccessory, Accessory> accessory = itemAccessory.join(ItemAccessory_.accessory);
			Expression<String> accessoryName = joinTranslation(cb, accessory, Accessory_.translations, locale);
			cq.where(cb.equal(jobItem.get(JobItem_.job), jobId));
			cq.select(cb.construct(ItemAccessoryDTO.class, itemAccessory.get(ItemAccessory_.id),
					jobItem.get(JobItem_.jobItemId), accessory.get(Accessory_.id), accessoryName,
					itemAccessory.get(ItemAccessory_.quantity)));
			return cq;
		});
	}
}