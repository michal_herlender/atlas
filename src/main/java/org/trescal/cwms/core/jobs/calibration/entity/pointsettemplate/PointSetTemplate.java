package org.trescal.cwms.core.jobs.calibration.entity.pointsettemplate;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.SortComparator;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.PointComparator;
import org.trescal.cwms.core.jobs.calibration.entity.pointset.PointSet;
import org.trescal.cwms.core.jobs.calibration.entity.templatepoint.TemplatePoint;

@Entity
@Table(name = "pointsettemplate")
public class PointSetTemplate extends PointSet
{
	private Contact setBy;
	private Date setOn;
	private String title;
	private String description;
	private Set<TemplatePoint> points;

	@Length(max = 500)
	@Column(name = "description", length = 500)
	public String getDescription()
	{
		return this.description;
	}

	@OneToMany(mappedBy = "template", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(PointComparator.class)
	public Set<TemplatePoint> getPoints()
	{
		return this.points;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid")
	public Contact getSetBy()
	{
		return this.setBy;
	}

	@Column(name = "seton", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSetOn()
	{
		return this.setOn;
	}

	@NotNull
	@NotEmpty
	@Length(max = 100, min = 1)
	@Column(name = "title", nullable = false, length = 100)
	public String getTitle()
	{
		return this.title;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setPoints(Set<TemplatePoint> points)
	{
		this.points = points;
	}

	public void setSetBy(Contact setBy)
	{
		this.setBy = setBy;
	}

	public void setSetOn(Date setOn)
	{
		this.setOn = setOn;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

}
