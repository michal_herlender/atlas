package org.trescal.cwms.core.jobs.certificate.entity.instcertlink.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;

@Service
public class InstCertLinkServiceImpl implements InstCertLinkService
{
	@Autowired
	private InstCertLinkDao instCertLinkDao;

	@Override
	public void insertInstCertLink(InstCertLink InstCertLink)
	{
		this.instCertLinkDao.persist(InstCertLink);
	}
	
	@Override
	public Long getInstCertLinkCount(Integer plantid) {
		return this.instCertLinkDao.getInstCertLinkCount(plantid);
	}
	
	@Override
	public List<InstCertLink> searchByPlantId(Integer plantid, Integer maxResults) {
		return this.instCertLinkDao.searchByPlantId(plantid, maxResults);
	}
}