package org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.db;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewLinkedCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.dto.ContractReviewCalibrationCostDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.CostLookUp;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.CostSourceSupportService;
import org.trescal.cwms.core.pricing.entity.costs.dto.CostJobItemDto;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.db.JobCostingCalibrationCostService;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutput;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.calcost.db.QuotationCalibrationCostService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.MultiCurrencyUtils;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

import io.vavr.control.Either;
import lombok.val;

/**
 * Contains functions for manipulating {@link ContractReviewCalibrationCost}
 * entities.
 *
 */
@Service("ContractReviewCalibrationCostService")
public class ContractReviewCalibrationCostServiceImpl implements ContractReviewCalibrationCostService {

	private final Log logger = LogFactory.getLog(this.getClass());
	
	/**
	 * Defines the lookup precedence hierarchy for looking up calibration costs
	 * for {@link JobItem}.
	 */
	@Value("#{props['cwms.config.contractreview.costs.lookuphierarchy.calibration']}")
	private String costSourceHierarchy;
	
	@Autowired
	private ContractReviewCalibrationCostDao contractReviewCalibrationCostDao;
	@Autowired
	private ContractReviewLinkedCalibrationCostService contractReviewLinkedCalCostService;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private JobCostingCalibrationCostService jobCostingCostService;
	@Autowired
	private MessageSource messages;
	@Autowired
	private QuotationCalibrationCostService quotationCostService;
	@Autowired
	private CatalogPriceService priceServ;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;

	@Override
	public ContractReviewCalibrationCost createCost(JobItem ji, PriceLookupOutput output,
			BigDecimal calibrationDiscount) {
		CostSource costSource;
		SupportedCurrency currency = ji.getJob().getCurrency();
		// TODO currency conversion, in case catalog price or quotation item are in
		// different currency?
		BigDecimal totalCost;
		BigDecimal discountRate;
		ContractReviewLinkedCalibrationCost linkedCost = null;
		ContractReviewCalibrationCost newCost = new ContractReviewCalibrationCost();

		switch (output.getQueryType()) {
		case CATALOG_PRICE_INSTRUMENT_MODEL:
		case CATALOG_PRICE_SALES_CATEGORY_MODEL:
			costSource = CostSource.MODEL;
			totalCost = output.getCatalogPrice();
			discountRate = calibrationDiscount;
			break;

		case QUOTATION_ITEM_INSTRUMENT:
		case QUOTATION_ITEM_INSTRUMENT_MODEL:
		case QUOTATION_ITEM_SALES_CATEGORY_MODEL:
			QuotationCalibrationCost qccRef = this.quotationCostService.getReference(output.getQuotationCalCostId());
			costSource = CostSource.QUOTATION;
			totalCost = output.getQuotationCalCostTotalCost();
			discountRate = output.getQuotationCalCostDiscountRate();

			linkedCost = new ContractReviewLinkedCalibrationCost();
			linkedCost.setCalibrationCost(newCost);
			linkedCost.setQuotationCalibrationCost(qccRef);
			break;
		default:
			throw new UnsupportedOperationException("Unsupported price source " + output.getQueryType());
		}

		newCost.setActive(true);
		newCost.setAutoSet(true);
		newCost.setCostSrc(costSource);
		newCost.setCostType(CostType.CALIBRATION);
		newCost.setCurrency(currency);
		// No setCustomOrderBy() - optional
		newCost.setDiscountRate(discountRate);
		// No setDiscountValue() or setFinalCost() - set by CostCalculator
		// No setHourlyRate() - not used
		newCost.setJobitem(ji);
		// No setLabourCostTotal() or setLabourTime() - not used
		newCost.setLinkedCost(linkedCost);
		newCost.setTotalCost(totalCost);

		CostCalculator.updateSingleCost(newCost, false);

		return newCost;
	}

	/**
	 * Create empty zero valued cost
	 * 
	 */
	@Override
	public ContractReviewCalibrationCost createEmptyCost(JobItem jobItem, boolean active) {
		ContractReviewCalibrationCost cost = new ContractReviewCalibrationCost();
		cost.setActive(true);
		cost.setAutoSet(true);
		cost.setDiscountRate(new BigDecimal("0.00"));
		cost.setDiscountValue(new BigDecimal("0.00"));
		cost.setFinalCost(new BigDecimal("0.00"));
		cost.setTotalCost(new BigDecimal("0.00"));
		cost.setJobitem(jobItem);
		cost.setCostType(CostType.CALIBRATION);
		cost.setCostSrc(CostSource.MANUAL);
		cost.setActive(active);
		return cost;
	}

	@Override
	public ContractReviewCalibrationCost copyContractReviewCost(ContractReviewCalibrationCost old,
			JobItem copyToJobitem) {
		ContractReviewCalibrationCost newCost = null;

		if (old != null) {
			newCost = new ContractReviewCalibrationCost();

			BeanUtils.copyProperties(old, newCost);

			newCost.setCostid(null);
			newCost.setJobitem(copyToJobitem);
			newCost.setAutoSet(true);

			if (old.getLinkedCost() != null) {
				ContractReviewLinkedCalibrationCost oldLinkedCost = old.getLinkedCost();
				ContractReviewLinkedCalibrationCost newLinkedCost = new ContractReviewLinkedCalibrationCost();
				BeanUtils.copyProperties(oldLinkedCost, newLinkedCost);
				newLinkedCost.setId(0);
				newLinkedCost.setCalibrationCost(newCost);
				newCost.setLinkedCost(newLinkedCost);
			}
		}
		return newCost;
	}

	@Override
	public void deleteLinkCost(ContractReviewLinkedCalibrationCost linkedCost) {
		this.contractReviewLinkedCalCostService.delete(linkedCost);
	}

	@Override
	public ContractReviewCalibrationCost findContractReviewCalibrationCost(int id) {
		return this.contractReviewCalibrationCostDao.find(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public CostLookUp getAlternativeCosts(int jobitemid, Integer caltypeid) {
		// get a list of costsources which we can lookup
		List<CostSource> costSources = CostSourceSupportService.resolveCostSourceList(this.costSourceHierarchy);
		// create our dto and set the costsources back into it
		CostLookUp lu = new CostLookUp();
		lu.setCostSources(costSources);
		// load the jobitem
		JobItem ji = this.jiServ.findEagerJobItem(jobitemid);
		// get some of the inital variables we'll need below
		int jobid = ji.getJob().getJobid();
		int modelid = ji.getInst().getModel().getModelid();
		int calTypeId = caltypeid == null ? ji.getCalType().getCalTypeId() : caltypeid;
		int plantid = ji.getInst().getPlantid();
		int coid = ji.getJob().getCon().getSub().getComp().getCoid();
		this.logger.info("jobid: " + jobid + " modelid: " + modelid + " calTypeId: " + calTypeId + " plantid: "
				+ plantid + " coid: " + coid);
		for (CostSource source : costSources) {
			switch (source) {
			case JOB_COSTING:
				this.logger.info("loading calibration costs from JOB_COSTING");
				List<JobCostingCalibrationCost> jobCostingCosts = (List<JobCostingCalibrationCost>) this.jobCostingCostService
						.findMatchingCosts(null, coid, modelid, caltypeid, 1, null);
				lu.setJobCostingCosts(jobCostingCosts);
				this.logger.info("Found " + jobCostingCosts.size() + " matching linked job costing costs.");
				break;
			case QUOTATION:
				this.logger.info("loading calibration costs from QUOTATION");
				this.logger.info("finding calibration costs from linked quotations");
				List<QuotationCalibrationCost> linkedQuotationCosts = this.quotationCostService
						.findMatchingCostOnLinkedQuotation(jobid, modelid, calTypeId);
				lu.setLinkedQuotationCosts(linkedQuotationCosts);
				this.logger.info("Found " + linkedQuotationCosts.size() + " matching linked quotation costs.");
				// Restrict to 1 page of results to prevent 1000s of results
				// over DWR
				List<QuotationCalibrationCost> unLinkedQuotationCosts = this.quotationCostService
						.findMatchingCosts(coid, modelid, calTypeId, 1, Constants.RESULTS_PER_PAGE);
				lu.setUnLinkedQuotationCosts(unLinkedQuotationCosts);
				this.logger.info("Found " + unLinkedQuotationCosts.size() + " matching non-linked quotation costs.");
				break;
			case INSTRUMENT:
				this.logger.info("loading calibration costs from INSTRUMENT " + plantid + " using job costings");

				List<JobCostingCalibrationCost> instJobCostingCosts = (List<JobCostingCalibrationCost>) this.jobCostingCostService
						.findMatchingCosts(plantid, coid, modelid, caltypeid, 1, null);
				lu.setPlantJobCostingCosts(instJobCostingCosts);
				this.logger.info("Found " + instJobCostingCosts.size() + " matching linked job costing costs.");
				break;
			case MODEL:
				this.logger.info("loading calibration costs from MODEL");
				CatalogPrice catalogPrice = priceServ.findCalCatalogPriceEager(ji);
				if (catalogPrice != null) {
					Set<Translation> translations = catalogPrice.getServiceType().getLongnameTranslation();
					lu.setTranslatedServiceTypeName(
							translationService.getCorrectTranslation(translations, LocaleContextHolder.getLocale()));
					BigDecimal priceInCurrency = MultiCurrencyUtils.convertCurrencyValue(catalogPrice.getFixedPrice(),
							catalogPrice.getOrganisation().getCurrency(), ji.getJob().getCurrency());
					lu.setCatalogPrices(Collections.singletonList(priceInCurrency));
					if (modelid > 0) {
						InstrumentModel instrumentModel = instrumentModelService.findInstrumentModel(modelid);
						String modelName = InstModelTools.modelNameViaTranslations(instrumentModel,
								LocaleContextHolder.getLocale(), supportedLocaleService.getPrimaryLocale());
						lu.setTranslatedFullModelName(modelName);
					}
					this.logger.info("Found matching catalog price.");
					lu.setCurrency(ji.getJob().getCurrency());
				}
				break;
				case CONTRACT_REVIEW:
				case DERIVED:
				case JOB:
				case MANUAL:
				default:
				break;
			}
		}
		return lu;
	}

	private ContractReviewCalibrationCost resetLinkedContactReviewCost(ContractReviewCalibrationCost cost) {
		ContractReviewLinkedCalibrationCost linkedCost = cost.getLinkedCost();
		cost.setLinkedCost(null);
		cost.setCostSrc(CostSource.MANUAL);
		if (linkedCost != null) {
			this.deleteLinkCost(linkedCost);
		}
		cost = contractReviewCalibrationCostDao.merge(cost);
		return cost;
	}

	@Override
	public Either<String,CostSource> resetLinkedCostByCostId(Integer costId){
		val cost = this.findContractReviewCalibrationCost(costId);
		if( cost == null)
			return Either.left(this.messages.getMessage("error.contractreview.costs.calibration.notfound",
				null, "The cost could not be found to be deleted", null));
		this.resetLinkedContactReviewCost(cost);
		return Either.right(cost.getCostSrc());
	}

	@Override
	public ResultWrapper resetLinkedCost(int costid) {
		ContractReviewCalibrationCost cost = this.findContractReviewCalibrationCost(costid);
		if ((costid == 0) || (cost == null)) {
			return new ResultWrapper(false, this.messages.getMessage("error.contractreview.costs.calibration.notfound",
					null, "The cost could not be found to be deleted", null));
		} else {
			this.resetLinkedContactReviewCost(cost);
			return new ResultWrapper(true, "", cost, null);
		}
	}

	private ContractReviewCalibrationCost newCalibrationCost(JobItem ji){
		val c = new ContractReviewCalibrationCost();
		c.setCostType(CostType.CALIBRATION);
		c.setJobitem(ji);
		ji.setCalibrationCost(c);
		return c;
	}

	@Override
	public Either<String, CostJobItemDto> updateCostSourceE(Integer jobItemId, Integer costId, CostSource costSource){
		val ji = jiServ.findJobItem(jobItemId);
		if(ji == null)
			return Either.left(messages.getMessage("error.jobitem.notfound", null,
				"Requested jobitem could not be found", null));
		val cost = ji.getCalibrationCost() != null?
			ji.getCalibrationCost(): newCalibrationCost(ji);
		cost.setCostSrc(costSource);
		cost.setActive(true);
		cost.setAutoSet(false);
		ContractReviewLinkedCalibrationCost linkedCost;
		if (cost.getLinkedCost() == null) {
			linkedCost = new ContractReviewLinkedCalibrationCost(cost);
			cost.setLinkedCost(linkedCost);
		} else {
			linkedCost = cost.getLinkedCost();
		}

		switch (costSource) {
			case JOB_COSTING:
				JobCostingCalibrationCost jCalCost = this.jobCostingCostService
					.findEagerJobCostingCalibrationCost(costId);
				linkedCost.setJobCostingCalibrationCost(jCalCost);
				linkedCost.setQuotationCalibrationCost(null);
				cost.setDiscountRate(jCalCost.getDiscountRate());
				cost.setDiscountValue(jCalCost.getDiscountValue());
				cost.setTotalCost(jCalCost.getTotalCost());
				cost.setFinalCost(jCalCost.getFinalCost());
				break;
			case QUOTATION:
				QuotationCalibrationCost qCalCost = this.quotationCostService.findEagerQuotationCalibrationCost(costId);
				linkedCost.setJobCostingCalibrationCost(null);
				linkedCost.setQuotationCalibrationCost(qCalCost);
				cost.setDiscountRate(qCalCost.getDiscountRate());
				cost.setDiscountValue(qCalCost.getDiscountValue());
				cost.setTotalCost(qCalCost.getTotalCost());
				cost.setFinalCost(qCalCost.getFinalCost());
				break;
			case INSTRUMENT:
				jCalCost = this.jobCostingCostService.findEagerJobCostingCalibrationCost(costId);
				linkedCost.setJobCostingCalibrationCost(jCalCost);
				linkedCost.setQuotationCalibrationCost(null);
				cost.setDiscountRate(jCalCost.getDiscountRate());
				cost.setDiscountValue(jCalCost.getDiscountValue());
				cost.setTotalCost(jCalCost.getTotalCost());
				cost.setFinalCost(jCalCost.getFinalCost());
				break;
			case MODEL:
				CatalogPrice catalogPrice = priceServ.findCalCatalogPrice(ji);
				if (catalogPrice != null) {
					this.logger.info("Setting calibration costs from instrument model defaults");
					cost.setCurrency(cost.getJobitem().getJob().getCurrency());
					cost.setDiscountRate(new BigDecimal("0.00"));
					cost.setDiscountValue(new BigDecimal("0.00"));
					BigDecimal priceInCurrency = MultiCurrencyUtils.convertCurrencyValue(catalogPrice.getFixedPrice(),
						catalogPrice.getOrganisation().getCurrency(), cost.getCurrency());
					cost.setFinalCost(priceInCurrency);
					cost.setTotalCost(priceInCurrency);
				}
			case CONTRACT_REVIEW:
			case DERIVED:
			case JOB:
			case MANUAL:
				cost.setLinkedCost(null);
				break;
			default:
				break;
		}
		ji.setCalibrationCost(cost);
		return Either.right(CostJobItemDto.fromCost(cost));
	}



	@Override
	public ResultWrapper updateCostSource(int jobitemid, Integer costid, CostSource costSource) {
		JobItem ji = this.jiServ.findJobItem(jobitemid);
		if (ji == null) {
			return new ResultWrapper(false, this.messages.getMessage("error.jobitem.notfound", null,
					"Requested jobitem could not be found", null));
		} else {
			ContractReviewCalibrationCost cost = ji.getCalibrationCost();
			// create a new cost in case none has been set
			if (cost == null) {
				cost = new ContractReviewCalibrationCost();
				cost.setCostType(CostType.CALIBRATION);
				cost.setJobitem(ji);
				ji.setCalibrationCost(cost);
			}
			cost.setCostSrc(costSource);
			cost.setActive(true);
			cost.setAutoSet(false);
			// if there already exists a linked cost, then overwrite it - otherwise issue
			// with flushing, insert before delete -> constraint violation exception
			ContractReviewLinkedCalibrationCost linkedCost;
			if (cost.getLinkedCost() == null) {
				linkedCost = new ContractReviewLinkedCalibrationCost(cost);
				cost.setLinkedCost(linkedCost);
			} else
				linkedCost = cost.getLinkedCost();
			switch (costSource) {
			case JOB_COSTING:
				this.logger.info("loading calibration costs from JOB_COSTING");
				JobCostingCalibrationCost jCalCost = this.jobCostingCostService
						.findEagerJobCostingCalibrationCost(costid);
				linkedCost.setJobCostingCalibrationCost(jCalCost);
				linkedCost.setQuotationCalibrationCost(null);
				cost.setDiscountRate(jCalCost.getDiscountRate());
				cost.setDiscountValue(jCalCost.getDiscountValue());
				cost.setTotalCost(jCalCost.getTotalCost());
				cost.setFinalCost(jCalCost.getFinalCost());
				break;
			case QUOTATION:
				QuotationCalibrationCost qCalCost = this.quotationCostService.findEagerQuotationCalibrationCost(costid);
				linkedCost.setJobCostingCalibrationCost(null);
				linkedCost.setQuotationCalibrationCost(qCalCost);
				cost.setDiscountRate(qCalCost.getDiscountRate());
				cost.setDiscountValue(qCalCost.getDiscountValue());
				cost.setTotalCost(qCalCost.getTotalCost());
				cost.setFinalCost(qCalCost.getFinalCost());
				break;
			case INSTRUMENT:
				this.logger.info("loading calibration costs from INSTRUMENT");
				jCalCost = this.jobCostingCostService.findEagerJobCostingCalibrationCost(costid);
				linkedCost.setJobCostingCalibrationCost(jCalCost);
				linkedCost.setQuotationCalibrationCost(null);
				cost.setDiscountRate(jCalCost.getDiscountRate());
				cost.setDiscountValue(jCalCost.getDiscountValue());
				cost.setTotalCost(jCalCost.getTotalCost());
				cost.setFinalCost(jCalCost.getFinalCost());
				break;
			case MODEL:
				CatalogPrice catalogPrice = priceServ.findCalCatalogPrice(ji);
				if (catalogPrice != null) {
					this.logger.info("Setting calibration costs from instrument model defaults");
					cost.setCurrency(cost.getJobitem().getJob().getCurrency());
					cost.setDiscountRate(new BigDecimal("0.00"));
					cost.setDiscountValue(new BigDecimal("0.00"));
					BigDecimal priceInCurrency = MultiCurrencyUtils.convertCurrencyValue(catalogPrice.getFixedPrice(),
							catalogPrice.getOrganisation().getCurrency(), cost.getCurrency());
					cost.setFinalCost(priceInCurrency);
					cost.setTotalCost(priceInCurrency);
				}
			case CONTRACT_REVIEW:
			case DERIVED:
			case JOB:
			case MANUAL:
				cost.setLinkedCost(null);
				break;
			default:
				break;
			}
			ji.setCalibrationCost(cost);
			return new ResultWrapper(true, "", cost, null);
		}
	}

	@Override
	public ContractReviewCalibrationCost resetLinkedCalContactReviewCost(ContractReviewCalibrationCost calCost) {
		return this.resetLinkedContactReviewCost(calCost);

	}

	@Override
	public ContractReviewCalibrationCostDTO findContractReviewCalibrationCost(Integer id, Integer servicetypeid, Locale locale){
		return this.contractReviewCalibrationCostDao.findContractReviewCalibrationCost(id, servicetypeid, locale);
	}

}