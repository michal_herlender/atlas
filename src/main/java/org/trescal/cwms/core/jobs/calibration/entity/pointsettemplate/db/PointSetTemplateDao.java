package org.trescal.cwms.core.jobs.calibration.entity.pointsettemplate.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.calibration.entity.pointsettemplate.PointSetTemplate;

public interface PointSetTemplateDao extends BaseDao<PointSetTemplate, Integer> {
	
	PointSetTemplate findPointSetTemplateWithPoints(int id);
}