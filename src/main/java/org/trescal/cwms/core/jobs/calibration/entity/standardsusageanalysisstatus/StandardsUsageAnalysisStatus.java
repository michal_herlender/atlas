package org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysisstatus;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum StandardsUsageAnalysisStatus {

	OPEN("standardsusageanalysisstatus.open", "Open"), // 0
	CLOSED("standardsusageanalysisstatus.closed", "Closed"), // 1
	CANCELLED("standardsusageanalysisstatus.cancelled", "Cancelled"); // 2

	private ReloadableResourceBundleMessageSource messages;
	private Locale loc = null;
	private String messageCode;
	private String name;

	@Component
	public static class StandardsUsageAnalysisStatusMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;

		@PostConstruct
		public void postConstruct() {
			for (StandardsUsageAnalysisStatus rt : EnumSet.allOf(StandardsUsageAnalysisStatus.class))
				rt.setMessageSource(messages);
		}
	}

	public void setMessageSource(ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}

	private StandardsUsageAnalysisStatus(String messageCode, String name) {
		this.messageCode = messageCode;
		this.name = name;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		loc = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.name, loc);
		}
		return this.toString();
	}

	public String getMessageCode() {
		return messageCode;
	}

}
