package org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.UpcomingWorkJobLink;
import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.UpcomingWorkJobLinkAjaxDto;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Service
public class UpcomingWorkJobLinkServiceImpl implements UpcomingWorkJobLinkService{

    private static final Logger logger = LoggerFactory.getLogger(UpcomingWorkJobLinkServiceImpl.class);

    private final UpcomingWorkJobLinkRepository upcomingWorkJobLinkRepository;
    private final JobService jobService;

    public UpcomingWorkJobLinkServiceImpl(UpcomingWorkJobLinkRepository upcomingWorkJobLinkRepository,
                                          JobService jobService) {
        this.upcomingWorkJobLinkRepository = upcomingWorkJobLinkRepository;
        this.jobService = jobService;
    }

    @Override
    public List<UpcomingWorkJobLinkAjaxDto> getJobByUpcomingWork(UpcomingWork upcomingWork) {

        List<UpcomingWorkJobLink> jobLinkByUpcomingWorkAndActive = this.upcomingWorkJobLinkRepository.findAllByUpcomingWorkAndActiveTrue(upcomingWork);
        return jobLinkByUpcomingWorkAndActive.stream()
                .filter(upcomingWorkJobLink -> nonNull(upcomingWorkJobLink.getJob()))
                .map(UpcomingWorkJobLinkAjaxDto::new)
                .collect(Collectors.toList());
    }

    @Override
    public void addJobToUpcomingWork(List<Job> jobs, UpcomingWork upcomingWork) throws UpcomingWorkJobLinkException {

        if (upcomingWork == null){
            logger.debug(" upcomingWorkId not exist");
            throw new UpcomingWorkJobLinkException("The upcoming Work not exist");
        }
        List<UpcomingWorkJobLink> upcomingWorkJobLinks = new ArrayList<>();
        jobs.forEach(job -> upcomingWorkJobLinks.add(new UpcomingWorkJobLink(0,true,job,upcomingWork)));
        this.upcomingWorkJobLinkRepository.saveAll(upcomingWorkJobLinks);
    }

    @Override
    public void updateJobToUpcomingWork(List<UpcomingWorkJobLinkAjaxDto> upcomingWorkJobLinkAjaxDtos, UpcomingWork upcomingWork) throws UpcomingWorkJobLinkException {
        saveOrUpdataUpcomingWorkJob(upcomingWorkJobLinkAjaxDtos,upcomingWork);
    }

    private void saveOrUpdataUpcomingWorkJob(List<UpcomingWorkJobLinkAjaxDto> upcomingWorkJobLinkAjaxDtos, UpcomingWork upcomingWork) throws UpcomingWorkJobLinkException {
        if (upcomingWork != null){
            List<UpcomingWorkJobLink> upcomingWorkJobLinks = new ArrayList<>();

            for (UpcomingWorkJobLinkAjaxDto dto : upcomingWorkJobLinkAjaxDtos){

                Job job = this.jobService.findByJobNo(dto.getJobNo());
                if (job == null){
                    logger.debug("The job {} could not be found", dto.getJobNo());
                    throw new UpcomingWorkJobLinkException("The job "+ dto.getJobNo() + "could not be found");
                }

                if(job.getType()!=JobType.SITE){
                    throw new UpcomingWorkJobLinkException("The Job "+job.getIdentifier()+" is not site job");
                }

                if (!hasSameSubdivision(job, upcomingWork)){
                    logger.debug("The job {} do not have good subdivision", dto.getJobNo());
                    throw new UpcomingWorkJobLinkException("The job "+ dto.getJobNo() +" do not have good subdivision");
                }

                if (dto.getId() == null){
                    if (this.upcomingWorkJobLinkExist(upcomingWork,job)){
                        logger.debug("The link of job {} and upcoming work {}  already exist ",job.getIdentifier(), upcomingWork.getId());
                        throw new UpcomingWorkJobLinkException("The job "+job.getIdentifier()+" already exist");
                    }
                    UpcomingWorkJobLink upcomingWorkJobLink = new UpcomingWorkJobLink();
                    upcomingWorkJobLink.setUpcomingWork(upcomingWork);
                    upcomingWorkJobLink.setJob(job);
                    upcomingWorkJobLink.setActive(true);
                    upcomingWorkJobLinks.add(upcomingWorkJobLink);
                }else {
                    Optional<UpcomingWorkJobLink> upcomingWorkJobLink = this.upcomingWorkJobLinkRepository.findById(dto.getId());
                    upcomingWorkJobLink.ifPresent(upcomingWorkJobLink1 -> {
                        upcomingWorkJobLink1.setUpcomingWork(upcomingWork);
                        upcomingWorkJobLink1.setJob(job);
                        upcomingWorkJobLink1.setActive(dto.isActive());
                        upcomingWorkJobLinks.add(upcomingWorkJobLink1);
                    });
                }
            }
            this.upcomingWorkJobLinkRepository.saveAll(upcomingWorkJobLinks);
        }
    }

    @Override
    public void deleteUpcomingWorkJobLink(Integer id) throws UpcomingWorkJobLinkException {

        Optional<UpcomingWorkJobLink> upcomingWorkJobLink = this.upcomingWorkJobLinkRepository.findById(id);

        upcomingWorkJobLink.map(upcomingWorkJobLink1 -> {
            upcomingWorkJobLink1.setActive(false);
            return this.upcomingWorkJobLinkRepository.save(upcomingWorkJobLink1);
        }).orElseThrow(() -> new UpcomingWorkJobLinkException("The upcoming work job link could not be found"));
    }

    @Override
    public boolean upcomingWorkJobLinkExist(UpcomingWork upcomingWork, Job job) {
        return this.upcomingWorkJobLinkRepository.countAllByUpcomingWorkAndJobAndActiveTrue(upcomingWork, job) > 0;
    }

    private boolean hasSameSubdivision(Job job, UpcomingWork upcomingWork){
        return job.getOrganisation().getSubdivid() == upcomingWork.getOrganisation().getSubdivid();
    }

    @Override
    public void deleteAllUpcomingWorkJobLinkByUpcomingWork(UpcomingWork upcomingWork){
        this.upcomingWorkJobLinkRepository.deleteAllByUpcomingWork(upcomingWork);
    }

}
