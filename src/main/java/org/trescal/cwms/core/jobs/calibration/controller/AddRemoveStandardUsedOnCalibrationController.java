package org.trescal.cwms.core.jobs.calibration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.db.StandardUsedService;

@Controller
@JsonController
public class AddRemoveStandardUsedOnCalibrationController {

	@Autowired
	private StandardUsedService standardUsedService;

	@RequestMapping(value = "/addstandardusedoncalibration.json", method=RequestMethod.GET)
	@ResponseBody
	public ResultWrapper addStandardUsedOncalibration(
			@RequestParam(name = "calId", required = true) Integer calId,
			@RequestParam(name = "plantId", required = true) Integer plantId) {
		return standardUsedService.ajaxAddStandardUsedOnCalibration(plantId, calId);
	}
	
	@RequestMapping(value = "/deletestandardusedoncalibration.json", method = RequestMethod.GET)
	@ResponseBody
	public ResultWrapper deleteStandardUsedOnCalibration(
			@RequestParam(name = "standardId", required = true) Integer standardId) {
		return standardUsedService.ajaxDeleteStandardUsedOnCalibration(standardId);
	}

}
