package org.trescal.cwms.core.jobs.calibration.form;

import org.jasypt.digest.StandardStringDigester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.tools.DigestConfigCreator;
import org.trescal.cwms.core.tools.PasswordTools;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class ViewBatchCalibrationValidator extends AbstractBeanValidator
{
	@Autowired
	DigestConfigCreator configCreator;

	public void setConfigCreator(DigestConfigCreator configCreator)
	{
		this.configCreator = configCreator;
	}

	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(ViewBatchCalibrationForm.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		ViewBatchCalibrationForm form = (ViewBatchCalibrationForm) target;
		super.validate(form, errors);

		if (form.isBatchRequiresCourier() && form.isDespatch()
				&& (form.getCdtId() == null))
		{
			errors.rejectValue("cdtId", null, "A courier despatch type must be selected");
		}

		if ((form.getConsignmentNo() != null)
				&& (form.getConsignmentNo().length() > 100))
		{
			errors.rejectValue("consignmentNo", null, "Consignment number must not be longer than 100 characters");
		}

		/**
		 * BUSINESS VALIDATION
		 */
		// if manual printing, then password is required at this stage
		if (!form.isAutoPrinting() && form.isCertify())
		{
			// The user is trying to use an existing password
			if (form.getCurrentContact().getEncryptedPassword() != null)
			{
				Contact con = form.getCurrentContact();

				StandardStringDigester digester = new StandardStringDigester();
				digester.setConfig(this.configCreator.createDigesterConfig());

				if (!digester.matches(form.getPassword(), con.getEncryptedPassword()))
				{
					errors.rejectValue("password", "error.calibration.validator.wrongsignpassword");
				}
			}
			// the user is setting a new password
			else
			{
				if (!form.getPassword().equals(form.getConfirmPassword()))
				{
					errors.rejectValue("confirmPassword", "error.calibration.validator.confirmpassword");
				}
				else
				{
					if (PasswordTools.getPasswordStrength(form.getPassword()) < 3)
					{
						errors.rejectValue("password", "error.calibration.validator.passwordstrength");
					}
				}
			}
		}
	}
}