package org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.db;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportAction;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportDescription;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportInstruction;

public interface FaultReportDetailService
{
	ResultWrapper ajaxFindFaultReportDetail(String type, int id);

	ResultWrapper deactivateFaultReportDetail(String type, int faultRepDetailId, int personId, int subdivId);

	ResultWrapper editFaultReportDetail(String type, int faultRepDetailId, int personId, String value);

	/**
	 * Inserts a {@link FaultReportAction} into the database.
	 * 
	 * @param faultRepId the id of the {@link FaultReport}.
	 * @param personId the id of the {@link Contact}.
	 * @param value the value to save.
	 */
	ResultWrapper insertFaultReportAction(int faultRepId, int personId, String value);

	/**
	 * Inserts a {@link FaultReportDescription} into the database.
	 * 
	 * @param faultRepId the id of the {@link FaultReport}.
	 * @param personId the id of the {@link Contact}.
	 * @param value the value to save.
	 */
	ResultWrapper insertFaultReportDescription(int faultRepId, int personId, String value);

	/**
	 * Inserts a {@link FaultReportInstruction} into the database.
	 * 
	 * @param faultRepId the id of the {@link FaultReport}.
	 * @param personId the id of the {@link Contact}.
	 * @param value the value to save.
	 */
	ResultWrapper insertFaultReportInstruction(int faultRepId, int personId, String value);

	/**
	 * Updates {@link FaultReportInstruction} sets the show on third party
	 * delivery note boolean field to either true or false.
	 * 
	 * @param faultRepId the id of the fault report instruction
	 * @param showOnTPDN boolean value to indicate whether this instruction
	 *        should be shown on third party delivery notes.
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper updateFaultReportInstructionShowOnTPDN(int faultRepInstId, boolean showOnTPDN); 
}