package org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportInstruction;

public interface FaultReportInstructionDao extends BaseDao<FaultReportInstruction, Integer> {

}
