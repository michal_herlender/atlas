package org.trescal.cwms.core.jobs.repair.validatedoperation.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperation;

@Service("ValidatedFreeRepairOperationService")
public class ValidatedFreeRepairOperationServiceImpl extends BaseServiceImpl<ValidatedFreeRepairOperation, Integer> implements ValidatedFreeRepairOperationService
{
	@Autowired
	private ValidatedFreeRepairOperationDao validatedFreeRepairOperationDao;

	@Override
	protected BaseDao<ValidatedFreeRepairOperation, Integer> getBaseDao() {
		return validatedFreeRepairOperationDao;
	}
}