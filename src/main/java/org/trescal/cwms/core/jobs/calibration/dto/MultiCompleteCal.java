package org.trescal.cwms.core.jobs.calibration.dto;

import java.util.ArrayList;
import java.util.List;

import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.recall.entity.recallrule.RecallRuleInterval;

public class MultiCompleteCal
{
	private Calibration cal;
	private RecallRuleInterval duration;
	private List<MultiCompleteCalItem> items;
	private boolean selected;

	public MultiCompleteCal(Calibration cal, RecallRuleInterval duration)
	{
		this.cal = cal;
		this.duration = duration;
		this.selected = false;
		this.items = new ArrayList<MultiCompleteCalItem>();
	}

	public Calibration getCal()
	{
		return this.cal;
	}

	public RecallRuleInterval getDuration()
	{
		return this.duration;
	}

	public List<MultiCompleteCalItem> getItems()
	{
		return this.items;
	}

	public boolean isSelected()
	{
		return this.selected;
	}

	public void setCal(Calibration cal)
	{
		this.cal = cal;
	}

	public void setDuration(RecallRuleInterval duration)
	{
		this.duration = duration;
	}

	public void setItems(List<MultiCompleteCalItem> items)
	{
		this.items = items;
	}

	public void setSelected(boolean selected)
	{
		this.selected = selected;
	}
}