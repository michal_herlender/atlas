package org.trescal.cwms.core.jobs.jobitem.controller;

import org.trescal.cwms.core.instrument.dto.InstrumentSearchedForNewJobItemSearchDto;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelForNewJobItemSearchDto;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NewJobItemSearchOut {

    private PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> instruments;
    private PagedResultSet<InstrumentModelForNewJobItemSearchDto> companyModels;
    private PagedResultSet<InstrumentModelForNewJobItemSearchDto> matchingModels;
}
