package org.trescal.cwms.core.jobs.jobitem.entity.lookupsource;

public enum CalibrationTypeLookupSource
{
	QUOTATION, JOB, INSTRUMENTMODEL, SYSTEM_DEFAULT;
}
