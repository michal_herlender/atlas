package org.trescal.cwms.core.jobs.repair.operation;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.jobs.repair.component.FreeRepairComponent;
import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationStateEnum;
import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationTypeEnum;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperation;

@Entity
@Table(name = "freerepairoperation")
public class FreeRepairOperation {

	private Integer operationId;
	private String name;
	private Integer labourTime;
	private BigDecimal cost;
	private int position;
	private RepairOperationTypeEnum operationType;
	private RepairOperationStateEnum operationStatus;
	private Boolean addedAfterRiRValidation;
	private RepairInspectionReport repairInspectionReport;
	private ValidatedFreeRepairOperation validatedFreeRepairOperation;
	private Set<FreeRepairComponent> freeRepairComponents;
	private Company tpCompany;
	private RepairCompletionReport repairCompletionReport;

	@Column(name = "cost", nullable = true, precision = 10, scale = 2)
	public BigDecimal getCost() {
		return this.cost;
	}

	@Column(name = "labourtime")
	public Integer getLabourTime() {
		return this.labourTime;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public Integer getOperationId() {
		return operationId;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	@Column(name = "position")
	public int getPosition() {
		return position;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "operationtype")
	public RepairOperationTypeEnum getOperationType() {
		return operationType;
	}

	@Column(name = "addedafterrirvalidation")
	public Boolean getAddedAfterRiRValidation() {
		return addedAfterRiRValidation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "repairinspectionreportid", foreignKey = @ForeignKey(name = "freerepairoperation_repairinspectionreport_FK"))
	public RepairInspectionReport getRepairInspectionReport() {
		return repairInspectionReport;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "operationstatus")
	public RepairOperationStateEnum getOperationStatus() {
		return operationStatus;
	}

	@OneToOne(mappedBy = "freeRepairOperation", cascade = CascadeType.ALL)
	public ValidatedFreeRepairOperation getValidatedFreeRepairOperation() {
		return validatedFreeRepairOperation;
	}

	@OneToMany(mappedBy = "freeRepairOperation", cascade = { CascadeType.MERGE}, fetch = FetchType.LAZY, orphanRemoval = true)
	public Set<FreeRepairComponent> getFreeRepairComponents() {
		return freeRepairComponents;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tpcoid", foreignKey = @ForeignKey(name = "freerepairoperation_company_FK"))
	public Company getTpCompany() {
		return tpCompany;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "repaircompletionreportid", foreignKey = @ForeignKey(name = "freerepairoperation_repaircompletionreport_FK"))
	public RepairCompletionReport getRepairCompletionReport() {
		return repairCompletionReport;
	}

	public void setRepairCompletionReport(RepairCompletionReport repairCompletionReport) {
		this.repairCompletionReport = repairCompletionReport;
	}

	public void setFreeRepairComponents(Set<FreeRepairComponent> freeRepairComponents) {
		this.freeRepairComponents = freeRepairComponents;
	}

	public void setValidatedFreeRepairOperation(ValidatedFreeRepairOperation validatedFreeRepairOperation) {
		this.validatedFreeRepairOperation = validatedFreeRepairOperation;
	}

	public void setOperationStatus(RepairOperationStateEnum operationStatus) {
		this.operationStatus = operationStatus;
	}

	public void setOperationType(RepairOperationTypeEnum operationType) {
		this.operationType = operationType;
	}

	public void setRepairInspectionReport(RepairInspectionReport repairInspectionReport) {
		this.repairInspectionReport = repairInspectionReport;
	}

	public void setTpCompany(Company tpCompany) {
		this.tpCompany = tpCompany;
	}

	public void setAddedAfterRiRValidation(Boolean addedAfterRiRValidation) {
		this.addedAfterRiRValidation = addedAfterRiRValidation;
	}

	public void setLabourTime(Integer labourTime) {
		this.labourTime = labourTime;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOperationId(Integer operationId) {
		this.operationId = operationId;
	}
}