package org.trescal.cwms.core.jobs.repair.repairinspectionreport.db;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.repair.component.db.FreeRepairComponentService;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairInspectionReportFormDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.transformer.RepairInspectionReportFormTransformer;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.validatedcomponent.ValidatedFreeRepairComponent;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperationComparator;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_RIRReviewByCSR;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_ValidateRIRByManager;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_ValidateRIRByManager.Input;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_ValidateRIRByTechnician;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.*;

@Service("RepairInspectionReportService")
public class RepairInspectionReportServiceImpl extends BaseServiceImpl<RepairInspectionReport, Integer>
    implements RepairInspectionReportService {
    @Autowired
    private RepairInspectionReportDao dao;
    @Autowired
    private RepairInspectionReportFormTransformer rirTransformer;
    @Autowired
    private HookInterceptor_ValidateRIRByTechnician validateRirByTechnician;
    @Autowired
    private HookInterceptor_ValidateRIRByManager validateRirByManager;
    @Autowired
    private HookInterceptor_RIRReviewByCSR rirReviewByCsr;
    @Autowired
    private UserService userService;
    @Autowired
    private NumerationService numerationService;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private CapabilityAuthorizationService capabilityAuthorizationService;
    @Autowired
    private FreeRepairComponentService frcService;

    @Override
    protected BaseDao<RepairInspectionReport, Integer> getBaseDao() {
        return dao;
    }

    @Override
    public RepairInspectionReport getRirByJobItemIdAndSubdivId(Integer jobitemid, Integer subdivId) {
        return dao.getRirByJobItemIdAndSubdivId(jobitemid, subdivId);
    }

	@Override
	public void validateByManager(RepairInspectionReportFormDTO form, String username, Integer subdivId) {
		RepairInspectionReport rir = rirTransformer.transform(form);
		rir.setValidatedByManager(true);
		rir.setManager(this.userService.get(username).getCon());
		rir.setValidatedByManagerOn(new Date());

        RepairCompletionReport currentRcr = null;

		List<RepairInspectionReport> jiRirs = this.dao.getAllRirByJobItemId(rir.getJi().getJobItemId());
        if (jiRirs != null)
            currentRcr = jiRirs.stream().map(RepairInspectionReport::getRepairCompletionReport)
                .filter(Objects::nonNull).findFirst().orElse(null);

		if (currentRcr == null) {
			currentRcr = new RepairCompletionReport();
			Subdiv allocatedSubdiv = subdivService.get(subdivId);
			String rcrNumber = numerationService.generateNumber(NumerationType.REPAIR_COMPLETION_REPORT,
					allocatedSubdiv.getComp(), allocatedSubdiv);
            currentRcr.setRcrNumber(rcrNumber);
            currentRcr.setRepairInspectionReports(new HashSet<>());
            currentRcr.setValidatedFreeRepairOperations(
                new TreeSet<>(new ValidatedFreeRepairOperationComparator()));
		}

		currentRcr.getRepairInspectionReports().add(rir);
		rir.setRepairCompletionReport(currentRcr);

		for (FreeRepairOperation o : rir.getFreeRepairOperations()) {
			ValidatedFreeRepairOperation vfro = new ValidatedFreeRepairOperation();

			vfro.setFreeRepairOperation(o);
			vfro.setLabourTime(vfro.getFreeRepairOperation().getLabourTime());
			vfro.setCost(vfro.getFreeRepairOperation().getCost());
			vfro.setValidatedStatus(vfro.getFreeRepairOperation().getOperationStatus());
			vfro.setRepairCompletionReport(currentRcr);
			o.setValidatedFreeRepairOperation(vfro);
			if (o.getFreeRepairComponents() != null && !o.getFreeRepairComponents().isEmpty()) {
				vfro.setValidatedFreeRepairComponents(new HashSet<>());
				o.getFreeRepairComponents().forEach(c -> {
					ValidatedFreeRepairComponent vfrc = new ValidatedFreeRepairComponent();
					vfrc.setFreeRepairComponent(c);
					vfrc.setCost(c.getCost());
					vfrc.setQuantity(c.getQuantity());
					vfrc.setValidatedStatus(c.getStatus());
					vfrc.setValidatedFreeRepairOperation(vfro);
					vfro.getValidatedFreeRepairComponents().add(vfrc);
					c.setValidatedFreeRepairComponent(vfrc);
				});
			}
			currentRcr.getValidatedFreeRepairOperations().add(vfro);
		}

		this.save(rir);
		// save freerepaircomponents
        rir.getFreeRepairOperations().forEach(o -> o.getFreeRepairComponents().forEach(c -> this.frcService.save(c)));
		Input input = new Input(rir, form.getTimeSpent());
		validateRirByManager.recordAction(input);
	}

	@Override
	public RepairInspectionReport validateByTechnician(RepairInspectionReportFormDTO form, String username,
			Integer subdivId) {

		RepairInspectionReport rir = rirTransformer.transform(form);
		rir.setCompletedByTechnician(true);
		rir.setTechnician(this.userService.get(username).getCon());
		rir.setCompletedByTechnicianOn(new Date());

        this.save(rir);
		// save freerepaircomponents
        rir.getFreeRepairOperations().forEach(o -> o.getFreeRepairComponents().forEach(c -> this.frcService.save(c)));
		org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_ValidateRIRByTechnician.Input input = new org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_ValidateRIRByTechnician.Input(
				rir, form.getTimeSpent());
		validateRirByTechnician.recordAction(input);
		return rir;
	}

	@Override
	public void refresh(RepairInspectionReport rir) {
		dao.refrech(rir);
	}

	@Override
	public List<RepairInspectionReport> getAllRirByJobItemId(int jobItemId) {
		return dao.getAllRirByJobItemId(jobItemId);
	}

	@Override
	public RepairInspectionReport getLatestValidatedRir(int jobItemId) {
		return dao.getLatestValidatedRir(jobItemId);
	}

	@Override
	public RepairInspectionReport getValidatedRirByJobItemAndValidationDate(int jobItemId, int allocatedSubdiv,
			boolean completedByTechnician) {
		return dao.getValidatedRirByJobItemAndValidationDate(jobItemId, allocatedSubdiv, completedByTechnician);
	}

	@Override
	public void saveRepairInspection(RepairInspectionReportFormDTO form, String username,
			KeyValue<Integer, String> subdivDto) {
		RepairInspectionReport rir = rirTransformer.transform(form);
		if (form.getCreationDate() == null) {
			form.setCreationDate(new Date());
			rir.setCreationDate(form.getCreationDate());
			rir.setCreatedBy(userService.get(username).getCon());
		}
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		rir.setOrganisation(allocatedSubdiv);
        String rirNumber = numerationService.generateNumber(NumerationType.REPAIR_INSPECTION_REPORT,
            allocatedSubdiv.getComp(), allocatedSubdiv);
        rir.setRirNumber(rirNumber);
        this.dao.persist(rir);
        // save freerepaircomponents
        rir.getFreeRepairOperations().forEach(o -> o.getFreeRepairComponents().forEach(c -> this.frcService.save(c)));
    }

    @Override
    public Boolean isAccreditedToPerformRirInitialization(JobItemWorkRequirement jiWorkRequirement, Contact con) {
        val validLevels = io.vavr.collection.List.of(AccreditationLevel.PERFORM, AccreditationLevel.SIGN,
            AccreditationLevel.SUPERVISED, AccreditationLevel.APPROVE);
        return isAuthorizedToGivenLevels(jiWorkRequirement, con, validLevels);
    }

    private Boolean isAuthorizedToGivenLevels(JobItemWorkRequirement jobItemWorkRequirement, Contact contact, io.vavr.collection.List<AccreditationLevel> levels) {
        if (contact != null) {
            val accStat = this.capabilityAuthorizationService.authorizedFor(jobItemWorkRequirement.getWorkRequirement().getCapability().getId(),
                jobItemWorkRequirement.getWorkRequirement().getServiceType() != null ?
                    jobItemWorkRequirement.getWorkRequirement().getServiceType().getCalibrationType().getCalTypeId() :
                    jobItemWorkRequirement.getJobitem().getServiceType().getCalibrationType().getCalTypeId(), contact.getPersonid());
            return accStat.filterOrElse(levels::contains, unused -> "ignored message").isRight();
        }
        return false;
    }

    @Override
    public Boolean isAccreditedToPerformRirTechCompletion(JobItemWorkRequirement jiWorkRequirement, Contact con) {
        val validLevels = io.vavr.collection.List.of(AccreditationLevel.SIGN,
            AccreditationLevel.APPROVE);
        return isAuthorizedToGivenLevels(jiWorkRequirement, con, validLevels);
    }

    @Override
    public Boolean isAccreditedToPerformRirManagerValidation(JobItemWorkRequirement jiWorkRequirement, Contact con) {
        val validLevels = io.vavr.collection.List.of(AccreditationLevel.APPROVE);
        return isAuthorizedToGivenLevels(jiWorkRequirement, con, validLevels);
    }

	@Override
	public void reviewByCsr(RepairInspectionReportFormDTO form, String username) {
		RepairInspectionReport rir = rirTransformer.transform(form);
		rir.setReviewedByCsr(true);
		rir.setCsr(this.userService.get(username).getCon());
		rir.setReviewedByCsrOn(new Date());
		this.save(rir);
		HookInterceptor_RIRReviewByCSR.Input input = new HookInterceptor_RIRReviewByCSR.Input(
				rir);
		rirReviewByCsr.recordAction(input);
	}

}