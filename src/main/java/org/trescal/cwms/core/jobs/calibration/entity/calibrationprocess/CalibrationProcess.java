package org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;

@Entity
@Table(name = "calibrationprocess")
public class CalibrationProcess {
	private Boolean actionAfterIssue;

	/**
	 * Defines if the process is a component is built in to this web application.
	 * i.e. the "CWMS" process (which uses the calibration procedure runner).
	 * IMPORTANT: Only 1 process in the database should have this set to true.
	 */
	private Boolean applicationComponent;

	private List<Calibration> calibrations;
	private String description;
	private String fileExtension;
	private Integer id;
	private Boolean issueImmediately;
	private String process;
	private Boolean quickSign;
	private Boolean signingSupported;
	private Boolean signOnIssue;
	private Boolean uploadAfterIssue;
	private Boolean uploadBeforeIssue;
	private String toolLink;
	private String toolContinueLink;

	public CalibrationProcess() {
		actionAfterIssue = Boolean.FALSE;
		applicationComponent = Boolean.FALSE;
		issueImmediately = Boolean.FALSE;
		quickSign = Boolean.FALSE;
		signingSupported = Boolean.FALSE;
		signOnIssue = Boolean.FALSE;
		uploadAfterIssue = Boolean.FALSE;
		uploadBeforeIssue = Boolean.FALSE;
	}

	/**
	 * @return the calibrations
	 */
	@OneToMany(mappedBy = "calProcess")
	public List<Calibration> getCalibrations() {
		return this.calibrations;
	}

	/**
	 * @return the description
	 */
	@Length(max = 200)
	@Column(name = "description", length = 200)
	public String getDescription() {
		return this.description;
	}

	/**
	 * @return the fileExtension
	 */
	@Length(max = 6)
	@Column(name = "fileextension", length = 6)
	public String getFileExtension() {
		return this.fileExtension;
	}

	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getId() {
		return this.id;
	}

	/**
	 * @return the process
	 */
	@Length(min = 1, max = 100)
	@Column(name = "process", length = 100)
	public String getProcess() {
		return this.process;
	}

	@Column(name = "actionafterissue", columnDefinition = "bit")
	public Boolean getActionAfterIssue() {
		return this.actionAfterIssue;
	}

	@Column(name = "applicationcomponent", columnDefinition = "bit")
	public Boolean getApplicationComponent() {
		return this.applicationComponent;
	}

	@Column(name = "issueimmediately", columnDefinition = "bit")
	public Boolean getIssueImmediately() {
		return this.issueImmediately;
	}

	@Column(name = "quicksign", columnDefinition = "bit")
	public Boolean getQuickSign() {
		return this.quickSign;
	}

	@Column(name = "signingsupported", columnDefinition = "bit")
	public Boolean getSigningSupported() {
		return this.signingSupported;
	}

	@Column(name = "signonissue", columnDefinition = "bit")
	public Boolean getSignOnIssue() {
		return this.signOnIssue;
	}

	@Column(name = "uploadafterissue", columnDefinition = "bit")
	public Boolean getUploadAfterIssue() {
		return this.uploadAfterIssue;
	}

	@Column(name = "uploadbeforeissue", columnDefinition = "bit")
	public Boolean getUploadBeforeIssue() {
		return this.uploadBeforeIssue;
	}

	@Column(name = "toollink")
	public String getToolLink() {
		return toolLink;
	}

	@Column(name = "toolcontinuelink")
	public String getToolContinueLink() {
		return toolContinueLink;
	}

	public void setActionAfterIssue(Boolean actionAfterIssue) {
		this.actionAfterIssue = actionAfterIssue;
	}

	public void setApplicationComponent(Boolean applicationComponent) {
		this.applicationComponent = applicationComponent;
	}

	/**
	 * @param calibrations
	 *            the calibrations to set
	 */
	public void setCalibrations(List<Calibration> calibrations) {
		this.calibrations = calibrations;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param fileExtension
	 *            the fileExtension to set
	 */
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	public void setIssueImmediately(Boolean issueImmediately) {
		this.issueImmediately = issueImmediately;
	}

	/**
	 * @param process
	 *            the process to set
	 */
	public void setProcess(String process) {
		this.process = process;
	}

	public void setQuickSign(Boolean quickSign) {
		this.quickSign = quickSign;
	}

	public void setSigningSupported(Boolean signingSupported) {
		this.signingSupported = signingSupported;
	}

	public void setSignOnIssue(Boolean signOnIssue) {
		this.signOnIssue = signOnIssue;
	}

	public void setUploadAfterIssue(Boolean uploadAfterIssue) {
		this.uploadAfterIssue = uploadAfterIssue;
	}

	public void setUploadBeforeIssue(Boolean uploadBeforeIssue) {
		this.uploadBeforeIssue = uploadBeforeIssue;
	}

	public void setToolLink(String toolLink) {
		this.toolLink = toolLink;
	}
	
	public void setToolContinueLink(String toolContinueLink) {
		this.toolContinueLink = toolContinueLink;
	}
}