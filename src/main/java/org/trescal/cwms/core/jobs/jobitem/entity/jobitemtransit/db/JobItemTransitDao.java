package org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.JobItemTransit;

public interface JobItemTransitDao extends BaseDao<JobItemTransit, Integer> {

}