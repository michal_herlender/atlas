package org.trescal.cwms.core.jobs.job.form;

import java.util.HashMap;
import java.util.Map;

import org.trescal.cwms.core.pricing.invoice.dto.PNotInvoiced;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditJobInvoicingForm {

	private int jobid;
	private Map<Integer, PNotInvoiced> notInvoiced;
	private Map<Integer, PNotInvoiced> expensesNotInvoiced;

	public EditJobInvoicingForm() {
		this.notInvoiced = new HashMap<>();
		this.expensesNotInvoiced = new HashMap<>();
	}

}