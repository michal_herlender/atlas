package org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.deliverynote.form.TPInstructionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction.TPInstruction;

public interface TPInstructionDao extends BaseDao<TPInstruction, Integer>
{
	
	/**
	 * Get all item accessories for a job.
	 * 
	 * @param jobId  identifier of the job
	 */
	List<TPInstructionDTO> getAllByJob(Integer jobId);
}