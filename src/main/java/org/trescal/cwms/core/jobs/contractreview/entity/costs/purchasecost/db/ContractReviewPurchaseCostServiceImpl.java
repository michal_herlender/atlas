package org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.db;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewLinkedPurchaseCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;

@Service
public class ContractReviewPurchaseCostServiceImpl implements ContractReviewPurchaseCostService {
	protected final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	private ContractReviewPurchaseCostDao ContractReviewPurchaseCostDao;

	/**
	 * Create empty zero valued cost
	 * 
	 * @return
	 */
	@Override
	public ContractReviewPurchaseCost createEmptyCost(JobItem jobItem, boolean active) {
		ContractReviewPurchaseCost cost = new ContractReviewPurchaseCost();
		cost.setActive(true);
		cost.setAutoSet(true);
		cost.setDiscountRate(new BigDecimal(0.00));
		cost.setDiscountValue(new BigDecimal(0.00));
		cost.setFinalCost(new BigDecimal(0.00));
		cost.setTotalCost(new BigDecimal(0.00));
		cost.setJobitem(jobItem);
		cost.setCostType(CostType.PURCHASE);
		cost.setCostSrc(CostSource.MANUAL);
		cost.setActive(active);
		return cost;
	}

	@Override
	public ContractReviewPurchaseCost copyContractReviewCost(ContractReviewPurchaseCost old, JobItem copyToJobitem) {
		ContractReviewPurchaseCost newCost = null;

		if (old != null) {
			newCost = new ContractReviewPurchaseCost();

			BeanUtils.copyProperties(old, newCost);

			newCost.setCostid(null);
			newCost.setJobitem(copyToJobitem);
			newCost.setAutoSet(true);

			if (old.getLinkedCost() != null) {
				ContractReviewLinkedPurchaseCost oldLinkedCost = old.getLinkedCost();
				ContractReviewLinkedPurchaseCost newLinkedCost = new ContractReviewLinkedPurchaseCost();
				BeanUtils.copyProperties(oldLinkedCost, newLinkedCost);
				newLinkedCost.setId(0);
				newLinkedCost.setPurchaseCost(newCost);
				newCost.setLinkedCost(newLinkedCost);
			}
		}
		return newCost;
	}

	@Override
	public ContractReviewPurchaseCost findContractReviewPurchaseCost(int id) {
		return this.ContractReviewPurchaseCostDao.find(id);
	}
}