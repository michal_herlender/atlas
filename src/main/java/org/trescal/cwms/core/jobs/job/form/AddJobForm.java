package org.trescal.cwms.core.jobs.job.form;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.CallOffItem;
import org.trescal.cwms.core.logistics.utils.RecordTypeEnum;

import lombok.Data;

@Data
public class AddJobForm {

	private Integer addrid;
	private Job basedOnJob;
	private Integer bookedInAddrId;
	private Integer bookedInLocId;
	private List<CallOffItem> callOffItems;
	private String clientref;
	private Integer coid;
	private String createJobFrom;
	private String currencyCode;
	private BigDecimal estCarriageOut;
	private Job job;
	private JobType jobType;
	private Integer locid;
	private List<Integer> oldJobItemIds;
	private List<Integer> oldJobServiceIds;
	private Integer personid;
	private List<String> poCommentList;
	private String ponumber;
	private List<String> poNumberList;
	private List<Integer> poIdList;
	private List<Integer> selectedCallOffItems;
	private List<Integer> selectedCollectedInsts;
	private Integer subdivid;

	// properties for prebooking job
	private RecordTypeEnum recordType;
	private Integer exchangeFormat;
	private Integer asnId;
	private MultipartFile uploadFile;
	private LocalDateTime receiptDate;
	@NotNull
	private Boolean overrideDateInOnJobItems;
	private LocalDateTime pickupDate;
	private Integer transportIn;
	private Integer carrier;
	private String trackingNumber;
	private Integer transportOut;
	private Integer numberOfPackages;
	private String packageType;
	private String storageArea;
}