package org.trescal.cwms.core.jobs.repair.validatedoperation.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperation;

public interface ValidatedFreeRepairOperationDao extends BaseDao<ValidatedFreeRepairOperation, Integer> {
	
}