package org.trescal.cwms.core.jobs.jobitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db.WorkRequirementService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.form.WorkRequirementForm;
import org.trescal.cwms.core.jobs.jobitem.form.WorkRequirementValidator;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME,
    Constants.SESSION_ATTRIBUTE_COMPANY})
public class WorkRequirementController {
    @Autowired
    private JobItemService jobItemService;
    @Autowired
    private WorkRequirementService wrService;
    @Autowired
    private CapabilityService capabilityService;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private WorkRequirementValidator validator;

    public static final String VIEW_NAME = "trescal/core/jobs/jobitem/workrequirementoverlay";
    public static final String VIEW_NAME_SUCCESS = "trescal/core/jobs/jobitem/workrequirementsuccess";
    public static final String FORM_NAME = "form";
	public static final String ACTION_ADD = "add";
    public static final String ACTION_EDIT_NOT_ALLOWED = "editNotAllowed";
	
	@InitBinder(FORM_NAME)
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
	
	@ModelAttribute("subdivsWithProcedure")
	public List<Subdiv> subdivsWithProcedure() {
        return capabilityService.getAllSubdivsWithCapabilities();
	}
	
	@ModelAttribute("subdivsWithProceduresMap")
	public Map<CompanyKeyValue, List<SubdivKeyValue>> subdivsWithProceduresMap(
			@ModelAttribute("subdivsWithProcedure") List<Subdiv> subdivsWithProcedure) {
		return subdivsWithProcedure.stream()
            .map(SubdivKeyValue::new)
				.collect(Collectors.groupingBy(
						skv -> new CompanyKeyValue(subdivService.get(skv.getKey()).getComp())));
	}
	
	// Used for binding of current values; future use in form submission?
	@ModelAttribute(FORM_NAME)
	public WorkRequirementForm formBackingObject(
        @RequestParam(name = "jobItemId") int jobItemId,
        @RequestParam(name = "wrId", required = false, defaultValue = "0") int wrId,
        @RequestParam(name = "modelId", required = false) Integer modelId,
        @ModelAttribute("subdivsWithProcedure") List<Subdiv> subdivsWithProcedure,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> allocatedSubdivDto,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyDto,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		WorkRequirementForm form = new WorkRequirementForm();
		form.setJobItemId(jobItemId);
		form.setModelId(modelId);
		if (wrId != 0) {
			WorkRequirement wr = this.wrService.get(wrId);
            Company wrComp = null;
            form.setWrId(wrId);
            form.setType(this.jobItemService.findJobItem(jobItemId).getJob().getType().toString());
            if (wr.getServiceType() != null) {
                form.setServiceTypeId(wr.getServiceType().getServiceTypeId());
            } else {
                form.setServiceTypeId(0);
            }
            form.setRequirementText(wr.getRequirement());
            form.setAction("edit");
            if (wr.getWorkInstruction() != null) {
                WorkInstruction wi = wr.getWorkInstruction();
                form.setWorkInstructionId(String.valueOf(wi.getId()));
                form.setWorkInstructionTitle(wi.getTitle());
                wrComp = wi.getOrganisation();
                form.setCoid(wrComp.getCoid());        // May be overwritten by procedure - but thats OK (validation ensures match)
                form.setSubdivId(allocatedSubdivDto.getKey());
            }
            // Only one procedure and one work instruction per work requirement at most
            if (wr.getCapability() != null) {
                form.setProcedureId(String.valueOf(wr.getCapability().getId()));
                form.setProcedureReferenceAndName(wr.getCapability().getReference() + " - " + wr.getCapability().getName());
                wrComp = wr.getCapability().getOrganisation().getComp();
                form.setCoid(wrComp.getCoid());
                form.setSubdivId(wr.getCapability().getOrganisation().getSubdivid());
            }
            // If procedure and WI are both blank (some existing legacy data, blank new work requirements),
            // the subdiv can be assumed to be the logged in subdiv
            if (wrComp == null) {
                wrComp = this.companyService.get(companyDto.getKey());
                form.setCoid(wrComp.getCoid());
                form.setSubdivId(allocatedSubdivDto.getKey());
            }

            // Access control - Work Requirement should only be editable if the user has rights for company of existing WI
            if (subdivsWithProcedure.stream()
                .map(Subdiv::getComp).collect(Collectors.toList()).contains(wrComp)) {
                form.setEditable(true);
            } else {
                form.setEditable(false);
                form.setAction(ACTION_EDIT_NOT_ALLOWED);
            }
			
		}
		else {
			JobItem jobItem = this.jobItemService.findJobItem(jobItemId);
			
			form.setAction(ACTION_ADD);
			form.setType(jobItem.getJob().getType().toString());
			form.setCoid(companyDto.getKey());
            form.setSubdivId(allocatedSubdivDto.getKey());
            form.setServiceTypeId(jobItem.getServiceType().getServiceTypeId());
            form.setEditable(true);
		}
		
		return form;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="workrequirementoverlay.htm")
	public String showOverlay(Model model, Locale locale,
			@ModelAttribute(FORM_NAME) WorkRequirementForm form) {
		JobType jobType = null;
		if (form.getJobItemId() != 0) {
			JobItem jobItem = this.jobItemService.findJobItem(form.getJobItemId());
			jobType = jobItem.getJob().getType();
		}
		model.addAttribute("serviceTypes", this.serviceTypeService.getDTOList(DomainType.INSTRUMENTMODEL, jobType, locale, true));
		if (form.isEditable()) return VIEW_NAME;
		else {
			// No edit permitted, populate model for read-only view
			WorkRequirement wr = this.wrService.get(form.getWrId());
			model.addAttribute("wr", wr);
			return VIEW_NAME_SUCCESS;
		}
	}

	@RequestMapping(method=RequestMethod.POST, value="workrequirementoverlay.htm")
	public String updateWorkRequirement(Model model, Locale locale,
			@Validated @ModelAttribute(FORM_NAME) WorkRequirementForm form, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return showOverlay(model, locale, form);
		}
		int wiId = 0;
		if (form.getWorkInstructionId() != null && form.getWorkInstructionId().length() > 0) {
            wiId = Integer.parseInt(form.getWorkInstructionId());
		}
		
		int procId = 0;
		if (form.getProcedureId().length() > 0) {
            procId = Integer.parseInt(form.getProcedureId());
		}

        WorkRequirement wr;
		if (form.getAction().equals(ACTION_ADD)) {
			wr = this.wrService.createWorkRequirement(form.getJobItemId(), procId, wiId, 
					form.getServiceTypeId(), form.getRequirementText());
		}
		else {
			// Updating existing WorkRequirement
			wr = this.wrService.updateWorkRequirement(form.getWrId(), procId, wiId, 
					form.getServiceTypeId(), form.getRequirementText());
		}
		model.addAttribute("wr", wr);
		return VIEW_NAME_SUCCESS;
	}
}