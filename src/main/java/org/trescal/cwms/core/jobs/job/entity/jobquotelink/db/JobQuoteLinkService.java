package org.trescal.cwms.core.jobs.job.entity.jobquotelink.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.dto.JobQuoteLinkDTO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;

public interface JobQuoteLinkService extends BaseService<JobQuoteLink, Integer>
{
	/**
	 * Returns all {@link JobQuoteLink} entities that have a {@link Quotation}
	 * and {@link Job} matching the given ids.
	 * 
	 * @param quoteid the id of the {@link Quotation}.
	 * @param jobid the id of the {@link Job}.
	 * @return list of matching {@link JobQuoteLink}.
	 */
	List<JobQuoteLink> findJobQuoteLinks(int quoteid, int jobid);
	
	/**
	 * Returns a specific amount of {@link JobQuoteLink}'s linked to a
	 * {@link Quotation}.
	 * 
	 * @param quoteid id of the quote to retrieve {@link Job}'s for
	 * @param page the current page number, if null or 0 then paging is
	 *        disabled.
	 * @param resPerPage number of results to show per page.
	 * @return {@link List} of {@link JobQuoteLink}'s
	 */
	List<JobQuoteLink> findJobQuoteLinksResultset(int quoteid, Integer page, Integer resPerPage);
	
	/**
	 * Returns a {@link ResultWrapper} containing a list of {@link JobQuoteLink}
	 * 's linked to a {@link Quotation}
	 * 
	 * @param quoteid id of the quote to retrieve {@link Job}'s for
	 * @param page the current page number, if null or 0 then paging is
	 *        disabled.
	 * @param resPerPage number of results to show per page.
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper findJobQuoteLinksResultsetDWR(int quoteid, Integer page, Integer resPerPage);
	
	/**
	 * Returns count of all {@link JobQuoteLink}'s for the quote id supplied
	 * 
	 * @param quoteid id of the {@link Quotation}
	 * @return {@link Integer} count of {@link JobQuoteLink}'s
	 */
	Integer getCountJobQuoteLinks(int quoteid);
	
	List<Integer> getModelIdsFromJobQuotes(Integer jobId, Integer jobQuoteLinkId, Boolean similar);

	/**
	 * Links the {@link Quotation} and {@link Job}. Tests that both exist and
	 * that both are not already linked.
	 * 
	 * @return {@link ResultWrapper} indicating if this was a success, if
	 *         sucessful then the {@link JobQuoteLink} is set as the results.
	 */
	JobQuoteLinkDTO linkJobToQuote(Quotation quotation, Job job, Contact currentContact);
}