package org.trescal.cwms.core.jobs.job.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.jobs.job.dto.BpoDTO;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.enums.Scope;

@Controller
@RequestMapping("bposelection.htm")
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class BPOSelectionController {

	@Autowired
	private BPOService bpoService;

	@GetMapping
	public String onRequest(@RequestParam(name = "jobitemid", required = false) Integer jobItemId,
			@RequestParam(name = "jobid", required = true) Integer jobId,
			@RequestParam(name = "activeonly", required = false, defaultValue = "true") Boolean activeOnly,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyDto, Model model) {
		Map<Scope, List<BpoDTO>> bpos = bpoService.findAllForJob(jobId, activeOnly);
		model.addAttribute("contactBPOs", bpos.get(Scope.CONTACT));
		model.addAttribute("subdivBPOs", bpos.get(Scope.SUBDIV));
		model.addAttribute("companyBPOs", bpos.get(Scope.COMPANY));
		model.addAttribute("jobItemId", jobItemId);
		model.addAttribute("jobId", jobId);
		return "trescal/core/jobs/bposelection";
	}
}