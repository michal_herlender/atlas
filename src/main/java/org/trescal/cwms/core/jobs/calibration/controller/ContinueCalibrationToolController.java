package org.trescal.cwms.core.jobs.calibration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;

@Controller
@JsonController
public class ContinueCalibrationToolController {

	@Autowired
	private CalibrationProcessService calProcessService;
	@Autowired
	private CertificateService certificateService;

	private String fillToolLink(String toolContinueLink, String certificateNumber) {
		return toolContinueLink.replace("{0}", certificateNumber);
	}

	@RequestMapping(value = "/continuecalibrationtool.json", method = RequestMethod.GET)
	@ResponseBody
	public String onToolSubmit(@RequestParam(name = "jobItemId", required = true) Integer jobItemId,
			@RequestParam(name = "calProcess", required = true) Integer calProcessId) {
		String certificateNumber = certificateService.getReservedCertificateNumber(jobItemId);
		String toolContinueLink = calProcessService.get(calProcessId).getToolContinueLink();
		return fillToolLink(toolContinueLink, certificateNumber);
	}
}