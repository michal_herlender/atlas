package org.trescal.cwms.core.jobs.jobitem.entity.jobitem;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BasketItemDto {
    private Integer instId;
    private Integer modelId;
    private String part;
    private Integer existingGroupId;
    private Integer temporalGroupId;
    private Integer baseItemId;
    private KeyValue<Integer, String> quotationItem;
    private String modelName;
    private String modelType;
    private Integer mfrId;
    private String mfrName;
    private Integer workInstructionId;
    private String workInstructionTitle;

    private Integer instPersonId;
    private Integer instAddressId;

    private String serialNo;
    private String plantNo;
    private Integer calFrequency;
    @JsonSerialize(using = IntervalUnitJsonConverter.class)
    private IntervalUnit calFrequencyUnit;
    private Integer serviceTypeSource;
    private Integer serviceTypeId;

    private Integer procId;
    private String procReference;
    private Integer jobId;
    private Integer jobPoNumber;
    private Integer jobDefaultPoNumber;
    private List<KeyValue<Integer, String>> serviceTypes;

}
