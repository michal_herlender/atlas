package org.trescal.cwms.core.jobs.jobitem.entity.faultreport.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;

@Component
public class AutomaticFaultReportGenerationEventListener
		implements ApplicationListener<AutomaticFaultReportGenerationEvent> {

	@Autowired
	private FaultReportService faultReportService;

	@Override
	public void onApplicationEvent(AutomaticFaultReportGenerationEvent event) {
		faultReportService.automaticFaultReportGeneration(event.getFaultReport(), event.isWithClientReponse());
	}

}
