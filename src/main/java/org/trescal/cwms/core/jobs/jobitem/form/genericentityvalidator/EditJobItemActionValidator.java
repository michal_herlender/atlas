package org.trescal.cwms.core.jobs.jobitem.form.genericentityvalidator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

/**
 * @author jamiev
 */
@Component("EditJobItemActionValidator")
public class EditJobItemActionValidator extends AbstractBeanValidator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(JobItemActivity.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		JobItemActivity jia = (JobItemActivity) target;

		super.validate(jia, errors);
	}
}