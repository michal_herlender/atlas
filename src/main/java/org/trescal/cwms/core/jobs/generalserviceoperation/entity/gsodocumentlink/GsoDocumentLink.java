package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Entity
@Table(name = "gsodocumentlink")
public class GsoDocumentLink extends Auditable {

	private int id;
	private GsoDocument gsoDocument;
	private JobItem ji;
	
	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "gsodocumentid", nullable = false)
	public GsoDocument getGsoDocument() {
		return this.gsoDocument;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "jobitemid", nullable = false)
	public JobItem getJi() {
		return this.ji;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public void setGsoDocument(GsoDocument gsoDocument) {
		this.gsoDocument = gsoDocument;
	}

	public void setJi(JobItem ji) {
		this.ji = ji;
	}

}