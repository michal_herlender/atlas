package org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.db;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.CallOffItem;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.dto.CallOffItemDTO;

import java.util.List;
import java.util.Locale;
import java.util.Set;

public interface CallOffItemDao extends AllocatedDao<Subdiv, CallOffItem, Integer> {
    Set<CallOffItemDTO> getAllActiveCallOffItems(Locale locale);

    List<CallOffItem> getAllActive();

    List<CallOffItemDTO> getAllForCompanyDto(int coid, Boolean active);

    List<CallOffItem> getAllForCompany(int coid, Boolean active);

    List<CallOffItem> getAll(List<Integer> ids);

    CallOffItem getActiveForJobItem(int jobItemId);
}