package org.trescal.cwms.core.jobs.job.entity.jobnote.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.jobnote.JobNote;
import org.trescal.cwms.core.jobs.job.entity.jobnote.JobNote_;

@Repository("JobNoteDao")
public class JobNoteDaoImpl extends BaseDaoImpl<JobNote, Integer> implements JobNoteDao {

	@Override
	protected Class<JobNote> getEntity() {
		return JobNote.class;
	}

	@Override
	public List<JobNote> getActiveJobNotesForJob(int jobid) {
		return getResultList(cb -> {
			CriteriaQuery<JobNote> cq = cb.createQuery(JobNote.class);
			Root<JobNote> root = cq.from(JobNote.class);
			Join<JobNote, Job> job = root.join(JobNote_.job);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(job.get(Job_.jobid), jobid));
			clauses.getExpressions().add(cb.isTrue(root.get(JobNote_.active)));
			cq.where(clauses);
			return cq;
		});
	}
}