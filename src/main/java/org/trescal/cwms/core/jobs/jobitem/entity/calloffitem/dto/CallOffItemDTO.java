package org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.dto;

import java.util.Date;

import lombok.Data;

@Data
public class CallOffItemDTO {

	private Integer calloffId;
	private Integer subdivId;
	private Integer coid;
	private Integer jobId;
	private String jobno;
	private Integer jobItemId;
	private String companyCode;
	private String coname;
	private String subname;
	private Boolean subActive;
	private String subdivCode;
	private String town;
	private String calledOffByFirstName;
	private String calledOffByLastName;
	private Date offDate;
	private String reason;
	private Integer plantId;
	private String instModel;
	private Integer itemNo;
	private Boolean contractReviewItemExist;
	private Long itemsCount;

	public CallOffItemDTO(Integer calloffId, Integer subdivId, Integer coid, Integer jobId, String jobno,
			Integer jobItemId, String companyCode, String coname, String subname, Boolean subActive, String subdivCode,
			String town, String calledOffByFirstName, String calledOffByLastName, Date offDate, String reason,
			Integer plantId, String instModel, Integer itemNo, Long contractReviewItem, Long itemsCount) {
		super();
		this.calloffId = calloffId;
		this.subdivId = subdivId;
		this.coid = coid;
		this.jobId = jobId;
		this.jobno = jobno;
		this.jobItemId = jobItemId;
		this.companyCode = companyCode;
		this.coname = coname;
		this.subname = subname;
		this.subActive = subActive;
		this.subdivCode = subdivCode;
		this.town = town;
		this.calledOffByFirstName = calledOffByFirstName;
		this.calledOffByLastName = calledOffByLastName;
		this.offDate = offDate;
		this.reason = reason;
		this.plantId = plantId;
		this.instModel = instModel;
		this.itemNo = itemNo;
		if (contractReviewItem > 0) {
			this.contractReviewItemExist = true;
		}
		this.itemsCount = itemsCount;
	}

}
