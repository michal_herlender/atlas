package org.trescal.cwms.core.jobs.contractreview.entity.contractreview.contoller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.db.ContractReviewService;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.db.ContractReviewItemService;
import org.trescal.cwms.core.jobs.contractreview.projection.ContractReviewProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.tools.StringTools;

@Controller
@IntranetController
public class ContractReviewJsonController {

	@Autowired
	private ContractReviewService crService;
	@Autowired
	private ContractReviewItemService critemService;

	@RequestMapping(value = "/getContractReviewByJobId.json", method = RequestMethod.GET)
	@ResponseBody
	protected List<ContractReviewProjectionDTO> getContractReviewByJob(
			@RequestParam(name = "jobId", required = true) Integer jobId) throws Exception {
		List<ContractReviewProjectionDTO> dtos = this.crService.getCrProjectionDTOsyjob(jobId);
		for (ContractReviewProjectionDTO dto : dtos) {
			List<Integer> itemsNos =this.critemService.getJobItemsCouverdInContractReview(dto.getId(), jobId);
			dto.setItemsNoCoverd(StringTools.combineSequenceListIntegerNumbers(itemsNos));
			dto.setItemsNoCoverdSize(itemsNos.size());
		}
		return dtos;
	}

	@RequestMapping(value = "/getContractReviewItemsByCrId.json", method = RequestMethod.GET)
	@ResponseBody
	protected List<JobItemProjectionDTO> getContractReviewItemsByCrId(
			@RequestParam(name = "crId", required = true) Integer crId, Locale locale) throws Exception {
		return this.critemService.getContractReviewItemsDTOs(crId, locale);
	}

}
