package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.form.JobItemForm;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME })
public class JIRepairsController extends JobItemController {

	private static final Logger logger = LoggerFactory.getLogger(JIRepairsController.class);

	@Autowired
	private RepairInspectionReportService rirService;

	@ModelAttribute("form")
	protected JobItemForm formBackingObject(@RequestParam(value = "jobitemid", required = true) Integer jobitemid,
			Model model) throws Exception {
		JobItemForm form = new JobItemForm();
		JobItem ji = this.jobItemService.findJobItem(jobitemid);
		if ((ji == null) || (jobitemid == 0)) {
			logger.error("Jobitem with id " + jobitemid + " could not be found");
			throw new Exception("Unable to locate requested jobitem");
		}
		form.setJi(ji);
		return form;
	}

	@PreAuthorize("hasAuthority('JI_REPAIR')")
	@RequestMapping(value = "/jirepairs.htm", method = { RequestMethod.GET })
	protected ModelAndView doRequest(@RequestParam(value = "jobitemid", required = true) Integer jobitemid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("form") JobItemForm form) throws Exception {

		Contact contact = this.userService.get(username).getCon();
		Map<String, Object> refData = super.referenceData(jobitemid, contact, subdivDto.getKey(),username);

		boolean allowRIRCreationForCurrentSubdiv = rirService.getRirByJobItemIdAndSubdivId(jobitemid,
				subdivDto.getKey()) == null;
		refData.put("allowRIRCreationForCurrentSubdiv", allowRIRCreationForCurrentSubdiv);
		// for displaying the repair time management link
		boolean allowRepairTimeUpdate = this.jobItemService.stateHasGroupOfKeyName(form.getJi().getState(), StateGroup.AWAITINGRCRVALIDATION) || 
										this.jobItemService.stateHasGroupOfKeyName(form.getJi().getState(), StateGroup.AWAITINGRCRUPDATE);
		refData.put("allowRepairTimeUpdate", allowRepairTimeUpdate);
		return new ModelAndView("trescal/core/jobs/jobitem/jirepairs", refData);
	}
	
	@PreAuthorize("@hasJobItemAuthorityOnConnectedSubdiv.check('JI_REPAIR')")
	@RequestMapping(value = "/jirepairs.htm", method = { RequestMethod.POST })
	protected ModelAndView doPost(@RequestParam(value = "jobitemid", required = true) Integer jobitemid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("form") JobItemForm form) throws Exception {

		return doRequest(jobitemid, subdivDto, username, form);
	}
}