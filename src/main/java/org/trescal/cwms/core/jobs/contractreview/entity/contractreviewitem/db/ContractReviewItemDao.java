package org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;

public interface ContractReviewItemDao extends BaseDao<ContractReviewItem, Integer> {
	
	List<Integer> getContractReviewedJobItemIds(int jobid);
	List<JobItemProjectionDTO> getContractReviewItemsDTOs(Integer ContractReviewId,Locale locale);
	List<Integer> getJobItemsCouverdInContractReview(Integer ContractReviewId,Integer jobid);
 }