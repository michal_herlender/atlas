package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;

@Entity
@Table(name = "jobitemnote")
public class JobItemNote extends Note
{
	private JobItem ji;
	
	public final static boolean privateOnly = true;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid")
	public JobItem getJi() {
		return ji;
	}
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.JOBITEMNOTE;
	}
	
	public void setJi(JobItem ji) {
		this.ji = ji;
	}
}