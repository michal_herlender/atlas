package org.trescal.cwms.core.jobs.jobitem.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class JIActivitiesValidator extends AbstractBeanValidator
{
	@Autowired
	private JobItemActionService jobItemActionService; 
	
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(JIActivitiesForm.class);
	}
	
	@Override
	public void validate(Object obj, Errors errors)
	{
		super.validate(obj, errors);
		
		JIActivitiesForm form = (JIActivitiesForm) obj;
		
		if (form.getSubmitAction().equalsIgnoreCase("override"))
		{
			if ((form.getOverrideRemark() == null)
					|| form.getOverrideRemark().trim().isEmpty())
			{
				errors.rejectValue("overrideRemark", "error.form.overrideRemark", null, "A remark must be entered when overriding a status.");
			}
		}
		if (form.getSubmitAction().equalsIgnoreCase("onhold"))
		{
			if ((form.getHoldRemark() == null)
					|| form.getHoldRemark().trim().isEmpty())
			{
				errors.rejectValue("holdRemark", "error.form.holdRemark", null, "A remark must be entered when placing an item on hold.");
			}
		}
		if (form.getSubmitAction().equalsIgnoreCase("delete"))
		{
			JobItemAction jobItemAction = jobItemActionService.get(form.getLastActionId());
			if (jobItemAction.getStartStatus() == null) {
				errors.reject("error.deletefirstaction", null, "Please, don't delete the first action!");
			}
		}
	}
}