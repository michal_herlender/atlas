package org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement_;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository("JobItemWorkRequirementDao")
public class JobItemWorkRequirementDaoImpl extends BaseDaoImpl<JobItemWorkRequirement, Integer>
    implements JobItemWorkRequirementDao {

    @Override
    protected Class<JobItemWorkRequirement> getEntity() {
        return JobItemWorkRequirement.class;
    }

    @Override
    public List<JobItemWorkRequirement> getForJobItemView(JobItem jobItem) {
        return getResultList(cb -> {
            CriteriaQuery<JobItemWorkRequirement> cq = cb.createQuery(JobItemWorkRequirement.class);
            Root<JobItemWorkRequirement> jobItemWorkRequirement = cq.from(JobItemWorkRequirement.class);
            Fetch<JobItemWorkRequirement, WorkRequirement> workRequirement = jobItemWorkRequirement
                .fetch(JobItemWorkRequirement_.workRequirement);
            workRequirement.fetch(WorkRequirement_.workInstruction, JoinType.LEFT);
            workRequirement.fetch(WorkRequirement_.capability, JoinType.LEFT);
            workRequirement.fetch(WorkRequirement_.serviceType, JoinType.LEFT);
            cq.where(cb.equal(jobItemWorkRequirement.get(JobItemWorkRequirement_.jobitem), jobItem));
            return cq;
        });
	}
}