package org.trescal.cwms.core.jobs.calibration.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.db.BatchCalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.db.QueuedCalibrationService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class ContinueBatchCalibrationController {
	@Autowired
	private BatchCalibrationService batchCalServ;
	@Autowired
	private CalibrationService calServ;
	@Autowired
	private QueuedCalibrationService qcServ;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/continuebatchcalibration.htm")
	protected ModelAndView handleRequestInternal(
			@RequestParam(value = "endcalid", required = false, defaultValue = "0") Integer completedCalId,
			@RequestParam(value = "batchid", required = false, defaultValue = "0") Integer batchId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		// get calibration
		Calibration cal = this.calServ.findCalibration(completedCalId);
		// if batch ID wasn't passed, get it from the passed calibration
		if (batchId == 0)
			batchId = cal.getBatch().getId();
		// load batch
		BatchCalibration batch = this.batchCalServ.findBatchCalibration(batchId);
		// get logged-in user
		Contact user = this.userService.get(username).getCon();
		// if a calibration has just been completed
		if (cal != null) {
			// set CWMS procedure as complete
			this.calServ.setProcedureComplete(cal.getXindiceKey());
			// finish completed cal
			Integer time = DateTools.getMinutesDifference(cal.getStartTime(), new Date());
			Map<Integer, Integer> timeSpents = new HashMap<Integer, Integer>();
			for (CalLink cl : cal.getLinks())
				timeSpents.put(cl.getId(), time);
			this.calServ.completeCalibration(cal, user, timeSpents, null, null, true, null, null, null);
			// queue up cert signing
			this.qcServ.queueCalibration(cal);
		}
		if (cal != null)
			// if next cal is to be manually selected
			if (cal.getBatch().isChooseNext())
				// redirect to the view batch screen
				return new ModelAndView(new RedirectView("viewbatchcalibration.htm?batchid=" + batchId));
		// get next un-finished cal in batch
		Calibration nextCal = this.batchCalServ.getNextIncompleteCalibrationInBatch(batchId, user.getPersonid());
		// go to next calibration
		if (nextCal != null) {
			// if not yet started
			if (nextCal.getStatus().getName().trim().equals("Awaiting work"))
				// set calibration as started
				this.calServ.startCalibration(nextCal, user, null, null);
			// run procedure
			return new ModelAndView(new RedirectView("runproc.htm?calid=" + nextCal.getId() + "&batchid="
					+ batch.getId() + "&runfile=" + nextCal.getXindiceKey()));
		}
		// OR if all calibrations are complete (or contact is not accredited for
		// any more)
		return new ModelAndView(
				new RedirectView("viewjob.htm?loadtab=batchcal-tab&jobid=" + batch.getJob().getJobid()));
	}
}