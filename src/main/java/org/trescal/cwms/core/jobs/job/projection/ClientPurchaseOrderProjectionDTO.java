package org.trescal.cwms.core.jobs.job.projection;

import lombok.Getter;
import lombok.Setter;

/**
 * Aggregates PO and BPO information for a job item or expense item
 * allowing PO information for many job items (or expense items) to be obtained in one query
 * 
 * key itemPoId (which could be either jobItemPoId or expenseItemPoId depending on usage) is included to aid in potential use in user data entry
 * 
 * The field itemId could be either jobItemId or expenseItemId depending on the usage
 *   
 * @author galen
 *
 */
@Getter @Setter
public class ClientPurchaseOrderProjectionDTO {
	private Integer itemPoId;
	private Integer itemId;
	private Integer bpoId;
	private String bpoNumber;
	private Integer poId;
	private String poNumber;
	
	/**
	 * Explicit constructor for query builder usage 
	 */
	public ClientPurchaseOrderProjectionDTO(Integer itemPoId, Integer itemId, Integer bpoId, String bpoNumber, Integer poId,
			String poNumber) {
		super();
		this.itemPoId = itemPoId;
		this.itemId = itemId;
		this.bpoId = bpoId;
		this.bpoNumber = bpoNumber;
		this.poId = poId;
		this.poNumber = poNumber;
	}
	
}
