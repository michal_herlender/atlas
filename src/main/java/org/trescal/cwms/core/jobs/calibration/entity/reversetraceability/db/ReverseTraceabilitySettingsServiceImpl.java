package org.trescal.cwms.core.jobs.calibration.entity.reversetraceability.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.jobs.calibration.dto.ReverseTraceabilitySettingsDTO;
import org.trescal.cwms.core.jobs.calibration.entity.reversetraceability.ReverseTraceabilitySettings;
import org.trescal.cwms.core.jobs.calibration.form.ReverseTraceabilitySettingsForm;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Service("ReverseTraceabilitySettingsService")
public class ReverseTraceabilitySettingsServiceImpl extends BaseServiceImpl<ReverseTraceabilitySettings, Integer>
	implements ReverseTraceabilitySettingsService {

	@Autowired
	private ReverseTraceabilitySettingsDao reverseTraceabilitySettingsDao;
	@Autowired
	private SubdivService subdivService;

	@Override
	protected BaseDao<ReverseTraceabilitySettings, Integer> getBaseDao() {
		return reverseTraceabilitySettingsDao;
	}

	@Override
	public List<ReverseTraceabilitySettingsDTO> getReverseTraceabilitySettingsFormSubdivIds(List<Integer> subdivIds) {
		return reverseTraceabilitySettingsDao.getReverseTraceabilitySettingsFormSubdivIds(subdivIds);
	}
	
	@Override
	public void updateReverseTraceabilitySettings(ReverseTraceabilitySettings reverseTraceabilitySettings){
		this.reverseTraceabilitySettingsDao.merge(reverseTraceabilitySettings);
	}
	
	@Override
	public void enableReverseTraceabilitySettings(ReverseTraceabilitySettingsDTO dto){
		ReverseTraceabilitySettings reverseTraceabilitySettings = new ReverseTraceabilitySettings();
		reverseTraceabilitySettings.setBusinessSubdiv(subdivService.get(dto.getSubdivid()));
		reverseTraceabilitySettings.setReverseTraceability(dto.getReverseTraceability());
		reverseTraceabilitySettings.setStartDate(dto.getStartDate());
		this.updateReverseTraceabilitySettings(reverseTraceabilitySettings);
	}
	
	@Override
	public void editReverseTraceabilitySettings(ReverseTraceabilitySettingsForm form){
		for(ReverseTraceabilitySettingsDTO dto : form.getReverseTraceabilitySettingsDtos()){
			ReverseTraceabilitySettings reverseTraceabilitySettings = this.get(dto.getId());
			if(reverseTraceabilitySettings != null) {
				// edit the reverse traceability field (yes = enable/ no= disable)
				if(dto.getReverseTraceability() != null){
					reverseTraceabilitySettings.setReverseTraceability(dto.getReverseTraceability());
				}
				// edit the start date 
				if(dto.getStartDate() != null){
					reverseTraceabilitySettings.setStartDate(dto.getStartDate());
				}
				// update the ReverseTraceabilitySettings entity
				this.updateReverseTraceabilitySettings(reverseTraceabilitySettings);
			}		
		}
	}

	@Override
	public Map<Integer, LocalDate> getSubdivsAndDateFromReverseTraceability() {
		return this.reverseTraceabilitySettingsDao.getSubdivsAndDateFromReverseTraceability();
	}
	
}
