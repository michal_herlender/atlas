package org.trescal.cwms.core.jobs.job.entity.poorigin;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;

import javax.persistence.*;

@Entity
@Table(name = "po_origin")
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Setter
@NoArgsConstructor
public abstract class POOrigin<Kind extends OriginKind> extends Versioned {

    private Long id;

    private Kind kind;

    private Job job;

    private TPQuotation tpQuotation;

    private PurchaseOrder purchaseOrder;

    @Transient
    public abstract KindName getKindName();

    public POOrigin(Kind kind) {
        this.kind = kind;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    @Transient
    public Kind getKind() {
        return kind;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "jobid")
    public Job getJob() {
        return job;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "poId")
    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quoteId")
    public TPQuotation getTpQuotation() {
        return tpQuotation;
    }
}
