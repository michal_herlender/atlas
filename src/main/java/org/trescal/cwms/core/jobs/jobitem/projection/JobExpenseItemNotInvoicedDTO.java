package org.trescal.cwms.core.jobs.jobitem.projection;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.NotInvoicedReason;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class JobExpenseItemNotInvoicedDTO extends NotInvoicedProjectionDTO {
	private Integer notInvoicedId;
	private JobExpenseItemProjectionDTO expenseItem;
	
	/**
	 * Creates reference to JobExpenseItemProjectionDTO (maybe initialized after creation)
	 * 
	 * Fields prefaced with "expense" are transferred to the expense item 
	 */
	
	public JobExpenseItemNotInvoicedDTO(Integer notInvoicedId, LocalDate date, Integer contactId, NotInvoicedReason reason, String comments, 
			Integer invoiceId, Integer expenseItemId, Integer expenseItemNo, Integer expenseServiceTypeId, Integer expenseModelId, 
			String expenseModelTranslation, BigDecimal expenseSinglePrice, Integer expenseQuantity, String expenseClientRef, String expenseComment, LocalDate expenseDate, Boolean expenseInvoiceable, 
			Integer jobId) {
		super(date, contactId, reason, comments, invoiceId);
		this.notInvoicedId = notInvoicedId;
		
		this.expenseItem = new JobExpenseItemProjectionDTO(expenseItemId, expenseItemNo, expenseServiceTypeId, 
				expenseModelId, expenseModelTranslation, expenseSinglePrice, expenseQuantity, expenseClientRef, expenseComment, expenseDate, 
				expenseInvoiceable, jobId);
	}
}
