package org.trescal.cwms.core.jobs.job.dto;

import com.fasterxml.jackson.databind.util.StdConverter;
import lombok.Value;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;

@Value(staticConstructor = "of")
public class JobTypeDto {
    String name;
    String description;

    public static class JobTypeJsonConverter extends StdConverter<JobType, JobTypeDto> {
        @Override
        public JobTypeDto convert(JobType value) {
            return of(value.getName(), value.getDescription());
        }
    }
}
