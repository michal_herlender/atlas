package org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction.db;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.deliverynote.form.TPInstructionDTO;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction.TPInstruction;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.spring.model.KeyValue;

@Service("TPInstructionService")
public class TPInstructionServiceImpl extends BaseServiceImpl<TPInstruction, Integer> implements TPInstructionService
{
	@Autowired
	private AuthenticationService authServ;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private UserService userService;
	@Autowired
	private TPInstructionDao tpInstructionDao;

	@Override
	public ResultWrapper deactivateTPInstruction(int instructionId, HttpSession session)
	{
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		@SuppressWarnings("unchecked")
		KeyValue<Integer,String> subdivDto = (KeyValue<Integer,String>) session.getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV);
		Contact contact = this.userService.getEagerLoad(username).getCon();
		// first find instruction
		TPInstruction tpin = this.get(instructionId);
		// instruction null?
		if (tpin != null)
		{
			// allow anybody with authority over the original author to edit
			// his/her
			// comments
			if (this.authServ.allowPrivileges(contact.getPersonid(), tpin.getRecordedBy().getPersonid(),subdivDto.getKey()))
			{
				// deactivate third party instruction
				tpin.setActive(false);
				tpin.setShowOnTPDelNote(false);
				// update instruction
				this.update(tpin);
				// return successful wrapper
				return new ResultWrapper(true, null, tpin, null);
			}
			else
			{
				return new ResultWrapper(false, "You do not have permission to de-activate "
						+ tpin.getRecordedBy().getName()
						+ "'s third party instruction");
			}
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "Third party instruction could not be found", null, null);
		}
	}


	@Override
	public ResultWrapper insertTPInstruction(int jobItemId, String instruction, HttpSession session)
	{
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact contact = this.userService.getEagerLoad(username).getCon();
		// first find job item
		JobItem ji = this.jiServ.findJobItem(jobItemId);
		// job item null?
		if (ji != null)
		{
			// create new tp instruction
			TPInstruction tpin = new TPInstruction();
			// set values
			tpin.setActive(true);
			tpin.setJi(ji);
			tpin.setLastModified(new Date());
			tpin.setRecordedBy(contact);
			tpin.setShowOnTPDelNote(true);
			tpin.setInstruction(instruction);
			// insert tp instruction
			this.save(tpin);
			// return successful wrapper
			return new ResultWrapper(true, "", tpin, null);
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "Job item could not be found", null, null);
		}
	}

	@Override
	public ResultWrapper updateTPInstruction(int instructionId, String instruction)
	{
		// first find instruction
		TPInstruction tpin = this.get(instructionId);
		// instruction null?
		if (tpin != null)
		{
			// update third party instruction
			tpin.setInstruction(instruction);
			// update instruction
			this.save(tpin);
			// return successful wrapper
			return new ResultWrapper(true, "", tpin, null);
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "Third party instruction could not be found", null, null);
		}
	}

	@Override
	public ResultWrapper updateTPInstructionShowOnTPDN(int instructionId, boolean showOnTPDN)
	{
		// first find instruction
		TPInstruction tpin = this.get(instructionId);
		// instruction null?
		if (tpin != null)
		{
			// update show on third party delivery note
			tpin.setShowOnTPDelNote(showOnTPDN);
			// update instruction
			this.update(tpin);
			// return successful wrapper
			return new ResultWrapper(true, "", tpin, null);
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "Third party instruction could not be found", null, null);
		}
	}

	@Override
	protected BaseDao<TPInstruction, Integer> getBaseDao() {
		return tpInstructionDao;
	}
	
	@Override
	public List<TPInstructionDTO> getAllByJob(Integer jobId) {
		return this.tpInstructionDao.getAllByJob(jobId);
	}
}