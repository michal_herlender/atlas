package org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db;

import java.util.List;

import org.trescal.cwms.batch.jobs.operationsimport.ImportedOperationItem;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.failurereport.dto.FailureReportDTO;
import org.trescal.cwms.core.failurereport.enums.FailureReportFinalOutcome;
import org.trescal.cwms.core.failurereport.enums.FailureReportRecommendationsEnum;
import org.trescal.cwms.core.failurereport.form.FailureReportForm;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.rest.adveso.dto.AdvesoFailureReportClientResponseInputDTO;
import org.trescal.cwms.rest.tlm.dto.TLMFailureReportInputDTO;

/**
 * Interface for accessing and manipulating {@link FaultReportService} entities.
 * 
 * @author jamiev
 */
public interface FaultReportService {
	/**
	 * Deletes the given {@link FaultReport} from the database.
	 * 
	 * @param faultreport the {@link FaultReport} to delete.
	 */
	void deleteFaultReport(FaultReport faultreport);

	/**
	 * Returns the {@link FaultReport} with the given ID.
	 * 
	 * @param id the {@link FaultReport} ID.
	 * @return the {@link FaultReport}
	 */
	FaultReport findFaultReport(int id);

	/**
	 * Returns all {@link FaultReport} entities stored in the database.
	 * 
	 * @return the {@link List} of {@link FaultReport} entities.
	 */
	List<FaultReport> getAllFaultReports();

	/**
	 * Updates the given {@link FaultReport} in the database.
	 * 
	 * @param faultreport the {@link FaultReport} to update.
	 */
	void updateFaultReport(FaultReport faultreport);

	List<FaultReport> getFaultReportListByJobitemId(Integer jobItemId);

	FaultReport getLastFailureReportByJobitemId(Integer jobitemId, Boolean valid);

	List<FailureReportDTO> getAllFailureReportsByJobitemId(Integer jobitemId);

	void setFailureReportClientResponseFromAdveso(FaultReport fr, AdvesoFailureReportClientResponseInputDTO inputDTO);

	FaultReport getFirstFailureReportByJobitemIdForDelivery(Integer jobitemId);

	void deliverInformationalFR(JobItem ji, JobItemActivity a);

	void setInformationalFRToBeSent(FaultReport fr);

	void createFailureReportActivites(FaultReport fr, Integer timeSpent, String remark, String validationRemark);

	FailureReportFinalOutcome deduceFrOutcomeFromRecommendation(boolean byTrescal,
			List<FailureReportRecommendationsEnum> recs, ServiceType expectedServiceType, Boolean clientResponseNeeded,
			Boolean clientDecision, boolean importMode);

	void automaticFaultReportGeneration(FaultReport fr, boolean withClientReponse);

	FaultReport createFailureReportForOnsiteOperationsImportation(ImportedOperationItem dto,
			JobItemWorkRequirement jiwr, Subdiv allocatedSubdiv, boolean isFrWithoutPriorCal);

	void createOrUpdateFailureReport(FailureReportForm form, Contact contact, Subdiv allocatedSubdiv,
			boolean isFrWithoutPriorCal);

	FaultReport createFailureReportForTLMInterface(TLMFailureReportInputDTO dto);
}