package org.trescal.cwms.core.jobs.certificate.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class CertManagementValidator extends AbstractBeanValidator
{
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(CertManagementForm.class);
	}
	
	public void validate(Object obj, Errors errors)
	{
		CertManagementForm form = (CertManagementForm) obj;
		super.validate(form, errors);

		/**
		 * BUSINESS VALIDATION
		 */
		if (form.getCalDate() == null)
		{
			errors.rejectValue("calDate", null, "Please select a calibration date for the certificates");
		}
	}
}
