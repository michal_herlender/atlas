package org.trescal.cwms.core.jobs.jobitem.entity.accessory.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.GroupAccessory;

public interface GroupAccessoryDao extends BaseDao<GroupAccessory, Integer> {

	/**
	 * Count all accessories linked to some job item group of this job.
	 * 
	 * @param jobId identifier of the job
	 * @return quantity of group accessories linked to this job
	 */
	Integer countByJob(Integer jobId);

	/**
	 * Count all groups of the job, where accessory free text is not empty
	 * 
	 * @param jobId identifier of the job
	 * @return quantity of groups with non empty accessory free text
	 */
	Integer countFreeTextAccessoriesByJob(Integer jobId);
}