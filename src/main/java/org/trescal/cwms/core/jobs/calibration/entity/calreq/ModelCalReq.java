package org.trescal.cwms.core.jobs.calibration.entity.calreq;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.system.Constants;

@Entity
@DiscriminatorValue("model")
public class ModelCalReq extends CalReq
{
	private InstrumentModel model;

	@Transient
	@Override
	public String getClassKey()
	{
		return Constants.CALREQ_MAP_MODEL;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelid", nullable = true)
	public InstrumentModel getModel()
	{
		return this.model;
	}

	/*
	 * GB 2016-02-06 : Previously, this would only return message key if model was not null;
	 * changed to return in all cases (to help identify inconsistent data in system) 
	 */
	@Transient
	@Override
	public String getSource()
	{
		return "calreq.modelcalreqsource";
	}
	
	@Transient
	@Override
	public String getSourceParameter()
	{
		if (this.model != null)
		{
			return this.model.getDefinitiveModel();
		}
		return "";
	}
	
	public void setModel(InstrumentModel model)
	{
		this.model = model;
	}
}
