package org.trescal.cwms.core.jobs.calibration.entity.pointsettemplate.db;

import java.util.List;

import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.jobs.calibration.entity.pointsettemplate.PointSetTemplate;
import org.trescal.cwms.core.jobs.calibration.entity.templatepoint.TemplatePoint;

public interface PointSetTemplateService
{
	void deletePointSetTemplate(PointSetTemplate pointsettemplate);

	PointSetTemplate findPointSetTemplate(int id);

	/**
	 * Return's the {@link PointSetTemplate} identified by the given id and with
	 * {@link TemplatePoint} and {@link UoM} eagerly loaded.
	 * 
	 * @param id the id of the {@link PointSetTemplate}.
	 * @return {@link PointSetTemplate} or null if none found.
	 */
	PointSetTemplate findPointSetTemplateWithPoints(int id);

	List<PointSetTemplate> getAllPointSetTemplates();

	void insertPointSetTemplate(PointSetTemplate pointsettemplate);

	void saveOrUpdatePointSetTemplate(PointSetTemplate pointsettemplate);

	void updatePointSetTemplate(PointSetTemplate pointsettemplate);
}