package org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportInstruction;

@Repository
public class FaultReportInstructionDaoImpl extends BaseDaoImpl<FaultReportInstruction, Integer> implements FaultReportInstructionDao {

	@Override
	protected Class<FaultReportInstruction> getEntity() {
		return FaultReportInstruction.class;
	}

}
