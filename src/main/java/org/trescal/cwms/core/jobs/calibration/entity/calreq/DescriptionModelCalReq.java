package org.trescal.cwms.core.jobs.calibration.entity.calreq;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.system.Constants;

import javax.persistence.*;

@Entity
@DiscriminatorValue(Constants.CALREQ_MAP_COMPANYDESCRIPTION)
public class DescriptionModelCalReq extends CalReq {

    private Company company;
    private Description description;

    @Transient
    @Override
    public String getClassKey() { return Constants.CALREQ_MAP_COMPANYDESCRIPTION; }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "compid", nullable = true)
    public Company getCompany() { return company; }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "descid", nullable = true)
    public Description getDescription() { return description; }

    @Override
    @Transient
    public int getGenericId()
    {
        return (this.description == null) ? 0 : this.description.getId();
    }

    /*
     * SC 2018-02-14 Message can be the same as  for company-model calrequ
     */
    @Transient
    @Override
    public String getSource() { return "calreq.companymodelcalreqsource"; }

    @Transient
    @Override
    public String getSourceParameter() {
        if (this.company != null && this.description != null) {
            return this.description.getDescription() + ", " + this.company.getConame();
        }
        return "";
    }

    public void setCompany(Company company) { this.company = company; }

    public void setDescription(Description description) { this.description = description; }
}
