package org.trescal.cwms.core.jobs.job.projection.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.stream.Collectors;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.company.projection.SubdivProjectionDTO;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.db.JobStatusService;
import org.trescal.cwms.core.jobs.job.entity.po.db.POService;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.jobs.job.projection.POProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import lombok.extern.slf4j.Slf4j;

@Service @Slf4j
public class JobProjectionServiceImpl implements JobProjectionService {
	
	@Autowired
	private ContactService contactService;
	@Autowired
	private JobService jobService;
	@Autowired
	private JobStatusService jobStatusService;
	@Autowired
	private POService poService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SupportedCurrencyService currencyService;
	@Autowired
	private TransportOptionService transportOptionService;

	private MultiValuedMap<Integer, JobItemProjectionDTO> getJobItemMapByJobId(
			Collection<JobItemProjectionDTO> jobItemDtos) {
		MultiValuedMap<Integer, JobItemProjectionDTO> mapJobItemsByJobId = new HashSetValuedHashMap<>(); 
		jobItemDtos.forEach(jiDto -> mapJobItemsByJobId.put(jiDto.getJobid(), jiDto));		
		return mapJobItemsByJobId;
	}
	
	@Override
	public void loadJobItemsIntoJobs(Collection<JobItemProjectionDTO> jobItemDtos,
			Collection<JobProjectionDTO> jobDtos) {
		MultiValuedMap<Integer, JobItemProjectionDTO> mapJobItemsByJobId = getJobItemMapByJobId(jobItemDtos);
		loadJobItemsIntoJobs(mapJobItemsByJobId, jobDtos);
	}
	
	private void loadJobItemsIntoJobs(MultiValuedMap<Integer, JobItemProjectionDTO> mapJobItemsByJobId,
			Collection<JobProjectionDTO> jobDtos) {
		for (JobProjectionDTO jobDto : jobDtos) {
			Collection<JobItemProjectionDTO> jobItems = mapJobItemsByJobId.get(jobDto.getJobid());
			// Set job on each jobItemDto for general use (e.g. reporting / exports)
			jobItems.forEach(jiDto -> jiDto.setJob(jobDto));
			List<JobItemProjectionDTO> orderedJobItems = jobItems.stream()
					.sorted(Comparator.comparing(JobItemProjectionDTO::getItemno))
					.collect(Collectors.toList());
			jobDto.setJobitems(orderedJobItems);
		}
	}


	@Override
	public JobProjectionResult loadJobProjections(List<JobProjectionDTO> jobDtos,
			JobProjectionCommand command) {
		log.debug("jobDtos.size() = "+jobDtos.size());
		JobProjectionResult result = new JobProjectionResult();
		result.setJobDtos(jobDtos);
		
		if (command.getLoadContact()) {
			result.setContactDtos(this.loadJobProjectionContacts(jobDtos, command));
		}
		if (command.getLoadCurrency()) {
			this.loadJobProjectionCurrency(jobDtos);
		}
		if (command.getLoadOrganisation()) {
			result.setBusinessSubdivDtos(this.loadJobProjectionOrganisation(jobDtos, command));
		}
		if (command.getLoadStatus()) {
			this.loadJobProjectionStatus(jobDtos, command);
		}
		if (command.getLoadTransportOut()) {
			result.setTransportOutDtos(this.loadJobProjectionTransportOut(jobDtos, command));
		}
		if (command.getLoadClientPOs()) {
			this.loadJobProjectionClientPOs(jobDtos);
		}
		return result;
	}
	
	/**
	 * TODO handle case of >2000 jobs, likely in JobService::getJobProjectionDTOList
	 */
	@Override
	public JobProjectionResult getDTOsByJobItems(Collection<JobItemProjectionDTO> jobItemDtos, JobProjectionCommand command) {
		MultiValuedMap<Integer, JobItemProjectionDTO> mapJobItemsByJobId = getJobItemMapByJobId(jobItemDtos);
		Set<Integer> jobIds = mapJobItemsByJobId.keySet();
		log.debug("jobIds.size() = "+jobIds.size());
		// TODO support having more than 2000 jobs at once in service method
		JobProjectionResult result = this.getDTOsByJobIds(jobIds, command);
		loadJobItemsIntoJobs(mapJobItemsByJobId, result.getJobDtos());
		
		return result;
	}
	
	/**
	 * TODO handle case of >2000 jobs, likely in JobService::getJobProjectionDTOList
	 */
	@Override
	public JobProjectionResult getDTOsByJobExpenseItems(Collection<JobItemProjectionDTO> jobExpenseItemDtos, JobProjectionCommand command) {
		MultiValuedMap<Integer, JobItemProjectionDTO> mapJobExpenseItemsByJobId = getJobItemMapByJobId(jobExpenseItemDtos);
		Set<Integer> jobIds = mapJobExpenseItemsByJobId.keySet();
		log.debug("jobIds.size() = "+jobIds.size());
		// TODO support having more than 2000 jobs at once in service method
		JobProjectionResult result = this.getDTOsByJobIds(jobIds, command);
		loadJobItemsIntoJobs(mapJobExpenseItemsByJobId, result.getJobDtos());
		
		return result;
	}
	
	@Override
	public JobProjectionResult getDTOsByJobIds(Collection<Integer> jobIds, JobProjectionCommand command) {
		// TODO support having more than 2000 jobs at once in service method
		List<JobProjectionDTO> jobDtos = this.jobService.getJobProjectionDTOList(jobIds, command.getCountJobItems(), command.getCountJobExpenseItems());
		return loadJobProjections(jobDtos, command);
	}
	
	/**
	 * Convenience method for simple usage, returns null if no job found for id
	 */
	@Override
	public JobProjectionDTO getDTOByJobId(Integer jobId, JobProjectionCommand command) {
		if (jobId == null) throw new IllegalArgumentException("jobId must be specified but was null");
		JobProjectionResult result = getDTOsByJobIds(Collections.singleton(jobId), command);
		return result.getJobDtos().isEmpty() ? null : result.getJobDtos().iterator().next();
	}
	
	private List<ContactProjectionDTO> loadJobProjectionContacts(Collection<JobProjectionDTO> jobDtos, JobProjectionCommand command) {
		MultiValuedMap<Integer, JobProjectionDTO> mapJobByContactId = new HashSetValuedHashMap<>(); 
		jobDtos.forEach(jobDto -> mapJobByContactId.put(jobDto.getContactid(), jobDto));
		Set<Integer> personIds = mapJobByContactId.keySet();
		log.debug("Loading DTOs for "+personIds.size()+ " contacts");
		
		List<ContactProjectionDTO> contactDtos = this.contactService.getContactProjectionDTOs(personIds, 
				command.getAllocatedCompanyId());
		for (ContactProjectionDTO contactDto : contactDtos) {
			for (JobProjectionDTO jobDto : mapJobByContactId.get(contactDto.getPersonid())) {
				jobDto.setContact(contactDto);
			}
		}
		return contactDtos;
	}

	private void loadJobProjectionCurrency(Collection<JobProjectionDTO> jobDtos) {
		MultiValuedMap<Integer, JobProjectionDTO> mapJobByCurrencyId = new HashSetValuedHashMap<>();
		jobDtos.forEach(jobDto -> mapJobByCurrencyId.put(jobDto.getCurrencyId(), jobDto));
				
		Set<Integer> currencyIds = mapJobByCurrencyId.keySet(); 
		List<KeyValueIntegerString> currencies = this.currencyService.getKeyValueList(currencyIds);
		for (KeyValueIntegerString currencyDto : currencies) {
			for (JobProjectionDTO jobDto : mapJobByCurrencyId.get(currencyDto.getKey())) {
				jobDto.setCurrency(currencyDto);
			}
		}
	}

	private Collection<SubdivProjectionDTO> loadJobProjectionOrganisation(Collection<JobProjectionDTO> jobDtos, JobProjectionCommand command) {
		MultiValuedMap<Integer, JobProjectionDTO> mapJobByOrgid = new HashSetValuedHashMap<>();
		jobDtos.forEach(jobDto -> mapJobByOrgid.put(jobDto.getOrgid(), jobDto));
		
		Set<Integer> subdivIds = mapJobByOrgid.keySet();
		List<SubdivProjectionDTO> subdivs = this.subdivService.getSubdivProjectionDTOs(subdivIds, command.getAllocatedCompanyId());
		for (SubdivProjectionDTO subdivDto : subdivs) {
			for (JobProjectionDTO jobDto : mapJobByOrgid.get(subdivDto.getSubdivid())) {
				jobDto.setOrganisation(subdivDto);
			}
		}
		return subdivs;
	}

	private void loadJobProjectionStatus(Collection<JobProjectionDTO> jobDtos, JobProjectionCommand command) {
		MultiValuedMap<Integer, JobProjectionDTO> mapJobByStatusId = new HashSetValuedHashMap<>();
		jobDtos.forEach(jobDto -> mapJobByStatusId.put(jobDto.getJobStatusId(), jobDto));
		
		Set<Integer> statusIds = mapJobByStatusId.keySet();
		List<KeyValueIntegerString> jobStatuses = this.jobStatusService.getKeyValueList(statusIds, command.getLocale());
		
		for (KeyValueIntegerString jobStatus : jobStatuses) {
			for (JobProjectionDTO jobDto : mapJobByStatusId.get(jobStatus.getKey())) {
				jobDto.setJobStatus(jobStatus);
			}
		}
	}
	
	private Collection<KeyValueIntegerString> loadJobProjectionTransportOut(Collection<JobProjectionDTO> jobDtos, JobProjectionCommand command) {
		MultiValuedMap<Integer, JobProjectionDTO> mapJobByTransportOutId = new HashSetValuedHashMap<>();
		jobDtos.forEach(jobDto -> mapJobByTransportOutId.put(jobDto.getTransportOutId(), jobDto));
		
		Set<Integer> transportOutIds = mapJobByTransportOutId.keySet();
		Boolean extendedFormat = true;
		// Already sorted by underlying service
		SortedSet<KeyValueIntegerString> transportDtos = this.transportOptionService.getTransportOptionDtosForIds(extendedFormat, transportOutIds, command.getLocale());
		
		for (KeyValueIntegerString transportOut : transportDtos) {
			for (JobProjectionDTO jobDto : mapJobByTransportOutId.get(transportOut.getKey())) {
				jobDto.setTransportOut(transportOut);
			}
		}
		
		return transportDtos;
	}
	
	private void loadJobProjectionClientPOs(Collection<JobProjectionDTO> jobDtos) {
		Map<Integer, JobProjectionDTO> mapJobByJobId = jobDtos.stream()
			.collect(Collectors.toMap(JobProjectionDTO::getJobid, jobDto -> jobDto));
		// TODO - consider Initializing all to single empty list (if needed - simpler if not?)
		// List<POProjectionDTO> emptyList = Collections.emptyList();
		// jobDtos.forEach(jobDto -> jobDto.setJobPos(emptyList));
		Collection<Integer> jobIds = mapJobByJobId.keySet();
		
		List<POProjectionDTO> jobPos = this.poService.getPOsByJobIds(jobIds);
		
		for (POProjectionDTO poDto : jobPos) {
			JobProjectionDTO jobDto = mapJobByJobId.get(poDto.getJobId());
			if (jobDto.getJobPos() == null)
				jobDto.setJobPos(new ArrayList<>());
			jobDto.getJobPos().add(poDto);
		}
	}
	
}
