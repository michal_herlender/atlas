package org.trescal.cwms.core.jobs.calibration.entity.batchcalibration;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "batchcalibration")
public class BatchCalibration extends Auditable
{
	private Date beginTime;
	private Date calDate;
	private List<Calibration> cals;
	private boolean chooseNext;
	private int id;
	private Job job;
	private Date lastCertify;
	private Date lastDespatch;
	private Date lastInvoice;
	private Date lastLabels;
	private Contact owner;
	private boolean passwordAccepted;
	private CalibrationProcess process;

	@Column(name = "begintime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getBeginTime()
	{
		return this.beginTime;
	}

	@NotNull
	@Column(name = "caldate", nullable = false, columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCalDate()
	{
		return this.calDate;
	}

	@OneToMany(mappedBy = "batch", cascade = CascadeType.ALL)
	@OrderBy("batchPosition")
	public List<Calibration> getCals()
	{
		return this.cals;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobid")
	public Job getJob()
	{
		return this.job;
	}

	@Column(name = "lastcertify", columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastCertify()
	{
		return this.lastCertify;
	}

	@Column(name = "lastdespatch", columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastDespatch()
	{
		return this.lastDespatch;
	}

	@Column(name = "lastinvoice", columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastInvoice()
	{
		return this.lastInvoice;
	}

	@Column(name = "lastlabels", columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastLabels()
	{
		return this.lastLabels;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ownerid", nullable=false)
	public Contact getOwner()
	{
		return this.owner;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "processid", nullable=false)
	public CalibrationProcess getProcess()
	{
		return this.process;
	}

	@Column(name = "choosenext", columnDefinition="bit")
	public boolean isChooseNext()
	{
		return this.chooseNext;
	}

	@Column(name = "passwordaccepted", columnDefinition="bit")
	public boolean isPasswordAccepted()
	{
		return this.passwordAccepted;
	}

	public void setBeginTime(Date beginTime)
	{
		this.beginTime = beginTime;
	}

	public void setCalDate(Date calDate)
	{
		this.calDate = calDate;
	}

	public void setCals(List<Calibration> cals)
	{
		this.cals = cals;
	}

	public void setChooseNext(boolean chooseNext)
	{
		this.chooseNext = chooseNext;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setJob(Job job)
	{
		this.job = job;
	}

	public void setLastCertify(Date lastCertify)
	{
		this.lastCertify = lastCertify;
	}

	public void setLastDespatch(Date lastDespatch)
	{
		this.lastDespatch = lastDespatch;
	}

	public void setLastInvoice(Date lastInvoice)
	{
		this.lastInvoice = lastInvoice;
	}

	public void setLastLabels(Date lastLabels)
	{
		this.lastLabels = lastLabels;
	}

	public void setOwner(Contact owner)
	{
		this.owner = owner;
	}

	public void setPasswordAccepted(boolean passwordAccepted)
	{
		this.passwordAccepted = passwordAccepted;
	}

	public void setProcess(CalibrationProcess process)
	{
		this.process = process;
	}
}