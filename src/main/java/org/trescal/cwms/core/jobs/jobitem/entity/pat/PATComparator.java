package org.trescal.cwms.core.jobs.jobitem.entity.pat;

import java.time.LocalDate;
import java.util.Comparator;

public class PATComparator implements Comparator<PAT>
{
	public int compare(PAT pat1, PAT pat2)
	{
		// compare jobitems
		int jiId1 = pat1.getJi().getJobItemId();
		int jiId2 = pat2.getJi().getJobItemId();

		if (jiId1 == jiId2) {
			// compare dates
			LocalDate date1 = pat1.getTestDate();
			LocalDate date2 = pat2.getTestDate();

			if (date1.isEqual(date2)) {
				// compare pat ids
				return Integer.compare(pat1.getId(), pat2.getId());
			} else {
				return date1.compareTo(date2);
			}
		}
		else {
			return (Integer.compare(jiId1, jiId2));
		}
	}
}
