package org.trescal.cwms.core.jobs.job.projection;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;

import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.company.projection.SubdivProjectionDTO;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import lombok.Getter;
import lombok.Setter;

/*
 * Used in updated JobSearch; contains information of job as query result
 * with ability to populate job items, pos, etc... with a separate query
 * 
 * Intended for more generic projection usage where we can selectively load relationships. 
 */
@Getter @Setter
public class JobProjectionDTO {
	// Below result fields always populated by query (constructor to match this order)
	// Key identification fields
	private Integer jobid;
	private String jobno;
	// Ids of relationships - alphabetical order
	private Integer bpoId;
	private Integer contactid;
	private Integer currencyId;
	private Integer defaultPoId;
	private Integer jobStatusId;
	private Integer orgid;
	private Integer orgcoid;
	private Integer transportOutId;
	// Additional fields - alphabetical order
	private String clientRef;
	private LocalDate dateComplete;
	private JobType type;
	private String bpoNumber;
	private ZonedDateTime receiptDate;
	private ZonedDateTime pickupDate;
	private LocalDate regDate;
	// Optional count of job items on job
	private Long jobItemCount;
	private Long jobExpenseItemCount; //Modification SK

	// Optional singular relationships
	private ContactProjectionDTO contact;
	private KeyValueIntegerString currency;        // ID and ISO symbol only
	private KeyValueIntegerString defaultPo;
	private KeyValueIntegerString jobBpo;
	private KeyValueIntegerString jobStatus;
	private SubdivProjectionDTO organisation;
	private KeyValueIntegerString transportOut;

	// Optional multivalued relationships
	private List<JobItemProjectionDTO> jobitems;
	private List<POProjectionDTO> jobPos;

	/**
	 * Used by JPA queries
	 */
	public JobProjectionDTO(Integer jobid, String jobno, Integer bpoId, Integer contactid, Integer currencyId,
							Integer defaultPoId, Integer jobStatusId, Integer orgid, Integer transportOutId, String clientRef,
							LocalDate dateComplete, ZonedDateTime receiptDate, LocalDate regDate, JobType type, Long jobItemCount, Long jobExpenseItemCount) {
		super();

		// Key fields
		this.jobid = jobid;
		this.jobno = jobno;

		// Ids
		this.bpoId = bpoId;
		this.contactid = contactid;
		this.currencyId = currencyId;
		this.defaultPoId = defaultPoId;
		this.jobStatusId = jobStatusId;
		this.orgid = orgid;
		this.transportOutId = transportOutId;
		
		// Additional fields
		this.clientRef = clientRef;
		this.dateComplete = dateComplete;
		this.receiptDate = receiptDate;
		this.regDate = regDate;
		this.type = type;
		
		// Optional field - uses null literal
		this.jobItemCount = jobItemCount;
		this.jobExpenseItemCount = jobExpenseItemCount;
	}
	
	public JobProjectionDTO(Integer jobid, String jobno, Integer bpoId, String bpoNumber, Integer contactid,
			Integer defaultPoId, String defaultPoNumber, Integer jobStatusId, Integer orgid, Integer orgcoid,
			String clientRef, LocalDate dateComplete, ZonedDateTime receiptDate, ZonedDateTime pickupDate, LocalDate regDate,
			JobType type, Long jobExpenseItemCount ) {
		super();
		
		// Key fields
		this.jobid = jobid;
		this.jobno = jobno;
		
		// Ids
		this.bpoId = bpoId;
		this.contactid = contactid;
		this.bpoNumber = bpoNumber;
		this.defaultPoId = defaultPoId;
		if(defaultPoId != null)
		this.defaultPo = new KeyValueIntegerString(defaultPoId, defaultPoNumber);
		this.jobStatusId = jobStatusId;
		this.orgid = orgid;
		this.orgcoid = orgcoid;
		
		// Additional fields
		this.clientRef = clientRef;
		this.dateComplete = dateComplete;
		this.receiptDate = receiptDate;
		this.pickupDate = pickupDate;
		this.regDate = regDate;
		this.type = type;
		this.jobExpenseItemCount = jobExpenseItemCount; 
	}


	public JobProjectionDTO(Integer jobid, String jobno, JobType type) {
		super();
		this.jobid = jobid;
		this.jobno = jobno;
		this.type = type;
	}

}
