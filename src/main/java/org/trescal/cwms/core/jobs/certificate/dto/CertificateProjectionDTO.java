package org.trescal.cwms.core.jobs.certificate.dto;

import java.util.Date;

import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.dto.InstrumentProjectionDTO;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
/**
 * Intended to be a flattened representation of a certificate 
 * for one job item (if queried via certlink) or
 * for one instrument (if queried via instcertlink). 
 * 
 * Aggregation can be done by certid if needed by the caller
 * to find all jobitems/instruments with one certId, if needed. 
 */
public class CertificateProjectionDTO {
	private Integer certId;
	private Integer plantId;	// Populated if queried via instcertlink
	private Integer jobItemId;	// Populated if queried via certlink
	private String certno;
	private Date calDate;
	private Date certDate;
//	private Integer supplementaryforId;		// Add when needed
	private String thirdCertNo;
//	private Integer thirdDivId;				// Add when needed
	private CertificateType type;
	private CalibrationVerificationStatus calibrationVerificationStatus;
	private CertStatusEnum certStatus;
	private Boolean optimization;
	private Boolean restriction;
	private Boolean adjustment;
	private Boolean repair;
	// Links to other DTOs below optionally populated after creation
	private InstrumentProjectionDTO instrument;
	private JobItemProjectionDTO jobItemDto;
	
	public CertificateProjectionDTO(Integer certId, Integer plantId, Integer jobItemId, String certno, 
			Date calDate, Date certDate, String thirdCertNo, CertificateType type, 
			CalibrationVerificationStatus calibrationVerificationStatus, CertStatusEnum certStatus,
			Boolean optimization, Boolean restriction, Boolean adjustment, Boolean repair) {
		this.certId = certId;
		this.plantId = plantId;
		this.jobItemId = jobItemId;
		this.certno = certno;
		this.calDate = calDate;
		this.certDate = certDate;
		this.thirdCertNo = thirdCertNo;
		this.type = type;
		this.calibrationVerificationStatus = calibrationVerificationStatus;
		this.certStatus = certStatus;
		this.optimization = optimization;
		this.restriction = restriction;
		this.adjustment = adjustment;
		this.repair = repair;
	}
}
