package org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirementDTO;

public interface TPRequirementDao extends BaseDao<TPRequirement, Integer>
{
	List<TPRequirementDTO> getProjectionDTOsForIds(Collection<Integer> requirementIds);	
}