package org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem;

import java.util.Comparator;

/**
 * Simple {@link Comparator} implementation that sorts
 * {@link ContractReviewItem} entities by id descending.
 * 
 * @author richard
 */
public class ContractReviewItemComparator implements Comparator<ContractReviewItem>
{
	@Override
	public int compare(ContractReviewItem o1, ContractReviewItem o2)
	{
		if ((o1.getId() == 0) && (o2.getId() == 0))
		{
			return 1;
		}
		else
		{
			return ((Integer) o1.getId()).compareTo(o2.getId());
		}
	}
}
