package org.trescal.cwms.core.jobs.certificate.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.db.InstCertLinkService;
import org.trescal.cwms.core.jobs.certificate.form.EditClientCertificateForm;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.recall.entity.recallrule.db.RecallRuleService;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CalibrationVerification;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.Deviation;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME })
public class EditClientCertificateController {
	public static final String FORM_NAME = "form";
	public static final String VIEW_NAME = "trescal/core/jobs/certificate/editclientcert";

	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private CertificateService certServ;
	@Autowired
	private InstCertLinkService iclServ;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private MessageSource messages;
	@Autowired
	private SubdivService subdivServ;
	@Autowired
	private UserService userService;
	@Autowired
	private RecallRuleService recallRuleService;
	@Autowired
	private CalibrationVerification systemDefaultCalibrationVerification;
	@Autowired
	private Deviation systemDefaultDeviation;

	@ModelAttribute("units")
	public List<KeyValue<String, String>> initializeRecallIntervalUnits() {
		return IntervalUnit.getUnitsForSelect();
	}

	@ModelAttribute("calVerificationStatusRequired")
	public Boolean isCalVerificationStatusRequired(@RequestParam(value = "plantid", required = true) int plantid) {
		Instrument inst = this.instServ.get(plantid);
		return this.systemDefaultCalibrationVerification.getValueByScope(Scope.COMPANY, inst.getComp(), null);
	}

	@ModelAttribute("deviationRequired")
	public Boolean isDeviationRequired(@RequestParam(value = "plantid", required = true) int plantid) {
		Instrument inst = this.instServ.get(plantid);
		return this.systemDefaultDeviation.getValueByScope(Scope.COMPANY, inst.getComp(), null);
	}

	@ModelAttribute("calVerificationStatuses")
	public List<KeyValue<CalibrationVerificationStatus, String>> initializeCalVerificationStatuses() {
		return Arrays.asList(CalibrationVerificationStatus.values()).stream()
				.map(cs -> new KeyValue<>(cs, cs.getName())).collect(Collectors.toList());
	}

	@ModelAttribute(FORM_NAME)
	protected EditClientCertificateForm formBackingObject(
			@RequestParam(value = "certid", defaultValue = "0", required = false) int certid,
			@RequestParam(value = "plantid", required = true) int plantid) throws Exception {
		Instrument inst = this.instServ.get(plantid);

		if (inst == null) {
			String message = this.messages.getMessage("error.instrument.plantid.notfound",
					new Object[] { String.valueOf(plantid) }, "Instrument could not be found", null);
			throw new Exception(message);
		}

		EditClientCertificateForm form = new EditClientCertificateForm();

		if (certid != 0) {
			Certificate cert = this.certServ.findCertificate(certid);
			form.setCert(cert);
			form.setInst(inst);
			form.setPersisted(true);
			form.setDurationUnitId(cert.getUnit().name());
			form.setCalVerificationStatus(cert.getCalibrationVerificationStatus());
			form.setServiceTypeId(cert.getServiceType() != null ? cert.getServiceType().getServiceTypeId() : null);
			if (cert.getDeviation() != null) {
				form.setDeviation(cert.getDeviation().stripTrailingZeros());
			}
		} else if ((plantid != 0) && (certid == 0)) {
			Certificate cert = new Certificate();
			cert.setCalDate(new Date());
			cert.setCertDate(new Date());
			cert.setDuration(inst.getCalFrequency());
			cert.setUnit(inst.getCalFrequencyUnit());
			cert.setType(CertificateType.CERT_CLIENT);

			form.setCert(cert);
			form.setPersisted(false);
			form.setInst(inst);
			form.setDurationUnitId(inst.getCalFrequencyUnit().name());
		}

		return form;
	}

	@InitBinder()
	protected void initBinder(ServletRequestDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
	}

	@RequestMapping(value = "/editclientcert.htm", method = RequestMethod.POST)
	protected String onSubmit(Model model, Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@Validated @ModelAttribute(FORM_NAME) EditClientCertificateForm form, BindingResult bindingResult)
			throws IOException {
		if (bindingResult.hasErrors())
			return referenceData(model, locale);
		Subdiv allocatedSubdiv = subdivServ.get(subdivDto.getKey());
		Contact currentUser = userService.get(username).getCon();
		IntervalUnit intervalUnit = IntervalUnit.valueOf(form.getDurationUnitId());
		Certificate cert = form.getCert();
		if (!form.isPersisted()) {
			cert.setCertno(this.certServ.findNewCertNo(cert.getType(), allocatedSubdiv));
			cert.setRegisteredBy(currentUser);
		}
		ServiceType serviceType = (form.getServiceTypeId() != null) ? this.serviceTypeService.get(form.getServiceTypeId()) : null;
		cert.setServiceType(serviceType);
		cert.setUnit(intervalUnit);
		cert.setCalibrationVerificationStatus(form.getCalVerificationStatus());
		cert.setDeviation(form.getDeviation());
		Subdiv subdiv = (form.getSubdivid() != null) ? this.subdivServ.get(form.getSubdivid()) : null;
		cert.setThirdDiv(subdiv);
		if (!form.isPersisted()) {
			this.certServ.insertCertificate(cert, currentUser);
			InstCertLink icl = new InstCertLink();
			icl.setCert(cert);
			icl.setInst(form.getInst());
			this.iclServ.insertInstCertLink(icl);
			cert.setInstCertLinks(new HashSet<InstCertLink>());
			cert.getInstCertLinks().add(icl);
			this.certServ.setCertificateDirectory(cert);
		}
		boolean pdfUploaded = certServ.uploadFile(form.getFile(), cert, currentUser);
		if (pdfUploaded) {
			cert.setStatus(CertStatusEnum.SIGNED);
		} else if (cert.getStatus() == null) {
			cert.setStatus(CertStatusEnum.SIGNING_NOT_SUPPORTED);
		}
		this.certServ.updateCertificate(cert);
		if (form.isUpdateNextCalDueDate()) {
			recallRuleService.insert(form.getInst().getPlantid(), cert.getDuration(), null, true, intervalUnit,
					RecallRequirementType.FULL_CALIBRATION, false, null);
			instServ.calculateRecallDateAfterTPCert(form.getInst(), cert);
		}
		return "redirect:viewinstrument.htm?loadtab=certs-tab&plantid=" + form.getInst().getPlantid();
	}

	@RequestMapping(value = "/editclientcert.htm", method = RequestMethod.GET)
	protected String referenceData(Model model, Locale locale) {
		model.addAttribute("serviceTypes", this.serviceTypeService.getDTOList(DomainType.INSTRUMENTMODEL, null, locale, true));
		return VIEW_NAME;
	}

}
