package org.trescal.cwms.core.jobs.certificate.entity.certificate.db;

import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.batch.jobs.operationsimport.ImportedOperationItem;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.WebPermissionException;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.certificate.dto.*;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateReplacementType;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.certificate.form.CertificateForm;
import org.trescal.cwms.core.jobs.certificate.form.CertificateSearchForm;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.CapabilityTrainingRecord;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.common.dto.RestCommonCertificateDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Interface for accessing and manipulating {@link Certificate} entities.
 *
 * @author jamiev
 */
public interface CertificateService {

    /**
     * Approves a {@link Certificate} that was previously Awaiting Approval. As well
     * as changing the status of the {@link Certificate} entity, this method adds
     * the {@link User}'s approval stamp image onto the {@link Certificate}'s PDF
     * file.
     *
     * @param certid             the ID of the {@link Certificate} to approve.
     * @param password           the password entered by the signing party
     * @param isProcTrainingCert indicates if the certificate should be marked as a
     *                           {@link CapabilityTrainingRecord}
     * @param req                the {@link HttpServletRequest} the operation was
     *                           called from.
     * @return a {@link ResultWrapper} indicating the success of the operation.
     */
	ResultWrapper approveCertificate(int certid, String password, boolean isProcTrainingCert, HttpServletRequest req);

	/**
	 * creates a preview of a {@link Certificate} pdf so an engineer can review the
	 * results of a calibration
	 * 
	 * @param certId       id of the {@link Certificate}
	 * @param calId        id of the {@link Calibration}
	 * @param certTemplate string filename of the certificate template
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper createCertificatePreview(Integer certId, Integer calId, String certTemplate, String tempDir,
			HttpSession session);

	/**
	 * Creates one or more certificates initialized in "Reserved" status, if they do
	 * not already exist.
	 */
	void createOrUpdateReservedCertificates(JobItem ji, Contact contact, Date certDate);

	void delayCertificate(Certificate cert);

	/**
	 * Returns the {@link Certificate} with the given ID from the database.
	 * 
	 * @param id the {@link Certificate} ID.
	 * @return the {@link Certificate}
	 */
	Certificate findCertificate(int id);

	/**
	 * Returns the {@link Certificate} with the (exact) given cert number
	 * 
	 * @param certno the cert number
	 * @return the {@link Certificate}, or null if not found
	 */
	Certificate findCertificateByCertNo(String certno);

	/**
	 * Gets the next certificate number specific to the calibration type.
	 * 
	 * @param type    the {@link CertificateType}
	 * @return the generated certificate number.
	 */
	String findNewCertNo(CertificateType type, Subdiv allocatedSubdiv);

	Certificate findWebSafeCertificate(int id) throws WebPermissionException;

	/**
	 * this method returns a list of all {@link Certificate} which are at the status
	 * of 'Unsigned - Awaiting Signature'.
	 * 
	 * @return {@link List} of {@link Certificate}s
	 */
	List<Certificate> getAllCertificatesAwaitingSignature();

	List<File> certificateFilesForNumbers(String jobNumber, List<String> certNumbers);

	/**
	 * Determines if a reserved certificate exists on the attached job item and if
	 * so, returns the first reserved certificate found.
	 */
	Certificate getReservedCertificate(JobItem jobItem);

	String getReservedCertificateNumber(Integer jobItemId);

	/**
	 * Inserts the given {@link Certificate} into the database.
	 * 
	 * @param c the {@link Certificate} to insert.
	 */
	void insertCertificate(Certificate c, Contact currentUser);

	/**
     * Issues a custom {@link Certificate}
     *
     */
    void publishCustomCert(CustomCertWrapper ccw, Date calDate, Contact issuedBy, Subdiv allocatedSubdiv);

    /**
     * Issues a custom group {@link Certificate}
     *
     */
    void publishCustomGroupCert(CustomGroupCertWrapper gw, Date calDate, Contact issuedBy,
                                Subdiv allocatedSubdiv);

    ResultWrapper linkCertToCal(int certid, int calid, HttpSession session);

	void linkCertToCal(Certificate cert, Calibration cal);

	/**
	 * this method sets the boolean field on {@link Certificate} as previewed
	 * 
	 * @param certid the id of the {@link Certificate} to be marked as previewed
	 * @return a {@link ResultWrapper}
	 */
	ResultWrapper markCertificateAsPreviewed(int certid, HttpSession session);

	/**
	 * Deletes the PDF {@link File} of the {@link Certificate} with the given ID (if
	 * there is one)
	 *
	 * @param certid the {@link Certificate} ID.
	 */
	void resetCertificate(int certid, Contact currentUser);

	void searchCertificatesJPA(CertificateSearchForm form, PagedResultSet<CertificateSearchWrapper> prs,
			boolean includeDeletedAndCancelledCertificates);

	void setCertificateDirectory(Certificate cert);
	
	File getCertificateDirectory(CertificateType certType, Integer firstInstLinkPlantId, String firstJobitemLinkJobNo);

	void signCertificate(Certificate cert, Contact con);

	/**
	 * Attempts to "un-issue" the given {@link Certificate}, effectively deleting it
	 * and all of its history from the database, and setting its number to be
	 * recycled.
	 * 
	 * @param certid the ID of the {@link Certificate} to delete
	 * @return the {@link ResultWrapper} indicating the success of the operation
	 */
	ResultWrapper unIssueCertificate(int certid, HttpSession session);

	/**
	 * Updates the given {@link Certificate} in the database.
	 * 
	 * @param c the {@link Certificate} to update.
	 */
	void updateCertificate(Certificate c);

	boolean wordFileExistsInCertificateDirectory(int certId);

	/**
	 * Creates a link from a certificate to an instrument without a calibration or
	 * job required
	 * 
	 * @param certificate The certificate to link
	 * @param instrument  The instrument to link it to
	 */
	void linkCertToInstrument(Certificate certificate, Instrument instrument);

	/**
	 * Gets the most recent signed certificate for this instrument whether it be a
	 * job certificate or a client certificate.
	 * 
	 * @param instrument - The instrument to search for a certificate for
	 * @parma calibratedBefore - Optional, only include certificates for
	 *        calibrations before this date.
	 */
	Certificate getMostRecentCertForInstrument(Instrument instrument, Date calibratedBefore);

	/**
	 * Gets the instruments that this certificate applies to (usually just 1
	 * instrument)
	 * 
	 */

	List<Instrument> getRelatedInstruments(Certificate certificate);

	Certificate createCommonCertificate(RestCommonCertificateDTO inputDTO, String fileExtension);

	PagedResultSet<Certificate> getAllCertificatesByRelatedInstrument(Integer plantid, Integer resultsPerPage,
			Integer currentPage);

	boolean getCertificateFileExists(Certificate cert);

	String readCertificateFileIntoBase64Format(Certificate cert);

	Certificate commonRestUpdateCertificate(RestCommonCertificateDTO inputDTO, String fileExtension);

	List<Certificate> getCertificateListByJobitemId(Integer jobitemid);

	/**
	 * Gets all certificates for instrument, both job related and non job instrument
	 * certs combined and sorted in chronological order
	 *
	 */

	List<Certificate> getCertificatesForInstrument(Instrument instrument);

	Certificate getMostRecentCertificateForJobItem(JobItem jobItem);

	/**
	 * Uploads file against cert. Does nothing if file is null.
	 *
	 */
	boolean uploadFile(MultipartFile file, Certificate cert, Contact contact) throws IOException;

	Certificate getLatestCertificatesForInstrument(Instrument instrument);

	void linkCertToJobItem(Certificate cert, JobItem ji, Contact contact, Date startDate);

	void createLinksForCert(Certificate cert, Calibration cal, List<Integer> calLinkIdsToInclude, Contact contact,
			Date startDate);

	void createLinksForCert(Certificate cert, List<Integer> calLinkIdsToInclude, Contact contact, Date startDate);

	/**
     * Creates a supplementary (replacement) certificate
     *         the certificate as a "certDate" and on the job item activity for any linked job items, indicating that
     *         a certificate has been created.  If not specified the current date is used.
     *
     * The certificate *must* have a calibration attached in order to reissue, this should be already checked via validation/logic
     */
    Certificate createSupplementaryCert(int certId, Contact currentUser, Date startDate);

	List<Certificate> getByThirdCertNoAndPlantId(String thirdCertificateNumber, Integer plantId);

	Certificate createThirdPartyCertFromCal(Subdiv allocatedSubdiv, Calibration cal, Contact registredBy,
			boolean signCert, Contact validatedBy, Instrument inst, String certNumber, Date certDate);

	Certificate importCertificate(ImportedOperationItem dto, Contact crc, JobItem jobitem,
			Contact certificateValidatedBy, Calibration cal);

	Certificate setStatus(Certificate certificate, String statusName);

	RestResultDTO<String> uploadOnSiteCertificate(Integer jobid, List<MultipartFile> multipartFiles, Locale locale,
			Contact user) throws IOException;

	List<Certificate> getByThirdCertNoAndJobId(String fileBaseName, int jobid);

	List<CertificateProjectionDTO> getCertificatesForJobItemIds(Collection<Integer> jobItemIds);

	void deleteCertificate(Certificate cert, Contact con);

	Certificate createCertFromCal(Contact con, Calibration cal, Integer duration, boolean signCert,
			Subdiv allocatedSubdiv, IntervalUnit intervalUnit, CertStatusEnum defaultCertStatus, Date certDate);

	Certificate createCertFromCal(Contact con, Calibration cal, Integer duration, boolean signCert,
			List<Integer> calLinkIdsToInclude, boolean certPreviewed, Subdiv allocatedSubdiv, IntervalUnit intervalUnit,
			CertStatusEnum defaultCertStatus, Date certDate);

	Date getLastTpCertDate(Integer jobitemid);

	void importCertificateFile(String jobNo, String fileExtension, byte[] bytes, List<Certificate> certificates);
	
	/**
	 * Returns the business subdivision ID associated with the specified certificate (via Calibration if it exists)
	 * If certificate doesn't exist or there is no associated Calibration null is returned 
	 */
	Integer getSubdivIdFromCalibration(int certid);

	ResultWrapper certificateReplacement(Integer jobItemId, Integer certificateId, Locale locale, CertificateReplacementType certType);
	
	JobItemWorkRequirement performCertificateReplacement(JobItem ji, WorkRequirement wr, Certificate cert, CertificateReplacementType certType);


    List<CertificateForInstrumentTooltipDto> findCertificatesForInstrumentTooltipByPlantId(Integer plantId);
    
    void searchCertificatesProjectionJPA(CertificateSearchForm form, PagedResultSet<CertificateSearchWrapper> prs,
			boolean includeDeletedAndCancelledCertificates, Locale locale);
    
    void signRestCertificate(Certificate certificate, Contact contact, Date issueDate, String thirdCertNo,
			Integer timeSpent, Date startDate);
    
    void resetCertificateValidation(Certificate certificate, Contact contact);
    
    List<String> getRelatedInstrumentsInstructions(Integer certificateId);
    
    List<String> getRelatedInstrumentsAcceptanceCriteria(Integer certificateId);
    
    String getRelatedInstrumentDeviation(Integer certificateId);
    
    Certificate editCertificate( Integer certid, CertificateForm form, String username, Integer allocatedSubdivId);
    
}
