package org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.AbstractJobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.JobItemType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobExpenseItemNotInvoiced;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingexpenseitem.JobCostingExpenseItem;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

import lombok.Setter;

@Setter
@Entity
@Table(name = "jobexpenseitem", uniqueConstraints = @UniqueConstraint(columnNames = { "job",
		"itemNo" }, name = "UQ_jobexpenseitem"))
@AssociationOverrides({
		@AssociationOverride(name = "job", joinColumns = @JoinColumn(name = "job"), foreignKey = @ForeignKey(name = "FK_jobexpenseitem_job")) })
public class JobExpenseItem extends AbstractJobItem<JobExpenseItemPO> implements Comparable<JobExpenseItem>, Serializable {

	private static final long serialVersionUID = -1442535401551932729L;

	private Integer id;
	private Integer itemNo;
	private ServiceType serviceType;
	private InstrumentModel model;
	private BigDecimal singlePrice;
	private Integer quantity;
	private String comment;
	private LocalDate date;
	private Boolean invoiceable;
	private List<JobCostingExpenseItem> costingItems;
	private Set<InvoiceItem> invoiceItems;
	private JobExpenseItemNotInvoiced notInvoiced;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	@NotNull
	public Integer getItemNo() {
		return itemNo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "serviceType", foreignKey = @ForeignKey(name = "FK_jobexpenseitem_serviceType"), nullable = false)
	public ServiceType getServiceType() {
		return serviceType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "model", foreignKey = @ForeignKey(name = "FK_jobexpenseitem_model"))
	public InstrumentModel getModel() {
		return model;
	}

	@NotNull
	@Column(name = "cost", nullable = false, precision = 10, scale = 2)
	public BigDecimal getSinglePrice() {
		return singlePrice;
	}

	@NotNull
	@Min(1)
	@Column(name = "quantity", nullable = false)
	public Integer getQuantity() {
		return quantity;
	}

	@Transient
	public BigDecimal getTotalPrice() {
		return getSinglePrice().multiply(BigDecimal.valueOf(getQuantity()));
	}

	public String getComment() {
		return comment;
	}

	@DateTimeFormat(iso = ISO.DATE)
	public LocalDate getDate() {
		return date;
	}

	@Column(name = "invoiceable")
	public Boolean getInvoiceable() {
		return invoiceable;
	}

	@OneToMany(mappedBy = "jobExpenseItem", fetch = FetchType.LAZY)
	public List<JobCostingExpenseItem> getCostingItems() {
		return costingItems;
	}

	public void setCostingItems(List<JobCostingExpenseItem> costingItems) {
		this.costingItems = costingItems;
	}

	@OneToMany(mappedBy = "expenseItem", fetch = FetchType.LAZY)
	public Set<InvoiceItem> getInvoiceItems() {
		return invoiceItems;
	}

	@Override
	public int compareTo(JobExpenseItem other) {
		return this.getJob().equals(other.getJob()) ? itemNo - other.itemNo
				: this.getJob().getJobno().compareTo(other.getJob().getJobno());
	}

	@Override
	@Transient
	public JobItemType getJobItemType() {
		return JobItemType.SERVICE;
	}

	@OneToOne(mappedBy = "expenseItem", cascade = CascadeType.ALL, orphanRemoval = true)
	public JobExpenseItemNotInvoiced getNotInvoiced() {
		return notInvoiced;
	}
}