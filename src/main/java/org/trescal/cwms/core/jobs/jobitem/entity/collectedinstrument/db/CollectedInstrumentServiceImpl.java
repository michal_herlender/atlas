package org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.dto.CollectedInstrumentDto;
import org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.CollectedInstrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.Discount;
import org.trescal.cwms.core.system.enums.Scope;

import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.trescal.cwms.core.tools.DateTools.dateToZonedDateTime;

@Service("CollectedInstrumentService")
public class CollectedInstrumentServiceImpl implements CollectedInstrumentService {
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private CollectedInstrumentDao collectedInstrumentDao;
    @Autowired
    private Discount discount;
    @Autowired
	private InstrumService instrumServ;
	@Autowired
	private JobItemService jiServ;

	@Override
	public int addCollectedInstrumentsToJob(List<Integer> collectedInstIds, Job job, Contact currentContact,
			int nextItemNo) {
		// look for discount defaults for this contact/sub/company or system default
		Double calDiscount = discount.parseValue(
				discount.getValueHierarchical(Scope.CONTACT, job.getCon(), job.getOrganisation().getComp()).getValue());
		
		Set<Integer> allowedServiceTypeIds = this.serviceTypeService.getIdsByJobType(job.getType());

		for (Integer id : collectedInstIds) {
            // find collected inst info
            CollectedInstrument ci = this.findCollectedInstrument(id);

            // insert as job item
            JobItem ji = new JobItem();
            ji.setInst(ci.getInst());
            ji.setItemNo(nextItemNo);
            nextItemNo++;
            if (jiServ.getDateIn(job) != null)
                ji.setDateIn(jiServ.getDateIn(job));
            else
                ji.setDateIn(dateToZonedDateTime(ci.getCollectStamp()));

            ji.setJob(job);
            ji.setCurrentAddr(job.getBookedInAddr());
            ji.setCurrentLoc(job.getBookedInLoc());
            ji.setBookedInAddr(job.getBookedInAddr());
            ji.setBookedInLoc(job.getBookedInLoc());

			// this will be overridden by the status set in any active hooks
			ji.setState(null);

			ji.setTurn(job.getDefaultTurn());
			ji.setReqCleaning(false);
			if ((ci.getInst().getDefaultServiceType() != null) && 
					allowedServiceTypeIds.contains(ci.getInst().getDefaultServiceType().getServiceTypeId())) {
				ji.setServiceType(ci.getInst().getDefaultServiceType());
			} else {
				ji.setServiceType(job.getDefaultCalType().getServiceType());
			}

			this.jiServ.insertJobItem(ji, currentContact, calDiscount, null);

			// add item to job
			job.getItems().add(ji);

			// delete collected inst from pool
			this.deleteCollectedInstrument(ci);
		}

		return nextItemNo;
	}

	@Override
	public void collectInstruments(List<Integer> plantIds, Contact collectedBy) {
		for (Integer plantId : plantIds) {
			if (plantId != null) {
				CollectedInstrument ci = new CollectedInstrument();
				ci.setInst(this.instrumServ.get(plantId));
				ci.setCollectedBy(collectedBy);
				ci.setCollectStamp(new Date());
				this.collectedInstrumentDao.persist(ci);
			}
		}
	}

	public void deleteCollectedInstrument(CollectedInstrument collectedinstrument) {
		this.collectedInstrumentDao.remove(collectedinstrument);
	}

	public CollectedInstrument findCollectedInstrument(int id) {
		return this.collectedInstrumentDao.find(id);
    }

    public List<CollectedInstrument> getAllCollectedInstruments() {
        return this.collectedInstrumentDao.findAll();
    }

    public List<CollectedInstrument> getCollectedInstrumentsForCompany(int coid) {
        return this.collectedInstrumentDao.getCollectedInstrumentsForCompany(coid);
    }

    public void insertCollectedInstrument(CollectedInstrument collectedInstrument) {
        this.collectedInstrumentDao.persist(collectedInstrument);
    }

    public void saveOrUpdateCollectedInstrument(CollectedInstrument collectedinstrument) {
        this.collectedInstrumentDao.merge(collectedinstrument);
    }

    public void updateCollectedInstrument(CollectedInstrument collectedInstrument) {
        this.collectedInstrumentDao.merge(collectedInstrument);
    }

    @Override
    public List<CollectedInstrumentDto> getCollectedInstrumentsDtoForCompany(Integer coid) {
        return collectedInstrumentDao.getCollectedInstrumentsDtoForCompany(coid);
    }
}