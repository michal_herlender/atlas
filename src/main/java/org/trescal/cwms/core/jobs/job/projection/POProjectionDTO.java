package org.trescal.cwms.core.jobs.job.projection;

import lombok.Getter;
import lombok.Setter;

/**
 * Aggregates PO for a job
 *
 */
@Getter
@Setter
public class POProjectionDTO {

	private Integer jobId;
	private Integer poId;
	private String poNumber;
	private Boolean active;
	private Boolean jobDefault;
	private String comment;
	private Long countJobItems;
	private Long countExpenseItems;

	public POProjectionDTO(Integer jobId, Integer poId, String poNumber) {
		super();
		this.jobId = jobId;
		this.poId = poId;
		this.poNumber = poNumber;
	}

	public POProjectionDTO(Integer jobId, Integer poId, String poNumber, Boolean active, Integer jobDefaultPoId,
			String comment, Long countJobItems, Long countExpenseItems) {
		super();
		this.jobId = jobId;
		this.poId = poId;
		this.poNumber = poNumber;
		this.active = active;
		this.jobDefault = poId == jobDefaultPoId;
		this.comment = comment;
		this.countJobItems = countJobItems;
		this.countExpenseItems = countExpenseItems;
	}
}