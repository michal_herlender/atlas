package org.trescal.cwms.core.jobs.job.controller;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.form.AddQuickItemsForm;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;

@Controller
@JsonController
@SessionAttributes(value = { Constants.SESSION_ATTRIBUTE_USERNAME })
public class AddQuickItemsController {

	@Autowired
	private JobService jobServ;
	@Autowired
	private UserService userService;
	@Autowired
	private AddressService addrServ;
	@Autowired
	private JobItemService jiService;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
	}

	@RequestMapping(value = "/addquickitems.json", method = RequestMethod.POST)
	@ResponseBody
	protected List<Integer> onSubmit(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("form") AddQuickItemsForm form, Locale locale) {

		Job job = jobServ.get(form.getJobId());
		Contact contact = this.userService.getEagerLoad(username).getCon();
		Address bookedInAt = this.addrServ.get(form.getBookingInAddrId());

		return this.jiService.insertQuickItems(job, form.getRows(), bookedInAt, contact);
	}
}
