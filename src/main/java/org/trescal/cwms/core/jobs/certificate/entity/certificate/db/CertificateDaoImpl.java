package org.trescal.cwms.core.jobs.certificate.entity.certificate.db;

import lombok.val;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrument.entity.instrumentcomplementaryfield.InstrumentComplementaryField_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration_;
import org.trescal.cwms.core.jobs.certificate.dto.*;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate_;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink_;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink_;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidation;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidationStatus;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidation_;
import org.trescal.cwms.core.jobs.certificate.form.CertificateSearchForm;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem_;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.web.certificate.form.WebCertificateSearchForm;

import javax.persistence.criteria.*;
import java.util.*;

@Repository("CertificateDao")
public class CertificateDaoImpl extends BaseDaoImpl<Certificate, Integer> implements CertificateDao {

	@Override
	protected Class<Certificate> getEntity() {
		return Certificate.class;
	}

	@Override
	public void delayCertificate(Certificate cert) {
		// This method is required for the hook to delay a certificate
	}

	public Certificate findCertificateByCertNo(String certno) {
		return getFirstResult(cb -> {
			CriteriaQuery<Certificate> cq = cb.createQuery(Certificate.class);
			Root<Certificate> certificate = cq.from(Certificate.class);
			cq.where(cb.equal(certificate.get(Certificate_.certno), certno));
			return cq;
		}).orElse(null);
	}

	public List<Certificate> getAllCertificatesAwaitingSignature() {
		return getResultList(cb -> {
			CriteriaQuery<Certificate> cq = cb.createQuery(Certificate.class);
			Root<Certificate> certificate = cq.from(Certificate.class);
			cq.where(cb.equal(certificate.get(Certificate_.status), CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE));
			return cq;
		});
	}

	public List<Certificate> getAllCertsForJob(Job job) {
		return getResultList(cb -> {
			CriteriaQuery<Certificate> cq = cb.createQuery(Certificate.class);
			Root<Certificate> certificate = cq.from(Certificate.class);
			Join<Certificate, CertLink> certLink = certificate.join(Certificate_.links);
			Join<CertLink, JobItem> jobItem = certLink.join(CertLink_.jobItem);
			cq.where(cb.equal(jobItem.get(JobItem_.job), job));
			cq.orderBy(cb.desc(certificate.get(Certificate_.certDate)), cb.desc(certificate.get(Certificate_.certno)));
			return cq;
		});
	}

	@Override
	public String getReservedCertificateNumber(Integer jobItemId) {
		return getFirstResult(cb -> {
			CriteriaQuery<String> cq = cb.createQuery(String.class);
			Root<CertLink> certLink = cq.from(CertLink.class);
			Join<CertLink, Certificate> certificate = certLink.join(CertLink_.cert);
			cq.where(cb.and(cb.equal(certLink.get(CertLink_.jobItem), jobItemId),
					cb.equal(certificate.get(Certificate_.status), CertStatusEnum.RESERVED)));
			cq.select(certificate.get(Certificate_.certno));
			return cq;
		}).orElse(null);
	}

	@Override
	public void searchCertificatesJPA(CertificateSearchForm form, PagedResultSet<CertificateSearchWrapper> prs,
			boolean includeDeletedAndCancelledCertificates) {
		completePagedResultSet(prs, CertificateSearchWrapper.class, cb -> cq -> {
			Root<Certificate> root = cq.from(Certificate.class);

			Predicate clauses = cb.conjunction();

			// do not include deleted certificates
			if (!includeDeletedAndCancelledCertificates) {
				clauses.getExpressions().add(cb.notEqual(root.get(Certificate_.status), CertStatusEnum.DELETED));
				clauses.getExpressions().add(cb.notEqual(root.get(Certificate_.status), CertStatusEnum.CANCELLED));
			}

			// third party company
			if (form.getTpId() != null && form.getTpId() != 0) {
				Join<Certificate, Subdiv> thirdSubdiv = root.join(Certificate_.thirdDiv);
				Join<Subdiv, Company> thirdCompany = thirdSubdiv.join(Subdiv_.comp);
				clauses.getExpressions().add(cb.equal(thirdCompany.get(Company_.coid), form.getTpId()));
			}
			// third party cert no
			if ((form.getTpCertNo() != null) && !form.getTpCertNo().trim().isEmpty()) {
				String likeParam = "%" + form.getTpCertNo().trim() + "%";
				clauses.getExpressions().add(cb.like(root.get(Certificate_.thirdCertNo), likeParam));
			}
			// cert no
			if ((form.getCertNo() != null) && !form.getCertNo().trim().isEmpty()) {
				String likeParam = "%" + form.getCertNo().trim() + "%";
				clauses.getExpressions().add(cb.like(root.get(Certificate_.certno), likeParam));
			}
			// status
			if (form.getStatus() != null) {
				clauses.getExpressions().add(cb.equal(root.get(Certificate_.status), form.getStatus()));
			}
			// cal type
			if (form.getCalTypeId() != null && form.getCalTypeId() != 0) {
				Join<Certificate, CalibrationType> calType = root.join(Certificate_.calType);
				clauses.getExpressions().add(cb.equal(calType.get(CalibrationType_.calTypeId), form.getCalTypeId()));
			}
			// issue date
			if (form.getIssueDate1() != null) {
				if (form.isIssueDateBetween()) {
					clauses.getExpressions().add(
							cb.between(root.get(Certificate_.certDate), form.getIssueDate1(), getEndDayDateTime(form.getIssueDate2())));
				} else {
					clauses.getExpressions().add(cb.between(root.get(Certificate_.certDate), form.getIssueDate1(), getEndDayDateTime(form.getIssueDate1())));
				}
			}
			// cal date
			if (form.getCalDate1() != null) {
				if (form.isCalDateBetween()) {
					clauses.getExpressions()
							.add(cb.between(root.get(Certificate_.calDate), form.getCalDate1(), getEndDayDateTime(form.getCalDate2())));
				} else {
					clauses.getExpressions().add(cb.between(root.get(Certificate_.calDate), form.getCalDate1(), getEndDayDateTime(form.getCalDate1())));
				}
			}
			// calverificationstatus
			if (form.getCalibrationVerificationStatus() != null) {
				clauses.getExpressions().add(cb.equal(root.get(Certificate_.calibrationVerificationStatus),
						form.getCalibrationVerificationStatus()));
			}
			if (form.hasJobSearch() || form.hasInstrumentSearch() || form.hasModelSearch() || form.hasSubfamilySearch()
					|| form.hasMfrSearch() || form.hasCompanySearch() || form.hasContactSearch()
					|| form.hasAddressSearch() || form.hasSubdivSearch() || form.hasJobitemSearch()) {
				// require job item join (all left joins, as instrument links
				// via job item or directly)
				Join<Certificate, CertLink> linkJobItem = root.join(Certificate_.links, JoinType.LEFT);
				Join<CertLink, JobItem> jobItem = linkJobItem.join(CertLink_.jobItem, JoinType.LEFT);
				
				// jobitem completion date
				if (form.getJobItemDateComplete1() != null) {
					if (form.isJobItemCompletionDateBetween()) {
						clauses.getExpressions()
								.add(cb.between(jobItem.get(JobItem_.dateComplete), form.getJobItemDateComplete1(), form.getJobItemDateComplete2()));
					} else {
						clauses.getExpressions().add(cb.between(jobItem.get(JobItem_.dateComplete), form.getJobItemDateComplete1(), getEndDayDateTime(form.getJobItemDateComplete1())));
					}
				}
				
				if (form.hasJobSearch()) {
					Join<JobItem, Job> job = jobItem.join(JobItem_.job);
					String likeParam = "%" + form.getJobNo() + "%";
					clauses.getExpressions().add(cb.like(job.get(Job_.jobno), likeParam));
				}
				if (form.hasInstrumentSearch() || form.hasModelSearch() || form.hasSubfamilySearch()
						|| form.hasMfrSearch() || form.hasCompanySearch() || form.hasContactSearch()
						|| form.hasAddressSearch() || form.hasSubdivSearch()) {
					// also require instrument join (all left joins, as
					// instrument links via job item or directly)
					Join<Certificate, InstCertLink> linkInstrument = root.join(Certificate_.instCertLinks,
							JoinType.LEFT);
					Join<InstCertLink, Instrument> certInstrument = linkInstrument.join(InstCertLink_.inst,
							JoinType.LEFT);
					Join<JobItem, Instrument> jiInstrument = jobItem.join(JobItem_.inst, JoinType.LEFT);
					// barcode = Plant ID (formerBarcode not included for query
					// simplicity/performance)
					if (((form.getBc() != null) && !form.getBc().trim().isEmpty())) {
						Predicate orBarcode = cb.disjunction();
						// If user enters non-integer, no action required
						// (checked in validator, invalid entries rejected)
						if (NumberTools.isAnInteger(form.getBc())) {
							Integer barcode = Integer.parseInt(form.getBc().trim());
							orBarcode.getExpressions().add(cb.equal(certInstrument.get(Instrument_.plantid), barcode));
							orBarcode.getExpressions().add(cb.equal(jiInstrument.get(Instrument_.plantid), barcode));
						}
						clauses.getExpressions().add(orBarcode);
					}
					// serial no
					if ((form.getSerialNo() != null) && !form.getSerialNo().trim().isEmpty()) {
						Predicate orSerial = cb.disjunction();
						String likeParam = "%" + form.getSerialNo().trim() + "%";
						orSerial.getExpressions().add(cb.like(certInstrument.get(Instrument_.serialno), likeParam));
						orSerial.getExpressions().add(cb.like(jiInstrument.get(Instrument_.serialno), likeParam));
						clauses.getExpressions().add(orSerial);
					}
					// plant no
					if ((form.getPlantNo() != null) && !form.getPlantNo().trim().isEmpty()) {
						Predicate orPlant = cb.disjunction();
						String likeParam = "%" + form.getPlantNo().trim() + "%";
						orPlant.getExpressions().add(cb.like(certInstrument.get(Instrument_.plantno), likeParam));
						orPlant.getExpressions().add(cb.like(jiInstrument.get(Instrument_.plantno), likeParam));
						clauses.getExpressions().add(orPlant);
					}
					if (form.hasModelSearch() || form.hasSubfamilySearch() || form.hasMfrSearch()) {
						Join<Instrument, InstrumentModel> jiModel = jiInstrument.join(Instrument_.model, JoinType.LEFT);
						Join<Instrument, InstrumentModel> certModel = certInstrument.join(Instrument_.model,
								JoinType.LEFT);

						if (form.hasModelSearch()) {
							Predicate orModel = cb.disjunction();
							// model (by id or name)
							if ((form.getModelid() != null) && (form.getModelid() != 0)) {
								orModel.getExpressions()
										.add(cb.equal(certModel.get(InstrumentModel_.modelid), form.getModelid()));
								orModel.getExpressions()
										.add(cb.equal(jiModel.get(InstrumentModel_.modelid), form.getModelid()));
							} else if ((form.getModel() != null) && !form.getModel().trim().isEmpty()) {
								String likeParam = "%" + form.getModel().trim() + "%";
								orModel.getExpressions().add(cb.like(certModel.get(InstrumentModel_.model), likeParam));
								orModel.getExpressions().add(cb.like(jiModel.get(InstrumentModel_.model), likeParam));
							}
							clauses.getExpressions().add(orModel);
						}
						if (form.hasSubfamilySearch()) {
							Join<InstrumentModel, Description> jiSubfamily = jiModel.join(InstrumentModel_.description,
									JoinType.LEFT);
							Join<InstrumentModel, Description> certSubfamily = certModel
									.join(InstrumentModel_.description, JoinType.LEFT);

							Predicate orSubfamily = cb.disjunction();
							// subfamily (by id or name)
							if ((form.getDescid() != null) && (form.getDescid() != 0)) {
								orSubfamily.getExpressions()
										.add(cb.equal(certSubfamily.get(Description_.id), form.getDescid()));
								orSubfamily.getExpressions()
										.add(cb.equal(jiSubfamily.get(Description_.id), form.getDescid()));
							} else if ((form.getDesc() != null) && !form.getDesc().trim().isEmpty()) {
								String likeParam = "%" + form.getDesc().trim() + "%";
								orSubfamily.getExpressions()
										.add(cb.like(certSubfamily.get(Description_.description), likeParam));
								orSubfamily.getExpressions()
										.add(cb.like(jiSubfamily.get(Description_.description), likeParam));
							}
							clauses.getExpressions().add(orSubfamily);
						}
						if (form.hasMfrSearch()) {
							Join<Instrument, Mfr> certInstMfr = certInstrument.join(Instrument_.mfr, JoinType.LEFT);
							Join<Instrument, Mfr> jiInstMfr = jiInstrument.join(Instrument_.mfr, JoinType.LEFT);
							Join<InstrumentModel, Mfr> certModelMfr = certModel.join(InstrumentModel_.mfr,
									JoinType.LEFT);
							Join<InstrumentModel, Mfr> jiModelMfr = jiModel.join(InstrumentModel_.mfr, JoinType.LEFT);

							Predicate orMfr = cb.disjunction();
							// mfr (by id or name)
							if ((form.getMfrid() != null) && (form.getMfrid() != 0)) {
								orMfr.getExpressions().add(cb.equal(certInstMfr.get(Mfr_.mfrid), form.getMfrid()));
								orMfr.getExpressions().add(cb.equal(jiInstMfr.get(Mfr_.mfrid), form.getMfrid()));
								orMfr.getExpressions().add(cb.equal(certModelMfr.get(Mfr_.mfrid), form.getMfrid()));
								orMfr.getExpressions().add(cb.equal(jiModelMfr.get(Mfr_.mfrid), form.getMfrid()));
							} else if ((form.getMfr() != null) && !form.getMfr().trim().isEmpty()) {
								String likeParam = "%" + form.getMfr().trim() + "%";
								orMfr.getExpressions().add(cb.like(certInstMfr.get(Mfr_.name), likeParam));
								orMfr.getExpressions().add(cb.like(jiInstMfr.get(Mfr_.name), likeParam));
								orMfr.getExpressions().add(cb.like(certModelMfr.get(Mfr_.name), likeParam));
								orMfr.getExpressions().add(cb.like(jiModelMfr.get(Mfr_.name), likeParam));
							}
							clauses.getExpressions().add(orMfr);
						}
					}
					// company
					if (form.getCoid() != null) {
						Join<Instrument, Company> certCompany = certInstrument.join(Instrument_.comp, JoinType.LEFT);
						Join<Instrument, Company> jiCompany = jiInstrument.join(Instrument_.comp, JoinType.LEFT);

						Predicate orCompany = cb.disjunction();
						orCompany.getExpressions().add(cb.equal(certCompany.get(Company_.coid), form.getCoid()));
						orCompany.getExpressions().add(cb.equal(jiCompany.get(Company_.coid), form.getCoid()));

						// For internal search only, include on-behalf-of client
						// company
						Join<JobItem, OnBehalfItem> onBehalf = jobItem.join(JobItem_.onBehalf, JoinType.LEFT);
						Join<OnBehalfItem, Company> oboCompany = onBehalf.join(OnBehalfItem_.company,
								JoinType.LEFT);
						orCompany.getExpressions().add(cb.equal(oboCompany.get(Company_.coid), form.getCoid()));
						clauses.getExpressions().add(orCompany);
					}
					// contact
					if (form.getPersonid() != null) {
						Join<Instrument, Contact> certContact = certInstrument.join(Instrument_.con, JoinType.LEFT);
						Join<Instrument, Contact> jiContact = jiInstrument.join(Instrument_.con, JoinType.LEFT);

						Predicate orContact = cb.disjunction();
						orContact.getExpressions()
								.add(cb.equal(certContact.get(Contact_.personid), form.getPersonid()));
						orContact.getExpressions().add(cb.equal(jiContact.get(Contact_.personid), form.getPersonid()));
						clauses.getExpressions().add(orContact);
					}
					if (form instanceof WebCertificateSearchForm) {
						WebCertificateSearchForm webForm = (WebCertificateSearchForm) form;
						if (form.hasAddressSearch() || form.hasSubdivSearch()) {
							Join<Instrument, Address> certAddress = certInstrument.join(Instrument_.add, JoinType.LEFT);
							Join<Instrument, Address> jiAddress = jiInstrument.join(Instrument_.add, JoinType.LEFT);

							// address id
							if (webForm.getAddrid() != null) {
								Predicate orAddress = cb.disjunction();
								orAddress.getExpressions()
										.add(cb.equal(certAddress.get(Address_.addrid), webForm.getAddrid()));
								orAddress.getExpressions()
										.add(cb.equal(jiAddress.get(Address_.addrid), webForm.getAddrid()));
								clauses.getExpressions().add(orAddress);
							}
							// subdiv id (search only needed in absence of
							// address)
							else if (webForm.getSubdivid() != null) {
								Join<Address, Subdiv> certSubdiv = certAddress.join(Address_.sub, JoinType.LEFT);
								Join<Address, Subdiv> jiSubdiv = jiAddress.join(Address_.sub, JoinType.LEFT);
								Predicate orSubdiv = cb.disjunction();
								orSubdiv.getExpressions()
										.add(cb.equal(certSubdiv.get(Subdiv_.subdivid), webForm.getSubdivid()));
								orSubdiv.getExpressions()
										.add(cb.equal(jiSubdiv.get(Subdiv_.subdivid), webForm.getSubdivid()));
								clauses.getExpressions().add(orSubdiv);
							}
						}
					}
				}
			}
			if (form instanceof WebCertificateSearchForm) {
				WebCertificateSearchForm webForm = (WebCertificateSearchForm) form;
				if (webForm.hasValidationSearch()) {
					Join<Certificate, CertificateValidation> validation = root.join(Certificate_.validations,
							JoinType.LEFT);
					// awaiting validation
					if (webForm.getUnAcceptedCertsOnly() != null && webForm.getUnAcceptedCertsOnly()) {
						clauses.getExpressions().add(validation.isNull());
					}
					// accept date
					if (webForm.getAcceptDate1() != null) {
						if (webForm.getAcceptDateBetween()) {
							clauses.getExpressions()
									.add(cb.between(validation.get(CertificateValidation_.datevalidated),
											webForm.getAcceptDate1(), webForm.getAcceptDate2()));
						} else {
							clauses.getExpressions().add(cb.equal(validation.get(CertificateValidation_.datevalidated),
									webForm.getAcceptDate1()));
						}
					}
				}
			}

			cq.where(clauses);
			cq.distinct(true);
			// TODO - refactor away this CertificateSearchWrapper (certDate used
			// for order) into a projection
			CompoundSelection<CertificateSearchWrapper> selection = cb.construct(CertificateSearchWrapper.class, root,
					root.get(Certificate_.certDate));

			// order results with most recent certs first
			List<javax.persistence.criteria.Order> order = new ArrayList<>();
			order.add(cb.desc(root.get(Certificate_.certDate)));

			return Triple.of(root, selection, order);
		});
	}
	
	@Override
	public void searchCertificatesProjectionJPA(CertificateSearchForm form, PagedResultSet<CertificateSearchWrapper> prs,
			boolean includeDeletedAndCancelledCertificates) {
		completePagedResultSet(prs, CertificateSearchWrapper.class, cb -> cq -> {
			Root<Certificate> root = cq.from(Certificate.class);

			Predicate clauses = cb.conjunction();

			// do not include deleted certificates
			if (!includeDeletedAndCancelledCertificates) {
				Predicate predicateForDeleted = cb.notEqual(root.get(Certificate_.status), CertStatusEnum.DELETED);
				Predicate predicateForCancelled = cb.notEqual(root.get(Certificate_.status), CertStatusEnum.CANCELLED);
				Predicate orPredicate = cb.or(predicateForCancelled, predicateForDeleted);
				clauses.getExpressions().add(orPredicate);
			}

			// third party company
			if (form.getTpId() != null && form.getTpId() != 0) {
				Join<Certificate, Subdiv> thirdSubdiv = root.join(Certificate_.thirdDiv);
				Join<Subdiv, Company> thirdCompany = thirdSubdiv.join(Subdiv_.comp);
				clauses.getExpressions().add(cb.equal(thirdCompany.get(Company_.coid), form.getTpId()));
			}
			// third party cert no
			if ((form.getTpCertNo() != null) && !form.getTpCertNo().trim().isEmpty()) {
				String likeParam = "%" + form.getTpCertNo().trim() + "%";
				clauses.getExpressions().add(cb.like(root.get(Certificate_.thirdCertNo), likeParam));
			}
			// cert no
			if ((form.getCertNo() != null) && !form.getCertNo().trim().isEmpty()) {
				String likeParam = "%" + form.getCertNo().trim() + "%";
				clauses.getExpressions().add(cb.like(root.get(Certificate_.certno), likeParam));
			}
			// status
			if (form.getStatus() != null) {
				clauses.getExpressions().add(cb.equal(root.get(Certificate_.status), form.getStatus()));
			}
			// cal type
			if (form.getCalTypeId() != null && form.getCalTypeId() != 0) {
				Join<Certificate, CalibrationType> calType = root.join(Certificate_.calType);
				clauses.getExpressions().add(cb.equal(calType.get(CalibrationType_.calTypeId), form.getCalTypeId()));
			}
			// issue date
			if (form.getIssueDate1() != null) {
				if (form.isIssueDateBetween()) {
					clauses.getExpressions().add(
							cb.between(root.get(Certificate_.certDate), form.getIssueDate1(), getEndDayDateTime(form.getIssueDate2())));
				} else {
					clauses.getExpressions().add(cb.between(root.get(Certificate_.certDate), form.getIssueDate1(), getEndDayDateTime(form.getIssueDate1())));
				}
			}
			// cal date
			if (form.getCalDate1() != null) {
				if (form.isCalDateBetween()) {
					clauses.getExpressions()
							.add(cb.between(root.get(Certificate_.calDate), form.getCalDate1(), getEndDayDateTime(form.getCalDate2())));
				} else {
					clauses.getExpressions().add(cb.between(root.get(Certificate_.calDate), form.getCalDate1(), getEndDayDateTime(form.getCalDate1())));
				}
			}
			// calverificationstatus
			if (form.getCalibrationVerificationStatus() != null) {
				clauses.getExpressions().add(cb.equal(root.get(Certificate_.calibrationVerificationStatus),
						form.getCalibrationVerificationStatus()));
			}
			
			if (form.hasJobSearch() || form.hasInstrumentSearch() || form.hasModelSearch() || form.hasSubfamilySearch()
					|| form.hasMfrSearch() || form.hasCompanySearch() || form.hasContactSearch()
					|| form.hasAddressSearch() || form.hasSubdivSearch() || form.hasJobitemSearch()) {
				// require job item join (all left joins, as instrument links
				// via job item or directly)
				Join<Certificate, CertLink> linkJobItem = root.join(Certificate_.links, JoinType.LEFT);
				Join<CertLink, JobItem> jobItem = linkJobItem.join(CertLink_.jobItem, JoinType.LEFT);
				Join<Certificate, InstCertLink> linkInstrument = root.join(Certificate_.instCertLinks,
						JoinType.LEFT);
				Join<InstCertLink, Instrument> certInstrument = linkInstrument.join(InstCertLink_.inst,
						JoinType.LEFT);
				// jobitem completion date
				if (form.getJobItemDateComplete1() != null) {
					if (form.isJobItemCompletionDateBetween()) {
						clauses.getExpressions()
								.add(cb.between(jobItem.get(JobItem_.dateComplete), form.getJobItemDateComplete1(), getEndDayDateTime(form.getJobItemDateComplete2())));
					} else {
						clauses.getExpressions().add(cb.between(jobItem.get(JobItem_.dateComplete), form.getJobItemDateComplete1(), getEndDayDateTime(form.getJobItemDateComplete1())));
					}
				}
				if (form.hasJobSearch()) {
					Join<JobItem, Job> job = jobItem.join(JobItem_.job);
					String likeParam = "%" + form.getJobNo() + "%";
					clauses.getExpressions().add(cb.like(job.get(Job_.jobno), likeParam));
				}
				if (form.hasInstrumentSearch() || form.hasModelSearch() || form.hasSubfamilySearch()
						|| form.hasMfrSearch() || form.hasCompanySearch() || form.hasContactSearch()
						|| form.hasAddressSearch() || form.hasSubdivSearch()) {
					// also require instrument join (all left joins, as
					// instrument links via job item or directly)
					
					Join<JobItem, Instrument> jiInstrument = jobItem.join(JobItem_.inst, JoinType.LEFT);
					// barcode = Plant ID (formerBarcode not included for query
					// simplicity/performance)
					if (((form.getBc() != null) && !form.getBc().trim().isEmpty())) {
						Predicate orBarcode = cb.disjunction();
						// If user enters non-integer, no action required
						// (checked in validator, invalid entries rejected)
						if (NumberTools.isAnInteger(form.getBc())) {
							Integer barcode = Integer.parseInt(form.getBc().trim());
							orBarcode.getExpressions().add(cb.equal(certInstrument.get(Instrument_.plantid), barcode));
							orBarcode.getExpressions().add(cb.equal(jiInstrument.get(Instrument_.plantid), barcode));
						}
						clauses.getExpressions().add(orBarcode);
					}
					// serial no
					if ((form.getSerialNo() != null) && !form.getSerialNo().trim().isEmpty()) {
						Predicate orSerial = cb.disjunction();
						String likeParam = "%" + form.getSerialNo().trim() + "%";
						orSerial.getExpressions().add(cb.like(certInstrument.get(Instrument_.serialno), likeParam));
						orSerial.getExpressions().add(cb.like(jiInstrument.get(Instrument_.serialno), likeParam));
						clauses.getExpressions().add(orSerial);
					}
					// plant no
					if ((form.getPlantNo() != null) && !form.getPlantNo().trim().isEmpty()) {
						Predicate orPlant = cb.disjunction();
						String likeParam = "%" + form.getPlantNo().trim() + "%";
						orPlant.getExpressions().add(cb.like(certInstrument.get(Instrument_.plantno), likeParam));
						orPlant.getExpressions().add(cb.like(jiInstrument.get(Instrument_.plantno), likeParam));
						clauses.getExpressions().add(orPlant);
					}
					if (form.hasModelSearch() || form.hasSubfamilySearch() || form.hasMfrSearch()) {
						Join<Instrument, InstrumentModel> jiModel = jiInstrument.join(Instrument_.model, JoinType.LEFT);
						Join<Instrument, InstrumentModel> certModel = certInstrument.join(Instrument_.model,
								JoinType.LEFT);

						if (form.hasModelSearch()) {
							Predicate orModel = cb.disjunction();
							// model (by id or name)
							if ((form.getModelid() != null) && (form.getModelid() != 0)) {
								orModel.getExpressions()
										.add(cb.equal(certModel.get(InstrumentModel_.modelid), form.getModelid()));
								orModel.getExpressions()
										.add(cb.equal(jiModel.get(InstrumentModel_.modelid), form.getModelid()));
							} else if ((form.getModel() != null) && !form.getModel().trim().isEmpty()) {
								String likeParam = "%" + form.getModel().trim() + "%";
								orModel.getExpressions().add(cb.like(certModel.get(InstrumentModel_.model), likeParam));
								orModel.getExpressions().add(cb.like(jiModel.get(InstrumentModel_.model), likeParam));
							}
							clauses.getExpressions().add(orModel);
						}
						if (form.hasSubfamilySearch()) {
							Join<InstrumentModel, Description> jiSubfamily = jiModel.join(InstrumentModel_.description,
									JoinType.LEFT);
							Join<InstrumentModel, Description> certSubfamily = certModel
									.join(InstrumentModel_.description, JoinType.LEFT);

							Predicate orSubfamily = cb.disjunction();
							// subfamily (by id or name)
							if ((form.getDescid() != null) && (form.getDescid() != 0)) {
								orSubfamily.getExpressions()
										.add(cb.equal(certSubfamily.get(Description_.id), form.getDescid()));
								orSubfamily.getExpressions()
										.add(cb.equal(jiSubfamily.get(Description_.id), form.getDescid()));
							} else if ((form.getDesc() != null) && !form.getDesc().trim().isEmpty()) {
								String likeParam = "%" + form.getDesc().trim() + "%";
								orSubfamily.getExpressions()
										.add(cb.like(certSubfamily.get(Description_.description), likeParam));
								orSubfamily.getExpressions()
										.add(cb.like(jiSubfamily.get(Description_.description), likeParam));
							}
							clauses.getExpressions().add(orSubfamily);
						}
						if (form.hasMfrSearch()) {
							Join<Instrument, Mfr> certInstMfr = certInstrument.join(Instrument_.mfr, JoinType.LEFT);
							Join<Instrument, Mfr> jiInstMfr = jiInstrument.join(Instrument_.mfr, JoinType.LEFT);
							Join<InstrumentModel, Mfr> certModelMfr = certModel.join(InstrumentModel_.mfr,
									JoinType.LEFT);
							Join<InstrumentModel, Mfr> jiModelMfr = jiModel.join(InstrumentModel_.mfr, JoinType.LEFT);

							Predicate orMfr = cb.disjunction();
							// mfr (by id or name)
							if ((form.getMfrid() != null) && (form.getMfrid() != 0)) {
								orMfr.getExpressions().add(cb.equal(certInstMfr.get(Mfr_.mfrid), form.getMfrid()));
								orMfr.getExpressions().add(cb.equal(jiInstMfr.get(Mfr_.mfrid), form.getMfrid()));
								orMfr.getExpressions().add(cb.equal(certModelMfr.get(Mfr_.mfrid), form.getMfrid()));
								orMfr.getExpressions().add(cb.equal(jiModelMfr.get(Mfr_.mfrid), form.getMfrid()));
							} else if ((form.getMfr() != null) && !form.getMfr().trim().isEmpty()) {
								String likeParam = "%" + form.getMfr().trim() + "%";
								orMfr.getExpressions().add(cb.like(certInstMfr.get(Mfr_.name), likeParam));
								orMfr.getExpressions().add(cb.like(jiInstMfr.get(Mfr_.name), likeParam));
								orMfr.getExpressions().add(cb.like(certModelMfr.get(Mfr_.name), likeParam));
								orMfr.getExpressions().add(cb.like(jiModelMfr.get(Mfr_.name), likeParam));
							}
							clauses.getExpressions().add(orMfr);
						}
					}
					// company
					if (form.getCoid() != null) {
						Join<Instrument, Company> certCompany = certInstrument.join(Instrument_.comp, JoinType.LEFT);
						Join<Instrument, Company> jiCompany = jiInstrument.join(Instrument_.comp, JoinType.LEFT);

						Predicate orCompany = cb.disjunction();
						orCompany.getExpressions().add(cb.equal(certCompany.get(Company_.coid), form.getCoid()));
						orCompany.getExpressions().add(cb.equal(jiCompany.get(Company_.coid), form.getCoid()));

						// For internal search only, include on-behalf-of client
						// company
						Join<JobItem, OnBehalfItem> onBehalf = jobItem.join(JobItem_.onBehalf, JoinType.LEFT);
						Join<OnBehalfItem, Company> oboCompany = onBehalf.join(OnBehalfItem_.company,
								JoinType.LEFT);
						orCompany.getExpressions().add(cb.equal(oboCompany.get(Company_.coid), form.getCoid()));
						clauses.getExpressions().add(orCompany);
					}
					// contact
					if (form.getPersonid() != null) {
						Join<Instrument, Contact> certContact = certInstrument.join(Instrument_.con, JoinType.LEFT);
						Join<Instrument, Contact> jiContact = jiInstrument.join(Instrument_.con, JoinType.LEFT);

						Predicate orContact = cb.disjunction();
						orContact.getExpressions()
								.add(cb.equal(certContact.get(Contact_.personid), form.getPersonid()));
						orContact.getExpressions().add(cb.equal(jiContact.get(Contact_.personid), form.getPersonid()));
						clauses.getExpressions().add(orContact);
					}
					if (form instanceof WebCertificateSearchForm) {
						WebCertificateSearchForm webForm = (WebCertificateSearchForm) form;
						if (form.hasAddressSearch() || form.hasSubdivSearch()) {
							Join<Instrument, Address> certAddress = certInstrument.join(Instrument_.add, JoinType.LEFT);
							Join<Instrument, Address> jiAddress = jiInstrument.join(Instrument_.add, JoinType.LEFT);

							// address id
							if (webForm.getAddrid() != null) {
								Predicate orAddress = cb.disjunction();
								orAddress.getExpressions()
										.add(cb.equal(certAddress.get(Address_.addrid), webForm.getAddrid()));
								orAddress.getExpressions()
										.add(cb.equal(jiAddress.get(Address_.addrid), webForm.getAddrid()));
								clauses.getExpressions().add(orAddress);
							}
							// subdiv id (search only needed in absence of
							// address)
							else if (webForm.getSubdivid() != null) {
								Join<Address, Subdiv> certSubdiv = certAddress.join(Address_.sub, JoinType.LEFT);
								Join<Address, Subdiv> jiSubdiv = jiAddress.join(Address_.sub, JoinType.LEFT);
								Predicate orSubdiv = cb.disjunction();
								orSubdiv.getExpressions()
										.add(cb.equal(certSubdiv.get(Subdiv_.subdivid), webForm.getSubdivid()));
								orSubdiv.getExpressions()
										.add(cb.equal(jiSubdiv.get(Subdiv_.subdivid), webForm.getSubdivid()));
								clauses.getExpressions().add(orSubdiv);
							}
						}
					}
				}
			}
			
			Join<Certificate, CertificateValidation> validation = root.join(Certificate_.validations,
					JoinType.LEFT);
			
			Subquery<Integer> validationSubQuery = cq.subquery(Integer.class);
			Root<CertificateValidation> sqValidation = validationSubQuery.from(CertificateValidation.class);
			validationSubQuery.where(cb.equal(sqValidation.get(CertificateValidation_.cert), root));
			validationSubQuery.select(cb.max(sqValidation.get(CertificateValidation_.id)));
			
			clauses.getExpressions().add(cb .or(cb.isNull(validation), 
					cb.equal(validation.get(CertificateValidation_.id), validationSubQuery.getSelection())));
			
			if (form instanceof WebCertificateSearchForm) {
				WebCertificateSearchForm webForm = (WebCertificateSearchForm) form;
				if (webForm.hasValidationSearch()) {
					
					// awaiting validation
					if (webForm.getUnAcceptedCertsOnly() != null && webForm.getUnAcceptedCertsOnly()) {
						Predicate orClauses = cb.disjunction();
						orClauses.getExpressions().add(validation.isNull());
						orClauses.getExpressions().add(cb.equal(validation.get(CertificateValidation_.status), CertificateValidationStatus.PENDING));

						Predicate signedPredicate = cb.equal(root.get(Certificate_.status), CertStatusEnum.SIGNED);
						Predicate notSupportedPredicate = cb.equal(root.get(Certificate_.status), CertStatusEnum.SIGNING_NOT_SUPPORTED);
						clauses.getExpressions().add(cb.or(signedPredicate, notSupportedPredicate));

						clauses.getExpressions().add(orClauses);
					}
					// accept date
					if (webForm.getAcceptDate1() != null) {
						if (webForm.getAcceptDateBetween()) {
							clauses.getExpressions()
									.add(cb.between(validation.get(CertificateValidation_.datevalidated),
											webForm.getAcceptDate1(), webForm.getAcceptDate2()));
						} else {
							clauses.getExpressions().add(cb.equal(validation.get(CertificateValidation_.datevalidated),
									webForm.getAcceptDate1()));
						}
					}
				}
			}
			
			cq.where(clauses);
			cq.distinct(true);
			// TODO - refactor away this CertificateSearchWrapper (certDate used
			// for order) into a projection
		
			CompoundSelection<CertificateSearchWrapper> selection = cb.construct(CertificateSearchWrapper.class, root.get(Certificate_.certid),
					root.get(Certificate_.certno), root.get(Certificate_.calDate), root.get(Certificate_.certDate),	root.get(Certificate_.thirdCertNo),
					root.get(Certificate_.type), root.get(Certificate_.calibrationVerificationStatus), root.get(Certificate_.status),
					root.get(Certificate_.deviation), validation.get(CertificateValidation_.datevalidated),
					validation.get(CertificateValidation_.status), validation.get(CertificateValidation_.note), 
					validation.get(CertificateValidation_.id), root.get(Certificate_.certClass));

			// order results with most recent certs first
			List<javax.persistence.criteria.Order> order = new ArrayList<>();
			order.add(cb.desc(root.get(Certificate_.certDate)));

 			return Triple.of(root, selection, order);
		});
	}
	
	private Date getEndDayDateTime(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.SECOND, 86399);
		return c.getTime();
	}
	
	@Override
	public List<WebCertificateJobItemLinkProjectionDTO> searchCertificateJobItemLinksProjectionJPA(List<Integer> certificateIds, Locale locale) {
		return this.getResultList(cb -> {
			val cq = cb.createQuery(WebCertificateJobItemLinkProjectionDTO.class);
			val root = cq.from(JobItem.class);
			val instJoin = root.join(JobItem_.inst);
			val instCompJoin = instJoin.join(Instrument_.instrumentComplementaryFields, JoinType.LEFT);
			val instModelJoin = instJoin.join(Instrument_.model, JoinType.LEFT);
			val instModelDescriptionJoin = instModelJoin.join(InstrumentModel_.description, JoinType.LEFT);
			val instModelNameTranslationJoin = instModelJoin.join(InstrumentModel_.nameTranslations, JoinType.LEFT);
			val instMfr = instJoin.join(Instrument_.mfr, JoinType.LEFT);	
			val instContact = instJoin.join(Instrument_.con, JoinType.LEFT);
			val instAddress = instJoin.join(Instrument_.add, JoinType.LEFT);
			val instSubdivContact = instContact.join(Contact_.sub, JoinType.LEFT);
			val instLastCalJoin = instJoin.join(Instrument_.lastCal, JoinType.LEFT);
			val jobJoin = root.join(JobItem_.job);
			val jiCertLinksJoin = root.join(JobItem_.certLinks, JoinType.LEFT);
			val certJoin = jiCertLinksJoin.join(CertLink_.cert, JoinType.LEFT);

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(certJoin.get(Certificate_.certid).in(certificateIds));
			clauses.getExpressions().add(cb.equal(instModelNameTranslationJoin.get(Translation_.locale), locale));

            CompoundSelection<WebCertificateJobItemLinkProjectionDTO> jobitemLinkSelection = cb.construct(WebCertificateJobItemLinkProjectionDTO.class,
                certJoin.get(Certificate_.certid), instJoin.get(Instrument_.plantid), instJoin.get(Instrument_.plantno), instJoin.get(Instrument_.serialno), instCompJoin.
                    get(InstrumentComplementaryField_.customerAcceptanceCriteria), instJoin.get(Instrument_.scrapped),
                instJoin.get(Instrument_.customerDescription), instJoin.get(Instrument_.modelname), instModelNameTranslationJoin.get(Translation_.translation),
                instModelDescriptionJoin.get(Description_.typology), instModelJoin.get(InstrumentModel_.modelMfrType),
                instMfr.get(Mfr_.name), instMfr.get(Mfr_.genericMfr), instJoin.get(Instrument_.status), root.get(JobItem_.itemNo),
                jobJoin.get(Job_.jobid), jobJoin.get(Job_.jobno), instJoin.get(Instrument_.addedOn), instJoin.get(Instrument_.nextCalDueDate),
                instLastCalJoin.get(Calibration_.completeTime), instContact.get(Contact_.personid), instAddress.get(Address_.addrid),
                instSubdivContact.get(Subdiv_.subdivid), instJoin.get(Instrument_.calFrequency), instJoin.get(Instrument_.calFrequencyUnit));
			cq.select(jobitemLinkSelection);

			cq.where(clauses);
			cq.distinct(true);
			return cq;
		});
	}
	
	@Override
	public List<WebCertificateInstrumentLinkProjectionDTO> searchCertificateInstrumentLinksProjectionJPA(List<Integer> certificateIds, Locale locale) {
		return this.getResultList(cb -> {
			val cq = cb.createQuery(WebCertificateInstrumentLinkProjectionDTO.class);
			val root = cq.from(Instrument.class);
			val instCompJoin = root.join(Instrument_.instrumentComplementaryFields, JoinType.LEFT);
			val instModelJoin = root.join(Instrument_.model, JoinType.LEFT);
			val instModelDescriptionJoin = instModelJoin.join(InstrumentModel_.description, JoinType.LEFT);
			val instModelNameTranslationJoin = instModelJoin.join(InstrumentModel_.nameTranslations, JoinType.LEFT);
			val instMfr = root.join(Instrument_.mfr, JoinType.LEFT);
			val instLastCalJoin = root.join(Instrument_.lastCal, JoinType.LEFT);
			val instCertLinkJoin = root.join(Instrument_.instCertLinks);
			val instCertJoin = instCertLinkJoin.join(InstCertLink_.cert);
			val instContact = root.join(Instrument_.con, JoinType.LEFT);
			val instAddress = root.join(Instrument_.add, JoinType.LEFT);
			val instSubdivContact = instContact.join(Contact_.sub, JoinType.LEFT);

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(instCertJoin.get(Certificate_.certid).in(certificateIds));
			clauses.getExpressions().add(cb.equal(instModelNameTranslationJoin.get(Translation_.locale), locale));

            CompoundSelection<WebCertificateInstrumentLinkProjectionDTO> instrumentLinkSelection = cb.construct(WebCertificateInstrumentLinkProjectionDTO.class,
                instCertJoin.get(Certificate_.certid), root.get(Instrument_.plantid), root.get(Instrument_.plantno), root.get(Instrument_.serialno),
                instCompJoin.get(InstrumentComplementaryField_.customerAcceptanceCriteria), root.get(Instrument_.scrapped),
                root.get(Instrument_.customerDescription), root.get(Instrument_.modelname), instModelNameTranslationJoin.get(Translation_.translation),
                instModelDescriptionJoin.get(Description_.typology), instModelJoin.get(InstrumentModel_.modelMfrType),
                instMfr.get(Mfr_.name), instMfr.get(Mfr_.genericMfr), root.get(Instrument_.status), root.get(Instrument_.addedOn),
                root.get(Instrument_.nextCalDueDate), instLastCalJoin.get(Calibration_.completeTime), instContact.get(Contact_.personid),
                instAddress.get(Address_.addrid), instSubdivContact.get(Subdiv_.subdivid),
                root.get(Instrument_.calFrequency), root.get(Instrument_.calFrequencyUnit)
            );
			cq.select(instrumentLinkSelection);
			
			cq.where(clauses);
			cq.distinct(true);
			return cq;
		});
	}

	@Override
	public void signCertificate(Certificate cert) {
		// This method is required for the hook to sign certificates
	}

	@Override
	public PagedResultSet<Certificate> getCertificatesByRelatedInstrument(Integer plantid, Integer resultsPerPage,
			Integer currentPage) {

		PagedResultSet<Certificate> prs = new PagedResultSet<>(resultsPerPage, currentPage);
		super.completePagedResultSet(prs, Certificate.class, cb -> cq -> {
			Root<Certificate> root = cq.from(Certificate.class);
			Join<Certificate, InstCertLink> instCertLink = root.join(Certificate_.instCertLinks, JoinType.LEFT);
			Join<InstCertLink, Instrument> certInstrument = instCertLink.join(InstCertLink_.inst, JoinType.LEFT);
			Join<Certificate, CertLink> certLink = root.join(Certificate_.links, JoinType.LEFT);
			Join<CertLink, JobItem> jobItem = certLink.join(CertLink_.jobItem, JoinType.LEFT);
			Join<JobItem, Instrument> jobItemInstrument = jobItem.join(JobItem_.inst, JoinType.LEFT);

			Predicate ji = cb.equal(jobItemInstrument.get(Instrument_.plantid), plantid);
			Predicate ci = cb.equal(certInstrument.get(Instrument_.plantid), plantid);

			cq.where(cb.or(ji, ci));

			List<Order> order = new ArrayList<>();
			order.add(cb.desc(root.get(Certificate_.certDate)));
			order.add(cb.desc(root.get(Certificate_.certid)));
			cq.distinct(true);

			return Triple.of(root.get(Certificate_.certid), null, order);
		});
		return prs;
	}

	@Override
	public List<Certificate> getCertificateListByJobitemId(Integer jobitemid) {

		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Certificate> query = builder.createQuery(Certificate.class);
		Root<CertLink> certLinkRoot = query.from(CertLink.class);
		Join<CertLink, JobItem> jobItemJoin = certLinkRoot.join(CertLink_.jobItem);
		Join<CertLink, Certificate> certificateJoin = certLinkRoot.join(CertLink_.cert);

		Predicate conjunction = builder.conjunction();
		conjunction.getExpressions().add(builder.equal(jobItemJoin.get(JobItem_.jobItemId), jobitemid));

		query.where(conjunction);

		query.select(certificateJoin);

		return getEntityManager().createQuery(query).getResultList();
	}

	public Certificate getMostRecentCertificateForJobItem(Integer jobItemId) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Certificate> query = builder.createQuery(Certificate.class);
		Root<CertLink> certLinkRoot = query.from(CertLink.class);
		Join<CertLink, JobItem> jobItemJoin = certLinkRoot.join(CertLink_.jobItem);
		Join<CertLink, Certificate> certificateJoin = certLinkRoot.join(CertLink_.cert);

		Predicate conjunction = builder.conjunction();
		conjunction.getExpressions().add(builder.equal(jobItemJoin.get(JobItem_.jobItemId), jobItemId));

		query.where(conjunction);

		query.select(certificateJoin);
		query.orderBy(builder.desc(certificateJoin.get(Certificate_.certid)));

		return getEntityManager().createQuery(query).setMaxResults(1).getResultList().size() > 0
				? getEntityManager().createQuery(query).setMaxResults(1).getResultList().get(0)
				: null;
	}

	@Override
	public List<Certificate> getJobCertsForInstrument(Instrument instrument) {

		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Certificate> query = builder.createQuery(Certificate.class);
		Root<CertLink> certLinkRoot = query.from(CertLink.class);
		Join<CertLink, JobItem> jobItemJoin = certLinkRoot.join(CertLink_.jobItem);
		Join<CertLink, Certificate> certificateJoin = certLinkRoot.join(CertLink_.cert);

		Predicate conjunction = builder.conjunction();
		conjunction.getExpressions().add(builder.equal(jobItemJoin.get(JobItem_.inst), instrument));

		query.where(conjunction);

		query.select(certificateJoin);

		return getEntityManager().createQuery(query).getResultList();
	}

	@Override
	public List<Certificate> getNonJobCertsForInstrument(Instrument instrument) {

		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Certificate> query = builder.createQuery(Certificate.class);
		Root<InstCertLink> instCertLinkRoot = query.from(InstCertLink.class);
		Join<InstCertLink, Certificate> certificateJoin = instCertLinkRoot.join(InstCertLink_.cert);

		Predicate conjunction = builder.conjunction();
		conjunction.getExpressions().add(builder.equal(instCertLinkRoot.get(InstCertLink_.inst), instrument));

		query.where(conjunction);

		query.select(certificateJoin);

		return getEntityManager().createQuery(query).getResultList();
	}

	@Override
	public Certificate getByThirdCertNo(String thirdCertificateNumber) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Certificate> query = cb.createQuery(Certificate.class);
		Root<Certificate> root = query.from(Certificate.class);

		query.select(root);
		query.where(cb.equal(root.get(Certificate_.thirdCertNo), thirdCertificateNumber));

		// there should be just one or none, because of the unique constraint
		return getEntityManager().createQuery(query).getResultList().stream().findAny().orElse(null);
	}

	@Override
	public List<Certificate> getByThirdCertNoAndPlantId(String thirdCertificateNumber, Integer plantid) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

		CriteriaQuery<Certificate> query = cb.createQuery(Certificate.class);
		Root<Certificate> certificateRoot = query.from(Certificate.class);
		Join<Certificate, CertLink> certLinkJoin = certificateRoot.join(Certificate_.links, JoinType.LEFT);
		Join<CertLink, JobItem> jobItemJoin = certLinkJoin.join(CertLink_.jobItem, JoinType.LEFT);
		Join<JobItem, Instrument> instrumentJIJoin = jobItemJoin.join(JobItem_.inst, JoinType.LEFT);

		Join<Certificate, InstCertLink> instCertLinkJoin = certificateRoot.join(Certificate_.instCertLinks,
				JoinType.LEFT);
		Join<InstCertLink, Instrument> instrumentJoin = instCertLinkJoin.join(InstCertLink_.inst, JoinType.LEFT);

		Predicate conjunction = cb.conjunction();

		Predicate ji = cb.equal(instrumentJIJoin.get(Instrument_.plantid), plantid);
		Predicate i = cb.equal(instrumentJoin.get(Instrument_.plantid), plantid);

		conjunction.getExpressions().add(cb.or(ji, i));
		conjunction.getExpressions()
				.add(cb.equal(certificateRoot.get(Certificate_.thirdCertNo), thirdCertificateNumber));

		query.select(certificateRoot);
		query.where(conjunction);

		return getEntityManager().createQuery(query).getResultList();
	}

	@Override
	public List<Certificate> getByThirdCertNoAndJobId(String thirdCertificateNumber, int jobid) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

		CriteriaQuery<Certificate> query = cb.createQuery(Certificate.class);
		Root<Certificate> certificateRoot = query.from(Certificate.class);
		Join<Certificate, CertLink> certLinkJoin = certificateRoot.join(Certificate_.links, JoinType.LEFT);
		Join<CertLink, JobItem> jobItemJoin = certLinkJoin.join(CertLink_.jobItem, JoinType.LEFT);
		Join<JobItem, Job> jobJoin = jobItemJoin.join(JobItem_.job, JoinType.LEFT);

		Predicate conjunction = cb.conjunction();

		conjunction.getExpressions().add(cb.equal(jobJoin.get(Job_.jobid), jobid));
		conjunction.getExpressions()
				.add(cb.equal(certificateRoot.get(Certificate_.thirdCertNo), thirdCertificateNumber));

		query.select(certificateRoot);
		query.where(conjunction);

		return getEntityManager().createQuery(query).getResultList();
	}

	/**
	 * Query is intentionally not using distinct(); e.g. same cert on two job items
	 * should give two DTO results, one for each job item.
	 */
	@Override
	public List<CertificateProjectionDTO> getCertificatesForJobItemIds(Collection<Integer> jobItemIds) {
		return getResultList(cb -> {
			CriteriaQuery<CertificateProjectionDTO> cq = cb.createQuery(CertificateProjectionDTO.class);
			Root<Certificate> root = cq.from(Certificate.class);
			Join<Certificate, CertLink> certLink = root.join(Certificate_.links, JoinType.LEFT);
			Join<CertLink, JobItem> jobItem = certLink.join(CertLink_.jobItem, JoinType.INNER);
			cq.where(jobItem.get(JobItem_.jobItemId).in(jobItemIds));

			cq.select(cb.construct(CertificateProjectionDTO.class, 
					root.get(Certificate_.certid), cb.nullLiteral(Integer.class), 
					jobItem.get(JobItem_.jobItemId), root.get(Certificate_.certno),
					root.get(Certificate_.calDate), root.get(Certificate_.certDate), 
					root.get(Certificate_.thirdCertNo), root.get(Certificate_.type), 
					root.get(Certificate_.calibrationVerificationStatus), root.get(Certificate_.status), 
					root.get(Certificate_.optimization), root.get(Certificate_.restriction),
					root.get(Certificate_.adjustment), root.get(Certificate_.repair)));

			return cq;
		});
	}
	
	public Integer getSubdivIdFromCalibration(int certid) {
		return getFirstResult(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<Certificate> root = cq.from(Certificate.class);
			Join<Certificate, Calibration> cal = root.join(Certificate_.cal, JoinType.INNER);
			Join<Calibration, Subdiv> subdiv = cal.join(Calibration_.ORGANISATION, JoinType.INNER);

			cq.where(cb.equal(root.get(Certificate_.certid), certid));
			cq.select(subdiv.get(Subdiv_.subdivid));

			return cq;
		}).orElse(null);
	}

    @Override
    public List<CertificateForInstrumentTooltipDto> findCertificatesForInstrumentTooltipByPlantId(Integer plantId) {
        return getResultList(cb -> {
        	val cq = cb.createQuery(CertificateForInstrumentTooltipDto.class);
        	val jobItems = cq.from(JobItem.class);
        	val certificateLinks = jobItems.join(JobItem_.certLinks,JoinType.LEFT);
        	val certificate = certificateLinks.join(CertLink_.cert,JoinType.LEFT);
        	val job = jobItems.join(JobItem_.job);
        	cq.where(cb.equal(jobItems.get(JobItem_.inst),plantId));
        	cq.select(cb.construct(CertificateForInstrumentTooltipDto.class,
					certificate.get(Certificate_.certno),
					job.get(Job_.jobno),
					job.get(Job_.jobid),
					jobItems.get(JobItem_.itemNo),
					jobItems.get(JobItem_.jobItemId),
					certificate.get(Certificate_.calDate),
					certificate.get(Certificate_.certDate)
			));
        	return cq;

		});
    }
}
