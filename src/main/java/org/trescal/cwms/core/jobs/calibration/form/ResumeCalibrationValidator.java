package org.trescal.cwms.core.jobs.calibration.form;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;

import javax.validation.constraints.NotNull;

@Component
public class ResumeCalibrationValidator implements Validator {
    @Autowired
    private CalibrationService calibrationService;
    @Autowired
    private CapabilityAuthorizationService procAccServ;

    public static String CODE_NO_CALIBRATION = "error.resumecalibration.nocalibration";
    public static String MESSAGE_NO_CALIBRATION = "No calibration found to resume with status of 'On Hold'. Please override status manually and create a new calibration.";

    public static String CODE_NOT_AUTHORISED = "error.resumecalibration.notauthorised";
    public static String MESSAGE_NOT_AUTHORISED = "Sorry, you are not authorised to resume procedure {0}.";

    @Override
    public boolean supports(@NotNull Class<?> clazz) {
        return ResumeCalibrationForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(@NotNull Object object, @NotNull Errors errors) {
        ResumeCalibrationForm form = (ResumeCalibrationForm) object;

        if (form.getCalibrationId() == null) {
            errors.reject(CODE_NO_CALIBRATION, MESSAGE_NO_CALIBRATION);
        } else {
            Calibration cal = this.calibrationService.findCalibration(form.getCalibrationId());
            // check accredited to perform the calibration
            val accStat = this.procAccServ.authorizedFor(cal.getCapability().getId(), cal.getCalType().getCalTypeId(), form.getContactId()).isLeft();

            if (accStat) {
                errors.reject(CODE_NOT_AUTHORISED, new Object[]{cal.getCapability().getReference()}, MESSAGE_NOT_AUTHORISED);
            }
		}
		

	}

}
