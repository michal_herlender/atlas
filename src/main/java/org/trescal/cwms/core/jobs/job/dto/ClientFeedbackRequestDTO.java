package org.trescal.cwms.core.jobs.job.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @AllArgsConstructor @NoArgsConstructor
public class ClientFeedbackRequestDTO
{
	private int jobItemId;
	private boolean confirmCalPoints;
	private boolean confirmCalType;
	private boolean confirmReturn;
	private boolean confirmTurnaround;
	private boolean confirmAdjustment;
	private String feedbackComment;

}