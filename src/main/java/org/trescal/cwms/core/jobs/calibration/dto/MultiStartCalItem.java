package org.trescal.cwms.core.jobs.calibration.dto;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public class MultiStartCalItem
{
	private JobItem ji;

	public void setJi(JobItem ji)
	{
		this.ji = ji;
	}

	public JobItem getJi()
	{
		return this.ji;
	}
}