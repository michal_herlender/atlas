package org.trescal.cwms.core.jobs.jobitem.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public  class  CustomItem {
	private String plantNo;
	private String serialNo;
}
