package org.trescal.cwms.core.jobs.job.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.jobs.job.dto.JobSearchParameterWrapper;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.time.LocalDate;
import java.util.Collection;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JobSearchForm {
	private String orgId;
	private Boolean active;
	private LocalDate bookedInDate1;
	private LocalDate bookedInDate2;
	private boolean bookedInDateBetween;
	private String clientRef;
	private String coid;
	private LocalDate completedDate1;
	private LocalDate completedDate2;
	private boolean completedDateBetween;
	private String desc;
	private Integer descid;
	private String forced;
	private Boolean highlight;
	private int jobItemCount;
	private String jobno;
	private Collection<JobProjectionDTO> jobs;
	private JobType jobType;
	private String mfr;
	private Integer mfrid;
	private String model;
	private Integer modelid;
	private int pageNo;
	private String personid;
	private String plantid;
	private String plantno;
	private Integer procId;
	private String purOrder;
	private int resultsPerPage;
	private int resultsTotal;
	private String returnTo;
	// results data
	private PagedResultSet<JobProjectionDTO> rs;
	private JobSearchParameterWrapper searchCriteria;
	private String serialno;
	private Integer subdivid;
}
