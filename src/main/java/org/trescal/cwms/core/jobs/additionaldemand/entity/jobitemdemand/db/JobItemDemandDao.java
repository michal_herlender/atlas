package org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemand.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemand.JobItemDemand;

public interface JobItemDemandDao extends BaseDao<JobItemDemand, Integer> {
}