package org.trescal.cwms.core.jobs.jobitem.entity.accessory.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.Accessory;

public interface AccessoryDao extends BaseDao<Accessory, Integer> {}