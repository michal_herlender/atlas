package org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.dto.ExpenseItemDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.ExpenseItemOnClientPurchaseOrderDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.InvoiceableItemsByCompanyDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.db.AbstractJobItemDao;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.db.AbstractJobItemServiceImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;

import java.time.LocalDate;
import java.util.List;

@Service
public class JobExpenseItemServiceImpl extends AbstractJobItemServiceImpl<JobExpenseItem>
		implements JobExpenseItemService {

	@Autowired
	private JobExpenseItemDao jobExpenseItemDao;

	@Override
	public JobExpenseItem copy(JobExpenseItem oldExpenseItem) {
		JobExpenseItem newExpenseItem = new JobExpenseItem();
		newExpenseItem.setClientRef(oldExpenseItem.getClientRef());
		newExpenseItem.setComment(oldExpenseItem.getComment());
		newExpenseItem.setDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		newExpenseItem.setInvoiceable(oldExpenseItem.getInvoiceable());
		newExpenseItem.setModel(oldExpenseItem.getModel());
		newExpenseItem.setQuantity(oldExpenseItem.getQuantity());
		newExpenseItem.setServiceType(oldExpenseItem.getServiceType());
		newExpenseItem.setSinglePrice(oldExpenseItem.getSinglePrice());
		return newExpenseItem;
	}

	@Override
	public void copyItemsToNewJob(List<Integer> expenseItemIds, Job newJob) {
		Integer itemNo = 1;
		if (expenseItemIds != null)
			for (Integer id : expenseItemIds) {
				JobExpenseItem oldExpenseItem = this.get(id);
				JobExpenseItem newExpenseItem = this.copy(oldExpenseItem);
				newExpenseItem.setItemNo(itemNo++);
				newExpenseItem.setJob(newJob);
				this.save(newExpenseItem);
			}
	}

	@Override
	protected AbstractJobItemDao<JobExpenseItem> getBaseDao() {
		return jobExpenseItemDao;
	}

	@Override
	public List<InvoiceableItemsByCompanyDTO> getInvoiceable(Integer allocatedCompanyId, Integer allocatedSubdivId) {
		return jobExpenseItemDao.getInvoiceable(allocatedCompanyId, allocatedSubdivId);
	}

	@Override
	public List<JobExpenseItem> getInvoiceable(Company clientCompany, Integer allocatedCompanyId,
			Integer allocatedSubdivId, String completeJob) {
		return jobExpenseItemDao.getInvoiceable(clientCompany, allocatedCompanyId, allocatedSubdivId, completeJob);
	}

	@Override
	public List<ExpenseItemOnClientPurchaseOrderDTO> getExpenseItemsOnClientBPO(Integer poId, Integer jobId) {
		return jobExpenseItemDao.getExpenseItemsOnClientBPO(poId, jobId, LocaleContextHolder.getLocale());
	}

	@Override
	public List<ExpenseItemOnClientPurchaseOrderDTO> getExpenseItemsOnClientPO(Integer poId, Integer jobId) {
		return jobExpenseItemDao.getExpenseItemsOnClientPO(poId, jobId, LocaleContextHolder.getLocale());
	}

	@Override
	public List<ExpenseItemDTO> getAvailableForInvoice(Integer invoiceId) {
		return jobExpenseItemDao.getAvailableForInvoice(invoiceId, LocaleContextHolder.getLocale());
	}
}