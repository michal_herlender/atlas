package org.trescal.cwms.core.jobs.jobitemhistory.db;

import java.util.Date;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.jobitemhistory.JobItemHistory;

public interface JobItemHistoryService extends BaseService<JobItemHistory, Integer>
{
	JobItemHistory getJobItemHistoryByChangeDate(Date changeDate);
}