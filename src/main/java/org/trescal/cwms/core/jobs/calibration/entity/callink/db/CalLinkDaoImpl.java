package org.trescal.cwms.core.jobs.calibration.entity.callink.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;

@Repository("CalLinkDao")
public class CalLinkDaoImpl extends BaseDaoImpl<CalLink, Integer> implements CalLinkDao {
	
	@Override
	protected Class<CalLink> getEntity() {
		return CalLink.class;
	}
}