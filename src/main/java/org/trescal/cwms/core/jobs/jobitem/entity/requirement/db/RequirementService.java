package org.trescal.cwms.core.jobs.jobitem.entity.requirement.db;

import java.util.List;
import java.util.Set;

import javax.sound.midi.Instrument;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.requirement.Requirement;

public interface RequirementService
{
	void deleteRequirement(Requirement r);

	void deleteRequirementById(String id);

	Requirement findRequirement(Integer id);

	/**
	 * Returns a {@link Set} containing all of the {@link Requirement} for the
	 * given jobitem id.
	 * 
	 * @param jobitemid the jobitem id.
	 * @return {@link Set} of matching {@link Requirement} entities.
	 */
	Set<Requirement> findRequirementByJobItem(int jobitemid);

	List<Requirement> getAllRequirements();

	/**
	 * Inserts the given {@link Requirement} and attaches it to the given
	 * {@link JobItem}.
	 * 
	 * @param requirement the requirement, not null.
	 * @param jobitemid the id of the {@link JobItem} this requirement will
	 *        apply to, not null.
	 * @param saveComment if true then this requirement will be saved as a
	 *        {@link JobItemRequirementComment}.
	 * @param saveInstrumentReq instrumentupdate id of checkbox that if checked
	 *        will set the requirement as the new instrument requirement.
	 * @param plantid the id of the {@link Instrument}.
	 * @return {@link ResultWrapper} to indicate success of operation.
	 */
	ResultWrapper insert(String requirement, int jobitemid, boolean saveComment, boolean saveInstrumentReq, int plantid);

	void insertRequirement(Requirement r);

	void saveOrUpdateAll(List<Requirement> requirements);

	List<Requirement> searchRequirement(String requirement);

	/**
	 * Validates that the given {@link Requirement} exists and if so saves the
	 * requirement.
	 * 
	 * @param id the id of the {@link Requirement}.
	 * @param requirement, the requirement text.
	 * @param saveComment if true then this requirement will be saved as a
	 *        {@link JobItemRequirementComment}.
	 * @param saveInstrumentReq instrumentupdate id of checkbox that if checked
	 *        will set the requirement as the new instrument requirement.
	 * @param plantid the id of the {@link Instrument}.
	 * @return
	 */
	ResultWrapper update(int id, String requirement, boolean saveComment, boolean saveInstrumentReq, int plantid);

	void updateRequirement(Requirement r);

	/**
	 * Updates the given {@link Requirement} and set's whether it has been
	 * deleted or not.
	 * 
	 * @param reqId the id of the {@link Requirement}.
	 * @param deleted boolean indicating whether this {@link Requirement} should
	 *        be deleted or not.
	 * @return {@link ResultWrapper} to indicate success of operation.
	 */
	ResultWrapper updateRequirementStatus(int reqId, boolean deleted);
}
