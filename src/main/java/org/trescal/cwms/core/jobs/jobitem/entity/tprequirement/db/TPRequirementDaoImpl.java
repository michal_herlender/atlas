package org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.db;

import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirementDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement_;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_RecordTPRequirements;

@Repository
public class TPRequirementDaoImpl extends BaseDaoImpl<TPRequirement, Integer> implements TPRequirementDao
{
	@Override
	protected Class<TPRequirement> getEntity() {
		return TPRequirement.class;
	}
	
	@Autowired
	private HookInterceptor_RecordTPRequirements hookInterceptor_RecordTPRequirements;

	@Override
	public void persist(TPRequirement tpr) {
		super.persist(tpr);
		hookInterceptor_RecordTPRequirements.recordAction(tpr);
	};
	
	@Override
	public List<TPRequirementDTO> getProjectionDTOsForIds(Collection<Integer> requirementIds) {
		if (requirementIds.isEmpty()) throw new IllegalArgumentException("No requirement ids were specified");
		return getResultList(cb -> {
			CriteriaQuery<TPRequirementDTO> cq = cb.createQuery(TPRequirementDTO.class);
			Root<TPRequirement> root = cq.from(TPRequirement.class);
			cq.where(root.get(TPRequirement_.id).in(requirementIds));
			cq.select(cb.construct(TPRequirementDTO.class,
					root.get(TPRequirement_.id),
					root.get(TPRequirement_.adjustment),
					root.get(TPRequirement_.calibration),
					root.get(TPRequirement_.investigation),
					root.get(TPRequirement_.repair)
					));
			
			return cq;
		});
	}
}