package org.trescal.cwms.core.jobs.repair.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.repair.component.enums.RepairComponentStateEnum;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairCompletionReportFormDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.transformer.RepairCompletionReportFormDTOTransformer;
import org.trescal.cwms.core.jobs.repair.dto.form.validator.RepairCompletionReportFormDTOValidator;
import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationStateEnum;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.db.RepairCompletionReportService;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.enums.AdjustmentPerformedEnum;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateTimeEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.spring.model.KeyValue;

import javax.validation.Valid;
import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME })
public class RepairCompletionReportController {

	@Autowired
	private RepairCompletionReportFormDTOValidator validator;
	@Autowired
	private RepairCompletionReportFormDTOTransformer transformer;
	@Autowired
	private RepairInspectionReportService rirService;
	@Autowired
	private RepairCompletionReportService rcrService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private CertificateService certificateService;
	
	@InitBinder("rcrForm")
	protected void initBinder(WebDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.dtf_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDateTime.class, new CustomLocalDateTimeEditor(DateTimeFormatter.ISO_DATE_TIME));
		binder.setValidator(validator);
	}

	@ModelAttribute("currencySymbol")
	protected String getCurrency(@RequestParam(value = "jobitemid", required = false) Integer jobItemId,
			@RequestParam(name = "rirId", required = false) Integer rirId) {
		JobItem ji;
		if (jobItemId != null)
			ji = jobItemService.findJobItem(jobItemId);
		else {
			ji = rirService.get(rirId).getJi();
		}
		return ji.getJob().getCurrency().getCurrencySymbol();
	}

	private void referenceInitData(Model model, Integer jobItemId, Integer rirId, Integer currentSubdivId) {
		RepairCompletionReportFormDTO form;
		RepairInspectionReport rir;
		RepairCompletionReport rcr = null;
		if (rirId == null)
			rir = this.rirService.getRirByJobItemIdAndSubdivId(jobItemId, currentSubdivId);
		else
			rir = this.rirService.get(rirId);
		if (rir != null && rir.getRepairCompletionReport() != null) {
			rcr = rir.getRepairCompletionReport();
			form = transformer.transform(rcr);
			form.setRirId(rir.getRirId());
			form.setMisuse(rir.getMisuse());
			form.setMisuseComment(rir.getMisuseComment());
		} else {
			form = new RepairCompletionReportFormDTO();
		}
		form.setJobItemId(jobItemId);
		model.addAttribute("rcrForm", form);
		model.addAttribute("disabledModification", form.getValidated() != null && form.getValidated());

		model.addAttribute("lastTpCertDate", certificateService.getLastTpCertDate(form.getJobItemId()));
		boolean needTPRcrAppendixDocument = false;
		String encryptedRcrAppendixFile = null;
		String rcrAppendixFileName = null;
		JobItem ji = this.jobItemService.findJobItem(jobItemId);
		if (rcr != null && ji != null
				&& ji.getNextWorkReq().getWorkRequirement().getType().equals(WorkRequirementType.THIRD_PARTY)) {
			needTPRcrAppendixDocument = true;
			File toDirectory = this.rcrService.getRepairCompletionReportDirectory(ji.getJob().getJobno());
			rcr.setDirectory(toDirectory);
			File rcrAppendixFile = new File(rcr.getRepairCompletionReportAppendixFilePath());
			if (rcrAppendixFile.exists()) {
				encryptedRcrAppendixFile = rcr.getEncryptedRepairCompletionReportAppendixFilePath();
				rcrAppendixFileName = rcr.getRepairCompletionReportAppendixFileName();
			}
		}
		model.addAttribute("needTPRcrAppendixDocument", needTPRcrAppendixDocument);
		model.addAttribute("encryptedRcrAppendixFile", encryptedRcrAppendixFile);
		model.addAttribute("rcrAppendixFileName", rcrAppendixFileName);
		referenceData(model, form);
	}
	
	private void referenceData(Model model, RepairCompletionReportFormDTO form) {
		model.addAttribute("canValidate", canValidate(form.getJobItemId(), form.getValidated()));
		// add rcr operation status using filter
		List<RepairOperationStateEnum> operationStatus = Arrays.stream(RepairOperationStateEnum.values()).filter(RepairOperationStateEnum::isRcrStatus).collect(Collectors.toList());
		model.addAttribute("operationStatus", operationStatus);

		// add rcr component status using filter
		List<RepairComponentStateEnum> componentStatus = Arrays.stream(RepairComponentStateEnum.values()).filter(e -> !e.isRirStatus()).collect(Collectors.toList());
		model.addAttribute("componentStatus", componentStatus);

		model.addAttribute("adjustmentPerformedValues", AdjustmentPerformedEnum.values());
		model.addAttribute("actionPerformed", false);
	}

	@RequestMapping(value = "/repaircompletionreport.htm", method = RequestMethod.GET)
	public ModelAndView doGet(Model model, @RequestParam(value = "jobitemid", required = false) Integer jobItemId,
			@RequestParam(name = "rirId", required = false) Integer rirId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		referenceInitData(model, jobItemId, rirId, subdivDto.getKey());
		return new ModelAndView("trescal/core/jobs/repair/repaircompletionreport");
	}

	@RequestMapping(value = "/repaircompletionreport.htm", method = RequestMethod.POST)
	public ModelAndView doDefaultPost(@ModelAttribute("rcrForm") RepairCompletionReportFormDTO form,
			@RequestParam(name = "rirId", required = false) Integer rirId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			BindingResult result, Model model) {
		referenceInitData(model, form.getJobItemId(), rirId, subdivDto.getKey());
		return new ModelAndView("trescal/core/jobs/repair/repaircompletionreport");
	}

	@RequestMapping(value = "/repaircompletionreport.htm", method = RequestMethod.POST, params = { "saveRcr",
			"!saveRir", "!saveAndValidateRir", "!saveAndValidateRcr" })
	public ModelAndView saveRcr(@Valid @ModelAttribute("rcrForm") RepairCompletionReportFormDTO form,
			BindingResult result,
			@RequestParam(name = "rirId", required = false) Integer rirId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Model model) {
		if (result.hasErrors()) {
			referenceData(model, form);
			return new ModelAndView("trescal/core/jobs/repair/repaircompletionreport");
		}

		this.rcrService.saveRepairCompletionReport(form, subdivDto);

		referenceData(model, form);
		model.addAttribute("actionPerformed", true);
		return new ModelAndView("trescal/core/jobs/repair/repaircompletionreport");
	}

	@RequestMapping(value = "/repaircompletionreport.htm", method = RequestMethod.POST, params = { "saveAndValidateRcr",
			"!saveRcr", "!saveAndValidateRir", "!saveRir" })
	public ModelAndView saveAndValidateRepairInspection(Locale locale,
			@Valid @ModelAttribute("rcrForm") RepairCompletionReportFormDTO form, BindingResult result,
			@RequestParam(name = "jobitemid", required = false) Integer jobItemId,
			@RequestParam(name = "rirId", required = false) Integer rirId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Model model) {
		if (result.hasErrors()) {
			referenceData(model, form);
			return new ModelAndView("trescal/core/jobs/repair/repaircompletionreport");
		}
		
		boolean canValidate = canValidate(form.getJobItemId(), form.getValidated());
		if ((form.getValidated() == null || !form.getValidated()) && canValidate)
			rcrService.validate(form, username, locale);
		referenceInitData(model, form.getJobItemId(), rirId, subdivDto.getKey());
		model.addAttribute("actionPerformed", canValidate);
		return new ModelAndView("trescal/core/jobs/repair/repaircompletionreport");
	}

	private boolean canValidate(int jobItemId, Boolean validated) {
		// can validate
		JobItem ji = jobItemService.findJobItem(jobItemId);
		return jobItemService.stateHasGroupOfKeyName(ji.getState(), StateGroup.AWAITINGRCRVALIDATION)
				&& (validated == null || !validated);
	}

}