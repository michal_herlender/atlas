package org.trescal.cwms.core.jobs.calibration.dto;

import org.trescal.cwms.spring.model.KeyValue;

public class CalibrationProcessKeyValue extends KeyValue<Integer, String> {

	public CalibrationProcessKeyValue(Integer id, String name) {
		super(id, name);
	}
}