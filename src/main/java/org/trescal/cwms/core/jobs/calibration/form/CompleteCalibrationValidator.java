package org.trescal.cwms.core.jobs.calibration.form;

import org.jasypt.digest.StandardStringDigester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.tools.DigestConfigCreator;
import org.trescal.cwms.core.tools.PasswordTools;

import javax.validation.constraints.NotNull;

@Component
public class CompleteCalibrationValidator implements Validator {
    @Autowired
    private DigestConfigCreator configCreator;
    @Autowired
    private CapabilityAuthorizationService procAccServ;
    @Autowired
    private CalibrationService calServ;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(CompleteCalibrationForm.class);
    }

    @Override
    public void validate(@NotNull Object target, @NotNull Errors errors) {
        CompleteCalibrationForm calForm = (CompleteCalibrationForm) target;
        Calibration cal = calServ.findCalibration(calForm.getCalId());

        if (this.procAccServ.authorizedFor(cal.getCapability().getId(), cal.getCalType().getCalTypeId(),
            calForm.getConCompletingCal().getPersonid()).isRight()) {
            if (calForm.isPasswordReq()) {
                boolean actionRequiresPassword = calForm.getSignNow();

                // The user is trying to use an existing password
                if (calForm.isAccreditedToSign() && calForm.isPwExists() && actionRequiresPassword) {
                    Contact con = calForm.getConCompletingCal();

                    StandardStringDigester digester = new StandardStringDigester();
                    digester.setConfig(this.configCreator.createDigesterConfig());

                    if (!digester.matches(calForm.getEncPassword(), con.getEncryptedPassword())) {
                        errors.rejectValue("encPassword", "error.calibration.validator.wrongsignpassword");
                    }
                }

                // The user is trying to set a new password
				if (calForm.isAccreditedToSign() && !calForm.isPwExists() && actionRequiresPassword) {
					if (!calForm.getEncPassword().equals(calForm.getConfirmPassword())) {
						errors.rejectValue("confirmPassword", "error.calibration.validator.confirmpassword");
					} else {
						if (PasswordTools.getPasswordStrength(calForm.getEncPassword()) < 3) {
							errors.rejectValue("encPassword", "error.calibration.validator.passwordstrength");
						}
					}
				}
			}
		} else {
			errors.rejectValue("cal.completedBy", null, "You are not accredited to edit this calibration.");
		}

		if ((calForm.getRemark() != null) && (calForm.getRemark().length() > 1000)) {
			errors.rejectValue("remark", null, "Remark must not be over 1000 characters in length.");
		}

		if (calForm.getIntervalUnitId() == null) {
			errors.reject(null, "Units missing for duration");
		}

		// if a cert is being issued but there are no items checked to be
		// included on the cert
        if (((calForm.getIssueNow() != null) && (calForm.getIssueNow()))
            && ((calForm.getIncludeOnCert() == null) || ((calForm.getIncludeOnCert().size() == 1)
            && (calForm.getIncludeOnCert().get(0) == 0)))) {
            errors.rejectValue("issueNow", null, "Please select the items you wish to include on the certificate.");
        }

    }
}