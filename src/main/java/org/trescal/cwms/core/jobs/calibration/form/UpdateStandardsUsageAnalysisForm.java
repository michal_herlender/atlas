package org.trescal.cwms.core.jobs.calibration.form;

import lombok.Setter;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysisstatus.StandardsUsageAnalysisStatus;

import lombok.Getter;

@Getter @Setter
public class UpdateStandardsUsageAnalysisForm {
	
	private Integer standardsUsageAnalysisId;
	private Date startDate;
	private Date finishDate;
	private StandardsUsageAnalysisStatus status;
	private List<StandardsUsageAnalysisStatus> standardsUsageAnalysisStatus;
	
}
