package org.trescal.cwms.core.jobs.jobitem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExpenseItemDTO {

	private Integer id;
	private Integer jobId;
	private String jobNo;
	private Integer itemNo;
	private String modelName;
	private String comment;
}