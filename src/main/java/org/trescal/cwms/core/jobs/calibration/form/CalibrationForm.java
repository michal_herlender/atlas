package org.trescal.cwms.core.jobs.calibration.form;

import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.form.JobItemForm;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandard;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class CalibrationForm extends JobItemForm {
	private boolean accredited;
	private boolean accreditedToSign;
	private Integer actionOutcomeId;
	private Integer[] addStdsOnScreen;
	private Boolean addToExistingCert;
	private boolean allowUncalibratedStds;
	private Integer calProcessId;
	private String certFilePath;
	private boolean certPreviewed;
	private Contact conCompletingCal;
	private String confirmPassword;
	private Integer duration;
	private String intervalUnitId;
	private String encPassword;
	private List<Instrument> errorAdditionalStds;
	private List<DefaultStandard> errorProcStds;
	private Integer existingCertId;
	private MultipartFile file;
	private boolean groupCal;
	private List<Integer> includeOnCert;
	private boolean infoAcknowledged;
	private Integer[] instrums;
	private Boolean issueNow;
	private List<Integer> itemsToCalibrate;
	private Map<Integer, Integer> linkedOutcomes;
	private Boolean openProc;
	private boolean passwordReq;
	private Integer procId;
	private Integer[] procStdsOnScreen;
	private boolean pwExists;
	private String remark;
	private Boolean signNow;
	private Set<Integer> stdIds;
	private String submitAction;
	private String template;
	private Map<Integer, Integer> timeSpents;
	private Integer workInstructionId;
	private Boolean certAllowed;
	private LocalDate calDate;
	private String calDateFromReservedCertificate;

	public Integer getActionOutcomeId() {
		return this.actionOutcomeId;
	}

	/**
	 * @return the addStdsOnScreen
	 */
	public Integer[] getAddStdsOnScreen()
	{
		return this.addStdsOnScreen;
	}

	public Boolean getAddToExistingCert()
	{
		return this.addToExistingCert;
	}

	/**
	 * @return the calProcessId
	 */
	public Integer getCalProcessId()
	{
		return this.calProcessId;
	}

	public String getCertFilePath()
	{
		return this.certFilePath;
	}

	/**
	 * @return the conCompletingCal
	 */
	public Contact getConCompletingCal()
	{
		return this.conCompletingCal;
	}

	/**
	 * @return the confirmPassword
	 */
	public String getConfirmPassword()
	{
		return this.confirmPassword;
	}

	/**
	 * @return the duration
	 */
	public Integer getDuration()
	{
		return this.duration;
	}

	/**
	 * @return the encPassword
	 */
	public String getEncPassword()
	{
		return this.encPassword;
	}

	/**
	 * @return the errorAdditionalStds
	 */
	public List<Instrument> getErrorAdditionalStds()
	{
		return this.errorAdditionalStds;
	}

	/**
	 * @return the errorProcStds
	 */
	public List<DefaultStandard> getErrorProcStds()
	{
		return this.errorProcStds;
	}

	public Integer getExistingCertId()
	{
		return this.existingCertId;
	}

	/**
	 * @return the file
	 */
	public MultipartFile getFile()
	{
		return this.file;
	}

	public List<Integer> getIncludeOnCert()
	{
		return this.includeOnCert;
	}

	/**
	 * @return the instrums
	 */
	public Integer[] getInstrums()
	{
		return this.instrums;
	}

	/**
	 * @return the issueNow
	 */
	public Boolean getIssueNow()
	{
		return this.issueNow;
	}

	public List<Integer> getItemsToCalibrate()
	{
		return this.itemsToCalibrate;
	}

	public Map<Integer, Integer> getLinkedOutcomes()
	{
		return this.linkedOutcomes;
	}

	/**
	 * @return the openProc
	 */
	public Boolean getOpenProc()
	{
		return this.openProc;
	}

	/**
	 * @return the procId
	 */
	public Integer getProcId()
	{
		return this.procId;
	}

	/**
	 * @return the procStdsOnScreen
	 */
	public Integer[] getProcStdsOnScreen()
	{
		return this.procStdsOnScreen;
	}

	public String getRemark()
	{
		return this.remark;
	}

	public Boolean getSignNow()
	{
		return this.signNow;
	}

	public Set<Integer> getStdIds()
	{
		return this.stdIds;
	}

	/**
	 * @return the submitAction
	 */
	public String getSubmitAction()
	{
		return this.submitAction;
	}

	/**
	 * @return the template
	 */
	public String getTemplate()
	{
		return this.template;
	}

	public Map<Integer, Integer> getTimeSpents()
	{
		return this.timeSpents;
	}

	public Integer getWorkInstructionId()
	{
		return this.workInstructionId;
	}

	/**
	 * @return the accredited
	 */
	public boolean isAccredited()
	{
		return this.accredited;
	}

	/**
	 * @return the accreditedToSign
	 */
	public boolean isAccreditedToSign()
	{
		return this.accreditedToSign;
	}

	public boolean isAllowUncalibratedStds()
	{
		return this.allowUncalibratedStds;
	}

	public boolean isCertPreviewed()
	{
		return this.certPreviewed;
	}

	public boolean isGroupCal()
	{
		return this.groupCal;
	}

	public boolean isInfoAcknowledged()
	{
		return this.infoAcknowledged;
	}

	public boolean isPasswordReq()
	{
		return this.passwordReq;
	}

	/**
	 * @return the pwExists
	 */
	public boolean isPwExists()
	{
		return this.pwExists;
	}

	/**
	 * @param accredited the accredited to set
	 */
	public void setAccredited(boolean accredited)
	{
		this.accredited = accredited;
	}

	/**
	 * @param accreditedToSign the accreditedToSign to set
	 */
	public void setAccreditedToSign(boolean accreditedToSign)
	{
		this.accreditedToSign = accreditedToSign;
	}

	public void setActionOutcomeId(Integer actionOutcomeId)
	{
		this.actionOutcomeId = actionOutcomeId;
	}

	/**
	 * @param addStdsOnScreen the addStdsOnScreen to set
	 */
	public void setAddStdsOnScreen(Integer[] addStdsOnScreen)
	{
		this.addStdsOnScreen = addStdsOnScreen;
	}

	public void setAddToExistingCert(Boolean addToExistingCert)
	{
		this.addToExistingCert = addToExistingCert;
	}

	public void setAllowUncalibratedStds(boolean allowUncalibratedStds)
	{
		this.allowUncalibratedStds = allowUncalibratedStds;
	}

	/**
	 * @param calProcessId the calProcessId to set
	 */
	public void setCalProcessId(Integer calProcessId)
	{
		this.calProcessId = calProcessId;
	}

	public void setCertFilePath(String certFilePath)
	{
		this.certFilePath = certFilePath;
	}

	public void setCertPreviewed(boolean certPreviewed)
	{
		this.certPreviewed = certPreviewed;
	}

	/**
	 * @param conCompletingCal the conCompletingCal to set
	 */
	public void setConCompletingCal(Contact conCompletingCal)
	{
		this.conCompletingCal = conCompletingCal;
	}

	/**
	 * @param confirmPassword the confirmPassword to set
	 */
	public void setConfirmPassword(String confirmPassword)
	{
		this.confirmPassword = confirmPassword;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Integer duration)
	{
		this.duration = duration;
	}

	/**
	 * @param encPassword the encPassword to set
	 */
	public void setEncPassword(String encPassword)
	{
		this.encPassword = encPassword;
	}

	/**
	 * @param errorAdditionalStds the errorAdditionalStds to set
	 */
	public void setErrorAdditionalStds(List<Instrument> errorAdditionalStds)
	{
		this.errorAdditionalStds = errorAdditionalStds;
	}

	/**
	 * @param errorProcStds the errorProcStds to set
	 */
	public void setErrorProcStds(List<DefaultStandard> errorProcStds)
	{
		this.errorProcStds = errorProcStds;
	}

	public void setExistingCertId(Integer existingCertId)
	{
		this.existingCertId = existingCertId;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(MultipartFile file)
	{
		this.file = file;
	}

	public void setGroupCal(boolean groupCal)
	{
		this.groupCal = groupCal;
	}

	public void setIncludeOnCert(List<Integer> includeOnCert)
	{
		this.includeOnCert = includeOnCert;
	}

	public void setInfoAcknowledged(boolean infoAcknowledged)
	{
		this.infoAcknowledged = infoAcknowledged;
	}

	/**
	 * @param instrums the instrums to set
	 */
	public void setInstrums(Integer[] instrums)
	{
		this.instrums = instrums;
	}

	/**
	 * @param issueNow the issueNow to set
	 */
	public void setIssueNow(Boolean issueNow)
	{
		this.issueNow = issueNow;
	}

	public void setItemsToCalibrate(List<Integer> itemsToCalibrate)
	{
		this.itemsToCalibrate = itemsToCalibrate;
	}

	public void setLinkedOutcomes(Map<Integer, Integer> linkedOutcomes)
	{
		this.linkedOutcomes = linkedOutcomes;
	}

	/**
	 * @param openProc the openProc to set
	 */
	public void setOpenProc(Boolean openProc)
	{
		this.openProc = openProc;
	}

	public void setPasswordReq(boolean passwordReq)
	{
		this.passwordReq = passwordReq;
	}

	/**
	 * @param procId the procId to set
	 */
	public void setProcId(Integer procId)
	{
		this.procId = procId;
	}

	/**
	 * @param procStdsOnScreen the procStdsOnScreen to set
	 */
	public void setProcStdsOnScreen(Integer[] procStdsOnScreen)
	{
		this.procStdsOnScreen = procStdsOnScreen;
	}

	/**
	 * @param pwExists the pwExists to set
	 */
	public void setPwExists(boolean pwExists)
	{
		this.pwExists = pwExists;
	}

	public void setRemark(String remark)
	{
		this.remark = remark;
	}

	public void setSignNow(Boolean signNow)
	{
		this.signNow = signNow;
	}

	public void setStdIds(Set<Integer> stdIds)
	{
		this.stdIds = stdIds;
	}

	/**
	 * @param submitAction the submitAction to set
	 */
	public void setSubmitAction(String submitAction)
	{
		this.submitAction = submitAction;
	}

	/**
	 * @param template the template to set
	 */
	public void setTemplate(String template)
	{
		this.template = template;
	}

	public void setTimeSpents(Map<Integer, Integer> timeSpents)
	{
		this.timeSpents = timeSpents;
	}

	public void setWorkInstructionId(Integer workInstructionId)
	{
		this.workInstructionId = workInstructionId;
	}

	public String getIntervalUnitId() {
		return intervalUnitId;
	}

	public void setIntervalUnitId(String intervalUnitId) {
		this.intervalUnitId = intervalUnitId;
	}

	public Boolean getCertAllowed() {
		return certAllowed;
	}

	public void setCertAllowed(Boolean certAllowed) {
		this.certAllowed = certAllowed;
	}

	@NotNull(message = "{error.value.notselected}")
	public LocalDate getCalDate() {
		return calDate;
	}

	public void setCalDate(LocalDate calDate) {
		this.calDate = calDate;
	}

	public String getCalDateFromReservedCertificate() {
		return calDateFromReservedCertificate;
	}

	public void setCalDateFromReservedCertificate(String calDateFromReservedCertificate) {
		this.calDateFromReservedCertificate = calDateFromReservedCertificate;
	}
}
