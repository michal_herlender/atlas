package org.trescal.cwms.core.jobs.generalserviceoperation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db.GeneralServiceOperationService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink;
import org.trescal.cwms.core.jobs.generalserviceoperation.form.GeneralServiceOperationForm;
import org.trescal.cwms.core.jobs.generalserviceoperation.form.validator.GeneralServiceOperationValidator;
import org.trescal.cwms.core.jobs.jobitem.controller.JobItemController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateTimeEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_GeneralServiceOperation;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class UploadGeneralServiceOperationDocumentController extends JobItemController {
	private static final Logger logger = LoggerFactory.getLogger(
		org.trescal.cwms.core.jobs.generalserviceoperation.controller.UploadGeneralServiceOperationDocumentController.class);
	public static final String ACTION_PERFORMED = "UPLOAD_DOCUMENT";
	private final String FORM_NAME = "form";

	@Autowired
	private GeneralServiceOperationService gsoServ;
	@Autowired
	private GeneralServiceOperationValidator validator;
	@Autowired
	private JobItemService jiService;
	@Autowired
	private SubdivService subdivService;

	@InitBinder(FORM_NAME)
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(Date.class,
			new MyCustomDateEditor(DateTools.dtf_ISO8601, true));
		binder.registerCustomEditor(LocalDateTime.class, new CustomLocalDateTimeEditor(DateTimeFormatter.ISO_DATE_TIME));
		binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
		binder.setValidator(validator);
	}

	@ModelAttribute(FORM_NAME)
	protected GeneralServiceOperationForm formBackingObject(
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") int jobitemid,
			@RequestParam(value = "gsoid", required = false) Integer gsoId) throws Exception {

		JobItem ji = this.jobItemService.findJobItem(jobitemid);
		if (ji == null || jobitemid == 0) {
			logger.error("Jobitem with id " + jobitemid + " could not be found");
			throw new Exception("Unable to locate requested jobitem");
		}

		GeneralServiceOperationForm form = new GeneralServiceOperationForm();
		GeneralServiceOperation gso = gsoId == null ? this.gsoServ.getOnGoingByJobItemId(jobitemid)
				: this.gsoServ.get(gsoId);
		if (gso != null) {
			form.setStartedOn(DateTools.dateToLocalDateTime(gso.getStartedOn()));
			form.setStartedByName(gso.getStartedBy().getName());
			form.setGso(gso);
			form.setActionPerformed(ACTION_PERFORMED);
			form.setCompletedOn(DateTools.dateToLocalDateTime(gso.getCompletedOn()));
			form.setCompletedByName(gso.getCompletedBy().getName());
		}
		form.setJi(ji);

		return form;
	}

	@RequestMapping(value = {"/uploadgsodocument.htm"}, method = {RequestMethod.GET})
	protected ModelAndView uploadGsoDocInit(@RequestParam(value = "jobitemid") Integer jobItemId,
											@RequestParam(value = "gsoid", required = false) Integer gsoId,
											@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
											@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
											@ModelAttribute("form") GeneralServiceOperationForm form) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		Map<String, Object> map = referenceData(jobItemId, contact, subdivDto.getKey(), username);

		List<JobItem> otherJis = form.getGso().getLinks().stream()
			.filter(e -> this.jiService.isReadyForGeneralServiceOperationDocumentUpload(e.getJi()) && !e
				.getOutcome().getGenericValue().equals(ActionOutcomeValue_GeneralServiceOperation.FAILED_REDO))
			.map(GsoLink::getJi).collect(Collectors.toList());
		// move current ji to first position
		int index = otherJis.indexOf(form.getJi());
		if (index != -1) {
			otherJis.remove(index);
			otherJis.add(0, form.getJi());
		}
		map.put("otherjobitems", otherJis);

		return new ModelAndView("trescal/core/jobs/generalserviceoperation/uploadgsodocument", map);
	}

	@RequestMapping(value = {"/uploadgsodocument.htm"}, method = {RequestMethod.POST})
	protected ModelAndView completeGSOPost(@RequestParam(value = "jobitemid") Integer jobItemId,
										   @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
										   @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
										   @ModelAttribute("form") GeneralServiceOperationForm form, BindingResult bindingResult, RedirectAttributes redirectAttributes) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		this.validator.validate(form, bindingResult, contact);
		if (bindingResult.hasErrors())
			return uploadGsoDocInit(jobItemId, form.getGso().getId(), username, subdivDto, form);
		Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
		this.gsoServ.uploadGeneralServiceOperationDocument(form, allocatedSubdiv);
		redirectAttributes.addAttribute("jobitemid", form.getJi().getJobItemId());
		return new ModelAndView(new RedirectView("jiactions.htm"));
	}

}
