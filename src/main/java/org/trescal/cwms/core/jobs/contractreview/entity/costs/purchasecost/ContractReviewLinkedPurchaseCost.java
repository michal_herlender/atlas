package org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.jobs.contractreview.entity.costs.ContractReviewLinkedCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;

@Entity
@Table(name = "contractreviewlinkedpurchasecost")
public class ContractReviewLinkedPurchaseCost extends ContractReviewLinkedCost
{
	private ContractReviewPurchaseCost purchaseCost;

	private QuotationPurchaseCost quotationPurchaseCost;
	private JobCostingPurchaseCost jobCostingPurchaseCost;

	@Transient
	public JobCostingPurchaseCost getJobCostingPurchaseCost()
	{
		return this.jobCostingPurchaseCost;
	}

	@OneToOne(fetch=FetchType.LAZY, cascade = javax.persistence.CascadeType.ALL)
	@JoinColumn(name = "purcostid", nullable = false, unique = true)
	public ContractReviewPurchaseCost getPurchaseCost()
	{
		return this.purchaseCost;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "quotepurcostid")
	public QuotationPurchaseCost getQuotationPurchaseCost()
	{
		return this.quotationPurchaseCost;
	}

	public void setJobCostingPurchaseCost(JobCostingPurchaseCost jobCostingPurchaseCost)
	{
		this.jobCostingPurchaseCost = jobCostingPurchaseCost;
	}

	public void setPurchaseCost(ContractReviewPurchaseCost purchaseCost)
	{
		this.purchaseCost = purchaseCost;
	}

	public void setQuotationPurchaseCost(QuotationPurchaseCost quotationPurchaseCost)
	{
		this.quotationPurchaseCost = quotationPurchaseCost;
	}

}
