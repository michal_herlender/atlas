package org.trescal.cwms.core.jobs.certificate.entity.certstatus;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum CertStatusEnum {
	UNDEFINED("Undefined", "certificate.status.undefined"),
	UNSIGNED_AWAITING_SIGNATURE("Unsigned - Awaiting Signature", "certificate.status.unsigned.awaiting.signature"),
	SIGNED_AWAITING_APPROVAL("Signed - Awaiting Approval", "certificate.status.signed.awaiting.approval"),
	SIGNED("Signed", "certificate.status.signed"),
	SIGNING_NOT_SUPPORTED("Signing not supported", "certificate.status.singing.not.supported"),
	RESERVED("Reserved", "certificate.status.reserved"), 
	OBSOLETE("Obsolete", "certificate.status.obsolete"),
	TO_BE_REPLACED("To be replaced", "certificate.status.tobereplaced"),
	REPLACED("Replaced", "certificate.status.replaced"), 
	DELETED("Deleted", "certificate.status.deleted"),
	CANCELLED("Cancelled", "certificate.status.cancelled");

	private ReloadableResourceBundleMessageSource messageSource;
	private String fullName;
	private String messageCode;

	private CertStatusEnum(String fullName, String messageCode) {
		this.fullName = fullName;
		this.messageCode = messageCode;
	}

	public String getFullName() {
		return fullName;
	}

	public String getMessage() {
		return messageSource.getMessage(messageCode, null, this.toString(), LocaleContextHolder.getLocale());
	}

	public String getMessageCode() {
		return messageCode;
	}

	@Component
	private static class CertStatusEnumMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (CertStatusEnum e : EnumSet.allOf(CertStatusEnum.class))
				e.messageSource = messageSource;
		}
	}

	public String getName() {
		return this.name();
	}

}