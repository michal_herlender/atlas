package org.trescal.cwms.core.jobs.jobitem.entity.pat.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitem.entity.pat.PAT;

public interface PATDao extends BaseDao<PAT, Integer> {}