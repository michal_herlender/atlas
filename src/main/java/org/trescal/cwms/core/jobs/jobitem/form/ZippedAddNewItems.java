package org.trescal.cwms.core.jobs.jobitem.form;

import lombok.Value;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.util.Date;

@Value(staticConstructor = "of")
public class ZippedAddNewItems {
    Integer basketBaseItemId;
    Integer basketExistingGroupId;
    Integer basketId;
    String basketPart;
    Integer basketTempGroupId;
    String basketType;
    Integer basketQuotationItemId;
    Integer serviceTypeId;
    Integer serviceTypeSource;
    Date datesIn;
    Integer bookInByContactId;
    Integer instAddrId;
    Integer instCalFreq;
    IntervalUnit instCalUnitFreq;
    Boolean instDelete;
    String instEndRange;
    Integer instMfrId;
    Integer instPersonId;
    String instPlantNo;
    String instSerialNo;
    String instStartRange;
    Integer instThreadId;
    Integer instUomId;
    String itemPO;
    Integer itemProcId;
    Integer itemWorkInstId;
    String instModelName;
    String instCustomerDesc;
}
