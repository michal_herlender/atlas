package org.trescal.cwms.core.jobs.repair.component;

import java.util.Comparator;

public class FreeRepairComponentComparator implements Comparator<FreeRepairComponent>
{
	@Override
	public int compare(FreeRepairComponent c1, FreeRepairComponent c2)
	{
		return ((Integer) c1.getComponentId()).compareTo(c2.getComponentId());
	}
}
