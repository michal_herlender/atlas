package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateReplacementType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@RestController
@RequestMapping("certificate")
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV,
		Constants.HTTPSESS_NEW_FILE_LIST })
public class JICertificatesReplacementAjaxController {
	
	@Autowired
	private CertificateService certServ;
	
	@PostMapping("replacement")
	protected ResultWrapper replacement(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@RequestParam(value = "jobitemid", required = true) Integer jobItemId,
			@RequestParam(value = "certificateid", required = true) Integer certificateId,
			@RequestParam(value = "certType", required = true) CertificateReplacementType certType, Locale locale) throws Exception {
		
		return this.certServ.certificateReplacement(jobItemId, certificateId, locale, certType);
	}
}