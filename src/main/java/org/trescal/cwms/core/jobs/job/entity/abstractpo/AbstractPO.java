package org.trescal.cwms.core.jobs.job.entity.abstractpo;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;

@MappedSuperclass
public abstract class AbstractPO extends Allocated<Company> {
	
	private int poId;
	private String poNumber;
	private String comment;
	
	/**
	 * @return the poId
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "poid", nullable = false, unique = true)
	@Type(type = "int")
	public int getPoId() {
		return this.poId;
	}
	
	/**
	 * @return the poNumber
	 */
	@NotNull
	@Length(min = 1, max = 60)
	@Column(name = "ponumber", nullable=false, length=60)
	public String getPoNumber() {
		return this.poNumber;
	}
	
	/**
	 * @return the comment
	 */
	@Length(min = 0, max = 1000)
	@Column(name = "comment", length=1000)
	public String getComment() {
		return this.comment;
	}
	
	@Transient
	public abstract boolean isBPO();
	
	/**
	 * @param poId the poId to set
	 */
	public void setPoId(int poId) {
		this.poId = poId;
	}
	
	/**
	 * @param poNumber the poNumber to set
	 */
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
}