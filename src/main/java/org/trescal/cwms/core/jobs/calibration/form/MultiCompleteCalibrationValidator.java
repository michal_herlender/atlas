package org.trescal.cwms.core.jobs.calibration.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.dto.MultiCompleteCal;
import org.trescal.cwms.core.jobs.calibration.dto.MultiCompleteCalItem;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.StringTools;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Component
public class MultiCompleteCalibrationValidator implements Validator {
    @Autowired
    private CapabilityAuthorizationService procAccServ;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(MultiCompleteCalibrationForm.class);
    }

    @Override
    public void validate(@NotNull Object target, @NotNull Errors errors) {
        MultiCompleteCalibrationForm form = (MultiCompleteCalibrationForm) target;

        Contact currentCon = form.getCurrentContact();

        List<MultiCompleteCal> relevantCals = null;
        String field = null;
        if ((form.getAction() == Constants.FORM_COMPLETE)
            || (form.getAction() == Constants.FORM_CANCEL)) {
            relevantCals = form.getOnGoingCals();
			field = "onGoingCals";
		}
		else if (form.getAction() == Constants.FORM_RESUME)
		{
			relevantCals = form.getOnHoldCals();
			field = "onHoldCals";
		}

		for (MultiCompleteCal mCal : relevantCals)
		{
			if (mCal.isSelected())
			{
                Calibration cal = mCal.getCal();

                List<JobItem> jis = new ArrayList<JobItem>();
                for (MultiCompleteCalItem item : mCal.getItems()) {
                    jis.add(item.getCalLink().getJi());
                }
                String items = StringTools.combineSequenceJobItemNumbers(jis);

                // not accredited
                if (!this.procAccServ.authorizedFor(cal.getCapability().getId(), cal.getCalType().getCalTypeId(), currentCon.getPersonid()).isRight()) {
                    errors.rejectValue(field, null, "Calibration of "
                        + ((jis.size() > 1) ? "items " : "item ")
                        + items
                        + " cannot be actioned: Not accredited for procedure "
                        + cal.getCapability().getReference() + " ("
                        + cal.getCalType().getServiceType().getShortName() + ")");
                }
            }
		}
	}
}