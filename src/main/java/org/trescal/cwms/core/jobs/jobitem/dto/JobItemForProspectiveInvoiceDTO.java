package org.trescal.cwms.core.jobs.jobitem.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class JobItemForProspectiveInvoiceDTO implements Comparable<JobItemForProspectiveInvoiceDTO> {

    private Integer id;
    private Integer itemNo;
    private Integer jobId;
    private String jobNo;
    private String jobStatus;
    private Integer itemCount;
    private Integer subdivId;
    private String subdivName;
    private String poNumbers;
    private Integer costingItemId;
    private LocalDate costingDate;
    private String costingCurrencySymbol;
    private BigDecimal costingCalibrationCost;
    private BigDecimal costingRepairCost;
    private BigDecimal costingAdjustmentCost;
    private BigDecimal costingPurchaseCost;
    private Integer invoiceCount;
    private String plantNo;
    private Boolean include;

    public JobItemForProspectiveInvoiceDTO(Integer id, Integer itemNo, Integer jobId, String jobNo, String jobStatus,
                                           Long itemCount, Integer subdivId, String subdivName, String poNumbers, Integer costingItemId,
                                           LocalDate costingDate, String costingCurrencySymbol, BigDecimal costingCalibrationCost,
                                           BigDecimal costingRepairCost, BigDecimal costingAdjustmentCost, BigDecimal costingPurchaseCost,
                                           Long invoiceCount, String plantNo) {
        super();
        this.id = id;
        this.itemNo = itemNo;
        this.jobId = jobId;
        this.jobNo = jobNo;
        this.jobStatus = jobStatus;
        this.itemCount = itemCount.intValue();
        this.subdivId = subdivId;
        this.subdivName = subdivName;
        this.poNumbers = poNumbers;
        this.costingItemId = costingItemId;
        this.costingDate = costingDate;
        this.costingCurrencySymbol = costingCurrencySymbol;
        this.costingCalibrationCost = costingCalibrationCost;
        this.costingRepairCost = costingRepairCost;
        this.costingAdjustmentCost = costingAdjustmentCost;
        this.costingPurchaseCost = costingPurchaseCost;
        this.invoiceCount = invoiceCount.intValue();
        this.plantNo = plantNo;
        this.include = true;
    }

    @Override
    public int compareTo(JobItemForProspectiveInvoiceDTO other) {
        int result = this.getJobNo().compareTo(other.getJobNo());
        if (result == 0)
            return this.getItemNo().compareTo(other.getItemNo());
        else
            return result;
    }
}