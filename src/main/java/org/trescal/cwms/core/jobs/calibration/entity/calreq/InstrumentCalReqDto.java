package org.trescal.cwms.core.jobs.calibration.entity.calreq;

/**
 * Currently just for InstrumentCalReq, if needed for others we should make more generic
 * @author galen
 *
 */
public class InstrumentCalReqDto {
	private Integer plantid;
	private String privateInstructions;
	private String publicInstructions;
	
	public InstrumentCalReqDto(Integer plantid, String privateInstructions, String publicInstructions) {
		this.plantid = plantid;
		this.privateInstructions = privateInstructions;
		this.publicInstructions = publicInstructions;
	}
	
	public Integer getPlantid() {
		return plantid;
	}
	public String getPrivateInstructions() {
		return privateInstructions;
	}
	public String getPublicInstructions() {
		return publicInstructions;
	}
	public void setPlantid(Integer plantid) {
		this.plantid = plantid;
	}
	public void setPrivateInstructions(String privateInstructions) {
		this.privateInstructions = privateInstructions;
	}
	public void setPublicInstructions(String publicInstructions) {
		this.publicInstructions = publicInstructions;
	}
}
