package org.trescal.cwms.core.jobs.jobitem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JobItemInStateDTO implements Comparable<JobItemInStateDTO> {

	private Integer jobItemId;
	private Integer stateId;

	@Override
	public int compareTo(JobItemInStateDTO other) {
		int result = this.stateId.compareTo(other.stateId);
		if (result == 0)
			return this.jobItemId.compareTo(other.jobItemId);
		else
			return result;
	}
}