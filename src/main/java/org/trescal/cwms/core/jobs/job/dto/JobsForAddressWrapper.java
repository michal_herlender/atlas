package org.trescal.cwms.core.jobs.job.dto;

import java.util.Date;

import org.hibernate.Hibernate;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

public class JobsForAddressWrapper
{
	private Instrument inst;
	private Date itemDueDate;
	private Integer itemNo;
	private Integer jobId;
	private String jobNo;
	private String state;

	public JobsForAddressWrapper(Integer jobId, String jobNo, Integer itemNo, Date itemDueDate, Instrument inst, String state)
	{
		this.jobId = jobId;
		this.jobNo = jobNo;
		this.itemNo = itemNo;
		this.itemDueDate = itemDueDate;
		this.state = state;
		this.inst = inst;

		this.fetchRequiredProperties();
	}

	/**
	 * Initialises fields required on the instrument that are lazily loaded by
	 * default, meaning that they cannot be read when outside of the session
	 * scope (i.e. when the object is passed back to the client-side using ajax)
	 */
	public void fetchRequiredProperties()
	{
		Hibernate.initialize(this.inst);
		Hibernate.initialize(this.inst.getMfr());
		Hibernate.initialize(this.inst.getModel());
		Hibernate.initialize(this.inst.getModel().getMfr());
		Hibernate.initialize(this.inst.getModel().getDescription());
	}

	public Instrument getInst()
	{
		return this.inst;
	}

	public Date getItemDueDate()
	{
		return this.itemDueDate;
	}

	public Integer getItemNo()
	{
		return this.itemNo;
	}

	public Integer getJobId()
	{
		return this.jobId;
	}

	public String getJobNo()
	{
		return this.jobNo;
	}

	public String getState()
	{
		return this.state;
	}

	public void setInst(Instrument inst)
	{
		this.inst = inst;
	}

	public void setItemDueDate(Date itemDueDate)
	{
		this.itemDueDate = itemDueDate;
	}

	public void setItemNo(Integer itemNo)
	{
		this.itemNo = itemNo;
	}

	public void setJobId(Integer jobId)
	{
		this.jobId = jobId;
	}

	public void setJobNo(String jobNo)
	{
		this.jobNo = jobNo;
	}

	public void setState(String state)
	{
		this.state = state;
	}
}