package org.trescal.cwms.core.jobs.job.controller;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.form.EditJobInvoicingForm;
import org.trescal.cwms.core.jobs.job.form.EditJobInvoicingValidator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db.JobExpenseItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.NotInvoicedReason;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db.ExpenseItemNotInvoicedService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db.JobItemNotInvoicedService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.invoice.dto.PNotInvoiced;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.system.Constants;

@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
@Controller
@IntranetController
public class EditJobInvoicingController {

	public static final String VIEW_NAME = "trescal/core/jobs/job/editjobinvoicing";
	public static final String REQUEST_URL = "editjobinvoicing.htm";
	public static final String REDIRECT_URL = "redirect:viewjob.htm?loadtab=invoices-tab&jobid=";
	public static final String FORM_NAME = "command";

	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private JobService jobService;
	@Autowired
	private JobItemNotInvoicedService jiniService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private ExpenseItemNotInvoicedService expenseItemNotInvoicedService;
	@Autowired
	private JobExpenseItemService jobExpenseItemService;
	@Autowired
	private UserService userService;
	@Autowired
	private EditJobInvoicingValidator validator;

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute(FORM_NAME)
	protected EditJobInvoicingForm createForm(@RequestParam(name = "jobid") int jobid) {
		Job job = this.jobService.get(jobid);
		if (job == null)
			throw new RuntimeException("Job " + jobid + " not found!");
		EditJobInvoicingForm form = new EditJobInvoicingForm();
		form.setJobid(jobid);
		job.getItems().forEach(
			item -> form.getNotInvoiced().put(item.getJobItemId(), new PNotInvoiced(item.getNotInvoiced())));
		job.getExpenseItems().forEach(
			item -> form.getExpensesNotInvoiced().put(item.getId(), new PNotInvoiced(item.getNotInvoiced())));
		return form;
	}

	@RequestMapping(method = RequestMethod.GET, value = REQUEST_URL)
	public String viewForm(@RequestParam(name = "jobid") int jobid, Model model, Locale locale) {
		Job job = this.jobService.get(jobid);
		List<Invoice> periodicInvoices = this.invoiceService.getRecentPeriodicInvoices(job.getCon().getSub().getComp(),
			job.getOrganisation().getComp(), job.getRegDate(), 1);

		model.addAttribute("job", job);
		model.addAttribute("notInvoicedReasons", NotInvoicedReason.values());
		model.addAttribute("periodicInvoices", invoiceService.getPeriodicInvociesOptions(periodicInvoices, locale));

		return VIEW_NAME;
	}

	@RequestMapping(method = RequestMethod.POST, value = REQUEST_URL)
	public String onSubmit(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Model model,
			@Validated @ModelAttribute(FORM_NAME) EditJobInvoicingForm form, BindingResult bindingResult,
			Locale locale) {
		if (bindingResult.hasErrors()) {
			return viewForm(form.getJobid(), model, locale);
		}
		Contact contact = this.userService.get(username).getCon();
		for (Map.Entry<Integer, PNotInvoiced> entry : form.getNotInvoiced().entrySet()) {
			updateNotInvoiced(entry.getKey(), entry.getValue(), contact);
		}
		form.getExpensesNotInvoiced().forEach((key, value) -> updateExpensesNotInvoiced(key, value, contact));
		return REDIRECT_URL + form.getJobid();
	}

	/*
	 * Note, no need to save job item as relationship is mapped within
	 * NotInvoicedReason
	 */
	protected void updateNotInvoiced(int jobItemId, PNotInvoiced notInvoiced, Contact contact) {
		JobItem jobitem = jobItemService.findJobItem(jobItemId);
		if (notInvoiced.isSelected()) {
			jiniService.recordNotInvoiced(jobitem, notInvoiced.getReason(), notInvoiced.getComments(),
					notInvoiced.getPeriodicInvoiceId(), contact);
		} else if (jobitem.getNotInvoiced() != null) {
			// Delete existing not invoiced entity
			JobItemNotInvoiced instance = jobitem.getNotInvoiced();
			jobitem.setNotInvoiced(null);
			jiniService.delete(instance);
		}
	}

	protected void updateExpensesNotInvoiced(Integer expenseItemId, PNotInvoiced notInvoiced, Contact contact) {
		JobExpenseItem expenseItem = jobExpenseItemService.get(expenseItemId);
		if (notInvoiced.isSelected()) {
			expenseItemNotInvoicedService.recordNotInvoiced(expenseItem, notInvoiced.getReason(),
					notInvoiced.getComments(), notInvoiced.getPeriodicInvoiceId(), contact);
		} else if (expenseItem.getNotInvoiced() != null) {
			// Delete existing not invoiced entity
			expenseItem.setNotInvoiced(null);
			jobExpenseItemService.merge(expenseItem);
		}
	}
}