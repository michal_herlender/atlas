package org.trescal.cwms.core.jobs.job.entity.jobstatus.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface JobStatusService
{
	void deleteJobStatus(JobStatus jobstatus);

	/**
	 * Returns the {@link JobStatus} that signifies a {@link Job} is complete.
	 * If more than one is found then the first result is returned.
	 * 
	 * @return {@link JobStatus}.
	 */
	JobStatus findCompleteStatus();

	JobStatus findJobStatus(int id);

	/**
	 * Returns the {@link JobStatus} that signifies a {@link Job} is ready for
	 * invoicing. If more than one is found then the first result is returned.
	 * 
	 * @return {@link JobStatus}.
	 */
	JobStatus findReadyToInvoiceStatus();
	
	List<KeyValueIntegerString> getKeyValueList(Collection<Integer> jobStatusIds, Locale locale);

	List<JobStatus> getAllJobStatuss();

	void insertJobStatus(JobStatus jobstatus);

	void saveOrUpdateJobStatus(JobStatus jobstatus);

	void updateJobStatus(JobStatus jobstatus);
}