package org.trescal.cwms.core.jobs.job.form;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.pricing.invoice.dto.PNotInvoiced;
import org.trescal.cwms.core.pricing.invoice.form.PNotInvoicedValidator;

@Component
public class EditJobInvoicingValidator implements Validator {
	
	@Autowired
	private PNotInvoicedValidator validatorNotInvoiced;

	@Override
	public boolean supports(Class<?> arg0) {
		return EditJobInvoicingForm.class.equals(arg0);
	}

	@Override
	public void validate(Object object, Errors errors) {
		EditJobInvoicingForm form = (EditJobInvoicingForm) object;
		for (Map.Entry<Integer, PNotInvoiced> entry : form.getNotInvoiced().entrySet()) {
			Integer jobItemId = entry.getKey();
			PNotInvoiced notInvoiced = entry.getValue();
			errors.pushNestedPath("notInvoiced["+jobItemId+"]");
			validatorNotInvoiced.validate(notInvoiced, errors);
			errors.popNestedPath();
		}
	}

}
