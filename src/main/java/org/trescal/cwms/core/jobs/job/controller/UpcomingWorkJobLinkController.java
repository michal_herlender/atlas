package org.trescal.cwms.core.jobs.job.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.UpcomingWorkJobLinkAjaxDto;
import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.db.UpcomingWorkJobLinkService;
import org.trescal.cwms.core.system.entity.upcomingwork.db.UpcomingWorkService;

import java.util.List;

@RestController
@RequestMapping("upcomingWorkJobLink")
public class UpcomingWorkJobLinkController {

    private final UpcomingWorkJobLinkService upcomingWorkJobLinkService;
    private final UpcomingWorkService upcomingWorkService;

    public UpcomingWorkJobLinkController(UpcomingWorkJobLinkService upcomingWorkJobLinkService,
                                         UpcomingWorkService upcomingWorkService) {
        this.upcomingWorkJobLinkService = upcomingWorkJobLinkService;
        this.upcomingWorkService = upcomingWorkService;
    }

    @GetMapping
    public List<UpcomingWorkJobLinkAjaxDto> get(@RequestParam(value = "upcomingWorkId") Integer upcomingWorkId){

        return this.upcomingWorkJobLinkService.getJobByUpcomingWork(this.upcomingWorkService.get(upcomingWorkId));

    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity add(@RequestBody List<UpcomingWorkJobLinkAjaxDto> upcomingWorkJobLinkAjaxDtos ){
        try {

            Integer upcomingWorkId = upcomingWorkJobLinkAjaxDtos.stream()
                    .findFirst()
                    .map(UpcomingWorkJobLinkAjaxDto::getUpcomingWorkId)
                    .orElseThrow(Exception::new);

            this.upcomingWorkJobLinkService.updateJobToUpcomingWork(upcomingWorkJobLinkAjaxDtos,this.upcomingWorkService.get(upcomingWorkId));
            return ResponseEntity.status(HttpStatus.OK)
                    .body("Success" );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        }
    }

    @DeleteMapping
    public ResponseEntity delete(@RequestParam(value = "upcomingWorkJobLinkId") Integer upcomingWorkJobLinkId){
        try {
            this.upcomingWorkJobLinkService.deleteUpcomingWorkJobLink(upcomingWorkJobLinkId);
            return ResponseEntity.status(HttpStatus.OK)
                    .body("Success" );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        }
    }
}
