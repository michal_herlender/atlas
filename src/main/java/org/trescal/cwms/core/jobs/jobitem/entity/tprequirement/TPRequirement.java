package org.trescal.cwms.core.jobs.jobitem.entity.tprequirement;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Entity representing a {@link JobItem}'s third party requirements.
 *
 * @author jamiev
 */
@Entity
@Table(name = "tprequirement")
public class TPRequirement extends Auditable {
	private boolean adjustment;
	private boolean calibration;
	private Date created;
	private JobDeliveryItem deliveryItem;
	private int id;
	private boolean investigation;
	private JobItem ji;
	private Date lastUpdated;
	private ActionOutcome outcome;
	private TPQuotation quote;
	private Contact recordedBy;
	private boolean repair;
	private Contact updatedBy;

	@Column(name = "created", columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreated()
	{
		return this.created;
	}
	
	@OneToOne(fetch = FetchType.LAZY, mappedBy="tpRequirement", orphanRemoval=true	, cascade = {CascadeType.PERSIST})
	public JobDeliveryItem getDeliveryItem()
	{
		return this.deliveryItem;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid")
	public JobItem getJi()
	{
		return this.ji;
	}

	@Column(name = "lastupdated", columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastUpdated()
	{
		return this.lastUpdated;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "actionoutcomeid")
	public ActionOutcome getOutcome()
	{
		return this.outcome;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tpquoteid")
	public TPQuotation getQuote()
	{
		return this.quote;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "recordedbyid")
	public Contact getRecordedBy()
	{
		return this.recordedBy;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "updatedbyid")
	public Contact getUpdatedBy()
	{
		return this.updatedBy;
	}

	@NotNull
	@Column(name = "adjustment", nullable = false, columnDefinition="tinyint")
	public boolean isAdjustment()
	{
		return this.adjustment;
	}

	@NotNull
	@Column(name = "calibration", nullable = false, columnDefinition="tinyint")
	public boolean isCalibration()
	{
		return this.calibration;
	}

	@NotNull
	@Column(name = "investigation", nullable = false, columnDefinition="tinyint")
	public boolean isInvestigation()
	{
		return this.investigation;
	}

	@NotNull
	@Column(name = "repair", nullable = false, columnDefinition="tinyint")
	public boolean isRepair()
	{
		return this.repair;
	}

	public void setAdjustment(boolean adjustment)
	{
		this.adjustment = adjustment;
	}

	public void setCalibration(boolean calibration)
	{
		this.calibration = calibration;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public void setDeliveryItem(JobDeliveryItem deliveryItem)
	{
		this.deliveryItem = deliveryItem;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setInvestigation(boolean investigation)
	{
		this.investigation = investigation;
	}

	public void setJi(JobItem ji)
	{
		this.ji = ji;
	}

	public void setLastUpdated(Date lastUpdated)
	{
		this.lastUpdated = lastUpdated;
	}

	public void setOutcome(ActionOutcome outcome)
	{
		this.outcome = outcome;
	}

	public void setQuote(TPQuotation quote)
	{
		this.quote = quote;
	}

	public void setRecordedBy(Contact recordedBy)
	{
		this.recordedBy = recordedBy;
	}

	public void setRepair(boolean repair)
	{
		this.repair = repair;
	}

	public void setUpdatedBy(Contact updatedBy)
	{
		this.updatedBy = updatedBy;
	}
}