package org.trescal.cwms.core.jobs.jobitem.form;

import java.util.Map;

import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

import lombok.Data;

@Data
public class ReturnToAddressForm {

	private Job job;
	private Map<Integer, Boolean> jobitemids;
	@NotNull(message = "{error.select.address}")
	private Integer addrid;
	private Integer personid;
	private JobItem srcJobitem;

}