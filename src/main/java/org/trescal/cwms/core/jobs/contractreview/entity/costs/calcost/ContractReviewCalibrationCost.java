package org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.base.CalCost;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costs.base.WithCostSource;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

import lombok.Setter;

@Entity
@Setter
@DiscriminatorValue("contract")
public class ContractReviewCalibrationCost extends CalCost implements WithCostSource<ContractReviewLinkedCalibrationCost> {

	private JobItem jobitem;
	private ContractReviewLinkedCalibrationCost linkedCost;
	private SupportedCurrency currency;

	@OneToOne(mappedBy = "calibrationCost")
	public JobItem getJobitem() {
		return this.jobitem;
	}

	@OneToOne(mappedBy = "calibrationCost", cascade = CascadeType.ALL, orphanRemoval = true)
	public ContractReviewLinkedCalibrationCost getLinkedCost() {
		return this.linkedCost;
	}

	@Override
	@Transient
	public Cost getLinkedCostSrc() {
		return null;
	}


	public void setLinkedCost(ContractReviewLinkedCalibrationCost linkedCost) {
		this.linkedCost = linkedCost;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency_currencyid")
	public SupportedCurrency getCurrency() {
		return currency;
	}

}