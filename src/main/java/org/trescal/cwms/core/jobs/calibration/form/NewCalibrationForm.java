package org.trescal.cwms.core.jobs.calibration.form;

import java.util.List;

import javax.validation.Valid;

import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.system.dto.InstructionDTO;

/*
 * Note: Began to separate the attributes of a "New" calibration from a "Completed" calibration
 *       as they are comingled in the legacy CalibrationForm.
 * GB 2015-12-16
 */
public class NewCalibrationForm extends CalibrationForm {

	private Calibration cal; // @Valid on getter
	private Boolean startFromTool;
	private List<InstructionDTO> contractInstructions;

	@Valid
	public Calibration getCal() {
		return cal;
	}

	public void setCal(Calibration cal) {
		this.cal = cal;
	}

	public Boolean getStartFromTool() {
		return startFromTool;
	}

	public void setStartFromTool(Boolean startFromTool) {
		this.startFromTool = startFromTool;
	}

	public List<InstructionDTO> getContractInstructions() {
		return contractInstructions;
	}

	public void setContractInstructions(List<InstructionDTO> contractInstructions) {
		this.contractInstructions = contractInstructions;
	}
	
}