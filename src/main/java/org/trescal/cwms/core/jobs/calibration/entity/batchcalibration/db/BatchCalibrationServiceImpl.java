package org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.db;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.clientdelivery.ClientDelivery;
import org.trescal.cwms.core.deliverynote.entity.courierdespatch.db.CourierDespatchService;
import org.trescal.cwms.core.deliverynote.entity.courierdespatchtype.CourierDespatchType;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumentPercentageService;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationToBatch;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.comparators.CalibrationJobItemNoComparator;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.comparators.CalibrationSerialNoComparator;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalibrationToCalLinkComparator;
import org.trescal.cwms.core.jobs.calibration.entity.callink.db.CalLinkService;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsedComparator;
import org.trescal.cwms.core.jobs.calibration.enums.BatchCalibrationInvoiceType;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.jobs.calibration.form.NewBatchCalibrationForm;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.invoice.entity.invoicetype.InvoiceTypeNames;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;

import javax.servlet.http.HttpSession;
import java.util.*;

@Service("BatchCalibrationService")
public class BatchCalibrationServiceImpl implements BatchCalibrationService {

	@Autowired
	private BatchCalibrationDao batchCalibrationDao;
	@Autowired
	private CalLinkService calLinkServ;
    @Autowired
    private CalibrationProcessService calProcessServ;
    @Autowired
    private CalibrationService calServ;
    @Autowired
    private CourierDespatchService cdServ;
    @Autowired
    private DeliveryService delServ;
    @Value("#{props['cwms.config.costs.invoice.one_invoice_per_item']}")
    private boolean invoiceItemOnlyOnce;
    @Autowired
    private JobItemService jiServ;
    @Autowired
    private CapabilityAuthorizationService procAccServ;
    @Autowired
    private CapabilityService procServ;
    @Autowired
    private ScheduleService scheduleServ;
    @Autowired
    private UserService userService;
    @Autowired
    private StatusService statusServ;
    @Autowired
    private InstrumentPercentageService instrumentPercentageService;

    @Override
    public BatchCalibration addCalibrationsToBatch(NewBatchCalibrationForm form, Subdiv allocatedSubdiv) {
        BatchCalibration batchCal = form.getExistingBatch();
        this.createCalibrationsForBatch(batchCal, form.getItemCals(), allocatedSubdiv);
        return batchCal;
    }

    @Override
    public ResultWrapper ajaxCheckItemsToInvoice(Integer[] batchIds, BatchCalibrationInvoiceType invType)
	{
		String warning = "The following items have already been invoiced and will not be included on the created invoice:<br/><br/>";
		boolean safeToInvoice = true;
		List<Integer> batchIdList = Arrays.asList(batchIds);

		if (this.invoiceItemOnlyOnce)
		{
			List<JobItem> invoicedItems = this.getItemsAlreadyInvoiced(batchIdList, invType);

			for (JobItem ji : invoicedItems)
			{
				safeToInvoice = false;
				warning = warning.concat("Job " + ji.getJob().getJobno()
						+ " - Item " + ji.getItemNo() + "<br/>");
			}
		}

		return new ResultWrapper(safeToInvoice, warning);
	}

	@Override
	public boolean batchCalibrationCanBeDeleted(BatchCalibration batchCal) {
		return !this.batchHasStartedCalibrations(batchCal);
	}

	@Override
	public boolean batchHasStartedCalibrations(BatchCalibration batchCal)
	{
		for (Calibration cal : batchCal.getCals())
		{
			if (!cal.getStatus().getName().equalsIgnoreCase("awaiting work"))
			{
				return true;
			}
		}

		return false;
	}

	@Override
	public BatchCalibration createBatchCalibration(NewBatchCalibrationForm form, Subdiv allocatedSubdiv) {
		CalibrationProcess process = this.calProcessServ.get(form.getCalProcessId());

		BatchCalibration batchCal = new BatchCalibration();
		batchCal.setJob(form.getJob());
		batchCal.setBeginTime(new Date());
		batchCal.setOwner(form.getOwner());
		batchCal.setPasswordAccepted((form.isAutoPrinting()));
		batchCal.setChooseNext(!process.getApplicationComponent() || form.isManualSelect());
		batchCal.setCals(new ArrayList<>());
		batchCal.setProcess(process);
		batchCal.setCalDate(form.getCalDate());

		this.insertBatchCalibration(batchCal);

		this.createCalibrationsForBatch(batchCal, form.getItemCals(), allocatedSubdiv);

		return batchCal;
	}
	
	private void createCalibrationsForBatch(BatchCalibration batchCal, List<CalibrationToBatch> itemCals, Subdiv allocatedSubdiv) {
		int order = batchCal.getCals().size() + 1;
		List<Calibration> cals = new ArrayList<>();
		for (CalibrationToBatch ctb : itemCals) {
			if (ctb.isToBatch()) {
                Calibration cal = new Calibration();
                cal.setOrganisation(allocatedSubdiv);
                cal.setBatch(batchCal);
                cal.setCalAddress(ctb.getJi().getCalAddr());
                cal.setCalType(ctb.getJi().getServiceType().getCalibrationType());
                cal.setCapability(this.procServ.get(ctb.getProcId()));
                cal.setCalProcess(batchCal.getProcess());
                cal.setCalDate(DateTools.dateToLocalDate(batchCal.getCalDate()));
                cal.setCalClass(CalibrationClass.AS_FOUND);

                if (cal.getCalProcess().getApplicationComponent()) {
                    cal.setCertTemplate(ctb.getCertTemplate());
                }

                cal.setStatus((CalibrationStatus) this.statusServ.findStatusByName("Awaiting work", CalibrationStatus.class));
                cal.setBatchPosition(order);

				Set<StandardUsed> standards = new TreeSet<>(new StandardUsedComparator());
				for (Instrument std : ctb.getStandards()) {
					StandardUsed su = new StandardUsed();
					su.setCalibration(cal);
					su.setInst(std);

					// store data as a snapshot for how long this instrument had
					// left in cal at the time of being used for the calibration
                    su.setNextCalDueDate(std.getNextCalDueDate());
                    su.setCalPercentage(instrumentPercentageService.totalCalibrationPercentage(std.getNextCalDueDate(), std.getCalFrequencyUnit(), std.getCalFrequency()));

					standards.add(su);
				}
				cal.setStandardsUsed(standards);

				this.calServ.insertCalibration(cal);
				CalLink calLink = new CalLink();

				// reload job item here so that collections can be accessed when
				// creating the entry in the xindice db
				calLink.setJi(this.jiServ.findJobItem(ctb.getJi().getJobItemId()));

				calLink.setCal(cal);
				this.calLinkServ.insertCalLink(calLink);

				// add the cal link to the cal for when the begin calibration
				// hook runs
				if (cal.getLinks() == null) {
					cal.setLinks(new TreeSet<>(new CalibrationToCalLinkComparator()));
				}
				cal.getLinks().add(calLink);

				this.calServ.updateCalibration(cal);

				cals.add(cal);
				order++;
			}
		}

		batchCal.setCals(cals);
		this.updateBatchCalibration(batchCal);
	}

	@Override
	public void deleteBatchCalibration(BatchCalibration batchcalibration)
	{
		this.batchCalibrationDao.remove(batchcalibration);
	}

	@Override
	public ResultWrapper deleteBatchCalibrationWithChecks(int id)
	{
		BatchCalibration batchCal = this.findBatchCalibration(id);

		// check that batch calibration has no calibrations that have been
		// started
		if (this.batchHasStartedCalibrations(batchCal))
		{
			return new ResultWrapper(false, "The batch calibration has calibrations that have been started");
		}

		// for each calibration in the batch
		for (Calibration cal : batchCal.getCals())
		{
			// decrement standard counters if necessary
			this.calServ.decrementStandardCounters(cal, batchCal.getBeginTime());
		}

		// delete the batch
		this.deleteBatchCalibration(batchCal);

		return new ResultWrapper(true, "Batch calibration successfully deleted");
	}

	@Override
	public ResultWrapper deleteCalibrationFromBatch(int calId, HttpSession session)
	{
		Calibration cal = this.calServ.findCalibration(calId);
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact contact = this.userService.get(username).getCon();

		// get the status of the calibration
		String status = cal.getStatus().getName();

		// if cal is finished
		/*
		 * if (!status.equalsIgnoreCase("awaiting work") &&
		 * !status.equalsIgnoreCase("on-going")) { return new
		 * ResultWrapper(false,
		 * "The calibration can not be deleted because it has been completed");
		 * }
		 */

		// move to the end (bottom) of the batch
		BatchCalibration batch = cal.getBatch();
		this.calServ.changeCalibrationPositionInBatch(batch.getId(), cal.getBatchPosition(), batch.getCals().size());

		// remove from batch
		batch.getCals().remove(cal);
		cal.setBatch(null);
		this.updateBatchCalibration(batch);

		// if cal has certs (e.g. Word cal that has issued cert number)
		if ((!status.equalsIgnoreCase("awaiting work") && !status.equalsIgnoreCase("on-going"))
				|| (cal.getCerts().size() > 0))
		{
			// cancel the cal, don't delete it
			this.calServ.cancelCalibration(cal.getId(), contact);

			return new ResultWrapper(true, "Calibration cancelled and removed from batch");
		}
		// else all evidence can be erased
		else
		{
			// decrement standard counters if necessary
			this.calServ.decrementStandardCounters(cal, batch.getBeginTime());

			// delete calibration
			this.calServ.deleteCalibration(cal, contact);

			return new ResultWrapper(true, "Calibration successfully deleted from batch");
		}
	}
	
	@Override
	public void despatchBatchCalibrations(List<Integer> batchIds, CourierDespatchType cdt, String consignmentNo, Contact creator, Subdiv allocatedSubdiv, List<String> sessionNewFileList) throws Exception {
		List<JobItem> jobItems = this.getDistinctItemsFromBatches(batchIds);
		if (jobItems == null || jobItems.size() == 0) {
			throw new Exception("No Items to despatch!");
		}
		// create delivery
		ClientDelivery delivery = this.delServ.createDeliveryForJobItems(jobItems, creator, allocatedSubdiv);
		// get mode of transport
		String transportMethod = delivery.getJob().getReturnOption().getMethod().getMethodText();
		// if this transport mode requires an entry on the scheduler
		if (transportMethod.equalsIgnoreCase("round robin") || transportMethod.equalsIgnoreCase("local"))
			// add to schedule
			this.scheduleServ.addDeliveryToSchedule(delivery, allocatedSubdiv);
			// else if this transport mode requires a courier despatch entry
			// TODO: I think this case will never be reached (no entry in database is "courier")
			// 		 only "Client Courier", "Courier - Next Day" and "Courier - Specific"
		else if (transportMethod.equalsIgnoreCase("courier"))
			// create courier despatch
			this.cdServ.addCourierDespatchForDelivery(delivery, cdt, consignmentNo, creator, allocatedSubdiv);
		// generate and pring delivery note - if reactivated and used, we should use the BIRT document
		// Document delNote = this.delServ.generateDeliveryNoteDocument(delivery.getDeliveryid(), creator, sessionNewFileList, allocatedSubdiv);
		// print delivery note
	}

	@Override
	public BatchCalibration findBatchCalibration(int id)
	{
		return this.batchCalibrationDao.find(id);
	}

	@Override
	public List<BatchCalibration> getAllBatchCalibrations()
	{
		return this.batchCalibrationDao.findAll();
	}

	@Override
	public List<Calibration> getAllNewCalibrationsInBatch(int batchId, int currentContactId) {
		BatchCalibration batchCal = this.findBatchCalibration(batchId);
		ArrayList<Calibration> newCalList = new ArrayList<>();

		// find next awaiting work
		for (Calibration cal : batchCal.getCals()) {
			if (cal.getStatus().getName().trim().equals("Awaiting work")) {
                // check accreditation
                int calTypeId = cal.getCalType().getCalTypeId();
                int procId = cal.getCapability().getId();
                val accStat = this.procAccServ.authorizedFor(procId, calTypeId, currentContactId).isRight();
                if (accStat) {
                    newCalList.add(cal);
                }
            }
		}

		return newCalList;
	}

	@Override
	public List<BatchCalibration> getBatchCalibrationsWithIds(List<Integer> batchIds)
	{
		return this.batchCalibrationDao.getBatchCalibrationsWithIds(batchIds);
	}

	@Override
	public List<JobItem> getDistinctItemsFromBatches(List<Integer> batchIds)
	{
		return this.batchCalibrationDao.getDistinctItemsFromBatches(batchIds);
	}

	@Override
	public List<JobItem> getItemsAlreadyInvoiced(List<Integer> batchIds, BatchCalibrationInvoiceType invType) {
		Collection<JobItem> jobItems;
		List<JobItem> alreadyInvoiced = new ArrayList<>();

		if (invType == BatchCalibrationInvoiceType.ALL_JOBITEMS) {
			BatchCalibration batchCal = this.findBatchCalibration(batchIds.get(0));
			jobItems = batchCal.getJob().getItems();
		} else {
			jobItems = this.getDistinctItemsFromBatches(batchIds);
		}

		for (JobItem ji : jobItems)
		{
			for (InvoiceItem ii : ji.getInvoiceItems())
			{
				if (!ii.getInvoice().getType().getName().equals(InvoiceTypeNames.PRO_FORMA))
				{
					alreadyInvoiced.add(ji);
				}
			}
		}

		return alreadyInvoiced;
	}

	@Override
	public Calibration getNextIncompleteCalibrationInBatch(int batchId, int currentContactId)
	{
		BatchCalibration batchCal = this.findBatchCalibration(batchId);
		Calibration nextCal = null;

		// find next on-going
		for (Calibration cal : batchCal.getCals())
		{
			if (cal.getStatus().getName().trim().equals("On-going")) {
                // check accreditation
                int calTypeId = cal.getCalType().getCalTypeId();
                int procId = cal.getCapability().getId();
                val accStat = this.procAccServ.authorizedFor(procId, calTypeId, currentContactId).isRight();
                if (accStat) {
                    nextCal = cal;
                    break;
                }
            }
		}

		// if still no cal
		if (nextCal == null)
		{
			// find next awaiting work
			for (Calibration cal : batchCal.getCals())
			{
				if (cal.getStatus().getName().trim().equals("Awaiting work")) {
                    // check accreditation
                    int calTypeId = cal.getCalType().getCalTypeId();
                    int procId = cal.getCapability().getId();
                    val accStat = this.procAccServ.authorizedFor(procId, calTypeId, currentContactId).isRight();
                    if (accStat) {
                        nextCal = cal;
                        break;
                    }
                }
			}
		}

		return nextCal;
	}

	@Override
	public List<BatchCalibration> getOtherCompleteBatchesOnJob(int jobId, int batchId)
	{
		return this.batchCalibrationDao.getOtherCompleteBatchesOnJob(jobId, batchId);
	}

	@Override
	public void insertBatchCalibration(BatchCalibration BatchCalibration)
	{
		this.batchCalibrationDao.persist(BatchCalibration);
	}
	
	@Override
	public void mergeBatchCalibration(BatchCalibration BatchCalibration)
	{
		this.batchCalibrationDao.merge(BatchCalibration);
	}

	@Override
	public ResultWrapper orderCalibrationsOnBatch(boolean bySerial, int batchId)
	{
		// first find batch calibration
		BatchCalibration batchCal = this.findBatchCalibration(batchId);
		// batch calibration null?
		if (batchCal != null) {
			// create temporary list of calibrations to be re-ordered
			// add all calibrations from batch
			List<Calibration> cals = new ArrayList<>(batchCal.getCals());
			// order by serial number?
			if (bySerial) {
				// order by serial number
				cals.sort(new CalibrationSerialNoComparator());
			} else {
				// order by default item no
				cals.sort(new CalibrationJobItemNoComparator());
			}
			// new batch position counter
			int batchpos = 1;
			// adjust batch position accordingly
			for (Calibration cal : cals)
			{
				// update batch calibration position
				cal.setBatchPosition(batchpos);
				// update calibration
				this.calServ.updateCalibration(cal);
				// increment batch position
				batchpos++;
			}
			// return successful wrapper
			return new ResultWrapper(true, "", null, null);
		}
		else
		{
			// return un-successful result wrapper
			return new ResultWrapper(false, "The batch calibration could not be found", null, null);
		}
	}

	@Override
	public HashSet<Integer> removeItemsNotCalibratedFromInvoice(HashSet<Integer> jobitems) {
		List<Integer> toRemove = new ArrayList<>();

		// for each jobitem
		for (Integer jiId : jobitems) {
			JobItem ji = this.jiServ.findJobItem(jiId);

			// loop through actions
			for (JobItemAction action : ji.getActions()) {
				if (action instanceof JobItemActivity)
				{
					JobItemActivity jia = (JobItemActivity) action;

					for (StateGroupLink sgl : jia.getActivity().getGroupLinks())
					{
						// if the item has a calibration activity
						if (sgl.getGroup().equals(StateGroup.CALIBRATION)
								&& !jia.isDeleted())
						{
							// if the outcome was a failure
							if (!jia.getOutcome().isPositiveOutcome())
							{
								// add the item to the list of items to remove
								toRemove.add(ji.getJobItemId());
							}
							else
							{
								// if the item was successfully calibrated at a
								// later time but has already been added to the
								// removal list
								if (toRemove.contains(ji.getJobItemId()))
								{
									// remove it from the removal list!
									toRemove.remove((Integer) ji.getJobItemId());
								}
							}
						}
					}
				}
			}
		}

		// remove everything on the removal list
        toRemove.forEach(jobitems::remove);

		return jobitems;
	}

	@Override
	public void saveOrUpdateBatchCalibration(BatchCalibration batchcalibration) {
		this.batchCalibrationDao.merge(batchcalibration);
	}

	@Override
	public void setChooseNext(int batchId, boolean chooseNext)
	{
		BatchCalibration batchCal = this.findBatchCalibration(batchId);
		batchCal.setChooseNext(chooseNext);
		this.updateBatchCalibration(batchCal);
	}

	public void updateBatchCalibration(BatchCalibration BatchCalibration) {
		this.batchCalibrationDao.merge(BatchCalibration);
	}
}