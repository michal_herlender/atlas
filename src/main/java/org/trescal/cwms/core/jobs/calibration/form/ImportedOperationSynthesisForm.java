package org.trescal.cwms.core.jobs.calibration.form;

import java.util.List;

import javax.validation.Valid;

import org.trescal.cwms.batch.jobs.operationsimport.ImportedOperationItem;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @Builder @NoArgsConstructor @AllArgsConstructor
public class ImportedOperationSynthesisForm {

	private Integer jobId;
	private Integer subdivid;
	private Integer exchangeFormatId;
	@Valid
	private List<ImportedOperationItem> rows;

}
