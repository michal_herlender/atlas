package org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.db;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqEdit;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqFallback;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqHistory;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqOverride;

@Repository("CalReqHistoryDao")
public class CalReqHistoryDaoImpl extends BaseDaoImpl<CalReqHistory, Integer> implements CalReqHistoryDao {
	
	@Override
	protected Class<CalReqHistory> getEntity() {
		return CalReqHistory.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<CalReqEdit> findAllEditsForCalReq(CalReq cr)
	{
		Criteria crit = this.getSession().createCriteria(CalReqEdit.class);
		crit.createCriteria("newCalReq", "ncr", JoinType.LEFT_OUTER_JOIN);
		crit.createCriteria("oldCalReq", "ocr", JoinType.LEFT_OUTER_JOIN);
		Criterion lhs = Restrictions.eq("ncr.id", cr.getId());
		Criterion rhs = Restrictions.eq("ocr.id", cr.getId());
		crit.add(Restrictions.or(lhs, rhs));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CalReqFallback> findFallbacksForInstrument(int plantId) {
		Criteria crit = this.getSession().createCriteria(CalReqFallback.class);
		crit.createCriteria("fallbackFromInst").add(Restrictions.idEq(plantId));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CalReqFallback> findFallbacksForJobItem(int jobItemId) {
		Criteria crit = this.getSession().createCriteria(CalReqFallback.class);
		crit.createCriteria("fallbackFromJobItem").add(Restrictions.idEq(jobItemId));
		return crit.list();
	}
	
	public CalReqHistory findHistoryForCalReqBetweenDates(CalReq cr, Date from, Date to, Class<? extends CalReqHistory> clazz, boolean future) {
		Criteria crit = this.getSession().createCriteria(clazz);
		if (future) crit.createCriteria("oldCalReq").add(Restrictions.idEq(cr.getId()));
		else crit.createCriteria("newCalReq").add(Restrictions.idEq(cr.getId()));
		if (from == null) crit.add(Restrictions.le("changeOn", to));
		else if (to == null) crit.add(Restrictions.ge("changeOn", from));
		else crit.add(Restrictions.between("changeOn", from, to));
		return (CalReqHistory) crit.uniqueResult();
	}
	
	public CalReqHistory findOverrideOrFallbackForCalReqBetweenDates(CalReq cr, Date from, Date to) {
		Criteria crit = this.getSession().createCriteria(CalReqOverride.class);
		crit.createCriteria("newCalReq").add(Restrictions.idEq(cr.getId()));
		if (from == null) crit.add(Restrictions.le("changeOn", to));
		else if (to == null) crit.add(Restrictions.ge("changeOn", from));
		else crit.add(Restrictions.between("changeOn", from, to)); 
		CalReqHistory overOrFall = (CalReqHistory) crit.uniqueResult();
		if (overOrFall == null) {
			crit = this.getSession().createCriteria(CalReqFallback.class);
			crit.createCriteria("oldCalReq").add(Restrictions.idEq(cr.getId()));
			if (from == null) crit.add(Restrictions.le("changeOn", to));
			else if (to == null) crit.add(Restrictions.ge("changeOn", from));
			else crit.add(Restrictions.between("changeOn", from, to));
			overOrFall = (CalReqHistory) crit.uniqueResult();
		}
		if ((overOrFall instanceof CalReqOverride)
				&& !(overOrFall.getNewCalReq().getClassKey().equalsIgnoreCase("instrument"))
				&& !(overOrFall.getNewCalReq().getClassKey().equalsIgnoreCase("jobitem")))
		{
			return overOrFall;
		}
		else if ((overOrFall instanceof CalReqFallback)
				&& !(overOrFall.getOldCalReq().getClassKey().equalsIgnoreCase("instrument"))
				&& !(overOrFall.getOldCalReq().getClassKey().equalsIgnoreCase("jobitem")))
		{
			return overOrFall;
		}
		else
		{
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CalReqOverride> findOverridesForInstrument(int plantId) {
		Criteria crit = this.getSession().createCriteria(CalReqOverride.class);
		crit.createCriteria("overrideOnInst").add(Restrictions.idEq(plantId));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CalReqOverride> findOverridesForJobItem(int jobItemId) {
		Criteria crit = this.getSession().createCriteria(CalReqOverride.class);
		crit.createCriteria("overrideOnJobItem").add(Restrictions.idEq(jobItemId));
		return crit.list();
	}
}