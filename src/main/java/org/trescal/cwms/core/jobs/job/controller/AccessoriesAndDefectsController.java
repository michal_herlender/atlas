package org.trescal.cwms.core.jobs.job.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.Accessory;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.db.AccessoryService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.db.JobItemGroupService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Controller @IntranetController
public class AccessoriesAndDefectsController
{
	@Autowired
	private AccessoryService accessoryService;
	@Autowired
	private JobItemGroupService groupServ;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private JobService jobServ;
	@Autowired
	private TranslationService translationService;
	
	@ModelAttribute("accessories")
	public List<Accessory> getAccessories() {
		Locale locale = LocaleContextHolder.getLocale();
		return accessoryService.getAll().stream()
				.sorted((acc1, acc2) -> translationService.getCorrectTranslation(acc1.getTranslations(), locale).toLowerCase()
						.compareTo(translationService.getCorrectTranslation(acc2.getTranslations(), locale).toLowerCase()))
				.collect(Collectors.toList());
	}
	
	@RequestMapping(value="/accessoriesanddefects.htm")
	protected ModelAndView handleRequestInternal(@RequestParam(value="jobid", required=false, defaultValue="0") Integer jobId,
												 @RequestParam(value="jobitemid", required=false, defaultValue="0") Integer itemId,
												 @RequestParam(value="groupid", required=false, defaultValue="0") Integer groupId) throws Exception {
		if (jobId == 0) throw new Exception("Job not found");
		Map<String, Object> model = new HashMap<>();
		model.put("job", this.jobServ.get(jobId));
		if (itemId != 0) model.put("selectedItem", this.jiServ.findJobItem(itemId));
		else if (groupId != 0) model.put("selectedGroup", this.groupServ.findJobItemGroup(groupId));
		return new ModelAndView("trescal/core/jobs/job/accessoriesanddefects", "model", model);
	}
}