package org.trescal.cwms.core.jobs.jobitem.projection.service;

import java.util.Locale;

import org.trescal.cwms.core.jobs.jobitem.dto.JobItemViewWrapper;

/**
 * Used by job item controllers 
 */
public interface JobItemViewService {

	JobItemViewWrapper getViewWrapper(Integer jobItemId, Locale locale);

}
