package org.trescal.cwms.core.jobs.job.form.genericentityvalidator;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

/**
 * Generic validator for validating {@link BPO} entities. Extends
 * {@link AbstractEntityValidator} to provide support for dwr based validation
 * which requires access to the resulting {@link BindException}.
 * 
 * @author jamiev
 */
public class BPOValidator extends AbstractBeanValidator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(BPO.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		BPO bpo = (BPO) target;
		super.validate(bpo, errors);

		// if start date exists, so should end
		if (bpo.getDurationFrom() != null)
		{
			if (bpo.getDurationTo() == null)
			{
				errors.rejectValue("durationTo", null, "There must be an end date if there is a start date.");
			}
		}

		// if end date exists, so should start
		if (bpo.getDurationTo() != null)
		{
			if (bpo.getDurationFrom() == null)
			{
				errors.rejectValue("durationFrom", null, "There must be a start date if there is an expiry date.");
			}
		}

		// end date cannot be before the start date
		if ((bpo.getDurationFrom() != null) && (bpo.getDurationTo() != null))
		{
			if (!bpo.getDurationTo().after(bpo.getDurationFrom()))
			{
				errors.rejectValue("durationTo", null, "The expiry date must be after the start date.");
			}
		}
	}
}
