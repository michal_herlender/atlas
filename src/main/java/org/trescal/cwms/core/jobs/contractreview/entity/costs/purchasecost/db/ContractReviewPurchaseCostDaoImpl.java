package org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost;

@Repository("ContractReviewPurchaseCostDao")
public class ContractReviewPurchaseCostDaoImpl extends BaseDaoImpl<ContractReviewPurchaseCost, Integer> implements ContractReviewPurchaseCostDao {
	
	@Override
	protected Class<ContractReviewPurchaseCost> getEntity() {
		return ContractReviewPurchaseCost.class;
	}
}