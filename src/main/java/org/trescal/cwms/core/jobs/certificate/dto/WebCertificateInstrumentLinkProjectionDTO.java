package org.trescal.cwms.core.jobs.certificate.dto;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.description.SubfamilyTypology;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import javax.persistence.Index;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Date;

@Table(indexes = {
    @Index(name = "idx_webcertificateinstrumentlinkprojectiondto", columnList = "calibrationInterval")
})
@Getter
@Setter
public class WebCertificateInstrumentLinkProjectionDTO {
	
	private Integer certId;
	private Integer plantid;
	private String plantno;
	private String serialno;
	private String complementaryFieldCustomerAcceptanceCriteria;
	private Boolean scrapped;
	private String customerDescription;
	private String instModelName;
	private String modelName;
    private SubfamilyTypology subfamilyTypology;
    private InstrumentStatus status;
    private ModelMfrType modelMfrType;
    private String mfrName;
    private Boolean mfrGeneric;
    private Integer contactId;
    private Integer addressId;
    private Integer subdivId;
    private LocalDate addedOn;
    private LocalDate nextCalDueDate;
    private Integer calibrationInterval;
    private IntervalUnit calibrationIntervalUnit;
    private Date lastCalCompleteTime;

    public WebCertificateInstrumentLinkProjectionDTO(Integer certId, Integer plantid, String plantno, String serialno,
                                                     String complementaryFieldCustomerAcceptanceCriteria, Boolean scrapped, String customerDescription,
                                                     String instModelName, String modelName, Integer typology, ModelMfrType modelMfrType, String mfrName,
                                                     Boolean mfrGeneric, InstrumentStatus status, LocalDate addedOn, LocalDate nextCalDueDate, Date lastCalCompleteTime,
                                                     Integer contactId, Integer addressId, Integer subdivId, Integer calibrationInterval, IntervalUnit calibrationIntervalUnit) {
        super();
        this.certId = certId;
        this.plantid = plantid;
        this.plantno = plantno;
        this.serialno = serialno;
        this.complementaryFieldCustomerAcceptanceCriteria = complementaryFieldCustomerAcceptanceCriteria;
        this.scrapped = scrapped;
        this.customerDescription = customerDescription;
        this.instModelName = instModelName;
        this.modelName = modelName;
		this.subfamilyTypology = SubfamilyTypology.convertFromInteger(typology);
        this.modelMfrType = modelMfrType;
        this.mfrName = mfrName;
        this.mfrGeneric = mfrGeneric;
        this.status = status;
        this.addedOn = addedOn;
        this.nextCalDueDate = nextCalDueDate;
        this.lastCalCompleteTime = lastCalCompleteTime;
        this.contactId = contactId;
        this.addressId = addressId;
        this.subdivId = subdivId;
        this.calibrationInterval = calibrationInterval;
        this.calibrationIntervalUnit = calibrationIntervalUnit;
    }
	
	
}
