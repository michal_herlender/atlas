package org.trescal.cwms.core.jobs.job.entity.job.db;

import io.vavr.control.Either;
import org.springframework.ui.Model;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.WebPermissionException;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.calibration.controller.ActiveInstrumentsJsonController.ActiveInstrumentDto;
import org.trescal.cwms.core.jobs.job.controller.GenerateClientFeedbackRequestIn;
import org.trescal.cwms.core.jobs.job.dto.JobKeyValue;
import org.trescal.cwms.core.jobs.job.dto.JobSearchResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.abstractpo.AbstractPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.form.EditJobForm;
import org.trescal.cwms.core.jobs.job.form.JobSearchForm;
import org.trescal.cwms.core.jobs.job.form.OldJobSearchForm;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;
import org.trescal.cwms.core.logistics.form.JobFinalSynthesisDTOForm;
import org.trescal.cwms.core.system.dto.EmailResultWrapper;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.vdi.projection.OrderProjection;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.web.home.dto.JobStats;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.math.BigDecimal;
import java.util.*;

/**
 * Interface for accessing and manipulating {@link Job} entities.
 *
 * @author jamiev
 */
public interface JobService extends BaseService<Job, Integer> {

	PagedResultSet<JobSearchResultWrapper> ajaxQueryJob(OldJobSearchForm jsf, int allocatedSubdivId, int resPerPage,
			int pageNo);

	PagedResultSet<JobSearchResultWrapper> ajaxQueryJobByCoid(int coid, int allocatedSubdivId, int resPerPage,
			int pageNo);

	Long countActiveByContactAndAllocatedSubdiv(Contact contact, Subdiv allocatedSubdiv);

	Long countByContact(Contact contact);

	Long countByContactAndAllocatedSubdiv(Contact contact, Subdiv allocatedSubdiv);

	Long countActiveBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv);

	Long countBySubdiv(Subdiv subdiv);

	Long countBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv);

	Long countActiveByCompanyAndAllocatedSubdiv(Company company, Subdiv allocatedSubdiv);

	Long countByCompany(Company company);

	Long countByCompanyAndAllocatedSubdiv(Company company, Subdiv allocatedSubdiv);

	/**
	 * Deletes the {@link Job} with the given ID from the database.
	 *
	 * @param id
	 *            the ID of the {@link Job} to delete.
	 */
	void deleteJobById(int id);

	ResultWrapper deleteJobWithChecks(int jobid, String reason, HttpSession session);

	void editJob(EditJobForm form, Contact editedBy);

	/**
	 * Returns a list of all {@link Job} entities that are ready to be invoiced
	 * that belong to the {@link Company} identified by the given id.
	 *
	 * @param coid
	 *            the {@link Company} id.
	 * @return {@link List} of matching {@link Job}.
	 */
	List<Job> findAllJobsReadyToInvoice(int coid);

	Job findByJobNo(String jobno);

	List<JobKeyValue> findByPartialJobNo(String partialJobNo);

	Job findEagerJobForView(int id);

	Job findWebSafeJob(int jobid) throws WebPermissionException;

	/**
	 * Get a distinct list of {@link Model}(s) that belong to the {@link Job}.
	 *
	 * @param job
	 *            the {@link Job} to get distinct models from.
	 * @return {@link Set}{@link Model}(s).
	 */
	Set<InstrumentModel> getDistinctModels(Job job);

	/**
	 * Returns the {@link Job} with the given Job No or null if there are no
	 * jobs with the given Job No.
	 *
	 * @param jobNo
	 *            the jobNo to match.
	 * @return the {@link Job}
	 */
	Job getJobByExactJobNo(String jobNo);

	/**
	 * Returns the path to the documents stored for the given {@link Job}.
	 *
	 * @param job
	 *            the {@link Job}
	 * @return the {@link File}
	 */
	File getJobPath(Job job);

	List<JobProjectionDTO> getJobProjectionDTOList(Collection<Integer> jobIds, boolean countJobItems, boolean countJobExpenseItems);


	@Deprecated
	ResultWrapper getJobsAndItemsNotInStateGroupForAddress(List<StateGroup> stateGroups, int addrId);

	/**
	 * Returns a list of all {@link Job} entites matching the given list of
	 * jobids.
	 *
	 * @param ids
	 *            {@link List} of ids.
	 * @return list of matching {@link Job} entities.
	 */
	List<Job> getJobsById(Collection<Integer> ids);

	/**
	 * Returns a {@link JobStats} DTO object containing counts of active items
	 * and jobs associated with the given IDs. Note: For this to return "real"
	 * statistics, the contact should be in the given subdiv, the subdiv in the
	 * given company, and the address in the given subdiv.
	 *
	 * @param personId
	 *            the {@link Contact} ID
	 * @param subdivId
	 *            the {@link Subdiv} ID
	 * @param compId
	 *            the {@link Company} ID
	 * @param addrId
	 *            the {@link Address} ID
	 * @return the {@link JobStats} containing the numbers
	 */
	JobStats getJobStats(int personId, int subdivId, int compId, int addrId);

	/**
	 * Returns the {@link PO} or {@link BPO} with the given PO number on the
	 * {@link Job} with the given ID, or null if there are no {@link BPO} or
	 * {@link PO} matches.
	 *
	 * @return the {@link PO} or {@link BPO}
	 */
	AbstractPO getMatchingJobPO(int jobId, String poNumber);

	/**
	 * Inserts the given {@link Job} into the database.
	 *
	 * @param j
	 *            the {@link Job} to insert.
	 */
	Integer insertJob(Job j, int subdivId);

	void mergeJob(Job j);

	/**
	 * Sets the {@link Job} with the given ID to complete if it is currently
	 * awaiting invoicing.
	 *
	 * @param jobId
	 *            the {@link Job} ID
	 * @return a {@link ResultWrapper} indicating the success of the operation
	 */
	ResultWrapper noInvoiceRequired(int jobId);

	/**
	 * Creates a job sheet document for the given {@link Job}, should be called
	 * from an ajax interface.
	 *
	 * @param jobid
	 *            the id of the {@link Job} to create the job sheet for.
	 * @return {@link ResultWrapper} indicating the sucess of this operation.
	 */
	ResultWrapper generateJobSheet(int jobid, HttpSession session);

	/**
	 * Creates a job sheet document for the given job, invoice address and
	 * carriage.
	 *
	 * @param job
	 *            the {@link Job} to create the job sheet for.
	 * @param address
	 *            the {@link Address} to appear as the invoice address.
	 * @param carriage
	 *            the carriage charge for the job.
	 * @return {@link Document} object indicating the location of the document
	 *         if it was sucessfully created.
	 */
	Document generateJobSheetInternal(Job job, Address address, BigDecimal carriage, HttpSession session);

	/**
	 * Returns all {@link Job} entities that match the criteria specified in the
	 * given {@link JobSearchForm} in a {@link PagedResultSet}
	 *
	 * @param jsf
	 *            the {@link JobSearchForm}
	 * @param rs
	 *            the {@link PagedResultSet}
	 * @return the {@link PagedResultSet}
	 *
	 * 2019-07-06 deprecating (poor performance), replacing with JPA query
	 */
	@Deprecated
	PagedResultSet<Job> queryJobOld(OldJobSearchForm jsf, PagedResultSet<Job> rs);

	void queryJobJPANew(JobSearchForm jsf, Locale locale, PagedResultSet<JobProjectionDTO> rs );

	/**
	 * Returns all {@link Job} entities that have a jobNo BEGINNING with the
	 * given jobNo parameter.
	 *
	 * @param jobNo
	 *            the jobNo to match.
	 * @return the {@link List} of {@link Job} entities.
	 */
	List<Job> searchJob(String jobNo);

	List<AbstractPO> searchMatchingJobPOs(int jobId, String partPoNumber);

	/**
	 * Updates the default {@link CalibrationType} of the {@link Job} with the
	 * given ID to the {@link CalibrationType} with the given ID.
	 *
	 * @param jobid
	 *            the {@link Job} ID.
	 * @param caltypeid
	 *            the {@link CalibrationType} ID.
	 */
	void updateDefaultCalType(int jobid, int caltypeid);

	/**
	 * Updates the default turnaround of the {@link Job} with the given ID to
	 * the given turnaround.
	 *
	 * @param jobid
	 *            the {@link Job} ID.
	 * @param turn
	 *            the new turnaround.
	 */
	void updateDefaultTurn(int jobid, int turn);

	List<Job> getActiveJobsBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv);

	Job createJobFromPrebookingOrFile(JobFinalSynthesisDTOForm jobFinalSynthDTOForm, Contact addedBy,
			Subdiv allocatedSubdiv);

	JobItem createNewRepairJobAndJobItem(JobItem oldJobItem, Date frApprovalOn, Contact con);

	void initializeJobItemsServiceTypes(AsnItemFinalSynthesisDTO dto,Map<Integer, String> servicesType,Integer serviceTypeId);

	Job addItemsToJobAlreadyExist(Job job, JobFinalSynthesisDTOForm jobFinalSynthDTOForm, Contact addedBy,
			Integer AsnId);


	Optional<Job> findJobByJobNumber(String jobNo);

	List<ActiveInstrumentDto> findActiveJobsByPlantids(List<String> plantIds);

	String getJobNo(Integer jobId);

	Either<String,EmailResultWrapper> generateClientFeedback(GenerateClientFeedbackRequestIn in, Contact contact);

	Optional<OrderProjection> getOrderProjectionForJob(Integer jobId);

	void modifyJobStatus(Job job) ;
}
