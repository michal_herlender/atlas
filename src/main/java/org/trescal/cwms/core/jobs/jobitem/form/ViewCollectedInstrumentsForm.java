package org.trescal.cwms.core.jobs.jobitem.form;

import java.util.List;

import org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.CollectedInstrument;

public class ViewCollectedInstrumentsForm
{
	private int coid;
	private List<CollectedInstrument> collectedInsts;
	private List<Integer> selectedInsts;

	public int getCoid()
	{
		return this.coid;
	}

	public List<CollectedInstrument> getCollectedInsts()
	{
		return this.collectedInsts;
	}

	public List<Integer> getSelectedInsts()
	{
		return this.selectedInsts;
	}

	public void setCoid(int coid)
	{
		this.coid = coid;
	}

	public void setCollectedInsts(List<CollectedInstrument> collectedInsts)
	{
		this.collectedInsts = collectedInsts;
	}

	public void setSelectedInsts(List<Integer> selectedInsts)
	{
		this.selectedInsts = selectedInsts;
	}
}