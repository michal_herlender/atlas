package org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportAction;

@Repository
public class FaultReportActionDaoImpl extends BaseDaoImpl<FaultReportAction, Integer> implements FaultReportActionDao {
	@Override
	protected Class<FaultReportAction> getEntity() {
		return FaultReportAction.class;
	}
}
