package org.trescal.cwms.core.jobs.job.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.tools.NumberTools;

@Component
public class JobSearchValidator implements Validator
{
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(JobSearchForm.class);
	}

	public void validate(Object obj, Errors errors)
	{
		JobSearchForm form = (JobSearchForm) obj;
		
		if (form.isBookedInDateBetween())
		{
			// if first date is given
			if (form.getBookedInDate1() != null)
			{
				// check second date is given
				if (form.getBookedInDate2() == null)
				{
					errors.rejectValue("bookedInDate2", "bookedInDate2", null, "Second booked in date has not been specified.");
				}
				else
				{
					// check second date is chronologically after the first date
					if (!form.getBookedInDate2().isAfter(form.getBookedInDate1())) {
						errors.rejectValue("bookedInDate2", "bookedInDate2", null, "Second booked in date must be after the first booked in date.");
					}
				}
			}
		}
		if (form.isCompletedDateBetween())
		{
			// if first date is given
			if (form.getCompletedDate1() != null)
			{
				// check second date is given
				if (form.getCompletedDate2() == null)
				{
					errors.rejectValue("completedDate2", "completedDate2", null, "Second completed date has not been specified.");
				}
				else {
					// check second date is chronologically after the first date

					if (!form.getCompletedDate2().isAfter(form.getCompletedDate1())) {
						errors.rejectValue("completedDate2", "completedDate2", null, "Second completed date must be after the first completed date.");
					}
				}
			}
		}

		// check barcode entered is a number
		if ((form.getPlantid() != null) && !form.getPlantid().trim().isEmpty()
				&& !NumberTools.isAnInteger(form.getPlantid().trim()))
		{
			errors.rejectValue("plantid", "plantid", null, "The barcode '"
					+ form.getPlantid() + "' specified is not valid.");
		}
	}
}