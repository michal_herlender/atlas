package org.trescal.cwms.core.jobs.jobitem.form;

import java.util.Map;

import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.CallOffItem;

public class JICallOffItemForm extends JobItemForm
{
	private CallOffItem callOffItem;
	private boolean incompleteActivity;
	private Map<Integer, Integer> jobItemIds;
	private Map<Integer, String> reasons;

	public CallOffItem getCallOffItem()
	{
		return this.callOffItem;
	}

	/**
	 * @return the jobItemIds
	 */
	public Map<Integer, Integer> getJobItemIds()
	{
		return this.jobItemIds;
	}

	/**
	 * @return the reasons
	 */
	public Map<Integer, String> getReasons()
	{
		return this.reasons;
	}

	/**
	 * @return the incompleteActivity
	 */
	public boolean isIncompleteActivity()
	{
		return this.incompleteActivity;
	}

	public void setCallOffItem(CallOffItem callOffItem)
	{
		this.callOffItem = callOffItem;
	}

	/**
	 * @param incompleteActivity the incompleteActivity to set
	 */
	public void setIncompleteActivity(boolean incompleteActivity)
	{
		this.incompleteActivity = incompleteActivity;
	}

	/**
	 * @param jobItemIds the jobItemIds to set
	 */
	public void setJobItemIds(Map<Integer, Integer> jobItemIds)
	{
		this.jobItemIds = jobItemIds;
	}

	/**
	 * @param reasons the reasons to set
	 */
	public void setReasons(Map<Integer, String> reasons)
	{
		this.reasons = reasons;
	}
}