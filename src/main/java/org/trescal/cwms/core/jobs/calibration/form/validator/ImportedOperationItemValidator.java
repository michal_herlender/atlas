package org.trescal.cwms.core.jobs.calibration.form.validator;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;
import org.trescal.cwms.batch.jobs.operationsimport.ImportedOperationItem;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatOperationTypeEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportFinalOutcome;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db.JobItemWorkRequirementService;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Calibration;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.rest.alligator.component.RestErrors;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ImportedOperationItemValidator extends AbstractBeanValidator implements SmartValidator {

	@Autowired
	private InstrumService instrumService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private JobItemWorkRequirementService jobItemWorkRequirementService;
	@Autowired
	private FaultReportService faultReportService;
	@Autowired
	private ServiceTypeService serviceTypeService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(ImportedOperationItem.class);
	}

	@Override
	public void validate(Object target, Errors errors, Object... validationHints) {
		super.validate(target, errors, validationHints);

		// client subdiv id
		Integer clientCoId = (Integer) validationHints[0];

		ImportedOperationItem dto = (ImportedOperationItem) target;
		ImportedOperationItem nextDto = (ImportedOperationItem) validationHints[1];

		// if plantid exists look if the instrument exists
		Integer identifiedInstrumentPlantId = null;
		if (dto.getPlantid() != null) {
			List<Instrument> instruments = instrumService.getByOwningCompany(dto.getPlantid(), clientCoId, null, null);
			if (instruments.isEmpty())
				errors.rejectValue("plantid", "error.rest.common.instrument.notfound",
						new Integer[] { dto.getPlantid() }, null);
			else
				identifiedInstrumentPlantId = instruments.stream().findFirst().orElse(null).getPlantid();
		} else if (dto.getPlantid() == null && StringUtils.isBlank(dto.getPlantno())
				&& StringUtils.isBlank(dto.getSerialNo())) {
			errors.reject("importedcalibrationssynthesis.instrumentnotfound");
			errors.rejectValue("plantno", "instrument.new.plantno.warning");
		} else if (dto.getPlantid() == null && StringUtils.isNoneBlank(dto.getPlantno())
				&& StringUtils.isNoneBlank(dto.getSerialNo())) {
			List<Instrument> instruments = instrumService.getByOwningCompany(null, clientCoId, dto.getPlantno(),
					dto.getSerialNo());
			if (instruments.isEmpty()) {
				errors.reject("importedcalibrationssynthesis.instrumentnotfound");
				errors.rejectValue("plantno", "importedcalibrationssynthesis.instrumentnotfound");
				errors.rejectValue("serialNo", "importedcalibrationssynthesis.instrumentnotfound");
			} else if (instruments.size() == 1) {
				identifiedInstrumentPlantId = instruments.stream().findFirst().orElse(null).getPlantid();
			}
		} else if (dto.getPlantid() == null && StringUtils.isBlank(dto.getPlantno())
				&& StringUtils.isNoneBlank(dto.getSerialNo())) {
			List<Instrument> instruments = instrumService.getByOwningCompany(null, clientCoId, null, dto.getSerialNo());
			if (instruments.isEmpty()) {
				errors.reject("importedcalibrationssynthesis.instrumentnotfound");
				errors.rejectValue("serialNo", "importedcalibrationssynthesis.instrumentnotfound");
			} else if (instruments.size() == 1) {
				identifiedInstrumentPlantId = instruments.stream().findFirst().orElse(null).getPlantid();
			}
		} else if (dto.getPlantid() == null && StringUtils.isNoneBlank(dto.getPlantno())
				&& StringUtils.isBlank(dto.getSerialNo())) {
			List<Instrument> instruments = instrumService.getByOwningCompany(null, clientCoId, dto.getPlantno(), null);
			if (instruments.isEmpty()) {
				errors.reject("importedcalibrationssynthesis.instrumentnotfound");
				errors.rejectValue("plantno", "importedcalibrationssynthesis.instrumentnotfound");
			} else if (instruments.size() == 1) {
				identifiedInstrumentPlantId = instruments.stream().findFirst().orElse(null).getPlantid();
			}
		}

		/** try to read the dates */
		// get startdate
		if (dto.getOperationStartDate() == null)
			errors.rejectValue("operationStartDate", "importedcalibrationssynthesis.datedoesntexistorpoorlyformatted");
		if (dto.getOperationDate() == null)
			errors.rejectValue("operationDate", "importedcalibrationssynthesis.datedoesntexistorpoorlyformatted");
		if (dto.getOperationValidatedOn() == null)
			errors.rejectValue("operationValidatedOn",
					"importedcalibrationssynthesis.datedoesntexistorpoorlyformatted");

		/** check contacts hr-ids */
		if (StringUtils.isNotEmpty(dto.getOperationByHrId())) {
			Contact technician = contactService.getByHrId(dto.getOperationByHrId());
			if (technician == null)
				errors.rejectValue("operationByHrId", "importedcalibrationssynthesis.techniciannotfound");
		} else {
			errors.rejectValue("operationByHrId", "importedcalibrationssynthesis.techniciannotfound");
		}
		if (StringUtils.isNotEmpty(dto.getOperationValidatedByHrId())) {
			Contact technician = contactService.getByHrId(dto.getOperationValidatedByHrId());
			if (technician == null)
				errors.rejectValue("operationValidatedByHrId", "importedcalibrationssynthesis.techniciannotfound");
		} else {
			if (dto.getRowspan() == 1)
				errors.rejectValue("operationValidatedByHrId", "importedcalibrationssynthesis.techniciannotfound");
		}

		// check operation type
		if (dto.getOperationType() == null) {
			errors.rejectValue("operationType", "error.rest.data.notfound", new String[] { "" }, null);
		}

		if (dto.getOperationType() != null && dto.getOperationType().equals(ExchangeFormatOperationTypeEnum.CAL)) {
			// check if the number is not duplicated within the instrument record
			if (identifiedInstrumentPlantId != null && StringUtils.isNotEmpty(dto.getDocumentNumber())) {
				List<Certificate> certs = certificateService.getByThirdCertNoAndPlantId(dto.getDocumentNumber(),
						identifiedInstrumentPlantId);
				if (CollectionUtils.isNotEmpty(certs)) {
					errors.rejectValue("documentNumber", RestErrors.CODE_THIRDPARTY_CERT_NO_ALREADY_EXISTS,
							new String[] { dto.getDocumentNumber() },
							RestErrors.MESSAGE_THIRDPARTY_CERT_NO_ALREADY_EXISTS);
				}
			}

			if (dto.getCalibrationOutcome() == null) {
				errors.rejectValue("calibrationOutcome", "error.rest.data.notfound", new String[] { "" }, null);
			} else if (dto.getCalibrationOutcome().equals(ActionOutcomeValue_Calibration.SUCCESSFUL)
					&& dto.getCalibrationOutcome().equals(ActionOutcomeValue_Calibration.NOT_SUCCESSFUL)) {
				// check cal process and call class
				if (dto.getCalibrationClass() == null) {
					errors.rejectValue("calibrationClass", "error.rest.data.notfound", new String[] { "" }, null);
				}
				if (dto.getCalibrationVerificationStatus() == null) {
					errors.rejectValue("calibrationVerificationStatus", "error.rest.data.notfound", new String[] { "" },
							null);
				}

			}
		}

		// check if a jobitem is selected
		if (dto.getJobitemId() == null && dto.getRowspan() > 0) {
			errors.rejectValue("jobitemId", "importedcalibrationssynthesis.jobitemnotselected");
		} else if (dto.getRowspan() > 0) {

			// check if jobitem have non completed and non cancelled workrequirements
			JobItem jobItem = jobItemService.findJobItem(dto.getJobitemId());
			List<JobItemWorkRequirement> wrs = jobItem.getWorkRequirements().stream().filter(jiwr -> {
				return !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE)
						&& !jiwr.getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED);
			}).collect(Collectors.toList());

			// check status
			boolean awaitingCalibration = jobItemService.isReadyForCalibration(jobItem);
			boolean awaitingContractReview = jobItemService.stateHasGroupOfKeyName(jobItem.getState(),
					StateGroup.AWAITING_CONTRACT_REVIEW);
			if (!awaitingCalibration && !awaitingContractReview) {
				errors.rejectValue("jobitemId", "importedcalibrationssynthesis.jobitemisincorrectstatus");
			}

			if (CollectionUtils.isEmpty(wrs)) {
				errors.rejectValue("jiWrId", "importedcalibrationssynthesis.allworkrequirementarecompleted");
			} else {
				// check if workrequirement have a procedure
				if (dto.getJiWrId() != null) {
                    JobItemWorkRequirement jiwr = jobItemWorkRequirementService
                        .findJobItemWorkRequirement(dto.getJiWrId());
                    if (jiwr != null && jiwr.getWorkRequirement().getCapability() == null)
                        errors.rejectValue("jiWrId", "error.rest.workrequirement.noprocedure", new String[]{""},
                            null);
                    if (jiwr != null && dto.getServiceTypeId() != null && !dto.getServiceTypeId()
                        .equals(jiwr.getWorkRequirement().getServiceType().getServiceTypeId()))
                        errors.rejectValue("jiWrId", "importedcalibrationssynthesis.servicetypdoesntmatch",
                            new String[]{""}, null);
                } else if (dto.getJiWrId() == null) {
					// we need just to show the page
//					errors.reject("error.rest.workrequirements.none");
					errors.rejectValue("jiWrId", "error.rest.workrequirements.none", new String[] { "" }, null);
				}
			}
		}

		// check if it is contract-reviewed
		if (BooleanUtils.isFalse(dto.getContractReviewed()) && BooleanUtils.isFalse(dto.getAutomaticContractReview())) {
			errors.rejectValue("contractReviewed", "lookupresultmessage.itemhasbeencontractreviewed.no",
					new String[] { "" }, null);
		}

		// check if owning subdiv of the failure report exists
		if (dto.getFrSubdivisionId() != null) {
			if (subdivService.get(dto.getFrSubdivisionId()) == null) {
				errors.rejectValue("frSubdivisionId", "error.rest.subdiv.notfound",
						new String[] { dto.getFrSubdivisionId().toString() }, null);
			}
		}

		boolean operationIsFr = dto.getOperationType() != null
				&& dto.getOperationType().equals(ExchangeFormatOperationTypeEnum.FR);

		// reject if the current operation if it is a NOT_SUCC cal, and the next cal is
		// not a FR (REMOVED : DEV-1894)
//		boolean operationIsNoSuccCal = dto.getOperationType() != null
//				&& dto.getOperationType().equals(ExchangeFormatOperationTypeEnum.CAL)
//				&& dto.getCalibrationOutcome().equals(ActionOutcomeValue_Calibration.NOT_SUCCESSFUL);
//		boolean nextOperationIsNotFR = nextDto != null && nextDto.getOperationType() != null
//				&& !nextDto.getOperationType().equals(ExchangeFormatOperationTypeEnum.FR);
//		if (operationIsNoSuccCal && (nextOperationIsNotFR || nextDto == null)) {
//			errors.rejectValue("calibrationOutcome", "importedcalibrationssynthesis.nextoperationshouldbeafr",
//					new String[] { "" }, null);
//		}

		if (operationIsFr) {

			boolean nextDtoExistsAndCheckedAndOnSameJobitem = nextDto != null && nextDto.isChecked()
					&& nextDto.getJobitemEquivalentId().equals(dto.getJobitemEquivalentId());

			FailureReportFinalOutcome finaloutcome = null;
			if (dto.getFrFinalOutcome() != null)
				finaloutcome = dto.getFrFinalOutcome();
			else {
				ServiceType nextSt = null;
				if (nextDto != null && nextDto.getServiceTypeId() != null)
					nextSt = serviceTypeService.get(nextDto.getServiceTypeId());
				finaloutcome = faultReportService.deduceFrOutcomeFromRecommendation(dto.getFrOperationByTrescal(),
						dto.getFrRecommendations(), nextSt, dto.getFrClientResponseNeeded(), dto.getFrClientDecision(),
						true);
			}

			// reject if the current fr outcome is a 'REPAIR' or 'THIRD_PARTY' and there are
			// still lines to import on the same jobitem
			boolean frOutcomeIsRepairOrTP = FailureReportFinalOutcome.INTERNAL_REPAIR.equals(finaloutcome)
					|| FailureReportFinalOutcome.THIRD_PARTY_REPAIR.equals(finaloutcome)
					|| FailureReportFinalOutcome.THIRD_PARTY_CALIBRATION_WITH_JUDGMENT.equals(finaloutcome)
					|| FailureReportFinalOutcome.THIRD_PARTY_CALIBRATION_WITHOUT_JUDGMENT.equals(finaloutcome);
			if (nextDtoExistsAndCheckedAndOnSameJobitem && frOutcomeIsRepairOrTP) {
				errors.rejectValue("documentNumber",
						"importedcalibrationssynthesis.outcomeisrepairortpwithfollowinglines", new String[] { "" },
						null);
			}

			// reject if we are still expecting the client response (finaloutcome is empty)
			// and also we have still pending lines to import
			boolean expectingClientResponseWithoutFrOutcome = finaloutcome == null
					&& BooleanUtils.isTrue(dto.getFrClientResponseNeeded()) && dto.getFrClientDecision() == null;
			if (expectingClientResponseWithoutFrOutcome && nextDtoExistsAndCheckedAndOnSameJobitem) {
				errors.rejectValue("documentNumber",
						"importedcalibrationssynthesis.faultreportawaitingclientdecisionwithnextlinestoimport",
						new String[] { "" }, null);
			}

			// reject if we couldn't deduce the final outcome of the FR (we will go to
			// 'awaiting selection of FR outcome') and still pending calibration to import
			boolean couldNotDeduceOutcome = finaloutcome == null;
			if (couldNotDeduceOutcome && nextDtoExistsAndCheckedAndOnSameJobitem) {
				errors.rejectValue("documentNumber", "importedcalibrationssynthesis.clientrefusedwithlinestoimport",
						new String[] { "" }, null);
			}

		}

	}

}
