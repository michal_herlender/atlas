package org.trescal.cwms.core.jobs.repair.validatedcomponent.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.repair.validatedcomponent.ValidatedFreeRepairComponent;

public interface ValidatedFreeRepairComponentDao extends BaseDao<ValidatedFreeRepairComponent, Integer> {
	
}