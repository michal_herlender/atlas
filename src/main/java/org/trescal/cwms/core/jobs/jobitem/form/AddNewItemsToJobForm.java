package org.trescal.cwms.core.jobs.jobitem.form;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.val;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class AddNewItemsToJobForm extends AbstractNewJobItemForm {
    private boolean contractReviewItems;
    private List<Integer> instAddrIds;
    private List<Integer> instCalFreq;
    private List<IntervalUnit> instCalUnitFreq;
    private List<Boolean> instDeletes;
    private List<String> instEndRanges;
    private List<Integer> instMfrIds;
    private List<Integer> instPersonIds;
    private List<String> instPlantNos = new LinkedList<>();
    private List<String> instSerialNos = new LinkedList<>();
    private List<String> instStartRanges;
    private List<Integer> instThreadIds;
    private List<Integer> instUomIds;
    private List<String> itemPOs;
    private List<Integer> itemProcIds;
    private List<Integer> itemWorkInstIds;
    private List<String> instModelNames;
    private List<String> instCustomerDescs;
    private Integer bussinessCompanyId;


    public List<ZippedAddNewItems> getZippedLists() {
        if (getBasketTypes().size() == 0)
            return Collections.emptyList();
        val list = new ArrayList<ZippedAddNewItems>();
        for (int i = 0; i < getBasketTypes().size(); i++) {
            list.add(
                ZippedAddNewItems.of(
                    basketBaseItemIds.get(i),
                    basketExistingGroupIds.get(i),
                    basketIds.get(i),
                    basketParts.get(i),
                    basketTempGroupIds.get(i),
                    basketTypes.get(i),
                    basketQuotationItemIds.get(i),
                    serviceTypeIds.get(i),
                    serviceTypeSources.get(i),
                    datesIn.get(i),
                    bookInByContactIds.get(i),
                    instAddrIds != null ? instAddrIds.get(i) : null,
                    instCalFreq != null ? instCalFreq.get(i) : null,
                    instCalUnitFreq != null ? instCalUnitFreq.get(i) : null,
                    instDeletes != null ? instDeletes.get(i) : null,
                    instEndRanges != null ? instEndRanges.get(i) : null,
                    instMfrIds != null ? instMfrIds.get(i) : null,
                    instPersonIds != null ? instPersonIds.get(i) : null,
                    !instPlantNos.isEmpty() ? instPlantNos.get(i) : null,
                    !instSerialNos.isEmpty() ? instSerialNos.get(i) : null,
                    instStartRanges != null ? instStartRanges.get(i) : null,
                    instThreadIds != null ? instThreadIds.get(i) : null,
                    instUomIds != null ? instUomIds.get(i) : null,
                    itemPOs != null ? itemPOs.get(i) : null,
                    itemProcIds != null ? itemProcIds.get(i) : null,
                    itemWorkInstIds != null ? itemWorkInstIds.get(i) : null,
                    instModelNames != null ? instModelNames.get(i) : null,
                    instCustomerDescs != null ? instCustomerDescs.get(i) : null
                )
            );
        }
        return list;
    }

}

