package org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;

import javax.persistence.*;

@MappedSuperclass
public abstract class FaultReportDetail extends Auditable
{
	private boolean active;
	private String description;
	private FaultReport faultRep;
	private int id;
	private Contact recordedBy;

	public FaultReportDetail()
	{
		this.active = true;
	}

	/**
	 * @return the description
	 */
	@Length(max = 1000)
	@Column(name = "description", length = 1000, columnDefinition="nvarchar(1000)")
	public String getDescription()
	{
		return this.description;
	}

	/**
	 * @return the faultRep
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "faultrepid")
	public FaultReport getFaultRep()
	{
		return this.faultRep;
	}

	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	/**
	 * @return the recordedBy
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "recordedby")
	public Contact getRecordedBy()
	{
		return this.recordedBy;
	}

	@Column(name = "active", nullable=false, columnDefinition="bit")
	public boolean isActive()
	{
		return this.active;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * @param faultRep the faultRep to set
	 */
	public void setFaultRep(FaultReport faultRep)
	{
		this.faultRep = faultRep;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @param recordedBy the recordedBy to set
	 */
	public void setRecordedBy(Contact recordedBy)
	{
		this.recordedBy = recordedBy;
	}
}