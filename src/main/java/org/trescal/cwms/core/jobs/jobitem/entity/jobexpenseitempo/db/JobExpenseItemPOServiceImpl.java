package org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.db;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.job.dto.ClientPurchaseOrderExpenseItemDTO;
import org.trescal.cwms.core.jobs.job.projection.ClientPurchaseOrderProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO;

@Service
public class JobExpenseItemPOServiceImpl extends BaseServiceImpl<JobExpenseItemPO, Integer>
		implements JobExpenseItemPOService {

	@Autowired
	private JobExpenseItemPODao baseDao;

	@Override
	protected BaseDao<JobExpenseItemPO, Integer> getBaseDao() {
		return baseDao;
	}

	@Override
	public List<ClientPurchaseOrderProjectionDTO> getProjectionDTOs(Collection<Integer> periodicInvoiceIds) {
		List<ClientPurchaseOrderProjectionDTO> result = Collections.emptyList();
		if ((periodicInvoiceIds != null) && !periodicInvoiceIds.isEmpty()) {
			result = this.baseDao.getProjectionDTOs(periodicInvoiceIds);
		}
		return result;
	}

	@Override
	public List<ClientPurchaseOrderExpenseItemDTO> getOnPO(Integer poId) {
		Locale locale = LocaleContextHolder.getLocale();
		return this.baseDao.getOnPO(poId, locale);
	}

	@Override
	public List<ClientPurchaseOrderExpenseItemDTO> getOnBPO(Integer poId, Integer jobId) {
		Locale locale = LocaleContextHolder.getLocale();
		return this.baseDao.getOnBPO(poId, jobId, locale);
	}


	@Override
	public List<Integer> getJobServicesIdsOnBPO(Integer bpoid, Integer jobId) {
		return this.baseDao.getJobServicesIdsOnBPO(bpoid, jobId);
	}

	@Override
	public JobExpenseItemPO getJobExpenseItemPO(Integer jobServicesId, int poId, Integer jobId) {
		return this.baseDao.getJobExpenseItemPO(jobServicesId, poId, jobId);
	}
}