package org.trescal.cwms.core.jobs.contractreview.projection;

import java.util.Date;

import org.trescal.cwms.spring.model.KeyValueIntegerString;

import lombok.Getter;
import lombok.Setter;

/*
 * Projection provided without job item id(s) for now;
 * revisit when using for view job improvements.
 * May need second query to track usage on job items (prevent redundant data). 
 */
@Getter
@Setter
public class ContractReviewProjectionDTO {
	private Integer id;
	private String comments;
	private Integer reviewById;
	private Date reviewDate;
	private String itemsNoCoverd;
	private Integer itemsNoCoverdSize;
	// Derived fields
	private KeyValueIntegerString reviewBy;

	public ContractReviewProjectionDTO(Integer id, String comments, Integer reviewById, String reviewByFirstName,
			String reviewByLastName, Date reviewDate) {
		super();
		this.id = id;
		this.comments = comments;
		this.reviewById = reviewById;
		this.reviewDate = reviewDate;
		if (this.reviewById != null) {
			this.reviewBy = new KeyValueIntegerString(reviewById, assembleName(reviewByFirstName, reviewByLastName));
		}
	}


	private String assembleName(String firstName, String lastName) {
		StringBuffer result = new StringBuffer();
		if (firstName != null)
			result.append(firstName);
		if (result.length() > 0)
			result.append(" ");
		if (lastName != null)
			result.append(lastName);
		return result.toString();
	}
}
