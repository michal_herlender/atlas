package org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.jobs.job.dto.ClientPurchaseOrderExpenseItemDTO;
import org.trescal.cwms.core.jobs.job.entity.abstractpo.AbstractPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO_;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.po.PO_;
import org.trescal.cwms.core.jobs.job.projection.ClientPurchaseOrderProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitempo.AbstractJobItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobExpenseItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobExpenseItemNotInvoiced_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO_;

@Repository
public class JobExpenseItemPODaoImpl extends BaseDaoImpl<JobExpenseItemPO, Integer> implements JobExpenseItemPODao {

	@Override
	protected Class<JobExpenseItemPO> getEntity() {
		return JobExpenseItemPO.class;
	}

	/**
	 * This is made specifically to get all the PO details for expense items
	 * which are "linked" to a periodic invoice (or optionally a set of periodic
	 * invoices)
	 */
	@Override
	public List<ClientPurchaseOrderProjectionDTO> getProjectionDTOs(Collection<Integer> periodicInvoiceIds) {
		if ((periodicInvoiceIds == null) || periodicInvoiceIds.isEmpty())
			throw new UnsupportedOperationException("Query must be called with at least one periodicInvoiceIds");
		return getResultList(cb -> {
			CriteriaQuery<ClientPurchaseOrderProjectionDTO> cq = cb.createQuery(ClientPurchaseOrderProjectionDTO.class);
			Root<JobExpenseItemPO> root = cq.from(JobExpenseItemPO.class);
			Join<JobExpenseItemPO, JobExpenseItem> expenseItem = root.join(JobExpenseItemPO_.ITEM, JoinType.INNER);
			Join<JobExpenseItem, JobExpenseItemNotInvoiced> notInvoiced = expenseItem.join(JobExpenseItem_.notInvoiced,
					JoinType.INNER);
			Join<JobExpenseItemPO, BPO> bpo = root.join(JobItemPO_.bpo, JoinType.LEFT);
			Join<JobExpenseItemPO, PO> po = root.join(JobItemPO_.po, JoinType.LEFT);

			cq.where(notInvoiced.get(JobExpenseItemNotInvoiced_.periodicInvoice).in(periodicInvoiceIds));

			cq.select(cb.construct(ClientPurchaseOrderProjectionDTO.class, root.get(JobExpenseItemPO_.id),
					expenseItem.get(JobExpenseItem_.id), bpo.get(BPO_.poId), bpo.get(BPO_.poNumber), po.get(PO_.poId),
					po.get(PO_.poNumber)));

			return cq;
		});
	}

	@SuppressWarnings("rawtypes")
	private <T extends AbstractPO> List<ClientPurchaseOrderExpenseItemDTO> getOnPOorBPO(Integer poId, Integer jobId,
			SingularAttribute<AbstractJobItemPO, T> poAttribute, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<ClientPurchaseOrderExpenseItemDTO> cq = cb
					.createQuery(ClientPurchaseOrderExpenseItemDTO.class);
			Root<JobExpenseItemPO> expenseItemPO = cq.from(JobExpenseItemPO.class);
			Join<JobExpenseItemPO, JobExpenseItem> expenseItem = expenseItemPO.join(JobExpenseItemPO_.ITEM);
			Join<JobExpenseItem, InstrumentModel> model = expenseItem.join(JobExpenseItem_.model);
			Expression<String> modelName = joinTranslation(cb, model, InstrumentModel_.nameTranslations, locale);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(expenseItemPO.get(poAttribute), poId));
			if (jobId != null)
				clauses.getExpressions().add(cb.equal(expenseItem.get(JobItem_.job), jobId));
			cq.where(clauses);
			cq.orderBy(cb.asc(expenseItem.get(JobExpenseItem_.itemNo)));
			cq.select(cb.construct(ClientPurchaseOrderExpenseItemDTO.class, expenseItem.get(JobExpenseItem_.itemNo),
					modelName, expenseItem.get(JobExpenseItem_.comment), expenseItem.get(JobExpenseItem_.quantity)));
			return cq;
		});
	}

	@Override
	public List<ClientPurchaseOrderExpenseItemDTO> getOnPO(Integer poId, Locale locale) {
		return getOnPOorBPO(poId, null, JobExpenseItemPO_.po, locale);
	}

	@Override
	public List<ClientPurchaseOrderExpenseItemDTO> getOnBPO(Integer poId, Integer jobId, Locale locale) {
		return getOnPOorBPO(poId, jobId, JobExpenseItemPO_.bpo, locale);
	}


	@Override
	public List<Integer> getJobServicesIdsOnBPO(Integer bpoid, Integer jobId) {
		return getResultList(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);

			Root<JobExpenseItemPO> expenseItemPO = cq.from(JobExpenseItemPO.class);
			Join<JobExpenseItemPO, JobExpenseItem> expenseItem = expenseItemPO.join(JobExpenseItemPO_.ITEM);

			Predicate clauses = cb.conjunction();

			clauses.getExpressions().add(cb.equal(expenseItemPO.get(JobExpenseItemPO_.bpo), bpoid));
			clauses.getExpressions().add(cb.equal(expenseItem.get(JobExpenseItem_.job), jobId));

			cq.where(clauses);

			cq.select(expenseItem.get(JobExpenseItem_.id));
			return cq;
		});
	}

	@Override
	public JobExpenseItemPO getJobExpenseItemPO(Integer jobServicesId, int poId, Integer jobId) {
		 return getSingleResult(cb ->{
			 CriteriaQuery<JobExpenseItemPO> cq = cb.createQuery(JobExpenseItemPO.class);
			 Root<JobExpenseItemPO> jobbExpenseItemPO = cq.from(JobExpenseItemPO.class);
			 Join<JobExpenseItemPO, JobExpenseItem> jobExpense = jobbExpenseItemPO.join(JobItemPO_.ITEM);
				Predicate clauses = cb.conjunction();
				clauses.getExpressions().add(cb.equal(jobExpense.get(JobItem_.job), jobId));
				clauses.getExpressions().add(cb.equal(jobExpense.get(JobExpenseItem_.id), jobServicesId));
				clauses.getExpressions().add(cb.equal(jobbExpenseItemPO.get(JobItemPO_.bpo), poId));
				cq.where(clauses);
			 return cq;
		});
	}
}