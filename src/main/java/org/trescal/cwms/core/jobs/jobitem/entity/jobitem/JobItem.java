package org.trescal.cwms.core.jobs.jobitem.entity.jobitem;

import lombok.Setter;
import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItemAgeComparator;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.documents.images.image.ImageComparator;
import org.trescal.cwms.core.documents.images.jobitemimage.JobItemImage;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemand.JobItemDemand;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.callink.JobItemToCalLinkComparator;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLinkComparator;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItemComparator;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.GsoDocumentLink;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.GsoDocumentLinkComparator;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLinkComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.AbstractJobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.JobItemType;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.CallOffItem;
import org.trescal.cwms.core.jobs.jobitem.entity.defect.ItemDefect;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnote.JobItemNote;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.entity.pat.PAT;
import org.trescal.cwms.core.jobs.jobitem.entity.pat.PATComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.requirement.Requirement;
import org.trescal.cwms.core.jobs.jobitem.entity.requirement.RequirementComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction.TPInstruction;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirementComparator;
import org.trescal.cwms.core.jobs.jobitemhistory.JobItemHistory;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.pricing.entity.costs.CostOrderComparator;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItemJobItemComparator;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.DistinctPurchaseOrderJobItemComparator;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.TPQuoteLink;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.TPQuoteLinkComparator;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItemComparator;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;

@Entity
@Table(name = "jobitem")
@AssociationOverrides({
		@AssociationOverride(name = "job", joinColumns = @JoinColumn(name = "jobid"), foreignKey = @ForeignKey(name = "FK_jobitem_job")) })
@Setter
public class JobItem extends AbstractJobItem<JobItemPO> implements NoteAwareEntity {

	private Set<ItemAccessory> accessories;
	private String accessoryFreeText;
	private Set<JobItemAction> actions;
	private ContractReviewAdjustmentCost adjustmentCost;
	private Boolean approvedToBypassCosting = false;
	private Address baseAddr;
	private Location baseLoc;
	private Address bookedInAddr;
	private Location bookedInLoc;
	private Address calAddr;
	private ContractReviewCalibrationCost calibrationCost;
	private Set<CalLink> calLinks;
	private Location calLoc;
	@Deprecated
	private CalibrationType calType;
	private ServiceType serviceType;
	private SortedSet<CertLink> certLinks;
	private Boolean chasingActive = false;
	private ActionOutcome conRevOutcome;
	private SortedSet<ContractReviewItem> contactReviewItems;
	private Set<JobItemHistory> history;
	/**
	 * Contract Review costs.
	 */
	protected Set<Cost> costs;
	private Address currentAddr;
	private Location currentLoc;
	private ItemDefect defect;
	private Set<JobDeliveryItem> deliveryItems;
	private Set<FaultReport> failureReports;
	// private FaultReport faultRep;
	private JobItemGroup group;
	private Set<JobItemImage> images;
	private TransportOption inOption;
	private Instrument inst;
	private Set<InvoiceItem> invoiceItems;
	private int itemNo; // Left as primitive as it's essential in comparators
    private Set<JobCostingItem> jobCostingItems;
    private int jobItemId;
    private Contact lastContractReviewBy;
    private Set<TPQuoteLink> linkedTPQuotes;
    private Boolean neverToBeChased = false;
    private JobItemWorkRequirement nextWorkReq;
    private Set<JobItemNote> notes;
    private JobItemNotInvoiced notInvoiced;
    private OnBehalfItem onBehalf;
    private Set<PAT> pats;
    private Boolean paymentReceived = false;
    private Capability capability;
    private ContractReviewPurchaseCost purchaseCost;
    private Set<PurchaseOrderJobItem> purchaseOrderItems;
    private ContractReviewRepairCost repairCost;
    private Set<Requirement> req;
    private Boolean reqCleaning = false;
    private TransportOption returnOption;
    private Set<TPInstruction> tpInstructions;
    private Set<TPQuoteRequestItem> tpQuoteItemRequests;
    private SortedSet<TPRequirement> tpRequirements;

    private ZonedDateTime dateIn;
	/**
	 * Next expected transit date.
	 */
	private LocalDate transitDate;
	private LocalDate dueDate;
	private LocalDate agreedDelDate;
	private Date dateComplete;

	private Integer turn = 0;
	private WorkInstruction workInstruction;
	private List<JobItemWorkRequirement> workRequirements;
	private Set<CallOffItem> callOffItems;
    private Set<CallOffItem> callOnItems;
    private List<EngineerAllocation> engineerAllocations;
    private Contract contract;
    private List<JobItemDemand> additionalDemands;
    private List<RepairInspectionReport> repairInspectionReports;
    private ZonedDateTime clientReceiptDate;
    private Set<GsoLink> gsoLinks;
    private Set<GsoDocumentLink> gsoDocumentLinks;
    private Address returnToAddress;
    private Contact returnToContact;
    private ActiveClientJobItem activeClientJobItem;
    private ActiveBusinessJobItem activeBusinessJobItem;

    public JobItem() {
        // Moving initialization of sets here which are typically needed at job
        // item creation
        // Refactor and remove from external classes when moved here, to avoid
        // duplicate code
        this.workRequirements = new ArrayList<>();
    }

    private void addCost(Cost cost) {
		if (cost != null) {
			if (this.costs == null) {
				this.costs = new TreeSet<>(new CostOrderComparator());
			}
			this.costs.add(cost);
		}
	}

	/**
	 * @param requirement Adds set of Requirement to existing requirements
	 */
	public void addRequirement(Set<Requirement> requirement) {
		this.req.addAll(requirement);
	}

	@OneToMany(mappedBy = "ji", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("id")
	public Set<ItemAccessory> getAccessories() {
		return this.accessories;
	}

	@Size(max = 500)
	@Column(name = "accessoryFreeText", length = 500)
	public String getAccessoryFreeText() {
		return accessoryFreeText;
	}

	@OneToMany(mappedBy = "jobItem", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@OrderBy("id")
	public Set<JobItemAction> getActions() {
		return this.actions;
	}

	@Transient
	public int getActiveActionsSize() {
		int size = 0;

		if (this.actions != null) {
			for (JobItemAction action : this.actions) {
				if (!action.isDeleted()) {
					size++;
				}
			}
		}

		return size;
	}

	@OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
	@JoinColumn(name = "adjust_id")
	public ContractReviewAdjustmentCost getAdjustmentCost() {
		return this.adjustmentCost;
	}

	@Column(name = "agreeddeldate", columnDefinition = "date")
	public LocalDate getAgreedDelDate() {
		return this.agreedDelDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "baseaddrid")
	public Address getBaseAddr() {
		return this.baseAddr;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "baselocid")
	public Location getBaseLoc() {
		return this.baseLoc;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bookedinaddrid")
	public Address getBookedInAddr() {
		return this.bookedInAddr;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bookedinlocid")
	public Location getBookedInLoc() {
		return this.bookedInLoc;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "caladdrid")
	public Address getCalAddr() {
		return this.calAddr;
	}

	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "calcost_id")
	public ContractReviewCalibrationCost getCalibrationCost() {
		return this.calibrationCost;
	}

	/*
	 * If job item is deleted, any associated CallOffItems (of which only one active
	 * at a time) should be deleted
	 */
	@OneToMany(mappedBy = "offItem", fetch = FetchType.LAZY, cascade = { CascadeType.REMOVE })
	public Set<CallOffItem> getCallOffItems() {
		return callOffItems;
	}

	/*
	 * If job item is deleted, any associated CallOffItem (should be just one) that
	 * this originated from should be deleted
	 */
	@OneToMany(mappedBy = "onItem", fetch = FetchType.LAZY, cascade = { CascadeType.REMOVE })
	public Set<CallOffItem> getCallOnItems() {
		return callOnItems;
	}

	@OneToMany(mappedBy = "ji", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@SortComparator(JobItemToCalLinkComparator.class)
	public Set<CalLink> getCalLinks() {
		return this.calLinks;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "callocid")
	public Location getCalLoc() {
		return this.calLoc;
	}

	/**
	 * @deprecated use getServiceType instead
	 */
	@NotNull
	@Deprecated
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "caltypeid")
	public CalibrationType getCalType() {
		return this.calType;
	}

	/**
	 * @deprecated Call only setServiceType; which will internally call setCalType().
	 * In the future the calType field will be removed, after all getCalType() refactoring done  
	 * Method created manually (not via Lombok) in order to write above deprecation comment  
	 */
	@Deprecated
	public void setCalType(CalibrationType calType) {
		this.calType = calType;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "servicetypeid", nullable=false, foreignKey=@ForeignKey(name="FK_jobitem_servicetypeid"))
	public ServiceType getServiceType() {
		return this.serviceType;
	}
	
	/**
	 * This method sets both serviceType and the calType field; necessary until all calls to getCalType() are removed
	 * (including JPA queries, HQL, DWR usage, JSPs, etc...)
	 */
	public void setServiceType(ServiceType serviceType) {
		this.serviceType = serviceType;
		setCalType(serviceType != null ? serviceType.getCalibrationType() : null);
	}
	
	/**
	 * @return the certLinks
	 */
	@OneToMany(mappedBy = "jobItem", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@SortComparator(CertLinkComparator.class)
	public SortedSet<CertLink> getCertLinks() {
		return this.certLinks;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "conrevoutcomeid")
	public ActionOutcome getConRevOutcome() {
		return this.conRevOutcome;
	}

	@OneToMany(mappedBy = "jobitem", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@SortComparator(ContractReviewItemComparator.class)
	public SortedSet<ContractReviewItem> getContactReviewItems() {
		return this.contactReviewItems;
	}

	@OneToMany(mappedBy = "jobItem", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@OrderBy("changeDate desc")
	public Set<JobItemHistory> getHistory() {
		return this.history;
	}

	@Transient
	public Set<Cost> getCosts() {
		return this.costs;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "addrid")
	public Address getCurrentAddr() {
		return this.currentAddr;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "locid")
	public Location getCurrentLoc() {
		return this.currentLoc;
	}

	/**
	 * @return the dateComplete
	 */
	@Column(name = "datecomplete", columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDateComplete() {
		return this.dateComplete;
	}

	@NotNull
	@Column(name = "datein", columnDefinition = "datetime")
	public ZonedDateTime getDateIn() {
		return this.dateIn;
	}

	/**
	 * @deprecated use ContractReviewItemService::getContractReviewedJobItemIds(int
	 *             jobid) instead this causes a lazy loading issue, with a big query
	 *             count impact on large jobs
	 */
	@Deprecated
	@Transient
	public String getDefaultPage() {
		// if item has already been contract reviewed,
		// actions page should be default
		if ((this.contactReviewItems != null) && (this.contactReviewItems.size() > 0)) {
			return "jiactions.htm";
		} else {
			return "jicontractreview.htm";
		}
	}

	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "defectid")
	public ItemDefect getDefect() {
		return this.defect;
	}

	@OneToMany(mappedBy = "jobitem", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST })
	@SortComparator(JobDeliveryItemAgeComparator.class)
	public Set<JobDeliveryItem> getDeliveryItems() {
		return this.deliveryItems;
	}

	/**
	 * @return the dueDate
	 */
	@NotNull
	@Column(name = "duedate", columnDefinition = "date")
	public LocalDate getDueDate() {
		return this.dueDate;
	}

	@OneToMany(mappedBy = "jobItem", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("jobitemid")
	public Set<FaultReport> getFailureReports() {
		if (failureReports == null)
			this.failureReports = new HashSet<>();
		return failureReports;
	}

	@OneToMany(mappedBy = "ji", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<RepairInspectionReport> getRepairInspectionReports() {
		return repairInspectionReports;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "groupid")
	public JobItemGroup getGroup() {
		return this.group;
	}

	@OneToMany(mappedBy = "jobitem", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@SortComparator(ImageComparator.class)
	public Set<JobItemImage> getImages() {
		return this.images;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inoptionid")
	public TransportOption getInOption() {
		return this.inOption;
	}

	/**
	 * @return the inst
	 */
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
	@JoinColumn(name = "plantid")
	public Instrument getInst() {
		return this.inst;
	}

	@OneToMany(mappedBy = "jobItem", cascade = { CascadeType.PERSIST })
	public Set<InvoiceItem> getInvoiceItems() {
		return this.invoiceItems;
	}

	/**
	 * Returns the item number in the form that it will appear on certificates and
	 * other documentation. Item 6 on Job 10152/08 will have item code: 10152.6-08
	 */
	@Transient
	public String getItemCode() {
		return this.getJob().getJobno() + "." + this.itemNo;
	}

	/**
	 * @return the itemNo
	 */
	@Column(name = "itemno")
	@Type(type = "int")
	public int getItemNo() {
		return this.itemNo;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "jobItem", cascade = { CascadeType.PERSIST })
	@SortComparator(JobCostingItemJobItemComparator.class)
	public Set<JobCostingItem> getJobCostingItems() {
		return this.jobCostingItems;
	}

	/**
	 * @return the jobItemId
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "jobitemid", nullable = false, unique = true)
	@Type(type = "int")
	public int getJobItemId() {
		return this.jobItemId;
	}

	/**
	 * Renamed the function on 2020/07/01, but not considered all jsp files, hence
	 * keep this copy for now.
	 * 
	 * @deprecated
	 */
	@Transient
	public Set<JobItemPO> getJobItemPOs() {
		return this.getItemPOs();
	}

	@Transient
	public String getLabviewCode() {
		return this.getJob().getJobno().concat(".").concat(String.valueOf(this.itemNo));
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "lastContractReviewBy_personid")
	public Contact getLastContractReviewBy() {
		return lastContractReviewBy;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "jobItem", cascade = { CascadeType.ALL })
	@SortComparator(TPQuoteLinkComparator.class)
	public Set<TPQuoteLink> getLinkedTPQuotes() {
		return this.linkedTPQuotes;
	}

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "nextworkreqid")
	public JobItemWorkRequirement getNextWorkReq() {
		return this.nextWorkReq;
	}

	/**
	 * @return the notes
	 */
	@OneToMany(mappedBy = "ji", cascade = { CascadeType.ALL })
	@SortComparator(NoteComparator.class)
	public Set<JobItemNote> getNotes() {
		return this.notes;
	}

	/*
	 * There are issues with orphanRemoval in OneToOne relationships in Hibernate
	 * 5.1, which are fixed in Hibernate 5.2.10 Hence, commented out orphanRemoval
	 * for now.
	 */
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "jobitem", /*
															 * orphanRemoval= true,
															 */ cascade = CascadeType.ALL)
	public OnBehalfItem getOnBehalf() {
		return this.onBehalf;
	}

	/**
	 * Optional field - records reason, if any, that job item is not invoiced.
	 * Presence of JobItemNotInvoiced excludes job item from invoicing
	 */
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "jobItem", orphanRemoval = true, cascade = CascadeType.ALL)
	public JobItemNotInvoiced getNotInvoiced() {
		return notInvoiced;
	}

	@OneToMany(mappedBy = "ji", fetch = FetchType.LAZY)
	@SortComparator(PATComparator.class)
	public Set<PAT> getPats() {
		return this.pats;
	}

	@Transient
	public Integer getPrivateActiveNoteCount() {
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount() {
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

    @Transient
    public Integer getPublicActiveNoteCount() {
        return NoteCountCalculator.getPublicActiveNoteCount(this);
    }

    @Transient
    public Integer getPublicDeactivatedNoteCount() {
        return NoteCountCalculator.getPublicDeactivedNoteCount(this);
    }

    /**
     * @return the proc
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "capabilityId")
    public Capability getCapability() {
        return this.capability;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "salescost_id")
    public ContractReviewPurchaseCost getPurchaseCost() {
        return this.purchaseCost;
    }

    @OneToMany(mappedBy = "ji", fetch = FetchType.LAZY)
    @SortComparator(DistinctPurchaseOrderJobItemComparator.class)
    public Set<PurchaseOrderJobItem> getPurchaseOrderItems() {
		return this.purchaseOrderItems;
	}

	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "repcost_id")
	public ContractReviewRepairCost getRepairCost() {
		return this.repairCost;
	}

	/**
	 * @return the req
	 */
	@OneToMany(mappedBy = "item", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@SortComparator(RequirementComparator.class)
	public Set<Requirement> getReq() {
		return this.req;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "returnoptionid")
	public TransportOption getReturnOption() {
		return this.returnOption;
	}

	@OneToMany(mappedBy = "ji", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	public Set<TPInstruction> getTpInstructions() {
		return this.tpInstructions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "requestFromJobItem", cascade = { CascadeType.ALL })
	@SortComparator(TPQuoteRequestItemComparator.class)
	public Set<TPQuoteRequestItem> getTpQuoteItemRequests() {
		return this.tpQuoteItemRequests;
	}

	@OneToMany(mappedBy = "ji", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
	@SortComparator(TPRequirementComparator.class)
	public SortedSet<TPRequirement> getTpRequirements() {
		return this.tpRequirements;
	}

	@Column(name = "transitdate", columnDefinition = "date")
	public LocalDate getTransitDate() {
		return this.transitDate;
	}

	/**
	 * @return the turn
	 */
	@Column(name = "turn")
	@Type(type = "int")
	public Integer getTurn() {
		return this.turn;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "wiid")
	public WorkInstruction getWorkInstruction() {
		return this.workInstruction;
	}

	@OneToMany(mappedBy = "jobitem", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	public List<JobItemWorkRequirement> getWorkRequirements() {
		return this.workRequirements;
	}

	@Column(name = "bypasscosting", columnDefinition = "tinyint")
	public Boolean getApprovedToBypassCosting() {
		return this.approvedToBypassCosting;
	}

	@Column(name = "chasingactive", columnDefinition = "tinyint")
	public Boolean getChasingActive() {
		return this.chasingActive;
	}

	@Column(name = "neverchase", columnDefinition = "tinyint")
	public Boolean getNeverToBeChased() {
		return this.neverToBeChased;
	}

	@Column(name = "paymentreceived", columnDefinition = "tinyint")
	public Boolean getPaymentReceived() {
		return this.paymentReceived;
	}

	/**
	 * @return the reqCleaning
	 */
	@Column(name = "reqcleaning", columnDefinition = "tinyint")
	public Boolean getReqCleaning() {
		return this.reqCleaning;
	}

	@OneToMany(mappedBy = "itemAllocated", fetch = FetchType.LAZY, cascade = { CascadeType.REMOVE })
	public List<EngineerAllocation> getEngineerAllocations() {
		return engineerAllocations;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contractid")
	public Contract getContract() {
		return contract;
	}

	@OneToMany(mappedBy = "jobItem", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<JobItemDemand> getAdditionalDemands() {
		return additionalDemands;
	}

	@Column(name = "clientreceiptdate", columnDefinition = "datetime2")
	@DateTimeFormat(iso = ISO.DATE_TIME)
	public ZonedDateTime getClientReceiptDate() {
		return clientReceiptDate;
	}

	@OneToMany(mappedBy = "ji", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@SortComparator(GsoLinkComparator.class)
	public Set<GsoLink> getGsoLinks() {
		return this.gsoLinks;
	}

	@OneToMany(mappedBy = "ji", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@SortComparator(GsoDocumentLinkComparator.class)
	public Set<GsoDocumentLink> getGsoDocumentLinks() {
		return this.gsoDocumentLinks;
	}

	/**
	 * Calculates the date that the jobitem is due back. Adds the turn around to the
	 * date in (or the job's registration date if the item's date in is not present)
	 */
	public void calculateAndSetDueDate() {
		LocalDate dateIn = (this.dateIn != null) ? this.dateIn.toLocalDate() :
			getJob().getRegDate();
		this.setDueDate(dateIn.plusDays(this.turn));
	}
	
	@Override
	public ItemState getState() {
		return super.getState();
	}
	

	/**
	 * @param state the state to set
	 */
	@Override
	public void setState(ItemState state) {
		// if changing to a different status, chasing should now be re-activated
		// unless the item is set to never be chased
		ItemState oldState = this.getState();
		if (oldState != null && state != null) {
			if (oldState.getStateid().intValue() != state.getStateid().intValue() && !this.neverToBeChased) {
				this.chasingActive = true;
			}
		}
		super.setState(state);
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "returnToAddress", foreignKey = @ForeignKey(name = "FK_address_addrid"))
	public Address getReturnToAddress() {
		return returnToAddress;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "returnToContact", foreignKey = @ForeignKey(name = "FK_contact_personid"))
	public Contact getReturnToContact() {
		return returnToContact;
	}

	@Override
	@Transient
	public JobItemType getJobItemType() {
		return JobItemType.INSTRUMENT_BASED;
	}

	@Transient
	public Subdiv getNextSubdiv() {
		return this.getNextWorkReq() == null
				|| this.getNextWorkReq().getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE)
				|| this.getNextWorkReq().getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED)
						? this.getReturnOption() != null ? this.getReturnOption().getSub():null
						: this.getNextWorkReq().getWorkRequirement().getAddress() == null ? null
								: this.getNextWorkReq().getWorkRequirement().getAddress().getSub();
	}

	public void setAdjustmentCost(ContractReviewAdjustmentCost adjustmentCost) {
		this.adjustmentCost = adjustmentCost;
		this.addCost(adjustmentCost);
	}

	public void setCalibrationCost(ContractReviewCalibrationCost calibrationCost) {
		this.calibrationCost = calibrationCost;
		this.addCost(calibrationCost);
	}

    public void setPurchaseCost(ContractReviewPurchaseCost purchaseCost) {
        this.purchaseCost = purchaseCost;
        this.addCost(purchaseCost);
    }

    public void setRepairCost(ContractReviewRepairCost repairCost) {
        this.repairCost = repairCost;
        this.addCost(repairCost);
    }

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "jobItem")
    public ActiveClientJobItem getActiveClientJobItem() {
        return activeClientJobItem;
    }

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "jobItem")
    public ActiveBusinessJobItem getActiveBusinessJobItem() {
        return activeBusinessJobItem;
    }
}