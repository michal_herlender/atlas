package org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.JobInstructionLink;

@Service
public class JobInstructionLinkServiceImpl extends BaseServiceImpl<JobInstructionLink, Integer> implements JobInstructionLinkService {

	@Autowired
	JobInstructionLinkDao baseDao;
	
	@Override
	protected BaseDao<JobInstructionLink, Integer> getBaseDao() {
		return baseDao;
	}
	
	public List<JobInstructionLink> getAllForJob(Job job) {
		return baseDao.getAllForJob(job);
	}
}