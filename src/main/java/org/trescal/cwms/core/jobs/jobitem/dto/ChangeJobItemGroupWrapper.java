package org.trescal.cwms.core.jobs.jobitem.dto;

import java.util.HashMap;

public class ChangeJobItemGroupWrapper {
	
	private HashMap<Integer, String> modelNames;
	private String message;
	private Object results;
	private boolean success;
	
	
	
	public ChangeJobItemGroupWrapper(boolean success, String message, Object results) {
		super();
		this.message = message;
		this.results = results;
		this.success = success;
	}
	
	
	public HashMap<Integer, String> getModelNames() {
		return modelNames;
	}
	public void setModelNames(HashMap<Integer, String> modelNames) {
		this.modelNames = modelNames;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getResults() {
		return results;
	}
	public void setResults(Object results) {
		this.results = results;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}

}
