package org.trescal.cwms.core.jobs.job.form;

import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.jobs.job.entity.job.Job;

public class ChangeClientForm
{
	private Integer addrid;
	private boolean incDeliveries;
	private boolean incInstruments;
	private Job job;
	private Integer personid;

	/**
	 * @return the addrid
	 */
	@NotNull(message = "A Delivery Address must be selected for the Job.")
	public Integer getAddrid()
	{
		return this.addrid;
	}

	/**
	 * @return the job
	 */
	public Job getJob()
	{
		return this.job;
	}

	/**
	 * @return the personid
	 */
	@NotNull(message = "A Contact must be selected for the Job.")
	public Integer getPersonid()
	{
		return this.personid;
	}


	/**
	 * @return the incDeliveries
	 */
	public boolean isIncDeliveries()
	{
		return this.incDeliveries;
	}

	/**
	 * @return the incInstruments
	 */
	public boolean isIncInstruments()
	{
		return this.incInstruments;
	}

	/**
	 * @param addrid the addrid to set
	 */
	public void setAddrid(Integer addrid)
	{
		this.addrid = addrid;
	}

	/**
	 * @param incDeliveries the incDeliveries to set
	 */
	public void setIncDeliveries(boolean incDeliveries)
	{
		this.incDeliveries = incDeliveries;
	}

	/**
	 * @param incInstruments the incInstruments to set
	 */
	public void setIncInstruments(boolean incInstruments)
	{
		this.incInstruments = incInstruments;
	}

	/**
	 * @param job the job to set
	 */
	public void setJob(Job job)
	{
		this.job = job;
	}

	/**
	 * @param personid the personid to set
	 */
	public void setPersonid(Integer personid)
	{
		this.personid = personid;
	}


}