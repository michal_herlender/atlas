package org.trescal.cwms.core.jobs.certificate.dto;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.time.LocalDate;
import java.util.Date;

@Getter @Setter
public class WebCertificateJobItemLinkProjectionDTO {
	
	private Integer certId;
	private Integer itemNo;
	private Integer jobId;
	private String jobNo;
	private WebCertificateInstrumentLinkProjectionDTO instDto;

    public WebCertificateJobItemLinkProjectionDTO(Integer certId, Integer instPlantid, String instPlantno, String instSerialno,
                                                  String instComplementaryFieldCustomerAcceptanceCriteria, Boolean instScrapped, String instCustomerDescription,
                                                  String instrumentModelName, String instModelName, Integer typology, ModelMfrType modelMfrType, String mfrName, Boolean mfrGeneric,
                                                  InstrumentStatus instStatus, Integer itemNo, Integer jobId, String jobNo, LocalDate instAddedOn, LocalDate instNextCalDueDate,
                                                  Date instLastCalCompleteTime, Integer instContactId, Integer instAddressId, Integer instSubdivId,
                                                  Integer calibrationInterval, IntervalUnit calibrationIntervalUnit) {
        this.certId = certId;
        this.itemNo = itemNo;
        this.jobId = jobId;
        this.jobNo = jobNo;
        this.instDto = new WebCertificateInstrumentLinkProjectionDTO(certId, instPlantid, instPlantno, instSerialno,
            instComplementaryFieldCustomerAcceptanceCriteria, instScrapped, instCustomerDescription, instrumentModelName,
            instModelName, typology, modelMfrType, mfrName, mfrGeneric, instStatus, instAddedOn, instNextCalDueDate, instLastCalCompleteTime,
            instContactId, instAddressId, instSubdivId, calibrationInterval, calibrationIntervalUnit);
    }
}
