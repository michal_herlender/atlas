package org.trescal.cwms.core.jobs.calibration.entity.standardused.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration_;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus_;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink_;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;

@Repository("StandardUsedDao")
public class StandardUsedDaoImpl extends BaseDaoImpl<StandardUsed, Integer> implements StandardUsedDao {

	@Override
	protected Class<StandardUsed> getEntity() {
		return StandardUsed.class;
	}

	/**
	 * Fetches instrument for basic display purposes 
	 */
	public List<StandardUsed> getAllStandardsUsedOnCal(Integer calId) {
		return getResultList(cb -> {
			CriteriaQuery<StandardUsed> cq = cb.createQuery(StandardUsed.class);
			Root<Calibration> calibration = cq.from(Calibration.class);
			Join<Calibration, StandardUsed> standardUsed = calibration.join(Calibration_.standardsUsed);
			standardUsed.fetch(StandardUsed_.inst);
			cq.where(cb.equal(calibration.get(Calibration_.id), calId));
			cq.select(standardUsed);
			return cq;
		});
	}

	@Override
	public List<StandardUsed> getAllStandardsUsedOnJobItem(Integer jobItemId) {
		return getResultList(cb -> {
			CriteriaQuery<StandardUsed> cq = cb.createQuery(StandardUsed.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, CalLink> calLink = jobItem.join(JobItem_.calLinks);
			Join<CalLink, Calibration> calibration = calLink.join(CalLink_.cal);
			Join<Calibration, CalibrationStatus> calStatus = calibration.join(Calibration_.status);
			Join<Calibration, StandardUsed> standardUsed = calibration.join(Calibration_.standardsUsed);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.jobItemId), jobItemId));
			clauses.getExpressions().add(cb.equal(calStatus.get(CalibrationStatus_.name), CalibrationStatus.COMPLETE));
			cq.where(clauses);
			cq.distinct(true);
			cq.select(standardUsed);
			return cq;
		});
	}

	@Override
	public Boolean standardsUsedOnJobItem(Integer jobItemId) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, CalLink> calLink = jobItem.join(JobItem_.calLinks);
			Join<CalLink, Calibration> calibration = calLink.join(CalLink_.cal);
			Join<Calibration, CalibrationStatus> calStatus = calibration.join(Calibration_.status);
			Join<Calibration, StandardUsed> standardUsed = calibration.join(Calibration_.standardsUsed);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.jobItemId), jobItemId));
			clauses.getExpressions().add(cb.equal(calStatus.get(CalibrationStatus_.name), CalibrationStatus.COMPLETE));
			cq.where(clauses);
			cq.select(cb.count(standardUsed));
			return cq;
		}) > 0;
	}
}