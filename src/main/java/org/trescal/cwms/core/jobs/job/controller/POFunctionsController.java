package org.trescal.cwms.core.jobs.job.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.job.dto.*;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.po.db.POService;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemPODto;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.db.JobExpenseItemPOService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.db.JobItemPOService;
import org.trescal.cwms.core.logistics.dto.PoDTO;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.userright.enums.Permission;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@JsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_USERNAME })
public class POFunctionsController {

	@Autowired
	private JobItemPOService jobItemPOService;
	@Autowired
	private JobExpenseItemPOService expenseItemPOService;
	@Autowired
	private POService poService;
	@Autowired
	private BPOService bpoService;

	@RequestMapping(value = "/resetitemsonpo.json", method = RequestMethod.POST)
	@ResponseBody
	public boolean assignJobItemsToBPO(
			@RequestParam(value = "jobitemsid[]", required = false, defaultValue = "") List<Integer> jobItemsId,
			@RequestParam(value = "jobservicesid[]", required = false, defaultValue = "") List<Integer> jobServicesId,
			@RequestParam(value = "poid") Integer poId,
			@RequestParam(value = "jobid") Integer jobId) {
		this.jobItemPOService.assignItemsToPO(jobItemsId, jobServicesId, poId, jobId);
		return true;
	}

	@RequestMapping(value = "/purchaseOrders.json", method = RequestMethod.GET)
	@ResponseBody
	public PurchaseOrderComponentDTO getPurchaseOrders(@RequestParam(name = "jobId") Integer jobId,
			@RequestParam(name = "personId") Integer personId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyDto) {
		List<ClientPurchaseOrderDTO> poDtos = poService.getProjectionByJob(jobId);
		Integer jobDefaultPO = poDtos.stream().filter(ClientPurchaseOrderDTO::getJobDefault).map(ClientPurchaseOrderDTO::getPoId)
				.findFirst().orElse(0);
		Map<Scope, List<BpoDTO>> bpoMap = bpoService.findAllForJob(jobId, false);
		List<BpoDTO> bpoDtos = bpoMap.values().stream().flatMap(List::stream).collect(Collectors.toList());
		Integer jobDefaultBPO = bpoDtos.stream().filter(BpoDTO::getDefaultForJob).map(BpoDTO::getPoId).findFirst()
				.orElse(0);
		List<BpoDTO> availableBPOs = bpoService.getAllByContactHierarchicalNotOnJob(personId, companyDto.getKey(),
				jobId);
		Collection<? extends GrantedAuthority> auth = SecurityContextHolder.getContext().getAuthentication()
				.getAuthorities();
		boolean hasAuthToCreatePO = auth.contains(Permission.PURCHASE_ORDER_CREATE);
		boolean hasAuthToEdit = auth.contains(Permission.PURCHASE_ORDER_CLIENT_EDIT);
		boolean hasAuthToDeactivate = auth.contains(Permission.PURCHASE_ORDER_CLIENT_DE_ACTIVATE);
		boolean hasAuthToAddAndRemoveItems = auth.contains(Permission.PURCHASE_ORDER_CLIENT_JOB_ITEM_ADD_REMOVE);
		boolean hasAuthToRemoveBPO = auth.contains(Permission.BPO_REMOVE);
		return new PurchaseOrderComponentDTO(poDtos, jobDefaultPO, bpoDtos, jobDefaultBPO, availableBPOs,
				hasAuthToCreatePO, hasAuthToAddAndRemoveItems, hasAuthToEdit, hasAuthToDeactivate, hasAuthToRemoveBPO);
	}

	@RequestMapping(value = "/updateJobDefaultPO.json", method = RequestMethod.POST)
	@ResponseBody
	public boolean updateJobDefaultPO(@RequestParam(name = "poId") Integer poId,
			@RequestParam(name = "jobId") Integer jobId) {
		poService.updateJobPODefault(jobId, poId);
		return true;
	}

	@RequestMapping(value = "/updateJobDefaultBPO.json", method = RequestMethod.POST)
	@ResponseBody
	public boolean updateJobDefaultBPO(@RequestParam(name = "poId") Integer poId,
			@RequestParam(name = "jobId") Integer jobId) {
		bpoService.updateJobBPODefault(jobId, poId);
		return true;
	}

	@RequestMapping(value = "/purchaseOrderItems.json", method = RequestMethod.GET)
	@ResponseBody
	public PurchaseOrderItemComponentDTO getPurchaseOrderItems(
			@RequestParam(name = "poId") Integer poId) {
		List<ClientPurchaseOrderItemDTO> jobItems = jobItemPOService.getOnPO(poId);
		List<ClientPurchaseOrderExpenseItemDTO> expenseItems = expenseItemPOService.getOnPO(poId);
		return new PurchaseOrderItemComponentDTO(jobItems, expenseItems);
	}

	@RequestMapping(value = "/blanketPurchaseOrderItems.json", method = RequestMethod.GET)
	@ResponseBody
	public PurchaseOrderItemComponentDTO getBlanketPurchaseOrderItems(
			@RequestParam(name = "poId") Integer poId,
			@RequestParam(name = "jobId") Integer jobId) {
		List<ClientPurchaseOrderItemDTO> jobItems = jobItemPOService.getOnBPO(poId, jobId);
		List<ClientPurchaseOrderExpenseItemDTO> expenseItems = expenseItemPOService.getOnBPO(poId, jobId);
		return new PurchaseOrderItemComponentDTO(jobItems, expenseItems);
	}

	@RequestMapping(value = "/deactivatePO.json", method = RequestMethod.POST)
	@ResponseBody
	public boolean deactivatePO(@RequestParam(name = "poId") Integer poId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName) {
		poService.deactivatePO(poId, userName);
		return true;
	}

	@RequestMapping(value = "/createPoOnJob.json", method = RequestMethod.POST)
	@ResponseBody
	public ClientPurchaseOrderDTO createPO(@RequestParam(name = "jobId") Integer jobId,
			@RequestParam(name = "poNumber") String poNumber,
			@RequestParam(name = "comment", required = false, defaultValue = "") String comment,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyDto) {
		PO po = poService.createPO(jobId, poNumber, comment, companyDto.getKey());
		return new ClientPurchaseOrderDTO(po);
	}

	@PutMapping(value = "/editPoOnJob.json")
	public void editPO(@RequestParam(name = "poId") Integer poId,
			@RequestParam(name = "poNumber") String poNumber,
			@RequestParam(name = "comment", required = false, defaultValue = "") String comment) {
		PO po = poService.get(poId);
		po.setComment(comment);
		po.setPoNumber(poNumber);
		poService.merge(po);
	}


	@GetMapping("validPOsForJobItem.json")
	public List<PoDTO> validPOsForJobItem(@RequestParam Integer jobItemId){
		return jobItemPOService.getValidPODtosForJobItem(jobItemId);
	}

	@PutMapping("assignItemToPO.json")
	public JobItemPODto assingItemToPO(@RequestParam Integer jobItemId, @RequestParam Integer poId){
		return JobItemPODto.fromJobItemPpo(jobItemPOService.insertJobItemPOWithIds(jobItemId,poId));
	}

	@PutMapping("deleteJobItemPObyId.json")
	public void deleteJobItemPOById(@RequestParam Integer jobItemPOId){
		jobItemPOService.deleteJobItemPOById(jobItemPOId);
	}
}