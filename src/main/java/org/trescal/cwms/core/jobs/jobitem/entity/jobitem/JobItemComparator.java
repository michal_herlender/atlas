package org.trescal.cwms.core.jobs.jobitem.entity.jobitem;

import java.util.Comparator;

/**
 * Inner comparator class for sorting JobItems when returning collections of
 * jobs, each with multiple items
 * 
 * @author jamiev
 */
public class JobItemComparator implements Comparator<JobItem>
{
	public int compare(JobItem jobItem, JobItem anotherJobItem)
	{
		// compare jobs
		Integer jobid1 = jobItem.getJob().getJobid();
		Integer jobid2 = anotherJobItem.getJob().getJobid();

		if (jobid1.equals(jobid2))
		{
			// compare itemNos
			Integer itemNo1 = jobItem.getItemNo();
			Integer itemNo2 = anotherJobItem.getItemNo();

			if ((itemNo1 == null) || (itemNo2 == null) || itemNo1.equals(itemNo2))
			{
				// items nos may be null during unit testing
				// equality should never happen (but is in so that if something goes
				// wrong and item numbers are the same, both items will still
				// show up on the job screen)
				Integer id1 = jobItem.getJobItemId();
				Integer id2 = anotherJobItem.getJobItemId();

				return id1.compareTo(id2);
			}
			else
			{
				return itemNo1.compareTo(itemNo2);
			}
		}
		else
		{
			return jobid1.compareTo(jobid2);
		}
	}
}