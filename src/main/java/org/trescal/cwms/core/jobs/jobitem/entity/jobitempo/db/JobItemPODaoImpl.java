package org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.persistence.metamodel.SingularAttribute;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.jobs.job.dto.BpoDTO;
import org.trescal.cwms.core.jobs.job.dto.ClientPurchaseOrderItemDTO;
import org.trescal.cwms.core.jobs.job.entity.abstractpo.AbstractPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.po.PO_;
import org.trescal.cwms.core.jobs.job.projection.ClientPurchaseOrderProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitempo.AbstractJobItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO_;
import org.trescal.cwms.core.logistics.dto.PoDTO;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;

import lombok.val;

@Repository
public class JobItemPODaoImpl extends BaseDaoImpl<JobItemPO, Integer> implements JobItemPODao {

	@Override
	protected Class<JobItemPO> getEntity() {
		return JobItemPO.class;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void deleteBPOLinksToJob(int bpoid, int jobid) {
		Criteria jipoCrit = getSession().createCriteria(JobItemPO.class);
		Criteria jobItemCrit = jipoCrit.createCriteria("item");
		Criteria jobCrit = jobItemCrit.createCriteria("job");
		jobCrit.add(Restrictions.idEq(jobid));
		Criteria bpoCrit = jipoCrit.createCriteria("bpo");
		bpoCrit.add(Restrictions.idEq(bpoid));
		List<JobItemPO> list = jipoCrit.list();
		for (JobItemPO jipo : list) {
			JobItem ji = jipo.getItem();
			ji.getItemPOs().remove(jipo);
			this.remove(jipo);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<JobItem> getItemsOnBPO(int bpoId, int jobId) {
		Criteria jipoCrit = getSession().createCriteria(JobItemPO.class);
		jipoCrit.setFetchMode("item", FetchMode.JOIN);
		jipoCrit.setFetchMode("item.inst", FetchMode.JOIN);
		jipoCrit.setFetchMode("item.inst.model", FetchMode.JOIN);
		jipoCrit.setFetchMode("item.inst.model.mfr", FetchMode.JOIN);
		jipoCrit.setFetchMode("item.inst.model.description", FetchMode.JOIN);
		jipoCrit.setFetchMode("item.calType", FetchMode.JOIN);
		jipoCrit.setFetchMode("item.calType.serviceType", FetchMode.JOIN);
		Criteria bpoCrit = jipoCrit.createCriteria("bpo");
		Criteria jobCrit = jipoCrit.createCriteria("item").createCriteria("job");
		bpoCrit.add(Restrictions.idEq(bpoId));
		jobCrit.add(Restrictions.idEq(jobId));
		List<JobItemPO> jipos = jipoCrit.list();
		ArrayList<JobItem> items = new ArrayList<JobItem>();
		for (JobItemPO jipo : jipos)
			items.add(jipo.getItem());
		return items;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<JobItem> getItemsOnPO(int poId) {
		Criteria jipoCrit = getSession().createCriteria(JobItemPO.class);
		Criteria jiCrit = jipoCrit.createCriteria("item");
		jiCrit.setFetchMode("inst", FetchMode.JOIN);
		jiCrit.setFetchMode("inst.model", FetchMode.JOIN);
		jiCrit.setFetchMode("inst.model.mfr", FetchMode.JOIN);
		jiCrit.setFetchMode("inst.model.description", FetchMode.JOIN);
		jiCrit.setFetchMode("calType", FetchMode.JOIN);
		jiCrit.addOrder(Order.asc("itemNo"));
		Criteria poCrit = jipoCrit.createCriteria("po");
		poCrit.add(Restrictions.idEq(poId));
		List<JobItemPO> jipos = jipoCrit.list();
		ArrayList<JobItem> items = new ArrayList<JobItem>();
		for (JobItemPO jipo : jipos)
			items.add(jipo.getItem());
		return items;
	}

	@Override
	public List<ClientPurchaseOrderProjectionDTO> getProjectionDTOs(Collection<Integer> jobIds,
			Collection<Integer> jobItemIds) {
		return getResultList(cb -> {
			CriteriaQuery<ClientPurchaseOrderProjectionDTO> cq = cb.createQuery(ClientPurchaseOrderProjectionDTO.class);
			Root<JobItemPO> root = cq.from(JobItemPO.class);
			Join<JobItemPO, JobItem> jobItem = root.join(JobItemPO_.ITEM, JoinType.INNER);
			Join<JobItemPO, BPO> bpo = root.join(JobItemPO_.bpo, JoinType.LEFT);
			Join<JobItemPO, PO> po = root.join(JobItemPO_.po, JoinType.LEFT);

			Predicate clauses = cb.disjunction();
			if (jobIds != null && !jobIds.isEmpty()) {
				Join<JobItem, Job> job = jobItem.join(JobItem_.job, JoinType.INNER);
				clauses.getExpressions().add(job.get(Job_.jobid).in(jobIds));
			}
			if (jobItemIds != null && !jobItemIds.isEmpty()) {
				clauses.getExpressions().add(jobItem.get(JobItem_.jobItemId).in(jobItemIds));
			}
			if (clauses.getExpressions().isEmpty())
				throw new UnsupportedOperationException("Query must be called with at least one jobId or jobItemId");
			cq.where(clauses);

			cq.select(cb.construct(ClientPurchaseOrderProjectionDTO.class, root.get(JobItemPO_.id),
					jobItem.get(JobItem_.jobItemId), bpo.get(BPO_.poId), bpo.get(BPO_.poNumber), po.get(PO_.poId),
					po.get(PO_.poNumber)));

			return cq;
		});
	}

	@Override
	public List<PoDTO> getValidPODtosForJobItem(Integer jobItemId) {
		return getResultList(cb -> {
			val cq = cb.createQuery(PoDTO.class);
			val po = cq.from(PO.class);
			val job = po.join(PO_.job);
			cq.where(cb.equal(job, jobIdForJobItemId(jobItemId, cq, cb)),
					cb.not(po.get(PO_.poId).in(poIdsForJobItemId(jobItemId, cq, cb))));
			cq.select(cb.construct(PoDTO.class, po.get(PO_.poId), po.get(PO_.poNumber), po.get(PO_.comment)));
			return cq;
		});
	}

	private Subquery<Integer> poIdsForJobItemId(Integer jobItemId, CriteriaQuery<PoDTO> cq, CriteriaBuilder cb) {
		val ccq = cq.subquery(Integer.class);
		val jobItemPO = ccq.from(JobItemPO.class);
		val po = jobItemPO.join(JobItemPO_.po, JoinType.INNER);
		ccq.where(cb.equal(jobItemPO.get(JobItemPO_.item), jobItemId));
		ccq.select(po.get(PO_.poId));
		return ccq;
	}

	private Subquery<Integer> jobIdForJobItemId(Integer jobItemId, CriteriaQuery<PoDTO> cq, CriteriaBuilder cb) {
		val ccq = cq.subquery(Integer.class);
		val jobItem = ccq.from(JobItem.class);
		ccq.where(cb.equal(jobItem, jobItemId));
		ccq.select(jobItem.get(JobItem_.job).get(Job_.jobid));
		return ccq;
	};

	@Override
	public String getAllPOsByJobItem(int jobItemId) {

		List<String> listPOs = new ArrayList<>();

		List<PoDTO> pos = getResultList(cb -> {
			CriteriaQuery<PoDTO> cq = cb.createQuery(PoDTO.class);
			Root<JobItemPO> jobItemPo = cq.from(JobItemPO.class);
			Join<JobItemPO, PO> po = jobItemPo.join(JobItemPO_.po, JoinType.LEFT);
			cq.where(cb.equal(jobItemPo.get(JobItemPO_.item), jobItemId));
			cq.select(cb.construct(PoDTO.class, po.get(PO_.poId), po.get(PO_.poNumber), po.get(PO_.comment)));
			return cq;
		});

		List<BpoDTO> bpos = getResultList(cb -> {
			CriteriaQuery<BpoDTO> cq = cb.createQuery(BpoDTO.class);
			Root<JobItemPO> jobItemPo = cq.from(JobItemPO.class);
			Join<JobItemPO, BPO> bpo = jobItemPo.join(JobItemPO_.bpo, JoinType.LEFT);
			cq.where(cb.equal(jobItemPo.get(JobItemPO_.item), jobItemId));
			cq.select(cb.construct(BpoDTO.class, bpo.get(BPO_.poId), bpo.get(BPO_.poNumber), bpo.get(BPO_.comment)));
			return cq;
		});

		pos.stream().forEach(po -> {
			if (po.getPoNumber() != null)
				listPOs.add(po.getPoNumber());
		});

		bpos.stream().forEach(bpo -> {
			if (bpo.getPoNumber() != null)
				listPOs.add(bpo.getPoNumber());
		});

		return String.join(",", listPOs);
	}

	@SuppressWarnings("rawtypes") // necessary due to metamodel generation of
									// generic classes
	private <T extends AbstractPO> List<ClientPurchaseOrderItemDTO> getOnPOorBPO(Integer poId, Integer jobId,
			SingularAttribute<AbstractJobItemPO, T> poAttribute, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<ClientPurchaseOrderItemDTO> cq = cb.createQuery(ClientPurchaseOrderItemDTO.class);
			Root<JobItemPO> jobItemPo = cq.from(JobItemPO.class);
			Join<JobItemPO, JobItem> jobItem = jobItemPo.join(JobItemPO_.ITEM);
			Join<JobItem, Instrument> instrument = jobItem.join(JobItem_.inst);
			Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model);
			Expression<String> modelName = joinTranslation(cb, model, InstrumentModel_.nameTranslations, locale);
			Join<JobItem, CalibrationType> calType = jobItem.join(JobItem_.calType, JoinType.LEFT);
			Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType, JoinType.LEFT);
			Expression<String> serviceTypeName = joinTranslation(cb, serviceType, ServiceType_.shortnameTranslation,
					locale);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(jobItemPo.get(poAttribute), poId));
			if (jobId != null)
				clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.job), jobId));
			cq.where(clauses);
			cq.orderBy(cb.asc(jobItem.get(JobItem_.itemNo)));
			cq.select(cb.construct(ClientPurchaseOrderItemDTO.class, jobItem.get(JobItem_.itemNo), modelName,
					instrument.get(Instrument_.serialno), instrument.get(Instrument_.plantno), serviceTypeName));
			return cq;
		});
	}

	@Override
	public List<ClientPurchaseOrderItemDTO> getOnPO(Integer poId, Locale locale) {
		return getOnPOorBPO(poId, null, JobItemPO_.po, locale);
	}

	@Override
	public List<ClientPurchaseOrderItemDTO> getOnBPO(Integer poId, Integer jobId, Locale locale) {
		return getOnPOorBPO(poId, jobId, JobItemPO_.bpo, locale);
	}


	@Override
	public List<Integer> getJobitemIdsOnBPO(Integer bpoid, Integer jobId) {
		return getResultList(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);

			Root<JobItemPO> jobItemPo = cq.from(JobItemPO.class);
			Join<JobItemPO, JobItem> jobItem = jobItemPo.join(JobItemPO_.ITEM);

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.job), jobId));
			clauses.getExpressions().add(cb.equal(jobItemPo.get(JobItemPO_.bpo), bpoid));

			cq.where(clauses);

			cq.select(jobItem.get(JobItem_.jobItemId));

			return cq;
		});
	}

	@Override
	public JobItemPO getJobItemPo(Integer jobItemId, int bpoid, Integer jobId) {
		return getSingleResult(cb ->{
			CriteriaQuery<JobItemPO> cq = cb.createQuery(JobItemPO.class);
			Root<JobItemPO> jobItemPo = cq.from(JobItemPO.class);
			Join<JobItemPO, JobItem> jobItem = jobItemPo.join(JobItemPO_.ITEM);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.job), jobId));
			clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.jobItemId), jobItemId));
			clauses.getExpressions().add(cb.equal(jobItemPo.get(JobItemPO_.bpo), bpoid));
			cq.where(clauses);
			return cq;
		});
	}


}