package org.trescal.cwms.core.jobs.jobitem.entity.pat.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.pat.PAT;

@Repository("PATDao")
public class PATDaoImpl extends BaseDaoImpl<PAT, Integer> implements PATDao {
	
	@Override
	protected Class<PAT> getEntity() {
		return PAT.class;
	}
}