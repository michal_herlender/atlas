package org.trescal.cwms.core.jobs.certificate.entity.certaction.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.certificate.entity.certaction.CertAction;

@Repository("CertActionDao")
public class CertActionDaoImpl extends BaseDaoImpl<CertAction, Integer> implements CertActionDao {
	
	@Override
	protected Class<CertAction> getEntity() {
		return CertAction.class;
	}
}