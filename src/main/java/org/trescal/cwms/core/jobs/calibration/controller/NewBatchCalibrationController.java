package org.trescal.cwms.core.jobs.calibration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactDWRService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationToBatch;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.db.BatchCalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.calibration.form.NewBatchCalibrationForm;
import org.trescal.cwms.core.jobs.calibration.form.NewBatchCalibrationValidator;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.procedure.entity.procedurestandard.CapabilityStandard;
import org.trescal.cwms.core.procedure.entity.workinstructionstandard.WorkInstructionStandard;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.instructionlink.db.InstructionLinkService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class NewBatchCalibrationController {

    @Value("#{props['cwms.config.certificate.batch.automatic_printing']}")
    private boolean allowUncalibratedStds;
    @Value("#{props['cwms.config.calibration.allow_uncalibrated_standards']}")
    private boolean autoPrinting;
    @Autowired
    private BatchCalibrationService batchCalServ;
    @Autowired
    private CalibrationProcessService calProServ;
    @Autowired
    private CalReqService calReqServ;
    @Autowired
    private CalibrationService calServ;
    @Autowired
    private ContactDWRService conServ;
    @Autowired
    private InstructionLinkService instructionLinkServ;
    @Autowired
    private InstrumService instServ;
    @Autowired
    private JobItemService jiServ;
    @Autowired
    private JobService jobServ;
    @Autowired
    private CapabilityAuthorizationService procAccServ;
    @Autowired
    private CapabilityService procServ;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private UserService userService;
    @Autowired
    private NewBatchCalibrationValidator validator;

    @ModelAttribute("form")
    protected NewBatchCalibrationForm formBackingObject(
        @RequestParam(value = "batchid", required = false, defaultValue = "0") Integer batchId,
        @RequestParam(value = "jobid", required = false, defaultValue = "0") Integer jobId,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		if (batchId == 0 && jobId == 0)
			throw new IllegalArgumentException("Either a jobId or batchId must be specified!");
		NewBatchCalibrationForm form = new NewBatchCalibrationForm();

		// load existing batch
		BatchCalibration batchCal = null;
		if (batchId > 0) {
			batchCal = this.batchCalServ.findBatchCalibration(batchId);
		}

		Job job;

		// if starting new batch
		if (batchCal == null) {
			// load job from request parameter
			job = this.jobServ.get(jobId);
		}
		// else if adding to existing batch
		else {
			// reload job for batch
			job = this.jobServ.get(batchCal.getJob().getJobid());
		}

		// store existing batch on form backing object
		form.setExistingBatch(batchCal);

		// store job on form backing object
		form.setJob(job);

		// load signed-in contact
		int contactId = this.userService.get(username).getCon().getPersonid();

		List<JobItem> itemsReadyForCal;

		// if starting a new batch
		if (batchCal == null) {
			// get job items ready for calibration
			itemsReadyForCal = this.jiServ.getItemsOnJobInStateGroup(job.getJobid(), StateGroup.AWAITINGCALIBRATION);

			// set default cal process to CWMS
			form.setCalProcessId(this.calProServ.findByName("CWMS").getId());

			// set calibration date to today's date
			form.setCalDate(new Date());
		}
		// else if adding to existing batch
		else {
			// get job items not on batch ready for calibration
			itemsReadyForCal = this.jiServ.getRemainingItemsOnJobInStateGroup(batchCal, StateGroup.AWAITINGCALIBRATION);
		}

		// create list of calibrations to batch
		List<CalibrationToBatch> ctbs = new ArrayList<>();

		// set information into DTOs
		for (JobItem ji : itemsReadyForCal) {
            CalibrationToBatch ctb = new CalibrationToBatch();

            ctb.setJi(ji);
            Integer procId = (ji.getCapability() == null) ? null : ji.getCapability().getId();
            ctb.setProcId(procId);
            boolean cwmsProcess = ji.getCapability() != null && (ji.getCapability().getCalibrationProcess() != null && (ji.getCapability().getCalibrationProcess().getApplicationComponent()));
            ctb.setCwmsProcess(cwmsProcess);
            Integer workInstructionId = (ji.getWorkInstruction() == null) ? null : ji.getWorkInstruction().getId();
            ctb.setWorkInstructionId(workInstructionId);
            Boolean accredited = ((ji.getServiceType().getCalibrationType() == null) || (procId == null)) ? null
                : this.procAccServ.authorizedFor(procId, ji.getServiceType().getCalibrationType().getCalTypeId(), contactId).isRight();
            ctb.setAccredited(accredited);
            boolean toBatch = accredited != null && accredited;
            ctb.setToBatch(toBatch);

            List<Integer> stds = new ArrayList<>();
            if (ji.getCapability() != null) {
                for (CapabilityStandard std : ji.getCapability().getStandards()) {
                    stds.add(std.getInstrument().getPlantid());
                }
            }
            if (ji.getWorkInstruction() != null) {
                for (WorkInstructionStandard std : ji.getWorkInstruction().getStandards()) {
                    stds.add(std.getInstrument().getPlantid());
                }
            }
            ctb.setStandardIds(stds);

            ctbs.add(ctb);
        }

		// set DTOs into backing object
		form.setItemCals(ctbs);

		// set auto printing + enforced stds
		form.setAutoPrinting(this.autoPrinting);
		form.setAllowUncalibratedStds(this.allowUncalibratedStds);

		// fetch and add related instructions to job
		form.getJob().setRelatedInstructionLinks(this.instructionLinkServ.getAllForJob(job));

		// return FBO
		return form;
	}

	@InitBinder("form")
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		// Validator is invoked manually in mapped methods due to onBind() calls
	}

	private void onBind(NewBatchCalibrationForm form, String username) {
		// set owner as current contact
		Contact currentContact = this.userService.get(username).getCon();

		form.setOwner(currentContact);

		// set standards as instruments into each cal instead of ids
		for (CalibrationToBatch ctb : form.getItemCals()) {
			List<Instrument> standards = new ArrayList<>();
			if (ctb.getStandardIds() == null) {
				ctb.setStandardIds(new ArrayList<>());
			}
			for (Integer plantid : ctb.getStandardIds()) {
				standards.add(this.instServ.get(plantid));
			}

			// convert standards to a set and back so that any standards added
			// in both the procedure and work instruction lists are only
			// included once
			Set<Instrument> set = new HashSet<>(standards);
			standards.clear();
			standards.addAll(set);

			ctb.setStandards(standards);
		}
	}

	private void updateEncryptedPassword(HttpServletRequest request, NewBatchCalibrationForm form) {
		// if new password has been set
		if (form.getOwner().getEncryptedPassword() == null) {
			// set password to current contact
			this.conServ.setEncryptedPassword(form.getOwner().getPersonid(), form.getPassword(), request);
		}
	}

	@RequestMapping(value = {"/newbatchcalibration.htm", "/addcalibrationstobatch.htm"}, method = RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest request, HttpSession session,
									@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
									@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
									@ModelAttribute("form") NewBatchCalibrationForm form, BindingResult bindingResult) {
		onBind(form, username);
		validator.validate(form, bindingResult);
		if (bindingResult.hasErrors()) {
			return referenceData(request, form, username);
		}
		updateEncryptedPassword(request, form);
		Subdiv subdiv = subdivService.get(subdivDto.getKey());
		// if creating a new batch calibration
		if (form.getExistingBatch() == null) {
			// create the batch calibration
			BatchCalibration batchCal = this.batchCalServ.createBatchCalibration(form, subdiv);

			// if next cal is to be manually selected
			if (batchCal.isChooseNext()) {
				// redirect to the view batch screen
				return new ModelAndView(new RedirectView("viewbatchcalibration.htm?batchid=" + batchCal.getId()));
			}

			// get first calibration
			Calibration cal = this.batchCalServ.getNextIncompleteCalibrationInBatch(batchCal.getId(),
					form.getOwner().getPersonid());

			// set calibration as started
			this.calServ.startCalibration(cal, form.getOwner(), null, null);

			// run procedure for first calibration
			return new ModelAndView(new RedirectView("runproc.htm?calid=" + cal.getId() + "&batchid=" + batchCal.getId()
					+ "&runfile=" + cal.getXindiceKey()));
		}
		// else if adding calibrations to existing batch
		else {
			// add the calibrations
			this.batchCalServ.addCalibrationsToBatch(form, subdiv);
			// go back to the view batch screen
			return new ModelAndView(
					new RedirectView("viewbatchcalibration.htm?batchid=" + form.getExistingBatch().getId()));
		}
	}

	@RequestMapping(value = {"/newbatchcalibration.htm", "/addcalibrationstobatch.htm"}, method = RequestMethod.GET)
	protected ModelAndView referenceData(HttpServletRequest request,
										 @ModelAttribute("form") NewBatchCalibrationForm form,
										 @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        onBind(form, username);
        Map<String, Object> refData = new HashMap<>();
        refData.put("certtemplates", new ArrayList<>());
        HashMap<Integer, Capability> procedures = new HashMap<>();
        List<Capability> procs = this.procServ.getAll();
        for (Capability proc : procs) {
            procedures.put(proc.getId(), proc);
        }
        refData.put("procedures", procedures);
        refData.put("calprocesses", this.calProServ.getAll());

        int itemsWithReqs = 0;
        for (JobItem ji : form.getJob().getItems()) {
            if (this.calReqServ.findCalReqsForJobItem(ji) != null) {
                itemsWithReqs++;
            }
        }
        refData.put("itemsWithReqs", itemsWithReqs);

        return new ModelAndView("trescal/core/jobs/calibration/newbatchcalibration", refData);
    }
}
