package org.trescal.cwms.core.jobs.calibration.entity.standardused;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StandardUsedDto {
	
	private Integer standardUsedId;
	private Integer plantId;
	private String translatedModelName;
	private String serialNumber;
	private String plantNumber;
	private String calDueDateAtTimeOfUse;
	private String dateOfUse;
	private Boolean inCalAtTimeOfUse;
	private Integer certId;
	private Boolean fileAvailable;
	private String definitiveInstrument;

	public StandardUsedDto() {
		super();
	}
	
	public StandardUsedDto(Integer standardUsedId, Integer plantId, String serialNumber, String plantNumber, String definitiveInstrument) {
		super();
		this.standardUsedId = standardUsedId;
		this.plantId = plantId;
		this.serialNumber = serialNumber;
		this.plantNumber = plantNumber;
		this.definitiveInstrument = definitiveInstrument;
	} 
	
}
