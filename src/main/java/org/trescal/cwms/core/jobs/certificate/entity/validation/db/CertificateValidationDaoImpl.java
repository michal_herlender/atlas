package org.trescal.cwms.core.jobs.certificate.entity.validation.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidation;

@Repository
public class CertificateValidationDaoImpl extends BaseDaoImpl<CertificateValidation, Integer> implements CertificateValidationDao {
	
	@Override
	protected Class<CertificateValidation> getEntity() {
		return CertificateValidation.class;
	}
}