package org.trescal.cwms.core.jobs.repair.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Size;

import org.trescal.cwms.core.jobs.repair.component.enums.RepairComponentSourceEnum;
import org.trescal.cwms.core.jobs.repair.component.enums.RepairComponentStateEnum;

import lombok.Data;

@Data
public class FreeRepairComponentDTO {

	private Integer frComponentId;
	@Size(min = 2, max = 100)
	private String name;
	private RepairComponentSourceEnum source;
	private Integer quantity;
	private BigDecimal cost;
	private RepairComponentStateEnum status;
	private RepairComponentStateEnum rirStatus;
	private Boolean addedAfterRiRValidation;
	private String freeRepairOperationName;
	private Integer freeRepairOperationId;
	private Integer validatedFreeRepairComponentId;
	private Integer componentId;

}
