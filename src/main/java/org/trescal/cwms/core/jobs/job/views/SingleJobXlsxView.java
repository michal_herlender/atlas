package org.trescal.cwms.core.jobs.job.views;

import lombok.val;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Map;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDate;

@Component
public class SingleJobXlsxView extends AbstractXlsxStreamingView {

	@Autowired
	private LocaleResolver localeResolver;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private TranslationService tranServ;
	
	public static final Logger logger = LoggerFactory.getLogger(SingleJobXlsxView.class);
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		long startTime = System.currentTimeMillis();
		Locale userLocale = localeResolver.resolveLocale(request);

		Job job = (Job) model.get("job");
		
		// create a new Excel sheet
		Sheet sheet1 = workbook.createSheet(messageSource.getMessage("web.job", null, "Job", userLocale));
		sheet1.setDefaultColumnWidth(35);
		
		
		// create style for header cells
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		style.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		font.setColor(HSSFColor.WHITE.index);
		style.setFont(font);

		// create style for header cells
		CellStyle styleHeader = workbook.createCellStyle();
		Font fontheader = workbook.createFont();
		fontheader.setFontName("Arial");
		styleHeader.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
		styleHeader.setFillPattern(CellStyle.SOLID_FOREGROUND);
		fontheader.setBoldweight(Font.BOLDWEIGHT_BOLD);
		fontheader.setColor(HSSFColor.WHITE.index);
		styleHeader.setFont(fontheader);

		workbook.createCellStyle();
		style.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style.setFont(font);

		// create style for date cells
		DataFormat dateFormat = workbook.createDataFormat();
		CellStyle dateStyle = workbook.createCellStyle();
		dateStyle.setDataFormat(dateFormat.getFormat("d/m/yyyy"));
		
		DataFormat dateFormat2 = workbook.createDataFormat();
		CellStyle dateStyle2 = workbook.createCellStyle();
		dateStyle2.setDataFormat(dateFormat2.getFormat("d/m/yyyy hh:mm:ss"));

		int cellHeader = 0;
		int cellValue = 1;
		int rowIndex = 0;
		
		// create header row
		Row row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue("Job Details  :");
		row.getCell(cellHeader).setCellStyle(styleHeader);
		
		
		rowIndex++;
		row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue(messageSource.getMessage("certsearch.jobno", null, "Job No", userLocale));
		row.getCell(cellHeader).setCellStyle(style);
		
		row.createCell(cellValue).setCellValue(job.getJobno());
		
		row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue(messageSource.getMessage("Number of jobIds", null, "Number of items in Job", userLocale));
		row.getCell(cellHeader).setCellStyle(style);
		
		row.createCell(cellValue).setCellValue(job.getItems().size());

		row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue(messageSource.getMessage("Number of jobIds expense", null, "Number of expense items on Job", userLocale));
		row.getCell(cellHeader).setCellStyle(style);
		
		row.createCell(cellValue).setCellValue(job.getExpenseItems().size());

		row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue(messageSource.getMessage("jobstatus", null, "Job Status", userLocale));
		row.getCell(cellHeader).setCellStyle(style);
		
		for (JobItem jobItem : job.getItems()){
			row.createCell(cellValue).setCellValue(tranServ.getCorrectTranslation(jobItem.getInst().getModel().getDescription().getTranslations(),userLocale));
		}
		
		row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue(messageSource.getMessage("clientcompany", null, "Client Company", userLocale));
		row.getCell(cellHeader).setCellStyle(style);
		
		row.createCell(cellValue).setCellValue(job.getCon().getSub().getComp().getConame());

		row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue(messageSource.getMessage("jobitemsheet.subdiv", null, "Client Subdivision", userLocale));
		row.getCell(cellHeader).setCellStyle(style);
		
		row.createCell(cellValue).setCellValue(job.getCon().getSub().getSubname());

		row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue(messageSource.getMessage("jobitemsheet.contactname", null, "Client Contact", userLocale));
		row.getCell(cellHeader).setCellStyle(style);
		
		row.createCell(cellValue).setCellValue(job.getCon().getName());
		
		row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue(messageSource.getMessage("createpurchaseorder.returnaddress", null, "Return Address", userLocale));
		row.getCell(cellHeader).setCellStyle(style);
		
		row.createCell(cellValue).setCellValue(job.getReturnTo().getAddressLine());
		
		row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue(messageSource.getMessage("clientref", null, "Client Ref", userLocale));
		row.getCell(cellHeader).setCellStyle(style);
		
		row.createCell(cellValue).setCellValue(job.getClientRef());

		row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue(messageSource.getMessage("clientpo", null, "Client PO", userLocale));
		row.getCell(cellHeader).setCellStyle(style);
	
		if(job.getBpo() != null){
		row.createCell(cellValue).setCellValue(job.getBpo().getPoNumber());
			} else if (job.getDefaultPO() != null){
				row.createCell(cellValue).setCellValue(job.getDefaultPO().getPoNumber());
			} else if (job.getPOs() != null) {
			String jobPos = "";
			for (PO jobPo : job.getPOs()) {
				jobPos = jobPos.concat(jobPo.getPoNumber() + " ");
			}
			row.createCell(cellValue).setCellValue(jobPos);
		}
			
		row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue(messageSource.getMessage("Collection Date", null, "Collection Date", userLocale));
		row.getCell(cellHeader).setCellStyle(style);
		if(job.getPickupDate() != null){
		row.createCell(cellValue).setCellValue(DateTools.dateFromZonedDateTime(job.getPickupDate()));
		}	
		
		row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue(messageSource.getMessage("addjob.receiptdate", null, "Receipt Date", userLocale));
		row.getCell(cellHeader).setCellStyle(style);
		if(job.getReceiptDate() != null){
		row.createCell(cellValue).setCellValue(DateTools.dateFromZonedDateTime(job.getReceiptDate()));
		}
		
		row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue(messageSource.getMessage("jobresults.datein", null, "Date In", userLocale));
		row.getCell(cellHeader).setCellStyle(style);
		if(job.getRegDate() != null) {
			row.createCell(cellValue).setCellValue(job.getRegDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
			row.getCell(cellValue).setCellStyle(dateStyle);

		}

		row = sheet1.createRow(rowIndex++);
		row.createCell(cellHeader).setCellValue(messageSource.getMessage("viewjob.agrdeldate", null, "Agreed Delivery Date", userLocale));
		row.getCell(cellHeader).setCellStyle(style);
		if (job.getAgreedDelDate() != null) {
			row.createCell(cellValue).setCellValue(dateFromLocalDate(job.getAgreedDelDate()));
			row.getCell(cellValue).setCellStyle(dateStyle);
		}

		row = sheet1.createRow(rowIndex);
		row.createCell(cellHeader).setCellValue(messageSource.getMessage("Completion Date", null, "Completion Date", userLocale));
		row.getCell(cellHeader).setCellStyle(style);
		if (job.getDateComplete() != null) {
			row.createCell(cellValue).setCellValue(dateFromLocalDate(job.getDateComplete()));
			row.getCell(cellValue).setCellStyle(dateStyle);
		}

		Sheet sheet2 = workbook.createSheet(messageSource.getMessage("web.report.fae.table.jobitem", null, "Job Items", userLocale));
		sheet2.setDefaultColumnWidth(30);
		
		Row header = sheet2.createRow(0);
		
		header.createCell(0).setCellValue(messageSource.getMessage("Job No", null, "Job No", userLocale));
		header.getCell(0).setCellStyle(style);
		
		header.createCell(1).setCellValue(messageSource.getMessage("Item No", null, "Item No", userLocale));
		header.getCell(1).setCellStyle(style);
		
		header.createCell(2).setCellValue(messageSource.getMessage("Barcode", null, "Barcode", userLocale));
		header.getCell(2).setCellStyle(style);
		
		header.createCell(3).setCellValue(messageSource.getMessage("Plant No", null, "Plant No", userLocale));
		header.getCell(3).setCellStyle(style);
		
		header.createCell(4).setCellValue(messageSource.getMessage("Serial No", null, "Serial No", userLocale));
		header.getCell(4).setCellStyle(style);
		
		header.createCell(5).setCellValue(messageSource.getMessage("Instrument Model Name", null, "Instrument Model Name", userLocale));
		header.getCell(5).setCellStyle(style);

		header.createCell(6).setCellValue(messageSource.getMessage("Service Type", null, "Service Type", userLocale));
		header.getCell(6).setCellStyle(style);

		header.createCell(7).setCellValue(messageSource.getMessage("Status - Job Item", null, "Status - Job Item", userLocale));
		header.getCell(7).setCellStyle(style);
		
		header.createCell(8).setCellValue(messageSource.getMessage("Status - Date", null, "Status - Date", userLocale));
		header.getCell(8).setCellStyle(style);
		
		header.createCell(9).setCellValue(messageSource.getMessage("Status - Remark", null, "Status - Remark", userLocale));
		header.getCell(9).setCellStyle(style);

		header.createCell(10).setCellValue(messageSource.getMessage("Capability Ref", null, "Capability Ref", userLocale));
		header.getCell(10).setCellStyle(style);

		header.createCell(11).setCellValue(messageSource.getMessage("Contract review by", null, "Contract review by", userLocale));
		header.getCell(11).setCellStyle(style);

		header.createCell(12).setCellValue(messageSource.getMessage("Collection Date", null, "Collection Date", userLocale));
		header.getCell(12).setCellStyle(style);
		

		header.createCell(13).setCellValue(messageSource.getMessage("Receipt Date", null, "Receipt Date", userLocale));
		header.getCell(13).setCellStyle(style);
		
		header.createCell(14).setCellValue(messageSource.getMessage("Date In", null, "Date In", userLocale));
		header.getCell(14).setCellStyle(style);
		
		header.createCell(15).setCellValue(messageSource.getMessage("Due Date", null, "Due Date", userLocale));
		header.getCell(15).setCellStyle(style);
		
		header.createCell(16).setCellValue(messageSource.getMessage("Target TAT", null, "Target TAT", userLocale));
		header.getCell(16).setCellStyle(style);
		
		header.createCell(17).setCellValue(messageSource.getMessage("Completion Date", null, "Completion Date", userLocale));
		header.getCell(17).setCellStyle(style);
		
		header.createCell(18).setCellValue(messageSource.getMessage("Client Delivery No", null, "Client Delivery No", userLocale));
		header.getCell(18).setCellStyle(style);
		
		header.createCell(19).setCellValue(messageSource.getMessage("Client Ref - Job Item", null, "Client Ref - Job Item", userLocale));
		header.getCell(19).setCellStyle(style);
		
		header.createCell(20).setCellValue(messageSource.getMessage("On behalf of", null, "On behalf of", userLocale));
		header.getCell(20).setCellStyle(style);
		
		header.createCell(21).setCellValue(messageSource.getMessage("Client PO - Job Item", null, "Client PO - Job Item", userLocale));
		header.getCell(21).setCellStyle(style);
		
		header.createCell(22).setCellValue(messageSource.getMessage("Calibration Price", null, "Calibration Price", userLocale));
		header.getCell(22).setCellStyle(style);
		
		header.createCell(23).setCellValue(messageSource.getMessage("Repair Price", null, "Repair Price", userLocale));
		header.getCell(23).setCellStyle(style);
		
		header.createCell(24).setCellValue(messageSource.getMessage("Adjustment Price", null, "Adjustment Price", userLocale));
		header.getCell(24).setCellStyle(style);
		
		header.createCell(25).setCellValue(messageSource.getMessage("Purchase Price", null, "Purchase Price", userLocale));
		header.getCell(25).setCellStyle(style);
		
		header.createCell(26).setCellValue(messageSource.getMessage("Service Price", null, "Service Price", userLocale));
		header.getCell(26).setCellStyle(style);
		
		header.createCell(27).setCellValue(messageSource.getMessage("Price Source", null, "Price Source", userLocale));
		header.getCell(27).setCellStyle(style);
		
		header.createCell(28).setCellValue(messageSource.getMessage("Internal Subcontactor", null, "Internal Subcontactor", userLocale));
		header.getCell(28).setCellStyle(style);
		
		header.createCell(29).setCellValue(messageSource.getMessage("Supplier PO Items", null, "Supplier PO Items", userLocale));
		header.getCell(29).setCellStyle(style);
		
		header.createCell(30).setCellValue(messageSource.getMessage("Supplier Company", null, "Supplier Company", userLocale));
		header.getCell(30).setCellStyle(style);
		
		header.createCell(31).setCellValue(messageSource.getMessage("Supplier PO", null, "Supplier PO", userLocale));
		header.getCell(31).setCellStyle(style);
		
		int rowCount = 1;
		{
			// show row with just job specific details even if the job has no job items
			if(job.getItems().size() == 0){
				Row row1 = sheet2.createRow(rowCount++);
				row1.createCell(0).setCellValue(job.getJobno());
				
				if(job.getPickupDate() != null){
					row1.createCell(12).setCellValue(DateTools.dateFromZonedDateTime(job.getPickupDate()));
					row1.getCell(12).setCellStyle(dateStyle);
				}
				
				if(job.getReceiptDate() != null){
					row1.createCell(13).setCellValue(DateTools.dateFromZonedDateTime(job.getReceiptDate()));
					row1.getCell(13).setCellStyle(dateStyle);
					}
				
				if(job.getRegDate() != null) {
					row1.createCell(14).setCellValue(job.getRegDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
					row1.getCell(14).setCellStyle(dateStyle);
				}
			}
				// show row with just job specific details even if the job has no job items
				if(job.getItems().size() == 0){
					Row row1 = sheet2.createRow(rowCount++);
					row1.createCell(0).setCellValue(job.getJobno());
					if(job.getItems().size() == 0){
						Row row2 = sheet2.createRow(rowCount++);
						row2.createCell(0).setCellValue(job.getJobno());
						
						if(job.getPickupDate() != null){
							row2.createCell(12).setCellValue(DateTools.dateFromZonedDateTime(job.getPickupDate()));
							row2.getCell(12).setCellStyle(dateStyle);
						    }
						
						if(job.getReceiptDate() != null){
							row2.createCell(13).setCellValue(DateTools.dateFromZonedDateTime(job.getReceiptDate()));
							row2.getCell(13).setCellStyle(dateStyle);
							}
						
						if(job.getRegDate() != null) {
							row2.createCell(14).setCellValue(job.getRegDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
							row2.getCell(14).setCellStyle(dateStyle);
						}
					}
				}
			for (JobItem jobItem : job.getItems()) {
				Row row1 = sheet2.createRow(rowCount++);
				row1.createCell(0).setCellValue(job.getJobno());
				row1.createCell(1).setCellValue(jobItem.getItemNo());
				row1.createCell(2).setCellValue(jobItem.getInst().getPlantid());
				row1.createCell(3).setCellValue(jobItem.getInst().getPlantno());
				row1.createCell(4).setCellValue(jobItem.getInst().getSerialno());
				row1.createCell(5).setCellValue(tranServ.getCorrectTranslation(jobItem.getInst().getModel().getDescription().getTranslations(), userLocale));
				row1.createCell(6).setCellValue(tranServ.getCorrectTranslation(jobItem.getServiceType().getLongnameTranslation(), userLocale));
				row1.createCell(7).setCellValue(jobItem.getState().getDescription());

                if (jobItem.getActions() != null && !jobItem.getActions().isEmpty()) {
                    val jiaOpt = jobItem.getActions().stream().reduce((ignored, second) -> second);

                    jiaOpt.ifPresent(jia -> {
                        row1.createCell(8).setCellValue(jia.getStartStamp());
                        row1.getCell(8).setCellStyle(dateStyle2);
                        row1.createCell(9).setCellValue(jia.getRemark());

                    });

                }

                if (jobItem.getCapability() != null) {
                    row1.createCell(10).setCellValue(jobItem.getCapability().getReference());
                }

                if (jobItem.getLastContractReviewBy() != null) {
                    row1.createCell(11).setCellValue(jobItem.getLastContractReviewBy().getName());
                }

                if (jobItem.getDueDate() != null) {
                    row1.createCell(15).setCellValue(dateFromLocalDate(jobItem.getDueDate()));
                    row1.getCell(15).setCellStyle(dateStyle);
                }
                if (jobItem.getTurn() != null) {
                    row1.createCell(16).setCellValue(jobItem.getTurn());
                }
                if (jobItem.getDateComplete() != null) {
                    row1.createCell(17).setCellValue(jobItem.getDateComplete());
                    row1.getCell(17).setCellStyle(dateStyle);
                }


                if (jobItem.getClientRef() != null) {
					row1.createCell(19).setCellValue(jobItem.getClientRef());
				}
					row1.createCell(22).setCellValue(jobItem.getCalibrationCost().getFinalCost().doubleValue());

					row1.createCell(23).setCellValue(jobItem.getRepairCost().getFinalCost().doubleValue());

					row1.createCell(24).setCellValue(jobItem.getAdjustmentCost().getFinalCost().doubleValue());
				
				if(jobItem.getPurchaseCost().getPurchaseFinalCostInConvertedCurrency() != null){
					row1.createCell(25).setCellValue(jobItem.getPurchaseCost().getPurchaseFinalCostInConvertedCurrency().doubleValue());
				}


				if(jobItem.getItemPOs() != null) {
					String jobItemPos = "";
					for (JobItemPO po : jobItem.getItemPOs()) {
						jobItemPos = ((po.getPo() != null ? jobItemPos.concat(po.getPo().getPoNumber() + " ") : jobItemPos.concat(po.getBpo().getPoNumber() + " ")));
					}
					row1.createCell(21).setCellValue(jobItemPos);
				}
				if (jobItem.getOnBehalf() != null) {
					row1.createCell(20).setCellValue(jobItem.getOnBehalf().getCompany().getConame());
				}
			}

			for (JobExpenseItem ignored : job.getExpenseItems()) {
				Row row1 = sheet2.createRow(rowCount++);
				row1.createCell(0).setCellValue(job.getJobno());

				{

					if (job.getPickupDate() != null) {
						row1.createCell(12).setCellValue(DateTools.dateFromZonedDateTime(job.getPickupDate()));
						row1.getCell(12).setCellStyle(dateStyle);
					}
				
				if(job.getReceiptDate() != null){
					row1.createCell(13).setCellValue(DateTools.dateFromZonedDateTime(job.getReceiptDate()));
					row1.getCell(13).setCellStyle(dateStyle);
					}
				
				if(job.getRegDate() != null) {
					row1.createCell(14).setCellValue(job.getRegDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
					row1.getCell(14).setCellStyle(dateStyle);
				}
				
				if(job.getExpenseItems() != null && !job.getExpenseItems().isEmpty()) {
					JobExpenseItem jei = job.getExpenseItems().last();
					row1.createCell(26).setCellValue(jei.getSinglePrice().doubleValue());
				}
			}
		}
		long finishTime = System.currentTimeMillis();
		logger.debug("JobItemViewXlsView processed "+ rowCount +" jobs/items in "+(finishTime-startTime)+"ms ");
		}
		
	}
}
