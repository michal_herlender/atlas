package org.trescal.cwms.core.jobs.jobitem.entity.tprequirement;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TPRequirementDTO {
	private int id;
	private boolean adjustment;
	private boolean calibration;
	private boolean investigation;
	private boolean repair;
	
	public TPRequirementDTO(int id, boolean adjustment, boolean calibration, boolean investigation, boolean repair) {
		// Explicit constructor, used in query projection
		this.id = id;
		this.adjustment = adjustment;
		this.calibration = calibration;
		this.investigation = investigation;
		this.repair = repair;
	}
}
