package org.trescal.cwms.core.jobs.jobitem.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;


@Component
public class CollectInstrumentsValidator extends AbstractBeanValidator
{
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(CollectInstrumentsForm.class);
	}
	
	public void validate(Object obj, Errors errors)
	{
		CollectInstrumentsForm form = (CollectInstrumentsForm) obj;

		super.validate(form, errors);

		if (form.getPlantIds() == null)
		{
			errors.rejectValue("plantIds", "error.form.plantIds", null, "At least one instrument must be collected.");
		}
	}
}