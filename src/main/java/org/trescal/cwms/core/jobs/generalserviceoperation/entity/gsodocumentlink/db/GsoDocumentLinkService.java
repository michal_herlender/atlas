package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.GsoDocumentLink;

public interface GsoDocumentLinkService extends BaseService<GsoDocumentLink, Integer> {
  
}
