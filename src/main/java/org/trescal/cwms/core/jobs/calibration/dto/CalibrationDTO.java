package org.trescal.cwms.core.jobs.calibration.dto;

import java.time.LocalDate;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CalibrationDTO {

	private Integer calId;
	private Date startTime;
	private LocalDate calDate;
	private String process;
	private String serviceType;
	private String capabilityReference;
	private Integer certid;
	private String certifNo;
	private Integer jobId; 
	private String jobNo;
	private Integer jobItemId;
	private Integer jobItemNo;
	private Integer coid;
	private String coname;
	private Boolean companyActive;
	private Boolean onStop;
	private Integer subdivid;
	private String subname;
	private Boolean subActive;
	private String contactName;
	private Integer plantId;
	private String instModel;
	private String brand;
	private String modelNumber;
	private String plantNo;
	private String serialNo;
	private String status;
	private Long standardsUsageSize;
	
	
	public CalibrationDTO(LocalDate calDate, String process, String serviceType, String capabilityReference, Integer certid,
			String certifNo, Integer jobId, String jobNo, Integer jobItemNo, Integer coid, String coname,
			Boolean companyActive, Boolean onStop, Integer subdivid, String subname, Boolean subActive,
			String firstName, String lastName, Integer plantId, String instModel, String brand, String modelNumber,
			String plantNo, String serialNo) {
		super();
		this.calDate = calDate;
		this.process = process;
		this.serviceType = serviceType;
		this.capabilityReference = capabilityReference;
		this.certid = certid;
		this.certifNo = certifNo;
		this.jobId = jobId;
		this.jobNo = jobNo;
		this.jobItemNo = jobItemNo;
		this.coid = coid;
		this.coname = coname;
		this.companyActive = companyActive;
		this.onStop = onStop;
		this.subdivid = subdivid;
		this.subname = subname;
		this.subActive = subActive;
		this.contactName = firstName + " " + lastName;
		this.plantId = plantId;
		this.instModel = instModel;
		this.brand = brand;
		this.modelNumber = modelNumber;
		this.plantNo = plantNo;
		this.serialNo = serialNo;
	}

	public CalibrationDTO(LocalDate calDate, String process, String serviceType, String capabilityReference, String certifNo,
			String jobNo, Integer jobItemNo, String coname, String subname, String firstName, String lastName, Integer plantId,
			String instModel, String brand, String modelNumber, String plantNo, String serialNo) {
		super();
		this.calDate = calDate;
		this.process = process;
		this.serviceType = serviceType;
		this.capabilityReference = capabilityReference;
		this.certifNo = certifNo;
		this.jobNo = jobNo;
		this.jobItemNo = jobItemNo;
		this.coname = coname;
		this.subname = subname;
		this.contactName = firstName + " " + lastName;
		this.plantId = plantId;
		this.instModel = instModel;
		this.brand = brand;
		this.modelNumber = modelNumber;
		this.plantNo = plantNo;
		this.serialNo = serialNo;
	}

	public CalibrationDTO(Integer calId, Date startTime, 
			String process, String serviceType, String capabilityReference, Integer jobId, String jobNo, 
			Integer jobItemId, Integer jobItemNo, 
			String contactName,
			String instModel, String status, Long standardsUsageSize) {
		super();
		this.calId = calId;
		this.startTime = startTime;
		this.process = process;
		this.serviceType = serviceType;
		this.capabilityReference = capabilityReference;
		this.jobId = jobId;
		this.jobNo = jobNo;
		this.jobItemId = jobItemId;
		this.jobItemNo = jobItemNo;
		this.contactName = contactName;
		this.instModel = instModel;
		this.status = status;
		this.standardsUsageSize = standardsUsageSize;
	}
}
