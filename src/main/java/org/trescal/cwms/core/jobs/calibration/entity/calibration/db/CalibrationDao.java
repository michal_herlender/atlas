package org.trescal.cwms.core.jobs.calibration.entity.calibration.db;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationDTO;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.form.CalibrationSearchForm;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface CalibrationDao extends AllocatedDao<Subdiv, Calibration, Integer> {
	// This method does nothing in the DAOImpl except call the updateCalibration
	// method. The reason it exists is so that a hook can be placed at the DAO
	// level instead of the Service level, but only firing off when the
	// completeCalibration method persists its object
	void completeCalibration(Calibration calibration, String remark);

	Calibration findBatchCalibrationByPosition(int batchCalId, int position);

	Calibration findCalibrationByXindiceKey(String xindiceKey);

	Calibration findEagerCalibration(int id);

	Calibration findOnHoldCalibrationForItem(int jobItemId);

	void flushSession();

	List<Calibration> getAllCalibrationsForJobAtStatus(int jobId, String statusName);

	List<Calibration> getAllCalibrationsForJobAtStatusUsingProc(int jobId, String statusName, int procid);

	List<Calibration> getAllCalibrationsPerformedWithInstrumentStandard(int plantid, Integer page, Integer resPerPage);

	List<Calibration> getAllOngoingCalibrationsByContact(int personid);

	PagedResultSet<CalibrationDTO> searchCalibrations(CalibrationSearchForm form, PagedResultSet<CalibrationDTO> prs,
			Subdiv allocatedSubdiv, Locale locale);

	// This method does nothing in the DAOImpl except call the updateCalibration
	// method. The reason it exists is so that a hook can be placed at the DAO
	// level instead of the Service level, but only firing off when the
	// startCalibration method persists its object
	void startCalibration(Calibration calibration, boolean resuming);

	public Calibration findLastCalibrationForJobitemByCalStatus(int jobitemid, CalibrationStatus calStatus);
	
	PagedResultSet<CalibrationDTO> getCalibrationsPerformed(Integer plantId, Date startCalDate, Date endCalDate, 
			Company allocatedCompany, Subdiv allocatedSubdiv, Locale locale, PagedResultSet<CalibrationDTO> prs);

	List<CalibrationDTO> getCalibrationsPerformed_(Integer plantId, Date startCalDate, Date endCalDate);
}