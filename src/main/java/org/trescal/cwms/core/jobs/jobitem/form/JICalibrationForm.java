package org.trescal.cwms.core.jobs.jobitem.form;

import java.util.List;

public class JICalibrationForm extends JobItemForm
{
	private int calId;
	private List<Integer> calLinkIdsToInclude;
	private int duration;
	private String unitCode;

	public int getCalId()
	{
		return this.calId;
	}

	public List<Integer> getCalLinkIdsToInclude()
	{
		return this.calLinkIdsToInclude;
	}

	public int getDuration()
	{
		return this.duration;
	}

	public void setCalId(int calId)
	{
		this.calId = calId;
	}

	public void setCalLinkIdsToInclude(List<Integer> calLinkIdsToInclude)
	{
		this.calLinkIdsToInclude = calLinkIdsToInclude;
	}

	public void setDuration(int duration)
	{
		this.duration = duration;
	}

    public String getUnitCode() {
        return unitCode;
    }

    public void setUnitCode(String unitCode) {
        this.unitCode = unitCode;
    }
}