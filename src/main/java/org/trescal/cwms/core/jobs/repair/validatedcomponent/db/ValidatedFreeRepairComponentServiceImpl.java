package org.trescal.cwms.core.jobs.repair.validatedcomponent.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.repair.validatedcomponent.ValidatedFreeRepairComponent;

@Service("ValidatedFreeRepairComponentService")
public class ValidatedFreeRepairComponentServiceImpl extends BaseServiceImpl<ValidatedFreeRepairComponent, Integer> implements ValidatedFreeRepairComponentService
{
	@Autowired
	private ValidatedFreeRepairComponentDao validatedFreeRepairComponentDao;

	@Override
	protected BaseDao<ValidatedFreeRepairComponent, Integer> getBaseDao() {
		return validatedFreeRepairComponentDao;
	}
}