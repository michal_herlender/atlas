package org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.db;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;

/**
 * Interface for accessing and manipulating {@link JobItemActivity} entities.
 * 
 * @author jamiev
 */
public interface JobItemActivityService extends BaseService<JobItemActivity, Integer>
{
	/**
	 * Updates a {@link JobItemActivity} entity, setting it as complete and
	 * populating the completion fields with the given parameter values.
	 * 
	 * @param jobItemId the ID of the {@link JobItem} to complete an activity
	 *        for.
	 * @param endStatId the {@link ItemStatus} ID after the activity was carried
	 *        out.
	 * @param remark a {@link String} field for comments or remarks regarding
	 *        the activity.
	 * @param timeSpent an {@link int} representing the time taken in minutes
	 *        for the activity to be carried out.
	 * @return a {@link ResultWrapper} containing either any error messages, or
	 *         the successfully updated {@link JobItemActivity} entity
	 */
	ResultWrapper completePendingActivity(int jobItemId, Integer endStatId, String remark, String timeSpent, Integer actionOutcomeId, HttpSession session);

	ResultWrapper deleteJobItemActivityById(int jiaId, HttpServletRequest req);

	/**
	 * Returns a list of all incomplete {@link JobItemActivity} entities started
	 * by the {@link Contact} with the given id. Ordered by job, item number.
	 * 
	 * @param contact the {@link Contact} to start the
	 *        {@link JobItemActivity}.
	 * @return list of {@link JobItemActivity}.
	 */
	List<JobItemActivity> getAllIncompleteActivityForContact(Contact contact);

	/**
	 * Returns the incomplete {@link JobItemActivity} if there is one for the
	 * {@link JobItem}. Otherwise, return null.
	 * 
	 * @param jobItem the {@link JobItem}.
	 * @return the incomplete {@link JobItemActivity} or null.
	 */
	JobItemActivity getIncompleteActivityForItem(JobItem jobItem);

	/**
	 * 
	 * 
	 */
	List<JobItem> getJobItemsWithSameState(int jobItemId);

	/**
	 * Returns the amount of minutes (rounded to nearest upper 5) since the
	 * {@link JobItemActivity} with the given ID was started.
	 * 
	 * @param actId the {@link JobItemActivity} ID
	 * @return amount of minutes since it began
	 */
	Integer getMinutesFromActivityStart(int actId);

	/**
	 * Inserts a new {@link JobItemActivity} for the {@link JobItem} with the
	 * given ID, populating all required fields and setting the activity as
	 * complete.
	 * 
	 * @param actId the {@link ItemActivity} ID being recorded.
	 * @param endStatId the {@link ItemStatus} ID after the activity was carried
	 *        out.
	 * @param jobItemIds the {@link JobItem} IDs.
	 * @param remark a {@link String} field for comments or remarks regarding
	 *        the activity.
	 * @param timeSpent an {@link int} representing the time taken in minutes
	 *        for the activity to be carried out.
	 * @return a {@link ResultWrapper} containing either any error messages, or
	 *         the successfully created {@link JobItemActivity} entity
	 */
	ResultWrapper newCompletedActivityDWR(int actId, Integer endStatId, int jobItemId, Integer[] otherJobItemIds, String remark, 
			String timeSpent, Integer actionOutcomeId, Boolean addRemarkAsJobNote, HttpSession session);
	
	ResultWrapper newCompletedActivity(Integer jobItemId, Integer activityId, Integer actionOutcomeId, Integer outcomeStatusId,
			String remarks, Integer timeSpent, Date startDate, Contact contacts, Integer[] otherJobItemIds, Boolean addRemarkAsJobNote);
	
	JobItemActivity createIncompleteAction(Integer activityId, Integer jobItemId, Contact contact);
}