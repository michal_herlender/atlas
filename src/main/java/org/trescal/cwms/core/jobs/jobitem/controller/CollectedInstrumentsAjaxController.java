package org.trescal.cwms.core.jobs.jobitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.jobs.jobitem.dto.CollectedInstrumentDto;
import org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.db.CollectedInstrumentService;

import java.util.List;

@RestController
@RequestMapping("collectedInstruments")
public class CollectedInstrumentsAjaxController {
    @Autowired
    private CollectedInstrumentService collectedInstrumentService;

    @GetMapping("getForCompany.josn")
    List<CollectedInstrumentDto> getForCompany(@RequestParam Integer coid) {
        return collectedInstrumentService.getCollectedInstrumentsDtoForCompany(coid);
    }
}
