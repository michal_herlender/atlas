package org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost;

public interface ContractReviewAdjustmentCostDao extends BaseDao<ContractReviewAdjustmentCost, Integer> {}