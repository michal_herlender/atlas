package org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.job.dto.ClientPurchaseOrderExpenseItemDTO;
import org.trescal.cwms.core.jobs.job.projection.ClientPurchaseOrderProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO;

public interface JobExpenseItemPOService extends BaseService<JobExpenseItemPO, Integer> {

	List<ClientPurchaseOrderProjectionDTO> getProjectionDTOs(Collection<Integer> periodicInvoiceIds);

	List<ClientPurchaseOrderExpenseItemDTO> getOnPO(Integer poId);

	List<ClientPurchaseOrderExpenseItemDTO> getOnBPO(Integer poId, Integer jobId);
	
	List<Integer> getJobServicesIdsOnBPO(Integer bpoid, Integer jobId);
	
	JobExpenseItemPO getJobExpenseItemPO( Integer jobServicesId, int poId, Integer jobId);
}