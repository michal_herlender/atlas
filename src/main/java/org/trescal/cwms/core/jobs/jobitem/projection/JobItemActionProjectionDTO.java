package org.trescal.cwms.core.jobs.jobitem.projection;

import java.util.Date;

import org.trescal.cwms.spring.model.KeyValueIntegerString;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class JobItemActionProjectionDTO {
	private Integer id;
	private Integer jobItemId;
	private Integer activityId;
	private Integer startedById;
	private Integer endStatusId;
	
	private Boolean deleted;
	private Integer timeSpent;
	private Date startStamp; 
	private Date endStamp; 
	private String remark;

	// Translated fields - in future, full projection DTOs might be needed?
	private KeyValueIntegerString activity;
	private KeyValueIntegerString startedBy;
	private KeyValueIntegerString endStatus;
	
	/**
	 * Full constructor, populates KeyValue details automatically
	 * (note, it's expected this is mainly used for one job item; , 
	 *  see basic constructor below for use on multiple job items) 
	 * TODO Consider expanding to handle fields in JobItemActivity / JobItemTransit and/or splitting?
	 */
	public JobItemActionProjectionDTO(Integer id, Integer jobItemId, Integer activityId, 
			String activityName, Integer startedById, String startedByFirstName, 
			String startedByLastName, Integer endStatusId, String endStatusName, 
			Boolean deleted, Integer timeSpent, Date startStamp, Date endStamp, 
			String remark) {
		this.id = id;
		this.jobItemId = jobItemId;
		this.activityId = activityId;
		this.startedById = startedById;
		this.endStatusId = endStatusId;
		this.deleted = deleted;
		this.timeSpent = timeSpent;
		this.startStamp = startStamp;
		this.endStamp = endStamp;
		this.remark = remark;
		/*
		 * Extra fields (note all are optional in DB) 
		 * - accommodate potentially missing state translations (by indicating they're missing)
		 */
		if (activityId != null) {
			this.activity = new KeyValueIntegerString(activityId, 
					activityName != null ? activityName : "No translation for activity id "+activityId);
		}
		if (endStatusId != null) {
			this.endStatus = new KeyValueIntegerString(endStatusId, 
					endStatusName != null ? endStatusName : "No translation for status id "+endStatusId);
			
		}
		if (startedById != null) {
			this.startedBy = new KeyValueIntegerString(startedById, 
					assembleName(startedByFirstName, startedByLastName));
		}
	}
	
	/*
	 * Basic constructor for multiple job items (used on view selected job items, for example) 
	 */
	public JobItemActionProjectionDTO(Integer id, Integer jobItemId, Date startStamp, String remark) {
		this.id = id;
		this.jobItemId = jobItemId;
		this.startStamp = startStamp;
		this.remark = remark;
	}
	
	private String assembleName(String firstName, String lastName) {
		StringBuffer result = new StringBuffer();
		if (firstName != null) result.append(firstName);
		if (result.length() > 0) result.append(" ");
		if (lastName != null) result.append(lastName);
		return result.toString();
	}
}
