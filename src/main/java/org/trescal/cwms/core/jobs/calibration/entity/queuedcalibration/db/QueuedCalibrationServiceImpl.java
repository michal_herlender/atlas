package org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.db;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.QueuedCalibration;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;

@Service("QueuedCalibrationService")
public class QueuedCalibrationServiceImpl implements QueuedCalibrationService
{
	@Autowired
	private CertificateService certServ;
	@Autowired
	private QueuedCalibrationDao queuedCalibrationDao;
	@Autowired
	private ScheduledTaskService schTaskServ;
	@Autowired
	private MessageSource messages;
	@Autowired
	private UserService userService;

	@Override
	public void ajaxReprintCertificates(int[] certIds, HttpSession session)
	{
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		// No result wrapper so eager load not needed
		Contact printedBy = this.userService.get(username).getCon();
		List<Certificate> certs = new ArrayList<Certificate>();

		for (int certId : certIds)
		{
			Certificate cert = this.certServ.findCertificate(certId);
			certs.add(cert);
		}

		this.reprintCertificates(certs, printedBy);
	}

	@Override
	public ResultWrapper checkAccreditedForBatchCertificates(int batchId, int personId)
	{
		List<QueuedCalibration> qcs = this.getAccreditedActiveQueuedCalibrationsForBatches(Arrays.asList(batchId), personId);

		if (qcs.size() > 0)
		{
			return new ResultWrapper(true, "Certifying this batch now will generate "
					+ qcs.size() + " certificate(s)");
		}
		else
		{
			return new ResultWrapper(false, this.messages.getMessage("viewbatchcalibration.msg1", null, "There are no complete calibrations on this batch that you can certify", null));
		}
	}

	@Override
	public void deleteQueuedCalibration(QueuedCalibration queuedcalibration)
	{
		this.queuedCalibrationDao.remove(queuedcalibration);
	}

	@Override
	public QueuedCalibration findQueuedCalibration(int id)
	{
		return this.queuedCalibrationDao.find(id);
	}

	@Override
	public List<QueuedCalibration> getAccreditedActiveQueuedCalibrations(int personId)
	{
		return this.queuedCalibrationDao.getAccreditedActiveQueuedCalibrations(personId);
	}

	@Override
	public List<QueuedCalibration> getAccreditedActiveQueuedCalibrationsForBatches(List<Integer> batchIds, int personId)
	{
		return this.queuedCalibrationDao.getAccreditedActiveQueuedCalibrationsForBatches(batchIds, personId);
	}

	@Override
	public List<QueuedCalibration> getActiveQueuedCalibrations()
	{
		return this.queuedCalibrationDao.getActiveQueuedCalibrations();
	}

	@Override
	public List<QueuedCalibration> getActiveQueuedCalibrationsForBatches(List<Integer> batchIds)
	{
		return this.queuedCalibrationDao.getActiveQueuedCalibrationsForBatches(batchIds);
	}

	@Override
	public List<QueuedCalibration> getActiveQueuedCalibrationsOrderedByJob()
	{
		return this.queuedCalibrationDao.getActiveQueuedCalibrationsOrderedByJob();
	}

	@Override
	public List<QueuedCalibration> getAllQueuedCalibrations()
	{
		return this.queuedCalibrationDao.findAll();
	}

	@Override
	public List<QueuedCalibration> getQueuedCalibrationsForReprint()
	{
		return this.queuedCalibrationDao.getQueuedCalibrationsForReprint();
	}

	@Override
	public List<QueuedCalibration> getQueuedCalibrationsToPrintNow()
	{
		return this.queuedCalibrationDao.getQueuedCalibrationsToPrintNow();
	}

	@Override
	public List<QueuedCalibration> getQueuedCalibrationsWithIds(List<Integer> ids)
	{
		return this.queuedCalibrationDao.getQueuedCalibrationsWithIds(ids);
	}

	@Override
	public void insertQueuedCalibration(QueuedCalibration QueuedCalibration)
	{
		this.queuedCalibrationDao.persist(QueuedCalibration);
	}

	@Override
	public void printActiveQueuedCalibrationsForBatches(List<Integer> batchIds, Contact printedBy, HashMap<Integer, Integer> map)
	{
		// set the active queued cals for the batch as 'print now'
		// this.queuedCalibrationDao.printActiveQueuedCalibrationsForBatch(batchId
		// , printedBy, map);

		List<QueuedCalibration> queuedCals = this.getAccreditedActiveQueuedCalibrationsForBatches(batchIds, printedBy.getPersonid());

		// set each one to be printed now
		for (QueuedCalibration queuedCal : queuedCals)
		{
			queuedCal.setPrintNow(true);
			queuedCal.setPrintedBy(printedBy);
			queuedCal.setCertId(map.get(queuedCal.getId()));
			System.out.println("Set to print: " + queuedCal.getId());
			this.updateQueuedCalibration(queuedCal);
		}

		// trigger the print job
		this.schTaskServ.triggerScheduledTaskAfterDelay("jobDetail_CertGenerator_Manual", 20);
	}

	@Override
	public void printSelectedQueuedCalibrations(List<QueuedCalibration> queuedCals, HttpSession session)
	{
		// get the currently logged-in contact
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		// No result wrapper so eager load not needed
		Contact printedBy = this.userService.get(username).getCon();

		// set the queued calibrations to print now
		for (QueuedCalibration queuedCal : queuedCals)
		{
			queuedCal.setPrintNow(true);
			queuedCal.setPrintedBy(printedBy);
			this.updateQueuedCalibration(queuedCal);
		}

		// trigger the print job
		this.schTaskServ.triggerScheduledTaskAfterDelay("jobDetail_CertGenerator_Manual", 10);
	}

	@Override
	public void queueCalibration(Calibration cal)
	{
		QueuedCalibration qc = new QueuedCalibration();
		qc.setActive(true);
		qc.setQueuedCal(cal);
		qc.setTimeQueued(new Date());

		this.insertQueuedCalibration(qc);
	}

	@Override
	public void reprintCertificates(List<Certificate> certs, Contact printedBy)
	{
		// set each one to be re-printed
		for (Certificate cert : certs)
		{
			QueuedCalibration qc = new QueuedCalibration();
			qc.setActive(true);
			qc.setCertId(cert.getCertid());
			qc.setPrintedBy(printedBy);
			qc.setPrintNow(false);
			qc.setQueuedCal(cert.getCal());
			qc.setReprint(true);
			qc.setTimeQueued(new Date());
			this.insertQueuedCalibration(qc);

			System.out.println("Set to re-print cert no: " + cert.getCertno());
		}

		// trigger the print job
		this.schTaskServ.triggerScheduledTaskAfterDelay("jobDetail_CertGenerator_Reprint", 20);
	}

	@Override
	public void saveOrUpdateQueuedCalibration(QueuedCalibration queuedcalibration)
	{
		this.queuedCalibrationDao.saveOrUpdate(queuedcalibration);
	}

	@Override
	public void setQueuedCalibrationAsCompleted(QueuedCalibration qc)
	{
		// QueuedCalibration qc =
		// this.findQueuedCalibration(queuedCalibrationId);
		qc.setActive(false);
		qc.setPrintNow(false);
		qc.setReprint(false);
		qc.setTimeCompleted(new Date());

		this.updateQueuedCalibration(qc);
	}

	@Override
	public void updateQueuedCalibration(QueuedCalibration QueuedCalibration)
	{
		this.queuedCalibrationDao.update(QueuedCalibration);
	}
}