package org.trescal.cwms.core.jobs.certificate.form;

import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class MoveCertForm
{
	private CertLink certLink;
	private Integer jobItemId;

}