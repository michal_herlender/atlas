package org.trescal.cwms.core.jobs.calibration.form;

import lombok.val;
import org.jasypt.digest.StandardStringDigester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.procedure.entity.defaultstandard.db.DefaultStandardService;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.tools.DigestConfigCreator;
import org.trescal.cwms.core.tools.PasswordTools;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import java.util.ArrayList;
import java.util.Arrays;

@Component
public class NewCalibrationValidator extends AbstractBeanValidator {

    @Autowired
    public DigestConfigCreator configCreator;
    @Autowired
    private DefaultStandardService defStdServ;
    @Autowired
    private InstrumService instServ;
    @Autowired
    private JobItemService jiServ;
    @Autowired
    private CapabilityAuthorizationService procAccServ;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(NewCalibrationForm.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        // do cal hibernate validation
        NewCalibrationForm calForm = (NewCalibrationForm) target;
		super.validate(calForm, errors);
		Calibration cal = calForm.getCal();

		// DURATION CHECK TEMPORARILY TAKEN OUT
		// if (calForm.getDuration() != null)
		// {
		// if (!NumberTools.isAnInteger(calForm.getDuration()))
		// {
		// errors.rejectValue(this.getFullPropertyPath("duration"), null, "An
		// integer duration for the certificate must be specified.");
		// }
		// }

		// business validation
		if (!errors.hasErrors()) {
            val accStat = this.procAccServ.authorizedFor(cal.getCapability().getId(),
                cal.getCalType().getCalTypeId(), cal.getStartedBy().getPersonid());
            if (accStat.isRight()) {
                if (calForm.isPasswordReq()
                    && (accStat.map(l -> l.equals(AccreditationLevel.APPROVE)).getOrElse(false))
                    && (calForm.getSignNow())) {
                    // The user is trying to use an existing password
                    if (calForm.isPwExists()) {
                        Contact con = calForm.getCal().getStartedBy();

                        StandardStringDigester digester = new StandardStringDigester();
                        digester.setConfig(this.configCreator.createDigesterConfig());

                        if (!digester.matches(calForm.getEncPassword(), con.getEncryptedPassword())) {
                            errors.rejectValue("encPassword", "error.calibration.validator.wrongsignpassword");
                        }
					}

					// The user is trying to set a new password
					if (!calForm.isPwExists()) {
						if (!calForm.getEncPassword().equals(calForm.getConfirmPassword())) {
							errors.rejectValue("confirmPassword", "error.calibration.validator.confirmpassword");
						} else {
							if (PasswordTools.getPasswordStrength(calForm.getEncPassword()) < 3) {
								errors.rejectValue("encPassword", "error.calibration.validator.passwordstrength");
							}
						}
					}
				}

				//set the value to 0 to indicate no work instruction selection
				calForm.setWorkInstructionId(0);
				if ((cal.getCalProcess().getApplicationComponent()) && (calForm.getWorkInstructionId() == null)) {
					errors.rejectValue("workInstructionId", "error.calibration.validator.workinstructiondelected");
                }

                if ((cal.getCalProcess().getApplicationComponent()) && (calForm.getWorkInstructionId() == null)) {
                    errors.rejectValue("workInstructionId", "error.calibration.validator.workinstructiondelected");
                }

                if (!calForm.isInfoAcknowledged()) {
                    errors.rejectValue("infoAcknowledged", "error.calibration.validator.readinstructionconfirm");
                }

                for (CalLink calLink : cal.getLinks()) {
                    // reload inst to prevent LIE
                    Instrument inst = this.instServ.get(calLink.getJi().getInst().getPlantid());

                    boolean notReadyForCal = !this.jiServ.isReadyForCalibration(calLink.getJi());
                    boolean notBaseUnit = !inst.getModel().getModelType().getModules();
                    boolean isMidwayThroughActivity = !calLink.getJi().getState().isStatus();

                    // reject items not ready for calibration UNLESS they are
                    // base units, which (for flexibility) should always be
                    // allowed to be calibrated with their modules regardless of
                    // status (unless midway through another activity of course)
                    if ((notReadyForCal && notBaseUnit) || isMidwayThroughActivity) {
                        errors.rejectValue("itemsToCalibrate", "error.calibration.validator.notappropriatestatus");
                        break;
                    }
                }

				if (calForm.getFile() != null) {
                    MultipartFile file = calForm.getFile();

                    String name = file.getOriginalFilename();
                    assert name != null;
                    if (!name.equals("")) {
                        if (file.getSize() == 0) {
                            errors.rejectValue("file", "error.calibration.validator.certificatenotexists");
                        } else {
                            try {
                                name.substring(0, name.lastIndexOf("."));
                                name.substring(name.lastIndexOf("."));
                            } catch (Exception e) {
                                errors.rejectValue("file", "error.calibration.validator.invalidfilename");
                            }
                        }
					}
				}

				// check all enforced stds are selected
				Integer[] instIds = calForm.getInstrums();
				if (instIds == null) {
					instIds = new Integer[0];
				}
				if (cal.getCalProcess().getApplicationComponent()) {
					if (cal.getWorkInstruction() != null) {
                        if (!this.defStdServ.enforcedAreCheckedForProcAndWI(cal.getCapability().getId(),
                            cal.getWorkInstruction().getId(), Arrays.asList(instIds))) {
                            errors.rejectValue("instrums", "error.calibration.validator.enforcedstandardsnotselected");
                        }
                    }
                } else {
                    if (!this.defStdServ.enforcedAreCheckedForProc(cal.getCapability().getId(), instIds)) {
                        errors.rejectValue("instrums", "error.calibration.validator.enforcedstandardsnotselected2");
                    }
                }

                ArrayList<Integer> selectedStds = new ArrayList<Integer>(Arrays.asList(instIds));
                for (Integer plantid : selectedStds) {
                    Instrument inst = this.instServ.get(plantid);
                    if (inst.getCalExpiresAfterNumberOfUses() && (inst.getUsesSinceLastCalibration() >= inst.getPermittedNumberOfUses())) {
                        errors.rejectValue("instrums", null,
                            "The instrument with barcode " + inst.getPlantid()
                                + " cannot be used because it has been used for "
                                + inst.getUsesSinceLastCalibration()
                                + " calibrations since it was last calibrated.");
                    }

                    if (!calForm.isAllowUncalibratedStds() && inst.isOutOfCalibration()) {
                        errors.rejectValue("instrums", null, "The instrument with barcode " + inst.getPlantid()
                            + " cannot be used because it is out of calibration.");
                    }
                }
            } else {
                errors.rejectValue("cal.startedBy", "error.calibration.validator.usernotaccreditedforcalibration");
            }
		}
	}
}