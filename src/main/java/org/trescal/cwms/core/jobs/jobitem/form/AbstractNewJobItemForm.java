package org.trescal.cwms.core.jobs.jobitem.form;

import lombok.Data;
import org.springframework.util.AutoPopulatingList;

import java.util.Date;

@Data
public abstract class AbstractNewJobItemForm {

	protected AutoPopulatingList<Integer> basketBaseItemIds;
	protected AutoPopulatingList<Integer> basketExistingGroupIds;
	protected AutoPopulatingList<Integer> basketIds;
	protected AutoPopulatingList<String> basketParts;
	protected AutoPopulatingList<Integer> basketTempGroupIds;
	protected AutoPopulatingList<String> basketTypes;
	protected AutoPopulatingList<Integer> basketQuotationItemIds;
	protected AutoPopulatingList<Integer> serviceTypeIds;
	protected AutoPopulatingList<Integer> serviceTypeSources;
	protected AutoPopulatingList<Date> datesIn;
	protected AutoPopulatingList<Integer> bookInByContactIds;

}