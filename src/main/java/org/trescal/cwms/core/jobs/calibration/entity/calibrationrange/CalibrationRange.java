package org.trescal.cwms.core.jobs.calibration.entity.calibrationrange;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.instrumentmodel.entity.range.Range;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.tools.StringTools;

import lombok.Setter;

@Entity
@Table(name = "calibrationrange")
@Setter
public class CalibrationRange extends Range
{
	private BigDecimal relatedPoint;
	private UoM relatedUom;

	@Transient
	/**
	 * Convenience property that returns the point with any trailing zeros
	 * removed.
	 */
	public String getFormattedRelatedPoint()
	{
		return StringTools.formatBigDecimalTrailingZeros(this.relatedPoint);
	}

	@Column(name = "relatedpoint", nullable = true)
	public BigDecimal getRelatedPoint()
	{
		return this.relatedPoint;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "relateduomid", nullable = true)
	public UoM getRelatedUom()
	{
		return this.relatedUom;
	}

	@Transient
	public boolean isRelational()
	{
		if (this.relatedPoint != null)
		{
			return true;
		}

		return false;
	}
}