package org.trescal.cwms.core.jobs.jobitem.entity.calloffitem;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Entity
@Table(name = "calloffitem")
public class CallOffItem extends Allocated<Subdiv>
{
	private boolean active;
	private Contact calledOffBy;
	private int id;
	private Date offDate;
	private JobItem offItem;
	private Date onDate;
	private JobItem onItem;
	private String reason;

	/**
	 * @return the calledOffBy
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "calledoffby")
	public Contact getCalledOffBy()
	{
		return this.calledOffBy;
	}

	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	/**
	 * @return the offDate
	 */
	@Column(name = "offdate", columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOffDate()
	{
		return this.offDate;
	}

	/**
	 * @return the offItem
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "offitem")
	public JobItem getOffItem()
	{
		return this.offItem;
	}

	/**
	 * @return the onDate
	 */
	@Column(name = "ondate", columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getOnDate()
	{
		return this.onDate;
	}

	/**
	 * @return the onItem
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "onitem")
	public JobItem getOnItem()
	{
		return this.onItem;
	}

	/**
	 * @return the reason
	 */
	@Length(max = 1000)
	@Column(name = "reason", length = 1000)
	public String getReason()
	{
		return this.reason;
	}

	/**
	 * @return the active
	 */
	@Column(name = "active", nullable=false, columnDefinition="bit")
	public boolean isActive()
	{
		return this.active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active)
	{
		this.active = active;
	}

	/**
	 * @param calledOffBy the calledOffBy to set
	 */
	public void setCalledOffBy(Contact calledOffBy)
	{
		this.calledOffBy = calledOffBy;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @param offDate the offDate to set
	 */
	public void setOffDate(Date offDate)
	{
		this.offDate = offDate;
	}

	/**
	 * @param offItem the offItem to set
	 */
	public void setOffItem(JobItem offItem)
	{
		this.offItem = offItem;
	}

	/**
	 * @param onDate the onDate to set
	 */
	public void setOnDate(Date onDate)
	{
		this.onDate = onDate;
	}

	/**
	 * @param onItem the onItem to set
	 */
	public void setOnItem(JobItem onItem)
	{
		this.onItem = onItem;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason)
	{
		this.reason = reason;
	}
}