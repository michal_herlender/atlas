package org.trescal.cwms.core.jobs.repair.dto.form.transformer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.instrumentmodel.entity.component.db.ComponentService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.repair.component.FreeRepairComponent;
import org.trescal.cwms.core.jobs.repair.dto.FreeRepairComponentDTO;
import org.trescal.cwms.core.jobs.repair.dto.FreeRepairOperationDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairInspectionReportFormDTO;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperationComparator;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;
import org.trescal.cwms.core.tools.EntityDtoTransformer;

@Component
public class RepairInspectionReportFormTransformer
		implements EntityDtoTransformer<RepairInspectionReport, RepairInspectionReportFormDTO> {

	@Autowired
	private RepairInspectionReportService rirService;
	@Autowired
	private JobItemService jiService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private ComponentService componentService;

	@Override
	public RepairInspectionReport transform(RepairInspectionReportFormDTO dto) {
		RepairInspectionReport entity;
		Set<FreeRepairOperation> opList = null;
		Set<FreeRepairComponent> compList = new HashSet<FreeRepairComponent>();
		if (dto.getRirId() != null) {
			entity = rirService.get(dto.getRirId());
			opList = entity.getFreeRepairOperations();
			entity.getFreeRepairOperations().forEach(o -> {
				compList.addAll(o.getFreeRepairComponents());
			});
		} else {
			entity = new RepairInspectionReport();
			entity.setCompletedByTechnician(false);
			entity.setFreeRepairOperations(new TreeSet<FreeRepairOperation>(new FreeRepairOperationComparator()));
		}

		entity.setClientDescription(dto.getClientDescription());
		entity.setClientInstructions(dto.getClientInstructions());
		entity.setInternalDescription(dto.getInternalDescription());
		entity.setInternalInspection(dto.getInternalInspection());
		entity.setInternalOperations(dto.getInternalOperations());
		entity.setManufacturerWarranty(dto.getManufacturerWarranty());
		entity.setTrescalWarranty(dto.getTrescalWarranty());
		entity.setInternalInstructions(dto.getInternalInstructions());
		entity.setExternalInstructions(dto.getExternalInstructions());
		entity.setLeadTime(dto.getLeadTime());
		entity.setEstimatedReturnDate(dto.getEstimatedReturnDate());
		entity.setJi(jiService.findJobItem(dto.getJobItemId()));
		entity.setMisuse(dto.getMisuse());
		entity.setMisuseComment(dto.getMisuseComment());
		
		Set<FreeRepairOperation> freeOps = new TreeSet<FreeRepairOperation>(new FreeRepairOperationComparator());
		for (FreeRepairOperationDTO o : dto.getOperationsForm()) {
			FreeRepairOperation fro;
			if (o.getFrOperationId() != null)
				fro = getExistingOperation(o.getFrOperationId(), opList);
			else {
				fro = new FreeRepairOperation();
				fro.setFreeRepairComponents(new HashSet<FreeRepairComponent>());
			}
			fro.setName(o.getName());
			fro.setLabourTime(o.getLabourTimeMinutes() + (o.getLabourTimeHours() * 60));
			fro.setOperationStatus(o.getStatus());
			fro.setOperationType(o.getType());
			fro.setCost(o.getCost());
			fro.setAddedAfterRiRValidation(o.getAddedAfterRiRValidation());
			fro.setPosition(o.getPosition());
			fro.setRepairInspectionReport(entity);
			if (o.getCoid() != null && o.getCoid() != 0) 
				fro.setTpCompany(companyService.get(o.getCoid()));
			
			if (dto.getComponentsForm() != null) {
				Set<FreeRepairComponent> freeComps = new HashSet<FreeRepairComponent>();
				for (FreeRepairComponentDTO c : dto.getComponentsForm()) {
					if ((c.getFreeRepairOperationId() != null
							&& c.getFreeRepairOperationId().equals(o.getFrOperationId()))
							|| o.getName().equals(c.getFreeRepairOperationName())) {
						FreeRepairComponent frc;

						if (c.getFrComponentId() != null)
							frc = getExistingComponent(c.getFrComponentId(), compList);
						else
							frc = new FreeRepairComponent();
						frc.setAddedAfterRiRValidation(c.getAddedAfterRiRValidation());
						frc.setCost(c.getCost());
						frc.setFreeRepairOperation(fro);
						fro.getFreeRepairComponents().add(frc);
						frc.setName(c.getName());
						frc.setQuantity(c.getQuantity());
						frc.setStatus(c.getStatus());
						frc.setSource(c.getSource());
						if (c.getComponentId() != null)
							frc.setComponent(componentService.get(c.getComponentId()));
						
						freeComps.add(frc);
					}
				}
				if (fro.getFreeRepairComponents() != null)
					fro.getFreeRepairComponents().clear();
				fro.getFreeRepairComponents().addAll(freeComps);
			} else if (fro.getFreeRepairComponents() != null && !fro.getFreeRepairComponents().isEmpty())
				fro.getFreeRepairComponents().clear();
			freeOps.add(fro);
		}
		if (entity.getFreeRepairOperations() != null)
			entity.getFreeRepairOperations().clear();
		entity.getFreeRepairOperations().addAll(freeOps);
		return entity;
	}

	@Override
	public RepairInspectionReportFormDTO transform(RepairInspectionReport entity) {

		RepairInspectionReportFormDTO dto = new RepairInspectionReportFormDTO();
		dto.setRirId(entity.getRirId());
		dto.setRirNumber(entity.getRirNumber());
		dto.setClientDescription(entity.getClientDescription());
		dto.setClientInstructions(entity.getClientInstructions());
		dto.setInternalDescription(entity.getInternalDescription());
		dto.setInternalInspection(entity.getInternalInspection());
		dto.setInternalOperations(entity.getInternalOperations());
		dto.setManufacturerWarranty(entity.getManufacturerWarranty());
		dto.setTrescalWarranty(entity.getTrescalWarranty());
		dto.setInternalInstructions(entity.getInternalInstructions());
		dto.setExternalInstructions(entity.getExternalInstructions());
		dto.setLeadTime(entity.getLeadTime());
		dto.setEstimatedReturnDate(entity.getEstimatedReturnDate());
		dto.setJi(entity.getJi());
		dto.setMisuse(entity.getMisuse());
		dto.setMisuseComment(entity.getMisuseComment());
		if (entity.getCompletedByTechnician()) {
			dto.setCompletedByTechnician(entity.getCompletedByTechnician());
			dto.setTechnician(entity.getTechnician().getName());
			dto.setCompletedByTechnicianOn(entity.getCompletedByTechnicianOn());
		}
		if (entity.getValidatedByManager() != null && entity.getValidatedByManager()) {
			dto.setValidatedByManager(entity.getValidatedByManager());
			dto.setManager(entity.getManager().getName());
			dto.setValidatedByManagerOn(entity.getValidatedByManagerOn());
		}
		if (entity.getReviewedByCsr() != null && entity.getReviewedByCsr()) {
			dto.setReviewedByCsr(entity.getReviewedByCsr());
			dto.setCsr(entity.getCsr().getName());
			dto.setReviewedByCsrOn(entity.getReviewedByCsrOn());
		}
		dto.setLastModified(entity.getLastModified());
		dto.setOperationsForm(new ArrayList<>());
		dto.setComponentsForm(new ArrayList<>());
		entity.getFreeRepairOperations().forEach(o -> {
			FreeRepairOperationDTO froDto = new FreeRepairOperationDTO();
			froDto.setFrOperationId(o.getOperationId());
			froDto.setName(o.getName());
			froDto.setLabourTimeMinutes(o.getLabourTime()%60);
			froDto.setLabourTimeHours(o.getLabourTime()/60);
			froDto.setStatus(o.getOperationStatus());
			froDto.setType(o.getOperationType());
			froDto.setCost(o.getCost());
			froDto.setAddedAfterRiRValidation(o.getAddedAfterRiRValidation());
			froDto.setPosition(o.getPosition());
			if (o.getTpCompany() != null) {
				froDto.setCoid(o.getTpCompany().getCoid());
				froDto.setCoName(o.getTpCompany().getConame());
			}
			dto.getOperationsForm().add(froDto);

			if (o.getFreeRepairComponents() != null)
				o.getFreeRepairComponents().stream().filter(c -> !c.getAddedAfterRiRValidation()).forEach(c -> {
					FreeRepairComponentDTO frcDto = new FreeRepairComponentDTO();
					frcDto.setFrComponentId(c.getComponentId());
					frcDto.setAddedAfterRiRValidation(c.getAddedAfterRiRValidation());
					frcDto.setCost(c.getCost());
					frcDto.setFreeRepairOperationName(froDto.getName());
					frcDto.setFreeRepairOperationId(froDto.getFrOperationId());
					frcDto.setName(c.getName());
					frcDto.setQuantity(c.getQuantity());
					frcDto.setStatus(c.getStatus());
					frcDto.setSource(c.getSource());
					dto.getComponentsForm().add(frcDto);
				});
		});

		return dto;
	}

	private FreeRepairOperation getExistingOperation(int operationId, Set<FreeRepairOperation> opList) {
		if (opList != null)
			return opList.stream().filter(e -> e.getOperationId() == operationId).findFirst().orElse(null);
		else
			return null;
	}

	private FreeRepairComponent getExistingComponent(int componentId, Set<FreeRepairComponent> compList) {
		FreeRepairComponent frc = null;
		if (compList != null)
			frc = compList.stream().filter(e -> e.getComponentId().equals(componentId)).findFirst().orElse(null);
		return frc;
	}
}
