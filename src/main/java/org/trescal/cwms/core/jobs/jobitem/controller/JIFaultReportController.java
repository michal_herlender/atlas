package org.trescal.cwms.core.jobs.jobitem.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.failurereport.dto.FailureReportDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.form.JIFailureReportForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY,
		Constants.SESSION_ATTRIBUTE_SUBDIV })
public class JIFaultReportController extends JobItemController {

	@Autowired
	private FaultReportService faultReportService;

	private static final Logger logger = LoggerFactory.getLogger(JIFaultReportController.class);

	@PreAuthorize("hasAuthority('JI_FAULT_REPORT')")
	@RequestMapping(value = "/jifaultreport.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(Model model,
			@RequestParam(value = "jobitemid", required = true) Integer jobitemid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		Map<String, Object> refData = super.referenceData(jobitemid, contact, subdivDto.getKey(),username);
		JobItem ji = this.jobItemService.findJobItem(jobitemid);
		if (ji == null) {
			logger.error("Jobitem with id " + jobitemid + " could not be found");
			throw new RuntimeException("Unable to locate requested jobitem");
		}
		List<FailureReportDTO> failurereports = faultReportService.getAllFailureReportsByJobitemId(jobitemid);
		JIFailureReportForm form = new JIFailureReportForm();
		form.setFailurereports(failurereports);
		form.setJi(ji);
		model.addAttribute("form", form);
		refData.put("form", form);

		return new ModelAndView("trescal/core/jobs/jobitem/jifaultreport", refData);
	}
}