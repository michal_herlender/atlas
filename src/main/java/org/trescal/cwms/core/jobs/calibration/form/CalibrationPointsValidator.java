package org.trescal.cwms.core.jobs.calibration.form;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.db.CalibrationPointService;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class CalibrationPointsValidator extends AbstractBeanValidator
{
	@Autowired
	private CalibrationPointService calPointServ;

	public void setCalPointServ(CalibrationPointService calPointServ)
	{
		this.calPointServ = calPointServ;
	}

	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.isAssignableFrom(CalibrationPointsForm.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		CalibrationPointsForm form = (CalibrationPointsForm) target;

		if (form.getAction().equalsIgnoreCase("pointset"))
		{
			List<String> points = this.calPointServ.trimCalPointsList(form.getPoints());

			int count = 0;
			// loop over each point and check it is - not empty, not a
			// string
			for (String point : points)
			{
				if (point.trim().equalsIgnoreCase("")
						|| !NumberTools.isADouble(point.trim()))
				{
					errors.rejectValue("points[" + count + "]", "calibration.points.invalidpoint", null, "You entered an invalid point");
				}

				if (form.isRelational())
				{
					String relativePoint = form.getRelativePoints().get(count);
					if ((relativePoint != null)
							&& !relativePoint.trim().isEmpty()
							&& !NumberTools.isADouble(relativePoint.trim()))
					{
						errors.rejectValue("relativePoints[" + count + "]", "calibration.points.invalidrelatedpoint", null, "You entered an invalid relational point");
					}
				}

				count++;
			}

			// if (form.isSaveAsTemplate())
			// {
			// InvalidValue[] invalids =
			// AnnotationValidator.getInvalidValues(form.getTemplate());
			// for (InvalidValue invalid : invalids)
			// {
			// errors.rejectValue("template." + invalid.getPropertyPath(), null,
			// invalid.getMessage());
			// }
			// }
		}
		else if (form.getAction().equalsIgnoreCase("range"))
		{
			form.getRangeFrom();
			form.getRangeTo();

			if (form.isRelationalRange())
			{
				form.getRangeRelatedPoint();
			}

			if (form.getRangeFrom().trim().equalsIgnoreCase("")
					|| !NumberTools.isADouble(form.getRangeFrom().trim()))
			{
				errors.rejectValue("rangeFrom", "", null, "An invalid value was entered for the range start");
			}

			if (form.getRangeTo().trim().equalsIgnoreCase("")
					|| !NumberTools.isADouble(form.getRangeTo().trim()))
			{
				errors.rejectValue("rangeTo", "", null, "An invalid value was entered for the range end");
			}

			if (form.isRelationalRange())
			{
				if (form.getRangeRelatedPoint().trim().equalsIgnoreCase("")
						|| !NumberTools.isADouble(form.getRangeRelatedPoint().trim()))
				{
					errors.rejectValue("rangeRelatedPoint", "", null, "An invalid value was entered for the range's relational point");
				}
			}
		}

		// always check text input size

		// do the hibernate validation
		errors.pushNestedPath("cr");
		super.validate(form.getCr(), errors);
		errors.popNestedPath();
	}
}
