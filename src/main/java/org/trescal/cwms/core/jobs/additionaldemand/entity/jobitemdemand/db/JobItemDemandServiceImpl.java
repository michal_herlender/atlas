package org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemand.db;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemand.JobItemDemand;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemand;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemandStatus;

@Service
public class JobItemDemandServiceImpl extends BaseServiceImpl<JobItemDemand, Integer> implements JobItemDemandService {

	@Autowired
	private JobItemDemandDao jobItemDemandDao;

	@Override
	protected BaseDao<JobItemDemand, Integer> getBaseDao() {
		return jobItemDemandDao;
	}

	@Override
	public boolean[] convertToBooleanArray(Collection<JobItemDemand> demands) {
		boolean[] demandArray = new boolean[AdditionalDemand.values().length];
		for(JobItemDemand demand: demands)
			if(demand.getAdditionalDemandStatus() == AdditionalDemandStatus.REQUIRED || demand.getAdditionalDemandStatus() == AdditionalDemandStatus.DONE)
				demandArray[demand.getAdditionalDemand().ordinal()] = true;
		return demandArray;
	}
}