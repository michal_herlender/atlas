package org.trescal.cwms.core.jobs.certificate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;

/**
 * Creates a supplementary certificate for the specified certid
 * These would be generally accomplished from jicertificates.htm or viewcertificate.htm screens
 * and is split apart here for code separation as well as authorization control; 
 * because only the subdivision owning the certificate should be able to perform actions 
 * such as creating a supplementary (replacement) certificate.
 */
@Controller @IntranetController 
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME})
public class CreateSupplementaryCertificateController {

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private UserService userService;
	
	@PreAuthorize("@hasCertificateAuthorityOnConnectedSubdiv.check('JI_CERTIFICATE_SUPPLEMENTARY')")
	@RequestMapping(path="createsupplementarycertificate.htm", method=RequestMethod.POST)
	public String createSupplementaryCertificate(@RequestParam(name="certid", required=true) Integer certid,
			@RequestParam(name="jobitemid", required=true) Integer jobitemid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		Contact contact = this.userService.get(username).getCon();
		this.certificateService.createSupplementaryCert(certid, contact, null);
		return "redirect:jicertificates.htm?jobitemid=" + jobitemid;
	}
}
