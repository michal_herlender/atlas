package org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirementDTO;
import org.trescal.cwms.core.login.entity.user.db.UserDao;
import org.trescal.cwms.core.system.Constants;

@Service("TPRequirementService")
public class TPRequirementServiceImpl extends BaseServiceImpl<TPRequirement, Integer> implements TPRequirementService
{
	@Autowired
	private UserDao userDao;
	@Autowired
	private TPRequirementDao tpRequirementDao;
	
	@Override
	public ResultWrapper ajaxUpdateTPRequirement(int id, boolean inv, boolean cal, boolean rep, boolean adj,
			HttpSession session)
	{
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact contact = this.userDao.getEagerLoad(username).getCon();
		TPRequirement tpr = this.tpRequirementDao.find(id);
		tpr.setInvestigation(inv);
		tpr.setCalibration(cal);
		tpr.setRepair(rep);
		tpr.setAdjustment(adj);
		tpr.setLastUpdated(new Date());
		tpr.setUpdatedBy(contact);
		this.tpRequirementDao.update(tpr);
		return new ResultWrapper(true, null, tpr, null);
	}

	@Override
	protected BaseDao<TPRequirement, Integer> getBaseDao() {
		return this.tpRequirementDao;
	}

	@Override
	public List<TPRequirementDTO> getProjectionDTOsForIds(Collection<Integer> requirementIds) {
		List<TPRequirementDTO> result = new ArrayList<>();
		if (!requirementIds.isEmpty()) {
			result = this.tpRequirementDao.getProjectionDTOsForIds(requirementIds);
		}
		return result;
	}
}