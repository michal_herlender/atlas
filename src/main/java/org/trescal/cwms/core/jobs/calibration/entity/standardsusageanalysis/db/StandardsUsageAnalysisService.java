package org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.db;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.calibration.dto.StandardsUsageAnalysisDTO;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.StandardsUsageAnalysis;
import org.trescal.cwms.core.jobs.calibration.form.StandardsUsageAnalysisSearchForm;
import org.trescal.cwms.core.jobs.calibration.form.UpdateStandardsUsageAnalysisForm;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface StandardsUsageAnalysisService extends BaseService<StandardsUsageAnalysis, Integer> {

	PagedResultSet<StandardsUsageAnalysisDTO> sreachStandardsUsageAnalysis(StandardsUsageAnalysisSearchForm form,
																		   PagedResultSet<StandardsUsageAnalysisDTO> prs, Locale locale);

	void updateStandardsUsageAnalysis(StandardsUsageAnalysis standardsUsageAnalysis);

	void editStandardsUsageAnalysis(StandardsUsageAnalysis standardsUsageAnalysis,
									UpdateStandardsUsageAnalysisForm form);
	
	StandardsUsageAnalysis getStandardsUsageAnalysisFromInstrument(Integer plantId);
	
	StandardsUsageAnalysis createStandardsUsageAnalysis(Integer plantId, Integer certificateid, LocalDate startDate, Date finishDate);
	
	Boolean notifySubscribers(Set<String> subscriberEmails, Locale locale, List<StandardsUsageAnalysisDTO> dtos);
	
	void storeFileGenerated(SXSSFWorkbook sxxfWorkbook, String identifier);

	List<StandardsUsageAnalysisDTO> certificatesConnectedToMeasurementStandards(Integer businessSubdivId,
																				LocalDate startDate, Locale locale);
}
