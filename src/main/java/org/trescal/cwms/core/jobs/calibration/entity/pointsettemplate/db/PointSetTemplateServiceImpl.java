package org.trescal.cwms.core.jobs.calibration.entity.pointsettemplate.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.jobs.calibration.entity.pointsettemplate.PointSetTemplate;

@Service("PointSetTemplateService")
public class PointSetTemplateServiceImpl implements PointSetTemplateService
{
	@Autowired
	PointSetTemplateDao PointSetTemplateDao;

	public void deletePointSetTemplate(PointSetTemplate pointsettemplate)
	{
		this.PointSetTemplateDao.remove(pointsettemplate);
	}

	public PointSetTemplate findPointSetTemplate(int id)
	{
		return this.PointSetTemplateDao.find(id);
	}

	@Override
	public PointSetTemplate findPointSetTemplateWithPoints(int id)
	{
		return this.PointSetTemplateDao.findPointSetTemplateWithPoints(id);
	}

	public List<PointSetTemplate> getAllPointSetTemplates()
	{
		return this.PointSetTemplateDao.findAll();
	}

	public void insertPointSetTemplate(PointSetTemplate PointSetTemplate)
	{
		this.PointSetTemplateDao.persist(PointSetTemplate);
	}

	public void saveOrUpdatePointSetTemplate(PointSetTemplate pointsettemplate)
	{
		this.PointSetTemplateDao.saveOrUpdate(pointsettemplate);
	}

	public void setPointSetTemplateDao(PointSetTemplateDao PointSetTemplateDao)
	{
		this.PointSetTemplateDao = PointSetTemplateDao;
	}

	public void updatePointSetTemplate(PointSetTemplate PointSetTemplate)
	{
		this.PointSetTemplateDao.update(PointSetTemplate);
	}
}