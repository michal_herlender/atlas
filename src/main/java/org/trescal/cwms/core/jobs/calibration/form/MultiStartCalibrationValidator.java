package org.trescal.cwms.core.jobs.calibration.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.dto.MultiStartCal;
import org.trescal.cwms.core.jobs.calibration.dto.MultiStartCalItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.tools.StringTools;

import java.util.ArrayList;
import java.util.List;

@Component
public class MultiStartCalibrationValidator implements Validator {
    @Autowired
    private CapabilityAuthorizationService procAccServ;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(MultiStartCalibrationForm.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MultiStartCalibrationForm form = (MultiStartCalibrationForm) target;
        List<MultiStartCal> cals = form.getCals();
        Contact currentCon = form.getCurrentContact();

		for (MultiStartCal cal : cals)
		{
			// check that this cal was selected
			if (cal.isSelected())
			{
				List<JobItem> jis = new ArrayList<JobItem>();
				for (MultiStartCalItem item : cal.getItems())
				{
					jis.add(item.getJi());
				}
				String items = StringTools.combineSequenceJobItemNumbers(jis);

				// not accredited
                if (!this.procAccServ.authorizedFor(cal.getCapabilityId(), cal.getCalTypeId(), currentCon.getPersonid()).isRight()) {
                    errors.rejectValue("cals", null, "Calibration of "
                        + ((jis.size() > 1) ? "items " : "item ")
                        + items
                        + " cannot be started: Not accredited for procedure "
                        + cal.getCapability().getReference() + " ("
                        + cal.getCalType().getServiceType().getShortName() + ")");
                }

				// info acknowledged

				// enforced stds all checked

				// stds checked all in cal
			}
		}
	}
}