package org.trescal.cwms.core.jobs.job.controller;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.departmenttype.db.DepartmentTypeService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courier.db.CourierService;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.documents.birt.jobitemsheet.JobItemSheetGroup;
import org.trescal.cwms.core.documents.birt.jobitemsheet.JobItemSheetOrder;
import org.trescal.cwms.core.documents.images.image.ImageType;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.db.ContractReviewService;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.db.ContractReviewItemService;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobnote.JobNote;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.form.DeliveryForm;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.db.AccessoryService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db.JobExpenseItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.db.JobItemPOService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.instructionlink.db.InstructionLinkService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.MonthlyInvoice;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.POInvoice;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SeparateCosts;
import org.trescal.cwms.core.system.entity.userinstructiontype.UserInstructionType;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.MathTools;
import org.trescal.cwms.core.tools.StringTools;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.core.tools.fileupload2.web.formobjects.DataImportForm;
import org.trescal.cwms.spring.model.KeyValue;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.HTTPSESS_NEW_FILE_LIST, Constants.SESSION_ATTRIBUTE_COMPANY,
	Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class ViewJobController {

	private static final Logger logger = LoggerFactory.getLogger(ViewJobController.class);

	@Value("#{props['cwms.config.jobitem.fast_track_turn']}")
	private int fastTrackTurn;
	@Value("#{props['cwms.config.quotation.resultsyearfilter']}")
	private Integer resultsYearFilter;
	@Value("#{props['cwms.config.instrument.use_threads']}")
	private boolean useThreads;
	@Value("#{props['cwms.config.jobitem.useworkrequirements']}")
	private boolean useWorkReqs;
	@Autowired
	private AddressService addressService;

	@Autowired
	private CalReqService calReqServ;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;
	@Autowired
	private ContractReviewItemService contractReviewItemService;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private InstructionLinkService instructionLinkService;
	@Autowired
	private InstrumService instrumServ;
	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private JobItemPOService jipoServ;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private JobExpenseItemService jobExpenseItemService;
	@Autowired
	private JobService jobServ;
	@Autowired
	private PurchaseOrderService purchaseOrderServ;
	@Autowired
	private QuotationService quotationService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SystemComponentService scService;
	@Autowired
	private UserService userService;
	// System Defaults
	@Autowired
	private MonthlyInvoice monthlyInvoice;
	@Autowired
	private POInvoice poInvoice;
	@Autowired
	private SeparateCosts separateCosts;
	@Autowired
	private CourierService courierServ;
	@Autowired
	private LinkToAdveso linkToAdvesoSettings;
	@Autowired
	private AccessoryService accessoryService;
	@Autowired
	private DepartmentTypeService deptTypeServ;
	@Autowired
	private DeliveryService delService;
	@Autowired
	private ContractReviewService crService;

	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
		return new ArrayList<>();
	}

	@ModelAttribute("command")
	protected DataImportForm formBackingObject() {
		return new DataImportForm();
	}
	
	@ModelAttribute("couriers")
	protected List<Courier> setCouriers(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		List<Courier> list = new ArrayList<>();
		list.add(new Courier());
		list.addAll(courierServ.getCouriersByBusinessCompany(companyDto.getKey()));
		return list;
	}

	private List<KeyValue<Integer, String>> getBookingInAddresses(String username, Job job) {
		List<KeyValue<Integer, String>> result;
		if (job.getType().equals(JobType.SITE)) {
			result = this.addressService.getAllActiveSubdivAddressesKeyValue(job.getCon().getSub().getSubdivid(),
					AddressType.DELIVERY, true);
		} else {
			result = this.addressService.getActiveUserRoleAddressesKeyValue(username, true);
		}
		return result;
	}

	@PreAuthorize("hasAuthority('JOB_VIEW')")
	@RequestMapping(value = "/viewjob.htm", method = RequestMethod.GET)
	public String referenceData(Model model, @RequestParam(value = "jobid") Integer jobid,
			@RequestParam(value = "quickplantid", required = false, defaultValue = "0") Integer plantid,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		Job job = this.jobServ.findEagerJobForView(jobid);
		if (job == null)
			throw new RuntimeException("Job not found for id : " + jobid);
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Company allocatedCompany = allocatedSubdiv.getComp();
		CompanySettingsForAllocatedCompany settings = this.companySettingsService
				.getByCompany(job.getCon().getSub().getComp(), allocatedCompany);
		User user = this.userService.get(username);
		Contact contact = user.getCon();
		model.addAttribute("companyActive", settings != null && settings.isActive());
		model.addAttribute("companyOnStop", settings == null || settings.isOnStop());
		// initialise variables for job item cost summary
		BigDecimal totalCost = new BigDecimal("0.00");
		BigDecimal vatFraction;
		BigDecimal vatValue;
		BigDecimal finalCost;
		// calculate total of all costed items
		for (JobItem ji : job.getItems()) {
			// job item has costs?
			if (ji.getJobCostingItems().size() > 0) {
				// get most recent cost
				for (JobCostingItem jci : ji.getJobCostingItems()) {
					// add latest costing to total for job
					totalCost = totalCost.add(jci.getFinalCost());
					// break out of loop, only most recent cost required
					break;
				}
			}
		}

		// get the vat rate, calculate vat using total costs and then work out the final
		// cost
		VatRate vatRate = settings != null ? settings.getVatrate() : null;
		vatFraction = vatRate != null && vatRate.getRate() != null && vatRate.getRate().doubleValue() > 0
				? vatRate.getRate().divide(new BigDecimal("100.00"))
				: new BigDecimal("0.00");
		vatValue = vatFraction.multiply(totalCost).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN);
		finalCost = totalCost.add(vatValue);
		model.addAttribute("jcTotalCost", totalCost);
		model.addAttribute("jcVatValue", vatValue);
		model.addAttribute("jcFinalCost", finalCost);
		job.setSentEmails(this.emailServ.getAllComponentEmails(Component.JOB, jobid));
		job.setRelatedInstructionLinks(this.instructionLinkService.getAllForJob(job));
		Map<Integer, Pair<Integer, Long>> bpoItemSizes = new HashMap<>();
		job.getBpoLinks().forEach(link -> {
			BPO bpo = link.getBpo();
			Integer count = this.jipoServ.getItemsOnBPO(link.getBpo().getPoId(), job.getJobid()).size();
			Long ecount = jobExpenseItemService.countItemsForJobOnBPO(job.getJobid(), bpo.getPoId());
			bpoItemSizes.put(link.getBpo().getPoId(), Pair.of(count, ecount));
		});
		
		FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ.getFilesForComponentRoot(Component.JOB, job.getJobid(), newFiles);
		model.addAttribute("bpoItemSizes", bpoItemSizes);
		model.addAttribute(Constants.REFDATA_SYSTEM_COMPONENT, this.scService.findComponent(Component.JOB));
		// load a list of available currencies
		model.addAttribute("resultsYearFilter", this.resultsYearFilter);
		model.addAttribute(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
		model.addAttribute(Constants.REFDATA_FILES_NAME, fileBrowserServ.getFilesName(fileBrowserWrapper));
		Locale locale = LocaleContextHolder.getLocale();
		model.addAttribute("expenseModels", instrumentModelService.getAll(DomainType.SERVICE, locale, null, null));
		model.addAttribute("expenseServiceTypes", serviceTypeService.getAllByDomainType(DomainType.SERVICE));
		model.addAttribute("linkedQuotesWithServiceModels",
				quotationService.findLinkedQuotationsWithServiceModels(job));
		model.addAttribute("contact", contact);
		String instructionTypes = contact.getUserPreferences().getUserInstructionTypes().stream().map(UserInstructionType::getInstructionType).map(InstructionType::name).collect(Collectors.joining(","));
		model.addAttribute("userInstructionTypes", contact.getUserPreferences().getUserInstructionTypes());
		model.addAttribute("instructionTypes", instructionTypes);
		// only if threads ARE used and work requirements are NOT used should
		// this page link be available (this is a very MD specific page!)
		model.addAttribute("allowUpdateAll", (this.useThreads && !this.useWorkReqs));
		// load a set of all jobitems that have not yet been costed
		model.addAttribute("nocostjobitems", this.jiServ.getJobItemsWithNoCostings(job.getJobid()));
		model.addAttribute("privateOnlyNotes", JobNote.privateOnly);
		model.addAttribute("stringtools", new StringTools());
		// load any monthly invoice settings that may apply to this job
		model.addAttribute("monthlyinvoicesystemdefault",
				monthlyInvoice.getValueByScope(Scope.COMPANY, job.getCon().getSub().getComp(), allocatedCompany));
		model.addAttribute("poinvoicesystemdefault",
				poInvoice.getValueByScope(Scope.COMPANY, job.getCon().getSub().getComp(), allocatedCompany));
		model.addAttribute("separatecostsinvoicesystemdefault",
				separateCosts.getValueByScope(Scope.COMPANY, job.getCon().getSub().getComp(), allocatedCompany));
		model.addAttribute("PoSize", this.purchaseOrderServ.countPoPerJob(jobid, locale));
		model.addAttribute("ContractReviewSize", this.crService.CountCryjob(jobid));
		model.addAttribute("contractReviewedJobItemIds",
				this.contractReviewItemService.getContractReviewedJobItemIds(job.getJobid()));
		model.addAttribute(Constants.FAST_TRACK_TURN, this.fastTrackTurn);
		// add datetools
		model.addAttribute("datetools", new DateTools());
		// if a plantid has been passed to pre-populate the quick item AND
		// the plantid is a valid instrument in the database AND
		// it's not already on the job (if page has been refreshed with same
		// parameters it could be!)
		if ((plantid != 0) && (this.instrumServ.get(plantid) != null)
				&& (!this.instrumServ.isInstrumentOnJob(plantid, job.getJobid()))) {
			model.addAttribute("quickitembc", plantid);
		}
		// find applicable calibration requirements that may have been set
		Map<Integer, CalReq> calReqs = this.calReqServ.findCalReqsForAllItemsOnJob(job.getJobid());
		model.addAttribute("calReqs", calReqs);
		model.addAttribute("job", job);
		model.addAttribute("imagetype", ImageType.JOBITEM);
		model.addAttribute("jobItemSheetGroupValues", JobItemSheetGroup.values());
		model.addAttribute("jobItemSheetOrderValues", JobItemSheetOrder.values());
		// For quick add - may separate
		model.addAttribute("bookingInAddresses", getBookingInAddresses(username, job));
		// set the client subdiv link to adveso boolean
		boolean islinkToAdveso = linkToAdvesoSettings.isSubdivLinkedToAdvesoCached(job.getCon().getSub().getSubdivid(),
				allocatedCompany.getCoid(),  new Date());
		model.addAttribute("isLinkedToAdveso", islinkToAdveso);
		model.addAttribute("countItemsJobAccessory", accessoryService.countByJob(job.getJobid()));
		model.addAttribute("jobDeliveryForm", new DeliveryForm(job.getNumberOfPackages(), job.getPackageType(),
				job.getStorageArea(), job.getJobid()));
		model.addAttribute("activeDeptDto",this.deptTypeServ.getActiveDepartmentTypeDtos());
		model.addAttribute("delivriesSize", this.delService.getdeliveriesIdsByJob(jobid).size());
		long count = job.getRelatedInstructionLinks().stream()
				.map(instructionLink -> instructionLink.getInstruction())
				.filter(instruction -> (instruction.getInstructiontype().equals(InstructionType.DISCOUNT) || instruction.getInstructiontype().equals(InstructionType.COSTING)))
				.count();

		logger.info("GET job last modified on " + job.getLastModified().toString());
		return "trescal/core/jobs/job/viewjob";
	}
}