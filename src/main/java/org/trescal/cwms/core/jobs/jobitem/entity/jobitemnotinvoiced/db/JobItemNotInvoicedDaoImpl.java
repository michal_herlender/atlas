package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db;

import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced_;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemNotInvoicedDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice_;

@Repository
public class JobItemNotInvoicedDaoImpl extends BaseDaoImpl<JobItemNotInvoiced, Integer>
		implements JobItemNotInvoicedDao {

	@Override
	protected Class<JobItemNotInvoiced> getEntity() {
		return JobItemNotInvoiced.class;
	}
	
	@Override
	public List<JobItemNotInvoicedDTO> getProjectionDTOs(Collection<Integer> periodicInvoiceIds) {
		if ((periodicInvoiceIds == null) || periodicInvoiceIds.isEmpty())
			throw new IllegalArgumentException("Must specify at least one invoiceId");
		return getResultList(cb -> {
			CriteriaQuery<JobItemNotInvoicedDTO> cq = cb.createQuery(JobItemNotInvoicedDTO.class); 
			Root<JobItemNotInvoiced> root = cq.from(JobItemNotInvoiced.class);
			Join<JobItemNotInvoiced, Invoice> periodicInvoice = root.join(JobItemNotInvoiced_.periodicInvoice, JoinType.INNER);
			Join<JobItemNotInvoiced, JobItem> jobItem = root.join(JobItemNotInvoiced_.jobItem, JoinType.INNER);
			Join<JobItemNotInvoiced, Contact> contact = root.join(JobItemNotInvoiced_.contact, JoinType.INNER);
			
			cq.where(periodicInvoice.get(Invoice_.id).in(periodicInvoiceIds));
			
			cq.select(cb.construct(JobItemNotInvoicedDTO.class,
				root.get(JobItemNotInvoiced_.date),
				contact.get(Contact_.personid),
				root.get(JobItemNotInvoiced_.reason),
				root.get(JobItemNotInvoiced_.comments),
				periodicInvoice.get(Invoice_.id),
				root.get(JobItemNotInvoiced_.id),
				jobItem.get(JobItem_.jobItemId)));
			
			return cq;
		});
	}
}