package org.trescal.cwms.core.jobs.jobitem.entity.accessory;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Entity
@Table(name = "itemaccessory")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
public class ItemAccessory {

	private Integer id;
	private JobItem ji;
	private Accessory accessory;
	private Integer quantity;
	private FaultReport faultReport;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid")
	public JobItem getJi() {
		return this.ji;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "accessory_id")
	public Accessory getAccessory() {
		return accessory;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "faultreportid", foreignKey = @ForeignKey(name="FK_itemaccessory_faultreportid"))
	public FaultReport getFaultReport() {
		return faultReport;
	}

	public Integer getQuantity() {
		return quantity;
	}
}