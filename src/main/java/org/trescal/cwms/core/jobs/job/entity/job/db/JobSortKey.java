package org.trescal.cwms.core.jobs.job.entity.job.db;

import org.trescal.cwms.core.jobs.job.entity.job.Job;

/**
 * Defines the different sort options available for sorting sets of {@link Job}
 * results.
 * 
 * @author Richard
 */
public enum JobSortKey
{
	COMPANY, DATECOMPLETE, JOB;
}
