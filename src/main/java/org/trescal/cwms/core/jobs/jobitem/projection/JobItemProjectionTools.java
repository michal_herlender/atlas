package org.trescal.cwms.core.jobs.jobitem.projection;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.jobcost.projection.JobCostingItemProjectionDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

/**
 * Static methods to help with job item projection data
 */
public class JobItemProjectionTools {
	
	/**
	 * price / currency to come from latest job pricing or contract review (if no job pricings)
	 * Relies on contract review or job pricing info loaded in JobItemProjectionDto
	 * @param jiDto
	 * @return
	 */
	
	public static PriceSummaryDTO getPriceAndCurrency(JobItemProjectionDTO jiDto) {
		
		KeyValueIntegerString jobCurrency = new KeyValueIntegerString();
		
		if(jiDto.getJob() != null){
			jobCurrency = jiDto.getJob().getCurrency();
		}
		else if(jiDto.getCurrency() != null)
			jobCurrency = jiDto.getCurrency();
		PriceSummaryDTO result = null;
		if ((jiDto.getCostingItems() != null) && !jiDto.getCostingItems().isEmpty()) {
			// Obtain job costing price from newest costing item (by ID of costing item)
			Comparator<JobCostingItemProjectionDTO> comparator = Comparator.comparingInt(JobCostingItemProjectionDTO::getPricingItemId);
			SortedSet<JobCostingItemProjectionDTO> sorted = new TreeSet<>(comparator);
			sorted.addAll(jiDto.getCostingItems());
			JobCostingItemProjectionDTO ciDto = sorted.last();
			result = new PriceSummaryDTO(ciDto.getFinalCost(), jobCurrency, CostSource.JOB_COSTING, ciDto.getJobCostingVersion());
		}

		if (result == null) {
			// Revert to contract review price if no job costing prices provided
			JobItemPriceProjectionDto crPriceDto = jiDto.getPriceContractReview();
			BigDecimal crTotal = crPriceDto.getPrices().stream()
				.filter(priceDto -> priceDto.getActive())
				.map(priceDto -> priceDto.getTotalCost())
				.reduce(BigDecimal.valueOf(0, 2), BigDecimal::add);
			
			result = new PriceSummaryDTO(crTotal, jobCurrency, CostSource.CONTRACT_REVIEW, null);
		}
		return result;
	}
	
	/**
	 * Sets price/currency via estimatedPrice for all job item DTOs passed
	 * The JobItemDTOs should have at least the job loaded with currency,
	 * and a contract review price loaded.  Will load prices first from 
	 * job costing if available.
	 * 
	 * @param jobItemDtos
	 */
	public static void setEstimatedPrices(Collection<JobItemProjectionDTO> jobItemDtos) {
		if (jobItemDtos == null) throw new IllegalArgumentException("null jobItemDtos passed");
		for (JobItemProjectionDTO jiDto : jobItemDtos) {
			PriceSummaryDTO estimatedPrice = getPriceAndCurrency(jiDto);
			 jiDto.setEstimatedPrice(estimatedPrice);
		}
	}
}
