package org.trescal.cwms.core.jobs.calibration.dto;

import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MultiStartCal {
    private Boolean accredited;
    private CalibrationClass calClass;
    private LocalDate calDate;
    private CalibrationProcess calProcess;
    private Integer calProcessId;
    private CalibrationType calType;
    private Integer calTypeId;
    private List<MultiStartCalItem> items;
    private Capability capability;
    private Integer capabilityId;
    private boolean selected;

    public MultiStartCal(Capability capability, CalibrationClass calClass, CalibrationType calType) {
        this.capability = capability;
        this.capabilityId = (capability == null) ? null : capability.getId();
        this.calType = calType;
        this.calTypeId = (calType == null) ? null : calType.getCalTypeId();
        this.calClass = calClass;
        this.calDate = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
        this.items = new ArrayList<>();
    }

    public Boolean getAccredited() {
        return this.accredited;
    }

    public CalibrationClass getCalClass() {
        return this.calClass;
    }

    public LocalDate getCalDate() {
		return this.calDate;
	}

	public CalibrationProcess getCalProcess() {
		return this.calProcess;
	}

	public Integer getCalProcessId()
	{
		return this.calProcessId;
	}

	public CalibrationType getCalType()
	{
		return this.calType;
	}

    public Integer getCalTypeId() {
        return this.calTypeId;
    }

    public List<MultiStartCalItem> getItems() {
        return this.items;
    }

    public Capability getCapability() {
        return this.capability;
    }

    public Integer getCapabilityId() {
        return this.capabilityId;
    }

    public boolean isSelected() {
        return this.selected;
    }

	public void setAccredited(Boolean accredited) {
		this.accredited = accredited;
	}

	public void setCalClass(CalibrationClass calClass) {
		this.calClass = calClass;
	}

	public void setCalDate(LocalDate calDate) {
		this.calDate = calDate;
	}

	public void setCalProcess(CalibrationProcess calProcess) {
		this.calProcess = calProcess;
	}

	public void setCalProcessId(Integer calProcessId)
	{
		this.calProcessId = calProcessId;
	}

	public void setCalType(CalibrationType calType)
	{
		this.calType = calType;
	}

    public void setCalTypeId(Integer calTypeId) {
        this.calTypeId = calTypeId;
    }

    public void setItems(List<MultiStartCalItem> items) {
        this.items = items;
    }

    public void setCapability(Capability capability) {
        this.capability = capability;
    }

    public void setCapabilityId(Integer capabilityId) {
        this.capabilityId = capabilityId;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}