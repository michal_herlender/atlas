package org.trescal.cwms.core.jobs.additionaldemand.enums;

public enum AdditionalDemand {
	CLEANING("additionalDemand.cleaning"), SEALING("additionalDemand.sealing"), ENGRAVING("additionalDemand.engraving");

	private String messageCode;

	private AdditionalDemand(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
}