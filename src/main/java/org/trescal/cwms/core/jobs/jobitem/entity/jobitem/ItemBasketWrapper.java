package org.trescal.cwms.core.jobs.jobitem.entity.jobitem;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.spring.model.KeyValue;

@Data
@JsonSerialize(converter = ItemBasketWrapperConverter.class)
public class ItemBasketWrapper {
    private JobItem baseItem;
    private Integer baseItemId;
    private Integer existingGroupId;
    private Instrument inst;
    private InstrumentModel model;
    private Quotationitem quotationItem; // Used by controller / service
    private KeyValue<Integer, String> quotationItemDto; // Used by view
    private String part;
    private Capability proc;
    private Integer tempGroupId;
    private WorkInstruction wi;
    private Boolean isInstrumentCalTypeCompatibleWithJobType;

    public ItemBasketWrapper(Instrument inst) {
        this.inst = inst;
    }

    public ItemBasketWrapper() {
        super();
    }
}
