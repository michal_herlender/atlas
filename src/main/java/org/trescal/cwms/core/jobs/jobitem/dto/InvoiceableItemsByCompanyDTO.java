package org.trescal.cwms.core.jobs.jobitem.dto;

import org.trescal.cwms.core.company.entity.corole.CompanyRole;

public class InvoiceableItemsByCompanyDTO {

	private Integer companyId;
	private String companyName;
	private CompanyRole companyRole;
	private Long itemsCount;

	public InvoiceableItemsByCompanyDTO() {
	}

	public InvoiceableItemsByCompanyDTO(Integer companyId, String companyName, CompanyRole companyRole,
			Long itemsCount) {
		this.companyId = companyId;
		this.companyName = companyName;
		this.companyRole = companyRole;
		this.itemsCount = itemsCount;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public CompanyRole getCompanyRole() {
		return companyRole;
	}

	public void setCompanyRole(CompanyRole companyRole) {
		this.companyRole = companyRole;
	}

	public Long getItemsCount() {
		return itemsCount;
	}

	public void setItemsCount(Long itemsCount) {
		this.itemsCount = itemsCount;
	}
}