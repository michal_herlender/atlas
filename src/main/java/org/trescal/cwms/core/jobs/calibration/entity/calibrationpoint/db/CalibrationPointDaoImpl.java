package org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.db;

import java.util.Collection;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.CalibrationPoint;

@Repository("CalibrationPointDao")
public class CalibrationPointDaoImpl extends BaseDaoImpl<CalibrationPoint, Integer> implements CalibrationPointDao {
	
	@Override
	protected Class<CalibrationPoint> getEntity() {
		return CalibrationPoint.class;
	}
	
	@Override
	public void deleteAll(Collection<CalibrationPoint> points) {
		for(CalibrationPoint cp : points) remove(cp);
	}
}