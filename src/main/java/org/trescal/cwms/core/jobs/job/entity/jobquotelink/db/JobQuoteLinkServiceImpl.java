package org.trescal.cwms.core.jobs.job.entity.jobquotelink.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.jobs.job.dto.JobQuoteLinkDTO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobnote.JobNote;
import org.trescal.cwms.core.jobs.job.entity.jobnote.db.JobNoteService;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.system.entity.defaultnote.DefaultNote;
import org.trescal.cwms.core.system.entity.defaultnote.db.DefaultNoteService;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

import java.util.*;
import java.util.stream.Collectors;

@Service("JobQuoteLinkService")
public class JobQuoteLinkServiceImpl extends BaseServiceImpl<JobQuoteLink, Integer> implements JobQuoteLinkService
{
	@Autowired
	private JobQuoteLinkDao jobQuoteLinkDao;
	@Autowired
	private JobNoteService jobNoteService;
	@Autowired
	private DefaultNoteService dnServ;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private MessageSource messageSource;
	
	public static final String MESSAGE_CODE_JOB = "job.messageNote";
	
	@Override
	protected BaseDao<JobQuoteLink, Integer> getBaseDao() {
		return jobQuoteLinkDao;
	}
	
	@Override
	public List<JobQuoteLink> findJobQuoteLinks(int quoteid, int jobid) {
		return this.jobQuoteLinkDao.findJobQuoteLinks(quoteid, jobid);
	}
	
	public List<JobQuoteLink> findJobQuoteLinksResultset(int quoteid, Integer page, Integer resPerPage) {
		return this.jobQuoteLinkDao.findJobQuoteLinksResultset(quoteid, page, resPerPage);
	}
	
	public ResultWrapper findJobQuoteLinksResultsetDWR(int quoteid, Integer page, Integer resPerPage) {
		// check the quote exists?
		if (this.quoteServ.get(quoteid) != null) {
			// new list of job quote links
			List<JobQuoteLink> jqlinks = this.findJobQuoteLinksResultset(quoteid, page, resPerPage);
			// return successful result wrapper
			return new ResultWrapper(true, "", jqlinks, null);
		}
		else
			// return un-successful result wrapper
			return new ResultWrapper(false, "The quotation could not be found", null, null);
	}
	
	public Integer getCountJobQuoteLinks(int quoteid) {
		return this.jobQuoteLinkDao.getCountJobQuoteLinks(quoteid);
	}
	
	public List<Integer> getModelIdsFromJobQuotes(Integer jobId, Integer jobQuoteLinkId, Boolean similar) {
		Set<InstrumentModel> modelSet;
		modelSet = jobQuoteLinkId != null ?
				this.jobQuoteLinkDao.getModelIdsFromJobQuote(jobQuoteLinkId) :
					this.jobQuoteLinkDao.getModelIdsFromAllJobQuotes(jobId);
		//See if any of the models found so far are sales category type models, if so find all models of same sales category but 
		//but exclude actual sales category type model from list
		Set<InstrumentModel> fullModelSet = new HashSet<>();
		for(InstrumentModel model : modelSet)
			if(model.getModelType().getSalescategory() && similar)
				//Sales category model and we're searching for similar so find all stand alone models with same sales category.
				fullModelSet.addAll(instrumentModelService.getOtherModelsWithMatchingSalesCategory(model));
			else if(model.getModelType().getSalescategory() && !similar)
				//Sales category model but we're not searching for similar so only find stand alone model with same sales category and same name. 
				fullModelSet.addAll(instrumentModelService.getStandAloneModelsMatchingSalesCategoryModel(model));
			else fullModelSet.add(model);

		return fullModelSet.stream().map(InstrumentModel::getModelid).collect(Collectors.toList());
	}

	@Override
	public JobQuoteLinkDTO linkJobToQuote(Quotation quotation, Job job, Contact currentContact) {
		
		JobQuoteLink jql = new JobQuoteLink();
		jql.setJob(job);
		jql.setQuotation(quotation);
		jql.setLinkedBy(currentContact);
		jql.setLinkedOn(new Date());
		this.save(jql);
		// check for any notes on the quotation and copy to the job
		if (quotation.getNotes() != null && quotation.getNotes().size() > 0) {
			// grab a list of default notes that we don't want to copy
			// over
			List<DefaultNote> defNotes = this.dnServ.getDefaultNotesByComponent(Component.QUOTATION, quotation.getOrganisation().getCoid());
			for (QuoteNote qnote : quotation.getNotes()) {
				if (qnote.isActive()) {
					boolean isDefault = defNotes.stream().anyMatch(dn -> qnote.getNote().equals(dn.getNote()));
					if (!isDefault) {
						JobNote jNote = new JobNote();
						jNote.setActive(true);
						jNote.setJob(job);
						jNote.setNote(qnote.getNote());
						// all job notes are private
						jNote.setPublish(false);
						jNote.setSetBy(qnote.getSetBy());
						jNote.setSetOn(qnote.getSetOn());
						jNote.setLabel(messageSource.getMessage(MESSAGE_CODE_JOB, null,"Note from linked quote", LocaleContextHolder.getLocale()) +" (" + quotation.getQno() + ")");
						jNote.setSetBy(qnote.getSetBy());
						jNote.setSetOn(qnote.getSetOn());
						if (job.getNotes() == null) job.setNotes(new TreeSet<>(new NoteComparator()));
						job.getNotes().add(jNote);
						jobNoteService.insertJobNote(jNote);
					}
				}
			}
		}
		JobQuoteLinkDTO dto = new JobQuoteLinkDTO();
        dto.setQuoteLinkId(jql.getId());
        dto.setQuoteId(jql.getQuotation().getId());
        dto.setQuoteQno(jql.getQuotation().getQno());
        dto.setQuoteCompany(jql.getQuotation().getContact().getSub().getComp().getConame());
        dto.setQuoteContact(jql.getQuotation().getContact().getName());
        dto.setQuoteVersion(jql.getQuotation().getVer());
		dto.setQuoteLinkedOn(jql.getLinkedOn());
		dto.setQuoteLinkedBy(jql.getLinkedBy().getName());
		return dto;
	}

}