package org.trescal.cwms.core.jobs.jobitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.db.OnBehalfItemService;

@Controller @JsonController
public class OnBehalfJsonController {
	
	@Autowired
	private OnBehalfItemService onBehalfItemService;
	
	@RequestMapping(value="deleteOnBehalfItem.json", method=RequestMethod.GET)
	@ResponseBody
	public void deleteOnBehalftItem(
			@RequestParam(name="onBehalfItemId", required=true) Integer onBehalfItemId) {
		System.out.println("CALL ON-BEHALF");
		OnBehalfItem onBehalfItem = onBehalfItemService.get(onBehalfItemId);
		if(onBehalfItem != null) onBehalfItemService.delete(onBehalfItem);
	}
}