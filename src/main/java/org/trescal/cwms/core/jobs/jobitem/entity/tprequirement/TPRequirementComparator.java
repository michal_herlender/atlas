package org.trescal.cwms.core.jobs.jobitem.entity.tprequirement;

import java.util.Comparator;

/**
 * @author jamiev
 */
public class TPRequirementComparator implements Comparator<TPRequirement>
{
	public int compare(TPRequirement tpr1, TPRequirement tpr2)
	{
		Integer id1 = tpr1.getId();
		Integer id2 = tpr2.getId();
		return id2.compareTo(id1);
	}
}