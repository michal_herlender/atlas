package org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;

public interface JobItemGroupDao extends BaseDao<JobItemGroup, Integer>
{
	List<JobItemGroup> getAll(int jobId);
	
	JobItemGroup getGroupWithItems(int groupId);
	
	List<Integer> getJobitemsByGroupId(int groupId);
	
	List<Integer> getGroupIdsByJobitemIds(List<Integer> jobitemIds);
}