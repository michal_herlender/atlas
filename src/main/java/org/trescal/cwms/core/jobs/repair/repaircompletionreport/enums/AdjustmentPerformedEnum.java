package org.trescal.cwms.core.jobs.repair.repaircompletionreport.enums;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum AdjustmentPerformedEnum {
	YES("yes"),
	NO("no"),
	NOT_APPLICABLE("repaircompletionreport.notapplicable");

	private String messageCode;
	private ReloadableResourceBundleMessageSource messageSource;

	private AdjustmentPerformedEnum(String messageCode) {
		this.messageCode = messageCode;
	}

	@Component
	private static class MessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (AdjustmentPerformedEnum e : EnumSet.allOf(AdjustmentPerformedEnum.class))
				e.messageSource = messageSource;
		}
	}

	public String getMessage() {
		return messageSource.getMessage(messageCode, null, this.toString(), LocaleContextHolder.getLocale());
	}

	public String getMessageCode() {
		return messageCode;
	}
	
	public String getValue() {
		Locale locale = LocaleContextHolder.getLocale();
		if (messageSource != null) {
			return messageSource.getMessage(this.messageCode, null, this.name(), locale);
		}
		return this.toString();
	}
}
