package org.trescal.cwms.core.jobs.calibration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.calibration.dto.MultiStartCal;
import org.trescal.cwms.core.jobs.calibration.dto.MultiStartCalItem;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalibrationToCalLinkComparator;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.db.StandardUsedService;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.jobs.calibration.form.MultiStartCalibrationForm;
import org.trescal.cwms.core.jobs.calibration.form.MultiStartCalibrationValidator;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandard;
import org.trescal.cwms.core.procedure.entity.defaultstandard.db.DefaultStandardService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedurestandard.CapabilityStandard;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.procedure.entity.workinstructionstandard.WorkInstructionStandard;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class MultiStartCalibrationController {
	@Autowired
	private CalibrationProcessService calProServ;
    @Autowired
    private CalibrationService calServ;
    @Autowired
    private CalibrationTypeService calTypeServ;
    @Autowired
    private DefaultStandardService defStandServ;
    @Autowired
    private InstrumService instrumServ;
    @Autowired
    private JobItemService jiServ;
    @Autowired
    private JobService jobServ;
    @Autowired
    private CapabilityService procServ;
    @Autowired
    private UserService userService;
    @Autowired
    private StandardUsedService stdUsedServ;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private MultiStartCalibrationValidator validator;

    @InitBinder("form")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
	}

	@ModelAttribute("form")
	protected MultiStartCalibrationForm formBackingObject(HttpServletRequest request,
														  @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		Contact contact = this.userService.get(username).getCon();
		// get request parameters
		int jobId = ServletRequestUtils.getIntParameter(request, "jobid", 0);
		Job job = this.jobServ.get(jobId);
		int jiId = ServletRequestUtils.getIntParameter(request, "jiid", 0);
		JobItem jItem = this.jiServ.findJobItem(jiId);

		// new list of multi start calibrations
		List<MultiStartCal> mCals = new ArrayList<>();
		// new list of job item ids
		Set<Integer> jobItemIds = new HashSet<>();
		// list of job items if job item passed
		List<JobItem> optionalMultiStartItems = new ArrayList<>();
		// job item passed?
		if ((jiId != 0) && (jItem != null)) {
            // find next procedure to be performed on job item
            Capability proc = null;
            if ((jItem.getNextWorkReq() != null)
                && (jItem.getNextWorkReq().getWorkRequirement().getCapability() != null)) {
                proc = jItem.getNextWorkReq().getWorkRequirement().getCapability();
            } else if (jItem.getCapability() != null) {
                proc = jItem.getCapability();
            }
            // get all other items at same state for option to start multiple
            // calibrations
            assert proc != null;
            optionalMultiStartItems = this.jiServ.getJobItemsByStatusAndProc(job.getJobid(),
                jItem.getState().getStateid(), proc.getId(), null);
            // multiple items to start?
            if (optionalMultiStartItems.size() > 0) {
                // populate job item ids
                for (JobItem ji : optionalMultiStartItems) {
                    jobItemIds.add(ji.getJobItemId());
				}
			}
		} else {
			// populate job item ids
			for (JobItem ji : job.getItems()) {
				jobItemIds.add(ji.getJobItemId());
			}
		}
		// check job groups
		for (JobItemGroup group : job.getGroups()) {
			if (group.isCalibrationGroup()) {
                boolean allowCal = true;
                Capability nextProc = null;
                CalibrationClass calClass = null;
                boolean firstInGroup = true;
                CalibrationType calType = null;

                for (JobItem ji : group.getItems()) {
                    if (firstInGroup) {
                        nextProc = this.getDefaultProcedure(ji);
                        calClass = this.getDefaultCalClass(ji);
                        calType = ji.getNextWorkReq().getWorkRequirement().getServiceType().getCalibrationType();
                        firstInGroup = false;
                    }

					if (!this.jiServ.isReadyForCalibration(ji)
							&& !ji.getInst().getModel().getModelType().getModules()) {
						allowCal = false;
					}
				}

				if (allowCal) {
					MultiStartCal mCal = new MultiStartCal(nextProc, calClass, calType);

					for (JobItem ji : group.getItems()) {
						MultiStartCalItem mCalItem = new MultiStartCalItem();
						mCalItem.setJi(ji);
						mCal.getItems().add(mCalItem);
						jobItemIds.remove(ji.getJobItemId());
					}

					mCals.add(mCal);
				}
			}
		}
		// job item passed?
		if ((jiId != 0) && (jItem != null)) {
			for (JobItem ji : optionalMultiStartItems) {
				if (jobItemIds.contains(ji.getJobItemId())
						&& ((ji.getGroup() == null) || !ji.getGroup().isCalibrationGroup())
						&& this.jiServ.isReadyForCalibration(ji)) {
					MultiStartCal mCal = new MultiStartCal(this.getDefaultProcedure(ji), this.getDefaultCalClass(ji),
						ji.getServiceType().getCalibrationType());
					MultiStartCalItem mCalItem = new MultiStartCalItem();
					mCalItem.setJi(ji);
					mCal.getItems().add(mCalItem);
					mCals.add(mCal);
				}
			}
		} else {
			for (JobItem ji : job.getItems()) {
				if (jobItemIds.contains(ji.getJobItemId())
						&& ((ji.getGroup() == null) || !ji.getGroup().isCalibrationGroup())
						&& this.jiServ.isReadyForCalibration(ji)) {
					MultiStartCal mCal = new MultiStartCal(this.getDefaultProcedure(ji), this.getDefaultCalClass(ji),
						ji.getServiceType().getCalibrationType());
					MultiStartCalItem mCalItem = new MultiStartCalItem();
					mCalItem.setJi(ji);
					mCal.getItems().add(mCalItem);
					mCals.add(mCal);
				}
			}
		}
		// create new form
		MultiStartCalibrationForm form = new MultiStartCalibrationForm();
		form.setCals(mCals);
		form.setJob(job);
		form.setJi(jItem);
		form.setCurrentContact(contact);
		return form;
	}

	public CalibrationClass getDefaultCalClass(JobItem ji) {
        if ((ji.getCalLinks() != null) && (ji.getCalLinks().size() > 0)) {
            for (CalLink cl : ji.getCalLinks()) {
                if (!cl.getCal().getStatus().getName().equalsIgnoreCase("cancelled")) {
                    return CalibrationClass.AS_LEFT;
                }
            }
        }

        return CalibrationClass.AS_FOUND;
    }

    public Capability getDefaultProcedure(JobItem ji) {
        if (ji.getNextWorkReq() != null && ji.getNextWorkReq().getWorkRequirement().getCapability() != null) {
            return ji.getNextWorkReq().getWorkRequirement().getCapability();
        }

        return null;
    }

    protected void onBind(MultiStartCalibrationForm form) {
        // has job item been passed?
        if (form.getJi() != null) {
            List<DefaultStandard> errorProcStds = new ArrayList<>();
            if (form.getProcStdsOnScreen() != null) {
                for (Integer i : form.getProcStdsOnScreen()) {
                    errorProcStds.add(this.defStandServ.findDefaultStandard(i));
                }
            }
            form.setErrorProcStds(errorProcStds);

            List<Instrument> errorAdditionalStds = new ArrayList<>();
            if (form.getAddStdsOnScreen() != null) {
                for (Integer i : form.getAddStdsOnScreen()) {
                    errorAdditionalStds.add(this.instrumServ.get(i));
				}
			}
			form.setErrorAdditionalStds(errorAdditionalStds);

			Set<Integer> set = new HashSet<>();
			if (form.getInstrums() != null) {
				set.addAll(Arrays.asList(form.getInstrums()));
			}
			set.remove(0);
			form.setStdIds(set);
			Integer[] stds = set.toArray(new Integer[0]);
			form.setInstrums(stds);
		}

		for (MultiStartCal mCal : form.getCals()) {
			// set ids to objects as part of binding process
			mCal.setCalProcess((mCal.getCalProcessId() == null) ? null : this.calProServ.get(mCal.getCalProcessId()));
			mCal.setCalType((mCal.getCalTypeId() == null) ? null : this.calTypeServ.find(mCal.getCalTypeId()));
			mCal.setCapability((mCal.getCapabilityId() == null) ? null : this.procServ.get(mCal.getCapabilityId()));
		}
	}

	@RequestMapping(value = "/multistart.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest request,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute("form") MultiStartCalibrationForm form, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors())
			return referenceData(form);
		onBind(form);
		Contact contact = this.userService.get(username).getCon();
		Subdiv subdiv = subdivService.get(subdivDto.getKey());
		for (MultiStartCal mCal : form.getCals()) {
			// check that this cal was selected
			if (mCal.isSelected()) {
                Calibration cal = new Calibration();
                cal.setOrganisation(subdiv);
                cal.setCalDate(mCal.getCalDate());
                cal.setCapability(mCal.getCapability());
                cal.setCalProcess(mCal.getCalProcess());
                cal.setCalType(mCal.getCalType());
                cal.setCalClass(mCal.getCalClass());
                if (form.getInstrums() != null) {
                    this.stdUsedServ.createStandardsUsed(form.getInstrums(), cal);
                }
                Set<CalLink> calLinks = new TreeSet<>(new CalibrationToCalLinkComparator());
                for (MultiStartCalItem mCalItem : mCal.getItems()) {
                    // reload item to prevent LIE
                    JobItem ji = this.jiServ.findJobItem(mCalItem.getJi().getJobItemId());
                    cal.setCalAddress(ji.getCalAddr());

					CalLink calLink = new CalLink();
					calLink.setJi(ji);
					calLink.setCal(cal);
					calLink.setMainItem(ji.getInst().getModel().getModelType().getModules());
					calLinks.add(calLink);
				}
				cal.setLinks(calLinks);

				this.calServ.insertAndStartCalibration(cal, contact, null, null);
			}
		}

		return new ModelAndView(new RedirectView("viewjob.htm?jobid=" + form.getJob().getJobid()));
	}

	@RequestMapping(value = "/multistart.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute("form") MultiStartCalibrationForm form) throws Exception {
		onBind(form);
		Map<String, Object> refData = new HashMap<>();

		refData.put("processes", this.calProServ.getAll());
		refData.put("classes", CalibrationClass.values());

		if ((form.getJi() != null) && (this.jiServ.isReadyForCalibration(form.getJi()))) {
            // pre-populate the defaults of the procedure
            Capability proc = null;
            if ((form.getJi().getNextWorkReq() != null)
                && (form.getJi().getNextWorkReq().getWorkRequirement().getCapability() != null)) {
                proc = form.getJi().getNextWorkReq().getWorkRequirement().getCapability();
            } else if (form.getJi().getCapability() != null) {
                proc = form.getJi().getCapability();
            }
            // pre-populate the defaults of the work instruction
            WorkInstruction wi = null;
            if ((form.getJi().getNextWorkReq() != null)
                && (form.getJi().getNextWorkReq().getWorkRequirement().getWorkInstruction() != null)) {
                wi = form.getJi().getNextWorkReq().getWorkRequirement().getWorkInstruction();
            } else if (form.getJi().getWorkInstruction() != null) {
                wi = form.getJi().getWorkInstruction();
            }

            if (proc != null) {
				refData.put("procstandards", this.defStandServ.getStandardsForProcedure(proc.getId(), null));
			} else {
                refData.put("procstandards", new ArrayList<CapabilityStandard>());
			}

			if (wi != null) {
				refData.put("wistandards", this.defStandServ.getStandardsForWorkInstruction(wi.getId(), null));
			} else {
				refData.put("wistandards", new ArrayList<WorkInstructionStandard>());
			}
		} else {
            refData.put("procstandards", new ArrayList<CapabilityStandard>());
            refData.put("wistandards", new ArrayList<WorkInstructionStandard>());
        }

		return new ModelAndView("trescal/core/jobs/calibration/multistart", refData);
	}
}