package org.trescal.cwms.core.jobs.certificate.entity;

public enum CertificateClass {

    CLASS_NONE("", 0),
    PERCENTAGE_CLASS_A("0.02%",1),
    PERCENTAGE_CLASS_B("0.025%",2),
    PERCENTAGE_CLASS_C("0.05%",3),
    PERCENTAGE_CLASS_D("0.1%",4),
    PERCENTAGE_CLASS_E("0.25%",5),
    PERCENTAGE_CLASS_F("0.6%",8),
    PERCENTAGE_CLASS_G("1%",9),
    PERCENTAGE_CLASS_H("1.6%",11),
    PERCENTAGE_CLASS_I("2.5%", 13),
    PERCENTAGE_CLASS_J("4%",14),
    PERCENTAGE_CLASS_K("0.5%",7),
    PERCENTAGE_CLASS_L("0.4%",6),
    PERCENTAGE_CLASS_M("1.5%",10),
    PERCENTAGE_CLASS_N("2%", 12);

    String description;
    Integer order;

    CertificateClass(String description, Integer order) {
        this.description = description;
        this.order = order;
    }

    public String getDescription() {
        return description;
    }

    public Integer getOrder() { return order; }


}
