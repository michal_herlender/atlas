package org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;

import javax.persistence.criteria.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity;
import org.trescal.cwms.core.external.adveso.entity.advesojobitemactivity.AdvesoJobItemActivity_;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting;
import org.trescal.cwms.core.external.adveso.entity.advesooverriddenactivitysetting.AdvesoOverriddenActivitySetting_;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity;
import org.trescal.cwms.core.external.adveso.entity.advesotransferableactivity.AdvesoTransferableActivity_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemActionForEmployeeDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity_;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemActionProjectionDTO;
import org.trescal.cwms.core.jobs.repair.dto.RepairTimeDTO;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault_;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication_;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity_;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus_;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink_;

@Repository("JobItemActionDao")
public class JobItemActionDaoImpl extends BaseDaoImpl<JobItemAction, Integer> implements JobItemActionDao {

    @Value("${cwms.advesointerface.decreasemaxactionidforsync}")
    private Integer decreaseMaxActionIdForSync;

    @Override
    protected Class<JobItemAction> getEntity() {
        return JobItemAction.class;
    }

    private List<JobItemActionForEmployeeDTO> find(Contact employee, LocalDate start, LocalDate end,
                                                   Function<CriteriaBuilder, Function<Path<StateGroupLink>, Expression<Boolean>>> stateGroupClause) {
        return getResultList(cb -> {
            CriteriaQuery<JobItemActionForEmployeeDTO> cq = cb.createQuery(JobItemActionForEmployeeDTO.class);
            Root<JobItemActivity> root = cq.from(JobItemActivity.class);
            Join<JobItemActivity, JobItem> jobItem = root.join(JobItemActivity_.jobItem);
            Join<JobItem, Job> job = jobItem.join(JobItem_.job);
            Join<JobItemActivity, ItemActivity> activity = root.join(JobItemActivity_.activity);
            Join<ItemActivity, StateGroupLink> stateGroupLink = activity.join(ItemActivity_.groupLinks, JoinType.LEFT);
            stateGroupLink.on(cb.equal(stateGroupLink.get(StateGroupLink_.groupId), StateGroup.TIME_RELATED.getId()));
            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.equal(root.get(JobItemActivity_.completedBy), employee));
            clauses.getExpressions()
                    .add(cb.between(root.get(JobItemActivity_.startStamp),
                            Date.from(start.atStartOfDay().toInstant(ZoneOffset.UTC)),
                            Date.from(end.atStartOfDay().toInstant(ZoneOffset.UTC))));
            clauses.getExpressions().add(stateGroupClause.apply(cb).apply(stateGroupLink));
            cq.where(clauses);
            cq.select(cb.construct(JobItemActionForEmployeeDTO.class, job.get(Job_.jobid), job.get(Job_.jobno),
                    jobItem.get(JobItem_.jobItemId), jobItem.get(JobItem_.itemNo),
                    root.get(JobItemActivity_.activityDesc), root.get(JobItemActivity_.remark),
                    root.get(JobItemActivity_.startStamp), root.get(JobItemActivity_.timeSpent)));
            return cq;
        });
    }

    @Override
    public List<JobItemAction> findForJobItemView(JobItem jobItem) {
        return getResultList(cb -> {
            CriteriaQuery<JobItemAction> cq = cb.createQuery(JobItemAction.class);
            Root<JobItemAction> action = cq.from(JobItemAction.class);

            cq.where(cb.equal(action.get(JobItemAction_.jobItem), jobItem));
            return cq;
        });
    }

    private Integer summarizeTimeSpent(Contact employee, LocalDate start, LocalDate end,
                                       Function<CriteriaBuilder, Function<Path<StateGroupLink>, Expression<Boolean>>> stateGroupClause) {
        Integer timeSpent = getSingleResult(cb -> {
            CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
            Root<JobItemActivity> root = cq.from(JobItemActivity.class);
            Join<JobItemActivity, ItemActivity> activity = root.join(JobItemActivity_.activity);
            Join<ItemActivity, StateGroupLink> stateGroupLink = activity.join(ItemActivity_.groupLinks, JoinType.LEFT);
            stateGroupLink.on(cb.equal(stateGroupLink.get(StateGroupLink_.groupId), StateGroup.TIME_RELATED.getId()));
            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.equal(root.get(JobItemActivity_.completedBy), employee));
            clauses.getExpressions()
                    .add(cb.between(root.get(JobItemActivity_.startStamp),
                            Date.from(start.atStartOfDay().toInstant(ZoneOffset.UTC)),
                            Date.from(end.atStartOfDay().toInstant(ZoneOffset.UTC))));
            clauses.getExpressions().add(stateGroupClause.apply(cb).apply(stateGroupLink));
            cq.where(clauses);
            cq.select(cb.sum(root.get(JobItemActivity_.timeSpent)));
            return cq;
        });
        // empty sum in SQL isn't 0, but NULL!!!
        return timeSpent == null ? 0 : timeSpent;
    }

    private Expression<Boolean> isTimeRelated(CriteriaBuilder cb, Path<StateGroupLink> sgl) {
        return cb.equal(sgl.get(StateGroupLink_.groupId), StateGroup.TIME_RELATED.getId());
    }

    private Expression<Boolean> isNotTimeRelated(CriteriaBuilder cb, Path<StateGroupLink> sgl) {
        return cb.isNull(sgl.get(StateGroupLink_.groupId));
    }

    @Override
    public List<JobItemActionForEmployeeDTO> findTimeRelated(Contact employee, LocalDate start, LocalDate end) {
        return find(employee, start, end, cb -> sgl -> isTimeRelated(cb, sgl));
    }

    @Override
    public List<JobItemActionForEmployeeDTO> findNotTimeRelated(Contact employee, LocalDate start, LocalDate end) {
        return find(employee, start, end, cb -> sgl -> isNotTimeRelated(cb, sgl));
    }

    @Override
    public List<JobItemActionProjectionDTO> getProjectionDTOsForJobItem(Integer jobItemId, Boolean deleted, Locale locale) {
        return getResultList(cb -> {
            CriteriaQuery<JobItemActionProjectionDTO> cq = cb.createQuery(JobItemActionProjectionDTO.class);
            Root<JobItemAction> root = cq.from(JobItemAction.class);
            Join<JobItemAction, JobItem> jobItem = root.join(JobItemAction_.jobItem, JoinType.INNER);
            Join<JobItemAction, ItemActivity> activity = root.join(JobItemAction_.activity, JoinType.LEFT);
            Join<ItemActivity, Translation> activityTranslation = activity.join(ItemActivity_.translations, JoinType.LEFT);
            activityTranslation.on(cb.equal(activityTranslation.get(Translation_.locale), locale));
            Join<JobItemAction, Contact> startedBy = root.join(JobItemAction_.startedBy, JoinType.LEFT);
            Join<JobItemAction, ItemStatus> endStatus = root.join(JobItemAction_.endStatus, JoinType.LEFT);
            Join<ItemStatus, Translation> endStatusTranslation = endStatus.join(ItemStatus_.translations, JoinType.LEFT);
            endStatusTranslation.on(cb.equal(endStatusTranslation.get(Translation_.locale), locale));

            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.jobItemId), jobItemId));
            if (deleted != null)
                clauses.getExpressions().add(cb.equal(root.get(JobItemAction_.deleted), deleted));

            cq.where(clauses);
            cq.select(cb.construct(JobItemActionProjectionDTO.class,
                    root.get(JobItemAction_.id), jobItem.get(JobItem_.jobItemId), activity.get(ItemActivity_.stateid),
                    activityTranslation.get(Translation_.translation), startedBy.get(Contact_.personid), startedBy.get(Contact_.firstName),
                    startedBy.get(Contact_.lastName), endStatus.get(ItemStatus_.stateid), endStatusTranslation.get(Translation_.translation),
                    root.get(JobItemAction_.deleted), root.get(JobItemAction_.timeSpent), root.get(JobItemAction_.startStamp),
                    root.get(JobItemAction_.endStamp), root.get(JobItemAction_.remark)
            ));

            cq.orderBy(cb.asc(root.get(JobItemAction_.id)));
            return cq;
        });
    }

    @Override
    public Integer summarizeTimeRelated(Contact employee, LocalDate start, LocalDate end) {
        return summarizeTimeSpent(employee, start, end, cb -> sgl -> isTimeRelated(cb, sgl));
    }

    @Override
    public Integer summarizeNotTimeRelated(Contact employee, LocalDate start, LocalDate end) {
        return summarizeTimeSpent(employee, start, end, cb -> sgl -> isNotTimeRelated(cb, sgl));
    }

    @Override
    public List<RepairTimeDTO> findTechnicianRepairTimeByRcr(Integer jiId, String rcrIdentifier, Contact contact) {
        return getResultList(cb -> {
            CriteriaQuery<RepairTimeDTO> cq = cb.createQuery(RepairTimeDTO.class);
            Root<JobItemAction> root = cq.from(JobItemAction.class);
            Join<JobItemAction, JobItem> jiJoin = root.join(JobItemAction_.jobItem);
            Join<JobItemAction, Contact> contactJoin = root.join(JobItemAction_.startedBy);

            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.equal(jiJoin.get(JobItem_.jobItemId), jiId));
            clauses.getExpressions().add(cb.like(root.get(JobItemAction_.remark), rcrIdentifier.concat("%")));
            clauses.getExpressions().add(cb.isFalse(root.get(JobItemAction_.deleted)));
            if (contact != null)
                clauses.getExpressions().add(cb.equal(contactJoin.get(Contact_.personid), contact.getPersonid()));
            cq.where(clauses);
            cq.orderBy(
                    cb.desc(contactJoin.get(Contact_.personid)),
                    cb.asc(root.get(JobItemAction_.remark)),
                    cb.desc(root.get(JobItemAction_.startStamp))
            );

            cq.select(cb.construct(RepairTimeDTO.class, root.get(JobItemAction_.id),
                    root.get(JobItemAction_.timeSpent),
                    root.get(JobItemAction_.startStamp),
                    root.get(JobItemAction_.remark),
                    contactJoin.get(Contact_.personid),
                    contactJoin.get(Contact_.firstName),
                    contactJoin.get(Contact_.lastName)
            ));

            return cq;
        });
    }

    @Override
    public List<Integer> findNotProcessedActionsIdForAdveso() {
        return getResultList(cb -> {
            CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
            Root<JobItemAction> root = cq.from(JobItemAction.class);
            Join<JobItemAction, JobItem> jobItemJoin = root.join(JobItemAction_.jobItem);
            Join<JobItem, Job> jobJoin = jobItemJoin.join(JobItem_.job);
            Join<Job, Contact> contactJoin = jobJoin.join(Job_.con);
            Join<Contact, Subdiv> subdivJoin = contactJoin.join(Contact_.sub);
            Join<Subdiv, SystemDefaultApplication> systemDefaultAppJoin = subdivJoin.join(Subdiv_.systemDefaults);
            Join<SystemDefaultApplication, SystemDefault> systemDefaultJoin = systemDefaultAppJoin.join(SystemDefaultApplication_.systemDefault);
            Join<JobItemAction, ItemActivity> activityJoin = root.join(JobItemAction_.activity);

            Subquery<Integer> lastActionIdSubQuery = cq.subquery(Integer.class);
            Root<AdvesoJobItemActivity> sqLastActionId = lastActionIdSubQuery.from(AdvesoJobItemActivity.class);
            Join<AdvesoJobItemActivity, JobItemAction> sqJobItemActionJoin = sqLastActionId.join(AdvesoJobItemActivity_.jobItemAction);
            lastActionIdSubQuery.select(cb.coalesce(cb.max(sqJobItemActionJoin.get(JobItemAction_.id)), cb.literal(0)));

            Subquery<Integer> configuredActivitySubQuery = cq.subquery(Integer.class);
            Root<AdvesoTransferableActivity> sqTransferableActivity = configuredActivitySubQuery.from(AdvesoTransferableActivity.class);
            Join<AdvesoTransferableActivity, AdvesoOverriddenActivitySetting> overriddenActivityJoin = sqTransferableActivity.join(AdvesoTransferableActivity_.advesoOverriddenActivitiesSetting, JoinType.LEFT);
            overriddenActivityJoin.on(cb.equal(overriddenActivityJoin.get(AdvesoOverriddenActivitySetting_.subdiv), subdivJoin));
            Join<AdvesoTransferableActivity, ItemActivity> itemActivityJoin = sqTransferableActivity.join(AdvesoTransferableActivity_.itemActivity);
            Predicate configuredActivitySubQueryClauses = cb.disjunction();
            configuredActivitySubQueryClauses.getExpressions().add(cb.and(cb.isNull(overriddenActivityJoin), cb.isTrue(sqTransferableActivity.get(AdvesoTransferableActivity_.isTransferredToAdveso))));
            configuredActivitySubQueryClauses.getExpressions().add(cb.and(cb.isNotNull(overriddenActivityJoin), cb.isTrue(overriddenActivityJoin.get(AdvesoOverriddenActivitySetting_.isTransferredToAdveso))));
            configuredActivitySubQuery.where(configuredActivitySubQueryClauses);
            configuredActivitySubQuery.select(itemActivityJoin.get(ItemActivity_.stateid));

            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.greaterThan(cb.sum(root.get(JobItemAction_.id), decreaseMaxActionIdForSync), lastActionIdSubQuery.getSelection()));
            clauses.getExpressions().add(cb.equal(systemDefaultJoin.get(SystemDefault_.id), SystemDefaultNames.LINK_TO_ADVESO));
            clauses.getExpressions().add(cb.equal(systemDefaultAppJoin.get(SystemDefaultApplication_.value), "true"));
            clauses.getExpressions().add(activityJoin.get(ItemActivity_.stateid).in(configuredActivitySubQuery.getSelection()));
            clauses.getExpressions().add(cb.isEmpty(root.get(JobItemAction_.advesoJobItemActivities)));
            cq.where(clauses);
            cq.select(root.get(JobItemAction_.id));
            return cq;
        });
    }
}