package org.trescal.cwms.core.jobs.repair.dto.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.repair.dto.RepairTimeDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairTimeForm;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class RepairTimeValidator extends AbstractBeanValidator {

	
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(RepairTimeForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		RepairTimeForm form = (RepairTimeForm) target;
		super.validate(target, errors);

		if(form.getAction().equals("EDIT")) {
			int i = 0;
			for (RepairTimeDTO dto : form.getRepairTimes()) {
				if(dto.getStartDate() != null && ((dto.getTimespentHour() == null && dto.getTimespentMinute() == null)
						|| (dto.getTimespentHour() == 0 && dto.getTimespentMinute() == 0)
						|| dto.getTimespentHour() < 0 || dto.getTimespentMinute() < 0 
						|| dto.getTimespentHour() > 99 || dto.getTimespentMinute() > 59))
					errors.rejectValue("repairTimes["+i+"].timespentHour", "repairtime.validator.timespent", "Time spent must be : in hours between 0 and 99, in minutes between 0 and 59");
				
				if (dto.getStartDate() == null && ((dto.getTimespentHour() >= 0 && dto.getTimespentMinute() > 0) ||
						(dto.getTimespentHour() > 0 && dto.getTimespentMinute() >= 0)) && 
						dto.getTimespentHour() <= 99 && dto.getTimespentMinute() <= 59)
					errors.rejectValue("repairTimes["+i+"].startDate", "repairtime.validator.startdate", "The start date is mandatory");
				i++;
			}
			
		}
	}

}
