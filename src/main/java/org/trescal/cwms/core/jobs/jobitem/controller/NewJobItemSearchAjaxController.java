package org.trescal.cwms.core.jobs.jobitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.instrument.dto.InstrumentSearchedForNewJobItemSearchDto;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelForNewJobItemSearchDto;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.db.JobQuoteLinkService;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchByLinkedQuotationForm;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchInstrumentForm;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.val;

@RestController
@RequestMapping("jobitem")
public class NewJobItemSearchAjaxController {

    @Autowired private InstrumService instrumService;
    @Autowired private InstrumentModelService instrumentModelService;

    @Autowired private JobQuoteLinkService jobQuoteLinkService;


    @GetMapping("newJobItemSearchMatchingModels.json")
    PagedResultSet<InstrumentModelForNewJobItemSearchDto> matchingModels(NewJobItemSearchInstrumentForm form){
        return instrumentModelService.searchAllMatchingInstrumentModelsForNewJobItem(form);
    }
    @GetMapping("newJobItemSearchCompanyModels.json")
    PagedResultSet<InstrumentModelForNewJobItemSearchDto> companyModels(NewJobItemSearchInstrumentForm form){
        return instrumentModelService.searchInstrumentModelsForNewJobItem(form);
    }

    @GetMapping("newJobItemSearchInstruments.json")
    PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> newJobItemSearchInstruments(NewJobItemSearchInstrumentForm form){
        return instrumService.searchInstrumentsForNewJobItem(form);
    }

    @GetMapping("newJobItemSearch.json")
    NewJobItemSearchOut newJobItemSearch(NewJobItemSearchInstrumentForm form){
        val instruments = instrumService.searchInstrumentsForNewJobItem(form);
        val companyModels = instrumentModelService.searchInstrumentModelsForNewJobItem(form);
        val allMatchingModels = instrumentModelService.searchAllMatchingInstrumentModelsForNewJobItem(form);

        return NewJobItemSearchOut.builder()
                    .instruments(instruments)
                    .companyModels(companyModels)
                    .matchingModels(allMatchingModels)
                .build();
    }

    @GetMapping(value = "quotedInstruments.json", params = {"action=Show_Instruments"})
    PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> quotedInstruments(NewJobItemSearchByLinkedQuotationForm form){
        return instrumService.findInstrumentsFromJobQuotes(form);
    }


    @GetMapping(value = "quotedInstruments.json", params = {"action=Show_Similar_Models" })
    PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> quotedInstrumentsForShowSimilarModels(NewJobItemSearchByLinkedQuotationForm form){
        val modelIds = jobQuoteLinkService.getModelIdsFromJobQuotes(form.getJobId(),form.getJobQuoteLinkId(),true);
        return instrumService.searchCompanyInstrumentsFromModelIds(form,modelIds);
    }

    @GetMapping(value = "quotedInstruments.json", params = {"action=Show_Models" })
    PagedResultSet<InstrumentSearchedForNewJobItemSearchDto> quotedInstrumentsForShowModels(NewJobItemSearchByLinkedQuotationForm form){
        val modelIds = jobQuoteLinkService.getModelIdsFromJobQuotes(form.getJobId(),form.getJobQuoteLinkId(),false);
        return instrumService.searchCompanyInstrumentsFromModelIds(form,modelIds);
    }


    @GetMapping(value = "quotedMatchingModels.json", params = {"action=Show_Similar_Models"})
    PagedResultSet<InstrumentModelForNewJobItemSearchDto> quotedMatchingModelsForShowSimilarModels(NewJobItemSearchByLinkedQuotationForm form){
        val modelIds = jobQuoteLinkService.getModelIdsFromJobQuotes(form.getJobId(),form.getJobQuoteLinkId(),true);
        return instrumentModelService.findModelsByIdsForNewJobItemSearch(form,modelIds);
    }

    @GetMapping(value = "quotedMatchingModels.json", params = {"action=Show_Models"})
    PagedResultSet<InstrumentModelForNewJobItemSearchDto> quotedMatchingModelsForShowModels(NewJobItemSearchByLinkedQuotationForm form){
        val modelIds = jobQuoteLinkService.getModelIdsFromJobQuotes(form.getJobId(),form.getJobQuoteLinkId(),false);
        return instrumentModelService.findModelsByIdsForNewJobItemSearch(form,modelIds);
    }

    @GetMapping(value = "quotedCompanyModels.json", params = {"action=Show_Similar_Models"})
    PagedResultSet<InstrumentModelForNewJobItemSearchDto> quotedCompanyModelsForShowSimilarModels(NewJobItemSearchByLinkedQuotationForm form){
        val modelIds = jobQuoteLinkService.getModelIdsFromJobQuotes(form.getJobId(),form.getJobQuoteLinkId(),true);
        return instrumentModelService.findCompanyModelsByIdsForNewJobIntemSearch(form,modelIds);
    }

    @GetMapping(value = "quotedCompanyModels.json", params = {"action=Show_Models"})
    PagedResultSet<InstrumentModelForNewJobItemSearchDto> quotedCompanyModelsForShowModels(NewJobItemSearchByLinkedQuotationForm form){
        val modelIds = jobQuoteLinkService.getModelIdsFromJobQuotes(form.getJobId(),form.getJobQuoteLinkId(),false);
        return instrumentModelService.findCompanyModelsByIdsForNewJobIntemSearch(form,modelIds);
    }


    @GetMapping(value = "quotedAll.json",params = {"action=Show_Models" } )
    NewJobItemSearchOut quotedAllForShowModels(NewJobItemSearchByLinkedQuotationForm form){
        return quotedInstrumentsShowModelsOrSimilarModles(form,false);
    }


    @GetMapping(value = "quotedAll.json",params = {"action=Show_Similar_Models" } )
    NewJobItemSearchOut quotedAllForShowSimilarModels(NewJobItemSearchByLinkedQuotationForm form){
        return quotedInstrumentsShowModelsOrSimilarModles(form,true);
    }

    @GetMapping(value = "quotedAll.json", params = {"action=Show_Instruments"})
    NewJobItemSearchOut quotedInstrumentsAll(NewJobItemSearchByLinkedQuotationForm form){
        return NewJobItemSearchOut.builder().instruments(instrumService.findInstrumentsFromJobQuotes(form))
                .build();
    }

    private  NewJobItemSearchOut quotedInstrumentsShowModelsOrSimilarModles(NewJobItemSearchByLinkedQuotationForm form, Boolean similar) {

        val modelIds = jobQuoteLinkService.getModelIdsFromJobQuotes(form.getJobId(),form.getJobQuoteLinkId(),similar);
        val instruments = instrumService.searchCompanyInstrumentsFromModelIds(form,modelIds);
        val models = instrumentModelService.findModelsByIdsForNewJobItemSearch(form,modelIds);
        val companyModels = instrumentModelService.findCompanyModelsByIdsForNewJobIntemSearch(form,modelIds);

        return NewJobItemSearchOut.builder()
                .instruments(instruments)
                .companyModels(companyModels)
                .matchingModels(models)
                .build();
    }

    @GetMapping(value = {"quotedInstruments.json", "quotedAll.json", "quotedCompanyModels.json", "quotedMatchingModels"})
    String quotedDefault(){
        return "Action not supported";
    }
}


