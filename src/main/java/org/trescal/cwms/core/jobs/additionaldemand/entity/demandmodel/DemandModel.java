package org.trescal.cwms.core.jobs.additionaldemand.entity.demandmodel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemand;

@Entity
@Table(name = "demandmodel")
public class DemandModel {

	private Integer id;
	private AdditionalDemand demand;
	private InstrumentModel model;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AdditionalDemand getDemand() {
		return demand;
	}

	public void setDemand(AdditionalDemand demand) {
		this.demand = demand;
	}

	@ManyToOne
	public InstrumentModel getModel() {
		return model;
	}

	public void setModel(InstrumentModel model) {
		this.model = model;
	}
}