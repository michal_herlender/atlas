package org.trescal.cwms.core.jobs.job.entity.jobinstructionlink;

import java.util.Comparator;

public class CustomOrderComparator implements Comparator<String>
{
	static final int CMP = 4;
	static final int CON = 2;
	static final int JOB = 1;
	static final int SUB = 3;

	public int compare(String str1, String str2)
	{
		Integer one = 0;
		Integer two = 0;

		if (str1.equals("Job") || str2.equals("Job"))
		{
			if (str1.equals("Job"))
			{
				one = JOB;
			}
			if (str2.equals("Job"))
			{
				two = JOB;
			}
		}
		if (str1.equals("Contact") || str2.equals("Contact"))
		{
			if (str1.equals("Contact"))
			{
				one = CON;
			}
			if (str2.equals("Contact"))
			{
				two = CON;
			}
		}
		if (str1.equals("Subdivision") || str2.equals("Subdivision"))
		{
			if (str1.equals("Subdivision"))
			{
				one = SUB;
			}
			if (str2.equals("Subdivision"))
			{
				two = SUB;
			}
		}
		if (str1.equals("Company") || str2.equals("Company"))
		{
			if (str1.equals("Company"))
			{
				one = CMP;
			}
			if (str2.equals("Company"))
			{
				two = CMP;
			}
		}

		return one.compareTo(two);
	}

}
