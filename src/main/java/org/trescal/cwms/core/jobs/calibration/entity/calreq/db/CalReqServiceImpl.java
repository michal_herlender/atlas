package org.trescal.cwms.core.jobs.calibration.entity.calreq.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CompanyModelCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.InstrumentCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.InstrumentCalReqDto;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.JobItemCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.ModelCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.CalReqFallback;
import org.trescal.cwms.core.jobs.calibration.entity.calreqhistory.db.CalReqHistoryService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.system.Constants;

@Service("CalReqService")
public class CalReqServiceImpl implements CalReqService
{
	@Autowired
	private CalReqDao calReqDao;
	@Autowired
	private CalReqHistoryService calReqHistoryServ;
	@Autowired
	private JobService jobServ;
	@Autowired
	private UserService userService;
	@Autowired
	private MessageSource messageSource;

	@Override
	public ResultWrapper ajaxDeactivateCalReqs(int id, String typeCode, HttpSession session)
	{
		Class<? extends CalReq> clazz = this.getClassFromString(typeCode);

		if (clazz != null)
		{
			CalReq cr = this.findCalReq(id);

			if (cr != null)
			{
				this.deactivateCalReqs(cr, session);

				CalReq returnreq = null;
				if (cr instanceof InstrumentCalReq)
				{
					returnreq = this.findCalReqsForInstrument(((InstrumentCalReq) cr).getInst());
				}
				else if (cr instanceof JobItemCalReq)
				{
					returnreq = this.findCalReqsForJobItem(((JobItemCalReq) cr).getJobItem());
				}
				if(returnreq instanceof InstrumentCalReq) {
					returnreq.setTranslatedSource(messageSource.getMessage(returnreq.getSource(), new Object[]{((InstrumentCalReq) returnreq).getInst().getPlantid()}, LocaleContextHolder.getLocale()));
				}
				this.logFallbackInHistory(cr, returnreq, session);
				return new ResultWrapper(true, "", returnreq, null);
			}
			else
			{
				return new ResultWrapper(false, "Could not find the calibration requirements");
			}
		}
		else
		{
			return new ResultWrapper(false, "Could not interpret type string");
		}
	}

	@Override
	public CalReq ajaxFindCalReq(int id)
	{
		return this.findCalReq(id);
	}
	

	@Override
	public void deactivateCalReqs(CalReq cr, HttpSession session)
	{
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		// No result wrapper so eager load not needed
		Contact contact = this.userService.get(username).getCon();
		cr.setDeactivatedBy(contact);
		cr.setDeactivatedOn(new Date());
		cr.setActive(false);

		this.updateCalReq(cr);
	}

	@Override
	public void deleteAllCalReqsImmediatelyAffectingJobItem(int jobItemId)
	{
		// first delete all override/fallback history lines for item
		this.calReqHistoryServ.deleteCalReqHistoryImmediatelyAffectingJobItem(jobItemId);

		// then find all cal reqs ever to affect item (active and inactive!)
		for (JobItemCalReq jicr : this.findAllCalReqsImmediatelyAffectingJobItem(jobItemId))
		{
			// and destroy each one
			this.deleteCalReq(jicr);
		}
	}

	@Override
	public void deleteCalReq(CalReq calreq)
	{
		this.calReqDao.remove(calreq);
	}

	@Override
	public List<JobItemCalReq> findAllCalReqsImmediatelyAffectingJobItem(int jobItemId)
	{
		return this.calReqDao.findAllCalReqsImmediatelyAffectingJobItem(jobItemId);
	}

	@Override
	public CalReq findCalReq(int id)
	{
		return this.calReqDao.find(id);
	}

	@Override
	public Map<Integer, CalReq> findCalReqsForAllItemsOnJob(int jobId)
	{
		Job job = this.jobServ.get(jobId);

		List<Integer> jobItemIds = new ArrayList<Integer>();
		Map<Integer, Integer> instToJobItemMap = new HashMap<Integer, Integer>();
		Map<Integer, List<Integer>> modelToJobItemMap = new HashMap<Integer, List<Integer>>();
		Map<Integer, List<Integer>> compModelMap = new HashMap<Integer, List<Integer>>();

		for (JobItem ji : job.getItems())
		{
			int jobItemId = ji.getJobItemId();
			int compId = ji.getInst().getComp().getCoid();
			Instrument inst = ji.getInst();
			int modelId = inst.getModel().getModelid();

			jobItemIds.add(jobItemId);
			instToJobItemMap.put(inst.getPlantid(), jobItemId);

			List<Integer> items = (modelToJobItemMap.get(modelId) != null) ? modelToJobItemMap.get(modelId) : new ArrayList<Integer>();
			items.add(jobItemId);
			modelToJobItemMap.put(modelId, items);

			List<Integer> mods = (compModelMap.get(compId) != null) ? compModelMap.get(compId) : new ArrayList<Integer>();
			mods.add(inst.getModel().getModelid());
			compModelMap.put(compId, mods);
		}

		Map<Integer, CalReq> map = new HashMap<Integer, CalReq>();
		if ((jobItemIds != null) && (jobItemIds.size() > 0))
		{
			map = this.findCalReqsForMultipleJobItems(jobItemIds);
			map = this.findCalReqsForMultipleInstruments(map, instToJobItemMap);
			map = this.findCalReqsForMultipleCompaniesAndModels(map, modelToJobItemMap, compModelMap);
			map = this.findCalReqsForMultipleJobItemModels(map, modelToJobItemMap);
		}

		return map;
	}

	/**
	 * Original note from Antech developers: 
	 * We originally had this as looping over the quote items and calling
	 * calReqServ.findCalReqsForQuotationItem(qi) on each item, but this
	 * causes a LOT of slow down for large quotes (100+ items). Now uses a
	 * method which only makes two database calls (albeit larger queries) no
	 * matter how many items are on the quote. Note: These are now mapped
	 * against model IDs instead of quote item IDs.
	 * 
	 * Note GB 2016-02-2017 - chaged to take set of actual quotation items being used
	 * - faster performance for paged quotations
	 * - still has problems with large quotations with lots of models
	 * - will possibly split between "whole quotation" query (for doc) and selected items 
	 */
	@Override
	public Map<Integer, CalReq> findCalReqsForQuotationItems(Quotation quotation, Collection<Quotationitem> qitems)
	{
		int compId = quotation.getContact().getSub().getComp().getCoid();
		
		List<Integer> quoteItemIds = new ArrayList<Integer>();
		// Map of instrument plantid to quoteitem id
		Map<Integer, Integer> instToQuoteItemMap = new HashMap<Integer, Integer>();
		// Map of model id to list of quoteitemids
		Map<Integer, List<Integer>> modelToQuoteItemMap = new HashMap<Integer, List<Integer>>();
		// Map of company ids to model ids, but with single comp id 
		Map<Integer, List<Integer>> compModelMap = new HashMap<Integer, List<Integer>>();

		for (Quotationitem qi : qitems)
		{
			int quoteItemId = qi.getId();
			Integer plantId = 0;
			Integer modelId = 0;
			// instrument quote item?
			if(qi.getInst() != null)
			{
				plantId = qi.getInst().getPlantid();
				modelId = qi.getInst().getModel().getModelid();
			}
			else if(qi.getModel() != null)
			{
				modelId = qi.getModel().getModelid();
			}
			quoteItemIds.add(quoteItemId);
			instToQuoteItemMap.put(plantId, quoteItemId);

			List<Integer> items = (modelToQuoteItemMap.get(modelId) != null) ? modelToQuoteItemMap.get(modelId) : new ArrayList<Integer>();
			items.add(quoteItemId);
			modelToQuoteItemMap.put(modelId, items);

			List<Integer> mods = (compModelMap.get(compId) != null) ? compModelMap.get(compId) : new ArrayList<Integer>();
			mods.add(modelId);
			compModelMap.put(compId, mods);
		}

		Map<Integer, CalReq> map = new HashMap<Integer, CalReq>();
		if ((quoteItemIds != null) && (quoteItemIds.size() > 0))
		{
			map = this.findCalReqsForMultipleInstruments(map, instToQuoteItemMap);
			map = this.findCalReqsForMultipleCompaniesAndModels(map, modelToQuoteItemMap, compModelMap);
			map = this.findCalReqsForMultipleJobItemModels(map, modelToQuoteItemMap);
		}

		return map;
	}

	@Override
	public CalReq findCalReqsForInstrument(Instrument i)
	{
		// try to find reqs for specific instrument
		CalReq cr = this.calReqDao.findCalReqsForInstrument(i.getPlantid());
		if (cr == null)
		{
			InstrumentModel model = i.getModel();
			// try to find reqs for this company and model
			cr = this.calReqDao.findCalReqsForCompanyAndModel(i.getComp().getCoid(), model.getModelid());

			if (cr == null)
			{
				// try to find default reqs for this model
				return this.calReqDao.findCalReqsForModel(model.getModelid());
			}
		}
		else {
			
		}
		return cr;
	}

	@Override
	public CalReq findCalReqsForJobItem(JobItem ji)
	{
		// try to find reqs for this specific job item
		CalReq cr = this.calReqDao.findCalReqsForJobItem(ji.getJobItemId());

		// otherwise just delegate to instrument reqs
		return (cr != null) ? cr : this.findCalReqsForInstrument(ji.getInst());
	}

	private Map<Integer, CalReq> findCalReqsForMultipleCompaniesAndModels(Map<Integer, CalReq> map, Map<Integer, List<Integer>> modelToJobItemMap, Map<Integer, List<Integer>> compModelMap)
	{
		return this.calReqDao.findCalReqsForMultipleCompaniesAndModels(map, modelToJobItemMap, compModelMap);
	}

	private Map<Integer, CalReq> findCalReqsForMultipleInstruments(Map<Integer, CalReq> map, Map<Integer, Integer> instToJobItemMap)
	{
		return this.calReqDao.findCalReqsForMultipleInstruments(map, instToJobItemMap);
	}

	private Map<Integer, CalReq> findCalReqsForMultipleJobItemModels(Map<Integer, CalReq> map, Map<Integer, List<Integer>> modelToJobItemMap)
	{
		return this.calReqDao.findCalReqsForMultipleJobItemModels(map, modelToJobItemMap);
	}

	private Map<Integer, CalReq> findCalReqsForMultipleJobItems(List<Integer> jobItemIds)
	{
		return this.calReqDao.findCalReqsForMultipleJobItems(jobItemIds);
	}

	@Override
	public CalReq findCalReqsForQuotationItem(Quotationitem qi)
	{
		// initialise calibration requirement
		CalReq cr = null;
		// does quote item have instrument?
		if (qi.getInst() != null)
		{
			// find calibration requirements for instrument?
			cr = this.findCalReqsForInstrument(qi.getInst());
		}
		else
		{
			int compId = qi.getQuotation().getContact().getSub().getComp().getCoid();
			int modelId = qi.getModel().getModelid();

			// try to find reqs for company model
			cr = this.calReqDao.findCalReqsForCompanyAndModel(compId, modelId);

			if (cr == null)
			{
				// try to find default reqs for this model
				cr = this.calReqDao.findCalReqsForModel(modelId);
			}
		}

		return cr;
	}

	@Override
	public List<CalReq> getAllCalReqs()
	{
		return this.calReqDao.findAll();
	}

	@Override
	public Class<? extends CalReq> getClassFromString(String code)
	{
		if ((code == null) || code.trim().isEmpty())
		{
			return null;
		}

		Map<String, Class<? extends CalReq>> map = this.getClassMap();

		if (map.containsKey(code))
		{
			return map.get(code);
		}
		else
		{
			return null;
		}
	}

	private Map<String, Class<? extends CalReq>> getClassMap()
	{
		// it would be nice to be able to iterate over all classes that extend
		// CalReq here and use an overridden method in each to populate this
		// map, but this is quite difficult to do in java and would be overkill
		// for our requirements, so we will manually add each child class

		Map<String, Class<? extends CalReq>> map = new HashMap<String, Class<? extends CalReq>>();
		map.put(Constants.CALREQ_MAP_JOBITEM, JobItemCalReq.class);
		map.put(Constants.CALREQ_MAP_INSTRUMENT, InstrumentCalReq.class);
		map.put(Constants.CALREQ_MAP_COMPANYMODEL, CompanyModelCalReq.class);
		map.put(Constants.CALREQ_MAP_MODEL, ModelCalReq.class);
		return map;
	}

	@Override
	public String getStringFromClass(Class<? extends CalReq> clazz)
	{
		if (clazz == null)
		{
			return null;
		}

		Map<String, Class<? extends CalReq>> map = this.getClassMap();

		if (map.containsValue(clazz))
		{
			for (Entry<String, Class<? extends CalReq>> entry : map.entrySet())
			{
				if (entry.getValue().equals(clazz))
				{
					return entry.getKey();
				}
			}
		}

		return null;
	}

	public void insertCalReq(CalReq CalReq)
	{
		this.calReqDao.persist(CalReq);
	}

	public void logFallbackInHistory(CalReq deactivated, CalReq fallbackTo, HttpSession session)
	{
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		// No result wrapper so eager load not needed
		Contact contact = this.userService.get(username).getCon();

		CalReqFallback f = new CalReqFallback();
		f.setChangeBy(contact);
		f.setChangeOn(new Date());
		f.setNewCalReq(fallbackTo);
		f.setOldCalReq(deactivated);
		f.setFallbackFromInst((deactivated instanceof InstrumentCalReq) ? ((InstrumentCalReq) deactivated).getInst() : null);
		f.setFallbackFromJobItem((deactivated instanceof JobItemCalReq) ? ((JobItemCalReq) deactivated).getJobItem() : null);

		this.calReqHistoryServ.insertCalReqHistory(f);
	}

	public void updateCalReq(CalReq CalReq)
	{
		this.calReqDao.update(CalReq);
	}

	@Override
	public InstrumentCalReq findInstrumentCalReqForInstrument(Instrument instrument) {
		return this.calReqDao.findCalReqsForInstrument(instrument.getPlantid());
	}

	@Override
	public InstrumentCalReq findInstrumentCalReqForInstrument(Integer plantId) {
		return this.calReqDao.findCalReqsForInstrument(plantId);
	}
	
	@Override
	public Map<Integer, InstrumentCalReqDto> findInstrumentCalReqsForPlantIds(List<Integer> plantids) {
		return this.calReqDao.findInstrumentCalReqsForPlantIds(plantids);
	}
}