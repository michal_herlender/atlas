package org.trescal.cwms.core.jobs.calibration.entity.calreqhistory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

@Entity
@DiscriminatorValue("fallback")
public class CalReqFallback extends CalReqHistory
{
	private Instrument fallbackFromInst;
	private JobItem fallbackFromJobItem;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fallfrominst")
	public Instrument getFallbackFromInst()
	{
		return this.fallbackFromInst;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fallfromji")
	public JobItem getFallbackFromJobItem()
	{
		return this.fallbackFromJobItem;
	}

	public void setFallbackFromInst(Instrument fallbackFromInst)
	{
		this.fallbackFromInst = fallbackFromInst;
	}

	public void setFallbackFromJobItem(JobItem fallbackFromJobItem)
	{
		this.fallbackFromJobItem = fallbackFromJobItem;
	}
}
