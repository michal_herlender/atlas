package org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.deliverynote.form.TPInstructionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction.TPInstruction;
import org.trescal.cwms.core.jobs.jobitem.entity.tpinstruction.TPInstruction_;

@Repository("TPInstructionDao")
public class TPInstructionDaoImpl extends BaseDaoImpl<TPInstruction, Integer> implements TPInstructionDao
{
	@Override
	protected Class<TPInstruction> getEntity() {
		return TPInstruction.class;
	} 
	
	@Override
	public List<TPInstructionDTO> getAllByJob(Integer jobId) {
		return getResultList(cb -> {
			CriteriaQuery<TPInstructionDTO> cq=cb.createQuery(TPInstructionDTO.class);
			Root<TPInstruction> tpInstruction= cq.from(TPInstruction.class);
			Join<TPInstruction, JobItem> jobItem=tpInstruction.join(TPInstruction_.ji);
			cq.where(cb.equal(jobItem.get(JobItem_.job), jobId));
			cq.select(cb.construct(TPInstructionDTO.class, tpInstruction.get(TPInstruction_.id),
					jobItem.get(JobItem_.jobItemId),
					tpInstruction.get(TPInstruction_.instruction), tpInstruction.get(TPInstruction_.recordedBy),
					tpInstruction.get(TPInstruction_.lastModified), tpInstruction.get(TPInstruction_.showOnTPDelNote),
					tpInstruction.get(TPInstruction_.active)
					));
			return cq;
		});
	}
}