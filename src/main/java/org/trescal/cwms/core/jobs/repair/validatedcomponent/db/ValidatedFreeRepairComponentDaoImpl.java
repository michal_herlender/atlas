package org.trescal.cwms.core.jobs.repair.validatedcomponent.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.repair.validatedcomponent.ValidatedFreeRepairComponent;

@Repository("ValidatedFreeRepairComponentDao")
public class ValidatedFreeRepairComponentDaoImpl extends BaseDaoImpl<ValidatedFreeRepairComponent, Integer> implements ValidatedFreeRepairComponentDao {
	
	@Override
	protected Class<ValidatedFreeRepairComponent> getEntity() {
		return ValidatedFreeRepairComponent.class;
	}

}