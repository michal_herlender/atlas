package org.trescal.cwms.core.jobs.job.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientPurchaseOrderExpenseItemDTO {

	private Integer itemNo;
	private String serviceModel;
	private String comment;
	private Integer quantity;
}