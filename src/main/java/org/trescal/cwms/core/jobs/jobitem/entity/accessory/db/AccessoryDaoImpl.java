package org.trescal.cwms.core.jobs.jobitem.entity.accessory.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.Accessory;

@Repository("AccessoryDao")
public class AccessoryDaoImpl extends BaseDaoImpl<Accessory, Integer> implements AccessoryDao {
	
	@Override
	protected Class<Accessory> getEntity() {
		return Accessory.class;
	}
}