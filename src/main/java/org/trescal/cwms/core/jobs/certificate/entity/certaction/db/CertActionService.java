package org.trescal.cwms.core.jobs.certificate.entity.certaction.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateAction;
import org.trescal.cwms.core.jobs.certificate.entity.certaction.CertAction;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

public interface CertActionService
{
	void deleteCertAction(CertAction certaction);

	CertAction findCertAction(int id);

	List<CertAction> getAllCertActions();

	void insertCertAction(CertAction certaction);
	
	/**
	 * Logs a {@link CertAction} (performed by the given {@link Contact}) in the
	 * database for the given {@link Certificate}
	 * 
	 * @param cert the {@link Certificate} that the {@link CertAction} is being
	 *        logged for
	 * @param action the {@link CertificateAction} that is happening
	 * @param con the {@link Contact} to log the action against
	 */
	void logCertAction(Certificate cert, CertificateAction action, Contact con);

	void saveOrUpdateCertAction(CertAction certaction);

	void updateCertAction(CertAction certaction);
}