package org.trescal.cwms.core.jobs.repair.dto.form;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.jobs.repair.dto.FreeRepairComponentDTO;
import org.trescal.cwms.core.jobs.repair.dto.FreeRepairOperationDTO;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.enums.AdjustmentPerformedEnum;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Setter
@Getter
public class RepairCompletionReportFormDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer jobItemId;
	private Integer rcrId;
	private Integer rirId;
	private String internalCompletionComments;
	private String externalCompletionComments;
	private Boolean validated;
	private Date validatedOn;
	private String validatedBy;
	@NotNull
	@Size(min = 1)
	private List<FreeRepairOperationDTO> rcrOperationsForm;
	private List<FreeRepairComponentDTO> rcrComponentsForm;
	private String lastModifiedBy;
	private Date lastModified;
	private Integer timeSpent;
	private MultipartFile rcrAppendixFile;
	private Boolean misuse;
	private String misuseComment;
	private AdjustmentPerformedEnum adjustmentPerformed;
	private LocalDateTime repairCompletionDate;
	private Date currentDate = new Date();
}
