package org.trescal.cwms.core.jobs.jobitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.jobitem.dto.ItemAtTP;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.reports.dto.ItemsAtTPWrapper;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Controller
@JsonController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class JIAtThirdPartyJsonController {

    private final JobItemService jiServ;
    private final SubdivService subdivService;

    @Autowired
    public JIAtThirdPartyJsonController(JobItemService jiServ, SubdivService subdivService) {
        this.jiServ = jiServ;
        this.subdivService = subdivService;
    }


    @RequestMapping(value = "/showItemsAtTP.json", method = RequestMethod.GET, produces = Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
    public List<ItemsAtTPWrapper> itemsAtTPWrapperList(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
        Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());

        return this.jiServ.getItemsAtThirdParty(allocatedSubdiv);
    }

    @RequestMapping(value = "/showItemsAtSpecificTP.json", method = RequestMethod.GET, produces = Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
    @ResponseBody
	public List<ItemAtTP> itemsAtSpecificThirdPartyList(
			Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@RequestParam(value = "subdivId", required = false) Integer subdivId) 
	{      
		List<Integer> subdivids = new ArrayList<Integer>();
		if(subdivId != null && subdivId != 0)
		subdivids.add(subdivId);
		return this.jiServ.getAllItemsAtSpecificThirdParty(subdivids, subdivDto.getKey(), locale);
	}
	
	
	
}
