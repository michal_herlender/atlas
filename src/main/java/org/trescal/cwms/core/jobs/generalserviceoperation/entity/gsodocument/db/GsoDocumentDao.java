package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;

public interface GsoDocumentDao extends BaseDao<GsoDocument, Integer> {
	
	GsoDocument findGsoDocumentByDocumentNumber(String documentNumber);
	
}
