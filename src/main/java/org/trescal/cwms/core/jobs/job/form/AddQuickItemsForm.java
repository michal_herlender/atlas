package org.trescal.cwms.core.jobs.job.form;

import java.util.List;

import lombok.Data;

@Data
public class AddQuickItemsForm {
	
	private Integer jobId;
	private Integer jobItemId;
	private Integer bookingInAddrId;
	private List<AddQuickItemsRowDTO> rows;

	@Data
	public static class AddQuickItemsRowDTO {
		private Integer barecode;
		private Integer turn;
		private Integer serviceTypeId;
		private Integer procId;
		private Integer workIstructionId;
		private boolean clean;
	}
}
