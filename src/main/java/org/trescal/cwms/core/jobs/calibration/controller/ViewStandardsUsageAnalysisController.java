package org.trescal.cwms.core.jobs.calibration.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.StandardsUsageAnalysis;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.db.StandardsUsageAnalysisService;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysisstatus.StandardsUsageAnalysisStatus;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsedModel;
import org.trescal.cwms.core.jobs.calibration.form.UpdateStandardsUsageAnalysisForm;
import org.trescal.cwms.core.jobs.calibration.views.ReverseTraceabilityModelGenerator;
import org.trescal.cwms.core.jobs.calibration.views.ReverseTraceabilityXlsxView;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;

@Controller
@IntranetController
@SessionAttributes(Constants.HTTPSESS_NEW_FILE_LIST)
public class ViewStandardsUsageAnalysisController {

	@Autowired
	private StandardsUsageAnalysisService standardsUsageAnalysisService;
	@Autowired
	private SystemComponentService scService;
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private ReverseTraceabilityModelGenerator reverseTraceabilityModelGenerator;
	@Autowired
	private ComponentDirectoryService componentDirectoryService;
	@Autowired
	private ReverseTraceabilityXlsxView reverseTraceabilityXlsxView;

	public static final String VIEW_NAME = "trescal/core/jobs/calibration/viewstandardsusageanalysis";
	public static final String FORM_NAME = "form";

	@ModelAttribute(FORM_NAME)
	protected UpdateStandardsUsageAnalysisForm formBackingObject() throws Exception {
		UpdateStandardsUsageAnalysisForm form = new UpdateStandardsUsageAnalysisForm();
		form.setStandardsUsageAnalysisStatus(Arrays.asList(StandardsUsageAnalysisStatus.values()));
		return form;
	}

	@ModelAttribute(Constants.REFDATA_SYSTEM_COMPONENT)
	public SystemComponent populateSystemComponent() {
		return scService.findComponent(Component.STANDARDS_USAGE_ANALYSIS);
	}

	@ModelAttribute(Constants.REFDATA_SC_ROOT_FILES)
	public FileBrowserWrapper populateRootFiles(
			@RequestParam(value = "id", required = false, defaultValue = "0") Integer id) {
		StandardsUsageAnalysis standardsUsageAnalysis = this.standardsUsageAnalysisService.get(id);
		return (FileBrowserWrapper) fileBrowserServ.getFilesForComponentRoot(Component.STANDARDS_USAGE_ANALYSIS,
				standardsUsageAnalysis.getId(), new ArrayList<String>());
	}

	@ModelAttribute(Constants.REFDATA_FILES_NAME)
	public List<String> populateFilesName(
			@RequestParam(value = "id", required = false, defaultValue = "0") Integer id) {
		return fileBrowserServ.getFilesName(populateRootFiles(id));
	}

	@RequestMapping(value = "/viewstandardsusageanalysis.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(Model model,
			@RequestParam(value = "id", required = false, defaultValue = "0") Integer id,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles) {

		StandardsUsageAnalysis standardsUsageAnalysis = this.standardsUsageAnalysisService.get(id);
		if (standardsUsageAnalysis == null) {
			throw new RuntimeException("Standards Usage Analysis not found for id " + id);
		}
		model.addAttribute("standardsUsageAnalysis", standardsUsageAnalysis);
		return new ModelAndView(VIEW_NAME);
	}

	@RequestMapping(value = "/generatereversetraceabilityreport.htm", method = RequestMethod.GET)
	protected String generateReverseTraceabilityReport(
			@RequestParam(value = "id", required = false, defaultValue = "0") Integer id,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList) throws Exception {
		SXSSFWorkbook sxxfWorkbook = new SXSSFWorkbook();
		StandardsUsageAnalysis standardsUsageAnalysis = this.standardsUsageAnalysisService.get(id);
		StandardUsedModel model = reverseTraceabilityModelGenerator.getModel(
				standardsUsageAnalysis.getInstrument().getPlantid(), standardsUsageAnalysis.getStartDate(),
				standardsUsageAnalysis.getFinishDate(), null, null, null, null);
		this.reverseTraceabilityXlsxView.generateExcelFileForReverseTraceability(sxxfWorkbook, model);
		File directory = this.componentDirectoryService.getDirectory(populateSystemComponent(),
				standardsUsageAnalysis.getIdentifier());
		File fileXlsx = new File(directory,
				"reversetraceabilityreport_" + new SimpleDateFormat("yyyyMMMdd_HHmm").format(new Date()) + ".xlsx");
		FileOutputStream fileOut = new FileOutputStream(fileXlsx);
		sxxfWorkbook.write(fileOut);
		return "redirect:viewstandardsusageanalysis.htm?id=" + id + "&loadtab=files-tab";
	}

}
