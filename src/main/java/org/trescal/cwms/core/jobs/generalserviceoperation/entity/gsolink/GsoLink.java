package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

@Entity
@Table(name = "gsolink")
public class GsoLink extends Auditable {

	private int id;
	private Integer timeSpent;
	private GeneralServiceOperation gso;
	private JobItem ji;
	private ActionOutcome outcome;
	
	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}
	
	@Column(name = "timespent")
	public Integer getTimeSpent() {
		return this.timeSpent;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "gsoid", nullable = false)
	public GeneralServiceOperation getGso() {
		return this.gso;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "jobitemid", nullable = false)
	public JobItem getJi() {
		return this.ji;
	}
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "actionoutcomeid")
	public ActionOutcome getOutcome() {
		return this.outcome;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public void setTimeSpent(Integer timeSpent) {
		this.timeSpent = timeSpent;
	}

	public void setJi(JobItem ji) {
		this.ji = ji;
	}

	public void setOutcome(ActionOutcome outcome) {
		this.outcome = outcome;
	}

	public void setGso(GeneralServiceOperation gso) {
		this.gso = gso;
	}
	
}