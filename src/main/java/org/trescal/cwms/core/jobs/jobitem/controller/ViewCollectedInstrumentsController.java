package org.trescal.cwms.core.jobs.jobitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.db.CollectedInstrumentService;
import org.trescal.cwms.core.jobs.jobitem.form.ViewCollectedInstrumentsForm;

@Controller @IntranetController
public class ViewCollectedInstrumentsController
{
	@Autowired
	private CollectedInstrumentService ciServ;

	@ModelAttribute("form")
	protected ViewCollectedInstrumentsForm formBackingObject() throws Exception
	{
		ViewCollectedInstrumentsForm form = new ViewCollectedInstrumentsForm();
		form.setCollectedInsts(this.ciServ.getAllCollectedInstruments());
		return form;
	}
	
	@RequestMapping(value="/viewcollectedinstruments.htm", method=RequestMethod.POST)
	protected RedirectView onSubmit(@ModelAttribute("form") ViewCollectedInstrumentsForm form) throws Exception
	{
		int coid = form.getCoid();
		String instsParam = "";
		for (Integer i : form.getSelectedInsts()) {
			instsParam = instsParam + i + ",";
		}
		instsParam = instsParam.substring(0, (instsParam.length() - 1));
		return new RedirectView("addjob.htm?collectedcoid=" + coid + "&insts=" + instsParam);
	}
	
	@RequestMapping(value="/viewcollectedinstruments.htm", method=RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/jobs/jobitem/viewcollectedinstruments";
	}
}