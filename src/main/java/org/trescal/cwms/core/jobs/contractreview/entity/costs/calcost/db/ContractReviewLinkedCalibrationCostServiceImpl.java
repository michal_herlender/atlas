package org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewLinkedCalibrationCost;

@Service
public class ContractReviewLinkedCalibrationCostServiceImpl extends BaseServiceImpl<ContractReviewLinkedCalibrationCost, Integer> implements ContractReviewLinkedCalibrationCostService {
	
	@Autowired
	private ContractReviewLinkedCalibrationCostDao contractReviewLinkedCalCostDao;
	
	@Override
	protected BaseDao<ContractReviewLinkedCalibrationCost, Integer> getBaseDao() {
		return contractReviewLinkedCalCostDao;
	}
}