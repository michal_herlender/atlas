package org.trescal.cwms.core.jobs.job.views;

import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.reports.view.XlsxViewDecorator;

/**
 * Base class for Job Search export, and View Selected Job Item export 
 * 
 * Note, the data included / provided in each is still slightly different hence the configuration options 
 */

public abstract class AbstractJobItemXlsxStreamingView extends AbstractXlsxStreamingView {

	@Autowired
	private MessageSource messageSource;
	
	private static Integer COL_WIDTH_PICKUP_DATE = (13 * 256);
	private static Integer COL_WIDTH_RECEIPT_DATE = (13 * 256);
	private static Integer COL_WIDTH_DATE_IN = (13 * 256);
	private static Integer COL_WIDTH_DUE_DATE = (13 * 256);
	
	/*
	 * Returns an integer -> integer map of column indexes which 
	 * need to be widened to specific sizes after auto-sizing. 
	 * (date fields, which Excel does not deal well with)
	 * 
	 * additionalFields : whether to include extra fields used by "selected job items" export
	 */
	public Map<Integer, Integer> writeLabels(XlsxViewDecorator decorator, Locale locale, boolean additionalFields) {
		int rowIndex = 0;
		int colIndex = 0;
		Map<Integer,Integer> columnWidthMap = new HashMap<>();
		CellStyle boldStyle = decorator.getStyleHolder().getHeaderStyle();

		String labelJobNo = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_JOB_NO, null,
				JobExportLabelDefinitions.TEXT_JOB_NO, locale);
		decorator.createCell(rowIndex, colIndex++, labelJobNo, boldStyle);

		String labelItemNo = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_ITEM_NO, null,
				JobExportLabelDefinitions.TEXT_ITEM_NO, locale);
		decorator.createCell(rowIndex, colIndex++, labelItemNo, boldStyle);

		String labelBarcode = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_BARCODE, null,
				JobExportLabelDefinitions.TEXT_BARCODE, locale);
		decorator.createCell(rowIndex, colIndex++, labelBarcode, boldStyle);

		String labelPlantNo = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_PLANTNO, null,
				JobExportLabelDefinitions.TEXT_PLANTNO, locale);
		decorator.createCell(rowIndex, colIndex++, labelPlantNo, boldStyle);

		String labelSerialNo = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_SERIALNO, null,
				JobExportLabelDefinitions.TEXT_SERIALNO, locale);
		decorator.createCell(rowIndex, colIndex++, labelSerialNo, boldStyle);

		String labelInstModelName = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_INSTRUMENT_MODEL_NAME,
				null, JobExportLabelDefinitions.TEXT_INSTRUMENT_MODEL_NAME, locale);
		decorator.createCell(rowIndex, colIndex++, labelInstModelName, boldStyle);

		String labelCustomerDescription = this.messageSource.getMessage(
				JobExportLabelDefinitions.CODE_CUSTOMER_DESCRIPTION, null,
				JobExportLabelDefinitions.TEXT_CUSTOMER_DESCRIPTION, locale);
		decorator.createCell(rowIndex, colIndex++, labelCustomerDescription, boldStyle);

		String labelServiceType = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_SERVICE_TYPE, null,
				JobExportLabelDefinitions.TEXT_SERVICE_TYPE, locale);
		decorator.createCell(rowIndex, colIndex++, labelServiceType, boldStyle);

		String labelStatus = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_STATUS, null,
				JobExportLabelDefinitions.TEXT_STATUS, locale);
		String labelJobItem = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_JOB_ITEM, null,
				JobExportLabelDefinitions.TEXT_JOB_ITEM, locale);
		decorator.createCell(rowIndex, colIndex++, labelStatus + " - " + labelJobItem, boldStyle);
		
		if (additionalFields) {
			String labelDate = this.messageSource.getMessage(
					JobExportLabelDefinitions.CODE_LAST_UPDATED, null, 
					JobExportLabelDefinitions.TEXT_LAST_UPDATED, locale);
			decorator.createCell(rowIndex, colIndex++, labelStatus+" - "+labelDate, boldStyle);

			String labelRemark = this.messageSource.getMessage(
					JobExportLabelDefinitions.CODE_REMARK, null, 
					JobExportLabelDefinitions.TEXT_REMARK, locale);
			decorator.createCell(rowIndex, colIndex++, labelStatus+" - "+labelRemark, boldStyle);
			
			String labelContractReviewBy = this.messageSource.getMessage(
					JobExportLabelDefinitions.CODE_CONTRACT_REVIEW_BY, null, 
					JobExportLabelDefinitions.TEXT_CONTRACT_REVIEW_BY, locale);
			decorator.createCell(rowIndex, colIndex++, labelContractReviewBy, boldStyle);
			
			String labelCapabilityRef = this.messageSource.getMessage(
					JobExportLabelDefinitions.CODE_CAPABILITY_REF, null, 
					JobExportLabelDefinitions.TEXT_CAPABILITY_REF, locale);
			decorator.createCell(rowIndex, colIndex++, labelCapabilityRef, boldStyle);
		}

		columnWidthMap.put(colIndex, COL_WIDTH_PICKUP_DATE);		
		String labelPickupDate = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_PICKUP_DATE, null,
				JobExportLabelDefinitions.TEXT_PICKUP_DATE, locale);
		decorator.createCell(rowIndex, colIndex++, labelPickupDate, boldStyle);

		columnWidthMap.put(colIndex, COL_WIDTH_RECEIPT_DATE);		
		String labelReceiptDate = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_RECEIPT_DATE, null,
				JobExportLabelDefinitions.TEXT_RECEIPT_DATE, locale);
		decorator.createCell(rowIndex, colIndex++, labelReceiptDate, boldStyle);

		columnWidthMap.put(colIndex, COL_WIDTH_DATE_IN);		
		String labelDateIn = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_DATE_IN, null,
				JobExportLabelDefinitions.TEXT_DATE_IN, locale);
		decorator.createCell(rowIndex, colIndex++, labelDateIn, boldStyle);

		columnWidthMap.put(colIndex, COL_WIDTH_DUE_DATE);		
		String labelDueDate = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_DUE_DATE, null,
				JobExportLabelDefinitions.TEXT_DUE_DATE, locale);
		decorator.createCell(rowIndex, colIndex++, labelDueDate, boldStyle);

		String labelTurnaround = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_TURNAROUND, null,
				JobExportLabelDefinitions.TEXT_TURNAROUND, locale);
		decorator.createCell(rowIndex, colIndex++, labelTurnaround, boldStyle);

		String labelCompletionDate = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_COMPLETION_DATE, null,
				JobExportLabelDefinitions.TEXT_COMPLETION_DATE, locale);
		decorator.createCell(rowIndex, colIndex++, labelCompletionDate, boldStyle);

		String labelClientRef = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_CLIENT_REF, null,
				JobExportLabelDefinitions.TEXT_CLIENT_REF, locale);
		String labelJob = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_JOB, null,
				JobExportLabelDefinitions.TEXT_JOB, locale);

		decorator.createCell(rowIndex, colIndex++, labelClientRef + " - " + labelJob, boldStyle);
		decorator.createCell(rowIndex, colIndex++, labelClientRef + " - " + labelJobItem, boldStyle);
		
		String labelContractNo = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_CONTRACT_NO, null,
				JobExportLabelDefinitions.TEXT_CONTRACT_NO, locale);
		decorator.createCell(rowIndex, colIndex++, labelContractNo, boldStyle);

		String labelClientCompany = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_CLIENT_COMPANY, null,
				JobExportLabelDefinitions.TEXT_CLIENT_COMPANY, locale);
		decorator.createCell(rowIndex, colIndex++, labelClientCompany, boldStyle);

		String labelSubdivision = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_SUBDIVISION, null,
				JobExportLabelDefinitions.TEXT_SUBDIVISION, locale);
		decorator.createCell(rowIndex, colIndex++, labelSubdivision, boldStyle);

		String labelContact = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_CONTACT, null,
				JobExportLabelDefinitions.TEXT_CONTACT, locale);
		decorator.createCell(rowIndex, colIndex++, labelContact, boldStyle);

		String labelOnBehalfOf = this.messageSource.getMessage(JobExportLabelDefinitions.CODE_ON_BEHALF_COMPANY, null,
				JobExportLabelDefinitions.TEXT_ON_BEHALF_COMPANY, locale);
		decorator.createCell(rowIndex, colIndex++, labelOnBehalfOf, boldStyle);
		
		return columnWidthMap;
	}

	/**
	 * 
	 * @param decorator
	 * @param jobs
	 * @param additionalFields whether to include extra fields used by "selected job items" export
	 */
	public void writeResults(XlsxViewDecorator decorator, Collection<JobProjectionDTO> jobs, boolean additionalFields) {
		int rowIndex = 1;

		CellStyle styleDate = decorator.getStyleHolder().getDateStyle();
		CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();
		CellStyle styleText = decorator.getStyleHolder().getTextStyle();

		for (JobProjectionDTO job : jobs) {
			for (JobItemProjectionDTO jobItem : job.getJobitems()) {
				int colIndex = 0;

				decorator.createCell(rowIndex, colIndex++, job.getJobno(), styleText);
				decorator.createCell(rowIndex, colIndex++, jobItem.getItemno(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, jobItem.getInstrument().getPlantid(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, jobItem.getInstrument().getPlantno(), styleText);
				decorator.createCell(rowIndex, colIndex++, jobItem.getInstrument().getSerialno(), styleText);
				decorator.createCell(rowIndex, colIndex++, jobItem.getInstrument().getInstrumentModelNameViaFields(false), styleText);
				decorator.createCell(rowIndex, colIndex++, jobItem.getInstrument().getCustomerDescription(), styleText);
				decorator.createCell(rowIndex, colIndex++, jobItem.getServiceType().getShortName(), styleText);
				decorator.createCell(rowIndex, colIndex++, jobItem.getState().getValue(), styleText);
				if (additionalFields) {
					if (jobItem.getLastAction() != null) {
						decorator.createCell(rowIndex, colIndex++, jobItem.getLastAction().getStartStamp(), styleDate);
						decorator.createCell(rowIndex, colIndex++, jobItem.getLastAction().getRemark(), styleText);
					}
					else {
						colIndex = colIndex + 2;
					}
					if (jobItem.getContractReviewBy() != null) {
						decorator.createCell(rowIndex, colIndex++, jobItem.getContractReviewBy().getName(), styleText);
					}
					else {
						colIndex++;
					}
					if (jobItem.getCapability() != null) {
						decorator.createCell(rowIndex, colIndex++, jobItem.getCapability().getReference(), styleText);
					}
					else {
						colIndex++;
					}
				}
				decorator.createCell(rowIndex, colIndex++, DateTools.dateFromZonedDateTime(job.getPickupDate()), styleDate);
				decorator.createCell(rowIndex, colIndex++, DateTools.dateFromZonedDateTime(job.getReceiptDate()), styleDate);
				decorator.createCell(rowIndex, colIndex++, job.getRegDate(), styleDate);
				decorator.createCell(rowIndex, colIndex++, jobItem.getDueDate(), styleDate);
				decorator.createCell(rowIndex, colIndex++, jobItem.getTurn(), styleInteger);
				decorator.createCell(rowIndex, colIndex++, jobItem.getDateComplete(), styleDate);
				decorator.createCell(rowIndex, colIndex++, job.getClientRef(), styleText);
				decorator.createCell(rowIndex, colIndex++, jobItem.getClientRef(), styleText);
				if (jobItem.getContract() != null) {
					decorator.createCell(rowIndex, colIndex++, jobItem.getContract().getValue(), styleText);
				}
				else {
					colIndex++;
				}
				if (job.getContact() != null) {
					decorator.createCell(rowIndex, colIndex++, job.getContact().getSubdiv().getCompany().getConame(), styleText);
					decorator.createCell(rowIndex, colIndex++, job.getContact().getSubdiv().getSubname(), styleText);
					decorator.createCell(rowIndex, colIndex++, job.getContact().getName(), styleText);
				}
				else {
					colIndex = colIndex+3;
				}
				
				if (jobItem.getOnBehalfCompany() != null) {
					decorator.createCell(rowIndex, colIndex++,  jobItem.getOnBehalfCompany().getConame(), styleText);
				}
				else {
					colIndex++;
				}
				

				rowIndex++;
			}
		}
	}
	
	public void resizeSelectedColumns(Sheet sheet, Map<Integer,Integer> columnWidthMap) {
		for (Integer columnIndex : columnWidthMap.keySet()) {
			Integer overrideWidth = columnWidthMap.get(columnIndex);
			sheet.setColumnWidth(columnIndex, overrideWidth);
		}
	}
}
