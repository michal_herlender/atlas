package org.trescal.cwms.core.jobs.certificate.entity.validation.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidation;

public interface CertificateValidationDao extends BaseDao<CertificateValidation, Integer> {}