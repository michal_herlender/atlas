package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument;

import java.util.Comparator;

public class GsoDocumentComparator implements Comparator<GsoDocument>
{
	@Override
	public int compare(GsoDocument o1, GsoDocument o2)
	{
		return o1.getId()>o2.getId()?-1:o1.getId()==o2.getId()?0:1;
	}
}
