package org.trescal.cwms.core.jobs.job.controller;

import io.vavr.control.Either;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.EmailResultWrapper;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.email.EmailDto;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipientDto;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("job/")
public class JobAjaxController {

    @Autowired
    private JobService jobService;

    @PostMapping("generateClientFeedbackRequest.json")
    Either<String, EmailResultDto> generateClientFeedbackRequest(@RequestBody GenerateClientFeedbackRequestIn in,
                                                                 @SessionAttribute(Constants.SESSION_ATTRIBUTE_CONTACT) Contact contact
    ) {

        return jobService.generateClientFeedback(in, contact).map(EmailResultDto::fromEmailResultWrapper);
    }


    @PostMapping("updateDefaultCalType.json")
    String updateDefaultCalType(@RequestBody UpdateDefaultsIn in){
        jobService.updateDefaultCalType(in.getJobId(),in.getValue());
        return "ok";
    }

    @PostMapping("updateDefaultTurn.json")
    String updateDefaultTurn(@RequestBody UpdateDefaultsIn in){
        jobService.updateDefaultTurn(in.getJobId(),in.getValue());
        return "ok";
    }

    @GetMapping("jobstatus.json")
    String queryjobstatus(@RequestParam Integer jobId ){
        return jobService.get(jobId).getJs().getName();
    }

}

@Data
class UpdateDefaultsIn {
    private Integer jobId,value;
}

@Value
@AllArgsConstructor
@Builder
class EmailResultDto {

    static EmailResultDto fromEmailResultWrapper(EmailResultWrapper wrapper) {
        List<List<String>> attachements = wrapper.getAttachments() != null?
                Arrays.stream(wrapper.getAttachments()).map(Arrays::asList).collect(Collectors.toList())
                :Collections.emptyList();
        return EmailResultDto.builder().body(wrapper.getBody()).send(wrapper.getSend())
                .attachments(attachements).email(EmailResultDto.fromEmail(wrapper.getEmail())).build();
    }

    static EmailDto fromEmail(Email entity) {
        val dto = new EmailDto(entity.getComponent(), entity.getEntityId(), entity.getFrom(), entity.getId(), entity.isPublish(),
                entity.getSentBy().getId(), entity.getSentBy().getFirstName(), entity.getSentBy().getLastName(), entity.getSentOn(),
                entity.getSubject()
        );
        dto.setRecipients(
                entity.getRecipients().stream().map(EmailResultDto::transformRecipient).collect(Collectors.toList()));
        return dto;
    }

    private static EmailRecipientDto transformRecipient(EmailRecipient emailRecipient) {
        val contact = emailRecipient.getRecipientCon();
        return contact!=null? new EmailRecipientDto(emailRecipient.getEmail().getId(),
                emailRecipient.getEmailAddress(),
                emailRecipient.getId(),
                contact.getPersonid(),
                contact.getFirstName(),
                contact.getLastName(),
                emailRecipient.getType()
        ):new EmailRecipientDto(emailRecipient.getEmailAddress(),emailRecipient.getType());
    }

    EmailDto email;
    String body;
    List<List<String>> attachments;
    Boolean send;
}
