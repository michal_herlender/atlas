package org.trescal.cwms.core.jobs.certificate.entity.certlink;

import java.util.Comparator;

public class CertLinkJobComparator implements Comparator<CertLink>
{
	public int compare(CertLink cl1, CertLink cl2)
	{
		int jobid1 = (cl1).getJobItem().getJob().getJobid();
		int jobid2 = (cl2).getJobItem().getJob().getJobid();

		if (jobid1 == jobid2)
		{
			Integer itemno1 = (cl1).getJobItem().getItemNo();
			Integer itemno2 = (cl2).getJobItem().getItemNo();

			return itemno1.compareTo(itemno2);
		}
		else
		{
			Integer intjobid1 = jobid1;
			Integer intjobid2 = jobid2;

			return intjobid1.compareTo(intjobid2);
		}
	}
}