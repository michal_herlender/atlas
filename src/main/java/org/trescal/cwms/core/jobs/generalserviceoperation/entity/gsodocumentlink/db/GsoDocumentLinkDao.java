package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.GsoDocumentLink;

public interface GsoDocumentLinkDao extends BaseDao<GsoDocumentLink, Integer> {
	
}
