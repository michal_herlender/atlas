package org.trescal.cwms.core.jobs.calibration.form.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.calibration.form.StandardsUsageAnalysisSearchForm;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class StandardsUsageAnalysisSearchFormValidator extends AbstractBeanValidator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(StandardsUsageAnalysisSearchForm.class);
	}
	
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			StandardsUsageAnalysisSearchForm form = (StandardsUsageAnalysisSearchForm) target;
			// date validation : the end date must be after the start date
			if(form.getStartDate() != null && form.getFinishDate() != null && form.getStartDate().after(form.getFinishDate())) {
				errors.rejectValue("startDate", "reversetraceabilitysearch.error.date", null,
						"The end date must be after the start date.");
			}
		}
	}
}
