package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "jobitemnotinvoiced", uniqueConstraints = @UniqueConstraint(columnNames = {"jobitemid"}))
public class JobItemNotInvoiced extends AbstractJobItemNotInvoiced {

	private int id;
	private JobItem jobItem;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@NotNull
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid")
	public JobItem getJobItem() {
		return jobItem;
	}

	public void setJobItem(JobItem jobItem) {
		this.jobItem = jobItem;
	}
}