package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink;

public interface GsoLinkService extends BaseService<GsoLink, Integer> {
  
}
