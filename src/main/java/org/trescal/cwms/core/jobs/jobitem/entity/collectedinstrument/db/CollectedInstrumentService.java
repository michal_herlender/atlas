package org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.db;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.dto.CollectedInstrumentDto;
import org.trescal.cwms.core.jobs.jobitem.entity.collectedinstrument.CollectedInstrument;

import java.util.List;

public interface CollectedInstrumentService {
	int addCollectedInstrumentsToJob(List<Integer> collectedInstIds, Job job, Contact currentContact, int nextItemNo);

	void collectInstruments(List<Integer> plantIds, Contact collectedBy);

	void deleteCollectedInstrument(CollectedInstrument collectedinstrument);

	CollectedInstrument findCollectedInstrument(int id);

	List<CollectedInstrument> getAllCollectedInstruments();

	List<CollectedInstrument> getCollectedInstrumentsForCompany(int coid);

	void insertCollectedInstrument(CollectedInstrument collectedinstrument);

	void saveOrUpdateCollectedInstrument(CollectedInstrument collectedinstrument);

	void updateCollectedInstrument(CollectedInstrument collectedinstrument);

	List<CollectedInstrumentDto> getCollectedInstrumentsDtoForCompany(Integer coid);
}