package org.trescal.cwms.core.jobs.jobitem.entity.pat;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "pat")
public class PAT extends Auditable {
	private Boolean bondingOk;
	private BigDecimal bondingReading;
	private Boolean cableOk;
	private boolean caseOk;
	private String comment;
	private Boolean fuseOk;
	private int id;
	private boolean insulationOk;
	private BigDecimal insulationReading;
	private JobItem ji;
	private Boolean plugOk;
	private boolean safe;
	private LocalDate testDate;
	private Contact testedBy;
	private String testEquipment;

	@Column(name = "bondingok", columnDefinition = "tinyint")
	public Boolean getBondingOk() {
		return this.bondingOk;
	}

	@Column(name = "bondingreading", precision = 10, scale = 2)
	public BigDecimal getBondingReading() {
		return this.bondingReading;
	}

	@Column(name = "cableok", columnDefinition = "tinyint")
	public Boolean getCableOk() {
		return this.cableOk;
	}

	@Length(max = 200)
	@Column(name = "comment", length = 200)
	public String getComment() {
		return this.comment;
	}

	@Column(name = "fuseok", columnDefinition = "tinyint")
	public Boolean getFuseOk() {
		return this.fuseOk;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@Column(name = "insulationreading", precision = 10, scale = 2)
	public BigDecimal getInsulationReading() {
		return this.insulationReading;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid")
	public JobItem getJi() {
		return this.ji;
	}

	@Column(name = "plugok", columnDefinition="tinyint")
	public Boolean getPlugOk()
	{
		return this.plugOk;
	}

	@Column(name = "testdate", columnDefinition = "datetime")
	public LocalDate getTestDate() {
		return this.testDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "testedbyid")
	public Contact getTestedBy() {
		return this.testedBy;
	}

	public void setTestDate(LocalDate testDate) {
		this.testDate = testDate;
	}

	@Column(name = "caseok", columnDefinition = "tinyint")
	public boolean isCaseOk() {
		return this.caseOk;
	}

	@Column(name = "insulationok", columnDefinition="tinyint")
	public boolean isInsulationOk()
	{
		return this.insulationOk;
	}

	@Column(name = "safetouse", columnDefinition="tinyint")
	public boolean isSafe()
	{
		return this.safe;
	}

	public void setBondingOk(Boolean bondingOk)
	{
		this.bondingOk = bondingOk;
	}

	public void setBondingReading(BigDecimal bondingReading)
	{
		this.bondingReading = bondingReading;
	}

	public void setCableOk(Boolean cableOk)
	{
		this.cableOk = cableOk;
	}

	public void setCaseOk(boolean caseOk)
	{
		this.caseOk = caseOk;
	}

	public void setComment(String comment)
	{
		this.comment = comment;
	}

	public void setFuseOk(Boolean fuseOk)
	{
		this.fuseOk = fuseOk;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setInsulationOk(boolean insulationOk)
	{
		this.insulationOk = insulationOk;
	}

	public void setInsulationReading(BigDecimal insulationReading)
	{
		this.insulationReading = insulationReading;
	}

	public void setJi(JobItem ji)
	{
		this.ji = ji;
	}

	public void setPlugOk(Boolean plugOk) {
		this.plugOk = plugOk;
	}

	public void setSafe(boolean safe) {
		this.safe = safe;
	}

	@NotNull
	@Length(max = 20)
	@Column(name = "testeq", nullable = false, length = 20)
	public String getTestEquipment() {
		return this.testEquipment;
	}

	public void setTestedBy(Contact testedBy) {
		this.testedBy = testedBy;
	}

	public void setTestEquipment(String testEquipment)
	{
		this.testEquipment = testEquipment;
	}
}
