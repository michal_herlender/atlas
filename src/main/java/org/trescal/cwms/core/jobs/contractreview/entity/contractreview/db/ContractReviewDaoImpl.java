package org.trescal.cwms.core.jobs.contractreview.entity.contractreview.db;

import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview_;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost_;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost_;
import org.trescal.cwms.core.jobs.contractreview.projection.ContractReviewProjectionDTO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemPriceProjectionDto;

@Repository("ContractReviewDao")
public class ContractReviewDaoImpl extends BaseDaoImpl<ContractReview, Integer> implements ContractReviewDao {
	@Override
	protected Class<ContractReview> getEntity() {
		return ContractReview.class;
	}

	@Override
	public ContractReview getMostRecentContractReviewforJobItem(JobItem jobitem) {

		return getFirstResult(cb -> {
			CriteriaQuery<ContractReview> query = cb.createQuery(ContractReview.class);
			Root<ContractReview> contractReview = query.from(ContractReview.class);
			Join<ContractReview, ContractReviewItem> contractReviewItemJoin = contractReview
					.join(ContractReview_.items);
			query.select(contractReview);
			query.where(cb.equal(contractReviewItemJoin.get(ContractReviewItem_.jobitem), jobitem));
			query.orderBy(cb.desc(contractReview.get(ContractReview_.id)));
			return query;
		}).orElse(null);
	}

	/**
	 * As a strategy, we just join the contract review items and Further
	 * information (job items, etc...) are obtained via the eager job query as
	 * this is mainly intended for use with the job view page
	 */
	@Override
	public List<ContractReview> getEagerContractReviewsForJob(int jobid) {
		return getResultList(cb -> {
			CriteriaQuery<ContractReview> cq = cb.createQuery(ContractReview.class);
			Root<ContractReview> root = cq.from(ContractReview.class);
			Join<ContractReview, Job> job = root.join(ContractReview_.job, JoinType.INNER);
			root.fetch(ContractReview_.items, JoinType.LEFT);

			cq.where(cb.equal(job.get(Job_.jobid), jobid));
			cq.distinct(true);
			return cq;
		});
	}

	/**
	 * 
	 */
	@Override
	public List<JobItemPriceProjectionDto> getPriceProjectionDTOs(Collection<Integer> jobItemIds) {
		return getResultList(cb -> {
			CriteriaQuery<JobItemPriceProjectionDto> cq = cb.createQuery(JobItemPriceProjectionDto.class);
			Root<JobItem> root = cq.from(JobItem.class);
			Join<JobItem, ContractReviewCalibrationCost> calCost = root.join(JobItem_.calibrationCost, JoinType.LEFT);
			Join<JobItem, ContractReviewAdjustmentCost> adjCost = root.join(JobItem_.adjustmentCost, JoinType.LEFT);
			Join<JobItem, ContractReviewRepairCost> repCost = root.join(JobItem_.repairCost, JoinType.LEFT);
			Join<JobItem, ContractReviewPurchaseCost> purCost = root.join(JobItem_.purchaseCost, JoinType.LEFT);
			cq.where(root.get(JobItem_.jobItemId).in(jobItemIds));
			cq.select(cb.construct(JobItemPriceProjectionDto.class, root.get(JobItem_.jobItemId),
					calCost.get(ContractReviewCalibrationCost_.costid),
					calCost.get(ContractReviewCalibrationCost_.active),
					calCost.get(ContractReviewCalibrationCost_.costSrc),
					calCost.get(ContractReviewCalibrationCost_.discountRate),
					calCost.get(ContractReviewCalibrationCost_.discountValue),
					calCost.get(ContractReviewCalibrationCost_.finalCost),
					calCost.get(ContractReviewCalibrationCost_.totalCost),
					adjCost.get(ContractReviewAdjustmentCost_.costid),
					adjCost.get(ContractReviewAdjustmentCost_.active),
					adjCost.get(ContractReviewAdjustmentCost_.costSrc),
					adjCost.get(ContractReviewAdjustmentCost_.discountRate),
					adjCost.get(ContractReviewAdjustmentCost_.discountValue),
					adjCost.get(ContractReviewAdjustmentCost_.finalCost),
					adjCost.get(ContractReviewAdjustmentCost_.totalCost), repCost.get(ContractReviewRepairCost_.costid),
					repCost.get(ContractReviewRepairCost_.active), repCost.get(ContractReviewRepairCost_.costSrc),
					repCost.get(ContractReviewRepairCost_.discountRate),
					repCost.get(ContractReviewRepairCost_.discountValue),
					repCost.get(ContractReviewRepairCost_.finalCost), repCost.get(ContractReviewRepairCost_.totalCost),
					purCost.get(ContractReviewPurchaseCost_.costid), purCost.get(ContractReviewPurchaseCost_.active),
					purCost.get(ContractReviewPurchaseCost_.costSrc),
					purCost.get(ContractReviewPurchaseCost_.discountRate),
					purCost.get(ContractReviewPurchaseCost_.discountValue),
					purCost.get(ContractReviewPurchaseCost_.finalCost),
					purCost.get(ContractReviewPurchaseCost_.totalCost)));
			return cq;
		});
	}

	@Override
	public List<ContractReviewProjectionDTO> getProjectionDTOsForJobItem(Integer jobItemId) {
		return getResultList(cb -> {
			CriteriaQuery<ContractReviewProjectionDTO> cq = cb.createQuery(ContractReviewProjectionDTO.class);
			Root<ContractReview> root = cq.from(ContractReview.class);
			Join<ContractReview, Contact> reviewBy = root.join(ContractReview_.reviewBy, JoinType.INNER);
			Join<ContractReview, ContractReviewItem> items = root.join(ContractReview_.items, JoinType.INNER);
			Join<ContractReviewItem, JobItem> jobItem = items.join(ContractReviewItem_.jobitem, JoinType.INNER);
			cq.where(cb.equal(jobItem.get(JobItem_.jobItemId), jobItemId));

			cq.select(cb.construct(ContractReviewProjectionDTO.class, root.get(ContractReview_.id),
					root.get(ContractReview_.comments), reviewBy.get(Contact_.personid),
					reviewBy.get(Contact_.firstName), reviewBy.get(Contact_.lastName),
					root.get(ContractReview_.reviewDate)));
			cq.orderBy(cb.asc(root.get(ContractReview_.id)));
			return cq;
		});
	}

	@Override
	public List<ContractReviewProjectionDTO> getCrProjectionDTOsyjob(Integer jobid) {
		return getResultList(cb -> {
			CriteriaQuery<ContractReviewProjectionDTO> cq = cb.createQuery(ContractReviewProjectionDTO.class);
			Root<ContractReview> root = cq.from(ContractReview.class);
			Join<ContractReview, Contact> reviewBy = root.join(ContractReview_.reviewBy, JoinType.INNER);
			Join<ContractReview, Job> job = root.join(ContractReview_.job, JoinType.INNER);
			cq.where(cb.equal(job.get(Job_.jobid), jobid));
			cq.select(cb.construct(ContractReviewProjectionDTO.class, root.get(ContractReview_.id),
					root.get(ContractReview_.comments), reviewBy.get(Contact_.personid),
					reviewBy.get(Contact_.firstName), reviewBy.get(Contact_.lastName),
					root.get(ContractReview_.reviewDate)));
			cq.orderBy(cb.asc(root.get(ContractReview_.id)));
			return cq;
		});
	}

	@Override
	public Long CountCryjob(Integer jobid) {
		return getFirstResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<ContractReview> root = cq.from(ContractReview.class);
			Join<ContractReview, Job> job = root.join(ContractReview_.job, JoinType.INNER);
			cq.where(cb.equal(job.get(Job_.jobid), jobid));
			cq.select(cb.count(root));
			return cq;
		}).orElse(0L);
	}

}