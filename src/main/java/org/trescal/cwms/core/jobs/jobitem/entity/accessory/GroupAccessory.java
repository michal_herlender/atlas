package org.trescal.cwms.core.jobs.jobitem.entity.accessory;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;

@Entity
@Table(name = "groupaccessory")
public class GroupAccessory
{
	private Integer id;
	private JobItemGroup group;
	private Accessory accessory;
	private Integer quantity;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "groupid")
	public JobItemGroup getGroup() {
		return this.group;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "accessory_id")
	public Accessory getAccessory() {
		return accessory;
	}
	
	public Integer getQuantity() {
		return quantity;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setGroup(JobItemGroup group) {
		this.group = group;
	}
	
	public void setAccessory(Accessory accessory) {
		this.accessory = accessory;
	}
	
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
}