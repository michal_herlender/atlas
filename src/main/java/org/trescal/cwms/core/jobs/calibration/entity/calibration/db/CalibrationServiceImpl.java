package org.trescal.cwms.core.jobs.calibration.entity.calibration.db;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.trescal.cwms.batch.jobs.operationsimport.ImportedOperationItem;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db.AliasGroupService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.db.ExchangeFormatFieldNameDetailsService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatOperationTypeEnum;
import org.trescal.cwms.core.external.adveso.events.CertificateStatusChangedEvent;
import org.trescal.cwms.core.failurereport.enums.FailureReportFinalOutcome;
import org.trescal.cwms.core.failurereport.enums.FailureReportRecommendationsEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportStateEnum;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.calibration.dto.CalToCertify;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationDTO;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalibrationToCalLinkComparator;
import org.trescal.cwms.core.jobs.calibration.entity.callink.db.CalLinkService;
import org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.QueuedCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.queuedcalibration.db.QueuedCalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.db.StandardUsedService;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.jobs.calibration.form.CalibrationSearchForm;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateAction;
import org.trescal.cwms.core.jobs.certificate.entity.certaction.db.CertActionService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.db.ContractReviewService;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItemComparator;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db.JobItemWorkRequirementService;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db.WorkRequirementService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.db.JobItemActivityService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.recall.entity.recallrule.RecallRuleInterval;
import org.trescal.cwms.core.recall.entity.recallrule.db.RecallRuleService;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.PasswordTools;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_BeginCalibration;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_CompleteCalibration;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_NoOperationPerformed;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_SignCertificate;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeType;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Calibration;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_NoOperationPerformed;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.trescal.cwms.core.tools.DateTools.dateToLocalDate;

@Service("CalibrationService")
public class CalibrationServiceImpl implements CalibrationService, ApplicationContextAware {

	@Autowired
	private ActionOutcomeService actionOutcomeServ;
	@Autowired
	private CalibrationDao calibrationDao;
	@Autowired
	private CalLinkService calLinkServ;
	@Autowired
	private CertificateService certServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private HookInterceptor_BeginCalibration interceptor_beginCalibration;
	@Autowired
	private HookInterceptor_CompleteCalibration interceptor_completeCalibration;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private ItemStateService itemStateServ;
	@Autowired
	private JobItemActivityService jiaServ;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private QueuedCalibrationService qcServ;
	@Autowired
	private RecallRuleService recallRuleServ;
	@Autowired
	private StandardUsedService standardUsedServ;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;
	@Autowired
	private WorkRequirementService wrServ;
	@Autowired
	private JobItemWorkRequirementService jobItemWorkRequirementService;
	@Autowired
	private SupportedLocaleService localeService;
	@Autowired
	private ExchangeFormatService exchangeFormatService;
	@Autowired
	private InstrumService instrumService;
	@Qualifier("messageSource")
	@Autowired
	private MessageSource messages;
	@Autowired
	private ContractReviewService contractReviewService;
	@Autowired
	private FaultReportService faultReportService;
	@Autowired
	private AliasGroupService aliasGroupService;
	@Autowired
	private ExchangeFormatFieldNameDetailsService efFieldNameDetailsService;
	@Autowired
	private HookInterceptor_NoOperationPerformed interceptor_noOperationPerformed;
	@Autowired
	private CertActionService certActionServ;
	@Autowired
	private HookInterceptor_SignCertificate interceptor_signCertificate;
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;
	@Autowired
	private CompanyService companyService;

	public final static String ACTIVITY_PRE_CALIBATION = "Pre-calibration";
	public final static String ACTIVITY_PRE_ONSITE_CALIBATION = "Pre-onsite calibration";

	private final static Logger logger = LoggerFactory.getLogger(CalibrationServiceImpl.class);

	@Override
	public void ajaxStartCalibration(int calid, int startedById) {
		// load up the required objects
		Calibration cal = this.findCalibration(calid);
		Contact startedBy = this.conServ.get(startedById);

		// set the cal date to the batch cal date
		LocalDate calDate = (cal.getBatch() != null) ? dateToLocalDate(cal.getBatch().getCalDate()) :
			LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		cal.setCalDate(calDate);

		// call the method
		this.startCalibration(cal, startedBy, null, null);
	}

	@Override
	public void cancelCalibration(int id, Contact contact) {
		Calibration cal = this.findCalibration(id);
		cal.setStatus((CalibrationStatus) this.statusServ.findStatusByName(CalibrationStatus.CANCELLED,
				CalibrationStatus.class));
		this.decrementStandardCounters(cal, cal.getStartTime());
		this.eraseIncompleteCalibrationActivity(cal, contact);

		// it is possible at this stage that the cal has been queued for cert
		// printing
		for (QueuedCalibration queuedInstance : cal.getQueued()) {
			// delete any instances of this
			this.qcServ.deleteQueuedCalibration(queuedInstance);
		}

		this.updateCalibration(cal);
	}

	@Override
	public void changeCalibrationPositionInBatch(int batchId, Integer oldPos, Integer newPos) {
		Calibration cal = this.findBatchCalibrationByPosition(batchId, oldPos);

		// set the position initially to 0 so that move other calibrations into
		// empty positions without breaking the unique constraint
		cal.setBatchPosition(0);
		// flush the session after each change of position. this is the only way
		// to prevent a unique constraint from popping
		this.updateCalibration(cal);
		this.calibrationDao.flushSession();

		Calibration temp;

		// the calibration is moving up the list
		if (oldPos > newPos) {
			// get each cal in between the new position and the old position and
			// move them down one place, flushing the session after each change
			for (int i = oldPos; i != newPos; i--) {
				temp = this.findBatchCalibrationByPosition(batchId, (i - 1));
				temp.setBatchPosition(i);
				this.updateCalibration(temp);
				this.calibrationDao.flushSession();
			}
		}
		// the calibration is moving down the list
		else {
			// get each cal in between the new position and the old position and
			// move them up one place, flushing the session after each change
			for (int i = oldPos + 1; i != newPos + 1; i++) {
				temp = this.findBatchCalibrationByPosition(batchId, i);
				temp.setBatchPosition(i - 1);
				this.updateCalibration(temp);
				this.calibrationDao.flushSession();
			}
		}

		// set the position of the cal that has been moved from 0 (it's
		// temporary position) to it's proper (new) position because the
		// position will now be vacant
		cal.setBatchPosition(newPos);
		this.updateCalibration(cal);
		this.calibrationDao.flushSession();
	}

	@Override
	public Integer checkPasswordStrength(String password) {
		return PasswordTools.getPasswordStrength(password);
	}

	@Override
	public ResultWrapper completeBatchedCal(int calId, HttpSession session) {
		if (session == null) {
			return new ResultWrapper(false, "The session has expired. Please refresh the page, log in and try again.");
		}
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact contact = this.userService.getEagerLoad(username).getCon();

		Calibration cal = this.findCalibration(calId);
		Integer timeSpent = DateTools.getMinutesDifference(cal.getStartTime(), new Date());

		Map<Integer, Integer> timeSpents = new HashMap<>();
		for (CalLink cl : cal.getLinks()) {
			timeSpents.put(cl.getId(), timeSpent);
		}
		// set calibration as complete
		this.completeCalibration(cal, contact, timeSpents, null, null, true, null, null, null);
		for (Certificate cert : cal.getCerts()) {
			// set certificate status
			this.certServ.signCertificate(cert, contact);
		}

		// queue up cert signing
		this.qcServ.queueCalibration(cal);

		// return results

		return new ResultWrapper(true, null, cal, null);
	}

	@Override
	public void completeCalibrationForTLM(Calibration calibration, Contact contact, Date calibrationEndDate,
										  Date calibrationDate, ActionOutcome actionOutcome, LocalDate nextCalDate, boolean cancelCertificate) {

		if (calibrationDate != null)
			calibration.setCalDate(dateToLocalDate(calibrationDate));
		calibration.setCompleteTime(calibrationEndDate);
		calibration.setCompletedBy(contact);

		for (CalLink cl : calibration.getLinks()) {
			cl.setOutcome(actionOutcome);
			Instrument inst = this.instrumService.get(cl.getJi().getInst().getPlantid());
			// set next cal date
			if (actionOutcome.isPositiveOutcome()) {
				// update recal date
				if (nextCalDate != null) {
					inst.setNextCalDueDate(nextCalDate);
				} else {
					RecallRuleInterval certDuration = this.recallRuleServ.getCurrentRecallInterval(inst,
							RecallRequirementType.FULL_CALIBRATION, calibration.getCalType().getServiceType());
					instrumService.calculateRecallDateAfterCalibration(inst, calibration, certDuration.getInterval(),
							certDuration.getUnit(), RecallRequirementType.FULL_CALIBRATION);
				}

				if (inst.getCalExpiresAfterNumberOfUses()) {
					inst.setUsesSinceLastCalibration(0);
					this.instrumService.merge(inst);
				}

				// set work requirement completed to true
				this.wrServ.checkCurrentWorkRequirement(cl.getJi());
			}
		}

		// resume cal if on hold
		if (calibration.getStatus().getName().equals(CalibrationStatus.ON_HOLD))
			this.resumeCalibration(calibration, contact, calibrationDate, calibrationDate,
					(CalibrationStatus) this.statusServ.findStatusByName(CalibrationStatus.ON_GOING,
							CalibrationStatus.class));

		// get current calibration status to use it for adveso calibration activity
		// synchronization
		CalibrationStatus oldStatus = calibration.getStatus();
		calibration.setStatus((CalibrationStatus) this.statusServ.findStatusByName(CalibrationStatus.COMPLETE,
				CalibrationStatus.class));

		this.updateCalibration(calibration);
		HookInterceptor_CompleteCalibration.Input input = new HookInterceptor_CompleteCalibration.Input(calibration,
				oldStatus, "", 0, contact, calibrationDate, calibrationDate);
		interceptor_completeCalibration.recordAction(input);

		// set correct cert issued
		for (CalLink cl : calibration.getLinks()) {
			cl.setCertIssued(!cancelCertificate);
		}

	}

	@Override
	public void completeCalibration(Calibration cal, Contact completedBy, Map<Integer, Integer> timeSpents,
			Map<Integer, Integer> calLinkOutcomes, String remark, boolean certIssued, List<Integer> includeOnCert,
			Integer certDuration, IntervalUnit intervalUnit) {
		// get current calibration status to use it for adveso calibration activity
		// synchronization
		CalibrationStatus oldStatus = cal.getStatus();
		cal.setStatus((CalibrationStatus) this.statusServ.findStatusByName(CalibrationStatus.COMPLETE,
			CalibrationStatus.class));
		cal.setCompletedBy(completedBy);
		cal.setCompleteTime(new Date());
		String activityName;
		if (cal.getLinks().iterator().next().getJi().getJob().getType().equals(JobType.SITE)) {
			activityName = ACTIVITY_PRE_ONSITE_CALIBATION;
		} else {
			activityName = ACTIVITY_PRE_CALIBATION;
		}
		ItemActivity activity = this.itemStateServ.findItemActivityByName(activityName);
		ActionOutcome defaultOutcome = this.actionOutcomeServ
			.findDefaultActionOutcomeForActivity(activity.getStateid());
		for (CalLink cl : cal.getLinks()) {
			Integer aoId = (calLinkOutcomes == null) ? null : calLinkOutcomes.get(cl.getId());
			ActionOutcome outcome = (aoId == null) ? defaultOutcome : this.actionOutcomeServ.get(aoId);
			Integer timeSpent = timeSpents.get(cl.getId());
			// set outcome/timespent for the item
			cl.setOutcome(outcome);
			cl.setTimeSpent(timeSpent);
			if (includeOnCert != null)
				cl.setCertIssued(certIssued && includeOnCert.contains(cl.getId()));
			else
				cl.setCertIssued(certIssued);
			// cal.setoutcome;
			// reload the instrument here to prevent LazyIExceptions
			Instrument inst = this.instServ.get(cl.getJi().getInst().getPlantid());

			RecallRequirementType requirementType = cl.getJi().getServiceType().getCalibrationType().getRecallRequirementType();

			if (outcome.isPositiveOutcome()) {
				// set recall date
				this.instServ.calculateRecallDateAfterCalibration(inst, cal, certDuration, intervalUnit,
					requirementType);
				if (inst.getCalExpiresAfterNumberOfUses()
					&& requirementType.equals(RecallRequirementType.FULL_CALIBRATION)) {
					inst.setUsesSinceLastCalibration(0);
					this.instServ.merge(inst);
				}
				this.wrServ.checkCurrentWorkRequirement(cl.getJi());
			}
		}
		this.calibrationDao.completeCalibration(cal, remark);
		HookInterceptor_CompleteCalibration.Input input = new HookInterceptor_CompleteCalibration.Input(cal, oldStatus,
				remark);
		interceptor_completeCalibration.recordAction(input);
	}

	@Override
	public ResultWrapper completeCalibrationAsUTC(int calId, int completedById, int actionOutcomeId, Integer timeSpent,
			HttpSession session) {
		Calibration cal = this.findCalibration(calId);
		Map<Integer, Integer> linkedOutcomes = new HashMap<>();
		Map<Integer, Integer> timeSpents = new HashMap<>();
		for (CalLink cl : cal.getLinks()) {
			linkedOutcomes.put(cl.getId(), actionOutcomeId);
			timeSpents.put(cl.getId(), timeSpent);
		}

		return this.completeCalibrationAsUTCMultipleOutcomes(calId, completedById, linkedOutcomes, timeSpents, null);
	}

	@Override
	public ResultWrapper completeCalibrationAsUTCMultipleOutcomes(int calId, int completedById,
			Map<Integer, Integer> linkedOutcomes, Map<Integer, Integer> timeSpents, String remark) {
		Contact completedBy = this.conServ.get(completedById);
		Calibration cal = this.findCalibration(calId);

		// if the calibration hasn't been started, start it so it can be
		// completed as utc
		if (cal.getStartTime() == null) {
			this.startCalibration(cal, completedBy, null, null);
			// reload started cal to continue
			cal = this.findCalibration(calId);
		}

		// get current calibration status to use it for adveso calibration activity
		// synchronization
		CalibrationStatus oldStatus = cal.getStatus();
		cal.setStatus((CalibrationStatus) this.statusServ.findStatusByName(CalibrationStatus.UNABLE_TO_CALIBRATE,
				CalibrationStatus.class));
		cal.setCompletedBy(completedBy);
		cal.setCompleteTime(new Date());

		for (CalLink cl : cal.getLinks()) {
			Integer aoId = linkedOutcomes.get(cl.getId());
			ActionOutcome outcome = this.actionOutcomeServ.get(aoId);
			cl.setOutcome(outcome);
			cl.setTimeSpent((timeSpents.get(cl.getId()) == null) ? 0 : timeSpents.get(cl.getId()));
		}

		this.calibrationDao.completeCalibration(cal, remark);
		HookInterceptor_CompleteCalibration.Input input = new HookInterceptor_CompleteCalibration.Input(cal, oldStatus,
				remark);
		interceptor_completeCalibration.recordAction(input);

		// initialise this field for ajax method
		cal.setBatch(cal.getBatch());

		return new ResultWrapper(true, null, cal, null);
	}

	@Override
	public void decrementStandardCounters(Calibration cal, Date beforeDate) {
		// for each standard used on the calibration
		for (StandardUsed su : cal.getStandardsUsed()) {
			Instrument inst = su.getInst();
			// if the standard uses cal intervals
			if (inst.getCalExpiresAfterNumberOfUses()) {
				// if the standard was last calibrated before the calibration
				// was recorded
				if ((inst.getLastCal() != null) && inst.getLastCal().getStartTime().before(beforeDate)) {
					int count = inst.getUsesSinceLastCalibration();
					count--;
					inst.setUsesSinceLastCalibration(count);
					this.instServ.merge(inst);
				}
			}
		}
	}

	@Override
	public void deleteCalibration(Calibration calibration, Contact contact) {

		// erase open activity if necessary
		this.eraseIncompleteCalibrationActivity(calibration, contact);

		// delete the cal links
		Set<CalLink> cls = calibration.getLinks();
		for (CalLink cl : cls) {
			calibration.getLinks().remove(cl);
			this.calLinkServ.deleteCalLink(cl);
		}

		// delete the calibration
		this.calibrationDao.remove(calibration);

	}

	@Override
	public void eraseIncompleteCalibrationActivity(Calibration cal, Contact contact) {
		// for all items on the calibration being deleted
		for (CalLink cl : cal.getLinks()) {
			JobItem ji = cl.getJi();

			// see if there is an incomplete activity
			JobItemActivity jia = this.jiaServ.getIncompleteActivityForItem(ji);

			// if there is an incomplete activity
			if (jia != null) {
				// check if the incomplete activity is a calibration
				boolean isCalibrationActivity = false;
				for (StateGroupLink sgl : jia.getActivity().getGroupLinks()) {
					if (sgl.getGroup().equals(StateGroup.CALIBRATION)) {
						isCalibrationActivity = true;
						break;
					}
				}

				// if the incomplete activity is a calibration activity
				if (isCalibrationActivity) {
					// set the item status to what it was before the calibration
					// began
					ji.setState(jia.getStartStatus());

					// remove the activity from the item's collection
					ji.getActions().remove(jia);

					// delete the incomplete activity
					this.jiaServ.delete(jia);

					// while the item is still at pre-cal on hold statuses, keep
					// deleting actions as this will otherwise block progression
					while (this.jiServ.isReadyToResumeCalibration(ji)) {
						// get all actions ordered properly
						TreeSet<JobItemAction> actions = new TreeSet<>(new JobItemActionComparator());
						actions.addAll(ji.getActions());

						JobItemActivity lastNonDeletedActivity = null;

						// loop backwards to find last non-deleted action
						for (JobItemAction action : actions.descendingSet()) {
							if (!action.isDeleted()) {
								lastNonDeletedActivity = (action instanceof JobItemActivity) ? (JobItemActivity) action
									: null;
								break;
							}
						}

						if (lastNonDeletedActivity != null) {
							// set the activity as deleted (this way any long
							// remarks entered will still technically be
							// available by viewing deleted actions)
							lastNonDeletedActivity.setDeleted(true);
							lastNonDeletedActivity.setDeletedBy(contact);
							lastNonDeletedActivity.setDeleteStamp(new Date());

							// persist the update
							this.jiaServ.merge(lastNonDeletedActivity);

							// set the item status to what it was before the
							// calibration began
							ji.setState(lastNonDeletedActivity.getStartStatus());
						}
					}
				}
			}
		}
	}

	@Override
	public Calibration findBatchCalibrationByPosition(int batchCalId, int position) {
		return this.calibrationDao.findBatchCalibrationByPosition(batchCalId, position);
	}

	@Override
	public Calibration findCalibration(int id) {
		return this.calibrationDao.find(id);
	}

	@Override
	public CalToCertify findCalToIssueCert(int calId, int plantId) {
		Calibration cal = this.findCalibration(calId);
		Instrument inst = this.instServ.get(plantId);
		RecallRuleInterval defaultDuration = this.recallRuleServ.getCurrentRecallInterval(inst,
				cal.getCalType().getRecallRequirementType(), null);

		Map<Integer, String> linkedItems = new TreeMap<>();
		for (CalLink cl : cal.getLinks()) {
			String str = "Item " + cl.getJi().getItemNo() + ": ";
			str = str + InstModelTools.modelNameViaTranslations(cl.getJi().getInst().getModel(),
				LocaleContextHolder.getLocale(), localeService.getPrimaryLocale());
			str = (cl.getOutcome() != null) ? str + "  (" + cl.getOutcome().getGenericValue().getDescription() + ")" : str;

			linkedItems.put(cl.getId(), str);
		}

		CalToCertify ctc = new CalToCertify();
		ctc.setDefaultDuration(defaultDuration.getInterval());
		ctc.setDefaultDurationUnitCode(defaultDuration.getUnit().name());
		ctc.setCalLinks(linkedItems);

		return ctc;
	}

	@Override
	public Calibration findEagerCalibration(int id) {
		return this.calibrationDao.findEagerCalibration(id);
	}

	@Override
	public Calibration findOnHoldCalibrationForItem(int jobItemId) {
		return this.calibrationDao.findOnHoldCalibrationForItem(jobItemId);
	}

	@Override
	public List<Calibration> getAllCalibrations() {
		return this.calibrationDao.findAll();
	}

	@Override
	public List<Calibration> getAllCalibrationsForJobAtStatus(int jobId, String statusName) {
		return this.calibrationDao.getAllCalibrationsForJobAtStatus(jobId, statusName);
	}

	@Override
	public List<Calibration> getAllCalibrationsForJobAtStatusUsingProc(int jobId, String statusName, int procid) {
		return this.calibrationDao.getAllCalibrationsForJobAtStatusUsingProc(jobId, statusName, procid);
	}

	@Override
	public List<Calibration> getAllCalibrationsPerformedWithInstrumentStandard(int plantid, Integer page,
			Integer resPerPage) {
		// get the list of calibrations
		List<Calibration> cals = this.calibrationDao.getAllCalibrationsPerformedWithInstrumentStandard(plantid, page,
				resPerPage);
		// now loop over these calibrations and load the standards used
		for (Calibration cal : cals) {
			for (StandardUsed su : this.standardUsedServ.ajaxGetStandardsUsedForCal(cal.getId())) {
				cal.getStandardsUsed().add(su);
			}
		}

		return cals;
	}

	@Override
	public List<Calibration> getAllOngoingCalibrationsByContact(int personid) {
		return this.calibrationDao.getAllOngoingCalibrationsByContact(personid);
	}

	@Override
	public Integer getMinutesFromCalibrationStart(int calid) {
		Calibration cal = this.findCalibration(calid);

		// use any job item linked to cal as they all should have the same start
		// time on the activity
		JobItem ji = cal.getLinks().iterator().next().getJi();

		// get start time of cal
		Date startStamp = cal.getStartTime();

		// try to find start stamp of action to use in preference to this
		// because it will be more accurate if the calibration has been on hold
		TreeSet<JobItemAction> actions = new TreeSet<>(new JobItemActionComparator());
		if ((ji.getActions() != null) && (ji.getActions().size() > 0)) {
			actions.addAll(ji.getActions());
			for (StateGroupLink sgl : actions.last().getActivity().getGroupLinks()) {
				if (sgl.getGroup().equals(StateGroup.CALIBRATION) && !actions.last().isDeleted()) {
					// bingo - use activity start stamp instead
					startStamp = actions.last().getStartStamp();
				}
			}
		}

		Integer time = DateTools.getMinutesDifference(startStamp, new Date());

		if (time == null) {
			return 0;
		} else {
			return time;
		}
	}

	@Override
	public Calibration createCalibrationForTLM(Subdiv businessSubdiv,
			List<JobItemWorkRequirement> jobItemWorkRequirements, CalibrationClass calClass,
			CalibrationProcess calProcess, Date calibrationStartDate, Date calDate) {

        Calibration calibration = new Calibration();

        JobItemWorkRequirement jiwr;
        if (!jobItemWorkRequirements.isEmpty())
            jiwr = jobItemWorkRequirements.stream().findFirst().orElse(null);
        else
            return null;

        calibration.setOrganisation(businessSubdiv);
        calibration.setCalAddress(jiwr.getWorkRequirement().getCapability().getDefAddress());
        calibration.setCalClass(calClass);
        calibration.setCalProcess(calProcess);
        calibration.setCalType(jiwr.getWorkRequirement().getServiceType().getCalibrationType());
        calibration.setCalDate(dateToLocalDate(calDate));
        calibration.setStartTime(calibrationStartDate);
        calibration.setCapability(jiwr.getWorkRequirement().getCapability());
        calibration.setWorkInstruction(jiwr.getWorkRequirement().getWorkInstruction());

        Certificate reservedCertificate = null;

        // Create CalLinks linking JobItem(s) and Calibration, also search for
        // reserved certificate
        Set<CalLink> calLinks = new TreeSet<>(new CalibrationToCalLinkComparator());
        for (JobItemWorkRequirement jobItemWorkRequirement : jobItemWorkRequirements) {
            JobItem jobItem = jobItemWorkRequirement.getJobitem();
            Certificate certificate = this.certServ.getReservedCertificate(jobItem);
            if (certificate != null)
				reservedCertificate = certificate;
			CalLink calLink = new CalLink();
			calLink.setCal(calibration);
			calLink.setJi(jobItem);
			calLink.setMainItem(jobItem.getInst().getModel().getModelType().getModules());
			/*
			  We suppose for the case of TLM that the certificate is always issued. But
			  there is a different in the certificate workflow between inhouse and onsite
			  inhouse: certIssued = true means the certificate will be issued. onsite:
			  certIssued = true means the certificate is already issued.
			 */
			calLink.setCertIssued(jobItem.getJob().getType().equals(JobType.SITE));
			calLinks.add(calLink);
		}
		calibration.setLinks(calLinks);

		// If there is a reserved certificate, use it; otherwise create a new
		// certificate.
		if (reservedCertificate != null) {
			calibration.setCalDate(dateToLocalDate(reservedCertificate.getCalDate()));
		}

		return calibration;
	}

	@Override
	public void insertAndStartCalibration(Calibration calibration, Contact user, Date startDate, String remark) {
		logger.debug(calibration.getLastModified() == null ? "null" : calibration.getLastModified().toString());
		// officially start the calibration (adds the Pre-calibration activity)
		this.startCalibration(calibration, user, startDate, remark);

		logger.debug(calibration.getLastModified() == null ? "null" : calibration.getLastModified().toString());
		// the cal links and stds are persisted automatically with the insert
		// below
		this.insertCalibration(calibration);
		logger.debug(calibration.getLastModified() == null ? "null" : calibration.getLastModified().toString());
		logger.debug("About to return from insertAndStartCalibration");
	}

	@Override
	public void insertCalibration(Calibration calibration) {
		if (calibration.getStandardsUsed() != null) {
			// for each standard
			for (StandardUsed std : calibration.getStandardsUsed()) {
				// if the standard has a cal interval
				if (std.getInst().getCalExpiresAfterNumberOfUses()) {
					// increment the counter (instrument is JPA managed entity, auto saved)
					Instrument inst = std.getInst();
					int count = inst.getUsesSinceLastCalibration();
					count++;
					inst.setUsesSinceLastCalibration(count);
				}
			}
		}

		this.calibrationDao.persist(calibration);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResultWrapper publishCertForBatchedCal(int calId, HttpSession session) {
		// TODO - potential issue, should this come from the instrument?
		int duration = 12;
		IntervalUnit intervalUnit = IntervalUnit.MONTH;
		if (session == null)
			return new ResultWrapper(false, "The session has expired. Please refresh the page, log in and try again.");
		// find contact and calibration objects
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact contact = this.userService.get(username).getCon();
		KeyValue<Integer, String> subdivDto = (KeyValue<Integer, String>) session
			.getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV);
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Calibration cal = this.findCalibration(calId);
		// set the cal date to the batch cal date
		LocalDate calDate = (cal.getBatch() != null) ? dateToLocalDate(cal.getBatch().getCalDate()) : LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		cal.setCalDate(calDate);
		// issue cert no
		Certificate cert = this.certServ.createCertFromCal(contact, cal, duration, false, allocatedSubdiv, intervalUnit,
			CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE, null);
		// start calibration activity
		this.startCalibration(cal, contact, null, null);
		// return results
		return new ResultWrapper(true, null, cert, null);
	}

	@Override
	public void placeCalibrationOnHold(Calibration cal, Contact completedBy, Map<Integer, Integer> timeSpents,
			Map<Integer, Integer> calLinkOutcomes, String remark) {
		// get current calibration status to use it for adveso calibration activity
		// synchronization
		CalibrationStatus oldStatus = cal.getStatus();
		cal.setStatus((CalibrationStatus) this.statusServ.findStatusByName(CalibrationStatus.ON_HOLD,
				CalibrationStatus.class));
		cal.setCompletedBy(completedBy);
		cal.setCompleteTime(new Date());

		// cal.setOutcome(outcome);

		for (CalLink cl : cal.getLinks()) {
			Integer aoId = calLinkOutcomes.get(cl.getId());
			cl.setOutcome(this.actionOutcomeServ.get(aoId));
			cl.setTimeSpent(timeSpents.get(cl.getId()));
		}

		// this must be called so the hook closes the activity
		this.calibrationDao.completeCalibration(cal, remark);
		HookInterceptor_CompleteCalibration.Input input = new HookInterceptor_CompleteCalibration.Input(cal, oldStatus,
				remark);
		interceptor_completeCalibration.recordAction(input);
	}

	@Override
	public void resumeCalibration(Calibration cal, Contact contact, Date startDate, Date endDate,
			CalibrationStatus onGoingStatus) {
		// don't need to set any other cal fields as they're already set from
		// the first time this cal was started
		// TODO to be optimized : get ON_GOING status before the main loop
		cal.setStatus(onGoingStatus);

		this.calibrationDao.startCalibration(cal, true);
		HookInterceptor_BeginCalibration.Input input = new HookInterceptor_BeginCalibration.Input(cal, true, contact,
				startDate, endDate, null);
		interceptor_beginCalibration.recordAction(input);
	}

	@Override
	public PagedResultSet<CalibrationDTO> searchCalibrations(CalibrationSearchForm form, PagedResultSet<CalibrationDTO> prs,
			Subdiv allocatedSubdiv, Locale locale) {
		return this.calibrationDao.searchCalibrations(form, prs, allocatedSubdiv, locale);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
	}

	public void setProcedureComplete(String xindiceKey) {
	}

	@Override
	public void startCalibration(Calibration cal, Contact startedBy, Date startDate, String remark) {
		cal.setStatus((CalibrationStatus) this.statusServ.findStatusByName(CalibrationStatus.ON_GOING,
				CalibrationStatus.class));
		if (startDate == null)
			cal.setStartTime(new Date());
		else
			cal.setStartTime(startDate);
		cal.setStartedBy(startedBy);

		// this.calibrationDao.startCalibration(cal, false);
		HookInterceptor_BeginCalibration.Input input = new HookInterceptor_BeginCalibration.Input(cal, false, startedBy,
				startDate, null, remark);
		interceptor_beginCalibration.recordAction(input);
	}

	@Override
	public void updateCalibration(Calibration Calibration) {
		this.calibrationDao.merge(Calibration);
	}

	@Override
	public Calibration findLastCompletedCalibrationForJobitem(int jobitemid) {
		CalibrationStatus calibrationStatus = (CalibrationStatus) statusServ
				.findStatusByName(CalibrationStatus.COMPLETE, CalibrationStatus.class);
		return calibrationDao.findLastCalibrationForJobitemByCalStatus(jobitemid, calibrationStatus);
	}

	@Override
	public Calibration findLastCalibrationForJobitem(Integer jobitemid) {
		return calibrationDao.findLastCalibrationForJobitemByCalStatus(jobitemid, null);
	}

	@Override
	public Pair<Calibration, Certificate> startCalibrationForTLM(List<JobItemWorkRequirement> jobItemWorkRequirements,
			Contact startedBy, CalibrationProcess calProcess, CalibrationClass calClass,
			String certificateSupplementaryFor, Date startDate, Date calDate, String comment) {

        /* set next workrequiements */
        jobItemWorkRequirements.forEach(jiwr -> jiwr.getJobitem().setNextWorkReq(jiwr));

        // Get parameters from first JobItemWorkRequirement
        Optional<JobItemWorkRequirement> firstJiwr = jobItemWorkRequirements.stream().findFirst();

        if (firstJiwr.get().getWorkRequirement().getCapability() != null) {

            Optional<Subdiv> businessSubdiv = firstJiwr.map(fi -> Objects
                .requireNonNull(fi.getWorkRequirement().getCapability(),
                    String.format("Job item (%d) without work requirement", fi.getJobitem().getInst().getPlantid()))
                .getOrganisation());

            return firstJiwr.flatMap(fi -> businessSubdiv.map(bs -> {
                /* create calibration */
                Calibration calibration = this.createCalibrationForTLM(bs, jobItemWorkRequirements, calClass, calProcess,
                    startDate, calDate);

                /* insert and start calibration */
                this.insertAndStartCalibration(calibration, startedBy, calDate, comment);

                // correct the date overridden by the insertion (case only for TLM and import
                // mode)
                calibration.setStartTime(startDate);

                return getCalibrationCertificatePair(jobItemWorkRequirements, startedBy, certificateSupplementaryFor,
                    calDate, fi, bs, calibration);
			})).orElse(null);
		}
		return null;
		
	}

	private Pair<Calibration, Certificate> getCalibrationCertificatePair(
			List<JobItemWorkRequirement> jobItemWorkRequirements, Contact startedBy, String certificateSupplementaryFor,
			Date calDate, JobItemWorkRequirement firstJiwr, Subdiv businessSubdiv, Calibration calibration) {
		/* find if there is a reserved certificate */
		Optional<Certificate> maybeReservedCertificate = jobItemWorkRequirements.stream()
				.map(JobItemWorkRequirement::getJobitem).map(certServ::getReservedCertificate).filter(Objects::nonNull)
				.findAny();

		/* load supplementaryFor certificate if exists */
		final Certificate supplementaryFor = StringUtils.isNotBlank(certificateSupplementaryFor)
				? this.certServ.findCertificateByCertNo(certificateSupplementaryFor)
				: null;

		/* create certificate or use reserved one */
		return maybeReservedCertificate.map(reservedCertificate -> {
			CertificateStatusChangedEvent event = new CertificateStatusChangedEvent(this, firstJiwr.getJobitem(),
					reservedCertificate, reservedCertificate.getStatus(), CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE);
			applicationEventPublisher.publishEvent(event);

			reservedCertificate.setStatus(CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE);
			reservedCertificate.setRegisteredBy(startedBy);
			reservedCertificate.setSupplementaryFor(supplementaryFor);
			this.certServ.linkCertToCal(reservedCertificate, calibration);

			return Pair.of(calibration, reservedCertificate);
		}).orElseGet(() -> {
			// cal frequency = duration (all previously validated to be on same
			// cal frequency)
			int duration = firstJiwr.getJobitem().getInst().getCalFrequency();
			IntervalUnit durationUnit = firstJiwr.getJobitem().getInst().getCalFrequencyUnit();

			Certificate certificate = this.certServ.createCertFromCal(startedBy, calibration, duration, true,
					businessSubdiv, durationUnit, CertStatusEnum.SIGNED, calDate);
			certificate.setSupplementaryFor(supplementaryFor);

			CertificateStatusChangedEvent eventOnNewCert = new CertificateStatusChangedEvent(this,
					firstJiwr.getJobitem(), certificate, null, certificate.getStatus());
			applicationEventPublisher.publishEvent(eventOnNewCert);

			if (supplementaryFor != null) {

				supplementaryFor.getLinks().forEach(l -> {
					// publish event for replaced certificate
					CertificateStatusChangedEvent event = new CertificateStatusChangedEvent(this, l.getJobItem(),
							supplementaryFor, supplementaryFor.getStatus(), CertStatusEnum.REPLACED);
					applicationEventPublisher.publishEvent(event);
				});

				// set status 'REPLACED' for old certificate
				supplementaryFor.setStatus(CertStatusEnum.REPLACED);
			}

			return Pair.of(calibration, certificate);
		});
	}

	@Override
	public List<Calibration> importOnsiteOperations(List<List<ImportedOperationItem>> groups, Integer allocatedSubdivId,
													Integer crcId, Locale locale, CalibrationProcess calProcess) {

		List<Calibration> createdCalibrations = new ArrayList<>();

		Subdiv allocatedSubdiv = subdivService.get(allocatedSubdivId);
		// Contact crc = conServ.get(crcId);

		CalibrationStatus onGoingCalStatus = (CalibrationStatus) this.statusServ
			.findStatusByName(CalibrationStatus.ON_GOING, CalibrationStatus.class);
		CalibrationStatus onHoldCalStatus = (CalibrationStatus) statusServ.findStatusByName(CalibrationStatus.ON_HOLD,
			CalibrationStatus.class);

		ItemActivity postOnSiteCalibrationActivity = this.getCalibrationTypeItemActivity(true, false);
		ItemActivity preOnsiteCalibrationActivity = this.getCalibrationTypeItemActivity(true, true);
		ItemActivity preCalibrationOnsite = this.getCalibrationTypeItemActivity(true, true);

		ActionOutcome unSuccCalibrationAO = preCalibrationOnsite.getActionOutcomes().stream()
			.filter(ao -> ao.getGenericValue().equals(ActionOutcomeValue_Calibration.NOT_SUCCESSFUL)).findFirst()
			.orElse(null);
		ActionOutcome succCalibrationAO = preCalibrationOnsite.getActionOutcomes().stream()
			.filter(ao -> ao.getGenericValue().equals(ActionOutcomeValue_Calibration.SUCCESSFUL)).findFirst()
			.orElse(null);
		ActionOutcome failureReportRequired = actionOutcomeServ.getByTypeAndValue(
			ActionOutcomeType.NO_OPERATION_PERFORMED,
			ActionOutcomeValue_NoOperationPerformed.FAILURE_REPORT_REQUIRED);
		// ----------

		/*
		  this is to track the instruments that we generated a comments for incoherent
		  data, in order to not show them two times in case there are multiple WR
		  (groups) a bit hackish
		 */
		List<Integer> instrumentsWithIncoherentData = new ArrayList<>();

		/* loop on grouped operations */
		for (List<ImportedOperationItem> group : groups) {

			/*
			  get first dto, to get common data from (jiwr, automatic contract //
			  review...)
			 */
			ImportedOperationItem firstDto = group.stream().findFirst().orElse(null);

			/* set next jiwr on jobitem */
			assert firstDto != null;
			JobItemWorkRequirement jiwr = jobItemWorkRequirementService.get(firstDto.getJiWrId());
			JobItem jobItem = jiwr.getJobitem();
			jobItem.setNextWorkReq(jiwr);

			/* detect instruments data incoherences between file and database */
			String commentToPutIntoFirstActivity = null;
			if (!instrumentsWithIncoherentData.contains(jiwr.getJobitem().getInst().getPlantid())) {
				commentToPutIntoFirstActivity = detecteInstrumentDataDifference(firstDto, jiwr.getJobitem().getInst(),
					locale);
				instrumentsWithIncoherentData.add(jiwr.getJobitem().getInst().getPlantid());
			}

			initializeContacts(group);

			/* do automatic contract review if needed */
			if (!firstDto.getContractReviewed() && firstDto.getAutomaticContractReview()) {
				automaticContractReview(firstDto.getOperationBy(), locale, jiwr.getJobitem(),
					firstDto.getOperationDate(), firstDto.getOperationDate(), commentToPutIntoFirstActivity);
				commentToPutIntoFirstActivity = null;
			}

			/*
			  if first Op is a FR, record activity : 'No Operation performed' with outcome
			  'FR required'
			 */
			if (firstDto.getOperationType().equals(ExchangeFormatOperationTypeEnum.FR)) {
				noOperationPerformed(jiwr.getJobitem().getJobItemId(), commentToPutIntoFirstActivity,
					failureReportRequired.getId(), firstDto.getOperationDate(), 0);
				commentToPutIntoFirstActivity = null;
			}

			copyDataToOnHoldRowsFromCompletedCalRows(group);

			// flag on whether we have to start a new calibration
			boolean newCalibration = true;

			Calibration calibration = null;
			Certificate certificate = null;

			// loop on rows
			for (ImportedOperationItem row : group) {

                // if no default capability found for allocated subdivision
                // we will stop the process (we cannot create a calibration without a procedure)
                if (jiwr.getWorkRequirement().getCapability() != null) {

                    if (row.getOperationType().equals(ExchangeFormatOperationTypeEnum.FR)) {

                        /* create and save failure report */
                        boolean isFrWithoutPriorCal = group.indexOf(row) == 0;
                        FaultReport fr = faultReportService.createFailureReportForOnsiteOperationsImportation(row, jiwr,
                            allocatedSubdiv, isFrWithoutPriorCal);

                        /* register activity fore failure report (completion and validation) */
                        faultReportService.createFailureReportActivites(fr, row.getOperationDuration(), null, null);

                        newCalibration = true;

                    } else if (row.getOperationType().equals(ExchangeFormatOperationTypeEnum.CAL)) {

						if (newCalibration) {
							/* create and start a new calibration */
							Pair<Calibration, Certificate> result = this.startCalibrationForTLM(Collections.singletonList(jiwr),
								row.getOperationBy(), calProcess, row.getCalibrationClass(), null,
								row.getOperationStartDate(), row.getOperationDate(), commentToPutIntoFirstActivity);
							certificate = result.getRight();
							// update certificate
							updateCertificateFromImportedRow(certificate, row);
							// get calibration
							calibration = result.getLeft();
							newCalibration = false;
							commentToPutIntoFirstActivity = null;
						}

						if (row.getCalibrationOutcome().equals(ActionOutcomeValue_Calibration.ON_HOLD)) {

							/* register time */
							calibration = this.registerCalibrationTime(calibration, row.getOperationDate(),
								row.getOperationDuration(), row.getOperationDate(), row.getOperationBy(), null,
								onHoldCalStatus, onGoingCalStatus, postOnSiteCalibrationActivity,
								preOnsiteCalibrationActivity);

						} else if (row.getCalibrationOutcome().equals(ActionOutcomeValue_Calibration.SUCCESSFUL)
								|| row.getCalibrationOutcome().equals(ActionOutcomeValue_Calibration.NOT_SUCCESSFUL)) {
							/* register time */
							calibration = this.registerCalibrationTime(calibration, row.getOperationDate(),
								row.getOperationDuration(), row.getOperationDate(), row.getOperationBy(), null,
								onHoldCalStatus, onGoingCalStatus, postOnSiteCalibrationActivity,
								preOnsiteCalibrationActivity);

							// check if didn't create another certificate with the same DocumentNo
							boolean documentNumberAlreadyExistsInGroup = documentNumberAlreadyExistsInGroup(group, row);

							// if document number is empty or blank, set the certificate status to CANCELED
							if (StringUtils.isBlank(row.getDocumentNumber())) {
								CertificateStatusChangedEvent event = new CertificateStatusChangedEvent(this, jobItem,
										certificate, certificate.getStatus(), CertStatusEnum.TO_BE_REPLACED);
								applicationEventPublisher.publishEvent(event);
								certificate.setStatus(CertStatusEnum.TO_BE_REPLACED);
							} else if (documentNumberAlreadyExistsInGroup) {
								CertificateStatusChangedEvent event = new CertificateStatusChangedEvent(this, jobItem,
									certificate, certificate.getStatus(), CertStatusEnum.SIGNED);
								applicationEventPublisher.publishEvent(event);
								// sign certificate
								certificate.setStatus(CertStatusEnum.SIGNED);
							}

							/* complete calibration */
							ActionOutcome calibrationAO;
							if (row.getCalibrationOutcome().equals(ActionOutcomeValue_Calibration.SUCCESSFUL))
								calibrationAO = succCalibrationAO;
							else
								calibrationAO = unSuccCalibrationAO;
							this.completeCalibrationForTLM(calibration, row.getOperationValidatedBy(),
								row.getOperationValidatedOn(), row.getOperationDate(), calibrationAO,
								row.getNextCalDueDate(), certificate != null);

							if (StringUtils.isNotBlank(row.getDocumentNumber())) {
								// log Certificate action
								certActionServ.logCertAction(certificate, CertificateAction.SIGNEDANDAPPROVED,
										row.getOperationValidatedBy());

								// do not sign if a different certificate is already created and signed that has
								// the same document no
								if (!documentNumberAlreadyExistsInGroup) {

									assert certificate != null;
									CertificateStatusChangedEvent eventOnNewCert = new CertificateStatusChangedEvent(this,
										jobItem, certificate, certificate.getStatus(), CertStatusEnum.SIGNED);
									applicationEventPublisher.publishEvent(eventOnNewCert);

									// sign certificate
									certificate.setStatus(CertStatusEnum.SIGNED);
									HookInterceptor_SignCertificate.Input input = new HookInterceptor_SignCertificate.Input(
										certificate, row.getOperationValidatedBy(), 0, row.getOperationDate());
									interceptor_signCertificate.recordAction(input);
								}
							}

							createdCalibrations.add(calibration);

							newCalibration = true;
						}
					}
				}
				
			}
		}
		return createdCalibrations;
	}

	private void updateCertificateFromImportedRow(Certificate certificate, ImportedOperationItem row) {
		if (certificate == null)
			return;
		certificate.setThirdCertNo(row.getDocumentNumber());
		certificate.setSignedBy(row.getOperationValidatedBy());
		if(row.getIntervalUnit() != null){
			certificate.setUnit(row.getIntervalUnit());
		}
		certificate.setDuration(row.getCalibrationFrequency());
		certificate.setAdjustment(row.getAdjustment());
		certificate.setOptimization(row.getOptimization());
		certificate.setRepair(row.getRepair());
		certificate.setRestriction(row.getRestiction());
		certificate.setCalibrationVerificationStatus(row.getCalibrationVerificationStatus());
		
		// As calDate is initialized from calibration (LocalDate) and thereby loses any time precision, 
		// we update calDate here using the original date from the imported row.
		certificate.setCalDate(row.getOperationDate());
	}

	private String detecteInstrumentDataDifference(ImportedOperationItem dto, Instrument inst, Locale locale) {
		StringBuilder sb = new StringBuilder();

		if (dto.getPlantno() != null && !dto.getPlantno().equals(inst.getPlantno())) {
			sb.append(messages.getMessage("importedcalibrations.incoherentdataforinstrumentplantno",
					new String[] { dto.getPlantno(), inst.getPlantno() }, locale));
		}
		if (dto.getSerialNo() != null && !dto.getSerialNo().equals(inst.getSerialno())) {
			sb.append(messages.getMessage("importedcalibrations.incoherentdataforinstrumentserialno",
					new String[] { dto.getSerialNo(), inst.getSerialno() }, locale));
		}

		return sb.toString();
	}

	private boolean documentNumberAlreadyExistsInGroup(List<ImportedOperationItem> group, ImportedOperationItem row) {
		int index = group.indexOf(row);
		String thirdPartyCertNoToLookFor = group.get(index).getDocumentNumber();
		if (thirdPartyCertNoToLookFor == null)
			return false;
		for (int i = 0; i < index; i++) {
			ImportedOperationItem r = group.get(i);
			if (thirdPartyCertNoToLookFor.equals(r.getDocumentNumber())
					&& (ActionOutcomeValue_Calibration.SUCCESSFUL.equals(r.getCalibrationOutcome())
							|| ActionOutcomeValue_Calibration.NOT_SUCCESSFUL.equals(r.getCalibrationOutcome())))
				return true;
		}

		return false;
	}

	/**
	 * loops backwards to copy calProcess and CalClass from Succ or UnSucc rows to
	 * Onhold rows
	 */
	private void copyDataToOnHoldRowsFromCompletedCalRows(List<ImportedOperationItem> group) {
		CalibrationClass calClass = null;
		CalibrationVerificationStatus calVerifStatus = null;
		// loop backwards
		for (ListIterator<ImportedOperationItem> iterator = group.listIterator(group.size()); iterator.hasPrevious();) {
			final ImportedOperationItem row = iterator.previous();
			if (ExchangeFormatOperationTypeEnum.CAL.equals(row.getOperationType())) {
				if (row.getCalibrationOutcome().equals(ActionOutcomeValue_Calibration.SUCCESSFUL)
						|| row.getCalibrationOutcome().equals(ActionOutcomeValue_Calibration.NOT_SUCCESSFUL)) {
					calClass = row.getCalibrationClass();
					calVerifStatus = row.getCalibrationVerificationStatus();
				} else if (row.getCalibrationOutcome().equals(ActionOutcomeValue_Calibration.ON_HOLD)) {
					row.setCalibrationClass(calClass);
					row.setCalibrationVerificationStatus(calVerifStatus);
				}
			}
		}
	}

	private void initializeContacts(List<ImportedOperationItem> group) {

		// gets contacts
		Set<String> hrIds = group.stream()
			.flatMap(r -> Stream.of(r.getOperationByHrId(), r.getOperationValidatedByHrId()))
				.collect(Collectors.toSet());
		List<Contact> contacts = conServ.getByHrIds(new ArrayList<>(hrIds));
		for (ImportedOperationItem row : group) {
			Contact opBy = contacts.stream().filter(c -> c.getHrid().equals(row.getOperationByHrId())).findAny()
					.orElse(null);
			row.setOperationBy(opBy);
			Contact opValidatedBy = contacts.stream().filter(c -> c.getHrid().equals(row.getOperationValidatedByHrId()))
					.findAny().orElse(null);
			row.setOperationValidatedBy(opValidatedBy);
		}
	}

	@Override
	public void automaticContractReview(Contact contact, Locale locale, JobItem ji, Date startDate, Date endDate,
			String comment) {

		ContractReview review = new ContractReview();
		review.setComments("[" + messages.getMessage("importedcalibrations.automaticcontractreview", null, locale)
				+ "] " + (comment == null ? "" : comment));
		review.setJob(ji.getJob());
		review.setReviewBy(contact);
		if (startDate == null)
			review.setReviewDate(new Date());
		else
			review.setReviewDate(startDate);
		review.setItems(new TreeSet<>(new ContractReviewItemComparator()));

		ContractReviewItem crItem = new ContractReviewItem();
		crItem.setJobitem(ji);
		crItem.setReview(review);
		review.getItems().add(crItem);

		if (ji.getContactReviewItems() == null)
			ji.setContactReviewItems(new TreeSet<>(new ContractReviewItemComparator()));
		ji.getContactReviewItems().add(crItem);

		contractReviewService.saveAndInsertContractReviewActivity(review, 0, startDate, endDate, contact);
	}

	@Override
	public Calibration registerCalibrationTime(Calibration calibration, Date startDate, Integer timeSpent, Date endDate,
			Contact technician, String remarks, CalibrationStatus onHoldCalStatus, CalibrationStatus onGoingCalStatus,
			ItemActivity postOnSiteCalibrationActivity, ItemActivity preOnsiteCalibrationActivity) {

		// calculate end date
		if (endDate == null)
			endDate = DateUtils.addMinutes(startDate, timeSpent);

		// resume the calibration if already on hold (not the first time
		// registering the time)
		if (calibration.getStatus().equals(onHoldCalStatus)) {
			this.resumeCalibration(calibration, technician, startDate, endDate, onGoingCalStatus);
		}

		// get current calibration status to use it for adveso calibration activity
		// synchronization
		CalibrationStatus oldStatus = calibration.getStatus();
		calibration.setStatus(onHoldCalStatus);

		boolean isInPostCalibration = calibration.getLinks().stream().allMatch(cl -> cl.getJi().getState().getDescription().toLowerCase().contains("post"));

		// update timeSpent on CalLink and actionOutcome depending on whether
		// we're pre or post and if we're onsite or not
		for (CalLink cl : calibration.getLinks()) {
			if (cl.getJi().getJob().getType().equals(JobType.SITE)) {
				if (isInPostCalibration) {
					ActionOutcome postOnsiteCalibrationOnHoldActionOutcome = postOnSiteCalibrationActivity
						.getActionOutcomes().stream().filter(ao -> ao.getGenericValue().equals(ActionOutcomeValue_Calibration.ON_HOLD)).findFirst().orElse(null);

					cl.setOutcome(postOnsiteCalibrationOnHoldActionOutcome);
				} else {
					ActionOutcome preOnSiteCalibrationOnHoldActionOutcome = preOnsiteCalibrationActivity
						.getActionOutcomes().stream().filter(ao -> ao.getGenericValue().equals(ActionOutcomeValue_Calibration.ON_HOLD)).findFirst().orElse(null);
					cl.setOutcome(preOnSiteCalibrationOnHoldActionOutcome);
				}
			} else {
				if (isInPostCalibration) {
					// load activity 'Post-calibration'
					ItemActivity postCalibrationActivity = this.getCalibrationTypeItemActivity(false, false);
					ActionOutcome postCalibrationOnHoldActionOutcome = postCalibrationActivity.getActionOutcomes()
						.stream().filter(ao -> ao.getGenericValue().equals(ActionOutcomeValue_Calibration.ON_HOLD)).findFirst().orElse(null);
					cl.setOutcome(postCalibrationOnHoldActionOutcome);
				} else {
					// load activity 'Pre-calibration'
					ItemActivity preCalibrationActivity = this.getCalibrationTypeItemActivity(false, true);
					ActionOutcome preCalibrationOnHoldActionOutcome = preCalibrationActivity.getActionOutcomes()
						.stream().filter(ao -> ao.getGenericValue().equals(ActionOutcomeValue_Calibration.ON_HOLD)).findFirst().orElse(null);
					cl.setOutcome(preCalibrationOnHoldActionOutcome);
				}
			}

			// increment the time
			cl.setTimeSpent((cl.getTimeSpent() != null ? cl.getTimeSpent() : 0) + timeSpent);
		}

		// update jobitem status and jobitemactivity
		HookInterceptor_CompleteCalibration.Input input = new HookInterceptor_CompleteCalibration.Input(calibration,
				oldStatus, remarks, timeSpent, technician, startDate, endDate);
		interceptor_completeCalibration.recordAction(input);

		return calibration;
	}

	// TODO: cache ?
	@Override
	public ItemActivity getCalibrationTypeItemActivity(boolean onSite, boolean preCal) {

		// load all cal type itemactivities
		List<ItemState> calibrationActivities = itemStateServ.getAll(StateGroup.CALIBRATION);

		return (ItemActivity) calibrationActivities.stream().filter(ia -> {
			// look if description contains the word 'pre' or 'post' and is for
			// onsite or not
			boolean forOnSite = jiServ.stateHasGroupOfKeyName(ia, StateGroup.DURINGONSITE);
			if (onSite)
				return forOnSite && ia.getDescription().toLowerCase().contains(preCal ? "pre" : "post");
			else
				return !forOnSite && ia.getDescription().toLowerCase().contains(preCal ? "pre" : "post");
		}).findFirst().orElse(null);
	}

	@Override
	public List<ImportedOperationItem> convertFileContentToImportedCalibrationDTO(Integer exchangeFormatId,
			ArrayList<LinkedCaseInsensitiveMap<String>> fileContent, Integer subdivid) throws ParseException {

		ExchangeFormat exchangeFormat = exchangeFormatService.get(exchangeFormatId);
		AliasGroup aliasGroup = exchangeFormat.getAliasGroup();

		SimpleDateFormat sdf = new SimpleDateFormat(exchangeFormat.getDateFormat().getValue());

		List<ExchangeFormatFieldNameDetails> efFields = efFieldNameDetailsService.getByExchangeFormat(exchangeFormatId);

		List<ImportedOperationItem> myList = new ArrayList<>();
		for (LinkedCaseInsensitiveMap<String> row : fileContent) {
			ImportedOperationItem dto = new ImportedOperationItem();
			dto.setUndefineds(new HashMap<>());
			for (String key : row.keySet()) {
				ExchangeFormatFieldNameEnum efFieldName = efFields.stream()
					.filter(e -> (e.getTemplateName() != null
						&& e.getTemplateName().trim().equalsIgnoreCase(key.trim()))
						|| (e.getFieldName().getValue().trim().equalsIgnoreCase(key.trim())))
					.map(ExchangeFormatFieldNameDetails::getFieldName).findFirst().orElse(null);
				if (efFieldName != null && StringUtils.isNoneBlank(row.get(key)))
					switch (efFieldName) {
						case ID_TRESCAL:
							if (StringUtils.isNotBlank(row.get(key)))
							dto.setPlantid(Integer.valueOf(row.get(key)));
						break;
					case PLANT_NO:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setPlantno(row.get(key));
						break;
					case SERIAL_NUMBER:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setSerialNo(row.get(key));
						break;
					case EXPECTED_SERVICE:
						dto.setService(row.get(key));
						ServiceType st = this.aliasGroupService.determineExpectedServiceTypeFromAliasOrValue(aliasGroup,
								row.get(key));
						if (st != null)
							dto.setServiceTypeId(st.getServiceTypeId());
						break;

					case SERVICETYPE_ID:
						dto.setServiceTypeId(Integer.valueOf(row.get(key)));
						break;
					case OPERATION_TYPE:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setOperationType(ExchangeFormatOperationTypeEnum.valueOf(row.get(key.toUpperCase())));
						break;
					case OPERATION_DATE:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setOperationDate(sdf.parse(row.get(key)));
						break;
					case OPERATION_START_DATE:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setOperationStartDate(sdf.parse(row.get(key)));
						break;
					case OPERATION_DURATION:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setOperationDuration(Integer.valueOf(row.get(key)));
						break;
					case OPERATION_BY_HRID:
						dto.setOperationByHrId(row.get(key));
						break;
					case DOCUMENT_NO:
						dto.setDocumentNumber(row.get(key));
						break;
					case OPERATION_VALIDATED_BY_HRID:
						dto.setOperationValidatedByHrId(row.get(key));
						break;
					case OPERATION_VALIDATED_ON:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setOperationValidatedOn(sdf.parse(row.get(key)));
						break;
					case BRAND:
						dto.setBrand(row.get(key));
						break;
					case CUSTOMER_DESCRIPTION:
						dto.setCustomerDescription(row.get(key));
						break;
					case CUSTOMER_COMMENTS:
						dto.setCustomerComments(row.get(key));
						break;
					case MODEL:
						dto.setModel(row.get(key));
						break;
					case JOBITEM_EQUIVALENT_ID:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setJobitemEquivalentId(row.get(key));
						break;
					case CALIBRATION_OUTCOME:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setCalibrationOutcome(ActionOutcomeValue_Calibration.valueOf(row.get(key)));
						break;
					case CAL_FREQUENCY:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setCalibrationFrequency(Integer.valueOf(row.get(key)));
						break;
					case INTERVAL_UNIT:
						dto.setIntervalUnit(
								this.aliasGroupService.determineIntervalUnitValue(aliasGroup, row.get(key)));
						break;
					case NEXT_CAL_DUEDATE:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setNextCalDueDate(dateToLocalDate(sdf.parse(row.get(key))));
						break;
					case CALIBRATION_CLASS:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setCalibrationClass(CalibrationClass.valueOf(row.get(key)));
						break;
					case CALIBRATION_VERIFICATION_STATUS:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setCalibrationVerificationStatus(CalibrationVerificationStatus.valueOf(row.get(key)));
						break;
					case ADJUSTMENT:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setAdjustment(Boolean.valueOf(row.get(key)));
						break;
					case OPTIMIZATION:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setOptimization(Boolean.valueOf(row.get(key)));
						break;
					case RESTRICTION:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setRestiction(Boolean.valueOf(row.get(key)));
						break;
					case REPAIR:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setRepair(Boolean.valueOf(row.get(key)));
						break;
					case FR_STATES:
						if (StringUtils.isNotBlank(row.get(key))) {
							List<FailureReportStateEnum> states = Arrays.stream(row.get(key).split(","))
								.map(s -> FailureReportStateEnum.valueOf(s.trim())).collect(Collectors.toList());
							dto.setFrStates(states);
						}
						break;
					case FR_STATE_OTHER:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setFrOtherState(row.get(key));
						break;
					case FR_TECHNICIAN_COMMENTS:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setFrTechnicianComment(row.get(key));
						break;
					case FR_RECOMMENDATIONS:
						if (StringUtils.isNotBlank(row.get(key))) {
							List<FailureReportRecommendationsEnum> recommendations = Arrays.stream(row.get(key).split(","))
								.map(s -> FailureReportRecommendationsEnum.valueOf(s.trim()))
								.collect(Collectors.toList());
							dto.setFrRecommendations(recommendations);
						}
						break;
					case FR_DISPENSATION_COMMENTS:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setFrDispensationComment(row.get(key));
						break;
					case FR_VALIDATION_COMMENTS:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setFrValidationComment(row.get(key));
						break;
					case FR_SEND_TO_CLIENT:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setFrSendToClient(Boolean.valueOf(row.get(key)));
						break;
					case FR_CLIENT_RESPONSE_NEEDED:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setFrClientResponseNeeded(Boolean.valueOf(row.get(key)));
						break;
					case FR_FINAL_OUTCOME:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setFrFinalOutcome(FailureReportFinalOutcome.valueOf(row.get(key)));
						break;
					case FR_CLIENT_APPROVAL_ON:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setFrClientApprovalOn(sdf.parse(row.get(key)));
						break;
					case FR_CLIENT_DECISION:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setFrClientDecision(Boolean.valueOf(row.get(key)));
						break;
					case FR_CLIENT_COMMENTS:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setFrClientComments(row.get(key));
						break;
					case FR_OPERATION_BY_TRESCAL:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setFrOperationByTrescal(Boolean.valueOf(row.get(key)));
						break;
					case FR_SUBDIV_ID:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setFrSubdivisionId(Integer.valueOf(row.get(key)));
						break;
					case FR_ESTIMATED_DELIVERY_TIME:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setFrEstimatedDeliveryTime(Integer.valueOf(row.get(key)));
						break;
					case FR_CLIENT_ALIAS:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setFrClientAlias(row.get(key));
						break;
					case UNDEFINED:
						dto.getUndefineds().put(key, row.get(key));
						break;
					default:
						break;
					}
			}

			myList.add(this.detectInstrumentImportedCalibration(dto, subdivid));
		}

		return myList;
	}

	@Override
	public ImportedOperationItem detectInstrumentImportedCalibration(ImportedOperationItem dto, Integer subdivid) {

		Integer coid = subdivService.get(subdivid).getComp().getCoid();
		if (dto.getPlantid() != null) {
			List<Instrument> instruments = instrumService.getByOwningCompany(dto.getPlantid(), coid, null, null);
			if (instruments.size() == 1) {
				dto.setInstrument(instruments.get(0));
			}
		} else if (StringUtils.isNoneBlank(dto.getPlantno()) && StringUtils.isNoneBlank(dto.getSerialNo())) {
			List<Instrument> instruments = instrumService.getByOwningCompany(null, coid, dto.getPlantno(),
				dto.getSerialNo());
			if (instruments.size() == 1) {
				dto.setInstrument(instruments.get(0));
				dto.setPlantid(instruments.get(0).getPlantid());
			}
		} else if (dto.getPlantid() == null && StringUtils.isBlank(dto.getPlantno())
			&& StringUtils.isNoneBlank(dto.getSerialNo())) {
			List<Instrument> instruments = instrumService.getByOwningCompany(null, coid, null, dto.getSerialNo());
			if (instruments.size() == 1) {
				dto.setInstrument(instruments.get(0));
				dto.setPlantid(instruments.get(0).getPlantid());
			}

		} else if (dto.getPlantid() == null && StringUtils.isNoneBlank(dto.getPlantno())
				&& StringUtils.isBlank(dto.getSerialNo())) {
			List<Instrument> instruments = instrumService.getByOwningCompany(null, coid, dto.getPlantno(), null);
			if (instruments.size() == 1) {
				dto.setInstrument(instruments.get(0));
				dto.setPlantid(instruments.get(0).getPlantid());
			}
		}

		return dto;
	}

	/**
	 * Gathers rows into groups of work requirements using HTML rowspan concept :
	 * rowspan = 1 : a unique row (no group) rowspan = n>1 : the row and the current
	 * 'n-1' rows constitutes a group rowspan = 0 : a row that belongs to a group
	 * (not the first row)
	 * 
	 * @param dtos to group
	 */
	@Override
	public List<List<ImportedOperationItem>> groupImportedOperations(List<ImportedOperationItem> dtos) {

		if (dtos == null || dtos.isEmpty())
			return null;

		List<List<ImportedOperationItem>> groups = new ArrayList<>();
		List<ImportedOperationItem> group = null;
		boolean newGroup = true;
		int i = 0;
		do {
			ImportedOperationItem dto = dtos.get(i);
			if (newGroup) {
				group = new ArrayList<>();
				groups.add(group);
				dto.setRowspan(1);
				newGroup = false;
			} else {
				dto.setRowspan(0);
				ImportedOperationItem firstDto = group.get(0);
				firstDto.setRowspan(firstDto.getRowspan() + 1);
			}
			group.add(dto);

			/* Decide whether the next element should go into a different group or not */
			ImportedOperationItem nextDto = (i + 1) < dtos.size() ? dtos.get(i + 1) : null;
			if (nextDto != null) {

				/*
				  create a new group if the next element is on a different jobitem equivalent
				  id
				 */
				boolean nextDtoNotOnSameJiEqId = !nextDto.getJobitemEquivalentId().equals(dto.getJobitemEquivalentId());
				if (nextDtoNotOnSameJiEqId) {
					newGroup = true;
					i++;
					continue;
				}

				// set the next service type id on the current dto if it is of type FR and if
				// the next one is on the same job item
				if (ExchangeFormatOperationTypeEnum.FR.equals(dto.getOperationType())) {
					dto.setNextServiceTypeId(nextDto.getServiceTypeId());
				}

				/*
				  we should stay on the same group if the current operation is FR and the next
				  one is an adjustment or restriction
				 */
				boolean nextOpIsadjustmentOrRestrictionAfterFr = ExchangeFormatOperationTypeEnum.FR
					.equals(dto.getOperationType()) && (nextDto.getRestiction() || nextDto.getAdjustment());
				if (nextOpIsadjustmentOrRestrictionAfterFr) {
					i++;
					continue;
				}

				/*
				  create new group if the next element has a different document no (compare if
				  not blank)
				 */
				if (StringUtils.isNotBlank(nextDto.getDocumentNumber())
					&& StringUtils.isNotBlank(dto.getDocumentNumber())) {
					boolean differentDocumentNos = !dto.getDocumentNumber()
						.equalsIgnoreCase(nextDto.getDocumentNumber());
					if (differentDocumentNos) {
						newGroup = true;
						i++;
						continue;
					}

				}
			}
			i++;
		} while (i < dtos.size());

		return groups;
	}

	@Override
	public void noOperationPerformed(Integer jobItemId, String remark, Integer aoid, Date startDate, Integer time) {
		JobItem ji = jiServ.findJobItem(jobItemId);
		JobItemWorkRequirement jiwr = ji.getNextWorkReq();
		ActionOutcome actionOutcome = actionOutcomeServ.get(aoid);
		// delete wr only if the FR is not required
		if (jiwr != null && actionOutcome.getGenericValue()
				.equals(ActionOutcomeValue_NoOperationPerformed.FAILURE_REPORT_NOT_REQUIRED)) {
			// cancel current workrequirement
			this.wrServ.cancelCurrentWorkRequirement(ji);
		}

		// call no action required hook for calibration
		HookInterceptor_NoOperationPerformed.Input input = new HookInterceptor_NoOperationPerformed.Input(ji, remark,
				actionOutcome, startDate, time);
		this.interceptor_noOperationPerformed.recordAction(input);
	}
	
	@Override
	public PagedResultSet<CalibrationDTO> getCalibrationsPerformed(Integer plantId, Date startCalDate,
			Date endCalDate, Integer allocatedCompanyId, Integer allocatedSubdivId, Locale locale, PagedResultSet<CalibrationDTO> prs){
		Company allocatedCompany = this.companyService.get(allocatedCompanyId);
		Subdiv allocatedSubdiv = this.subdivService.get(allocatedSubdivId);
		return this.calibrationDao.getCalibrationsPerformed(plantId, startCalDate, endCalDate,
				allocatedCompany, allocatedSubdiv, locale, prs);
	}

	@Override
	public List<CalibrationDTO> getCalibrationsPerformed_(Integer plantId, Date startCalDate, Date endCalDate){
		return this.calibrationDao.getCalibrationsPerformed_(plantId, startCalDate, endCalDate);
	}
}