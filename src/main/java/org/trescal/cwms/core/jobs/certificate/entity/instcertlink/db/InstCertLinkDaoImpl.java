package org.trescal.cwms.core.jobs.certificate.entity.instcertlink.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate_;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink_;

@Repository("InstCertLinkDao")
public class InstCertLinkDaoImpl extends BaseDaoImpl<InstCertLink, Integer> implements InstCertLinkDao {
	
	@Override
	protected Class<InstCertLink> getEntity() {
		return InstCertLink.class;
	}

	/**
	 * Returns the total number of instcertlinks associated with the instrument 
	 * @param plantid
	 * @return
	 */
	@Override
	public Long getInstCertLinkCount(Integer plantid) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<InstCertLink> root = cq.from(InstCertLink.class);
			Join<InstCertLink, Instrument> instrument = root.join(InstCertLink_.inst);
			cq.where(cb.equal(instrument.get(Instrument_.plantid), plantid));
			cq.select(cb.count(root));
			return cq;
		});
	}
	
	/**
	 * Returns the most recent maxResults instcertlinks associated with the instrument
	 * sorted in descending order by cal date, cert number, and then inst cert link id.
	 * 
	 * Use as replacement for instrument.getCertLink() as 100s of cert links may exist
	 * for one instrument. 
	 *  
	 * @param plantid
	 * @return
	 */
	public List<InstCertLink> searchByPlantId(Integer plantid, Integer maxResults) {
		if (plantid == null) throw new IllegalArgumentException("plantid must be specified");
		if (maxResults == null) throw new IllegalArgumentException("maxResults must be specified");
		return getResultList(cb -> {
			CriteriaQuery<InstCertLink> cq = cb.createQuery(InstCertLink.class);
			Root<InstCertLink> root = cq.from(InstCertLink.class);
			Join<InstCertLink, Instrument> instrument = root.join(InstCertLink_.inst);
			Join<InstCertLink, Certificate> certificate = root.join(InstCertLink_.cert);
			cq.where(cb.equal(instrument.get(Instrument_.plantid), plantid));
			cq.orderBy(cb.desc(certificate.get(Certificate_.calDate)),
					cb.desc(certificate.get(Certificate_.certno)),
					cb.desc(root.get(InstCertLink_.id)));
			
			return cq;
		}, 0, maxResults);
	}
}