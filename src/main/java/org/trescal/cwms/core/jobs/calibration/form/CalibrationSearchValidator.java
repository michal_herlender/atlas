package org.trescal.cwms.core.jobs.calibration.form;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class CalibrationSearchValidator extends AbstractBeanValidator
{
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(CalibrationSearchForm.class);
	}

	public void validate(Object obj, Errors errors)
	{
		CalibrationSearchForm form = (CalibrationSearchForm) obj;
		super.validate(form, errors);

		/**
		 * BUSINESS VALIDATION
		 */
		if (form.isStartDateBetween())
		{
			// if first date is given
			if (form.getStartDate1() != null)
			{
				// check second date is given
				if (form.getStartDate2() == null)
				{
					errors.rejectValue("startDate2", "startDate2", null, "Second start date has not been specified.");
				}
				else
				{
					// check second date is chronologically after the first date
					Calendar cal1 = new GregorianCalendar();
					cal1.setTime(form.getStartDate1());
					Calendar cal2 = new GregorianCalendar();
					cal2.setTime(form.getStartDate2());

					if (!cal2.after(cal1))
					{
						errors.rejectValue("startDate2", "startDate2", null, "Second start date must be after the first start date.");
					}
				}
			}
		}
		if (form.getBarcode() != null)
		{
			if (!form.getBarcode().trim().isEmpty()
					&& !NumberTools.isAnInteger(form.getBarcode().trim()))
			{
				// check that it converts into a number
				errors.rejectValue("barcode", "barcode", null, "The barcode given must be a number.");
			}
		}
		if (form.getStandardBarcode() != null)
		{
			if (!form.getStandardBarcode().trim().isEmpty()
					&& !NumberTools.isAnInteger(form.getStandardBarcode().trim()))
			{
				// check that it converts into a number
				errors.rejectValue("barcode", "barcode", null, "The standard barcode given must be a number.");
			}
		}
	}
}