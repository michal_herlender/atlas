package org.trescal.cwms.core.jobs.jobitem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.db.CallOffItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.dto.CallOffItemDTO;

import java.util.List;

@RestController
@RequestMapping("calloffitems")
public class CallOffItemsAjaxController {

    @Autowired
    private CallOffItemService callOffItemService;

    @GetMapping("byCompany.json")
    List<CallOffItemDTO> getByCompanyAndActiveState(@RequestParam Integer coid, @RequestParam(required = false) Boolean active) {
        return callOffItemService.getCallOffItemsDtoForCompany(coid, active);
    }
}
