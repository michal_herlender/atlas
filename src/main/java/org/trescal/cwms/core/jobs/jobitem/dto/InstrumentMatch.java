package org.trescal.cwms.core.jobs.jobitem.dto;

import java.util.List;

import org.trescal.cwms.core.instrument.dto.InstrumentCheckDuplicatesDto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data @AllArgsConstructor 
public class InstrumentMatch
{
	private List<InstrumentCheckDuplicatesDto> insts;

}