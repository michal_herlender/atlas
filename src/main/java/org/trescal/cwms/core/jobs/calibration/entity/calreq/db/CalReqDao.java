package org.trescal.cwms.core.jobs.calibration.entity.calreq.db;

import java.util.List;
import java.util.Map;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.InstrumentCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.InstrumentCalReqDto;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.JobItemCalReq;

public interface CalReqDao extends BaseDao<CalReq, Integer> {
	
	List<JobItemCalReq> findAllCalReqsImmediatelyAffectingJobItem(int jobItemId);
	
	CalReq findCalReqsForCompanyAndModel(int compId, int modelId);
	
	InstrumentCalReq findCalReqsForInstrument(int plantId);
	
	CalReq findCalReqsForJobItem(int jobItemId);
	
	CalReq findCalReqsForModel(int modelId);
	
	Map<Integer, CalReq> findCalReqsForMultipleCompaniesAndModels(Map<Integer, CalReq> map, Map<Integer, List<Integer>> modelToJobItemMap, Map<Integer, List<Integer>> compModelMap);
	
	Map<Integer, CalReq> findCalReqsForMultipleInstruments(Map<Integer, CalReq> map, Map<Integer, Integer> instToJobItemMap);
	
	Map<Integer, CalReq> findCalReqsForMultipleJobItemModels(Map<Integer, CalReq> map, Map<Integer, List<Integer>> modelToJobItemMap);
	
	Map<Integer, CalReq> findCalReqsForMultipleJobItems(List<Integer> jobItemIds);

	Map<Integer, InstrumentCalReqDto> findInstrumentCalReqsForPlantIds(List<Integer> plantids);
}