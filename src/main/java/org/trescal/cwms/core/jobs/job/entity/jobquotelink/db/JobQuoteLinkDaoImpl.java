package org.trescal.cwms.core.jobs.job.entity.jobquotelink.db;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink_;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation_;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem_;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository("JobQuoteLinkDao")
public class JobQuoteLinkDaoImpl extends BaseDaoImpl<JobQuoteLink, Integer> implements JobQuoteLinkDao
{
	@Override
	protected Class<JobQuoteLink> getEntity() {
		return JobQuoteLink.class;
	}
	
	@Override
	public List<JobQuoteLink> findJobQuoteLinks(int quoteid, int jobid)
	{
		return getResultList(cb ->{
			CriteriaQuery<JobQuoteLink> cq = cb.createQuery(JobQuoteLink.class);
			Root<JobQuoteLink> root = cq.from(JobQuoteLink.class);
			Join<JobQuoteLink, Job> job = root.join(JobQuoteLink_.job);
			Join<JobQuoteLink, Quotation> quotation = root.join(JobQuoteLink_.quotation);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(job.get(Job_.jobid), jobid));
			clauses.getExpressions().add(cb.equal(quotation.get(Quotation_.id), quoteid));
			cq.where(clauses);
			return cq;
		});
	}
	
	@SuppressWarnings("unchecked")
	public List<JobQuoteLink> findJobQuoteLinksResultset(int quoteid, Integer page, Integer resPerPage)
	{
		Criteria criteria = getSession().createCriteria(JobQuoteLink.class);
		criteria.createCriteria("quotation").add(Restrictions.idEq(quoteid));
		criteria.setFetchMode("linkedBy", FetchMode.JOIN);
		criteria.setFetchMode("job", FetchMode.JOIN);
		criteria.setFetchMode("job.con", FetchMode.JOIN);
		criteria.setFetchMode("job.con.sub", FetchMode.JOIN);
		criteria.setFetchMode("job.con.sub.comp", FetchMode.JOIN);
		criteria.addOrder(Order.desc("linkedOn"));
		// apply paging if requested
		if (page != null) {
			resPerPage = resPerPage == null ? Constants.RESULTS_PER_PAGE : resPerPage;
			criteria.setFirstResult((resPerPage * page) - resPerPage);
			criteria.setMaxResults(resPerPage);
		}
		return criteria.list();
	}
	
	public Integer getCountJobQuoteLinks(int quoteid)
	{
		Criteria criteria = getSession().createCriteria(JobQuoteLink.class);
		criteria.createCriteria("quotation").add(Restrictions.idEq(quoteid));
		criteria.setProjection(Projections.rowCount());
		return ((Long) criteria.uniqueResult()).intValue();
	}
	

	public Set<InstrumentModel> getModelIdsFromAllJobQuotes(int jobId)
	{

		//get model ids for linked models
        List<InstrumentModel> list1 = getResultList(cb -> {
			CriteriaQuery<InstrumentModel> cq = cb.createQuery(InstrumentModel.class);
			Root<InstrumentModel> instrumentModel = cq.from(InstrumentModel.class);
			Join<InstrumentModel, Quotationitem> quotaionItems = instrumentModel.join(InstrumentModel_.quoteItems, javax.persistence.criteria.JoinType.LEFT);
			Join<Quotationitem, Quotation> quotation = quotaionItems.join(Quotationitem_.quotation);
			Join<Quotation, JobQuoteLink> linkedJobs = quotation.join(Quotation_.linkedJobs);
			Join<JobQuoteLink, Job> job = linkedJobs.join(JobQuoteLink_.job);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(job.get(Job_.jobid), jobId));
		    cq.where(clauses);
			return cq;
		});
		HashSet<InstrumentModel> models = new HashSet<>(list1);
	
		//get model ids for linked instruments
		List<Instrument> list2 = getResultList(cb -> {
			CriteriaQuery<Instrument> cq = cb.createQuery(Instrument.class);
			Root<Instrument> instrument = cq.from(Instrument.class);
			Join<Instrument, Quotationitem> quotationItems = instrument.join(Instrument_.quoteItems);
			Join<Quotationitem, Quotation> quotation = quotationItems.join(Quotationitem_.quotation);
			Join<Quotation, JobQuoteLink> linkedJobs = quotation.join(Quotation_.linkedJobs);
			Join<JobQuoteLink, Job> job = linkedJobs.join(JobQuoteLink_.job);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(job.get(Job_.jobid), jobId));
		    cq.where(clauses);
			return cq;
		});
		
		list2.forEach(inst->models.add(inst.getModel()));
		
		return models;
	}
	
	public Set<InstrumentModel> getModelIdsFromJobQuote(int jobQuoteLinkId)
	{

		//get model ids for linked models
        List<InstrumentModel> list1 = getResultList(cb -> {
			CriteriaQuery<InstrumentModel> cq = cb.createQuery(InstrumentModel.class);
			Root<InstrumentModel> instrumentModel = cq.from(InstrumentModel.class);
			Join<InstrumentModel, Quotationitem> quotationItems=instrumentModel.join(InstrumentModel_.quoteItems, javax.persistence.criteria.JoinType.LEFT);
			Join<Quotationitem, Quotation> quotation = quotationItems.join(Quotationitem_.quotation);
			Join<Quotation, JobQuoteLink> linkedJobs = quotation.join(Quotation_.linkedJobs);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(linkedJobs.get(JobQuoteLink_.id), jobQuoteLinkId));
		    cq.where(clauses);
			return cq;
		});

		HashSet<InstrumentModel> models = new HashSet<>(list1);
				
		//get model ids for linked instruments		
		List<Instrument> list2 = getResultList(cb -> {
			CriteriaQuery<Instrument> cq = cb.createQuery(Instrument.class);
			Root<Instrument> instrument = cq.from(Instrument.class);
			Join<Instrument, Quotationitem> quotaionItems=instrument.join(Instrument_.quoteItems);
			Join<Quotationitem, Quotation> quotation = quotaionItems.join(Quotationitem_.quotation);
			Join<Quotation, JobQuoteLink> linkedJobs=quotation.join(Quotation_.linkedJobs);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(linkedJobs.get(JobQuoteLink_.id), jobQuoteLinkId));
		    cq.where(clauses);
			return cq;
		});
		
		list2.forEach(inst->models.add(inst.getModel()));

		return models;
	}
	
	@Override
	public PagedResultSet<Instrument> getInstrumentsFromAllJobQuotes(PagedResultSet<Instrument> prs, Integer jobId) {
		Criteria criteria = getSession().createCriteria(Instrument.class);
		criteria.createCriteria("quoteItems").createCriteria("quotation").createCriteria("linkedJobs").createCriteria("job").add(Restrictions.idEq(jobId));
		
		// get count of results
		criteria.setProjection(Projections.rowCount());
		Long count = (Long) criteria.uniqueResult();
		int itemCount = count.intValue();
		prs.setResultsCount(itemCount);

		// remove the projection from the query so it will return a normal
		// resultset
		criteria.setProjection(null);
		
		if(prs.getResultsPerPage() >0){
			criteria.setFirstResult(prs.getStartResultsFrom());
			criteria.setMaxResults(prs.getResultsPerPage());
		}
		@SuppressWarnings("unchecked")
		List<Instrument> instruments = criteria.list();
		prs.setResults(instruments);
		return prs;
	}


    @Override
	public PagedResultSet<Instrument> getInstrumentsFromJobQuote(PagedResultSet<Instrument> prs, Integer jobQuoteLinkId) {
		Criteria criteria = getSession().createCriteria(Instrument.class);
		criteria.createCriteria("quoteItems").createCriteria("quotation").createCriteria("linkedJobs").add(Restrictions.idEq(jobQuoteLinkId));
		
		// get count of results
		criteria.setProjection(Projections.rowCount());
		Long count = (Long) criteria.uniqueResult();
		int itemCount = count.intValue();
		prs.setResultsCount(itemCount);

		// remove the projection from the query so it will return a normal
		// resultset
		criteria.setProjection(null);
				
		if(prs.getResultsPerPage() >0){
			criteria.setFirstResult(prs.getStartResultsFrom());
			criteria.setMaxResults(prs.getResultsPerPage());
		}
		@SuppressWarnings("unchecked")
		List<Instrument> instruments = criteria.list();
		prs.setResults(instruments);
		return prs;
	}
}