package org.trescal.cwms.core.jobs.calibration.entity.callink;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;

@Entity
@Table(name = "calibrationactivitylink")
public class CalibrationActivityLink {

	private int id;
	private Calibration calibration;
	private JobItemActivity jobItemActivity;

	// Explicit constructor for Hibernate
	public CalibrationActivityLink() {
		super();
	}
	
	public CalibrationActivityLink(Calibration calibration, JobItemActivity jobItemActivity) {
		super();
		this.calibration = calibration;
		this.jobItemActivity = jobItemActivity;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "calid", nullable = false, foreignKey=@ForeignKey(name="FK_calibrationactivitylink_calibrationid"))
	public Calibration getCalibration() {
		return calibration;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemactivityid", nullable = false, foreignKey=@ForeignKey(name="FK_calibrationactivitylink_jobitemactivityid"))
	public JobItemActivity getJobItemActivity() {
		return jobItemActivity;
	}

	public void setJobItemActivity(JobItemActivity jobItemActivity) {
		this.jobItemActivity = jobItemActivity;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCalibration(Calibration calibration) {
		this.calibration = calibration;
	}

}
