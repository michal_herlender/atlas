package org.trescal.cwms.core.jobs.contractreview.entity.contractreview;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItemComparator;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails;
import org.trescal.cwms.hibernateextension.BitSetJPAConverter;

import javax.persistence.*;
import java.util.BitSet;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "contractreview")
public class ContractReview extends Auditable {
	private String comments;
	private int id;
	private Set<ContractReviewItem> items;
	private Job job;
	private Contact reviewBy;
	private Date reviewDate;
	private PrebookingJobDetails prebookingJobDetails;
	private BitSet demandSet;

	@Length(max = 500)
	@Column(name = "comments", length = 200)
	public String getComments() {
		return this.comments;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@OneToMany(mappedBy = "review", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(ContractReviewItemComparator.class)
	public Set<ContractReviewItem> getItems() {
		return this.items;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobid", nullable = false)
	public Job getJob() {
		return this.job;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", nullable = false)
	public Contact getReviewBy() {
		return this.reviewBy;
	}

	@Column(name = "reviewdate", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getReviewDate() {
		return this.reviewDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "prebookingjobdetailsid", foreignKey = @ForeignKey(name = "FK_prebookingjobdetails_prebookingjobdetailsid_contractreview"))
	public PrebookingJobDetails getPrebookingJobDetails() {
		return this.prebookingJobDetails;
	}

	@Column(name = "demandset")
	@Convert(converter = BitSetJPAConverter.class)
	public BitSet getDemandSet() {
		return demandSet;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setItems(Set<ContractReviewItem> items) {
		this.items = items;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public void setReviewBy(Contact reviewBy) {
		this.reviewBy = reviewBy;
	}

	public void setReviewDate(Date reviewDate) {
		this.reviewDate = reviewDate;
	}

	public void setPrebookingJobDetails(PrebookingJobDetails prebookingJobDetails) {
		this.prebookingJobDetails = prebookingJobDetails;
	}

	public void setDemandSet(BitSet demandSet) {
		this.demandSet = demandSet;
	}
}