package org.trescal.cwms.core.jobs.jobitem.entity.accessory.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.GroupAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.GroupAccessory_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup_;

@Repository("groupAccessoryDao")
public class GroupAccessoryDaoImpl extends BaseDaoImpl<GroupAccessory, Integer> implements GroupAccessoryDao {

	@Override
	protected Class<GroupAccessory> getEntity() {
		return GroupAccessory.class;
	}

	@Override
	public Integer countByJob(Integer jobId) {
		Long result = getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<JobItemGroup> group = cq.from(JobItemGroup.class);
			Join<JobItemGroup, GroupAccessory> accessory = group.join(JobItemGroup_.accessories);
			cq.where(cb.equal(group.get(JobItemGroup_.job), jobId));
			cq.select(cb.sumAsLong(accessory.get(GroupAccessory_.quantity)));
			return cq;
		});
		return result == null ? 0 : result.intValue();
	}

	@Override
	public Integer countFreeTextAccessoriesByJob(Integer jobId) {
		return getCount(cb -> cq -> {
			Root<JobItemGroup> group = cq.from(JobItemGroup.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(group.get(JobItemGroup_.job), jobId));
			clauses.getExpressions().add(cb.isNotNull(group.get(JobItemGroup_.accessoryFreeText)));
			clauses.getExpressions().add(cb.notEqual(group.get(JobItemGroup_.accessoryFreeText), ""));
			cq.where(clauses);
			return Triple.of(group.get(JobItemGroup_.id), null, null);
		});
	}
}