package org.trescal.cwms.core.jobs.repair.operation.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;

@Service("FreeRepairOperationService")
public class FreeRepairOperationServiceImpl extends BaseServiceImpl<FreeRepairOperation, Integer> implements FreeRepairOperationService
{
	@Autowired
	private FreeRepairOperationDao dao;

	@Override
	protected BaseDao<FreeRepairOperation, Integer> getBaseDao() {
		return dao;
	}

	@Override
	public void refrech(FreeRepairOperation fro) {
		dao.refresh(fro);
	}
}