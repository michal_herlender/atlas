package org.trescal.cwms.core.jobs.job.entity.jobtype;

import java.util.EnumSet;
import java.util.Locale;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

/**
 * Merged JobType and JobTypeNr 2016-10-31,
 * JobType is now Enum and jobtype table is unused
 */
//@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum JobType {
    // Note - the order is important, as existing mappings use numbers
    UNDEFINED("jobtype.undefined", "Undefined"),                // 0
    STANDARD("jobtype.standard", "Standard Job"),               // 1
    SINGLE_PRICE("jobtype.singleprice", "Single Price Job"),    // 2 - formerly site job, but really "single price"
    VALVE("jobtype.valve", "Valve Job"),                        // 3
    SITE("jobtype.site", "Onsite Job");							// 4

    private ReloadableResourceBundleMessageSource messages;
    private final String messageCode;

    private final String defaultDescription;

    JobType(String messageCode, String defaultDescription) {
        this.messageCode = messageCode;
        this.defaultDescription = defaultDescription;
    }

    public static Set<JobType> getActiveJobTypes() {
        Set<JobType> result = EnumSet.noneOf(JobType.class);
        result.add(STANDARD);
        result.add(SINGLE_PRICE);
        result.add(SITE);
        return result;
    }

    public String getDescription() {
        Locale locale = LocaleContextHolder.getLocale();
        if (messages != null) {
            return messages.getMessage(this.messageCode, null, this.defaultDescription, locale);
        }
        return this.toString();
    }

	/*
	 * Needed to expose as bean property for JSP tags etc...
	 */
	public String getName() {
		return this.name();
	}

	@Component
	public static class JobTypeMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (JobType jobType : EnumSet.allOf(JobType.class))
               jobType.setMessageSource(messages);
        }
	}

	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
}