package org.trescal.cwms.core.jobs.job.entity.jobstatus.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CompoundSelection;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Repository("JobStatusDao")
public class JobStatusDaoImpl extends BaseDaoImpl<JobStatus, Integer> implements JobStatusDao {
	
	@Override
	protected Class<JobStatus> getEntity() {
		return JobStatus.class;
	}
	
	@Override
	public JobStatus findCompleteStatus() {
		return getFirstResult(cb ->{
			CriteriaQuery<JobStatus> cq = cb.createQuery(JobStatus.class);
			Root<JobStatus> root = cq.from(JobStatus.class);
			cq.where(cb.isTrue(root.get(JobStatus_.complete)));
			return cq;
		}).orElse(null);
	}
	
	@Override
	public JobStatus findReadyToInvoiceStatus() {
		return getFirstResult(cb ->{
			CriteriaQuery<JobStatus> cq = cb.createQuery(JobStatus.class);
			Root<JobStatus> root = cq.from(JobStatus.class);
			cq.where(cb.isTrue(root.get(JobStatus_.readyToInvoice)));
			return cq;
		}).orElse(null);
	}
	
	@Override
	public List<KeyValueIntegerString> getKeyValueList(Collection<Integer> jobStatusIds, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class);
			Root<JobStatus> root = cq.from(JobStatus.class);
			Join<JobStatus, Translation> translation = root.join(JobStatus_.nametranslations, JoinType.LEFT);
			translation.on(cb.equal(translation.get(Translation_.locale), locale));
			cq.where(root.get(JobStatus_.statusid).in(jobStatusIds));
			
			CompoundSelection<KeyValueIntegerString> selection = cb.construct(KeyValueIntegerString.class, 
					root.get(JobStatus_.statusid), translation.get(Translation_.translation)
				);
			cq.select(selection);
			return cq;
		});
	}
}