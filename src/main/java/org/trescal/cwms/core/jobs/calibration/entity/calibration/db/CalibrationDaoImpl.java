package org.trescal.cwms.core.jobs.calibration.entity.calibration.db;

import org.apache.commons.lang3.tuple.Triple;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToSubdivDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.Department_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationDTO;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration_;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration_;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess_;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationstatus.CalibrationStatus_;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink_;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsed_;
import org.trescal.cwms.core.jobs.calibration.form.CalibrationSearchForm;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate_;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability_;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.Capability_;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory_;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.StringJpaUtils;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;

import static org.trescal.cwms.core.tools.StringJpaUtils.ilike;

@Repository("CalibrationDao")
public class CalibrationDaoImpl extends AllocatedToSubdivDaoImpl<Calibration, Integer> implements CalibrationDao {

    @Override
    protected Class<Calibration> getEntity() {
        return Calibration.class;
    }

    public void completeCalibration(Calibration calibration, String remark) {
        this.update(calibration);
    }

    @Override
    public Calibration findBatchCalibrationByPosition(int batchCalId, int position) {
        return getFirstResult(cb -> {
            CriteriaQuery<Calibration> cq = cb.createQuery(Calibration.class);
            Root<Calibration> root = cq.from(Calibration.class);
			Join<Calibration, BatchCalibration> batch = root.join(Calibration_.batch);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(Calibration_.batchPosition), position));
			clauses.getExpressions().add(cb.equal(batch.get(BatchCalibration_.id), batchCalId));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

	@Override
	public Calibration findCalibrationByXindiceKey(String xindiceKey) {
		return getFirstResult(cb ->{
			CriteriaQuery<Calibration> cq = cb.createQuery(Calibration.class);
			Root<Calibration> root = cq.from(Calibration.class);
			cq.where(cb.equal(root.get(Calibration_.xindiceKey), xindiceKey));
			return cq;
		}).orElse(null);
	}

	public Calibration findEagerCalibration(int id) {
		Criteria crit = this.getSession().createCriteria(Calibration.class);
		crit.add(Restrictions.idEq(id));
		crit.setFetchMode("ji", FetchMode.JOIN);
		crit.setFetchMode("ji.job", FetchMode.JOIN);
		crit.setFetchMode("ji.job.con", FetchMode.JOIN);
		crit.setFetchMode("ji.job.returnTo", FetchMode.JOIN);

		return (Calibration) crit.uniqueResult();
	}

	@Override
	public Calibration findOnHoldCalibrationForItem(int jobItemId) {
		return getFirstResult(cb ->{
			CriteriaQuery<Calibration> cq = cb.createQuery(Calibration.class);
			Root<Calibration> root = cq.from(Calibration.class);
			Join<Calibration, CalLink> links = root.join(Calibration_.links);
			Join<CalLink, JobItem> ji = links.join(CalLink_.ji);
			Join<Calibration, CalibrationStatus> status = root.join(Calibration_.status);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(ji.get(JobItem_.jobItemId), jobItemId));
			clauses.getExpressions().add(cb.equal(status.get(CalibrationStatus_.name), CalibrationStatus.ON_HOLD));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

	public void flushSession() {
		this.getSession().flush();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Calibration> getAllCalibrationsForJobAtStatus(int jobId, String statusName) {
		Criteria crit = this.getSession().createCriteria(Calibration.class);
		crit.createCriteria("status").add(Restrictions.eq("name", statusName));
		crit.createCriteria("links").createCriteria("ji").createCriteria("job").add(Restrictions.idEq(jobId));
		crit.addOrder(Order.asc("id"));
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return crit.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Calibration> getAllCalibrationsForJobAtStatusUsingProc(int jobId, String statusName, int procid) {
		Criteria crit = this.getSession().createCriteria(Calibration.class);
		crit.createCriteria("capability").add(Restrictions.idEq(procid));
		crit.createCriteria("status").add(Restrictions.eq("name", statusName));
		crit.createCriteria("links").createCriteria("ji").createCriteria("job").add(Restrictions.idEq(jobId));
		crit.addOrder(Order.asc("id"));
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Calibration> getAllCalibrationsPerformedWithInstrumentStandard(int plantid, Integer page,
			Integer resPerPage) {
		Criteria crit = this.getSession().createCriteria(Calibration.class);
		crit.createCriteria("standardsUsed").createCriteria("inst").add(Restrictions.idEq(plantid));

		// eagerly load
		crit.setFetchMode("calType", FetchMode.JOIN);
		crit.setFetchMode("calType.serviceType", FetchMode.JOIN);
		crit.setFetchMode("completedBy", FetchMode.JOIN);
		crit.setFetchMode("calProcess", FetchMode.JOIN);
		crit.setFetchMode("capability", FetchMode.JOIN);
		crit.setFetchMode("links", FetchMode.JOIN);
		crit.setFetchMode("links.ji", FetchMode.JOIN);
		crit.setFetchMode("links.ji.job", FetchMode.JOIN);
		crit.setFetchMode("certs", FetchMode.JOIN);
		crit.setFetchMode("certs.links", FetchMode.JOIN);
		crit.setFetchMode("certs.status", FetchMode.JOIN);
		crit.setFetchMode("status", FetchMode.JOIN);

		// ordering
		crit.addOrder(Order.desc("completeTime"));

		// crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		// apply paging
		if (page != null) {
			resPerPage = resPerPage == null ? Constants.RESULTS_PER_PAGE : resPerPage;

			crit.setFirstResult((resPerPage * page) - resPerPage);
			crit.setMaxResults(resPerPage);
		}

		return crit.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Calibration> getAllOngoingCalibrationsByContact(int personid) {
		Criteria crit = this.getSession().createCriteria(Calibration.class);
		crit.createCriteria("startedBy").add(Restrictions.idEq(personid));
		crit.createCriteria("status").add(Restrictions.eq("name", CalibrationStatus.ON_GOING));
		crit.addOrder(Order.asc("startTime"));
		return crit.list();
	}

	@Override
	public PagedResultSet<CalibrationDTO> searchCalibrations(CalibrationSearchForm form, PagedResultSet<CalibrationDTO> prs,
			Subdiv allocatedSubdiv, Locale locale) {

        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<CalibrationDTO> cq = cb.createQuery(CalibrationDTO.class);
        Root<Calibration> root = cq.from(Calibration.class);
        Join<Calibration, CalLink> calLink = root.join(Calibration_.links);//
        Join<CalLink, JobItem> ji = calLink.join(CalLink_.ji);//
        Join<JobItem, Instrument> inst = ji.join(JobItem_.inst);//
        Join<Instrument, InstrumentModel> model = inst.join(Instrument_.model, JoinType.LEFT);
        Join<Calibration, Capability> proc = root.join(Calibration_.capability);//
        Join<Calibration, Contact> calContact = root.join(Calibration_.startedBy, JoinType.LEFT);
        Join<Calibration, Subdiv> subdiv = root.join(Calibration_.ORGANISATION);
        Join<Calibration, CalibrationStatus> status = root.join(Calibration_.status, JoinType.LEFT);
        Join<Calibration, CalibrationType> calType = root.join(Calibration_.calType, JoinType.LEFT);
        Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType, JoinType.LEFT);
        Join<JobItem, Job> job = ji.join(JobItem_.job, JoinType.LEFT);
        Join<Calibration, CalibrationProcess> calProcess = root.join(Calibration_.calProcess, JoinType.LEFT);

        Predicate clauses = cb.conjunction();
        clauses.getExpressions().add(cb.equal(subdiv, allocatedSubdiv));
        // status
			if (form.getStatusId() != null) {
				clauses.getExpressions().add(cb.equal(status.get(CalibrationStatus_.statusid), form.getStatusId()));
			}
			// cal type
			if (form.getCalTypeId() != null) {
				
				clauses.getExpressions().add(cb.equal(calType.get(CalibrationType_.calTypeId), form.getCalTypeId()));
			}
			// cal process
			if (form.getCalProId() != null) {
				clauses.getExpressions().add(cb.equal(calProcess.get(CalibrationProcess_.id), form.getCalProId()));
			}
			// job no
			if ((form.getJobNo() != null) && (!form.getJobNo().trim().isEmpty())) {
				clauses.getExpressions().add(ilike(cb, job.get(Job_.jobno), form.getJobNo().trim(), MatchMode.ANYWHERE));
			}
			// procedure
			if (form.getProcedureId() != null) {
                clauses.getExpressions().add(cb.equal(proc.get(Capability_.id), form.getProcedureId()));
			}
			// department
			if (form.getDeptId() != null) {
                Join<Capability, CategorisedCapability> categories = proc.join(Capability_.categories);
                Join<CategorisedCapability, CapabilityCategory> category = categories.join(CategorisedCapability_.category);
                Join<CapabilityCategory, Department> department = category.join(CapabilityCategory_.department);
                clauses.getExpressions().add(cb.equal(department.get(Department_.deptid), form.getDeptId()));
            }
			// started by
			if (form.getPersonid() != null) {
				clauses.getExpressions().add(cb.equal(root.get(Calibration_.startedBy), form.getPersonid()));
			}
			// start date
			if (form.getStartDate1() != null) {
				Date date1, date2;
				date1 = DateTools.setTimeToZeros(form.getStartDate1());
				Calendar cal = new GregorianCalendar();

				if (form.isStartDateBetween()) {
					cal.setTime(form.getStartDate2());
				} else {
					cal.setTime(form.getStartDate1());
				}

				cal.add(Calendar.DAY_OF_MONTH, 1);
				date2 = DateTools.setTimeToZeros(cal.getTime());
				clauses.getExpressions().add(cb.between(root.get(Calibration_.startTime), date1, date2));
			}
			// barcode
			if ((form.getBarcode() != null) && !form.getBarcode().trim().isEmpty()) {
				try {
					clauses.getExpressions().add(cb.equal(inst.get(Instrument_.plantid), Integer.parseInt(form.getBarcode())));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
			// standard barcode?
			if ((form.getStandardBarcode() != null) && !form.getStandardBarcode().trim().isEmpty()) {
				// create criteria for standards
				
				Join<Calibration, StandardUsed> standardUsed = root.join(Calibration_.standardsUsed);
				Join<StandardUsed, Instrument> standardUsedInst = standardUsed.join(StandardUsed_.inst);
				try {
					clauses.getExpressions().add(cb.equal(standardUsedInst.get(Instrument_.plantid), Integer.parseInt(form.getStandardBarcode())));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
			if (form.getModelid() != null) {	
				
				clauses.getExpressions().add(cb.equal(model.get(InstrumentModel_.modelid), form.getModelid()));
			}
			
			Subquery<Long> standardUsedsCount = cq.subquery(Long.class);
			Root<StandardUsed> standardUsedRoot = standardUsedsCount.from(StandardUsed.class);
			standardUsedsCount.where(cb.equal(standardUsedRoot.get(StandardUsed_.calibration), root));
			standardUsedsCount.select(cb.count(standardUsedRoot));
			
			cq.where(clauses);
        cq.distinct(true);
        cq.select(cb.construct(CalibrationDTO.class,
            root.get(Calibration_.id),
            root.get(Calibration_.startTime),
            calProcess.get(CalibrationProcess_.process),
            joinTranslation(cb, serviceType, ServiceType_.shortnameTranslation, locale),
            proc.get(Capability_.reference),
            job.get(Job_.jobid), job.get(Job_.jobno),
            ji.get(JobItem_.jobItemId), ji.get(JobItem_.itemNo),
            StringJpaUtils.trimAndConcatWithWhitespace(calContact.get(Contact_.firstName), calContact.get(Contact_.lastName)).apply(cb),
            joinTranslation(cb, model, InstrumentModel_.nameTranslations, locale),
            joinTranslation(cb, status, CalibrationStatus_.nametranslations, locale),
            standardUsedsCount.getSelection()
        ));
        cq.orderBy(cb.desc(root.get(Calibration_.startTime)));
			
			TypedQuery<CalibrationDTO> query = getEntityManager().createQuery(cq);
			prs.setResultsCount(query.getResultList().size());
			query.setFirstResult(prs.getStartResultsFrom());
			query.setMaxResults(prs.getResultsPerPage());
			prs.setResults(query.getResultList());
		
		return prs;
	}

	/**
	 * Starts a calibration
	 * 
	 * @param calibration
	 *            the calibration to start
	 * @param resuming
	 *            used by the the Hooks/Interceptors to determine whether or not
	 *            this cal is being started for the first time or whether it is
	 *            being resumed from being on hold
	 */
	public void startCalibration(Calibration calibration, boolean resuming) {
		this.update(calibration);
	}

	@Override
	public Calibration findLastCalibrationForJobitemByCalStatus(int jobitemid, CalibrationStatus calStatus) {

		Criteria crit = getSession().createCriteria(Calibration.class, "cal");
		crit.createAlias("cal." + Calibration_.links.getName(), "link");
		crit.createAlias("link." + CalLink_.ji.getName(), "ji");
		crit.createAlias("cal." + Calibration_.status.getName(), "status");

		crit.add(Restrictions.eq("ji." + JobItem_.jobItemId.getName(), jobitemid));
		if (calStatus != null)
			crit.add(Restrictions.eq("status." + CalibrationStatus_.name.getName(), calStatus.getName()));

		crit.addOrder(Order.desc("cal." + Calibration_.calDate.getName()));
		crit.setMaxResults(1);

		return (Calibration) crit.uniqueResult();

	}
	
	@Override
	public PagedResultSet<CalibrationDTO> getCalibrationsPerformed(Integer plantId, 
			Date startCalDate, Date endCalDate, Company allocatedCompany, Subdiv allocatedSubdiv, 
			Locale locale, PagedResultSet<CalibrationDTO> prs) {
		completePagedResultSet(prs, CalibrationDTO.class, cb -> cq -> {
            Root<Instrument> root = cq.from(Instrument.class);
            Join<Instrument, StandardUsed> standardUsed = root.join(Instrument_.calibrations);
            Join<StandardUsed, Calibration> calibration = standardUsed.join(StandardUsed_.calibration);
            Join<Calibration, CalibrationProcess> calibrationProcess = calibration.join(Calibration_.calProcess, JoinType.LEFT);
            Join<Calibration, Capability> capability = calibration.join(Calibration_.capability, JoinType.LEFT);
            Join<Calibration, CalibrationType> calType = calibration.join(Calibration_.calType, JoinType.LEFT);
            Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType, JoinType.LEFT);
            Join<Calibration, CalLink> calLink = calibration.join(Calibration_.links);
            Join<CalLink, JobItem> jobItem = calLink.join(CalLink_.ji);
            Join<JobItem, CertLink> certLinks = jobItem.join(JobItem_.certLinks, JoinType.LEFT);
            Join<CertLink, Certificate> certifcates = certLinks.join(CertLink_.cert, JoinType.LEFT);
            Join<JobItem, Job> job = jobItem.join(JobItem_.job);
            Join<JobItem, Instrument> inst = jobItem.join(JobItem_.inst);
            Join<Instrument, Mfr> instMfr = inst.join(Instrument_.mfr, JoinType.LEFT);
            Join<Instrument, InstrumentModel> model = inst.join(Instrument_.model);
            Join<InstrumentModel, Translation> modelName = model.join(InstrumentModel_.nameTranslations,
					javax.persistence.criteria.JoinType.LEFT);
			modelName.on(cb.equal(modelName.get(Translation_.locale), locale));
			Join<InstrumentModel, Mfr> modelMfr = model.join(InstrumentModel_.mfr, JoinType.LEFT);
			Join<Job, Contact> clientContact = job.join(Job_.con, javax.persistence.criteria.JoinType.LEFT);
			Join<Contact, Subdiv> clientSubdiv = clientContact.join(Contact_.sub);
			Join<Subdiv, Company> clientCompany = clientSubdiv.join(Subdiv_.comp);
			Join<Company, CompanySettingsForAllocatedCompany> settings = clientCompany
					.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			settings.on(cb.equal(settings.get(CompanySettingsForAllocatedCompany_.organisation), allocatedCompany));
			
			Predicate clauses = cb.conjunction();
			
			if(plantId != null){
				clauses.getExpressions().add(cb.equal(root.get(Instrument_.plantid), plantId));
			}

            if (startCalDate != null) {
                clauses.getExpressions().add(cb.greaterThanOrEqualTo(calibration.get(Calibration_.startTime), startCalDate));
            }

            if (endCalDate != null) {
                clauses.getExpressions().add(cb.lessThanOrEqualTo(calibration.get(Calibration_.completeTime), endCalDate));
            }

            cq.where(clauses);
            CompoundSelection<CalibrationDTO> selection = cb.construct(CalibrationDTO.class,
                calibration.get(Calibration_.calDate),
                calibrationProcess.get(CalibrationProcess_.process),
                joinTranslation(cb, serviceType, ServiceType_.shortnameTranslation, locale),
                capability.get(Capability_.reference),
                certifcates.get(Certificate_.certid),
                certifcates.get(Certificate_.certno),
                job.get(Job_.jobid),
                job.get(Job_.jobno),
                jobItem.get(JobItem_.itemNo),
                clientCompany.get(Company_.coid),
                clientCompany.get(Company_.coname),
                settings.get(CompanySettingsForAllocatedCompany_.active),
                settings.get(CompanySettingsForAllocatedCompany_.onStop),
                clientSubdiv.get(Subdiv_.subdivid),
                clientSubdiv.get(Subdiv_.subname),
                clientSubdiv.get(Subdiv_.active),
                clientContact.get(Contact_.firstName),
                clientContact.get(Contact_.lastName),
                inst.get(Instrument_.plantid),
                modelName.get(Translation_.translation),
                cb.coalesce(modelMfr.get(Mfr_.name), instMfr.get(Mfr_.name)),
                cb.coalesce(model.get(InstrumentModel_.model), inst.get(Instrument_.modelname)),
                inst.get(Instrument_.plantno),
                inst.get(Instrument_.serialno));
			cq.distinct(true);
			
			List<javax.persistence.criteria.Order> order = new ArrayList<>();
			order.add(cb.asc(calibration.get(Calibration_.calDate)));
			
			return Triple.of(root, selection, order);
		});

		return prs;
	}
	
	@Override
	public List<CalibrationDTO> getCalibrationsPerformed_(Integer plantId, Date startCalDate, Date endCalDate){
		Locale locale = LocaleContextHolder.getLocale();
		return getResultList(cb -> {
            CriteriaQuery<CalibrationDTO> cq = cb.createQuery(CalibrationDTO.class);
            Root<Instrument> root = cq.from(Instrument.class);
            Join<Instrument, StandardUsed> standardUsed = root.join(Instrument_.calibrations);
            Join<StandardUsed, Calibration> calibration = standardUsed.join(StandardUsed_.calibration);
            Join<Calibration, CalibrationProcess> calibrationProcess = calibration.join(Calibration_.calProcess, JoinType.LEFT);
            Join<Calibration, Capability> capability = calibration.join(Calibration_.capability, JoinType.LEFT);
            Join<Calibration, CalibrationType> calType = calibration.join(Calibration_.calType, JoinType.LEFT);
            Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType, JoinType.LEFT);
            Join<Calibration, CalLink> calLink = calibration.join(Calibration_.links);
            Join<CalLink, JobItem> jobItem = calLink.join(CalLink_.ji);
            Join<JobItem, CertLink> certLinks = jobItem.join(JobItem_.certLinks, JoinType.LEFT);
            Join<CertLink, Certificate> certifcates = certLinks.join(CertLink_.cert, JoinType.LEFT);
            Join<JobItem, Job> job = jobItem.join(JobItem_.job);
            Join<JobItem, Instrument> inst = jobItem.join(JobItem_.inst);
            Join<Instrument, Mfr> instMfr = inst.join(Instrument_.mfr, JoinType.LEFT);
            Join<Instrument, InstrumentModel> model = inst.join(Instrument_.model);
            Join<InstrumentModel, Translation> modelName = model.join(InstrumentModel_.nameTranslations,
					javax.persistence.criteria.JoinType.LEFT);
			modelName.on(cb.equal(modelName.get(Translation_.locale), locale));
			Join<InstrumentModel, Mfr> modelMfr = model.join(InstrumentModel_.mfr, JoinType.LEFT);
			Join<Job, Contact> clientContact = job.join(Job_.con, javax.persistence.criteria.JoinType.LEFT);
			Join<Contact, Subdiv> clientSubdiv = clientContact.join(Contact_.sub);
			Join<Subdiv, Company> clientCompany = clientSubdiv.join(Subdiv_.comp);
			
			Predicate clauses = cb.conjunction();
			
			if(plantId != null){
				clauses.getExpressions().add(cb.equal(root.get(Instrument_.plantid), plantId));
			}

            if (startCalDate != null) {
                clauses.getExpressions().add(cb.greaterThanOrEqualTo(calibration.get(Calibration_.startTime), startCalDate));
            }

            if (endCalDate != null) {
                clauses.getExpressions().add(cb.lessThanOrEqualTo(calibration.get(Calibration_.completeTime), endCalDate));
            }

            cq.where(clauses);
            cq.distinct(true);
            cq.select(cb.construct(CalibrationDTO.class,
                calibration.get(Calibration_.calDate),
                calibrationProcess.get(CalibrationProcess_.process),
                joinTranslation(cb, serviceType, ServiceType_.shortnameTranslation, locale),
                capability.get(Capability_.reference),
                certifcates.get(Certificate_.certno),
                job.get(Job_.jobno),
                jobItem.get(JobItem_.itemNo),
                clientCompany.get(Company_.coname),
                clientSubdiv.get(Subdiv_.subname),
                clientContact.get(Contact_.firstName),
                clientContact.get(Contact_.lastName),
                inst.get(Instrument_.plantid),
                modelName.get(Translation_.translation),
                cb.coalesce(modelMfr.get(Mfr_.name), instMfr.get(Mfr_.name)),
                cb.coalesce(model.get(InstrumentModel_.model), inst.get(Instrument_.modelname)),
                inst.get(Instrument_.plantno),
                inst.get(Instrument_.serialno)));
            return cq;
        });
	}

}