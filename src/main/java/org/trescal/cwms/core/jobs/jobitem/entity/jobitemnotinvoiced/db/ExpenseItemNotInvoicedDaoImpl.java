package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobExpenseItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobExpenseItemNotInvoiced_;
import org.trescal.cwms.core.jobs.jobitem.projection.JobExpenseItemNotInvoicedDTO;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;

@Repository
public class ExpenseItemNotInvoicedDaoImpl extends BaseDaoImpl<JobExpenseItemNotInvoiced, Integer>
		implements ExpenseItemNotInvoicedDao {

	@Override
	protected Class<JobExpenseItemNotInvoiced> getEntity() {
		return JobExpenseItemNotInvoiced.class;
	}
	
	@Override
	public List<JobExpenseItemNotInvoicedDTO> getProjectionDTOs(Collection<Integer> invoiceIds, Locale locale) {
		if (invoiceIds == null || invoiceIds.isEmpty()) 
			throw new IllegalArgumentException("At least one invoiceId must be specified");
		return getResultList(cb -> {
			CriteriaQuery<JobExpenseItemNotInvoicedDTO> cq = cb.createQuery(JobExpenseItemNotInvoicedDTO.class);
			Root<JobExpenseItemNotInvoiced> root = cq.from(JobExpenseItemNotInvoiced.class);
			Join<JobExpenseItemNotInvoiced, JobExpenseItem> expenseItem = root.join(JobExpenseItemNotInvoiced_.expenseItem, JoinType.INNER);
			Join<JobExpenseItem, Job> job = expenseItem.join(JobExpenseItem_.job, JoinType.INNER);
			Join<JobExpenseItem, ServiceType> serviceType = expenseItem.join(JobExpenseItem_.serviceType, JoinType.INNER);
			Join<JobExpenseItem, InstrumentModel> model = expenseItem.join(JobExpenseItem_.model, JoinType.INNER);
			Join<InstrumentModel, Translation> modelTranslation = model.join(InstrumentModel_.nameTranslations, JoinType.LEFT);
			modelTranslation.on(cb.equal(modelTranslation.get(Translation_.locale), locale));
			Join<JobExpenseItemNotInvoiced, Contact> contact = root.join(JobExpenseItemNotInvoiced_.contact, JoinType.INNER);
			Join<JobExpenseItemNotInvoiced, Invoice> periodicInvoice = root.join(JobExpenseItemNotInvoiced_.periodicInvoice, JoinType.INNER);
			
			cq.where(periodicInvoice.get(Invoice_.id).in(invoiceIds));
			
			cq.select(cb.construct(JobExpenseItemNotInvoicedDTO.class,
				root.get(JobExpenseItemNotInvoiced_.id),
				root.get(JobExpenseItemNotInvoiced_.date),
				contact.get(Contact_.personid),
				root.get(JobExpenseItemNotInvoiced_.reason),
				root.get(JobExpenseItemNotInvoiced_.comments),
				periodicInvoice.get(Invoice_.id),
				expenseItem.get(JobExpenseItem_.id),
				expenseItem.get(JobExpenseItem_.itemNo),
				serviceType.get(ServiceType_.serviceTypeId),
				model.get(InstrumentModel_.modelid),
				modelTranslation.get(Translation_.translation),
				expenseItem.get(JobExpenseItem_.singlePrice),
				expenseItem.get(JobExpenseItem_.quantity),
				expenseItem.get(JobExpenseItem_.clientRef),
				expenseItem.get(JobExpenseItem_.comment),
				expenseItem.get(JobExpenseItem_.date),
				expenseItem.get(JobExpenseItem_.invoiceable),
				job.get(Job_.jobid)
				));
			
			return cq;
		});
	}
}