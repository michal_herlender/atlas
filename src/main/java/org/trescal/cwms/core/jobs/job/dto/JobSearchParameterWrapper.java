package org.trescal.cwms.core.jobs.job.dto;

import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;

public class JobSearchParameterWrapper {

	private Boolean active;
	private String clientref;
	private String company;
	private String subdiv;
	private String contact;
	private String desc;
	private Boolean highlight;
	private String jobno;
	private JobType jobType;
	private String mfr;
	private String model;
	private String plantid;
	private String plantno;
	private String procno;
	private String purchaseorder;
	private String serialno;

	public Boolean getActive() {
		return this.active;
	}

	public String getClientref() {
		return this.clientref;
	}

	public String getCompany() {
		return this.company;
	}

	public String getSubdiv() {
		return subdiv;
	}

	public String getContact() {
		return this.contact;
	}

	public String getDesc() {
		return this.desc;
	}

	public Boolean getHighlight() {
		return this.highlight;
	}

	public String getJobno() {
		return this.jobno;
	}

	public JobType getJobType() {
		return this.jobType;
	}

	public String getMfr() {
		return this.mfr;
	}

	public String getModel() {
		return this.model;
	}

	public String getPlantid() {
		return this.plantid;
	}

	public String getPlantno() {
		return this.plantno;
	}

	public String getProcno() {
		return this.procno;
	}

	public String getPurchaseorder() {
		return this.purchaseorder;
	}

	public String getSerialno() {
		return this.serialno;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public void setClientref(String clientref) {
		this.clientref = clientref;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setSubdiv(String subdiv) {
		this.subdiv = subdiv;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setHighlight(Boolean highlight) {
		this.highlight = highlight;
	}

	public void setJobno(String jobno) {
		this.jobno = jobno;
	}

	public void setJobType(JobType jobType) {
		this.jobType = jobType;
	}

	public void setMfr(String mfr) {
		this.mfr = mfr;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public void setPlantid(String plantid) {
		this.plantid = plantid;
	}

	public void setPlantno(String plantno) {
		this.plantno = plantno;
	}

	public void setProcno(String procno) {
		this.procno = procno;
	}

	public void setPurchaseorder(String purchaseorder) {
		this.purchaseorder = purchaseorder;
	}

	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}
}