package org.trescal.cwms.core.jobs.jobitem.controller;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.AutoPopulatingList;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.db.ThreadTypeService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.instrumentmodel.entity.uom.db.UoMService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.BasketItemDto;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.ItemBasketWrapper;
import org.trescal.cwms.core.jobs.jobitem.form.AddNewItemsToJobForm;
import org.trescal.cwms.core.jobs.jobitem.form.AddNewItemsToJobValidator;
import org.trescal.cwms.core.jobs.jobitem.form.ZippedAddNewItems;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;
import org.trescal.cwms.core.login.entity.userpreferences.db.UserPreferencesService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.DisallowSamePlantNumberDefaultType;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.spring.helpers.IntegerElementFactory;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY })
@Slf4j
public class AddNewItemsToJobController extends AbstractNewJobItemController {
	@Value("#{props['cwms.config.newitem.submit.display_confirmation']}")
	private boolean confirmOnSubmit;
	@Value("#{props['cwms.config.goodsin.update_cal_freq']}")
	private boolean updateCalFrequency;
	@Value("#{props['cwms.config.instrument.use_threads']}")
	private boolean useThreads;
	@Autowired
	private UserService userService;
    @Autowired
    private ThreadTypeService threadTypeServ;
    @Autowired
    private UoMService uomServ;
    @Autowired
    private AddressService addrServ;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private DisallowSamePlantNumberDefaultType disallowSamePlantNumber;
    @Autowired
    private UserPreferencesService userPreferencesService;

    @Autowired
    private AddNewItemsToJobValidator validator;

    @InitBinder("form")
    protected void initBinder(ServletRequestDataBinder binder) throws Exception {
        MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.dtf_ISO8601, true);
        binder.registerCustomEditor(Date.class, editor);
        binder.setValidator(validator);
    }

    private void addInstrumentsToBasketItems(List<ItemBasketWrapper> basketItems,
                                             Map<Integer, Instrument> newInstruments) {
		for (Integer i : newInstruments.keySet()) {
			ItemBasketWrapper ibw = basketItems.get(i);
			Instrument instrument = newInstruments.get(i);
			ibw.setInst(instrument);
		}
	}

    @ModelAttribute("form")
    protected AddNewItemsToJobForm formBackingObject(@RequestParam(value = "jobid") Integer jobId,
                                                     @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                                                     @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto
    ) throws Exception {

        // get logged in user
        Contact loggedInUser = this.userService.get(username).getCon();

        Job job = super.jobServ.get(jobId);

        Date dateIn;
        if (jiServ.getDateIn(job) != null)
            dateIn = DateTools.dateFromZonedDateTime(jiServ.getDateIn(job));
        else
			dateIn = new Date();

		AddNewItemsToJobForm form = new AddNewItemsToJobForm();
		// AutoPopulatingList does not auto-instantiate (this used in post)
		form.setBasketBaseItemIds(new AutoPopulatingList<>(i -> 0));
        form.setBasketExistingGroupIds(new AutoPopulatingList<>(i -> 0));
        form.setBasketIds(new AutoPopulatingList<>(i -> 0));
        form.setBasketParts(new AutoPopulatingList<>(i -> ""));
        form.setBasketTempGroupIds(new AutoPopulatingList<>(i -> 0));
        form.setBasketTypes(new AutoPopulatingList<>(i -> ""));
        form.setBasketQuotationItemIds(new AutoPopulatingList<>(i -> 0));
        form.setServiceTypeIds(new AutoPopulatingList<>(new IntegerElementFactory(0)));
        form.setServiceTypeSources(new AutoPopulatingList<>(new IntegerElementFactory(0)));
        form.setDatesIn(new AutoPopulatingList<>(i -> dateIn));
        form.setBookInByContactIds(new AutoPopulatingList<>(i -> loggedInUser.getPersonid()));
        form.setBussinessCompanyId(companyDto.getKey());
        return form;
    }

	/**
	 * Initializes the model map for the addnewitemstojob GET view. This is not done
	 * via individual @ModelAttribute methods for performance reasons; some of these
	 * perform queries and there's no need to do them on a POST
	 * 
	 */
	private void initializeModel(Model model, Job job, Locale locale) {
        model.addAttribute("units", IntervalUnit.getUnitsForSelect());
        model.addAttribute("job", job);
        model.addAttribute("updateCalFrequency", this.updateCalFrequency);
        model.addAttribute("useThreads", this.useThreads);
        model.addAttribute("confirmOnSubmit", this.confirmOnSubmit);
        model.addAttribute("allowContractReview", false);
        model.addAttribute("threadTypes", this.useThreads ? this.threadTypeServ.getAll() : Collections.emptyList());
        model.addAttribute("uoms", this.uomServ.getAllUoMs());
        model.addAttribute("rangeUnits", this.uomServ.getAllUoMs().stream().map(u -> new KeyValue<>(u.getId(), u.getFormattedSymbol())).collect(Collectors.toList()));
        model.addAttribute("serviceTypes",
            this.serviceTypeService.getDTOList(DomainType.INSTRUMENTMODEL, job.getType(), locale, false));
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/addnewitemstojob.htm", method = RequestMethod.GET)
    protected String showView(Locale locale, Model model, @RequestParam(value = "jobid") Integer jobId,
                              @ModelAttribute("form") AddNewItemsToJobForm form,
                              @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        // Model contains flashAttributes, these won't auto-bind to form though
        // (even if named the same)
        Map<String, Object> modelMap = model.asMap();
        // If user has refreshed URL, than flash map is gone, no basket info -
        // return to job page.
        if (!modelMap.containsKey("flashTypes")) {
            return "redirect:newjobitemsearch.htm?jobid=" + jobId;
        } else {
            form.setBasketBaseItemIds((AutoPopulatingList<Integer>) modelMap.get("flashBaseItemIds"));
            log.debug("test: " + modelMap.get("flashBaseItemIds"));
            form.setBasketExistingGroupIds((AutoPopulatingList<Integer>) modelMap.get("flashExistingGroupIds"));
            form.setBasketIds((AutoPopulatingList<Integer>) modelMap.get("flashIds"));
            form.setBasketParts((AutoPopulatingList<String>) modelMap.get("flashParts"));
            form.setBasketTempGroupIds((AutoPopulatingList<Integer>) modelMap.get("flashTempGroupIds"));
            form.setBasketTypes((AutoPopulatingList<String>) modelMap.get("flashTypes"));
            form.setBasketQuotationItemIds((AutoPopulatingList<Integer>) modelMap.get("flashQuotationItemIds"));
            form.setServiceTypeIds((AutoPopulatingList<Integer>) modelMap.get("flashServiceTypeIds"));
            form.setServiceTypeSources((AutoPopulatingList<Integer>) modelMap.get("flashServiceTypeSources"));
            form.setDatesIn((AutoPopulatingList<Date>) modelMap.get("flashDatesIn"));
            form.setBookInByContactIds((AutoPopulatingList<Integer>) modelMap.get("flashBookInByContactIds"));
            Job job = super.jobServ.get(jobId);
            initializeModel(model, job, locale);
            model.addAttribute("disallowSamePlantNumber",
                    disallowSamePlantNumber.getValueByScope(Scope.COMPANY, job.getCon().getSub().getComp(), null));
            val baskteItems = newBasketItems(form, job, userPreferencesService.findUserPreferencesByUsername(username));
            model.addAttribute("contacts", job.getCon().getSub().getContacts().stream()
                    .filter(Contact::isActive).map(c -> new KeyValue<>(c.getPersonid(), c.getName())).collect(Collectors.toList()));
            model.addAttribute("addresses", job.getReturnTo().getSub().getAddresses().stream()
                    .filter(Address::getActive)
                    .map(a -> new KeyValue<>(a.getAddrid(), a.getAddr1() + ", " + a.getTown())));
            model.addAttribute("newBasketItems", baskteItems);
            super.initializeBasketItems(model, form, job, false);
            return "trescal/core/jobs/jobitem/addnewitemstojob";
        }
    }

    private List<BasketItemDto> newBasketItems(AddNewItemsToJobForm form, Job job, UserPreferences userPreferences) {
        return form.getZippedLists().stream().map(this.createBasketItem(job, userPreferences)).collect(Collectors.toList());
    }

    private Function<ZippedAddNewItems, BasketItemDto> createBasketItem(Job job, UserPreferences userPreferences) {
        return (ZippedAddNewItems zipped) ->
        {
            val locale = LocaleContextHolder.getLocale();
            val basketItem = BasketItemDto.builder();
            Quotationitem qItem = null;
            if (zipped.getBasketQuotationItemId() != 0) {
                qItem = this.qiService.findQuotationItem(zipped.getBasketQuotationItemId());
            }
            basketItem.quotationItem(getDto(qItem, locale));

            Boolean isCompatible = false;
            Instrument inst = null;
            InstrumentModel model = null;
            if (zipped.getBasketType().equals("instrument")) {
                inst = this.instServ.get(zipped.getBasketId());
                basketItem.instId(inst.getPlantid());
                basketItem.plantNo(inst.getPlantno());
                basketItem.serialNo(inst.getSerialno());
                basketItem.calFrequency(inst.getCalFrequency());
                basketItem.calFrequencyUnit(inst.getCalFrequencyUnit());
                basketItem.modelName(InstModelTools.instrumentModelNameViaTranslations(inst, false, locale, Locale.getDefault()));
                basketItem.modelType(inst.getModel().getModelMfrType().name());
                basketItem.modelId(inst.getModel().getModelid());
                basketItem.mfrName(inst.getModel().getMfr().getName());
                basketItem.mfrId(inst.getModel().getMfr().getMfrid());
                if (inst.getDefaultServiceType() != null) {
                    // If defaultServiceTypeUsage = true, we allow only the default job type
                    isCompatible = calibrationTypeService.isCompatibleWithJobType(
                        inst.getDefaultServiceType().getCalibrationType(), job.getType());
                }
            } else if (zipped.getBasketType().equals("model")) {
                model = this.modelServ.findInstrumentModel(zipped.getBasketId());
                basketItem.modelType(model.getModelMfrType().name());
                basketItem.mfrId(model.getMfr().getMfrid());
                basketItem.mfrName(model.getMfr().getName());
                basketItem.modelId(model.getModelid());
                basketItem.modelName(InstModelTools.instrumentModelNameWithTypology(model, locale));
                basketItem.instPersonId(job.getCon().getId());
                basketItem.instAddressId(job.getReturnTo().getAddrid());
            }
            Pair<Integer, Integer> serviceTypeResult = getServiceTypeId(job, qItem, inst, isCompatible, userPreferences.getDefaultServiceTypeUsage());
            Integer serviceTypeSource = serviceTypeResult.getLeft();
            Integer serviceTypeId = serviceTypeResult.getRight();
            basketItem.serviceTypeSource(serviceTypeSource);
            basketItem.serviceTypeId(serviceTypeId);
            // Lookup the default procedure using capability service type filters
            val proc = inst != null ? this.capabilityService.getActiveCapabilityUsingCapabilityFilter
                (inst.getModel().getDescription().getId(), serviceTypeId, job.getOrganisation().getSubdivid()) : Optional.<Capability>empty();

            proc.ifPresent(p -> {
                basketItem.procId(p.getId());
                basketItem.procReference(p.getReference());
            });
            if (!proc.isPresent()) {
                // Lookup the default procedure and work instruction for the
                // instrument or model (in future reference on quotation item)
                // Note we no longer use the default procedure on instrument -
                // may not be correct, target removal
                Integer instModelId = inst == null ? model != null ? model.getModelid() : 0
                    : inst.getModel().getModelid();
                List<ServiceCapability> capabilities = this.serviceCapabilityService.find(instModelId, serviceTypeId,
                    null, job.getOrganisation().getSubdivid());
                if (!capabilities.isEmpty()) {
                    // Otherwise null; consider putting id / name into form from
                    // projection
                    ServiceCapability capability = capabilities.get(0);
                    basketItem.procId(capability.getCapability().getId());
                    basketItem.procReference(capability.getCapability().getReference());
                    if (capability.getWorkInstruction() != null) {
                        basketItem.workInstructionId(capability.getWorkInstruction().getId());
                        basketItem.workInstructionTitle(capability.getWorkInstruction().getTitle());
                    }
                }
            }

            // If no type specified, the item has been deleted from the
            // basket (order/index maintained)
            basketItem.existingGroupId(zipped.getBasketExistingGroupId());
            basketItem.temporalGroupId(zipped.getBasketTempGroupId());
            basketItem.part(zipped.getBasketPart());
            basketItem.baseItemId(zipped.getBasketBaseItemId());
            return basketItem.build();
        };
    }


    @RequestMapping(value = "/addnewitemstojob.json", method = RequestMethod.POST)
    protected @ResponseBody
    Map<String, Object> onSubmit(Model model, @RequestParam(value = "jobid") Integer jobId,
                                 @RequestParam(value = "bookingInAddrId") Integer bookinInAddrId,
                                 @Validated @ModelAttribute("form") AddNewItemsToJobForm form,
                                 BindingResult bindingResult,
                                 RedirectAttributes redirectAttributes,
                                 @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username
    ) {

        if (bindingResult.hasErrors())
            return Collections.singletonMap("errors", bindingResult.getAllErrors());

        // Rebuild basket reference data
        Job job = super.jobServ.get(jobId);
        // Note - may need to add new instruments first then models in order
        List<ItemBasketWrapper> basketItems = super.initializeBasketItems(model, form, job, false);

        Address bookingInAddr = addrServ.get(bookinInAddrId);

        // get logged in user
        Contact loggedInUser = this.userService.get(username).getCon();

		// create instruments
		Map<Integer, Instrument> newInstruments = this.instServ.createInstruments(form.getInstSerialNos(),
				form.getInstPlantNos(), form.getInstPersonIds(), form.getInstAddrIds(), form.getInstMfrIds(),
				form.getInstCalFreq(), form.getInstCalUnitFreq(), form.getItemProcIds(), form.getItemWorkInstIds(),
				form.getInstStartRanges(), form.getInstEndRanges(), form.getInstUomIds(), form.getInstThreadIds(),
				form.getInstDeletes(), form.getInstModelNames(), form.getServiceTypeIds(), basketItems,
				form.getInstCustomerDescs(), loggedInUser);
		// set new instruments ready for job items
		this.addInstrumentsToBasketItems(basketItems, newInstruments);
		// create job items
		List<Integer> jobItemIds = this.jiServ.createJobItems(form.getItemProcIds(), form.getItemWorkInstIds(),
				form.getItemPOs(), basketItems, job, form.getServiceTypeIds(), form.getBookInByContactIds(),
				bookingInAddr, form.getDatesIn());

		// jobItemIds used for auto-sticker-printing
		Map<String, Object> result = new HashMap<>();
		result.put("redirectPath", "viewjob.htm?jobid=" + job.getJobid());
		result.put("jobItemIds", jobItemIds);
		return result;
	}

}
