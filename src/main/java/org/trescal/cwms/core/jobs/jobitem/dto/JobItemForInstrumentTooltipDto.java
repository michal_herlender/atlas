package org.trescal.cwms.core.jobs.jobitem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
public class JobItemForInstrumentTooltipDto {

    private String jobNo;
    private Integer jobId;
    private Integer itemNo;
    private Integer jobItemId;
    private String serviceTypeShortName;
    private Integer turn;
    private ZonedDateTime dateIn;
    private Date dateComplete;
    private BigDecimal calibrationJobReviewCost;
    private Integer jobCostingId;
    private String currencyCode;
    private BigDecimal calibrationCost;
    private BigDecimal calibrationInvoiceCost;
    private BigDecimal repairCost;
}
