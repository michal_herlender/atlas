package org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.jobs.contractreview.entity.costs.ContractReviewLinkedCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost;

@Entity
@Table(name = "contractreviewlinkedrepaircost")
public class ContractReviewLinkedRepairCost extends ContractReviewLinkedCost
{
	private ContractReviewRepairCost repairCost;
	private JobCostingRepairCost jobCostingRepairCost;

	@Transient
	public JobCostingRepairCost getJobCostingRepairCost()
	{
		return this.jobCostingRepairCost;
	}

	@OneToOne(fetch=FetchType.LAZY, cascade = javax.persistence.CascadeType.ALL)
	@JoinColumn(name = "repcostid", nullable = false, unique = true)
	public ContractReviewRepairCost getRepairCost()
	{
		return this.repairCost;
	}

	public void setJobCostingRepairCost(JobCostingRepairCost jobCostingRepairCost)
	{
		this.jobCostingRepairCost = jobCostingRepairCost;
	}

	public void setRepairCost(ContractReviewRepairCost repairCost)
	{
		this.repairCost = repairCost;
	}

}
