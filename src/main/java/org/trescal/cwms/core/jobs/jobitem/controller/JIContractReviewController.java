package org.trescal.cwms.core.jobs.jobitem.controller;

import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.contract.dtos.MatchingContractDto;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.db.ContractService;
import org.trescal.cwms.core.contract.entity.contractdemand.ContractDemand;
import org.trescal.cwms.core.contract.entity.contractdemand.db.ContractDemandService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemand.db.JobItemDemandService;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemand;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.db.ContractReviewService;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.db.ContractReviewAdjustmentCostService;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.db.ContractReviewCalibrationCostService;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.db.ContractReviewPurchaseCostService;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.db.ContractReviewRepairCostService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.dto.JobItemPODto;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.jobitem.entity.requirement.db.RequirementService;
import org.trescal.cwms.core.jobs.jobitem.form.JIContractReviewForm;
import org.trescal.cwms.core.jobs.jobitem.form.JIContractReviewValidator;
import org.trescal.cwms.core.jobs.jobitemhistory.JobItemHistory;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.CustomLocalDateTimeEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.pricing.entity.costs.dto.CostJobItemDto;
import org.trescal.cwms.core.pricing.entity.costs.dto.CostJobItemDtoComparator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CalibrationWarranty;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.RepairWarranty;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultReserveCertificate;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class JIContractReviewController extends JobItemController {
	private static final Logger logger = LoggerFactory.getLogger(JIContractReviewController.class);

	@Value("${cwms.config.contractreview.newoverlay}")
	private boolean showNewOverlay;

	@Autowired
	private ContractReviewAdjustmentCostService adjCostServ;
	@Autowired
	private ContractReviewCalibrationCostService calCostServ;
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private ContractReviewService contractRevServ;
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private ItemStateService itemStateServ;
	@Autowired
	private ContractReviewPurchaseCostService purCostServ;
	@Autowired
	private ContractReviewRepairCostService repCostServ;
	@Autowired
	private RequirementService reqServ;
	@Autowired
	private TransportOptionService transOptServ;
	@Autowired
	private JIContractReviewValidator validator;
	@Autowired
	private UserService userService;
	@Autowired
	private SystemDefaultReserveCertificate systemDefaultReserveCertificate;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ContractService contractService;
	@Autowired
	private ContractDemandService contractDemandService;
	@Autowired
	private JobItemDemandService jobItemDemandService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private CalibrationWarranty sdCalibrationWarranty;
	@Autowired
	private RepairWarranty sdRepairWarranty;
	@Autowired
	private InstrumService instrumServ;

	public static final String FORM_NAME = "form";

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.registerCustomEditor(LocalDateTime.class, new CustomLocalDateTimeEditor(DateTimeFormatter.ISO_DATE_TIME));
	}

	@ModelAttribute("contracts")
	protected List<MatchingContractDto> getMatchingContracts(
			@RequestParam(value = "jobitemid") Integer jobitemid) {
		JobItem jobItem = jobItemService.findJobItem(jobitemid);
		return contractService.findMatchingContractsForJobItem(jobItem, null);
	}

	@ModelAttribute("form")
	protected JIContractReviewForm formBackingObject(HttpServletRequest request,
			@RequestParam(value = "jobitemid") Integer jobitemid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		JobItem ji = this.jobItemService.findJobItem(jobitemid);
		if (ji == null) {
			logger.error("Jobitem with id " + jobitemid + " could not be found");
			throw new RuntimeException("Unable to locate requested jobitem");
		}
		JIContractReviewForm form = new JIContractReviewForm();
		form.setJi(ji);
		form.setSaveComments(false);
		// copy the current costs onto the form
		form.setCalTotalCost(ji.getCalibrationCost().getTotalCost());
		form.setCalibrationCost(ji.getCalibrationCost());
		form.setAdjustmentCost(ji.getAdjustmentCost());
		form.setRepairCost(ji.getRepairCost());
		form.setPurchaseCost(ji.getPurchaseCost());
		form.setInMethodId(getId(ji.getInOption()));
		form.setReturnMethodId(getId(ji.getReturnOption()));
		form.setCalType(ji.getServiceType().getCalibrationType().getCalTypeId());
		form.setClientReference(ji.getClientRef());
		form.setAgreedDelDate(ji.getAgreedDelDate());
		form.setTurnaround(ji.getTurn());
		if (ji.getClientReceiptDate() != null)
			form.setClientReceiptDate(DateTools.localDateTimeFromZonedDateTime(ji.getClientReceiptDate()));
		// If job item isn't linked to a contract set the contract id in the
		// form to the id of the contract
		// with the best match for this instrument so that select in jsp has
		// best item selected.
		Contract contract = ji.getContract();
		if (ji.getContract() == null) {
			form.setContractId(0);
		} else
			form.setContractId(contract.getId());
		Subdiv businessSubdiv = this.subdivService.get(subdivDto.getKey());
		Boolean reserveCertificate = systemDefaultReserveCertificate.parseValueHierarchical(Scope.SUBDIV,
				businessSubdiv, businessSubdiv.getComp());
		form.setReserveCertificate(reserveCertificate);

		form.setCertDate(getReservedCertificateCalDate(ji));
		if (ji.getConRevOutcome() != null)
			form.setActionOutcomeId(ji.getConRevOutcome().getId());
		// set the different note types and preset comment types into the
		// session for use by dwr
		HttpSession sess = request.getSession();
		sess.setAttribute("contractreviewcomment", PresetCommentType.CONTRACT_REVIEW);
		sess.setAttribute("jobitemrequirementcomment", PresetCommentType.JOB_ITEM_REQUIREMENT);
		// the pricing type class name
		sess.setAttribute(Constants.HTTPSESS_PRICING_CLASS_NAME, Job.class);
		boolean[] demandArray = new boolean[AdditionalDemand.values().length];
		if (!ji.getContactReviewItems().isEmpty()) {
			demandArray = jobItemDemandService.convertToBooleanArray(ji.getAdditionalDemands());
		} else if (contract != null) {
			ContractDemand contractDemand = contractDemandService.findBestMatch(contract.getId(),
					ji.getInst().getPlantid());
			if (contractDemand != null)
				for (AdditionalDemand demand : AdditionalDemand.values())
					demandArray[demand.ordinal()] = contractDemand.getDemandSet().get(demand.ordinal());
		}
		form.setDemands(demandArray);

		ContractReview mostRecentContractRev = this.contractRevServ.getMostRecentContractReviewForJobitem(ji);
		if (mostRecentContractRev != null) {
			form.setReviewComments(mostRecentContractRev.getComments());
		}

		return form;
	}

	private String getActivityName(JobItem jobItem) {
		if (jobItem.getJob().getType().equals(JobType.SITE)) {
			return "Onsite contract review";
		} else {
			return "Contract review";
		}
	}

	private Date getReservedCertificateCalDate(JobItem jobItem) {
		Date calDate = null;
		Certificate reservedCertitifate = this.certificateService.getReservedCertificate(jobItem);
		if (reservedCertitifate != null) {
			calDate = reservedCertitifate.getCalDate();
		}
		return calDate;
	}

	private int getId(TransportOption option) {
		if (option == null)
			return 0;
		return option.getId();
	}

	@PreAuthorize("@hasJobItemAuthorityOnConnectedSubdiv.check('JI_CONTRACT_REVIEW')")
	@RequestMapping(value = "/jicontractreview.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(@RequestParam(value = "jobitemid") Integer jobItemId,
			@Validated @ModelAttribute(FORM_NAME) JIContractReviewForm form, BindingResult bindingResult,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Locale locale) throws Exception {
		if (bindingResult.hasErrors()) {
			try {
				return referenceData(jobItemId, username, subdivDto, locale);
			} catch (Exception ex) {
				logger.error(ex.getMessage());
				return new ModelAndView(
						new RedirectView("jicontractreview.htm?jobitemid=" + form.getJi().getJobItemId()));
			}
		}
		this.updateAjaxStaleResources(form, form.getJi());
		JobItem ji = contractRevServ.contractReview(form, subdivDto, username);
		return new ModelAndView(new RedirectView("jicontractreview.htm?jobitemid=" + ji.getJobItemId(), true));
	}

	/**
	 * Transport in options are available from following subdivs: - Organisation
	 * of job - Subdivision belonging to booked in address (relevant if business
	 * subdiv only)
	 * 
	 * Note, subdiv ids are included in the list regardless of their owning
	 * company's role, to simplify logic - transport options only belong to
	 * business subdivs though.
	 */
	private List<KeyValueIntegerString> getTransportInOptions(JobItem jobItem, Locale locale) {
		Set<Integer> subdivIds = new HashSet<>();
		subdivIds.add(jobItem.getJob().getOrganisation().getSubdivid());
		if (jobItem.getBookedInAddr() != null)
			subdivIds.add(jobItem.getBookedInAddr().getSub().getSubdivid());
		String notSelectedText = this.messageSource.getMessage("company.nonespecified", null, "None Specified", locale);

		List<KeyValueIntegerString> results = new ArrayList<>();
		results.add(new KeyValueIntegerString(0, notSelectedText));
		results.addAll(this.transOptServ.getTransportOptionDtosForSubdivs(true, true, locale, subdivIds));

		return results;
	}

	/**
	 * Transport out options are available from the following subdivs: -
	 * Organisation of job - Subdivision belonging to booked in address
	 * (relevant if business subdiv only) - Subdivision of any work requirements
	 * on job item (relevant if business subdiv only) - Subdivision of current
	 * transport out option (in case it's selected, then a work requirement is
	 * deleted...)
	 * 
	 * Note, subdiv ids are included in the list regardless of their owning
	 * company's role, to simplify logic - transport options only belong to
	 * business subdivs though.
	 */
	private List<KeyValueIntegerString> getTransportOutOptions(JobItem jobItem, Locale locale) {
        Set<Integer> subdivIds = new HashSet<>();
        subdivIds.add(jobItem.getJob().getOrganisation().getSubdivid());
        if (jobItem.getBookedInAddr() != null)
            subdivIds.add(jobItem.getBookedInAddr().getSub().getSubdivid());
        jobItem.getWorkRequirements().stream().filter(w -> w.getWorkRequirement().getCapability() != null)
            .map(w -> w.getWorkRequirement().getCapability().getOrganisation().getSubdivid())
            .forEach(subdivIds::add);
        if (jobItem.getReturnOption() != null)
            subdivIds.add(jobItem.getReturnOption().getSub().getSubdivid());

        String notSelectedText = this.messageSource.getMessage("company.nonespecified", null, "None Specified", locale);

        List<KeyValueIntegerString> results = new ArrayList<>();
        results.add(new KeyValueIntegerString(0, notSelectedText));
        results.addAll(this.transOptServ.getTransportOptionDtosForSubdivs(true, true, locale, subdivIds));

        return results;
    }

	@PreAuthorize("hasAuthority('JI_CONTRACT_REVIEW')")
	@RequestMapping(value = "/jicontractreview.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@RequestParam(value = "jobitemid") Integer jobItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Locale locale)
			throws Exception {
		Contact contact = this.userService.get(username).getCon();
		Map<String, Object> refData = super.referenceData(jobItemId, contact, subdivDto.getKey(),username);
		JobItem jobItem = jobItemService.findJobItem(jobItemId);

		refData.put("transportInOptions", getTransportInOptions(jobItem, locale));
		refData.put("transportOutOptions", getTransportOutOptions(jobItem, locale));

		refData.put("caltypes", calTypeServ.getActiveCalTypes(jobItem.getJob().getType()));
		refData.put("lastDateComment", !jobItem.getHistory().isEmpty() ? jobItem.getHistory().stream()
				.max(Comparator.comparing(JobItemHistory::getId))
				.filter(h -> h.getFieldname() != null && h.getFieldname().equals(JobItem_.agreedDelDate.getName()))
				.map(JobItemHistory::getComment).orElse(null) : null);
		refData.put("additionalDemands", Arrays.asList(AdditionalDemand.values()));
		refData.put("showNewOverlay", showNewOverlay);
		refData.put("costs",jobItem.getCosts().stream()
				.map(CostJobItemDto::fromCost)
				.sorted(new CostJobItemDtoComparator())
				.collect(Collectors.toList()));

		refData.put("calWarterms", this.getCalibrationWarrantyTerms(subdivDto, jobItem.getJob().getCon().getSub()));
		refData.put("repWarterms", this.getRepairWarrantyTerms(subdivDto, jobItem.getJob().getCon().getSub()));

		refData.put("CalibrationWarrantyApplicable", this.getWarrantyCalibrationEvaluation(
				this.getCalibrationWarrantyTerms(subdivDto, jobItem.getJob().getCon().getSub()), jobItem));

		refData.put("RepairWarrantyApplicable", this.getWarrantyRepairEvaluation(
				this.getRepairWarrantyTerms(subdivDto, jobItem.getJob().getCon().getSub()), jobItem));
		refData.put("jobItemBPOs",jobItemToJobItemBPOs(jobItem));
		refData.put("jobItemPOs",jobItemToJobItemPOs(jobItem));

		return new ModelAndView("trescal/core/jobs/jobitem/jicontractreview", refData);
	}

	private List<JobItemPODto>  jobItemToJobItemPOs(JobItem jobItem) {
		return jobItem.getItemPOs().stream()
			.filter(jipo -> jipo.getPo()!=null)
			.map(JobItemPODto::fromJobItemPpo)
			.collect(Collectors.toList());
	}

	private List<JobItemPODto> jobItemToJobItemBPOs(JobItem jobItem) {
		return jobItem.getItemPOs().stream()
			.filter(jipo -> jipo.getBpo()!=null)
			.map(JobItemPODto::fromJobItemPpo)
			.collect(Collectors.toList());
	}

	@ModelAttribute("defaultCurrency")
	public SupportedCurrency defaultCurrency(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		Subdiv subDiv = subdivService.get(subdivDto.getKey());
		return subDiv.getComp().getCurrency();
	}

	@ModelAttribute("actionoutcomes")
	public Set<ActionOutcome> actionOutcomes(@RequestParam(value = "jobitemid") int jobItemId) {
		JobItem jobItem = this.jobItemService.findJobItem(jobItemId);
		String activityName = getActivityName(jobItem);
		ItemActivity activity = this.itemStateServ.findItemActivityByName(activityName);
		return activity == null ? new HashSet<>() : activity.getActionOutcomes();
	}

	/**
	 * This form has an inherent problem caused by the fact that between a
	 * jobitem being put onto the formbacking object and then being updated in
	 * onSubmit() it is possible for the jobitem to be altered by ajax dwr
	 * calls. This can cause a number of problems and result in either
	 * exceptions being thrown or (worse) data that has been changed by dwr
	 * calls being overwritten by old or 'stale' data from the jobitem in the
	 * formbacking object. To counter this we use this method to reload the
	 * jobitem with any data that may have been altered as a result of a dwr
	 * call - in this case jobitem requirements and jobitem costs. Back in
	 * onSubmit() we can then call merge() to merge what was our 'stale' jobitem
	 * reference back as the persistent entity.
	 *
	 * @param form
	 *            the {@link JIContractReviewForm}.
	 * @param ji
	 *            the {@link JobItem}
	 */
	protected void updateAjaxStaleResources(JIContractReviewForm form, JobItem ji) {
		// refresh the jobitem's requirements
		ji.setReq(this.reqServ.findRequirementByJobItem(ji.getJobItemId()));

		// refresh the jobitem's current costs
		// and update each cost with potentially updated data from the form
		if (ji.getCalibrationCost() != null) {

			ContractReviewCalibrationCost cost = this.calCostServ
					.findContractReviewCalibrationCost(ji.getCalibrationCost().getCostid());

			if (form.getCalTotalCost().compareTo(ji.getCalibrationCost().getTotalCost())!=0) {
				cost = this.calCostServ.resetLinkedCalContactReviewCost(cost);
			}
			ji.setCalibrationCost(cost);
			ji.getCalibrationCost().setTotalCost(form.getCalibrationCost().getTotalCost());
			ji.getCalibrationCost().setDiscountRate(form.getCalibrationCost().getDiscountRate());
			ji.getCalibrationCost().setActive(form.getCalibrationCost().isActive());
		}

		if (ji.getRepairCost() != null) {
			ji.setRepairCost(this.repCostServ.findContractReviewRepairCost(ji.getRepairCost().getCostid()));
			ji.getRepairCost().setTotalCost(form.getRepairCost().getTotalCost());
			ji.getRepairCost().setDiscountRate(form.getRepairCost().getDiscountRate());
			ji.getRepairCost().setActive(form.getRepairCost().isActive());
		}

		if (ji.getPurchaseCost() != null) {
			ji.setPurchaseCost(this.purCostServ.findContractReviewPurchaseCost(ji.getPurchaseCost().getCostid()));
			ji.getPurchaseCost().setTotalCost(form.getPurchaseCost().getTotalCost());
			ji.getPurchaseCost().setDiscountRate(form.getPurchaseCost().getDiscountRate());
			ji.getPurchaseCost().setActive(form.getPurchaseCost().isActive());
		}

		if (ji.getAdjustmentCost() != null) {
			ji.setAdjustmentCost(this.adjCostServ.findContractReviewAdjustmentCost(ji.getAdjustmentCost().getCostid()));
			ji.getAdjustmentCost().setTotalCost(form.getAdjustmentCost().getTotalCost());
			ji.getAdjustmentCost().setDiscountRate(form.getAdjustmentCost().getDiscountRate());
			ji.getAdjustmentCost().setActive(form.getAdjustmentCost().isActive());
		}
	}

	private Map<Integer, String> getCalibrationWarrantyTerms(KeyValue<Integer, String> subdivDtos, Subdiv clientSub) {

		Map<Integer, String> warrantyCalTermsMap = new HashMap<>();
		Subdiv sub = this.subdivService.get(subdivDtos.getKey());

		int calWarrantyBusinessSub = sdCalibrationWarranty.getValueByScope(Scope.SUBDIV, sub, sub.getComp())
				.intValue();
		int calWarrantyClientSub = sdCalibrationWarranty
				.getValueByScope(Scope.SUBDIV, clientSub, clientSub.getComp()).intValue();

		int calWarrantyBusinessCompsd = sdCalibrationWarranty
				.getValueByScope(Scope.COMPANY, sub.getComp(), sub.getComp()).intValue();

		int calWarrantyClientCompsd = sdCalibrationWarranty
				.getValueByScope(Scope.COMPANY, clientSub.getComp(), clientSub.getComp()).intValue();

		Integer calWarrantyBusinessCompSetting = sub.getComp().getBusinessSettings().getWarrantyCalibration();

		// calibration Warranty Terms
		if (calWarrantyClientSub != -1)
			warrantyCalTermsMap.put(calWarrantyClientSub, "client subdiv");
		else if (calWarrantyClientCompsd != -1)
			warrantyCalTermsMap.put(calWarrantyClientCompsd, "client company");
		else if (calWarrantyBusinessSub != -1)
			warrantyCalTermsMap.put(calWarrantyBusinessSub, "Business subdiv");
		else if (calWarrantyBusinessCompsd != -1)
			warrantyCalTermsMap.put(calWarrantyBusinessCompsd, "Business company");
		else warrantyCalTermsMap.put(calWarrantyBusinessCompSetting, "default");

		return warrantyCalTermsMap;
	}

	private Map<Integer, String> getRepairWarrantyTerms(KeyValue<Integer, String> subdivDtos, Subdiv clientSub) {

		Map<Integer, String> warrantyTermsMap = new HashMap<>();

		Subdiv sub = this.subdivService.get(subdivDtos.getKey());

		// Repair Warranty Terms
		int repWarrantyBusinessSub = sdRepairWarranty.getValueByScope(Scope.SUBDIV, sub, sub.getComp()).intValue();
		int repWarrantyClientSub = sdRepairWarranty.getValueByScope(Scope.SUBDIV, clientSub, clientSub.getComp())
				.intValue();

		int repWarrantyBusinessCompSd = sdRepairWarranty
				.getValueByScope(Scope.COMPANY, sub.getComp(), sub.getComp()).intValue();

		int repWarrantyClientCompSd = sdRepairWarranty
				.getValueByScope(Scope.COMPANY, clientSub.getComp(), clientSub.getComp()).intValue();

		Integer repWarrantyBusinessCompSetting = sub.getComp().getBusinessSettings().getWarrantyRepaire();

		if (repWarrantyClientSub != -1)
			warrantyTermsMap.put(repWarrantyClientSub, "client subdiv");
		else if (repWarrantyClientCompSd != -1)
			warrantyTermsMap.put(repWarrantyClientCompSd, "client company");
		else if (repWarrantyBusinessSub != -1)
			warrantyTermsMap.put(repWarrantyBusinessSub, "Business subdiv");
		else if (repWarrantyBusinessCompSd != -1)
			warrantyTermsMap.put(repWarrantyBusinessCompSd, "Business company");
		else warrantyTermsMap.put(repWarrantyBusinessCompSetting, "default");

		return warrantyTermsMap;
	}

	private Boolean getWarrantyCalibrationEvaluation(Map<Integer, String> warrantyCalTermsMap, JobItem newji) {

		return this.instrumServ.getHistory(newji.getInst()).stream().findFirst()
			.filter(mrc -> !mrc.getCalType().getServiceType().getRepair())
			.map(checkIfBeforeWarrantyCal(warrantyCalTermsMap)).orElse(false);
	}

	Function<Calibration,Boolean> checkIfBeforeWarrantyCal(Map<Integer,String> warrantyCalTermsMap) {
		return mrc -> {
			val wcd = warrantyCalTermsMap.keySet().stream().findFirst()
				.map(k -> mrc.getCalDate().plusDays(k)).orElse(mrc.getCalDate());
			val date = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
			return date.isBefore(wcd);
		};
	}

	private Boolean getWarrantyRepairEvaluation(Map<Integer, String> warrantyCalTermsMap, JobItem newji) {

		return this.instrumServ.getHistory(newji.getInst()).stream().findFirst()
		.filter(mrc -> mrc.getCalType().getServiceType().getRepair())
		.map(checkIfBeforeWarrantyCal(warrantyCalTermsMap)).orElse(false);
	}

}