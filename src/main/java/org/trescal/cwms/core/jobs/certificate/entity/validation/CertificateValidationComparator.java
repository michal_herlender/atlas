package org.trescal.cwms.core.jobs.certificate.entity.validation;

import java.util.Comparator;

public class CertificateValidationComparator implements Comparator<CertificateValidation>
{
	@Override
	public int compare(CertificateValidation cv1, CertificateValidation cv2)
	{
		Integer c1 = cv1.getId();
		Integer c2 = cv2.getId();

		return c1.compareTo(c2);
	}

}
