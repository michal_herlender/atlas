package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.GsoDocumentLink;

@Service("GsoDocumentLinkService")
public class GsoDocumentLinkServiceImpl extends BaseServiceImpl<GsoDocumentLink, Integer> implements GsoDocumentLinkService
{
  @Autowired
  private GsoDocumentLinkDao gsoDocumentLinkDao;
  
  protected BaseDao<GsoDocumentLink, Integer> getBaseDao() {
     return (BaseDao<GsoDocumentLink, Integer>)this.gsoDocumentLinkDao;
  }
  
}