package org.trescal.cwms.core.jobs.jobitem.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.AutoPopulatingList;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.enums.NewJobItemSearchOrderByEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.ItemBasketWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchForm;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.helpers.IntegerElementFactory;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@IntranetController
@RequestMapping(value = "/newjobitemsearch.htm")
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
@Slf4j
public class NewJobItemSearchController extends AbstractNewJobItemController {

	@Value("${cwms.config.goodsin.max_basket_size}")
	private Integer maxBasketSize;
	@Autowired
	private AddressService addrServ;
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private JobService jobServ;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private UserService userService;

	public static final String FORM_NAME = "form";
	public static final String VIEW_NAME = "trescal/core/jobs/jobitem/newjobitemsearch";

	private static final String ACTION_ADD_STRAIGHT_TO_JOB = "Add Straight to Job";
	private static final String ACTION_ADD_ITEMS = "Add Items";

	@ModelAttribute(FORM_NAME)
	public NewJobItemSearchForm formBackingObject(@RequestParam(value = "jobid") Integer jobId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {

		Contact currentContact = userService.get(username).getCon();
		Job job = this.jobServ.get(jobId);

		Date dateIn;
		if (jiServ.getDateIn(job) != null)
			dateIn = DateTools.dateFromZonedDateTime(jiServ.getDateIn(job));
		else
			dateIn = new Date();

		// create form
		NewJobItemSearchForm form = new NewJobItemSearchForm();
		// Make sure all lists relating to hidden inputs are initialised to
		// prevent null
		// pointer exceptions
		form.setBasketTempGroupIds(new AutoPopulatingList<>(i -> 0));
		form.setBasketExistingGroupIds(new AutoPopulatingList<>(new IntegerElementFactory(0)));
		form.setBasketParts(new AutoPopulatingList<>(String.class));
		form.setBasketBaseItemIds(new AutoPopulatingList<>(new IntegerElementFactory(0)));
		form.setBasketTypes(new AutoPopulatingList<>(String.class));
		form.setBasketIds(new AutoPopulatingList<>(new IntegerElementFactory(0)));
		form.setBasketQuotationItemIds(new AutoPopulatingList<>(new IntegerElementFactory(0)));
		form.setServiceTypeIds(new AutoPopulatingList<>(new IntegerElementFactory(0)));
		form.setServiceTypeSources(new AutoPopulatingList<>(new IntegerElementFactory(0)));
		form.setDatesIn(new AutoPopulatingList<>(i -> dateIn));
		form.setBookInByContactIds(new AutoPopulatingList<>(i -> currentContact.getId()));

		// set form properties
		form.setSearchForm(new InstrumentAndModelSearchForm<>());
		form.getSearchForm().setCompId(job.getCon().getSub().getComp().getCoid());
		form.setResultsPerPage(20);
		form.setPageInstruments(0);
		form.setPageModels(0);
		form.setPageDistinctModels(0);
		form.setBasketNextTempGroup(0);
		form.setTabSelected("instrument");
		form.setContractReviewItems(false);
		form.setOrderby(NewJobItemSearchOrderByEnum.SERIAL_NO);
		// Use the job's booked in address (or booked in location) as default
		// booking in
		// address
		if (job.getBookedInAddr() != null) {
			form.setBookingInAddrId(job.getBookedInAddr().getAddrid());
		} else if (job.getBookedInLoc() != null) {
			form.setBookingInAddrId(job.getBookedInLoc().getAdd().getAddrid());
		} else {
			form.setBookingInAddrId(0);
		}
		return form;
	}

	private List<Address> getBookingInAddresses(Job job, User user) {
		if (job.getType().equals(JobType.SITE)) {
			// Site job at client address
			return job.getBookedInAddr().getSub().getAddresses().stream().filter(Address::getActive)
					.collect(Collectors.toList());
		} else {
			return user.getUserRoles().stream()
					.map(org.trescal.cwms.core.userright.entity.userrole.UserRole::getOrganisation).distinct()
					.map(Subdiv::getAddresses).flatMap(Set::stream).collect(Collectors.toList());
		}
	}

	@PreAuthorize("hasAuthority('JOB_ITEM_NEW_AND_SEARCH')")
	@RequestMapping(method = RequestMethod.POST)
	protected String onSubmit(Model model, RedirectAttributes redirectAttributes,
			@RequestParam(value = "jobid") Integer jobId,
			@Validated @ModelAttribute(FORM_NAME) NewJobItemSearchForm form, BindingResult bindingResult,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Locale locale) throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData(model, jobId, username);
		}

		log.info("Submission to 'newjobitemsearch.htm' with attribute 'action' set to '" + form.getAction() + "'");
		// Make sure only 'proper' models are retrieved in model searches
		// Updated to include quarantined models (e.g standalone) 2017-02-15 per
		// DBD
		// direction
		form.getSearchForm().setExcludeSalesCategoryModelTypes(true);
		form.getSearchForm().setExcludeCapabilityModelTypes(true);
		form.getSearchForm().setExcludeQuarantined(true);

		Contact currentContact = userService.get(username).getCon();
		boolean defaultServiceTypeUsage = currentContact.getUserPreferences().getDefaultServiceTypeUsage();
		Job job = this.jobServ.get(jobId);
		List<ItemBasketWrapper> basketItems = initializeBasketItems(model, form, job, defaultServiceTypeUsage);
		// if quick adding insts (note, all must now be insts, checked in
		// validation)
		if (form.getAction().equals(ACTION_ADD_STRAIGHT_TO_JOB)) {
			// get booking in address
			Address bookingInAddr = this.addrServ.get(form.getBookingInAddrId());
			// add instruments from basket
			this.jiServ.addInstrumentsStraightToJob(basketItems, job, currentContact, bookingInAddr, 
					defaultServiceTypeUsage);
			return "redirect:viewjob.htm?jobid=" + job.getJobid();
		}
		// if submitting basket
		else if (form.getAction().equals(ACTION_ADD_ITEMS)) {
			log.info("form TO ADD NEW INSTRUMENT" + form);
			redirectAttributes.addFlashAttribute("flashBaseItemIds", form.getBasketBaseItemIds());
			redirectAttributes.addFlashAttribute("flashExistingGroupIds", form.getBasketExistingGroupIds());
			redirectAttributes.addFlashAttribute("flashIds", form.getBasketIds());
			redirectAttributes.addFlashAttribute("flashParts", form.getBasketParts());
			redirectAttributes.addFlashAttribute("flashTempGroupIds", form.getBasketTempGroupIds());
			redirectAttributes.addFlashAttribute("flashTypes", form.getBasketTypes());
			redirectAttributes.addFlashAttribute("flashQuotationItemIds", form.getBasketQuotationItemIds());
			redirectAttributes.addFlashAttribute("flashServiceTypeIds", form.getServiceTypeIds());
			redirectAttributes.addFlashAttribute("flashServiceTypeSources", form.getServiceTypeSources());
			redirectAttributes.addFlashAttribute("flashDatesIn", form.getDatesIn());
			redirectAttributes.addFlashAttribute("flashBookInByContactIds", form.getBookInByContactIds());

			// pass to next page
			return "redirect:addnewitemstojob.htm?jobid=" + job.getJobid() + "&bookingInAddrId="
					+ form.getBookingInAddrId();
		} else {
			return referenceData(model, jobId, username);
		}
	}

	@PreAuthorize("hasAuthority('JOB_ITEM_NEW_AND_SEARCH')")
	@RequestMapping(method = RequestMethod.GET)
	protected String referenceData(Model model, @RequestParam(value = "jobid") Integer jobId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		User user = userService.get(username);
		Job job = this.jobServ.get(jobId);

		model.addAttribute("bookingInAddresses", getBookingInAddresses(job, user).
				stream().map(a -> new KeyValue<>(a.getAddrid(),a.getAddressLine())).collect(Collectors.toList()));
		model.addAttribute("job", job);
		model.addAttribute("activeCalTypes", this.calTypeServ.getActiveCalTypes(job.getType()));
		model.addAttribute("activeServiceTypeIds", this.serviceTypeService.getIdsByJobType(job.getType()));
		model.addAttribute("maxBasketSize", this.maxBasketSize);
		model.addAttribute("barcodesOnJob", this.instServ.getBarcodesOnJob(jobId));
		model.addAttribute("allowContractReview", false);
		model.addAttribute("searchOrderBy", NewJobItemSearchOrderByEnum.values());
		model.addAttribute("linkedQuotes",job.getLinkedQuotes().stream()
				.map(ql -> LinkedQuotDto.of(ql.getId(),ql.getQuotation().getQno()+" ver "+
						ql.getQuotation().getVer()) ).collect(Collectors.toList()));
		model.addAttribute("defaultServiceTypeUsage", user.getCon().getUserPreferences().getDefaultServiceTypeUsage());

		return VIEW_NAME;
	}

}


@lombok.Value(staticConstructor = "of")
class LinkedQuotDto{
	Integer id;
	String label;
}