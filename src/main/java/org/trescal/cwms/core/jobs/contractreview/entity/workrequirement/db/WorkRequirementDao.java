package org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;

public interface WorkRequirementDao extends BaseDao<WorkRequirement, Integer> {
}