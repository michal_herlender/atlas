package org.trescal.cwms.core.jobs.jobitem.dto;

public class JobItemOnClientPurchaseOrderDTO {
	
	private Integer id;
	private Integer itemNo;
	private String modelName;
	private String serialNo;
	private String plantNo;
	private String calType;
	private String calTypeLong;
	private String calTypeColour;
	private Boolean onClientPO;
	private Boolean onAnyClientPO;
	
	public JobItemOnClientPurchaseOrderDTO() {
		super();
	}
	
	public JobItemOnClientPurchaseOrderDTO(Integer id, Integer itemNo, String modelName, String serialNo, String plantNo,
			String calType, String calTypeLong, String calTypeColour, Long onClientPO, Long onAnyClientPO) {
		super();
		this.id = id;
		this.itemNo = itemNo;
		this.modelName = modelName;
		this.serialNo = serialNo;
		this.plantNo = plantNo;
		this.calType = calType;
		this.calTypeLong = calTypeLong;
		this.calTypeColour = calTypeColour;
		this.onClientPO = onClientPO > 0;
		this.onAnyClientPO = onAnyClientPO - onClientPO > 0;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getItemNo() {
		return itemNo;
	}
	
	public void setItemNo(Integer itemNo) {
		this.itemNo = itemNo;
	}
	
	public String getModelName() {
		return modelName;
	}
	
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	
	public String getSerialNo() {
		return serialNo;
	}
	
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	
	public String getPlantNo() {
		return plantNo;
	}
	
	public void setPlantNo(String plantNo) {
		this.plantNo = plantNo;
	}
	
	public String getCalType() {
		return calType;
	}
	
	public void setCalType(String calType) {
		this.calType = calType;
	}
	
	public String getCalTypeLong() {
		return calTypeLong;
	}
	
	public void setCalTypeLong(String calTypeLong) {
		this.calTypeLong = calTypeLong;
	}
	
	public String getCalTypeColour() {
		return calTypeColour;
	}
	
	public void setCalTypeColour(String calTypeColour) {
		this.calTypeColour = calTypeColour;
	}
	
	public Boolean getOnClientPO() {
		return onClientPO;
	}
	
	public void setOnClientPO(Boolean onClientPO) {
		this.onClientPO = onClientPO;
	}
	
	public Boolean getOnAnyClientPO() {
		return onAnyClientPO;
	}
	
	public void setOnAnyClientPO(Boolean onAnyClientPO) {
		this.onAnyClientPO = onAnyClientPO;
	}
}