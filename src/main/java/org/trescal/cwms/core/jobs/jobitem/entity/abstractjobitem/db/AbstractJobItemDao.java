package org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.AbstractJobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitempo.AbstractJobItemPO;

public interface AbstractJobItemDao<ItemType extends AbstractJobItem<? extends AbstractJobItemPO<ItemType>>>
		extends BaseDao<ItemType, Integer> {

	Long countItemsForJobOnBPO(Integer jobId, Integer bpoId);
}