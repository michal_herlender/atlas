package org.trescal.cwms.core.jobs.calibration.dto;

import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;

public class OtherCompleteBatchWrapper
{
	private BatchCalibration batchCal;
	private int missingFiles;

	public OtherCompleteBatchWrapper(BatchCalibration batchCal, int missingFiles)
	{
		this.batchCal = batchCal;
		this.missingFiles = missingFiles;
	}

	public BatchCalibration getBatchCal()
	{
		return this.batchCal;
	}

	public int getMissingFiles()
	{
		return this.missingFiles;
	}

	public void setBatchCal(BatchCalibration batchCal)
	{
		this.batchCal = batchCal;
	}

	public void setMissingFiles(int missingFiles)
	{
		this.missingFiles = missingFiles;
	}
}