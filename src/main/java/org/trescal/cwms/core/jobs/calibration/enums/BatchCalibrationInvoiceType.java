package org.trescal.cwms.core.jobs.calibration.enums;

/**
 * Defines the different types of invoice that can be created from a batch
 * calibration.
 * 
 * @author Richard
 */
public enum BatchCalibrationInvoiceType
{
	ALL_JOBITEMS("All jobitems"), ALL_BATCH_ITEMS("All batch items");

	private String desc;

	private BatchCalibrationInvoiceType(String desc)
	{
		this.desc = desc;
	}

	public String getDesc()
	{
		return this.desc;
	}

	public void setDesc(String desc)
	{
		this.desc = desc;
	}
}
