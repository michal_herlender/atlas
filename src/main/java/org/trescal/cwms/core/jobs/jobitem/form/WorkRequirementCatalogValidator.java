package org.trescal.cwms.core.jobs.jobitem.form;

import javax.validation.groups.Default;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class WorkRequirementCatalogValidator extends AbstractBeanValidator {
	@Override
	public boolean supports(Class<?> clazz) {
		return WorkRequirementCatalogForm.class.isAssignableFrom(clazz);
	}
	@Override
	public void validate(Object target, Errors errors) {
		WorkRequirementCatalogForm form = (WorkRequirementCatalogForm) target;
		if (form.getAction().equals(WorkRequirementCatalogForm.ACTION_CREATE)) {
			super.validate(target, errors, Default.class, WorkRequirementCatalogForm.CreateGroup.class);
		}
		else if (form.getAction().equals(WorkRequirementCatalogForm.ACTION_SEARCH) && form.getIncludeGenericCapabilities()) {
			super.validate(target, errors, Default.class);
		}
		else if (form.getAction().equals(WorkRequirementCatalogForm.ACTION_SEARCH) && !form.getIncludeGenericCapabilities()) {
			super.validate(target, errors, Default.class, WorkRequirementCatalogForm.SearchGroupSubfamily.class);
		}
		else {
			errors.rejectValue("action", null, "Invalid action");
		}
		super.validate(target, errors);
	}
}
