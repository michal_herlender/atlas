package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;

@Entity
@Table(name = "jobexpenseitemnotinvoiced")
@AssociationOverrides({
		@AssociationOverride(name = "contact", joinColumns = @JoinColumn(name = "contactid"), foreignKey = @ForeignKey(name = "FK_jobexpenseitemnotinvoiced_contact")),
		@AssociationOverride(name = "periodicInvoice", joinColumns = @JoinColumn(name = "periodicinvoiceid"), foreignKey = @ForeignKey(name = "FK_jobexpenseitemnotinvoiced_periodicinvoice")) })
public class JobExpenseItemNotInvoiced extends AbstractJobItemNotInvoiced {

	private Integer id;
	private JobExpenseItem expenseItem;

	@Id
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@MapsId
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "expenseitemid", foreignKey = @ForeignKey(name = "FK_jobexpenseitemnotinvoiced_expenseitem"))
	public JobExpenseItem getExpenseItem() {
		return expenseItem;
	}

	public void setExpenseItem(JobExpenseItem expenseItem) {
		this.expenseItem = expenseItem;
	}
}