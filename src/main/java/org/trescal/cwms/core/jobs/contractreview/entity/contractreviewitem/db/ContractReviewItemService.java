package org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.db;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.trescal.cwms.core.jobs.contractreview.entity.contractreviewitem.ContractReviewItem;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;

public interface ContractReviewItemService
{
	ContractReviewItem findContractReviewItem(int id);
	void insertContractReviewItem(ContractReviewItem contractreviewitem);
	void updateContractReviewItem(ContractReviewItem contractreviewitem);
	List<ContractReviewItem> getAllContractReviewItems();
	void deleteContractReviewItem(ContractReviewItem contractreviewitem);
	void saveOrUpdateContractReviewItem(ContractReviewItem contractreviewitem);
	
	Set<Integer> getContractReviewedJobItemIds(int jobid);
	List<JobItemProjectionDTO> getContractReviewItemsDTOs(Integer ContractReviewId,Locale locale);
	List<Integer> getJobItemsCouverdInContractReview(Integer ContractReviewId,Integer jobid);
}