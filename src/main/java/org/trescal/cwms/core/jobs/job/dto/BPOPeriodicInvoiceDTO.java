package org.trescal.cwms.core.jobs.job.dto;

import java.math.BigDecimal;
import java.util.List;

public class BPOPeriodicInvoiceDTO {

	private Integer periodicInvoiceId;
	private String periodicInvoiceNumber;
	private BigDecimal periodicInvoiceAmount;
	private List<BPOUsageDTO> jobItems;

	public BPOPeriodicInvoiceDTO(Integer periodicInvoiceId, String periodicInvoiceNumber,
			BigDecimal periodicInvoiceAmount) {
		this.periodicInvoiceId = periodicInvoiceId;
		this.periodicInvoiceNumber = periodicInvoiceNumber;
		this.periodicInvoiceAmount = periodicInvoiceAmount;
	}

	public Integer getPeriodicInvoiceId() {
		return periodicInvoiceId;
	}

	public void setPeriodicInvoiceId(Integer periodicInvoiceId) {
		this.periodicInvoiceId = periodicInvoiceId;
	}

	public String getPeriodicInvoiceNumber() {
		return periodicInvoiceNumber;
	}

	public void setPeriodicInvoiceNumber(String periodicInvoiceNumber) {
		this.periodicInvoiceNumber = periodicInvoiceNumber;
	}

	public BigDecimal getPeriodicInvoiceAmount() {
		return periodicInvoiceAmount;
	}

	public void setPeriodicInvoiceAmount(BigDecimal periodicInvoiceAmount) {
		this.periodicInvoiceAmount = periodicInvoiceAmount;
	}

	public List<BPOUsageDTO> getJobItems() {
		return jobItems;
	}

	public void setJobItems(List<BPOUsageDTO> jobItems) {
		this.jobItems = jobItems;
	}
}