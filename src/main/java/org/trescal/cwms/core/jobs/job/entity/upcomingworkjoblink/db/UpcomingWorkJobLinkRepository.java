package org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.db;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.UpcomingWorkJobLink;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork;

import java.util.List;

@Repository
public interface UpcomingWorkJobLinkRepository extends CrudRepository<UpcomingWorkJobLink,Integer> {
    List<UpcomingWorkJobLink> findAllByUpcomingWorkAndActiveTrue(UpcomingWork upcomingWork);
    long countAllByUpcomingWorkAndJobAndActiveTrue(UpcomingWork upcomingWork, Job job);
    void deleteAllByUpcomingWork(UpcomingWork upcomingWork);
}



