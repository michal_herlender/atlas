package org.trescal.cwms.core.jobs.job.entity.bpo.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.job.dto.BPOPeriodicInvoiceDTO;
import org.trescal.cwms.core.jobs.job.dto.BPOUsageDTO;
import org.trescal.cwms.core.jobs.job.dto.BpoDTO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.RelatedBPODWRSet;
import org.trescal.cwms.core.jobs.job.entity.bpo.RelatedBPOSet;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Interface for accessing and manipulating {@link BPO} entities.
 */
public interface BPOService extends BaseService<BPO, Integer> {
	/**
	 * Attaches the {@link BPO} with the given ID to the {@link Job} with the given
	 * ID and sets the {@link BPO} as the default for the {@link Job}.
	 * 
	 * @param jobid the {@link Job} ID.
	 * @param bpoid the {@link BPO} ID.
	 * @return the newly attached {@link BPO} entity.
	 */
	BpoDTO addBPOToJob(int jobid, int bpoid);

	BpoDTO addBPOToJobItem(Integer jobItemId, Integer bpoId);

	Map<Scope, List<BpoDTO>> findAllForJob(Integer jobId, boolean activeOnly);

	/**
	 * Returns all {@link BPO} entities owned by the {@link Contact} with the given
     * ID, the {@link Contact}'s {@link Subdivision} or the {@link Contact}'s
     * {@link Company}.
     *
     * @param contact the {@link Contact} to get {@link BPO}s for
     * @return a {@link List} of the related {@link BPO}s.
     */
    List<BPO> getAllByContactHierarchical(Contact contact, Company allocatedCompany);

    List<BpoDTO> getAllByContactHierarchicalNotOnJob(Integer personId, Integer allocatedCompanyId, Integer jobId);

    RelatedBPOSet getAllBPOsRelatedToContact(int personid, KeyValue<Integer, String> allocatedCompanyKV);

    /**
     * Returns all {@link BPO} entities owned by the {@link Contact} with the given
     * ID, the {@link Contact}'s {@link Subdivision} or the {@link Contact}'s
     * {@link Company}.
     *
     * @param personid the {@link Contact} to get {@link BPO}s for
     * @return a {@link RelatedBPODWRSet} wrapper containing categorised {@link Map}s
     * of the related {@link BPO}s.
     */
    RelatedBPODWRSet getAllBPOsRelatedToJob(int personid, HttpSession session);

	/**
	 * Returns all active {@link BPO} entities that are due to expire in the next
	 * two weeks, and where an expiry warning has NOT already been sent.
	 *
	 * @return the {@link List} of {@link BPO} entities.
	 */
	List<BPO> getExpiringBPOs();

	List<BpoDTO> getAllByCompany(Company company, Boolean active, Company allocatedCompany);

	List<BpoDTO> getAllBySubdiv(Subdiv subdiv, Boolean active, Company allocatedCompany);

	List<BpoDTO> getAllByContact(Contact contact, Boolean active, Company allocatedCompany);

	List<BPOPeriodicInvoiceDTO> getPeriodicInvoicesLinkedTo(Integer bpoId, Locale locale);

	List<BPOUsageDTO> getBPOUsage(Integer bpoId, Locale locale);

	List<BPOUsageDTO> getExpenseItemAmounts(Integer bpoId, Locale locale);

	void removeItemFromBPO(Integer jobItemId, Integer bpoId);

	void updateJobBPODefault(int jobid, int poid);

	BpoDTO saveBpoDto(BpoDTO inBpo, KeyValue<Integer, String> allocatedCompay);
	
	List<Integer> getLinkedToJobsOnInvoice(Integer invoiceId);
}