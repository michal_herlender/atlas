package org.trescal.cwms.core.jobs.job.dto;

import lombok.Data;
import org.trescal.cwms.core.jobs.job.entity.po.PO;

import java.util.Date;

@Data
public class ClientPurchaseOrderDTO {

    private Integer jobId;
    private Integer poId;
    private String poNumber;
    private Boolean active;
    private Boolean jobDefault;
    private String comment;
    private Long countJobItems;
    private Long countExpenseItems;
    private String deactivatedBy;
    private Date deactivatedTime;

    public ClientPurchaseOrderDTO(Integer jobId, Integer poId, String poNumber, Boolean active, Integer jobDefaultPoId,
                                  String comment, Long countJobItems, Long countExpenseItems, String deactivatedByFirstName,
                                  String deactivatedByLastName, Date deactivatedTime) {
        this.jobId = jobId;
        this.poId = poId;
        this.poNumber = poNumber;
        this.active = active;
        this.jobDefault = poId.equals(jobDefaultPoId);
        this.comment = comment;
        this.countJobItems = countJobItems;
        this.countExpenseItems = countExpenseItems;
        if (deactivatedByFirstName != null) {
            this.deactivatedBy = deactivatedByFirstName;
            if (deactivatedByLastName != null)
                this.deactivatedBy += " " + deactivatedByLastName;
        }
        if (deactivatedTime != null) this.deactivatedTime = deactivatedTime;
    }

	public ClientPurchaseOrderDTO(PO po) {
        this.jobId = po.getJob().getJobid();
        this.poId = po.getPoId();
        this.poNumber = po.getPoNumber();
        this.active = po.isActive();
        this.jobDefault = po.equals(po.getJob().getDefaultPO());
        this.comment = po.getComment();
        this.countJobItems = po.getJobItemPOs() == null ? 0L : (long) po.getJobItemPOs().size();
        this.countExpenseItems = po.getExpenseItemPOs() == null ? 0L : (long) po.getExpenseItemPOs().size();
        this.deactivatedBy = po.getDeactivatedBy() == null ? "" : po.getDeactivatedBy().getName();
        this.deactivatedTime = po.getDeactivatedTime();
    }
}