package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink;

import java.util.Comparator;

public class GsoDocumentLinkComparator implements Comparator<GsoDocumentLink>
{
	@Override
	public int compare(GsoDocumentLink o1, GsoDocumentLink o2)
	{
		return o1.getJi().getJobItemId() - o2.getJi().getJobItemId();
	}
}
