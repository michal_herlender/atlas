package org.trescal.cwms.core.jobs.jobitem.form;

public interface NewJobItemBaseSearchForm {
    Integer getJobId();
}
