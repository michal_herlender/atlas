package org.trescal.cwms.core.jobs.certificate.entity.certaction.db;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateAction;
import org.trescal.cwms.core.jobs.certificate.entity.certaction.CertAction;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

@Service("CertActionService")
public class CertActionServiceImpl implements CertActionService
{
	@Autowired
	private CertActionDao certActionDao;
	
	@Override
	public void deleteCertAction(CertAction certaction)
	{
		this.certActionDao.remove(certaction);
	}

	@Override
	public CertAction findCertAction(int id)
	{
		return this.certActionDao.find(id);
	}

	@Override
	public List<CertAction> getAllCertActions()
	{
		return this.certActionDao.findAll();
	}

	@Override
	public void insertCertAction(CertAction CertAction)
	{
		this.certActionDao.persist(CertAction);
	}
	
	@Override
	public void logCertAction(Certificate cert, CertificateAction action, Contact con)
	{
		CertAction ca = new CertAction();
		ca.setCert(cert);
		ca.setAction(action);
		ca.setActionBy(con);
		ca.setActionOn(new Date());
		this.insertCertAction(ca);
	}

	@Override
	public void saveOrUpdateCertAction(CertAction certaction)
	{
		this.certActionDao.saveOrUpdate(certaction);
	}

	@Override
	public void updateCertAction(CertAction CertAction)
	{
		this.certActionDao.update(CertAction);
	}
}