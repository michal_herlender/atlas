package org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.jobs.contractreview.entity.costs.ContractReviewLinkedCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost;

@Entity
@Table(name = "contractreviewlinkedadjustmentcost")
public class ContractReviewLinkedAdjustmentCost extends ContractReviewLinkedCost
{
	private ContractReviewAdjustmentCost adjustmentCost;
	private JobCostingAdjustmentCost jobCostingAdjustmentCost;

	@OneToOne(fetch=FetchType.LAZY, cascade = javax.persistence.CascadeType.ALL)
	@JoinColumn(name = "adjcostid", nullable = false, unique = true)
	public ContractReviewAdjustmentCost getAdjustmentCost()
	{
		return this.adjustmentCost;
	}

	@Transient
	public JobCostingAdjustmentCost getJobCostingAdjustmentCost()
	{
		return this.jobCostingAdjustmentCost;
	}

	public void setAdjustmentCost(ContractReviewAdjustmentCost adjustmentCost)
	{
		this.adjustmentCost = adjustmentCost;
	}

	public void setJobCostingAdjustmentCost(JobCostingAdjustmentCost jobCostingAdjustmentCost)
	{
		this.jobCostingAdjustmentCost = jobCostingAdjustmentCost;
	}
}
