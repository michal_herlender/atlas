package org.trescal.cwms.core.jobs.jobitemhistory.db;

import java.util.Date;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitemhistory.JobItemHistory;

public interface JobItemHistoryDao extends BaseDao<JobItemHistory, Integer> {
	
	public JobItemHistory getJobItemHistoryByChangeDate(Date changeDate);
	
}