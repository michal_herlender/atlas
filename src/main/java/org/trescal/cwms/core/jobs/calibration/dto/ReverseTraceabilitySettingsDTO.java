package org.trescal.cwms.core.jobs.calibration.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ReverseTraceabilitySettingsDTO {

	private Integer id;
	private Integer subdivid;
	private String subname;
	private Boolean reverseTraceability;
	private LocalDate startDate;

	public ReverseTraceabilitySettingsDTO() {
		super();
	}

	public ReverseTraceabilitySettingsDTO(Integer id, Integer subdivid, String subname, Boolean reverseTraceability, LocalDate startDate) {
		super();
		this.id = id;
		this.subdivid = subdivid;
		this.subname = subname;
		this.reverseTraceability = reverseTraceability;
		this.startDate = startDate;
	}

}
