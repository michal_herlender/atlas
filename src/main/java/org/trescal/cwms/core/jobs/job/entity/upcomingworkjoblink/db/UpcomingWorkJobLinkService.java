package org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.db;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.UpcomingWorkJobLink;
import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.UpcomingWorkJobLinkAjaxDto;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork;

import java.util.List;


public interface UpcomingWorkJobLinkService {
    /**
     * Find all {@link UpcomingWorkJobLink} by {@link UpcomingWork} id
     * @param upcomingWork
     * @return
     */
    List<UpcomingWorkJobLinkAjaxDto> getJobByUpcomingWork(UpcomingWork upcomingWork);

    /**
     *
     * @param jobs
     * @return {@link ResultWrapper} indicating success of the operation.
     */
    void addJobToUpcomingWork(List<Job> jobs, UpcomingWork upcomingWork) throws UpcomingWorkJobLinkException;


    /**
     *
     * @param upcomingWorkJobLinkAjaxDtos
     * @param upcomingWork
     * @return {@link ResultWrapper} indicating success of the operation.
     */
    void updateJobToUpcomingWork(List<UpcomingWorkJobLinkAjaxDto> upcomingWorkJobLinkAjaxDtos, UpcomingWork upcomingWork) throws UpcomingWorkJobLinkException;

    /**
     * Add or Update {@link UpcomingWorkJobLink}
     * @param  id the id of the {@link UpcomingWorkJobLink} to delete.
     * @return {@link ResultWrapper} indicating success of the operation.
     */
    void deleteUpcomingWorkJobLink(Integer id) throws UpcomingWorkJobLinkException;

    /**
     *
     * Check if {@link UpcomingWorkJobLink} exist
     * @param upcomingWork
     * @param job
     * @return {@link boolean} indicating success of the operation.
     */
    boolean upcomingWorkJobLinkExist(UpcomingWork upcomingWork, Job job);

    /**
     * Delete all  {@link UpcomingWorkJobLink}
     * @param  upcomingWork to delete.
     * @return {@link ResultWrapper} indicating success of the operation.
     */
    void deleteAllUpcomingWorkJobLinkByUpcomingWork(UpcomingWork upcomingWork);
}
