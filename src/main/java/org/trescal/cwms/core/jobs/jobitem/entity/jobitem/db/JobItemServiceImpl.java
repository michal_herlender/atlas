package org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.SubdivSettingsForAllocatedSubdiv;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.companysettings.db.SubdivSettingsForAllocatedSubdivService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.documents.images.image.db.ImageService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db.ServiceCapabilityService;
import org.trescal.cwms.core.jobs.calibration.entity.batchcalibration.BatchCalibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLinkComparator;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.ContractReviewAdjustmentCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.adjcost.db.ContractReviewAdjustmentCostService;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.db.ContractReviewCalibrationCostService;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewPurchaseCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.db.ContractReviewPurchaseCostService;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.db.ContractReviewRepairCostService;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.JobItemWorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.jobitemworkrequirement.db.JobItemWorkRequirementService;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.WorkRequirement;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db.WorkRequirementService;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirementstatus.WorkrequirementStatus;
import org.trescal.cwms.core.jobs.job.entity.abstractpo.AbstractPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobSortKey;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.po.db.POService;
import org.trescal.cwms.core.jobs.job.form.AddQuickItemsForm.AddQuickItemsRowDTO;
import org.trescal.cwms.core.jobs.jobitem.dto.*;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.db.AbstractJobItemDao;
import org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem.db.AbstractJobItemServiceImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.*;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.db.JobItemActionService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.db.JobItemGroupService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.NotInvoicedReason;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db.JobItemNotInvoicedService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.JobItemTransit;
import org.trescal.cwms.core.jobs.jobitem.entity.onbehalfitem.OnBehalfItem;
import org.trescal.cwms.core.jobs.jobitem.entity.requirement.Requirement;
import org.trescal.cwms.core.jobs.jobitem.entity.requirement.RequirementComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirement;
import org.trescal.cwms.core.jobs.jobitem.entity.tprequirement.TPRequirementComparator;
import org.trescal.cwms.core.jobs.jobitem.form.ReturnToAddressForm;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.dao.AsnService;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;
import org.trescal.cwms.core.misc.entity.hook.Hook;
import org.trescal.cwms.core.misc.entity.hook.db.HookService;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.lookup.PriceLookupRequirements;
import org.trescal.cwms.core.pricing.lookup.PriceLookupService;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupInput;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupInputFactory;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutput;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderjobitem.PurchaseOrderJobItemComparator;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.procedure.entity.workinstruction.db.WorkInstructionService;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quoteitemnote.QuoteItemNote;
import org.trescal.cwms.core.system.entity.basestatus.db.BaseStatusService;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.Discount;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.autoservice.AutoServiceCalculationTool;
import org.trescal.cwms.core.tools.reports.dto.ItemsAtTPWrapper;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.tools.transitcalendar.ClientTransit;
import org.trescal.cwms.core.tools.transitcalendar.TransitCalendar;
import org.trescal.cwms.core.tools.transitcalendar.TransitCalendarService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.core.vdi.projection.PositionProjection;
import org.trescal.cwms.core.workallocation.engineerallocation.form.AllocateWorkToEngineerSearchForm;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.*;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_SelectNextWorkRequirement.Input;
import org.trescal.cwms.core.workflow.automation.antech.jobinterceptor.JobItemDeletion;
import org.trescal.cwms.core.workflow.dto.SelectedJobDTO;
import org.trescal.cwms.core.workflow.dto.SelectedJobItemDTO;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.itemstatus.db.ItemStatusService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLink;
import org.trescal.cwms.core.workflow.entity.stategrouplink.StateGroupLinkType;
import org.trescal.cwms.core.workflow.entity.workassignment.db.WorkAssignmentService;
import org.trescal.cwms.rest.tlm.dto.TLMJobItemDTO;
import org.trescal.cwms.spring.model.KeyValue;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.trescal.cwms.core.tools.DateTools.dateFromZonedDateTime;
import static org.trescal.cwms.core.tools.DateTools.dateToZonedDateTime;

@Service("jobItemService")
@Slf4j
public class JobItemServiceImpl extends AbstractJobItemServiceImpl<JobItem> implements JobItemService {

    private static final String CONTRACT_REVIEW = "Contract review";
    private static final String ONSITE_CONTRACT_REVIEW = "Onsite contract review";
    private static final String REPAIR_SERVICE_TYPE_LONGNAME = "Repair";
    public static final String ERROR_CODE_JOBITEM_NOT_FOUND_FOR_BARCODE = "error.jobitem.notfound.forbarcode";
    public static final String ERROR_CODE_JOBITEM_NOT_DESPATCH = "error.jobitem.notdespatch";
    public static final String ERROR_CODE_JOBITEM_NOT_TRANSIT = "error.jobitem.nottransit";

    @Value("${cwms.config.jobitem.fast_track_turn}")
    private int fastTrackTurn;
    @Value("${cwms.config.jobitem.delete.renumber}")
    private boolean renumberWhenDeleting;
    @Value("${cwms.config.fileserver.root}")
    private String fileServerRoot;
    @Autowired
    private AddressService addrServ;
    @Autowired
    private ActionOutcomeService aoServ;
    @Autowired
    private AsnService asnService;
    @Autowired
    private CalReqService calReqServ;
    @Autowired
    private ServiceCapabilityService serviceCapabilityService;
    @Autowired
    private CompanySettingsForAllocatedCompanyService companySettingsService;
    @Autowired
    private ContractReviewAdjustmentCostService conRevAdjCostServ;
    @Autowired
    private ContractReviewCalibrationCostService conRevCalCostServ;
    @Autowired
    private ContractReviewPurchaseCostService conRevPurCostServ;
    @Autowired
    private ContractReviewRepairCostService conRevRepCostServ;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private Discount discount;
    @Autowired
    private HookInterceptor_BookInItem interceptor_bookInItem;
    @Autowired
    private HookInterceptor_CleanItem interceptor_cleanItem;
    @Autowired
    private HookInterceptor_DespatchItem interceptor_despatchItem;
    @Autowired
    private HookInterceptor_OverrideStatus interceptor_overrideStatus;
    @Autowired
    private HookInterceptor_PlaceOnHold interceptor_placeOnHold;
    @Autowired
    private HookInterceptor_ReceiveItem interceptor_receiveItem;
    @Autowired
    private HookInterceptor_ReceivePayment interceptor_receivePayment;
    @Autowired
    private HookInterceptor_RecordAdditionalDemands interceptor_recordAdditionalDemands;
    @Autowired
    private HookInterceptor_TakeOffHold interceptor_takeOffHold;
    @Autowired
    private HookService hookService;
    @Autowired
    private JobItemGroupService groupServ;
    @Autowired
    private ImageService imageService;
    @Autowired
    private InstrumService instrumServ;
    @Autowired
    private ItemStatusService ItemStatusService;
    @Autowired
    private ItemStateService itemStateServ;
    @Autowired
    private JobItemActionService jiaServ;
    @Autowired
    private JobItemWorkRequirementService jobItemWorkRequirementService;
    @Autowired
    private JobItemDao jobItemDao;
    @Autowired
    private JobService jobServ;
    @Autowired
    private JobItemNotInvoicedService jobItemNotInvoicedService;
    @Autowired
    private LocationService locServ;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private POService poService;
    @Autowired
    private PriceLookupService priceLookupService;
    @Autowired
    private CapabilityAuthorizationService procAccServ;
    @Autowired
    private CapabilityService procServ;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private SubdivSettingsForAllocatedSubdivService subdivSettingsService;
    @Autowired
    private SupportedLocaleService supportedLocaleService;
    @Autowired
    private WorkAssignmentService workAssignmentServ;
    @Autowired
    private TransitCalendarService tcServ;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private WorkInstructionService wiServ;
    @Autowired
    private WorkRequirementService workRequirementService;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private HookInterceptor_SelectNextWorkRequirement hookInterceptor_SelectNextWorkRequirement;
    @Autowired
    private ContactService contacService;
    @Autowired
    private BaseStatusService baseStatusService;

    @Override
    protected AbstractJobItemDao<JobItem> getBaseDao() {
        return this.jobItemDao;
    }

    public void modifyJobStatus(Job job) {
        JobStatus jobStatus = baseStatusService.findDefaultStatus(JobStatus.class);
        job.setJs(jobStatus);
        jobServ.merge(job);
    }

    @Override
    public void addInstrumentsStraightToJob(List<ItemBasketWrapper> basketItems, Job job, Contact bookedInBy,
                                            Address bookingInAddr, boolean defaultServiceTypeUsage) {

        // look for discount defaults for this contact/sub/company or system
        // default
        Double calDiscount = discount.parseValue(
                discount.getValueHierarchical(Scope.CONTACT, job.getCon(), job.getOrganisation().getComp()).getValue());
        Set<Integer> allowedServiceTypeIds;
        if (defaultServiceTypeUsage) {
            allowedServiceTypeIds = Collections.singleton(job.getDefaultCalType().getServiceType().getServiceTypeId());
        } else {
            allowedServiceTypeIds = this.serviceTypeService.getIdsByJobType(job.getType());
        }
        Integer tempGroup = null;
        JobItemGroup group = null;
        int nextItemNo = this.getNextItemNo(job);
        // avoid creating two job items with the same instrument
        Set<Integer> instAlreadyAdded = new HashSet<>();
        List<Integer> instAlreadyExistInTheJob = this.getInstrumentIdsFromJob(job.getJobid());
        for (ItemBasketWrapper ibw : basketItems) {
            if (ibw.getInst() != null && !instAlreadyAdded.contains(ibw.getInst().getPlantid())
                    && !instAlreadyExistInTheJob.contains(ibw.getInst().getPlantid())) {
                instAlreadyAdded.add(ibw.getInst().getPlantid());
                JobItem ji = new JobItem();
                ji.setBookedInAddr(bookingInAddr);
                ji.setBookedInLoc(null);
                ji.setCurrentAddr(bookingInAddr);
                ji.setCurrentLoc(null);
                if (ibw.getInst().getComp().getCoid() != job.getCon().getSub().getComp().getCoid()) {
                    OnBehalfItem onBehalfItem = new OnBehalfItem();
                    onBehalfItem.setCompany(ibw.getInst().getComp());
                    onBehalfItem.setAddress(ibw.getInst().getCon().getDefAddress());
                    onBehalfItem.setSetOn(new Date());
                    onBehalfItem.setSetBy(bookedInBy);
                    onBehalfItem.setJobitem(ji);
                    ji.setOnBehalf(onBehalfItem);
                }
                ServiceType st = null;
                if (ibw.getQuotationItem() != null) {
                    st = ibw.getQuotationItem().getServiceType();
                }
                if (st == null && ibw.getInst().getDefaultServiceType() != null
                        && allowedServiceTypeIds.contains(ibw.getInst().getDefaultServiceType().getServiceTypeId())) {
                    // Only use default service type from instrument if
                    // compatible with job,
                    // otherwise we should use the job's default cal type
                    st = ibw.getInst().getDefaultServiceType();
                }
                if (st == null) {
                    // Fallback value for service type; will never be null
                    st = job.getDefaultCalType().getServiceType();
                }

                ji.setServiceType(st);
                ji.setReturnOption(job.getReturnOption());
                ji.setInOption(job.getInOption());
                ji.setItemNo(nextItemNo);
                ji.setJob(job);
                ji.setTurn(job.getDefaultTurn());
                ji.setInst(ibw.getInst());
                ji.setReqCleaning(false);

                if (ibw.getInst().getDefaultClientRef() != null) {
                    ji.setClientRef(ibw.getInst().getDefaultClientRef());
                }

                // use the default proc and wi from the basket wrapper (already
                // looked up by controller!)
                ji.setCapability(ibw.getProc());
                ji.setWorkInstruction(ibw.getWi());

                if (this.getDateIn(job) != null)
                    ji.setDateIn(this.getDateIn(job));
                else
                    ji.setDateIn(ZonedDateTime.now(LocaleContextHolder.getTimeZone().toZoneId()));

                // calculate due Date
                if (job.getAgreedDelDate() != null)
                    ji.setDueDate(job.getAgreedDelDate());
                else
                    ji.calculateAndSetDueDate();

                SubdivSettingsForAllocatedSubdiv subdivSettings = subdivSettingsService.get(job.getCon().getSub(),
                        job.getOrganisation());
                if (subdivSettings != null)
                    ji.setLastContractReviewBy(subdivSettings.getDefaultBusinessContact());

                // add a default contract review outcome
                ji.setConRevOutcome(this.getDefaultConRevOutcomeForItem(ji));

                // if the job has a default PO or BPO, add the item to it
                if (ji.getItemPOs() == null) {
                    ji.setItemPOs(new HashSet<>());
                }
                if (ji.getContract() != null && ji.getContract().getBpo() != null) {
                    JobItemPO jipo = new JobItemPO();
                    jipo.setItem(ji);
                    jipo.setBpo(ji.getContract().getBpo());
                    ji.getItemPOs().add(jipo);
                } else if (job.getBpo() != null) {
                    JobItemPO jipo = new JobItemPO();
                    jipo.setItem(ji);
                    jipo.setBpo(job.getBpo());
                    ji.getItemPOs().add(jipo);
                } else if (job.getDefaultPO() != null) {
                    JobItemPO jipo = new JobItemPO();
                    jipo.setItem(ji);
                    jipo.setPo(job.getDefaultPO());
                    ji.getItemPOs().add(jipo);
                }

                // if this item is to join a new group
                if (ibw.getTempGroupId() != null && ibw.getTempGroupId() != 0) {
                    if (!ibw.getTempGroupId().equals(tempGroup)) {
                        tempGroup = ibw.getTempGroupId();

                        group = new JobItemGroup();
                        group.setJob(job);
                        this.groupServ.insertJobItemGroup(group);
                    }

                    ji.setGroup(group);
                }
                // else if this item is to join an existing group
                else if (ibw.getExistingGroupId() != null) {
                    JobItemGroup existingGroup = this.groupServ.findJobItemGroup(ibw.getExistingGroupId());
                    ji.setGroup(existingGroup);
                }
                // else if this item is to join a base unit on job
                else if (ibw.getBaseItem() != null) {
                    // reload jobitem into session
                    JobItem baseItem = this.findJobItem(ibw.getBaseItem().getJobItemId());

                    if (baseItem.getGroup() != null) {
                        ji.setGroup(baseItem.getGroup());
                    } else {
                        // create new group for base item and new module
                        JobItemGroup baseModGroup = new JobItemGroup();
                        baseModGroup.setJob(job);
                        this.groupServ.insertJobItemGroup(baseModGroup);

                        // set the new group to the two items
                        ji.setGroup(baseModGroup);
                        baseItem.setGroup(baseModGroup);
                        baseItem = this.mergeJobItem(baseItem);
                        ibw.setBaseItem(baseItem);
                    }
                }
                this.insertJobItem(ji, bookedInBy, calDiscount, null);


                // If there is a linked quotation item, transfer notes as cal
                // reqs
                if (ibw.getQuotationItem() != null) {
                    this.addRequirementsFromQuoteNotes(ji, ibw.getQuotationItem(), bookedInBy);
                }
                this.updateJobItem(ji);
                nextItemNo++;
            }
        }
    }

    /**
     * Initializes one new work requirement based on the procedure + WI set on
     * the job item
     * <p>
     * JobItem must already have a set of JobItemWorkRequirements initialized.
     * Also sets the next work requirement on the job, if not yet set. This
     * method is used when new job items are created, after the job item has
     * been persisted
     * <p>
     * (the data model is a chicken-and-egg design, due to nextWorkRequirement
     * on JobItem hence the need to set / add work requirement after job item
     * save)
     * <p>
     * The new work requirement will be saved through JPA cascade when the job
     * item is updated; it is unnecessary to save here.
     */
    private void addNewWorkRequirement(JobItem ji) {
        // add new work requirement based on proc and wi
        WorkRequirement wr = new WorkRequirement();

        wr.setStatus(WorkrequirementStatus.PENDING);
        wr.setCapability(ji.getCapability());
        wr.setWorkInstruction(ji.getWorkInstruction());
        wr.setServiceType(ji.getServiceType());

        // add jobitem link for this wr
        JobItemWorkRequirement jiwr = new JobItemWorkRequirement();
        jiwr.setJobitem(ji);
        jiwr.setWorkRequirement(wr);

        this.workRequirementService.updateDepartmentAndAddress(wr, ji);

        // New work requirement will be saved via cascade
        ji.getWorkRequirements().add(jiwr);

        // set next work requirement if uninitalized
        if (ji.getNextWorkReq() == null) {
            ji.setNextWorkReq(jiwr);
        }
    }

    /**
     * This copies all the work requirements from an old job item to new job
     * item (generally used in creating new job item from old job item only)
     * This is no longer called at goods in, and all the WIs are now copied
     * (2018-06-24) The new JobItem should be already initialized with a set of
     * job item work requirements
     */
    private void copyPreviousCalRequirements(JobItem oldJobItem, JobItem ji) {
        // for each of its old requirements
        for (JobItemWorkRequirement jiwr : oldJobItem.getWorkRequirements()) {
            WorkRequirement oldWr = jiwr.getWorkRequirement();

            WorkRequirement newWr = new WorkRequirement();
            newWr.setStatus(WorkrequirementStatus.PENDING);
            newWr.setDepartment(oldWr.getDepartment());
            newWr.setType(oldWr.getType());
            newWr.setWorkInstruction(oldWr.getWorkInstruction());
            newWr.setCapability(oldWr.getCapability());
            newWr.setServiceType(oldWr.getServiceType());
            if (newWr.getDepartment() != null) {
                newWr.setAddress(oldWr.getDepartment().getAddress());
            } else {
                newWr.setAddress(oldWr.getAddress());
            }

            String newReq = oldWr.getRequirement();
            String prepend = "(Taken from last time item came in) ";
            // check field is not null and can fit the prepended warning
            if ((newReq != null) && (newReq.length() < (500 - prepend.length()))) {
                newReq = prepend.concat(newReq);
            }
            newWr.setRequirement(newReq);

            // add jobitem link for this wr
            JobItemWorkRequirement newJiwr = new JobItemWorkRequirement();
            newJiwr.setJobitem(ji);
            newJiwr.setWorkRequirement(newWr);
            // Saved via cascade from job item
            ji.getWorkRequirements().add(newJiwr);
            if (ji.getNextWorkReq() == null) {
                ji.setNextWorkReq(newJiwr);
            }
        }
    }

    /**
     * Creates individual Requirement entities for any QuoteItemNote associated
     * with a linked quotation for the job item. This is separated from any
     * CalReq entity so that those can continue to be used at their level
     * (ModelCompany, Instrument, JobItem...) rather than appending all notes
     * into one new JobItemCalReq.
     */
    @Override
    public void addRequirementsFromQuoteNotes(JobItem jobItem, Quotationitem qitem, Contact contact) {
        if (jobItem.getReq() == null) {
            jobItem.setReq(new TreeSet<>(new RequirementComparator()));
        }
        for (QuoteItemNote quoteItemNote : qitem.getNotes()) {
            if (quoteItemNote.isActive()) {
                Requirement requirement = new Requirement();
                requirement.setCreated(new Date());
                requirement.setCreatedBy(contact);
                requirement.setLabel(quoteItemNote.getLabel());
                requirement.setRequirement(quoteItemNote.getNote());
                requirement.setDeleted(false);
                requirement.setItem(jobItem);
                jobItem.getReq().add(requirement);
            }
        }
    }

    @Override
    public ResultWrapper ajaxExportItemDataToLabview(int jobItemId) {
        JobItem ji = this.findJobItem(jobItemId);
        boolean success = this.exportItemDataToLabview(ji);

        return new ResultWrapper(success, null);
    }

    @Override
    public void ajaxReceivePaymentForItems(List<String> jobItemIds) {
        List<Integer> itemIds = new ArrayList<>();
        for (String itemId : jobItemIds) {
            itemIds.add(Integer.parseInt(itemId));
        }

        this.receivePaymentForItems(itemIds);
    }

    @Override
    public ResultWrapper ajaxSetJobItemChaseOptions(int jobItemId, boolean chaseActive, boolean neverChase) {
        JobItem ji = this.findJobItem(jobItemId);

        if (ji == null) {
            return new ResultWrapper(false, "Job Item not found");
        }

        ji.setChasingActive(chaseActive);
        ji.setNeverToBeChased(neverChase);
        this.updateJobItem(ji);

        return new ResultWrapper(true, null, ji, null);
    }

    @Override
    public ResultWrapper ajaxSetJobItemNextWorkRequirement(int jobItemId, int wrId) {
        // get job item
        JobItem ji = this.findJobItem(jobItemId);
        // job item null?
        if (ji != null) {
            // get work requirement
            JobItemWorkRequirement workReq = this.jobItemWorkRequirementService.findJobItemWorkRequirement(wrId);
            // work requirement null?
            if (workReq != null) {
                setNextWorkRequirement(ji, workReq);
                // return successful result wrapper
                return new ResultWrapper(true, "", null, null);
            } else {
                return new ResultWrapper(false, "Work requirement could not be found", null, null);
            }
        } else {
            return new ResultWrapper(false, "Job item could not be found", null, null);
        }
    }

    @Override
    public void setNextWorkRequirement(JobItem ji, JobItemWorkRequirement workReq) {
        // set work requirement to job item
        ji.setNextWorkReq(workReq);
        // update proc
        this.updateProcIfNecessary(ji, workReq);
        // save updated work requirement
        this.jobItemDao.merge(ji);
        // advance the workflow if the current state belongs to the
        // group 'select next work reqruiement'
        if (this.hasStateGroupOfKeyName(ji.getState(), StateGroup.SELECT_NEXT_WORK_REQUIREMENT)
                && workReq.getWorkRequirement().getCapability() != null
                && !workReq.getWorkRequirement().getStatus().equals(WorkrequirementStatus.COMPLETE)
                && !workReq.getWorkRequirement().getStatus().equals(WorkrequirementStatus.CANCELLED)) {
            Input input = new Input(ji, null);
            hookInterceptor_SelectNextWorkRequirement.recordAction(input);
        }
    }

    @Override
    public LocalDate calculateNextTransitDate(JobItem ji) {
        TransitCalendar tc = this.tcServ.generateCalendar(ji.getCurrentAddr().getSub().getSubdivid(), 4);

        boolean calibrated = false;

        // check activities to see if item has been calibrated
        activityLoop:
        for (JobItemAction action : ji.getActions()) {
            if (action instanceof JobItemActivity) {
                JobItemActivity activity = (JobItemActivity) action;

                if (activity.isComplete()) {
                    for (StateGroupLink sgl : activity.getActivity().getGroupLinks()) {
                        if (sgl.getGroup().equals(StateGroup.CALIBRATION) && !activity.isDeleted()) {
                            calibrated = true;
                            break activityLoop;
                        }
                    }
                }
            }
        }

        LocalDate dueDate = ji.getDueDate();
        LocalDate transitDate = null;

        if (calibrated) {
            // does return option leave from current location?
            if (ji.getReturnOption().getSub() != null) {
                Subdiv leavingFrom = ji.getReturnOption().getSub();

                ClientTransit ct;

                // if due date has still not passed
                if (dueDate.isAfter(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()))) {
                    ct = this.tcServ.getLastClientTransitBeforeDate(tc, ji.getReturnOption().getId(), dueDate);
                }
                // else due date has passed or is today
                else {
                    ct = this.tcServ.getNextAvailableClientTransit(tc, ji.getReturnOption().getId());
                }

                if (leavingFrom.getSubdivid() == ji.getCurrentAddr().getSub().getSubdivid()) {
                    // send back to client
                    if (ct != null) {
                        transitDate = ct.getTransitDate();
                    }
                }
            } else {
                // client courier and courier
                transitDate = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
            }
        }

        return transitDate;
    }

    @Override
    public void changeItemLocation(int plantid, int locId) {
        List<JobItem> items = this.getActiveJobItemsFromBarcode(plantid);
        Location loc = this.locServ.get(locId);
        for (JobItem ji : items) {
            ji.setCurrentAddr(loc.getAdd());
            ji.setCurrentLoc(loc);
            this.jobItemDao.merge(ji);
        }
    }

    @Override
    public ChangeJobItemGroupWrapper changeJobItemGroup(int jobItemId, int groupId) {
        JobItem ji = this.findJobItem(jobItemId);
        JobItemGroup group = this.groupServ.findJobItemGroup(groupId);
        HashMap<Integer, String> fullModelNames = new HashMap<>();

        if (ji != null) {
            if (group == null) {
                ji.setGroup(null);

                ChangeJobItemGroupWrapper wrapper = new ChangeJobItemGroupWrapper(true, null, ji);

                fullModelNames.put(ji.getItemNo(), InstModelTools.modelNameViaTranslations(ji.getInst().getModel(),
                        LocaleContextHolder.getLocale(), supportedLocaleService.getPrimaryLocale()));

                wrapper.setModelNames(fullModelNames);

                return wrapper;
            } else if (group.getJob().getJobid() == ji.getJob().getJobid()) {
                ji.setGroup(group);

                if (group.getItems() == null) {
                    group.setItems(new TreeSet<>(new JobItemsInGroupComparator()));
                }
                group.getItems().add(ji);

                // reload all info for each job item
                for (JobItem item : group.getItems()) {
                    item = this.jobItemDao.find(item.getJobItemId());
                    fullModelNames.put(item.getItemNo(),
                            InstModelTools.modelNameViaTranslations(item.getInst().getModel(),
                                    LocaleContextHolder.getLocale(), supportedLocaleService.getPrimaryLocale()));
                }

                ChangeJobItemGroupWrapper wrapper = new ChangeJobItemGroupWrapper(true, null, group);

                wrapper.setModelNames(fullModelNames);

                return wrapper;
            }

            return new ChangeJobItemGroupWrapper(false, "Chosen group is not on the correct Job.", null);
        }

        return new ChangeJobItemGroupWrapper(false, "The Job Item could not be found.", null);
    }

    @Override
    public ResultWrapper changeJobItemInst(int jobitemid, String barcode, int personId) {

        Contact con = this.contacService.get(personId);
        ResultWrapper rs = new ResultWrapper();
        if (con != null) {
            if (!con.getUser().getAuthorities().contains(Permission.JI_CHANGE_INSTRUMENT)) {
                rs.setSuccess(false);
                rs.setMessage("User Don't have Permission to Change Instrument of the Current JobItem.");
            } else {

                if (NumberTools.isAnInteger(barcode.trim())) {
                    JobItem ji = this.jobItemDao.find(jobitemid);
                    Instrument inst = this.instrumServ.get(Integer.parseInt(barcode.trim()));

                    if (inst != null) {
                        boolean isfound = this.instrumServ.isInstrumentOnJob(Integer.parseInt(barcode.trim()),
                                ji.getJob().getJobid());
                        if (isfound) {
                            rs.setSuccess(false);
                            rs.setMessage("Can't add instrument twice to the same Job");
                        } else {
                            ji.setInst(inst);
                            ji = this.jobItemDao.merge(ji);
                            rs.setSuccess(true);
                            rs.setResults(ji);
                        }
                    } else {
                        rs.setSuccess(false);
                        rs.setMessage("The barcode entered did not match any instruments in the database.");
                    }
                } else {
                    rs.setSuccess(false);
                    rs.setMessage("The barcode entered was not a number.");
                }

            }
        }

        return rs;

    }

    @Override
    public void cleanItem(JobItem ji, int timeSpent, String remark) {
        HookInterceptor_CleanItem.Input input = new HookInterceptor_CleanItem.Input(ji, timeSpent, remark);
        this.interceptor_cleanItem.recordAction(input);
    }

    @Override
    public void copyContractReviewCosts(JobItem jiSrc, JobItem jiDest) {
        jiDest.setCalibrationCost(this.conRevCalCostServ.copyContractReviewCost(jiSrc.getCalibrationCost(), jiDest));
        jiDest.setAdjustmentCost(this.conRevAdjCostServ.copyContractReviewCost(jiSrc.getAdjustmentCost(), jiDest));
        jiDest.setRepairCost(this.conRevRepCostServ.copyContractReviewCost(jiSrc.getRepairCost(), jiDest));
        jiDest.setPurchaseCost(this.conRevPurCostServ.copyContractReviewCost(jiSrc.getPurchaseCost(), jiDest));
    }

    @Override
    public void copyItemsToNewJob(List<Integer> oldJobItemIds, Job job, Contact currentContact, int nextItemNo) {
        // look for discount defaults for this contact/sub/company or system
        // default
        Double calDiscount = discount.parseValue(
                discount.getValueHierarchical(Scope.CONTACT, job.getCon(), job.getOrganisation().getComp()).getValue());

        List<JobItem> oldItems = this.getAllItems(oldJobItemIds);

        if (job.getItems() == null) {
            job.setItems(new TreeSet<>(new JobItemComparator()));
        }

        for (JobItem oldItem : oldItems) {
            JobItem ji = new JobItem();
            ji.setCurrentAddr(job.getBookedInAddr());
            ji.setCurrentLoc(job.getBookedInLoc());
            ji.setBookedInAddr(job.getBookedInAddr());
            ji.setBookedInLoc(job.getBookedInLoc());

            ji.setServiceType(oldItem.getServiceType());
            ji.setReturnOption(job.getReturnOption());
            ji.setInOption(job.getInOption());
            ji.setItemNo(nextItemNo);
            nextItemNo++;
            ji.setJob(job);
            ji.setTurn(job.getDefaultTurn());
            ji.setInst(oldItem.getInst());
            ji.setReqCleaning(oldItem.getReqCleaning());
            ji.setCapability(oldItem.getCapability());
            ji.setWorkInstruction(oldItem.getWorkInstruction());

            if (oldItem.getClientRef() != null)
                ji.setClientRef(oldItem.getClientRef());

            this.copyPreviousCalRequirements(oldItem, ji);

            if (this.getDateIn(job) != null)
                ji.setDateIn(this.getDateIn(job));
            else
                ji.setDateIn(ZonedDateTime.now(LocaleContextHolder.getTimeZone().toZoneId()));

            ji.setAgreedDelDate(oldItem.getAgreedDelDate());
            ji.setDueDate(oldItem.getDueDate());

            // add a default contract review outcome
            ji.setConRevOutcome(this.getDefaultConRevOutcomeForItem(ji));

            // if the job has a default PO or BPO, add the item to it
            if (ji.getItemPOs() == null) {
                ji.setItemPOs(new HashSet<>());
            }
            if (ji.getContract() != null && ji.getContract().getBpo() != null) {
                JobItemPO jipo = new JobItemPO();
                jipo.setItem(ji);
                jipo.setBpo(ji.getContract().getBpo());
                ji.getItemPOs().add(jipo);
            } else if (job.getBpo() != null) {
                JobItemPO jipo = new JobItemPO();
                jipo.setItem(ji);
                jipo.setBpo(job.getBpo());
                ji.getItemPOs().add(jipo);
            } else if (job.getBpoLinks() != null) {
                BPO defaultBpo = job.getBpoLinks().stream().filter(JobBPOLink::isDefaultForJob).map(JobBPOLink::getBpo)
                        .findFirst().orElse(null);
                JobItemPO jipo = new JobItemPO();
                jipo.setItem(ji);
                jipo.setBpo(defaultBpo);
                ji.getItemPOs().add(jipo);
            } else if (job.getDefaultPO() != null) {
                JobItemPO jipo = new JobItemPO();
                jipo.setItem(ji);
                jipo.setPo(job.getDefaultPO());
                ji.getItemPOs().add(jipo);
            }


            this.insertJobItem(ji, currentContact, calDiscount, null);
            // add item to job
            job.getItems().add(ji);

            this.updateJobItem(ji);
        }
    }

    @Override
    public Set<JobItem> createJobItems(Job job, List<AsnItemFinalSynthesisDTO> identifiedJobItemsList, Contact addedBy,
                                       Integer asnId) {

        Set<JobItem> res = new HashSet<>();

        // get the responsible CSR that should do the contract review
        SubdivSettingsForAllocatedSubdiv sett = subdivSettingsService.get(job.getCon().getSub(), job.getOrganisation());
        Contact defaultBusinessContact = null;
        if (sett != null)
            defaultBusinessContact = sett.getDefaultBusinessContact();

        // key=instrument id, value = instrument id
        Map<Integer, Instrument> identifiedInstruments = instrumServ
                .fetchInstrumentsForJobItemCreation(identifiedJobItemsList.stream()
                        .map(AsnItemFinalSynthesisDTO::getIdTrescal).collect(Collectors.toList()));

        MultiValuedMap<Integer, Integer> instrumenModelIdAndCalibrationTypeId = new HashSetValuedHashMap<>();
        identifiedJobItemsList.forEach(iji -> instrumenModelIdAndCalibrationTypeId.put(
                identifiedInstruments.get(iji.getIdTrescal()).getModel().getModelid(),
                serviceTypeService.get(iji.getServicetypeid()).getCalibrationType().getCalTypeId()));

        MultiValuedMap<Integer, Integer> subFamilyIdAndServiceTypeId = new HashSetValuedHashMap<>();
        identifiedJobItemsList.forEach(iji -> subFamilyIdAndServiceTypeId.put(
                identifiedInstruments.get(iji.getIdTrescal()).getModel().getDescription().getId(),
                iji.getServicetypeid()));

        List<Triple<Integer, Integer, ServiceCapability>> serviceCapabilities = new ArrayList<>();
        List<Triple<Integer, Integer, Capability>> procedures;
        // get procedures
        procedures = procServ.getActiveCapabilitiesUsingListOfCapabilityFilter(subFamilyIdAndServiceTypeId,
                job.getOrganisation().getId());
        if (procedures.isEmpty() || procedures.size() < identifiedJobItemsList.size()) {
            // get service capabilities
            serviceCapabilities = serviceCapabilityService
                    .getCalServiceCapabilities(instrumenModelIdAndCalibrationTypeId, job.getOrganisation().getId());
        }

        // Note, new price lookup service is always used (removed async code
        // 2019-09-25)
        BigDecimal calDiscount = BigDecimal.valueOf(discount.parseValue(discount
                .getValueHierarchical(Scope.CONTACT, job.getCon(), job.getOrganisation().getComp()).getValue()));
        PriceLookupRequirements reqs = new PriceLookupRequirements(job.getOrganisation().getComp().getId(),
                job.getCon().getSub().getComp().getCoid(),
                job.getOrganisation().getComp().getBusinessSettings().getUsePriceCatalog());
        PriceLookupInputFactory factory = new PriceLookupInputFactory(reqs);
        // Exchange format code uses cal type ids for final user selection
        for (AsnItemFinalSynthesisDTO dto : identifiedJobItemsList) {
            factory.addInstrument(dto.getServicetypeid(), dto.getIdTrescal());
        }

        this.priceLookupService.findPrices(reqs);

        List<PO> jobPoList = new ArrayList<>();
        if (asnId != null && asnId != 0) {
            // load already created POs
            Asn asn = asnService.get(asnId);
            if (asn != null)
                jobPoList.addAll(asn.getPrebookingJobDetails().getPoList());
        }

        // get 'book-in-item' hook and attached activity
        Hook bookInItemHook = hookService.findHookByName(HookInterceptor_BookInItem.HOOK_NAME);
        ItemActivity unitreceivedItemActivity = bookInItemHook.getActivity();

        // job data
        Address bookedInAddr = job.getBookedInAddr();
        TransportOption returnOption = job.getReturnOption();
        TransportOption inOption = job.getInOption();

        ActionOutcome defaultContractReviewActionOutcome;
        ItemActivity activity;
        if (job.getType().equals(JobType.SITE)) {
            activity = this.itemStateServ.findItemActivityByName(ONSITE_CONTRACT_REVIEW);
        } else {
            activity = this.itemStateServ.findItemActivityByName(CONTRACT_REVIEW);
        }
        defaultContractReviewActionOutcome = this.aoServ.findDefaultActionOutcomeForActivity(activity.getStateid());

        int itemNo = 0;
        // initialize itemNo in case we have the jobitems
        List<JobItem> itemsList;
        if (!ObjectUtils.isEmpty(job.getItems())) {
            itemsList = new ArrayList<>(job.getItems());
            itemNo = itemsList.get(job.getItems().size() - 1).getItemNo();
        }

        for (AsnItemFinalSynthesisDTO dto : identifiedJobItemsList) {
            itemNo++;

            // model, comp, con, con.defaultAddr
            Instrument inst = identifiedInstruments.get(dto.getIdTrescal());

            JobItem ji = new JobItem();

            // set calibration frequency and interval unit values
            if (!ObjectUtils.isEmpty(dto.getCalibrationFrequency())) {
                inst.setCalFrequency(dto.getCalibrationFrequency());
            }
            if (!ObjectUtils.isEmpty(dto.getIntervalUnit())) {
                inst.setCalFrequencyUnit(dto.getIntervalUnit());
            }

            // set fields
            ji.setBookedInAddr(bookedInAddr);
            ji.setBookedInLoc(null);
            ji.setCurrentAddr(bookedInAddr);
            ji.setCurrentLoc(null);
            ji.setLastContractReviewBy(defaultBusinessContact);
            ji.setReturnOption(returnOption);
            ji.setInOption(inOption);
            ji.setItemNo(itemNo);
            ji.setJob(job);
            ji.setTurn(job.getDefaultTurn());
            ji.setInst(inst);
            ji.setReqCleaning(false);
            ServiceType st = serviceTypeService.get(dto.getServicetypeid());
            ji.setServiceType(st);

            // if client ref is specified in the ef we will use it
            // else we will use the default client ref from the instrument
            if (dto.getJobitemClientRef() != null)
                ji.setClientRef(dto.getJobitemClientRef());
            else
                ji.setClientRef(inst.getDefaultClientRef());

            // set ji date in
            if (this.getDateIn(job) != null)
                ji.setDateIn(this.getDateIn(job));
            else
                ji.setDateIn(ZonedDateTime.now(LocaleContextHolder.getTimeZone().toZoneId()));

            // calculate due Date
            if (job.getAgreedDelDate() != null)
                ji.setDueDate(job.getAgreedDelDate());
            else
                ji.calculateAndSetDueDate();

            // on-behalf : check if the instrument belongs to another company
            // that is defined in the job
            if (inst.getComp().getCoid() != job.getCon().getSub().getComp().getCoid()) {
                OnBehalfItem onBehalfItem = new OnBehalfItem();
                onBehalfItem.setCompany(inst.getComp());
                onBehalfItem.setAddress(inst.getCon().getDefAddress());
                onBehalfItem.setSetOn(new Date());
                onBehalfItem.setSetBy(addedBy);
                onBehalfItem.setJobitem(ji);
                ji.setOnBehalf(onBehalfItem);
            }

            // create work requirement
            WorkRequirement wr = new WorkRequirement();
            wr.setStatus(WorkrequirementStatus.PENDING);
            if (!procedures.isEmpty() && procedures.size() == identifiedJobItemsList.size()) {
                Triple<Integer, Integer, Capability> proc = procedures.stream()
                        .filter(triple -> triple.getLeft() != null && triple.getMiddle() != null
                                && triple.getRight() != null)
                        .filter(triple -> triple.getLeft().equals(inst.getModel().getDescription().getId())
                                && triple.getMiddle().equals(dto.getServicetypeid()))
                        .findFirst().orElse(null);
                if (proc != null) {
                    // set procedure in job item
                    ji.setCapability(proc.getRight());
                    // set procedure in work requirement
                    wr.setCapability(proc.getRight());
                }
            } else {
                Triple<Integer, Integer, ServiceCapability> tr = serviceCapabilities.stream()
                        .filter(triple -> triple.getLeft() != null && triple.getMiddle() != null
                                && triple.getRight() != null && triple.getRight().getCapability().isActive())
                        .filter(triple -> triple.getLeft().equals(inst.getModel().getModelid())
                                && triple.getMiddle().equals(serviceTypeService.get(dto.getServicetypeid())
                                .getCalibrationType().getCalTypeId()))
                        .findFirst().orElse(null);
                if (tr != null) {
                    // set procedure in job item
                    ji.setCapability(tr.getRight().getCapability());
                    ji.setWorkInstruction(tr.getRight().getWorkInstruction());
                    // set procedure in work requirement
                    wr.setCapability(tr.getRight().getCapability());
                    wr.setWorkInstruction(tr.getRight().getWorkInstruction());
                }
            }

            wr.setServiceType(serviceTypeService.get(dto.getServicetypeid()));

            JobItemWorkRequirement jiwr = new JobItemWorkRequirement();
            jiwr.setJobitem(ji);
            jiwr.setWorkRequirement(wr);
            this.workRequirementService.updateDepartmentAndAddress(wr, ji);

            ji.getWorkRequirements().add(jiwr);
            ji.setNextWorkReq(jiwr);

            // add a default contract review outcome
            ji.setConRevOutcome(defaultContractReviewActionOutcome);

            // PO/BPO
            ji.setItemPOs(new HashSet<>());
            JobItemPO jipo = new JobItemPO();
            // find correspondant PO(if exists) from file/prebooking
            String poNumber = dto.getPoNumber();
            jobPoList.stream().filter(poo -> poo.getPoNumber().equalsIgnoreCase(poNumber)).findAny()
                    .ifPresent(jipo::setPo);
            if (ji.getContract() != null && ji.getContract().getBpo() != null) {
                jipo.setBpo(ji.getContract().getBpo());
            } else if (job.getBpo() != null)
                jipo.setBpo(job.getBpo());
            if (jipo.getBpo() != null || jipo.getPo() != null) {
                jipo.setItem(ji);
                ji.getItemPOs().add(jipo);
            }

            // calculate calibration location and address
            this.lookUpCalAddress(ji);

            // set status
            ji.setState(itemStateServ.getReferenceOrNull(dto.getNextStatuId()));

            // set action record
            JobItemActivity jia = new JobItemActivity();
            jia.setCreatedOn(new Date());
            jia.setActivity(unitreceivedItemActivity);
            jia.setActivityDesc(unitreceivedItemActivity.getDescription());
            jia.setStartStamp(new Date());
            jia.setStartedBy(addedBy);
            jia.setJobItem(ji);
            jia.setComplete(true);
            jia.setEndStamp(new Date());
            jia.setTimeSpent(0);
            jia.setCompletedBy(addedBy);
            jia.setEndStatus(ItemStatusService.getReferenceOrNull(dto.getNextStatuId()));

            ji.setActions(new TreeSet<>(new JobItemActionComparator()));
            ji.getActions().add(jia);

            // If job performed within same company as contact, exclude from
            // invoicing
            // Since job item isn't saved yet, not invoiced reason is saved
            // through JPA cascade
            if (ji.getJob().getCon().getSub().getComp().getCoid() == ji.getJob().getOrganisation().getComp()
                    .getCoid()) {
                NotInvoicedReason reason;
                if (ji.getJob().getCon().getSub().getSubdivid() == ji.getJob().getOrganisation().getSubdivid()) {
                    reason = NotInvoicedReason.INTERNAL_JOB;
                } else {
                    reason = NotInvoicedReason.INTER_SUBDIVISION;
                }
                JobItemNotInvoiced notInvoiced = new JobItemNotInvoiced();
                notInvoiced.setComments("");
                notInvoiced.setContact(addedBy);
                notInvoiced.setDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
                notInvoiced.setJobItem(ji);
                notInvoiced.setReason(reason);
                ji.setNotInvoiced(notInvoiced);
            }

            Integer serviceTypeId = dto.getServicetypeid();
            PriceLookupInput input = reqs.getInputByInstrumentId(serviceTypeId, dto.getIdTrescal());
            this.setDefaultCostsFromPriceLookup(ji, input, calDiscount);

            res.add(ji);
        }

        return res;
    }

    @Override
    public List<JobItem> createJobItems(Integer jobId, Map<Integer, Integer> plantIdsAndDefaultSt,
                                        Integer bookedInByContactId, Integer bookingInAddrId) {

        List<JobItem> jobitems = new ArrayList<>();
        Job job = jobServ.get(jobId);
        List<Instrument> instruments = instrumServ.getInstruments(new ArrayList<>(plantIdsAndDefaultSt.keySet()));
        Contact bookedInBy = contacService.get(bookedInByContactId);
        Address bookingInAddr = addrServ.get(bookingInAddrId);
        SubdivSettingsForAllocatedSubdiv subdivSettings = subdivSettingsService.get(job.getCon().getSub(),
                job.getOrganisation());

        // look for discount defaults for this contact/sub/company or system
        // default
        Double calDiscount = discount.parseValue(
                discount.getValueHierarchical(Scope.CONTACT, job.getCon(), job.getOrganisation().getComp()).getValue());

        for (Integer plantId : plantIdsAndDefaultSt.keySet()) {
            // get instrument
            val instOpt = instruments.stream().filter(i -> i.getPlantid() == plantId).findAny();
            // get cal type
            CalibrationType calType;
            if (plantIdsAndDefaultSt.get(plantId) != null) {
                calType = serviceTypeService.get(plantIdsAndDefaultSt.get(plantId)).getCalibrationType();
            } else {
                calType = job.getDefaultCalType();
            }

            Integer nextItemNo = this.getNextItemNo(job);
            val jiOpt = instOpt.map(inst -> {
                // get procedure using capability service type filters
                val procOpt = procServ.getActiveCapabilityUsingCapabilityFilter(
                        inst.getModel().getDescription().getId(), calType.getServiceType().getServiceTypeId(),
                        job.getOrganisation().getSubdivid());
                Capability proc;
                if (procOpt.isPresent()) {
                    proc = procOpt.get();
                } else {
                    // get procedure using ServiceCapability (if present!)
                    ServiceCapability sc = serviceCapabilityService.getCalService(inst.getModel(), calType,
                            job.getOrganisation());
                    proc = sc != null ? sc.getCapability() : null;
                }
                return this.createJobitem(job, inst, bookedInBy, bookingInAddr, subdivSettings, nextItemNo, calType,
                        proc, null, null);
            });

            jiOpt.ifPresent(ji -> {

                this.insertJobItem(ji, bookedInBy, calDiscount, null);
                // add item to job
                job.getItems().add(ji);

                jobitems.add(ji);
            });
        }
        return jobitems;
    }

    @Override
    public List<Integer> createJobItems(List<Integer> procIds, List<Integer> workInstIds, List<String> itemPOs,
                                        List<ItemBasketWrapper> basketItems, Job job, List<Integer> defaultServiceTypeIds,
                                        List<Integer> bookedInByContactIds, Address bookingInAddr, List<Date> datesIn) {


        List<Integer> newJobItemIds = new ArrayList<>();
        Integer tempGroup = null;
        JobItemGroup group = null;
        SubdivSettingsForAllocatedSubdiv subdivSettings = subdivSettingsService.get(job.getCon().getSub(),
                job.getOrganisation());
        // look for discount defaults for this contact/sub/company or system
        // default
        Double calDiscount = discount.parseValue(
                discount.getValueHierarchical(Scope.CONTACT, job.getCon(), job.getOrganisation().getComp()).getValue());

        int i = 0;
        int nextItemNo = this.getNextItemNo(job);
        List<ServiceType> allServiceTypes = serviceTypeService.getAll();
        for (ItemBasketWrapper ibw : basketItems) {
            if (ibw.getInst() != null) {
                Instrument inst = ibw.getInst();

                // get caltype
                CalibrationType ct = null;
                if (defaultServiceTypeIds != null && defaultServiceTypeIds.get(i) != null) {
                    Integer defaultServiceTypeId = defaultServiceTypeIds.get(i);
                    ct = allServiceTypes.stream()
                            .filter(ss -> ss.getServiceTypeId() == defaultServiceTypeId
                                    && ss.getCalibrationType() != null)
                            .map(ServiceType::getCalibrationType).findFirst().orElse(null);

                } else if (ibw.getQuotationItem() != null) {

                    ServiceType st = ibw.getQuotationItem().getServiceType();
                    if (st != null)
                        ct = st.getCalibrationType();
                } else {
                    ct = job.getDefaultCalType();
                }

                // procedure
                Capability proc = (procIds == null || procIds.get(i) == null) ? null : this.procServ.get(procIds.get(i));

                // work instruction
                WorkInstruction wi = (workInstIds == null || workInstIds.get(i) == null) ? null
                        : this.wiServ.get(workInstIds.get(i));

                Date dateIn;
                if (datesIn != null && datesIn.get(i) != null)
                    dateIn = datesIn.get(i);
                else if (getDateIn(job) != null)
                    dateIn = DateTools.dateFromZonedDateTime(getDateIn(job));
                else
                    dateIn = new Date();

                // book in by contact
                Contact bookedInBy = (bookedInByContactIds == null || bookedInByContactIds.get(i) == null) ? null
                        : contacService.get(bookedInByContactIds.get(i));

                JobItem ji = createJobitem(job, inst, bookedInBy, bookingInAddr, subdivSettings, nextItemNo, ct, proc,
                        wi, dateIn);

                // set the PO of the item
                JobItemPO link = null;
                if (itemPOs != null && itemPOs.size() > i && itemPOs.get(i) != null) {
                    String poNumber = itemPOs.get(i).trim();

                    if (!poNumber.isEmpty()) {
                        AbstractPO match = this.jobServ.getMatchingJobPO(job.getJobid(), poNumber);

                        if (match != null) {
                            link = new JobItemPO();
                            link.setItem(ji);

                            if (match instanceof BPO) {
                                link.setBpo((BPO) match);
                            } else {
                                link.setPo((PO) match);
                            }
                        } else {
                            // create a new PO with the given po number
                            PO newPO = new PO();
                            newPO.setActive(true);
                            newPO.setPoNumber(poNumber);
                            newPO.setJob(job);
                            newPO.setOrganisation(job.getOrganisation().getComp());
                            poService.save(newPO);
                            // and create a PO link to the new job item
                            link = new JobItemPO();
                            link.setItem(ji);
                            link.setPo(newPO);
                        }
                    }
                }

                // if this item is to join a new group
                if (ibw.getTempGroupId() != null && ibw.getTempGroupId() != 0) {
                    if (!ibw.getTempGroupId().equals(tempGroup)) {
                        tempGroup = ibw.getTempGroupId();

                        group = new JobItemGroup();
                        group.setJob(job);
                        this.groupServ.insertJobItemGroup(group);
                    }

                    ji.setGroup(group);
                }
                // else if this item is to join an existing group
                else if (ibw.getExistingGroupId() != null && ibw.getExistingGroupId() != 0) {
                    JobItemGroup existingGroup = this.groupServ.findJobItemGroup(ibw.getExistingGroupId());
                    ji.setGroup(existingGroup);
                }
                // else if this item is to join a base unit on job
                else if (ibw.getBaseItem() != null) {
                    // reload jobitem into session
                    JobItem baseItem = this.findJobItem(ibw.getBaseItem().getJobItemId());

                    if (baseItem.getGroup() != null) {
                        ji.setGroup(baseItem.getGroup());
                    } else {
                        // create new group for base item and new module
                        JobItemGroup baseModGroup = new JobItemGroup();
                        baseModGroup.setJob(job);
                        this.groupServ.insertJobItemGroup(baseModGroup);

                        // set the new group to the two items
                        ji.setGroup(baseModGroup);
                        baseItem.setGroup(baseModGroup);
                        baseItem = this.mergeJobItem(baseItem);
                        ibw.setBaseItem(baseItem);
                    }
                }

                // adding this set to the job item persists it automatically
                ji.setItemPOs(new HashSet<>());
                if (link != null) {
                    ji.getItemPOs().add(link);
                }

                this.insertJobItem(ji, bookedInBy, calDiscount, null);
                // If there is a linked quotation item, transfer notes as cal
                // reqs
                if (ibw.getQuotationItem() != null) {
                    this.addRequirementsFromQuoteNotes(ji, ibw.getQuotationItem(), bookedInBy);
                    this.updateJobItem(ji);
                }

                newJobItemIds.add(ji.getJobItemId());
                i++;
                nextItemNo++;
            }
        }

        return newJobItemIds;
    }

    @Override
    public List<PositionProjection> getPositionProjectionForJob(Integer jobId) {
        return jobItemDao.getPositionProjectionForJob(jobId);
    }

    private JobItem createJobitem(Job job, Instrument inst, Contact bookedInBy, Address bookingInAddr,
                                  SubdivSettingsForAllocatedSubdiv subdivSettings, Integer nextItemNo, CalibrationType calType,
                                  Capability proc, WorkInstruction wi, Date dateIn) {
        JobItem ji = new JobItem();
        ji.setBookedInAddr(bookingInAddr);
        ji.setBookedInLoc(null);
        ji.setCurrentAddr(bookingInAddr);
        ji.setCurrentLoc(null);
        if (inst.getComp().getCoid() != job.getCon().getSub().getComp().getCoid()) {
            OnBehalfItem onBehalfItem = new OnBehalfItem();
            onBehalfItem.setCompany(inst.getComp());
            onBehalfItem.setAddress(inst.getCon().getDefAddress());
            onBehalfItem.setSetOn(new Date());
            onBehalfItem.setSetBy(bookedInBy);
            onBehalfItem.setJobitem(ji);
            ji.setOnBehalf(onBehalfItem);
        }
        if (subdivSettings != null)
            ji.setLastContractReviewBy(subdivSettings.getDefaultBusinessContact());

        ji.setReturnOption(job.getReturnOption());
        ji.setInOption(job.getInOption());
        ji.setItemNo(nextItemNo);
        ji.setJob(job);
        ji.setTurn(job.getDefaultTurn());
        ji.setInst(inst);
        ji.setReqCleaning(false);
        ji.setCapability(proc);
        ji.setWorkInstruction(wi);

        if (inst.getDefaultClientRef() != null)
            ji.setClientRef(inst.getDefaultClientRef());

        if (dateIn != null) {
            ji.setDateIn(dateToZonedDateTime(dateIn));
        } else if (this.getDateIn(job) != null)
            ji.setDateIn(this.getDateIn(job));
        else
            ji.setDateIn(ZonedDateTime.now(LocaleContextHolder.getTimeZone().toZoneId()));

        // calculate due Date
        if (job.getAgreedDelDate() != null)
            ji.setDueDate(job.getAgreedDelDate());
        else
            ji.calculateAndSetDueDate();

        // add a default contract review outcome
        ji.setConRevOutcome(this.getDefaultConRevOutcomeForItem(ji));

        ji.setServiceType(calType.getServiceType());
        if (inst.getDefaultServiceType() == null) {
            // Initialize default service type only if unset; saved as managed
            // JPA entity
            ServiceType serviceType = calType.getServiceType();
            inst.setDefaultServiceType(serviceType);
        }

        return ji;
    }

    @Override
    public void deleteJobItem(final JobItem ji) {
        int plantid = ji.getInst().getPlantid();
        this.jobItemDao.remove(ji);
        // update the recalled not received field of the instrument - maybe
        // switch this to use around adivce on this function?
        this.instrumServ.updateInstrumentRecalledNotReceived(plantid);
    }

    @Override
    @JobItemDeletion
    public ResultWrapper deleteJobItemWithChecks(int id) {
        JobItem ji = this.findJobItem(id);
        String message = null;

        int cals = ji.getCalLinks().size();
        int certs = ji.getCertLinks().size();
        int purOrds = ji.getPurchaseOrderItems().size();
        int invs = ji.getInvoiceItems().size();
        int costings = ji.getJobCostingItems().size();
        int dels = ji.getDeliveryItems().size();
        boolean group = ji.getGroup() != null;

        if ((cals <= 0) && (certs <= 0) && (purOrds <= 0) && (invs <= 0) && (costings <= 0) && (dels <= 0)
                && (!group)) {
            // po links are deleted through cascade (check BPO!!!)

            // accessories

            // delete actions
            for (JobItemAction jia : ji.getActions()) {
                this.jiaServ.delete(jia);
            }
            // delete calreqs + history
            this.calReqServ.deleteAllCalReqsImmediatelyAffectingJobItem(ji.getJobItemId());
            if (ji.getInst().getScrappedOnJI() != null && ji.getInst().getScrappedOnJI().equals(ji)) {
                ji.getInst().setScrappedOnJI(null);
            }

            try {
                int idToDelete = ji.getJobItemId();

                // renumber items if required
                if (this.renumberWhenDeleting) {
                    this.moveItemToBottomOfJob(ji);
                    message = "Some items may have been re-numbered. Please check any labels and re-print if neccessary.";
                }

                // get items that aren't being deleted
                Set<JobItem> jis = new TreeSet<>(new JobItemComparator());
                for (JobItem p : ji.getJob().getItems()) {
                    if (p.getJobItemId() != idToDelete) {
                        jis.add(p);
                    }
                }
                // set remaining job items into job
                ji.getJob().setItems(jis);

                this.deleteJobItem(ji);

                return new ResultWrapper(true, message, null, null);
            } catch (Exception e) {
                log.error("Caught exception", e);
                return new ResultWrapper(false,
                        "The job item was eligible for deletion, but an error occurred during the delete.", null, null);
            }
        } else {
            String reason = "";

            reason = (cals > 0) ? reason + " " + cals + " calibrations," : reason;
            reason = (certs > 0) ? reason + " " + certs + " certificates," : reason;
            reason = (purOrds > 0) ? reason + " " + purOrds + " purchase orders," : reason;
            reason = (invs > 0) ? reason + " " + invs + " invoices," : reason;
            reason = (costings > 0) ? reason + " " + costings + " costings," : reason;
            reason = (dels > 0) ? reason + " " + dels + " deliveries," : reason;
            reason = (group) ? reason + " a group," : reason;

            message = "Job Item " + ji.getItemNo() + " on Job " + ji.getJob().getJobno()
                    + " cannot be deleted because it has" + reason;

            return new ResultWrapper(false, message, null, null);
        }
    }

    @Override
    public ResultWrapper despatchItem(int barcode) {
        JobItem ji = this.findActiveJobItemFromBarcode(barcode);

        if (ji == null) {
            String message = this.messageSource.getMessage(ERROR_CODE_JOBITEM_NOT_FOUND_FOR_BARCODE, null,
                    "Active job item not found for instrument barcode", LocaleContextHolder.getLocale());
            return new ResultWrapper(false, message);
        }

        if (!this.itemCanBeDespatched(ji)) {
            String message = this.messageSource.getMessage(ERROR_CODE_JOBITEM_NOT_DESPATCH, null,
                    "This job item is not ready to be despatched", LocaleContextHolder.getLocale());
            return new ResultWrapper(false, message);
        }

        // if item is in a group that is to be delivered together AND the item
        // has modules
        if ((ji.getGroup() != null) && ji.getGroup().isDeliveryGroup() && (ji.getInst().getModules() != null)
                && (ji.getInst().getModules().size() > 0)) {
            // for each of the items in the group
            for (JobItem groupItem : ji.getGroup().getItems()) {
                // if this isn't the unit that was scanned
                if (groupItem.getJobItemId() != ji.getJobItemId()) {
                    // check if the item is one of the base unit's modules
                    for (Instrument module : ji.getInst().getModules()) {
                        // if the item matches a module
                        if (groupItem.getInst().getPlantid() == module.getPlantid()) {
                            // check that the item is awaiting despatch
                            if (this.itemCanBeDespatched(groupItem)) {
                                // despatch the module
                                this.despatchItem(groupItem.getInst().getPlantid());
                            }
                        }
                    }
                }
            }
        }

        Address fromAddr = ji.getCurrentAddr();
        Location fromLoc = ji.getCurrentLoc();
        ji.setCurrentAddr(null);
        ji.setCurrentLoc(null);
        Address toAddr = null;
        boolean isBusinessTransit = false, isThirdPartyTransit = false, isLabourTransit = false,
                isInspectionTransit = false;
        log.debug("ITERATE THROUGH ALL STATE GROUP LINKS");
        for (StateGroupLink sgl : ji.getState().getGroupLinks()) {
            log.debug("SGL TEST: " + sgl.getGroup());
            if (sgl.getGroup().equals(StateGroup.BUSINESSTRANSIT)) {
                isBusinessTransit = true;
                log.info("isBusinessTransit");
            }

            if (sgl.getGroup().equals(StateGroup.TPTRANSIT)) {
                isThirdPartyTransit = true;
                log.info("isThirdPartyTransit");
            }

            if (sgl.getGroup().equals(StateGroup.LABOURTRANSIT)) {
                isLabourTransit = true;
                log.info("isLabourTransit");
            }

            if (sgl.getGroup().equals(StateGroup.INSPECTIONTRANSIT)) {
                isInspectionTransit = true;
                log.info("isInspectionTransit");
            }
        }
        // if this item is going to another business company
        if (isBusinessTransit) {
            if (isLabourTransit) {
                toAddr = ji.getCalAddr();
                log.info("labourTransit: Using cal address of " + toAddr);
            } else if (isInspectionTransit) {
                toAddr = ji.getCalAddr();
                log.info("inspectionTransit: Using cal address of " + toAddr);
            } else {
                Subdiv toSub = ji.getReturnOption().getSub();
                log.debug("default: Using address from return option subdiv " + toSub);
                if (toSub.getDefaultAddress() != null)
                    toAddr = toSub.getDefaultAddress();
                else
                    toAddr = toSub.getAddresses().iterator().next();
                log.info("inspectionTransit: Using address of " + toAddr);
            }
        }
        // If this item is going to a third party
        else if (isThirdPartyTransit) {
            if (ji.getTpRequirements().size() > 0) {
                TreeSet<TPRequirement> reqs = new TreeSet<>(new TPRequirementComparator());
                reqs.addAll(ji.getTpRequirements());
                // get most recent third party requirements on item
                TPRequirement tpReq = reqs.first();
                // get address from third party requirement delivery note
                Address addr = tpReq.getDeliveryItem().getDelivery().getAddress();
                log.info("Third Party Transit, using ThirdPartRequirement DeliveryItem Delivery address");
                ji.setCurrentAddr(addr);
                toAddr = addr;
            } else {
                for (JobDeliveryItem jdi : ji.getDeliveryItems())
                    if (jdi.getDelivery().isThirdPartyDelivery()) {
                        Address addr = jdi.getDelivery().getAddress();
                        log.info("Third Party Transit, using JobDeliveryItem address");
                        ji.setCurrentAddr(addr);
                        toAddr = addr;
                    }
            }
        }
        // else this item is going to client
        else {
            log.info("Returning to client, using JobItem or Job returnTo address");
            // get latest job delivery item
            val jd = ji.getDeliveryItems().stream()
                    .filter(jdi -> jdi.getDelivery().getType().equals(DeliveryType.CLIENT)).findFirst();
            toAddr = jd.map(JobDeliveryItem::getDelivery).map(Delivery::getAddress).orElse(null);
        }
        ji = this.jobItemDao.merge(ji);
        HookInterceptor_DespatchItem.Input input = new HookInterceptor_DespatchItem.Input(ji, fromAddr, fromLoc, toAddr,
                null);
        this.interceptor_despatchItem.recordAction(input);
        return new ResultWrapper(true, null, null, null);
    }

    @Override
    public boolean exportItemDataToLabview(JobItem ji) {
        String labviewDir = this.fileServerRoot.concat(File.separator).concat("Labview");
        File dir = new File(labviewDir);
        dir.mkdirs();
        File file = new File(dir.getAbsolutePath().concat(File.separator).concat(ji.getLabviewCode()).concat(".txt"));
        PrintWriter out = null;
        boolean success = false;
        try {
            // reload job to set directory
            Job j = this.jobServ.get(ji.getJob().getJobid());
            Instrument i = ji.getInst();
            InstrumentModel m = i.getModel();
            Mfr mfr = (m.getModelMfrType().equals(ModelMfrType.MFR_SPECIFIC)) ? m.getMfr() : i.getMfr();
            Address a;
            if (ji.getReturnToAddress() != null)
                a = ji.getReturnToAddress();
            else
                a = j.getReturnTo();
            Certificate c = null;
            if (ji.getCertLinks() != null && ji.getCertLinks().size() > 0) {
                TreeSet<CertLink> links = new TreeSet<>(new CertLinkComparator());
                links.addAll(ji.getCertLinks());
                c = links.last().getCert();
            }
            AbstractPO po = null;
            if (ji.getItemPOs() != null && ji.getItemPOs().size() > 0) {
                JobItemPO jipo = ji.getItemPOs().iterator().next();
                po = (jipo.getBpo() != null) ? jipo.getBpo() : jipo.getPo();
            }
            out = new PrintWriter(new BufferedWriter(new FileWriter(file)));
            out.println(
                    "\"JOBNO\",\"ITEMNO\",\"PLANTID\",\"PLANTNO\",\"SERIALNO\",\"INSTMODEL\",\"DESCRIPTION\",\"MFR\",\"COMPANY\",\"ADDR1\",\"ADDR2\",\"ADDR3\",\"TOWN\",\"POSTCODE\",\"CONTACT\",\"JOB PATH\",\"CERTNO\",\"PURORDER\",\"DATEIN\"");
            out.print("\"" + j.getJobno() + "\"" + ",");
            out.print("\"" + ji.getItemNo() + "\"" + ",");
            out.print("\"" + i.getPlantid() + "\"" + ",");
            out.print("\"" + i.getPlantno() + "\"" + ",");
            out.print("\"" + i.getSerialno() + "\"" + ",");
            out.print("\"" + m.getModel() + "\"" + ",");
            String description = translationService.getCorrectTranslation(m.getDescription().getTranslations(),
                    LocaleContextHolder.getLocale());
            out.print("\"" + description + "\"" + ",");
            out.print("\"" + mfr.getName() + "\"" + ",");
            out.print("\"" + i.getComp().getConame() + "\"" + ",");
            out.print("\"" + a.getAddr1() + "\"" + ",");
            out.print("\"" + a.getAddr2() + "\"" + ",");
            out.print("\"" + a.getAddr3() + "\"" + ",");
            out.print("\"" + a.getTown() + "\"" + ",");
            out.print("\"" + a.getPostcode() + "\"" + ",");
            out.print("\"" + j.getCon().getName() + "\"" + ",");
            out.print("\"" + j.getDirectory().getAbsolutePath().concat(File.separator) + "\"" + ",");
            out.print("\"" + ((c != null) ? c.getCertno() : "") + "\"" + ",");
            out.print("\"" + ((po != null) ? po.getPoNumber() : "") + "\"" + ",");
            out.print("\"" + (DateTools.df.format(ji.getDateIn())) + "\"");
            success = true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null)
                    out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return success;
    }

    @Override
    public Optional<JobItem> findActiveJobItemFromBarcodeOrFormerBarcode(String barcode) {
        return jobItemDao.findActiveJobItemFromBarcodeOrFormerBarcode(barcode);
    }

    @Override
    public JobItem findActiveJobItemFromBarcode(int plantid) {
        return this.jobItemDao.findActiveJobItemFromBarcode(plantid);
    }

    @Override
    public JobItem findEagerJobItem(int jobItemId) {
        return this.jobItemDao.findEagerJobItem(jobItemId);
    }

    @Override
    public JobItem findEagerPopulatedJobitem(int jobitemid) {
        return this.jobItemDao.findEagerPopulatedJobitem(jobitemid);
    }

    @Override
    public JobItem findJobItem(int id) {
        return this.jobItemDao.find(id);
    }

    @Override
    public JobItem findJobItem(String jobItemNumber) {
        String[] numbers = jobItemNumber.split("\\.");
        if (numbers.length == 2) {
            return this.jobItemDao.getJobItemByItemCode(numbers[0], Integer.valueOf(numbers[1]));
        } else
            return null;
    }

    @Override
    public List<JobItemInvoiceDto> findAllCompleteJobItemsWithoutInvoicesProjection(Integer allocatedCompanyId,
                                                                                    Integer allocatedSubdivId, Company company, JobSortKey sortKey, String completeJob) {
        return this.jobItemDao.findAllCompleteJobItemsWithoutInvoicesProjection(allocatedCompanyId, allocatedSubdivId,
                company, sortKey, completeJob);
    }

    @Override
    public List<JobItemInvoiceDto> findAllCompleteJobItemsWithOnlyProformaInvoicesProjection(Integer allocatedCompanyId,
                                                                                             Integer allocatedSubdivId, Company company, JobSortKey sortKey, String completeJob) {
        return this.jobItemDao.findAllCompleteJobItemsWithOnlyProformaInvoicesProjection(allocatedCompanyId,
                allocatedSubdivId, company, sortKey, completeJob);
    }

    /**
     * Only used by LocationChangerPlugIn.js via DWR
     */
    @Override
    public JobItem findEagerJobItemFromBarcode(int plantid) {
        List<JobItem> items = this.getActiveJobItemsFromBarcode(plantid);
        if (items.size() > 0) {
            JobItem item = items.get(0);
            /*
             * The properties have to be explicitly loaded here to be passed
             * back through Ajax to web tier!
             */
            if (item != null) {
                if (item.getCurrentAddr() != null) {
                    item.getCurrentAddr().setAddr1(item.getCurrentAddr().getAddr1());
                    item.getCurrentAddr().setTown(item.getCurrentAddr().getTown());
                }
                if (item.getCurrentLoc() != null) {
                    item.getCurrentLoc().setLocation(item.getCurrentLoc().getLocation());
                }
            }
            return item;
        } else
            return null;
    }

    @Override
    public JobItem findMostRecentJobItem(int plantid) {
        return this.jobItemDao.findMostRecentJobItem(plantid);
    }

    @Override
    public SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>> getActiveJobItemsForDeptReadyForWork(int deptId) {
        log.debug("deptId: " + deptId);
        Department dept = departmentService.get(deptId);
        List<Integer> procIds = workAssignmentServ.getCapabilitiesIdsAssignedToDepartment(dept);

        // EnumSet<DepartmentType> deptTypes =
        // EnumSet.of(DepartmentType.LABORATORY, DepartmentType.ONSITE);
        // List<Integer> stateIds =
        // itemStateServ.getItemStateIdsForDepartmentTypes(deptTypes);
        // Include just states that are in advance of or during the service
        // process
        EnumSet<StateGroup> stateGroups = EnumSet.of(StateGroup.CALIBRATION, StateGroup.AWAITINGCALIBRATION,
                StateGroup.RESUMECALIBRATION, StateGroup.AWAITINGGENERALSERVICEOPERATION,
                StateGroup.GENERALSERVICEOPERATION, StateGroup.RESUMEGENERALSERVICEOPERATION,
                StateGroup.AWAITING_REPAIR_WORK_BY_TECHNICIAN);
        List<Integer> stateIds = itemStateServ.getItemStateIdsForStateGroups(stateGroups);

        log.debug(stateIds.size() + " labStateIds: " + stateIds);

        // Note, intentionally includes both onsite and laboratory items -
        // allocation is
        // used for both
        List<SelectedJobItemDTO> selectedJobItems = this.jobItemDao.getActiveJobItems(procIds, stateIds, false,
                dept.getSubdiv(), null, null);
        Set<Integer> clientCompanyIds = selectedJobItems.stream().map(SelectedJobItemDTO::getClientCompanyId)
                .collect(Collectors.toSet());
        List<CompanySettingsForAllocatedCompany> companySettings = companySettingsService.getAll(clientCompanyIds,
                dept.getSubdiv().getComp());
        SortedMap<Integer, CompanySettingsForAllocatedCompany> companySettingsMap = new TreeMap<>();
        companySettings.forEach(cs -> companySettingsMap.put(cs.getCompany().getCoid(), cs));
        selectedJobItems.forEach(ji -> {
            if (companySettingsMap.containsKey(ji.getClientCompanyId())) {
                CompanySettingsForAllocatedCompany settings = companySettingsMap.get(ji.getClientCompanyId());
                ji.setClientCompanyActive(settings.isActive());
                ji.setClientCompanyOnStop(settings.isOnStop());
            } else {
                ji.setClientCompanyActive(false);
                ji.setClientCompanyOnStop(true);
            }
        });
        Map<Integer, List<SelectedJobItemDTO>> groupedJobItems = selectedJobItems.stream()
                .collect(Collectors.groupingBy(SelectedJobItemDTO::getJobId));
        SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>> resultMap = new TreeMap<>();
        groupedJobItems
                .forEach(
                        (jobId, jobItems) -> resultMap.put(
                                new SelectedJobDTO(jobId, jobItems.get(0).getJobNo(), jobItems.get(0).getJobStatus(),
                                        jobItems.get(0).getJobRegDate(),
                                        jobItems.stream()
                                                .map(SelectedJobItemDTO::getDueDate).min(LocalDate::compareTo)
                                                .orElse(null),
                                        jobItems.stream().map(SelectedJobItemDTO::getTurn).min(Integer::compareTo)
                                                .orElse(-1) <= fastTrackTurn,
                                        jobItems.get(0).getJobType(), jobItems.get(0).getClientContactId(),
                                        jobItems.get(0).getClientContactFirstName() + " "
                                                + jobItems.get(0).getClientContactLastName(),
                                        jobItems.get(0).getClientContactActive()
                                                && jobItems.get(0).getClientSubdivActive()
                                                && jobItems.get(0).getClientCompanyActive(),
                                        jobItems.get(0).getClientCompanyId(), jobItems.get(0).getClientCompanyName(),
                                        jobItems.get(0).getClientSubdivId(), jobItems.get(0).getClientSubdivName(),
                                        jobItems.get(0).getClientCompanyActive(),
                                        jobItems.get(0).getClientCompanyOnStop(), jobItems.get(0).getTransportOutId(),
                                        jobItems.get(0).getTransportOutName() == null ? ""
                                                : jobItems.get(0).getTransportOutName()
                                                + (jobItems.get(0).getTransportOutSubdiv() == null ? ""
                                                : " - " + jobItems.get(0).getTransportOutSubdiv()),
                                        jobItems.get(0).getAllocatedSubdivId(),
                                        jobItems.get(0).getAllocatedSubdivCode()),
                                jobItems));
        return resultMap;
    }

    /*
     * Only used internally for location changes (eager fetching)
     */
    private List<JobItem> getActiveJobItemsFromBarcode(int plantid) {
        Instrument inst = this.instrumServ.findEagerModelInstrum(plantid);
        List<JobItem> items = new ArrayList<>();

        if (inst != null) {
            for (JobItem ji : inst.getJobItems()) {
                if (ji.getState().getActive()) {
                    items.add(ji);
                }
            }
        }

        return items;
    }

    @Override
    public List<JobItem> getAllItems(List<Integer> ids) {
        return this.jobItemDao.getAllItems(ids);
    }

    @Override
    public List<JobItem> getAllByState(ItemState state, Subdiv subdiv) {
        return jobItemDao.getAllByState(state, subdiv);
    }

    @Override
    public List<JobItem> getAllJobItems() {
        return this.jobItemDao.findAll();
    }

    @Override
    public List<JobItemOnClientPurchaseOrderDTO> getJobItemsOnClientPO(Integer poId, Integer jobId) {
        return jobItemDao.getJobItemsOnClientPO(poId, jobId, LocaleContextHolder.getLocale());
    }

    @Override
    public List<JobItemFeedbackDto> getAllJobItemsByJobId(Integer jobId) {
        Locale locale = LocaleContextHolder.getLocale();
        val fallbackLocale = supportedLocaleService.getPrimaryLocale();
        return jobItemDao.getAllJobItems(jobId, locale, fallbackLocale);
    }

    @Override
    public List<JobItem> getAvailableTaskList(Contact contact) {
        return this.jobItemDao.getAvailableTaskList(contact);
    }

    @Override
    public List<JobItem> getContactItemsOnJobInStateGroup(int personId, StateGroup group) {
        return this.jobItemDao.getContactItemsOnJobInStateGroup(personId, group);
    }

    @Override
    public Integer getCountActiveJobItemsWithProcs(List<Integer> procIds, Integer days, Boolean includeBusiness,
                                                   boolean labGroupsOnly, Subdiv subdiv) {
        List<StateGroup> stateGroups = null;
        if (labGroupsOnly) {
            stateGroups = workAssignmentServ.getLabStateGroups();
        }

        return this.jobItemDao.getCountActiveJobItemsWithProcs(procIds, days, includeBusiness, stateGroups, subdiv);
    }

    @Override
    public Long getCountAllJobItemsForState(ItemState state, int subdivId, StateGroupLinkType groupType) {
        Subdiv subdiv = subdivService.get(subdivId);
        return this.jobItemDao.getCountAllJobItemsForState(state, subdiv, groupType);
    }

    @Override
    public Integer getCountJobItemsByDate(String days, int subdivId) {
        return this.jobItemDao.getCountJobItemsByDays(days, subdivId);
    }

    @Override
    public Long getCountJobItems(int jobid) {
        return this.jobItemDao.getCountJobItems(jobid);
    }

    @Override
    public ActionOutcome getDefaultConRevOutcomeForItem(JobItem ji) {
        ActionOutcome ao;
        ItemActivity activity;
        if (ji.getJob().getType().equals(JobType.SITE)) {
            activity = this.itemStateServ.findItemActivityByName(ONSITE_CONTRACT_REVIEW);
        } else {
            activity = this.itemStateServ.findItemActivityByName(CONTRACT_REVIEW);
        }
        ao = this.aoServ.findDefaultActionOutcomeForActivity(activity.getStateid());
        return ao;
    }

    @Override
    public DetailedJobItem getDetailedJobItem(int jobitemid) {
        DetailedJobItem dji = new DetailedJobItem();
        JobItem jobItem = this.findEagerPopulatedJobitem(jobitemid);
        dji.setJobitem(jobItem);
        // job item has image?
        if (dji.getJobitem() != null) {
            if ((dji.getJobitem().getImages() == null) || (dji.getJobitem().getImages().size() == 0)) {
                dji.setIpdto(this.imageService.getNextApplicableImageForJI(jobitemid));
            }
        }

        dji.setCalTypeLongName(translationService.getCorrectTranslation(
                jobItem.getServiceType().getLongnameTranslation(), LocaleContextHolder.getLocale()));

        dji.setCalTypeShortName(translationService.getCorrectTranslation(
                jobItem.getServiceType().getShortnameTranslation(), LocaleContextHolder.getLocale()));

        dji.setFullModelName(InstModelTools.modelNameViaTranslations(jobItem.getInst().getModel(),
                LocaleContextHolder.getLocale(), supportedLocaleService.getPrimaryLocale()));

        return dji;
    }

    @Override
    public List<ItemsAtTPWrapper> getItemsAtThirdParty(Subdiv subdiv) {
        return this.jobItemDao.getItemsAtThirdParty(subdiv);
    }

    @Override
    public List<JobItem> getItemsOnJobInStateGroup(int jobId, StateGroup group) {
        return this.jobItemDao.getItemsOnJobInStateGroup(jobId, group);
    }

    @Override
    public Map<Contact, List<JobItem>> getItemsRequiringClientChase() {
        List<JobItem> jis = this.jobItemDao.getItemsRequiringChase(StateGroup.AWAITINGDECISION);
        Map<Contact, List<JobItem>> map = new HashMap<>();
        if (jis.size() > 0) {
            JobItem ji = jis.get(0);
            Contact con = ji.getJob().getCon();
            List<JobItem> itemsForContact = new ArrayList<>();
            itemsForContact.add(ji);
            for (int i = 1; i < jis.size(); i++) {
                JobItem item = jis.get(i);
                if (item.getJob().getCon().getPersonid() != con.getPersonid()) {
                    map.put(con, itemsForContact);
                    con = item.getJob().getCon();
                    itemsForContact = new ArrayList<>();
                }
                itemsForContact.add(item);
            }
            map.put(con, itemsForContact);
        }
        return map;
    }

    @Override
    public Map<Contact, List<JobItem>> getItemsRequiringSupplierChase() {
        List<JobItem> jis = this.jobItemDao.getItemsRequiringChase(StateGroup.AWAITINGSUPPLIER);
        Map<Contact, List<JobItem>> map = new HashMap<>();
        for (JobItem item : jis) {
            boolean chaseThisItem = true;
            // can only continue if a purchase order exists so we know who to
            // chase!
            if ((item.getPurchaseOrderItems() != null) && (item.getPurchaseOrderItems().size() > 0)) {
                TreeSet<PurchaseOrderJobItem> pos = new TreeSet<>(new PurchaseOrderJobItemComparator());
                pos.addAll(item.getPurchaseOrderItems());
                PurchaseOrderItem lastPoItem = pos.last().getPoitem();
                if (item.getCurrentAddr() == null || lastPoItem.getDeliveryDate() == null
                        || lastPoItem.getOrder().getIssuedate() == null
                        || AutoServiceCalculationTool.getSupplierDeliveryNoteItem(item, true) == null) {
                    chaseThisItem = false;
                }
                // if item is at 'unit at third party for work' status only
                // chase items that are expected back by now!
                if (chaseThisItem
                        && item.getState().getDescription().toLowerCase().contains("unit at third party for work")) {
                    // set not to chase until we're completely satisfied it
                    // should
                    // be chased
                    chaseThisItem = false;
                    int lastPurOrdCompId = lastPoItem.getOrder().getContact().getSub().getComp().getCoid();
                    int currentCompId = item.getCurrentAddr().getSub().getComp().getCoid();
                    // if the item is at the company the last PO was issued to
                    // and if the item is now still there after the expected
                    // return date
                    if (currentCompId == lastPurOrdCompId && LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())
                            .isAfter(lastPoItem.getDeliveryDate()))
                        // chase it!
                        chaseThisItem = true;
                }
                // only if this item should be chased
                if (chaseThisItem) {
                    Contact poOwner = lastPoItem.getOrder().getContact();
                    if (!map.containsKey(poOwner)) {
                        map.put(poOwner, new ArrayList<>());
                    }
                    List<JobItem> items = map.get(poOwner);
                    items.add(item);
                    map.put(poOwner, items);
                }
            }
        }
        return map;
    }

    @Override
    public List<JobItemProjectionDTO> getJobItemProjectionDTOs(Collection<Integer> jobIds,
                                                               Collection<Integer> jobItemIds) {
        List<JobItemProjectionDTO> result = Collections.emptyList();
        if ((jobIds != null && !jobIds.isEmpty()) || (jobItemIds != null) && !jobItemIds.isEmpty()) {
            result = this.jobItemDao.getJobItemProjectionDTOs(jobIds, jobItemIds);
        }
        return result;
    }

    @Override
    public List<JobItem> getJobItemsByIds(List<Integer> jobitemids) {
        return (jobitemids == null) || (jobitemids.size() < 1) ? new ArrayList<>()
                : this.jobItemDao.getJobItemsByIds(jobitemids);
    }

    @Override
    public List<JobItem> getJobItemsByStatusAndProc(int jobid, int statusid, int procid, Integer excludejiid) {
        return this.jobItemDao.getJobItemsByStatusAndProc(jobid, statusid, procid, excludejiid);
    }

    @Override
    public List<JobItemProjectionDTO> getJobItemsFromJobs(List<String> jobnos, Locale locale) {
        List<JobItemProjectionDTO> result;
        if (jobnos.isEmpty())
            result = Collections.emptyList();
        else
            result = this.jobItemDao.getJobItemsFromJobs(jobnos, locale);
        return result;
    }

    @Override
    public Set<JobItem> getJobItemsWithNoCostings(int job) {
        TreeSet<JobItem> items = new TreeSet<>(new JobItemComparator());
        Job j = this.jobServ.get(job);

        // this isn't the neatest way to do this - we could probably write a
        // nicer db query to handle this
        if ((j != null) && (j.getItems().size() > 0)) {
            for (JobItem ji : j.getItems()) {
                if (ji.getJobCostingItems().size() < 1) {
                    items.add(ji);
                }
            }
        }

        return items;
    }

    @Override
    public JobItem getMostRecentJobItemForInstrument(Instrument instrument) {
        return this.jobItemDao.getMostRecentJobItemForInstrument(instrument);
    }

    // Used internally only
    protected int getNextItemNo(Job job) {
        int highest = 0;

        if (job.getItems() != null && job.getItems().size() > 0) {
            for (JobItem ji : job.getItems()) {
                if (ji.getItemNo() > highest) {
                    highest = ji.getItemNo();
                }
            }
        }

        return (highest + 1);
    }

    @Override
    public JobItemPlantillasDTO getPlantillasJobItem(int jobitemId, int allocatedAddressId) {
        return this.jobItemDao.getPlantillasJobItem(jobitemId, allocatedAddressId);
    }

    @Override
    public PagedResultSet<JobItemPlantillasDTO> getPlantillasJobItems(int allocatedCompanyId, int allocatedAddressId,
                                                                      Date lastModified, Integer clientAddressId, int businessSubdivId, int resultsPerPage, int currentPage) {
        return this.jobItemDao.getPlantillasJobItems(allocatedCompanyId, allocatedAddressId, lastModified,
                clientAddressId, businessSubdivId, resultsPerPage, currentPage);
    }

    @Override
    public JobItemTransit getPreviousTransitActionForItem(JobItem ji) {
        TreeSet<JobItemAction> actions = new TreeSet<>(new JobItemActionComparator());
        actions.addAll(ji.getActions());

        // loop backwards through actions
        for (JobItemAction action : actions.descendingSet()) {
            // find where action is a transit
            if (action instanceof JobItemTransit) {
                // return the transit
                return (JobItemTransit) action;
            }
        }

        // cannot find the last transit
        return null;
    }

    @Override
    public List<JobItem> getRemainingItemsOnJobInStateGroup(BatchCalibration batchCal, StateGroup group) {
        int jobId = batchCal.getJob().getJobid();
        List<JobItem> itemsInStateGroup = this.getItemsOnJobInStateGroup(jobId, group);
        for (Calibration cal : batchCal.getCals())
            for (CalLink cl : cal.getLinks())
                itemsInStateGroup.remove(cl.getJi());
        return itemsInStateGroup;
    }

    @Override
    public List<JobItem> getRemainingJobItemsForDelivery(JobDelivery delivery) {
        Set<JobItem> itemsInDelivery = new HashSet<>();
        for (JobDeliveryItem jdi : delivery.getItems()) {
            itemsInDelivery.add(jdi.getJobitem());
        }
        Job job = delivery.getJob();
        Set<JobItem> itemsInJob = job.getItems();
        Iterator<JobItem> jobItemIterator = itemsInJob.iterator();
        List<JobItem> remainingJobItems = new ArrayList<>();
        while (jobItemIterator.hasNext()) {
            JobItem tempItem = jobItemIterator.next();
            if (!itemsInDelivery.contains(tempItem))
                remainingJobItems.add(tempItem);
        }
        return remainingJobItems;
    }

    // Internal use
    protected boolean hasStateGroupOfKeyName(ItemState state, StateGroup group) {
        return this.stateHasGroupOfKeyName(state, group);
    }

    @Override
    public void insertJobItem(JobItem ji, Contact currentContact, Double calDiscount, String bookInComment) {

        // set the default costs for the instrument (system default lookup
        // externalized, once per job)
        this.setDefaultCosts(ji, new BigDecimal(calDiscount));
        // calculate calibration location and address
        this.lookUpCalAddress(ji);
        // calculate next transit date
        // ji.setTransitDate(this.calculateNextTransitDate(ji));
        // calculate dueDate
        if (ji.getJob().getAgreedDelDate() != null)
            ji.setDueDate(ji.getJob().getAgreedDelDate());
        else
            ji.calculateAndSetDueDate();

        ItemActivity state = this.interceptor_bookInItem.getDefaultActivity();
        ji.setState(state);

        // insert the jobitem
        this.jobItemDao.persist(ji);
        modifyJobStatus(ji.getJob());
        // create work requirement (must be done after job item insertion!)
        // 	2021-05-20 EF: not sure why, because we can also cascade it, but definitely not, if there already exists at least one
        if (ji.getWorkRequirements().isEmpty())
            this.addNewWorkRequirement(ji);
        // Set state and add jobitemaction (job item now exist for workflow
        // lookups)
        HookInterceptor_BookInItem.Input input = new HookInterceptor_BookInItem.Input(ji, bookInComment,
                dateFromZonedDateTime(ji.getDateIn()), null, currentContact);
        this.interceptor_bookInItem.recordAction(input);

        // If job item is being performed for ourselves (same business
        // subdivision), exclude from invoicing as internal job
        if (ji.getJob().getCon().getSub().getSubdivid() == ji.getJob().getOrganisation().getSubdivid()) {
            this.jobItemNotInvoicedService.recordNotInvoiced(ji, NotInvoicedReason.INTERNAL_JOB, "", 0, currentContact);
        }
        // Otherwise if job item is being performed for ourselves (same business
        // company), exclude from invoicing as inter-subdivision job
        else if (ji.getJob().getCon().getSub().getComp().getCoid() == ji.getJob().getOrganisation().getComp()
                .getCoid()) {
            this.jobItemNotInvoicedService.recordNotInvoiced(ji, NotInvoicedReason.INTER_SUBDIVISION, "", 0,
                    currentContact);
        }
        // mark the instrument as received since it's last recall
        Instrument instrument = ji.getInst();
        instrument.setRecalledNotReceived(false);
    }

    @Override
    public List<Integer> insertQuickItems(Job job, List<AddQuickItemsRowDTO> rows, Address bookedInAt,
                                          Contact contact) {
        // look for discount defaults for this contact/sub/company or system
        // default
        Double calDiscount = discount.parseValue(
                discount.getValueHierarchical(Scope.CONTACT, job.getCon(), job.getOrganisation().getComp()).getValue());
        SubdivSettingsForAllocatedSubdiv subdivSettings = subdivSettingsService.get(job.getCon().getSub(),
                job.getOrganisation());
        int nextItemNo = this.getNextItemNo(job);
        List<Integer> jobItemsIds = new ArrayList<>();
        for (AddQuickItemsRowDTO row : rows) {

            if (row.getProcId() == null) {
                row.setProcId(0);
            }

            if (row.getWorkIstructionId() == null) {
                row.setWorkIstructionId(0);
            }

            Capability proc = this.procServ.get(row.getProcId());
            WorkInstruction wi = this.wiServ.get(row.getWorkIstructionId());

            // Eager fetch no longer necessary after DWR -> Json controller migration
            Instrument inst = this.instrumServ.get(row.getBarecode());

            ServiceType serviceType = this.serviceTypeService.get(row.getServiceTypeId());

            // update default service type on instrument
            inst.setDefaultServiceType(serviceType);
            JobItem ji = new JobItem();

            ji.setJob(job);
            ji.setInst(inst);
            ji.setTurn(row.getTurn());
            ji.setServiceType(serviceType);
            ji.setItemNo(nextItemNo);
            ji.setReqCleaning(row.isClean());
            ji.setCapability(proc);
            ji.setWorkInstruction(wi);

            // add a default contract review outcome
            ji.setConRevOutcome(this.getDefaultConRevOutcomeForItem(ji));

            ji.setBookedInAddr(bookedInAt);
            ji.setBookedInLoc(null);
            ji.setCurrentAddr(bookedInAt);
            ji.setCurrentLoc(null);
            ji.setReturnOption(job.getReturnOption());
            ji.setInOption(job.getInOption());

            if (inst.getDefaultClientRef() != null)
                ji.setClientRef(inst.getDefaultClientRef());

            if (this.getDateIn(job) != null)
                ji.setDateIn(this.getDateIn(job));
            else
                ji.setDateIn(ZonedDateTime.now(LocaleContextHolder.getTimeZone().toZoneId()));

            if (job.getAgreedDelDate() != null)
                ji.setDueDate(job.getAgreedDelDate());
            else
                ji.calculateAndSetDueDate();

            if (subdivSettings != null)
                ji.setLastContractReviewBy(subdivSettings.getDefaultBusinessContact());

            // if the job has a default PO or BPO, add the item to it
            if (ji.getItemPOs() == null) {
                ji.setItemPOs(new HashSet<>());
            }
            if (ji.getContract() != null && ji.getContract().getBpo() != null) {
                JobItemPO jipo = new JobItemPO();
                jipo.setItem(ji);
                jipo.setBpo(ji.getContract().getBpo());
                ji.getItemPOs().add(jipo);

            } else if (job.getBpo() != null) {
                JobItemPO jipo = new JobItemPO();
                jipo.setItem(ji);
                jipo.setBpo(job.getBpo());
                ji.getItemPOs().add(jipo);
            } else if (job.getDefaultPO() != null) {
                JobItemPO jipo = new JobItemPO();
                jipo.setItem(ji);
                jipo.setPo(job.getDefaultPO());
                ji.getItemPOs().add(jipo);
            }
            this.insertJobItem(ji, contact, calDiscount, null);
            jobItemsIds.add(ji.getJobItemId());
            nextItemNo++;
        }
        modifyJobStatus(job);
        return jobItemsIds;
    }

    @Override
    public boolean isInTransit(int jobItemId) {
        JobItem ji = this.findJobItem(jobItemId);

        if (ji.getState() != null) {
            return ji.getState().getDescription().trim().equals("In Transit");
        }

        return false;
    }

    @Override
    public boolean isInCalibration(JobItem ji) {
        return this.hasStateGroupOfKeyName(ji.getState(), StateGroup.CALIBRATION);
    }

    @Override
    public boolean isReadyForCalibration(JobItem ji) {
        return this.hasStateGroupOfKeyName(ji.getState(), StateGroup.AWAITINGCALIBRATION);
    }

    @Override
    public boolean isReadyForClientDelivery(JobItem ji, Integer businessSubdivId) {
        boolean result = false;
        if (ji.getCurrentAddr() != null && businessSubdivId.equals(ji.getCurrentAddr().getSub().getSubdivid())) {
            result = this.hasStateGroupOfKeyName(ji.getState(), StateGroup.CLIENTDELIVERY);
        }
        return result;
    }

    @Override
    public boolean isReadyForInternalDelivery(JobItem ji, Integer businessSubdivId) {
        boolean result = false;
        if (ji.getCurrentAddr() != null && businessSubdivId.equals(ji.getCurrentAddr().getSub().getSubdivid())) {
            result = this.hasStateGroupOfKeyName(ji.getState(), StateGroup.INTERNALDELIVERY);
        }
        return result;
    }

    @Override
    public boolean isReadyForInternalReturnDelivery(JobItem ji, Integer businessSubdivId) {
        boolean result = false;
        if (ji.getCurrentAddr() != null && businessSubdivId.equals(ji.getCurrentAddr().getSub().getSubdivid())) {
            result = this.hasStateGroupOfKeyName(ji.getState(), StateGroup.INTERNAL_RETURN_DELIVERY);
        }
        return result;
    }

    @Override
    public boolean isAwaitingInternalPO(JobItem ji) {
        return this.hasStateGroupOfKeyName(ji.getState(), StateGroup.AWAITINGINTERNALPO);
    }

    @Override
    public boolean isReadyForTPDelivery(JobItem ji, Integer businessSubdivId) {
        if (ji.getCurrentAddr() != null && businessSubdivId.equals(ji.getCurrentAddr().getSub().getSubdivid())) {
            return this.hasStateGroupOfKeyName(ji.getState(), StateGroup.TPDELIVERY);
        }
        return false;
    }

    @Override
    public boolean isReadyForTPRequirement(ItemState state) {
        return this.hasStateGroupOfKeyName(state, StateGroup.TPREQUIREMENT);
    }

    @Override
    public boolean isReadyToResumeCalibration(JobItem ji) {
        return this.hasStateGroupOfKeyName(ji.getState(), StateGroup.RESUMECALIBRATION);
    }

    @Override
    public boolean isReadyForGeneralServiceOperation(JobItem ji) {
        return this.hasStateGroupOfKeyName(ji.getState(), StateGroup.AWAITINGGENERALSERVICEOPERATION);
    }

    @Override
    public boolean isReadyToResumeGeneralServiceOperation(JobItem ji) {
        return this.hasStateGroupOfKeyName(ji.getState(), StateGroup.RESUMEGENERALSERVICEOPERATION);
    }

    @Override
    public boolean isReadyForGeneralServiceOperationDocumentUpload(JobItem ji) {
        return this.hasStateGroupOfKeyName(ji.getState(), StateGroup.AWAITING_GSO_DOCUMENT_UPLOAD);
    }

    @Override
    public boolean itemCanBeDespatched(JobItem ji) {
        return this.hasStateGroupOfKeyName(ji.getState(), StateGroup.DESPATCH);
    }

    @Override
    public boolean itemCanBeReceived(JobItem ji) {
        return this.hasStateGroupOfKeyName(ji.getState(), StateGroup.RECEIVE);
    }

    @Override
    public boolean itemHasBeenCalibrated(JobItem ji) {
        if ((ji != null) && (ji.getActions() != null) && (ji.getActions().size() > 0)) {
            // loop through actions to see if there is a calibration at any
            // point
            for (JobItemAction action : ji.getActions()) {
                if (action instanceof JobItemActivity) {
                    JobItemActivity jia = (JobItemActivity) action;

                    for (StateGroupLink sgl : jia.getActivity().getGroupLinks()) {
                        if (sgl.getGroup().equals(StateGroup.CALIBRATION) && !jia.isDeleted()) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    @Override
    public void linkOnBehalf(JobItem jobItem, Contact setBy, Company onBehalfCompany, Address onBehalfAddress) {
        OnBehalfItem item = new OnBehalfItem();
        item.setJobitem(jobItem);
        item.setSetBy(setBy);
        item.setSetOn(new Date());
        item.setAddress(onBehalfAddress);
        item.setCompany(onBehalfCompany);
        jobItem.setOnBehalf(item);
    }

    /*
     * This method looks up (and sets) the next calAddress on the JobItem, which
     * is the next address that the instrument is to be calibrated at. The
     * following logic is used:
     *
     * 1) For onsite jobs, the address is the customer address of the instrument
     *
     * 2) If the JobItem has BOTH a Procedure proc set and an Address
     * currentAddr set then the ProcedureCategory is used to determine the next
     * address. a) preference for same address first if available b) if not,
     * then any other address
     *
     * 3) Otherwise the current address of the job item is used.
     */
    @Override
    public void lookUpCalAddress(JobItem ji) {
        Address calAddr = null;

        if (ji.getJob().getType().equals(JobType.SITE)) {
            calAddr = ji.getInst().getAdd();
        }
        // check procedure on job item (set when next work requirement is
        // selected)
        else if (ji.getCapability() != null) {
            List<CategorisedCapability> catProcs = ji.getCapability().getCategories();
            if (catProcs.size() > 0) {
                for (CategorisedCapability cp : catProcs) {
                    Address addr = cp.getCategory().getDepartment().getAddress();

                    // if the cal address hasn't yet been set
                    // and this dept is at the current address
                    if ((calAddr == null) && (ji.getCurrentAddr() != null)
                            && (ji.getCurrentAddr().getAddrid().equals(addr.getAddrid()))) {
                        calAddr = addr;
                    }
                }

            }
            // if there wasn't a match by procedure category, use default
            // procedure address
            if (calAddr == null) {
                calAddr = ji.getCapability().getDefAddress();
            }
        }
        // CURRENT ADDRESS OR PROC IS NULL
        else {
            // set the cal address to current address until proc is set
            calAddr = ji.getCurrentAddr();
        }

        ji.setCalAddr(calAddr);
    }

    @Override
    public JobItem mergeJobItem(JobItem ji) {
        return this.jobItemDao.merge(ji);
    }

    @Override
    public void moveItemToBottomOfJob(JobItem ji) {
        // add the items to a more traverse-friendly collection
        List<JobItem> itemList = new ArrayList<>(ji.getJob().getItems());
        itemList.sort(new JobItemComparator());

        // if the item is not already the last
        if (itemList.get(itemList.size() - 1).getJobItemId() != ji.getJobItemId()) {
            // last number (may not always be the size!)
            int lastNo = itemList.get(itemList.size() - 1).getItemNo();

            // get position in the list currently
            int currentNo = ji.getItemNo();

            // set the no initially to 0 so that other items can be moved into
            // empty no slots without breaking the unique constraint
            ji.setItemNo(0);
            // flush the session after each change of number. this is the only
            // way
            // to prevent a unique constraint from popping
            this.updateJobItem(ji);

            for (int i = currentNo + 1; i <= itemList.size(); i++) {
                JobItem temp = itemList.get(i - 1);
                temp.setItemNo(i - 1);
                this.updateJobItem(ji);
            }

            // set the no of the job item to the last item no
            ji.setItemNo(lastNo);
            this.updateJobItem(ji);
        }
    }

    @Override
    public void overrideStatusOfItems(int statusId, List<Integer> jobItemIds, String remark) {
        ItemStatus itemStatus = this.itemStateServ.findItemStatus(statusId);

        for (int jobItemId : jobItemIds) {
            JobItem ji = this.findJobItem(jobItemId);

            log.info("Overriding status of job item with ID " + jobItemId + " : From '" + ji.getState().getDescription()
                    + "' to '" + itemStatus.getDescription() + "'");

            // the status is actually set in the hook when the override activity
            // is added
            ji = this.jobItemDao.merge(ji);
            HookInterceptor_OverrideStatus.Input input = new HookInterceptor_OverrideStatus.Input(ji, itemStatus,
                    remark);
            this.interceptor_overrideStatus.recordAction(input);
        }
    }

    @Override
    public void placeItemsOnHold(int statusId, List<Integer> jobItemIds, String remark) {
        // Will be a holdstatus, using ItemStatus as proxy narrowing disallows
        // cast to HoldStatus
        ItemStatus holdStatus = this.itemStateServ.findItemStatus(statusId);
        for (int jobItemId : jobItemIds) {
            JobItem ji = this.findJobItem(jobItemId);
            // the status is actually set in the hook when the on hold activity
            // is added
            ji = this.jobItemDao.merge(ji);
            HookInterceptor_PlaceOnHold.Input input = new HookInterceptor_PlaceOnHold.Input(ji, holdStatus, remark);
            this.interceptor_placeOnHold.recordAction(input);
        }
    }

    @Override
    public ResultWrapper receiveItem(int barcode, int addrId, Integer locId) {
        JobItem ji = this.findMostRecentActiveJobItem(barcode);
        if (ji == null) {
            String message = this.messageSource.getMessage(ERROR_CODE_JOBITEM_NOT_FOUND_FOR_BARCODE, null,
                    "Job item not found for instrument barcode", LocaleContextHolder.getLocale());
            return new ResultWrapper(false, message);
        }

        if (!this.itemCanBeReceived(ji)) {
            String message = this.messageSource.getMessage(ERROR_CODE_JOBITEM_NOT_TRANSIT, null,
                    "This job item is not currently in transit", LocaleContextHolder.getLocale());
            return new ResultWrapper(false, message);
        }

        // if item is in a group that is to be delivered together AND the item
        // has modules
        if ((ji.getGroup() != null) && ji.getGroup().isDeliveryGroup() && (ji.getInst().getModules() != null)
                && (ji.getInst().getModules().size() > 0)) {
            // for each of the items in the group
            for (JobItem groupItem : ji.getGroup().getItems()) {
                // if this isn't the unit that was scanned
                if (groupItem.getJobItemId() != ji.getJobItemId()) {
                    // check if the item is one of the base unit's modules
                    for (Instrument module : ji.getInst().getModules()) {
                        // if the item matches a module
                        if (groupItem.getInst().getPlantid() == module.getPlantid()) {
                            // check that the item is awaiting receipt
                            if (this.itemCanBeReceived(groupItem)) {
                                // receive the module
                                this.receiveItem(groupItem.getInst().getPlantid(), addrId, locId);
                            }
                        }
                    }
                }
            }
        }

        ji.setCurrentAddr(this.addrServ.get(addrId));
        Location loc = (locId != null) ? this.locServ.get(locId) : null;
        ji.setCurrentLoc(loc);
        ji = this.jobItemDao.merge(ji);
        this.interceptor_receiveItem.recordAction(ji);
        return new ResultWrapper(true, null, null, null);
    }

    @Override
    public void receivePaymentForItems(List<Integer> jobItemIds) {
        List<JobItem> items = this.getAllItems(jobItemIds);
        for (JobItem ji : items) {
            ji.setPaymentReceived(true);
            ji = this.jobItemDao.merge(ji);
            this.interceptor_receivePayment.recordAction(ji);
        }
    }

    /**
     * Creates a new Contract Review Cost instance for each active
     * {@link CostType} on the system and assigns it to the {@link JobItem}
     * currently being inserted. This configuration automatically sets sales,
     * repair and adjustment costs to false for contract review.
     *
     * @param ji the {@link JobItem}.
     */
    // Used internally
    @Override
    public void setDefaultCosts(JobItem ji, BigDecimal calibrationDiscount) {
        // NEW price lookup only
        // TODO refactor job item creation to externalize this lookup
        // - so lookup only called once per transaction (multiple job items)
        // Note, the NoInvoices system default is unused (no true values) so
        // not implemented
        Integer businessCompanyId = ji.getJob().getOrganisation().getComp().getCoid();
        Integer clientCompanyId = ji.getJob().getCon().getSub().getComp().getCoid();
        List<Integer> includeQuotationIds = ji.getJob().getLinkedQuotes().stream().map(q -> q.getQuotation().getId())
                .collect(Collectors.toList());

        boolean usePriceCatalog = ji.getJob().getOrganisation().getComp().getBusinessSettings().getUsePriceCatalog();
        PriceLookupRequirements reqs = new PriceLookupRequirements(businessCompanyId, clientCompanyId, usePriceCatalog);
        PriceLookupInputFactory inputFactory = new PriceLookupInputFactory(reqs);
        PriceLookupInput input = inputFactory.addInstrument(ji.getServiceType().getServiceTypeId(),
                ji.getInst().getPlantid());
        reqs.setIncludeQuotationIds(includeQuotationIds);
        this.priceLookupService.findPrices(reqs);

        setDefaultCostsFromPriceLookup(ji, input, calibrationDiscount);
    }

    protected void setDefaultCostsFromPriceLookup(JobItem jobItem, PriceLookupInput input,
                                                  BigDecimal calibrationDiscount) {
        if (input.hasOutputs()) {
            // Match was found from new price lookup - use it
            PriceLookupOutput bestMatch = input.getOutputs().first();
            ContractReviewCalibrationCost calCost = this.conRevCalCostServ.createCost(jobItem, bestMatch,
                    calibrationDiscount);
            jobItem.setCalibrationCost(calCost);
        } else {
            // No match found - create empty cost
            ContractReviewCalibrationCost calCost = this.conRevCalCostServ.createEmptyCost(jobItem, true);
            jobItem.setCalibrationCost(calCost);
        }
        setOtherEmptyCosts(jobItem);
    }

    protected void setOtherEmptyCosts(JobItem jobItem) {
        ContractReviewPurchaseCost purchaseCost = this.conRevPurCostServ.createEmptyCost(jobItem, false);
        ContractReviewRepairCost repairCost = this.conRevRepCostServ.createEmptyCost(jobItem, false);
        ContractReviewAdjustmentCost adjustmentCost = this.conRevAdjCostServ.createEmptyCost(jobItem, false);

        jobItem.setPurchaseCost(purchaseCost);
        jobItem.setRepairCost(repairCost);
        jobItem.setAdjustmentCost(adjustmentCost);
    }

    @Override
    public boolean stateHasGroupOfKeyName(ItemState state, StateGroup group) {
        return jobItemDao.stateHasGroupOfKeyName(state, group);
    }

    @Override
    public void takeItemsOffHold(List<Integer> jobItemIds, String remark) {
        for (int jobItemId : jobItemIds) {
            JobItem ji = this.findJobItem(jobItemId);
            // if item is on hold
            if (ji.getState().isStatus() && ((ItemStatus) ji.getState()).isHold()) {
                // take off hold
                HookInterceptor_TakeOffHold.Input input = new HookInterceptor_TakeOffHold.Input(ji, remark);
                this.interceptor_takeOffHold.recordAction(input);
            }
        }
    }

    @Override
    @Deprecated
    public void updateJobItem(JobItem ji) {
        this.jobItemDao.update(ji);
    }

    @Override
    public void updateProcIfNecessary(JobItem ji, JobItemWorkRequirement jiwr) {
        if (jiwr.getWorkRequirement().getCapability() != null) {
            ji.setCapability(jiwr.getWorkRequirement().getCapability());
        } else {
            ji.setCapability(null);
        }
        /*
         * Calibration address must be updated as result of setting procedure
         * Added to enable transfer to other subdivisions/addresses 2016-01-27
         * GB
         */
        this.lookUpCalAddress(ji);
    }

    @Override
    public Boolean userAccreditedForItemProc(Contact con, JobItem ji) {
        if (ji.getCapability() == null)
            return null;
        else
            return this.procAccServ.authorizedFor(ji.getCapability().getId(),
                    ji.getServiceType().getCalibrationType().getCalTypeId(), con.getPersonid()).isRight();
    }

    @Override
    public JobItem getJobItemByItemCode(String itemCode) {
        String[] parts = itemCode.split("\\.");
        if (parts.length > 1)
            return jobItemDao.getJobItemByItemCode(parts[0], Integer.parseInt(parts[1]));
        else
            return null;
    }

    @Override
    public List<KeyValue<Integer, String>> getJobItemContractReviewCosts(JobItem ji) {
        List<KeyValue<Integer, String>> costs = new ArrayList<>();

        if (ji.getCalibrationCost() != null && ji.getCalibrationCost().getTotalCost().compareTo(BigDecimal.ZERO) != 0
                && ji.getCalibrationCost().isActive()) {
            costs.add(new KeyValue<>(ji.getCalibrationCost().getCostType().getTypeid(),
                    ji.getCalibrationCost().getTotalCost().toString()));
        }
        if (ji.getRepairCost() != null && ji.getRepairCost().getTotalCost().compareTo(BigDecimal.ZERO) != 0
                && ji.getRepairCost().isActive()) {
            costs.add(new KeyValue<>(ji.getRepairCost().getCostType().getTypeid(),
                    ji.getRepairCost().getTotalCost().toString()));
        }
        if (ji.getPurchaseCost() != null && ji.getPurchaseCost().getTotalCost().compareTo(BigDecimal.ZERO) != 0
                && ji.getPurchaseCost().isActive()) {
            costs.add(new KeyValue<>(ji.getPurchaseCost().getCostType().getTypeid(),
                    ji.getPurchaseCost().getTotalCost().toString()));
        }
        if (ji.getAdjustmentCost() != null && ji.getAdjustmentCost().getTotalCost().compareTo(BigDecimal.ZERO) != 0
                && ji.getAdjustmentCost().isActive()) {
            costs.add(new KeyValue<>(ji.getAdjustmentCost().getCostType().getTypeid(),
                    ji.getAdjustmentCost().getTotalCost().toString()));
        }
        return costs;
    }

    @Override
    public JobItem getForView(Integer jobItemId) {
        return this.jobItemDao.getForView(jobItemId);
    }

    @Override
    public PagedResultSet<TLMJobItemDTO> searchJobitems(boolean active, Integer plantId, String plantno,
                                                        String serialno, String formerBarcode, Integer calibrationSubdiv, String jobitemno, int currentPage,
                                                        int resultsPerPage, Locale locale) {
        return jobItemDao.searchJobitems(active, plantId, plantno, serialno, formerBarcode, calibrationSubdiv,
                jobitemno, currentPage, resultsPerPage, locale);
    }

    @Override
    public List<JobItemForProspectiveInvoiceDTO> getForProspectiveInvoice(Integer allocatedCompanyId,
                                                                          Integer companyId) {
        Locale locale = LocaleContextHolder.getLocale();
        List<JobItemForProspectiveInvoiceDTO> jobItems = jobItemDao.getForProspectiveInvoice(allocatedCompanyId,
                companyId, locale);
        jobItems.sort(JobItemForProspectiveInvoiceDTO::compareTo);
        return jobItems;
    }

    @Override
    public void updateAdditionalDemands(JobItem jobItem) {
        interceptor_recordAdditionalDemands.recordAction(jobItem);
        mergeJobItem(jobItem);
    }

    @Override
    public JobItem createNewRepairJobItem(Job newJob, JobItem oldJi, Contact con) {
        JobItem newJobItem = new JobItem();
        newJobItem.setBookedInAddr(oldJi.getBookedInAddr());
        newJobItem.setBookedInLoc(oldJi.getBookedInLoc());
        newJobItem.setCurrentAddr(oldJi.getCurrentAddr());
        newJobItem.setCurrentLoc(oldJi.getCurrentLoc());
        // set onBehalf
        if (oldJi.getOnBehalf() != null) {
            OnBehalfItem onBehalfItem = new OnBehalfItem();
            onBehalfItem.setCompany(oldJi.getOnBehalf().getCompany());
            onBehalfItem.setAddress(oldJi.getOnBehalf().getAddress());
            onBehalfItem.setSetOn(new Date());
            onBehalfItem.setSetBy(con);
            onBehalfItem.setJobitem(newJobItem);
            newJobItem.setOnBehalf(onBehalfItem);
        }

        Locale localeEnglish = new Locale("en", "GB");
        ServiceType repairServiceType = serviceTypeService.getByLongName(REPAIR_SERVICE_TYPE_LONGNAME, localeEnglish);
        newJobItem.setServiceType(repairServiceType);
        newJobItem.setReturnOption(oldJi.getReturnOption());
        newJobItem.setInOption(oldJi.getInOption());
        newJobItem.setReturnToAddress(oldJi.getReturnToAddress());
        newJobItem.setReturnToContact(oldJi.getReturnToContact());

        newJobItem.setItemNo(this.getNextItemNo(newJob));
        newJobItem.setJob(newJob);
        newJobItem.setTurn(oldJi.getTurn());
        newJobItem.setInst(oldJi.getInst());
        newJobItem.setReqCleaning(oldJi.getReqCleaning());
        newJobItem.setClientRef(oldJi.getClientRef());
        // changed to current date to fix issue with goods-in activity date on the new repair jobitem
        newJobItem.setDateIn(ZonedDateTime.now(LocaleContextHolder.getTimeZone().toZoneId()));
        newJobItem.setDueDate(oldJi.getDueDate());
        // copy accessories
        newJobItem.setAccessoryFreeText(oldJi.getAccessoryFreeText());
        final HashSet<ItemAccessory> newItemAccessories = new HashSet<>();
        oldJi.getAccessories().forEach(oldAccessory ->
                newItemAccessories.add(ItemAccessory.builder()
                        .accessory(oldAccessory.getAccessory())
                        .ji(newJobItem)
                        .quantity(oldAccessory.getQuantity())
                        .faultReport(oldAccessory.getFaultReport())
                        .build()));
        newJobItem.setAccessories(newItemAccessories);
        SubdivSettingsForAllocatedSubdiv subdivSettings = subdivSettingsService.get(newJob.getCon().getSub(),
                newJob.getOrganisation());
        if (subdivSettings != null)
            newJobItem.setLastContractReviewBy(subdivSettings.getDefaultBusinessContact());

        // add a default contract review outcome
        newJobItem.setConRevOutcome(oldJi.getConRevOutcome());

        // if this item is to join a new group
        newJobItem.setGroup(oldJi.getGroup());
        // look for discount defaults for this contact/sub/company or system
        // default
        Double calDiscount = discount.parseValue(discount
                .getValueHierarchical(Scope.CONTACT, newJob.getCon(), newJob.getOrganisation().getComp()).getValue());
        Locale locale = LocaleContextHolder.getLocale();
        String message = messageSource.getMessage("jiactions.oldrepairji",
                new Object[]{"<a href='jiactions.htm?jobitemid=" + oldJi.getJobItemId() + "'><u>"
                        + oldJi.getItemCode() + "</u></a>"},
                locale);
        this.insertJobItem(newJobItem, con, calDiscount, message);

        return newJobItem;
    }

    @Override
    public List<JobItem> findMostRecentJobItems(Integer plantid, Integer maxResults) {
        return this.jobItemDao.findMostRecentJobItems(plantid, maxResults);
    }

    @Override
    public Long getCountJobItemsForPlantid(Integer plantid) {
        return this.jobItemDao.getCountJobItemsForPlantid(plantid);
    }

    @Override
    public Page<JobItemIdDto> getJobItemIdDto(String days) {
        return jobItemDao.getJobItemIdDto(days);
    }

    @Override
    public List<ItemAtTP> getAllItemsAtSpecificThirdParty(List<Integer> subdivids, int businessSubdivId,
                                                          Locale locale) {
        List<ItemAtTP> listItemAtTP = this.jobItemDao.getAllItemsAtSpecificThirdParty(subdivids, businessSubdivId,
                locale);
        for (ItemAtTP item : listItemAtTP) {
            JobItem ji = this.findJobItem(item.getJobItemId());
            item.setDaysAway(AutoServiceCalculationTool.getDaysAwayAtSupplier(ji, false));
            item.setSentOn(AutoServiceCalculationTool.getOurDeliveryDate(ji, false));
            item.setPoDelDate(AutoServiceCalculationTool.getPromisedReturnDate(ji));
            item.setSentOn(AutoServiceCalculationTool.getOurDeliveryDate(ji, false));
            PurchaseOrderItem poi = AutoServiceCalculationTool.getSupplierPurchaseOrderItem(ji);
            PurchaseOrder po = (poi == null) ? null : poi.getOrder();
            if (po != null)
                item.setPoid(po.getId());
            item.setPono((po == null) ? null : po.getPono());
            item.setPoIssueDate(((poi == null) || !poi.getOrder().isIssued()) ? null : DateTools.dateFromLocalDate(poi.getOrder().getIssuedate()));
            DeliveryItem di = AutoServiceCalculationTool.getSupplierDeliveryNoteItem(ji, false);
            Contact contactDelivery = (di == null) ? null : di.getDelivery().getContact();
            item.setTelephone((contactDelivery == null) ? null : contactDelivery.getTelephone());
            item.setMobile((contactDelivery == null) ? null : contactDelivery.getMobile());
            item.setName((contactDelivery == null) ? null : contactDelivery.getName());
            item.setEmail((contactDelivery == null) ? null : contactDelivery.getEmail());

            // only if there is a PO with a promised date can we calculate if
            // this
            // item is now overdue
            item.setOverdue((item.getPoDelDate() != null) && item.getPoDelDate().before(new Date()));
        }
        return listItemAtTP;
    }

    @Override
    public List<JobItemImportInfoDto> getJobItemsInfo(Integer jobId) {
        return jobItemDao.getJobItemsInfo(jobId);
    }

    @Override
    public List<JobItemProjectionDTO> getJobItemProjectionDTOsByJobIds(Collection<Integer> jobIds, Locale locale) {
        return this.jobItemDao.getJobItemProjectionDTOsByJobIds(jobIds, locale);
    }

    @Override
    public Map<Instrument, JobItem> findMostRecentJobItem(List<Integer> plantids) {
        return jobItemDao.findMostRecentJobItem(plantids);
    }

    @Override
    public Map<Integer, JobItemProjectionDTO> findMostRecentJobItemDTO(List<Integer> plantids) {
        return jobItemDao.findMostRecentJobItemDTO(plantids);
    }

    @Override
    public ZonedDateTime getDateIn(Job job) {
        if (BooleanUtils.isTrue(job.getOverrideDateInOnJobItems())) {
            if (job.getPickupDate() != null)
                return job.getPickupDate();
            else if (job.getReceiptDate() != null)
                return job.getReceiptDate();
        }
        return null;
    }

    @Override
    public List<JobItemOnClientPurchaseOrderDTO> getJobItemsOnClientBPO(Integer poId, Integer jobId) {
        return this.jobItemDao.getJobItemsOnClientBPO(poId, jobId, LocaleContextHolder.getLocale());
    }

    @Override
    public void updateReturnToAddress(ReturnToAddressForm form) {
        Set<JobItem> jobItems = form.getJob().getItems();
        Address address = this.addrServ.get(form.getAddrid());
        Contact contact = form.getPersonid() == null ? null : this.contacService.get(form.getPersonid());
        for (JobItem jobItem : jobItems) {
            if (form.getJobitemids().get(jobItem.getJobItemId())) {
                jobItem.setReturnToAddress(address);
                jobItem.setReturnToContact(contact);
            }
        }
    }

    @Override
    public List<JobItemForInstrumentTooltipDto> findJobItemsForInstrumentTooltipByPlantId(Integer plantId) {
        return jobItemDao.findJobItemsForInstrumentTooltipByPlantId(plantId);
    }

    @Override
    public JobItem findMostRecentActiveJobItem(int plantid) {
        return this.jobItemDao.findMostRecentActiveJobItem(plantid);
    }

    @Override
    public List<Integer> getInstrumentIdsFromJob(Integer jobid) {
        return this.jobItemDao.getInstrumentIdsFromJob(jobid);
    }

    @Override
    public JobItemProjectionDTO getJobItemProjectionDTO(Integer jobitemId, Locale locale) {
        return this.jobItemDao.getJobItemProjectionDTO(jobitemId, locale);
    }

    @Override
    public ResultWrapper scanOutItem(JobItem ji, Address deliveryAddress) {

        Address fromAddr = ji.getCurrentAddr();
        Location fromLoc = ji.getCurrentLoc();
        ji.setCurrentAddr(null);
        ji.setCurrentLoc(null);
        Address toAddr = null;
        boolean isBusinessTransit = false, isThirdPartyTransit = false, isLabourTransit = false,
                isInspectionTransit = false;
        // ITERATE THROUGH ALL STATE GROUP LINKS
        for (StateGroupLink sgl : ji.getState().getGroupLinks()) {

            if (sgl.getGroup().equals(StateGroup.BUSINESSTRANSIT)) {
                // isBusinessTransit
                isBusinessTransit = true;
            }

            if (sgl.getGroup().equals(StateGroup.TPTRANSIT)) {
                // isThirdPartyTransit
                isThirdPartyTransit = true;
            }

            if (sgl.getGroup().equals(StateGroup.LABOURTRANSIT)) {
                // isLabourTransit
                isLabourTransit = true;
            }

            if (sgl.getGroup().equals(StateGroup.INSPECTIONTRANSIT)) {
                // isInspectionTransit
                isInspectionTransit = true;
            }
        }

        // if this item is going to another business company
        if (isBusinessTransit) {
            if (isLabourTransit || isInspectionTransit) {
                toAddr = ji.getCalAddr();
            } else {
                Subdiv toSub = ji.getReturnOption().getSub();
                // default: Using address from return option subdiv
                if (toSub.getDefaultAddress() != null)
                    toAddr = toSub.getDefaultAddress();
                else
                    toAddr = toSub.getAddresses().iterator().next();
                // inspectionTransit: Using address
            }
        }
        // If this item is going to a third party
        else if (isThirdPartyTransit) {
            if (ji.getTpRequirements().size() > 0) {
                TreeSet<TPRequirement> reqs = new TreeSet<>(new TPRequirementComparator());
                reqs.addAll(ji.getTpRequirements());
                // get most recent third party requirements on item
                TPRequirement tpReq = reqs.first();
                // get address from third party requirement delivery note
                Address addr = tpReq.getDeliveryItem().getDelivery().getAddress();
                // log.info("Third Party Transit, using ThirdPartRequirement
                // DeliveryItem Delivery address");
                ji.setCurrentAddr(addr);
                toAddr = addr;
            } else {
                for (JobDeliveryItem jdi : ji.getDeliveryItems())
                    if (jdi.getDelivery().isThirdPartyDelivery()) {
                        Address addr = jdi.getDelivery().getAddress();
                        // log.info("Third Party Transit, using JobDeliveryItem address");
                        ji.setCurrentAddr(addr);
                        toAddr = addr;
                    }
            }
        } else {
            toAddr = deliveryAddress;
        }
        ji = this.jobItemDao.merge(ji);
        HookInterceptor_DespatchItem.Input input = new HookInterceptor_DespatchItem.Input(ji, fromAddr, fromLoc, toAddr,
                null);
        this.interceptor_despatchItem.recordAction(input);
        return new ResultWrapper(true, null, null, null);
    }

    @Override
    public List<JobItem> getJobItemByDeliveryId(String delivryNo) {
        return this.jobItemDao.getJobItemByDeliveryId(delivryNo);
    }

    @Override
    public ResultWrapper scanInDeliveryItems(JobItem ji, int addrId, Integer locId) {

        if (!this.itemCanBeReceived(ji)) {
            String message = this.messageSource.getMessage(ERROR_CODE_JOBITEM_NOT_TRANSIT, null,
                    "This job item is not currently in transit", LocaleContextHolder.getLocale());
            return new ResultWrapper(false, message);
        }

        ji.setCurrentAddr(this.addrServ.get(addrId));
        Location loc = (locId != null) ? this.locServ.get(locId) : null;
        ji.setCurrentLoc(loc);
        ji = this.jobItemDao.merge(ji);
        this.interceptor_receiveItem.recordAction(ji);
        return new ResultWrapper(true, null, null, null);
    }

    @Override
    public ResultWrapper scanInItem(int barcode, int addrId, Integer locId) {
        JobItem ji = this.findMostRecentActiveJobItem(barcode);
        if (ji == null) {
            String message = this.messageSource.getMessage(ERROR_CODE_JOBITEM_NOT_FOUND_FOR_BARCODE, null,
                    "Job item not found for instrument barcode", LocaleContextHolder.getLocale());
            return new ResultWrapper(false, message);
        }

        if (!this.itemCanBeReceived(ji)) {
            String message = this.messageSource.getMessage(ERROR_CODE_JOBITEM_NOT_TRANSIT, null,
                    "This job item is not currently in transit", LocaleContextHolder.getLocale());
            return new ResultWrapper(false, message);
        }

        ji.setCurrentAddr(this.addrServ.get(addrId));
        Location loc = (locId != null) ? this.locServ.get(locId) : null;
        ji.setCurrentLoc(loc);
        ji = this.jobItemDao.merge(ji);
        this.interceptor_receiveItem.recordAction(ji);
        return new ResultWrapper(true, null, null, null);
    }

    @Override
    public List<SelectedJobItemDTO> getActiveJobItems(List<Integer> procIds, List<Integer> stateIds, Boolean isAllocatedToEngineer, Subdiv subdiv, StateGroupLinkType sglType, Integer addressId) {
        return this.jobItemDao.getActiveJobItems(procIds,stateIds,isAllocatedToEngineer,subdiv,sglType,addressId);
    }

    @Override
    public SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>> getActiveJobItemsReadyForAllocation(Subdiv subdiv, AllocateWorkToEngineerSearchForm form) {
        EnumSet<StateGroup> stateGroups = EnumSet.of(StateGroup.CALIBRATION, StateGroup.AWAITINGCALIBRATION,
                StateGroup.RESUMECALIBRATION, StateGroup.AWAITINGGENERALSERVICEOPERATION,
                StateGroup.GENERALSERVICEOPERATION, StateGroup.RESUMEGENERALSERVICEOPERATION,
                StateGroup.AWAITING_REPAIR_WORK_BY_TECHNICIAN);
        List<Integer> stateIds = itemStateServ.getItemStateIdsForStateGroups(stateGroups);

        List<Integer> procIds = new ArrayList<Integer>();

        if(form.getLaboratoryId()!=null){
            Department dept = departmentService.get(form.getLaboratoryId());
            procIds = workAssignmentServ.getCapabilitiesIdsAssignedToDepartment(dept);
        }
        else {
            for (Department dept : subdiv.getDepartments()) {
                if (dept.getType().isProcedures()) {
                    procIds.addAll(workAssignmentServ.getCapabilitiesIdsAssignedToDepartment(dept));
                }
            }
        }

        List<SelectedJobItemDTO> selectedJobItems = this.getActiveJobItemsForWorkAllocation(procIds, stateIds, false,
                subdiv, null, null,form);

        Set<Integer> clientCompanyIds = selectedJobItems.stream().map(SelectedJobItemDTO::getClientCompanyId)
                .collect(Collectors.toSet());
        List<CompanySettingsForAllocatedCompany> companySettings = companySettingsService.getAll(clientCompanyIds,
                subdiv.getComp());
        SortedMap<Integer, CompanySettingsForAllocatedCompany> companySettingsMap = new TreeMap<>();
        companySettings.forEach(cs -> companySettingsMap.put(cs.getCompany().getCoid(), cs));
        selectedJobItems.forEach(ji -> {
            if (companySettingsMap.containsKey(ji.getClientCompanyId())) {
                CompanySettingsForAllocatedCompany settings = companySettingsMap.get(ji.getClientCompanyId());
                ji.setClientCompanyActive(settings.isActive());
                ji.setClientCompanyOnStop(settings.isOnStop());
            } else {
                ji.setClientCompanyActive(false);
                ji.setClientCompanyOnStop(true);
            }
        });
        Map<Integer, List<SelectedJobItemDTO>> groupedJobItems = selectedJobItems.stream()
                .collect(Collectors.groupingBy(SelectedJobItemDTO::getJobId));
        SortedMap<SelectedJobDTO, List<SelectedJobItemDTO>> resultMap = new TreeMap<>();
        groupedJobItems
                .forEach(
                        (jobId, jobItems) -> resultMap.put(
                                new SelectedJobDTO(jobId, jobItems.get(0).getJobNo(), jobItems.get(0).getJobStatus(),
                                        jobItems.get(0).getJobRegDate(),
                                        jobItems.stream()
                                                .map(SelectedJobItemDTO::getDueDate).min(LocalDate::compareTo)
                                                .orElse(null),
                                        jobItems.stream().map(SelectedJobItemDTO::getTurn).min(Integer::compareTo)
                                                .orElse(-1) <= fastTrackTurn,
                                        jobItems.get(0).getJobType(), jobItems.get(0).getClientContactId(),
                                        jobItems.get(0).getClientContactFirstName() + " "
                                                + jobItems.get(0).getClientContactLastName(),
                                        jobItems.get(0).getClientContactActive()
                                                && jobItems.get(0).getClientSubdivActive()
                                                && jobItems.get(0).getClientCompanyActive(),
                                        jobItems.get(0).getClientCompanyId(), jobItems.get(0).getClientCompanyName(),
                                        jobItems.get(0).getClientSubdivId(), jobItems.get(0).getClientSubdivName(),
                                        jobItems.get(0).getClientCompanyActive(),
                                        jobItems.get(0).getClientCompanyOnStop(), jobItems.get(0).getTransportOutId(),
                                        jobItems.get(0).getTransportOutName() == null ? ""
                                                : jobItems.get(0).getTransportOutName()
                                                + (jobItems.get(0).getTransportOutSubdiv() == null ? ""
                                                : " - " + jobItems.get(0).getTransportOutSubdiv()),
                                        jobItems.get(0).getAllocatedSubdivId(),
                                        jobItems.get(0).getAllocatedSubdivCode()),
                                jobItems));
        return resultMap;
    }

    @Override
    public List<SelectedJobItemDTO> getActiveJobItemsForWorkAllocation(List<Integer> procIds, List<Integer> stateIds, Boolean isAllocatedToEngineer, Subdiv subdiv, StateGroupLinkType sglType, Integer addressId, AllocateWorkToEngineerSearchForm form) {
        return this.jobItemDao.getActiveJobItemsForWorkAllocation(procIds,stateIds,false,subdiv,sglType,addressId,form);
    }


}