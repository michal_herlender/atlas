package org.trescal.cwms.core.jobs.calibration.form;

import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;

/*
 * Note: Began to separate the attributes of a "New" calibration from a "Completed" calibration
 *       as they are comingled in the legacy CalibrationForm.
 * GB 2015-12-16
 */
public class CompleteCalibrationForm extends CalibrationForm {
	private Integer calId;
	private CalibrationClass calClass;
	private Boolean includeRemarksOnCert;
	
	public Integer getCalId() {
		return calId;
	}

	public void setCalId(Integer calId) {
		this.calId = calId;
	}

	public CalibrationClass getCalClass() {
		return calClass;
	}

	public void setCalClass(CalibrationClass calClass) {
		this.calClass = calClass;
	}

	public Boolean getIncludeRemarksOnCert() {
		return includeRemarksOnCert;
	}

	public void setIncludeRemarksOnCert(Boolean includeRemarksOnCert) {
		this.includeRemarksOnCert = includeRemarksOnCert;
	}
}
