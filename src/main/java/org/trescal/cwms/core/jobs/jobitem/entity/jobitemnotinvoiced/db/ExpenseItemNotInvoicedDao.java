package org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.JobExpenseItemNotInvoiced;
import org.trescal.cwms.core.jobs.jobitem.projection.JobExpenseItemNotInvoicedDTO;

public interface ExpenseItemNotInvoicedDao extends BaseDao<JobExpenseItemNotInvoiced, Integer> {
	List<JobExpenseItemNotInvoicedDTO> getProjectionDTOs(Collection<Integer> invoiceIds, Locale locale);
}