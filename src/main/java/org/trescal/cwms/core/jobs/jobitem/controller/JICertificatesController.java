package org.trescal.cwms.core.jobs.jobitem.controller;

import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.jasypt.digest.StandardStringDigester;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.jobdeliveryitem.JobDeliveryItemAgeComparator;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.external.adveso.events.UploadCertificateEvent;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateReplacementType;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.UserCertWrap;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLinkComparator;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.db.CertLinkService;
import org.trescal.cwms.core.jobs.certificate.entity.certnote.CertNote;
import org.trescal.cwms.core.jobs.certificate.entity.certnote.db.CertNoteService;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.contractreview.entity.workrequirement.db.WorkRequirementService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.form.JICertificatesForm;
import org.trescal.cwms.core.jobs.jobitem.form.JICertificatesValidator;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.db.ProcedureTrainingRecordService;
import org.trescal.cwms.core.recall.entity.recallrule.RecallRuleInterval;
import org.trescal.cwms.core.recall.entity.recallrule.db.RecallRuleService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.labelprinter.db.LabelPrinterService;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.CalibrationVerification;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.Deviation;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.DigestConfigCreator;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.labelprinting.certificate.CertificateLabelSettings;
import org.trescal.cwms.core.tools.labelprinting.certificate.CertificateLabelSettingsService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_CreateCertificate;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_CreateCertificate.Input;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.FilenameFilter;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV,
    Constants.HTTPSESS_NEW_FILE_LIST})
public class JICertificatesController extends JobItemController {
    private final Logger logger = LoggerFactory.getLogger(JICertificatesController.class);

    private static final String THIRD_PARTY_CERTIFICATE = "Third party certificate added";

	@Value("${cwms.config.certfolder}")
	private String certificateFolder;
	@Value("${cwms.config.certificate.metcal.template_path}")
	private String metcalTemplatePath;
	@Value("${cwms.config.printing.certificate.label.certlinkbased}")
	private String labelCertLinkBased;
	@Autowired
	private ActionOutcomeService aoServ;
	@Autowired
	private AuthenticationService authServ;
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private CalibrationVerification systemDefaultCalibrationVerification;
	@Autowired
	private Deviation systemDefaultDeviation;
	@Autowired
	private CertificateService certServ;
	@Autowired
	private CertificateLabelSettingsService labelSettingsService;
	@Autowired
	private CertNoteService certificateNoteService;
	@Autowired
	private CertLinkService clServ;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private ContactService conServ;
    @Autowired
    private DigestConfigCreator configCreator;
    @Autowired
    private FileBrowserService fileBrowserService;
    @Autowired
    private InstrumService instrumServ;
    @Autowired
    private ItemStateService itemStateService;
    @Autowired
    private HookInterceptor_CreateCertificate interceptor_createCertificate;
    @Autowired
    private LabelPrinterService labelPrinterService;
    @Autowired
    private CapabilityAuthorizationService procAccServ;
    @Autowired
    private ProcedureTrainingRecordService procRecordServ;
    @Autowired
    private RecallRuleService recallRuleServ;
    @Autowired
    private SystemComponentService scServ;
    @Autowired
    private SubdivService subServ;
    @Autowired
    private JICertificatesValidator validator;
    @Autowired
    private ServletContext servletContext;
	@Autowired
	private AuthenticationService authenticationService;
	@Autowired
	private WorkRequirementService workRequirementService;
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
	}

	@ModelAttribute("units")
	public List<KeyValue<String, String>> initializeRecallIntervalUnits() {
		return IntervalUnit.getUnitsForSelect();
	}

	@ModelAttribute("verificationstatuses")
	public List<KeyValue<CalibrationVerificationStatus, String>> getVerificationStatuses() {
        return Arrays.stream(CalibrationVerificationStatus.values()).map(s -> new KeyValue<>(s, s.getName()))
            .collect(Collectors.toList());
    }

	@ModelAttribute("form")
	protected JICertificatesForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") Integer jobitemid,
			Model model)
			throws Exception {
		JICertificatesForm form = new JICertificatesForm();
		JobItem ji = this.jobItemService.findJobItem(jobitemid);
		if ((ji == null) || (jobitemid == 0)) {
			logger.error("Jobitem with id " + jobitemid + " could not be found");
            throw new Exception("Unable to locate requested jobitem");
        }
        Contact contact = this.userService.get(username).getCon();
        form.setJi(ji);
        form.setCurrentContactId(contact.getPersonid());
        File directoryJobCerts = this.compDirServ.getDirectory(this.scServ.findComponent(Component.CERTIFICATE),
            ji.getJob().getJobno());
        Certificate c = new Certificate();
        c.setCalDate(new Date());
        c.setCertDate(new Date());
        RecallRuleInterval recallRuleInterval = this.recallRuleServ.getCurrentRecallInterval(ji.getInst(),
            ji.getServiceType().getCalibrationType().getRecallRequirementType(), null);
        c.setUnit(recallRuleInterval.getUnit());
        form.setDurationUnitId(recallRuleInterval.getUnit().name());
        c.setDuration(recallRuleInterval.getInterval());
        c.setDirectory(directoryJobCerts);
        form.setC(c);
        form.setCalTypes(this.calTypeServ.getActiveCalTypes());
        form.setCalTypeId(form.getJi().getServiceType().getCalibrationType().getCalTypeId());
        // sets the component for the cert file uploader on this page
        form.setComponent(this.scServ.findComponent(Component.CERTIFICATE));
        form.getJi().setJob(this.jobService.get(form.getJi().getJob().getJobid()));
        form.setUserCertWraps(this.getAccreditationsForCerts(form.getJi().getCertLinks(), contact, directoryJobCerts));
        boolean pwExists = (contact.getEncryptedPassword() != null) && !contact.getEncryptedPassword().equals("");
        form.setPwExists(pwExists);
        // tp certificates
        Map<Integer, String> tpCertificates = new HashMap<>();
        tpCertificates.put(null, null);
        for (CertLink certLink : ji.getCertLinks()) {
            if (CertificateType.CERT_THIRDPARTY.equals(certLink.getCert().getType()))
                tpCertificates.put(certLink.getCert().getCertid(), certLink.getCert().getCertno());
        }
        model.addAttribute("tpCertificates", tpCertificates);
        if (ji.getJob().getType().equals(JobType.STANDARD))
            model.addAttribute("certType", CertificateReplacementType.LAB_CERT.name());
        else if (ji.getJob().getType().equals(JobType.SITE))
            model.addAttribute("certType", CertificateReplacementType.ONSITE_CERT.name());
        return form;
    }

	public List<UserCertWrap> getAccreditationsForCerts(Set<CertLink> certLinks, Contact con, File directoryJobCerts) {
        ArrayList<UserCertWrap> wraps = new ArrayList<>();
        int procId;
        int calTypeId;
        int personId = con.getPersonid();
        for (CertLink cl : certLinks) {
            // sets directory
            cl.getCert().setDirectory(directoryJobCerts);
            CertificateLabelSettings labelSettings = this.labelSettingsService.get(cl);
            if (cl.getCert().getCal() != null) {
                procId = cl.getCert().getCal().getCapability().getId();
                calTypeId = cl.getCert().getCal().getCalType().getCalTypeId();
                val accStat = this.procAccServ.authorizedFor(procId, calTypeId, personId);
                UserCertWrap wrap = accStat.map(l -> new UserCertWrap(cl, true, l, labelSettings))
                    .getOrElse(new UserCertWrap(cl, false, null, labelSettings));

                wraps.add(wrap);
            } else {
                wraps.add(new UserCertWrap(cl, null, null, labelSettings));
			}
		}
		return wraps;
	}

    @PreAuthorize("@hasJobItemAuthorityOnConnectedSubdiv.check('JI_CERTIFICATE')")
    @RequestMapping(value = "/jicertificates.htm", method = RequestMethod.POST)
    protected ModelAndView onSubmit(
        @RequestParam(value = "subdivid", required = false, defaultValue = "0") Integer subdivId,
        @RequestParam(value = "jobitemid") Integer jobItemId,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
        @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
        @Validated @ModelAttribute("form") JICertificatesForm form, BindingResult bindingResult) throws Exception {
        if (bindingResult.hasErrors())
            return referenceData(username, subdivDto, newFiles, jobItemId);
        Contact contact = this.userService.get(username).getCon();
        Subdiv allocatedSubdiv = subServ.get(subdivDto.getKey());
        IntervalUnit intervalUnit = IntervalUnit.valueOf(form.getDurationUnitId());
        JobItem ji = form.getJi();
        switch (form.getAction()) {
            case "reset":
                // find procedure training record
                procRecordServ.findCapabilityTrainingRecordViaCertificate(form.getActionForCertId())
                    .ifPresent(procRecordServ::deleteCapabilityTrainingRecord);
                // procedure training record returned?
                // reset certificate
                this.certServ.resetCertificate(form.getActionForCertId(), contact);
                break;
            case "delay": {
                Certificate cert = this.certServ.findCertificate(form.getActionForCertId());
                this.certServ.delayCertificate(cert);
                break;
            }
            case "sign": {
                Certificate cert = this.certServ.findCertificate(form.getActionForCertId());
                // if a file has been uploaded then copy it into the certificate
                // directory
                certServ.uploadFile(form.getFile(), cert, contact);
                // if setting new password
                if (!form.isPwExists()) {
                    StandardStringDigester digester = new StandardStringDigester();
                    digester.setConfig(this.configCreator.createDigesterConfig());
                    String encryptedPassword = digester.digest(form.getEncPassword());
                    this.conServ.setEncryptedPassword(contact.getPersonid(), encryptedPassword);
                }
                // sign cert
                this.certServ.signCertificate(cert, contact);

                // is this certificate to be a procedure training record?
                if (form.isProcTrainingRecord()) {
                    // insert procedure training record
                    if ((contact != null) && (this.authServ.hasRight(contact, Permission.PROCEDURE_TRAINING_CERTIFICATION,
                        subdivDto.getKey())))
                        this.procRecordServ.insertProcedureTrainingRecord(cert, cert.getCal().getCapability(), contact);
                }
                break;
            }
            case "new": {
                Certificate cert = form.getC();
                CertificateType type = CertificateType.CERT_THIRDPARTY;
                CalibrationType calType = this.calTypeServ.find(form.getCalTypeId());
                String note = form.getNote();
                cert.setServiceType(calType.getServiceType());
                cert.setRegisteredBy(contact);
                cert.setCalibrationVerificationStatus(form.getCalibrationVerificationStatus());
                cert.setDeviation(form.getDeviation());
                cert.setProcTemplateKey("");
                cert.setStatus(CertStatusEnum.SIGNING_NOT_SUPPORTED);
                cert.setType(type);
                cert.setUnit(intervalUnit);
                cert.setCertno(this.certServ.findNewCertNo(type, allocatedSubdiv));
                cert.setThirdDiv(this.subServ.get(subdivId));
                // set certificate supplementary for added
                // after tp certificate cancel and replace
                if (form.getSupplementaryForId() != null) {
                    Certificate certSuppFor = this.certServ.findCertificate(form.getSupplementaryForId());
                    certSuppFor.setStatus(CertStatusEnum.REPLACED);
                    cert.setSupplementaryFor(certSuppFor);
                    if (StringUtils.isNoneBlank(form.getSupplementaryForComment()))
                        cert.setSupplementaryCertificateComments(form.getSupplementaryForComment());
                }
                ActionOutcome ao = (form.getActionOutcomeId() == null) ? null : this.aoServ.get(form.getActionOutcomeId());
                cert.setThirdOutcome(ao);
                CertLink certLink = new CertLink();
                certLink.setJobItem(ji);
                certLink.setCert(cert);
                cert.setLinks(new TreeSet<>(new CertLinkComparator()));
                cert.getLinks().add(certLink);
                ji.getCertLinks().add(certLink);
                if (ji.getInst().getInstrumentComplementaryField() != null
                    && ji.getInst().getInstrumentComplementaryField().getCertificateClass() != null) {
                    cert.setCertClass(ji.getInst().getInstrumentComplementaryField().getCertificateClass());
                }

                this.certServ.insertCertificate(cert, contact);

                // check current WR if outcome is successful and and type of certificate is
                // third party
                if (cert.getThirdOutcome() != null && cert.getThirdOutcome().isPositiveOutcome()
                    && jobItemService.stateHasGroupOfKeyName(ji.getState(), StateGroup.AWAITING_TP_CERTIFICATE)) {
                    workRequirementService.checkCurrentWorkRequirement(jobItemService.findJobItem(jobItemId));
                }

                interceptor_createCertificate.recordAction(
                    new Input(certLink, HookInterceptor_CreateCertificate.CERTIFICATE_DEFAULT_TIME_SPENT));
                // reload instrument to prevent session error
                Instrument inst = this.instrumServ.get(ji.getInst().getPlantid());
                // update the instrument's recall date
                this.instrumServ.calculateRecallDateAfterTPCert(inst, cert);
                // Create CertLinks for all other checked items
                if (form.getOtherJobItems() != null) {
                    for (Integer id : form.getOtherJobItems()) {
                        JobItem otherItem = this.jobItemService.findJobItem(id);
                        CertLink cl = new CertLink();
                        cl.setCert(cert);
                        cl.setJobItem(otherItem);
                        cert.getLinks().add(cl);
                        this.clServ.insertCertLink(cl);
                        // reload instrument to prevent session error
                        inst = this.instrumServ.get(otherItem.getInst().getPlantid());
                        // update the instrument's recall date
                        this.instrumServ.calculateRecallDateAfterTPCert(inst, cert);
                        // then persist the changes
                        this.instrumServ.merge(inst);
                    }
                }
                cert.setNotes(new TreeSet<>(new NoteComparator()));
                // If there is text in the note box, create the CertNote
                if (!note.trim().equals("")) {
                    CertNote certNote = new CertNote();
                    certNote.setActive(true);
                    certNote.setCert(cert);
                    certNote.setNote(note);
                    certNote.setPublish(true);
                    certNote.setSetBy(contact);
                    certNote.setSetOn(new Date());
                    cert.getNotes().add(certNote);
                    certificateNoteService.insertCertNote(certNote);
                }

                // uploads cert file if one was supplied
                certServ.uploadFile(form.getFile(), cert, contact);

                // create new UploadCertificate notification
                // send event for upload certificate
                UploadCertificateEvent uploadCertEvent = new UploadCertificateEvent(this, cert, false, ji);
                applicationEventPublisher.publishEvent(uploadCertEvent);
                break;
            }
        }
        return new ModelAndView(new RedirectView("jicertificates.htm?jobitemid=" + jobItemId));
    }

    @PreAuthorize("hasAuthority('JI_CERTIFICATE')")
    @RequestMapping(value = "/jicertificates.htm", method = RequestMethod.GET)
    protected ModelAndView referenceData(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                                         @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                         @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
                                         @RequestParam(value = "jobitemid") Integer jobItemId) throws Exception {
        Contact contact = this.userService.get(username).getCon();
        User user = this.userService.get(username);
        Map<String, Object> map = super.referenceData(jobItemId, contact, subdivDto.getKey(), username);
        JobItem ji = jobItemService.findJobItem(jobItemId);
        map.put(Constants.REFDATA_SC_ROOT_FILES,
            this.fileBrowserService.getFilesForComponentRoot(Component.JOB, ji.getJob().getJobid(), newFiles));
        File dvslDir = new File(this.certificateFolder);
        File[] templatesF = dvslDir.listFiles();
        Vector<String> templates = new Vector<>();
        if (templatesF != null) {
            for (File f : templatesF) {
                templates.add(f.getName());
            }
        }
        map.put("certtemplates", templates);
        map.put("calibrationVerification", this.systemDefaultCalibrationVerification.getValueByScope(Scope.COMPANY,
            ji.getJob().getCon().getSub().getComp(), null));
        map.put("caneditverificationstatus",
            authenticationService.hasRight(user.getCon(), Permission.VERIFICATION_STATUT_EDIT, subdivDto.getKey()));
        map.put("deviationAllowed", this.systemDefaultDeviation.getValueByScope(Scope.COMPANY,
            ji.getJob().getCon().getSub().getComp(), null));
        File metDir = new File(this.metcalTemplatePath);
        ArrayList<String> metTemplates = new ArrayList<>();
        // check if metcal dir exists before trying to get files
        if (metDir.exists()) {
            FilenameFilter filter = (dir, name) -> name.endsWith(".rpt");
            File[] metTemps = metDir.listFiles(filter);
            for (File f : Objects.requireNonNull(metTemps)) {
                metTemplates.add(f.getName());
            }
        }
        map.put("metcaltemplates", metTemplates);
        ItemActivity activity = itemStateService.findItemActivityByName(THIRD_PARTY_CERTIFICATE);
		map.put("actionoutcomes", this.aoServ.getActionOutcomesForActivity(activity));
		Subdiv tpSubdiv = null;
		if ((ji.getDeliveryItems() != null) && (ji.getDeliveryItems().size() > 0)) {
            TreeSet<JobDeliveryItem> dis = new TreeSet<>(new JobDeliveryItemAgeComparator());
            dis.addAll(ji.getDeliveryItems());
            for (JobDeliveryItem jdi : dis.descendingSet()) {
                // have to use string comparison here because instanceof isn't
                // working for some reason!?
                if (jdi.getDelivery().isThirdPartyDelivery()) {
                    tpSubdiv = jdi.getDelivery().getAddress().getSub();

                    // break after first because this set is ordered to be most
                    // recent first
                    break;
                }
            }
        }
		map.put("mostRecentTpSubdiv", tpSubdiv);
		boolean isAdminUser = this.authServ.hasRight(contact, Permission.JI_CERTIFICATE_ADMIN, subdivDto.getKey());
		map.put("isAdminUser", isAdminUser);
		boolean isUserManager = this.authServ.hasRight(contact, Permission.JI_CERTIFICATE_MANAGE, subdivDto.getKey());
		map.put("isUserManager", isUserManager);
		boolean hasSupplementaryCertPermission = this.authServ.hasRight(contact, Permission.JI_CERTIFICATE_SUPPLEMENTARY, subdivDto.getKey());
		map.put("hasSupplementaryCertPermission", hasSupplementaryCertPermission);
		
		// May be null if no label printer defined for user or user's default
		// address
		map.put("labelPrinter", labelPrinterService.getDefaultLabelPrinterForContact(contact));
		map.put("useCertLinks", this.labelCertLinkBased);
		return new ModelAndView("trescal/core/jobs/jobitem/jicertificates", map);
	}
}