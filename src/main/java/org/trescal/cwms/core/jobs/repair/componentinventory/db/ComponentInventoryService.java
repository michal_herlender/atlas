package org.trescal.cwms.core.jobs.repair.componentinventory.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.repair.componentinventory.ComponentInventory;

public interface ComponentInventoryService extends BaseService<ComponentInventory, Integer>
{
	
}