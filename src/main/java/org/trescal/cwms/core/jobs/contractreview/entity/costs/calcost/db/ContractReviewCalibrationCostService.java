package org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.db;

import io.vavr.control.Either;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.contractreview.entity.contractreview.ContractReview;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewLinkedCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.dto.ContractReviewCalibrationCostDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costs.CostLookUp;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costs.dto.CostJobItemDto;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutput;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

import java.math.BigDecimal;
import java.util.Locale;

public interface ContractReviewCalibrationCostService {
	/**
	 * Create empty zero valued cost
	 * 
	 */
	ContractReviewCalibrationCost createEmptyCost(JobItem jobItem, boolean active);

	/**
	 * Creates costs based on PriceLookupOutput
	 */
	ContractReviewCalibrationCost createCost(JobItem ji, PriceLookupOutput priceLookupOutput,
			BigDecimal calibrationDiscount);

	/**
	 * Creates a copy of the {@link Cost} and assigns it to the given
	 * {@link JobItem}. The cost must be a {@link ContractReview} {@link Cost}.
	 * 
	 * @param oldCost       the {@link Cost} to copy from.
	 * @param copyToJobitem the {@link JobItem} to copy into.
	 * @return the new {@link Cost}.
	 */
	ContractReviewCalibrationCost copyContractReviewCost(ContractReviewCalibrationCost oldCost, JobItem copyToJobitem);

	void deleteLinkCost(ContractReviewLinkedCalibrationCost linkedCost);

	ContractReviewCalibrationCost findContractReviewCalibrationCost(int id);

	/**
	 * Loads a dto object ({@link CostLookUp}) and populates it with sets of
	 * alternative costs for the given {@link JobItem}.
	 * 
	 * @param jobitemid the id of the {@link JobItem}.
	 * @param caltypeid the id of the {@link CalibrationType} of this
	 *                  {@link JobItem} (if null then the {@link CalibrationType} of
	 *                  the actual {@link JobItem} is used).
	 * @return {@link CostLookUp}.
	 */
	CostLookUp getAlternativeCosts(int jobitemid, Integer caltypeid);

	Either<String,CostSource> resetLinkedCostByCostId(Integer costId);

	/**
	 * Resets the {@link ContractReviewCalibrationCost} to be manually set. Any
	 * linked cost sources are discarded as a result.
	 * 
	 * @param costid       the id of the cost to reset.
	 * @return {@link ResultWrapper} indicating success and if successful the
	 *         {@link ContractReviewCalibrationCost} affected.
	 */
	ResultWrapper resetLinkedCost(int costid);

    Either<String, CostJobItemDto> updateCostSourceE(Integer jobItemId, Integer costId, CostSource costSource);

    /*
	 * Updates the {@link JobItem} identified by the given id with a new
	 * {@link ContractReviewCalibrationCost} of the given {@link CostSource} type.
	 * Any existing {@link ContractReviewLinkedCalibrationCost} costs are discarded
	 * and if required a new one is created. Validates the {@link JobItem} and
	 * {@link CostSource}.
	 * 
	 * @param jobitemid  the id of the {@link JobItem} to update.
	 * @param costid     the id of the new linked cost source, nullable if manual or
	 *                   instrumentmodel defaults are set as the {@link CostSource}.
	 * @param costSource the {@link CostSource} of the new cost.
	 * @return {@link ResultWrapper} indicating success of operation.
	 */
	ResultWrapper updateCostSource(int jobitemid, Integer costid, CostSource costSource);
	
	ContractReviewCalibrationCost resetLinkedCalContactReviewCost(ContractReviewCalibrationCost calCost);
	
	ContractReviewCalibrationCostDTO findContractReviewCalibrationCost(Integer id, Integer servicetypeid, Locale locale);
}