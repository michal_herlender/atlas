package org.trescal.cwms.core.jobs.job.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.job.dto.BpoDTO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.db.JobItemPOService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@JsonController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class BPOFunctionsController {

	@Autowired
	private BPOService bpoService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private JobItemPOService jiPoService;

	@RequestMapping(value = "/contactrelatedbpos.json", method = RequestMethod.GET)
	@ResponseBody
	public Map<Scope, List<BpoDTO>> contactrelatedbpos(@RequestParam(value = "personid") Integer personid,
			@RequestParam(value = "jobid") Integer jobid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		Contact contact = contactService.get(personid);
		Company allocatedCompany = companyService.get(companyDto.getKey());
		List<BPO> bpos = bpoService.getAllByContactHierarchical(contact, allocatedCompany);
		Map<Scope, List<BpoDTO>> result = new HashMap<>();
		result.put(Scope.CONTACT, new ArrayList<>());
		result.put(Scope.SUBDIV, new ArrayList<>());
		result.put(Scope.COMPANY, new ArrayList<>());
		for (BPO bpo : bpos)
			if (bpo.getJobLinks() != null && bpo.getJobLinks().stream().noneMatch(e -> e.getJob().getJobid() == jobid))
				switch (bpo.getScope()) {
				case CONTACT:
					result.get(Scope.CONTACT).add(new BpoDTO(bpo));
					break;
				case SUBDIV:
					result.get(Scope.SUBDIV).add(new BpoDTO(bpo));
					break;
				case COMPANY:
					result.get(Scope.COMPANY).add(new BpoDTO(bpo));
				default:
				}
		return result;
	}

	@RequestMapping(value = "/assignbpotojob.json", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> assignBPOtoJob(@RequestParam(value = "jobid") Integer jobId,
			@RequestParam(value = "bpoid") Integer bpoId) {
		Map<String, Object> result = new HashMap<>();
		result.put("bpo", bpoService.addBPOToJob(jobId, bpoId));
		return result;
	}

	@RequestMapping(value = "/assignbpotojobitem.json", method = RequestMethod.PUT)
	public Map<String, BpoDTO> assignBPOtoJobItem(@RequestParam Integer jobItemId, @RequestParam Integer bpoId) {
		Map<String, BpoDTO> result = new HashMap<>();
		result.put("bpo", bpoService.addBPOToJobItem(jobItemId, bpoId));
		return result;
	}

	@RequestMapping(value = "/removeitemfrombpo.json", method = RequestMethod.PUT)
	public void removeItemFromBPO(@RequestParam Integer jobItemId, @RequestParam Integer bpoId) {
		bpoService.removeItemFromBPO(jobItemId, bpoId);
	}

	@RequestMapping(value = "/assignjobitemstobpo.json", method = RequestMethod.POST)
	@ResponseBody
	public boolean assignJobItemsToBPO(
			@RequestParam(value = "jobitemsid[]", required = false, defaultValue = "") List<Integer> jobItemsId,
			@RequestParam(value = "jobservicesid[]", required = false, defaultValue = "") List<Integer> jobServicesId,
			@RequestParam(value = "bpoid") Integer bpoId, @RequestParam(value = "jobid") Integer jobId) {
		ResultWrapper result = this.jiPoService.assignJIandJobServicestoBPO(jobItemsId, jobServicesId, bpoId, jobId);
		return result.isSuccess();
	}

	@RequestMapping(value = "/unsignJobitemsAndJobServicesToBPO.json", method = RequestMethod.POST)
	@ResponseBody
	public boolean unsignJobitemsAndJobServicesToBPO(
			@RequestParam(value = "jobitemid", required = false, defaultValue = "") Integer jobItemId,
			@RequestParam(value = "jobserviceid", required = false, defaultValue = "") Integer jobServiceId,
			@RequestParam(value = "bpoid") Integer bpoId, @RequestParam(value = "jobid") Integer jobId) {
		ResultWrapper result = this.jiPoService.unassignJIandJobServicestoBPO(jobItemId, jobServiceId, bpoId, jobId);
		return result.isSuccess();
	}

	@RequestMapping(value = "/removeJobBPO.json", method = RequestMethod.POST)
	@ResponseBody
	public boolean removeJobBPO(@RequestParam(value = "jobid") Integer jobId,
			@RequestParam(value = "bpoid") Integer bpoId) {
		this.jiPoService.deleteBPOLinksToJob(bpoId, jobId);
		return true;
	}

	@RequestMapping("bposelection.json")
	public Map<Scope, List<BpoDTO>> bpoSeclection(@RequestParam Integer jobId,
			@RequestParam(required = false, defaultValue = "true") Boolean activeOnly,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyDto) {
		return bpoService.findAllForJob(jobId, activeOnly);
	}

}