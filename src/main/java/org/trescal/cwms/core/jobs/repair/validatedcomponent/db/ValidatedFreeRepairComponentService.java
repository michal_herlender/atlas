package org.trescal.cwms.core.jobs.repair.validatedcomponent.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.repair.validatedcomponent.ValidatedFreeRepairComponent;

public interface ValidatedFreeRepairComponentService extends BaseService<ValidatedFreeRepairComponent, Integer>
{
	
}