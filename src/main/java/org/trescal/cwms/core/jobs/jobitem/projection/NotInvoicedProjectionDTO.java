package org.trescal.cwms.core.jobs.jobitem.projection;

import java.time.LocalDate;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnotinvoiced.NotInvoicedReason;
import org.trescal.cwms.core.pricing.invoice.projection.InvoiceProjectionDTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public abstract class NotInvoicedProjectionDTO {
	private LocalDate date;
	private Integer contactId;
	private NotInvoicedReason reason;
	private String comments;
	private Integer periodicInvoiceId;

	// Optionally initialized after creation
	private InvoiceProjectionDTO invoice;
	
	public NotInvoicedProjectionDTO(LocalDate date, Integer contactId, NotInvoicedReason reason, String comments,
			Integer invoiceId) {
		super();
		this.date = date;
		this.contactId = contactId;
		this.reason = reason;
		this.comments = comments;
		this.periodicInvoiceId = invoiceId;
	}
}
