package org.trescal.cwms.core.jobs.calibration.entity.calreqhistory;

import java.util.Comparator;

public class CalReqHistoryComparator implements Comparator<CalReqHistory>
{
	@Override
	public int compare(CalReqHistory h1, CalReqHistory h2)
	{
		return h1.getChangeOn().compareTo(h2.getChangeOn());
	}
}
