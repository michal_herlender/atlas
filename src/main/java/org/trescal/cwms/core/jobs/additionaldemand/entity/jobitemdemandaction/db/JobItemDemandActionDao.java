package org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemandaction.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.additionaldemand.entity.jobitemdemandaction.JobItemDemandAction;

public interface JobItemDemandActionDao extends BaseDao<JobItemDemandAction, Integer> {
}