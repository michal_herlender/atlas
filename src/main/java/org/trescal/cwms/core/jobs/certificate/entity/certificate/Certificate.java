package org.trescal.cwms.core.jobs.certificate.entity.certificate;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateClass;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certaction.CertAction;
import org.trescal.cwms.core.jobs.certificate.entity.certaction.CertActionComparator;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLinkJobComparator;
import org.trescal.cwms.core.jobs.certificate.entity.certnote.CertNote;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidation;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidationComparator;
import org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.CapabilityTrainingRecord;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.EncryptionTools;
import org.trescal.cwms.core.tools.supportedlocale.LocaleFilter;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileFilter;
import java.math.BigDecimal;
import java.util.*;

@Entity
@Table(name = "certificate", indexes = {@Index(columnList = "caldate", name = "IDX_certificate_calDate")})
public class Certificate extends Auditable implements ComponentEntity, NoteAwareEntity {
	private Set<CertAction> actions;
	private Contact approvedBy;
	private Calibration cal;
	private Date calDate;
	private CalibrationType calType;    // Note both serviceType and calType are set/maintained, calType still in use
	private ServiceType serviceType;
	private Date certDate;
    private int certid;
    private String certno;
    private Boolean certPreviewed;
    private File directory;
    private String docTemplateKey;
    private Integer duration;
    private IntervalUnit unit;
    private Set<InstCertLink> instCertLinks;
    private Set<CertLink> links;
    private Set<CertNote> notes;
    private String procTemplateKey;
    private CapabilityTrainingRecord procTrainingRec;
    private Contact registeredBy;
    private List<Email> sentEmails;
    private Contact signedBy;
    private CertStatusEnum status;
    private Certificate supplementaryFor;
    private String thirdCertNo;
    private Subdiv thirdDiv;
    private ActionOutcome thirdOutcome;
    private CertificateType type;
    private Set<CertificateValidation> validations;
    private String remarks;
	private CalibrationVerificationStatus calibrationVerificationStatus;
	private BigDecimal deviation;
	private CertificateClass certClass;
	private Boolean optimization;
	private Boolean restriction;
	private Boolean adjustment;
	private Boolean repair;
	private String supplementaryCertificateComments;
	private final Logger logger = LoggerFactory.getLogger(LocaleFilter.class);

	public Certificate() {
		certPreviewed = Boolean.FALSE;
	}

	@OneToMany(mappedBy = "cert", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(CertActionComparator.class)
	public Set<CertAction> getActions() {
		return this.actions;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approvedby")
	public Contact getApprovedBy() {
		return this.approvedBy;
	}

	/**
	 * @return the cal
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "calid")
	public Calibration getCal() {
		return this.cal;
	}

	/**
	 * @return the calDate
	 */
	@NotNull
	@Column(name = "caldate", nullable = false, columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCalDate() {
		return this.calDate;
	}

	/**
	 * @deprecated use getServiceType(); fields maintained in parallel until calType fully removed
	 */
	@Deprecated
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "caltype")
	public CalibrationType getCalType() {
		return this.calType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "servicetypeid", foreignKey=@ForeignKey(name="FK_certificate_servicetype"))
	public ServiceType getServiceType() {
		return serviceType;
	}
	
	/**
	 * @return the certDate
	 */
	@NotNull
	@Column(name = "certdate", nullable = false, columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCertDate() {
		return this.certDate;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "certid", nullable = false, unique = true)
	@Type(type = "int")
	public int getCertid() {
		return this.certid;
	}

	/**
	 * @return the certno
	 */
	@Length(max = 60)
	@Column(name = "certno", unique = true, length = 60)
	public String getCertno() {
		return this.certno;
	}

	@Override
	@Transient
	public Contact getDefaultContact() {
		return null;
	}

	@Override
	@Transient
	public File getDirectory() {
		return this.directory;
	}

	@Column(name = "doctemplatekey", length = 100)
	public String getDocTemplateKey() {
		return this.docTemplateKey;
	}

	/**
	 * @return the duration
	 */
	@Column(name = "duration", nullable = false)
	@Min(0)
	public Integer getDuration() {
		return this.duration;
	}


	@Override
	@Transient
	public String getIdentifier() {
		return this.certno;
	}

	@OneToMany(mappedBy = "cert", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<InstCertLink> getInstCertLinks() {
		return this.instCertLinks;
	}

	/**
	 * @return the links
	 */
	@OneToMany(mappedBy = "cert", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(CertLinkJobComparator.class)
	public Set<CertLink> getLinks() {
		return this.links;
	}

	/**
	 * @return the notes
	 */
	@OneToMany(mappedBy = "cert", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(NoteComparator.class)
	public Set<CertNote> getNotes() {
		return this.notes;
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity() {
		if (this.links != null) {
			if (this.links.size() > 0) {
				return this.links.iterator().next().getJobItem().getJob();
			}
		}

		return null;
	}

	@Transient
	public Integer getPrivateActiveNoteCount() {
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount() {
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount() {
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount() {
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
    }

    /**
     * @return the procTemplateKey
     */
    @Column(name = "proctemplatekey", length = 100)
    public String getProcTemplateKey() {
        return this.procTemplateKey;
    }

    @OneToOne(mappedBy = "cert")
    @JoinColumn(name = "certid")
    public CapabilityTrainingRecord getProcTrainingRec() {
        return this.procTrainingRec;
    }

    /**
     * @return the registeredBy
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "registeredby")
    public Contact getRegisteredBy() {
        return this.registeredBy;
    }

	@Override
	@Transient
	public List<Email> getSentEmails() {
		return this.sentEmails;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "signedby")
	public Contact getSignedBy() {
		return this.signedBy;
	}

	/**
	 * @return the status
	 */
	@Enumerated(EnumType.STRING)
	@Column(name = "certstatus", nullable = false)
	public CertStatusEnum getStatus() {
		return this.status;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "supplementaryforid")
	public Certificate getSupplementaryFor() {
		return this.supplementaryFor;
	}

	@Column(name = "thirdcertno", length = 30)
	public String getThirdCertNo() {
		return this.thirdCertNo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "thirddiv")
	public Subdiv getThirdDiv() {
		return this.thirdDiv;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "thirdoutcome")
	public ActionOutcome getThirdOutcome() {
		return this.thirdOutcome;
	}

	/**
	 * @return the type
	 */
	@Enumerated(EnumType.STRING)
	@Column(name = "type", nullable = false)
	public CertificateType getType() {
		return this.type;
	}

	@Override
	@Transient
	public boolean isAccountsEmail() {
		return false;
	}

	@Column(name = "certpreviewed", columnDefinition = "bit")
	public Boolean getCertPreviewed() {
		return this.certPreviewed;
	}

	@OneToMany(mappedBy = "cert", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(CertificateValidationComparator.class)
	public Set<CertificateValidation> getValidations() {
		return validations;
	}
	
	@Transient
	public CertificateValidation getValidation() {
		CertificateValidation lastValidation = null;
		for (CertificateValidation validation : this.validations) lastValidation = validation;
		return lastValidation;
	}
	
	@Transient
	public boolean validationExist(CertificateValidation validation) {
		return CollectionUtils.isNotEmpty(this.validations) && this.validations.stream().anyMatch(v -> v.getStatus().equals(validation.getStatus()) &&
			v.getDatevalidated().compareTo(validation.getDatevalidated()) == 0 &&
			v.getNote().equals(validation.getNote()));
	}
	
	@Transient
	public CertificateValidation getBeforeLastValidation() {
		CertificateValidation beforeLastValidation = null;
		CertificateValidation lastValidation = null;
		for (CertificateValidation validation : this.validations) {
			beforeLastValidation = lastValidation;
			lastValidation = validation;
		}
		return beforeLastValidation;
	}
	
	public void setValidation(CertificateValidation certificateValidation) {

		if(this.validations == null)
			{this.validations = new TreeSet<CertificateValidation>(new CertificateValidationComparator());}
		if(certificateValidation != null)
			{this.validations.add(certificateValidation);}
	}

	@Column(name = "remarks", length = 1000)
	public String getRemarks() {
		return remarks;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "intervalunit")
	public IntervalUnit getUnit() {
		return unit;
	}

	@Column(name = "calibrationverificationstatus")
	@Enumerated(EnumType.STRING)
	public CalibrationVerificationStatus getCalibrationVerificationStatus() {
		return calibrationVerificationStatus;
	}

	@Column(name = "deviation", precision = 15, scale = 5)
	public BigDecimal getDeviation() {
		return deviation;
	}

	@Column(name = "certclass")
	@Enumerated(EnumType.STRING)
	public CertificateClass getCertClass() {
		return certClass;
	}

	@Column(name = "optimization")
	public Boolean getOptimization() {
		return optimization;
	}

	@Column(name = "repair")
	public Boolean getRepair() {
		return repair;
	}

	@Column(name = "restriction")
	public Boolean getRestriction() {
		return restriction;
	}

	@Column(name = "adjustment")
	public Boolean getAdjustment() {
		return adjustment;
	}

	@Column(name = "supplementarycomments")
	public String getSupplementaryCertificateComments() {
		return supplementaryCertificateComments;
	}

	public void setSupplementaryCertificateComments(String supplementaryComments) {
		this.supplementaryCertificateComments = supplementaryComments;
	}

	public void setOptimization(Boolean optimization) {
		this.optimization = optimization;
	}

	public void setRestriction(Boolean restriction) {
		this.restriction = restriction;
	}

	public void setAdjustment(Boolean adjustment) {
		this.adjustment = adjustment;
	}

	public void setRepair(Boolean repair) {
		this.repair = repair;
	}

	public void setCertClass(CertificateClass certClass) {
		this.certClass = certClass;
	}

	public void setDeviation(BigDecimal deviation) {
		this.deviation = deviation;
	}

	public void setCalibrationVerificationStatus(CalibrationVerificationStatus calibrationVerificationStatus) {
		this.calibrationVerificationStatus = calibrationVerificationStatus;
	}

	public void setUnit(IntervalUnit unit) {
		this.unit = unit;
	}

	public void setActions(Set<CertAction> actions) {
		this.actions = actions;
	}

	public void setApprovedBy(Contact approvedBy) {
		this.approvedBy = approvedBy;
	}

	/**
	 * @param cal the cal to set
	 */
	public void setCal(Calibration cal) {
		this.cal = cal;
	}

	/**
	 * @param calDate the calDate to set
	 */
	public void setCalDate(Date calDate) {
		this.calDate = calDate;
	}

	/**
	 * @deprecated We can call only setServiceType; which will internally call setCalType().
	 * In the future the calType field will be removed, after all getCalType() refactoring done  
	 * Method created manually (not via Lombok) in order to write above deprecation comment  
	 */
	@Deprecated
	public void setCalType(CalibrationType calType) {
		this.calType = calType;
	}

	/**
	 * This method sets both serviceType and the calType field; necessary until all calls to getCalType() are removed
	 * (including JPA queries, HQL, DWR usage, JSPs, etc...)
	 */
	public void setServiceType(ServiceType serviceType) {
		this.serviceType = serviceType;
		setCalType(serviceType != null ? serviceType.getCalibrationType() : null);
	}

	/**
	 * @param certDate the certDate to set
	 */
	public void setCertDate(Date certDate) {
		this.certDate = certDate;
	}

	public void setCertid(int certid) {
		this.certid = certid;
	}

	/**
	 * @param certno the certno to set
	 */
	public void setCertno(String certno) {
		this.certno = certno;
	}

	public void setCertPreviewed(Boolean certPreviewed) {
		this.certPreviewed = certPreviewed;
	}

	@Override
	public void setDirectory(File file) {
		this.directory = file;
	}

	public void setDocTemplateKey(String docTemplateKey) {
		this.docTemplateKey = docTemplateKey;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public void setInstCertLinks(Set<InstCertLink> instCertLinks) {
		this.instCertLinks = instCertLinks;
	}

	/**
	 * @param links the links to set
	 */
	public void setLinks(Set<CertLink> links) {
		this.links = links;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(Set<CertNote> notes) {
        this.notes = notes;
    }

    /**
     * @param procTemplateKey the procTemplateKey to set
     */
    public void setProcTemplateKey(String procTemplateKey) {
        this.procTemplateKey = procTemplateKey;
    }

    public void setProcTrainingRec(CapabilityTrainingRecord procTrainingRec) {
        this.procTrainingRec = procTrainingRec;
    }

    /**
     * @param registeredBy the registeredBy to set
     */
    public void setRegisteredBy(Contact registeredBy) {
        this.registeredBy = registeredBy;
    }

    @Override
    public void setSentEmails(List<Email> emails) {
		this.sentEmails = emails;
	}

	public void setSignedBy(Contact signedBy) {
		this.signedBy = signedBy;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(CertStatusEnum status) {
		this.status = status;
	}

	public void setSupplementaryFor(Certificate supplementaryFor) {
		this.supplementaryFor = supplementaryFor;
	}

	public void setThirdCertNo(String thirdCertNo) {
		this.thirdCertNo = thirdCertNo;
	}

	public void setThirdDiv(Subdiv thirdDiv) {
		this.thirdDiv = thirdDiv;
	}

	public void setThirdOutcome(ActionOutcome thirdOutcome) {
		this.thirdOutcome = thirdOutcome;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(CertificateType type) {
		this.type = type;
	}

	public void setValidations(Set<CertificateValidation> validations) {
		this.validations = validations;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/*
	 * Moved implementation here from CertLink 2016-02-15 GB (Also consider moving
	 * to a utility method in future.)
	 */
	@Transient
	public String getCertFilePath() {
		File directory = this.getDirectory();
		String certno = this.getCertno();

		if (directory == null) {
			return null;
		}

		File file = new File(directory.getAbsolutePath().concat(File.separator).concat(certno).concat(".pdf"));

		if (!file.exists()) {
			return null;
		} else {
			return file.getAbsolutePath();
		}
	}

	@Transient
	public String getEncryptedCertFilePath() {
		String result = "";
		try {
			String certFilePath = this.getCertFilePath();
			// Encryption is only relevant / possible if directory has been initialized; 
			// otherwise silently return empty string (this prevents DWR exceptions)  
			if (certFilePath != null) {
				return EncryptionTools.encrypt(certFilePath);
			}
		} catch (Exception ex) {
			logger.error("Error encrypting filePath", ex);
		}
		return result;
	}

	/*
	 * Moved implementation here from CertLink 2016-02-15 GB (Also consider moving
	 * to a utility method in future.)
	 */
	@Transient
	public ArrayList<String> getCertFilePaths() {
		ArrayList<String> paths = new ArrayList<>();

		List<File> files = this.getCertFiles();
		if (files != null) {
			for (File f : files) {
				paths.add(f.getAbsolutePath());
			}
		}

		return paths;
	}

	/*
	 * Moved implementation here from CertLink 2016-02-15 GB (Also consider moving
	 * to a utility method in future.)
	 */
	@Transient
	public ArrayList<File> getCertFiles() {
		if (this.getDirectory() != null) {
			File directory = this.getDirectory();
			final String certno = this.getCertno();

			FileFilter fileFilter = file -> (!file.isDirectory() && file.getName().contains(certno));

			File[] filesAsArray = directory.listFiles(fileFilter);

			if (filesAsArray == null) {
				// in here if directory doesn't exist or is not a directory
				return new ArrayList<>();
			} else {

				return new ArrayList<>(Arrays.asList(filesAsArray));
			}
		} else {
			return null;
		}
	}

	/*
	 * Moved implementation here from CertLink 2016-02-15 GB (Also consider moving
	 * to a utility method in future.)
	 */
	@Transient
	public boolean getHasCertFile() {
		File directory = this.getDirectory();
		if (directory != null) {
			String fileName = this.getCertno().concat(".pdf");
			File file = new File(directory.getAbsolutePath().concat(File.separator).concat(fileName));
			return file.exists();
		}

		return false;
	}

	/*
	 * Moved implementation here from CertLink 2016-02-15 GB (Also consider moving
	 * to a utility method in future.)
	 */
	@Transient
	public boolean hasWordFile() {
		File directory = this.getDirectory();
		if (directory != null) {
			String fileName = this.getCertno().concat(".doc");
			File file = new File(directory.getAbsolutePath().concat(File.separator).concat(fileName));
			return file.exists();
		}
		return false;
	}
}