package org.trescal.cwms.core.jobs.jobitem.entity.accessory.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.jobs.jobitem.dto.ItemAccessoryDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory;

public interface ItemAccessoryService extends BaseService<ItemAccessory, Integer> {

	/**
	 * Count all accessories linked to a item of this job. Accessory free texts are
	 * counted as one accessory.
	 * 
	 * @param jobId identifier of the job
	 * @return quantity of item accessories linked to this job
	 */
	Integer countByJob(Integer jobId);

	/**
	 * Get all item accessories for a job. The accessory name is loaded in the given
	 * language.
	 * 
	 * @param jobId  identifier of the job
	 * @param locale locale to specify the language of accessory name
	 * @return dto of all item accessories of the job
	 */
	List<ItemAccessoryDTO> getAllByJob(Integer jobId, Locale locale);
}