package org.trescal.cwms.core.jobs.job.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelWithPriceDTO;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.JobExpenseItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db.JobExpenseItemService;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.InvoiceItem;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Controller
@JsonController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class JobExpenseItemController {

	private static final Logger logger = LoggerFactory.getLogger(JobExpenseItemController.class);

	@Autowired
	private CatalogPriceService catalogPriceService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private InstrumentModelService modelService;
	@Autowired
	private JobService jobService;
	@Autowired
	private JobExpenseItemService jobExpenseItemService;
	@Autowired
	private QuotationService quotationService;
	@Autowired
	private ServiceTypeService serviceTypeService;

	private ModelAndView createModel(Job job) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("job", job);
		model.put("expenseServiceTypes", serviceTypeService.getAllByDomainType(DomainType.SERVICE));
		model.put("expenseModels",
				modelService.getAll(DomainType.SERVICE, LocaleContextHolder.getLocale(), null, null));
		return new ModelAndView("trescal/core/jobs/job/expenseitems", model);
	}

	@RequestMapping(value = "/addexpenseitem.json")
	public ModelAndView addExpenseItem(@RequestParam(value = "jobId", required = true) Integer jobId,
			@RequestParam(value = "modelId", required = true) Integer modelId,
			@RequestParam(value = "date", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate date,
			@RequestParam(value = "serviceTypeId", required = true) Integer serviceTypeId,
			@RequestParam(value = "comment", required = false, defaultValue = "") String comment,
			@RequestParam(value = "clientRef", required = false, defaultValue = "") String clientRef,
			@RequestParam(value = "price", required = false) BigDecimal price,
			@RequestParam(value = "quantity", required = false, defaultValue = "1") Integer quantity,
			@RequestParam(value = "invoiceable", required = false, defaultValue = "false") Boolean invoiceable,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyKeyValue) {
		Job job = jobService.get(jobId);
		InstrumentModel model = modelService.get(modelId);
		ServiceType serviceType = serviceTypeService.get(serviceTypeId);
		Company company = companyService.get(companyKeyValue.getKey());
		Integer itemNo = job.getExpenseItems().size() + 1;
		JobExpenseItem expenseItem = new JobExpenseItem();
		expenseItem.setJob(job);
		expenseItem.setItemNo(itemNo);
		expenseItem.setModel(model);
		expenseItem.setDate(date == null ? LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()) : date);
		expenseItem.setServiceType(serviceType);
		expenseItem.setComment(comment);
		expenseItem.setClientRef(clientRef);
		if (price == null) {
			CatalogPrice catalogPrice = catalogPriceService.findSingle(model, serviceType, CostType.SERVICE, company);
			price = catalogPrice == null ? new BigDecimal(BigInteger.ZERO, 2) : catalogPrice.getFixedPrice();
		}
		expenseItem.setSinglePrice(price);
		expenseItem.setQuantity(quantity);
		expenseItem.setInvoiceable(invoiceable);
		expenseItem.setInvoiceItems(new HashSet<InvoiceItem>());
		job.getExpenseItems().add(expenseItem);
		job = jobService.merge(job);
		jobService.modifyJobStatus(job);
		return createModel(job);
	}

	@RequestMapping(value = "/deleteexpenseitem.json")
	public ModelAndView deleteExpenseItem(@RequestParam(value = "id", required = true) Integer id,
			@RequestParam(value = "jobId", required = true) Integer jobId) {
		JobExpenseItem expenseItem = jobExpenseItemService.get(id);
		try {
			jobExpenseItemService.delete(expenseItem);
			Job job = jobService.get(jobId);
			// reorganize numbers
			Integer itemNo = 1;
			for (JobExpenseItem expItem : job.getExpenseItems())
				expItem.setItemNo(itemNo++);
			job = jobService.merge(job);
			logger.info("Expense item " + id + " deleted from job " + job.getJobno());
			return createModel(job);
		} catch (DataIntegrityViolationException ex) {
			logger.info(ex.getMessage());
			throw ex;
		}
	}

	@RequestMapping(value = "/editexpenseitem.json")
	public ModelAndView editExpenseItem(@RequestParam(value = "id", required = true) Integer id,
			@RequestParam(value = "jobId", required = true) Integer jobId,
			@RequestParam(value = "date", required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate date,
			@RequestParam(value = "serviceTypeId", required = true) Integer serviceTypeId,
			@RequestParam(value = "comment", required = false, defaultValue = "") String comment,
			@RequestParam(value = "clientRef", required = false, defaultValue = "") String clientRef,
			@RequestParam(value = "price", required = false, defaultValue = "0") BigDecimal price,
			@RequestParam(value = "quantity", required = false, defaultValue = "1") Integer quantity,
			@RequestParam(value = "invoiceable", required = false, defaultValue = "false") Boolean invoiceable) {
		JobExpenseItem expenseItem = jobExpenseItemService.get(id);
		expenseItem.setDate(date == null ? LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()) : date);
		expenseItem.setServiceType(serviceTypeService.get(serviceTypeId));
		expenseItem.setComment(comment);
		expenseItem.setClientRef(clientRef);
		expenseItem.setSinglePrice(price);

		expenseItem.setQuantity(quantity);
		expenseItem.setInvoiceable(invoiceable);
		jobExpenseItemService.merge(expenseItem);
		Job job = jobService.get(jobId);
		return createModel(job);
	}

	@RequestMapping(value = "/findSelectableServicesForNewJobServices.json")
	@ResponseBody
	public List<InstrumentModelWithPriceDTO> findSelectableServicesForNewJobServices(
			@RequestParam(value = "quoteId", required = false, defaultValue = "-1") Integer quoteId,
			@RequestParam(value = "jobId", required = true) Integer jobId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyKeyValue) {
		List<ServiceType> services = serviceTypeService.getAllByDomainType(DomainType.SERVICE);
		Company company = companyService.get(companyKeyValue.getKey());
		if (quoteId == -1)
			return modelService.getAll(DomainType.SERVICE, LocaleContextHolder.getLocale(), services.get(0), company);
		else if (quoteId == 0)
			return quotationService.findServiceModelsOnLinkedQuotations(jobId);
		else
			return quotationService.findServiceModelsOnQuotation(quoteId);
	}
}