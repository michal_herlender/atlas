package org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SortComparator;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.GroupAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItemsInGroupComparator;

@Entity
@Table(name = "jobitemgroup")
public class JobItemGroup extends Auditable
{
	private Set<GroupAccessory> accessories;
	private String accessoryFreeText;
	private boolean calibrationGroup;
	private boolean deliveryGroup;
	private int id;
	private Set<JobItem> items;
	private Job job;
	
	public JobItemGroup() {
		this.calibrationGroup = true;
		this.deliveryGroup = true;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}
	
	@OneToMany(mappedBy = "group", cascade=CascadeType.ALL, orphanRemoval=true, fetch = FetchType.LAZY)
	public Set<GroupAccessory> getAccessories() {
		return this.accessories;
	}
	
	@Column(name="accessoryFreeText", length=500)
	public String getAccessoryFreeText() {
		return accessoryFreeText;
	}
	
	@OneToMany(mappedBy = "group", fetch = FetchType.LAZY)
	@SortComparator(JobItemsInGroupComparator.class)
	public Set<JobItem> getItems() {
		return this.items;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobid")
	public Job getJob() {
		return this.job;
	}
	
	@Column(name = "calibration", columnDefinition="tinyint")
	public boolean isCalibrationGroup() {
		return this.calibrationGroup;
	}
	
	@Column(name = "delivery", columnDefinition="tinyint")
	public boolean isDeliveryGroup() {
		return this.deliveryGroup;
	}
	
	public void setAccessories(Set<GroupAccessory> accessories) {
		this.accessories = accessories;
	}
	
	public void setAccessoryFreeText(String accessoryFreeText) {
		this.accessoryFreeText = accessoryFreeText;
	}
	
	public void setCalibrationGroup(boolean calibrationGroup) {
		this.calibrationGroup = calibrationGroup;
	}
	
	public void setDeliveryGroup(boolean deliveryGroup) {
		this.deliveryGroup = deliveryGroup;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setItems(Set<JobItem> items) {
		this.items = items;
	}
	
	public void setJob(Job job) {
		this.job = job;
	}
}