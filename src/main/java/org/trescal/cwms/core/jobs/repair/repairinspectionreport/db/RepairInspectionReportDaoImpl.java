package org.trescal.cwms.core.jobs.repair.repairinspectionreport.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport_;

@Repository("RepairInspectionReportDao")
public class RepairInspectionReportDaoImpl extends BaseDaoImpl<RepairInspectionReport, Integer>
		implements RepairInspectionReportDao {

	@Override
	protected Class<RepairInspectionReport> getEntity() {
		return RepairInspectionReport.class;
	}

	@Override
	public RepairInspectionReport getRirByJobItemIdAndSubdivId(Integer jobitemid, Integer subdivId) {
		return getFirstResult(cb -> {
			CriteriaQuery<RepairInspectionReport> cq = cb.createQuery(RepairInspectionReport.class);
			Root<RepairInspectionReport> root = cq.from(RepairInspectionReport.class);
			Join<RepairInspectionReport, JobItem> jobItem = root.join(RepairInspectionReport_.ji);
			Join<RepairInspectionReport, Subdiv> subdivJoin = root.join(RepairInspectionReport_.organisation.getName());
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.jobItemId), jobitemid));
			clauses.getExpressions().add(cb.equal(subdivJoin.get(Subdiv_.subdivid), subdivId));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

	@Override
	public void refrech(RepairInspectionReport rir) {
		getEntityManager().refresh(rir);
	}

	@Override
	public List<RepairInspectionReport> getAllRirByJobItemId(int jobItemId) {
		return getResultList(cb -> {
			CriteriaQuery<RepairInspectionReport> cq = cb.createQuery(RepairInspectionReport.class);
			Root<RepairInspectionReport> root = cq.from(RepairInspectionReport.class);
			Join<RepairInspectionReport, JobItem> jobItem = root.join(RepairInspectionReport_.ji);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.jobItemId), jobItemId));
			cq.where(clauses);
			return cq;
		});
	}
	
	@Override
	public RepairInspectionReport getLatestValidatedRir(int jobItemId) {
		return getFirstResult(cb -> {
			CriteriaQuery<RepairInspectionReport> cq = cb.createQuery(RepairInspectionReport.class);
			Root<RepairInspectionReport> root = cq.from(RepairInspectionReport.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(RepairInspectionReport_.ji), jobItemId));
			clauses.getExpressions().add(cb.isTrue(root.get(RepairInspectionReport_.validatedByManager)));
			clauses.getExpressions().add(cb.isTrue(root.get(RepairInspectionReport_.completedByTechnician)));
			
			cq.where(clauses);
			cq.orderBy(cb.desc(root.get(RepairInspectionReport_.validatedByManagerOn)));
			return cq;
		}).orElse(null);
	}

	@Override
	public RepairInspectionReport getValidatedRirByJobItemAndValidationDate(int jobItemId, int allocatedSubdiv,
			boolean completedByTechnician) {
		return getFirstResult(cb -> {
			CriteriaQuery<RepairInspectionReport> cq = cb.createQuery(RepairInspectionReport.class);
			Root<RepairInspectionReport> root = cq.from(RepairInspectionReport.class);
			Join<RepairInspectionReport, JobItem> jobItem = root.join(RepairInspectionReport_.ji);
			Join<RepairInspectionReport, Subdiv> subdivJoin = root.join(RepairInspectionReport_.organisation.getName());
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(jobItem.get(JobItem_.jobItemId), jobItemId));
			clauses.getExpressions().add(cb.equal(subdivJoin.get(Subdiv_.subdivid), allocatedSubdiv));
			if(completedByTechnician){
				clauses.getExpressions().add(cb.equal(root.get(RepairInspectionReport_.completedByTechnician), true));
			}
			else {
				clauses.getExpressions().add(cb.equal(root.get(RepairInspectionReport_.validatedByManager), true));
			}
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

}