package org.trescal.cwms.core.jobs.jobitem.entity.abstractjobitem;

public enum JobItemType {
	INSTRUMENT_BASED, SERVICE;
}