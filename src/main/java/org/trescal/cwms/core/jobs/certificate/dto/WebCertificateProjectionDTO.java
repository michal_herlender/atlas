package org.trescal.cwms.core.jobs.certificate.dto;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateClass;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidationStatus;
import org.trescal.cwms.core.tools.EncryptionTools;

import lombok.Getter;
import lombok.Setter;

@Data
public class WebCertificateProjectionDTO {
	
	private static final Logger logger = LoggerFactory.getLogger(WebCertificateProjectionDTO.class);
	
	private Integer certId;
	private String certno;
	private Date calDate;
	private Date certDate;
	private String thirdCertNo;
	private CertificateType type;
	private CalibrationVerificationStatus calibrationVerificationStatus;
	private CertStatusEnum certStatus;
	private BigDecimal deviation;
	private LocalDate validationDate;
	private CertificateValidationStatus validationStatus;
	private String validationNote;
	private Integer validationId;
	private CertificateClass certClass;
	private String certEncryptedAbsolutePath;
	private List<WebCertificateJobItemLinkProjectionDTO> jobitemLinkDtos;
	private List<WebCertificateInstrumentLinkProjectionDTO> instrumentLinkDtos;
	private List<String> instrumentsInstructions;
	private List<String> instrumentsAcceptanceCriteria;
	private String instrumentsDeviation;
	
	public WebCertificateProjectionDTO(Integer certId, String certno, Date calDate, Date certDate, String thirdCertNo,
			CertificateType type, CalibrationVerificationStatus calibrationVerificationStatus,
			CertStatusEnum certStatus, BigDecimal deviation, LocalDate validationDate,
			CertificateValidationStatus validationStatus, String validationNote, Integer validationId, CertificateClass certClass) {
		super();
		this.certId = certId;
		this.certno = certno;
		this.calDate = calDate;
		this.certDate = certDate;
		this.thirdCertNo = thirdCertNo;
		this.type = type;
		this.calibrationVerificationStatus = calibrationVerificationStatus;
		this.certStatus = certStatus;
		this.deviation = deviation;
		this.validationDate = validationDate;
		this.validationStatus = validationStatus;
		this.validationNote = validationNote;
		this.validationId = validationId;
		this.certClass = certClass;
	}
	
	public void setCertEncryptedFilePath(File certDirectory) {
		try {
			String certFilePath = null;
			String certno = this.getCertno();

			if (certDirectory == null) {
				certFilePath = null;
			}

			File file = new File(certDirectory.getAbsolutePath().concat(File.separator).concat(certno).concat(".pdf"));

			if (!file.exists()) {
				certFilePath = null;
			} else {
				certFilePath = file.getAbsolutePath();
			}
			// Encryption is only relevant / possible if directory has been initialized; 
			// otherwise silently return empty string (this prevents DWR exceptions)  
			if (certFilePath != null) {
				this.setCertEncryptedAbsolutePath(EncryptionTools.encrypt(certFilePath));
			}
		} catch (Exception ex) {
			logger.error("Error encrypting filePath", ex);
		}
	}
}