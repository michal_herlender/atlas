package org.trescal.cwms.core.jobs.certificate.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateClass;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.certificate.entity.validation.CertificateValidationStatus;

public class CertificateSearchWrapper {

    // to delete on next update after completing performance
    // on certificate management
    private Certificate cert;
    private Boolean createCertVal; //is the current user allowed to validate this certificate?
    private WebCertificateProjectionDTO certDto;

    public CertificateSearchWrapper(Integer certId, String certno, Date calDate, Date certDate, String thirdCertNo,
                                    CertificateType type, CalibrationVerificationStatus calibrationVerificationStatus,
                                    CertStatusEnum certStatus, BigDecimal deviation, LocalDate validationDate,
                                    CertificateValidationStatus validationStatus, String validationNote, Integer validationId, CertificateClass certClass) {
        this.certDto = new WebCertificateProjectionDTO(certId, certno, calDate, certDate, thirdCertNo, type, calibrationVerificationStatus,
                certStatus, deviation, validationDate, validationStatus, validationNote, validationId, certClass);
    }

    public CertificateSearchWrapper(Certificate cert) {
        this.cert = cert;
    }

    public CertificateSearchWrapper(Certificate cert, Date certDate) {
        this.cert = cert;
    }

    public Boolean getCreateCertVal() {
        return createCertVal;
    }

    public void setCreateCertVal(Boolean createCertVal) {
        this.createCertVal = createCertVal;
    }

    public WebCertificateProjectionDTO getCertDto() {
        return certDto;
    }

    public void setCertDto(WebCertificateProjectionDTO certDto) {
        this.certDto = certDto;
    }

    public Certificate getCert() {
        return cert;
    }

    public void setCert(Certificate cert) {
        this.cert = cert;
    }
}