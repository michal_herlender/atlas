package org.trescal.cwms.core.jobs.certificate.entity;

public enum CertificateAction
{
	ISSUED("Certificate number issued"), 
	SIGNEDANDAPPROVED("Certificate signed & approved"), 
	SIGNED("Certificate signed"), 
	APPROVED("Certificate approved"), 
	RESET("Certificate status reset to unsigned"), 
	TPUPLOAD("Third party certificate uploaded"), 
	REPLACED("Certificate Replaced"), //only for administrative cancel and replace
	DELETED("Certificate deleted");
	
	private String description;
	
	private CertificateAction(String description)
	{
		this.description = description;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}
}
