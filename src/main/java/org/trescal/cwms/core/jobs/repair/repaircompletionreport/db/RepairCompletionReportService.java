package org.trescal.cwms.core.jobs.repair.repaircompletionreport.db;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.repair.dto.RepairTimeDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairCompletionReportFormDTO;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.spring.model.KeyValue;

public interface RepairCompletionReportService extends BaseService<RepairCompletionReport, Integer> {

	RepairCompletionReport updateRcr(RepairCompletionReportFormDTO form, Integer allocatedSubdivId);

	void refresh(RepairCompletionReport rcr);

	void validate(RepairCompletionReportFormDTO form, String username, Locale locale);

	RepairCompletionReport getByJobitem(Integer jobitemid);
	
	RepairCompletionReport getValidatedRcrByJobItem(int jobItemId);
	
	File getRepairCompletionReportDirectory(String jobNo);
	
	void publishRCRCompletionDateChangedEvent(JobItem jobItem, RepairCompletionReport rcr, Date newRepairCompletionDate);
	
	void saveRepairCompletionReport(RepairCompletionReportFormDTO form, KeyValue<Integer, String> subdivDto);

	void preparRepairTime(RepairCompletionReport rcr, List<RepairTimeDTO> repairTimes, Contact contact);
}