package org.trescal.cwms.core.jobs.job.dto;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import lombok.Getter;

/**
 * Marshalling object only for RelatedBPOSet usage with DWR until it could be replaced
 * see BPOServiceImpl::getAllBPOsRelatedToJob(...)
 * see searchBPO function in ContactSearchPlugin.js  
 * provides formatted information with minimal updates to old DWR / javascript code
 * such that BPO selection at goods in could work again

 * Delete this class when contact search plugin is replaced...
 */
@Getter
public class DWRBpoDTO {
	
	private Integer poId;
	private String poNumber;
	private String comment;
	private String formattedDateFrom;
	private String formattedDateTo;
	private String currencyERSymbol;
	private String limitAmount;
	
	public DWRBpoDTO(BpoDTO bpoDto, DateTimeFormatter formatter) {
		this.poId = bpoDto.getPoId();
		this.poNumber = bpoDto.getPoNumber();
		this.comment = bpoDto.getComment();
		this.formattedDateFrom = "";
		if (bpoDto.getDurationFrom() != null) {
			LocalDate startDate = bpoDto.getDurationFrom().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			formattedDateFrom = startDate.format(formatter);
		}
		this.formattedDateTo = "";
		if (bpoDto.getDurationTo() != null) {
			LocalDate endDate = bpoDto.getDurationTo().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			formattedDateTo = endDate.format(formatter);
		}
		this.currencyERSymbol = bpoDto.getCurrencySymbol();
		this.limitAmount = bpoDto.getLimitAmount() != null ? bpoDto.getLimitAmount().toString() : ""; 
		
	}
}
