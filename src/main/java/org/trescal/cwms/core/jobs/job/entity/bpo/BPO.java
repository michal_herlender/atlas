package org.trescal.cwms.core.jobs.job.entity.bpo;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.job.entity.abstractpo.AbstractPO;
import org.trescal.cwms.core.jobs.job.entity.jobbpolink.JobBPOLink;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.system.enums.Scope;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "bpo")
public class BPO extends AbstractPO {
	private boolean active;
	private Company company;
	private Contact contact;
	private Date durationFrom;
	private Date durationTo;
	private Date expirationWarningDate;
	private boolean expirationWarningSent;
	private Set<JobItemPO> jobItemPOs;
	private Set<JobExpenseItemPO> expenseItemPOs;
	private BigDecimal limitAmount;
	private Subdiv subdiv;
	private Set<JobBPOLink> jobLinks;

	public BPO() {
		this.setActive(true);
		this.setExpirationWarningSent(false);
	}

	/**
	 * @return the company
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coid")
	public Company getCompany() {
		return this.company;
	}

	/**
	 * @return the contact
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid")
	public Contact getContact() {
		return this.contact;
	}

	/**
	 * @return the durationFrom
	 */
	@Column(name = "durationfrom", columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDurationFrom() {
		return this.durationFrom;
	}

	/**
	 * @return the durationTo
	 */
	@Column(name = "durationto", columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDurationTo() {
		return this.durationTo;
	}

	/**
	 * @return the expirationWarningDate
	 */
	@Column(name = "expirwarningdate", columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getExpirationWarningDate() {
		return this.expirationWarningDate;
	}

	/**
	 * @return the jobItemPOs
	 */
	@OneToMany(mappedBy = "bpo", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<JobItemPO> getJobItemPOs() {
		return this.jobItemPOs;
	}

	/**
	 * @return the expenseItemPOs
	 */
	@OneToMany(mappedBy = "bpo", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<JobExpenseItemPO> getExpenseItemPOs() {
		return this.expenseItemPOs;
	}

	/**
	 * @return the limitAmount
	 */
	@Column(name = "limitamount", unique = false, nullable = true, insertable = true, updatable = true, precision = 10, scale = 2)
	public BigDecimal getLimitAmount() {
		return this.limitAmount;
	}

	@Transient
	public Scope getScope() {
		assert ((company == null || subdiv == null) && (company == null || contact == null)
				&& (subdiv == null || contact == null));
		if (company != null)
			return Scope.COMPANY;
		else if (subdiv != null)
			return Scope.SUBDIV;
		else if (contact != null)
			return Scope.CONTACT;
		else
			return null;
	}

	/**
	 * @return the subdiv
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subdivid")
	public Subdiv getSubdiv() {
		return this.subdiv;
	}

	/**
	 * @return the active
	 */
	@Column(name = "active", columnDefinition = "bit")
	public boolean isActive() {
		return this.active;
	}

	@Override
	@Transient
	public boolean isBPO() {
		return true;
	}

	/**
	 * @return the expirationWarningSent
	 */
	@Column(name = "expirwarningsent", columnDefinition = "bit")
	public boolean isExpirationWarningSent() {
		return this.expirationWarningSent;
	}

	/**
	 * @return the Job links
	 */
	@OneToMany(mappedBy = "bpo", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public Set<JobBPOLink> getJobLinks() {
		return this.jobLinks;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact(Contact contact) {
		this.contact = contact;
	}

	/**
	 * @param durationFrom the durationFrom to set
	 */
	public void setDurationFrom(Date durationFrom) {
		this.durationFrom = durationFrom;
	}

	/**
	 * @param durationTo the durationTo to set
	 */
	public void setDurationTo(Date durationTo) {
		this.durationTo = durationTo;
	}

	/**
	 * @param expirationWarningDate the expirationWarningDate to set
	 */
	public void setExpirationWarningDate(Date expirationWarningDate) {
		this.expirationWarningDate = expirationWarningDate;
	}

	/**
	 * @param expirationWarningSent the expirationWarningSent to set
	 */
	public void setExpirationWarningSent(boolean expirationWarningSent) {
		this.expirationWarningSent = expirationWarningSent;
	}

	/**
	 * @param jobItemPOs the jobItemPOs to set
	 */
	public void setJobItemPOs(Set<JobItemPO> jobItemPOs) {
		this.jobItemPOs = jobItemPOs;
	}

	/**
	 */
	public void setExpenseItemPOs(Set<JobExpenseItemPO> expenseItemPOs) {
		this.expenseItemPOs = expenseItemPOs;
	}

	/**
	 * @param limitAmount the limitAmount to set
	 */
	public void setLimitAmount(BigDecimal limitAmount) {
		this.limitAmount = limitAmount;
	}

	/**
	 * @param subdiv the subdiv to set
	 */
	public void setSubdiv(Subdiv subdiv) {
		this.subdiv = subdiv;
	}

	public void setJobLinks(Set<JobBPOLink> jobLinks) {
		this.jobLinks = jobLinks;
	}

}