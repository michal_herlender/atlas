package org.trescal.cwms.core.jobs.jobitem.form;

import io.vavr.collection.Stream;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AllowInstrumentCreationWithoutPlantNo;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.AllowInstrumentCreationWithoutSerialNo;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import javax.validation.constraints.NotNull;

@Component
public class AddNewItemsToJobValidator extends AbstractBeanValidator {
    @Autowired
    private AllowInstrumentCreationWithoutPlantNo allowInstrumentCreationWithoutPlantNo;

    @Autowired
    private AllowInstrumentCreationWithoutSerialNo allowInstrumentCreationWithoutSerialNo;

    @Override
    public boolean supports(@NotNull Class<?> clazz) {
        return clazz.equals(AddNewItemsToJobForm.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        super.validate(target, errors);
        val form = (AddNewItemsToJobForm) target;

        if (!allowInstrumentCreationWithoutSerialNo.getValueByScope(Scope.COMPANY, form.getBussinessCompanyId(), form.getBussinessCompanyId())) {
            Stream.ofAll(form.getInstSerialNos().stream()).zipWithIndex().filter(t -> StringUtils.isBlank(t._1()))
                .forEach(t -> {
                    errors.rejectValue("instSerialNos[" + t._2 + "]", "error.instrument.serialno", "A serial no must be specified for the instrument");
                });
        }

        if (!allowInstrumentCreationWithoutPlantNo.getValueByScope(Scope.COMPANY, form.getBussinessCompanyId(), form.getBussinessCompanyId())) {
            Stream.ofAll(form.getInstPlantNos().stream()).zipWithIndex().filter(t -> StringUtils.isBlank(t._1()))
                .forEach(t -> {
                    errors.rejectValue("instPlantNos[" + t._2 + "]", "error.instrument.plantno", "A plant no must be specified for the instrument");
                });
        }
    }
}
