package org.trescal.cwms.core.jobs.job.entity.bpo;

import java.util.List;

import org.trescal.cwms.core.jobs.job.dto.BpoDTO;

import lombok.Data;

/**
 * Marshalling object only for RelatedBPOSet usage with DWR until it could be replaced
 * see BPOServiceImpl::getAllBPOsRelatedToJob(...)
 * see searchBPO function in ContactSearchPlugin.js  
 *
 * Delete this class when contact search plugin is replaced...
 */
@Data
public class RelatedBPOSet {
    private String companyName;
    private String subdivName;
    private String contactName;

    private List<BpoDTO> companyBPOs;
    private List<BpoDTO> subdivBPOs;
    private List<BpoDTO> contactBPOs;
    private String currencyERSymbol;
    private int size;

}
