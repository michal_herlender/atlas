package org.trescal.cwms.core.jobs.certificate.entity.instcertlink.db;

import java.util.List;

import org.trescal.cwms.core.jobs.certificate.entity.instcertlink.InstCertLink;

public interface InstCertLinkService
{
	void insertInstCertLink(InstCertLink instcertlink);
	
	Long getInstCertLinkCount(Integer plantid);
	
	/**
	 * Returns the most recent maxResults instcertlinks associated with the instrument
	 * sorted in descending order by cert date, cert number, and then inst cert link id.
	 * 
	 * Use as replacement for instrument.getCertLink() as 100s of cert links may exist
	 * for one instrument. 
	 * 
	 * @param plantid
	 * @param maxResults
	 * @return
	 */
	List<InstCertLink> searchByPlantId(Integer plantid, Integer maxResults);
}