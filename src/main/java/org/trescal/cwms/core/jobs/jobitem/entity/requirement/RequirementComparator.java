package org.trescal.cwms.core.jobs.jobitem.entity.requirement;

import java.util.Comparator;

/**
 * Inner comparator class for sorting Job related purchase orders when returning
 * collections of jobs, each with multiple POs
 * 
 * @author ross
 */
public class RequirementComparator implements Comparator<Requirement>
{
	public int compare(Requirement requirement, Requirement anotherRequirement)
	{
		if (requirement.getCreated().equals(anotherRequirement.getCreated()))
		{
			if ((requirement.getId() == null)
					|| (anotherRequirement.getId() == null))
			{
				return 1;
			}
			return requirement.getId().compareTo(anotherRequirement.getId());
		}
		else
		{
			return requirement.getCreated().compareTo(anotherRequirement.getCreated());
		}
	}
}
