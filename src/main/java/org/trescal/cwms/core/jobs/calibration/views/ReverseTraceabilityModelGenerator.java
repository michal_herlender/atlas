package org.trescal.cwms.core.jobs.calibration.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationDTO;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsedModel;
import org.trescal.cwms.core.tools.PagedResultSet;

@Component
public class ReverseTraceabilityModelGenerator {

	@Autowired
	private CalibrationService calibrationService;

	public StandardUsedModel getModel(Integer plantId, Date startCalDate, Date endCalDate, Integer allocatedCompanyId, 
			Integer allocatedSubdivId, Integer resultsTotal, Locale locale) {
		StandardUsedModel model = StandardUsedModel.builder()
				.plantId(plantId)
				.startCalDate(startCalDate)
				.endCalDate(endCalDate)
				.build();
		Collection<CalibrationDTO> calibrationDTOs = new ArrayList<CalibrationDTO>();
		if(allocatedCompanyId != null && allocatedSubdivId != null){
			calibrationDTOs = calibrationService.getCalibrationsPerformed(plantId, startCalDate,
					endCalDate, allocatedCompanyId, allocatedSubdivId, locale, new PagedResultSet<CalibrationDTO>(resultsTotal, 0)).getResults();
		}
		else 
			calibrationDTOs = calibrationService.getCalibrationsPerformed_(plantId, startCalDate, endCalDate);
		
		model.setCalibrationDTOs(calibrationDTOs);
		return model;
	}

}
