package org.trescal.cwms.core.jobs.jobitem.form;

import java.util.List;

import org.trescal.cwms.core.failurereport.dto.FailureReportDTO;


public class JIFailureReportForm extends JobItemForm {

	private List<FailureReportDTO> failurereports;

	public List<FailureReportDTO> getFailurereports() {
		return failurereports;
	}

	public void setFailurereports(List<FailureReportDTO> failurereports) {
		this.failurereports = failurereports;
	}

}
