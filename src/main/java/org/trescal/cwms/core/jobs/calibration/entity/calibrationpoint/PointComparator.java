package org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint;

import java.util.Comparator;

import org.trescal.cwms.core.jobs.calibration.entity.point.Point;

/**
 * Basic {@link Comparator} implementation for comparing
 * {@link CalibrationPoint} entities. Compares by id.
 * 
 * @author Richard
 */
public class PointComparator implements Comparator<Point>
{
	@Override
	public int compare(Point o1, Point o2)
	{
		if ((o1.getId() == 0) && (o2.getId() == 0))
		{
			// force a positive return for non-persisted hibernate entities
			// see
			// http://antyardb03:888/cwms2/wiki/development/specifications/hibernatecollections
			return 1;
		}
		else if (o1.getId() == o2.getId())
		{
			return 0;
		}
		else
		{
			return ((Integer) o1.getId()).compareTo(o2.getId());
		}
	}
}
