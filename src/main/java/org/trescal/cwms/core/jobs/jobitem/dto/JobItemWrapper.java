package org.trescal.cwms.core.jobs.jobitem.dto;

import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public class JobItemWrapper {
	
	private JobItem jobItem;
	private String modelName;
	private String calTypeName;
	
	
	public JobItemWrapper(JobItem jobItem, String modelName, String calTypeName) {
		super();
		this.jobItem = jobItem;
		this.modelName = modelName;
		this.calTypeName = calTypeName;
	}
	
	public JobItem getJobItem() {
		return jobItem;
	}
	public void setJobItem(JobItem jobItem) {
		this.jobItem = jobItem;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getCalTypeName() {
		return calTypeName;
	}

	public void setCalTypeName(String calTypeName) {
		this.calTypeName = calTypeName;
	}

}
