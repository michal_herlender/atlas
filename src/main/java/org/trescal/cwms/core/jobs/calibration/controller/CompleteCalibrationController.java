package org.trescal.cwms.core.jobs.calibration.controller;

import io.vavr.Tuple;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactDWRService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.callink.CalLink;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.jobs.calibration.form.CompleteCalibrationForm;
import org.trescal.cwms.core.jobs.calibration.form.CompleteCalibrationValidator;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.CertificateComparator;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certlink.CertLink;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.jobs.jobitem.controller.JobItemController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnote.JobItemNote;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.recall.entity.recallrule.RecallRuleIntervalDto;
import org.trescal.cwms.core.recall.entity.recallrule.db.RecallRuleService;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.DisplayUncertainties;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.FileTools;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_CreateCertificate;
import org.trescal.cwms.core.workflow.automation.antech.hookinterceptor.HookInterceptor_CreateCertificate.Input;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
@Slf4j
public class CompleteCalibrationController extends JobItemController {

	public static final String SUBMIT_ACTION_CANCEL = "cancelButton";
	public static final String SUBMIT_ACTION_COMPLETE = "completeButton";

	@Value("#{props['cwms.config.certfolder']}")
	private String certificateFolder;
    @Value("#{props['cwms.config.certificate.password.password_required']}")
    private boolean passwordRequired;
    @Autowired
    private ActionOutcomeService aoServ;
    @Autowired
    private CalibrationService calServ;
    @Autowired
    private CertificateService certServ;
    @Autowired
    private ContactDWRService conDWRServ;
    @Autowired
    private JobItemService jobItemService;
    @Autowired
    private CapabilityAuthorizationService procAccServ;
    @Autowired
    private RecallRuleService recallRuleServ;
    @Autowired
    private CompleteCalibrationValidator validator;
    @Autowired
    private SubdivService subdivService;
    // Other components
    @Autowired
    private DisplayUncertainties displayUncertainties;
    @Autowired
    private HookInterceptor_CreateCertificate hookCreateCertificate;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) {
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
	}

	@ModelAttribute("units")
	public List<KeyValue<String, String>> initializeRecallIntervalUnits() {
		return IntervalUnit.getUnitsForSelect();
	}

	@ModelAttribute("form")
	protected CompleteCalibrationForm formBackingObject(HttpServletRequest request,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@RequestParam(value = "calid", required = false, defaultValue = "0") Integer calid,
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") Integer jobitemid,
			@RequestParam(value = "openproc", required = false, defaultValue = "false") Boolean openproc)
			throws Exception {
		Calibration cal = this.calServ.findCalibration(calid);
		CompleteCalibrationForm form = new CompleteCalibrationForm();
		form.setCalId(calid);
		form.setPasswordReq(this.passwordRequired);
		if ((calid == 0) || (cal == null)) {
			log.error("No calibration found with the calid " + calid);
			throw new Exception("Calibration requested could not be found.");
		}
		JobItem ji = null;
		// if jobitem has been specified in URL, get that item
		if (jobitemid != 0) {
			ji = jobItemService.findJobItem(jobitemid);
		}
		// otherwise just take the first item
		if (ji == null)
			ji = cal.getLinks().stream().findFirst().map(CalLink::getJi).orElse(null);
		form.setJi(ji);
		form.setCertAllowed(ji != null && ji.getNextWorkReq() != null && ji.getNextWorkReq().getWorkRequirement().getServiceType().getCalibrationType()
			.getRecallRequirementType().equals(RecallRequirementType.FULL_CALIBRATION));
		Contact loggedIn = this.userService.get(username).getCon();
		// set the different note types into the session
		HttpSession sess = request.getSession();
		sess.setAttribute("jobitemnote", JobItemNote.class);
		sess.setAttribute("completecalibrationcomment", PresetCommentType.COMPLETE_CALIBRATION);
		if (!cal.getStatus().getName().equals("On-going")) {
            log.error("Calibration with calid " + calid + " is no longer On-going.");
            throw new Exception("Calibration is already completed and locked.");
        }
        // only open the CWMS procedure window if CWMS calibration
        if (cal.getCalProcess().getApplicationComponent()) {
            form.setOpenProc(openproc);
        } else {
            form.setOpenProc(false);
        }
        val accStat = this.procAccServ.authorizedFor(cal.getCapability().getId(),
            cal.getCalType().getCalTypeId(), loggedIn.getPersonid());
        val isAuthorizedAndCanSign = accStat.map(l -> Tuple.of(true, l.equals(AccreditationLevel.APPROVE) || l.equals(AccreditationLevel.SIGN)))
            .getOrElse(Tuple.of(false, false));

        form.setAccreditedToSign(isAuthorizedAndCanSign._2);
        form.setAccredited(isAuthorizedAndCanSign._1);
        // If there are no certificates already linked to the calibration, issue
        // new cert by default
        form.setIssueNow(form.getCertAllowed() && cal.getCerts().isEmpty());
        form.setSignNow(false);
        RecallRuleIntervalDto recallRuleInterval = ji != null ? this.recallRuleServ.getCurrentRecallIntervalDto(ji.getInst(),
            ji.getServiceType().getCalibrationType().getRecallRequirementType()) : new RecallRuleIntervalDto();
        form.setDuration(recallRuleInterval.getInterval());
        form.setIntervalUnitId(recallRuleInterval.getUnitId());
        boolean pwExists = loggedIn.getEncryptedPassword() != null && !loggedIn.getEncryptedPassword().equals("");
        form.setPwExists(pwExists);
		// set up a linked outcome for each cal link so that the outcomes can be
		// selected and bound back on the page and select all items to be
		// included on the cert by default, and add default time for each item
		Map<Integer, Integer> linkedOutcomes = new TreeMap<>();
		Map<Integer, Integer> timeSpents = new HashMap<>();
		List<Integer> includeOnCert = new ArrayList<>();
		// use the same method to calculate this as the ajax call on the page
		Integer time = this.calServ.getMinutesFromCalibrationStart(calid);
		for (CalLink cl : cal.getLinks()) {
			linkedOutcomes.put(cl.getId(), null);
			timeSpents.put(cl.getId(), time);
			includeOnCert.add(cl.getId());
		}
		form.setLinkedOutcomes(linkedOutcomes);
		form.setTimeSpents(timeSpents);
		form.setIncludeOnCert(includeOnCert);
		form.setCalDate(cal.getCalDate());
		// Use existing cert (a) if already connected to calibration or 
		// (b) if reserved certificate exists (use the oldest / first one found)
		Certificate reservedCertificate = this.certServ.getReservedCertificate(ji);
		if (!cal.getCerts().isEmpty()) {
			form.setExistingCertId(cal.getCerts().get(0).getCertid());
			form.setAddToExistingCert(true);
			form.setIssueNow(false);
		}
		else if (reservedCertificate != null) {
			form.setExistingCertId(reservedCertificate.getCertid());
			form.setAddToExistingCert(true);
			form.setIssueNow(false);
		}
		return form;
	}

	/*
	 * HttpServletRequest just used for password encryption features for DWR
	 * (evaluate removal?)
	 */
	@RequestMapping(value = "/completecalibration.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(@RequestParam(value = "jobitemid") Integer jobItemId,
									@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
									@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
									@ModelAttribute("form") CompleteCalibrationForm form, BindingResult bindingResult,
									HttpServletRequest request) throws Exception {
		Contact contact = userService.get(username).getCon();
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Calibration cal = calServ.findCalibration(form.getCalId());
		if (form.isAccredited())
			form.setConCompletingCal(contact);
		validator.validate(form, bindingResult);
		if (bindingResult.hasErrors())
			return referenceData(jobItemId, username, subdivDto, form);
		IntervalUnit intervalUnit = IntervalUnit.valueOf(form.getIntervalUnitId());
		RecallRequirementType requirementType = cal.getCalType().getRecallRequirementType();
		cal.setCalClass(form.getCalClass());
		cal.setCalDate(form.getCalDate());
		if (form.getSubmitAction().equals(SUBMIT_ACTION_CANCEL)) {
			this.calServ.cancelCalibration(cal.getId(), contact);
			return new ModelAndView(new RedirectView("jicalibration.htm?jobitemid=" + form.getJi().getJobItemId()));
		} else if (form.getSubmitAction().equals(SUBMIT_ACTION_COMPLETE)) {
			boolean failedCal = true;
			boolean onHold = false;
			for (Integer calLinkId : form.getLinkedOutcomes().keySet()) {
				ActionOutcome ao = this.aoServ.get(form.getLinkedOutcomes().get(calLinkId));
				if (ao.getGenericValue().getDescription().contains("on hold")) {
					onHold = true;
					failedCal = false;
					break;
				} else if (ao.isPositiveOutcome()) {
					failedCal = false;
					break;
				}
			}
			// IF ALL ITEMS FAILED TO CALIBRATE
			if (failedCal) {
				this.calServ.completeCalibrationAsUTCMultipleOutcomes(cal.getId(), contact.getPersonid(),
						form.getLinkedOutcomes(), form.getTimeSpents(), form.getRemark());
			}
			// IF ON HOLD BUTTON PRESSED
			else if (onHold) {
				this.calServ.placeCalibrationOnHold(cal, contact, form.getTimeSpents(), form.getLinkedOutcomes(),
						form.getRemark());
			}
			// IF AT LEAST ONE ITEM SUCCESSFULLY CALIBRATED
			else {
				// system component calibration?
				if (cal.getCalProcess().getApplicationComponent()) {
					// set template to be used
					cal.setCertTemplate(form.getTemplate());
				}
				// Ensure that any connected or new certs are marked as issued 
				boolean certIssued = !cal.getCerts().isEmpty() || form.getIssueNow();
				// complete calibration
				this.calServ.completeCalibration(cal, contact, form.getTimeSpents(), form.getLinkedOutcomes(),
					form.getRemark(), certIssued, form.getIncludeOnCert(), form.getDuration(),
					intervalUnit);
			}
			// if new cert is being issued
			if ((form.getIssueNow() != null) && (form.getIssueNow())
				&& requirementType.equals(RecallRequirementType.FULL_CALIBRATION)) {
				// if a new password has been set, update the contact
				if (form.getSignNow() && !form.isPwExists()) {
					this.conDWRServ.setEncryptedPassword(contact.getPersonid(), form.getEncPassword(), request);
				}
				// create certificate
				Certificate cert = this.certServ.createCertFromCal(contact, cal, form.getDuration(), form.getSignNow(),
					form.getIncludeOnCert(), form.isCertPreviewed(), allocatedSubdiv, intervalUnit,
					CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE, null);
				if (form.getIncludeRemarksOnCert()) {
					cert.setRemarks(form.getRemark());
					this.certServ.updateCertificate(cert);
				}

				// if CWMS
				if (cal.getCalProcess().getApplicationComponent()) {
					// set procedure as complete in xindice db
					this.calServ.setProcedureComplete(cal.getXindiceKey());
					// store selected cert template for later
					cert.setDocTemplateKey(form.getTemplate());
					this.certServ.updateCertificate(cert);
				}
				// sign and generate cert if user has specified to
				if (form.getSignNow()) {
					// if a file has been uploaded then copy it into
					// the certificate directory
					MultipartFile file = form.getFile();
					if (file != null) {
						// reload cert to set directory
						cert = this.certServ.findCertificate(cert.getCertid());
						String name = file.getOriginalFilename();
						if (!name.equals("")) {
							// get name without extension
							String nameWithoutExt;
							// get extension
							String ext = name.substring(name.lastIndexOf("."));
							// rename file to cert no
							nameWithoutExt = cert.getCertno();
							if (name.lastIndexOf('\\') > -1) {
								name = name.substring(name.lastIndexOf('\\'));
								nameWithoutExt = name.substring(0, name.lastIndexOf("."));
							}
							file.transferTo(new File(cert.getDirectory().getAbsolutePath().concat(File.separator)
								.concat(nameWithoutExt).concat(ext)));
							log.debug("file renamed from '" + name + "' to '" + nameWithoutExt + ext + "'");
						}
					}
				}
				return new ModelAndView(
					new RedirectView("jicertificates.htm?jobitemid=" + form.getJi().getJobItemId()));
			}
			// else if item is being added to already existing cert for its
			// group
			else if ((form.getAddToExistingCert() != null) && (form.getAddToExistingCert())
				&& requirementType.equals(RecallRequirementType.FULL_CALIBRATION)) {
				// create and persist new cert link for any required items
				Certificate certificate = this.certServ.findCertificate(form.getExistingCertId());
				this.certServ.createLinksForCert(certificate, cal, form.getIncludeOnCert(), null, null);
				// update certificate cal date (may have changed from user
				// input)
				certificate.setCalDate(DateTools.dateFromLocalDate(cal.getCalDate()));
				// Ensure cal links updated (not done yet if certificate reserved) 
				this.certServ.linkCertToCal(certificate, cal);
				// The original creator (from contract review) is updated
				certificate.setRegisteredBy(contact);
				// We treat the action of linking the reserved cert to cal
				// as issuing it (workflow)
				for (CertLink cl : certificate.getLinks()) {
					this.hookCreateCertificate.recordAction(
						new Input(cl, HookInterceptor_CreateCertificate.CERTIFICATE_DEFAULT_TIME_SPENT));
				}
				if (form.getSignNow() || !cal.getCalProcess().getSigningSupported()) {
					this.certServ.signCertificate(certificate, contact);
				} else {
					certificate.setStatus(CertStatusEnum.UNSIGNED_AWAITING_SIGNATURE);
				}
				this.certServ.updateCertificate(certificate);
				// if CWMS, set procedure as complete in xindice db
				if (cal.getCalProcess().getApplicationComponent()) {
					this.calServ.setProcedureComplete(cal.getXindiceKey());
				}
				return new ModelAndView(
						new RedirectView("jicertificates.htm?jobitemid=" + form.getJi().getJobItemId()));
			} else {
				return new ModelAndView(new RedirectView("jicalibration.htm?jobitemid=" + form.getJi().getJobItemId()));
			}
		} else {
			log.error("The value of the submit button pressed on the form was not recognised.");
			throw new Exception("The button pressed was not recognised.");
		}
	}

	@RequestMapping(value = "/completecalibration.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@RequestParam(value = "jobitemid") Integer jobItemId,
										 @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
										 @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
										 @ModelAttribute("form") CompleteCalibrationForm form) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Map<String, Object> map = super.referenceData(jobItemId, contact, subdivDto.getKey(),username);
		Calibration cal = calServ.findCalibration(form.getCalId());
		map.put("cal", cal);
		if (cal.getCalProcess().getApplicationComponent()) {
			File dvslDir = new File(this.certificateFolder);
			File[] templatesF = dvslDir.listFiles();
			Vector<String> templates = new Vector<>();
			if (templatesF != null) {
				for (File f : templatesF) {
					templates.add(f.getName());
				}
			}
			map.put("certtemplates", templates);
		}
		JobItem ji = this.jobItemService.findJobItem(form.getJi().getJobItemId());
		map.put("jobfiles",
			FileTools.getFileListForDir(
				ji.getJob().getDirectory().getAbsolutePath().concat(File.separator).concat("Certificates"),
				Collections.singletonList(".doc")));
		// JobItem state should always be Pre-calibration or Pre-onsite
		// calibration, so we can determine outcomes from job item.
		if (ji.getState().isStatus())
			throw new RuntimeException("Job item in unexpected status, does not have calibration activity open");
		ItemActivity activity = (ItemActivity) ji.getState();
		map.put("actionoutcomes", activity.getActionOutcomes());
		KeyValue<Scope, String> displayUncertaintiesValue = displayUncertainties.getValueHierarchical(Scope.CONTACT,
			ji.getJob().getCon(), allocatedSubdiv.getComp());
		map.put("displayUncertainties", displayUncertainties.parseValue(displayUncertaintiesValue.getValue()));
		Set<Certificate> certsForGroupItems = new TreeSet<>(new CertificateComparator());
		// if item is in a group
		if (ji.getGroup() != null) {
			// build list of certs to offer attaching item on to
			for (JobItem item : ji.getGroup().getItems()) {
				for (CertLink cl : item.getCertLinks()) {
					certsForGroupItems.add(cl.getCert());
				}
			}
		}
		map.put("certsForGroupItems", certsForGroupItems);
		map.put("classes", CalibrationClass.values());
		return new ModelAndView("trescal/core/jobs/calibration/completecalibration", map);
	}
}