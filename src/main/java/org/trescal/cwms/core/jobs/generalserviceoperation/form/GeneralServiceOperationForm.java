package org.trescal.cwms.core.jobs.generalserviceoperation.form;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class GeneralServiceOperationForm {
    private JobItem ji;
    private String actionPerformed;
    private String submitAction;
    private GeneralServiceOperation gso;
    private MultipartFile file;
    private GsoDocument gsoDocument;
    private Integer documentId;
    private String documentNumber;
    private LocalDateTime documentDate;
    private LocalDateTime startedOn;
    private String startedByName;
    private LocalDateTime completedOn;
    private String completedByName;
    private Map<Integer, Integer> linkedOutcomes;
    private Map<Integer, Integer> linkedTimesSpent;
    private String actionOutcomeName;
    private String remark;
    private List<Integer> otherJobItemsId;
    private Boolean clientDecisionNeeded;
    private Integer duration;
    private String intervalUnitId;

}