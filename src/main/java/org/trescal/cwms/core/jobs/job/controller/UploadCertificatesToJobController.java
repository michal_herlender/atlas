package org.trescal.cwms.core.jobs.job.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
@JsonController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class UploadCertificatesToJobController {

	@Autowired
	private CertificateService certificateService;
	@Autowired
	private UserService userService;

	@ResponseBody
	@RequestMapping(value = "/uploadcert.json", method = {RequestMethod.GET, RequestMethod.POST},
			consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	protected RestResultDTO<String> uploadCert(HttpServletRequest request,
			@RequestParam(value = "files", required = true) List<MultipartFile> multipartFiles,
			@RequestParam(value = "jobid", required = true) Integer jobid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Locale locale) throws Exception {

		return this.certificateService.uploadOnSiteCertificate(jobid, multipartFiles, locale,
				this.userService.get(username).getCon());

	}
}
