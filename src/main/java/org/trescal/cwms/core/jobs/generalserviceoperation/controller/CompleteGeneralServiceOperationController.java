package org.trescal.cwms.core.jobs.generalserviceoperation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db.GeneralServiceOperationService;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsolink.GsoLink;
import org.trescal.cwms.core.jobs.generalserviceoperation.form.GeneralServiceOperationForm;
import org.trescal.cwms.core.jobs.generalserviceoperation.form.validator.GeneralServiceOperationValidator;
import org.trescal.cwms.core.jobs.jobitem.controller.JobItemController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.misc.tool.CustomLocalDateTimeEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.recall.entity.recallrule.RecallRuleIntervalDto;
import org.trescal.cwms.core.recall.entity.recallrule.db.RecallRuleService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class CompleteGeneralServiceOperationController extends JobItemController {
	private static final Logger logger = LoggerFactory.getLogger(
			org.trescal.cwms.core.jobs.generalserviceoperation.controller.CompleteGeneralServiceOperationController.class);
	public static final String ACTION_PERFORMED = "COMPLETE";
	private final String FORM_NAME = "form";

	@Autowired
	private GeneralServiceOperationService gsoServ;
	@Autowired
	private ActionOutcomeService aoService;
	@Autowired
	private GeneralServiceOperationValidator validator;
	@Autowired
	private RecallRuleService recallRuleServ;

	@InitBinder(FORM_NAME)
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(LocalDateTime.class, new CustomLocalDateTimeEditor(DateTimeFormatter.ISO_DATE_TIME));
		binder.registerCustomEditor(Date.class, new MyCustomDateEditor(DateTools.dtf_ISO8601, true));
		binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
		binder.setValidator(validator);
	}
	
	@ModelAttribute("units")
	public List<KeyValue<String, String>> initializeRecallIntervalUnits() {
		return IntervalUnit.getUnitsForSelect();
	}

	@ModelAttribute(FORM_NAME)
	protected GeneralServiceOperationForm formBackingObject(
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") int jobitemid,
			@RequestParam(value = "gsoid", required = false) Integer gsoId) throws Exception {
		JobItem ji = this.jobItemService.findJobItem(jobitemid);
		if (ji == null || jobitemid == 0) {
			logger.error("Jobitem with id " + jobitemid + " could not be found");
			throw new Exception("Unable to locate requested jobitem");
		}
		GeneralServiceOperationForm form = new GeneralServiceOperationForm();
		GeneralServiceOperation gso = gsoId == null ? this.gsoServ.getOnGoingByJobItemId(jobitemid)
				: this.gsoServ.get(gsoId);
		if (gso != null) {
			form.setStartedOn(DateTools.dateToLocalDateTime(gso.getStartedOn()));
			form.setStartedByName(gso.getStartedBy().getName());
			form.setGso(gso);
			form.setActionPerformed(ACTION_PERFORMED);
			form.setLinkedOutcomes(new HashMap<>());
			form.setLinkedTimesSpent(new HashMap<>());
			for (GsoLink link : gso.getLinks()) {
				form.getLinkedOutcomes().put(link.getId(), null);
				form.getLinkedTimesSpent().put(link.getId(), null);
			}
		}
		form.setJi(ji);
		//set current instrument recall interval 
		RecallRuleIntervalDto recallRuleInterval = this.recallRuleServ.getCurrentRecallIntervalDto(ji.getInst(),
			ji.getServiceType().getCalibrationType().getRecallRequirementType());
		form.setDuration(recallRuleInterval.getInterval());
		form.setIntervalUnitId(recallRuleInterval.getUnitId());

		return form;
	}

	@RequestMapping(value = {"/completegso.htm"}, method = {RequestMethod.GET})
	protected ModelAndView completeGSOGet(@RequestParam(value = "jobitemid") Integer jobItemId,
										  @RequestParam(value = "gsoid", required = false) Integer gsoId, @ModelAttribute("username") String username,
										  @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
										  @ModelAttribute("form") GeneralServiceOperationForm form) throws Exception {
		Contact contact = this.userService.get(username).getCon();
		Map<String, Object> map = referenceData(jobItemId, contact, subdivDto.getKey(),username);
		if (form.getJi() != null) {

			List<ActionOutcome> outcomes =
				form.getJi().getState() instanceof ItemActivity ?
					aoService
						.getActionOutcomesForActivity((ItemActivity) form.getJi().getState())
					: Collections.emptyList();
			map.put("outcomes", outcomes);
		}

		return new ModelAndView("trescal/core/jobs/generalserviceoperation/completegeneralserviceoperation", map);
	}

	@RequestMapping(value = {"/completegso.htm"}, method = {RequestMethod.POST})
	protected ModelAndView completeGSOPost(@RequestParam(value = "jobitemid") Integer jobItemId,
										   @ModelAttribute("username") String username,
										   @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
										   @ModelAttribute("form") GeneralServiceOperationForm form, BindingResult bindingResult, RedirectAttributes redirectAttributes)
		throws Exception {
		Contact contact = this.userService.get(username).getCon();

		this.validator.validate(form, bindingResult, contact);
		if (bindingResult.hasErrors())
			this.referenceData(jobItemId, contact, subdivDto.getKey(), username);

		if (form.getSubmitAction().equals("complete")) {
			if (bindingResult.hasErrors())
				return completeGSOGet(jobItemId, form.getGso().getId(), username, subdivDto, form);
			this.gsoServ.completeGeneralServiceOperation(form, contact);
		} else {
			this.gsoServ.cancelGeneralServiceOperation(form, contact);
		}

		redirectAttributes.addAttribute("jobitemid", form.getJi().getJobItemId());
		return new ModelAndView(new RedirectView("jiactions.htm"));
	}

}
