package org.trescal.cwms.core.jobs.job.entity.jobstatus.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.job.entity.jobstatus.JobStatus;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface JobStatusDao extends BaseDao<JobStatus, Integer> {
	
	JobStatus findCompleteStatus();
	
	JobStatus findReadyToInvoiceStatus();
	
	List<KeyValueIntegerString> getKeyValueList(Collection<Integer> jobStatusIds, Locale locale);
}