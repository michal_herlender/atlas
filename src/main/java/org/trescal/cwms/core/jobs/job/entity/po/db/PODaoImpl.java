package org.trescal.cwms.core.jobs.job.entity.po.db;

import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import lombok.val;
import org.hibernate.criterion.MatchMode;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.po.PO_;
import org.trescal.cwms.core.jobs.job.projection.POProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitempo.JobExpenseItemPO_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO_;
import org.trescal.cwms.core.logistics.dto.PoDTO;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails_;
import org.trescal.cwms.core.jobs.job.dto.ClientPurchaseOrderDTO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.tools.StringJpaUtils;

@Repository("PODao")
public class PODaoImpl extends BaseDaoImpl<PO, Integer> implements PODao {
	@Override
	protected Class<PO> getEntity() {
		return PO.class;
	}

	@Override
	public List<PoDTO> getPOsByPrebooking(Integer prebookingdetailsid) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<PoDTO> query = builder.createQuery(PoDTO.class);
		Root<PO> poRoot = query.from(PO.class);
		Join<PO, PrebookingJobDetails> prebookingJobDetailsJoin = poRoot.join(PO_.prebooking.getName());
		Predicate conjunction = builder.conjunction();
		conjunction.getExpressions().add(
				builder.equal(prebookingJobDetailsJoin.get(PrebookingJobDetails_.id.getName()), prebookingdetailsid));
		query.where(conjunction);
		// get results
		query.select(builder.construct(PoDTO.class, poRoot.get(PO_.poId.getName()), poRoot.get(PO_.poNumber.getName()),
				poRoot.get(PO_.comment.getName())));
		return getEntityManager().createQuery(query).getResultList();
	}

	@Override
	public List<ClientPurchaseOrderDTO> getProjectionByJob(Integer jobId) {
		return getResultList(cb -> {
			CriteriaQuery<ClientPurchaseOrderDTO> cq = cb.createQuery(ClientPurchaseOrderDTO.class);
			Root<Job> job = cq.from(Job.class);
			Join<Job, PO> po = job.join(Job_.POs);
			Join<Job, PO> defaultPO = job.join(Job_.defaultPO, JoinType.LEFT);
			Join<PO, Contact> deactivatedBy = po.join(PO_.deactivatedBy, JoinType.LEFT);
			Subquery<Long> jobItemsSq = cq.subquery(Long.class);
			Root<JobItemPO> jobItems = jobItemsSq.from(JobItemPO.class);
			jobItemsSq.where(cb.equal(jobItems.get(JobItemPO_.po), po));
			jobItemsSq.select(cb.count(jobItems));
			Subquery<Long> expenseItemsSq = cq.subquery(Long.class);
			Root<JobExpenseItemPO> expenseItems = expenseItemsSq.from(JobExpenseItemPO.class);
			expenseItemsSq.where(cb.equal(expenseItems.get(JobExpenseItemPO_.po), po));
			expenseItemsSq.select(cb.count(expenseItems));
			cq.where(cb.equal(job.get(Job_.jobid), jobId));
			cq.select(cb.construct(ClientPurchaseOrderDTO.class, cb.literal(jobId), po.get(PO_.poId),
					po.get(PO_.poNumber), po.get(PO_.active), defaultPO.get(PO_.poId), po.get(PO_.comment),
					jobItemsSq.getSelection(), expenseItemsSq.getSelection(), deactivatedBy.get(Contact_.firstName),
					deactivatedBy.get(Contact_.lastName), po.get(PO_.deactivatedTime)));
			return cq;
		});
	}

	@Override
	public List<POProjectionDTO> getPOsByJobIds(Collection<Integer> jobIds) {
		return getResultList(cb -> {
			CriteriaQuery<POProjectionDTO> cq = cb.createQuery(POProjectionDTO.class);
			Root<PO> poRoot = cq.from(PO.class);
			Join<PO, Job> jobJoin = poRoot.join(PO_.job);
			cq.where(jobJoin.get(Job_.jobid).in(jobIds));
			cq.select(cb.construct(POProjectionDTO.class, jobJoin.get(Job_.jobid), poRoot.get(PO_.poId),
					poRoot.get(PO_.poNumber)));
			cq.distinct(true);
			return cq;
		});
	}

	@Override
	public List<PO> searchMatchingJobPOs(int jobId, String partPoNumber) {
		return getResultList(cb -> {
			val cq = cb.createQuery(PO.class);
			val po = cq.from(PO.class);
			cq.where(cb.and(StringJpaUtils.ilike(cb, po.get(PO_.poNumber), partPoNumber, MatchMode.START),
					cb.equal(po.get(PO_.job), jobId)));
			cq.orderBy(cb.asc(po.get(PO_.poNumber)));
			return cq;
		});
	}

	@Override
	public PO getMatchingJobPO(int jobId, String poNumber) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(PO.class);
			val po = cq.from(PO.class);
			cq.where(cb.equal(po.join(PO_.job), jobId));
			return cq;
		}).orElse(null);
	}
}