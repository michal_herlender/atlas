package org.trescal.cwms.core.jobs.calibration.dto;

import java.util.List;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;

public class CalibrationToBatch
{
	private Boolean accredited;
	private String certTemplate;
	private boolean cwmsProcess;
	private JobItem ji;
	private Integer procId;
	private List<Integer> standardIds;
	private List<Instrument> standards;
	private boolean toBatch;
	private Integer workInstructionId;

	public Boolean getAccredited()
	{
		return this.accredited;
	}

	public String getCertTemplate()
	{
		return this.certTemplate;
	}

	public JobItem getJi()
	{
		return this.ji;
	}

	public Integer getProcId()
	{
		return this.procId;
	}

	public List<Integer> getStandardIds()
	{
		return this.standardIds;
	}

	public List<Instrument> getStandards()
	{
		return this.standards;
	}

	public Integer getWorkInstructionId()
	{
		return this.workInstructionId;
	}

	public boolean isCwmsProcess()
	{
		return this.cwmsProcess;
	}

	public boolean isToBatch()
	{
		return this.toBatch;
	}

	public void setAccredited(Boolean accredited)
	{
		this.accredited = accredited;
	}

	public void setCertTemplate(String certTemplate)
	{
		this.certTemplate = certTemplate;
	}

	public void setCwmsProcess(boolean cwmsProcess)
	{
		this.cwmsProcess = cwmsProcess;
	}

	public void setJi(JobItem ji)
	{
		this.ji = ji;
	}

	public void setProcId(Integer procId)
	{
		this.procId = procId;
	}

	public void setStandardIds(List<Integer> standardIds)
	{
		this.standardIds = standardIds;
	}

	public void setStandards(List<Instrument> standards)
	{
		this.standards = standards;
	}

	public void setToBatch(boolean toBatch)
	{
		this.toBatch = toBatch;
	}

	public void setWorkInstructionId(Integer workInstructionId)
	{
		this.workInstructionId = workInstructionId;
	}
}