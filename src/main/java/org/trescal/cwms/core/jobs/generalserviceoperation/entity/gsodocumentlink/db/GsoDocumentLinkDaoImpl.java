package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocumentlink.GsoDocumentLink;

@Repository("GsoDocumentLinkDao")
public class GsoDocumentLinkDaoImpl extends BaseDaoImpl<GsoDocumentLink, Integer> implements GsoDocumentLinkDao {
protected Class<GsoDocumentLink> getEntity() {
     return GsoDocumentLink.class;
     }
 }
