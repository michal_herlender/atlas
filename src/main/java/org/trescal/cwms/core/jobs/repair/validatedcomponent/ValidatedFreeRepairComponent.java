package org.trescal.cwms.core.jobs.repair.validatedcomponent;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.jobs.repair.component.FreeRepairComponent;
import org.trescal.cwms.core.jobs.repair.component.enums.RepairComponentStateEnum;
import org.trescal.cwms.core.jobs.repair.validatedoperation.ValidatedFreeRepairOperation;

@Entity
@Table(name = "validatedfreerepaircomponent")
public class ValidatedFreeRepairComponent extends Auditable {

	private int validatedComponentId;
	private Integer quantity;
	private BigDecimal cost;
	private RepairComponentStateEnum validatedStatus;
	private FreeRepairComponent freeRepairComponent;
	private ValidatedFreeRepairOperation validatedFreeRepairOperation;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getValidatedComponentId() {
		return validatedComponentId;
	}

	public void setValidatedComponentId(int validatedComponentId) {
		this.validatedComponentId = validatedComponentId;
	}

	@Column(name = "quantity", nullable = false)
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name = "cost")
	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	
	@Column(name = "validatedstatus", nullable = true)
	@Enumerated(EnumType.STRING)
	public RepairComponentStateEnum getValidatedStatus() {
		return validatedStatus;
	}

	public void setValidatedStatus(RepairComponentStateEnum validatedStatus) {
		this.validatedStatus = validatedStatus;
	}

	@OneToOne
	@JoinColumn(name = "freerepaircomponentid", foreignKey = @ForeignKey(name = "validatedfreerepaircomponent_freerepaircomponent_FK"))
	public FreeRepairComponent getFreeRepairComponent() {
		return freeRepairComponent;
	}

	public void setFreeRepairComponent(FreeRepairComponent freeRepairComponent) {
		this.freeRepairComponent = freeRepairComponent;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "validatedfreerepairoperationid", foreignKey = @ForeignKey(name = "validatedfreerepaircomponent_validatedfreerepairoperation_FK"))
	public ValidatedFreeRepairOperation getValidatedFreeRepairOperation() {
		return validatedFreeRepairOperation;
	}

	public void setValidatedFreeRepairOperation(ValidatedFreeRepairOperation validatedFreeRepairOperation) {
		this.validatedFreeRepairOperation = validatedFreeRepairOperation;
	}
}