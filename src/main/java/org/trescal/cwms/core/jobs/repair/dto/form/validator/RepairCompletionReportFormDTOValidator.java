package org.trescal.cwms.core.jobs.repair.dto.form.validator;

import java.lang.reflect.Method;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.trescal.cwms.core.jobs.repair.dto.FreeRepairComponentDTO;
import org.trescal.cwms.core.jobs.repair.dto.FreeRepairOperationDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairCompletionReportFormDTO;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class RepairCompletionReportFormDTOValidator extends AbstractBeanValidator {

	@Autowired
	private FreeRepairOperationDTOValidator operationValidator;
	@Autowired
	private FreeRepairComponentDTOValidator componentValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(RepairCompletionReportFormDTO.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		RepairCompletionReportFormDTO rcrForm = (RepairCompletionReportFormDTO) target;
		super.validate(rcrForm, errors);

		/* General validation */
		if (rcrForm.getAdjustmentPerformed() == null)
			errors.rejectValue("adjustmentPerformed", "repairinspection.validator.mandatory", "Mandatory field");
		
		// remove deleted ops from list, and validate
		int i = 0;
		if (rcrForm.getRcrOperationsForm() != null && !rcrForm.getRcrOperationsForm().isEmpty())
			for (Iterator<FreeRepairOperationDTO> iterator = rcrForm.getRcrOperationsForm().iterator(); iterator.hasNext();) {
				FreeRepairOperationDTO op = (FreeRepairOperationDTO) iterator.next();
				if (allFieldsAreNull(op))
					iterator.remove();
				else {
					try {
						errors.pushNestedPath("rcrOperationsForm[" + (i++) + "]");
						ValidationUtils.invokeValidator(this.operationValidator, op, errors);

					} finally {
						errors.popNestedPath();
					}
				}
			}

		// remove deleted components from list, and validate
		i = 0;
		if (rcrForm.getRcrComponentsForm() != null && !rcrForm.getRcrComponentsForm().isEmpty())
			for (Iterator<FreeRepairComponentDTO> iterator = rcrForm.getRcrComponentsForm().iterator(); iterator.hasNext();) {
				FreeRepairComponentDTO comp = (FreeRepairComponentDTO) iterator.next();
				if (allFieldsAreNull(comp))
					iterator.remove();
				else {
					try {
						errors.pushNestedPath("rcrComponentsForm[" + (i++) + "]");
						ValidationUtils.invokeValidator(this.componentValidator, comp, errors);

					} finally {
						errors.popNestedPath();
					}
				}
			}

	}

	/**
	 * checks if all the getters return null
	 */
	public boolean allFieldsAreNull(Object o) {
		try {
			for (Method m : o.getClass().getDeclaredMethods())
				if (m.getName().startsWith("get") && m.invoke(o) != null)
					return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

}
