package org.trescal.cwms.core.jobs.certificate.entity.certaction;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateAction;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;

@Entity
@Table(name = "certaction")
public class CertAction
{
	private CertificateAction action;
	private Contact actionBy;
	private Date actionOn;
	private Certificate cert;
	private int id;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "action", nullable = false)
	public CertificateAction getAction()
	{
		return this.action;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "actionbyid")
	public Contact getActionBy()
	{
		return this.actionBy;
	}

	@NotNull
	@Column(name = "actionon")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getActionOn()
	{
		return this.actionOn;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "certid")
	public Certificate getCert()
	{
		return this.cert;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	public void setAction(CertificateAction action)
	{
		this.action = action;
	}

	public void setActionBy(Contact actionBy)
	{
		this.actionBy = actionBy;
	}

	public void setActionOn(Date actionOn)
	{
		this.actionOn = actionOn;
	}

	public void setCert(Certificate cert)
	{
		this.cert = cert;
	}

	public void setId(int id)
	{
		this.id = id;
	}
}
