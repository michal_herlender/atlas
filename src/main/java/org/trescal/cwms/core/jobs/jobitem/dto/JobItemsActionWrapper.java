package org.trescal.cwms.core.jobs.jobitem.dto;

import java.util.Date;

/**
 * Created by tinakuhbach on 03/05/2016.
 */
public class JobItemsActionWrapper {
    private String activityDesc;
    private String endStatus;
    private String name;
    private Date endStamp;

    public JobItemsActionWrapper() {

    }

    public JobItemsActionWrapper(String activityDesc, String endStatus, String name, Date endStamp) {
        this.activityDesc = activityDesc;
        this.endStatus = endStatus;
        this.name = name;
        this.endStamp = endStamp;
    }

    public String getName() {
        return name;
    }

	public void setName(String name) {
		this.name = name;
	}

	public String getEndStatus() {
		return endStatus;
	}

	public void setEndStatus(String endStatus) {
		this.endStatus = endStatus;
    }

    public String getActivityDesc() {
        return activityDesc;
    }

    public void setActivityDesc(String activityDesc) {
        this.activityDesc = activityDesc;
    }

    public Date getEndStamp() {
        return endStamp;
    }

    public void setEndStamp(Date endStamp) {
        this.endStamp = endStamp;
    }
}
