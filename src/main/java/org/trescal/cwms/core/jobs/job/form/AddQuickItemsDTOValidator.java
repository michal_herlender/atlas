package org.trescal.cwms.core.jobs.job.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;
import org.trescal.cwms.core.instrument.dto.AddItemsValidDTO;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumDao;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class AddQuickItemsDTOValidator extends AbstractBeanValidator implements SmartValidator {

	@Autowired
	InstrumService instService;
	@Autowired
	InstrumDao instDao;

	@Qualifier("messageSource")
	@Autowired
	private MessageSource messages;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(AddItemsValidDTO.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		AddItemsValidDTO dto = (AddItemsValidDTO) target;
		super.validate(dto, errors);

		Instrument inst = instService.get(dto.getInstrument().getPlantid());
		boolean ownedByCompany = instService.isInstrumentOwnedByCompany(dto.getInstrument().getPlantid(), dto.getCoid());
		boolean onJob = instService.isInstrumentOnJob(dto.getInstrument().getPlantid(), dto.getJobId());
		// instrument not found
		if (inst == null) {
			errors.reject("error.instrument.plantid.notfound", "Instrument could not be found");
		} else {
			// indicates if instrument is on another job
			boolean onAnotherJob = false;
			// inst has job items?
			if (inst.getJobItems() != null) {
				// check instruments job items
				for (JobItem ji : inst.getJobItems()) {
					// job item status still active?
					if (ji.getState().getActive()) {
						// set as on another job
						onAnotherJob = true;
						break;
					}
				}
			}
			if (!ownedByCompany) {
				errors.reject("error.instrument.company.notowner", "Instrument not owned by company");
			} else if (onJob) {
				errors.reject("error.instrument.onjob", "Instrument already on job");
			} else if (onAnotherJob) {
				errors.reject("error.instrument.onanotheractivejob", "Instrument is currently on another active job");
			} else if (inst.getBaseUnit() != null && inst.getBaseUnit().getScrapped()) {
				errors.reject("error.instrument.baseunit.scrapped", "The instruments base unit is scrapped");
			}
		}
	}
}
