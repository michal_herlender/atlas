package org.trescal.cwms.core.jobs.calibration.views;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationDTO;
import org.trescal.cwms.core.jobs.calibration.entity.standardused.StandardUsedModel;
import org.trescal.cwms.core.tools.reports.view.XlsxCellStyleHolder;
import org.trescal.cwms.core.tools.reports.view.XlsxViewDecorator;

@Component
public class ReverseTraceabilityXlsxView extends AbstractXlsxStreamingView {

	private static final boolean automaticAutosize = true;

	@Override
	protected void buildExcelDocument(Map<String, Object> modelMap, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		StandardUsedModel model = (StandardUsedModel) modelMap.get("model");
		SXSSFWorkbook sxxfWorkbook = (SXSSFWorkbook) workbook;
		generateExcelFileForReverseTraceability(sxxfWorkbook, model);
		response.setHeader("Content-Disposition", "attachment; filename=\"reversetraceabilitysearch_export.xlsx\"");
	}

	public SXSSFWorkbook generateExcelFileForReverseTraceability(SXSSFWorkbook sxxfWorkbook, StandardUsedModel model) {
		XlsxCellStyleHolder styleHolder = new XlsxCellStyleHolder(sxxfWorkbook);
		// create style for date cells
		DataFormat dateFormat = sxxfWorkbook.createDataFormat();
		CellStyle dateStyle = sxxfWorkbook.createCellStyle();
		dateStyle.setDataFormat(dateFormat.getFormat("d/m/yyyy"));
		// create new Excel sheets
		Sheet sheetStandardused = sxxfWorkbook.createSheet("Standardused");
		Sheet sheetCalibrations = sxxfWorkbook.createSheet("Calibrations performed");

		XlsxViewDecorator decoratorStandardused = new XlsxViewDecorator(sheetStandardused, sxxfWorkbook,
				automaticAutosize, styleHolder);
		XlsxViewDecorator decoratorCalibrations = new XlsxViewDecorator(sheetCalibrations, sxxfWorkbook,
				automaticAutosize, styleHolder);

		createLabelsStandardused(decoratorStandardused);
		createLabelsCalibrations(decoratorCalibrations);

		populateStandardused(decoratorStandardused, model, dateStyle);
		populateCalibrations(decoratorCalibrations, model, dateStyle);

		decoratorStandardused.autoSizeColumns();
		decoratorCalibrations.autoSizeColumns();

		return sxxfWorkbook;
	}

	private void createLabelsStandardused(XlsxViewDecorator decorator) {
		int colIndex = 0;
		int rowIndex = 0;
		CellStyle styleHeader = decorator.getStyleHolder().getHeaderStyle();
		decorator.createCell(rowIndex, colIndex++, "Instrument", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Start cal date", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "End cal date", styleHeader);
	}

	private void createLabelsCalibrations(XlsxViewDecorator decorator) {
		int colIndex = 0;
		int rowIndex = 0;
		CellStyle styleHeader = decorator.getStyleHolder().getHeaderStyle();

		decorator.createCell(rowIndex, colIndex++, "Cal date", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Cal process", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Service Type", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Capability", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Certificate Number", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Job No", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Job Item No", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Client Company Name", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Client Subdivision Name", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Client Contact Name", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Plant Id", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Instrument Model", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Brand", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Model Number", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Serial Number", styleHeader);
		decorator.createCell(rowIndex, colIndex++, "Plant Number", styleHeader);
	}

	private void populateStandardused(XlsxViewDecorator decorator, StandardUsedModel model, CellStyle styleDate) {
		CellStyle styleText = decorator.getStyleHolder().getTextStyle();

		int colIndex = 0;
		int rowIndex = 1;
		decorator.createCell(rowIndex, colIndex++, model.getPlantId(), styleText);
		decorator.createCell(rowIndex, colIndex++, model.getStartCalDate(), styleDate);
		decorator.createCell(rowIndex, colIndex++, model.getEndCalDate(), styleDate);
	}

	private void populateCalibrations(XlsxViewDecorator decorator, StandardUsedModel model, CellStyle styleDate) {
		CellStyle styleText = decorator.getStyleHolder().getTextStyle();
		CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();

		int rowIndex = 1;
		for (CalibrationDTO dto : model.getCalibrationDTOs()) {
			int colIndex = 0;
			decorator.createCell(rowIndex, colIndex++, dto.getCalDate(), styleDate);
			decorator.createCell(rowIndex, colIndex++, dto.getProcess(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getServiceType(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getCapabilityReference(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getCertifNo(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getJobNo(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getJobItemNo(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getConame(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getSubname(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getContactName(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getPlantId(), styleInteger);
			decorator.createCell(rowIndex, colIndex++, dto.getInstModel(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getBrand(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getModelNumber(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getSerialNo(), styleText);
			decorator.createCell(rowIndex, colIndex++, dto.getPlantNo(), styleText);
			rowIndex++;
		}
	}

}
