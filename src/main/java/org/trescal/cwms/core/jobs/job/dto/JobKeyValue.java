package org.trescal.cwms.core.jobs.job.dto;

import org.trescal.cwms.spring.model.KeyValue;

public class JobKeyValue extends KeyValue<Integer, String> {
	
	public JobKeyValue(Integer key, String value) {
		super(key, value);
	}
}