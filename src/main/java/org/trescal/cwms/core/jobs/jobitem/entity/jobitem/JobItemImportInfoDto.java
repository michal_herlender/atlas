package org.trescal.cwms.core.jobs.jobitem.entity.jobitem;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class JobItemImportInfoDto {
	
	private Integer plantId;
	private Integer jobitemId;
	private Integer jiWrId;
	private Integer procedureId;
	private Integer serviceTypeId;
	private Boolean contractReviewed;

}
