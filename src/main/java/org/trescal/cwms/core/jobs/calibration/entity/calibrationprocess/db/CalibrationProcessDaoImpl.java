package org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.calibration.dto.CalibrationProcessKeyValue;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess_;

@Repository
public class CalibrationProcessDaoImpl extends BaseDaoImpl<CalibrationProcess, Integer>
		implements CalibrationProcessDao {

	@Override
	protected Class<CalibrationProcess> getEntity() {
		return CalibrationProcess.class;
	}

	@Override
	public CalibrationProcess findByName(String name) {
		return getSingleResult(cb -> {
			CriteriaQuery<CalibrationProcess> cq = cb.createQuery(CalibrationProcess.class);
			Root<CalibrationProcess> process = cq.from(CalibrationProcess.class);
			cq.where(cb.equal(process.get(CalibrationProcess_.process), name));
			return cq;
		});
	}

	@Override
	public List<CalibrationProcessKeyValue> findAllKeyValue() {
		return getResultList(cb -> {
			CriteriaQuery<CalibrationProcessKeyValue> cq = cb.createQuery(CalibrationProcessKeyValue.class);
			Root<CalibrationProcess> process = cq.from(CalibrationProcess.class);
			cq.orderBy(cb.asc(process.get(CalibrationProcess_.process)));
			cq.select(cb.construct(CalibrationProcessKeyValue.class, process.get(CalibrationProcess_.id),
					process.get(CalibrationProcess_.process)));
			return cq;
		});
	}
}