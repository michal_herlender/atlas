package org.trescal.cwms.core.jobs.jobitem.entity.accessory;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "accessory")
public class Accessory extends Versioned
{
	private int id;
	private String desc;
	private Set<Translation> translations;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@NotNull
	public int getId() {
		return this.id;
	}
	
	/**
	 * @deprecated
	 * use translations
	 */
	@Column(name = "description")
	@Size(max = 150)
	public String getDesc() {
		return this.desc;
	}
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="accessorytranslation", joinColumns=@JoinColumn(name="accessoryid"))
	public Set<Translation> getTranslations() {
		return translations;
	}
	
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setTranslations(Set<Translation> translations) {
		this.translations = translations;
	}
}