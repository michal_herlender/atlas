package org.trescal.cwms.core.jobs.jobitem.entity.requirement.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.requirement.Requirement;

@Repository("RequirementDao")
public class RequirementDaoImpl extends BaseDaoImpl<Requirement, Integer> implements RequirementDao {
	
	@Override
	protected Class<Requirement> getEntity() {
		return Requirement.class;
	}
	
	public void deleteRequirementById(String id) {
		Integer i = Integer.parseInt(id);
		Requirement r = this.find(i);
		this.remove(r);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Requirement> findRequirementByJobItem(int jobitemid) {
		Criteria crit = this.getSession().createCriteria(Requirement.class);
		crit.createCriteria("item").add(Restrictions.idEq(jobitemid));
		return (List<Requirement>) crit.list();
	}
	
	@Override
	public void saveOrUpdateAll(List<Requirement> requirements) {
		for (Requirement requirement : requirements)
			getSession().save(requirement);
	}
	
	@SuppressWarnings("unchecked")
	public List<Requirement> searchRequirement(String requirement) {
		Criteria requirementCriteria = this.getSession().createCriteria(Requirement.class);
		requirementCriteria.add(Restrictions.sqlRestriction("lower({alias}.requirement) like lower('" + requirement + "%')"));
		requirementCriteria.addOrder(Order.asc("instruction"));
		return requirementCriteria.list();
	}
}