package org.trescal.cwms.core.jobs.job.entity.po.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.job.dto.ClientPurchaseOrderDTO;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.projection.POProjectionDTO;
import org.trescal.cwms.core.logistics.dto.PoDTO;

public interface PODao extends BaseDao<PO, Integer> {

	List<PoDTO> getPOsByPrebooking(Integer prebookingdetailsid);

	List<ClientPurchaseOrderDTO> getProjectionByJob(Integer jobId);

	List<POProjectionDTO> getPOsByJobIds(Collection<Integer> jobIds);

	List<PO> searchMatchingJobPOs(int jobId, String partPoNumber);

	PO getMatchingJobPO(int jobId, String poNumber);
}