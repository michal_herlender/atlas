package org.trescal.cwms.core.jobs.job.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.jobs.job.dto.JobSearchParameterWrapper;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.form.OldJobSearchForm;
import org.trescal.cwms.core.jobs.job.form.OldJobSearchValidator;
import org.trescal.cwms.core.jobs.job.views.OldJobSearchXlsxView;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.ServletException;
import javax.validation.Valid;
import java.util.*;

/**
 * Form Controller used to search for job(s).
 */
@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class OldJobSearchController {
    @Value("#{props['cwms.config.jobitem.fast_track_turn']}")
    private int fastTrackTurn;
    @Autowired
    private CompanyService com;
    @Autowired
    private ContactService cont;
    @Autowired
    private NewDescriptionService descServ;
    @Autowired
    private JobService js;
    @Autowired
    private MfrService mfrServ;
    @Autowired
    private CapabilityService procServ;
    @Autowired
    private OldJobSearchValidator validator;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private OldJobSearchXlsxView jobSearchXlsxView;
    @Value("${cwms.config.web.oldjobsearch.size.restrictexport}")
    private long sizeRestrictExport;
	
	public final static int JOB_RESULTS_PER_PAGE = 10;	// Many job items in one job; default of 25 reduced for usability.  
	
	@InitBinder("jobsearchform")
    public void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
	
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
	}
	
	private JobSearchParameterWrapper extractSearchCriteria(OldJobSearchForm jsf, Locale locale)
	{
		JobSearchParameterWrapper jspw = new JobSearchParameterWrapper();
		if ((jsf.getHighlight() != null) && jsf.getHighlight()) {
			jspw.setHighlight(jsf.getHighlight());
		}
		else {
			jspw.setHighlight(false);
		}
		if ((jsf.getJobType() != null) && !jsf.getJobType().equals(JobType.UNDEFINED)) {
			jspw.setJobType(jsf.getJobType());
		}
		if ((jsf.getJobno() != null) && !jsf.getJobno().equals("")) {
			jspw.setJobno(jsf.getJobno());
		}
		if ((jsf.getCoid() != null) && !jsf.getCoid().equals("")) {
			Company c = this.com.get(Integer.parseInt(jsf.getCoid()));
			if (c != null) jspw.setCompany(c.getConame());
		}
		if (jsf.getSubdivid() != null && jsf.getSubdivid() != 0) {
			Subdiv s = this.subdivService.get(jsf.getSubdivid());
			if (s != null) jspw.setSubdiv(s.getSubname());
		}
		if ((jsf.getPersonid() != null) && !jsf.getPersonid().equals("")) {
			Contact contact = this.cont.get(Integer.parseInt(jsf.getPersonid()));
			if (contact != null) jspw.setContact(contact.getName());
		}
		if ((jsf.getMfrid() != null) && !jsf.getMfrid().equals(0)) {
			Mfr mfr = this.mfrServ.findMfr(jsf.getMfrid());
			jspw.setMfr(mfr.getName());
		}
		else if ((jsf.getMfr() != null) && !jsf.getMfr().equals("")) {
			jspw.setMfr(jsf.getMfr());
		}
		if ((jsf.getModel() != null) && !jsf.getModel().equals("")) {
			jspw.setModel(jsf.getModel());
		}
		if ((jsf.getDescid() != null) && !jsf.getDescid().equals(0)) {
			Description desc = this.descServ.findDescription(jsf.getDescid());
			jspw.setDesc(translationService.getCorrectTranslation(desc.getTranslations(), locale));
		}
		else if ((jsf.getDesc() != null) && !jsf.getDesc().equals("")) {
			jspw.setDesc(jsf.getDesc());
		}
		if ((jsf.getClientRef() != null) && !jsf.getClientRef().equals("")) {
			jspw.setClientref(jsf.getClientRef());
		}
		if ((jsf.getPurOrder() != null) && !jsf.getPurOrder().equals("")) {
			jspw.setPurchaseorder(jsf.getPurOrder());
		}
		if ((jsf.getActive() != null) && jsf.getActive()) {
			jspw.setActive(true);
		}
		if ((jsf.getPlantno() != null) && !jsf.getPlantno().equals("")) {
			jspw.setPlantno(jsf.getPlantno());
		}
		if ((jsf.getPlantid() != null) && !jsf.getPlantid().equals("")) {
			jspw.setPlantid(jsf.getPlantid());
		}
		if ((jsf.getSerialno() != null) && !jsf.getSerialno().equals("")) {
			jspw.setSerialno(jsf.getSerialno());
		}
		if ((jsf.getProcId() != null) && (jsf.getProcId().intValue() != 0)) {
            Capability proc = this.procServ.get(jsf.getProcId().intValue());
			jspw.setProcno(proc.getReference());
		}
		return jspw;
	}
	
	@ModelAttribute("jobsearchform")
	protected OldJobSearchForm formBackingObject(@RequestParam(value="mid", required=false) Integer mid,
			@RequestParam(value="compid", required=false) Integer compid,
			@RequestParam(value="subdivid", required=false) Integer subdivid,
			@RequestParam(value="contactid", required=false) Integer contactid,
			@RequestParam(value="activeonly", required=false) Boolean activeonly) throws ServletException {
		OldJobSearchForm jsf = new OldJobSearchForm();
		// you have to lookup and set some fields separately as if they're
		// set from a url spring will break and try and double submit them,
		// therefore they must also be named differently
		if (mid != null) jsf.setModelid(mid);
		if (compid != null) jsf.setCoid(compid.toString());
		if (subdivid != null) jsf.setSubdivid(subdivid);
		if (contactid != null) jsf.setPersonid(contactid.toString());
		if (activeonly != null) {
			jsf.setActive(activeonly);
		}
		else {
			jsf.setActive(true);
		}
		return jsf;
	}
	
	@RequestMapping(value="/oldjobsearch.htm", method=RequestMethod.POST, params="!export")
	public ModelAndView onSubmit(Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto, 
			@Valid @ModelAttribute("jobsearchform") OldJobSearchForm jsf, BindingResult bindingResult) throws Exception {
		if(bindingResult.hasErrors()) return referenceData(jsf);
		else {
			// perform the search
			jsf.setOrgId(subdivDto.getKey().toString());
			PagedResultSet<Job> rs = this.js.queryJobOld(jsf, new PagedResultSet<Job>(jsf.getResultsPerPage() == 0 ? JOB_RESULTS_PER_PAGE : jsf.getResultsPerPage(), jsf.getPageNo() == 0 ? 1 : jsf.getPageNo()));
			jsf.setResultsTotal(rs.getResultsCount());
			jsf.setRs(rs);
			jsf.setJobs((Collection<Job>) rs.getResults());

			// Get the total number of job items found in the search
			int jobitems = 0;
			for (Job j : jsf.getJobs()) {
				jobitems = jobitems + j.getItems().size();
			}
			jsf.setJobItemCount(jobitems);
			jsf.setSearchCriteria(this.extractSearchCriteria(jsf, locale));
			// re-call reference data and add to the model passed to results page
			Map<String, Object> model = this.referenceData(jsf).getModel();
			model.put("jobsearchform", jsf);
			return new ModelAndView("trescal/core/jobs/job/oldjobresults", model);
		}
	}
	
	@RequestMapping(value="/oldjobsearch.htm", method=RequestMethod.POST, params="export")
	public ModelAndView onExport(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto, 
			@ModelAttribute("jobsearchform") OldJobSearchForm jsf) throws Exception {
		if (jsf.getResultsTotal() >= sizeRestrictExport) {
			throw new RuntimeException("This search has "+jsf.getResultsTotal()+" job results and due to temporary performance restrictions, searches with "+sizeRestrictExport+" job results or more cannot currently be exported."); 
		}
		jsf.setOrgId(subdivDto.getKey().toString());
		PagedResultSet<Job> rs = this.js.queryJobOld(jsf, new PagedResultSet<Job>(jsf.getResultsTotal(), 0));
		Set<Job> jobs = new HashSet<Job>(rs.getResults());	
		return new ModelAndView(jobSearchXlsxView, "jobs", jobs);
	}

	
	@RequestMapping(value="/oldjobsearch.htm", params="forced=forced", method=RequestMethod.GET)
	public ModelAndView forceSubmit(Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto, 
			@ModelAttribute("jobsearchform") OldJobSearchForm jsf, BindingResult bindingResult) throws Exception {
		return onSubmit(locale, subdivDto, jsf, bindingResult);
	}
	
	@RequestMapping(value="/oldjobsearch.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute("jobsearchform") OldJobSearchForm jsf) throws Exception
	{
		Map<String, Object> refData = new HashMap<String, Object>();
		refData.put(Constants.FAST_TRACK_TURN, this.fastTrackTurn);
		refData.put("jobTypes", getJobTypeDtos());
		refData.put("procText", getProcText(jsf.getProcId()));
		return new ModelAndView("trescal/core/jobs/job/jobsearch", refData);
	}
	
	private String getProcText(Integer procId) {
		String result = "";
		if (procId != null && procId != 0) {
			result = procServ.get(procId).getReference();
		}
		return result;
	}
	
	protected List<KeyValue<JobType,String>> getJobTypeDtos() {
		// Special list, just for search form
		List<KeyValue<JobType,String>> result = new ArrayList<>();
		Locale locale = LocaleContextHolder.getLocale();
		result.add(new KeyValue<JobType, String>(JobType.UNDEFINED, "-- "+messageSource.getMessage("jobsearch.alljobtypes", null, locale)+" --"));
		for (JobType jobType : JobType.getActiveJobTypes()) {
			result.add(new KeyValue<JobType, String>(jobType, jobType.getDescription()));
		}
		return result;
	}
}