package org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpointset.CalibrationPointSet;

public interface CalibrationPointSetDao extends BaseDao<CalibrationPointSet, Integer> {
	
	void deleteAll(List<CalibrationPointSet> calibrationpointset);

}