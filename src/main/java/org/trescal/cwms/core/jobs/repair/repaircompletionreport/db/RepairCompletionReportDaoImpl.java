package org.trescal.cwms.core.jobs.repair.repaircompletionreport.db;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport_;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport_;

@Repository("RepairCompletionReportDao")
public class RepairCompletionReportDaoImpl extends BaseDaoImpl<RepairCompletionReport, Integer>
		implements RepairCompletionReportDao {

	@Override
	protected Class<RepairCompletionReport> getEntity() {
		return RepairCompletionReport.class;
	}

	@Override
	public void refresh(RepairCompletionReport rcr) {
		getEntityManager().refresh(rcr);

	}

	@Override
	public RepairCompletionReport getByJobitem(Integer jobitemid) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<RepairCompletionReport> cq = cb.createQuery(RepairCompletionReport.class);

		Root<RepairCompletionReport> root = cq.from(RepairCompletionReport.class);
		Join<RepairCompletionReport, RepairInspectionReport> rirJoin = root
				.join(RepairCompletionReport_.repairInspectionReports);
		Join<RepairInspectionReport, JobItem> jiJoin = rirJoin.join(RepairInspectionReport_.ji);

		cq.select(root);
		cq.where(cb.equal(jiJoin.get(JobItem_.jobItemId), jobitemid));

		return getEntityManager().createQuery(cq).getResultList().stream().findFirst().orElse(null);
	}

	@Override
	public RepairCompletionReport getValidatedRcrByJobItem(int jobItemId) {
		return getFirstResult(cb -> {
			CriteriaQuery<RepairCompletionReport> cq = cb.createQuery(RepairCompletionReport.class);
			Root<RepairCompletionReport> root = cq.from(RepairCompletionReport.class);
			Join<RepairCompletionReport, RepairInspectionReport> rirJoin = root.join(RepairCompletionReport_.repairInspectionReports);
			Join<RepairInspectionReport, JobItem> jobItemJoin = rirJoin.join(RepairInspectionReport_.ji);
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(jobItemJoin.get(JobItem_.jobItemId), jobItemId));
			clauses.getExpressions().add(cb.isTrue(root.get(RepairCompletionReport_.validated)));
			
			cq.where(clauses);
			cq.orderBy(cb.desc(root.get(RepairCompletionReport_.rcrId)));
			return cq;
		}).orElse(null);
	}
}