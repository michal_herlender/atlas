package org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.db;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.jobnote.JobNote;
import org.trescal.cwms.core.jobs.job.entity.jobnote.db.JobNoteService;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemActionComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemactivity.JobItemActivity;
import org.trescal.cwms.core.jobs.jobitem.form.genericentityvalidator.JobItemActivityValidator;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.StringTools;
import org.trescal.cwms.core.workflow.automation.antech.jobinterceptor.JobItemActionChange;
import org.trescal.cwms.core.workflow.automation.common.lookup.StatusCalculationService;
import org.trescal.cwms.core.workflow.entity.actionoutcome.ActionOutcome;
import org.trescal.cwms.core.workflow.entity.actionoutcome.db.ActionOutcomeService;
import org.trescal.cwms.core.workflow.entity.itemactivity.ItemActivity;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.core.workflow.entity.itemstatus.ItemStatus;
import org.trescal.cwms.core.workflow.entity.progressactivity.ProgressActivity;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.*;

@Service("JobItemActivityService")
public class JobItemActivityServiceImpl extends BaseServiceImpl<JobItemActivity, Integer>
	implements JobItemActivityService {

	private ActionOutcomeService actOutServ;

	private AuthenticationService authServ;

	private ContactService conServ;

	private ItemStateService itemStateServ;

	private JobItemActivityValidator jiaValidator;

	private JobItemService jiServ;

	private JobItemActivityDao jobItemActivityDao;

	private JobNoteService jobNoteService;

	private UserService userService;

	private StatusCalculationService statusCalcServ;

	private TranslationService translationService;

	private MessageSource messageSource;

	private FaultReportService faultReportService;

	public JobItemActivityServiceImpl(ActionOutcomeService actOutServ, AuthenticationService authServ,
									  ContactService conServ, ItemStateService itemStateServ,
									  JobItemActivityValidator jiaValidator, JobItemService jiServ,
									  JobItemActivityDao jobItemActivityDao, JobNoteService jobNoteService,
									  UserService userService, StatusCalculationService statusCalcServ,
									  TranslationService translationService, MessageSource messageSource,
									  FaultReportService faultReportService) {
		this.actOutServ = actOutServ;
		this.authServ = authServ;
		this.conServ = conServ;
		this.itemStateServ = itemStateServ;
		this.jiaValidator = jiaValidator;
		this.jiServ = jiServ;
		this.jobItemActivityDao = jobItemActivityDao;
		this.jobNoteService = jobNoteService;
		this.userService = userService;
		this.statusCalcServ = statusCalcServ;
		this.translationService = translationService;
		this.messageSource = messageSource;
		this.faultReportService = faultReportService;
	}

	// Redefined here to enable AOP to trigger (not possible with inherited
	// methods or interface)
	@Override
	@JobItemActionChange
	public void save(JobItemActivity object) {
		super.save(object);
	}

	// Redefined here to enable AOP to trigger (not possible with inherited
	// methods)
	@Override
	@JobItemActionChange
	public JobItemActivity merge(JobItemActivity object) {
		return super.merge(object);
	}

	// Redefined here to enable AOP to trigger (not possible with inherited
	// methods)
	@Override
	@JobItemActionChange
	public void update(JobItemActivity object) {
		super.update(object);
	}

	private Boolean canDeleteLastActivity(int jobItemId, int contactId, HttpSession session) {
		JobItem ji = this.jiServ.findEagerJobItem(jobItemId);
		int activityCount = ji.getActions().size();

		if (activityCount < 1) {
			// there are no activities on this job item
			return null;
		} else {
			// get most recent activity for job item (last in list)
			ArrayList<JobItemAction> list = new ArrayList<JobItemAction>(ji.getActions());
			JobItemAction jia = list.get(activityCount - 1);
			@SuppressWarnings("unchecked")
			KeyValue<Integer, String> subdivDto = (KeyValue<Integer, String>) session
					.getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV);
			return this.authServ.allowPrivileges(contactId, jia.getStartedBy().getPersonid(), subdivDto.getKey());
		}
	}

	@Override
	public ResultWrapper completePendingActivity(int jobItemId, Integer endStatId, String remark, String timeSpent,
			Integer actionOutcomeId, HttpSession session) {
		JobItem jobItem = jiServ.findJobItem(jobItemId);
		JobItemActivity a = this.getIncompleteActivityForItem(jobItem);
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact con = this.userService.getEagerLoad(username).getCon();

		if (con == null) {
			return new ResultWrapper(false, "The session has expired. Please refresh the page, log in and try again.");
		}

		if (a != null) {
			JobItemActivity tempA = new JobItemActivity();
			BeanUtils.copyProperties(a, tempA);

			tempA.setEndStamp(new Date());
			// tempA.setEndStatus(this.itemStateServ.findItemStatus(endStatId));
			tempA.setRemark(remark);

			try {
				tempA.setTimeSpent(Integer.valueOf(timeSpent));
			} catch (NumberFormatException e) {
				tempA.setTimeSpent(-1);
			}

			ActionOutcome actOut = (actionOutcomeId == null) ? null : this.actOutServ.get(actionOutcomeId);
			tempA.setOutcome(actOut);

			JobItem ji = tempA.getJobItem();

			// call method for delivering informational failure report
			this.faultReportService.deliverInformationalFR(ji, a);

			// set the action outcome in the jobitem here too
			for (JobItemAction action : ji.getActions()) {
				if (action instanceof JobItemActivity) {
					JobItemActivity activityBeingCompleted = (JobItemActivity) action;
					if (!activityBeingCompleted.isComplete()) {
						activityBeingCompleted.setOutcome(actOut);
					}
				}
			}

			if (tempA.getActivity().getLookup() != null) {
				// if there is a lookup, calculate the end status automatically
				ItemStatus status = this.statusCalcServ.getNextStatus(tempA.getActivity(), ji);
				// the status has to be reloaded here so that the DWR callback
				// method can reach it
				status = this.itemStateServ.findItemStatus(status.getStateid());
				tempA.setEndStatus(status);
			} else {
				// if this is a progress activity, the end status will be the
				// same
				// as the start status
				if (endStatId != null) {
					tempA.setEndStatus(this.itemStateServ.findItemStatus(endStatId));
				} else {
					tempA.setEndStatus((ItemStatus) ji.getState());
				}
			}

			tempA.setCompletedBy(con);
			tempA.setComplete(true);

			// attempt to fix the JS loading problems...
			int startStatId = tempA.getStartStatus().getStateid();
			tempA.setStartStatus(this.itemStateServ.findItemStatus(startStatId));
			int conId = tempA.getStartedBy().getPersonid();
			tempA.setStartedBy(this.conServ.get(conId));

			ji.setState(tempA.getEndStatus());
			// this.jiServ.updateJobItem(ji);

			// create a new validator to validate this JobItemActivity before
			// trying the insert
			BindException errors = new BindException(tempA, "jobitemactivity");
			this.jiaValidator.validate(tempA, errors);

			// if validation passes then insert the JobItemActivity
			if (!errors.hasErrors()) {
				BeanUtils.copyProperties(tempA, a);
				this.update(a);
				this.jiServ.mergeJobItem(ji);
			}

			// return null here to prevent LIE that occurs in JobItem bean
			// loading up set of contractReviewItems. returning null will force
			// a page refresh in the javascript file.
			return new ResultWrapper(errors, null);
		}

		return null;
	}

	@Override
	public ResultWrapper deleteJobItemActivityById(int jiaId, HttpServletRequest req) {
		ResultWrapper resWrap = new ResultWrapper();

		JobItemActivity jia = this.jobItemActivityDao.find(jiaId);
		JobItem ji = this.jiServ.findEagerJobItem(jia.getJobItem().getJobItemId());
		ArrayList<JobItemAction> actionList = new ArrayList<JobItemAction>(ji.getActions());

		JobItemAction lastJia = actionList.get(actionList.size() - 1);

		JobItemAction penultimateJia = null;
		if (actionList.size() > 1) {
			penultimateJia = actionList.get(actionList.size() - 2);
		}

		if (lastJia.getId().intValue() == jia.getId().intValue()) {
			if (req.getSession() == null) {
				return new ResultWrapper(false,
						"The session has expired. Please refresh the page, log in and try again.");
			}
			String username = (String) req.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
			Contact con = this.userService.getEagerLoad(username).getCon();

			Boolean permission = this.canDeleteLastActivity(ji.getJobItemId(), con.getPersonid(), req.getSession());

			if (permission == true) {
				jia.setDeleted(true);
				jia.setComplete(true);
				jia.setDeletedBy(con);
				jia.setDeleteStamp(new Date());
				// jia.setStepNo(this.getNextDeleteStepNo(ji));
				this.jobItemActivityDao.update(jia);

				if (penultimateJia != null) {
					if (penultimateJia.getEndStatus() != null) {
						ji.setState(penultimateJia.getEndStatus());
					}
				} else {
					ji.setState(this.itemStateServ.findItemState(90));
				}
				this.jiServ.updateJobItem(ji);

				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("jia", jia);
				map.put("state", ji.getState());
				resWrap.setResults(map);
				resWrap.setSuccess(true);
			} else if (permission == false) {
				resWrap.setSuccess(false);
				resWrap.setMessage("You do not have appropriate permissions to delete the last activity on item "
						+ ji.getItemNo() + " on Job " + ji.getJob().getJobno());
			} else {
				resWrap.setSuccess(false);
				resWrap.setMessage("There was an error attempting to delete the last activity on item " + ji.getItemNo()
						+ " on Job " + ji.getJob().getJobno());
			}
		} else {
			resWrap.setSuccess(false);
			resWrap.setMessage("This is no longer the last activity on item " + ji.getItemNo() + " on Job "
					+ ji.getJob().getJobno());
		}

		return resWrap;
	}

	@Override
	public List<JobItemActivity> getAllIncompleteActivityForContact(Contact contact) {
		return this.jobItemActivityDao.getAllIncompleteActivityForContact(contact);
	}

	public JobItemActivity getIncompleteActivityForItem(JobItem jobItem) {
		return jobItem.getActions().stream().filter(jia -> !jia.isTransit()).map(jia -> (JobItemActivity) jia)
				.filter(jia -> !jia.isComplete()).findFirst().orElse(null);
	}

	public List<JobItem> getJobItemsWithSameState(int jobItemId) {
		JobItem ji = this.jiServ.findJobItem(jobItemId);
		int jobId = ji.getJob().getJobid();
		int stateId = ji.getState().getStateid();

		return this.jobItemActivityDao.getJobItemsWithSameState(jobId, stateId, jobItemId);
	}

	public Integer getMinutesFromActivityStart(int actId) {
		JobItemActivity jia = this.get(actId);
		Integer time = DateTools.getMinutesDifference(jia.getStartStamp(), new Date());

		if (time == null) {
			return 0;
		} else {
			return time;
		}
	}

	@Override
	public ResultWrapper newCompletedActivity(Integer jobItemId, Integer actId, Integer actionOutcomeId,
			Integer endStatId, String remark, Integer timeSpent, Date startDate, Contact con, Integer[] otherJobItemIds,
			Boolean addRemarkAsJobNote) {

		JobItemActivity theOne = null;
		List<Integer> allItemIds = new ArrayList<Integer>();
		if (otherJobItemIds != null) {
			allItemIds.addAll(Arrays.asList(otherJobItemIds));
		}
		allItemIds.add(jobItemId);
		List<JobItem> jis = new ArrayList<JobItem>();
		BindException errors = new BindException(new JobItemActivity(), "jobitemactivity");

		itemLoop: for (int jiId : allItemIds) {
			JobItem ji = this.jiServ.findJobItem(jiId);
			jis.add(ji);
			JobItemActivity a = new JobItemActivity();

			a.setCreatedOn(new Date());
			a.setStartedBy(con);
			a.setCompletedBy(con);
			a.setStartStamp(startDate);
			a.setEndStamp(DateUtils.addMinutes(startDate, timeSpent));

			if (!(ji.getState() instanceof ItemStatus)) {
				return new ResultWrapper(false, "Cannot create a new activity whilst the item state is an activity.",
						null, null);
			}

			a.setStartStatus((ItemStatus) ji.getState());
			ItemActivity activity = this.itemStateServ.findItemActivity(actId);
			a.setActivity(activity);
			a.setActivityDesc(a.getActivity().getDescription());
			a.setRemark(remark);
			a.setTimeSpent(timeSpent);
			ActionOutcome actOut = (actionOutcomeId == null) ? null : this.actOutServ.get(actionOutcomeId);
			a.setOutcome(actOut);
			if (ji.getActions() == null) {
				ji.setActions(new TreeSet<>(new JobItemActionComparator()));
			}

			// call method for delivering informational failure report
			this.faultReportService.deliverInformationalFR(ji, a);

			ji.getActions().add(a);
			if (activity.getLookup() != null) {
				// if there is a lookup, calculate the end status automatically
				ItemStatus status = this.statusCalcServ.getNextStatus(activity, ji);
				// the status has to be reloaded here so that the DWR callback
				// method can reach it
				status = this.itemStateServ.findItemStatus(status.getStateid());
				a.setEndStatus(status);
			} else {
				// if this is a progress activity, the end status will be the
				// same as the start status
				if (endStatId != null) {
					a.setEndStatus(this.itemStateServ.findItemStatus(endStatId));
				} else {
					a.setEndStatus((ItemStatus) ji.getState());
				}
			}

			a.setComplete(true);

			// if a RESPONSE progress activity is being recorded, then prevent
			// the item from being chased further until the next status change
			if ((activity instanceof ProgressActivity)
					&& activity.getDescription().toLowerCase().contains("response")) {
				ji.setChasingActive(false);
			}
			ji.setState(a.getEndStatus());
			a.setJobItem(ji);
			// create a new validator to validate this JobItemActivity before
			// trying the insert
			this.jiaValidator.validate(a, errors);
			// if validation passes then insert the JobItemActivity
			if (!errors.hasErrors()) {
				this.save(a);
				this.jiServ.updateJobItem(ji);
				// logger.debug("Inserted new activity "+a.getId());

				// return the activity that is for the item currently being
				// viewed on screen
				theOne = ((theOne == null) && (ji.getJobItemId() == jobItemId)) ? a : theOne;
			} else {
				// logger.debug("Item had errorCount: "+errors.getErrorCount());
				break itemLoop;
			}
		}

		if ((addRemarkAsJobNote != null) && (addRemarkAsJobNote == true) && !errors.hasErrors()) {
			JobNote note = new JobNote();
			note.setActive(true);
			note.setJob(theOne.getJobItem().getJob());

			Locale locale = theOne.getJobItem().getJob().getOrganisation().getComp().getDocumentLanguage();
			if (locale == null)
				locale = con.getLocale();

			note.setLabel(this.messageSource.getMessage("jiactions.progaction", null, "Progress action", locale));
			note.setNote("Item(s) " + StringTools.combineSequenceJobItemNumbers(jis) + ": "
					+ this.translationService.getCorrectTranslation(theOne.getActivity().getTranslations(), locale)
					+ ". " + remark);
			note.setPublish(false);
			note.setSetBy(con);
			note.setSetOn(new Date());
			jobNoteService.insertJobNote(note);
		}

		return new ResultWrapper(errors, theOne);

	}

	@Override
	public ResultWrapper newCompletedActivityDWR(int actId, Integer endStatId, int jobItemId, Integer[] otherJobItemIds,
			String remark, String timeSpentString, Integer actionOutcomeId, Boolean addRemarkAsJobNote,
			HttpSession session) {
		if (session == null) {
			return new ResultWrapper(false, "The session has expired. Please refresh the page, log in and try again.");
		}
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact con = this.userService.getEagerLoad(username).getCon();

		Integer timeSpent;
		try {
			timeSpent = Integer.valueOf(timeSpentString);
		} catch (NumberFormatException e) {
			timeSpent = -1;
		}

		Date startDate = new Date();
		return this.newCompletedActivity(jobItemId, actId, actionOutcomeId, endStatId, remark, timeSpent, startDate,
				con, otherJobItemIds, addRemarkAsJobNote);
	}

	@Override
	protected BaseDao<JobItemActivity, Integer> getBaseDao() {
		return this.jobItemActivityDao;
	}

	@Override
	public JobItemActivity createIncompleteAction(Integer activityId, Integer jobItemId, Contact contact) {

		ItemActivity itemActivity= itemStateServ.findItemActivity(activityId);

		JobItem jobItem = jiServ.findJobItem(jobItemId);

		if (!(jobItem.getState() instanceof ItemStatus)){
			throw new IllegalArgumentException("Cannot create a new activity whilst the item state is an activity.");
		}

		Date date = new Date();

		JobItemActivity jobItemActivity = new JobItemActivity();

		jobItemActivity.setActivity(itemActivity);
		jobItemActivity.setJobItem(jobItem);
		jobItemActivity.setStartedBy(contact);
		jobItemActivity.setCreatedOn(date);
		jobItemActivity.setStartStamp(date);
		jobItemActivity.setStartStatus((ItemStatus)(jobItem.getState()));
		jobItemActivity.setActivityDesc(itemActivity.getDescription());
		jobItemActivity.setComplete(false);

		jobItemActivityDao.persist(jobItemActivity);

		return jobItemActivity;
	}
}