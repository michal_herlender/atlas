package org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gsodocument.GsoDocument_;

@Repository("GsoDocumentDao")
public class GsoDocumentDaoImpl extends BaseDaoImpl<GsoDocument, Integer> implements GsoDocumentDao {
	
	protected Class<GsoDocument> getEntity() {
	     return GsoDocument.class;
	     }
	
	@Override
	public GsoDocument findGsoDocumentByDocumentNumber(String documentNumber) {
		return getFirstResult(cb -> {
			CriteriaQuery<GsoDocument> cq = cb.createQuery(GsoDocument.class);
			Root<GsoDocument> root = cq.from(GsoDocument.class);
			
			cq.where(cb.equal(root.get(GsoDocument_.documentNumber), documentNumber));
			return cq;
		}).orElse(null);
	}
	
 }
