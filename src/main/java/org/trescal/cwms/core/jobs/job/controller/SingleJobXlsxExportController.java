package org.trescal.cwms.core.jobs.job.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.views.SingleJobXlsxView;

@Controller
@IntranetController
public class SingleJobXlsxExportController {

	@Autowired
	private JobService  jobService;
	@Autowired
	private SingleJobXlsxView singleJobXlsxView;
	
	@RequestMapping(value = "singlejobexport.xlsx", method = RequestMethod.GET)
	public ModelAndView singleJobXlsxView(@RequestParam(name = "jobid", required = true) Integer jobId)
 {
		Job job = jobService.get(jobId);
				if(job != null){
					Map<String, Object> model = new HashMap<String, Object>();
					model.put("job", job);
					return new ModelAndView(singleJobXlsxView, model);
				}
		
		return null;
	}
	
}
