package org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.repcost.ContractReviewRepairCost;

public interface ContractReviewRepairCostDao extends BaseDao<ContractReviewRepairCost, Integer> {}