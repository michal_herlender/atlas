package org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemtransit.JobItemTransit;

public interface JobItemTransitService extends BaseService<JobItemTransit, Integer>
{

	ResultWrapper receiveInTransitItem(int jobItemId, String remark, int endStatId, String username);

}