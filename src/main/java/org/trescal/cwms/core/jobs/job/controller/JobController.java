package org.trescal.cwms.core.jobs.job.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.deliverynote.entity.courier.Courier;
import org.trescal.cwms.core.deliverynote.entity.courier.db.CourierService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.jobs.job.dto.JobKeyValue;
import org.trescal.cwms.core.jobs.job.dto.JobQuoteLinkDTO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.db.JobQuoteLinkService;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.jobs.job.entity.po.db.POService;
import org.trescal.cwms.core.jobs.job.form.DeliveryForm;
import org.trescal.cwms.core.jobs.jobitem.entity.jobexpenseitem.db.JobExpenseItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.JobItemGroup;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemgroup.db.JobItemGroupService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.logistics.entity.jobcourier.JobCourierLink;
import org.trescal.cwms.core.logistics.entity.jobcourier.db.JobCourierLinkService;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderDTO;
import org.trescal.cwms.core.pricing.purchaseorder.dto.PurchaseOrderItemDTO;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.db.PurchaseOrderItemService;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@JsonController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY})
public class JobController {

	private static final Logger logger = LoggerFactory.getLogger(JobController.class);

	@Autowired
	private CompanyService companyService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private JobExpenseItemService jobExpenseItemService;
	@Autowired
	private JobItemGroupService groupService;
	@Autowired
	private JobQuoteLinkService jobQuoteLinkService;
	@Autowired
	private JobService jobService;
	@Autowired
	private POService poService;
	@Autowired
	private PurchaseOrderService purchaseOrderService;
	@Autowired
	private PurchaseOrderItemService poItemService;
	@Autowired
	private QuotationService quotationService;
	@Autowired
	private StatusService statusService;
	@Autowired
	private UserService userService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private JobCourierLinkService jobCourierLinkServ;
	@Autowired
	private CourierService courierServ;

	@RequestMapping(value = "updatejobitemgroup.json", method = RequestMethod.GET)
	@ResponseBody
	public void updateJobItemGroup(@RequestParam(name = "jobItemId") Integer jobItemId,
			@RequestParam(name = "groupId") Integer groupId) {
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		JobItemGroup group = groupService.findJobItemGroup(groupId);
		jobItem.setGroup(group);
		jobItemService.merge(jobItem);
	}

	@RequestMapping(value = "newjobitemgroup.json", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> newJobItemGroup(@RequestParam(name = "jobId") Integer jobId) {
		Job job = jobService.get(jobId);
		JobItemGroup group = new JobItemGroup();
		group.setJob(job);
		groupService.insertJobItemGroup(group);
		return Collections.singletonMap("groupId", group.getId());
	}

	@RequestMapping(value = "deletejobitemgroup.json", method = RequestMethod.GET)
	@ResponseBody
	public void deleteJobItemGroup(@RequestParam(name = "groupId") Integer groupId) {
		JobItemGroup group = groupService.findJobItemGroup(groupId);
		group.getItems().forEach(ji -> {
			ji.setGroup(null);
			jobItemService.merge(ji);
		});
		groupService.deleteJobItemGroup(group);
	}

	@RequestMapping(value = "linkjobtoquotation.htm", method = RequestMethod.GET)
	public ModelAndView linkJobToQuotation(@RequestParam(name = "jobId") Integer jobId,
			@RequestParam(name = "quoteId") Integer quoteId,
			@RequestParam(name = "acceptStatus", required = false, defaultValue = "0") Integer acceptStatusId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		logger.debug("Link job " + "jobId" + " to quotation " + quoteId);
		Job job = jobService.get(jobId);
		Quotation quotation = quotationService.get(quoteId);
		if (job == null) {
			String errorMessage = messageSource.getMessage("error.job.notfound", null, "Could not find job",
					LocaleContextHolder.getLocale());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		} else if (quotation == null) {
			String errorMessage = messageSource.getMessage("error.quotation.notfound", null, "Could not find quotation",
					LocaleContextHolder.getLocale());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		} else if (jobQuoteLinkService.findJobQuoteLinks(quoteId, jobId).size() > 0) {
			String errorMessage = messageSource.getMessage("error.job.jobquotelink.duplicate", null,
					"'This quotation is already linked to this job'", LocaleContextHolder.getLocale());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		} else {
			Contact currentContact = userService.get(username).getCon();
			if (acceptStatusId > 0 && quotation.getQuotestatus().getIssued()
					&& !quotation.getQuotestatus().getAccepted()) {
				QuoteStatus acceptStatus = (QuoteStatus) statusService.get(acceptStatusId);
				quotation.setQuotestatus(acceptStatus);
				quotation.setAccepted(true);
				quotation.setAcceptedBy(currentContact);
				quotation.setAcceptedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			}
			JobQuoteLinkDTO link = jobQuoteLinkService.linkJobToQuote(quotation, job, currentContact);
			return new ModelAndView("trescal/core/jobs/job/linkjobtoquotation", "jobQuoteLink", link);
		}
	}

	@RequestMapping(value = "deletejobquotelink.json", method = RequestMethod.GET)
	@ResponseBody
	public void deleteJobQuoteLink(@RequestParam(name = "jobQuoteLinkId") Integer jobQuoteLinkId) {
		JobQuoteLink jobQuoteLink = jobQuoteLinkService.get(jobQuoteLinkId);
		jobQuoteLinkService.delete(jobQuoteLink);
	}

	@RequestMapping("addJobItemsToPO.htm")
	public ModelAndView addJobItemsToPO(@RequestParam(name = "jobId") Integer jobId,
			@RequestParam(name = "poId") Integer poId) {
		Map<String, Object> model = new HashMap<>();
		PO po = poService.get(poId);
		model.put("jobItems", jobItemService.getJobItemsOnClientPO(poId, jobId));
		model.put("jobServices", jobExpenseItemService.getExpenseItemsOnClientPO(poId, jobId));
		model.put("poId", poId);
		model.put("jobId", jobId);
		model.put("poNo", po.getPoNumber());
		model.put("actionOnPO", true);
		return new ModelAndView("trescal/core/jobs/job/addjobitemstopo", model);
	}

	@RequestMapping("addJobItemsToBPO.htm")
	public ModelAndView addJobItemsToBPO(@RequestParam(name = "jobId") Integer jobId,
			@RequestParam(name = "poId") Integer poId,
			@RequestParam(name = "poNo", required = false, defaultValue = "") String poNo) {
		Map<String, Object> model = new HashMap<>();
		model.put("jobItems", jobItemService.getJobItemsOnClientBPO(poId, jobId));
		model.put("jobServices", jobExpenseItemService.getExpenseItemsOnClientBPO(poId, jobId));
		model.put("poId", poId);
		model.put("jobId", jobId);
		model.put("poNo", poNo);
		model.put("actionOnPO", false);
		return new ModelAndView("trescal/core/jobs/job/addjobitemstopo", model);
	}

	@RequestMapping(value = "jobitemsonclientpurchaseorder.htm", method = RequestMethod.GET)
	public ModelAndView onRequest(
			@RequestParam(name = "titleAction", required = false, defaultValue = "") String titleAction,
			@RequestParam(name = "poId", required = false, defaultValue = "0") Integer poId,
			@RequestParam(name = "jobId", required = false, defaultValue = "0") Integer jobId) {
		Map<String, Object> model = new HashMap<>();
		PO po = poId == 0 ? null : poService.get(poId);
		Job job = jobId == 0 ? null : jobService.get(jobId);
		model.put("titleAction", titleAction);
		model.put("poId", poId);
		model.put("poNo", po == null ? "" : po.getPoNumber());
		model.put("poComment", po == null ? "" : po.getComment());
		model.put("jobId", jobId);
		model.put("jobNo", job == null ? "" : job.getJobno());
		return new ModelAndView("trescal/core/jobs/job/showitemsoncpo", model);
	}

	@RequestMapping(value = "/displayLinkedPurchaseOrders.htm")
	public ModelAndView displayLinkedPurchaseOrders(@RequestParam(name = "jobId") Integer jobId,
			@RequestParam(name = "jobItemId") Integer jobItemId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		Job job = jobService.get(jobId);
		Map<String, Object> model = new HashMap<>();
		List<PurchaseOrderDTO> linkedOrders = purchaseOrderService.findAllByJob(job);
		model.put("linkedOrders", linkedOrders);
		Company allocatedCompany = companyService.get(companyDto.getKey());
		List<PurchaseOrderDTO> mostRecentOrders = purchaseOrderService.findMostRecent(allocatedCompany,
				Constants.RESULTS_PER_PAGE);
		model.put("mostRecentOrders", mostRecentOrders);
		model.put("jobItemId", jobItemId);
		return new ModelAndView("trescal/core/jobs/jobitem/fragments/linkedPurchaseOrders", model);
	}

	@RequestMapping(value = "/displayLinkedPurchaseOrderItems.htm")
	public ModelAndView displayLinkedPurchaseOrderItems(@RequestParam(name = "purchaseOrderId") Integer purchaseOrderId,
			@RequestParam(name = "appendTo") String appendTo, @RequestParam(name = "jobItemId") Integer jobItemId) {
		List<PurchaseOrderItemDTO> poItems = poItemService.findAllFromPurchaseOrder(purchaseOrderId);
		Map<String, Object> model = new HashMap<>();
		model.put("poItems", poItems);
		model.put("appendTo", appendTo);
		model.put("jobItemId", jobItemId);
		List<KeyValue<Integer, Integer>> jobItems = jobItemService.findJobItem(jobItemId).getJob().getItems().stream()
				.map(ji -> new KeyValue<>(ji.getJobItemId(), ji.getItemNo())).collect(Collectors.toList());
		model.put("jobItems", jobItems);
		return new ModelAndView("trescal/core/jobs/jobitem/fragments/linkedPurchaseOrderItems", model);
	}

	@RequestMapping(value = "/linkJobItemToPoItem.htm")
	public ModelAndView linkJobItemToPoItem(@RequestParam(name = "jobItemId") Integer jobItemId,
			@RequestParam(name = "poItemId") Integer poItemId) {
		JobItem jobItem = jobItemService.findJobItem(jobItemId);
		PurchaseOrderItem poItem = poItemService.findPurchaseOrderItem(poItemId);
		poItemService.linkJobItem(poItem, jobItem);
		Map<String, Object> model = new HashMap<>();
		model.put("poItem", poItem);
		return new ModelAndView("trescal/core/jobs/jobitem/fragments/newPurchaseOrderItem", model);
	}

	@RequestMapping(value = "/searchjobs.json")
	@ResponseBody
	public List<JobKeyValue> searchJobs(@RequestParam(name = "partialJobNo") String partialJobNo) {
		return jobService.findByPartialJobNo(partialJobNo);
	}

	@RequestMapping(value = "updateJobCourier.json", method = RequestMethod.POST)
	@ResponseBody
	public void updateJobCourier(@RequestParam(name = "jobCourierId") Integer jobCourierId,
			@RequestParam(name = "courierId") Integer courierId,
			@RequestParam(name = "jobCourierTrackingNumber") String jobCourierTrackingNumber) {
		JobCourierLink jobCourier = jobCourierLinkServ.get(jobCourierId);
		if (jobCourier.getCourier().getCourierid() != courierId) {
			Courier courier = courierServ.get(courierId);
			jobCourier.setCourier(courier);
		}
		jobCourier.setTrackingNumber(jobCourierTrackingNumber);

		jobCourierLinkServ.merge(jobCourier);
	}

	@RequestMapping(value = "updateJobDelivery.json", method = RequestMethod.POST)
	@ResponseBody
	public DeliveryForm updateJobDelivery(@ModelAttribute("jobDeliveryForm") DeliveryForm form,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return form;
		}
		Job job = jobService.get(form.getJobId());
		job.setNumberOfPackages(form.getNumberOfPackages());
		job.setPackageType(form.getPackageType());
		job.setStorageArea(form.getStorageArea());
		jobService.save(job);
		return new DeliveryForm(job.getNumberOfPackages(), job.getPackageType(), job.getStorageArea(), job.getJobid());
	}

}