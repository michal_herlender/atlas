package org.trescal.cwms.core.jobs.jobitem.form;

import java.util.List;

import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;

public class JIThirdPartyForm extends JobItemForm
{
	private Integer actionOutcomeId;
	private boolean adjustment;
	private boolean calibration;
	private List<Delivery> deliveries;
	private boolean investigation;
	private List<Integer> otherItemsForTPReq;
	private boolean repair;
	private String tpInstruction;

	public Integer getActionOutcomeId()
	{
		return this.actionOutcomeId;
	}

	/**
	 * @return the deliveries
	 */
	public List<Delivery> getDeliveries()
	{
		return this.deliveries;
	}

	public List<Integer> getOtherItemsForTPReq()
	{
		return this.otherItemsForTPReq;
	}

	public String getTpInstruction()
	{
		return this.tpInstruction;
	}

	public boolean isAdjustment()
	{
		return this.adjustment;
	}

	public boolean isCalibration()
	{
		return this.calibration;
	}

	public boolean isInvestigation()
	{
		return this.investigation;
	}

	public boolean isRepair()
	{
		return this.repair;
	}

	public void setActionOutcomeId(Integer actionOutcomeId)
	{
		this.actionOutcomeId = actionOutcomeId;
	}

	public void setAdjustment(boolean adjustment)
	{
		this.adjustment = adjustment;
	}

	public void setCalibration(boolean calibration)
	{
		this.calibration = calibration;
	}

	/**
	 * @param deliveries the deliveries to set
	 */
	public void setDeliveries(List<Delivery> deliveries)
	{
		this.deliveries = deliveries;
	}

	public void setInvestigation(boolean investigation)
	{
		this.investigation = investigation;
	}

	public void setOtherItemsForTPReq(List<Integer> otherItemsForTPReq)
	{
		this.otherItemsForTPReq = otherItemsForTPReq;
	}

	public void setRepair(boolean repair)
	{
		this.repair = repair;
	}

	public void setTpInstruction(String tpInstruction)
	{
		this.tpInstruction = tpInstruction;
	}
}