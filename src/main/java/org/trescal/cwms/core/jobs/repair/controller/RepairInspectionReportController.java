package org.trescal.cwms.core.jobs.repair.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.repair.component.enums.RepairComponentStateEnum;
import org.trescal.cwms.core.jobs.repair.dto.form.RepairInspectionReportFormDTO;
import org.trescal.cwms.core.jobs.repair.dto.form.transformer.RepairInspectionReportFormTransformer;
import org.trescal.cwms.core.jobs.repair.dto.form.validator.RepairInspectionReportValidator;
import org.trescal.cwms.core.jobs.repair.operation.enums.RepairOperationStateEnum;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.db.RepairInspectionReportService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.core.workflow.entity.stategrouplink.db.StateGroupLinkService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME })
public class RepairInspectionReportController {

	@Autowired
	private RepairInspectionReportValidator validator;
	@Autowired
	private RepairInspectionReportService rirService;
	@Autowired
	private RepairInspectionReportFormTransformer rirTransformer;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private StateGroupLinkService stateGroupLinkService;
	@Autowired
	private UserService userService;

	@InitBinder("rirForm")
	protected void initBinder(WebDataBinder binder) {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.setValidator(validator);
	}

	@RequestMapping(value = "/repairinspectionreport.htm", method = RequestMethod.GET)
	public ModelAndView getRepairInspection(@RequestParam(name = "jobitemid", required = false) Integer jobItemId,
			@RequestParam(name = "rirId", required = false) Integer rirId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Model model) {
		referenceInitData(jobItemId, rirId, model, subdivDto.getKey());
		return new ModelAndView("trescal/core/jobs/repair/repairinspectionreport");
	}

	private void referenceInitData(Integer jobItemId, Integer rirId, Model model, Integer currentSubdivId) {
		RepairInspectionReportFormDTO form;
		if (rirId != null) {
			RepairInspectionReport rir = rirService.get(rirId);
			form = rirTransformer.transform(rir);
			form.setJobItemId(rir.getJi().getJobItemId());
		} else {
			// try to find the rir by jobitem and subdiv
			RepairInspectionReport rir = rirService.getRirByJobItemIdAndSubdivId(jobItemId, currentSubdivId);
			if (rir != null) {
				form = rirTransformer.transform(rir);
				form.setJobItemId(rir.getJi().getJobItemId());
			} else {
				// if none is found, this probably a creation
				form = new RepairInspectionReportFormDTO();
				form.setJobItemId(jobItemId);
				form.setJi(jobItemService.findJobItem(jobItemId));
			}

		}
		if (form.getJi() != null) {
			if (form.getJi().getNextWorkReq() != null && form.getJi().getNextWorkReq().getWorkRequirement() != null
					&& form.getJi().getNextWorkReq().getWorkRequirement().getRequirement() != null
					&& !form.getJi().getNextWorkReq().getWorkRequirement().getRequirement().isEmpty())
				form.setInternalInspection(form.getJi().getNextWorkReq().getWorkRequirement().getRequirement());
			if (form.getJi().getContactReviewItems() != null && !form.getJi().getContactReviewItems().isEmpty()
					&& form.getJi().getContactReviewItems().first().getReview() != null
					&& form.getJi().getContactReviewItems().first().getReview().getComments() != null
					&& !form.getJi().getContactReviewItems().first().getReview().getComments().isEmpty())
				form.setClientDescription(form.getJi().getContactReviewItems().first().getReview().getComments());
		}

		model.addAttribute("rirForm", form);
		model.addAttribute("disabled", form.getReviewedByCsr() != null && form.getReviewedByCsr());
		referenceData(model);
	}
	
	private void referenceData(Model model) {
		model.addAttribute("actionPerformed", false);
		// add rir operation status using filter
		model.addAttribute("operationStatus", Arrays.asList(RepairOperationStateEnum.values()).stream().filter(e -> e.isRirStatus()).collect(Collectors.toList()));
		// add rir component status using filter
		model.addAttribute("componentStatus", Arrays.asList(RepairComponentStateEnum.values()).stream().filter(e -> e.isRirStatus()).collect(Collectors.toList()));
	}

	@RequestMapping(value = "/repairinspectionreport.htm", method = RequestMethod.POST)
	public ModelAndView doDefaultPost(@Valid @ModelAttribute("rirForm") RepairInspectionReportFormDTO form,
			BindingResult result, @RequestParam(name = "jobitemid", required = false) Integer jobItemId,
			@RequestParam(name = "rirId", required = false) Integer rirId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Model model) {
		referenceInitData(jobItemId, rirId, model, subdivDto.getKey());
		return new ModelAndView("trescal/core/jobs/repair/repairinspectionreport");
	}

	@RequestMapping(value = "/repairinspectionreport.htm", method = RequestMethod.POST, params = { "!saveRcr",
			"!saveAndValidateRir", "saveRir" })
	public ModelAndView saveRepairInspection(@Valid @ModelAttribute("rirForm") RepairInspectionReportFormDTO form,
			BindingResult result, @RequestParam(name = "jobitemid", required = false) Integer jobItemId,
			@RequestParam(name = "rirId", required = false) Integer rirId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Model model) {
		if (result.hasErrors()) {
			referenceData(model);
			return new ModelAndView("trescal/core/jobs/repair/repairinspectionreport");
		}
		else {
			JobItem ji = this.jobItemService.findJobItem(jobItemId);
			Contact currentContact = this.userService.get(username).getCon();
			if(!this.rirService.isAccreditedToPerformRirInitialization(ji.getNextWorkReq(), currentContact)) {
				result.rejectValue("isAccredited", "repairinpectionreport.authorization.warning1", "You are not authorized to perform the repair inspection report initialisation, you need one of those authorization levels (SUPERVISED,PERFORM, APPROVE or SIGN)");
				form.setIsAccredited(false);
				referenceData(model);
				return new ModelAndView("trescal/core/jobs/repair/repairinspectionreport");
			}
		}
		this.rirService.saveRepairInspection(form, username, subdivDto);
		referenceInitData(jobItemId, rirId, model, subdivDto.getKey());
		model.addAttribute("actionPerformed", true);
		return new ModelAndView("trescal/core/jobs/repair/repairinspectionreport");
	}

	@RequestMapping(value = "/repairinspectionreport.htm", method = RequestMethod.POST, params = { "!saveRcr",
			"saveAndValidateRir", "!saveRir", "!reviewAndSaveRir" })
	public ModelAndView saveAndValidateRepairInspection(
			@Valid @ModelAttribute("rirForm") RepairInspectionReportFormDTO form, BindingResult result,
			@RequestParam(name = "rirId", required = false) Integer rirId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Model model) {
		JobItem ji = jobItemService.findJobItem(form.getJobItemId());
		if (result.hasErrors()) {
			referenceData(model);
			return new ModelAndView("trescal/core/jobs/repair/repairinspectionreport");
		}
		
		if ((form.getCompletedByTechnician() == null || !form.getCompletedByTechnician())
				&& stateGroupLinkService.containsStateGroup(ji.getState().getGroupLinks(), StateGroup.LAB)) {
			Contact currentContact = this.userService.get(username).getCon();
			if(!this.rirService.isAccreditedToPerformRirTechCompletion(ji.getNextWorkReq(), currentContact)) {
				result.rejectValue("isAccredited", "repairinpectionreport.authorization.warning2", "You are not authorized to perform the repair inspection report completion, you need one of those authorization levels (APPROVE or SIGN)");
				form.setIsAccredited(false);
				referenceData(model);
				return new ModelAndView("trescal/core/jobs/repair/repairinspectionreport");
			}
			rirService.validateByTechnician(form, username, subdivDto.getKey());
		}
		else {
			Contact currentContact = this.userService.get(username).getCon();
			if(!this.rirService.isAccreditedToPerformRirManagerValidation(ji.getNextWorkReq(), currentContact)) {
				result.rejectValue("isAccredited", "repairinpectionreport.authorization.warning3", "You are not authorized to perform the repair inspection report validation, you need to have APPROVE authorization level");
				form.setIsAccredited(false);
				referenceData(model);
				return new ModelAndView("trescal/core/jobs/repair/repairinspectionreport");
			}
			rirService.validateByManager(form, username, subdivDto.getKey());
		}

		referenceInitData(null, rirId, model, subdivDto.getKey());
		model.addAttribute("actionPerformed", true);
		return new ModelAndView("trescal/core/jobs/repair/repairinspectionreport");
	}

	@RequestMapping(value = "/repairinspectionreport.htm", method = RequestMethod.POST, params = { "!saveRcr",
			"!saveAndValidateRir", "!saveRir", "reviewAndSaveRir" })
	public ModelAndView reviewRepairInspection(
			@Valid @ModelAttribute("rirForm") RepairInspectionReportFormDTO form, BindingResult result,
			@RequestParam(name = "rirId", required = false) Integer rirId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Model model) {
		JobItem ji = jobItemService.findJobItem(form.getJobItemId());
		if (result.hasErrors()) {
			referenceData(model);
			return new ModelAndView("trescal/core/jobs/repair/repairinspectionreport");
		}
		else {
			Contact currentContact = this.userService.get(username).getCon();
			if(!this.rirService.isAccreditedToPerformRirManagerValidation(ji.getNextWorkReq(), currentContact)) {
				result.rejectValue("isAccredited", "repairinpectionreport.authorization.warning3", "You are not authorized to perform the repair inspection report validation, you need to have APPROVE authorization level");
				form.setIsAccredited(false);
				referenceData(model);
				return new ModelAndView("trescal/core/jobs/repair/repairinspectionreport");
			}
		}
		
		rirService.reviewByCsr(form, username);
		model.addAttribute("disableRcrTab", false);

		referenceInitData(null, rirId, model, subdivDto.getKey());
		model.addAttribute("actionPerformed", true);
		return new ModelAndView("trescal/core/jobs/repair/repairinspectionreport");
	}
}