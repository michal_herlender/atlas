package org.trescal.cwms.core.jobs.jobitem.entity.faultreport;

import java.io.File;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.failurereport.enums.FailureReportApprovalTypeEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportFinalOutcome;
import org.trescal.cwms.core.failurereport.enums.FailureReportRecommendationsEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportSourceEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportStateEnum;
import org.trescal.cwms.core.failurereport.enums.FaultReportVersionEnum;
import org.trescal.cwms.core.jobs.jobitem.entity.accessory.ItemAccessory;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportAction;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportDescription;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportDetailComparator;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreportdetail.FaultReportInstruction;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

import lombok.Setter;

@Entity
@Table(name = "faultreport")
@Setter
public class FaultReport extends Allocated<Subdiv> implements ComponentEntity {

	private int faultRepId;
	private JobItem jobItem;
	private String faultReportNumber;
	private FaultReportVersionEnum version;

	private Boolean sendToClient;
	private FailureReportApprovalTypeEnum approvalType;
	private Boolean noCostingRequired;
	private Boolean outcomePreselected;

	private boolean awaitingFinalisation;

	private BigDecimal cost;
	private BigDecimal valueInBusinessCompanyCurrency;
	private SupportedCurrency businessCompanyCurrency;
	private Integer estAdjustTime;
	private Integer estRepairTime;
	private Integer leadTime;
	private Boolean requireDeliveryToClient;

	/* new fields */
	private Date issueDate;
	private String trescalRef;
	private List<ItemAccessory> missingAccessories;
	private FailureReportSourceEnum source;

	/* technician infos */
	private Contact technician;
	private String technicianComments;
	private List<FailureReportStateEnum> states;
	private String otherState;

	/* manager infos */
	private Boolean managerValidation;
	private Contact managerValidationBy;
	private Date managerValidationOn;
	private String managerComments;
	private Integer estDeliveryTime;
	private Boolean operationByTrescal;
	private Boolean operationInLaboratory;
	private Boolean clientResponseNeeded;
	private List<FailureReportRecommendationsEnum> recommendations;
	private Boolean dispensation;
	private String dispensationComments;

	/* client infos */
	private Boolean clientApproval;
	private Contact clientApprovalBy;
	private Date clientApprovalOn;
	private String clientComments;
	private FailureReportRecommendationsEnum clientOutcome;
	private String clientAlias;

	/*
	 * FR final outcome : previously the outcome use to be selected using the
	 * workflow 'new activity', now is via the failure report view
	 */
	private FailureReportFinalOutcome finalOutcome;

	// interface / abstract fields
	private File directory;
	private List<Email> sentEmails;

	// Data used by OLD fault reports maintained in system
	private Boolean preCert;
	private Boolean preRecord;
	private boolean ber;
	private Contact berCheckBy;
	private String berCheckComments;
	private Date berCheckOn;
	private Contact berSetBy;
	private Date berSetOn;
	private Set<FaultReportAction> actions;
	private Set<FaultReportDescription> descriptions;
	private Set<FaultReportInstruction> instructions;

	public FaultReport() {
		this.ber = false;
		this.awaitingFinalisation = true;
		this.operationByTrescal = true;
		this.operationInLaboratory = true;
		this.outcomePreselected = false;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bercheckbyid")
	public Contact getBerCheckBy() {
		return this.berCheckBy;
	}

	@Length(max = 500)
	@Column(name = "bercheckcomment", length = 500)
	public String getBerCheckComments() {
		return this.berCheckComments;
	}

	@Column(name = "bercheckon", columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getBerCheckOn() {
		return this.berCheckOn;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bersetbyid")
	public Contact getBerSetBy() {
		return this.berSetBy;
	}

	@Column(name = "berseton", columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getBerSetOn() {
		return this.berSetOn;
	}

	/**
	 * @return the cost
	 */
	@Column(name = "cost", nullable = true, precision = 10, scale = 2)
	public BigDecimal getCost() {
		return this.cost;
	}

	/**
	 * @return the descriptions
	 */
	@OneToMany(mappedBy = "faultRep", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@SortComparator(FaultReportDetailComparator.class)
	public Set<FaultReportDescription> getDescriptions() {
		return this.descriptions;
	}

	@Column(name = "estadjusttime")
	@Type(type = "int")
	public Integer getEstAdjustTime() {
		return this.estAdjustTime;
	}

	/**
	 * @return the estTime
	 */
	@Column(name = "estrepairtime")
	@Type(type = "int")
	public Integer getEstRepairTime() {
		return this.estRepairTime;
	}

	/**
	 * @return the faultRepId
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "faultrepid", nullable = false, unique = true)
	@Type(type = "int")
	public int getFaultRepId() {
		return this.faultRepId;
	}

	@OneToMany(mappedBy = "faultRep", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@SortComparator(FaultReportDetailComparator.class)
	public Set<FaultReportInstruction> getInstructions() {
		return this.instructions;
	}

	/**
	 * @return the jobItem
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid")
	public JobItem getJobItem() {
		return this.jobItem;
	}

	/**
	 * @return the leadTime
	 */
	@Column(name = "leadtime")
	public Integer getLeadTime() {
		return this.leadTime;
	}

	@NotNull
	@Column(name = "awaitingfinalisation", nullable = false, columnDefinition = "bit")
	public boolean isAwaitingFinalisation() {
		return this.awaitingFinalisation;
	}

	@NotNull
	@Column(name = "ber", nullable = false, columnDefinition = "bit")
	public boolean isBer() {
		return this.ber;
	}

	@Column(name = "currencyvalue")
	public BigDecimal getValueInBusinessCompanyCurrency() {
		return this.valueInBusinessCompanyCurrency;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "currencyid", nullable = true)
	public SupportedCurrency getBusinessCompanyCurrency() {
		return businessCompanyCurrency;
	}

	@Length(max = 30)
	@Column(name = "faultreportnumber", nullable = true, columnDefinition = "varchar(30)")
	public String getFaultReportNumber() {
		return faultReportNumber;
	}

	@Column(name = "estdeliverytime")
	public Integer getEstDeliveryTime() {
		return estDeliveryTime;
	}

	@Column(name = "approvaltype")
	@Enumerated(EnumType.STRING)
	public FailureReportApprovalTypeEnum getApprovalType() {
		return approvalType;
	}

	@Column(name = "clientapproval")
	public Boolean getClientApproval() {
		return clientApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "clientapprovalbyid", foreignKey = @ForeignKey(name = "FK_faultreport_clientapprovalbyid"))
	public Contact getClientApprovalBy() {
		return clientApprovalBy;
	}

	@Column(name = "clientapprovalon", columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getClientApprovalOn() {
		return clientApprovalOn;
	}
	
	@Size(max = 500)
	@Column(name = "clientcomments", columnDefinition = "varchar(500)")
	public String getClientComments() {
		return clientComments;
	}

	@Column(name = "operationbytrescal")
	public Boolean getOperationByTrescal() {
		return operationByTrescal;
	}

	@Column(name = "operationinlaboratory")
	public Boolean getOperationInLaboratory() {
		return operationInLaboratory;
	}

	@Column(name = "clientresponseneeded")
	public Boolean getClientResponseNeeded() {
		return clientResponseNeeded;
	}

	@ElementCollection(targetClass = FailureReportStateEnum.class)
	@Enumerated(EnumType.STRING)
	@CollectionTable(name = "faultreportstate", joinColumns = @JoinColumn(name = "faultreportid", referencedColumnName = "faultrepid"))
	@Column(name = "state")
	public List<FailureReportStateEnum> getStates() {
		return states;
	}

	@ElementCollection(targetClass = FailureReportRecommendationsEnum.class)
	@Enumerated(EnumType.STRING)
	@CollectionTable(name = "faultreportrecommendation", joinColumns = @JoinColumn(name = "faultreportid", referencedColumnName = "faultrepid"))
	@Column(name = "recommendation")
	public List<FailureReportRecommendationsEnum> getRecommendations() {
		return this.recommendations;
	}

	@Column(name = "version")
	@Enumerated(EnumType.STRING)
	public FaultReportVersionEnum getVersion() {
		return version;
	}

	@Column(name = "techniciancomments", length = 2000)
	public String getTechnicianComments() {
		return technicianComments;
	}

	@Column(name = "issuedate")
	public Date getIssueDate() {
		return issueDate;
	}

	@OneToMany(mappedBy = "faultReport", cascade = { CascadeType.ALL })
	public List<ItemAccessory> getMissingAccessories() {
		return missingAccessories;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "technicianid", foreignKey = @ForeignKey(name = "FK_faultreport_technicianid"))
	public Contact getTechnician() {
		return technician;
	}

	@Column(name = "managervalidation")
	public Boolean getManagerValidation() {
		return managerValidation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "managervalidationbyid", foreignKey = @ForeignKey(name = "FK_faultreport_managervalidationbyid"))
	public Contact getManagerValidationBy() {
		return managerValidationBy;
	}

	@Column(name = "managervalidationon")
	public Date getManagerValidationOn() {
		return managerValidationOn;
	}

	@Column(name = "managercomments")
	public String getManagerComments() {
		return managerComments;
	}

	@Column(name = "otherstate")
	public String getOtherState() {
		return otherState;
	}

	@Column(name = "trescalref")
	public String getTrescalRef() {
		return trescalRef;
	}

	@Column(name = "clientoutcome")
	@Enumerated(EnumType.STRING)
	public FailureReportRecommendationsEnum getClientOutcome() {
		return clientOutcome;
	}

	@Column(name = "nocostingrequired", columnDefinition = "bit")
	public Boolean getNoCostingRequired() {
		return noCostingRequired;
	}

	@Column(name = "outcomepreselected", columnDefinition = "bit")
	public Boolean getOutcomePreselected() {
		return outcomePreselected;
	}

	@Column(name = "sendtoclient", columnDefinition = "bit")
	public Boolean getSendToClient() {
		return sendToClient;
	}

	@Column(name = "dispensation", columnDefinition = "bit")
	public Boolean getDispensation() {
		return dispensation;
	}

	@Column(name = "dispensationcomments")
	public String getDispensationComments() {
		return dispensationComments;
	}

	@OneToMany(mappedBy = "faultRep", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@SortComparator(FaultReportDetailComparator.class)
	public Set<FaultReportAction> getActions() {
		return this.actions;
	}

	@Column(name = "precert", nullable = false, columnDefinition = "bit")
	public Boolean getPreCert() {
		return this.preCert;
	}

	@Column(name = "prerecord", nullable = false, columnDefinition = "bit")
	public Boolean getPreRecord() {
		return this.preRecord;
	}

	@Column(name = "source")
	@Enumerated(EnumType.STRING)
	public FailureReportSourceEnum getSource() {
		return source;
	}

	@Column(name = "finaloutcome")
	@Enumerated(EnumType.STRING)
	public FailureReportFinalOutcome getFinalOutcome() {
		return finalOutcome;
	}

	@Column(name = "requiredeliverytoclient", columnDefinition = "bit")
	public Boolean getRequireDeliveryToClient() {
		return requireDeliveryToClient;
	}

	@Override
	@Transient
	public Contact getDefaultContact() {
		// TODO Auto-generated method stub
		return this.getJobItem().getJob().getCon();
	}

	@Override
	@Transient
	public File getDirectory() {
		return this.directory;
	}

	@Override
	@Transient
	public String getIdentifier() {
		return "Failure Report  - " + this.getFaultReportNumber();
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity() {
		return this.getJobItem().getJob();
	}

	@Override
	@Transient
	public List<Email> getSentEmails() {
		return this.sentEmails;
	}

	@Override
	@Transient
	public boolean isAccountsEmail() {
		return false;
	}

	@Column(name = "clientalias")
	public String getClientAlias() {
		return clientAlias;
	}

}