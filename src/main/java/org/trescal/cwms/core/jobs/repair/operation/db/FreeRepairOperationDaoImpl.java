package org.trescal.cwms.core.jobs.repair.operation.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.repair.operation.FreeRepairOperation;

@Repository("FreeRepairOperationDao")
public class FreeRepairOperationDaoImpl extends BaseDaoImpl<FreeRepairOperation, Integer> implements FreeRepairOperationDao {
	
	@Override
	protected Class<FreeRepairOperation> getEntity() {
		return FreeRepairOperation.class;
	}

	@Override
	public void refresh(FreeRepairOperation fro) {
		getEntityManager().refresh(fro);
	}

}