package org.trescal.cwms.core.jobs.jobitem.entity.calloffitem;

import java.util.Comparator;

import org.trescal.cwms.core.jobs.jobitem.entity.calloffitem.dto.CallOffItemDTO;

public class CallOffItemDTOComparator implements Comparator<CallOffItemDTO> {

	public int compare(CallOffItemDTO callOffItem0, CallOffItemDTO callOffItem1) {
		String name0 = callOffItem0.getConame();
		String name1 = callOffItem1.getConame();
		int result = name0.compareTo(name1);
		if (result == 0) {
			String jobNo0 = callOffItem0.getJobno();
			String jobNo1 = callOffItem1.getJobno();
			result = jobNo0.compareTo(jobNo1);
			if (result == 0) {
				int itemNo0 = callOffItem0.getItemNo();
				int itemNo1 = callOffItem1.getItemNo();
				result = itemNo0 - itemNo1;
				if (result == 0) {
					result = callOffItem0.getCalloffId() - callOffItem1.getCalloffId();
				}
			}
		}
		return result;
	}

}
