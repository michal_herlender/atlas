package org.trescal.cwms.core.asset.form;

import javax.validation.constraints.NotNull;

import lombok.Setter;

@Setter
public class NewAssetFromInstrumentForm {
	
	@NotNull(message = "A barecode must be specified")
	private Integer plantId;
	private Integer coid;

	
	public Integer getPlantId() {
		return plantId;
	}

	public Integer getCoid() {
		return coid;
	}
}
