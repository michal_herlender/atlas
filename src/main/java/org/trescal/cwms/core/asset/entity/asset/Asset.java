package org.trescal.cwms.core.asset.entity.asset;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.asset.entity.AssetClassType;
import org.trescal.cwms.core.asset.entity.assetnote.AssetNote;
import org.trescal.cwms.core.asset.entity.assetstatus.AssetStatus;
import org.trescal.cwms.core.asset.entity.assettype.AssetType;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "asset")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "assettype", discriminatorType = DiscriminatorType.STRING)
public abstract class Asset extends Auditable implements NoteAwareEntity {
	private int assetId;
	private String assetNo;
	private Contact custodian;
	private Department dept;
	private LocalDate editableAddedOn;
	private String name;
	private Set<AssetNote> notes;
	private Contact owner;
	private BigDecimal price;
	private Contact primaryUser;
	private LocalDate purchasedOn;
	private int quantity;
	private AssetStatus status;
	private Contact systemAddedBy;
	private LocalDate systemAddedOn;
	private AssetType type;

	public Asset() {
		// set defaults for new asset
		this.quantity = 1;
		this.editableAddedOn = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getAssetId()
	{
		return this.assetId;
	}

	@NotNull
	@Column(name = "assetno", length = 30, nullable = false)
	@Length(max = 30)
	public String getAssetNo()
	{
		return this.assetNo;
	}

	/**
	 * @return the asset type
	 */
	@Transient
	abstract public AssetClassType getAssettype();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "custodianid")
	public Contact getCustodian()
	{
		return this.custodian;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deptid")
	@NotNull(message = "A department must be specified")
	public Department getDept() {
		return this.dept;
	}

	@Column(name = "editableaddedon", columnDefinition = "date")
	public LocalDate getEditableAddedOn() {
		return this.editableAddedOn;
	}

	public void setEditableAddedOn(LocalDate editableAddedOn) {
		this.editableAddedOn = editableAddedOn;
	}

	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "asset")
	@SortComparator(NoteComparator.class)
	public Set<AssetNote> getNotes() {
		return this.notes;
	}

	/**
	 * @return the opposite asset type to change the type while creating new one
	 */
	@Transient
	abstract AssetClassType getOppassettype();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ownerid")
	@NotNull(message = "An owner must be specified")
	public Contact getOwner() {
		return this.owner;
	}

	@Column(name = "assetname", length = 150)
	@Length(max = 150)
	public String getName() {
		return this.name;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "primaryuserid")
	public Contact getPrimaryUser() {
		return this.primaryUser;
	}

	@Transient
	public Integer getPrivateActiveNoteCount()
	{
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount()
	{
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount() {
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount() {
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}

	@Column(name = "price", precision = 10, scale = 2)
	public BigDecimal getPrice() {
		return this.price;
	}

	@Column(name = "qty", nullable = false)
	@Type(type = "int")
	public int getQuantity() {
		return this.quantity;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid")
	@NotNull(message = "A status must be specified")
	public AssetStatus getStatus() {
		return this.status;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sysaddedbyid")
	public Contact getSystemAddedBy() {
		return this.systemAddedBy;
	}

	@Column(name = "purchasedon", columnDefinition = "date")
	public LocalDate getPurchasedOn() {
		return this.purchasedOn;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "typeid")
	@NotNull(message = "A type must be specified")
	public AssetType getType() {
		return this.type;
	}

	public void setAssetId(int assetId)
	{
		this.assetId = assetId;
	}

	public void setAssetNo(String assetNo)
	{
		this.assetNo = assetNo;
	}

	public void setCustodian(Contact custodian) {
		this.custodian = custodian;
	}

	public void setDept(Department dept) {
		this.dept = dept;
	}

	public void setPurchasedOn(LocalDate purchasedOn) {
		this.purchasedOn = purchasedOn;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNotes(Set<AssetNote> notes)
	{
		this.notes = notes;
	}

	public void setOwner(Contact owner)
	{
		this.owner = owner;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public void setPrimaryUser(Contact primaryUser) {
		this.primaryUser = primaryUser;
	}

	@Column(name = "sysaddedon", columnDefinition = "date")
	public LocalDate getSystemAddedOn() {
		return this.systemAddedOn;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setStatus(AssetStatus status) {
		this.status = status;
	}

	public void setSystemAddedBy(Contact systemAddedBy) {
		this.systemAddedBy = systemAddedBy;
	}

	public void setSystemAddedOn(LocalDate systemAddedOn) {
		this.systemAddedOn = systemAddedOn;
	}

	public void setType(AssetType type) {
		this.type = type;
	}
}