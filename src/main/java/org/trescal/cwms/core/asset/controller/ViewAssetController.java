package org.trescal.cwms.core.asset.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.asset.entity.asset.Asset;
import org.trescal.cwms.core.asset.entity.asset.db.AssetService;
import org.trescal.cwms.core.asset.entity.assetnote.AssetNote;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller @IntranetController
public class ViewAssetController
{
	@Autowired
	private AssetService assetServ;

	@RequestMapping(value="/viewasset.htm", method=RequestMethod.GET)
	protected ModelAndView handleRequestInternal(@RequestParam(value = "id", required = false, defaultValue = "0") int id) throws Exception
	{
		Asset asset = this.assetServ.findAsset(id);

		if ((asset == null) || (id == 0))
		{
			throw new Exception("Asset could not be found");
		}

		Map<String, Object> modelMap = new HashMap<String, Object>();

		modelMap.put("asset", asset);

		modelMap.put("privateOnlyNotes", AssetNote.privateOnly);

		return new ModelAndView("trescal/core/asset/viewasset", "command", modelMap);
	}
}
