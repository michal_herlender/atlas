package org.trescal.cwms.core.asset.entity.assetstatus.db;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.trescal.cwms.core.asset.entity.assetstatus.AssetStatus;
import org.trescal.cwms.core.system.entity.translation.Translation;

public class AssetStatusServiceImpl implements AssetStatusService
{
	private AssetStatusDao assetStatusDao;

	public void deleteAssetStatus(AssetStatus assetstatus)
	{
		this.assetStatusDao.remove(assetstatus);
	}

	public AssetStatus findAssetStatus(int id)
	{
		return this.assetStatusDao.find(id);
	}

	public List<AssetStatus> getAllAssetStatuses()
	{
		return this.assetStatusDao.findAll();
	}

	public void insertAssetStatus(AssetStatus AssetStatus)
	{
		this.assetStatusDao.persist(AssetStatus);
	}

	public void setAssetStatusDao(AssetStatusDao assetStatusDao)
	{
		this.assetStatusDao = assetStatusDao;
	}
	
	/*
	 * Meant to be run once as a utility method, will scan all existing assetstatus descriptions, 
	 * and update it to populate translations for specified locale.
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void populateDescriptionTranslationsForSystemLocale(Locale locale) {
		//ItemStatus 
		List<AssetStatus> listdescriptions = assetStatusDao.findAll();
		for (AssetStatus description : listdescriptions) {
			Set<Translation> translations = description.getTranslations();
			// If there is no translation for specified locale, add it
			Translation translation = assetStatusDao.getTranslationForLocaleOrNull(description, locale);
			if (translation == null) {
				translation = new Translation();
				translation.setLocale(locale);
				translation.setTranslation(description.getName());
				translations.add(translation);
			}
		}
	}
}