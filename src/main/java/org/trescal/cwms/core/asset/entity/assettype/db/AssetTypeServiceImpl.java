package org.trescal.cwms.core.asset.entity.assettype.db;

import java.util.List;

import org.trescal.cwms.core.asset.entity.assettype.AssetType;

public class AssetTypeServiceImpl implements AssetTypeService
{
	private AssetTypeDao assetTypeDao;

	public void deleteAssetType(AssetType assettype)
	{
		this.assetTypeDao.remove(assettype);
	}

	public AssetType findAssetType(int id)
	{
		return this.assetTypeDao.find(id);
	}

	public AssetType findAssetTypeForInstruments()
	{
		return this.assetTypeDao.findAssetTypeForInstruments();
	}

	public List<AssetType> getAllAssetTypes()
	{
		return this.assetTypeDao.findAll();
	}

	public void insertAssetType(AssetType AssetType)
	{
		this.assetTypeDao.persist(AssetType);
	}

	public void setAssetTypeDao(AssetTypeDao assetTypeDao)
	{
		this.assetTypeDao = assetTypeDao;
	}

	public void updateAssetType(AssetType AssetType)
	{
		this.assetTypeDao.update(AssetType);
	}
}