package org.trescal.cwms.core.asset.entity.assettype;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.system.entity.translation.Translation;


@Entity
@Table(name = "assettype")
public class AssetType
{
	private String description;
	private int id;
	private boolean instType;
	private String name;
	private Set<Translation> translations;

	@Column(name = "description", length = 200, nullable = true)
	@Length(max = 200)
	public String getDescription()
	{
		return this.description;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Column(name = "name", length = 100, nullable = true)
	@Length(max = 100)
	public String getName()
	{
		return this.name;
	}

	@Column(name = "insttype", nullable = false, columnDefinition="bit")
	public boolean isInstType()
	{
		return this.instType;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setInstType(boolean instType)
	{
		this.instType = instType;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="assettypetranslation", joinColumns=@JoinColumn(name="id"))
	public Set<Translation> getTranslations() {
		return translations;
	}

	public void setTranslations(Set<Translation> translations) {
		this.translations = translations;
	}
}