package org.trescal.cwms.core.asset.entity.assetstatus.db;

import java.util.Locale;

import org.trescal.cwms.core.asset.entity.assetstatus.AssetStatus;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.translation.Translation;

public interface AssetStatusDao extends BaseDao<AssetStatus, Integer> {
	
	// Utility method to get a translation for the description in the specified language
	public Translation getTranslationForLocaleOrNull(AssetStatus description, Locale locale);
}