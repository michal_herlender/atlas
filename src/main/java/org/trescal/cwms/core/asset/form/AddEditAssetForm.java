package org.trescal.cwms.core.asset.form;

import javax.validation.Valid;

import org.trescal.cwms.core.asset.entity.asset.Asset;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AddEditAssetForm
{
	@Valid
	private Asset asset;
	private Integer custodianId;
	private Integer deptId;
	private Integer ownerId;
	private boolean persisted;
	private Integer physLocId;
	private Integer primaryUserId;
	private Integer statusId;
	private Integer supplierId;
	private String supplierName;
	private Integer typeId;

}
