package org.trescal.cwms.core.asset.entity.asset.db;

import org.trescal.cwms.core.asset.entity.asset.Asset;
import org.trescal.cwms.core.asset.form.AssetSearchForm;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface AssetDao extends BaseDao<Asset, Integer> {

	PagedResultSet<Asset> searchAssets(AssetSearchForm form, PagedResultSet<Asset> prs);
}