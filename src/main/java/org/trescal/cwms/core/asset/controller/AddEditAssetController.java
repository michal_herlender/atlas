package org.trescal.cwms.core.asset.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.asset.entity.AssetClassType;
import org.trescal.cwms.core.asset.entity.asset.Asset;
import org.trescal.cwms.core.asset.entity.asset.NetworkAsset;
import org.trescal.cwms.core.asset.entity.asset.PhysicalAsset;
import org.trescal.cwms.core.asset.entity.asset.db.AssetService;
import org.trescal.cwms.core.asset.entity.assetstatus.db.AssetStatusService;
import org.trescal.cwms.core.asset.entity.assettype.db.AssetTypeService;
import org.trescal.cwms.core.asset.form.AddEditAssetForm;
import org.trescal.cwms.core.asset.form.AddEditAssetValidator;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV,
		Constants.SESSION_ATTRIBUTE_COMPANY })
public class AddEditAssetController {

	@Autowired
	private AssetService assetServ;
	@Autowired
	private AssetStatusService statusServ;
	@Autowired
	private AssetTypeService typeServ;
	@Autowired
	private AddEditAssetValidator validator;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private DepartmentService deptServ;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private LocationService locServ;
	@Autowired
	private MessageSource messages;

	public static final String FORM_NAME = "form";

	@InitBinder(FORM_NAME)
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}

	@ModelAttribute(FORM_NAME)
	protected AddEditAssetForm formBackingObject(
			@RequestParam(value = "type", required = false, defaultValue = "physical") String type,
			@RequestParam(value = "plantid", required = false, defaultValue = "0") int plantId,
			@RequestParam(value = "assetid", required = false, defaultValue = "0") int assetId) throws Exception {
		AddEditAssetForm form = new AddEditAssetForm();
		if (assetId != 0) {
			Asset asset = this.assetServ.findAsset(assetId);
			form.setTypeId(asset.getType().getId());
			form.setStatusId(asset.getStatus().getId());
			form.setOwnerId(asset.getOwner().getPersonid());
			form.setCustodianId((asset.getCustodian() == null) ? null : asset.getCustodian().getPersonid());
			form.setPrimaryUserId((asset.getPrimaryUser() == null) ? null : asset.getPrimaryUser().getPersonid());
			form.setDeptId(asset.getDept().getDeptid());
			if (asset.getAssettype().equals(AssetClassType.PHYSICAL)) {
				PhysicalAsset physicalAsset = (PhysicalAsset) asset;
				form.setPhysLocId((physicalAsset.getPhysicalLocation() == null) ? null
						: physicalAsset.getPhysicalLocation().getLocationid());
				form.setSupplierId(
						(physicalAsset.getSupplier() == null) ? null : physicalAsset.getSupplier().getCoid());
			}
			form.setAsset(asset);
			form.setPersisted(true);
		} else {
			if (type.equalsIgnoreCase("network")) {
				NetworkAsset asset = new NetworkAsset();
				form.setAsset(asset);
			} else {
				PhysicalAsset asset = new PhysicalAsset();
				if (plantId != 0) {
					asset.setInst(this.instServ.get(plantId));
					asset.setType(this.typeServ.findAssetTypeForInstruments());
					form.setTypeId(asset.getType().getId());
				}
				form.setAsset(asset);
			}
			form.setPersisted(false);
		}
		return form;
	}

	private void onBind(AddEditAssetForm form) {
		Asset asset = form.getAsset();
		if (!form.isPersisted()) {
			asset.setAssetNo(""); // To pass validation until created
		}
		asset.setType((form.getTypeId() == null) ? null : this.typeServ.findAssetType(form.getTypeId()));
		asset.setStatus((form.getStatusId() == null) ? null : this.statusServ.findAssetStatus(form.getStatusId()));
		asset.setOwner((form.getOwnerId() == 0) ? null : this.conServ.get(form.getOwnerId()));
		asset.setCustodian((form.getCustodianId() == null) ? null : this.conServ.get(form.getCustodianId()));
		asset.setPrimaryUser((form.getPrimaryUserId() == null) ? null : this.conServ.get(form.getPrimaryUserId()));
		asset.setDept((form.getDeptId() == null) ? null : this.deptServ.get(form.getDeptId()));
		if (asset instanceof PhysicalAsset) {
			((PhysicalAsset) asset)
					.setPhysicalLocation((form.getPhysLocId() == null) ? null : this.locServ.get(form.getPhysLocId()));
			((PhysicalAsset) asset)
					.setSupplier((form.getSupplierId() == null) ? null : this.compServ.get(form.getSupplierId()));
		}
	}

	@RequestMapping(value = "/addeditasset.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(FORM_NAME) AddEditAssetForm form, BindingResult bindingResult, Locale locale)
			throws Exception {
		onBind(form);
		validator.validate(form, bindingResult);
		if (bindingResult.hasErrors())
			return referenceData(companyDto, locale);
		else {
			Asset asset = this.assetServ.addEditAsset(form, username, subdivDto.getKey());
			return new ModelAndView(new RedirectView("/viewasset.htm?id=" + asset.getAssetId(), true));
		}
	}

	@RequestMapping(value = "/addeditasset.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto, Locale locale)
			throws Exception {
		Map<String, Object> refData = new HashMap<>();
		ContactKeyValue defaultDto = new ContactKeyValue(0,
				"-- " + messages.getMessage("system.selectcontact", null, locale) + " --");
		refData.put("statuses", this.statusServ.getAllAssetStatuses());
		refData.put("types", this.typeServ.getAllAssetTypes());
		refData.put("depts", this.deptServ.getAll());
		refData.put("companylocations", this.locServ.getAllCompanyLocationsActive(companyDto.getKey()));
		refData.put("companycontacts", conServ.getContactDtoList(companyDto.getKey(), null, true, defaultDto));

		return new ModelAndView("trescal/core/asset/addeditasset", refData);
	}
}