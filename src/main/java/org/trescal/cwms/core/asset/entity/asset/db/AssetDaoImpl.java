package org.trescal.cwms.core.asset.entity.asset.db;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.asset.entity.asset.Asset;
import org.trescal.cwms.core.asset.entity.asset.Asset_;
import org.trescal.cwms.core.asset.entity.asset.PhysicalAsset;
import org.trescal.cwms.core.asset.entity.asset.PhysicalAsset_;
import org.trescal.cwms.core.asset.entity.assetstatus.AssetStatus;
import org.trescal.cwms.core.asset.entity.assetstatus.AssetStatus_;
import org.trescal.cwms.core.asset.entity.assettype.AssetType;
import org.trescal.cwms.core.asset.entity.assettype.AssetType_;
import org.trescal.cwms.core.asset.form.AssetSearchForm;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.Department_;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.location.Location_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.tools.PagedResultSet;

@Repository("AssetDao")
public class AssetDaoImpl extends BaseDaoImpl<Asset, Integer> implements AssetDao {

	@Override
	protected Class<Asset> getEntity() {
		return Asset.class;
	}

	@Override
	public PagedResultSet<Asset> searchAssets(AssetSearchForm form, PagedResultSet<Asset> prs) {
		completePagedResultSet(prs, Asset.class, cb -> cq -> {
			Root<Asset> asset = cq.from(Asset.class);
			Join<Asset, Contact> owner = asset.join(Asset_.owner);
			Root<PhysicalAsset> physicalAsset = cb.treat(asset, PhysicalAsset.class);
			Join<PhysicalAsset, Instrument> instrument = physicalAsset.join(PhysicalAsset_.inst, JoinType.LEFT);
			Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model, JoinType.LEFT);
			Join<InstrumentModel, Mfr> mfr = model.join(InstrumentModel_.mfr, JoinType.LEFT);
			Join<InstrumentModel, Description> description = model.join(InstrumentModel_.description, JoinType.LEFT);
			Predicate clauses = cb.conjunction();
			if (form.getAssetNo() != null && !form.getAssetNo().trim().equals(""))
				clauses.getExpressions()
						.add(cb.like(cb.lower(asset.get(Asset_.assetNo)), "%" + form.getAssetNo().toLowerCase() + "%"));
			if (form.getPersonid() != null && !form.getPersonid().equals(""))
				clauses.getExpressions()
						.add(cb.equal(owner.get(Contact_.personid), Integer.parseInt(form.getPersonid())));
			if (form.getName() != null && !form.getName().trim().equals(""))
				clauses.getExpressions()
						.add(cb.like(cb.lower(asset.get(Asset_.name)), "%" + form.getName().toLowerCase() + "%"));
			if (form.getSupplierId() != null) {
				Join<PhysicalAsset, Company> supplier = physicalAsset.join("supplier", JoinType.LEFT);
				clauses.getExpressions().add(cb.equal(supplier.get(Company_.coid), form.getSupplierId()));
			}
			if (form.getAssettypeId() != null) {
				Join<Asset, AssetType> assetType = asset.join(Asset_.type);
				clauses.getExpressions().add(cb.equal(assetType.get(AssetType_.id), form.getAssettypeId()));
			}
			if (form.getPhysLocId() != null) {
				Join<Asset, Location> location = asset.join("physicalLocation");
				clauses.getExpressions().add(cb.equal(location.get(Location_.locationid), form.getPhysLocId()));
			}
			if (form.getDepartmentId() != null) {
				Join<Asset, Department> department = asset.join(Asset_.dept);
				clauses.getExpressions().add(cb.equal(department.get(Department_.deptid), form.getDepartmentId()));
			}
			if (form.getStatusId() != null) {
				Join<Asset, AssetStatus> status = asset.join(Asset_.status);
				clauses.getExpressions().add(cb.equal(status.get(AssetStatus_.id), form.getStatusId()));
			}
			if (form.getSerialNo() != null && !form.getSerialNo().trim().equals("")) {
				String compareString = "%" + form.getSerialNo().toLowerCase() + "%";
				clauses.getExpressions()
						.add(cb.or(cb.like(cb.lower(instrument.get(Instrument_.serialno)), compareString),
								cb.like(cb.lower(asset.get("serialNo")), compareString)));
			}
			if (form.getPlantNo() != null && !form.getPlantNo().trim().equals("")) {
				String compareString = "%" + form.getPlantNo().toLowerCase() + "%";
				clauses.getExpressions()
						.add(cb.or(cb.like(cb.lower(instrument.get(Instrument_.plantno)), compareString),
								cb.like(cb.lower(asset.get("plantNo")), compareString)));
			}
			if (form.getPlantId() != null && !form.getPlantId().trim().equals(""))
				clauses.getExpressions()
						.add(cb.equal(instrument.get(Instrument_.plantid), Integer.parseInt(form.getPlantId())));
			cq.where(clauses);
			List<Order> orders = new ArrayList<Order>();
			orders.add(cb.asc(asset.get(Asset_.name)));
			orders.add(cb.asc(mfr.get(Mfr_.name)));
			orders.add(cb.asc(model.get(InstrumentModel_.model)));
			orders.add(cb.asc(description.get(Description_.description)));
			return Triple.of(asset.get(Asset_.assetId), null, orders);
		});
		return prs;
	}
}