package org.trescal.cwms.core.asset.entity.assetstatus.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.asset.entity.assetstatus.AssetStatus;

public interface AssetStatusService
{
	void deleteAssetStatus(AssetStatus assetstatus);

	AssetStatus findAssetStatus(int id);

	List<AssetStatus> getAllAssetStatuses();

	void insertAssetStatus(AssetStatus assetstatus);
	
	/**
	 * Meant to be run once as a utility method, will scan all existing assetstatus descriptions, 
	 * and update it to populate translations for specified locale.
	 * @param locale
	 */
	public void populateDescriptionTranslationsForSystemLocale(Locale locale);
}