package org.trescal.cwms.core.asset.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.asset.form.NewAssetFromInstrumentForm;
import org.trescal.cwms.core.asset.form.validator.NewAssetFromInstrumentFormValidator;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller
@IntranetController
public class NewAssetFromInstrumentController {
	@Autowired
	private CompanyService compServ;
	@Autowired
	private NewAssetFromInstrumentFormValidator validator;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	@ModelAttribute("form")
	protected NewAssetFromInstrumentForm formBackingObject() throws Exception {
		return new NewAssetFromInstrumentForm();
	}

	@RequestMapping(value = "/newassetfrominst.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(@Valid @ModelAttribute("form") NewAssetFromInstrumentForm form,
			BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData();
		} else {
			return new ModelAndView(new RedirectView("addeditasset.htm?type=physical&plantid=" + form.getPlantId()));
		}

	}

	@RequestMapping(value = "/newassetfrominst.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData() throws Exception {
		Map<String, Object> refData = new HashMap<String, Object>();
		refData.put("businesscomps", this.compServ.getCompaniesByRole(CompanyRole.BUSINESS));
		return new ModelAndView("/trescal/core/asset/newassetfrominstrument", refData);
	}
}