package org.trescal.cwms.core.asset.entity.asset;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.asset.entity.AssetClassType;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("network")
public class NetworkAsset extends Asset {
	private String networkLocation;

	@Column(name = "networkloc", length = 100)
	@Length(max = 100)
	public String getNetworkLocation() {
		return this.networkLocation;
	}

	public void setNetworkLocation(String networkLocation) {
		this.networkLocation = networkLocation;
	}

	@Transient
	@Override
	public AssetClassType getAssettype() {
		return AssetClassType.NETWORK;
	}

	/**
	 * @return the opposite asset type to change the type while creating new one
	 */
	@Transient
	@Override
	public AssetClassType getOppassettype() {
		return AssetClassType.PHYSICAL;
	}
}