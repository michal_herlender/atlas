package org.trescal.cwms.core.asset.entity.asset.db;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.asset.entity.asset.Asset;
import org.trescal.cwms.core.asset.entity.asset.PhysicalAsset;
import org.trescal.cwms.core.asset.form.AddEditAssetForm;
import org.trescal.cwms.core.asset.form.AssetSearchForm;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.tools.PagedResultSet;

@Service("AssetService")
public class AssetServiceImpl extends BaseServiceImpl<Asset, Integer> implements AssetService {

	@Autowired
	private AssetDao assetDao;
	@Autowired
	private UserService userService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private InstrumService instServ;

	@Override
	protected BaseDao<Asset, Integer> getBaseDao() {
		return assetDao;
	}

	public Asset findAsset(int id) {
		return this.assetDao.find(id);
	}

	public PagedResultSet<Asset> searchAssets(AssetSearchForm form, PagedResultSet<Asset> prs) {
		return this.assetDao.searchAssets(form, prs);
	}
	
	@Override
	public Asset addEditAsset(AddEditAssetForm form, String username, Integer subdivid){
		Contact currentCon = this.userService.get(username).getCon();
		Asset asset = form.getAsset();

		if (form.isPersisted()) {
			asset = this.merge(asset);
		} else {
			Subdiv allocatedSubdiv = this.subdivService.get(subdivid);
			String assetNo = this.numerationService.generateNumber(NumerationType.ASSET, allocatedSubdiv.getComp(),
				allocatedSubdiv);
			asset.setAssetNo(assetNo);
			if ((asset instanceof PhysicalAsset) && (((PhysicalAsset) asset).getInst() != null)) {
				Instrument inst = this.instServ.get(((PhysicalAsset) asset).getInst().getPlantid());
				inst.setAsset((PhysicalAsset) asset);
			}
			asset.setSystemAddedBy(currentCon);
			asset.setSystemAddedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			this.save(asset);
		}
		return asset;
	}
}