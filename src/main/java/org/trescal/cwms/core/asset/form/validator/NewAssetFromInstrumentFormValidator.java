package org.trescal.cwms.core.asset.form.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.asset.form.NewAssetFromInstrumentForm;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class NewAssetFromInstrumentFormValidator extends AbstractBeanValidator {

	@Autowired
	InstrumService instrumentService;
	
	public boolean supports(Class<?> clazz) {
		return clazz.equals(NewAssetFromInstrumentForm.class);
	}

	public void validate(Object obj, Errors errors) {
		NewAssetFromInstrumentForm form = (NewAssetFromInstrumentForm) obj;
		super.validate(form, errors);

		if(form.getPlantId() != null){
			if ( instrumentService.get(form.getPlantId()) == null){
				errors.rejectValue("plantId", "importedcalibrationssynthesis.instrumentnotfound ", null, "Instrument not found");
			}
			else {
				Instrument instrument = instrumentService.get(form.getPlantId());
				if (!instrumentService.checkExists(instrument.getPlantno(), instrument.getSerialno(), form.getCoid(), null)){
					errors.rejectValue("plantId", "error.instrument.notavailable", null, "The instrument isn't available in the company");
				}

		}
	 }
		
	}

}
