package org.trescal.cwms.core.asset.entity.assetnote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.asset.entity.asset.Asset;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;

@Entity
@Table(name = "assetnote")
public class AssetNote extends Note
{
	public final static boolean privateOnly = true;
	
	private Asset asset;
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	// TODO 2017-08-25 GB restore nullable=false, add not null to column, but has index 
	@JoinColumn(name = "assetid", unique = false, insertable = true, updatable = true)
	public Asset getAsset() {
		return this.asset;
	}
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.ASSETNOTE;
	}
	
	public void setAsset(Asset asset) {
		this.asset = asset;
	}
}