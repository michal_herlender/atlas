package org.trescal.cwms.core.asset.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.asset.entity.asset.Asset;
import org.trescal.cwms.core.asset.entity.asset.PhysicalAsset;
import org.trescal.cwms.core.asset.entity.asset.db.AssetService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.JsonController;

@Controller
@JsonController
public class AssetController {

	@Autowired
	private AssetService assetService;

	@RequestMapping(value = "deleteasset.json", method = RequestMethod.GET)
	@ResponseBody
	public ResultWrapper deleteAsset(@RequestParam(name = "assetId", required = true) Integer assetId) {
		Asset asset = assetService.get(assetId);
		if (asset instanceof PhysicalAsset) {
			PhysicalAsset physicalAsset = (PhysicalAsset) asset;
			if (physicalAsset.getInst() != null)
				physicalAsset.getInst().setAsset(null);
		}
		assetService.delete(asset);
		return new ResultWrapper(true, "Asset deleted");
	}
}