package org.trescal.cwms.core.asset.entity.assetstatus;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.system.entity.translation.Translation;


@Entity
@Table(name = "assetstatus")
public class AssetStatus
{
	private boolean active;
	private int id;
	private String name;
	private Set<Translation> translations;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Deprecated
	@Column(name = "name", length = 200, nullable = true)
	@Length(max = 200)
	public String getName()
	{
		return this.name;
	}

	@NotNull
	@Column(name = "active", nullable = false, columnDefinition="bit")
	public boolean isActive()
	{
		return this.active;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setName(String name)
	{
		this.name = name;
	}
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="assetstatustranslation", joinColumns=@JoinColumn(name="id"))
	public Set<Translation> getTranslations() {
		return translations;
	}
	
	public void setTranslations(Set<Translation> translations) {
		this.translations = translations;
	}
	
}