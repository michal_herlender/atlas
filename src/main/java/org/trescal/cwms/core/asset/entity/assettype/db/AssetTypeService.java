package org.trescal.cwms.core.asset.entity.assettype.db;

import java.util.List;

import org.trescal.cwms.core.asset.entity.assettype.AssetType;

public interface AssetTypeService
{
	void deleteAssetType(AssetType assettype);

	AssetType findAssetType(int id);

	AssetType findAssetTypeForInstruments();

	List<AssetType> getAllAssetTypes();

	void insertAssetType(AssetType assettype);

	void updateAssetType(AssetType assettype);
}