package org.trescal.cwms.core.asset.entity.asset;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.asset.entity.AssetClassType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

@Entity
@DiscriminatorValue("physical")
public class PhysicalAsset extends Asset
{
	private Instrument inst;
	private String ipAddress;
	private Location physicalLocation;
	private String plantNo;
	private String serialNo;
	private Company supplier;
	
	/**
	 * @return the instrument
	 */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid")
	public Instrument getInst()
	{
		return this.inst;
	}

	@Column(name = "ipaddress", length = 20, nullable = true)
	@Length(max = 20)
	public String getIpAddress()
	{
		return this.ipAddress;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "physicallocid")
	public Location getPhysicalLocation()
	{
		return this.physicalLocation;
	}

	@Column(name = "plantno", length = 50, nullable = true)
	@Length(max = 50)
	public String getPlantNo()
	{
		return this.plantNo;
	}

	@Column(name = "serialno", length = 50, nullable = true)
	@Length(max = 50)
	public String getSerialNo()
	{
		return this.serialNo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "supplierid")
	public Company getSupplier()
	{
		return this.supplier;
	}

	public void setInst(Instrument inst)
	{
		this.inst = inst;
	}

	public void setIpAddress(String ipAddress)
	{
		this.ipAddress = ipAddress;
	}

	public void setPhysicalLocation(Location physicalLocation)
	{
		this.physicalLocation = physicalLocation;
	}

	public void setPlantNo(String plantNo)
	{
		this.plantNo = plantNo;
	}

	public void setSerialNo(String serialNo)
	{
		this.serialNo = serialNo;
	}

	public void setSupplier(Company supplier)
	{
		this.supplier = supplier;
	}

	@Transient
	@Override
	public AssetClassType getAssettype() {
		return AssetClassType.PHYSICAL;
	}

	/**
	 * @return the opposite asset type to change the type while creating new one
	 */
	@Override
	@Transient
	public AssetClassType getOppassettype() {
		return AssetClassType.NETWORK;
	}
}