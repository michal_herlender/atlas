package org.trescal.cwms.core.asset.entity.assettype.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.asset.entity.assettype.AssetType;
import org.trescal.cwms.core.asset.entity.assettype.AssetType_;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;

@Repository("AssetTypeDao")
public class AssetTypeDaoImpl extends BaseDaoImpl<AssetType, Integer> implements AssetTypeDao {
	
	@Override
	protected Class<AssetType> getEntity() {
		return AssetType.class;
	}
	
	public AssetType findAssetTypeForInstruments() {
		return getFirstResult(cb ->{
			CriteriaQuery<AssetType> cq = cb.createQuery(AssetType.class);
			Root<AssetType> root = cq.from(AssetType.class);
			cq.where(cb.isTrue(root.get(AssetType_.instType)));
			return cq;
		}).orElse(null);
	}
}