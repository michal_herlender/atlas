package org.trescal.cwms.core.asset.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.asset.entity.asset.Asset;
import org.trescal.cwms.core.asset.entity.asset.db.AssetService;
import org.trescal.cwms.core.asset.entity.assetstatus.db.AssetStatusService;
import org.trescal.cwms.core.asset.entity.assettype.db.AssetTypeService;
import org.trescal.cwms.core.asset.form.AssetSearchForm;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes( Constants.SESSION_ATTRIBUTE_COMPANY )
public class AssetSearchController
{
	@Autowired
	private AssetService assetServ;
	
	@Autowired
	private AssetStatusService assetStatusServ;
	
	@Autowired
	private AssetTypeService assetTypeServ;
	
	@Autowired
	private DepartmentService deptServ;
	
	@Autowired
	private LocationService locServ;

	@ModelAttribute("form")
	protected AssetSearchForm formBackingObject(HttpServletRequest request) throws Exception
	{
		return new AssetSearchForm();
	}

	public AssetService getAssetServ()
	{
		return this.assetServ;
	}

	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception
	{
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
	}
	
	@RequestMapping(value="/assetsearch.htm", method=RequestMethod.POST)
	protected String onSubmit(@ModelAttribute("form") AssetSearchForm form) throws Exception
	{
		PagedResultSet<Asset> rs = this.assetServ.searchAssets(form, new PagedResultSet<Asset>(form.getResultsPerPage() == 0 ? Constants.RESULTS_PER_PAGE : form.getResultsPerPage(), form.getPageNo() == 0 ? 1 : form.getPageNo()));
		form.setRs(rs);
		return "trescal/core/asset/assetsearchresults";
	}
	
	@RequestMapping(value="/assetsearch.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer,String> companyDto) throws Exception
	{
		Map<String, Object> refData = new HashMap<String, Object>();

		refData.put("assettypes", this.assetTypeServ.getAllAssetTypes());
		refData.put("departments", this.deptServ.getBusinessDepartments());
		refData.put("businesscompanylocations", this.locServ.getAllCompanyLocationsActive(companyDto.getKey()));
		refData.put("assetstatus", this.assetStatusServ.getAllAssetStatuses());

		return new ModelAndView("trescal/core/asset/assetsearch", refData);
	}
}