package org.trescal.cwms.core.asset.entity.assettype.db;

import org.trescal.cwms.core.asset.entity.assettype.AssetType;
import org.trescal.cwms.core.audit.entity.db.BaseDao;

public interface AssetTypeDao extends BaseDao<AssetType, Integer> {
	
	AssetType findAssetTypeForInstruments();
}