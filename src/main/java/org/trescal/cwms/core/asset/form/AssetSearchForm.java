package org.trescal.cwms.core.asset.form;

import org.trescal.cwms.core.asset.entity.asset.Asset;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.Data;

@Data
public class AssetSearchForm {
	private String assetNo;
	private Integer assettypeId;
	private Integer departmentId;
	private String name;
	private int pageNo;
	private String personid;
	private Integer physLocId;
	private String plantId;
	private String plantNo;
	private int resultsPerPage = 50;
	private PagedResultSet<Asset> rs;
	private String serialNo;
	private Integer statusId;
	private Integer supplierId;
}
