package org.trescal.cwms.core.asset.entity.assetstatus.db;

import java.util.Locale;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.asset.entity.assetstatus.AssetStatus;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Repository("AssetStatusDao")
public class AssetStatusDaoImpl extends BaseDaoImpl<AssetStatus, Integer> implements AssetStatusDao {

	@Override
	protected Class<AssetStatus> getEntity() {
		return AssetStatus.class;
	}

	/*
	 * Returns translation for specified locale, or null if not found
	 */
	@Override
	public Translation getTranslationForLocaleOrNull(AssetStatus description, Locale locale) {
		for (Translation t : description.getTranslations())
			if (t.getLocale().equals(locale))
				return t;
		return null;
	}
}