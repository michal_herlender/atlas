package org.trescal.cwms.core.asset.entity;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum AssetClassType {
	PHYSICAL("asset.physical", "Physical"), 
	NETWORK("asset.network", "Network");
	
	private ReloadableResourceBundleMessageSource messages;
	private Locale loc = null;
	private String messageCode;
	private String name;
	
	@Component
	public static class AssetClassTypeMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (AssetClassType rt : EnumSet.allOf(AssetClassType.class))
               rt.setMessageSource(messages);
        }
	}
	
	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}
	
	private AssetClassType(String messageCode, String name) {
		this.messageCode = messageCode;
		this.name = name;
	}
	
	public String getType() 
	{
		loc = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.name, loc);
		}
		throw new IndexOutOfBoundsException();
	}
	
	public String getMessageCode() {
		return messageCode;
	}
}
