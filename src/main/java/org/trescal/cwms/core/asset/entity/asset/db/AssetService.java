package org.trescal.cwms.core.asset.entity.asset.db;

import org.trescal.cwms.core.asset.entity.asset.Asset;
import org.trescal.cwms.core.asset.form.AddEditAssetForm;
import org.trescal.cwms.core.asset.form.AssetSearchForm;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface AssetService extends BaseService<Asset, Integer> {

	Asset findAsset(int id);

	PagedResultSet<Asset> searchAssets(AssetSearchForm form, PagedResultSet<Asset> prs);
	
	Asset addEditAsset(AddEditAssetForm form, String username, Integer subdivid);
}