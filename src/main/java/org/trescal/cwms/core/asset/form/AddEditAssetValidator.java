package org.trescal.cwms.core.asset.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.asset.entity.asset.NetworkAsset;
import org.trescal.cwms.core.asset.entity.asset.PhysicalAsset;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class AddEditAssetValidator extends AbstractBeanValidator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(AddEditAssetForm.class);
	}
	
	@Override
	public void validate(Object command, Errors errors)
	{
		super.validate(command, errors);
		AddEditAssetForm form = (AddEditAssetForm) command;
		
		// add business logic depending on specifics of asset
		if (form.getAsset() instanceof PhysicalAsset)
		{
			// location supplied?
			if (form.getPhysLocId() == null)
			{
				errors.rejectValue("physLocId", "error.physlocid", "Please select a location before submitting your asset");
			}

			PhysicalAsset asset = (PhysicalAsset) form.getAsset();

			if ((asset.getIpAddress() != null)
					&& !asset.getIpAddress().trim().isEmpty())
			{
				String strValidChars = "0123456789.-";
				Character strChar;

				// test strString consists of valid characters listed above
				for (int i = 0; i < asset.getIpAddress().length(); i++)
				{
					// get character in this position
					strChar = asset.getIpAddress().charAt(i);
					// check it's valid
					if (strValidChars.indexOf(strChar) == -1)
					{
						errors.rejectValue("asset.ipAddress", "error.asset.ipAddress", "The IP address specified is not valid");
						break;
					}
				}
			}
		}
		else if (form.getAsset() instanceof NetworkAsset)
		{
			NetworkAsset asset = (NetworkAsset) form.getAsset();

			if ((asset.getNetworkLocation() == null)
					|| asset.getNetworkLocation().trim().isEmpty())
			{
				errors.rejectValue("asset.networkLocation", "error.asset.networkLocation", "A network path must be specified");
			}
		}
	}
}