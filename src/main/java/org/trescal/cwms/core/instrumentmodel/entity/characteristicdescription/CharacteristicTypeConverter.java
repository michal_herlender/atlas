package org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription;


import javax.persistence.AttributeConverter;

//TODO Refactor CharacteristicDescription to use @Converter (not defined here due to bean collision) with Hibernate 5
public class CharacteristicTypeConverter implements AttributeConverter<CharacteristicType, Integer>{

	@Override
	public Integer convertToDatabaseColumn(CharacteristicType attribute) {
		switch (attribute) {
        case FREE_FIELD:
            return 5;
        case VALUE_WITH_UNIT:
            return 14;
        case LIBRARY:
            return 6;
        default:
            throw new IllegalArgumentException("Unknown" + attribute);
		}
	}

	@Override
	public CharacteristicType convertToEntityAttribute(Integer dbData) {
		switch (dbData) {
        case 5:
            return CharacteristicType.FREE_FIELD;
        case 14:
            return CharacteristicType.VALUE_WITH_UNIT;
        case 6:
            return CharacteristicType.LIBRARY;
        default:
            throw new IllegalArgumentException("Unknown" + dbData);
		}
	}

}
