package org.trescal.cwms.core.instrumentmodel.entity.domain.db;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.referential.dto.TMLDomainDTO;
import org.trescal.cwms.spring.model.InstrumentModelDomainJsonDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public class InstrumentModelDomainServiceImpl extends BaseServiceImpl<InstrumentModelDomain, Integer> implements InstrumentModelDomainService {

	@Autowired
	private InstrumentModelDomainDao instrumentModelDomainDao;
	
	@Override
	protected BaseDao<InstrumentModelDomain, Integer> getBaseDao() {
		return instrumentModelDomainDao;
	}

	public void createInstrumentModelDomain(InstrumentModelDomain instrumentModelDomain) {
		this.instrumentModelDomainDao.persist(instrumentModelDomain);

	}

	public boolean isDuplicate(String name) {
		return this.instrumentModelDomainDao.existsByName(name);
	}
	/*
	 * @Deprecated
	 * 
	 * @Override public List<InstrumentModelDomainJsonDTO> getAllDomains() { return
	 * this.instrumentModelDomainDao.getAllDomains(); }
	 * 
	 * @Override public List<InstrumentModelDomainJsonDTO> getAllDomains(Locale
	 * userLocale) { return this.instrumentModelDomainDao.getAllDomains(userLocale);
	 * }
	 */

	@Override
	public InstrumentModelDomain getDomain(int id) {
		return this.instrumentModelDomainDao.find(id);
	}

	@Override
	public void updateInstrumentModelDomain(InstrumentModelDomain domain) {
		this.instrumentModelDomainDao.merge(domain);

	}

	@Override
	public void deleteInstrumentModelDomain(InstrumentModelDomain domain) {
		this.instrumentModelDomainDao.remove(domain);

	}

	@Override
	public InstrumentModelDomain getTMLDomain(int tmlid) {
		return this.instrumentModelDomainDao.getTMLDomain(tmlid);
	}

	@Override
	public void createTMLInstrumentModelDomain(TMLDomainDTO dto) throws Exception {
		this.instrumentModelDomainDao.insertTMLInstrumentModelDomain(dto);

	}

	@Override
	public void updateTMLInstrumentModelDomain(TMLDomainDTO dto) throws Exception {
		this.instrumentModelDomainDao.updateTMLDomain(dto);

	}

	@Override
	public void deleteTMLInstrumentModelDomain(TMLDomainDTO dto) throws Exception {
		this.instrumentModelDomainDao.deleteTMLDomain(dto);

	}

	@Override
	public int getResultCountExact(String translation, Locale locale, int excludeId) {
		return this.instrumentModelDomainDao.getResultCountExact(translation, locale, excludeId);
	}

	@Override
	public List<KeyValueIntegerString> getAllKeyValues(String domainFragment, DomainType domainType,
			Locale searchLanguage, int maxResults) {
		return instrumentModelDomainDao.getAllKeyValues(domainFragment, domainType, searchLanguage, maxResults);
	}

	@Override
	public List<InstrumentModelDomainJsonDTO> getAllDomains(Locale userLocale) {
		return this.instrumentModelDomainDao.getAllDomains(userLocale);
	}

}
