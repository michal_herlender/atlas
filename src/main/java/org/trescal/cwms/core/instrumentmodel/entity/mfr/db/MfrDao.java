package org.trescal.cwms.core.instrumentmodel.entity.mfr.db;

import java.util.List;

import org.hibernate.Criteria;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.dto.MfrSearchResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.referential.dto.TMLManufacturerDTO;
import org.trescal.cwms.spring.model.LabelIdDTO;

public interface MfrDao extends BaseDao<Mfr, Integer> {
	
	void addMfrCriteria(Criteria instCrit, Criteria mfrCrit, String mfrName);
	
	List<Mfr> findMfr(String nameFragment, Integer ignoreMfrId);
	
	Mfr findMfrByName(String name);
	
	List<Mfr> getSortedActiveMfr();
	
	Mfr getSystemGenericMfr();
	
	List<MfrSearchResultWrapper> searchSortedActiveMfrHQL(String searchName);

	List<Mfr> searchSortedMfr(String nameFragment, Boolean active);
	
	Mfr findTMLMfr(int tmlid);

	void insertTMLMfr(TMLManufacturerDTO dto) throws Exception;

	void updateTMLMfr(TMLManufacturerDTO dto) throws Exception;

	void deleteTMLMfr(TMLManufacturerDTO dto) throws Exception;

	List<LabelIdDTO> getLabelIdList(String descriptionFragment, Boolean searchEverywhere, int maxResults);
}