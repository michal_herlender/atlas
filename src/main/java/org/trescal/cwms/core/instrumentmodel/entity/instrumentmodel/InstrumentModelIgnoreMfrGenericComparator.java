package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel;

import java.util.Comparator;

import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;

/**
 * Comparator implementation that sorts lists of {@link InstrumentModel}. In
 * this case if a model has a mfr_generic set to true, then the default mfr is
 * not included in the sort and the model is ordered just by model name and
 * description
 * 
 * @author Richard
 */
public class InstrumentModelIgnoreMfrGenericComparator implements Comparator<InstrumentModel>
{
	public int compare(InstrumentModel o1, InstrumentModel o2)
	{
		if (o1.getModelMfrType().equals(ModelMfrType.MFR_SPECIFIC)
				&& o2.getModelMfrType().equals(ModelMfrType.MFR_SPECIFIC))
		{
			return compareModelDesc(o1, o2);
		}
		else if (o1.getModelMfrType().equals(ModelMfrType.MFR_GENERIC)
				&& o2.getModelMfrType().equals(ModelMfrType.MFR_GENERIC))
		{
			return compareModelDesc(o1, o2);
		}
		else if (o1.getModelMfrType().equals(ModelMfrType.MFR_GENERIC)
				&& o2.getModelMfrType().equals(ModelMfrType.MFR_SPECIFIC))
		{
			return -1;
		}
		else if (o1.getModelMfrType().equals(ModelMfrType.MFR_SPECIFIC)
				&& o2.getModelMfrType().equals(ModelMfrType.MFR_GENERIC))
		{
			return 1;
		}
		else
		{
			return compareModelDesc(o1, o2);
		}
	}

	public int compareModelDesc(InstrumentModel o1, InstrumentModel o2)
	{
		if (o1.getModel().compareTo(o2.getModel()) == 0)
		{
			return ((Integer) o1.getModelid()).compareTo(o2.getModelid());
		}
		return o1.getModel().compareTo(o2.getModel());
	}
}
