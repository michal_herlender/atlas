/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.entity.component;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.instrumentmodel.entity.componentdescription.ComponentDescription;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent.InstrumentModelComponent;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.stockablemodel.StockableModel;

/**
 * Entity representing a component or part. Components can form parts of
 * {@link Instrument}s.
 * 
 * @author richard
 */
@Entity
@Table(name = "component")
public class Component extends StockableModel
{
	private ComponentDescription description;
	private int id;
	private Mfr mfr;
	private Set<InstrumentModelComponent> modelComponents;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "descriptionid", nullable = false)
	public ComponentDescription getDescription()
	{
		return this.description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "mfrid", nullable = true)
	public Mfr getMfr()
	{
		return this.mfr;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "component")
	public Set<InstrumentModelComponent> getModelComponents()
	{
		return this.modelComponents;
	}

	public void setDescription(ComponentDescription description)
	{
		this.description = description;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setMfr(Mfr mfr)
	{
		this.mfr = mfr;
	}

	public void setModelComponents(Set<InstrumentModelComponent> modelComponents)
	{
		this.modelComponents = modelComponents;
	}
}
