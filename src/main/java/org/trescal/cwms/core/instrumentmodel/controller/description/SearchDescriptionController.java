package org.trescal.cwms.core.instrumentmodel.controller.description;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.form.description.SearchDescriptionForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@Controller
public class SearchDescriptionController {
	@Autowired
	private NewDescriptionService descriptionService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	
	@ModelAttribute("command")
	public SearchDescriptionForm formBackingObject(Locale locale) {
		SearchDescriptionForm form = new SearchDescriptionForm();
		form.setDescription("");
		form.setFirstResult(0);
		form.setPageNo(0);
		form.setResultsPerPage(Constants.RESULTS_PER_PAGE);
		// Default the search language to the primary locale of the application
		form.setSearchLanguage(supportedLocaleService.getPrimaryLocale());
		// Default the additional display language to the current locale of the logged in user
		form.setDisplayLanguage(locale);
		return form;
	}
	
	@RequestMapping(value="/searchdescription.htm", method=RequestMethod.GET)
	public String referenceData(Model model) {
		model.addAttribute("supportedLocales", supportedLocaleService.getSupportedLocales());
		return "trescal/core/instrumentmodel/description/searchdescription";
	}
	
	@RequestMapping(value="/searchdescription.htm", method=RequestMethod.POST)
	public String performSearch(Model model,
			@ModelAttribute("command") SearchDescriptionForm form) {
		PagedResultSet<Description> pagedResultSet = new PagedResultSet<Description>(Constants.RESULTS_PER_PAGE, form.getPageNo());
		int resultCount = descriptionService.getResultCountFragment(form.getDescription(), form.getSearchLanguage());
		List<Description> results = descriptionService.performQueryFragment(form.getDescription(), form.getSearchLanguage(), resolveFirstResult(form), Constants.RESULTS_PER_PAGE);
		pagedResultSet.setResults(results);
		pagedResultSet.setResultsCount(resultCount);
		model.addAttribute("results", pagedResultSet);
		model.addAttribute("searchLocale", form.getSearchLanguage());
		model.addAttribute("displayLocale", form.getDisplayLanguage());
		model.addAttribute("defaultLocale", supportedLocaleService.getPrimaryLocale());
		return "trescal/core/instrumentmodel/description/searchdescriptionresults";
	}
	
	protected int resolveFirstResult(SearchDescriptionForm form) {
		if (form.getPageNo() < 2) return 0;
		return Constants.RESULTS_PER_PAGE * (form.getPageNo() - 1);
	}
}
