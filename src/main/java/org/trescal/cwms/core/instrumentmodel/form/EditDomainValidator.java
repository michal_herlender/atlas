package org.trescal.cwms.core.instrumentmodel.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.instrumentmodel.entity.domain.db.InstrumentModelDomainService;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@Component
public class EditDomainValidator implements Validator {

	@Autowired
	InstrumentModelDomainService instModDomainServ;
	
	@Autowired
	SupportedLocaleService supportedLocaleService;
	
	@Override
	public void validate(Object target, Errors errors) {

		EditDomainForm form = (EditDomainForm) target;
	
		for (Translation __t: form.getTranslations()) {
			if (__t.getLocale().equals(supportedLocaleService.getPrimaryLocale()) && (__t.getTranslation() == null || __t.getTranslation().length() == 0)) {
				errors.rejectValue("translations", "editdescription.error.primaryLocaleNull", 
						"The primary Locale (" + supportedLocaleService.getPrimaryLocale().getDisplayLanguage() + ") mustn't be empty.");
				break;
			}
		}
		//translation is unique for every language
		for (Translation __t: form.getTranslations()) {
			if (__t.getTranslation().length() > 0 &&
					instModDomainServ.getResultCountExact(__t.getTranslation(), __t.getLocale(), form.getDomain().getDomainid()) > 0) {
				errors.rejectValue("translations", "editdescription.error.translationNotUnique",
						"Description '" + __t.getTranslation() + "' already defined.");
			}
		}

	}

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(EditDomainForm.class);
	}

}
