package org.trescal.cwms.core.instrumentmodel.form;

import java.util.SortedSet;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.system.entity.translation.Translation;

public class EditSalesCategoryForm {
	
	private SalesCategory salesCategory;
	private SortedSet<Translation> translations;
	private String designationdefault;
	
	
	public SalesCategory getSalesCategory() {
		return salesCategory;
	}
	public void setSalesCategory(SalesCategory salescategory) {
		this.salesCategory = salescategory;
	}
	public SortedSet<Translation> getTranslations() {
		return translations;
	}
	public void setTranslations(SortedSet<Translation> translations) {
		this.translations = translations;
	}
	public String getDesignationdefault() {
		return designationdefault;
	}
	public void setDesignationdefault(String designationdefault) {
		this.designationdefault = designationdefault;
	}

}
