package org.trescal.cwms.core.instrumentmodel.controller.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.controller.AbstractModelController;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db.ServiceCapabilityService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class ViewModelServicesController extends AbstractModelController {
	
	@Autowired
	ServiceCapabilityService serviceCapabilityService;
	
	@ModelAttribute("selectedTab")
	public String initialiseSelectedTab(){
		return "services";
	}
	
	@ModelAttribute("services")
	public  List<ServiceCapability>populateServices(@RequestParam("modelid") int modelid){
		 return serviceCapabilityService.find(modelid, null, null, null);
	}
	
	@RequestMapping(value="/instrumentmodelservices.htm", method=RequestMethod.GET)
	public String requestHandler(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto,
			@ModelAttribute("model") InstrumentModel instrumentModel,
			@ModelAttribute("services") List<ServiceCapability> serviceCapabilities,
			Model model) throws Exception
	{
		InstrumentModelType modelType = instrumentModel.getModelType();
		boolean showInitializeServices = !(instrumentModel.getQuarantined() || modelType.getCapability() || modelType.getSalescategory() ||
				serviceCapabilities != null && serviceCapabilities.stream().anyMatch(s -> s.getOrganisation().getSubdivid() == subdivDto.getKey()));
		model.addAttribute("showInitializeServices", showInitializeServices);
		return "/trescal/core/instrumentmodel/services/modelviewservices";
	}
}
