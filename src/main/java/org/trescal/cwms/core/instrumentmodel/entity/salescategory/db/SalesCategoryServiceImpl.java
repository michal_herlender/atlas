package org.trescal.cwms.core.instrumentmodel.entity.salescategory.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.Tuple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValueIntegerString;
import org.trescal.cwms.spring.model.LabelIdDTO;
import org.trescal.cwms.spring.model.SalesCategoryJsonDTO;

@Service
public class SalesCategoryServiceImpl extends BaseServiceImpl<SalesCategory, Integer> implements SalesCategoryService
{
	@Autowired
	private SalesCategoryDao salesCategoryDao;
	@Autowired
	private TranslationService transService;
	@Override
	protected BaseDao<SalesCategory, Integer> getBaseDao() {
		return salesCategoryDao;
	}

	@Override
	public List<SalesCategoryJsonDTO> getAllSalesCategories(Locale locale) {
		List<SalesCategoryJsonDTO> translatedSalesCategoriesDTO = new ArrayList<SalesCategoryJsonDTO>();
		List<SalesCategory> salesCategories = this.salesCategoryDao.getAllSalesCategories();
		for(SalesCategory sc : salesCategories){
			translatedSalesCategoriesDTO.add(new SalesCategoryJsonDTO(sc.getId(), transService.getCorrectTranslation(sc.getTranslations(), locale), sc.getActive()));		
		}
		return translatedSalesCategoriesDTO;
	}

	@Override
	public List<LabelIdDTO> getLabelIdList(String salesCategoryFragment,Locale searchLanguage, int maxResults) {
			return salesCategoryDao.getLabelIdList(salesCategoryFragment, searchLanguage, maxResults);
	}
	
	@Override
	public int getResultsCountExact(String translation, Locale searchLanguage, int excludeId){
		return salesCategoryDao.getResultsCountExact(translation, searchLanguage, excludeId);
	}
	
	@Override
	public List<Tuple> getSalesCategoryIdsForModelIds(Collection<Integer> modelIds) {
		return this.salesCategoryDao.getSalesCategoryIdsForModelIds(modelIds);
	}

	@Override
	public SalesCategory getSalesCategory(Integer id) {
		return salesCategoryDao.find(id);
	}
	@Override
	public String getSalesCategoryTranslationForLocale(int id, Locale locale){
		SalesCategory salesCategory = salesCategoryDao.find(id);
		return transService.getCorrectTranslation(salesCategory.getTranslations(), locale);
	}
	
	/**
	 * Regenerates the translations of a sales category, based on the instrument model definition
	 * Should be called if instrument model definition and/or characteristics have changed, 
	 * resulting in a change of the name.
	 */
	@Override
	public void regenerateSalesCategoryTranslations(SalesCategory salesCategory, InstrumentModel instModel) {
		Map<Locale, List<Translation>> sctMap = salesCategory.getTranslations().stream()
				.collect(Collectors.groupingBy(sct -> sct.getLocale()));
		for (Translation mt : instModel.getNameTranslations()) {
			if (!sctMap.containsKey(mt.getLocale())) {
				// New translation to insert
				Translation newSct = new Translation(mt.getLocale(), mt.getTranslation());
				salesCategory.getTranslations().add(newSct);
			}
			
			else {
				List<Translation> sctList = sctMap.get(mt.getLocale());
				Translation firstSct = sctList.get(0);
				if (sctList.size() > 1) {
					// Multiple translations, remove all except first
					salesCategory.getTranslations().removeAll(sctList);
					salesCategory.getTranslations().add(firstSct);
				}
				if (!firstSct.getTranslation().equals(mt.getTranslation())) {
					firstSct.setTranslation(mt.getTranslation());
				}
			}
		}
	}
	
	@Override
	public List<KeyValueIntegerString> getSalesCategoriesForSubfamily(Integer subFamilyId, Locale locale) {
		return this.salesCategoryDao.getSalesCategoriesForSubfamily(subFamilyId, locale);
	}
}
