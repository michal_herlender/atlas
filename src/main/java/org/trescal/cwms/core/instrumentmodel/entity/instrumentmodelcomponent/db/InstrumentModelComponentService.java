package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent.db;

import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;
import org.trescal.cwms.core.instrumentmodel.entity.componentdescription.ComponentDescription;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent.InstrumentModelComponent;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;

public interface InstrumentModelComponentService
{
	void deleteInstrumentModelComponent(InstrumentModelComponent instrumentmodelcomponent);

	/**
	 * Deletes the {@link InstrumentModelComponent} identified by the given id.
	 * 
	 * @param id the id of the {@link InstrumentModelComponent}.
	 * @return {@link ResultWrapper} indicating the outcome of the operation.
	 */
	ResultWrapper deleteModelComponent(int id);

	/**
	 * Returns the {@link InstrumentModelComponent} identified by the given id.
	 * Eagerly loads the {@link Component} and associated {@link Mfr} and
	 * {@link ComponentDescription} data.
	 * 
	 * @param id
	 * @return
	 */
	InstrumentModelComponent findInstrumentModelComponent(int id);

	List<InstrumentModelComponent> getAllInstrumentModelComponents();

	/**
	 * Returns a {@link List} of all {@link Component} entities that are not
	 * already attached to the {@link InstrumentModel} identified by the given
	 * id.
	 * 
	 * @param modelid the id of the {@link InstrumentModel}.
	 * @return {@link List} of available {@link Component} entities.
	 */
	List<Component> getAllUnAttachedComponents(int modelid);

	/**
	 * Gets all {@link InstrumentModelComponent} entities whose modelid and
	 * componentid ids match those supplied as arguments. An
	 * {@link InstrumentModelComponent} identified by the given id will be
	 * ignored.
	 * 
	 * @param modelid the id of the {@link InstrumentModel}.
	 * @param componentid the id of the {@link Component}.
	 * @param id the id of and {@link InstrumentModelComponent} to ignore in
	 *            this query.
	 * @return {@link List} of {@link InstrumentModelComponent} entities.
	 */
	List<InstrumentModelComponent> getInstrumentModelComponents(int modelid, int componentid, Integer id);

	/**
	 * Creates a new {@link InstrumentModelComponent} from the given
	 * {@link InstrumentModel} and the given {@link Component}.
	 * 
	 * @param modelid id of the {@link InstrumentModel}.
	 * @param componentid id of the {@link Component}.
	 * @param includeByDefault value of the includeByDefault property.
	 * @return {@link ResultWrapper} indicating the outcome of the operation.
	 */
	ResultWrapper insert(int modelid, int componentid, boolean includeByDefault);

	void insertInstrumentModelComponent(InstrumentModelComponent instrumentmodelcomponent);

	void saveOrUpdateInstrumentModelComponent(InstrumentModelComponent instrumentmodelcomponent);

	/**
	 * Updates the value of the includeByDefault property of the
	 * {@link InstrumentModelComponent} identified by the given id.
	 * 
	 * @param id the id of the {@link InstrumentModelComponent}.
	 * @param includeByDefault the new value of the includeByDefault property.
	 * @return {@link ResultWrapper} containing the outcome of the operation.
	 */
	ResultWrapper updateIncludeByDefault(int id, boolean includeByDefault);

	void updateInstrumentModelComponent(InstrumentModelComponent instrumentmodelcomponent);
}