package org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db;

import org.apache.commons.collections4.MultiValuedMap;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability_;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.Capability_;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;

import javax.persistence.criteria.*;
import java.util.List;

@Repository
public class ServiceCapabilityDaoImpl extends BaseDaoImpl<ServiceCapability, Integer> implements ServiceCapabilityDao {
    protected Class<ServiceCapability> getEntity() {
        return ServiceCapability.class;
    }

    @Override
    public ServiceCapability getCalServiceCapability(InstrumentModel model, CalibrationType calType, Subdiv subDiv) {
        return getFirstResult(cb -> {
            CriteriaQuery<ServiceCapability> cq = cb.createQuery(ServiceCapability.class);
            Root<ServiceCapability> root = cq.from(ServiceCapability.class);
            Join<ServiceCapability, Capability> procJoin = root.join(ServiceCapability_.capability);
            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.isTrue(procJoin.get(Capability_.active)));
            clauses.getExpressions().add(cb.equal(root.get(ServiceCapability_.instrumentModel), model));
            clauses.getExpressions().add(cb.equal(root.get(ServiceCapability_.calibrationType), calType));
            clauses.getExpressions().add(cb.equal(root.get(ServiceCapability_.organisation), subDiv));
            cq.where(clauses);
            return cq;
        }).orElse(null);
	}

	/**
	 * 
	 * @param instrumentModelCaltype
	 *            = key : instrument model id, value : cal type id
	 */
	@Override
	public List<ServiceCapability> getCalServiceCapabilities(MultiValuedMap<Integer, Integer> instrumentModelCaltype,
			Integer businessSubdivId) {
		return getResultList(cb -> {
            CriteriaQuery<ServiceCapability> cq = cb.createQuery(ServiceCapability.class);
            Root<ServiceCapability> root = cq.from(ServiceCapability.class);
            Join<ServiceCapability, CalibrationType> caltypeJoin = root.join(ServiceCapability_.calibrationType);
            Join<ServiceCapability, Subdiv> subdivJoin = root.join(ServiceCapability_.organisation.getName());

            Fetch<ServiceCapability, Capability> procFetch = root.fetch(ServiceCapability_.capability,
                javax.persistence.criteria.JoinType.LEFT);
            procFetch.fetch(Capability_.defAddress);
            procFetch.fetch(Capability_.categories);
            root.fetch(ServiceCapability_.workInstruction, javax.persistence.criteria.JoinType.LEFT);

            Predicate orPredicate = cb.disjunction();

            for (Integer key : instrumentModelCaltype.keySet()) {
                for (Integer value : instrumentModelCaltype.get(key)) {
                    Predicate andPredicate = cb.and(cb.equal(root.get(ServiceCapability_.instrumentModel), key),
                        cb.equal(caltypeJoin.get(CalibrationType_.calTypeId), value),
                        cb.equal(subdivJoin.get(Subdiv_.subdivid), businessSubdivId));
                    orPredicate.getExpressions().add(cb.or(andPredicate));
                }
            }

            cq.where(orPredicate);

            return cq;
        });
	}

	@Override
	public List<ServiceCapability> find(Integer modelId, Integer serviceTypeId, CostType costType, Integer businessSubdivId) {
        if ((modelId == null) || modelId == 0) throw new IllegalArgumentException("modelId must be specified");
		return getResultList(cb -> {
            CriteriaQuery<ServiceCapability> cq = cb.createQuery(ServiceCapability.class);
            Root<ServiceCapability> root = cq.from(ServiceCapability.class);
            Join<ServiceCapability, InstrumentModel> instrumentModel = root.join(ServiceCapability_.instrumentModel);
            Join<ServiceCapability, Capability> procedure = root.join(ServiceCapability_.capability);
            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.equal(instrumentModel.get(InstrumentModel_.modelid), modelId));
            clauses.getExpressions().add(cb.isTrue(procedure.get(Capability_.active)));

            if (serviceTypeId != null) {
                Join<ServiceCapability, CalibrationType> calType = root.join(ServiceCapability_.calibrationType);
                Join<CalibrationType, ServiceType> serviceType = calType.join(CalibrationType_.serviceType);
                clauses.getExpressions().add(cb.equal(serviceType.get(ServiceType_.serviceTypeId), serviceTypeId));
            }
            if (costType != null) {
                clauses.getExpressions().add(cb.equal(root.get(ServiceCapability_.costType), costType));
            }
            if (businessSubdivId != null) {
				Join<ServiceCapability, Subdiv> subdiv = root.join(ServiceCapability_.organisation.getName());
				clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), businessSubdivId));
			}
			cq.where(clauses);
			
			return cq;
		});

	}

	@Override
	public List<ServiceCapability> find(InstrumentModel instrumentModel, CalibrationType calibrationType,
			CostType costType, Subdiv organisation) {
		return this.find(instrumentModel.getModelid(), calibrationType.getCalTypeId(), costType,
				organisation.getSubdivid());
	}
}
