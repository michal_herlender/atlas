package org.trescal.cwms.core.instrumentmodel.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InstrumentModelCompanyCalReqDTO {
	
	private Integer crid;
	private String description;
	private String mfr;
	private String model;
	private String instrumentModelName;
	private Integer modelid;
	private String modelType;

}
