package org.trescal.cwms.core.instrumentmodel.entity.hiremodel.db;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.hiremodel.HireModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

public interface HireModelService
{
	ResultWrapper addHireModelCategory(int hiremodelid, String plantcode, int categoryid);

	void deleteHireModel(HireModel hm);

	HireModel findHireModel(int id);

	List<HireModel> getAllHireModels();

	ResultWrapper getHireModelPlantCode(int modelid, HttpSession session);
	
	HireModel get(InstrumentModel instrumentModel, Company allocatedCompany);
	
	void insertHireModel(HireModel hm);

	void saveOrUpdateHireModel(HireModel hm);

	void updateHireModel(HireModel hm);
}