package org.trescal.cwms.core.instrumentmodel.entity.hiremodel.db;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrumentmodel.entity.hiremodel.HireModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

public interface HireModelDao extends AllocatedDao<Company, HireModel, Integer>
{
	HireModel get(InstrumentModel instrumentModel, Company allocatedCompany);
}