package org.trescal.cwms.core.instrumentmodel.controller;


import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@Controller
@RequestMapping(value="/modeldescription.htm")
public class ModelDescriptionController {
	
	@Autowired
	InstrumentModelService instrumentModelService;
	
	@Autowired
	SupportedLocaleService supportedLocaleService;
	
	@RequestMapping(method=RequestMethod.GET)
	public @ResponseBody String outputModelName(@RequestParam("modelid") int modeId, @RequestParam("language") String languageCode){
		
		InstrumentModel model = instrumentModelService.findInstrumentModel(modeId);
		
		Locale defaultLocale = supportedLocaleService.getPrimaryLocale();
		
		Locale locale = defaultLocale;
		
		//Obviously this might not find exactly the correct locale but it's only the language that matters in this instance.
		for(Locale supportedLocale : supportedLocaleService.getSupportedLocales()){
			if(supportedLocale.getLanguage().equals(languageCode)){
				locale = supportedLocale;
			}
		}
		
		return InstModelTools.modelNameViaTranslations(model, locale, defaultLocale);

	}

}
