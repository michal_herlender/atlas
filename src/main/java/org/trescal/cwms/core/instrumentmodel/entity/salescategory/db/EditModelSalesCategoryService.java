package org.trescal.cwms.core.instrumentmodel.entity.salescategory.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.form.EditModelSalesCategoryForm;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface EditModelSalesCategoryService {
	void editModelSalesCategoryService(EditModelSalesCategoryForm form);
	List<KeyValueIntegerString> getSalesCategoryOptions(InstrumentModel instModel, Locale locale);
}

