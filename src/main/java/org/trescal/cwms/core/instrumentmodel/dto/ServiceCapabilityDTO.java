package org.trescal.cwms.core.instrumentmodel.dto;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.system.entity.translation.Translation;

import java.util.Locale;
import java.util.Set;

@Getter
@Setter
public class ServiceCapabilityDTO {

	private int id;
	private String subdiv;
	private String costType;
	private String calibrationType;
	private String procedure;
	private String calibrationProcess;
	private String workInstruction;
	private String checkSheet;
	private String procedureRef;
	
	private Integer modelId;
	private Integer calTypId;
	private Integer procedureId;
	private Integer processId;
	
	private InstrumentModel instruModel;
	
	
	public ServiceCapabilityDTO(String subname, ServiceCapability serviceCapability, Locale locale, Locale defaultLocale) {
        id = serviceCapability.getId();
        setSubdiv(subname);
        costType = serviceCapability.getCostType().getMessage();

        Set<Translation> translations = serviceCapability.getCalibrationType().getServiceType().getLongnameTranslation();
        Translation translation = translations.stream().filter(t -> t.getLocale().equals(locale)).findAny().orElse(null);
        if (translation == null)
            translation = translations.stream().filter(t -> t.getLocale().equals(defaultLocale)).findAny().orElse(null);
        if (translation != null) calibrationType = translation.getTranslation();


        procedure = serviceCapability.getCapability() == null ? "" : serviceCapability.getCapability().getReference() + " - " + serviceCapability.getCapability().getName();
        calibrationProcess = serviceCapability.getCalibrationProcess() == null ? "" : serviceCapability.getCalibrationProcess().getProcess();
        workInstruction = serviceCapability.getWorkInstruction() == null ? "" : serviceCapability.getWorkInstruction().getName();
        checkSheet = serviceCapability.getChecksheet();

        modelId = serviceCapability.getInstrumentModel().getModelid();
        calTypId = serviceCapability.getCalibrationType().getCalTypeId();
        procedureId = serviceCapability.getCapability().getId();
        processId = serviceCapability.getCalibrationProcess() == null ? null : serviceCapability.getCalibrationProcess().getId();

        procedureRef = serviceCapability.getCapability().getReference();
        instruModel = serviceCapability.getInstrumentModel();

    }

}