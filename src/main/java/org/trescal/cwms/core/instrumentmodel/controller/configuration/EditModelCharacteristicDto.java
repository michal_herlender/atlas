package org.trescal.cwms.core.instrumentmodel.controller.configuration;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditModelCharacteristicDto {

	@NotNull
  	private Integer characteristicDescriptionId;

	@NotNull
	private Boolean included;						// Whether a model range should be included/saved

	private Double decimalStart;					// For VALUE_WITH_UNIT (start or end must be set when included)
	private Double decimalEnd;						// For VALUE_WITH_UNIT (start or end must be set when included)

	private String textStart;						// For FREE_FIELD (start or end must be set when included)
	private String textEnd;							// For FREE_FIELD (start or end must be set when included)

	@NotNull(groups={Library.class})
	private Integer characteristicLibraryId;		// For LIBRARY

	@NotNull(groups={ValueWithUnit.class})
	private Integer uomId;							// For VALUE_WITH_UNIT
	
	// Validation groups
	public static interface ValueWithUnit {};
	public static interface FreeField {};
	public static interface Library {};
}
