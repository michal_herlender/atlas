package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.dto.*;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchByLinkedQuotationForm;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchInstrumentForm;
import org.trescal.cwms.core.login.SessionUtilsServiceImpl;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.referential.dto.TMLInstrumentModelDTO;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

import javax.persistence.Tuple;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service("InstrumentModelService")
public class InstrumentModelServiceImpl extends BaseServiceImpl<InstrumentModel, Integer>
		implements InstrumentModelService {

	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private InstrumentModelDao instrumentModelDao;
	@Autowired
	private MessageSource messages;
	@Autowired
	private SystemComponentService scServ;
	@Autowired
	private CatalogPriceService priceServ;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private SupportedCurrencyService supportedCurrencyService;
	@Autowired
	private QuotationItemService quotationItemService;
	@Autowired
	private SessionUtilsServiceImpl sessionUtilsService;


	@Override
	protected BaseDao<InstrumentModel, Integer> getBaseDao() {
		return instrumentModelDao;
	}

	public void deleteInstrumentModel(InstrumentModel instrumentmodel) {
		this.instrumentModelDao.remove(instrumentmodel);
	}

	@Override
	public ServiceCapabilitiesAndCurrencyWrapper findAjaxCapabilityAndCurrencyDetails(int modelid, String curCode) {
		InstrumentModel im = this.findInstrumentModel(modelid);
		SupportedCurrency currency = supportedCurrencyService.findCurrencyByCode(curCode);
		if (im == null) {
			String message = this.messages.getMessage("error.instrumentmodel.modelid.notfound",
					new Object[] { modelid }, "Model not found", null);
			return new ServiceCapabilitiesAndCurrencyWrapper(false, message, null, null, null, null);
		} else if (currency == null) {
			String message = this.messages.getMessage("error.currency.currencycode.notfound", new Object[] { curCode },
					"Currency not found", null);
			return new ServiceCapabilitiesAndCurrencyWrapper(false, message, null, null, null, null);
		} else {
			List<Integer> pricedCalibrationTypeIds = im.getCatalogPrices().stream()
					.map(cp -> cp.getServiceType().getCalibrationType().getCalTypeId()).collect(Collectors.toList());
			return new ServiceCapabilitiesAndCurrencyWrapper(true, "", pricedCalibrationTypeIds, curCode,
					currency.getCurrencyERSymbol(), currency.getDefaultRate());
		}
	}

	@Override
	public InstrumentModel findLazyInstrumentModel(int id) {
		return this.instrumentModelDao.find(id);
	}

	@Override
	public InstrumentModel findInstrumentModel(int id) {
		InstrumentModel im = this.instrumentModelDao.find(id);
		// 2018-12-12 GB : Prevent directory lookup on repeated gets.
		if (im != null && im.getDirectory() == null) {
			im.setDirectory((this.compDirServ.getDirectory(this.scServ.findComponent(Component.INSTRUMENTMODEL),
					String.valueOf(im.getModelid()))));
		}
		return im;
	}

	@Override
	public List<InstrumentModel> findInstrumentModel(Description description) {
		return this.instrumentModelDao.findInstrumentModel(description);
	}

	@Override
	public List<InstrumentModel> findInstrumentModel(SalesCategory salesCategory) {
		return this.instrumentModelDao.findInstrumentModel(salesCategory);
	}

	@Override
	public InstrumentModel findCapabilityModel(Description description) throws HibernateException {
		return instrumentModelDao.findCapabilityModel(description);
	}

	@Override
	public InstrumentModel findSalesCategoryModel(SalesCategory salesCategory) throws HibernateException {
		return instrumentModelDao.findSalesCategoryModel(salesCategory);
	}

	@Override
	public List<InstrumentModel> getAllInstrumentModels() {
		return this.instrumentModelDao.findAll();
	}

	@Override
	public List<InstrumentModelWithPriceDTO> getAll(DomainType domainType, Locale locale, ServiceType serviceType,
			Company allocatedCompany) {
		return this.instrumentModelDao.getAll(domainType, locale, serviceType, allocatedCompany);
	}

	public List<Integer> getDistinctModelidListForQuote(int quoteId) {
		return this.instrumentModelDao.getDistinctModelidListForQuote(quoteId);
	}

	@Override
	public PagedResultSet<InstrumentModelCompanyCalReqDTO> getInstrumentModelsWithCompanyCalRequirements(int coid,
			String mfr, String model, String desc, Integer resultsPerPage, Integer currentPage, Locale locale,
			boolean onlyWithCalReqs) {
		return instrumentModelDao.getInstrumentModelsWithCompanyCalRequirements(coid, mfr, model, desc, resultsPerPage,
				currentPage, locale, onlyWithCalReqs);
	}

	@Override
	public List<Tuple> getModelIdsForInstruments(Collection<Integer> instrumentIds) {
		return this.instrumentModelDao.getModelIdsForInstruments(instrumentIds);
	}

	@Override
	public BigDecimal getModelDefaultCalCost(InstrumentModel model, CalibrationType calType, BigDecimal exRate,
			Company company) {
		BigDecimal cost = new BigDecimal("0.00");

		if (model != null && model.getCatalogPrices().size() > 0 && calType != null) {

			CatalogPrice price = priceServ.findSingle(model, calType.getServiceType(), CostType.CALIBRATION, company);
			if (price != null) {
				cost = price.getFixedPrice();
			}
			if (exRate != null) {
				cost = cost.multiply(exRate);
			}
		}
		return cost;

	}

	@Override
	public List<InstrumentModel> getModelsByIds(Collection<Integer> modelids) {
		return this.instrumentModelDao.getModelsByIds(modelids);
	}

	public PagedResultSet<InstrumentModel> getModelsFromIds(PagedResultSet<InstrumentModel> prs,
			List<Integer> modelIds) {
		List<InstrumentModel> models = this.instrumentModelDao.getModelsFromIds(modelIds);
		// get count of results
		prs.setResultsCount(models.size());
		// update the query so it will return the correct number of rows
		// from the correct start point
		if ((prs.getResultsPerPage() + prs.getStartResultsFrom()) >= models.size()) {
			models = models.subList(prs.getStartResultsFrom(), (models.size()));
		} else {
			models = models.subList(prs.getStartResultsFrom(), (prs.getStartResultsFrom() + prs.getResultsPerPage()));
		}
		// get results
		prs.setResults(models);
		return prs;
	}

	@Override
	public InstrumentModelPlantillasDTO getPlantillasModel(Locale locale, int modelid) {
		return this.instrumentModelDao.getPlantillasModel(locale, modelid);
	}

	@Override
	public PagedResultSet<InstrumentModelPlantillasDTO> getPlantillasModels(Locale locale, Date afterLastModified,
			int resultsPerPage, int currentPage) {
		return this.instrumentModelDao.getPlantillasModels(locale, afterLastModified, resultsPerPage, currentPage);
	}

	@Override
	public List<InstrumentModel> searchInstrumentModels(InstrumentAndModelSearchForm<InstrumentModel> form) {
		// 2016-12-21 GB - Updated to use a reasonable size, was unrestricted before
		PagedResultSet<InstrumentModel> prs = new PagedResultSet<>(500, 1);
		return (List<InstrumentModel>) this.searchInstrumentModelsPaged(prs, form).getResults();
	}

	@Override
	public PagedResultSet<InstrumentModel> searchInstrumentModelsPaged(PagedResultSet<InstrumentModel> prs,
			InstrumentAndModelSearchForm<InstrumentModel> form) {
		return this.instrumentModelDao.searchInstrumentModelsPaged(prs, form);
	}

	public List<InstModSearchResultWrapper> searchInstrumentModelsDWR(String mfr, String model, String description,
			Integer descriptionId, Integer manufacturerId) {
		final List<InstModSearchResultWrapper> wrappersWithModelNames = new ArrayList<>();
		final InstrumentAndModelSearchForm<InstrumentModel> searchForm = new InstrumentAndModelSearchForm<>();
		searchForm.setMfrNm(mfr);
		searchForm.setModel(model);
		searchForm.setDescNm(description);
		searchForm.setDescId(descriptionId);
		searchForm.setMfrId(manufacturerId);
		final List<InstrumentModel> models = this.searchInstrumentModels(searchForm);
		for (InstrumentModel instrumentmModel : models) {
			InstModSearchResultWrapper wrapper = new InstModSearchResultWrapper(instrumentmModel.getModelid(),
					instrumentmModel.getModel(), instrumentmModel.getMfr().getName(),
					translationService.getCorrectTranslation(instrumentmModel.getDescription().getTranslations(),
							LocaleContextHolder.getLocale()),
					instrumentmModel.getModelMfrType());
			String fullModelName = InstModelTools.modelNameViaTranslations(instrumentmModel,
					LocaleContextHolder.getLocale(), supportedLocaleService.getPrimaryLocale());
			wrapper.setFullModelName(fullModelName);
			wrappersWithModelNames.add(wrapper);
		}
		return wrappersWithModelNames;
	}

	@Override
	public List<InstrumentModel> searchModelSignature(Integer mfrId, String model, Integer descriptionId,
			Integer modelTypeId, Integer excludeModelId) {
		return this.instrumentModelDao.searchModelSignature(mfrId, model, descriptionId, modelTypeId, excludeModelId);
	}

	@Override
	public InstrumentModel getByTmlId(Integer tmlModelId) {
		return this.instrumentModelDao.getByTmlId(tmlModelId);
	}
	
	@Override
	public Long getCount() {
		return this.instrumentModelDao.getCount();
	}

	@Override
	@Deprecated
	public InstrumentModel getTMLInstrumentModel(int tmlid) {
		return this.instrumentModelDao.getTMLInstrumentModel(tmlid);
	}

	@Override
	@Deprecated
	public InstrumentModel insertTMLInstrumentModel(TMLInstrumentModelDTO dto) {
		return this.instrumentModelDao.insertTMLInstrumentModel(dto);
	}

	@Override
	@Deprecated
	public InstrumentModel updateTMLInstrumentModel(TMLInstrumentModelDTO dto) {
		return this.instrumentModelDao.updateTMLInstrumentModel(dto);
	}

	/**
	 * Regenerates the embedded transaltions for an instrument model Can be called
	 * for a new (unsaved) model or on a previously saved model instModel should
	 * already have nameTranslations initialized
	 */
	@Override
	public void regenerateInstrumentModelTranslations(InstrumentModel instModel) {
		for (Locale locale : this.supportedLocaleService.getSupportedLocales()) {
			updateInstrumentModelTranslation(instModel, locale);
		}
	}

	private boolean updateInstrumentModelTranslation(InstrumentModel instModel, Locale locale) {
		Translation trans = null;
		for (Translation t : instModel.getNameTranslations()) {
			if (locale.equals(t.getLocale())) {
				trans = t;
				break;
			}
		}
		String modelName = InstModelTools.modelName(instModel, locale, Locale.UK);
		boolean updated = false;
		if (trans != null) {
			if (!modelName.equals(trans.getTranslation())) {
				// debug mode format : modelid|modeltype|locale|oldTranslation|newTranslation
				if (log.isDebugEnabled()) log.debug("Update|" + instModel.getModelid() + "|" + instModel.getModelType().getInstModelTypeId()
					+ "|" + locale + "|" + trans.getTranslation() + "|" + modelName);
				trans.setTranslation(modelName);
				updated = true;
			}
		} else {
			// Add new translation
			if (log.isDebugEnabled()) log.debug("Add|" + instModel.getModelid() + "|" + instModel.getModelType().getInstModelTypeId() + "|"
					+ locale.toString() + "||" + modelName);
			trans = new Translation(locale, modelName);
			instModel.getNameTranslations().add(trans);
			updated = true;
		}
		return updated;
	}

	@Override
	public int updateInstrumentModelTranslations(Collection<InstrumentModel> instModels, Locale locale) {
		if (log.isDebugEnabled()) log.debug("Scanning " + instModels.size() + " models for locale " + locale.toString());
		int scannedCount = 0;
		int updatedCount = 0;
		for (InstrumentModel instModel : instModels) {
            boolean changed = updateInstrumentModelTranslation(instModel, locale);
            if (changed) {
                updatedCount++;
            }
            scannedCount++;
            if (log.isDebugEnabled() && (scannedCount % 1000 == 0))
                log.debug("Scanned " + scannedCount + " updated " + updatedCount);
        }
		if (log.isDebugEnabled()) log.debug("Done, scanned " + scannedCount + " updated " + updatedCount);
		return updatedCount;
	}

	/**
	 * Scans/updates translations and returns a short description of how many updated for each locale.  
	 */
	@Override
	public String updateAllInstrumentModelTranslations(Collection<Locale> locales, int offset, int batchSize) {
        // Performance is worse with fetching translations in paged results (HHH000104 issue)
        boolean fetchTranslations = false;
        List<InstrumentModel> models = instrumentModelDao.getPaged(offset, batchSize, fetchTranslations);
        StringBuilder result = new StringBuilder();
        for (Locale locale : locales) {
            if (result.length() > 0) result.append(", ");
            result.append(locale.toString());
            result.append("=");
            int updatedCount = updateInstrumentModelTranslations(models, locale);
            result.append(updatedCount);
        }

        return result.toString();
    }

	@Override
	public List<InstrumentModel> getOtherModelsWithMatchingSalesCategory(InstrumentModel instrumentModel) {
		return instrumentModelDao.getOtherModelsWithMatchingSalesCategory(instrumentModel);
	}

	@Override
	public List<InstrumentModel> getStandAloneModelsMatchingSalesCategoryModel(InstrumentModel salesCategoryModel) {
		String modelName = translationService.getCorrectTranslation(salesCategoryModel.getNameTranslations(),
				LocaleContextHolder.getLocale());
		return instrumentModelDao.getStandAloneModelsMatchingSalesCategoryAndModelName(
				salesCategoryModel.getSalesCategory(), modelName, LocaleContextHolder.getLocale());
	}

	@Override
	public PagedResultSet<InstrumentModelForNewJobItemSearchDto> searchInstrumentModelsForNewJobItem(NewJobItemSearchInstrumentForm form) {
		val prs = instrumentModelDao.findCompanyOrGroupInstrumentModelForNewJobItemSearch(form);
		unsafeUpdateAvailableQuotations(form.getJobId(), prs);
		return prs;
	}

	@Override
	public PagedResultSet<InstrumentModelForNewJobItemSearchDto> searchAllMatchingInstrumentModelsForNewJobItem(NewJobItemSearchInstrumentForm form) {
		val prs = instrumentModelDao.findAllMatchingInstrumentModelForNewJobItemSearch(form);
		unsafeUpdateAvailableQuotations(form.getJobId(), prs);
		return prs;
	}

	@Override
	public PagedResultSet<InstrumentModelForNewJobItemSearchDto> findModelsByIdsForNewJobItemSearch(NewJobItemSearchByLinkedQuotationForm form, List<Integer> modelIds) {
		val prs = instrumentModelDao.findModelsByIdsForNewJobItemSearch(form,modelIds);
		unsafeUpdateAvailableQuotations(form.getJobId(), prs);
		return prs;
	}

	@Override
	public PagedResultSet<InstrumentModelForNewJobItemSearchDto> findCompanyModelsByIdsForNewJobIntemSearch(NewJobItemSearchByLinkedQuotationForm form, List<Integer> modelIds) {
		val prs = instrumentModelDao.findCompanyModelsByIdsForNewJobIntemSearch(form,modelIds);
		unsafeUpdateAvailableQuotations(form.getJobId(), prs);
		return prs;
	}

	private void unsafeUpdateAvailableQuotations(Integer jobId , PagedResultSet<InstrumentModelForNewJobItemSearchDto> prs) {
		val modelIds = prs.getResults().stream().map(InstrumentModelForNewJobItemSearchDto::getModelId).collect(Collectors.toList());
		val options = quotationItemService.findQuotationItemsOptionsForModelIdsJobIdAndDefaultUsage(modelIds, jobId,sessionUtilsService.getCurrentContact().getUserPreferences().getDefaultServiceTypeUsage());
		prs.getResults().forEach(i -> i.setAvailableQuotations(options.get(i.getModelId())));
	}
}