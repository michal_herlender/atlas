/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.entity.modelrange;

import java.text.DecimalFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.instrumentmodel.RangeType;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.option.Option;
import org.trescal.cwms.core.instrumentmodel.entity.range.Range;
import org.trescal.cwms.core.tools.NumberTools;

import lombok.Setter;

/**
 * Note 2019-11-19 : We should consider making this non-Comparable,
 * and use a comparator externally 
 * as existing compareTo() is inconsistent with equals
 */
@Entity
@Table(name = "modelrange")
@Setter
public class ModelRange extends Range implements Comparable<ModelRange>
{
	private InstrumentModel model;
	private Option modelOption;
	private RangeType characteristicType;
	private CharacteristicDescription characteristicDescription;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelid", nullable=false)
	public InstrumentModel getModel()
	{
		return this.model;
	}
	
	@Override
	public int compareTo(ModelRange other) {
		if (this.getCharacteristicDescription() != null && other.getCharacteristicDescription() != null)
		{
			return this.getCharacteristicDescription().getOrderno() - other.getCharacteristicDescription().getOrderno();
		}
		return this.getId() - other.getId();
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modeloptionid")
	public Option getModelOption() {
		return modelOption;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "characteristicdescriptionid", nullable = true)
	public CharacteristicDescription getCharacteristicDescription() {
		return characteristicDescription;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "characteristictype",columnDefinition = "nvarchar(2000)", nullable=false)
	public RangeType getCharacteristicType() {
		return characteristicType;
	}
	
	@Transient
	public String getValue()
	{
		return this.modelOption == null ? this.rangeValue() : "";
	}

	@Transient
	@Override
	public String rangeValue()
	{
		StringBuffer sb = new StringBuffer();
		
		if (this.characteristicDescription != null)
		{
			switch (this.characteristicDescription.getCharacteristicType()) 
			{
				case FREE_FIELD:
				{
					if (super.getMaxvalue() == null)
					{
						if (super.isMaxIsInfinite())
						{
							sb.append(">= ").append(super.getMinvalue()).append(" ");
						}
						else
						{
							sb.append(super.getMinvalue()).append(" ");
						}
					}
					else
					{
						if (!super.getMinvalue().equals("0"))
						{
							sb.append(super.getMinvalue());
							sb.append(" ");
							if (super.getMinvalue() != null)
							{
								sb.append("- ").append(super.getMaxvalue()).append(" ");
							}
						}	
						else
						{
							sb.append("<= ").append(super.getMaxvalue()).append(" ");
						}
					}			
					break;
				}
				case LIBRARY:
				{
//					if (this.modelRangeCharacteristicLibraries != null)
//					{
//						for (ModelRangeCharacteristicLibrary modelRangeCharacteristicLibrary : modelRangeCharacteristicLibraries) {
//							if (!modelRangeCharacteristicLibrary.equals(modelRangeCharacteristicLibraries.toArray()[0])) sb.append(" - ");
//							sb.append(modelRangeCharacteristicLibrary.getCharacteristicLibrary().getName());
//						}
//					}
					break;
				}
				case VALUE_WITH_UNIT:
				{
					if (super.getEnd() == null)
					{
						if (super.isMaxIsInfinite())
						{
							sb.append(">= ").append(NumberTools.printPreciseDecimal(super.getStart())).append(" ");
						}
						else
						{
							sb.append(NumberTools.printPreciseDecimal(super.getStart())).append(" ");
						}
					}
					else						
					{
						if (super.getStart() > 0)
						{
							sb.append(NumberTools.printPreciseDecimal(super.getStart()));
							sb.append(" ");
							if (super.getEnd() != null)
							{
								sb.append("- ").append(NumberTools.printPreciseDecimal(super.getEnd())).append(" ");
							}
						}
						else
						{
							sb.append("<= ").append(NumberTools.printPreciseDecimal(super.getEnd())).append(" ");
						}
					}
					sb.append(super.getUom().toString());
					break;
				}
				default:
				{
					break;
				}
			}
		}
		else
		{
			sb.append(NumberTools.printPreciseDecimal(super.getStart()));
			sb.append(" ");
			if (super.getEnd() != null)
			{
				sb.append("to ").append(NumberTools.printPreciseDecimal(super.getEnd())).append(" ");
			}
			sb.append(super.getUom().toString());
		}
		return sb.toString();
	}
	
	@Transient
	@Override
	public String toString() {
		//Set decimal formating
		DecimalFormat decimalFormat = new DecimalFormat("0.#");
		StringBuffer result = new StringBuffer();
		if(getCharacteristicDescription() != null && getCharacteristicDescription().getCharacteristicType() == CharacteristicType.FREE_FIELD){
			// Free field characteristic
			if (getMinvalue() != null || !getMinvalue().isEmpty())
				result.append(getMinvalue());
		}
		// value with unit characteristic
		if (getCharacteristicDescription() != null && getCharacteristicDescription().getCharacteristicType() == CharacteristicType.VALUE_WITH_UNIT) {
			//only a minimum value
			if (getStart() != null && getEnd() == null) {
				result.append(decimalFormat.format(getStart()));
				result.append(" ");
				result.append(getUom().getFormattedSymbol());
			}
			//only a maximum value
			if (getStart() == null && getEnd() != null) {
				result.append(decimalFormat.format(getEnd()));
				result.append(" ");
				result.append(getUom().getFormattedSymbol());
			}
			//max and min values
			if (getStart() != null && getEnd() != null) {
				// max and min equal
				if (getStart().equals(getEnd())){
					result.append(decimalFormat.format(getStart()));
					result.append(" ");
					result.append(getUom().getFormattedSymbol());
				} else {
					result.append(decimalFormat.format(getStart()));
					result.append(" ");
					result.append(getUom().getFormattedSymbol());
					result.append(" < ");
					result.append(decimalFormat.format(getEnd()));
					result.append(" ");
					result.append(getMaxUom() != null ? getMaxUom().getFormattedSymbol() : getUom().getFormattedSymbol());
				}
			}
		}
		return result.toString();
	}
}