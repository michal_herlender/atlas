package org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.ModelRangeCharacteristicLibrary;

@Service
public class ModelRangeCharacteristicLibraryServiceImpl extends
		BaseServiceImpl<ModelRangeCharacteristicLibrary, Integer> implements ModelRangeCharacteristicLibraryService {

	@Autowired
	private ModelRangeCharacteristicLibraryDao baseDao;
	
	@Override
	protected BaseDao<ModelRangeCharacteristicLibrary, Integer> getBaseDao() {
		return this.baseDao;
	}


}
