/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.entity.componentdescription;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;

/**
 * Descriptions used to form {@link Component}.
 * 
 * @author richard
 */
@Entity
@Table(name = "componentdescription")
public class ComponentDescription
{
	private Set<Component> components;
	private String description;
	private int id;

	@OneToMany(mappedBy = "description", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Component> getComponents()
	{
		return this.components;
	}

	@NotNull
	@Length(min = 1, max = 100)
	@Column(name = "description", unique = true, nullable = false, length = 100)
	public String getDescription()
	{
		return this.description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "descriptionid", nullable = false)
	public int getId()
	{
		return this.id;
	}

	public void setComponents(Set<Component> components)
	{
		this.components = components;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setId(int id)
	{
		this.id = id;
	}
}
