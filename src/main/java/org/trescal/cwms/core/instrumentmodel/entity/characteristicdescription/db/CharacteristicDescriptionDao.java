package org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.referential.dto.TMLCharacteristicDTO;
import org.trescal.cwms.spring.model.LabelIdDTO;

public interface CharacteristicDescriptionDao extends BaseDao<CharacteristicDescription, Integer> {

	List<LabelIdDTO> getLabelIdList(String descriptionFragment, Locale searchLanguage, int maxResults);
	
	CharacteristicDescription getByInternalNameAndSubfamilyId(String internalName, Integer subfamilyId);
	
	CharacteristicDescription getTMLCharacteristicDescription(int tmlid);

	void insertTMLCharacteristicDescription(TMLCharacteristicDTO dto) throws Exception;

	void updateTMLCharacteristicDescription(TMLCharacteristicDTO dto) throws Exception;

	void deleteTMLCharacteristicDescription(TMLCharacteristicDTO dto) throws Exception;
}
