package org.trescal.cwms.core.instrumentmodel.entity.option.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.entity.option.Option;
import org.trescal.cwms.core.referential.dto.TMLOptionDTO;

public interface OptionDao extends BaseDao<Option, Integer> {
	
	Option getTMLOption(int tmlModelid,String code);
	Option getTMLOption(int tmlid);	
	void insertTMLOption(TMLOptionDTO dto) throws Exception;
	void updateTMLOption(TMLOptionDTO dto) throws Exception;
	void deleteTMLOption(TMLOptionDTO dto) throws Exception;
}