package org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription;

import java.io.Serializable;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public enum CharacteristicType implements Serializable {
	FREE_FIELD("Free_field", "viewinstrument.instrumentmodel.characteristicType1"),
	VALUE_WITH_UNIT("Value_with_unit", "viewinstrument.instrumentmodel.characteristicType2"),
	LIBRARY("Library", "viewinstrument.instrumentmodel.characteristicType3");

	private String messageCode;
	private String defaultName;
	private MessageSource messageSource;

	private CharacteristicType(String defaultName, String messageCode) {
		this.messageCode = messageCode;
		this.defaultName = defaultName;
	}

	@Component
	public static class CharacteristicTypeMessageInjector {
		@Autowired
		private MessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (CharacteristicType characteristicType : CharacteristicType.values()) {
				characteristicType.setMessageSource(this.messageSource);
			}
		}
	}

	public String getMessageCode() {
		return messageCode;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public String getDefaultName() {
		return defaultName;
	}

	public void setDefaultName(String defaultName) {
		this.defaultName = defaultName;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getMessage() {
		Locale locale = LocaleContextHolder.getLocale();
		return getMessageForLocale(locale);
	}

	public String getMessageForLocale(Locale locale) {
		return this.messageSource.getMessage(messageCode, null, defaultName, locale);
	}
}