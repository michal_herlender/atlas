package org.trescal.cwms.core.instrumentmodel.entity.uom.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.referential.dto.TMLUnitDTO;

public interface UoMDao extends BaseDao<UoM, Integer> {
	
	List<UoM> getAllUoMsByName();
	
	UoM findDefaultUoM();
	
	UoM findUoMbySymbol(String symbol);
	
	UoM getTMLUoM(int tmlid);
	
	void insertTMLUoM(TMLUnitDTO dto) throws Exception;
	
	void updateTMLUoM(TMLUnitDTO dto) throws Exception;
	
	void deleteTMLUoM(TMLUnitDTO dto) throws Exception;
}