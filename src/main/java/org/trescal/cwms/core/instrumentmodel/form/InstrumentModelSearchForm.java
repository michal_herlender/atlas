package org.trescal.cwms.core.instrumentmodel.form;

import java.util.List;
import java.util.Map;

import org.trescal.cwms.core.instrumentmodel.controller.InstrumentModelSearchController;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.Data;

/**
 * Form backing object for {@link InstrumentModelSearchController}.
 * 
 * @author richard
 */
@Data
public class InstrumentModelSearchForm
{
	private List<CalibrationType> calTypes;
	private String desc;
	private Integer descId;
	private String domainNm;
	private Integer domainId;
	private String familyNm;
	private Integer familyId;
	private boolean hireOnlyModels;
	private String mfrtext;
	private Integer mfrId;
	private String mfrType;
	private Integer modelTypeId;
	private String model;
	private ModelMfrType modelMfrType;
	private int pageNo;
	private Boolean excludeQuarantined;
	private int resultsPerPage;
	private PagedResultSet<InstrumentModel> rs;
	private String salesCat;
	private Integer salesCatId;
	private List<String> searchedfor;
	private Map<Integer, Integer> characteristics;
	private String sortBy;

}