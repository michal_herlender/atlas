/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.entity;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;

/**
 * Enum identifying the two main types of {@link InstrumentModel}, those that
 * have a specific {@link Mfr} defined and those that do not (and are therefore
 * considered generic {@link InstrumentModel}s of that type.
 * 
 * @author richard
 */
public enum ModelMfrType
{
	MFR_SPECIFIC(true), MFR_GENERIC(false);

	private boolean mfrRequiredForModel;

	ModelMfrType(boolean mfrRequiredForModel)
	{
		this.mfrRequiredForModel = mfrRequiredForModel;
	}

	public String getName()
	{
		return this.name();
	}

	public boolean isMfrRequiredForModel()
	{
		return this.mfrRequiredForModel;
	}

	public void setMfrRequiredForModel(boolean mfrRequiredForModel)
	{
		this.mfrRequiredForModel = mfrRequiredForModel;
	}
}