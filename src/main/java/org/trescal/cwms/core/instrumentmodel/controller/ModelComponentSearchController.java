package org.trescal.cwms.core.instrumentmodel.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.instrumentmodel.entity.component.db.ComponentService;
import org.trescal.cwms.core.instrumentmodel.form.ModelComponentSearchForm;
import org.trescal.cwms.core.instrumentmodel.form.ModelComponentSearchValidator;

@Controller
public class ModelComponentSearchController
{
	protected final Log logger = LogFactory.getLog(getClass());
	@Autowired
	private ComponentService compServ;

	@ModelAttribute("command")
	protected ModelComponentSearchForm formBackingObject(HttpServletRequest request) throws Exception
	{
		return new ModelComponentSearchForm();
	}
	@InitBinder("formBackingObject")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new ModelComponentSearchValidator());
    }

	@RequestMapping(value="/searchmodelcomponentform.htm", method=RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/instrumentmodel/searchmodelcomponentform";
	}
	
	@RequestMapping(value="/searchmodelcomponentform.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(@ModelAttribute("command") @Validated ModelComponentSearchForm form, BindingResult bindingResult) throws Exception
	{
		this.logger.info("Entering ModelComponentSearchController handleRequest method ....");
		if (bindingResult.hasErrors()) {
			return new ModelAndView("trescal/core/instrumentmodel/searchmodelcomponentform", "command", form);
		}

		Integer mfrid = ((form.getMfrid() == null) || form.getMfrid().equals("")) ? 0 : Integer.parseInt(form.getMfrid());
		Integer descid = ((form.getDescid() == null) || form.getDescid().equals("")) ? 0 : Integer.parseInt(form.getDescid());

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("modelcomponents", this.compServ.searchSortedModelComponent(mfrid, descid, form.isInStock()));

		return new ModelAndView("trescal/core/instrumentmodel/searchmodelcomponentresults", model);
	}
}
