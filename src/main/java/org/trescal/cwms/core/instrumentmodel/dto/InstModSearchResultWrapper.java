package org.trescal.cwms.core.instrumentmodel.dto;

import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;

import lombok.Getter;
import lombok.Setter;

/**
 * Class is only used in WebSearchPlugin.js and related code via DWR for web portal
 *
 */
@Getter @Setter
public class InstModSearchResultWrapper {
	private String description;
	private String mfr;
	private String model;
	private int modelid;
	private ModelMfrType modMfrType;
	private String modelName;
	private String fullModelName;
	
	public InstModSearchResultWrapper(int modelid, String model, String mfr, String description,
			ModelMfrType modMfrType) {
		this.modelid = modelid;
		this.model = model;
		this.mfr = mfr;
		this.description = description;
		this.modMfrType = modMfrType;
	}

}