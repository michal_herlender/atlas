package org.trescal.cwms.core.instrumentmodel.dto;

public class MfrSearchResultWrapper
{
	private int mfrid;
	private String name;

	public MfrSearchResultWrapper(int mfrid, String name)
	{
		this.mfrid = mfrid;
		this.name = name;
	}

	public int getMfrid()
	{
		return mfrid;
	}

	public void setMfrid(int mfrid)
	{
		this.mfrid = mfrid;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

}