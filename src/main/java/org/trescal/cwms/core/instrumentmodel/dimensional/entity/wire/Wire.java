package org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;

@Entity
@Table(name = "dimensional_wire")
public class Wire
{
	private BigDecimal ba;
	private BigDecimal ballNo;
	private BigDecimal baPitch;
	private String designation;
	private BigDecimal eoISO;
	private BigDecimal eoUnified;
	private BigDecimal eoWidth;
	private int id;
	private BigDecimal metricPitch;
	private BigDecimal rubyBall;
	private Set<Thread> threads;
	private BigDecimal unifiedTPI;
	private BigDecimal whitworthTPI;
	private BigDecimal wire1;
	private BigDecimal wire2;

	private BigDecimal wireMean;

	@Column(name = "ba", precision = 20, scale = 5, nullable = true)
	public BigDecimal getBa()
	{
		return this.ba;
	}

	@Column(name = "ballno", precision = 20, scale = 5, nullable = true)
	public BigDecimal getBallNo()
	{
		return this.ballNo;
	}

	@Column(name = "bapitch", precision = 20, scale = 5, nullable = true)
	public BigDecimal getBaPitch()
	{
		return this.baPitch;
	}

	@NotNull
	@Length(min = 1, max = 50)
	@Column(name = "designation", length = 50, nullable = false)
	public String getDesignation()
	{
		return this.designation;
	}

	@Column(name = "eoiso", precision = 20, scale = 5, nullable = true)
	public BigDecimal getEoISO()
	{
		return this.eoISO;
	}

	@Column(name = "eounified", precision = 20, scale = 5, nullable = true)
	public BigDecimal getEoUnified()
	{
		return this.eoUnified;
	}

	@Column(name = "eowidth", precision = 20, scale = 5, nullable = true)
	public BigDecimal getEoWidth()
	{
		return this.eoWidth;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Column(name = "metricpitch", precision = 20, scale = 5, nullable = true)
	public BigDecimal getMetricPitch()
	{
		return this.metricPitch;
	}

	@Column(name = "rubyball", precision = 20, scale = 5, nullable = true)
	public BigDecimal getRubyBall()
	{
		return this.rubyBall;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "wire")
	public Set<Thread> getThreads()
	{
		return this.threads;
	}

	@Column(name = "unifiedtpi", precision = 20, scale = 5, nullable = true)
	public BigDecimal getUnifiedTPI()
	{
		return this.unifiedTPI;
	}

	@Column(name = "whitworthtpi", precision = 20, scale = 5, nullable = true)
	public BigDecimal getWhitworthTPI()
	{
		return this.whitworthTPI;
	}

	@Column(name = "wire1", precision = 20, scale = 5, nullable = true)
	public BigDecimal getWire1()
	{
		return this.wire1;
	}

	@Column(name = "wire2", precision = 20, scale = 5, nullable = true)
	public BigDecimal getWire2()
	{
		return this.wire2;
	}

	@Column(name = "wiremean", precision = 20, scale = 5, nullable = true)
	public BigDecimal getWireMean()
	{
		return this.wireMean;
	}

	public void setBa(BigDecimal ba)
	{
		this.ba = ba;
	}

	public void setBallNo(BigDecimal ballNo)
	{
		this.ballNo = ballNo;
	}

	public void setBaPitch(BigDecimal baPitch)
	{
		this.baPitch = baPitch;
	}

	public void setDesignation(String designation)
	{
		this.designation = designation;
	}

	public void setEoISO(BigDecimal eoISO)
	{
		this.eoISO = eoISO;
	}

	public void setEoUnified(BigDecimal eoUnified)
	{
		this.eoUnified = eoUnified;
	}

	public void setEoWidth(BigDecimal eoWidth)
	{
		this.eoWidth = eoWidth;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setMetricPitch(BigDecimal metricPitch)
	{
		this.metricPitch = metricPitch;
	}

	public void setRubyBall(BigDecimal rubyBall)
	{
		this.rubyBall = rubyBall;
	}

	public void setThreads(Set<Thread> threads)
	{
		this.threads = threads;
	}

	public void setUnifiedTPI(BigDecimal unifiedTPI)
	{
		this.unifiedTPI = unifiedTPI;
	}

	public void setWhitworthTPI(BigDecimal whitworth)
	{
		this.whitworthTPI = whitworth;
	}

	public void setWire1(BigDecimal wire1)
	{
		this.wire1 = wire1;
	}

	public void setWire2(BigDecimal wire2)
	{
		this.wire2 = wire2;
	}

	public void setWireMean(BigDecimal wireMean)
	{
		this.wireMean = wireMean;
	}
}