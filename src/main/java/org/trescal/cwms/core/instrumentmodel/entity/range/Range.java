/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.entity.range;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.tools.NumberTools;

import lombok.Setter;

/**
 * Abstract class representing a range that can be applied to an
 * {@link InstrumentModel} or Instrument.
 * 
 * @author richard
 */
@Setter
@MappedSuperclass
public abstract class Range extends Auditable
{
	private Double end;
	private int id;
	private Double start;
	private UoM uom;
	private UoM maxUom;
	private String minvalue;
	private String maxvalue;
	private boolean maxIsInfinite;
	
	@Column(name = "endd", nullable = true)
	public Double getEnd()
	{
		return this.end;
	}

	@Transient
	public String getEndAsString()
	{
		String endString = "";

		if ((this.end != null))
		{
			endString = NumberTools.convertDoubleToFormattedString(this.end.toString());
		}

		return endString;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@Column(name = "start", nullable = false)
	public Double getStart()
	{
		return this.start;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uomid", nullable = false)
	public UoM getUom()
	{
		return this.uom;
	}

	@Column(name = "minvalue", columnDefinition = "nvarchar(2000)")
	public String getMinvalue() {
		return minvalue;
	}

	@Column(name = "maxvalue", columnDefinition = "nvarchar(2000)")
	public String getMaxvalue() {
		return maxvalue;
	}

	@Column(nullable = false,columnDefinition = "bit default 0")
	public boolean isMaxIsInfinite() {
		return maxIsInfinite;
	}

	@Transient
	public String getStartAsString()
	{
		String startString = "";

		if (this.start != null)
		{
			startString = NumberTools.convertDoubleToFormattedString(this.start.toString());
		}

		return startString;
	}
	
	@Transient
	public String rangeValue() {
		// Subclasses may provide own implementation; e.g. see ModelRange
		StringBuffer sb = new StringBuffer();
		
		if (this.start != null) {
			sb.append(NumberTools.printPreciseDecimal(this.start));
			if (this.end != null)
			{
				sb.append(" to ");
			}
		}
		if (this.end != null)
		{
			sb.append(NumberTools.printPreciseDecimal(this.end));
		}
		sb.append(" ");
		sb.append(this.uom.toString());
		return sb.toString();
	}
	
	
	@Override
	@Transient
	public String toString()
	{
		// Subclasses may provide own implementation; e.g. see ModelRange
		return rangeValue();
	}
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "maxuomid", nullable = true)
	public UoM getMaxUom() {
		return maxUom;
	}
}
