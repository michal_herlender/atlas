package org.trescal.cwms.core.instrumentmodel.entity.salescategory.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.persistence.Tuple;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.spring.model.KeyValueIntegerString;
import org.trescal.cwms.spring.model.LabelIdDTO;
import org.trescal.cwms.spring.model.SalesCategoryJsonDTO;

public interface SalesCategoryService extends BaseService<SalesCategory, Integer> {

	List<SalesCategoryJsonDTO> getAllSalesCategories(Locale userLocale);
	
	SalesCategory getSalesCategory(Integer id);

	int getResultsCountExact(String translation, Locale searchLanguage,int excludeId);

	List<LabelIdDTO> getLabelIdList(String salesCategoryFragment,Locale searchLanguage, int maxResults);

	String getSalesCategoryTranslationForLocale(int id, Locale locale);

	List<Tuple> getSalesCategoryIdsForModelIds(Collection<Integer> modelIds);
	
	void regenerateSalesCategoryTranslations(SalesCategory salesCategory, InstrumentModel instModel);
	
	List<KeyValueIntegerString> getSalesCategoriesForSubfamily(Integer subFamilyId, Locale locale);
}
