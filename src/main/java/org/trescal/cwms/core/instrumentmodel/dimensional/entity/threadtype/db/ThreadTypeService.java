package org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.ThreadType;

public interface ThreadTypeService extends BaseService<ThreadType, Integer>
{
}