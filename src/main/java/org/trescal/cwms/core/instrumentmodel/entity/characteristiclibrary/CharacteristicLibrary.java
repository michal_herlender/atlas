package org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.ModelRangeCharacteristicLibrary;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "characteristiclibrary")
public class CharacteristicLibrary {

	private int characteristiclibraryid;
	private CharacteristicDescription characteristicDescription;
	private String name;	
	private Date log_createdon;	
	private Boolean active;	
	private Integer tmlid;
	
	private Set<ModelRangeCharacteristicLibrary> modelRangeCharacteristicLibraries;
	private Set<Translation> translation;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getCharacteristiclibraryid() {
		return characteristiclibraryid;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "characteristicdescriptionid", nullable = false)
	public CharacteristicDescription getCharacteristicDescription() {
		return characteristicDescription;
	}

	@Column(name = "name", nullable = false,columnDefinition = "nvarchar(2000)")
	public String getName() {
		return name;
	}

	@Column(name = "log_createdon", nullable=false)
	public Date getLog_createdon() {
		return log_createdon;
	}

	@Column(name = "active", nullable=false)
	public Boolean getActive() {
		return active;
	}

	@Column(name = "tmlid", nullable=true)
	public Integer getTmlid() {
		return tmlid;
	}
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "characteristicLibrary")
	public Set<ModelRangeCharacteristicLibrary> getModelRangeCharacteristicLibraries() {
		return modelRangeCharacteristicLibraries;
	}
	
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="characteristiclibrarytranslation", joinColumns=@JoinColumn(name="characteristiclibraryid"))
	public Set<Translation> getTranslation() {
		return translation;
	}
	
	public void setCharacteristiclibraryid(int characteristiclibraryid) {
		this.characteristiclibraryid = characteristiclibraryid;
	}
	
	public void setCharacteristicDescription(
			CharacteristicDescription characteristicDescription) {
		this.characteristicDescription = characteristicDescription;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setLog_createdon(Date log_createdon) {
		this.log_createdon = log_createdon;
	}
	
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	public void setTmlid(Integer tmlid) {
		this.tmlid = tmlid;
	}
	
	public void setModelRangeCharacteristicLibraries(Set<ModelRangeCharacteristicLibrary> modelRangeCharacteristicLibraries) {
		this.modelRangeCharacteristicLibraries = modelRangeCharacteristicLibraries;
	}
	
	public void setTranslation(Set<Translation> translations) {
		this.translation = translations;
	}
	
	@Transient
	public String toFullString(){
		return "CharacteristicLibrary [characteristiclibraryid=" + characteristiclibraryid + ", name=" + name
				+ ", translation=" + (translation != null ? translation : "") + ", active=" + active
				+ ", tmlid=" + tmlid + "]";
	}
}
