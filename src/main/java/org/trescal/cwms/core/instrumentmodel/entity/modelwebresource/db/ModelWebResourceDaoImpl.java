package org.trescal.cwms.core.instrumentmodel.entity.modelwebresource.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.modelwebresource.ModelWebResource;
import org.trescal.cwms.core.instrumentmodel.entity.modelwebresource.ModelWebResource_;

@Repository("ModelWebResourceDao")
public class ModelWebResourceDaoImpl extends BaseDaoImpl<ModelWebResource, Integer> implements ModelWebResourceDao {
	
	@Override
	protected Class<ModelWebResource> getEntity() {
		return ModelWebResource.class;
	}

	@Override
	public List<ModelWebResource> findModelWebResource(String url, int modelid) {
		return getResultList(cb ->{
			CriteriaQuery<ModelWebResource> cq = cb.createQuery(ModelWebResource.class);
			Root<ModelWebResource> root = cq.from(ModelWebResource.class); 
			Join<ModelWebResource, InstrumentModel> model = root.join(ModelWebResource_.model);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(ModelWebResource_.url), url));
			clauses.getExpressions().add(cb.equal(model.get(InstrumentModel_.modelid), modelid));
			cq.where(clauses);
			return cq;
		});
	}
}