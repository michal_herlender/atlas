package org.trescal.cwms.core.instrumentmodel.form;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EditModelSalesCategoryForm {
	@NotNull
	private Integer modelid;
	// 0 selected for no sales category
	@NotNull(message = "{error.value.notselected}")
	private Integer salesCategoryId;
}
