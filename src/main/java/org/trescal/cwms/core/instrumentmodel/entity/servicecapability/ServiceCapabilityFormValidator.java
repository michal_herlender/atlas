package org.trescal.cwms.core.instrumentmodel.entity.servicecapability;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.instrumentmodel.dto.ServiceCapabilityForm;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db.ServiceCapabilityService;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;

@Component
public class ServiceCapabilityFormValidator implements Validator {
	

	@Autowired
	public ServiceCapabilityService serviceCapabilityService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(ServiceCapabilityForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ServiceCapabilityForm form = (ServiceCapabilityForm) target;
		
		
		if(form.getProcedureId()==null){
			errors.reject("error.serviceCapability.capabiltiy", "capability can't be Null");
		}
		
		if (form.getId() == 0) {
			// Creating new service
			
			if (serviceCapabilityService.exists(form.getModelId(), CostType.CALIBRATION,
					form.getServiceTypeId(), form.getSubdivId())) {
				errors.reject("error.duplicate.serviceCapability", "Service already exists");
			}
		}
		
		
	}
	
	

}
