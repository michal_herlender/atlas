package org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.ThreadUoM;

@Service
public class ThreadUoMServiceImpl extends BaseServiceImpl<ThreadUoM, Integer> implements ThreadUoMService
{
	@Autowired
	private ThreadUoMDao threadUoMDao;

	@Override
	protected BaseDao<ThreadUoM, Integer> getBaseDao() {
		return threadUoMDao;
	}
}