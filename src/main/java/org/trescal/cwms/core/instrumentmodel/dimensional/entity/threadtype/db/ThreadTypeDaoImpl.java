package org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.ThreadType;

@Repository("ThreadTypeDao")
public class ThreadTypeDaoImpl extends BaseDaoImpl<ThreadType, Integer> implements ThreadTypeDao {
	
	@Override
	protected Class<ThreadType> getEntity() {
		return ThreadType.class;
	}
}