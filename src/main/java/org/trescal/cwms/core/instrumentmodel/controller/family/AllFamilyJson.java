package org.trescal.cwms.core.instrumentmodel.controller.family;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db.InstrumentModelFamilyService;
import org.trescal.cwms.spring.model.InstrumentModelFamilyJsonDTO;

@RestController
public class AllFamilyJson {
	@Autowired
	private MessageSource messages;
	@Autowired
	private InstrumentModelFamilyService instModFamServ;
	
	@RequestMapping(value="/allfamilies.json", method=RequestMethod.GET)
	public List<InstrumentModelFamilyJsonDTO> getAllFamilies(Locale locale) {
	    List<InstrumentModelFamilyJsonDTO> insts = instModFamServ.getAllFamilies(locale);
	    insts.stream().forEach(i -> {
	    	if(i.getActive().equals("Yes"))
	    		i.setActive(messages.getMessage("yes", null, locale));
	    	else
	    		i.setActive(messages.getMessage("no", null, locale));
	    		});
	    	return  insts;
	    }

	    }
