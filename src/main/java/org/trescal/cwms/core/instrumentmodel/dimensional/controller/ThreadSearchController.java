package org.trescal.cwms.core.instrumentmodel.dimensional.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.db.CylindricalStandardService;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.db.ThreadService;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.db.ThreadTypeService;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.db.ThreadUoMService;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.db.WireService;
import org.trescal.cwms.core.instrumentmodel.dimensional.form.ThreadSearchForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;

@Controller
public class ThreadSearchController
{
	@Autowired
	private ThreadService threadServ;
	@Autowired
	private CylindricalStandardService cylStanServ;
	@Autowired
	private WireService wireServ;
	@Autowired
	private ThreadUoMService threadUOMServ;
	@Autowired
	private ThreadTypeService threadTypeServ;

	@ModelAttribute("command")
	protected Object formBackingObject() throws Exception
	{
		return new ThreadSearchForm();
	}
	
	@RequestMapping(value="/threadsearch.htm", method=RequestMethod.POST)
	protected String onSubmit(@ModelAttribute("command") ThreadSearchForm form) throws Exception
	{
		PagedResultSet<Thread> rs = this.threadServ.searchThreads(form, new PagedResultSet<Thread>(form.getResultsPerPage() == 0 ? Constants.RESULTS_PER_PAGE : form.getResultsPerPage(), form.getPageNo() == 0 ? 1 : form.getPageNo()));
		form.setRs(rs);
		form.setThreads(rs.getResults());
		
		return "trescal/core/instrumentmodel/dimensional/threadresults";
	}
	
	@RequestMapping(value="/threadsearch.htm", method=RequestMethod.GET)
	protected String referenceData(Model model) throws Exception
	{
		model.addAttribute("wires", this.wireServ.getAll());
		model.addAttribute("threaduoms", this.threadUOMServ.getAll());
		model.addAttribute("cylindricalstandards", this.cylStanServ.getAll());
		model.addAttribute("threadtypes", this.threadTypeServ.getAll());

		return "trescal/core/instrumentmodel/dimensional/threadsearch";
	}
}
