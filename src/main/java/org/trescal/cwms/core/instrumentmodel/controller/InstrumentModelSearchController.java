package org.trescal.cwms.core.instrumentmodel.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrumentmodel.CanSelect;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.db.InstrumentModelTypeService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.instrumentmodel.form.InstrumentModelSearchForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class InstrumentModelSearchController {
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private NewDescriptionService descriptionService;
	@Autowired
	private InstrumentModelService instModelServ;
	@Autowired
	private MfrService mfrServ;
	@Autowired
	private InstrumentModelTypeService instrumentModelTypeService;

	/**
	 * Builds an {@link ArrayList} of elements that make up the user's current
	 * search.
	 * 
	 * @param isf the {@link InstrumentModelSearchForm}.
	 * @return {@link ArrayList} of labelled search inputs.
	 */
	private ArrayList<String> buildSearchString(InstrumentModelSearchForm isf) {
		ArrayList<String> inputs = new ArrayList<String>();
		if (isf.getDomainId() != null || (isf.getDomainNm() != null && !isf.getDomainNm().isEmpty())) {
			inputs.add("<strong>Domain:</strong> '" + isf.getDomainNm() + "'");
		}
		if (isf.getFamilyId() != null || (isf.getFamilyNm() != null && !isf.getFamilyNm().isEmpty())) {
			inputs.add("<strong>Family:</strong> '" + isf.getFamilyNm() + "'");
		}
		if (isf.getDescId() != null || (isf.getDesc() != null && !isf.getDesc().isEmpty())) {
			inputs.add("<strong>Description:</strong> '" + isf.getDesc() + "'");
		}
		if ((isf.getMfrId() != null) && (isf.getMfrId().intValue() != 0)) {
			Mfr mfr = this.mfrServ.findMfr(isf.getMfrId());
			inputs.add("<strong>Manufacturer:</strong> '" + mfr.getName() + "'");
		} else if ((isf.getMfrtext() != null) && !isf.getMfrtext().trim().equals("")) {
			inputs.add("<strong>Manufacturer:</strong> '" + isf.getMfrtext() + "'");
		}
		if ((isf.getModel() != null) && !isf.getModel().trim().equals("")) {
			inputs.add("<strong>Model:</strong> '" + isf.getModel() + "'");
		}
		if (isf.getSalesCatId() != null || (isf.getSalesCat() != null && !isf.getSalesCat().isEmpty())) {
			inputs.add("<strong>Sales Category:</strong> '" + isf.getSalesCat() + "'");
		}
		return inputs;
	}

	@ModelAttribute("modeltypes")
	private List<InstrumentModelType> fetchModelTypeList() {
		return instrumentModelTypeService.getAllInstrumentModelTypes(CanSelect.ALL);
	}

	@ModelAttribute("command")
	protected InstrumentModelSearchForm formBackingObject(
			@RequestParam(value = "descid", required = false) Integer descid) {
		InstrumentModelSearchForm instsearch = new InstrumentModelSearchForm();
		if (descid != null) {
			// For the search to work correctly (and view to format nicely), both parameters
			// are specified
			Description subfamily = this.descriptionService.findDescription(descid);
			instsearch.setDescId(descid);
			instsearch.setDesc(subfamily.getDescription());
		}
		instsearch.setExcludeQuarantined(true);
		return instsearch;
	}

	@RequestMapping(value = "/searchmodel.htm", method = RequestMethod.GET)
	public String formView() {
		return "trescal/core/instrumentmodel/searchmodelform";
	}

	@RequestMapping(value = "/searchmodel.htm", method = RequestMethod.GET, params = "forced=forced")
	public ModelAndView formSearchDescr(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute("command") InstrumentModelSearchForm isf, Locale userlocale) {
		return onSubmit(subdivDto, isf, userlocale);
	}

	@RequestMapping(value = "/searchmodel.htm", method = RequestMethod.POST)
	public ModelAndView onSubmit(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute("command") InstrumentModelSearchForm isf, Locale userlocale) {
		InstrumentAndModelSearchForm<InstrumentModel> form = new InstrumentAndModelSearchForm<>();
		form.setDescId(isf.getDescId());
		form.setDescNm(isf.getDesc());

		form.setDomainId(isf.getDomainId());
		form.setDomainNm(isf.getDomainNm());
		form.setFamilyId(isf.getFamilyId());
		form.setFamilyNm(isf.getFamilyNm());
		form.setCharLibraryValues(isf.getCharacteristics());
		form.setMfrId(isf.getMfrId());
		form.setMfrNm(isf.getMfrtext());
		form.setModel(isf.getModel());
		form.setSalesCatId(isf.getSalesCatId());
		form.setSalesCat(isf.getSalesCat());
		form.setHireOnlyModels(isf.isHireOnlyModels());
		form.setLocale(userlocale);
		form.setExcludeQuarantined(isf.getExcludeQuarantined());
		form.setMfrType(isf.getMfrType());
		form.setModelMfrType(isf.getModelMfrType());
		form.setSortBy(isf.getSortBy());

		if (isf.getModelTypeId() != null && isf.getModelTypeId() > 0)
			form.setModelTypeIds(new ArrayList<>(Arrays.asList(isf.getModelTypeId())));
		PagedResultSet<InstrumentModel> rs = this.instModelServ
				.searchInstrumentModelsPaged(new PagedResultSet<>(
						isf.getResultsPerPage() == 0 ? Constants.RESULTS_PER_PAGE : isf.getResultsPerPage(),
						isf.getPageNo() == 0 ? 1 : isf.getPageNo()), form);
		isf.setRs(rs);
		isf.setSearchedfor(this.buildSearchString(isf));
		isf.setCalTypes(this.calTypeServ.getActiveCalTypes());
		return new ModelAndView("trescal/core/instrumentmodel/newsearchmodelresults", "command", isf);
	}
}