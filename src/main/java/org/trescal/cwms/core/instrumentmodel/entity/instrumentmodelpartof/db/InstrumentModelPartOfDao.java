package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.InstrumentModelPartOf;

public interface InstrumentModelPartOfDao extends BaseDao<InstrumentModelPartOf, Integer> {
	
	InstrumentModelPartOf findInstrumentModelPartOf(int moduleid, int baseid);
	
	List<InstrumentModelPartOf> getModulesAvailableForBaseUnit(int baseid);
}