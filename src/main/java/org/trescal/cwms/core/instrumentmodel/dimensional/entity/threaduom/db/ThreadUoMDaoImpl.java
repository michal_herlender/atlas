package org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.ThreadUoM;

@Repository("ThreadUoMDao")
public class ThreadUoMDaoImpl extends BaseDaoImpl<ThreadUoM, Integer> implements ThreadUoMDao {
	
	@Override
	protected Class<ThreadUoM> getEntity() {
		return ThreadUoM.class;
	}
}