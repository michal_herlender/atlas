package org.trescal.cwms.core.instrumentmodel.entity.hiremodel.db;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.entity.hirecategory.HireCategory;
import org.trescal.cwms.core.hire.entity.hirecategory.db.HireCategoryService;
import org.trescal.cwms.core.hire.entity.hiremodelcategory.HireModelCategory;
import org.trescal.cwms.core.hire.entity.hiremodelcategory.db.HireModelCategoryService;
import org.trescal.cwms.core.hire.entity.hiremodelincategory.HireModelInCategory;
import org.trescal.cwms.core.instrumentmodel.entity.hiremodel.HireModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Service("HireModelService")
public class HireModelServiceImpl implements HireModelService
{
	@Autowired
	private CompanyService companyService;
	@Autowired
	private HireCategoryService hireCategoryServ;
	@Autowired
	private HireModelCategoryService hireModCatServ;
	@Autowired
	private HireModelDao hireModelDao;
	@Autowired
	private InstrumentModelService modelServ;

	public ResultWrapper addHireModelCategory(int hiremodelid, String plantcode, int categoryid)
	{
		// find hire model
		HireModel hm = this.findHireModel(hiremodelid);
		// found hire model?
		if (hm != null)
		{
			// find chosen category
			HireCategory hc = this.hireCategoryServ.findHireCategory(categoryid);
			// hire category found?
			if (hc != null)
			{
				// pattern to match with plantcode
				String pattern = "[A-Z]*";
				// check plantcode supplied
				if (plantcode.toUpperCase().matches(pattern))
				{
					// create new hire model category
					HireModelCategory hmc = new HireModelCategory();
					// set values
					hmc.setHireCategory(hc);
					hmc.setModelcode(this.hireModCatServ.getNextModelCodeForCategory(categoryid));
					hmc.setPlantcode(plantcode);
					// create hire model in categories
					HireModelInCategory hmic = new HireModelInCategory();
					// set values
					hmic.setHireModel(hm);
					hmic.setHireModelCategory(hmc);
					// create new set of hire model in category
					Set<HireModelInCategory> hmicats = new HashSet<HireModelInCategory>();
					// add our hire model in category
					hmicats.add(hmic);
					// initialise hire model in category
					hmc.setHireModelInCategories(hmicats);
					// save hire model category
					this.hireModCatServ.insertHireModelCategory(hmc);
					// return successful wrapper
					return new ResultWrapper(true, "", hmc, null);
				}
				else
				{
					// return un-successful wrapper
					return new ResultWrapper(false, "The plant code you supplied is invalid", null, null);
				}
			}
			else
			{
				// return un-successful wrapper
				return new ResultWrapper(false, "Hire category could not be found", null, null);
			}
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "Hire model could not be found", null, null);
		}
	}

	public void deleteHireModel(HireModel hm)
	{
		this.hireModelDao.remove(hm);
	}

	public HireModel findHireModel(int id)
	{
		return this.hireModelDao.find(id);
	}

	public List<HireModel> getAllHireModels()
	{
		return this.hireModelDao.findAll();
	}

	@SuppressWarnings("unchecked")
	public ResultWrapper getHireModelPlantCode(int modelid, HttpSession session)
	{
		// find instrument model
		InstrumentModel instmod = this.modelServ.findInstrumentModel(modelid);
		// model found?
		if (instmod != null)
		{
			KeyValue<Integer, String> companyDto = (KeyValue<Integer, String>) session.getAttribute(Constants.SESSION_ATTRIBUTE_COMPANY);
			Company allocatedCompany = companyService.get(companyDto.getKey());
			HireModel hireModel = hireModelDao.get(instmod, allocatedCompany);
			// check for hire model
			if (hireModel != null)
			{
				// check hire model has categories and hence plant code
				if (hireModel.getHireModelInCategories() != null)
				{
					// plant code variable
					String plantcode = "";
					// find plant code
					for (HireModelInCategory hmic : hireModel.getHireModelInCategories())
					{
						// assign plant code to variable
						plantcode = hmic.getHireModelCategory().getPlantcode();
						// check plant code found?
						if (!plantcode.isEmpty())
						{
							break;
						}
					}
					// return successful wrapper
					return new ResultWrapper(true, "", plantcode, null);
				}
				else
				{
					// return un-successful wrapper
					return new ResultWrapper(false, "This hire model has not been added to a valid hire category", null, null);
				}
			}
			else
			{
				// return un-successful wrapper
				return new ResultWrapper(false, "This is not a hire model", null, null);
			}
		}
		else
		{
			// return un-successful wrapper
			return new ResultWrapper(false, "Instrument model could not be found", null, null);
		}
	}

	public void insertHireModel(HireModel hm)
	{
		this.hireModelDao.persist(hm);
	}

	public void saveOrUpdateHireModel(HireModel hm)
	{
		this.hireModelDao.saveOrUpdate(hm);
	}

	public void setHireCategoryServ(HireCategoryService hireCategoryServ)
	{
		this.hireCategoryServ = hireCategoryServ;
	}

	public void setHireModCatServ(HireModelCategoryService hireModCatServ)
	{
		this.hireModCatServ = hireModCatServ;
	}

	public void setHireModelDao(HireModelDao hireModelDao)
	{
		this.hireModelDao = hireModelDao;
	}

	public void setModelServ(InstrumentModelService modelServ)
	{
		this.modelServ = modelServ;
	}

	public void updateHireModel(HireModel hm)
	{
		this.hireModelDao.update(hm);
	}
	
	@Override
	public HireModel get(InstrumentModel instrumentModel, Company allocatedCompany) {
		return hireModelDao.get(instrumentModel, allocatedCompany);
	}
}