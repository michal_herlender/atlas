package org.trescal.cwms.core.instrumentmodel.form;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class EditModelValidator extends AbstractBeanValidator 
{
	@Autowired
	private InstrumentModelService instModelServ;
	@Autowired
	private MfrService mfrServ;

	protected final Log logger = LogFactory.getLog(this.getClass());

	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(EditModelForm.class);
	}
	
	public void validate(Object obj, Errors errors)
	{
		EditModelForm emf = (EditModelForm) obj;
		super.validate(emf, errors);
		
		if (!errors.hasErrors()) {
			Integer mfrId = emf.getMfrid();
			if (ModelMfrType.MFR_GENERIC.equals(emf.getModelMfrType())) {
				Mfr mfr = mfrServ.getSystemGenericMfr();
				if (mfr == null) {
					errors.rejectValue("mfrid", "error.nodefaultmfr", null, "There is no default manufacturer in the system");
				}
				else {
					mfrId = mfr.getMfrid();
				}
			}
			// We check for duplicates / display with reference data; user must view and physically confirm 
			if (emf.getConfirmDuplicate() == null || !emf.getConfirmDuplicate()) {
				List<InstrumentModel> listModels = this.instModelServ.searchModelSignature(mfrId, emf.getModelName(), emf.getDescid(), emf.getInstModelTypeId(), emf.getModelid());  
				if (listModels.size() > 0)
				{
					errors.rejectValue("confirmDuplicate", "instmod.confirmationrequired", "Confirmation required");
				}
			}

			// only validate the mfr name if the model mfr type actually requires a mfr
			if ((emf.getMfrid() == null || emf.getMfrid() == 0)
					&& emf.getModelMfrType().isMfrRequiredForModel())
			{
				errors.rejectValue("mfrid", "error.integermfrid", "A manufacturer must be specified.");
			}
			
			
			if (emf.getModelid() != 0) {
				InstrumentModel instModel = this.instModelServ.get(emf.getModelid());
				
				// Don't allow editing / changes to TML instrument models
				if (instModel.getTmlid() != null && instModel.getTmlid() > 0) {
					errors.reject("error.editmodel.tmlmodel", "TML instrument models may not be edited in Atlas.");
				}
				
				// only allow change of model type among "instrument" model types
				if (emf.getInstModelTypeId() != instModel.getModelType().getInstModelTypeId()) {
					// Changing model type only allowed among instrument model types
					// Logic related to sales categories, etc... for other types prevents changing
					if (!instModel.getModelType().getCanSelect()) {
						errors.rejectValue("instModelTypeId", "error.modeltypechange", "This model type may not be changed once set.");
					}
				}
				
			}
		}
		
	}
}
