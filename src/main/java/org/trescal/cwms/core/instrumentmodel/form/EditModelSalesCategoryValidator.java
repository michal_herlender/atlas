package org.trescal.cwms.core.instrumentmodel.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.db.SalesCategoryService;

@Component
public class EditModelSalesCategoryValidator extends AbstractBeanValidator {

	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private SalesCategoryService salesCategoryService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return EditModelSalesCategoryForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors)
	{
		System.out.println("In validator");
		super.validate(obj, errors);
		if (!errors.hasErrors()) {
			EditModelSalesCategoryForm form = (EditModelSalesCategoryForm) obj;
			InstrumentModel instModel = instrumentModelService.findInstrumentModel(form.getModelid());
			// Instrument models of sales category type can't have a new sales category assigned; they define the sales category.
			if (instModel.getModelType().getSalescategory()) {
				errors.reject("error.editmodel.salescategorymodel", "Sales category models may not have their sales category updated.");
			}
			
			// If selecting a sales category, it must be from the same subfamily as the instrument model.
			if (form.getSalesCategoryId() > 0) {
				SalesCategory salesCategory = this.salesCategoryService.getSalesCategory(form.getSalesCategoryId());
				InstrumentModel salesCategoryInstrumentModel = this.instrumentModelService.findSalesCategoryModel(salesCategory);
				if (salesCategoryInstrumentModel.getDescription().getId() != instModel.getDescription().getId()) {
					errors.reject("error.editmodel.subfamilyconflict", "The updated sales category must be from the same subfamily as the instrument model.");
				}
			}
		}
	}

}
