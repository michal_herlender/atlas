package org.trescal.cwms.core.instrumentmodel.controller.images;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.documents.images.image.ImageType;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.controller.AbstractModelController;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

@Controller @IntranetController
public class ViewModelImagesController extends AbstractModelController {
	
	@RequestMapping(value="/instrumentmodelimages.htm", method=RequestMethod.GET)
	public ModelAndView requestHandler(@ModelAttribute("model") InstrumentModel instrumentModel, Model model) throws Exception
	{
		model.addAttribute("selectedTab", "images");
		model.addAttribute("imageType", ImageType.INSTRUMENTMODEL);
		return new ModelAndView("/trescal/core/instrumentmodel/images/modelviewimages", "command", model);
	}
}