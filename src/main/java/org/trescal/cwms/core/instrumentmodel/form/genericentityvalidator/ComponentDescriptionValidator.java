package org.trescal.cwms.core.instrumentmodel.form.genericentityvalidator;

import org.springframework.validation.Errors;
import org.trescal.cwms.core.instrumentmodel.entity.componentdescription.ComponentDescription;
import org.trescal.cwms.core.instrumentmodel.entity.componentdescription.db.ComponentDescriptionService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

public class ComponentDescriptionValidator extends AbstractBeanValidator
{
	private ComponentDescriptionService compDescServ;

	public void setCompDescServ(ComponentDescriptionService compDescServ)
	{
		this.compDescServ = compDescServ;
	}

	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(ComponentDescription.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		ComponentDescription desc = (ComponentDescription) target;
		super.validate(desc, errors);

		// do the business validation
		if (this.compDescServ.isDuplicateComponentDescription(desc.getDescription(), desc.getId()))
		{
			errors.rejectValue("description", null, "A description with this name already exists");
		}
	}
}
