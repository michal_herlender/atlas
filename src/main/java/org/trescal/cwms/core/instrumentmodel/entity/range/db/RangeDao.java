package org.trescal.cwms.core.instrumentmodel.entity.range.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.entity.range.Range;

public interface RangeDao extends BaseDao<Range, Integer> {}