/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.form.genericentityvalidator;

import org.apache.commons.validator.UrlValidator;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource.MfrWebResource;
import org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource.db.MfrWebResourceService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

/**
 * @author Richard
 */
public class MfrWebResourceValidator extends AbstractBeanValidator
{
	private MfrWebResourceService mfrWebResourceServ;

	public void setMfrWebResourceServ(MfrWebResourceService mfrWebResourceServ)
	{
		this.mfrWebResourceServ = mfrWebResourceServ;
	}

	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(MfrWebResource.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		MfrWebResource res = (MfrWebResource) target;
		super.validate(res, errors);

		UrlValidator urlValidator = new UrlValidator();
		if (!urlValidator.isValid(res.getUrl()))
		{
			errors.rejectValue("url", null, "Invalid url specified, must be of the format http://www.domainname.*");
		}

		// do the business validation
		if (this.mfrWebResourceServ.isDuplicateMfrWebResource(res.getUrl(), res.getMfr().getMfrid()))
		{
			errors.rejectValue("url", null, "This url already exists for this Mfr");
		}
	}
}
