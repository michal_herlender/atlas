package org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.Wire;

@Repository("WireDao")
public class WireDaoImpl extends BaseDaoImpl<Wire, Integer> implements WireDao
{
	@Override
	protected Class<Wire> getEntity() {
		return Wire.class;
	}
}