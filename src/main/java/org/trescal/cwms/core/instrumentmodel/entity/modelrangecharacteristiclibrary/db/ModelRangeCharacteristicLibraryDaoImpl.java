package org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.ModelRangeCharacteristicLibrary;

@Repository
public class ModelRangeCharacteristicLibraryDaoImpl extends BaseDaoImpl<ModelRangeCharacteristicLibrary, Integer>
		implements ModelRangeCharacteristicLibraryDao {
	
	@Override
	protected Class<ModelRangeCharacteristicLibrary> getEntity() {
		return ModelRangeCharacteristicLibrary.class;
	}

}
