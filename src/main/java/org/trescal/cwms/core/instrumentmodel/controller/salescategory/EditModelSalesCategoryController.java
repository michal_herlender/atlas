package org.trescal.cwms.core.instrumentmodel.controller.salescategory;

import java.util.Locale;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.db.EditModelSalesCategoryService;
import org.trescal.cwms.core.instrumentmodel.form.EditModelSalesCategoryForm;
import org.trescal.cwms.core.instrumentmodel.form.EditModelSalesCategoryValidator;
import org.trescal.cwms.core.system.entity.translation.TranslationService;

@Controller @IntranetController
public class EditModelSalesCategoryController {
	@Autowired
	private EditModelSalesCategoryService editModelSalesCategoryService;
	@Autowired
	private InstrumentModelService instModelServ;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private EditModelSalesCategoryValidator validator;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	@ModelAttribute("form")
	protected EditModelSalesCategoryForm formBackingObject(Locale locale,
			@RequestParam(name = "modelid", required = false, defaultValue = "0") Integer modelid)
			throws ServletException {
		EditModelSalesCategoryForm form = new EditModelSalesCategoryForm();
		form.setModelid(modelid);
	
		InstrumentModel model = this.instModelServ.findInstrumentModel(modelid);
		form.setSalesCategoryId(model.getSalesCategory() == null ? 0 : model.getSalesCategory().getId());
		return form;
	}
	
	@RequestMapping(value = "/editmodelsalescategory.htm", method = RequestMethod.POST)
	public String onSubmit(Model model, Locale locale,
			@Validated @ModelAttribute("form") EditModelSalesCategoryForm form, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData(model, locale, form);
		}
		Integer instModelId = form.getModelid();
		this.editModelSalesCategoryService.editModelSalesCategoryService(form);
		
		model.asMap().clear();
		return "redirect:instrumentmodel.htm?modelid=" + instModelId;
	}
	
	@RequestMapping(value = "/editmodelsalescategory.htm", method = RequestMethod.GET)
	public String referenceData(Model model, Locale locale,
			@ModelAttribute("form") EditModelSalesCategoryForm form)  {
				
		InstrumentModel instModel = this.instModelServ.get(form.getModelid());
		String modelTypeTranslation = this.translationService.getCorrectTranslation(instModel.getModelType().getModelTypeNameTranslation(), locale);
		String subFamilyName = translationService.getCorrectTranslation(instModel.getDescription().getTranslations(), locale);
		
		model.addAttribute("instModel", instModel);
		model.addAttribute("subFamilyName", subFamilyName);
		model.addAttribute("quarantined", instModel.getQuarantined());
		
		model.addAttribute("modelType", modelTypeTranslation);
		model.addAttribute("modelName", instModel.getModel()); 
		model.addAttribute("mfrName", instModel.getMfr().getName());
		model.addAttribute("salesCategories", this.editModelSalesCategoryService.getSalesCategoryOptions(instModel, locale));

		return "trescal/core/instrumentmodel/salescategory/editmodelsalescategory";
	}

}
