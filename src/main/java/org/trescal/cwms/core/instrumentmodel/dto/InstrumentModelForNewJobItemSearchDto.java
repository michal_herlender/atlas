package org.trescal.cwms.core.instrumentmodel.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.Map;

@Data
@RequiredArgsConstructor
public class InstrumentModelForNewJobItemSearchDto {


    private final Integer modelId;
    private final String modelName;
    private final String companyName;
    private final Integer companyId;
    private Map<Integer,String> availableQuotations = Collections.emptyMap();
}
