package org.trescal.cwms.core.instrumentmodel.dimensional.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.ThreadType;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.db.ThreadTypeService;

@Controller
public class ThreadTypeController
{
	@Autowired
	private ThreadTypeService threadTypeServ;

	@ModelAttribute("command")
	protected Object formBackingObject(@RequestParam(name="id", required=false, defaultValue="0") Integer id) throws Exception
	{
		ThreadType type = null;

		if (id == 0)
		{
			type = new ThreadType();
		}
		else {
			type = this.threadTypeServ.get(id);
		}
		return type;
	}

	@RequestMapping(value="/threadtype.htm", method=RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/instrumentmodel/dimensional/threadtype";
	}
	@RequestMapping(value="/threadtype.htm", method=RequestMethod.POST)
	protected String onSubmit(@Valid @ModelAttribute("command") ThreadType type, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) return referenceData();
		if (type.getId() == 0) {
			this.threadTypeServ.save(type);
		}
		else {
			this.threadTypeServ.merge(type);
		}
		return "redirect:threadtype.htm?id="+ type.getId();
	}
}
