package org.trescal.cwms.core.instrumentmodel.entity.hiremodel.db;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrumentmodel.entity.hiremodel.HireModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

@Repository("HireModelDao")
public class HireModelDaoImpl extends AllocatedToCompanyDaoImpl<HireModel, Integer> implements HireModelDao
{
	@Override
	protected Class<HireModel> getEntity() {
		return HireModel.class;
	}
	
	@Override
	public HireModel get(InstrumentModel instrumentModel, Company allocatedCompany) {
		Criteria criteria = getSession().createCriteria(HireModel.class);
		criteria.add(Restrictions.eq("instmodel", instrumentModel));
		criteria.add(getCompanyCrit(allocatedCompany));
		return (HireModel) criteria.uniqueResult();
	}
}