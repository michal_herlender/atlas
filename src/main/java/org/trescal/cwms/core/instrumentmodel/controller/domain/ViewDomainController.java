package org.trescal.cwms.core.instrumentmodel.controller.domain;

import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value="instrumentmodeldomain.htm")
public class ViewDomainController {

	@RequestMapping(method = RequestMethod.GET)
	public String displayForm(Locale locale, Model model){
		model.addAttribute("locale", locale.toLanguageTag());
		return "trescal/core/instrumentmodel/domain/viewdomain";
	}	

}
