package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily;


import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.system.entity.translation.Translation;


@Entity
@Table(name = "instmodelfamily")
public class InstrumentModelFamily extends Versioned {
	
	private int familyid;
	
	private String name;
	
	private Set<Translation> translation;
	
	private Boolean active;
	
	private Integer tmlid;
	
	private InstrumentModelDomain domain;	

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="instmodelfamilytranslation", joinColumns=@JoinColumn(name="familyid"))
	public Set<Translation> getTranslation() {
		return translation;
	}
	public void setTranslation(Set<Translation> translations) {
		this.translation = translations;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getFamilyid() {
		return familyid;
	}
	public void setFamilyid(int familyid) {
		this.familyid = familyid;
	}
	
	@Column(nullable=true, unique=true)
	public String getName() {
		return name;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "domainid", nullable = true)
	public InstrumentModelDomain getDomain() {
		return domain;
	}
	
	public void setDomain(InstrumentModelDomain domain) {
		this.domain = domain;
	}

	public void setName(String name) {
		this.name = name;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	@Column(nullable=true)
	public Integer getTmlid() {
		return tmlid;
	}
	public void setTmlid(Integer tmlid) {
		this.tmlid = tmlid;
	}
	
	@Override
	public String toString() {
		return "InstrumentModelFamily [familyid=" + familyid + ", name=" + name
				+ ", translation=" + translation + ", active=" + active
				+ ", tmlid=" + tmlid + "]";
	}
	
	@Transient
	public String toFullString(){
		return "InstrumentModelFamily [familyid=" + familyid + ", name=" + name
				+ ", translation=" + (translation != null ? translation : "") + ", active=" + active
				+ ", tmlid=" + tmlid + "]";
	}
}
