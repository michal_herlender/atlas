package org.trescal.cwms.core.instrumentmodel.dto;

import java.math.BigDecimal;

public class InstrumentModelWithPriceDTO extends InstrumentModelDTO {

	private BigDecimal finalPrice;
	private String reference;

	public InstrumentModelWithPriceDTO() {
		super();
	}

	public InstrumentModelWithPriceDTO(Integer id, String name, BigDecimal finalPrice) {
		super(id, name);
		this.finalPrice = finalPrice;
	}

	public InstrumentModelWithPriceDTO(Integer id, String name, BigDecimal finalPrice, String reference) {
		super(id, name);
		this.finalPrice = finalPrice;
		this.reference = reference;
	}

	public BigDecimal getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(BigDecimal finalPrice) {
		this.finalPrice = finalPrice;
	}

	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}
}