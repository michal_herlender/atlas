package org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;

@Entity
@Table(name = "dimensional_threadtype")
public class ThreadType
{
	private int id;
	private String type;
	private Set<Thread> threads;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "type")
	public Set<Thread> getThreads()
	{
		return this.threads;
	}

	@NotNull
	@Length(min = 1, max = 100)
	@Column(name = "type", nullable = false, length = 100, unique = true)
	public String getType()
	{
		return this.type;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setThreads(Set<Thread> threads)
	{
		this.threads = threads;
	}

	public void setType(String type)
	{
		this.type = type;
	}

}
