package org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource.db;

import java.util.List;

import org.springframework.validation.BindException;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource.MfrWebResource;
import org.trescal.cwms.core.instrumentmodel.form.genericentityvalidator.MfrWebResourceValidator;

public class MfrWebResourceServiceImpl implements MfrWebResourceService
{
	private MfrService mfrServ;
	MfrWebResourceDao MfrWebResourceDao;
	private MfrWebResourceValidator validator;

	@Override
	public ResultWrapper deleteWebResource(Integer resourceId)
	{
		if (resourceId == null)
		{
			return new ResultWrapper(false, "Unable to find resource to delete.", null, null);
		}

		MfrWebResource wr = this.MfrWebResourceDao.find(resourceId);

		if (wr == null)
		{
			return new ResultWrapper(false, "Unable to find resource to delete.", null, null);
		}
		else
		{
			this.MfrWebResourceDao.remove(wr);
			return new ResultWrapper(true, "deleted", wr, null);
		}
	}

	public void deleteMfrWebResource(MfrWebResource mfrwebresource)
	{
		this.MfrWebResourceDao.remove(mfrwebresource);
	}

	public MfrWebResource findMfrWebResource(int id)
	{
		return this.MfrWebResourceDao.find(id);
	}

	public List<MfrWebResource> findMfrWebResource(String url, int mfrid)
	{
		return this.MfrWebResourceDao.findMfrWebResource(url, mfrid);
	}

	public List<MfrWebResource> getAllMfrWebResources()
	{
		return this.MfrWebResourceDao.findAll();
	}

	@Override
	public ResultWrapper insert(int mfrid, String description, String url)
	{
		MfrWebResource res = new MfrWebResource();
		res.setMfr(this.mfrServ.findMfr(mfrid));
		res.setDescription(description);
		res.setUrl(url);

		BindException errors = new BindException(res, "mfrwebres");
		this.validator.validate(res, errors);
		if (!errors.hasErrors())
		{
			this.insertMfrWebResource(res);
		}
		return new ResultWrapper(errors, res);
	}

	public void insertMfrWebResource(MfrWebResource MfrWebResource)
	{
		this.MfrWebResourceDao.persist(MfrWebResource);
	}

	@Override
	public boolean isDuplicateMfrWebResource(String url, int mfrid)
	{
		return this.findMfrWebResource(url, mfrid).size() > 0 ? true : false;
	}

	public void saveOrUpdateMfrWebResource(MfrWebResource mfrwebresource)
	{
		this.MfrWebResourceDao.saveOrUpdate(mfrwebresource);
	}

	public void setMfrServ(MfrService mfrServ)
	{
		this.mfrServ = mfrServ;
	}

	public void setMfrWebResourceDao(MfrWebResourceDao MfrWebResourceDao)
	{
		this.MfrWebResourceDao = MfrWebResourceDao;
	}

	public void setValidator(MfrWebResourceValidator validator)
	{
		this.validator = validator;
	}

	public void updateMfrWebResource(MfrWebResource MfrWebResource)
	{
		this.MfrWebResourceDao.update(MfrWebResource);
	}
}