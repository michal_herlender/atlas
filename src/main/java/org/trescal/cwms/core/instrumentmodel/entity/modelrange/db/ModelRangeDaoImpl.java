package org.trescal.cwms.core.instrumentmodel.entity.modelrange.db;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange_;
import org.trescal.cwms.core.instrumentmodel.entity.option.Option;
import org.trescal.cwms.core.instrumentmodel.entity.option.Option_;

@Repository("ModelRangeDao")
public class ModelRangeDaoImpl extends BaseDaoImpl<ModelRange, Integer> implements ModelRangeDao {
	
	@Override
	protected Class<ModelRange> getEntity() {
		return ModelRange.class;
	}
	
	public void deleteAll(Collection<ModelRange> ranges) {
		ranges.forEach(this::remove);
	}
	
	public ModelRange getByModelIdAndOptionId(int modelId, int optionId) {
		Optional<ModelRange> result = getFirstResult(cb -> {
			CriteriaQuery<ModelRange> cq = cb.createQuery(ModelRange.class);
			Root<ModelRange> root = cq.from(ModelRange.class);
			Join<ModelRange, InstrumentModel> model = root.join(ModelRange_.model);
			Join<ModelRange, Option> option = root.join(ModelRange_.modelOption);
			
			Predicate clauses = cb.conjunction();
			
			clauses.getExpressions().add(cb.equal(model.get(InstrumentModel_.modelid), modelId));
			clauses.getExpressions().add(cb.equal(option.get(Option_.optionid), optionId));
			
			cq.where(clauses);
			return cq;
		});
		return result.orElse(null);
	}
	
	@Override
	public List<ModelRange> loadAll(List<Integer> ids) {
		return getResultList(cb -> {
			CriteriaQuery<ModelRange> cq = cb.createQuery(ModelRange.class);
			Root<ModelRange> root = cq.from(ModelRange.class);
			
			cq.where(root.get(ModelRange_.id).in(ids));
			
			return cq;
		});
	}
}