package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelsubfamily.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelsubfamily.InstrumentModelSubfamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelsubfamily.InstrumentModelSubfamily_;

@Repository("InstrumentModelSubfamilyDao")
public class InstrumentModelSubfamilyDaoImpl extends BaseDaoImpl<InstrumentModelSubfamily, Integer>
		implements InstrumentModelSubfamilyDao {

	@Override
	protected Class<InstrumentModelSubfamily> getEntity() {
		return InstrumentModelSubfamily.class;
	}

	@Override
	public InstrumentModelSubfamily getByTmlId(Integer tmlSubFamilyId) {
		return findByUniqueProperty(InstrumentModelSubfamily_.tmlid, tmlSubFamilyId);
	}

}
