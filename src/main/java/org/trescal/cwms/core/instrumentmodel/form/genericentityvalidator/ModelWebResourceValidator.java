/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.form.genericentityvalidator;

import org.apache.commons.validator.UrlValidator;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.instrumentmodel.entity.modelwebresource.ModelWebResource;
import org.trescal.cwms.core.instrumentmodel.entity.modelwebresource.db.ModelWebResourceService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

/**
 * @author richard
 */
public class ModelWebResourceValidator extends AbstractBeanValidator
{
	private ModelWebResourceService modelWebResourceServ;

	public void setModelWebResourceServ(ModelWebResourceService modelWebResourceServ)
	{
		this.modelWebResourceServ = modelWebResourceServ;
	}

	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(ModelWebResource.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		ModelWebResource res = (ModelWebResource) target;
		super.validate(res, errors);

		UrlValidator urlValidator = new UrlValidator();
		if (!urlValidator.isValid(res.getUrl()))
		{
			errors.rejectValue("url", null, "Invalid url specified, must be of the format http://www.domainname.*");
		}

		// do the business validation
		if (this.modelWebResourceServ.isDuplicateModelWebResource(res.getUrl(), res.getModel().getModelid()))
		{
			errors.rejectValue("url", null, "This url already exists for this Model");
		}
	}
}
