package org.trescal.cwms.core.instrumentmodel.entity.componentdescription.db;

import java.util.List;

import org.springframework.validation.BindException;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.dto.CompDescSearchResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.componentdescription.ComponentDescription;
import org.trescal.cwms.core.instrumentmodel.form.genericentityvalidator.ComponentDescriptionValidator;

public class ComponentDescriptionServiceImpl implements ComponentDescriptionService
{
	ComponentDescriptionDao ComponentDescriptionDao;
	private ComponentDescriptionValidator compDescValidator;

	public void deleteComponentDescription(ComponentDescription componentdescription)
	{
		ComponentDescriptionDao.remove(componentdescription);
	}

	public ComponentDescription findComponentDescription(int id)
	{
		return ComponentDescriptionDao.find(id);
	}

	public List<ComponentDescription> getAllComponentDescriptions()
	{
		return ComponentDescriptionDao.findAll();
	}

	@Override
	public ResultWrapper insert(String description)
	{
		ComponentDescription desc = new ComponentDescription();
		desc.setDescription(description.trim());
		BindException bindException = new BindException(desc, "desc");
		compDescValidator.validate(desc, bindException);
		if (!bindException.hasErrors())
		{
			insertComponentDescription(desc);
		}
		return new ResultWrapper(bindException, desc);
	}

	public void insertComponentDescription(ComponentDescription ComponentDescription)
	{
		ComponentDescriptionDao.persist(ComponentDescription);
	}

	@Override
	public boolean isDuplicateComponentDescription(String nameFragment, Integer descriptionid)
	{
		return ComponentDescriptionDao.findComponentDescription(nameFragment, descriptionid).size() > 0 ? true : false;
	}

	public void saveOrUpdateComponentDescription(ComponentDescription componentdescription)
	{
		ComponentDescriptionDao.saveOrUpdate(componentdescription);
	}

	@Override
	public List<ComponentDescription> searchSortedAllComponentDescriptions(String nameFragment)
	{
		return ComponentDescriptionDao.searchSortedAllComponentDescriptions(nameFragment);
	}

	@Override
	public List<CompDescSearchResultWrapper> searchSortedAllComponentDescriptionsHQL(String nameFragment)
	{
		return ComponentDescriptionDao.searchSortedAllComponentDescriptionsHQL(nameFragment);
	}

	public void setCompDescValidator(ComponentDescriptionValidator compDescValidator)
	{
		this.compDescValidator = compDescValidator;
	}

	public void setComponentDescriptionDao(ComponentDescriptionDao ComponentDescriptionDao)
	{
		this.ComponentDescriptionDao = ComponentDescriptionDao;
	}

	public void updateComponentDescription(ComponentDescription ComponentDescription)
	{
		ComponentDescriptionDao.update(ComponentDescription);
	}
}