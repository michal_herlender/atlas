package org.trescal.cwms.core.instrumentmodel.entity.uom.db;

import java.util.List;

import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.referential.dto.TMLUnitDTO;

public interface UoMService
{
	UoM findUoM(int id);
	void insertUoM(UoM uom);
	void updateUoM(UoM uom);
	List<UoM> getAllUoMs();	
	List<UoM> getAllActiveUoMs();
	void deleteUoM(UoM uom);
	void saveOrUpdateUoM(UoM uom);
	UoM findDefaultUoM();
	UoM findUoMbySymbol(String symbol);
	
	UoM getTMLUoM(int tmlid);

	void insertTMLUoM(TMLUnitDTO dto) throws Exception;

	void updateTMLUoM(TMLUnitDTO dto) throws Exception;

	void deleteTMLUoM(TMLUnitDTO dto) throws Exception;
}