package org.trescal.cwms.core.instrumentmodel.entity.component.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;
import org.trescal.cwms.core.jobs.repair.dto.ComponentJsonDto;

public interface ComponentDao extends BaseDao<Component, Integer> {
	
	List<Component> getComponents(int mfrid, int descid, Integer cid);
	
	List<Component> searchSortedModelComponent(Integer mfrid, Integer descid, Boolean inStock);

	List<ComponentJsonDto> searchComponents(String keyword, int maxResults);
}