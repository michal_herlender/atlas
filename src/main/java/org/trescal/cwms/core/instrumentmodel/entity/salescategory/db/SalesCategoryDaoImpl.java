package org.trescal.cwms.core.instrumentmodel.entity.salescategory.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType_;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.spring.model.KeyValueIntegerString;
import org.trescal.cwms.spring.model.LabelIdDTO;

@Repository
public class SalesCategoryDaoImpl extends BaseDaoImpl<SalesCategory, Integer> implements SalesCategoryDao {
	
	private static String SELECT_QUERY_STRING_LABELVALUE = 
			"select new org.trescal.cwms.spring.model.LabelIdDTO(t.translation, sc.id) "+
			"from SalesCategory sc "+
			"left join sc.translations t "+
			"where t.locale = :locale "+
			"and t.translation like :translation "+
			"and sc.active = 1 "+
			"group by sc.id, t.translation "+
			"order by t.translation";
	
	protected Class<SalesCategory> getEntity() {
		return SalesCategory.class;
	}

	public List<SalesCategory> getAllSalesCategories() {
		return super.findAll();
	}
	
	@Override
	public int getResultsCountExact(String translation, Locale searchLanguage, int excludeId) {
		Criteria criteria = this.getSession()
				.createCriteria(SalesCategory.class)
					.add(Restrictions.ne("id",excludeId))
					.setProjection(Projections.projectionList().add(Projections.rowCount()))
					.createCriteria("translations")
						.add(Restrictions.eq("locale", searchLanguage))
						.add(Restrictions.eq("translation", translation));
		return   ((Long) criteria.uniqueResult()).intValue();
	}

	@Override
	public List<LabelIdDTO> getLabelIdList(String salesCategoryFragment, Locale searchLanguage, int maxResults) {
		TypedQuery<LabelIdDTO> query = getEntityManager().createQuery(SELECT_QUERY_STRING_LABELVALUE, LabelIdDTO.class)
			.setParameter("translation", "%"+salesCategoryFragment+"%")
			.setParameter("locale", searchLanguage);
		query.setFirstResult(0);
		query.setMaxResults(maxResults);
		
		return query.getResultList();
	}
	
	@Override
	public List<Tuple> getSalesCategoryIdsForModelIds(Collection<Integer> modelIds) {
		return getResultList(cb -> {
			CriteriaQuery<Tuple> cq = cb.createQuery(Tuple.class);
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);
			Join<InstrumentModel, SalesCategory> salesCategory = root.join(InstrumentModel_.salesCategory, JoinType.LEFT);
			cq.where(root.get(InstrumentModel_.modelid).in(modelIds));
			cq.multiselect(root.get(InstrumentModel_.modelid), salesCategory.get(SalesCategory_.id));
			return cq;
		});
	}

	@Override
	public String getTranslationForLocale(int id, Locale locale) {
		Criteria criteria = this.getSession()
				.createCriteria(SalesCategory.class)
					.add(Restrictions.eq("id",id))				
					.createCriteria("translations")
						.add(Restrictions.eq("locale", locale))
						.setProjection(Projections.projectionList().add(Projections.property("translation")));
		return   (String) criteria.uniqueResult();
	}
	
	/**
	 * Note, we get the sales categories via instrument model as it is where the subfamily is maintained
	 */
	@Override
	public List<KeyValueIntegerString> getSalesCategoriesForSubfamily(Integer subFamilyId, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class);
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);
			Join<InstrumentModel, InstrumentModelType> modelType = root.join(InstrumentModel_.modelType, JoinType.INNER);
			Join<InstrumentModel, Description> description = root.join(InstrumentModel_.description, JoinType.INNER);
			// Inner joins, as if no sales category set on sales category inst models, won't return null results 
			Join<InstrumentModel, SalesCategory> salesCategory = root.join(InstrumentModel_.salesCategory, JoinType.INNER);
			Join<SalesCategory, Translation> translation = salesCategory.join(SalesCategory_.translations, JoinType.INNER);
			translation.on(cb.equal(translation.get(Translation_.locale), locale));
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(modelType.get(InstrumentModelType_.salescategory)));
			clauses.getExpressions().add(cb.equal(description.get(Description_.id), subFamilyId));
			cq.where(clauses);
			
			cq.select(cb.construct(KeyValueIntegerString.class, salesCategory.get(SalesCategory_.id), translation.get(Translation_.translation)));
			
			return cq;
		});
	}

}
