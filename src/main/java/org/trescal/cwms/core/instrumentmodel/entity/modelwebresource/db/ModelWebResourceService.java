package org.trescal.cwms.core.instrumentmodel.entity.modelwebresource.db;

import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource.MfrWebResource;
import org.trescal.cwms.core.instrumentmodel.entity.modelwebresource.ModelWebResource;

public interface ModelWebResourceService
{
	void deleteModelWebResource(ModelWebResource modelwebresource);

	/**
	 * Delete's the {@link MfrWebResource} identified by the given resourceId.
	 * 
	 * @param resourceId the id of the {@link MfrWebResource} to delete.
	 * @return {@link ResultWrapper} containing results of the operation.
	 */
	ResultWrapper deleteWebResource(Integer resourceId);

	ModelWebResource findModelWebResource(int id);

	/**
	 * Returns all {@link ModelWebResource} that match the given url and belong
	 * to the given {@link InstrumentModel}.
	 * 
	 * @param url the url to match.
	 * @param modelid the id of the {@link InstrumentModel}.
	 * @return list of matching {@link ModelWebResource}.
	 */
	public List<ModelWebResource> findModelWebResource(String url, int modelid);

	List<ModelWebResource> getAllModelWebResources();

	/**
	 * Inserts the given data as a new {@link ModelWebResource}, intended for
	 * use by DWR calls.
	 * 
	 * @param modelid the id of the {@link InstrumentModel}.
	 * @param description the description of the {@link ModelWebResource}.
	 * @param url the url of the {@link ModelWebResource}.
	 * @return {@link ResultWrapper} holding the results of the operation.
	 */
	ResultWrapper insert(int modelid, String description, String url);

	void insertModelWebResource(ModelWebResource modelwebresource);

	/**
	 * Tests if the given url has already been recorded for the
	 * {@link InstrumentModel} identified by the given id.
	 * 
	 * @param url the url to test.
	 * @param modelid the id of the {@link InstrumentModel}, not null.
	 * @return true if the url already exists for the {@link InstrumentModel},.
	 */
	boolean isDuplicateModelWebResource(String url, int modelid);

	void saveOrUpdateModelWebResource(ModelWebResource modelwebresource);

	void updateModelWebResource(ModelWebResource modelwebresource);
}