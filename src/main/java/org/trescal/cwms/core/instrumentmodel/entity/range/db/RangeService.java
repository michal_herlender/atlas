package org.trescal.cwms.core.instrumentmodel.entity.range.db;

import java.util.List;

import org.trescal.cwms.core.instrumentmodel.entity.range.Range;

public interface RangeService
{
	Range findRange(int id);
	void insertRange(Range range);
	void updateRange(Range range);
	List<Range> getAllRanges();
	void deleteRange(Range range);
	void saveOrUpdateRange(Range range);
}