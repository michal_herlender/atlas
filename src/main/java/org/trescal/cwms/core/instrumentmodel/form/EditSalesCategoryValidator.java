package org.trescal.cwms.core.instrumentmodel.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.db.SalesCategoryService;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@Component
public class EditSalesCategoryValidator implements Validator {
	@Autowired
	private SalesCategoryService salesCatService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	
	@Override
	public void validate(Object target, Errors errors) {

		EditSalesCategoryForm form = (EditSalesCategoryForm) target;
		
		//translation for primary language is set.
		for (Translation __t: form.getTranslations()) {
			if (__t.getLocale().equals(supportedLocaleService.getPrimaryLocale()) && (__t.getTranslation() == null || __t.getTranslation().length() == 0)) {
				errors.rejectValue("translations", "editsalescategory.error.primaryLocaleNull", 
						"The primary Locale (" + supportedLocaleService.getPrimaryLocale().getDisplayLanguage() + ") mustn't be empty.");
				break;
			}
		}
		
		//translation is unique for every language.
		for (Translation __t: form.getTranslations()) {
			if (__t.getTranslation().length() > 0 &&
					salesCatService.getResultsCountExact(__t.getTranslation(), __t.getLocale(), form.getSalesCategory().getId()) > 0) {
				errors.rejectValue("translations", "editsalescategory.error.translationNotUnique",
						"Sales Category '" + __t.getTranslation() + "' already defined.");
			}
		}
	}
	
	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

}
