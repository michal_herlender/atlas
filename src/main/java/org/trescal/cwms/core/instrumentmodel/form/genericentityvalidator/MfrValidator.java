/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.form.genericentityvalidator;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

/**
 * Generic validator for validating {@link Mfr} entities. Extends
 * {@link GenericEntityValidator} to provide support for dwr based validation
 * which requires access to the resulting {@link BindException}.
 * 
 * @author Richard
 */
public class MfrValidator extends AbstractBeanValidator
{
	private MfrService mfrServ;

	public void setMfrServ(MfrService mfrServ)
	{
		this.mfrServ = mfrServ;
	}

	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(Mfr.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		Mfr mfr = (Mfr) target;
		super.validate(mfr, errors);

		// do the business validation
		if (this.mfrServ.isDuplicateMfr(mfr.getName(), mfr.getMfrid()))
		{
			errors.rejectValue("name", null, "A manufacturer with this name already exists");
		}
	}
}
