package org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.db;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.RangeType;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.ModelRangeCharacteristicLibrary;
import org.trescal.cwms.core.referential.dto.TMLCharacteristicDTO;
import org.trescal.cwms.spring.model.LabelIdDTO;

@Service
public class CharacteristicDescriptionServiceImpl extends BaseServiceImpl<CharacteristicDescription, Integer>
		implements CharacteristicDescriptionService {

	@Autowired
	CharacteristicDescriptionDao characteristicDescriptionDao;

	@Override
	public List<LabelIdDTO> getLabelIdList(String descriptionFragment, Locale userLocale, int mAX_RESULTS) {
		return characteristicDescriptionDao.getLabelIdList(descriptionFragment, userLocale, mAX_RESULTS);
	}

	@Override
	public CharacteristicDescription getTMLCharacteristicDescription(int tmlid) {
		return this.characteristicDescriptionDao.getTMLCharacteristicDescription(tmlid);
	}

	@Override
	public CharacteristicDescription getBySubfamilyAndInternalName(Integer subfamilyId, String internalName) {
		return this.characteristicDescriptionDao.getByInternalNameAndSubfamilyId(internalName, subfamilyId);
	}
	
	@Override
	public void insertTMLCharacteristicDescription(TMLCharacteristicDTO dto)
			throws Exception {
		this.characteristicDescriptionDao.insertTMLCharacteristicDescription(dto);
	}

	@Override
	public void updateTMLCharacteristicDescription(TMLCharacteristicDTO dto)
			throws Exception {
		this.characteristicDescriptionDao.updateTMLCharacteristicDescription(dto);
	}

	@Override
	public void deleteTMLCharacteristicDescription(TMLCharacteristicDTO dto)
			throws Exception {
		this.characteristicDescriptionDao.deleteTMLCharacteristicDescription(dto);
	}

	@Override
	protected BaseDao<CharacteristicDescription, Integer> getBaseDao() {
		return characteristicDescriptionDao;
	}

	@Override
	public Set<CharacteristicDescription> getUnasignedSubFamilyCharacteristics(InstrumentModel instrumentModel) {
		// Note, may need to add comparator.
		Set<CharacteristicDescription> characteristics = new HashSet<>();
		characteristics.addAll(instrumentModel.getDescription().getCharacteristicDescriptions());
		//Loop through this instrument model's ranges and remove any characteristics that have been initialised.
		for(ModelRange range : instrumentModel.getRanges() ){
			characteristics.remove(range.getCharacteristicDescription());
		}
		for(ModelRangeCharacteristicLibrary library : instrumentModel.getLibraryValues()){
			characteristics.remove(library.getCharacteristicLibrary().getCharacteristicDescription());
		}
		return characteristics;
	}

	@Override
	public List<CharacteristicDescription> getSortedCharacteristics(InstrumentModel instrumentModel) {
		List<CharacteristicDescription> allCharacteristicDescriptions = instrumentModel.getRanges().stream()
				.filter(r -> r.getCharacteristicType().equals(RangeType.CHARACTERISTIC))
				.map(r -> r.getCharacteristicDescription())
				.collect(Collectors.toList());
		allCharacteristicDescriptions.addAll(instrumentModel.getLibraryCharacteristics().keySet());
		List<CharacteristicDescription> sortedCharacteristicDescriptions = allCharacteristicDescriptions.stream()
				.sorted(Comparator.comparing(cd -> cd.getOrderno()))
				.collect(Collectors.toList());
		return sortedCharacteristicDescriptions;
	}
}