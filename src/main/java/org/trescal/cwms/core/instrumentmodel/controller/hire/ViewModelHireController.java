package org.trescal.cwms.core.instrumentmodel.controller.hire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.controller.AbstractModelController;
import org.trescal.cwms.core.instrumentmodel.entity.hiremodel.HireModel;
import org.trescal.cwms.core.instrumentmodel.entity.hiremodel.db.HireModelService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class ViewModelHireController extends AbstractModelController {
	
	@Autowired
	private CompanyService companyService;
	@Autowired
	private HireModelService hireModelService;
	@Autowired
	private InstrumentModelService instrumentModelService;
	
	@ModelAttribute("hireModel")
	public HireModel getHireModel(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute("model") InstrumentModel instrumentModel) {
		Company allocatedCompany = companyService.get(companyDto.getKey());
		HireModel hireModel = hireModelService.get(instrumentModel, allocatedCompany);
		if (hireModel == null) hireModel = new HireModel(instrumentModel, allocatedCompany);
		return hireModel;
	}
	
	@RequestMapping(value="/instrumentmodelhire.htm", method=RequestMethod.POST)
	public ModelAndView onSubmit(
			@ModelAttribute("hireModel") HireModel hireModel,
			@ModelAttribute("model") InstrumentModel instrumentModel, Model model) throws Exception {
		instrumentModel.getHireModels().add(hireModel);
		instrumentModelService.update(instrumentModel);
		return requestHandler(instrumentModel, model);
	}
	
	@RequestMapping(value="/instrumentmodelhire.htm", method=RequestMethod.GET)
	public ModelAndView requestHandler(
			@ModelAttribute("model") InstrumentModel instrumentModel, Model model) throws Exception
	{
		model.addAttribute("selectedTab", "hire");
		return new ModelAndView("/trescal/core/instrumentmodel/hire/modelviewhire", "command", model);
	}
}