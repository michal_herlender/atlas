package org.trescal.cwms.core.instrumentmodel.dimensional.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.ThreadUoM;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.db.ThreadUoMService;

@Controller
public class ThreadUOMController
{
	@Autowired
	private ThreadUoMService threadUOMServ;

	@ModelAttribute("command")
	protected Object formBackingObject(@RequestParam(name="id", required=false, defaultValue="0") Integer id) throws Exception
	{
		ThreadUoM uom = null;
		if (id != 0) {
			uom = this.threadUOMServ.get(id);
		}
		else {
			uom = new ThreadUoM();
		}
		return uom;
	}

	@RequestMapping(value="/threaduom.htm", method=RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/instrumentmodel/dimensional/threaduom";
	}
	@RequestMapping(value="/threaduom.htm", method=RequestMethod.POST)
	protected String onSubmit(@Valid @ModelAttribute("command") ThreadUoM uom, BindingResult errors) throws Exception
	{
		if (errors.hasErrors()) return referenceData();
		if (uom.getId() == 0) {
			this.threadUOMServ.save(uom);
		}
		else {
			this.threadUOMServ.merge(uom);
		}
		return "redirect:threaduom.htm?id="+uom.getId();
	}
}
