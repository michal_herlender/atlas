package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrumentmodel.CanSelect;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;

public interface InstrumentModelTypeService extends BaseService<InstrumentModelType, Integer>
{
	/**
	 * Gets the current default {@link InstrumentModelType}. Returns null if no
	 * default set, returns the first {@link InstrumentModelType} found if more
	 * than one is set as the default.
	 * 
	 * @return {@link InstrumentModelType}.
	 */
	InstrumentModelType findDefaultType();

	List<InstrumentModelType> getAllInstrumentModelTypes(CanSelect canSelect);
}