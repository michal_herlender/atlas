package org.trescal.cwms.core.instrumentmodel.dto;

public class ThreadSearchResultWrapper
{

	private int id;
	private String size;

	public ThreadSearchResultWrapper(int id, String size)
	{
		this.id = id;
		this.size = size;
	}

	public int getId()
	{
		return this.id;
	}

	public String getSize()
	{
		return this.size;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setSize(String size)
	{
		this.size = size;
	}

}
