package org.trescal.cwms.core.instrumentmodel.controller.salescategory;

import java.util.Locale;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller @IntranetController
@RequestMapping(value="salescategory.htm")
public class ViewSalesCategoryController
{
	@RequestMapping(method = RequestMethod.GET)
	public String displayForm(Locale locale, Model model){	
		model.addAttribute("locale", locale.toLanguageTag());
		return "trescal/core/instrumentmodel/salescategory/viewsalescategory";
	}	
}