/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.entity.modelwebresource;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.system.entity.webresource.WebResource;

/**
 * Entity that holds all url resources for an {@link InstrumentModel}.
 * 
 * @author richard
 */
@Entity
@DiscriminatorValue("model")
public class ModelWebResource extends WebResource
{
	private InstrumentModel model;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelid")
	public InstrumentModel getModel()
	{
		return this.model;
	}

	public void setModel(InstrumentModel model)
	{
		this.model = model;
	}
}
