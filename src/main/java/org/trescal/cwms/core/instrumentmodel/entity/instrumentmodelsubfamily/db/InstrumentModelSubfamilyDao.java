package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelsubfamily.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelsubfamily.InstrumentModelSubfamily;

public interface InstrumentModelSubfamilyDao extends BaseDao<InstrumentModelSubfamily, Integer> {

	InstrumentModelSubfamily getByTmlId(Integer tmlSubFamilyId);

}
