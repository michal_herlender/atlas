package org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.CylindricalStandard;

@Repository("CylindricalStandardDao")
public class CylindricalStandardDaoImpl extends BaseDaoImpl<CylindricalStandard, Double> implements CylindricalStandardDao {
	
	@Override
	protected Class<CylindricalStandard> getEntity() {
		return CylindricalStandard.class;
	}
}