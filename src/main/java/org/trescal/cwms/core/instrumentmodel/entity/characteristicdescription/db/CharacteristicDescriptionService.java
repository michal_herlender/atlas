package org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.db;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.referential.dto.TMLCharacteristicDTO;
import org.trescal.cwms.spring.model.LabelIdDTO;

public interface CharacteristicDescriptionService extends BaseService<CharacteristicDescription, Integer>{
	
	Set<CharacteristicDescription> getUnasignedSubFamilyCharacteristics(InstrumentModel instrumentModel);

	List<CharacteristicDescription> getSortedCharacteristics(InstrumentModel instrumentModel);
	
	CharacteristicDescription getBySubfamilyAndInternalName(Integer subfamilyId, String internalName);

	List<LabelIdDTO> getLabelIdList(String descriptionFragment, Locale userLocale, int mAX_RESULTS);
	
	CharacteristicDescription getTMLCharacteristicDescription(int tmlid);

	void insertTMLCharacteristicDescription(TMLCharacteristicDTO dto) throws Exception;

	void updateTMLCharacteristicDescription(TMLCharacteristicDTO dto) throws Exception;

	void deleteTMLCharacteristicDescription(TMLCharacteristicDTO dto) throws Exception;
}
