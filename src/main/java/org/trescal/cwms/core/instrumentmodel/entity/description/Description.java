package org.trescal.cwms.core.instrumentmodel.entity.description;

import org.hibernate.annotations.SortComparator;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModelComparator;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.system.entity.translation.Translation;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "description")
public class Description extends Versioned {
    private String description;
    private int id;
    private Set<InstrumentModel> models;
    private Set<Translation> translations;
    private InstrumentModelFamily family;
    private Integer tmlid;
    private Boolean active = false;
    private Integer typology;
    private Set<CharacteristicDescription> characteristicDescriptions;

    @Transient
    @Deprecated
    /**
     * Formerly tracked in defaultprocid column; unused and removed
     * TODO remove referenced from codebase and test
     */
    public Capability getDefaultProc() {
        return null;
    }

    /*
     * Primary language still maintained in this field, but new functionality should
     * use DescriptionTranslation to get a localized Description
     */
    @NotNull
    @Length(min = 1, max = 100)
    @Column(name = "description", nullable = false, length = 100, columnDefinition = "nvarchar(100)")
    public String getDescription() {
		return this.description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "descriptionid", nullable = false)
	public int getId()
	{
		return this.id;
	}

	@OneToMany(mappedBy = "description", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(InstrumentModelComparator.class)
	public Set<InstrumentModel> getModels()
	{
		return this.models;
	}
	
	@OneToMany(mappedBy = "description", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@OrderBy("orderno ASC")
	public Set<CharacteristicDescription> getCharacteristicDescriptions()
	{
		return this.characteristicDescriptions;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="descriptiontranslation", joinColumns=@JoinColumn(name="descriptionid"))
	public Set<Translation> getTranslations() {
		return translations;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "familyid", nullable = true)
	public InstrumentModelFamily getFamily() {
		return family;
	}

	@Column(name="tmlid", nullable=true)
	public Integer getTmlid() {
		return tmlid;
	}
	public void setTmlid(Integer tmlid) {
		this.tmlid = tmlid;
	}

	@NotNull
	@Column(name="typology", nullable=false)
	public Integer getTypology() {
		return typology;
	}
	public void setTypology(Integer typology) {
		this.typology = typology;
	}
	
	@Transient
	public SubfamilyTypology getSubfamilyTypology() {
		return SubfamilyTypology.convertFromInteger(typology);
	}
	public void setSubfamilyTypology(SubfamilyTypology value) {
		// Setter required for Hibernate
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setModels(Set<InstrumentModel> models)
	{
		this.models = models;
	}

	public void setCharacteristicDescriptions(Set<CharacteristicDescription> characteristicDescriptions)
	{
		this.characteristicDescriptions = characteristicDescriptions;
	}

	public void setTranslations(Set<Translation> translations) {
		this.translations = translations;
	}
	
	public void setFamily(InstrumentModelFamily family) {
		this.family = family;
	}
	
	@NotNull
	@Column(name="active", nullable=false)
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	@Transient
	public String toFullString(){
		return "Description [id=" + id + ", description=" + description
				+ ", translation=" + translations + ", active=" + active
				+ ", tmlid=" + tmlid + "]";
	}
}
