package org.trescal.cwms.core.instrumentmodel.entity.mfr.db;

import static org.trescal.cwms.core.tools.StringJpaUtils.ilike;

import java.text.MessageFormat;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.dto.MfrSearchResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.referential.dto.TMLManufacturerDTO;
import org.trescal.cwms.spring.model.LabelIdDTO;

@Repository("MfrDao")
public class MfrDaoImpl extends BaseDaoImpl<Mfr, Integer> implements MfrDao {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	protected Class<Mfr> getEntity() {
		return Mfr.class;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void addMfrCriteria(Criteria instCrit, Criteria mfrCrit, String mfrName) {
		// get distinct list matching mfrids
		Criteria mfrNameCrit = getSession().createCriteria(Mfr.class)
				.add(Restrictions.ilike("name", mfrName.trim(), MatchMode.ANYWHERE));
		mfrNameCrit.setProjection(Projections
				.distinct(Projections.projectionList().add(Projections.id()).add(Projections.property("mfrid"))));
		// build a comma separated list of mfrids
		String m = "";
		for (Object[] o : (List<Object[]>) mfrNameCrit.list()) {
			if (!m.equals(""))
				m = m + ",";
			m = m + o[0];
		}
		// if no mfrs match the name, add -1 to the list of IDs to stop the
		// criteria from throwing an exception with empty "in ()" clause
		if (m.trim().isEmpty())
			m = "-1";
		// this will find all instruments with a model whose mfr matches
		// the given name
		instCrit.add(Restrictions.sqlRestriction("({alias}.mfrid in (" + m + ") or {alias}.mfrid is null)"));
		// this will find all instruments whose instrument specific mfr
		// matches the given name and whose model type mfr is set to be
		// generic
		mfrCrit.add(Restrictions.sqlRestriction(
				"({alias}.mfrid in (" + m + ") or ({alias}.mfrid not in (" + m + ") and {alias}.genericmfr = 1))"));
	}

	@Override
	public List<Mfr> findMfr(String nameFragment, Integer ignoreMfrId) {
		return getResultList(cb -> {
			CriteriaQuery<Mfr> cq = cb.createQuery(Mfr.class);
			Root<Mfr> root = cq.from(Mfr.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(Mfr_.name), nameFragment));
			if (ignoreMfrId != null)
				clauses.getExpressions().add(cb.notEqual(root.get(Mfr_.mfrid), ignoreMfrId));
			cq.where(clauses);
			return cq;
		});
	}

	@Override
	public List<LabelIdDTO> getLabelIdList(String nameFragment, Boolean searchEverywhere, int maxResults) {
		return getResultList(cb -> {
			CriteriaQuery<LabelIdDTO> cq = cb.createQuery(LabelIdDTO.class);
			Root<Mfr> root = cq.from(Mfr.class);
			cq.where(
					ilike(cb,root.get(Mfr_.name),nameFragment,searchEverywhere?MatchMode.ANYWHERE:MatchMode.START)
			);
			cq.select(cb.construct(LabelIdDTO.class, root.get(Mfr_.name), root.get(Mfr_.mfrid)));
			return cq;
		}, 0, maxResults);
	}

	@Override
	public Mfr findMfrByName(String name) {
		return getFirstResult(cb -> {
			CriteriaQuery<Mfr> cq = cb.createQuery(Mfr.class);
			Root<Mfr> root = cq.from(Mfr.class);
			cq.where(cb.equal(root.get(Mfr_.name), name));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<Mfr> getSortedActiveMfr() {
		return getResultList(cb -> {
			CriteriaQuery<Mfr> cq = cb.createQuery(Mfr.class);
			Root<Mfr> root = cq.from(Mfr.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(root.get(Mfr_.active)));
			clauses.getExpressions().add(cb.isFalse(root.get(Mfr_.genericMfr)));
			cq.where(clauses);
			cq.orderBy(cb.asc(root.get(Mfr_.name)));
			return cq;
		});
	}

	@Override
	public Mfr getSystemGenericMfr() {
		return getFirstResult(cb -> {
			CriteriaQuery<Mfr> cq = cb.createQuery(Mfr.class);
			Root<Mfr> root = cq.from(Mfr.class);
			cq.where(cb.equal(root.get(Mfr_.genericMfr), true));
			cq.orderBy(cb.asc(root.get(Mfr_.mfrid)));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<MfrSearchResultWrapper> searchSortedActiveMfrHQL(String searchName) {
		return getResultList(cb -> {
			CriteriaQuery<MfrSearchResultWrapper> cq = cb.createQuery(MfrSearchResultWrapper.class);
			Root<Mfr> root = cq.from(Mfr.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.like(root.get(Mfr_.name), searchName + "%"));
			clauses.getExpressions().add(cb.isTrue(root.get(Mfr_.active)));
			clauses.getExpressions().add(cb.isFalse(root.get(Mfr_.genericMfr)));
			cq.where(clauses);
			cq.orderBy(cb.asc(root.get(Mfr_.name)));
			cq.select(cb.construct(MfrSearchResultWrapper.class, root.get(Mfr_.mfrid), root.get(Mfr_.name)));
			return cq;
		});
	}

	@Override
	public List<Mfr> searchSortedMfr(String nameFragment, Boolean active) {
		return getResultList(cb -> {
			CriteriaQuery<Mfr> cq = cb.createQuery(Mfr.class);
			Root<Mfr> root = cq.from(Mfr.class);
			Predicate clauses = cb.conjunction();
			if (active != null)
				clauses.getExpressions().add(cb.equal(root.get(Mfr_.active), active));
			clauses.getExpressions().add(cb.notEqual(root.get(Mfr_.genericMfr), true));
			clauses.getExpressions().add(cb.like(root.get(Mfr_.name), nameFragment + "%"));
			cq.where(clauses);
			cq.orderBy(cb.asc(root.get(Mfr_.name)));

			return cq;
		});
	}

	@Override
	public Mfr findTMLMfr(int tmlid) {
		return getFirstResult(cb -> {
			CriteriaQuery<Mfr> cq = cb.createQuery(Mfr.class);
			Root<Mfr> root = cq.from(Mfr.class);
			cq.where(cb.equal(root.get(Mfr_.tmlid), tmlid));
			return cq;
		}).orElse(null);
	}

	@Override
	public void insertTMLMfr(TMLManufacturerDTO dto) throws Exception {
		// First try to find if a domain already exists with the same name and not
		// linked to TML
		Mfr existingMfr = getFirstResult(cb -> {
			CriteriaQuery<Mfr> cq = cb.createQuery(Mfr.class);
			Root<Mfr> root = cq.from(Mfr.class);
			cq.where(cb.equal(root.get(Mfr_.name), dto.getName()));
			return cq;
		}).orElse(null);
		
		if (existingMfr == null) {
			Mfr mfr = new Mfr();
			mfr.setTmlid(dto.getTMLID());
			mfr.setName(dto.getName());
			mfr.setActive(dto.isIsEnabled());

			logger.debug("Inserting: " + mfr.toFullString());
			this.persist(mfr);
			// Add this to avoid locks
			logger.debug("Inserted: " + mfr.toFullString());
		} else {
			if (existingMfr.getTmlid() != null && !existingMfr.getTmlid().equals(dto.getTMLID())) {
				String errorMessage = MessageFormat.format(
						"Error while inserting, the manufacturer {0} already exists with a different TMLID.",
						dto.getName());
				logger.error(errorMessage);
				throw new Exception(errorMessage);
			} else {
				if (existingMfr.getTmlid() == null) {
					existingMfr.setTmlid(dto.getTMLID());
					this.merge(existingMfr);
					// Add this to avoid locks
				}

				if (dto.isIsEnabled()) {
					this.updateTMLMfr(dto);
				} else {
					this.deleteTMLMfr(dto);
				}
			}
		}
	}

	@Override
	public void updateTMLMfr(TMLManufacturerDTO dto) throws Exception {
		Mfr mfr = this.findTMLMfr(dto.getTMLID());
		if (mfr != null) {
			// First try to find if a domain already exists with the same name and not
			// linked to TML
			Mfr existingMfr = getFirstResult(cb -> {
				CriteriaQuery<Mfr> cq = cb.createQuery(Mfr.class);
				Root<Mfr> root = cq.from(Mfr.class);
				cq.where(cb.and(cb.equal(root.get(Mfr_.name), dto.getName()),
						        cb.notEqual(root.get(Mfr_.mfrid), mfr.getMfrid())));
				return cq;
			}).orElse(null);

			if (existingMfr == null) {
				mfr.setTmlid(dto.getTMLID());
				mfr.setName(dto.getName());
				mfr.setActive(dto.isIsEnabled());

				logger.debug("Updating: " + mfr.toFullString());
				this.merge(mfr);
				// Add this to avoid locks
				logger.debug("Updated: " + mfr.toFullString());
			} else {
				String errorMessage = MessageFormat.format(
						"Error while updating, the manufacturer {0} already exists with a different TMLID.",
						dto.getName());
				logger.error(errorMessage);
				throw new Exception(errorMessage);
			}
		}
		else {
			String errorMessage = MessageFormat.format(
					"Error while updating, the manufacturer with TML ID {0} doesn't exists.",
					dto.getTMLID());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		}
	}

	@Override
	public void deleteTMLMfr(TMLManufacturerDTO dto) throws Exception {
		Mfr mfr = this.findTMLMfr(dto.getTMLID());

		if (mfr != null) {
			// Check if this domain is already used by active instrument
			Integer count = getCount(cb -> cq -> {
				Root<Instrument> root = cq.from(Instrument.class);
				Join<Instrument, InstrumentModel> instModelJoin = root.join(Instrument_.model);
				Join<InstrumentModel, Mfr> mfrJoin = instModelJoin.join(InstrumentModel_.mfr);

				cq.where(cb.equal(mfrJoin.get(Mfr_.tmlid), dto.getTMLID()));
				return Triple.of(root.get(Instrument_.plantid), null, null);
			});

			if (count <= 0) {
				mfr.setTmlid(dto.getTMLID());
				mfr.setName(dto.getName());
				mfr.setActive(dto.isIsEnabled());

				logger.debug("Deleting: " + mfr.toFullString());
				this.merge(mfr);
				// Add this to avoid locks
				logger.debug("Deleting: " + mfr.toFullString());
			} else {
				String errorMessage = MessageFormat.format(
						"Error while deleting, the manufacturer {0} is used by at least 1 active instrument.",
						dto.getName());
				logger.error(errorMessage);
				throw new Exception(errorMessage);
			}
		}
	}
}