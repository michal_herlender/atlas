package org.trescal.cwms.core.instrumentmodel.dto;

public class CompDescSearchResultWrapper
{
	private int id;
	private String description;

	public CompDescSearchResultWrapper(int id, String description)
	{
		this.id = id;
		this.description = description;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

}