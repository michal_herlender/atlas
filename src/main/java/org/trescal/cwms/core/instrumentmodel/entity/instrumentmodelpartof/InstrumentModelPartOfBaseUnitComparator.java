package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof;

import java.util.Comparator;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModelComparator;

/**
 * Comparator used to sort {@link InstrumentModelPartOf} base unit entities.
 * 
 * @author richard
 */
public class InstrumentModelPartOfBaseUnitComparator implements Comparator<InstrumentModelPartOf>
{
	@Override
	public int compare(InstrumentModelPartOf o1, InstrumentModelPartOf o2)
	{
		if (o1.getBase() == null)
		{
			return 1;
		}
		else if (o2.getBase() == null)
		{
			return 0;
		}
		else
		{
			InstrumentModelComparator c = new InstrumentModelComparator();
			int i = c.compare(o1.getBase(), o2.getBase());

			// always return one ahead of the other
			if (i == 0)
			{
				return 1;
			}
			else
			{
				return i;
			}
		}
	}
}
