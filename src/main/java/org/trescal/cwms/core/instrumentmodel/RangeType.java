package org.trescal.cwms.core.instrumentmodel;

public enum RangeType {
	CHARACTERISTIC, OPTION;
}	
