package org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.db;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription_;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary_;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.ModelRangeCharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.ModelRangeCharacteristicLibrary_;
import org.trescal.cwms.core.referential.dto.TMLChoiceDTO;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Repository("CharacteristicLibraryDao")
public class CharacteristicLibraryDaoImpl extends BaseDaoImpl<CharacteristicLibrary, Integer>
		implements CharacteristicLibraryDao {

	private static final Logger logger = LoggerFactory.getLogger(CharacteristicLibraryDaoImpl.class);

	@Override
	protected Class<CharacteristicLibrary> getEntity() {
		return CharacteristicLibrary.class;
	}

	@Override
	public CharacteristicLibrary getByCharacteristicDescriptionIdAndTmlId(Integer characteristicDescriptionId,
			Integer tmlId) {
		if (characteristicDescriptionId == null)
			throw new IllegalArgumentException("characteristicDescriptionId may not be null");
		if (tmlId == null)
			throw new IllegalArgumentException("tmlId may not be null");

		Optional<CharacteristicLibrary> result = getFirstResult(cb -> {
			CriteriaQuery<CharacteristicLibrary> cq = cb.createQuery(CharacteristicLibrary.class);
			Root<CharacteristicLibrary> root = cq.from(CharacteristicLibrary.class);
			Join<CharacteristicLibrary, CharacteristicDescription> description = root
					.join(CharacteristicLibrary_.characteristicDescription, JoinType.INNER);

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(CharacteristicLibrary_.tmlid), tmlId));
			clauses.getExpressions()
					.add(cb.equal(description.get(CharacteristicDescription_.characteristicDescriptionId),
							characteristicDescriptionId));

			cq.where(clauses);

			return cq;
		});
		return result.orElse(null);

	}

	@Override
	public ResultWrapper getLibrariesByCharacteristicDescription(int id) {
		List<CharacteristicLibrary> result = getResultList(cb -> {
			CriteriaQuery<CharacteristicLibrary> cq = cb.createQuery(CharacteristicLibrary.class);
			Root<CharacteristicLibrary> root = cq.from(CharacteristicLibrary.class);
			Join<CharacteristicLibrary, CharacteristicDescription> caractDescJoin = root
					.join(CharacteristicLibrary_.characteristicDescription.getName());

			cq.where(
					cb.equal(caractDescJoin.get(CharacteristicDescription_.characteristicDescriptionId.getName()), id));
			cq.distinct(true);

			return cq;
		});
		try {
			return new ResultWrapper(true, "", result, null);
		} catch (Exception ex) {
			System.out.print(ex.getMessage());
			return new ResultWrapper(false, ex.getMessage(), null, null);
		}
	}

	@Override
	public ResultWrapper getLibrariesByModelRange(int id) {
		List<CharacteristicLibrary> result = getResultList(cb -> {
			CriteriaQuery<CharacteristicLibrary> cq = cb.createQuery(CharacteristicLibrary.class);
			Root<CharacteristicLibrary> root = cq.from(CharacteristicLibrary.class);
			Join<CharacteristicLibrary, ModelRangeCharacteristicLibrary> modelRangeJoin = root
					.join(CharacteristicLibrary_.modelRangeCharacteristicLibraries.getName());

			cq.where(cb.equal(
					modelRangeJoin.get(ModelRangeCharacteristicLibrary_.modelrangecharacteristiclibraryid.getName()),
					id));
			cq.distinct(true);

			return cq;
		});
		try {
			return new ResultWrapper(true, "", result, null);
		} catch (Exception ex) {
			System.out.print(ex.getMessage());
			return new ResultWrapper(false, ex.getMessage(), null, null);
		}
	}

	@Override
	public void updateTMLLibraries(int characteristicDescriptionId, Set<TMLChoiceDTO> dto) {
		if (dto != null && dto.size() > 0) {
			for (TMLChoiceDTO tmlChoiceDTO : dto) {
				CharacteristicLibrary characteristicLibrary = getFirstResult(cb -> {
					CriteriaQuery<CharacteristicLibrary> cq = cb.createQuery(CharacteristicLibrary.class);
					Root<CharacteristicLibrary> root = cq.from(CharacteristicLibrary.class);
					Join<CharacteristicLibrary, CharacteristicDescription> caractDescJoin = root
							.join(CharacteristicLibrary_.characteristicDescription.getName());

					cq.where(cb.and(
							cb.equal(caractDescJoin.get(CharacteristicLibrary_.tmlid.getName()),
									tmlChoiceDTO.getKey()),
							cb.equal(
									caractDescJoin
											.get(CharacteristicDescription_.characteristicDescriptionId.getName()),
									characteristicDescriptionId)));

					return cq;
				}).orElse(null);

				if (characteristicLibrary != null) {
					characteristicLibrary.setName(tmlChoiceDTO.getName());
					SortedSet<Translation> newTranslations = new TreeSet<Translation>();
					for (String lang : tmlChoiceDTO.getValues().keySet()) {
						String value = tmlChoiceDTO.getValues().get(lang);
						Locale locale = new Locale(lang, lang != null && lang.equals("en") ? "GB" : lang);
						Translation t = new Translation(locale, !value.equals(null) ? value.toString() : "");
						newTranslations.add(t);
					}
					characteristicLibrary.setTranslation(newTranslations);
					logger.debug("Updating: " + characteristicLibrary.toFullString());
					// getSession().update(characteristicLibrary);
					this.merge(characteristicLibrary);
					logger.debug("Updated: " + characteristicLibrary.toFullString());
				} else {
					CharacteristicDescription characteristicDescription = getFirstResult(cb -> {
						CriteriaQuery<CharacteristicDescription> cq = cb.createQuery(CharacteristicDescription.class);
						Root<CharacteristicDescription> root = cq.from(CharacteristicDescription.class);

						cq.where(cb.equal(root.get(CharacteristicDescription_.characteristicDescriptionId.getName()),
								characteristicDescriptionId));

						return cq;
					}).orElse(null);
					characteristicLibrary = new CharacteristicLibrary();
					characteristicLibrary.setCharacteristicDescription(characteristicDescription);
					characteristicLibrary.setTmlid(tmlChoiceDTO.getKey());
					characteristicLibrary.setActive(true);
					characteristicLibrary.setName(tmlChoiceDTO.getName());
					characteristicLibrary.setLog_createdon(new Date());
					SortedSet<Translation> newTranslations = new TreeSet<Translation>();
					for (String lang : tmlChoiceDTO.getValues().keySet()) {
						String value = tmlChoiceDTO.getValues().get(lang);
						Locale locale = new Locale(lang, lang != null && lang.equals("en") ? "GB" : lang);
						Translation t = new Translation(locale, !value.equals(null) ? value.toString() : "");
						newTranslations.add(t);
					}
					characteristicLibrary.setTranslation(newTranslations);
					logger.debug("Inserting: " + characteristicLibrary.toFullString());
					// getSession().save(characteristicLibrary);
					this.merge(characteristicLibrary);
					// Add this to avoid locks
					// getSession().clear();
					logger.debug("Inserted: " + characteristicLibrary.toFullString());
				}
			}
		}
	}
}