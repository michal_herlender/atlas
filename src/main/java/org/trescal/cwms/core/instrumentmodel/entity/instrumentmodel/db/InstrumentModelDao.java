package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db;

import org.hibernate.HibernateException;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.dto.*;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchByLinkedQuotationForm;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchInstrumentForm;
import org.trescal.cwms.core.referential.dto.TMLInstrumentModelDTO;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tools.PagedResultSet;

import javax.persistence.Tuple;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public interface InstrumentModelDao extends BaseDao<InstrumentModel, Integer> {
	
	List<InstrumentModel> findInstrumentModel(SalesCategory salesCategory);
	
	InstrumentModel findCapabilityModel(Description description) throws HibernateException;
	
	InstrumentModel findSalesCategoryModel(SalesCategory salesCategory) throws HibernateException;
	
	List<InstrumentModel> findInstrumentModel(Description description);

	List<InstrumentModelWithPriceDTO> getAll(DomainType domainType, Locale locale, ServiceType serviceType,
			Company allocatedCompany);
	
	InstrumentModelPlantillasDTO getPlantillasModel(Locale locale, int modelid);
	
	PagedResultSet<InstrumentModelPlantillasDTO> getPlantillasModels(Locale locale, Date afterLastModified,
			int resultsPerPage, int currentPage);

	List<Integer> getDistinctModelidListForQuote(int quoteId);

	List<Tuple> getModelIdsForInstruments(Collection<Integer> instrumentIds);

	List<InstrumentModel> getModelsByIds(Collection<Integer> modelids);

	List<InstrumentModel> getModelsFromIds(List<Integer> modelIds);

	PagedResultSet<InstrumentModel> searchInstrumentModelsPaged(PagedResultSet<InstrumentModel> prs,
			InstrumentAndModelSearchForm<InstrumentModel> form);
	
	List<InstrumentModel> searchModelSignature(Integer mfrId, String model, Integer descriptionId, Integer modelTypeId,
			Integer excludeModelId);
	
	InstrumentModel getByTmlId(Integer tmlModelId);
	
	Long getCount();
	
	List<InstrumentModel> getPaged(int firstResult, int maxResults, boolean fetchTranslations);
	
	@Deprecated
	InstrumentModel getTMLInstrumentModel(int tmlid);

	@Deprecated
	InstrumentModel insertTMLInstrumentModel(TMLInstrumentModelDTO dto);

	@Deprecated
	InstrumentModel updateTMLInstrumentModel(TMLInstrumentModelDTO dto);

    List<InstrumentModel> getOtherModelsWithMatchingSalesCategory(InstrumentModel instrumentModel);

	List<InstrumentModel> getStandAloneModelsMatchingSalesCategoryAndModelName(SalesCategory salesCategoryToSearch,
			String modelNameToFind, Locale locale);

	/**
	 * @param coid            : client company
	 * @param mfr             : manufacturer [filter]
	 * @param model           : [filter]
	 * @param desc            : sub-family [filter]
	 * @param onlyWithCalReqs : returns only instrument models that already have a
	 *                        cal req
	 * @return searched/filtered instrument models and also any linked calreqs by
	 *         the client company
	 */
	PagedResultSet<InstrumentModelCompanyCalReqDTO> getInstrumentModelsWithCompanyCalRequirements(int coid, String mfr,
			String model, String desc, Integer resultsPerPage, Integer currentPage, Locale locale,
			boolean onlyWithCalReqs);

    PagedResultSet<InstrumentModelForNewJobItemSearchDto> findCompanyOrGroupInstrumentModelForNewJobItemSearch(NewJobItemSearchInstrumentForm form);

	PagedResultSet<InstrumentModelForNewJobItemSearchDto> findAllMatchingInstrumentModelForNewJobItemSearch(NewJobItemSearchInstrumentForm form);

    PagedResultSet<InstrumentModelForNewJobItemSearchDto> findModelsByIdsForNewJobItemSearch(NewJobItemSearchByLinkedQuotationForm form, List<Integer> modelIds);

	PagedResultSet<InstrumentModelForNewJobItemSearchDto> findCompanyModelsByIdsForNewJobIntemSearch(NewJobItemSearchByLinkedQuotationForm form, List<Integer> modelIds);
}