package org.trescal.cwms.core.instrumentmodel.dimensional.form;

import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;

import lombok.Data;

@Data
public class ThreadForm
{
	private Thread thread;
	private boolean create;

	private Integer wireId;
	private Integer typeId;
	private Double cylindricalStandardId;
	private Integer uomId;

}
