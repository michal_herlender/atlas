package org.trescal.cwms.core.instrumentmodel.form;

import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

@Setter
public class EditComponentForm
{
	private Integer componentId;
	private Integer descid;
	private String descName;    // Used for pre-fill
	private Integer mfrid;
	private String mfrName;        // Used for pre-fill

	private Integer inStock;
	private Integer minimumStockLevel;
	private BigDecimal salesCost;

	private String stockLastCheckedBy;
	private LocalDate stockLastCheckedOn;

	public static final String ERROR_CODE_VALUE_NOT_SELECTED = "{error.value.notselected}";

	@NotNull
	public Integer getComponentId() {
		return componentId;
	}

	// Mandatory
	@NotNull
	@Min(value = 1, message = ERROR_CODE_VALUE_NOT_SELECTED)
	public Integer getDescid()
	{
		return this.descid;
	}

	// 0 = no manufacturer
	@NotNull
	public Integer getMfrid()
	{
		return this.mfrid;
	}

	@Min(0)	// Optional
	public Integer getInStock()
	{
		return this.inStock;
	}

	@Min(0)	// Optional
	public Integer getMinimumStockLevel() {
		return minimumStockLevel;
	}

	// Optional
	public BigDecimal getSalesCost() {
		return salesCost;
	}

	public String getDescName() {
		return descName;
	}

	public String getMfrName() {
		return mfrName;
	}

	public String getStockLastCheckedBy() {
		return stockLastCheckedBy;
	}

	public LocalDate getStockLastCheckedOn() {
		return stockLastCheckedOn;
	}


}
