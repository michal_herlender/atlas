package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof;

import java.util.Comparator;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModelComparator;

/**
 * Comparator used to sort {@link InstrumentModelPartOf} module entities.
 * 
 * @author richard
 */
public class InstrumentModelPartOfModuleComparator implements Comparator<InstrumentModelPartOf>
{
	@Override
	public int compare(InstrumentModelPartOf o1, InstrumentModelPartOf o2)
	{
		if (o1.getModule() == null)
		{
			return 1;
		}
		else if (o2.getModule() == null)
		{
			return 0;
		}
		else
		{
			InstrumentModelComparator c = new InstrumentModelComparator();
			int i = c.compare(o1.getModule(), o2.getModule());

			// always return one ahead of the other
			if (i == 0)
			{
				return 1;
			}
			else
			{
				return i;
			}
		}
	}
}
