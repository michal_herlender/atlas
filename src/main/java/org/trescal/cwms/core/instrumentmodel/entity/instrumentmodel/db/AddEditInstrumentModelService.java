package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db;

import org.trescal.cwms.core.instrumentmodel.controller.configuration.EditModelCharacteristicForm;
import org.trescal.cwms.core.instrumentmodel.form.EditModelForm;

public interface AddEditInstrumentModelService {
	Integer addEditInstrumentModel(EditModelForm form, String username);
	void editCharacteristics(EditModelCharacteristicForm form);
}
