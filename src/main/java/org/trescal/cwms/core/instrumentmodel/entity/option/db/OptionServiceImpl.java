package org.trescal.cwms.core.instrumentmodel.entity.option.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.entity.option.Option;
import org.trescal.cwms.core.referential.dto.TMLOptionDTO;

@Service
public class OptionServiceImpl extends BaseServiceImpl<Option, Integer> implements OptionService {
	@Autowired
	OptionDao optionDao;
	
	public Option getTMLOption(int tmlModelid,String code)
	{
		return this.optionDao.getTMLOption(tmlModelid, code);
	}	
	public Option getTMLOption(int tmlid)
	{
		return this.optionDao.getTMLOption(tmlid);
	}
	public void insertTMLOption(TMLOptionDTO dto) throws Exception
	{
		this.optionDao.insertTMLOption(dto);
	}
	public void updateTMLOption(TMLOptionDTO dto) throws Exception
	{
		this.optionDao.updateTMLOption(dto);
	}
	public void deleteTMLOption(TMLOptionDTO dto) throws Exception
	{
		this.optionDao.deleteTMLOption(dto);
	}
	@Override
	protected BaseDao<Option, Integer> getBaseDao() {
		return this.optionDao;
	}
}
