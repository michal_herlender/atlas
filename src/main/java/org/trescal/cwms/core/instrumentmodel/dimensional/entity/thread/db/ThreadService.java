package org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.db;

import java.util.List;

import org.hibernate.criterion.MatchMode;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;
import org.trescal.cwms.core.instrumentmodel.dimensional.form.ThreadSearchForm;
import org.trescal.cwms.core.instrumentmodel.dto.ThreadSearchResultWrapper;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface ThreadService extends BaseService<Thread, Integer>
{
	/**
	 * Returns all {@link Thread}s of the given type and size. If "all" is
	 * passed as a type argument then threads of all types are returned. Size
	 * matches on the {@link MatchMode}.START of the size field.
	 * 
	 * @param type the type, "all" to find all.
	 * @param size the size of the {@link Thread}.
	 * @return list of matching {@link Thread}.
	 */
	List<Thread> findAllThreadsBySizeAndType(String type, String size);

	/**
	 * Returns all {@link Thread}s of the given type. If "all" is passed as an
	 * argument then threads of all types are returned.
	 * 
	 * @param type the type, "all" to find all.
	 * @return list of matching {@link Thread}.
	 */
	List<Thread> findAllThreadsByType(String type);

	PagedResultSet<Thread> searchThreads(ThreadSearchForm form, PagedResultSet<Thread> prs);

	List<ThreadSearchResultWrapper> searchThreadsHQL(String searchTerm, Integer typeId);
}