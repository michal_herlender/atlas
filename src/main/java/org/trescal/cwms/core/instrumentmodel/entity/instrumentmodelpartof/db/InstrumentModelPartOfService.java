package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.db;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.InstrumentModelPartOf;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;

public interface InstrumentModelPartOfService
{
	/**
	 * Associates the given {@link InstrumentModel} module as a module of the
	 * given {@link InstrumentModel} base unit. Also sets if this relationship
	 * is enforced by default. Method implements validation that the both
	 * {@link InstrumentModel}s exist, each is of the correct
	 * {@link InstrumentModelType} and that this relationship does not already
	 * exist.
	 * 
	 * @param modeluleid the id of the {@link InstrumentModel} module.
	 * @param baseid the id of the {@link InstrumentModel} base unit.
	 * @return {@link ResultWrapper} indicating the outcome of this operation.
	 */
	ResultWrapper createPartOfRelationShip(int modeluleid, int baseid, boolean addAsDefault, HttpSession session);

	void deleteInstrumentModelPartOf(InstrumentModelPartOf instrumentmodelpartof);

	/**
	 * Deletes the {@link InstrumentModelPartOf} entity identified by the given
	 * id.
	 * 
	 * @param partOfId the id of the {@link InstrumentModelPartOf}.
	 * @return {@link ResultWrapper} indicating the success of the operation.
	 */
	ResultWrapper deletePartOfRelationShip(int partOfId, HttpSession session);

	InstrumentModelPartOf findInstrumentModelPartOf(int id);

	/**
	 * Returns the {@link InstrumentModelPartOf} entity that matches the given
	 * module and base unit.
	 * 
	 * @param moduleid the id of the {@link InstrumentModel} module.
	 * @param baseid the id of the {@link InstrumentModel} base unit.
	 * @return the {@link InstrumentModelPartOf} if found, null if not.
	 */
	InstrumentModelPartOf findInstrumentModelPartOf(int moduleid, int baseid);

	List<InstrumentModelPartOf> getAllInstrumentModelPartOfs();

	List<InstrumentModelPartOf> getModulesAvailableForBaseUnit(int baseid);

	void insertInstrumentModelPartOf(InstrumentModelPartOf instrumentmodelpartof);

	void saveOrUpdateInstrumentModelPartOf(InstrumentModelPartOf instrumentmodelpartof);

	/**
	 * Updates the value of the includeByDefault property of the
	 * {@link InstrumentModelPartOf} identified by the given id.
	 * 
	 * @param id the id of the {@link InstrumentModelPartOf}.
	 * @param includeByDefault the new value of the includeByDefault property.
	 * @return {@link ResultWrapper} containing the outcome of the operation.
	 */
	ResultWrapper updateIncludeByDefault(int partOfId, boolean includeByDefault);

	void updateInstrumentModelPartOf(InstrumentModelPartOf instrumentmodelpartof);
}