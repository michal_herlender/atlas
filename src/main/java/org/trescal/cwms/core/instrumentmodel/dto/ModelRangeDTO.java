package org.trescal.cwms.core.instrumentmodel.dto;

import org.trescal.cwms.core.instrumentmodel.RangeType;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.instrumentmodel.entity.option.Option;

public class ModelRangeDTO implements Comparable<ModelRangeDTO>
{
	private String end;
	private Integer id;
	private String start;
	private Integer uomId;
	private String minvalue;
	private String maxvalue;
	private boolean maxIsInfinite;
	private RangeType characteristicType;
	private CharacteristicDescription characteristicDescription;
	private Option modelOption;
	private String value;
	
	public ModelRangeDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public ModelRangeDTO(ModelRange range) {
		if (range != null)
		{
			this.setId(range.getId());
			this.setStart(range.getStartAsString());
			this.setEnd(range.getEndAsString());
			if (range.getUom() != null)
			{
				this.setUomId(range.getUom().getId());
			}
			this.setMinvalue(range.getMinvalue());
			this.setMaxvalue(range.getMaxvalue());
			this.setMaxIsInfinite(range.isMaxIsInfinite());
			this.setCharacteristicDescription(range.getCharacteristicDescription());
			this.setCharacteristicType(range.getCharacteristicType());
			this.setModelOption(range.getModelOption());
			this.setValue(range.getValue());
		}
	}
	
	public String getEnd()
	{
		return this.end;
	}

	public Integer getId()
	{
		return this.id;
	}

	public String getStart()
	{
		return this.start;
	}

	public Integer getUomId()
	{
		return this.uomId;
	}

	public void setEnd(String end)
	{
		this.end = end;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public void setStart(String start)
	{
		this.start = start;
	}

	public void setUomId(Integer uomId)
	{
		this.uomId = uomId;
	}

	public String getMinvalue() {
		return minvalue;
	}

	public String getMaxvalue() {
		return maxvalue;
	}

	public boolean isMaxIsInfinite() {
		return maxIsInfinite;
	}

	public RangeType getCharacteristicType() {
		return characteristicType;
	}

	public CharacteristicDescription getCharacteristicDescription() {
		return characteristicDescription;
	}

	public Option getModelOption() {
		return modelOption;
	}
	
	public String getValue()
	{
		return this.value;
	}
	
	public void setMinvalue(String minvalue) {
		this.minvalue = minvalue;
	}

	public void setMaxvalue(String maxvalue) {
		this.maxvalue = maxvalue;
	}

	public void setMaxIsInfinite(boolean maxIsInfinite) {
		this.maxIsInfinite = maxIsInfinite;
	}

	public void setCharacteristicType(RangeType characteristicType) {
		this.characteristicType = characteristicType;
	}

	public void setCharacteristicDescription(
			CharacteristicDescription characteristicDescription) {
		this.characteristicDescription = characteristicDescription;
	}

	public void setModelOption(Option modelOption) {
		this.modelOption = modelOption;
	}
	
	public void setValue(String value)
	{
		this.value = value;
	}
	
	@Override
	public int compareTo(ModelRangeDTO other) {
		if (this.getCharacteristicDescription() != null && other.getCharacteristicDescription() != null) {
			return this.getCharacteristicDescription().getOrderno() - other.getCharacteristicDescription().getOrderno();
		}
		return -1;
	}
}