package org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.db;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription_;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.db.CharacteristicLibraryDao;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionDao;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange_;
import org.trescal.cwms.core.instrumentmodel.entity.uom.db.UoMDao;
import org.trescal.cwms.core.referential.db.TMLLocaleResolver;
import org.trescal.cwms.core.referential.dto.TMLCharacteristicDTO;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.spring.model.LabelIdDTO;

@Repository
public class CharacteristicDescriptionDaoImpl extends BaseDaoImpl<CharacteristicDescription, Integer> implements CharacteristicDescriptionDao {
	
	private static final Logger logger = LoggerFactory.getLogger(CharacteristicDescriptionDaoImpl.class);
	
	@Autowired
	private NewDescriptionDao instModelSubfamilyDao;
	@Autowired
	private UoMDao uomDao;
	@Autowired
	private CharacteristicLibraryDao characteristicLibraryDao;
	
	
	
	@Override
	protected Class<CharacteristicDescription> getEntity() {
		return CharacteristicDescription.class;
	}
	
	// Only return characteristic descriptions with characteristic type of 14 which is VALUE_WITH_UNIT
	@Override
	public List<LabelIdDTO> getLabelIdList(String descriptionFragment, Locale searchLanguage, int maxResults) {
		return getResultList(cb -> {
		CriteriaQuery<LabelIdDTO> cq = cb.createQuery(LabelIdDTO.class);

		Root<CharacteristicDescription> rootCharactDesc = cq.from(CharacteristicDescription.class);
		Join<CharacteristicDescription, Translation> joinTranslation = rootCharactDesc.join(CharacteristicDescription_.translations, JoinType.LEFT);
		
		cq.select(cb.construct(LabelIdDTO.class, 
				joinTranslation.get(Translation_.translation),
				rootCharactDesc.get(CharacteristicDescription_.characteristicDescriptionId)));

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(joinTranslation.get(Translation_.locale), searchLanguage));
		clauses.getExpressions().add(cb.like(joinTranslation.get(Translation_.translation), "%"+descriptionFragment+"%"));
		clauses.getExpressions().add(cb.equal(rootCharactDesc.get(CharacteristicDescription_.characteristicTypeInt), 14));
		
		cq.where(clauses);
		cq.groupBy(rootCharactDesc.get(CharacteristicDescription_.characteristicDescriptionId),
				joinTranslation.get(Translation_.translation));
		cq.orderBy(cb.asc(joinTranslation.get(Translation_.translation)));
		
		return cq;
		}, 0, maxResults);
	}
	
	@Override
	public CharacteristicDescription getTMLCharacteristicDescription(int tmlid) {
		return getFirstResult(cb -> {
			CriteriaQuery<CharacteristicDescription> cq=cb.createQuery(CharacteristicDescription.class);
			Root<CharacteristicDescription> charaDescRoot= cq.from(CharacteristicDescription.class);
			cq.where(cb.equal(charaDescRoot.get(CharacteristicDescription_.tmlid), tmlid));
			return cq;
		}).orElse(null);
	}

	@Override
	public CharacteristicDescription getByInternalNameAndSubfamilyId(String internalName, Integer subfamilyId) {
		if (internalName == null) throw new IllegalArgumentException("internalName may not be null");
		if (subfamilyId == null) throw new IllegalArgumentException("subfamilyId may not be null");

		Optional<CharacteristicDescription> result = getFirstResult(cb -> {
			CriteriaQuery<CharacteristicDescription> cq = cb.createQuery(CharacteristicDescription.class);
			Root<CharacteristicDescription> root = cq.from(CharacteristicDescription.class);
			Join<CharacteristicDescription, Description> subfamily = root.join(CharacteristicDescription_.description, JoinType.INNER);
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(CharacteristicDescription_.internalName), internalName));
			clauses.getExpressions().add(cb.equal(subfamily.get(Description_.id), subfamilyId));
			
			cq.where(clauses);
			
			return cq;
		});
		return result.orElse(null);
	}


	@Override
	public void insertTMLCharacteristicDescription(TMLCharacteristicDTO dto)
			throws Exception {
		// First try to find if a characteristic already exists with the same name and not linked to TML
		Description description = instModelSubfamilyDao.getTMLDescription(dto.getSubFamily());
		CharacteristicDescription existingCharacteristicDescription = getFirstResult(cb -> {
		CriteriaQuery<CharacteristicDescription> cq = cb.createQuery(CharacteristicDescription.class);

		Root<CharacteristicDescription> rootCharactDesc = cq.from(CharacteristicDescription.class);
		Join<CharacteristicDescription, Description> joinDesc = rootCharactDesc.join(CharacteristicDescription_.description);
		
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(rootCharactDesc.get(CharacteristicDescription_.internalName), dto.getInternalName()));
		clauses.getExpressions().add(cb.equal(joinDesc.get(Description_.id), description.getId()));
		
		Predicate andClause = cb.conjunction();
		andClause.getExpressions().add(cb.isNotNull(rootCharactDesc.get(CharacteristicDescription_.tmlid)));
		andClause.getExpressions().add(cb.notEqual(rootCharactDesc.get(CharacteristicDescription_.tmlid), dto.getTMLID()));
		
		Predicate orClause = cb.disjunction();
		orClause.getExpressions().add(cb.isNull(rootCharactDesc.get(CharacteristicDescription_.tmlid)));
		orClause.getExpressions().add(andClause);
		
		clauses.getExpressions().add(orClause);
		
		cq.where(clauses);
		return cq;
		}).orElse(null);
		
		if (existingCharacteristicDescription == null)
		{
			CharacteristicDescription characteristicDescription = new CharacteristicDescription();
			characteristicDescription.setTmlid(dto.getTMLID());
			characteristicDescription.setInternalName(dto.getInternalName());
			characteristicDescription.setDescription(description);
			characteristicDescription.setName(dto.getDefaultName());
			characteristicDescription.setShortName(dto.getShortName());
			characteristicDescription.setActive(dto.isIsEnabled());
			characteristicDescription.setRequired(dto.isIsRequired());
			characteristicDescription.setOrderno(dto.getOrderNo());
			characteristicDescription.setCharacteristicTypeInt(dto.getFieldType());
			characteristicDescription.setMetadata(dto.getMetaData());
			characteristicDescription.setUom(dto.getUnit() != null ? !dto.getUnit().equals("") ? uomDao.findUoMbySymbol(dto.getUnit()) : uomDao.findDefaultUoM() : uomDao.findDefaultUoM());
//			characteristicDescription.setUnitList(dto.getUnitList());
			
			SortedSet<Translation> newTranslations = new TreeSet<Translation>();
			
			Iterator<String> iterator = dto.getRegionalNames().keySet().iterator();
			
			while(iterator.hasNext()) 
			{
				String lang = iterator.next();
				String value = dto.getRegionalNames().get(lang);	
				if (TMLLocaleResolver.isResolvable(lang)) {
					Locale locale = TMLLocaleResolver.resolveLocale(lang); 
					Translation t = new Translation(locale,!value.equals(null) ? value.toString() : "");
					newTranslations.add(t);
				}
			}
			
			characteristicDescription.setTranslations(newTranslations);
			
			logger.debug("Inserting: " + characteristicDescription.toFullString());
			this.persist(characteristicDescription);
			// Add this to avoid locks
			logger.debug("Inserted: " + characteristicDescription.toFullString());
			
			this.updateTMLCharacteristicDescription(dto);
		}
	}

	@Override
	public void updateTMLCharacteristicDescription(TMLCharacteristicDTO dto)
			throws Exception {
		CharacteristicDescription characteristicDescription = this.getTMLCharacteristicDescription(dto.getTMLID());
		
		// First try to find if a characteristic already exists with the same name and not linked to TML
		Description description = instModelSubfamilyDao.getTMLDescription(dto.getSubFamily());
		CharacteristicDescription existingCharacteristicDescription = null;
		
		try
		{
			existingCharacteristicDescription = getFirstResult(cb -> {
				CriteriaQuery<CharacteristicDescription> cq = cb.createQuery(CharacteristicDescription.class);

				Root<CharacteristicDescription> rootCharactDesc = cq.from(CharacteristicDescription.class);
				Join<CharacteristicDescription, Description> joinDesc = rootCharactDesc.join(CharacteristicDescription_.description);
				
				Predicate clauses = cb.conjunction();
				clauses.getExpressions().add(cb.equal(rootCharactDesc.get(CharacteristicDescription_.internalName), dto.getDefaultName()));
				clauses.getExpressions().add(cb.notEqual(rootCharactDesc.get(CharacteristicDescription_.characteristicDescriptionId), characteristicDescription.getCharacteristicDescriptionId()));
				clauses.getExpressions().add(cb.equal(joinDesc.get(Description_.id), description.getId()));
				
				Predicate andClause = cb.conjunction();
				andClause.getExpressions().add(cb.isNotNull(rootCharactDesc.get(CharacteristicDescription_.tmlid)));
				andClause.getExpressions().add(cb.notEqual(rootCharactDesc.get(CharacteristicDescription_.tmlid), dto.getTMLID()));
				
				Predicate orClause = cb.disjunction();
				orClause.getExpressions().add(cb.isNull(rootCharactDesc.get(CharacteristicDescription_.tmlid)));
				orClause.getExpressions().add(andClause);
				
				clauses.getExpressions().add(orClause);
				
				cq.where(clauses);
				return cq;
				}).orElse(null);
		}
		catch (Exception ex)
		{
			throw ex;
		}
		
		if (existingCharacteristicDescription == null)
		{
			characteristicDescription.setTmlid(dto.getTMLID());
			characteristicDescription.setInternalName(dto.getInternalName());
			characteristicDescription.setDescription(description);
			characteristicDescription.setName(dto.getDefaultName());
			characteristicDescription.setShortName(dto.getShortName());
			characteristicDescription.setActive(dto.isIsEnabled());
			characteristicDescription.setRequired(dto.isIsRequired());
			characteristicDescription.setOrderno(dto.getOrderNo());
			characteristicDescription.setCharacteristicTypeInt(dto.getFieldType());
			characteristicDescription.setMetadata(dto.getMetaData());
			
			if (dto.getUnit() != null && dto.getUnit().equals(""))
			{
				characteristicDescription.setUom(uomDao.findDefaultUoM());
			}
			else
			{
				characteristicDescription.setUom(uomDao.findUoMbySymbol(dto.getUnit()));
			}
			
//			characteristicDescription.setUnitList(dto.getUnitList());
//			characteristicDescription.setShortName(dto.getShortName());
			
			SortedSet<Translation> newTranslations = new TreeSet<Translation>();
			
			Iterator<String> iterator = dto.getRegionalNames().keySet().iterator();
			
			while(iterator.hasNext()) 
			{
				String lang = iterator.next();
				String value = dto.getRegionalNames().get(lang);	
				if (TMLLocaleResolver.isResolvable(lang)) {
					Locale locale = TMLLocaleResolver.resolveLocale(lang); 
					Translation t = new Translation(locale,!value.equals(null) ? value.toString() : "");
					newTranslations.add(t);
				}
			}
			
			characteristicDescription.setTranslations(newTranslations);
	
			logger.debug("Updating: " + characteristicDescription.toFullString());
			this.merge(characteristicDescription);
			// Add this to avoid locks
			logger.debug("Updated: " + characteristicDescription.toFullString());
			
			this.characteristicLibraryDao.updateTMLLibraries(characteristicDescription.getCharacteristicDescriptionId(), dto.getChoices());
		}
		else
		{
			String errorMessage = MessageFormat.format("Error while updating, the characteristic {0} already exists with a different TMLID.",dto.getDefaultName());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		}
	}

	@Override
	public void deleteTMLCharacteristicDescription(TMLCharacteristicDTO dto)
			throws Exception {
		// Check if this characteristic is already used by active instrument
		Integer count = getCount(cb -> cq -> {
	    	Root<ModelRange> rootModelRange = cq.from(ModelRange.class);
		    Join<ModelRange, CharacteristicDescription> joinCharaDesc = rootModelRange.join(ModelRange_.characteristicDescription);
		   		    
		    cq.where(cb.equal(joinCharaDesc.get(CharacteristicDescription_.tmlid), dto.getTMLID()));
			return Triple.of(rootModelRange.get(ModelRange_.id.getName()), null, null);
		});
		
		if (count <= 0)
		{
			CharacteristicDescription characteristicDescription = this.getTMLCharacteristicDescription(dto.getTMLID());
			
			characteristicDescription.setActive(dto.isIsEnabled());

			logger.debug("Deleting: " + characteristicDescription.toFullString());
			this.merge(characteristicDescription);
			// Add this to avoid locks
			logger.debug("Deleted: " + characteristicDescription.toFullString());
		}
		else				
		{
			String errorMessage = MessageFormat.format("Error while deleting, the characteristic {0} is used by at least 1 active instrument/instrument model.",dto.getDefaultName());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		}
	}
}
