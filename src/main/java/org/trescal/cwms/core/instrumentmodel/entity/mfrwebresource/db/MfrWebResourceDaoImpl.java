package org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource.MfrWebResource;
import org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource.MfrWebResource_;

@Repository("MfrWebResourceDao")
public class MfrWebResourceDaoImpl extends BaseDaoImpl<MfrWebResource, Integer> implements MfrWebResourceDao {
	
	@Override
	protected Class<MfrWebResource> getEntity() {
		return MfrWebResource.class;
	}

	@Override
	public List<MfrWebResource> findMfrWebResource(String url, int mfrid) {
		return getResultList(cb ->{
			CriteriaQuery<MfrWebResource> cq = cb.createQuery(MfrWebResource.class);
			Root<MfrWebResource> root = cq.from(MfrWebResource.class); 
			Join<MfrWebResource, Mfr> mfr = root.join(MfrWebResource_.mfr);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(MfrWebResource_.url), url));
			clauses.getExpressions().add(cb.equal(mfr.get(Mfr_.mfrid), mfrid));
			cq.where(clauses);
			return cq;
		});
	}
}