package org.trescal.cwms.core.instrumentmodel.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.userright.enums.Permission;

/*
 * Contains base elements that are shared by all Model Controller subclasses
 * (now partitioned by tab for separation of concerns - smaller classes are more manageable - and performance)
 */
public abstract class AbstractModelController {
	@Autowired
	private InstrumentModelService instrumentModelService;
	
	@ModelAttribute("model")
	public InstrumentModel findInstrumentModel(@RequestParam("modelid") int modelId) {
		return instrumentModelService.findInstrumentModel(modelId);
	}
	
	@ModelAttribute("canEditInstrumentModel")
	public boolean getCanEditInstrumentModel() {
		Collection<? extends GrantedAuthority> permissions = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		return permissions.contains(Permission.MODEL_EDIT);
	}
	
	@ModelAttribute("canEditSalesCategory")
	public boolean getCanEditSalesCategory() {
		Collection<? extends GrantedAuthority> permissions = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		return permissions.contains(Permission.INSTRUMENT_MODEL_SALES_CATEGORY);
	}
}