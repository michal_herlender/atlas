/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

/**
 * Entity that maps a base unit {@link InstrumentModel} onto a module
 * {@link InstrumentModel}.
 * 
 * @author richard
 */
@Entity
@Table(name = "modelpartof", uniqueConstraints = @UniqueConstraint(columnNames = { "baseid", "moduleid" }))
public class InstrumentModelPartOf extends Auditable
{
	private int id;
	private InstrumentModel base;
	private InstrumentModel module;
	/**
	 * Defines if this relationship should always be 'on' (when adding the base
	 * unit to a quote if this is true then the module would automatically be
	 * added)
	 */
	private boolean includedByDefault;
	private String baseUnitName;
	private String moduleName;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "baseid", nullable=true)
	public InstrumentModel getBase() {
		return this.base;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "moduleid", nullable=true)
	public InstrumentModel getModule() {
		return this.module;
	}
	
	@Transient
	public String getBaseUnitName() {
		return baseUnitName;
	}
	
	@Transient
	public String getModuleName() {
		return moduleName;
	}
	
	@NotNull
	@Column(name = "includedbydefault", nullable = false, columnDefinition="tinyint")
	public boolean isIncludedByDefault() {
		return this.includedByDefault;
	}
	
	public void setBase(InstrumentModel base) {
		this.base = base;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setIncludedByDefault(boolean includedByDefault) {
		this.includedByDefault = includedByDefault;
	}
	
	public void setModule(InstrumentModel module) {
		this.module = module;
	}
	
	public void setBaseUnitName(String baseUnitName) {
		this.baseUnitName = baseUnitName;
	}
	
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
}