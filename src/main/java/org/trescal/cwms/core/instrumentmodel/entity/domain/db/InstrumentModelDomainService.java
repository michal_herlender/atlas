package org.trescal.cwms.core.instrumentmodel.entity.domain.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.referential.dto.TMLDomainDTO;
import org.trescal.cwms.spring.model.InstrumentModelDomainJsonDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface InstrumentModelDomainService extends BaseService<InstrumentModelDomain, Integer> {

	void createInstrumentModelDomain(InstrumentModelDomain instrumentModelDomain);

	boolean isDuplicate(String name);

	List<InstrumentModelDomainJsonDTO> getAllDomains(Locale userLocale);

	InstrumentModelDomain getDomain(int id);

	void updateInstrumentModelDomain(InstrumentModelDomain instrumentModelDomain);

	void deleteInstrumentModelDomain(InstrumentModelDomain domain);

	int getResultCountExact(String translation, Locale locale, int excludeId);

	InstrumentModelDomain getTMLDomain(int tmlid);

	void createTMLInstrumentModelDomain(TMLDomainDTO dto) throws Exception;

	void updateTMLInstrumentModelDomain(TMLDomainDTO dto) throws Exception;

	void deleteTMLInstrumentModelDomain(TMLDomainDTO dto) throws Exception;

	List<KeyValueIntegerString> getAllKeyValues(String domainFragment, DomainType domainType, Locale userLocale,
			int maxResults);
}