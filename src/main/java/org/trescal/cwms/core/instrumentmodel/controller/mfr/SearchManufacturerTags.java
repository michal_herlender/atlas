package org.trescal.cwms.core.instrumentmodel.controller.mfr;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.spring.model.LabelIdDTO;

@Controller @JsonController
public class SearchManufacturerTags {
	
	@Autowired
	MfrService manufacturerService;
	
	public static int MAX_RESULTS = 50; 
	
	@RequestMapping(value="/searchmanufacturertags.json", method=RequestMethod.GET)
	public @ResponseBody List<LabelIdDTO> getTagList(@RequestParam("term") String descriptionFragment,
													 @RequestParam(value = "searchEverywhere",required = false,defaultValue = "true") Boolean searchEverywhere) {
		return manufacturerService.getLabelIdList(descriptionFragment, searchEverywhere, MAX_RESULTS);
	}

}
