package org.trescal.cwms.core.instrumentmodel.dto;

import java.util.Date;

public class DescriptionPlantillasDTO {
	
	private Date lastModified;
	private Boolean active;
	private Integer subfamilyid;
	private Integer familyid;
	private String translation_en;
	private String translation_fr;
	private String translation_es;
	
	public DescriptionPlantillasDTO(Date lastModified, Boolean active, Integer subfamilyid, Integer familyid,
			String translation_en, String translation_fr, String translation_es) {
		super();
		this.lastModified = lastModified;
		this.active = active;
		this.subfamilyid = subfamilyid;
		this.familyid = familyid;
		this.translation_en = translation_en;
		this.translation_fr = translation_fr;
		this.translation_es = translation_es;
	}
		
	public Date getLastModified() {
		return lastModified;
	}
	public Boolean getActive() {
		return active;
	}
	public Integer getSubfamilyid() {
		return subfamilyid;
	}
	public Integer getFamilyid() {
		return familyid;
	}
	public String getTranslation_en() {
		return translation_en;
	}
	public String getTranslation_fr() {
		return translation_fr;
	}
	public String getTranslation_es() {
		return translation_es;
	}
}
