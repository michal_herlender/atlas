package org.trescal.cwms.core.instrumentmodel.entity.modelrange;

import java.util.Comparator;

/**
 * Comparator used for comparing {@link ModelRange}
 * 
 * @author Stuart
 */
public class ModelRangeComparator implements Comparator<ModelRange>
{
	@Override
	public int compare(ModelRange r1, ModelRange r2)
	{
		return -1;
	}
}
