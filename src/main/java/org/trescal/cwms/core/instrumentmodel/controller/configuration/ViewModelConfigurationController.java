package org.trescal.cwms.core.instrumentmodel.controller.configuration;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.controller.AbstractModelController;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@RequestMapping(value="/instrumentmodelconfig.htm")
@Controller @IntranetController
public class ViewModelConfigurationController extends AbstractModelController{
	
	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	
	@ModelAttribute("selectedTab")
	public String selectedTab(){
		return "configuration";
	}

	@RequestMapping(method=RequestMethod.GET)
	public String requestHandler(Model model,
			@RequestParam("modelid") Integer modelid)
	{
		InstrumentModel instModel = this.instrumentModelService.findInstrumentModel(modelid);

		// All characteristics defined for the sub-family are displayed; not all may be set on instrument model
		List<CharacteristicDescription> sortedCharacteristics = new ArrayList<>(instModel.getDescription().getCharacteristicDescriptions());
		sortedCharacteristics.sort(Comparator.comparing(cd -> cd.getOrderno()));
		
		// It's possible, if errors have entered via the TML sync, 
		// to have two ModelRange with same CharacteristicDescription or CD to be null
		// We want to show them in the interface, for debugging purposes hence the list
		Map<CharacteristicDescription, List<ModelRange>> rangeMap = instModel.getRanges().stream()
				.filter(mr -> mr.getCharacteristicDescription() != null)
				.collect(Collectors.groupingBy(mr -> mr.getCharacteristicDescription()));
		
		// Must be a non-TML model to edit characteristics 
		model.addAttribute("sortedCharacteristics", sortedCharacteristics);
		model.addAttribute("rangeMap", rangeMap); 
		model.addAttribute("supportedLocales", supportedLocaleService.getSupportedLocales());
		model.addAttribute("translationMap", getTranslationMap(instModel.getNameTranslations()));
		
		return "/trescal/core/instrumentmodel/configuration/modelviewconfiguration";
	}
	
	private Map<Locale, String> getTranslationMap(Set<Translation> translations) {
		Map<Locale, String> result = new HashMap<>();
		translations.forEach(tr -> result.put(tr.getLocale(), tr.getTranslation()));
		return result;
	}
}