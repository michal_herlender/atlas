package org.trescal.cwms.core.instrumentmodel.entity.description.db;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.dto.DescriptionPlantillasDTO;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db.InstrumentModelFamilyDao;
import org.trescal.cwms.core.referential.db.TMLLocaleResolver;
import org.trescal.cwms.core.referential.dto.TMLSubFamilyDTO;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;
import org.trescal.cwms.spring.model.InstrumentModelDescriptionJsonDto;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Repository
public class NewDescriptionDaoImpl extends BaseDaoImpl<Description, Integer> implements NewDescriptionDao {

	private static final Logger logger = LoggerFactory.getLogger(NewDescriptionDaoImpl.class);

	@Autowired
	private InstrumentModelFamilyDao instModelFamDao;

	/*
	 * The use of a Criteria query for joining ElementCollection is not currently
	 * supported so using HQL for joining translations associated with a Description
	 * See 1st section of
	 * https://developer.jboss.org/wiki/HibernateFAQ-AdvancedProblems This is an
	 * unsorted query for simplicity, sorted used in DTO list only
	 */
//	private static String FROM_QUERY_STRING = "from Description d " + "left join d.translations t "
//			+ "where t.locale = :locale " + "and t.translation like :translation";
//	private static String FROM_QUERY_STRING_EXCLUDE = FROM_QUERY_STRING + " and d.id != :exclude";

	@Override
	protected Class<Description> getEntity() {
		return Description.class;
	}

	@Override
	public List<InstrumentModelDescriptionJsonDto> findDescriptionsForFamily(int familyid, Locale searchLanguage) {
		return getResultList(cb -> {
		CriteriaQuery<InstrumentModelDescriptionJsonDto> cq = cb.createQuery(InstrumentModelDescriptionJsonDto.class);
		Root<Description> description = cq.from(Description.class);
		Join<Description, InstrumentModelFamily> family = description.join(Description_.family, JoinType.INNER);
		Join<Description, Translation> translation = description.join(Description_.translations, JoinType.LEFT);

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(translation.get(Translation_.locale), searchLanguage));
		clauses.getExpressions().add(cb.equal(family.get(InstrumentModelFamily_.familyid), familyid));
		cq.where(clauses);

		List<Selection<?>> selectionList = new ArrayList<>();
		selectionList.add(description.get(Description_.id));
		selectionList.add(description.get(Description_.active));
		selectionList.add(description.get(Description_.description));
		selectionList.add(translation.get(Translation_.translation));
		selectionList.add(description.get(Description_.tmlid));
		cq.multiselect(selectionList);

		return cq;
		});
	}

	/*
	 * Performs a query for a description fragment, meant for use with paged result
	 * set.
	 */
	@Override
	public List<Description> performQueryFragment(String descriptionFragment, Locale searchLanguage, int firstResult,
			int maxResults) {
		return getResultList(cb ->{
				CriteriaQuery<Description> cq = cb.createQuery(Description.class);
				Root<Description> root = cq.from(Description.class);
				Join<Description, Translation> translationJoin = root.join(Description_.translations, JoinType.LEFT);
				
				cq.where(cb.and(cb.equal(translationJoin.get(Translation_.locale), searchLanguage),
						        cb.like(translationJoin.get(Translation_.translation), "%" + descriptionFragment + "%")));
				
				return cq;
		}, firstResult, maxResults);
	}

	// Gets a result count for the description fragment in the specified search
	// language
	@Override
	public int getResultCountFragment(String descriptionFragment, Locale searchLanguage) {
		return (int) getCount(cb -> cq -> {
			Root<Description> root = cq.from(Description.class);
			Join<Description, Translation> translationJoin = root.join(Description_.translations, JoinType.LEFT);
			
			cq.where(cb.and(cb.equal(translationJoin.get(Translation_.locale), searchLanguage),
					        cb.like(translationJoin.get(Translation_.translation), "%" + descriptionFragment + "%")));
			
			return Triple.of(root.get(Description_.id), null, null);
		});
	}

	// Gets a result count for the description (matching exactly) in the specified
	// search language
	@Override
	public int getResultCountExact(String description, Locale searchLanguage) {
		return (int) getCount(cb -> cq -> {
			Root<Description> root = cq.from(Description.class);
			Join<Description, Translation> translationJoin = root.join(Description_.translations, JoinType.LEFT);
			
			cq.where(cb.and(cb.equal(translationJoin.get(Translation_.locale), searchLanguage),
					        cb.like(translationJoin.get(Translation_.translation), description)));
			
			return Triple.of(root.get(Description_.id), null, null);
		});
	}

	/*
	 * Gets a result count for the description search term (matching exactly) in the
	 * specified search language, excluding the specified description entity from
	 * the search.
	 */
	@Override
	public int getResultCountExact(String description, Locale searchLanguage, Description exclude) {
		return (int) getCount(cb -> cq -> {
			Root<Description> root = cq.from(Description.class);
			Join<Description, Translation> translationJoin = root.join(Description_.translations, JoinType.LEFT);
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(translationJoin.get(Translation_.locale), searchLanguage));
			clauses.getExpressions().add(cb.like(translationJoin.get(Translation_.translation), description));
			clauses.getExpressions().add(cb.notEqual(root.get(Description_.id), exclude.getId()));
			cq.where(clauses);
			
			return Triple.of(root.get(Description_.id), null, null);
		});
	}

	/*
	 * Returns translation for specified locale, or null if not found
	 */
	@Override
	public Translation getTranslationForLocaleOrNull(Description description, Locale locale) {
		for (Translation t : description.getTranslations()) {
			if (t.getLocale().equals(locale))
				return t;
		}
		return null;
	}

	/*
	 * Performs a search using the description fragment in the specified language
	 */
	@Override
	public List<KeyValueIntegerString> getAllKeyValues(String descriptionFragment, DomainType domainType,
			Locale searchLanguage, int maxResults) {
		return getResultList(cb -> {
			CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class);
			Root<Description> subfamily = cq.from(Description.class);
			Expression<String> subfamilyName = joinTranslation(cb, subfamily, Description_.translations,
					searchLanguage);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.like(subfamilyName, '%' + descriptionFragment + '%'));
			if (domainType != null) {
				Join<Description, InstrumentModelFamily> family = subfamily.join(Description_.family);
				Join<InstrumentModelFamily, InstrumentModelDomain> domain = family.join(InstrumentModelFamily_.domain);
				clauses.getExpressions().add(cb.equal(domain.get(InstrumentModelDomain_.domainType), domainType));
			}
			cq.where(clauses);
			cq.orderBy(cb.asc(subfamilyName));
			cq.select(cb.construct(KeyValueIntegerString.class, subfamily.get(Description_.id), subfamilyName));
			return cq;
		}, 0, maxResults);
	}

	@Override
	public List<Description> getDescriptions(String description, Locale searchLanguage) {
		return getResultList(cb -> {
			CriteriaQuery<Description> cq = cb.createQuery(Description.class);
			Root<Description> root = cq.from(Description.class);
			Join<Description, Translation> translationJoin = root.join(Description_.translations, JoinType.LEFT);
			
			cq.where(cb.and(cb.equal(translationJoin.get(Translation_.locale), searchLanguage),
					        cb.like(translationJoin.get(Translation_.translation), description)));
			
			return cq;
		});
	}
	
	@Override
	public Description getByTmlId(Integer tmlSubFamilyId) {
		if (tmlSubFamilyId == null) throw new IllegalArgumentException("tmlSubFamilyId must not be null!");
		return super.findByUniqueProperty(Description_.tmlid, tmlSubFamilyId);
	}

	// Finds a description by TML ID (not primary key)
	@Override
	@Deprecated
	public Description getTMLDescription(int tmlid) {
		return getFirstResult(cb -> {
			CriteriaQuery<Description> cq = cb.createQuery(Description.class);
			Root<Description> root = cq.from(Description.class);
			
			cq.where(cb.equal(root.get(Description_.tmlid), tmlid));
			
			return cq;
		}).orElse(null);
	}

	// Saves a newly created description in the database
	@Override
	public void insertTMLDescription(TMLSubFamilyDTO dto) throws Exception {
		// First try to find if a subfamily already exists with the same name and not
		// linked to TML
		InstrumentModelFamily instrumentModelFamily = instModelFamDao.getTMLFamily(dto.getFamilyID());
		if(instrumentModelFamily == null) {
			String errorMessage = MessageFormat.format(
					"Error while inserting the description {0}, the related Family with ID {1} doesn't exists.",
					dto.getDefaultName(), dto.getFamilyID());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		}
		else {
			Description existingDescription = getFirstResult(cb -> {
				CriteriaQuery<Description> cq = cb.createQuery(Description.class);
				Root<Description> root = cq.from(Description.class);
				Join<Description, InstrumentModelFamily> instModelFamilyJoin = root.join(Description_.family);
				
				Predicate clauses = cb.conjunction();
				clauses.getExpressions().add(cb.equal(root.get(Description_.description), dto.getDefaultName()));
				clauses.getExpressions().add(cb.equal(instModelFamilyJoin.get(InstrumentModelFamily_.familyid), instrumentModelFamily.getFamilyid()));
				
				Predicate andClause = cb.conjunction();
				andClause.getExpressions().add(cb.isNotNull(root.get(Description_.tmlid)));
				andClause.getExpressions().add(cb.notEqual(root.get(Description_.tmlid), dto.getTMLID()));
				
				Predicate orClause = cb.disjunction();
				orClause.getExpressions().add(cb.isNull(root.get(Description_.tmlid)));
				orClause.getExpressions().add(andClause);
				
				clauses.getExpressions().add(orClause);
				
				cq.where(clauses);
				
				return cq;
			}).orElse(null);
	
			if (existingDescription == null) {
				Description description = new Description();
	
				description.setTmlid(dto.getTMLID());
				description.setDescription(dto.getDefaultName());
				description.setFamily(instrumentModelFamily);
				description.setActive(dto.isIsEnabled());
				description.setTypology(dto.getTypology());
	
				SortedSet<Translation> newTranslations = new TreeSet<Translation>();
	
				Iterator<String> iterator = dto.getRegionalNames().keySet().iterator();
	
				while (iterator.hasNext()) {
					String lang = iterator.next();
					String value = dto.getRegionalNames().get(lang);
					if (TMLLocaleResolver.isResolvable(lang)) {
						Locale locale = TMLLocaleResolver.resolveLocale(lang); 
						Translation t = new Translation(locale, !value.equals(null) ? value.toString() : "");
						newTranslations.add(t);
					}
				}
	
				description.setTranslations(newTranslations);
	
				logger.debug("Inserting: " + description.toFullString());
				this.persist(description);
				// Add this to avoid locks
				logger.debug("Inserted: " + description.toFullString());
			} else {
				if (existingDescription.getTmlid() != null && !existingDescription.getTmlid().equals(dto.getTMLID())) {
					String errorMessage = MessageFormat.format(
							"Error while inserting, the subfamily {0} already exists with a different TMLID.",
							dto.getDefaultName());
					logger.error(errorMessage);
					throw new Exception(errorMessage);
				} else {
					if (existingDescription.getTmlid() == null) {
						existingDescription.setTmlid(dto.getTMLID());
						this.merge(existingDescription);
						// Add this to avoid locks
					}
	
					if (dto.isIsEnabled()) {
						this.updateTMLDescription(dto);
					} else {
						this.deleteTMLDescription(dto);
					}
				}
			}
		}
	}

	// Saves a newly created description in the database
	@Override
	public void updateTMLDescription(TMLSubFamilyDTO dto) throws Exception {
		Description description = this.getTMLDescription(dto.getTMLID());

		// First try to find if a subfamily already exists with the same name and not
		// linked to TML
		InstrumentModelFamily instrumentModelFamily = instModelFamDao.getTMLFamily(dto.getFamilyID());
		if(instrumentModelFamily == null) {
			String errorMessage = MessageFormat.format(
					"Error while updating the description {0}, the related Family with ID {1} doesn't exists.",
					dto.getDefaultName(), dto.getFamilyID());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		}
		else {
			Description existingDescription = getFirstResult(cb -> {
				CriteriaQuery<Description> cq = cb.createQuery(Description.class);
				Root<Description> root = cq.from(Description.class);
				Join<Description, InstrumentModelFamily> instModelFamilyJoin = root.join(Description_.family, JoinType.LEFT);
				
				Predicate clauses = cb.conjunction();
				clauses.getExpressions().add(cb.equal(root.get(Description_.description), dto.getDefaultName()));
				clauses.getExpressions().add(cb.notEqual(root.get(Description_.id), description.getId()));
				clauses.getExpressions().add(cb.equal(instModelFamilyJoin.get(InstrumentModelFamily_.familyid), instrumentModelFamily.getFamilyid()));
				
				Predicate andClause = cb.conjunction();
				andClause.getExpressions().add(cb.isNotNull(root.get(Description_.tmlid)));
				andClause.getExpressions().add(cb.notEqual(root.get(Description_.tmlid), dto.getTMLID()));
				
				Predicate orClause = cb.disjunction();
				orClause.getExpressions().add(cb.isNull(root.get(Description_.tmlid)));
				orClause.getExpressions().add(andClause);
				
				clauses.getExpressions().add(orClause);
				
				cq.where(clauses);
				
				return cq;
			}).orElse(null);
	
			if (existingDescription == null) {
				description.setTmlid(dto.getTMLID());
				description.setDescription(dto.getDefaultName());
				description.setFamily(instrumentModelFamily);
				description.setActive(dto.isIsEnabled());
				description.setTypology(dto.getTypology());
	
				SortedSet<Translation> newTranslations = new TreeSet<Translation>();
	
				Iterator<String> iterator = dto.getRegionalNames().keySet().iterator();
	
				while (iterator.hasNext()) {
					String lang = iterator.next();
					String value = dto.getRegionalNames().get(lang);
					if (TMLLocaleResolver.isResolvable(lang)) {
						Locale locale = TMLLocaleResolver.resolveLocale(lang); 
						Translation t = new Translation(locale, !value.equals(null) ? value.toString() : "");
						newTranslations.add(t);
					}
				}
	
				description.setTranslations(newTranslations);
	
				logger.debug("Updating: " + description.toFullString());
				this.merge(description);
				// Add this to avoid locks
				logger.debug("Updated: " + description.toFullString());
			} else {
				String errorMessage = MessageFormat.format(
						"Error while updating, the subfamily {0} already exists with a different TMLID.",
						dto.getDefaultName());
				logger.error(errorMessage);
				throw new Exception(errorMessage);
			}
		}
	}

	// Deletes a description from the database
	@Override
	public void deleteTMLDescription(TMLSubFamilyDTO dto) throws Exception {
		// Check if this domain is already used by active instrument
		int count = (int) getCount(cb -> cq -> {
			Root<Instrument> root = cq.from(Instrument.class);
			Join<Instrument, InstrumentModel> instModelJoin = root.join(Instrument_.model);
			Join<InstrumentModel, Description> descriptionJoin = instModelJoin.join(InstrumentModel_.description);
			
			cq.where(cb.equal(descriptionJoin.get(Description_.tmlid), dto.getTMLID()));
			
			return Triple.of(root.get(Instrument_.plantid), null, null);
		});

		if (count <= 0) {
			Description description = this.getTMLDescription(dto.getTMLID());
			description.setActive(dto.isIsEnabled());

			logger.debug("Deleting: " + description.toFullString());
			this.merge(description);
			// Add this to avoid locks
			logger.debug("Deleted: " + description.toFullString());
			;
		} else {
			String errorMessage = MessageFormat.format(
					"Error while deleting, the subfamily {0} is used by at least 1 active instrument.",
					dto.getDefaultName());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		}
	}

	/**
	 * Used for both count and results queries
	 * 
	 * @param cq
	 * @param id
	 * @param afterLastModified
	 */
	private void assemblePlantillasQuery(CriteriaQuery<?> cq, Root<Description> root, Integer id,
			Date afterLastModified) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		if ((id != null) && (afterLastModified != null)) {
			throw new IllegalArgumentException("At least one of id or afterLastModified must be null");
		}
		if (id != null) {
			cq.where(cb.equal(root.get(Description_.id), id));
		} else if (afterLastModified != null) {
			cq.where(cb.greaterThan(root.get(Description_.lastModified), afterLastModified));
		}
	}

	private CriteriaQuery<Long> getPlantillasCountQuery(Integer id, Date afterLastModified) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);

		Root<Description> root = cq.from(Description.class);
		assemblePlantillasQuery(cq, root, id, afterLastModified);
		cq.multiselect(cb.count(root));
		return cq;
	}

	private CriteriaQuery<DescriptionPlantillasDTO> getPlantillasResultQuery(Integer id, Date afterLastModified) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<DescriptionPlantillasDTO> cq = cb.createQuery(DescriptionPlantillasDTO.class);

		Root<Description> root = cq.from(Description.class);
		Join<Description, InstrumentModelFamily> family = root.join(Description_.family,
				javax.persistence.criteria.JoinType.LEFT);

		Join<Description, Translation> translationEnglish = root.join(Description_.translations,
				javax.persistence.criteria.JoinType.LEFT);
		translationEnglish.on(cb.equal(translationEnglish.get(Translation_.locale), SupportedLocales.ENGLISH_GB));

		Join<Description, Translation> translationFrench = root.join(Description_.translations,
				javax.persistence.criteria.JoinType.LEFT);
		translationFrench.on(cb.equal(translationFrench.get(Translation_.locale), SupportedLocales.FRENCH_FR));

		Join<Description, Translation> translationSpanish = root.join(Description_.translations,
				javax.persistence.criteria.JoinType.LEFT);
		translationSpanish.on(cb.equal(translationSpanish.get(Translation_.locale), SupportedLocales.SPANISH_ES));

		cq.select(cb.construct(DescriptionPlantillasDTO.class, root.get(Description_.lastModified),
				root.get(Description_.active), root.get(Description_.id), family.get(InstrumentModelFamily_.familyid),
				translationEnglish.get(Translation_.translation), translationFrench.get(Translation_.translation),
				translationSpanish.get(Translation_.translation)));

		assemblePlantillasQuery(cq, root, id, afterLastModified);

		return cq;
	}

	@Override
	public DescriptionPlantillasDTO getPlantillasDescription(int id) {
		CriteriaQuery<DescriptionPlantillasDTO> cq = getPlantillasResultQuery(id, null);

		List<DescriptionPlantillasDTO> result = getEntityManager().createQuery(cq).getResultList();
		return result.isEmpty() ? null : result.get(0);
	}

	@Override
	public PagedResultSet<DescriptionPlantillasDTO> getPlantillasDescriptions(Date afterLastModified,
			int resultsPerPage, int currentPage) {
		PagedResultSet<DescriptionPlantillasDTO> prs = new PagedResultSet<>(resultsPerPage, currentPage);

		CriteriaQuery<Long> cqCount = getPlantillasCountQuery(null, afterLastModified);
		TypedQuery<Long> countQuery = getEntityManager().createQuery(cqCount);
		prs.setResultsCount(countQuery.getSingleResult().intValue());

		CriteriaQuery<DescriptionPlantillasDTO> cqResults = getPlantillasResultQuery(null, afterLastModified);
		TypedQuery<DescriptionPlantillasDTO> query = getEntityManager().createQuery(cqResults);
		query.setFirstResult(prs.getStartResultsFrom());
		query.setMaxResults(prs.getResultsPerPage());
		prs.setResults(query.getResultList());
		return prs;
	}
}