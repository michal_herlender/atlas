package org.trescal.cwms.core.instrumentmodel.entity.mfr.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.dto.MfrSearchResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.form.genericentityvalidator.MfrValidator;
import org.trescal.cwms.core.referential.dto.TMLManufacturerDTO;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.spring.model.LabelIdDTO;

@Service("MfrService")
public class MfrServiceImpl extends BaseServiceImpl<Mfr, Integer> implements MfrService
{
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private MfrDao mfrDao;
	@Autowired
	private MfrValidator mfrValidator;
	@Autowired
	private SystemComponentService scServ;

	@Override
	public void delete(int id)
	{
		Mfr m = this.findMfr(id);
		if (m != null)
		{
			this.mfrDao.remove(m);
		}
	}

	public void deleteMfr(Mfr mfr)
	{
		this.mfrDao.remove(mfr);
	}

	public Mfr findMfr(int id)
	{
		Mfr mfr = this.mfrDao.find(id);
		// 2018-12-12 GB : Prevent directory lookup on repeated gets.
		if (mfr != null && mfr.getDirectory() == null)
		{
			mfr.setDirectory((this.compDirServ.getDirectory(this.scServ.findComponent(Component.MFR), String.valueOf(mfr.getMfrid()))));
		}
		return mfr;
	}
	
	public List<Mfr> findMfr(String nameFragment, Integer ignoreMfrId) {
		return mfrDao.findMfr(nameFragment, ignoreMfrId);
	}
	
	public Mfr findMfrByName(String name)
	{
		return this.mfrDao.findMfrByName(name);
	}

	public ResultWrapper findMfrDWR(int id)
	{
		Mfr mfr = this.findMfr(id);
		if (mfr != null)
		{
			return new ResultWrapper(true, "", mfr, null);
		}
		else
		{
			return new ResultWrapper(false, "The mfr could not be found", null, null);
		}
	}

	public List<Mfr> getAllMfrs()
	{
		return this.mfrDao.findAll();
	}

	@Override
	public List<Mfr> getSortedActiveMfr()
	{
		return this.mfrDao.getSortedActiveMfr();
	}

	@Override
	public Mfr getSystemGenericMfr()
	{
		return this.mfrDao.getSystemGenericMfr();
	}

	@Override
	public ResultWrapper insert(String name)
	{
		Mfr mfr = new Mfr();
		mfr.setName(name.trim());
		mfr.setActive(true);
		mfr.setGenericMfr(false);

		// validate this Mfr before trying the insert
		BindException bindException = new BindException(mfr, "mfr");
		this.mfrValidator.validate(mfr, bindException);

		// if validation passes then insert the Mfr
		if (!bindException.hasErrors())
		{
			this.insertMfr(mfr);
		}

		return new ResultWrapper(bindException, mfr);
	}

	public void insertMfr(Mfr Mfr)
	{
		this.mfrDao.persist(Mfr);
	}

	@Override
	public boolean isDuplicateMfr(String nameFragment, Integer mfrid)
	{
		return this.mfrDao.findMfr(nameFragment, mfrid).size() > 0 ? true : false;
	}

	public void saveOrUpdateMfr(Mfr mfr)
	{
		this.mfrDao.saveOrUpdate(mfr);
	}

	@Override
	public List<Mfr> searchSortedActiveMfr(String nameFragment)
	{
		return this.mfrDao.searchSortedMfr(nameFragment, true);
	}

	public List<MfrSearchResultWrapper> searchSortedActiveMfrHQL(String searchName)
	{
		return this.mfrDao.searchSortedActiveMfrHQL(searchName);
	}

	@Override
	public List<Mfr> searchSortedMfr(String nameFragment)
	{
		return this.mfrDao.searchSortedMfr(nameFragment, null);
	}

	public void setCompDirServ(ComponentDirectoryService compDirServ)
	{
		this.compDirServ = compDirServ;
	}

	public void setMfrDao(MfrDao MfrDao)
	{
		this.mfrDao = MfrDao;
	}

	public void setMfrValidator(MfrValidator mfrValidator)
	{
		this.mfrValidator = mfrValidator;
	}

	public void setScServ(SystemComponentService scServ)
	{
		this.scServ = scServ;
	}

	public void updateMfr(Mfr Mfr)
	{
		this.mfrDao.update(Mfr);
	}

	public Mfr findTMLMfr(int tmlid)
	{
		return this.mfrDao.findTMLMfr(tmlid);
	}

	public void insertTMLMfr(TMLManufacturerDTO dto) throws Exception
	{
		this.mfrDao.insertTMLMfr(dto);
	}

	public void updateTMLMfr(TMLManufacturerDTO dto) throws Exception
	{
		this.mfrDao.updateTMLMfr(dto);
	}

	public void deleteTMLMfr(TMLManufacturerDTO dto) throws Exception
	{
		this.mfrDao.deleteTMLMfr(dto);
	}

	@Override
	public List<LabelIdDTO> getLabelIdList(String descriptionFragment, Boolean searchEverywhere, int maxResults) {
		return this.mfrDao.getLabelIdList(descriptionFragment,searchEverywhere, maxResults);
	}

	@Override
	protected BaseDao<Mfr, Integer> getBaseDao() {
		return this.mfrDao;
	}
}