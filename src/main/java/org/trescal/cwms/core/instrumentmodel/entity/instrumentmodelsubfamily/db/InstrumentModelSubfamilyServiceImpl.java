package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelsubfamily.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelsubfamily.InstrumentModelSubfamily;

@Service("InstrumentModelSubfamilyService")
public class InstrumentModelSubfamilyServiceImpl extends BaseServiceImpl<InstrumentModelSubfamily, Integer>
		implements InstrumentModelSubfamilyService {

	@Autowired
	private InstrumentModelSubfamilyDao dao;

	@Override
	protected BaseDao<InstrumentModelSubfamily, Integer> getBaseDao() {
		return this.dao;
	}

	@Override
	public InstrumentModelSubfamily getByTmlId(Integer tmlSubFamilyId) {
		return dao.getByTmlId(tmlSubFamilyId);
	}

}
