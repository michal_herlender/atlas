package org.trescal.cwms.core.instrumentmodel.entity.salescategory.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.persistence.Tuple;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.spring.model.KeyValueIntegerString;
import org.trescal.cwms.spring.model.LabelIdDTO;


public interface SalesCategoryDao extends BaseDao<SalesCategory, Integer> {

	List<SalesCategory> getAllSalesCategories();

	int getResultsCountExact(String translation,
			Locale searchLanguage, int excludeId);

	List<LabelIdDTO> getLabelIdList(String salesCategoryFragment,Locale searchLanguage, int maxResults);
	
	List<Tuple> getSalesCategoryIdsForModelIds(Collection<Integer> modelIds);

	String getTranslationForLocale(int id, Locale locale);
	
	List<KeyValueIntegerString> getSalesCategoriesForSubfamily(Integer subFamilyId, Locale locale);
}
