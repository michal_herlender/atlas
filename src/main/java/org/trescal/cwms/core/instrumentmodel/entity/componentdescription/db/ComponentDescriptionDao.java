package org.trescal.cwms.core.instrumentmodel.entity.componentdescription.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.dto.CompDescSearchResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.componentdescription.ComponentDescription;

public interface ComponentDescriptionDao extends BaseDao<ComponentDescription, Integer> {
	
	List<ComponentDescription> findComponentDescription(String nameFragment, Integer descriptionid);
	
	List<ComponentDescription> searchSortedAllComponentDescriptions(String nameFragment);
	
	List<CompDescSearchResultWrapper> searchSortedAllComponentDescriptionsHQL(String nameFragment);
}