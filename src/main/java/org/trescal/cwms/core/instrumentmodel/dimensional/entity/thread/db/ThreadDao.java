package org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;
import org.trescal.cwms.core.instrumentmodel.dimensional.form.ThreadSearchForm;
import org.trescal.cwms.core.instrumentmodel.dto.ThreadSearchResultWrapper;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface ThreadDao extends BaseDao<Thread, Integer> {
	
	List<Thread> findAllThreadsBySizeAndType(String type, String size);
	
	List<Thread> findAllThreadsByType(String type);
	
	PagedResultSet<Thread> searchThreads(ThreadSearchForm form, PagedResultSet<Thread> prs);
	
	List<ThreadSearchResultWrapper> searchThreadsHQL(String searchTerm, Integer typeId);
}