package org.trescal.cwms.core.instrumentmodel.entity.modelrange.db;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;

@Service("modelRangeService")
public class ModelRangeServiceImpl extends BaseServiceImpl<ModelRange, Integer> implements ModelRangeService
{
	@Autowired
	private ModelRangeDao modelRangeDao;

	@Override
	public void deleteAll(Collection<ModelRange> ranges)
	{
		this.modelRangeDao.deleteAll(ranges);
	}

	@Override
	public void deleteModelRange(ModelRange modelrange)
	{
		modelrange.getModel().getRanges().remove(modelrange);

		this.modelRangeDao.remove(modelrange);
	}
	
	@Override
	public void deleteModelRange(Integer modelRangeId)
	{
		
		ModelRange modRange = this.findModelRange(modelRangeId);
		
		this.deleteModelRange(modRange);
	}
	
	@Override
	public ModelRange findModelRange(int id)
	{
		return this.modelRangeDao.find(id);
	}

	@Override
	public List<ModelRange> getAllModelRanges()
	{
		return this.modelRangeDao.findAll();
	}
	
	public ModelRange getByModelIdAndOptionId(int modelId, int optionId) {
		return this.modelRangeDao.getByModelIdAndOptionId(modelId, optionId);
	}

	@Override
	public void insertModelRange(ModelRange ModelRange)
	{
		this.modelRangeDao.persist(ModelRange);
	}

	@Override
	public void saveOrUpdateModelRange(ModelRange modelrange)
	{
		this.modelRangeDao.saveOrUpdate(modelrange);
	}

	@Override
	public void updateModelRange(ModelRange ModelRange)
	{
		this.modelRangeDao.update(ModelRange);
	}

	@Override
	protected BaseDao<ModelRange, Integer> getBaseDao() {
		return this.modelRangeDao;
	}
}