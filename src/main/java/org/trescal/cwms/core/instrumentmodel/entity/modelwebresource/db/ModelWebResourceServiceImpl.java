package org.trescal.cwms.core.instrumentmodel.entity.modelwebresource.db;

import java.util.List;

import org.springframework.validation.BindException;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.modelwebresource.ModelWebResource;
import org.trescal.cwms.core.instrumentmodel.form.genericentityvalidator.ModelWebResourceValidator;

public class ModelWebResourceServiceImpl implements ModelWebResourceService
{
	private InstrumentModelService modelServ;
	ModelWebResourceDao ModelWebResourceDao;
	private ModelWebResourceValidator validator;

	public void deleteModelWebResource(ModelWebResource modelwebresource)
	{
		this.ModelWebResourceDao.remove(modelwebresource);
	}

	@Override
	public ResultWrapper deleteWebResource(Integer resourceId)
	{
		if (resourceId == null)
		{
			return new ResultWrapper(false, "Unable to find resource to delete.", null, null);
		}

		ModelWebResource wr = this.ModelWebResourceDao.find(resourceId);

		if (wr == null)
		{
			return new ResultWrapper(false, "Unable to find resource to delete.", null, null);
		}
		else
		{
			this.ModelWebResourceDao.remove(wr);
			return new ResultWrapper(true, "deleted", wr, null);
		}
	}

	public ModelWebResource findModelWebResource(int id)
	{
		return this.ModelWebResourceDao.find(id);
	}

	@Override
	public List<ModelWebResource> findModelWebResource(String url, int modelid)
	{
		return this.ModelWebResourceDao.findModelWebResource(url, modelid);
	}

	public List<ModelWebResource> getAllModelWebResources()
	{
		return this.ModelWebResourceDao.findAll();
	}

	@Override
	public ResultWrapper insert(int modelid, String description, String url)
	{
		ModelWebResource res = new ModelWebResource();
		res.setModel(this.modelServ.findInstrumentModel(modelid));
		res.setDescription(description);
		res.setUrl(url);
		
		BindException errors = new BindException(res, "modelwebres");
		this.validator.validate(res, errors);
		if (!errors.hasErrors())
		{
			this.insertModelWebResource(res);
		}
		return new ResultWrapper(errors, res);
	}

	public void insertModelWebResource(ModelWebResource ModelWebResource)
	{
		this.ModelWebResourceDao.persist(ModelWebResource);
	}

	@Override
	public boolean isDuplicateModelWebResource(String url, int modelid)
	{
		return this.findModelWebResource(url, modelid).size() > 0 ? true : false;
	}

	public void saveOrUpdateModelWebResource(ModelWebResource modelwebresource)
	{
		this.ModelWebResourceDao.saveOrUpdate(modelwebresource);
	}

	public void setModelServ(InstrumentModelService modelServ)
	{
		this.modelServ = modelServ;
	}

	public void setModelWebResourceDao(ModelWebResourceDao ModelWebResourceDao)
	{
		this.ModelWebResourceDao = ModelWebResourceDao;
	}

	public void setValidator(ModelWebResourceValidator validator)
	{
		this.validator = validator;
	}

	public void updateModelWebResource(ModelWebResource ModelWebResource)
	{
		this.ModelWebResourceDao.update(ModelWebResource);
	}
}