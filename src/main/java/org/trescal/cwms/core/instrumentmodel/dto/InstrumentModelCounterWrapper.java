package org.trescal.cwms.core.instrumentmodel.dto;

public class InstrumentModelCounterWrapper
{
	private Integer cals;
	private Integer certs;
	private Integer comps;
	private Integer insts;
	private Integer jobs;
	private Integer quotes;
	private Integer recentquotes;
	private Integer tpquotes;

	public InstrumentModelCounterWrapper()
	{
		this.jobs = new Integer(0);
		this.quotes = new Integer(0);
		this.recentquotes = new Integer(0);
		this.certs = new Integer(0);
		this.cals = new Integer(0);
		this.tpquotes = new Integer(0);
		this.insts = new Integer(0);
		this.comps = new Integer(0);
	}

	public Integer getCals()
	{
		return this.cals;
	}

	public Integer getCerts()
	{
		return this.certs;
	}

	public Integer getComps()
	{
		return this.comps;
	}

	public Integer getInsts()
	{
		return this.insts;
	}

	public Integer getJobs()
	{
		return this.jobs;
	}

	public Integer getQuotes()
	{
		return this.quotes;
	}

	public Integer getRecentquotes()
	{
		return this.recentquotes;
	}

	public Integer getTpquotes()
	{
		return this.tpquotes;
	}

	public void setCals(Integer cals)
	{
		this.cals = cals;
	}

	public void setCerts(Integer certs)
	{
		this.certs = certs;
	}

	public void setComps(Integer comps)
	{
		this.comps = comps;
	}

	public void setInsts(Integer insts)
	{
		this.insts = insts;
	}

	public void setJobs(Integer jobs)
	{
		this.jobs = jobs;
	}

	public void setQuotes(Integer quotes)
	{
		this.quotes = quotes;
	}

	public void setRecentquotes(Integer recentquotes)
	{
		this.recentquotes = recentquotes;
	}

	public void setTpquotes(Integer tpquotes)
	{
		this.tpquotes = tpquotes;
	}

}
