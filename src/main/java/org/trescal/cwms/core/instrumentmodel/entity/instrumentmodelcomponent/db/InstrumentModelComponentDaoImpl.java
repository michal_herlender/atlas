package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent.InstrumentModelComponent;

@Repository("InstrumentModelComponentDao")
public class InstrumentModelComponentDaoImpl extends BaseDaoImpl<InstrumentModelComponent, Integer> implements InstrumentModelComponentDao {
	
	@Override
	protected Class<InstrumentModelComponent> getEntity() {
		return InstrumentModelComponent.class;
	}
	
	@Override
	public InstrumentModelComponent find(Integer id) {
		Criteria criteria = getSession().createCriteria(InstrumentModelComponent.class);
		criteria.setFetchMode("component", FetchMode.JOIN);
		criteria.setFetchMode("component.mfr", FetchMode.JOIN);
		criteria.setFetchMode("component.description", FetchMode.JOIN);
		criteria.add(Restrictions.idEq(id));
		return (InstrumentModelComponent) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Component> getAllUnAttachedComponents(int modelid) {
		Criteria crit = getSession().createCriteria(Component.class);
		crit.setFetchMode("description", FetchMode.JOIN);
		crit.setFetchMode("mfr", FetchMode.JOIN);
		crit.setFetchMode("stockLastCheckedBy", FetchMode.JOIN);
		// build a subquery to to get all Components already attached to this
		// model
		DetachedCriteria detCrit = DetachedCriteria.forClass(Component.class);
		detCrit.setProjection(Projections.property("id"));
		DetachedCriteria detCrit2 = detCrit.createCriteria("modelComponents");
		detCrit2.createCriteria("model").add(Restrictions.idEq(modelid));
		List<Integer> excluded = (List<Integer>) detCrit2.getExecutableCriteria(getSession()).list();
		Conjunction disJun = Restrictions.conjunction();
		for (Integer id : excluded) disJun.add(Restrictions.ne("id", id));
		crit.add(disJun);
		crit.createCriteria("mfr").addOrder(Order.asc("name"));
		crit.createCriteria("description").addOrder(Order.asc("description"));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<InstrumentModelComponent> getInstrumentModelComponents(int modelid, int componentid, Integer id) {
		Criteria crit = getSession().createCriteria(InstrumentModelComponent.class);
		crit.createCriteria("model").add(Restrictions.idEq(modelid));
		crit.createCriteria("component").add(Restrictions.idEq(componentid));
		if ((id != null) && (id != 0)) crit.add(Restrictions.ne("id", id));
		return crit.list();
	}
}