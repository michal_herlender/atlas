package org.trescal.cwms.core.instrumentmodel.controller.prices;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.controller.AbstractModelController;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.pricing.catalogprice.dto.CatalogPriceDTO;
import org.trescal.cwms.core.pricing.catalogprice.dto.CatalogPriceWithCurrencyValue;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.tools.MultiCurrencyUtils;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
@RequestMapping(value = {"/instrumentmodel.htm","/instrumentmodelprices.htm"})
public class ViewModelPricesController extends AbstractModelController {
	
	@Autowired
	private CompanyService companyService;
	@Autowired
	private CatalogPriceService catalogPriceService;
	
	@ModelAttribute("selectedTab")
	public String setSelectedTab(){
		return "prices";
	}
		
	@ModelAttribute("prices")
	public Map<Integer, List<CatalogPriceWithCurrencyValue>> getPrices(@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer,String> companyDto,
			@ModelAttribute("model") InstrumentModel instrumentModel, Locale locale){
		Company usersCompany = companyService.get(companyDto.getKey());
		List<CatalogPriceDTO> prices = this.catalogPriceService.findPrices(instrumentModel.getModelid(), locale);
		if(instrumentModel.getSalesCategory() != null && !instrumentModel.getModelType().getSalescategory()){
			prices.addAll(this.catalogPriceService.getCatalogPricesFromSalesCategory
					(instrumentModel.getSalesCategory().getId(), locale));
		}
		// group the result by business company id
		 Map<Integer, List<CatalogPriceWithCurrencyValue>> catalogPriceWithCurrencyValues = prices.stream().map(p -> this.createCatalogPriceWithCurrencyValue(p,usersCompany.getCurrency()))
				.collect(Collectors.groupingBy(CatalogPriceWithCurrencyValue::getCoid));
		 
        return catalogPriceWithCurrencyValues;
        
	}
		
	@RequestMapping(method=RequestMethod.GET)
	public String requestHandler() throws Exception
	{
        return "/trescal/core/instrumentmodel/prices/modelviewprices";
	}
	
	private CatalogPriceWithCurrencyValue createCatalogPriceWithCurrencyValue(CatalogPriceDTO catalogPrice,
			SupportedCurrency requiredCurrency) {
	
		BigDecimal currencyPrice = MultiCurrencyUtils.convertCurrencyValue(catalogPrice.getPrice(),
				catalogPrice.getSupportedCurrency(), requiredCurrency);
		
		BigDecimal currencyUnitPrice = (catalogPrice.getVariablePrice() != null && !StringUtils.isEmpty(catalogPrice.getVariablePriceUnit())) ?  MultiCurrencyUtils.convertCurrencyValue(
				catalogPrice.getUnitPrice(), catalogPrice.getSupportedCurrency(),
				requiredCurrency) : new BigDecimal(0) ;

		return new CatalogPriceWithCurrencyValue(catalogPrice, currencyPrice, currencyUnitPrice, requiredCurrency, catalogPrice.getCoid());
	}
}