package org.trescal.cwms.core.instrumentmodel.entity.hiremodel;

import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.hire.entity.hiremodelincategory.HireModelInCategory;
import org.trescal.cwms.core.hire.entity.hiremodelincategory.HireModelInCategoryComparator;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.system.entity.supportedcurrency.MultiCurrencyAware;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "hiremodel", uniqueConstraints = @UniqueConstraint(columnNames = {"modelid", "orgid"}))
public class HireModel extends Allocated<Company> implements MultiCurrencyAware {
	private BigDecimal hireCost;
	private String hireModelDetail;
	private Set<HireModelInCategory> hireModelInCategories;
	private int id;
	private InstrumentModel instmodel;
	private BigDecimal purchaseCost;
	
	public HireModel() {}
	
	public HireModel(InstrumentModel instrumentModel, Company allocatedCompany) {
		this.instmodel = instrumentModel;
		this.organisation = allocatedCompany;
	}
	
	@Column(name = "hirecost", unique = false, nullable = true, insertable = true, updatable = true, precision = 10, scale = 2)
	public BigDecimal getHireCost()
	{
		return this.hireCost;
	}
	
	@Length(max = 500)
	@Column(name = "hiremodeldetail", length = 500)
	public String getHireModelDetail()
	{
		return this.hireModelDetail;
	}
	
	/**
	 * @return the hire model categories
	 */
	@OneToMany(mappedBy = "hireModel", fetch = FetchType.LAZY)
	@Cascade(CascadeType.ALL)
	@SortComparator(HireModelInCategoryComparator.class)
	public Set<HireModelInCategory> getHireModelInCategories()
	{
		return this.hireModelInCategories;
	}
	
	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelid")
	public InstrumentModel getInstmodel()
	{
		return this.instmodel;
	}
	
	@Column(name = "purchasecost", unique = false, nullable = true, insertable = true, updatable = true, precision = 10, scale = 2)
	public BigDecimal getPurchaseCost()
	{
		return this.purchaseCost;
	}
	
	@Override
	@Transient
	public SupportedCurrency getCurrency() {
		return organisation.getCurrency();
	}
	
	@Override
	@Transient
	public BigDecimal getRate() {
		return organisation.getRate();
	}
	
	public void setHireCost(BigDecimal hireCost)
	{
		this.hireCost = hireCost;
	}
	
	public void setHireModelDetail(String hireModelDetail)
	{
		this.hireModelDetail = hireModelDetail;
	}
	
	public void setHireModelInCategories(Set<HireModelInCategory> hireModelInCategories)
	{
		this.hireModelInCategories = hireModelInCategories;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public void setInstmodel(InstrumentModel instmodel)
	{
		this.instmodel = instmodel;
	}
	
	public void setPurchaseCost(BigDecimal purchaseCost)
	{
		this.purchaseCost = purchaseCost;
	}
	
	@Override
	public void setCurrency(SupportedCurrency currency) {
		throw new NotImplementedException();
	}
	
	@Override
	public void setRate(BigDecimal rate) {
		throw new NotImplementedException();
	}
}