/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.entity.uom;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.instrumentmodel.entity.range.Range;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationpoint.CalibrationPoint;
import org.trescal.cwms.core.system.entity.translation.Translation;

/**
 * Simple entity representing a unit of measurement used by a {@link Range}.
 * 
 * @author richard
 */
@Entity
@Table(name = "uom")
public class UoM extends Auditable
{
	private String description;
	private int id;
	private String name;
	private Set<CalibrationPoint> points;
	private String symbol;
	private Integer tmlid;
	private String shortName;
	private Boolean active = false;
	
	private Set<Translation> nameTranslation;

	@Length(min = 0, max = 200)
	@Column(name = "description", nullable = true, length = 200)
	public String getDescription()
	{
		return this.description;
	}

	/**
	 * Must supply this method as well as the overridden toString() so that the
	 * value of this can be retrieved by ajax calls
	 */
	@Transient
	public String getFormattedSymbol()
	{
		return this.toString();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@Length(min = 1, max = 50)
	@Column(name = "name", nullable = false, length = 50)
	public String getName()
	{
		return this.name;
	}

	@OneToMany(mappedBy = "uom", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<CalibrationPoint> getPoints()
	{
		return this.points;
	}

	@Length(max = 25)
	@Column(name = "symbol", length = 25)
	public String getSymbol()
	{
		return this.symbol;
	}
	
	@Column(name="tmlid", nullable=true)
	public Integer getTmlid() {
		return tmlid;
	}

	@Column(name = "ShortName", nullable = true,columnDefinition = "nvarchar(25)")
	public String getShortName() {
		return shortName;
	}
	
	@Column(name="active", columnDefinition="bit")
	public Boolean getActive() {
		return active;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setPoints(Set<CalibrationPoint> points)
	{
		this.points = points;
	}

	public void setSymbol(String symbol)
	{
		this.symbol = symbol;
	}
	
	public void setTmlid(Integer tmlid) {
		this.tmlid = tmlid;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	/**
	 * Returns the symbol for this UOM, or the name in lower case to suit those
	 * that do not have symbols, e.g. bar, psi, torr
	 */
	@Override
	@Transient
	public String toString()
	{
		if ((this.symbol == null) || (this.symbol.length() < 1))
		{
			return this.name.toLowerCase();
		}
		else
		{
			return this.symbol;
		}
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="uomnametranslation", joinColumns=@JoinColumn(name="id"))
	public Set<Translation> getNameTranslation() {
		return nameTranslation;
	}

	public void setNameTranslation(Set<Translation> nameTranslation) {
		this.nameTranslation = nameTranslation;
	}
	
	@Transient
	public String toFullString(){
		return "UoM [id=" + id + ", symbol=" + symbol
				+ ", translation=" + (nameTranslation != null ? nameTranslation : "")
				+ ", tmlid=" + tmlid + "]";
	}
}