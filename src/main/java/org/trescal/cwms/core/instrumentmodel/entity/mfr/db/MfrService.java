package org.trescal.cwms.core.instrumentmodel.entity.mfr.db;

import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.dto.MfrSearchResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.referential.dto.TMLManufacturerDTO;
import org.trescal.cwms.spring.model.LabelIdDTO;

public interface MfrService
{
	/**
	 * Delete's the {@link Mfr} identified by the given id. Warning, this method
	 * does not check for any associated entites and so should be used with
	 * caution.
	 * 
	 * @param id the {@link Mfr} id.
	 */
	void delete(int id);

	void deleteMfr(Mfr mfr);

	Mfr findMfr(int id);
	
	List<Mfr> findMfr(String nameFragment, Integer ignoreMfrId);
	
	Mfr findMfrByName(String name);

	ResultWrapper findMfrDWR(int id);

	List<Mfr> getAllMfrs();

	/**
	 * Gets a {@link List} of {@link Mfr} that are all active and sorted by
	 * name.
	 * 
	 * @return {@link List} of {@link Mfr}.
	 */
	List<Mfr> getSortedActiveMfr();

	/**
	 * Return's the system generic {@link Mfr} entity or null if none exists. If
	 * more than one is found then the first in the returned list is returned.
	 * 
	 * @return {@link Mfr}.
	 */
	Mfr getSystemGenericMfr();

	/**
	 * Inserts a new {@link Mfr} into the database, primarily designed for use
	 * by ajax calls.
	 * 
	 * @param name the name of the {@link Mfr}.
	 * @return {@link ResultWrapper}.
	 */
	ResultWrapper insert(String name);

	void insertMfr(Mfr mfr);

	/**
	 * Tests if the given {@link Mfr} name already exists for a different mfr
	 * whilst ignoring the {@link Mfr} identified by the given id.
	 * 
	 * @param nameFragment the name to match.
	 * @param mfrid the id of the current {@link Mfr} (will be ignored in
	 *        matching).
	 * @return true if a duplicate name exists.
	 */
	boolean isDuplicateMfr(String nameFragment, Integer mfrid);

	void saveOrUpdateMfr(Mfr mfr);

	/**
	 * Returns a sorted list of active {@link Mfr} whose name matches the name
	 * fragment passed as a parameter.
	 * 
	 * @param nameFragment begining one or more characters of the {@link Mfr}
	 *        name.
	 * @return {@link List} of {@link Mfr}.
	 */
	List<Mfr> searchSortedActiveMfr(String nameFragment);

	/**
	 * Returns a {@link List} of {@link MfrSearchResultWrapper} whose
	 * (case-insensitive) name matches the given name (Searches all active
	 * {@link Mfr}s).
	 * 
	 * @param nameFragment the name to match.
	 * @return {@link List} of {@link MfrSearchResultWrapper}.
	 */
	List<MfrSearchResultWrapper> searchSortedActiveMfrHQL(String searchName);

	/**
	 * Returns a {@link List} of {@link Mfr} whose (case-insensitive) name
	 * matches the given name (Searches all active and inactive {@link Mfr}s).
	 * 
	 * @param nameFragment the name to match.
	 * @return {@link List} of {@link Mfr}.
	 */
	List<Mfr> searchSortedMfr(String nameFragment);

	/**
	 * Updates the name of the {@link Mfr} identified by the given id.
	 * 
	 * @param id the id of the {@link Mfr}, not null.
	 * @param name the new name of the {@link Mfr}.
	 * @return {@link ResultWrapper}. ResultWrapper updateMfr(int id, String
	 *         name);
	 */
	void updateMfr(Mfr mfr);
	
	Mfr findTMLMfr(int tmlid);

	void insertTMLMfr(TMLManufacturerDTO dto) throws Exception;

	void updateTMLMfr(TMLManufacturerDTO dto) throws Exception;

	void deleteTMLMfr(TMLManufacturerDTO dto) throws Exception;

	List<LabelIdDTO> getLabelIdList(String descriptionFragment, Boolean searchEverywhere, int maxResults);
}