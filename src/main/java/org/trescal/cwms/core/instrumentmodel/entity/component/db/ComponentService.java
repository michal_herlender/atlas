package org.trescal.cwms.core.instrumentmodel.entity.component.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;
import org.trescal.cwms.core.instrumentmodel.entity.componentdescription.ComponentDescription;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.form.EditComponentForm;
import org.trescal.cwms.core.jobs.repair.dto.ComponentJsonDto;

public interface ComponentService extends BaseService<Component, Integer>
{
	Component createOrUpdateComponent(EditComponentForm form, Contact contact);
	
	/**
	 * Gets all {@link Component} with the {@link Mfr} and
	 * {@link ComponentDescription} identified by the given parameters exists.
	 * The {@link Component} identified by the cid will be ignored.
	 * 
	 * @param mfrid the id of the {@link Mfr}.
	 * @param descid the id of the {@link ComponentDescription}.
	 * @param cid the id of a {@link Component} to ignore in this query.
	 * @return
	 */
	List<Component> getComponents(int mfrid, int descid, Integer cid);

	/**
	 * Returns a {@link List} of all {@link Component} entities that match the
	 * given criteria. Sorted by {@link Mfr} name, {@link Description}
	 * description.
	 * 
	 * @param mfrid the {@link Mfr} id of the {@link Component}.
	 * @param descid the {@link Description} id of the the {@link Component}.
	 * @param inStock boolean indicating if the {@link Component} entities
	 *            returned should have one or more in stock.
	 * @return {@link List} of {@link Component}.
	 */
	List<Component> searchSortedModelComponent(Integer mfrid, Integer descid, Boolean inStock);

	List<ComponentJsonDto> searchComponents(String keyword, int mAX_RESULTS);
}