package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelsubfamily;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "instmodelsubfamily")
public class InstrumentModelSubfamily extends Auditable{
	
	private int subfamilyid;
	
	private String name;
	
	private Set<Translation> translation;
	
	private Boolean active;
	
	private Integer tmlid;
	
	private InstrumentModelFamily family;	
	
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="instmodelsubfamilytranslation", joinColumns=@JoinColumn(name="subfamilyid"))
	public Set<Translation> getTranslation() {
		return translation;
	}
	public void setTranslation(Set<Translation> translations) {
		this.translation = translations;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getSubfamilyid() {
		return subfamilyid;
	}
	public void setSubfamilyid(int subfamilyid) {
		this.subfamilyid = subfamilyid;
	}
	
	@Column(nullable=false, unique=true)
	public String getName() {
		return name;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "familyid", nullable = true)
	public InstrumentModelFamily getFamily() {
		return family;
	}
	
	public void setFamily(InstrumentModelFamily family) {
		this.family = family;
	}

	public void setName(String name) {
		this.name = name;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	@Column(nullable=true)
	public Integer getTmlid() {
		return tmlid;
	}
	public void setTmlid(Integer tmlid) {
		this.tmlid = tmlid;
	}
	@Override
	public String toString() {
		return "InstrumentModelFamily [subfamilyid=" + subfamilyid + ", name=" + name
				+ ", translation=" + (translation != null ? translation : "") + ", active=" + active
				+ ", tmlid=" + tmlid + "]";
	}
}