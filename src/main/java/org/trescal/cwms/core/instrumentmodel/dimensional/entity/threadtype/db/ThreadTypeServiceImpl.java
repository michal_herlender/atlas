package org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.ThreadType;

@Service
public class ThreadTypeServiceImpl extends BaseServiceImpl<ThreadType, Integer> implements ThreadTypeService
{
	@Autowired
	private ThreadTypeDao threadTypeDao;

	@Override
	protected BaseDao<ThreadType, Integer> getBaseDao() {
		return threadTypeDao;
	}
}