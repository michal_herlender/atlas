package org.trescal.cwms.core.instrumentmodel.dimensional.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.Wire;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.db.WireService;

@Controller
public class WireController
{
	@Autowired
	private WireService wireServ;

	@ModelAttribute("command")
	protected Wire formBackingObject(@RequestParam(name="id", required=false, defaultValue="0") Integer id) throws Exception
	{
		Wire wire = this.wireServ.get(id);

		if ((id == 0) || (wire == null))
		{
			wire = new Wire();
		}
		return wire;
	}

	@RequestMapping(value="/wire.htm", method=RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/instrumentmodel/dimensional/wire";
	}
	
	@RequestMapping(value="/wire.htm", method=RequestMethod.POST)
	protected String onSubmit(@Valid @ModelAttribute("command") Wire wire, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) {
			return referenceData();
		}
		if (wire.getId() == 0) {
			this.wireServ.save(wire);
		}
		else {
			this.wireServ.merge(wire);
		}
		
		return "redirect:wire.htm?id=" + wire.getId();
	}
}
