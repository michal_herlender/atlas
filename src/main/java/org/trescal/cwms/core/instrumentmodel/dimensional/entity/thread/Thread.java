package org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.CylindricalStandard;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.ThreadType;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.ThreadUoM;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.Wire;

@Entity
@Table(name = "dimensional_thread")
public class Thread extends Auditable
{
	private BigDecimal ball;
	private CylindricalStandard cylindricalStandard;
	private BigDecimal depthStep; // BSP tapered screw only
	private BigDecimal depthStepLower; // BSP tapered screw only
	private BigDecimal depthStepUpper; // BSP tapered screw only
	private BigDecimal goEffectiveDiameter;

	private BigDecimal goLowerLimit;

	private BigDecimal goUpperLimit;
	private BigDecimal goWornLimit;
	private int id;
	private Set<Instrument> instruments;

	private BigDecimal lead;
	private BigDecimal lengthToGaugePlane;
	private BigDecimal lengthToGaugePlaneLower;
	private BigDecimal lengthToGaugePlaneUpper;

	private BigDecimal maxStep;
	private BigDecimal minStep;
	// end tapered screw nominals
	private Integer noStarts;
	private BigDecimal notGoEffective;

	private BigDecimal notGoLowerLimit;
	private BigDecimal notGoUpperLimit;
	private BigDecimal notGoWornLimit;
	private BigDecimal overallLength;
	private BigDecimal overallLengthB;
	private BigDecimal overallLengthLower;
	private BigDecimal overallLengthUpper;
	private BigDecimal pitch;
	private BigDecimal sDatum; // BSP tapered screw only
	private String size;
	// tapered screw nominals
	private BigDecimal taperLimit;
	private ThreadType type;
	private ThreadUoM uom;
	private Wire wire;

	@Deprecated
	private BigDecimal wornLimit;

	@Column(name = "ball", precision = 20, scale = 5, nullable = true)
	public BigDecimal getBall()
	{
		return this.ball;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cylstandid")
	public CylindricalStandard getCylindricalStandard()
	{
		return this.cylindricalStandard;
	}

	@Column(precision = 20, scale = 5, nullable = true)
	public BigDecimal getDepthStep()
	{
		return this.depthStep;
	}

	@Column(precision = 20, scale = 5, nullable = true)
	public BigDecimal getDepthStepLower()
	{
		return this.depthStepLower;
	}

	@Column(precision = 20, scale = 5, nullable = true)
	public BigDecimal getDepthStepUpper()
	{
		return this.depthStepUpper;
	}

	@Column(name = "goeffectivediameter", precision = 20, scale = 5, nullable = true)
	public BigDecimal getGoEffectiveDiameter()
	{
		return this.goEffectiveDiameter;
	}

	@Column(name = "golowerlimit", precision = 20, scale = 5, nullable = true)
	public BigDecimal getGoLowerLimit()
	{
		return this.goLowerLimit;
	}

	@Column(name = "goupperlimit", precision = 20, scale = 5, nullable = true)
	public BigDecimal getGoUpperLimit()
	{
		return this.goUpperLimit;
	}

	@Column(name = "gowornlimit", precision = 20, scale = 5, nullable = true)
	public BigDecimal getGoWornLimit()
	{
		return this.goWornLimit;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "thread")
	public Set<Instrument> getInstruments()
	{
		return this.instruments;
	}

	@Column(name = "lead", precision = 20, scale = 5, nullable = true)
	public BigDecimal getLead()
	{
		return this.lead;
	}

	@Column(precision = 20, scale = 5, nullable = true)
	public BigDecimal getLengthToGaugePlane()
	{
		return this.lengthToGaugePlane;
	}

	@Column(precision = 20, scale = 5, nullable = true)
	public BigDecimal getLengthToGaugePlaneLower()
	{
		return this.lengthToGaugePlaneLower;
	}

	@Column(precision = 20, scale = 5, nullable = true)
	public BigDecimal getLengthToGaugePlaneUpper()
	{
		return this.lengthToGaugePlaneUpper;
	}

	@Column(precision = 20, scale = 5, nullable = true)
	public BigDecimal getMaxStep()
	{
		return this.maxStep;
	}

	@Column(precision = 20, scale = 5, nullable = true)
	public BigDecimal getMinStep()
	{
		return this.minStep;
	}

	@Column(name = "nostarts")
	public Integer getNoStarts()
	{
		return this.noStarts;
	}

	@Column(name = "notgoeffective", precision = 20, scale = 5, nullable = true)
	public BigDecimal getNotGoEffective()
	{
		return this.notGoEffective;
	}

	@Column(name = "notgolowerlimit", precision = 20, scale = 5, nullable = true)
	public BigDecimal getNotGoLowerLimit()
	{
		return this.notGoLowerLimit;
	}

	@Column(name = "notgoupperlimit", precision = 20, scale = 5, nullable = true)
	public BigDecimal getNotGoUpperLimit()
	{
		return this.notGoUpperLimit;
	}

	@Column(name = "notgowornlimit", precision = 20, scale = 5, nullable = true)
	public BigDecimal getNotGoWornLimit()
	{
		return this.notGoWornLimit;
	}

	@Column(precision = 20, scale = 5, nullable = true)
	public BigDecimal getOverallLength()
	{
		return this.overallLength;
	}

	@Column(precision = 20, scale = 5, nullable = true)
	public BigDecimal getOverallLengthB()
	{
		return this.overallLengthB;
	}

	@Column(precision = 20, scale = 5, nullable = true)
	public BigDecimal getOverallLengthLower()
	{
		return this.overallLengthLower;
	}

	@Column(precision = 20, scale = 5, nullable = true)
	public BigDecimal getOverallLengthUpper()
	{
		return this.overallLengthUpper;
	}

	@Column(name = "pitch", precision = 20, scale = 5, nullable = true)
	public BigDecimal getPitch()
	{
		return this.pitch;
	}

	@Column(precision = 20, scale = 5, nullable = true)
	public BigDecimal getSDatum()
	{
		return this.sDatum;
	}

	@NotNull
	@Length(min = 0, max = 100)
	@Column(name = "size", nullable = false, length = 100)
	public String getSize()
	{
		return this.size;
	}

	@Column(precision = 20, scale = 5, nullable = true)
	public BigDecimal getTaperLimit()
	{
		return this.taperLimit;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "typeid")
	public ThreadType getType()
	{
		return this.type;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uomid")
	public ThreadUoM getUom()
	{
		return this.uom;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "wireid")
	public Wire getWire()
	{
		return this.wire;
	}

	@Column(name = "wornlimit", precision = 20, scale = 5, nullable = true)
	public BigDecimal getWornLimit()
	{
		return this.wornLimit;
	}

	public void setBall(BigDecimal ball)
	{
		this.ball = ball;
	}

	public void setCylindricalStandard(CylindricalStandard cylindricalStandard)
	{
		this.cylindricalStandard = cylindricalStandard;
	}

	public void setDepthStep(BigDecimal depthStep)
	{
		this.depthStep = depthStep;
	}

	public void setDepthStepLower(BigDecimal depthStepLower)
	{
		this.depthStepLower = depthStepLower;
	}

	public void setDepthStepUpper(BigDecimal depthStepUpper)
	{
		this.depthStepUpper = depthStepUpper;
	}

	public void setGoEffectiveDiameter(BigDecimal goEffectiveDiameter)
	{
		this.goEffectiveDiameter = goEffectiveDiameter;
	}

	public void setGoLowerLimit(BigDecimal goLowerLimit)
	{
		this.goLowerLimit = goLowerLimit;
	}

	public void setGoUpperLimit(BigDecimal goUpperLimit)
	{
		this.goUpperLimit = goUpperLimit;
	}

	public void setGoWornLimit(BigDecimal goWornLimit)
	{
		this.goWornLimit = goWornLimit;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setInstruments(Set<Instrument> instruments)
	{
		this.instruments = instruments;
	}

	public void setLead(BigDecimal lead)
	{
		this.lead = lead;
	}

	public void setLengthToGaugePlane(BigDecimal lengthToGaugePlane)
	{
		this.lengthToGaugePlane = lengthToGaugePlane;
	}

	public void setLengthToGaugePlaneLower(BigDecimal lengthToGaugePlaneLower)
	{
		this.lengthToGaugePlaneLower = lengthToGaugePlaneLower;
	}

	public void setLengthToGaugePlaneUpper(BigDecimal lengthToGaugePlaneUpper)
	{
		this.lengthToGaugePlaneUpper = lengthToGaugePlaneUpper;
	}

	public void setMaxStep(BigDecimal maxStep)
	{
		this.maxStep = maxStep;
	}

	public void setMinStep(BigDecimal minStep)
	{
		this.minStep = minStep;
	}

	public void setNoStarts(Integer noStarts)
	{
		this.noStarts = noStarts;
	}

	public void setNotGoEffective(BigDecimal notGoEffective)
	{
		this.notGoEffective = notGoEffective;
	}

	public void setNotGoLowerLimit(BigDecimal notGoLowerLimit)
	{
		this.notGoLowerLimit = notGoLowerLimit;
	}

	public void setNotGoUpperLimit(BigDecimal notGoUpperLimit)
	{
		this.notGoUpperLimit = notGoUpperLimit;
	}

	public void setNotGoWornLimit(BigDecimal notGoWornLimit)
	{
		this.notGoWornLimit = notGoWornLimit;
	}

	public void setOverallLength(BigDecimal overallLength)
	{
		this.overallLength = overallLength;
	}

	public void setOverallLengthB(BigDecimal overallLengthB)
	{
		this.overallLengthB = overallLengthB;
	}

	public void setOverallLengthLower(BigDecimal overallLengthLower)
	{
		this.overallLengthLower = overallLengthLower;
	}

	public void setOverallLengthUpper(BigDecimal overallLengthUpper)
	{
		this.overallLengthUpper = overallLengthUpper;
	}

	public void setPitch(BigDecimal pitch)
	{
		this.pitch = pitch;
	}

	public void setSDatum(BigDecimal datum)
	{
		this.sDatum = datum;
	}

	public void setSize(String size)
	{
		this.size = size;
	}

	public void setTaperLimit(BigDecimal taperLimit)
	{
		this.taperLimit = taperLimit;
	}

	public void setType(ThreadType type)
	{
		this.type = type;
	}

	public void setUom(ThreadUoM uom)
	{
		this.uom = uom;
	}

	public void setWire(Wire wire)
	{
		this.wire = wire;
	}

	public void setWornLimit(BigDecimal wornLimit)
	{
		this.wornLimit = wornLimit;
	}
}