package org.trescal.cwms.core.instrumentmodel.form.description;

import java.util.SortedSet;

import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.system.entity.translation.Translation;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditDescriptionForm {
	
	private Description description;
	private Integer procId;	
	private String designationdefault;
	private SortedSet<Translation> translations;
	private String sourcepage; //page, where I come from
}
