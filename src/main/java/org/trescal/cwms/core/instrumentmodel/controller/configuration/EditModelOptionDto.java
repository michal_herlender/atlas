package org.trescal.cwms.core.instrumentmodel.controller.configuration;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditModelOptionDto {
	private Integer rangeId;
	private Integer optionId;
	private String code;
	private String name;
	private Boolean included;
}
