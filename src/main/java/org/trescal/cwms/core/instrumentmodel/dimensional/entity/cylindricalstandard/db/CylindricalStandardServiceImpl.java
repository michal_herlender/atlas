package org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.CylindricalStandard;

@Service("CylindricalStandardService")
public class CylindricalStandardServiceImpl extends BaseServiceImpl<CylindricalStandard, Double> implements CylindricalStandardService
{
	@Autowired
	private CylindricalStandardDao cylindricalStandardDao;

	@Override
	protected BaseDao<CylindricalStandard, Double> getBaseDao() {
		return cylindricalStandardDao;
	}
}