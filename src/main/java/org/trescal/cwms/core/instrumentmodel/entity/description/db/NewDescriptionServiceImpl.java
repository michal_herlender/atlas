package org.trescal.cwms.core.instrumentmodel.entity.description.db;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.dto.DescriptionPlantillasDTO;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.referential.dto.TMLSubFamilyDTO;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.spring.model.InstrumentModelDescriptionJsonDto;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Service
public class NewDescriptionServiceImpl extends BaseServiceImpl<Description, Integer> implements NewDescriptionService {

	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private NewDescriptionDao descriptionDao;
	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;

	@Override
	public String getStringForPrimaryLocale(Description description) {
		Locale primaryLocale = supportedLocaleService.getPrimaryLocale();
		Translation t = descriptionDao.getTranslationForLocaleOrNull(description, primaryLocale);
		if (t == null)
			return "";
		return t.getTranslation();
	}

	/*
	 * Returns Description for a specific primary key, or null if not defined
	 */
	@Override
	public Description findDescription(int id) {
		return descriptionDao.find(id);
	}

	@Override
	public List<InstrumentModelDescriptionJsonDto> findDescriptionsForFamily(int familyid, Locale searchLanguage) {
		return descriptionDao.findDescriptionsForFamily(familyid, searchLanguage);
	}

	/*
	 * Performs a search using the specified criteria
	 */
	@Override
	public List<Description> performQueryFragment(String descriptionFragment, Locale searchLanguage, int firstResult,
			int maxResults) {
		List<Description> results = descriptionDao.performQueryFragment(descriptionFragment, searchLanguage,
				firstResult, maxResults);
		return results;
	}

	@Override
	public DescriptionPlantillasDTO getPlantillasDescription(int id) {
		return descriptionDao.getPlantillasDescription(id);
	}

	@Override
	public PagedResultSet<DescriptionPlantillasDTO> getPlantillasDescriptions(Date afterLastModified,
			int resultsPerPage, int currentPage) {
		return descriptionDao.getPlantillasDescriptions(afterLastModified, resultsPerPage, currentPage);
	}

	public List<Description> getDescriptions(String description, Locale searchLanguage) {
		return descriptionDao.getDescriptions(description, searchLanguage);
	}

	/*
	 * Gets a count of total # of results for the specified criteria
	 */
	@Override
	public int getResultCountFragment(String descriptionFragment, Locale searchLanguage) {
		return descriptionDao.getResultCountFragment(descriptionFragment, searchLanguage);
	}

	@Override
	public int getResultCountExact(String descriptionExact, Locale searchLanguage) {
		return descriptionDao.getResultCountExact(descriptionExact, searchLanguage);
	}

	@Override
	public int getResultCountExact(String descriptionExact, Locale searchLanguage, Description exclude) {
		return descriptionDao.getResultCountExact(descriptionExact, searchLanguage, exclude);
	}

	/*
	 * Deletes the supplied description from the database
	 */
	@Override
	public void deleteDescription(Description description) {
		descriptionDao.remove(description);
	}

	@Override
	public Description getByTmlId(Integer tmlSubFamilyId) {
		return descriptionDao.getByTmlId(tmlSubFamilyId);
	}
	
	/*
	 * Returns Description for a specific TMLID, or null if not defined
	 */
	@Override
	@Deprecated
	public Description getTMLDescription(int tmlid) {
		return descriptionDao.getTMLDescription(tmlid);
	}

	/*
	 * Saves a new description to the database (note no update method as detached
	 * instances are not anticipated)
	 */
	@Override
	public void insertTMLDescription(TMLSubFamilyDTO dto) throws Exception {
		descriptionDao.insertTMLDescription(dto);
	}

	/*
	 * Saves a new description to the database (note no update method as detached
	 * instances are not anticipated)
	 */
	@Override
	public void updateTMLDescription(TMLSubFamilyDTO dto) throws Exception {
		descriptionDao.updateTMLDescription(dto);
	}

	/*
	 * Deletes the supplied description from the database
	 */
	@Override
	public void deleteTMLDescription(TMLSubFamilyDTO dto) throws Exception {
		descriptionDao.deleteTMLDescription(dto);
	}

	/*
	 * Performs a search using the description fragment in the specified language
	 */
	@Override
	public List<KeyValueIntegerString> getAllKeyValues(String descriptionFragment, DomainType domainType, Locale searchLanguage,
			int maxResults) {
		return descriptionDao.getAllKeyValues(descriptionFragment, domainType, searchLanguage, maxResults);
	}

	public NewDescriptionDao getDescriptionDao() {
		return descriptionDao;
	}

	public void setDescriptionDao(NewDescriptionDao descriptionDao) {
		this.descriptionDao = descriptionDao;
	}

	public InstrumentModelService getInstrumentModelService() {
		return instrumentModelService;
	}

	public void setInstrumentModelService(InstrumentModelService instrumentModelService) {
		this.instrumentModelService = instrumentModelService;
	}

	public SupportedLocaleService getSupportedLocaleService() {
		return supportedLocaleService;
	}

	public void setSupportedLocaleService(SupportedLocaleService supportedLocaleService) {
		this.supportedLocaleService = supportedLocaleService;
	}

	@Override
	protected BaseDao<Description, Integer> getBaseDao() {
		return this.descriptionDao;
	}
}