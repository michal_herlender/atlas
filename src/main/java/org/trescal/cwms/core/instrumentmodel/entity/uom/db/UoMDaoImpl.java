package org.trescal.cwms.core.instrumentmodel.entity.uom.db;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange_;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM_;
import org.trescal.cwms.core.referential.db.TMLLocaleResolver;
import org.trescal.cwms.core.referential.dto.TMLUnitDTO;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Repository("UoMDao")
public class UoMDaoImpl extends BaseDaoImpl<UoM, Integer> implements UoMDao {
	
	private Log logger = LogFactory.getLog(getClass());
	
	@Override
	protected Class<UoM> getEntity() {
		return UoM.class;
	}
	
	public List<UoM> getAllUoMsByName() {
		return getResultList(cb -> {
			CriteriaQuery<UoM> cq = cb.createQuery(UoM.class);
			Root<UoM> root = cq.from(UoM.class);
			
			cq.orderBy(cb.asc(root.get(UoM_.name)));
			
			return cq;
		});
	}
	
	public UoM findDefaultUoM() {
		return getFirstResult(cb -> {
			CriteriaQuery<UoM> cq = cb.createQuery(UoM.class);
			Root<UoM> root = cq.from(UoM.class);
			
			cq.where(cb.and(cb.equal(root.get(UoM_.symbol), ""),
					        cb.equal(root.get(UoM_.name), "")));
			
			return cq;
		}).orElse(null);
	}
	
	@Override
	public UoM findUoMbySymbol(String symbol) {
		Criteria criteria = getSession()
				.createCriteria(UoM.class).
				add(Restrictions.sqlRestriction( "shortName = ? collate Latin1_General_CS_AS", symbol, new StringType())); 
		return   (UoM) criteria.uniqueResult();
	}
	
	@Override
	public UoM getTMLUoM(int tmlid) {
		// TODO Auto-generated method stub
		return getFirstResult(cb -> {
			CriteriaQuery<UoM> cq = cb.createQuery(UoM.class);
			Root<UoM> root = cq.from(UoM.class);
			
			cq.where(cb.equal(root.get(UoM_.tmlid), tmlid));
			
			return cq;
		}).orElse(null);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void insertTMLUoM(TMLUnitDTO dto) throws Exception {
		// First try to find if a unit already exists with the same symbol and not linked to TML
		Criteria criteria = getSession()
				.createCriteria(UoM.class)
				.add(Restrictions.sqlRestriction( "shortName = ? collate Latin1_General_CS_AS", dto.getSymbol(), new StringType())); 
		List<UoM> existingUoMList = criteria.list();
		UoM existingUoM = null;
		if (existingUoMList != null && existingUoMList.size() > 0)
			for (UoM uom : existingUoMList) {
				if ((uom.getTmlid() == null) || (uom.getTmlid() != null && uom.getTmlid() != dto.getTMLID())) {
					existingUoM = uom;
					break;
				}
			}
		if (existingUoM == null) {
			// If no unit found, do the insert
			UoM uom = new UoM();
			uom.setTmlid(dto.getTMLID());
			uom.setName(dto.getDefaultName());
			uom.setSymbol(dto.getSymbol());
			uom.setShortName(dto.getSymbol());
			uom.setActive(dto.isIsEnabled());
			
			SortedSet<Translation> newTranslations = new TreeSet<Translation>();
			
			Iterator<String> iterator = dto.getRegionalNames().keySet().iterator();
			
			while(iterator.hasNext()) 
			{
				String lang = iterator.next();
				String value = dto.getRegionalNames().get(lang);		
				if (TMLLocaleResolver.isResolvable(lang)) {
					Locale locale = TMLLocaleResolver.resolveLocale(lang); 
					Translation t = new Translation(locale,!value.equals(null) ? value.toString() : "");
					newTranslations.add(t);
				}
			}
			
			uom.setNameTranslation(newTranslations);

			logger.debug("Inserting: " + uom.toFullString());
			this.persist(uom);
			// Add this to avoid locks
			logger.debug("Inserted: " + uom.toFullString());
		}
		else
		{
			if (existingUoM.getTmlid() != null && !existingUoM.getTmlid().equals(dto.getTMLID()))
			{
				String errorMessage = MessageFormat.format("Error while inserting, the unit {0} already exists with a different TMLID.",dto.getDefaultName());
				logger.error(errorMessage);
				throw new Exception(errorMessage);
			}
			else				
			{
				if (existingUoM.getTmlid() == null)
				{
					existingUoM.setTmlid(dto.getTMLID());
					this.merge(existingUoM);
					// Add this to avoid locks
				}
				
				if (dto.isIsEnabled())
				{
					this.updateTMLUoM(dto);
				}
				else
				{
					this.deleteTMLUoM(dto);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void updateTMLUoM(TMLUnitDTO dto) throws Exception {
		UoM uom = this.getTMLUoM(dto.getTMLID());
		
		// First try to find if a unit already exists with the same symbol and not linked to TML
		Criteria criteria = getSession()
				.createCriteria(UoM.class)
				.add(Restrictions.ne("id", uom.getId()))
				.add(Restrictions.sqlRestriction( "shortName = ? collate Latin1_General_CS_AS", dto.getSymbol(), new StringType())); 
		
		List<UoM> existingUoMList = criteria.list();
		
		UoM existingUoM = null;
		
		if (existingUoMList != null && existingUoMList.size() > 0)
		{
			for (UoM uomFound : existingUoMList) {
				if ((uomFound.getTmlid() == null) || (uomFound.getTmlid() != null && uomFound.getTmlid() != dto.getTMLID()))
				{
					existingUoM = uomFound;
					break;
				}
			}
		}	
		
		if (existingUoM == null)
		{
			uom.setTmlid(dto.getTMLID());
			uom.setName(dto.getDefaultName());
			uom.setSymbol(dto.getSymbol());
			uom.setShortName(dto.getSymbol());
			uom.setActive(dto.isIsEnabled());
			
			SortedSet<Translation> newTranslations = new TreeSet<Translation>();
			
			Iterator<String> iterator = dto.getRegionalNames().keySet().iterator();
			
			while(iterator.hasNext()) 
			{
				String lang = iterator.next();
				String value = dto.getRegionalNames().get(lang);	
				if (TMLLocaleResolver.isResolvable(lang)) {
					Locale locale = TMLLocaleResolver.resolveLocale(lang); 
					Translation t = new Translation(locale,!value.equals(null) ? value.toString() : "");
					newTranslations.add(t);
				}
			}
			
			uom.setNameTranslation(newTranslations);

			logger.debug("Updating: " + uom.toFullString());
			this.merge(uom);
			// Add this to avoid locks
			logger.debug("Updated: " + uom.toFullString());
		}
		else
		{
			String errorMessage = MessageFormat.format("Error while updating, the unit {0} already exists with a different TMLID.",dto.getDefaultName());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		}
	}

	@Override
	public void deleteTMLUoM(TMLUnitDTO dto) throws Exception {
		UoM uom = this.getTMLUoM(dto.getTMLID());
		
		if (uom != null)
		{
			// Check if this domain is already used by active instrument
			Integer count = getCount(cb -> cq -> {
				Root<ModelRange> root = cq.from(ModelRange.class);
				Join<ModelRange, UoM> uomJoin = root.join(ModelRange_.uom);
				
				cq.where(cb.equal(uomJoin.get(UoM_.tmlid), dto.getTMLID()));
				
				return Triple.of(root, null, null);
			});
			
			if (count <= 0)
			{
				uom.setTmlid(dto.getTMLID());
				uom.setName(dto.getDefaultName());
				uom.setActive(dto.isIsEnabled());
				uom.setSymbol(dto.getSymbol());
				uom.setShortName(dto.getSymbol());
				
				logger.debug("Deleting: " + uom.toFullString());
				this.merge(uom);
				// Add this to avoid locks
				logger.debug("Deleting: " + uom.toFullString());
			}
			else				
			{
				String errorMessage = MessageFormat.format("Error while deleting, the unit {0} is used by at least 1 active instrument model.",dto.getDefaultName());
				logger.error(errorMessage);
				throw new Exception(errorMessage);
			}
		}
	}
}