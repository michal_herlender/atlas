package org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
@Entity
@Table(name = "modelrangecharacteristiclibrary")
public class ModelRangeCharacteristicLibrary
{
	private int modelrangecharacteristiclibraryid;
	private CharacteristicLibrary characteristicLibrary;
	private InstrumentModel model;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getModelrangecharacteristiclibraryid() {
		return modelrangecharacteristiclibraryid;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "characteristiclibraryid", nullable = false)
	public CharacteristicLibrary getCharacteristicLibrary() {
		return characteristicLibrary;
	}
		
	@ManyToOne
	@JoinColumn(name = "modelid")
	public InstrumentModel getModel() {
		return model;
	}
	
	public void setModelrangecharacteristiclibraryid(
			int modelrangecharacteristiclibraryid) {
		this.modelrangecharacteristiclibraryid = modelrangecharacteristiclibraryid;
	}
	
	public void setCharacteristicLibrary(
			CharacteristicLibrary characteristicLibrary) {
		this.characteristicLibrary = characteristicLibrary;
	}
	
	public void setModel(InstrumentModel model) {
		this.model = model;
	}
}