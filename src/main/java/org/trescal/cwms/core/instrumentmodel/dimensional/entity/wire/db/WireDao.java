package org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.Wire;

public interface WireDao extends BaseDao<Wire, Integer>
{
}