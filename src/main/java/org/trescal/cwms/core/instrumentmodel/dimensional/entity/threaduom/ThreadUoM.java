package org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;

@Entity
@Table(name = "dimensional_threaduom")
public class ThreadUoM
{
	private int id;
	private String description;
	private Set<Thread> threads;

	@NotNull
	@Length(min = 1, max = 50)
	@Column(name = "description", nullable = false, length = 50, unique = true)
	public String getDescription()
	{
		return this.description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "uom")
	public Set<Thread> getThreads()
	{
		return this.threads;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setThreads(Set<Thread> threads)
	{
		this.threads = threads;
	}
}