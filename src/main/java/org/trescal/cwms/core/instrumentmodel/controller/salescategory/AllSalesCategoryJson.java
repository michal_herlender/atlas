package org.trescal.cwms.core.instrumentmodel.controller.salescategory;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.db.SalesCategoryService;
import org.trescal.cwms.spring.model.SalesCategoryJsonDTO;

@Controller @JsonController
public class AllSalesCategoryJson {
	@Autowired
	private SalesCategoryService salesCategoryServ;
	
	@RequestMapping(value="/allsalescategories.json", method=RequestMethod.GET)
	public @ResponseBody List<SalesCategoryJsonDTO> getAllSalesCategories(Locale userLocale) {
		return  salesCategoryServ.getAllSalesCategories(userLocale);
	}
	
}
