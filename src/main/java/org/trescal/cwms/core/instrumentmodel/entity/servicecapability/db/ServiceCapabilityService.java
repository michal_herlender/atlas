package org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db;

import java.util.List;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.tuple.Triple;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrumentmodel.dto.ServiceCapabilityForm;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

public interface ServiceCapabilityService extends BaseService<ServiceCapability, Integer> {

	ServiceCapability getCalService(InstrumentModel model, CalibrationType calType, Subdiv subDiv);

	/**
	 * All parameters except modelId are optional and may be null to specify no search
	 * @param modelId
	 * @param serviceTypeId
	 * @param costType
	 * @param businessSubdivId
	 * @return
	 */
	List<ServiceCapability> find(Integer modelId, Integer serviceTypeId, CostType costType, Integer businessSubdivId);

	boolean exists(Integer modelId, CostType costType, Integer serviceTypeId,
			Integer subdivId);

	public List<Triple<Integer, Integer, ServiceCapability>> getCalServiceCapabilities(
			MultiValuedMap<Integer, Integer> instrumentModelCaltype, Integer businessSubdivId);

	ServiceCapability getCalService(int modelid, Integer serviceTypeId, Integer businessSubdivId);
	
	void createServiceCapability(ServiceCapabilityForm dto);
	
	void updateServiceCapability(ServiceCapabilityForm dto);
	
}
