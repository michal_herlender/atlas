package org.trescal.cwms.core.instrumentmodel.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ModelComponentSearchValidator implements Validator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.isAssignableFrom(ModelComponentSearchForm.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		ModelComponentSearchForm form = (ModelComponentSearchForm) target;

		if ((form.getMfrid() != null) && !form.getMfrid().equals(""))
		{
			try
			{
				Integer.parseInt(form.getMfrid());
			}
			catch (NumberFormatException e)
			{
				errors.rejectValue("mfrid", "errors.invalid.mfr", null, "An invalid mfr was selected");
			}
		}

		if ((form.getDescid() != null) && !form.getDescid().equals(""))
		{
			try
			{
				Integer.parseInt(form.getDescid());
			}
			catch (NumberFormatException e)
			{
				errors.rejectValue("descid", "errors.invalid.desc", null, "An invalid description was selected");
			}
		}
	}
}
