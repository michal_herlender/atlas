package org.trescal.cwms.core.instrumentmodel.controller.services;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrumentmodel.dto.ServiceCapabilityDTO;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db.ServiceCapabilityService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Controller
@JsonController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE})
public class InitializeServicesController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private InstrumentModelService instrumentModelService;
    @Autowired
    private ServiceCapabilityService capabilityService;
    @Autowired
    private MessageSource messageSource;

    private ServiceCapability cloneCapability(ServiceCapability capability) {
        ServiceCapability newCapability = new ServiceCapability();
        newCapability.setCalibrationProcess(capability.getCalibrationProcess());
        newCapability.setCalibrationType(capability.getCalibrationType());
        newCapability.setChecksheet(capability.getChecksheet());
        newCapability.setCostType(capability.getCostType());
        newCapability.setOrganisation(capability.getOrganisation());
        newCapability.setCapability(capability.getCapability());
        newCapability.setWorkInstruction(capability.getWorkInstruction());
        return newCapability;
    }
	
	@RequestMapping(value="/initializeinstrumentmodelservices.json", method=RequestMethod.GET)
	public Set<ServiceCapabilityDTO> initializeServices(
			@RequestParam(value="modelId", required=false, defaultValue="0") Integer modelId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE) Locale defaultLocale,
			Locale locale) throws Exception
	{
		InstrumentModel instrumentModel = instrumentModelService.findLazyInstrumentModel(modelId);
		try {
			InstrumentModel capabilityCategoryModel = instrumentModelService.findCapabilityModel(instrumentModel.getDescription());
			if(capabilityCategoryModel == null) {
				String message = "There exist no corresponding instrument model of type 'capability' (subfamily id: " + instrumentModel.getDescription().getId() + ").";
				logger.error(message);
				throw new Exception(message);
			}
			else {
				Set<ServiceCapabilityDTO> newCapabilities = new HashSet<ServiceCapabilityDTO>();
				if(instrumentModel.getServiceCapabilities() == null)
					instrumentModel.setServiceCapabilities(new HashSet<ServiceCapability>());
				for(ServiceCapability capability : capabilityCategoryModel.getServiceCapabilities())
					if(capability.getOrganisation().getSubdivid() == subdivDto.getKey()) {
						ServiceCapability newCapability = cloneCapability(capability);
						newCapability.setInstrumentModel(instrumentModel);
						capabilityService.save(newCapability);
						instrumentModel.getServiceCapabilities().add(newCapability);
						newCapabilities.add(new ServiceCapabilityDTO(subdivDto.getValue(), newCapability, locale, defaultLocale));
					}
				return newCapabilities;
			}
		}
		catch(HibernateException ex) {
			String message = "Corresponding instrument model of type 'capability' is not unique (subfamily id: " + instrumentModel.getDescription().getId() + ").";
			logger.error(message);
			throw new Exception(message);
		}
	}
	
	@RequestMapping(value="/copyinstrumentmodelservices.json", method=RequestMethod.GET)
	@ResponseBody
	public String copyServices(
			@RequestParam(value="modelId", required=false, defaultValue="0") Integer modelId,
			@RequestParam(value="replace", required=false, defaultValue="false") Boolean replace,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			Locale locale)
	{
		Integer updatedModelCounter = 0;
		InstrumentModel capabilityModel = instrumentModelService.findLazyInstrumentModel(modelId);
		Set<ServiceCapability> capabilities = capabilityModel.getServiceCapabilities().stream()
				.filter(sc -> sc.getOrganisation().getSubdivid() == subdivDto.getKey()).collect(Collectors.toSet());
		if(capabilities.size() > 0) {
			List<InstrumentModel> instrumentModels = instrumentModelService.findInstrumentModel(capabilityModel.getDescription());
			Predicate<ServiceCapability> capabilityOfSubdiv = sc -> sc.getOrganisation().getSubdivid() == subdivDto.getKey();
			for(InstrumentModel instrumentModel : instrumentModels) {
				InstrumentModelType modelType = instrumentModel.getModelType();
				if(!(instrumentModel.getQuarantined() || modelType.getCapability())) {
					if(replace) {
						// delete old capabilities
						instrumentModel.getServiceCapabilities().stream().filter(capabilityOfSubdiv)
						.forEach(sc -> capabilityService.delete(sc));
						instrumentModel.getServiceCapabilities().removeIf(capabilityOfSubdiv);
					}
					if(!instrumentModel.getServiceCapabilities().stream().anyMatch(capabilityOfSubdiv)) {
						updatedModelCounter++;
						for(ServiceCapability capability : capabilities) {
							ServiceCapability newCapability = cloneCapability(capability);
							newCapability.setInstrumentModel(instrumentModel);
							capabilityService.save(newCapability);
							instrumentModel.getServiceCapabilities().add(newCapability);
						}
					}
				}
			}
		}
		Object[] args = new Object[] { updatedModelCounter };
		return messageSource.getMessage("viewmod.copiedServices", args, locale);
	}
}