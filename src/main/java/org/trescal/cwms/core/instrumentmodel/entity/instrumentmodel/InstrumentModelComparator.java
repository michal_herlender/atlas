/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel;

import java.util.Comparator;

import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;

/**
 * Custom comparator that sorts {@link InstrumentModel} entities alphabetically
 * by {@link Mfr}.name, {@link InstrumentModel}.model and finally
 * {@link Description}.desc
 * 
 * @author richard
 */
public class InstrumentModelComparator implements Comparator<InstrumentModel>
{
	@Override
	public int compare(InstrumentModel o1, InstrumentModel o2)
	{
		if ((o1.getMfr() != null)
				&& (o2.getMfr() != null)
				&& !o1.getMfr().getName().equalsIgnoreCase(o2.getMfr().getName()))
		{
			return o1.getMfr().getName().compareToIgnoreCase((o2.getMfr().getName()));
		}
		else if (!o1.getModel().equalsIgnoreCase(o2.getModel()))
		{
			return o1.getModel().compareToIgnoreCase(o2.getModel());
		}
		else if (!o1.getDescription().getDescription().equalsIgnoreCase(o2.getDescription().getDescription()))
		{
			return o1.getDescription().getDescription().compareToIgnoreCase(o2.getDescription().getDescription());
		}
		else
		{
			return ((Integer) o1.getModelid()).compareTo(o2.getModelid());
		}

	}
}
