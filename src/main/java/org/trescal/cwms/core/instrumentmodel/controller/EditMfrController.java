/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.instrumentmodel.form.genericentityvalidator.MfrValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;

/**
 * Displays a page showing information about a {@link Mfr} and allowing editing
 * of basic information.
 */
@Controller
@SessionAttributes(Constants.HTTPSESS_NEW_FILE_LIST)
public class EditMfrController
{
	protected final Log logger = LogFactory.getLog(getClass());
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private MfrService mfrServ;
	@Autowired
	private SystemComponentService systemCompServ;

	@InitBinder("command")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new MfrValidator());
    }
	
	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
		return new ArrayList<String>();
	}
	
	@ModelAttribute("command")
	protected Mfr formBackingObject(
			@RequestParam(value="mfrid", required=false, defaultValue="0") Integer mfrid) throws Exception
	{
		Mfr mfr = this.mfrServ.findMfr(mfrid);
		if ((mfr == null) || (mfrid == 0)) {
			this.logger.error("Mfr with id '" + mfrid + "' not found");
			throw new Exception("Requested manufacturer not found");
		}
		else if (mfr.getGenericMfr()) {
			this.logger.error("Attempt to edit generic mfr '" + mfrid + "'");
			throw new Exception("Illegal attempt to edit system mfr");
		}
		else {
			return mfr;
		}
	}
	
	@RequestMapping(value="/editmfr.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(@ModelAttribute("command") Mfr mfr) throws Exception
	{
		this.mfrServ.updateMfr(mfr);
		return new ModelAndView(new RedirectView("/editmfr.htm?mfrid="+ mfr.getMfrid(), true));
	}

	@RequestMapping(value="/editmfr.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@ModelAttribute Mfr mfr) throws Exception
	{
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("instruments", null);
		// add the SystemComponent entry for InstrumentModels for file upload
		// purposes
		FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ.getFilesForComponentRoot(Component.MFR, mfr.getMfrid(), newFiles);
		model.put(Constants.REFDATA_SYSTEM_COMPONENT, this.systemCompServ.findComponent(Component.MFR));
		model.put(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
		model.put(Constants.REFDATA_FILES_NAME, fileBrowserServ.getFilesName(fileBrowserWrapper));
		return new ModelAndView("trescal/core/instrumentmodel/editmfr", model);
	}
}
