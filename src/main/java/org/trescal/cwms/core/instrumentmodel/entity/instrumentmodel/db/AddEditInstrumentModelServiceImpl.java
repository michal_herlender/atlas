package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.instrumentmodel.RangeType;
import org.trescal.cwms.core.instrumentmodel.controller.configuration.EditModelCharacteristicDto;
import org.trescal.cwms.core.instrumentmodel.controller.configuration.EditModelCharacteristicForm;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicType;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.db.CharacteristicDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.db.CharacteristicLibraryService;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.db.InstrumentModelTypeService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.db.ModelRangeService;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.ModelRangeCharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.db.ModelRangeCharacteristicLibraryService;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.db.SalesCategoryService;
import org.trescal.cwms.core.instrumentmodel.entity.uom.db.UoMService;
import org.trescal.cwms.core.instrumentmodel.form.EditModelForm;
import org.trescal.cwms.core.login.entity.user.db.UserService;

@Service
public class AddEditInstrumentModelServiceImpl implements AddEditInstrumentModelService {

	@Autowired
	private CharacteristicDescriptionService cdService;
	@Autowired
	private CharacteristicLibraryService clService;
	@Autowired
	private NewDescriptionService descriptionService;
	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private InstrumentModelTypeService instModelTypeService;
	@Autowired
	private MfrService mfrService;
	@Autowired
	private ModelRangeCharacteristicLibraryService mrclService;
	@Autowired
	private ModelRangeService modelRangeService; 
	@Autowired
	private SalesCategoryService salesCategoryService;
	@Autowired
	private UserService userService;
	@Autowired
	private UoMService uomService;
	@Value("${cwms.config.model.default.genericmodelname}")
	private String genericModelName;
	
	private static final Logger logger = LoggerFactory.getLogger(AddEditInstrumentModelServiceImpl.class);

	@Override
	public Integer addEditInstrumentModel(EditModelForm form, String username) {
		if (form.getModelid() == null) {
			throw new IllegalArgumentException("modelid was null");
		}
		InstrumentModel model = null;
		if (form.getModelid() == 0) {
			model = new InstrumentModel();
			model.setAddedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			model.setAddedBy(this.userService.get(username).getCon());
			model.setNameTranslations(new HashSet<>());
			model.setLibraryValues(new ArrayList<>());
		} else {
			model = this.instrumentModelService.get(form.getModelid());
			if (model == null) {
				throw new RuntimeException("No model found for id "+form.getModelid());
			}
			// Should be also checked in validator / controller / view logic
			if (model.getTmlid() != null && model.getTmlid() != 0) {
				throw new UnsupportedOperationException("TML models are not allowed to be edited in Atlas"); 
			}
		}
		// is this mfr specific (i.e mfr required)
		if (form.getModelMfrType().isMfrRequiredForModel()) {
			if (form.getMfrid() == null) {
				throw new IllegalArgumentException("Manufacturer must be specified for this model type");
			}
			Mfr mfr = this.mfrService.findMfr(form.getMfrid());
			model.setMfr(mfr);
		} else {
			// set the mfr to be the system generic mfr
			model.setMfr(this.mfrService.getSystemGenericMfr());
		}
		model.setDescription(this.descriptionService.findDescription(form.getDescid()));
		model.setModelMfrType(form.getModelMfrType());
		model.setModel(form.getModelName().isEmpty() ? genericModelName : form.getModelName());
		if (model.getModelType() == null || model.getModelType().getCanSelect()) {
			// Only "can select" instrument model types can be changed to each other; 
			// others fixed once set; this is checked in validator...
			model.setModelType(this.instModelTypeService.get(form.getInstModelTypeId()));
		}
		model.setQuarantined(form.getQuarantined());
		this.instrumentModelService.regenerateInstrumentModelTranslations(model);
		
		if (!model.getModelType().getSalescategory()) {
			// Sales category is "set" from choices
			model.setSalesCategory(form.getSalesCategoryId() != null ? 
					this.salesCategoryService.getSalesCategory(form.getSalesCategoryId()) : null);
		}
		else {
			// Sales category is generated / updated for models of "sales category" type
			addEditSalesCategory(model);
		}

		if (form.getModelid() == 0) {
			this.instrumentModelService.save(model);
		}
		return model.getModelid();
	}
	
	/**
	 * Internally called to maintain sales category definitions
	 */
	private void addEditSalesCategory(InstrumentModel instModel) {
		SalesCategory salesCategory = instModel.getSalesCategory();
		if (salesCategory != null) {
			// Model has existing sales category
			this.salesCategoryService.regenerateSalesCategoryTranslations(salesCategory, instModel);
		}
		else {
			// Model should get new sales category
			salesCategory = new SalesCategory();
			salesCategory.setActive(true);
			salesCategory.setTranslations(new HashSet<>());
			this.salesCategoryService.regenerateSalesCategoryTranslations(salesCategory, instModel);
			instModel.setSalesCategory(salesCategory);
			this.salesCategoryService.save(salesCategory);
		}
	}
	
	/*
	 * TODO - LIBRARY values just need a library value set on instrument model, no model range
	 */
	@Override
	public void editCharacteristics(EditModelCharacteristicForm form) {
		// Note, at present time options are non-editable and it is assumed that there are no options on model
		// for demo / testing purposes only on non-TML models
		// We also support selection of a single library value per model range only.
		// Multiple library values only on sub-family definition.
		InstrumentModel instModel = this.instrumentModelService.get(form.getModelId());
		if (instModel.getTmlid() != null && instModel.getTmlid() != 0) throw new UnsupportedOperationException("TML models may not have their characteristics edited");
		
		Map<Integer, EditModelCharacteristicDto> dtoMap = new HashMap<>(); 
		form.getCharacteristics().forEach(dto -> dtoMap.put(dto.getCharacteristicDescriptionId(), dto));
		logger.debug("Processing "+dtoMap.size()+" input DTOs");
		
		// Perform edits, deletes to model ranges
		List<ModelRange> modelRanges = new ArrayList<>(instModel.getRanges());
		logger.debug("Scanning "+modelRanges.size()+" existing ModelRanges");

		for (ModelRange mr : modelRanges) {
			CharacteristicDescription cd = mr.getCharacteristicDescription();
			EditModelCharacteristicDto dto = dtoMap.get(cd.getCharacteristicDescriptionId());
			if (dto == null || !dto.getIncluded()) {
				logger.debug("Removing ModelRange id "+mr.getId()+" for CharacteristicDescription id "+cd.getCharacteristicDescriptionId());
				// Remove model range from model and delete model range
				instModel.getRanges().remove(mr);
				this.modelRangeService.deleteModelRange(mr);
			}
			else {
				// Update existing model range
				logger.debug("Updating ModelRange id "+mr.getId()+" for CharacteristicDescription id "+cd.getCharacteristicDescriptionId());
				updateModelRangeFields(mr, cd, dto);
				dtoMap.remove(dto.getCharacteristicDescriptionId());
			}
		}
		
		// Perform edits, deletes to library values
		List<ModelRangeCharacteristicLibrary> libraryValues = new ArrayList<>(instModel.getLibraryValues());
		logger.debug("Scanning "+libraryValues.size()+" existing library values");
		
		for (ModelRangeCharacteristicLibrary mrcl : libraryValues) {
			int cdId = mrcl.getCharacteristicLibrary().getCharacteristicDescription().getCharacteristicDescriptionId();
			int clId = mrcl.getCharacteristicLibrary().getCharacteristiclibraryid();
			EditModelCharacteristicDto dto = dtoMap.get(cdId);
			if (dto == null || !dto.getIncluded()) {
				logger.debug("Removing MRCL for CharacteristicLibrary id "+clId);
				instModel.getLibraryValues().remove(mrcl);
				mrclService.delete(mrcl);
			}
			else if (dto.getCharacteristicLibraryId() != clId) {
				// Update library value if different
				logger.debug("Updating MRCL from CharacteristicLibrary id "+clId+" to "+dto.getCharacteristicLibraryId());
				CharacteristicLibrary cl = this.clService.get(dto.getCharacteristicLibraryId());
				mrcl.setCharacteristicLibrary(cl);
				dtoMap.remove(dto.getCharacteristicDescriptionId());
			}
		}
		
		// Perform adds to library values
		// Perform adds to model ranges

		logger.debug("Scanning "+dtoMap.size()+" remaining DTOs");
		for (EditModelCharacteristicDto dto : dtoMap.values()) {
			if (dto.getIncluded()) {
				CharacteristicDescription cd = this.cdService.get(dto.getCharacteristicDescriptionId());
				if (cd.getCharacteristicType().equals(CharacteristicType.LIBRARY)) {
					// Insert library value
					logger.debug("Adding new Library Value for CharacteristicDescription id "+dto.getCharacteristicDescriptionId());
					insertLibraryValue(instModel, dto);
				}
				else {
					// Add new model range
					logger.debug("Adding new ModelRange for CharacteristicDescription id "+dto.getCharacteristicDescriptionId());
					ModelRange mr = new ModelRange();
					mr.setModel(instModel);
					mr.setCharacteristicDescription(cd);
					mr.setCharacteristicType(RangeType.CHARACTERISTIC);
					updateModelRangeFields(mr, cd, dto);
					// Saved by JPA Cascade
					instModel.getRanges().add(mr);
				}
			}
		}
		
		// Update model translations - key values potentially affect model name
		// If it's a sales category model, regenerate the sales category name and translations, 
		// based on the new inst model definition
		if (instModel.getModelType().getSalescategory() && instModel.getSalesCategory() != null) {
			// Prevent inst model generation from using sales category as its definition
			SalesCategory salesCategory = instModel.getSalesCategory();
			instModel.setSalesCategory(null);
			this.instrumentModelService.regenerateInstrumentModelTranslations(instModel);
			this.salesCategoryService.regenerateSalesCategoryTranslations(salesCategory, instModel);
			instModel.setSalesCategory(salesCategory);
		}
		else {
			this.instrumentModelService.regenerateInstrumentModelTranslations(instModel);
		}
	}

	private void insertLibraryValue(InstrumentModel instModel, EditModelCharacteristicDto dto) {
		CharacteristicLibrary cl = this.clService.get(dto.getCharacteristicLibraryId()); 
		
		ModelRangeCharacteristicLibrary mrcl = new ModelRangeCharacteristicLibrary();
		mrcl.setModel(instModel);
		mrcl.setCharacteristicLibrary(cl);
		// Saved by JPA Cascade
		instModel.getLibraryValues().add(mrcl);
	}
	
	private void updateModelRangeFields(ModelRange mr, CharacteristicDescription cd, EditModelCharacteristicDto dto) {
		mr.setEnd(dto.getDecimalEnd());
		mr.setMaxIsInfinite(false);
		mr.setStart(dto.getDecimalStart());
		mr.setMaxvalue(dto.getTextEnd());
		mr.setMinvalue(dto.getTextStart());
		if (dto.getUomId() != null) {
			mr.setUom(this.uomService.findUoM(dto.getUomId()));
		}
		else {
			mr.setUom(this.uomService.findDefaultUoM());
		}
	}

}