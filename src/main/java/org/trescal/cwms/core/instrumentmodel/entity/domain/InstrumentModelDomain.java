package org.trescal.cwms.core.instrumentmodel.entity.domain;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "instmodeldomain")
public class InstrumentModelDomain extends Versioned {
	
	private int domainid;
	private String name;
	private Set<Translation> translation;
	private Boolean active;
	private Integer tmlid;
	private DomainType domainType;
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="instmodeldomaintranslation", joinColumns=@JoinColumn(name="domainid"))
	public Set<Translation> getTranslation() {
		return translation;
	}
	
	public void setTranslation(Set<Translation> translations) {
		this.translation = translations;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getDomainid() {
		return domainid;
	}
	
	public void setDomainid(int familyid) {
		this.domainid = familyid;
	}
	
	@Column(nullable=false, unique=true)
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Boolean getActive() {
		return active;
	}
	
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	@Column(nullable=true)
	public Integer getTmlid() {
		return tmlid;
	}
	
	public void setTmlid(Integer tmlid) {
		this.tmlid = tmlid;
	}
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="domaintype")
	public DomainType getDomainType() {
		return domainType;
	}
	
	public void setDomainType(DomainType domainType) {
		this.domainType = domainType;
	}
	
	@Override
	public String toString() {
		return "InstrumentModelDomain [domainid=" + domainid + ", name=" + name
				+ ", translation=" + translation + ", active=" + active
				+ ", tmlid=" + tmlid + "]";
	}
	
	@Transient
	public String toFullString() {
		return "InstrumentModelDomain [domainid=" + domainid + ", name=" + name
				+ ", translation=" + (translation != null ? translation : "") + ", active=" + active
				+ ", tmlid=" + tmlid + "]";
	}
}