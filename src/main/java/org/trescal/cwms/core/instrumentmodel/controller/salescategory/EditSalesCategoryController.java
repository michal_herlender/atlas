package org.trescal.cwms.core.instrumentmodel.controller.salescategory;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.db.SalesCategoryService;
import org.trescal.cwms.core.instrumentmodel.form.EditSalesCategoryForm;
import org.trescal.cwms.core.instrumentmodel.form.EditSalesCategoryValidator;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@Controller @IntranetController
@RequestMapping(value="editsalescategory.htm")
public class EditSalesCategoryController {
	
	@Autowired
	EditSalesCategoryValidator editSalesCategoryValidator;
	
	@Autowired 
	private SalesCategoryService salesCatServ;
	
	@Autowired
	private SupportedLocaleService supportedLocaleService;

	@ModelAttribute("editsalescategoryform")
	public EditSalesCategoryForm createEditSalesCategoryForm() {
		
		EditSalesCategoryForm form = new EditSalesCategoryForm();
		
		SortedSet<Translation> translations = new TreeSet<Translation>();
		for (Locale locale: supportedLocaleService.getSupportedLocales())
			translations.add(new Translation(locale, ""));
		form.setTranslations(translations);
		
		return form;
		
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String displayForm(@ModelAttribute("editsalescategoryform") EditSalesCategoryForm form, @RequestParam(value="id", required=false) Integer id){	
		
		Map<Locale, String> translationMap = new HashMap<Locale, String>();
		
		if(id != null) {
			SalesCategory salesCategory = salesCatServ.getSalesCategory(id);
			form.setSalesCategory(salesCategory);;

			for (Translation t: salesCategory.getTranslations())
				translationMap.put(t.getLocale(), t.getTranslation());
			
			SortedSet<Translation> translations = new TreeSet<Translation>();
			for (Translation ft: form.getTranslations())
				translations.add(new Translation(ft.getLocale(), translationMap.containsKey(ft.getLocale()) ? translationMap.get(ft.getLocale()) : ""));
			form.setTranslations(translations);			
				
		}
		return "trescal/core/instrumentmodel/salescategory/editsalescategory";
	}
	
	@RequestMapping(params = "save", method = RequestMethod.POST)
	public String processFormSave(@Validated @ModelAttribute("editsalescategoryform") EditSalesCategoryForm form, BindingResult result){
		
		editSalesCategoryValidator.validate(form, result);
		if(result.hasErrors()) {
			return "trescal/core/instrumentmodel/salescategory/editsalescategory";
		}
		// Remove any empty translations
		SortedSet<Translation> newTranslations = new TreeSet<Translation>();
		for(Translation t : form.getTranslations())
			if (!t.getTranslation().isEmpty()) newTranslations.add(t);
		form.getSalesCategory().setTranslations(newTranslations);
		this.salesCatServ.merge(form.getSalesCategory());
		return "trescal/core/instrumentmodel/salescategory/viewsalescategory";
	}
	
	@RequestMapping(params = "delete", method = RequestMethod.POST)
	public String processFormDelete(@Validated @ModelAttribute("editsalescategoryform") EditSalesCategoryForm form, BindingResult result){

		// Remove any empty translations from the form
		SortedSet<Translation> newTranslations = new TreeSet<Translation>();
		for(Translation t : form.getTranslations())
			if (!t.getTranslation().isEmpty()) newTranslations.add(t);
		form.getSalesCategory().setTranslations(newTranslations);
		
		this.salesCatServ.delete(form.getSalesCategory());
		
		return "trescal/core/instrumentmodel/salescategory/viewsalescategory";
	}
}