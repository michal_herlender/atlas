package org.trescal.cwms.core.instrumentmodel.controller;

import java.util.Locale;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.system.Constants;

import lombok.Data;

@Controller
public class InstrumentModelCompanyCalReqsController {

	@Autowired
	private InstrumentModelService instrumentModelService;

	@ModelAttribute("form")
	protected ModelcompanycalreqsForm getModelcompanycalreqsForm() {
		return new ModelcompanycalreqsForm();
	}

	@GetMapping("/modelcompanycalreqs.htm")
	public String get() {
		return "trescal/core/instrumentmodel/modelcompanycalreqs";
	}

	@PostMapping("/modelcompanycalreqs.htm")
	public String post(@Valid @ModelAttribute("form") ModelcompanycalreqsForm form, BindingResult bindingResult,
			Model model, Locale locale) {

		if (bindingResult.hasErrors()) {
			return get();
		}

		model.addAttribute("pagedResults",
				instrumentModelService.getInstrumentModelsWithCompanyCalRequirements(form.getCoid(), form.getMfr(),
						form.getModel(), form.getDesc(), Constants.RESULTS_PER_PAGE,
						form.getPageNo() == 0 ? 1 : form.getPageNo(), locale, form.isOnlyWithCalReqs()));
		return "trescal/core/instrumentmodel/modelcompanycalreqs";
	}

	@Data
	public static class ModelcompanycalreqsForm {
		@NotNull
		private Integer coid;
		private String comptext;
		private String mfr;
		private Integer mfrId;
		private String model;
		private Integer descId;
		private String desc;
		private int pageNo;
		private boolean onlyWithCalReqs;
	}

}
