package org.trescal.cwms.core.instrumentmodel.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;

@Controller
public class CharacteristicsController {
	
	@Autowired
	private NewDescriptionService subfamilyService;
	
	@ModelAttribute("characteristics")
	public List<CharacteristicDescription> getCharacteristics(
			@RequestParam(name="subfamilyId", required=true) Integer subfamilyId) {
		Description subfamily = subfamilyService.findDescription(subfamilyId);
		return subfamily.getCharacteristicDescriptions().stream().filter(ch -> ch.getCharacteristicTypeInt() == 6).collect(Collectors.toList());
	}
	
	@RequestMapping(value="/characteristics.json", method=RequestMethod.GET)
	public String onRequest() {
		return "trescal/core/instrumentmodel/characteristics";
	}
}