package org.trescal.cwms.core.instrumentmodel.entity.range.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.range.Range;

@Repository("RangeDao")
public class RangeDaoImpl extends BaseDaoImpl<Range, Integer> implements RangeDao {
	
	@Override
	protected Class<Range> getEntity() {
		return Range.class;
	}
}