package org.trescal.cwms.core.instrumentmodel.entity.modelrange;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ModelRangeValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(ModelRange.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ModelRange modelRange = (ModelRange)target;
		if(modelRange.getEnd() != null && modelRange.getEnd() < modelRange.getStart()){
			errors.reject("error.range.mingreaterthanmax");
		}

	}

}
