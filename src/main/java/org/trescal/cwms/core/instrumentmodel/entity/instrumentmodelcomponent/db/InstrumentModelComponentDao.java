package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent.InstrumentModelComponent;

public interface InstrumentModelComponentDao extends BaseDao<InstrumentModelComponent, Integer> {
	
	List<Component> getAllUnAttachedComponents(int modelid);
	
	List<InstrumentModelComponent> getInstrumentModelComponents(int modelid, int componentid, Integer id);
}