package org.trescal.cwms.core.instrumentmodel.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;
import org.trescal.cwms.core.instrumentmodel.entity.component.db.ComponentService;
import org.trescal.cwms.core.instrumentmodel.form.EditComponentForm;
import org.trescal.cwms.core.instrumentmodel.form.EditComponentValidator;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.system.Constants;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class EditComponentController {
	protected final Log logger = LogFactory.getLog(getClass());
	@Autowired
	private ComponentService compServ;
	@Autowired
	private UserService userService;
	@Autowired
	private EditComponentValidator validator;

	@InitBinder("command")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}
	
	@ModelAttribute("command")
	protected Object formBackingObject(@RequestParam(name="cid", required=false, defaultValue="0") int cid)
	{
		EditComponentForm form = new EditComponentForm();

		if (cid == 0)
		{
			form.setInStock(1);
			form.setDescid(0);
			form.setMfrid(0);
			form.setComponentId(0);
		}
		else
		{
			Component comp = this.compServ.get(cid);
			if (comp == null)
			{
				throw new RuntimeException("Requested model component not found");
			}
			form.setComponentId(cid);
			form.setDescid(comp.getDescription().getId());
			form.setDescName(comp.getDescription().getDescription());
			form.setMfrid(comp.getMfr() != null ? comp.getMfr().getMfrid() : 0);
			form.setMfrName(comp.getMfr() != null ? comp.getMfr().getName() : "");
			form.setInStock(comp.getInStock());
			form.setMinimumStockLevel(comp.getMinimumStockLevel());
			form.setSalesCost(comp.getSalesCost());
			form.setStockLastCheckedOn(comp.getStockLastCheckedOn());
			form.setStockLastCheckedBy(comp.getStockLastCheckedBy() != null ? comp.getStockLastCheckedBy().getName() : "");
		}

		return form;
	}

	@RequestMapping(value="/editmodelcomponent.htm", method=RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/instrumentmodel/editmodelcomponent";
	}
	
	@RequestMapping(value="/editmodelcomponent.htm", method=RequestMethod.POST)
	protected String onSubmit(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("command") @Validated EditComponentForm form, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) {
			return referenceData();
		}
		Contact currentContact = userService.get(username).getCon();
		Component comp = this.compServ.createOrUpdateComponent(form, currentContact);

		return "redirect:editmodelcomponent.htm?cid="+comp.getId();
	}
}
