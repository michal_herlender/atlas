package org.trescal.cwms.core.instrumentmodel.controller.salescategory;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.db.SalesCategoryService;
import org.trescal.cwms.spring.model.LabelIdDTO;

@Controller @JsonController
public class SearchSalesCategoryTags {
	@Autowired
	private SalesCategoryService salesCatServ;
	public static int MAX_RESULTS = 50;

	@RequestMapping(value="/searchsalescategorytags.json", method=RequestMethod.GET)
	public @ResponseBody List<LabelIdDTO> getTagList(@RequestParam("term") String salesCategoryFragment, Locale userLocale) {
		return salesCatServ.getLabelIdList(salesCategoryFragment, userLocale, MAX_RESULTS);
	}
}
