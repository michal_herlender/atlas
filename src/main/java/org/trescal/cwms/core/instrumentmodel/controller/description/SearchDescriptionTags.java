package org.trescal.cwms.core.instrumentmodel.controller.description;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Controller
@JsonController
public class SearchDescriptionTags {

	@Autowired
	private NewDescriptionService descriptionService;

	public static int MAX_RESULTS = 50;

	@RequestMapping(value = "/searchdescriptiontags.json", method = RequestMethod.GET)
	public @ResponseBody List<KeyValueIntegerString> getTagList(
			@RequestParam(name = "domaintype", required = false) DomainType domainType,
			@RequestParam("term") String descriptionFragment, Locale userLocale) {
		return descriptionService.getAllKeyValues(descriptionFragment, domainType, userLocale, MAX_RESULTS);
	}
}