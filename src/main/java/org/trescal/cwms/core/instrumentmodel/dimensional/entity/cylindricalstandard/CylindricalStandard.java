package org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;

@Entity
@Table(name = "dimensional_cylindricalstandard")
public class CylindricalStandard
{
	private Double id;
	private BigDecimal sizeImperial;
	private BigDecimal sizeMetric;
	private Set<Thread> threads;

	@Id
	@Column(name = "id", nullable = false)
	@Type(type = "double")
	public Double getId()
	{
		return this.id;
	}

	@Column(name = "sizeimperial", precision = 20, scale = 5, nullable = true)
	public BigDecimal getSizeImperial()
	{
		return this.sizeImperial;
	}

	@Column(name = "sizemetric", precision = 20, scale = 5, nullable = true)
	public BigDecimal getSizeMetric()
	{
		return this.sizeMetric;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "cylindricalStandard")
	public Set<Thread> getThreads()
	{
		return this.threads;
	}

	public void setId(Double id)
	{
		this.id = id;
	}

	public void setSizeImperial(BigDecimal sizeImperial)
	{
		this.sizeImperial = sizeImperial;
	}

	public void setSizeMetric(BigDecimal sizeMetric)
	{
		this.sizeMetric = sizeMetric;
	}

	public void setThreads(Set<Thread> threads)
	{
		this.threads = threads;
	}
}