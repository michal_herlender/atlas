package org.trescal.cwms.core.instrumentmodel.dimensional.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.CylindricalStandard;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.db.CylindricalStandardService;
import org.trescal.cwms.core.instrumentmodel.dimensional.form.CylindricalStandardForm;

@Controller
public class CylindricalStandardController
{
	@Autowired
	private CylindricalStandardService cylStanServ;

	@ModelAttribute("command")
	protected CylindricalStandardForm formBackingObject(@RequestParam(name="id", required=false, defaultValue="0") double id) throws Exception
	{
		CylindricalStandard cs = this.cylStanServ.get(id);

		CylindricalStandardForm form = new CylindricalStandardForm();
		if ((id == 0) || (cs == null))
		{
			form.setCreate(true);
			cs = new CylindricalStandard();
		}
		else
		{
			form.setCreate(false);
		}
		form.setCs(cs);
		return form;
	}

	@RequestMapping(value="/cylindricalstandard.htm", method=RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/instrumentmodel/dimensional/cylindricalstandard";
	}
	
	@RequestMapping(value="/cylindricalstandard.htm", method=RequestMethod.POST)
	protected String onSubmit(@ModelAttribute("command") CylindricalStandardForm form) throws Exception
	{
		if (form.getCs().getId() == 0) {
			this.cylStanServ.save(form.getCs());
		}
		else {
			this.cylStanServ.merge(form.getCs());
		}
		

		return "redirect:cylindricalstandard.htm?id="+form.getCs().getId();
	}
}
