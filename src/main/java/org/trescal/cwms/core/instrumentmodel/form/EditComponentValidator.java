package org.trescal.cwms.core.instrumentmodel.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.instrumentmodel.entity.component.db.ComponentService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class EditComponentValidator extends AbstractBeanValidator
{
	@Autowired
	private ComponentService compServ;

	public boolean supports(Class<?> clazz)
	{
		return clazz.isAssignableFrom(EditComponentForm.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		super.validate(target, errors);
		EditComponentForm form = (EditComponentForm) target;

		if ((form.getMfrid() != null) && (form.getDescid() != null))
		{
			if (this.compServ.getComponents(form.getMfrid(), form.getDescid(), form.getComponentId()).size() > 0)
			{
				errors.rejectValue("", "error.duplicatemfrdesc", null, "A component with this mfr and description already exists.");
			}
		}
	}
}
