package org.trescal.cwms.core.instrumentmodel.entity.componentdescription.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.dto.CompDescSearchResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.componentdescription.ComponentDescription;

@Repository("ComponentDescriptionDao")
public class ComponentDescriptionDaoImpl extends BaseDaoImpl<ComponentDescription, Integer> implements ComponentDescriptionDao {
	
	@Override
	protected Class<ComponentDescription> getEntity() {
		return ComponentDescription.class;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ComponentDescription> findComponentDescription(String nameFragment, Integer descriptionid) {
		Criteria crit = getSession().createCriteria(ComponentDescription.class);
		crit.add(Restrictions.ilike("description", nameFragment, MatchMode.START));
		if (descriptionid != null) crit.add(Restrictions.ne("id", descriptionid));
		return crit.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<ComponentDescription> searchSortedAllComponentDescriptions(String nameFragment) {
		Criteria crit = getSession().createCriteria(ComponentDescription.class);
		crit.add(Restrictions.ilike("description", nameFragment, MatchMode.START));
		crit.addOrder(Order.asc("description"));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<CompDescSearchResultWrapper> searchSortedAllComponentDescriptionsHQL(String nameFragment) {
		String hql = "select new org.trescal.cwms.core.instrumentmodel.dto.CompDescSearchResultWrapper(compdsc.id, compdsc.description) "
				+ "from ComponentDescription compdsc where compdsc.description like :namefrag order by compdsc.description";
		Query query = getSession().createQuery(hql);
		query.setString("namefrag", nameFragment + "%");
		List<CompDescSearchResultWrapper> compdescs = query.list();
		return compdescs;
	}
}