package org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.Wire;

@Service("WireService")
public class WireServiceImpl extends BaseServiceImpl<Wire, Integer> implements WireService
{
	@Autowired
	private WireDao wireDao;
	
	@Override
	protected BaseDao<Wire, Integer> getBaseDao() {
		return wireDao;
	}
}