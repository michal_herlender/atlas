package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.InstrumentModelPartOf;

@Repository("InstrumentModelPartOfDao")
public class InstrumentModelPartOfDaoImpl extends BaseDaoImpl<InstrumentModelPartOf, Integer> implements InstrumentModelPartOfDao {
	
	@Override
	protected Class<InstrumentModelPartOf> getEntity() {
		return InstrumentModelPartOf.class;
	}
	
	@Override
	public InstrumentModelPartOf find(Integer id) {
		Criteria criteria = getSession().createCriteria(InstrumentModelPartOf.class);
		criteria.add(Restrictions.idEq(id));
		criteria.setFetchMode("module", FetchMode.JOIN);
		criteria.setFetchMode("base", FetchMode.JOIN);
		return (InstrumentModelPartOf) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public InstrumentModelPartOf findInstrumentModelPartOf(int moduleid, int baseid) {
		Criteria criteria = getSession().createCriteria(InstrumentModelPartOf.class);
		criteria.createCriteria("module").add(Restrictions.idEq(moduleid));
		criteria.createCriteria("base").add(Restrictions.idEq(baseid));
		List<InstrumentModelPartOf> list = criteria.list();
		return list.size() > 0 ? list.get(0) : null;
	}
	
	@SuppressWarnings("unchecked")
	public List<InstrumentModelPartOf> getModulesAvailableForBaseUnit(int baseid) {
		Criteria criteria = getSession().createCriteria(InstrumentModelPartOf.class);
		Criteria baseCrit = criteria.createCriteria("base");
		baseCrit.add(Restrictions.idEq(baseid));
		criteria.setFetchMode("module", FetchMode.JOIN);
		criteria.setFetchMode("module.mfr", FetchMode.JOIN);
		criteria.setFetchMode("module.description", FetchMode.JOIN);
		return criteria.list();
	}
}