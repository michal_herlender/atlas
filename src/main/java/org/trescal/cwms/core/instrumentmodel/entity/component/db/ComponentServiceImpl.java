package org.trescal.cwms.core.instrumentmodel.entity.component.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;
import org.trescal.cwms.core.instrumentmodel.entity.componentdescription.db.ComponentDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.instrumentmodel.form.EditComponentForm;
import org.trescal.cwms.core.jobs.repair.dto.ComponentJsonDto;

import java.time.LocalDate;
import java.util.List;

@Service("ComponentService")
public class ComponentServiceImpl extends BaseServiceImpl<Component, Integer> implements ComponentService {
	@Autowired
	private ComponentDao componentDao;
	@Autowired
	private ComponentDescriptionService compDescServ;
	@Autowired
	private MfrService mfrService;

	@Override
	public Component createOrUpdateComponent(EditComponentForm form, Contact contact) {
		Component comp;
		if (form.getComponentId() == 0) {
			comp = new Component();
		} else {
			comp = this.get(form.getComponentId());
		}

		comp.setMfr(form.getMfrid() == 0 ? null : this.mfrService.findMfr(form.getMfrid()));
		comp.setDescription(this.compDescServ.findComponentDescription(form.getDescid()));

		comp.setStockLastCheckedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		comp.setStockLastCheckedBy(contact);
		comp.setSalesCost(form.getSalesCost());

		if (form.getComponentId() == 0) {
			this.save(comp);
		}
		return comp;
	}

	@Override
	public List<Component> getComponents(int mfrid, int descid, Integer cid) {
		return this.componentDao.getComponents(mfrid, descid, cid);
	}

	@Override
	public List<Component> searchSortedModelComponent(Integer mfrid, Integer descid, Boolean inStock) {
		return this.componentDao.searchSortedModelComponent(mfrid, descid, inStock);
	}

	@Override
	protected BaseDao<Component, Integer> getBaseDao() {
		return componentDao;
	}

	@Override
	public List<ComponentJsonDto> searchComponents(String keyword, int maxResults) {
		return componentDao.searchComponents(keyword, maxResults);
	}

}