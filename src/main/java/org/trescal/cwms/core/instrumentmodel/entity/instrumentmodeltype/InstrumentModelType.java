package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.system.entity.translation.Translation;

/**
 * Entity that defines different high-level types of {@link InstrumentModel},
 * such as base unit, module, stand alone unit as well as a number of different
 * properties that relate to each type.
 * 
 * @author richard
 */
@Entity
@Table(name = "modeltype")
public class InstrumentModelType
{
	private boolean active;
	/**
	 * Defines if this modeltype is allowed a parent base unit
	 */
	private Boolean baseUnits;
	private Boolean defaultModelType;
	private int instModelTypeId;

	private Set<InstrumentModel> models;
	private String modelTypeDesc;

	private String modelTypeName;
	/**
	 * Defines if this modeltype is allowed a children modules(s)
	 */
	private Boolean modules;

	private Boolean canSelect;				// modeltype used for instruments (and changeable to other canSelect types)
	
	private Boolean salescategory;
	
	private Boolean capability;
	
	private Boolean undefined;
	
	private Set<Translation> modelTypeDescTranslation;
	private Set<Translation> modelTypeNameTranslation;

	public InstrumentModelType() {
		baseUnits = Boolean.FALSE;
		defaultModelType = Boolean.FALSE;
		modules = Boolean.FALSE;
		canSelect = Boolean.FALSE;
		salescategory = Boolean.FALSE;
		capability = Boolean.FALSE;
		undefined = Boolean.FALSE;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "instmodeltypeid", unique = true, nullable = false)
	public int getInstModelTypeId()
	{
		return this.instModelTypeId;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "modelType")
	public Set<InstrumentModel> getModels()
	{
		return this.models;
	}

	@Column(name = "modeltypedesc", nullable = true, length = 200)
	public String getModelTypeDesc()
	{
		return this.modelTypeDesc;
	}

	@Column(name = "modeltypename", nullable = true, length = 20)
	public String getModelTypeName()
	{
		return this.modelTypeName;
	}

	@NotNull
	@Column(name = "active", nullable = false, columnDefinition="tinyint")
	public boolean isActive()
	{
		return this.active;
	}

	@NotNull
	@Column(name = "baseunits", nullable = false, columnDefinition="tinyint")
	public Boolean getBaseUnits()
	{
		return this.baseUnits;
	}

	@NotNull
	@Column(name = "defaulttype", nullable = false, columnDefinition="tinyint")
	public Boolean getDefaultModelType()
	{
		return this.defaultModelType;
	}

	@NotNull
	@Column(name = "modules", nullable = false, columnDefinition="tinyint")
	public Boolean getModules()
	{
		return this.modules;
	}

	@NotNull
	@Column(name = "canselect", nullable=false, columnDefinition="bit default 0")
	public Boolean getCanSelect() {
		return canSelect;
	}

	@NotNull
	@Column(name = "salecategory", nullable = false, columnDefinition = "bit default 0")
	public Boolean getSalescategory() {
		return salescategory;
	}

	@NotNull
	@Column(name = "capability", nullable = false, columnDefinition = "bit default 0")
	public Boolean getCapability() {
		return capability;
	}

	@NotNull
	@Column(name = "undefined", nullable = false, columnDefinition = "bit default 0")
	public Boolean getUndefined() {
		return undefined;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public void setBaseUnits(Boolean baseUnits)
	{
		this.baseUnits = baseUnits;
	}

	public void setDefaultModelType(Boolean defaultModelType)
	{
		this.defaultModelType = defaultModelType;
	}

	public void setInstModelTypeId(int instModelTypeId)
	{
		this.instModelTypeId = instModelTypeId;
	}

	public void setModels(Set<InstrumentModel> models)
	{
		this.models = models;
	}

	public void setModelTypeDesc(String modelTypeDesc)
	{
		this.modelTypeDesc = modelTypeDesc;
	}

	public void setModelTypeName(String modelTypeName)
	{
		this.modelTypeName = modelTypeName;
	}

	public void setModules(Boolean modules)
	{
		this.modules = modules;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="modeltypenametranslation", joinColumns=@JoinColumn(name="instmodeltypeid"))
	public Set<Translation> getModelTypeNameTranslation() {
		return modelTypeNameTranslation;
	}

	public void setModelTypeNameTranslation(Set<Translation> modelTypeNameTranslation) {
		this.modelTypeNameTranslation = modelTypeNameTranslation;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="modeltypedesctranslation", joinColumns=@JoinColumn(name="instmodeltypeid"))
	public Set<Translation> getModelTypeDescTranslation() {
		return modelTypeDescTranslation;
	}

	public void setModelTypeDescTranslation(Set<Translation> modelTypeDescTranslation) {
		this.modelTypeDescTranslation = modelTypeDescTranslation;
	}

	public void setCanSelect(Boolean canSelect) {
		this.canSelect = canSelect;
	}

	public void setSalescategory(Boolean salescategory) {
		this.salescategory = salescategory;
	}

	public void setCapability(Boolean capability) {
		this.capability = capability;
	}

	public void setUndefined(Boolean undefined) {
		this.undefined = undefined;
	}
}
