package org.trescal.cwms.core.instrumentmodel.form;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ModelComponentSearchForm
{
	private String mfrid;
	private String descid;
	private boolean inStock;

}
