package org.trescal.cwms.core.instrumentmodel.entity.componentdescription.db;

import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.dto.CompDescSearchResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.componentdescription.ComponentDescription;

public interface ComponentDescriptionService
{
	void deleteComponentDescription(ComponentDescription componentdescription);

	ComponentDescription findComponentDescription(int id);

	List<ComponentDescription> getAllComponentDescriptions();

	/**
	 * Creates a new {@link ComponentDescription} entity from the given
	 * description and persists to the db, primarliy designed for ajax calls.
	 * 
	 * @param description the description.
	 * @return {@link ResultWrapper} containing the outcome of the operation.
	 */
	ResultWrapper insert(String description);

	void insertComponentDescription(ComponentDescription componentdescription);

	/**
	 * Tests if the given {@link ComponentDescription} description already
	 * exists for a different {@link ComponentDescription} whilst ignoring the
	 * {@link ComponentDescription} identified by the given id.
	 * 
	 * @param nameFragment the name to match.
	 * @param descriptionid the id of the current {@link ComponentDescription}
	 *            (will be ignored in matching).
	 * @return true if a duplicate name exists.
	 */
	boolean isDuplicateComponentDescription(String nameFragment, Integer descriptionid);

	void saveOrUpdateComponentDescription(ComponentDescription componentdescription);

	/**
	 * Returns a {@link List} of {@link ComponentDescription} entities whose
	 * case insensitive description start matches the given nameFragment.
	 * 
	 * @param nameFragment the text to match the description against.
	 * @return {@link List} of matching {@link ComponentDescription} entities.
	 */
	List<ComponentDescription> searchSortedAllComponentDescriptions(String nameFragment);

	/**
	 * Returns a {@link List} of {@link CompDescSearchResultWrapper} whose
	 * (case-insensitive) description start matches the nameFragment parameter.
	 * 
	 * @param nameFragment the name to match.
	 * @return {@link List} of {@link CompDescSearchResultWrapper}.
	 */
	List<CompDescSearchResultWrapper> searchSortedAllComponentDescriptionsHQL(String nameFragment);

	void updateComponentDescription(ComponentDescription componentdescription);
}