/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.system.entity.webresource.WebResource;

/**
 * Entity that holds all url resources for an {@link Mfr}.
 * 
 * @author richard
 */
@Entity
@DiscriminatorValue("mfr")
public class MfrWebResource extends WebResource
{
	private Mfr mfr;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "mfrid")
	public Mfr getMfr()
	{
		return this.mfr;
	}

	public void setMfr(Mfr mfr)
	{
		this.mfr = mfr;
	}
}
