package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.CanSelect;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;

public interface InstrumentModelTypeDao extends BaseDao<InstrumentModelType, Integer> {
	
	InstrumentModelType findDefaultType();
	
	List<InstrumentModelType> getAllInstrumentModelTypes(CanSelect canSelect);
}