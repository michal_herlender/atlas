package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.CanSelect;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType_;

@Repository("InstrumentModelTypeDao")
public class InstrumentModelTypeDaoImpl extends BaseDaoImpl<InstrumentModelType, Integer> implements InstrumentModelTypeDao {
	
	@Override
	protected Class<InstrumentModelType> getEntity() {
		return InstrumentModelType.class;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public InstrumentModelType findDefaultType() {
		return getFirstResult(cb -> {
				CriteriaQuery<InstrumentModelType> cq = cb.createQuery(InstrumentModelType.class);
				Root<InstrumentModelType> root = cq.from(InstrumentModelType.class);
				
				cq.where(cb.and(cb.equal(root.get(InstrumentModelType_.defaultModelType), true), 
						        cb.equal(root.get(InstrumentModelType_.active), true)));
				return cq;
		}).orElse(null);
	}
	
	@SuppressWarnings("unchecked")
	public List<InstrumentModelType> getAllInstrumentModelTypes(CanSelect canSelect) {
		return getResultList(cb -> {
			CriteriaQuery<InstrumentModelType> cq = cb.createQuery(InstrumentModelType.class);
			Root<InstrumentModelType> root = cq.from(InstrumentModelType.class);
			
			if(CanSelect.SELECTABLE.equals(canSelect))
				cq.where(cb.equal(root.get(InstrumentModelType_.canSelect), true));
			else if(CanSelect.RESERVED.equals(canSelect))
				cq.where(cb.equal(root.get(InstrumentModelType_.canSelect), false));
				
			return cq;
	});
	}
}