package org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.ModelRangeCharacteristicLibrary;

public interface ModelRangeCharacteristicLibraryDao extends BaseDao<ModelRangeCharacteristicLibrary, Integer> {

}
