package org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.CylindricalStandard;

public interface CylindricalStandardService extends BaseService<CylindricalStandard, Double>
{
}