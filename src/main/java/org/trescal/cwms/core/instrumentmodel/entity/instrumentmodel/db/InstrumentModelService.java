package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.Tuple;

import org.hibernate.HibernateException;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.dto.InstModSearchResultWrapper;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelCompanyCalReqDTO;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelForNewJobItemSearchDto;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelPlantillasDTO;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelWithPriceDTO;
import org.trescal.cwms.core.instrumentmodel.dto.ServiceCapabilitiesAndCurrencyWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchByLinkedQuotationForm;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchInstrumentForm;
import org.trescal.cwms.core.referential.dto.TMLInstrumentModelDTO;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tools.PagedResultSet;

/**
 * @author scott.chamberlain
 *
 */
public interface InstrumentModelService extends BaseService<InstrumentModel, Integer> {
	/**
	 * Returns list of models in the ame sales category as the supplied model
	 * 
	 *
	 */
	List<InstrumentModel> getOtherModelsWithMatchingSalesCategory(InstrumentModel instrumentmodel);
	
	void deleteInstrumentModel(InstrumentModel instrumentmodel);
	
	InstrumentModel findInstrumentModel(int id);
	
	List<InstrumentModel> findInstrumentModel(Description description);
	
	List<InstrumentModel> findInstrumentModel(SalesCategory salesCategory);
	
	InstrumentModel findCapabilityModel(Description description) throws HibernateException;
	
	InstrumentModel findSalesCategoryModel(SalesCategory salesCategory) throws HibernateException;

	List<InstrumentModel> getAllInstrumentModels();
	
	List<InstrumentModelWithPriceDTO> getAll(DomainType domainType, Locale locale, ServiceType serviceType,
			Company allocatedCompany);

	List<Integer> getDistinctModelidListForQuote(int quoteId);

	List<Tuple> getModelIdsForInstruments(Collection<Integer> instrumentIds);

	/**
	 * Test's if the {@link InstrumentModel} has a default calibration cost for the
	 * given {@link CalibrationType} and returns the value of the default in the
	 * required currency by using the supplied exchange rate.
	 * 
	 * @param model the {@link InstrumentModel}.
	 * @param calType the {@link CalibrationType}.
	 * @param exRate the exchange rate.
	 * @return the calculated cost or 0.00 if none could be found.
	 */
	BigDecimal getModelDefaultCalCost(InstrumentModel model, CalibrationType calType, BigDecimal exRate,
			Company company);

	List<InstrumentModel> getModelsByIds(Collection<Integer> modelids);

	PagedResultSet<InstrumentModel> getModelsFromIds(PagedResultSet<InstrumentModel> prs, List<Integer> modelIds);


	InstrumentModelPlantillasDTO getPlantillasModel(Locale locale, int modelid);
	
	PagedResultSet<InstrumentModelPlantillasDTO> getPlantillasModels(Locale locale, Date afterLastModified,
			int resultsPerPage, int currentPage);
	
	/**
	 * Tests if an {@link InstrumentModel} with the same model name, {@link Mfr} and
	 * {@link Description} already exists. If so then returns a {@link List} of all
	 * matching instances.
	 * 
	 * @param mfrId the {@link Mfr} id.
	 * @param model the {@link InstrumentModel} name.
	 * @param descriptionId the {@link Description} id.
	 * @param excludeModelId id of an {@link InstrumentModel} to exclude.
	 * @return {@link List} of matching {@link InstrumentModel}s, empty if no
	 *         matches found.
	 */
	List<InstrumentModel> searchModelSignature(Integer mfrId, String model, Integer descriptionId, Integer modelTypeId,
			Integer excludeModelId);

	PagedResultSet<InstrumentModel> searchInstrumentModelsPaged(PagedResultSet<InstrumentModel> prs,
			InstrumentAndModelSearchForm<InstrumentModel> form);
	
	/**
	 * Returns a {@link List} of {@link InstrumentModel} entities that match the
	 * given criteria. Blank or null properties are ignored.
	 * 
	 * @return {@link List} of matching {@link InstrumentModel} entities.
	 */
	List<InstrumentModel> searchInstrumentModels(InstrumentAndModelSearchForm<InstrumentModel> form);

	/**
	 * Returns a {@link List} of {@link InstModSearchResultWrapper} whose
	 * (case-insensitive) content matches the given criteria. Blank or null
	 * properties are ignored. {@link InstrumentModel}s).
	 * 
	 * @param mfr the name of the {@link Mfr}.
	 * @param model the name of the {@link InstrumentModel} model.
	 * @param description the name of the {@link Description}.
	 * @return {@link List} of {@link InstModSearchResultWrapper}.
	 */
	List<InstModSearchResultWrapper> searchInstrumentModelsDWR(String mfr, String model, String description,
			Integer descriptionId, Integer manufacturerId);

	ServiceCapabilitiesAndCurrencyWrapper findAjaxCapabilityAndCurrencyDetails(int modelid, String curCode);
	
	InstrumentModel findLazyInstrumentModel(int id);

	InstrumentModel getByTmlId(Integer tmlModelId);
	
	Long getCount();
	
	@Deprecated
	InstrumentModel getTMLInstrumentModel(int tmlid);

	@Deprecated
	InstrumentModel insertTMLInstrumentModel(TMLInstrumentModelDTO dto);

	@Deprecated
	InstrumentModel updateTMLInstrumentModel(TMLInstrumentModelDTO dto);

    void regenerateInstrumentModelTranslations(InstrumentModel instModel);
	
	int updateInstrumentModelTranslations(Collection<InstrumentModel> instModels, Locale locale);
	
	String updateAllInstrumentModelTranslations(Collection<Locale> locales, int offset, int batchSize);

	List<InstrumentModel> getStandAloneModelsMatchingSalesCategoryModel(InstrumentModel salesCategoryModel);

	PagedResultSet<InstrumentModelCompanyCalReqDTO> getInstrumentModelsWithCompanyCalRequirements(int coid, String mfr,
			String model, String desc, Integer resultsPerPage, Integer currentPage, Locale locale,
			boolean onlyWithCalReqs);

    PagedResultSet<InstrumentModelForNewJobItemSearchDto> searchInstrumentModelsForNewJobItem(NewJobItemSearchInstrumentForm form);

	PagedResultSet<InstrumentModelForNewJobItemSearchDto> searchAllMatchingInstrumentModelsForNewJobItem(NewJobItemSearchInstrumentForm form);

    PagedResultSet<InstrumentModelForNewJobItemSearchDto> findModelsByIdsForNewJobItemSearch(NewJobItemSearchByLinkedQuotationForm form, List<Integer> modelIds);

	PagedResultSet<InstrumentModelForNewJobItemSearchDto> findCompanyModelsByIdsForNewJobIntemSearch(NewJobItemSearchByLinkedQuotationForm form, List<Integer> modelIds);
}