package org.trescal.cwms.core.instrumentmodel.entity.salescategory.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.form.EditModelSalesCategoryForm;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Service
public class EditModelSalesCategoryServiceImpl implements EditModelSalesCategoryService {
	
	@Autowired
	private SalesCategoryService salesCategoryService;
	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private MessageSource messages;
	@Autowired
	private TranslationService translationService;

	@Override
	public void editModelSalesCategoryService(EditModelSalesCategoryForm form) {
		InstrumentModel model = this.instrumentModelService.get(form.getModelid());
		model.setSalesCategory(form.getSalesCategoryId() != null ? this.salesCategoryService.getSalesCategory(form.getSalesCategoryId()) : null);
	}
	
	@Override
	public List<KeyValueIntegerString> getSalesCategoryOptions(InstrumentModel instModel, Locale locale) {
		List<KeyValueIntegerString> results = new ArrayList<>();
		String notApplicable = this.messages.getMessage("company.nonespecified", null, "None Specified", locale);
		results.add(new KeyValueIntegerString(0, notApplicable));
		
		// If the current sales category belongs to the *wrong* subfamily prepend to the list
		if (instModel.getSalesCategory() != null) {
			InstrumentModel currentSalesCategoryInstrumentModel = this.instrumentModelService.findSalesCategoryModel(instModel.getSalesCategory());
			if (currentSalesCategoryInstrumentModel.getDescription().getId() != instModel.getDescription().getId()) {
				String translation = this.translationService.getCorrectTranslation(instModel.getSalesCategory().getTranslations(), locale);
				KeyValueIntegerString currentSelection= new KeyValueIntegerString(instModel.getSalesCategory().getId(), translation);
				results.add(currentSelection);
			}
		}
		
		Integer subFamilyId = instModel.getDescription().getId();
		results.addAll(this.salesCategoryService.getSalesCategoriesForSubfamily(subFamilyId, locale));
		return results;
	}
}
