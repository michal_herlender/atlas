package org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.ThreadType;

public interface ThreadTypeDao extends BaseDao<ThreadType, Integer> {}