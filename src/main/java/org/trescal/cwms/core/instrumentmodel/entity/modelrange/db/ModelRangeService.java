package org.trescal.cwms.core.instrumentmodel.entity.modelrange.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;

public interface ModelRangeService
{
	/**
	 * Deletes all {@link ModelRange} entities in given list.
	 * 
	 * @param ranges list of {@link ModelRange}.
	 */
	void deleteAll(Collection<ModelRange> ranges);

	void deleteModelRange(ModelRange modelrange);

	ModelRange findModelRange(int id);

	ModelRange getByModelIdAndOptionId(int modelId, int optionId);
	
	List<ModelRange> getAllModelRanges();

	void insertModelRange(ModelRange modelrange);

	void saveOrUpdateModelRange(ModelRange modelrange);

	void updateModelRange(ModelRange modelrange);

	void deleteModelRange(Integer modelRangeId);

}