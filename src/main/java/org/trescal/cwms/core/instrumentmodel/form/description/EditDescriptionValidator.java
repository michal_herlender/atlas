package org.trescal.cwms.core.instrumentmodel.form.description;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@Component
public class EditDescriptionValidator implements Validator {
	
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	
	@Autowired
	private NewDescriptionService descriptionService;
	
	@Override
	public boolean supports(Class<?> arg0) {
		if (arg0.equals(EditDescriptionForm.class)) return true;
		return false;
	}
	
	@Override
	public void validate(Object instance, Errors errors) {
		EditDescriptionForm form = (EditDescriptionForm) instance;
		if (form.getDescription() == null) {
			errors.rejectValue("description", "editdescription.error.null", 
					"No description record was found to edit.");
		}
		for (Translation __t: form.getTranslations()) {
			if (__t.getLocale().equals(supportedLocaleService.getPrimaryLocale()) && (__t.getTranslation() == null || __t.getTranslation().length() == 0)) {
				errors.rejectValue("translations", "editdescription.error.primaryLocaleNull", 
						"The primary Locale (" + supportedLocaleService.getPrimaryLocale().getDisplayLanguage() + ") mustn't be empty.");
				break;
			}
		}
		//translation is unique for every language
		for (Translation __t: form.getTranslations()) {
			if (__t.getTranslation().length() > 0 &&
					descriptionService.getResultCountExact(__t.getTranslation(), __t.getLocale(), form.getDescription()) > 0) {
				errors.rejectValue("translations", "editdescription.error.translationNotUnique",
						"Description '" + __t.getTranslation() + "' already defined.");
			}
		}
	}
}