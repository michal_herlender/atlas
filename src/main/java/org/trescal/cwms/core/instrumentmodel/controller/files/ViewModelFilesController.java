package org.trescal.cwms.core.instrumentmodel.controller.files;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.controller.AbstractModelController;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

@Controller @IntranetController
public class ViewModelFilesController extends AbstractModelController {
	@RequestMapping(value="/instrumentmodelfiles.htm", method=RequestMethod.GET)
	public ModelAndView requestHandler(@ModelAttribute("model") InstrumentModel instrumentModel, Model model) throws Exception
	{
		// TODO load page specific data for lookup
		model.addAttribute("selectedTab", "files");
		return new ModelAndView("/trescal/core/instrumentmodel/files/modelviewfiles", "command", model);
	}
}
