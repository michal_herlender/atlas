package org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.ModelRangeCharacteristicLibrary;

public interface ModelRangeCharacteristicLibraryService extends BaseService<ModelRangeCharacteristicLibrary, Integer> {

}
