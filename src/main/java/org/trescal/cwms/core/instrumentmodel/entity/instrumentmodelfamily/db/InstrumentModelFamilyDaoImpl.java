package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain_;
import org.trescal.cwms.core.instrumentmodel.entity.domain.db.InstrumentModelDomainDao;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily_;
import org.trescal.cwms.core.referential.db.TMLLocaleResolver;
import org.trescal.cwms.core.referential.dto.TMLFamilyDTO;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.spring.model.InstrumentModelFamilyJsonDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Repository("InstrumentModelFamilyDao")
public class InstrumentModelFamilyDaoImpl extends BaseDaoImpl<InstrumentModelFamily, Integer>
		implements InstrumentModelFamilyDao {

	private static final Logger logger = LoggerFactory.getLogger(InstrumentModelFamilyDaoImpl.class);

	@Autowired
	private InstrumentModelDomainDao instModelDomDao;

	@Override
	protected Class<InstrumentModelFamily> getEntity() {
		return InstrumentModelFamily.class;
	}

//	private static String SELECT_QUERY_STRING_JSON = "select new org.trescal.cwms.spring.model.InstrumentModelFamilyJsonDTO("
//			+ "f.familyid, case when t.translation is null then f.name else t.translation end, f.active, f.tmlid) "
//			+ "from InstrumentModelFamily f " + "left join f.translation t " + "with t.locale = :locale "
//			+ "order by t.translation";
//
//	private static String SELECT_QUERY_STRING_DOMAIN = "select new org.trescal.cwms.spring.model.InstrumentModelFamilyJsonDTO("
//			+ "f.familyid, case when t.translation is null then f.name else t.translation end, f.active, f.tmlid) "
//			+ "from InstrumentModelFamily f " + "left join f.translation t " + "with t.locale = :locale "
//			+ "join f.domain d " + "with d.domainid = :domainid " + "order by t.translation";
//
//	private static String FROM_QUERY_STRING = "from InstrumentModelFamily f " + "left join f.translation t "
//			+ "where t.locale = :locale " + "and t.translation like :translation";
//
//	private static String FROM_QUERY_STRING_EXCLUDE = FROM_QUERY_STRING + " and f.id != :exclude";

	@Override
	public boolean existsByName(String name) {
		return getFirstResult(cb -> {
			CriteriaQuery<InstrumentModelFamily> cq = cb.createQuery(InstrumentModelFamily.class);
			Root<InstrumentModelFamily> root = cq.from(InstrumentModelFamily.class);
			cq.where(cb.equal(root.get(InstrumentModelFamily_.name), name));
			return cq;
		}).orElse(null) != null;
		
	}

	/*
	 * Returns all Instrument Model Families for the given current user locale.
	 */
	@Override
	public List<InstrumentModelFamilyJsonDTO> getAllFamilies(Locale searchLanguage) {
		return getResultList(cb -> {
			CriteriaQuery<InstrumentModelFamilyJsonDTO> cq = cb.createQuery(InstrumentModelFamilyJsonDTO.class);
			Root<InstrumentModelFamily> root = cq.from(InstrumentModelFamily.class);
			Join<InstrumentModelFamily, Translation> translationJoin = root.join(InstrumentModelFamily_.translation, JoinType.LEFT);
			
			cq.where(cb.equal(translationJoin.get(Translation_.locale), searchLanguage));
			cq.select(cb.construct(InstrumentModelFamilyJsonDTO.class, 
					root.get(InstrumentModelFamily_.familyid),
					cb.selectCase()
					.when(cb.isNotNull(translationJoin.get(Translation_.translation)), translationJoin.get(Translation_.translation))
					.otherwise(root.get(InstrumentModelFamily_.name)),
					root.get(InstrumentModelFamily_.active),
					root.get(InstrumentModelFamily_.tmlid)));
			cq.orderBy(cb.asc(translationJoin.get(Translation_.translation)));
			
			return cq;
		});
	}

	@Override
	public List<InstrumentModelFamilyJsonDTO> getAllForDomain(Locale userLocale, int domainid) {
		return getResultList(cb -> {
			CriteriaQuery<InstrumentModelFamilyJsonDTO> cq = cb.createQuery(InstrumentModelFamilyJsonDTO.class);
			Root<InstrumentModelFamily> root = cq.from(InstrumentModelFamily.class);
			Join<InstrumentModelFamily, Translation> translationJoin = root.join(InstrumentModelFamily_.translation, JoinType.LEFT);
			Join<InstrumentModelFamily, InstrumentModelDomain> instModelDomainlJoin = root.join(InstrumentModelFamily_.domain);
			
			cq.where(cb.and(cb.equal(translationJoin.get(Translation_.locale), userLocale),
					        cb.equal(instModelDomainlJoin.get(InstrumentModelDomain_.domainid), domainid)));
			cq.select(cb.construct(InstrumentModelFamilyJsonDTO.class, 
					root.get(InstrumentModelFamily_.familyid),
					cb.selectCase()
					.when(cb.isNotNull(translationJoin.get(Translation_.translation)), translationJoin.get(Translation_.translation))
					.otherwise(root.get(InstrumentModelFamily_.name)),
					root.get(InstrumentModelFamily_.active),
					root.get(InstrumentModelFamily_.tmlid)));
			cq.orderBy(cb.asc(translationJoin.get(Translation_.translation)));
			
			return cq;
		});
	}

	// Gets a result count for the family (matching exactly) in the specified search
	// language
	@Override
	public int getResultCountExact(String translation, Locale searchLanguage, int excludeId) {
		return getCount(cb -> cq -> {
			Root<InstrumentModelFamily> root = cq.from(InstrumentModelFamily.class);
			Join<InstrumentModelFamily, Translation> translationJoin = root.join(InstrumentModelFamily_.translation, JoinType.LEFT);
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(translationJoin.get(Translation_.locale), searchLanguage));
			clauses.getExpressions().add(cb.like(translationJoin.get(Translation_.translation), translation));
			clauses.getExpressions().add(cb.notEqual(root.get(InstrumentModelFamily_.familyid), excludeId));
			cq.where(clauses);

			return Triple.of(root.get(InstrumentModelFamily_.familyid), null, null);
		});
	}

	@Override
	public List<KeyValueIntegerString> getAllKeyValues(String familyFragment, DomainType domainType,
			Locale searchLanguage, int maxResults) {
		return getResultList(cb -> {
			CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class);
			Root<InstrumentModelFamily> family = cq.from(InstrumentModelFamily.class);
			Expression<String> familyName = joinTranslation(cb, family, InstrumentModelFamily_.translation,
					searchLanguage);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.like(familyName, '%' + familyFragment + '%'));
			if (domainType != null) {
				Join<InstrumentModelFamily, InstrumentModelDomain> domain = family.join(InstrumentModelFamily_.domain);
				clauses.getExpressions().add(cb.equal(domain.get(InstrumentModelDomain_.domainType), domainType));
			}
			cq.where(clauses);
			cq.orderBy(cb.asc(familyName));
			cq.select(
					cb.construct(KeyValueIntegerString.class, family.get(InstrumentModelFamily_.familyid), familyName));
			return cq;
		}, 0, maxResults);
	}

	@Override
	public InstrumentModelFamily getTMLFamily(int tmlid) {
		return getFirstResult(cb -> {
			CriteriaQuery<InstrumentModelFamily> cq = cb.createQuery(InstrumentModelFamily.class);
			Root<InstrumentModelFamily> root = cq.from(InstrumentModelFamily.class);
			
			cq.where(cb.equal(root.get(InstrumentModelFamily_.tmlid), tmlid));

			return cq;
		}).orElse(null);
	}

	public void insertTMLInstrumentModelFamily(TMLFamilyDTO dto) throws Exception {
		// First try to find if a family already exists with the same name and not
		// linked to TML
		InstrumentModelDomain instrumentModelDomain = instModelDomDao.getTMLDomain(dto.getDomainID());
		if(instrumentModelDomain == null) {
			String errorMessage = MessageFormat.format(
					"Error while inserting the family {0}, the related Domain with ID {1} doesn't exists.",
					dto.getDefaultName(), dto.getDomainID());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		}
		else {
			InstrumentModelFamily existingFamily = getFirstResult(cb -> {
				CriteriaQuery<InstrumentModelFamily> cq = cb.createQuery(InstrumentModelFamily.class);
				Root<InstrumentModelFamily> root = cq.from(InstrumentModelFamily.class);
				Join<InstrumentModelFamily, InstrumentModelDomain> instModelModel = root.join(InstrumentModelFamily_.domain);
				
				Predicate clauses = cb.conjunction();
				clauses.getExpressions().add(cb.equal(root.get(InstrumentModelFamily_.name), dto.getDefaultName()));
				clauses.getExpressions().add(cb.equal(instModelModel.get(InstrumentModelDomain_.domainid), instrumentModelDomain.getDomainid()));
				
				Predicate andClause = cb.conjunction();
				andClause.getExpressions().add(cb.isNotNull(root.get(InstrumentModelFamily_.tmlid)));
				andClause.getExpressions().add(cb.notEqual(root.get(InstrumentModelFamily_.tmlid), dto.getTMLID()));
				
				Predicate orClause = cb.disjunction();
				orClause.getExpressions().add(cb.isNull(root.get(InstrumentModelFamily_.tmlid)));
				orClause.getExpressions().add(andClause);
				
				clauses.getExpressions().add(orClause);
				
				cq.where(clauses);
	
				return cq;
			}).orElse(null);
	
			if (existingFamily == null) {
				InstrumentModelFamily instrumentModelFamily = new InstrumentModelFamily();
				instrumentModelFamily.setTmlid(dto.getTMLID());
				instrumentModelFamily.setDomain(instrumentModelDomain);
				instrumentModelFamily.setName(dto.getDefaultName());
				instrumentModelFamily.setActive(dto.isIsEnabled());
	
				SortedSet<Translation> newTranslations = new TreeSet<Translation>();
	
				Iterator<String> iterator = dto.getRegionalNames().keySet().iterator();
	
				while (iterator.hasNext()) {
					String lang = iterator.next();
					String value = dto.getRegionalNames().get(lang);
					if (TMLLocaleResolver.isResolvable(lang)) {
						Locale locale = TMLLocaleResolver.resolveLocale(lang); 
						Translation t = new Translation(locale, !value.equals(null) ? value.toString() : "");
						newTranslations.add(t);
					}
				}
	
				instrumentModelFamily.setTranslation(newTranslations);
	
				logger.debug("Inserting: " + instrumentModelFamily.toFullString());
				this.persist(instrumentModelFamily);
				// Add this to avoid locks
				logger.debug("Inserted: " + instrumentModelFamily.toFullString());
			} else {
				if (existingFamily.getTmlid() != null && !existingFamily.getTmlid().equals(dto.getTMLID())) {
					String errorMessage = MessageFormat.format(
							"Error while inserting, the family {0} already exists with a different TMLID.",
							dto.getDefaultName());
					logger.error(errorMessage);
					throw new Exception(errorMessage);
				} else {
					if (existingFamily.getTmlid() == null) {
						existingFamily.setTmlid(dto.getTMLID());
						this.merge(existingFamily);
						// Add this to avoid locks
					}
	
					if (dto.isIsEnabled()) {
						this.updateTMLFamily(dto);
					} else {
						this.deleteTMLFamily(dto);
					}
				}
			}
		}
	}

	@Override
	public void updateTMLFamily(TMLFamilyDTO dto) throws Exception {
		InstrumentModelFamily instrumentModelFamily = this.getTMLFamily(dto.getTMLID());

		// First try to find if a family already exists with the same name and not
		// linked to TML
		InstrumentModelDomain instrumentModelDomain = instModelDomDao.getTMLDomain(dto.getDomainID());
		if(instrumentModelDomain == null) {
			String errorMessage = MessageFormat.format(
					"Error while updating the family {0}, the related Domain with ID {1} doesn't exists.",
					dto.getDefaultName(), dto.getDomainID());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		}
		else {
			InstrumentModelFamily existingFamily = getFirstResult(cb -> {
				CriteriaQuery<InstrumentModelFamily> cq = cb.createQuery(InstrumentModelFamily.class);
				Root<InstrumentModelFamily> root = cq.from(InstrumentModelFamily.class);
				Join<InstrumentModelFamily, InstrumentModelDomain> instModelModel = root.join(InstrumentModelFamily_.domain);
				
				Predicate clauses = cb.conjunction();
				clauses.getExpressions().add(cb.equal(root.get(InstrumentModelFamily_.name), dto.getDefaultName()));
				clauses.getExpressions().add(cb.notEqual(root.get(InstrumentModelFamily_.familyid), instrumentModelFamily.getFamilyid()));
				clauses.getExpressions().add(cb.equal(instModelModel.get(InstrumentModelDomain_.domainid), instrumentModelDomain.getDomainid()));
				
				Predicate andClause = cb.conjunction();
				andClause.getExpressions().add(cb.isNotNull(root.get(InstrumentModelFamily_.tmlid)));
				andClause.getExpressions().add(cb.notEqual(root.get(InstrumentModelFamily_.tmlid), dto.getTMLID()));
				
				Predicate orClause = cb.disjunction();
				orClause.getExpressions().add(cb.isNull(root.get(InstrumentModelFamily_.tmlid)));
				orClause.getExpressions().add(andClause);
				
				clauses.getExpressions().add(orClause);
				
				cq.where(clauses);
	
				return cq;
			}).orElse(null);
	
			if (existingFamily == null) {
				instrumentModelFamily.setTmlid(dto.getTMLID());
				instrumentModelFamily.setDomain(instrumentModelDomain);
				instrumentModelFamily.setName(dto.getDefaultName());
				instrumentModelFamily.setActive(dto.isIsEnabled());
	
				SortedSet<Translation> newTranslations = new TreeSet<Translation>();
	
				Iterator<String> iterator = dto.getRegionalNames().keySet().iterator();
	
				while (iterator.hasNext()) {
					String lang = iterator.next();
					String value = dto.getRegionalNames().get(lang);
					if (TMLLocaleResolver.isResolvable(lang)) {
						Locale locale = TMLLocaleResolver.resolveLocale(lang); 
						Translation t = new Translation(locale, !value.equals(null) ? value.toString() : "");
						newTranslations.add(t);
					}
				}
	
				instrumentModelFamily.setTranslation(newTranslations);
	
				logger.debug("Updating: " + instrumentModelFamily.toFullString());
				this.merge(instrumentModelFamily);
				// Add this to avoid locks
				logger.debug("Updated: " + instrumentModelFamily.toFullString());
			} else {
				String errorMessage = MessageFormat.format("Error while updating, the family {0} already exists with a different TMLID.",
								dto.getDefaultName());
				logger.error(errorMessage);
				throw new Exception(errorMessage);
			}
		}
	}

	@Override
	public void deleteTMLFamily(TMLFamilyDTO dto) throws Exception {
		// Check if this domain is already used by active instrument
		int count = (int) getCount(cb -> cq -> {
			Root<Instrument> root = cq.from(Instrument.class);
			Join<Instrument, InstrumentModel> instModelJoin = root.join(Instrument_.model);
			Join<InstrumentModel, Description> descriptionJoin = instModelJoin.join(InstrumentModel_.description);
			Join<Description, InstrumentModelFamily> instModelFamilyJoin = descriptionJoin.join(Description_.family);
			
			cq.where(cb.equal(instModelFamilyJoin.get(InstrumentModelFamily_.tmlid), dto.getTMLID()));
			
			return Triple.of(root.get(Instrument_.plantid), null, null);
		});
		if (count <= 0) {
			InstrumentModelFamily instrumentModelFamily = this.getTMLFamily(dto.getTMLID());
			instrumentModelFamily.setActive(dto.isIsEnabled());
			logger.debug("Deleting: " + instrumentModelFamily.toFullString());
			this.merge(instrumentModelFamily);
			// Add this to avoid locks
			logger.debug("Deleted: " + instrumentModelFamily.toFullString());
		} else {
			String errorMessage = MessageFormat.format(
					"Error while deleting, the family {0} is used by at least 1 active instrument.",
					dto.getDefaultName());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		}
	}
}