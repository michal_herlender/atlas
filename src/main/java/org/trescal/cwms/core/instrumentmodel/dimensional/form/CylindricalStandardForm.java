package org.trescal.cwms.core.instrumentmodel.dimensional.form;

import org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.CylindricalStandard;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CylindricalStandardForm
{
	private CylindricalStandard cs;
	private boolean create;

}
