/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

/**
 * Entity representing one {@link Component} that <i>can</i> belong to the
 * mapped {@link InstrumentModel}.
 * 
 * @author richard
 */
@Entity
@Table(name = "modelcomponent", uniqueConstraints = { @UniqueConstraint(columnNames = { "modelid", "componentid" }) })
public class InstrumentModelComponent
{
	private Component component;
	private int id;
	private boolean includeByDefault;
	private InstrumentModel model;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "componentid", nullable = false)
	public Component getComponent()
	{
		return this.component;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelid", nullable = false)
	public InstrumentModel getModel()
	{
		return this.model;
	}

	@NotNull
	@Column(name = "includebydefault", nullable = false, columnDefinition="tinyint")
	public boolean isIncludeByDefault()
	{
		return this.includeByDefault;
	}

	public void setComponent(Component component)
	{
		this.component = component;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setIncludeByDefault(boolean includeByDefault)
	{
		this.includeByDefault = includeByDefault;
	}

	public void setModel(InstrumentModel model)
	{
		this.model = model;
	}
}