package org.trescal.cwms.core.instrumentmodel.entity.stockablemodel;

import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.contact.Contact;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Abstract class that provides functionality which allows all Model subclasses
 * to be held in stock.
 *
 * @author Richard
 */
@MappedSuperclass
public abstract class StockableModel extends Versioned {
	private Integer inStock;
	private Integer minimumStockLevel;
	private BigDecimal salesCost;
	private Contact stockLastCheckedBy;
	private LocalDate stockLastCheckedOn;

	@Min(0)
	@Column(name = "instock")
	public Integer getInStock() {
		return this.inStock;
	}

	@Column(name = "minstocklevel")
	public Integer getMinimumStockLevel() {
		return this.minimumStockLevel;
	}

	@Column(name = "salescost", precision = 10, scale = 2)
    public BigDecimal getSalesCost() {
        return this.salesCost;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "stocklastcheckedby")
    public Contact getStockLastCheckedBy() {
        return this.stockLastCheckedBy;
    }

    @Column(name = "stocklastchecked", columnDefinition = "date")
    public LocalDate getStockLastCheckedOn() {
        return this.stockLastCheckedOn;
    }

    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    public void setMinimumStockLevel(Integer minimumStockLevel) {
		this.minimumStockLevel = minimumStockLevel;
	}

	public void setSalesCost(BigDecimal salesCost) {
		this.salesCost = salesCost;
	}

	public void setStockLastCheckedBy(Contact stockLastCheckedBy) {
		this.stockLastCheckedBy = stockLastCheckedBy;
	}

	public void setStockLastCheckedOn(LocalDate stockLastCheckedOm) {
		this.stockLastCheckedOn = stockLastCheckedOm;
	}

}
