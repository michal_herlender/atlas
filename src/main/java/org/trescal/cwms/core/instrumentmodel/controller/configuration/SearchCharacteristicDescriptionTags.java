package org.trescal.cwms.core.instrumentmodel.controller.configuration;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.db.CharacteristicDescriptionService;
import org.trescal.cwms.spring.model.LabelIdDTO;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;

/* 
 * This currently only returns characteristic descriptions of type VALUE_WITH_UNIT
 * as this is the only type of characteristic that needs to be added to a non
 * TML model.
 */

@Controller
public class SearchCharacteristicDescriptionTags {
	
	@Autowired
	private CharacteristicDescriptionService characteristicDescriptionService;
	public static int MAX_RESULTS = 50;
	
	@RequestMapping(value="/searchcharacteristicdescriptiontags.json", method=RequestMethod.GET)
	public @ResponseBody List<LabelIdDTO> getTagList(@RequestParam("term") String descriptionFragment, Locale userLocale) {
		return characteristicDescriptionService.getLabelIdList(descriptionFragment, userLocale, MAX_RESULTS);
	}

}
