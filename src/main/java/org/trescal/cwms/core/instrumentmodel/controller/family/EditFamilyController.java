package org.trescal.cwms.core.instrumentmodel.controller.family;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db.InstrumentModelFamilyService;
import org.trescal.cwms.core.instrumentmodel.form.EditFamilyForm;
import org.trescal.cwms.core.instrumentmodel.form.EditFamilyValidator;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;


@Controller
@RequestMapping(value="editinstrumentmodelfamily.htm")
public class EditFamilyController {
	
	private Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	EditFamilyValidator editFamilyValidator;
	
	@Autowired 
	private InstrumentModelFamilyService instModFamServ;
	
	@Autowired
	private SupportedLocaleService supportedLocaleService;

	@ModelAttribute("editfamilyform")
	public EditFamilyForm createEditFamilyForm() {
		
		EditFamilyForm form = new EditFamilyForm();
		
		SortedSet<Translation> translations = new TreeSet<Translation>();
		for (Locale locale: supportedLocaleService.getSupportedLocales())
			translations.add(new Translation(locale, ""));
		form.setTranslations(translations);
		
		return form;
		
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String displayForm(@ModelAttribute("editfamilyform") EditFamilyForm form, @RequestParam(value="id", required=false) Integer id){	
		
		Map<Locale, String> translationMap = new HashMap<Locale, String>();
		
		if(id != null) {
			InstrumentModelFamily family = instModFamServ.getFamily(id);
			
			form.setFamily(family);

			for (Translation t: family.getTranslation())
				translationMap.put(t.getLocale(), t.getTranslation());
			
			SortedSet<Translation> translations = new TreeSet<Translation>();
			for (Translation ft: form.getTranslations())
				translations.add(new Translation(ft.getLocale(), translationMap.containsKey(ft.getLocale()) ? translationMap.get(ft.getLocale()) : ""));
			form.setTranslations(translations);			
			
			logger.debug("Get Translation: " + form.getTranslations());
				
		}

		return "trescal/core/instrumentmodel/family/editfamily";
	}
	
	@RequestMapping(params = "save", method = RequestMethod.POST)
	public String processFormSave(@Validated @ModelAttribute("editfamilyform") EditFamilyForm editFamilyForm, BindingResult result){
		editFamilyValidator.validate(editFamilyForm, result);
		if(result.hasErrors()) {
			return "trescal/core/instrumentmodel/family/editfamily";
		}
		
		InstrumentModelFamily family = editFamilyForm.getFamily();
		
		// Remove any empty translations
		SortedSet<Translation> newTranslations = new TreeSet<Translation>();
		logger.debug("Default locale is "+supportedLocaleService.getPrimaryLocale());
		
		logger.debug(editFamilyForm.getTranslations().size()+" translations were provided.");
		for(Translation t : editFamilyForm.getTranslations()) {
			logger.debug("Adding translation "+t.getTranslation()+" for locale "+t.getLocale());
			if (!t.getTranslation().isEmpty()) {
				newTranslations.add(t);
			}
			if (t.getLocale().equals(supportedLocaleService.getPrimaryLocale())) {
				logger.debug("Setting default translation "+t.getTranslation()+" for locale "+t.getLocale());
				family.setName(t.getTranslation());
			}
		}
			
		family.setTranslation(newTranslations);
		
		if(editFamilyForm.getFamily().getFamilyid() > 0) {
			logger.debug("Updating family");
			this.instModFamServ.updateInstrumentModelFamily(editFamilyForm.getFamily());
		}
		else {
			logger.debug("Creating family");
			this.instModFamServ.createInstrumentModelFamily(editFamilyForm.getFamily());
		}
		return "trescal/core/instrumentmodel/family/viewfamily";
	}
	
	@RequestMapping(params = "delete", method = RequestMethod.POST)
	public String processFormDelete(@Validated @ModelAttribute("editfamilyform") EditFamilyForm editFamilyForm, BindingResult result){
		editFamilyValidator.validate(editFamilyForm, result);
		if(result.hasErrors() || editFamilyForm.getFamily().getFamilyid() == 0) {
			return "trescal/core/instrumentmodel/family/editfamily";
		}

		// Remove any empty translations from the form
		SortedSet<Translation> newTranslations = new TreeSet<Translation>();
		for(Translation t : editFamilyForm.getTranslations())
			if (!t.getTranslation().isEmpty()) newTranslations.add(t);
		editFamilyForm.getFamily().setTranslation(newTranslations);
		
		//set family name using the default language 
		for (Translation t: editFamilyForm.getFamily().getTranslation()) {
			if (t.getLocale().equals(supportedLocaleService.getPrimaryLocale())) {
				editFamilyForm.getFamily().setName(t.getTranslation());
				break;
			}
		}
		
		this.instModFamServ.deleteInstrumentModelFamily(editFamilyForm.getFamily());
		
		return "trescal/core/instrumentmodel/family/viewfamily";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String processFormOops(@Validated @ModelAttribute("editfamilyform") EditFamilyForm editFamilyForm, BindingResult result) {
		//This should never happen
		return "trescal/core/instrumentmodel/family/editfamily";
	}

}
