package org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.CylindricalStandard;

public interface CylindricalStandardDao extends BaseDao<CylindricalStandard, Double> {}