package org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.ThreadUoM;

public interface ThreadUoMDao extends BaseDao<ThreadUoM, Integer> {}