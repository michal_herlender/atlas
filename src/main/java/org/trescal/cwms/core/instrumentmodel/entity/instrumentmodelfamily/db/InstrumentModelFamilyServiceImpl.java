package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.referential.dto.TMLFamilyDTO;
import org.trescal.cwms.spring.model.InstrumentModelFamilyJsonDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Service("InstrumentModelFamilyService")
public class InstrumentModelFamilyServiceImpl extends BaseServiceImpl<InstrumentModelFamily, Integer> implements InstrumentModelFamilyService {

	@Autowired
	private InstrumentModelFamilyDao instrumentModelFamilyDao;

	public void createInstrumentModelFamily(InstrumentModelFamily instrumentModelFamily) {
		this.instrumentModelFamilyDao.persist(instrumentModelFamily);

	}

	public boolean isDuplicate(String name) {
		return this.instrumentModelFamilyDao.existsByName(name);
	}

	@Override
	public List<InstrumentModelFamilyJsonDTO> getAllFamilies(Locale userLocale) {
		return this.instrumentModelFamilyDao.getAllFamilies(userLocale);
	}

	@Override
	public List<InstrumentModelFamilyJsonDTO> getAllForDomain(Locale userLocale, int domainid) {
		return this.instrumentModelFamilyDao.getAllForDomain(userLocale, domainid);
	}

	@Override
	public InstrumentModelFamily getFamily(int id) {
		return this.instrumentModelFamilyDao.find(id);
	}

	@Override
	public void updateInstrumentModelFamily(InstrumentModelFamily family) {
		this.instrumentModelFamilyDao.update(family);

	}

	@Override
	public void deleteInstrumentModelFamily(InstrumentModelFamily family) {
		this.instrumentModelFamilyDao.remove(family);

	}

	@Override
	public int getResultCountExact(String translation, Locale locale, int excludeId) {
		return this.instrumentModelFamilyDao.getResultCountExact(translation, locale, excludeId);
	}

	@Override
	public List<KeyValueIntegerString> getAllKeyValues(String familyFragment, DomainType domainType,
			Locale searchLanguage, int maxResults) {
		return instrumentModelFamilyDao.getAllKeyValues(familyFragment, domainType, searchLanguage, maxResults);
	}

	@Override
	public InstrumentModelFamily getTMLFamily(int tmlid) {
		return this.instrumentModelFamilyDao.getTMLFamily(tmlid);
	}

	public void createTMLInstrumentModelFamily(TMLFamilyDTO dto) throws Exception {
		this.instrumentModelFamilyDao.insertTMLInstrumentModelFamily(dto);

	}

	@Override
	public void updateTMLInstrumentModelFamily(TMLFamilyDTO dto) throws Exception {
		this.instrumentModelFamilyDao.updateTMLFamily(dto);

	}

	@Override
	public void deleteTMLInstrumentModelFamily(TMLFamilyDTO dto) throws Exception {
		this.instrumentModelFamilyDao.deleteTMLFamily(dto);

	}

	@Override
	protected BaseDao<InstrumentModelFamily, Integer> getBaseDao() {
		return this.instrumentModelFamilyDao;
	}
}
