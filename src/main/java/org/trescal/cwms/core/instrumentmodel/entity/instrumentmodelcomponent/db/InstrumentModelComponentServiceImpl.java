package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent.db;

import org.springframework.validation.BindException;
import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;
import org.trescal.cwms.core.instrumentmodel.entity.component.db.ComponentService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent.InstrumentModelComponent;
import org.trescal.cwms.core.instrumentmodel.form.genericentityvalidator.InstrumentModelComponentValidator;

public class InstrumentModelComponentServiceImpl implements InstrumentModelComponentService
{
	InstrumentModelComponentDao InstrumentModelComponentDao;
	private InstrumentModelComponentValidator validator;
	private InstrumentModelService instServ;
	private ComponentService compServ;

	public void deleteInstrumentModelComponent(InstrumentModelComponent instrumentmodelcomponent)
	{
		this.InstrumentModelComponentDao.remove(instrumentmodelcomponent);
	}

	@Override
	public ResultWrapper deleteModelComponent(int id)
	{
		InstrumentModelComponent modelComp = this.findInstrumentModelComponent(id);
		
		if (modelComp == null)
		{
			return new ResultWrapper(false, "Could not find component", null, null);
		}
		else
		{
			Component comp = modelComp.getComponent();
			this.deleteInstrumentModelComponent(modelComp);
			return new ResultWrapper(true, "", comp, null);
		}
	}

	public InstrumentModelComponent findInstrumentModelComponent(int id)
	{
		return this.InstrumentModelComponentDao.find(id);
	}

	public List<InstrumentModelComponent> getAllInstrumentModelComponents()
	{
		return this.InstrumentModelComponentDao.findAll();
	}

	@Override
	public List<Component> getAllUnAttachedComponents(int modelid)
	{
		return this.InstrumentModelComponentDao.getAllUnAttachedComponents(modelid);
	}

	@Override
	public List<InstrumentModelComponent> getInstrumentModelComponents(int modelid, int componentid, Integer id)
	{
		return this.InstrumentModelComponentDao.getInstrumentModelComponents(modelid, componentid, id);
	}

	@Override
	public ResultWrapper insert(int modelid, int componentid, boolean includeByDefault)
	{
		InstrumentModelComponent comp = new InstrumentModelComponent();
		comp.setIncludeByDefault(includeByDefault);
		comp.setComponent(this.compServ.get(componentid));
		comp.setModel(this.instServ.findInstrumentModel(modelid));

		if (comp.getModel() == null)
		{
			return new ResultWrapper(false, "No model found", null, null);
		}
		else if (comp.getComponent() == null)
		{
			return new ResultWrapper(false, "No component found", null, null);
		}
		else
		{
			BindException bindException = new BindException(comp, "comp");
			this.validator.validate(comp, bindException);
			if (!bindException.hasErrors())
			{
				this.insertInstrumentModelComponent(comp);
			}
			return new ResultWrapper(bindException, comp);
		}
	}

	public void insertInstrumentModelComponent(InstrumentModelComponent InstrumentModelComponent)
	{
		this.InstrumentModelComponentDao.persist(InstrumentModelComponent);
	}

	public void saveOrUpdateInstrumentModelComponent(InstrumentModelComponent instrumentmodelcomponent)
	{
		this.InstrumentModelComponentDao.saveOrUpdate(instrumentmodelcomponent);
	}

	public void setCompServ(ComponentService compServ)
	{
		this.compServ = compServ;
	}

	public void setInstrumentModelComponentDao(InstrumentModelComponentDao InstrumentModelComponentDao)
	{
		this.InstrumentModelComponentDao = InstrumentModelComponentDao;
	}

	public void setInstServ(InstrumentModelService instServ)
	{
		this.instServ = instServ;
	}

	public void setValidator(InstrumentModelComponentValidator validator)
	{
		this.validator = validator;
	}

	@Override
	public ResultWrapper updateIncludeByDefault(int id, boolean includeByDefault)
	{
		InstrumentModelComponent comp = this.findInstrumentModelComponent(id);
		if (comp == null)
		{
			return new ResultWrapper(false, "Could not find component", null, null);
		}
		else
		{
			comp.setIncludeByDefault(includeByDefault);
			this.updateInstrumentModelComponent(comp);
			return new ResultWrapper(true, "", comp, null);
		}
	}

	public void updateInstrumentModelComponent(InstrumentModelComponent InstrumentModelComponent)
	{
		this.InstrumentModelComponentDao.update(InstrumentModelComponent);
	}
}