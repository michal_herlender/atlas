package org.trescal.cwms.core.instrumentmodel.entity.modelwebresource.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.entity.modelwebresource.ModelWebResource;

public interface ModelWebResourceDao extends BaseDao<ModelWebResource, Integer> {
	
	List<ModelWebResource> findModelWebResource(String url, int modelid);
}