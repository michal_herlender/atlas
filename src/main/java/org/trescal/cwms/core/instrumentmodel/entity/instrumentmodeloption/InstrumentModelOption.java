package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeloption;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.option.Option;

@Entity
@Table(name = "instmodeloption")
public class InstrumentModelOption {
	private int instrumentmodeloptionid;
	
	private InstrumentModel instrumentmodel;
	
	private Option option;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getInstrumentmodeloptionid() {
		return instrumentmodeloptionid;
	}
	public void setInstrumentmodeloptionid(int instrumentmodeloptionid) {
		this.instrumentmodeloptionid = instrumentmodeloptionid;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "instmodelid", nullable = false)
	public InstrumentModel getInstrumentModel() {
		return instrumentmodel;
	}
	
	public void setInstrumentModel(InstrumentModel instrumentmodel) {
		this.instrumentmodel = instrumentmodel;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "optionid", nullable = false)
	public Option getOption() {
		return option;
	}
	
	public void setOption(Option option) {
		this.option = option;
	}
}
