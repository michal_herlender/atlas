package org.trescal.cwms.core.instrumentmodel.entity.range;

import java.util.Comparator;

import org.trescal.cwms.core.instrument.entity.instrumentrange.InstrumentRange;

public class RangeComparator implements Comparator<InstrumentRange>
{
	@Override
	public int compare(InstrumentRange r1, InstrumentRange r2)
	{
		// same uom
		if (r1.getUom().getId() == r2.getUom().getId())
		{
			// compare ids
			return ((Integer) r1.getId()).compareTo(r2.getId());
		}
		// different uom
		else
		{
			return ((Integer) r1.getUom().getId()).compareTo(r2.getUom().getId());
		}
	}
}
