package org.trescal.cwms.core.instrumentmodel.entity.option.db;

import java.text.MessageFormat;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.instrumentmodel.entity.option.Option;
import org.trescal.cwms.core.instrumentmodel.entity.option.Option_;
import org.trescal.cwms.core.referential.dto.TMLOptionDTO;

@Repository("OptionDao")
public class OptionDaoImpl extends BaseDaoImpl<Option, Integer> implements OptionDao {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	protected Class<Option> getEntity() {
		return Option.class;
	}
	
	@Override
	public Option getTMLOption(int tmlModelid,String code)
	{
		return getFirstResult(cb -> {
			CriteriaQuery<Option> cq = cb.createQuery(Option.class);
			Root<Option> root = cq.from(Option.class);
			
			cq.where(cb.and(cb.equal(root.get(Option_.modeltmlid), tmlModelid),
					        cb.equal(root.get(Option_.code), code)));
			
			return cq;
		}).orElse(null);
	}
	
	@Override
	public Option getTMLOption(int tmlid)
	{
		return getFirstResult(cb -> {
			CriteriaQuery<Option> cq = cb.createQuery(Option.class);
			Root<Option> root = cq.from(Option.class);
			
			cq.where(cb.equal(root.get(Option_.tmlid), tmlid));
			
			return cq;
		}).orElse(null);
	}
	
	@Override
	public void insertTMLOption(TMLOptionDTO dto) throws Exception
	{
		// First try to find if an option already exists with the same code and not linked to TML
//		Criteria criteria = getSession()
//				.createCriteria(Option.class)
//				.add(Restrictions.eq("modeltmlid", dto.getModelTMLID()))
//				.add(Restrictions.eq("code", dto.getCode()));
//		
//		List<Option> existingOptionList = criteria.list();
//		
//		Option existingOption = null;
//		
//		if (existingOptionList != null && existingOptionList.size() > 0)
//		{
//			for (Option op : existingOptionList) 
//			{
//				if ((op.getTmlid() == null) || (op.getTmlid() != null && op.getTmlid() != dto.getTMLID()))
//				{
//					existingOption = op;
//					break;
//				}
//			}
//		}	
//		
//		if (existingOption == null)
//		{
			Option option = new Option();
			option.setCode(dto.getCode());
			option.setName(dto.getDefaultName());
			option.setModeltmlid(dto.getModelTMLID());
			option.setTmlid(dto.getTMLID());
			option.setActive(dto.isIsEnabled());
						
			logger.debug("Inserting: " + option.toFullString());
			this.persist(option);
			// Add this to avoid locks
//			getSession().clear();
			logger.debug("Inserted: " + option.toFullString());
//		}
//		else
//		{
//			if (existingOption.getTmlid() != null && !existingOption.getTmlid().equals(dto.getTMLID()))
//			{
//				throw new Exception(MessageFormat.format("Error while inserting, the option {0} already exists with a different TMLID.",dto.getCode()));
//			}
//			else				
//			{
//				if (existingOption.getTmlid() == null)
//				{
//					existingOption.setTmlid(dto.getTMLID());
//					getSession().update(existingOption);
//				}
//				
//				if (dto.isIsEnabled())
//				{
//					this.updateTMLOption(dto);
//				}
//				else
//				{
//					this.deleteTMLOption(dto);
//				}
//			}
//		}
	}

	@Override
	public void updateTMLOption(TMLOptionDTO dto) throws Exception
	{
		Option option = this.getTMLOption(dto.getTMLID());
		
//		// First try to find if a family already exists with the same name and not linked to TML
//		Criteria criteria = getSession()
//				.createCriteria(Option.class)
//				.add(Restrictions.eq("modeltmlid", dto.getModelTMLID()))
//				.add(Restrictions.eq("code", dto.getCode()))
//				.add(Restrictions.ne("optionid", option.getOptionid()));
//		
//		List<Option> existingOptionList = criteria.list();
//		
//		Option existingOption = null;
//		
//		if (existingOptionList != null && existingOptionList.size() > 0)
//		{
//			for (Option op : existingOptionList) 
//			{
//				if ((op.getTmlid() == null) || (op.getTmlid() != null && op.getTmlid() != dto.getTMLID()))
//				{
//					existingOption = op;
//					break;
//				}
//			}
//		}	
//		
//		if (existingOption == null)
//		{
			option.setCode(dto.getCode());
			option.setName(dto.getDefaultName());
			option.setModeltmlid(dto.getModelTMLID());
			option.setTmlid(dto.getTMLID());
			option.setActive(dto.isIsEnabled());
				
			logger.debug("Updating: " + option.toFullString());
			this.merge(option);
			// Add this to avoid locks
			logger.debug("Updated: " + option.toFullString());
//		}
//		else
//		{
//			throw new Exception(MessageFormat.format("Error while updating, the option {0} already exists with a different TMLID.",dto.getCode()));
//		}
	}
	
	@Override
	public void deleteTMLOption(TMLOptionDTO dto) throws Exception
	{
		// Check if this option is already used by active instrument
		Integer count = getCount(cb -> cq -> {
			Root<Option> root = cq.from(Option.class);
			Join<Option, ModelRange> modelRangeJoin = root.join(Option_.modelRanges);
			
			cq.where(cb.equal(root.get(Option_.tmlid), dto.getTMLID()));
			
			return Triple.of(root.get(Option_.optionid), null, null);
		});
		
		if (count <= 0)
		{
			Option option = this.getTMLOption(dto.getTMLID());
			
			option.setActive(dto.isIsEnabled());

			logger.debug("Deleting: " + option.toFullString());
			this.merge(option);
			// Add this to avoid locks
			logger.debug("Deleted: " + option.toFullString());
		}
		else				
		{
			String errorMessage = MessageFormat.format("Error while deleting, the option {0} is used by at least 1 active instrument.",dto.getDefaultName());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		}
	}
}