package org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.instrumentmodel.dto.ServiceCapabilityForm;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceCapabilityServiceImpl extends BaseServiceImpl<ServiceCapability, Integer>
    implements ServiceCapabilityService {

    @Autowired
    private SubdivService subdivService;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private InstrumentModelService instrumentModelService;
    @Autowired
    private ServiceCapabilityDao dao;
    @Autowired
    private CapabilityService capabilityService;
    @Autowired
    private CalibrationProcessService calibrationProcessService;

    @Override
    protected BaseDao<ServiceCapability, Integer> getBaseDao() {
        return dao;
    }

    @Override
    public ServiceCapability getCalService(InstrumentModel model, CalibrationType calType, Subdiv subDiv) {
        return dao.getCalServiceCapability(model, calType, subDiv);
    }

	@Override
	public ServiceCapability getCalService(int modelid, Integer serviceTypeId, Integer businessSubdivId) {
		InstrumentModel model = instrumentModelService.get(modelid);
		CalibrationType calType = serviceTypeService.get(serviceTypeId).getCalibrationType();
		Subdiv subDiv = subdivService.get(businessSubdivId);
		return dao.getCalServiceCapability(model, calType, subDiv);
	}

	@Override
	public List<ServiceCapability> find(Integer modelId, Integer serviceTypeId, CostType costType,
			Integer businessSubdivId) {
		return dao.find(modelId, serviceTypeId, costType, businessSubdivId);
	}

	@Override
	public boolean exists(Integer modelId, CostType costType, Integer serviceTypeId,
			Integer businessSubdivId) {
		return dao.find(modelId, serviceTypeId, costType, businessSubdivId).size() > 0;
	}

	/**
	 * 
	 * @param instrumentModelCaltype = key : instrument model id, value : cal type
	 *                               id
	 * @param businessSubdivId
	 * @return = triple <instrument model id, cal type id, servicetype>
	 */
	@Override
	public List<Triple<Integer, Integer, ServiceCapability>> getCalServiceCapabilities(
			MultiValuedMap<Integer, Integer> instrumentModelCaltype, Integer businessSubdivId) {

		List<Triple<Integer, Integer, ServiceCapability>> res = new ArrayList<>();
		List<ServiceCapability> serviceCapabilites = dao.getCalServiceCapabilities(instrumentModelCaltype,
				businessSubdivId);

		for (ServiceCapability sc : serviceCapabilites) {
			res.add(new ImmutableTriple<>(sc.getInstrumentModel().getModelid(), sc.getCalibrationType().getCalTypeId(),
					sc));
		}

		return res;
	}

	@Override
	public void createServiceCapability(ServiceCapabilityForm dto) {
		ServiceCapability service = new ServiceCapability();
		service.setCostType(CostType.CALIBRATION);
		
		if(dto.getServiceTypeId() != null){
			service.setCalibrationType(this.serviceTypeService.get(dto.getServiceTypeId()).getCalibrationType());
		}
		if(dto.getProcedureId() != null){
            service.setCapability(this.capabilityService.get(dto.getProcedureId()));
		}
		if(dto.getProcessId()!=null){
			service.setCalibrationProcess(this.calibrationProcessService.get(dto.getProcessId()));
		}
		service.setOrganisation(this.subdivService.get(dto.getSubdivId()));
		service.setInstrumentModel(this.instrumentModelService.get(dto.getModelId()));
		
		service.setChecksheet(dto.getCheckSheet());
		this.save(service);
	}
	
	@Override
	public void updateServiceCapability(ServiceCapabilityForm dto) {
		// Edited record is automatically saved by JPA
		ServiceCapability service = this.get(dto.getId());
		
		if(dto.getProcedureId() != null){
            service.setCapability(this.capabilityService.get(dto.getProcedureId()));
		}
		
		if(dto.getProcessId() != null){
			service.setCalibrationProcess(this.calibrationProcessService.get(dto.getProcessId()));
		}else{
			service.setCalibrationProcess(null);
		}
		
		service.setChecksheet(dto.getCheckSheet());
	}
}