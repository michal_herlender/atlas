package org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource.MfrWebResource;

public interface MfrWebResourceDao extends BaseDao<MfrWebResource, Integer> {
	
	List<MfrWebResource> findMfrWebResource(String url, int mfrid);
}