package org.trescal.cwms.core.instrumentmodel.controller.domain;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.domain.db.InstrumentModelDomainService;
import org.trescal.cwms.core.instrumentmodel.form.EditDomainForm;
import org.trescal.cwms.core.instrumentmodel.form.EditDomainValidator;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;


@Controller
@RequestMapping(value="editinstrumentmodeldomain.htm")
public class EditDomainController {
	
	private Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	EditDomainValidator editDomainValidator;
	
	@Autowired 
	private InstrumentModelDomainService instModDomainServ;
	
	@Autowired
	private SupportedLocaleService supportedLocaleService;

	@ModelAttribute("editdomainform")
	public EditDomainForm createEditDomainForm() {
		
		EditDomainForm form = new EditDomainForm();
		
		SortedSet<Translation> translations = new TreeSet<Translation>();
		for (Locale locale: supportedLocaleService.getSupportedLocales())
			translations.add(new Translation(locale, ""));
		form.setTranslations(translations);
		logger.debug("Form Translation: " + translations);
		
		return form;
		
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String displayForm(@ModelAttribute("editdomainform") EditDomainForm form, @RequestParam(value="id", required=false) Integer id){	
		
		Map<Locale, String> translationMap = new HashMap<Locale, String>();
		
		if(id != null) {
			InstrumentModelDomain domain = instModDomainServ.getDomain(id);
			form.setDomain(domain);

			for (Translation t: domain.getTranslation())
				translationMap.put(t.getLocale(), t.getTranslation());
			
			SortedSet<Translation> translations = new TreeSet<Translation>();
			for (Translation ft: form.getTranslations())
				translations.add(new Translation(ft.getLocale(), translationMap.containsKey(ft.getLocale()) ? translationMap.get(ft.getLocale()) : ""));
			form.setTranslations(translations);			
			
			logger.debug("Get Translation: " + form.getTranslations());
				
		}

		return "trescal/core/instrumentmodel/domain/editdomain";
	}
	
	@RequestMapping(params = "save", method = RequestMethod.POST)
	public String processFormSave(@Validated @ModelAttribute("editdomainform") EditDomainForm editDomainForm, BindingResult result){
		editDomainValidator.validate(editDomainForm, result);
		if(result.hasErrors()) {
			return "trescal/core/instrumentmodel/domain/editdomain";
		}
		
		// Remove any empty translations
		SortedSet<Translation> newTranslations = new TreeSet<Translation>();
		for(Translation t : editDomainForm.getTranslations())
			if (!t.getTranslation().isEmpty()) newTranslations.add(t);
		editDomainForm.getDomain().setTranslation(newTranslations);

		
		//default language 
		for (Translation t: editDomainForm.getDomain().getTranslation()) {
			if (t.getLocale().equals(supportedLocaleService.getPrimaryLocale())) {
				editDomainForm.getDomain().setName(t.getTranslation());
				break;
			}
		}
		
		if(editDomainForm.getDomain().getDomainid() > 0) {
			
			this.instModDomainServ.updateInstrumentModelDomain(editDomainForm.getDomain());
			
		} else {
			
			this.instModDomainServ.createInstrumentModelDomain(editDomainForm.getDomain());
			
		}
		return "trescal/core/instrumentmodel/domain/viewdomain";
	}
	
	@RequestMapping(params = "delete", method = RequestMethod.POST)
	public String processFormDelete(@Validated @ModelAttribute("editdomainform") EditDomainForm editDomainForm, BindingResult result){
		editDomainValidator.validate(editDomainForm, result);
		if(result.hasErrors() || editDomainForm.getDomain().getDomainid() == 0) {
			return "trescal/core/instrumentmodel/domain/editdomain";
		}

		// Remove any empty translations from the form
		SortedSet<Translation> newTranslations = new TreeSet<Translation>();
		for(Translation t : editDomainForm.getTranslations())
			if (!t.getTranslation().isEmpty()) newTranslations.add(t);
		editDomainForm.getDomain().setTranslation(newTranslations);
		
		//set family name using the default language 
		for (Translation t: editDomainForm.getDomain().getTranslation()) {
			if (t.getLocale().equals(supportedLocaleService.getPrimaryLocale())) {
				editDomainForm.getDomain().setName(t.getTranslation());
				break;
			}
		}
		
		this.instModDomainServ.deleteInstrumentModelDomain(editDomainForm.getDomain());
		
		return "trescal/core/instrumentmodel/domain/viewdomain";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String processFormOops(@Validated @ModelAttribute("editdomainform") EditDomainForm editDomainForm, BindingResult result) {
		//This should never happen
		logger.debug("Something went wrong");
		return "trescal/core/instrumentmodel/domain/editdomain";
	}

}
