package org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;

@Service("CharacteristicLibraryService")
public class CharacteristicLibraryServiceImpl extends BaseServiceImpl<CharacteristicLibrary, Integer> implements CharacteristicLibraryService
{
	@Autowired
	private CharacteristicLibraryDao characteristicLibraryDao;
	
	@Override
	protected BaseDao<CharacteristicLibrary, Integer> getBaseDao() {
		return characteristicLibraryDao;
	}
	
	@Override
	public CharacteristicLibrary getByCharacteristicDescriptionIdAndTmlId(Integer characteristicDescriptionId, Integer tmlId) {
		return getByCharacteristicDescriptionIdAndTmlId(characteristicDescriptionId, tmlId);
	}
	
	@Override
	public ResultWrapper getLibrariesByCharacteristicDescription(int id) {
		return this.characteristicLibraryDao.getLibrariesByCharacteristicDescription(id);
	}
	
	@Override
	public ResultWrapper getLibrariesByModelRange(int id) {
		return this.characteristicLibraryDao.getLibrariesByModelRange(id);
	}
}