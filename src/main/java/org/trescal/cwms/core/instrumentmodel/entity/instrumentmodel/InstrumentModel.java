package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel;

import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.documents.images.image.ImageComparator;
import org.trescal.cwms.core.documents.images.instrumentmodelimage.InstrumentModelImage;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.hiremodel.HireModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent.InstrumentModelComponent;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.InstrumentModelPartOf;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.InstrumentModelPartOfBaseUnitComparator;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.InstrumentModelPartOfModuleComparator;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.modelnote.ModelNote;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.ModelRangeCharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.modelwebresource.ModelWebResource;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CompanyModelCalReq;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.system.entity.translation.Translation;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name = "instmodel")
@Setter
public class InstrumentModel extends Versioned implements NoteAwareEntity, ComponentEntity {
	private Contact addedBy;
	private LocalDate addedOn;
	private Set<InstrumentModelComponent> components;
	private Description description;
	private File directory;
	private Set<HireModel> hireModels;
	private SortedSet<InstrumentModelImage> images;
	private Mfr mfr;
	private String model;
	private Set<CatalogPrice> catalogPrices;
	private Set<ServiceCapability> serviceCapabilities;
	private int modelid;
	private ModelMfrType modelMfrType;
	private InstrumentModelType modelType;
	private Set<ModelNote> notes;
	private Boolean quarantined = false;
	private Set<Quotationitem> quoteItems;
	private Integer tmlid;
	private SalesCategory salesCategory;
	private List<ModelRange> ranges;
	private List<ModelRangeCharacteristicLibrary> libraryValues;
	/**
	 * Defines a list of all {@link InstrumentModelPartOf} relationships where this
	 * instmodel is the base unit.
	 */
	private Set<InstrumentModelPartOf> thisBaseUnitsModules;
	/**
	 * Defines a list of all {@link InstrumentModelPartOf} relationships where this
	 * instmodel is the module or function
	 */
	private Set<InstrumentModelPartOf> thisModulesBaseUnits;
	private Set<ModelWebResource> webResources;
	private Set<Translation> nameTranslations;
	private List<CompanyModelCalReq> companyModelCalReqs;

	private List<CompanyModelCalReq> companyModelCalReq;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "salescategoryid")
    public SalesCategory getSalesCategory() {
        return salesCategory;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "addedby")
    public Contact getAddedBy() {
        return this.addedBy;
    }

    @Column(name = "addedon", columnDefinition = "date")
    public LocalDate getAddedOn() {
        return this.addedOn;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "model")
    public Set<InstrumentModelComponent> getComponents() {
        return this.components;
    }

    @Override
	@Transient
	public Contact getDefaultContact() {
		return null;
	}

	/**
	 * This method returns the complete description for the instrument
	 * 
	 * @return {@link String} complete model for instrument Deprecated 2015-09-18 GB
	 *         - multi-language version is now in InstrumentModelService which
	 *         provides access to the default locale.
	 */
	@Transient
	@Deprecated
	public String getDefinitiveModel() {
		return this.getDefinitiveModel(null);
	}

	@Transient
	@Deprecated
	public String getDefinitiveModel(String instrumentRange) {
		String range = ((instrumentRange == null) ? "" : instrumentRange);
		if (range.trim().isEmpty() && this.ranges != null && this.ranges.size() > 0) {
			range = this.ranges.iterator().next().toString().concat(" ");
		}

		if (this.getModelMfrType().isMfrRequiredForModel()) {
			return this.getMfr().getName() + " " + range + this.getModel() + " "
					+ this.getDescription().getDescription();
		} else {
			return range + this.getModel() + " " + this.getDescription().getDescription();
		}
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "descriptionid", nullable = false)
	public Description getDescription() {
		return this.description;
	}

	@Transient
	@Override
	public File getDirectory() {
		return this.directory;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "instmodel", cascade=CascadeType.ALL, orphanRemoval=true)
	public Set<HireModel> getHireModels() {
		return this.hireModels;
	}
	
	@Override
	@Transient
	public String getIdentifier() {
		return String.valueOf(this.modelid);
	}

	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	@OneToMany(mappedBy = "model", fetch = FetchType.LAZY)
	@SortComparator(ImageComparator.class)
	public SortedSet<InstrumentModelImage> getImages() {
		return this.images;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "mfrid", nullable = false)
	public Mfr getMfr() {
		return this.mfr;
	}

	@Length(max = 100)
	@Column(name = "model", length = 100)
	public String getModel() {
		return this.model;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "modelid", nullable = false)
	@Type(type = "int")
	public int getModelid() {
		return this.modelid;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "modelmfrtype", nullable = false)
	public ModelMfrType getModelMfrType() {
		return this.modelMfrType;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modeltypeid", nullable = false)
	public InstrumentModelType getModelType() {
		return this.modelType;
	}

	@Override
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "model")
	@SortComparator(NoteComparator.class)
	public Set<ModelNote> getNotes() {
		return this.notes;
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity() {
		return null;
	}
	
	@Transient
	public Integer getPrivateActiveNoteCount() {
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount() {
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount() {
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount() {
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}

	@NotNull
	@Column(name = "quarantined", nullable = false, columnDefinition="bit")
	public Boolean getQuarantined() {
		return quarantined;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "model")
	public Set<Quotationitem> getQuoteItems() {
		return this.quoteItems;
	}

	/**
	 * Note - GB 2019-11-17 Changed from set to list, to avoid potential compareTo()
	 * issues Plan to add orphan removal, after refactoring complete
	 */
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "model")
	public List<ModelRange> getRanges() {
		return this.ranges;
	}
	
	/**
	 * Note - GB 2019-11-17 Changed from set to list, to avoid potential compareTo()
	 * issues Plan to add orphan removal, after refactoring complete
	 */
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "model")
	public List<ModelRangeCharacteristicLibrary> getLibraryValues() {
		return this.libraryValues;
	}
	
	@Transient
	public Map<CharacteristicDescription, List<CharacteristicLibrary>> getLibraryCharacteristics() {
		return getLibraryValues().stream().map(ModelRangeCharacteristicLibrary::getCharacteristicLibrary)
				.collect(Collectors.groupingBy(CharacteristicLibrary::getCharacteristicDescription));
	}
	
	@Override
	@Transient
	public List<Email> getSentEmails() {
		return null;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "base")
	@SortComparator(InstrumentModelPartOfModuleComparator.class)
	public Set<InstrumentModelPartOf> getThisBaseUnitsModules() {
		return this.thisBaseUnitsModules;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "module")
	@SortComparator(InstrumentModelPartOfBaseUnitComparator.class)
	public Set<InstrumentModelPartOf> getThisModulesBaseUnits() {
		return this.thisModulesBaseUnits;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "model")
	public Set<ModelWebResource> getWebResources() {
		return this.webResources;
	}

	@Override
	@Transient
	public boolean isAccountsEmail() {
		return false;
	}

	@Override
	public void setSentEmails(List<Email> emails) {

	}

	@Column()
	public Integer getTmlid() {
		return tmlid;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "instrumentModel")
	public Set<CatalogPrice> getCatalogPrices() {
		return catalogPrices;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "instrumentModel")
	public Set<ServiceCapability> getServiceCapabilities() {
		return serviceCapabilities;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "model")
	public List<CompanyModelCalReq> getCompanyModelCalReqs() {
		return companyModelCalReqs;
	}
	
	@Transient
	public String getUniqueIdentifier() {
		return MessageFormat.format("{0}${1}${2}${3}${4}", this.description.getId(), this.mfr.getMfrid(),
				this.model, this.modelType, this.ranges.toString());
	}

	@Transient
	public SortedSet<ModelRange> getSortedCharacteristics() {
		return new TreeSet<>(this.ranges);
	}
	
	@Transient
	public SortedSet<ModelRange> getRangesForAddOrEdit() {
		return new TreeSet<>();
	}
	
	public void addModelRange(ModelRange modelRange) {
		this.ranges.add(modelRange);
	}

	public void removeAllModelRangeCharacteristicLibrary() {
		this.ranges.clear();
	}
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="instmodeltranslation", joinColumns=@JoinColumn(name="modelid"))
	public Set<Translation> getNameTranslations() {
		return nameTranslations;
	}

	@Transient
	public String toFullString(){
		return "InstrumentModel [id=" + modelid + ", description=" + description + ", manufacturer=" + mfr + ", tmlid="
				+ tmlid + "]";
	}

	@OneToMany(fetch = FetchType.LAZY,mappedBy = "model")
	public List<CompanyModelCalReq> getCompanyModelCalReq() {
		return companyModelCalReq;
	}

	public void setCompanyModelCalReq(List<CompanyModelCalReq> companyModelCalReq) {
		this.companyModelCalReq = companyModelCalReq;
	}
}