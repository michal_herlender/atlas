package org.trescal.cwms.core.instrumentmodel.dto;

import java.util.Date;

import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;

public class InstrumentModelPlantillasDTO {

	// Last modified information
	private Date lastModifiedInstrumentModel;
	private Date lastModifiedDescription;
	private Date lastModifiedSalesCategory;
	private Date lastModifiedFamily;
	private Date lastModifiedDomain;
	// ID information
	private Integer modelId;
	private Integer descriptionId;
	// Instrument model identification / classification
	private Boolean canSelect;
	private String type;
	private String modelMfr;
	private ModelMfrType modelMfrType;
	private String model;
	private String salesCategory;
	private String domain;
	private String family;
	private String subFamily;

	public InstrumentModelPlantillasDTO() {
	}

	public InstrumentModelPlantillasDTO(Date lastModifiedInstrumentModel, Date lastModifiedDescription,
			Date lastModifiedSalesCategory, Date lastModifiedFamily, Date lastModifiedDomain, Boolean canSelect,
			String type, String modelMfr, ModelMfrType modelMfrType, String model, Integer modelId,
			Integer descriptionId, String salesCategory, String domain, String family, String subFamily) {
		this.lastModifiedInstrumentModel = lastModifiedInstrumentModel;
		this.lastModifiedDescription = lastModifiedDescription;
		this.lastModifiedSalesCategory = lastModifiedSalesCategory;
		this.lastModifiedFamily = lastModifiedFamily;
		this.lastModifiedDomain = lastModifiedDomain;
		this.canSelect = canSelect;
		this.type = type;
		this.modelMfr = modelMfr;
		this.modelMfrType = modelMfrType;
		this.model = model;
		this.modelId = modelId;
		this.descriptionId = descriptionId;
		this.salesCategory = salesCategory;
		this.domain = domain;
		this.family = family;
		this.subFamily = subFamily;
	}

	public Date getLastModifiedInstrumentModel() {
		return lastModifiedInstrumentModel;
	}

	public Date getLastModifiedDescription() {
		return lastModifiedDescription;
	}

	public Date getLastModifiedSalesCategory() {
		return lastModifiedSalesCategory;
	}

	public Date getLastModifiedFamily() {
		return lastModifiedFamily;
	}

	public Date getLastModifiedDomain() {
		return lastModifiedDomain;
	}

	public Integer getModelId() {
		return modelId;
	}

	public String getModelMfr() {
		return modelMfr;
	}

	public ModelMfrType getModelMfrType() {
		return modelMfrType;
	}

	public String getModel() {
		return model;
	}

	public String getSalesCategory() {
		return salesCategory;
	}

	public String getDomain() {
		return domain;
	}

	public String getFamily() {
		return family;
	}

	public String getSubFamily() {
		return subFamily;
	}

	public Integer getDescriptionId() {
		return descriptionId;
	}

	public Boolean getCanSelect() {
		return canSelect;
	}

	public String getType() {
		return type;
	}

	public void setLastModifiedInstrumentModel(Date lastModifiedInstrumentModel) {
		this.lastModifiedInstrumentModel = lastModifiedInstrumentModel;
	}

	public void setLastModifiedDescription(Date lastModifiedDescription) {
		this.lastModifiedDescription = lastModifiedDescription;
	}

	public void setLastModifiedSalesCategory(Date lastModifiedSalesCategory) {
		this.lastModifiedSalesCategory = lastModifiedSalesCategory;
	}

	public void setLastModifiedFamily(Date lastModifiedFamily) {
		this.lastModifiedFamily = lastModifiedFamily;
	}

	public void setLastModifiedDomain(Date lastModifiedDomain) {
		this.lastModifiedDomain = lastModifiedDomain;
	}

	public void setModelId(Integer modelId) {
		this.modelId = modelId;
	}

	public void setModelMfr(String modelMfr) {
		this.modelMfr = modelMfr;
	}

	public void setModelMfrType(ModelMfrType modelMfrType) {
		this.modelMfrType = modelMfrType;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public void setSalesCategory(String salesCategory) {
		this.salesCategory = salesCategory;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public void setSubFamily(String subFamily) {
		this.subFamily = subFamily;
	}

	public void setDescriptionId(Integer subFamilyId) {
		this.descriptionId = subFamilyId;
	}

	public void setCanSelect(Boolean canSelect) {
		this.canSelect = canSelect;
	}

	public void setType(String type) {
		this.type = type;
	}
}