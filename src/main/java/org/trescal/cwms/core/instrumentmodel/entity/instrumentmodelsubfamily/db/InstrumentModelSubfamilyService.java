package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelsubfamily.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelsubfamily.InstrumentModelSubfamily;

public interface InstrumentModelSubfamilyService extends BaseService<InstrumentModelSubfamily, Integer> {

	InstrumentModelSubfamily getByTmlId(Integer tmlSubFamilyId);

}
