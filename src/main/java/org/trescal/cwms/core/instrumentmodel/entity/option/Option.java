package org.trescal.cwms.core.instrumentmodel.entity.option;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "modeloption")
public class Option extends Auditable {
	
	private int optionid;
	
	private String name;
	
	private String code;
	
	private Set<Translation> translation;
	
	private Date log_createdon;
	
	private Integer tmlid;
	
	private Integer modeltmlid;
	
	private Boolean active;
	
	private Set<ModelRange> modelRanges;
	
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="optiontranslation", joinColumns=@JoinColumn(name="optionid"))
	public Set<Translation> getTranslation() {
		return translation;
	}
	public void setTranslation(Set<Translation> translations) {
		this.translation = translations;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getOptionid() {
		return optionid;
	}
	public void setOptionid(int optionid) {
		this.optionid = optionid;
	}
	
	@Column(name="name", nullable=false, columnDefinition="nvarchar(4000)")
	public String getName() {
		return name;
	}
	
	@Column(name="code", nullable=false)
	public String getCode() {
		return code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@Column(name="tmlid", nullable=true)
	public Integer getTmlid() {
		return tmlid;
	}
	public void setTmlid(Integer tmlid) {
		this.tmlid = tmlid;
	}
	
	public Date getLog_createdon() {
		return log_createdon;
	}
	public void setLog_createdon(Date log_createdon) {
		this.log_createdon = log_createdon;
	}
	
	@Column(name="modeltmlid", nullable=true)
	public Integer getModeltmlid() {
		return modeltmlid;
	}
	public void setModeltmlid(Integer modeltmlid) {
		this.modeltmlid = modeltmlid;
	}
	
	@Column(name = "active", nullable = false, columnDefinition = "bit default 0")
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "modelOption")
	public Set<ModelRange> getModelRanges() {
		return this.modelRanges;
	}
	public void setModelRanges(Set<ModelRange> modelRanges) {
		this.modelRanges = modelRanges;
	}
	
	@Transient
	public String toFullString(){
		return "Option [id=" + optionid + ", code=" + code
				+ ", tmlid=" + tmlid + "]";
	}
}