package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.CanSelect;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;

@Service
public class InstrumentModelTypeServiceImpl extends BaseServiceImpl<InstrumentModelType, Integer> 
	implements InstrumentModelTypeService
{
	@Autowired
	private InstrumentModelTypeDao baseDao;

	@Override
	public InstrumentModelType findDefaultType()
	{
		return this.baseDao.findDefaultType();
	}

	@Override
	public List<InstrumentModelType> getAllInstrumentModelTypes(CanSelect canSelect)
	{
		return this.baseDao.getAllInstrumentModelTypes(canSelect);
	}

	@Override
	protected BaseDao<InstrumentModelType, Integer> getBaseDao() {
		return baseDao;
	}
}