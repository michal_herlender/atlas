package org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db;

import java.util.List;

import org.apache.commons.collections4.MultiValuedMap;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

public interface ServiceCapabilityDao extends BaseDao<ServiceCapability, Integer> {

	ServiceCapability getCalServiceCapability(InstrumentModel model, CalibrationType calType, Subdiv subDiv);

	List<ServiceCapability> find(Integer modelId, Integer serviceTypeId, CostType costType, Integer businessSubdivId);

	List<ServiceCapability> find(InstrumentModel instrumentModel, CalibrationType calibrationType, CostType costType, Subdiv organisation);

	List<ServiceCapability> getCalServiceCapabilities(MultiValuedMap<Integer, Integer> instrumentModelCaltype,
			Integer businessSubdivId);

}
