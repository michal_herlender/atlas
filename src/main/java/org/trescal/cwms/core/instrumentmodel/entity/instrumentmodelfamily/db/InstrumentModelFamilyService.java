package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.referential.dto.TMLFamilyDTO;
import org.trescal.cwms.spring.model.InstrumentModelFamilyJsonDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface InstrumentModelFamilyService {

	void createInstrumentModelFamily(InstrumentModelFamily instrumentModelFamily);

	boolean isDuplicate(String name);

	List<InstrumentModelFamilyJsonDTO> getAllFamilies(Locale userLocale);

	List<InstrumentModelFamilyJsonDTO> getAllForDomain(Locale userLocale, int domainid);

	InstrumentModelFamily getFamily(int id);

	void updateInstrumentModelFamily(InstrumentModelFamily instrumentModelFamily);

	void deleteInstrumentModelFamily(InstrumentModelFamily family);

	int getResultCountExact(String translation, Locale locale, int excludeId);

	List<KeyValueIntegerString> getAllKeyValues(String familyFragment, DomainType domainType, Locale userLocale,
			int maxResults);

	InstrumentModelFamily getTMLFamily(int tmlid);

	void createTMLInstrumentModelFamily(TMLFamilyDTO dto) throws Exception;

	void updateTMLInstrumentModelFamily(TMLFamilyDTO dto) throws Exception;

	void deleteTMLInstrumentModelFamily(TMLFamilyDTO dto) throws Exception;

}
