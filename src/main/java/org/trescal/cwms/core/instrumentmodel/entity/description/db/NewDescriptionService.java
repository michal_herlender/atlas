package org.trescal.cwms.core.instrumentmodel.entity.description.db;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.dto.DescriptionPlantillasDTO;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.referential.dto.TMLSubFamilyDTO;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.InstrumentModelDescriptionJsonDto;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface NewDescriptionService {

	String getStringForPrimaryLocale(Description description);

	Description findDescription(int id);

	DescriptionPlantillasDTO getPlantillasDescription(int id);

	PagedResultSet<DescriptionPlantillasDTO> getPlantillasDescriptions(Date afterLastModified, int resultsPerPage,
			int currentPage);

	List<Description> performQueryFragment(String descriptionFragment, Locale searchLanguage, int firstResult,
			int maxResults);

	List<Description> getDescriptions(String description, Locale searchLanguage);

	List<InstrumentModelDescriptionJsonDto> findDescriptionsForFamily(int familyid, Locale searchLanguage);

	int getResultCountFragment(String descriptionFragment, Locale searchLanguage);

	int getResultCountExact(String descriptionExact, Locale searchLanguage);

	int getResultCountExact(String descriptionExact, Locale searchLanguage, Description exclude);

	void deleteDescription(Description description);

	Description getByTmlId(Integer tmlSubFamilyId);
	
	@Deprecated
	Description getTMLDescription(int tmlid);

	void insertTMLDescription(TMLSubFamilyDTO dto) throws Exception;

	void updateTMLDescription(TMLSubFamilyDTO dto) throws Exception;

	void deleteTMLDescription(TMLSubFamilyDTO dto) throws Exception;

	List<KeyValueIntegerString> getAllKeyValues(String descriptionFragment, DomainType domainType, Locale searchLanguage, int maxResults);
}