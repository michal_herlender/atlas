package org.trescal.cwms.core.instrumentmodel.controller.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.RangeType;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.AddEditInstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelCharacteristicValidator;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.instrumentmodel.entity.uom.db.UoMService;

@Controller
@IntranetController
public class EditModelCharacteristicController {

	@Autowired
	private AddEditInstrumentModelService addEditInstrumentModelService;
	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private ModelCharacteristicValidator validator;
	@Autowired
	private UoMService unitService;

	private static final String VIEW_NAME = "trescal/core/instrumentmodel/configuration/editmodelcharacteristic";
	private static final Logger logger = LoggerFactory.getLogger(EditModelCharacteristicController.class);

	/**
	 * Builds form backing model, intended for editing a model's characteristics.
	 * 
	 * Intended for creating / editing models that are not connected to TML
	 * 
	 * Caveats:
	 * (a) Ability to add/edit options is NOT yet supported
	 * (b) Only the following characteristics are supported 
	 *     (one library value, or one free field text, one value with unit, range with unit)  
	 * (c) it's expected that these constraints are maintained for the model record 
	 * 
	 * @param modelId
	 * @return
	 */
	@ModelAttribute("form")
	public EditModelCharacteristicForm initializeForm(@RequestParam(value = "modelid", required = true) int modelId) {
		InstrumentModel instModel = getInstrumentModel(modelId);
		Map<CharacteristicDescription, List<CharacteristicLibrary>> libraryMap = instModel.getLibraryCharacteristics();
		
		List<EditModelCharacteristicDto> characteristicsDtoList = new ArrayList<>();
		Map<Integer, ModelRange> characteristicsModelRangeMap = new HashMap<>();
		List<EditModelOptionDto> optionsDtoList = new ArrayList<>();

		// Model ranges contain characteristics with values, library values contain preselected
		// Note - it's possible that subfamily changes, or that definitions change.
		// (Could display orphaned model ranges)
		for (ModelRange mr : instModel.getRanges()) {
			if (RangeType.CHARACTERISTIC.equals(mr.getCharacteristicType())) {
				if (mr.getCharacteristicDescription() != null) {
					characteristicsModelRangeMap.put(mr.getCharacteristicDescription().getCharacteristicDescriptionId(),
							mr);
				} else {
					logger.error("Unexpected empty characteristic description on model range id " + mr.getId());
				}
			} else if (RangeType.OPTION.equals(mr.getCharacteristicType())) {
				EditModelOptionDto dto = new EditModelOptionDto();
				dto.setCode(mr.getModelOption().getCode());
				dto.setIncluded(true);
				dto.setName(mr.getModelOption().getName());
				dto.setOptionId(mr.getModelOption().getOptionid());
				dto.setRangeId(mr.getId());
				optionsDtoList.add(dto);
			}
		}

		// Available characteristics depend on subfamily
		Set<CharacteristicDescription> cds = instModel.getDescription().getCharacteristicDescriptions();
		for (CharacteristicDescription cd : cds) {
			EditModelCharacteristicDto dto = new EditModelCharacteristicDto();
			dto.setCharacteristicDescriptionId(cd.getCharacteristicDescriptionId());
			ModelRange mr = characteristicsModelRangeMap.get(cd.getCharacteristicDescriptionId());
			switch (cd.getCharacteristicType()) {
			case FREE_FIELD:
				if (mr != null) {
					dto.setTextStart(mr.getMinvalue());
					dto.setTextEnd(mr.getMaxvalue());
					dto.setUomId(mr.getUom().getId());
					dto.setIncluded(true);
				}
				break;

			case LIBRARY:
				List<CharacteristicLibrary> list = libraryMap.get(cd);
				// Single value expected at most for editable models
				dto.setCharacteristicLibraryId(list == null || list.isEmpty() ? 0 : list.get(0).getCharacteristiclibraryid());
				dto.setIncluded(dto.getCharacteristicLibraryId() != 0);
				break;

			case VALUE_WITH_UNIT:
				if (mr != null) {
					dto.setDecimalStart(mr.getStart());
					dto.setDecimalEnd(mr.getEnd());
					dto.setUomId(mr.getUom().getId());
					dto.setIncluded(true);
				}
				break;

			default:
				logger.error("Unexpected type "+cd.getCharacteristicType()+" for cd id "+cd.getCharacteristicDescriptionId());
				break;
			}

			characteristicsDtoList.add(dto);
		}

		EditModelCharacteristicForm form = new EditModelCharacteristicForm();
		form.setModelId(modelId);
		form.setCharacteristics(characteristicsDtoList);
		form.setOptions(optionsDtoList);

		return form;
	}
	
	private InstrumentModel getInstrumentModel(Integer modelId) {
		InstrumentModel instModel = this.instrumentModelService.get(modelId);
		if (instModel == null) throw new IllegalArgumentException("No instrument model exists for modelid "+modelId);
		if (instModel.getTmlid() != null && instModel.getTmlid() != 0) throw new UnsupportedOperationException("TML model may not be modified");
		return instModel;
	}

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/editmodelcharacteristic.htm")
	public String referenceData(Model model, @ModelAttribute("form") EditModelCharacteristicForm form) {

		InstrumentModel instModel = getInstrumentModel(form.getModelId());
		Map<Integer, CharacteristicDescription> charDescMap =
			instModel.getDescription().getCharacteristicDescriptions().stream().
				collect(Collectors.toMap(cd -> cd.getCharacteristicDescriptionId(), cd -> cd));

		model.addAttribute("model", instModel);
		model.addAttribute("selectedTab", "configuration");
		model.addAttribute("units", unitService.getAllUoMs());
		model.addAttribute("charDescMap", charDescMap);

		return VIEW_NAME;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/editmodelcharacteristic.htm")
	public String onSubmit(Model model,
			@Validated @ModelAttribute("form") EditModelCharacteristicForm form,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData(model, form);
		}

		this.addEditInstrumentModelService.editCharacteristics(form);

		return "redirect:instrumentmodelconfig.htm?modelid=" + form.getModelId();
	}

}
