package org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;

public interface CharacteristicLibraryService extends BaseService<CharacteristicLibrary, Integer>
{
	CharacteristicLibrary getByCharacteristicDescriptionIdAndTmlId(Integer characteristicDescriptionId, Integer tmlId);
	public ResultWrapper getLibrariesByCharacteristicDescription(int id); 
	public ResultWrapper getLibrariesByModelRange(int id);	
}