package org.trescal.cwms.core.instrumentmodel.controller.domain;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.entity.domain.db.InstrumentModelDomainService;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Controller
public class SearchDomainTags {

	@Autowired
	private InstrumentModelDomainService instModDomainServ;

	public static int MAX_RESULTS = 50;

	@RequestMapping(value = "/searchdomaintags.json", method = RequestMethod.GET)
	public @ResponseBody List<KeyValueIntegerString> getTagList(
			@RequestParam(name = "domaintype", required = false) DomainType domainType,
			@RequestParam("term") String domainFragment, Locale userLocale) {
		return instModDomainServ.getAllKeyValues(domainFragment, domainType, userLocale, MAX_RESULTS);
	}
}