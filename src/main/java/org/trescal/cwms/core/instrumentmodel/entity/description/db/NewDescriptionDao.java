package org.trescal.cwms.core.instrumentmodel.entity.description.db;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.dto.DescriptionPlantillasDTO;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.referential.dto.TMLSubFamilyDTO;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.InstrumentModelDescriptionJsonDto;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface NewDescriptionDao extends BaseDao<Description, Integer> {

	List<InstrumentModelDescriptionJsonDto> findDescriptionsForFamily(int familyid, Locale searchLanguage);

	// Performs a query for a description fragment, meant for use with paged result
	// set.
	List<Description> performQueryFragment(String descriptionFragment, Locale searchLanguage, int firstResult,
			int maxResults);

	// Gets a result count for the description fragment in the specified search
	// language
	int getResultCountFragment(String descriptionFragment, Locale searchLanguage);

	// Gets a result count for the description (matching exactly) in the specified
	// search language
	int getResultCountExact(String descriptionExact, Locale searchLanguage);

	/*
	 * Gets a result count for the description search term (matching exactly) in the
	 * specified search language, excluding the specified description entity from
	 * the search.
	 */
	int getResultCountExact(String descriptionExact, Locale searchLanguage, Description exclude);

	// Utility method to get a translation for the description in the specified
	// language
	Translation getTranslationForLocaleOrNull(Description description, Locale locale);

	/*
	 * Returns list of generic label / value pairs for description fragment in
	 * specified language
	 */
	List<KeyValueIntegerString> getAllKeyValues(String descriptionFragment, DomainType domainType, Locale searchLanguage,
			int maxResults);

	List<Description> getDescriptions(String description, Locale searchLanguage);

	DescriptionPlantillasDTO getPlantillasDescription(int id);

	PagedResultSet<DescriptionPlantillasDTO> getPlantillasDescriptions(Date afterLastModified, int resultsPerPage,
			int currentPage);
	
	Description getByTmlId(Integer tmlSubFamilyId);

	// Finds a description by TMLID
	@Deprecated
	Description getTMLDescription(int tmlid);

	void insertTMLDescription(TMLSubFamilyDTO dto) throws Exception;

	// Saves a newly created description in the database
	void updateTMLDescription(TMLSubFamilyDTO dto) throws Exception;

	// Deletes a description from the database
	void deleteTMLDescription(TMLSubFamilyDTO dto) throws Exception;
}