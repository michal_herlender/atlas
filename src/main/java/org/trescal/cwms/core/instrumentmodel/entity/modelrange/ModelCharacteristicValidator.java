package org.trescal.cwms.core.instrumentmodel.entity.modelrange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.instrumentmodel.controller.configuration.EditModelCharacteristicDto;
import org.trescal.cwms.core.instrumentmodel.controller.configuration.EditModelCharacteristicForm;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.db.CharacteristicDescriptionService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class ModelCharacteristicValidator extends AbstractBeanValidator {
	
	@Autowired
	private CharacteristicDescriptionService cdService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(EditModelCharacteristicForm.class);
	}

	/**
	 * Could consider additional validation - units within ranges, etc...
	 */
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		if (!errors.hasErrors()) {
			EditModelCharacteristicForm form = (EditModelCharacteristicForm) target;
			for (int i = 0; i < form.getCharacteristics().size(); i++) {
				EditModelCharacteristicDto dto = form.getCharacteristics().get(i);
				if (dto.getIncluded()) {
					// Only validate the values which are to be saved / included
					CharacteristicDescription cd = this.cdService.get(dto.getCharacteristicDescriptionId());
					String propertyPath = "characteristics["+i+"]";
					errors.pushNestedPath(propertyPath);
					switch (cd.getCharacteristicType()) {
						case FREE_FIELD:
							super.validate(dto, errors, EditModelCharacteristicDto.FreeField.class);
							// At least one value must be set
							if ((dto.getTextStart() == null || dto.getTextStart().isEmpty()) && 
								(dto.getTextEnd() == null || dto.getTextEnd().isEmpty())) {
								errors.rejectValue("textStart", "error.value.notselected", "A value must be selected");
							}
							break;
						case LIBRARY:
							super.validate(dto, errors, EditModelCharacteristicDto.Library.class);
							break;
						case VALUE_WITH_UNIT:
							super.validate(dto, errors, EditModelCharacteristicDto.ValueWithUnit.class);
							// At least one value must be set
							if (dto.getDecimalStart() == null && dto.getDecimalEnd() == null) {
								errors.rejectValue("decimalStart", "error.value.notselected", "A value must be selected");
							}
							break;
						default:
							break;
					}
					errors.popNestedPath();
				}
			}
		}
	}
}
