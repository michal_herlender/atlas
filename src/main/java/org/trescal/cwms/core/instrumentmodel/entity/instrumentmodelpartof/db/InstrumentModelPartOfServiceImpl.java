package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.db;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.InstrumentModelPartOf;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.InstModelTools;

public class InstrumentModelPartOfServiceImpl implements InstrumentModelPartOfService
{
	InstrumentModelService instModelServ;
	InstrumentModelPartOfDao InstrumentModelPartOfDao;

	@Override
	public ResultWrapper createPartOfRelationShip(int moduleid, int baseid, boolean addAsDefault, HttpSession session)
	{
		InstrumentModel module = this.instModelServ.findInstrumentModel(moduleid);
		InstrumentModel baseUnit = this.instModelServ.findInstrumentModel(baseid);

		if ((baseUnit == null) || (module == null))
		{
			return new ResultWrapper(false, "One or more of the models requested could not be found", null, null);
		}
		else
		{
			if (!baseUnit.getModelType().getModules()
					|| !module.getModelType().getBaseUnits())
			{
				return new ResultWrapper(false, "Illegal attempt to associate a module to a non-base unit", null, null);
			}
			else if (this.findInstrumentModelPartOf(moduleid, baseid) != null)
			{
				return new ResultWrapper(false, "Illegal attempt to create a duplicate baseunit/module pairing", null, null);
			}
			else
			{
				InstrumentModelPartOf po = new InstrumentModelPartOf();
				po.setBase(baseUnit);
				po.setModule(module);
				po.setIncludedByDefault(addAsDefault);
				Locale locale = LocaleContextHolder.getLocale();
				Locale defaultLocale = (Locale) session.getAttribute(Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE);
				po.setBaseUnitName(InstModelTools.modelNameViaTranslations(baseUnit, locale, defaultLocale));
				po.setModuleName(InstModelTools.modelNameViaTranslations(module, locale, defaultLocale));
				this.insertInstrumentModelPartOf(po);
				return new ResultWrapper(true, "Instrument Model pairing created", po, null);
			}
		}
	}

	public void deleteInstrumentModelPartOf(InstrumentModelPartOf instrumentmodelpartof)
	{
		this.InstrumentModelPartOfDao.remove(instrumentmodelpartof);
	}

	@Override
	public ResultWrapper deletePartOfRelationShip(int partOfId, HttpSession session)
	{
		InstrumentModelPartOf po = this.findInstrumentModelPartOf(partOfId);
		if (po != null)
		{
			Locale locale = LocaleContextHolder.getLocale();
			Locale defaultLocale = (Locale) session.getAttribute(Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE);
			po.setBaseUnitName(InstModelTools.modelNameViaTranslations(po.getBase(), locale, defaultLocale));
			po.setModuleName(InstModelTools.modelNameViaTranslations(po.getModule(), locale, defaultLocale));
			this.deleteInstrumentModelPartOf(po);
			return new ResultWrapper(true, "", po, null);
		}
		else
		{
			return new ResultWrapper(false, "Could not find instrumentmodel part of relationship with id '"
					+ partOfId + "'", null, null);
		}
	}

	public InstrumentModelPartOf findInstrumentModelPartOf(int id)
	{
		return this.InstrumentModelPartOfDao.find(id);
	}

	@Override
	public InstrumentModelPartOf findInstrumentModelPartOf(int moduleid, int baseid)
	{
		return this.InstrumentModelPartOfDao.findInstrumentModelPartOf(moduleid, baseid);
	}

	public List<InstrumentModelPartOf> getAllInstrumentModelPartOfs()
	{
		return this.InstrumentModelPartOfDao.findAll();
	}

	public List<InstrumentModelPartOf> getModulesAvailableForBaseUnit(int baseid)
	{
		return this.InstrumentModelPartOfDao.getModulesAvailableForBaseUnit(baseid);
	}

	public void insertInstrumentModelPartOf(InstrumentModelPartOf InstrumentModelPartOf)
	{
		this.InstrumentModelPartOfDao.persist(InstrumentModelPartOf);
	}

	public void saveOrUpdateInstrumentModelPartOf(InstrumentModelPartOf instrumentmodelpartof)
	{
		this.InstrumentModelPartOfDao.saveOrUpdate(instrumentmodelpartof);
	}

	public void setInstModelServ(InstrumentModelService instModelServ)
	{
		this.instModelServ = instModelServ;
	}

	public void setInstrumentModelPartOfDao(InstrumentModelPartOfDao InstrumentModelPartOfDao)
	{
		this.InstrumentModelPartOfDao = InstrumentModelPartOfDao;
	}

	@Override
	public ResultWrapper updateIncludeByDefault(int partOfId, boolean includeByDefault)
	{
		InstrumentModelPartOf po = this.findInstrumentModelPartOf(partOfId);
		if (po != null)
		{
			po.setIncludedByDefault(includeByDefault);
			this.updateInstrumentModelPartOf(po);
			return new ResultWrapper(true, "", po, null);
		}
		else
		{
			return new ResultWrapper(false, "Could not find instrumentmodel part of relationship with id '"
					+ partOfId + "'", null, null);
		}
	}

	public void updateInstrumentModelPartOf(InstrumentModelPartOf InstrumentModelPartOf)
	{
		this.InstrumentModelPartOfDao.update(InstrumentModelPartOf);
	}
}