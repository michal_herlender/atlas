package org.trescal.cwms.core.instrumentmodel.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.instrumentmodel.CanSelect;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.AddEditInstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.db.InstrumentModelTypeService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.instrumentmodel.form.EditModelForm;
import org.trescal.cwms.core.instrumentmodel.form.EditModelValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

/**
 * Multi-function form controller used for adding and editing
 * {@link InstrumentModel} entities.
 * 
 * @author richard
 */
@Controller
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME })
public class EditModelController {
	protected final Log logger = LogFactory.getLog(getClass());
	@Autowired
	private AddEditInstrumentModelService addEditInstrumentModelService;
	@Autowired
	private InstrumentModelService instModelServ;
	@Autowired
	private InstrumentModelTypeService instModelTypeServ;
	@Autowired
	private MfrService mfrService;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private EditModelValidator validator;

	@Value("#{props['cwms.config.model.default.modelmfrtype']}")
	private ModelMfrType defaultModelMfrType;
	
	@InitBinder("command")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("command")
	protected EditModelForm formBackingObject(Locale locale,
			@RequestParam(name = "modelid", required = false, defaultValue = "0") Integer modelid)
			throws ServletException {
		EditModelForm emf = new EditModelForm();
		emf.setModelid(modelid);
		// this is a request for a new InstrumentModel, set defaults on form
		if (modelid == 0) {
			emf.setDescid(0);
			emf.setDescname("");
			emf.setInstModelTypeId(this.instModelTypeServ.findDefaultType().getInstModelTypeId());
			emf.setModelMfrType(this.defaultModelMfrType);
			emf.setQuarantined(false);
			emf.setSalesCategoryId(0);
			emf.setSalesCategoryText("");
		} else {
			// this is editing an existing InstrumentModel
			InstrumentModel model = this.instModelServ.findInstrumentModel(modelid);
			emf.setDescid(model.getDescription().getId());
			emf.setDescname(translationService.getCorrectTranslation(model.getDescription().getTranslations(), locale));
			emf.setInstModelTypeId(model.getModelType().getInstModelTypeId());
			emf.setMfrid(model.getMfr().getMfrid());
			emf.setMfrname(model.getMfr().getName());
			emf.setModelMfrType(model.getModelMfrType());
			emf.setModelName(model.getModel());
			emf.setQuarantined(model.getQuarantined());
			emf.setSalesCategoryId(model.getSalesCategory() == null ? 0 : model.getSalesCategory().getId());
			emf.setSalesCategoryText(model.getSalesCategory() == null ? ""
					: translationService.getCorrectTranslation(model.getSalesCategory().getTranslations(), locale));
		}
		return emf;
	}

	@RequestMapping(params = "submit", value = "/editmodel.htm", method = RequestMethod.POST)
	public String onSubmit(Model model, Locale locale,
			@Validated @ModelAttribute("command") EditModelForm form, BindingResult bindingResult,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		if (bindingResult.hasErrors()) {
			return referenceData(model, locale, form);
		}
		Integer instModelId = this.addEditInstrumentModelService.addEditInstrumentModel(form, username);
		model.asMap().clear();
		return "redirect:instrumentmodelconfig.htm?modelid=" + instModelId;
	}

	@RequestMapping(value = "/editmodel.htm", method = RequestMethod.GET)
	public String referenceData(Model model, Locale locale,
			@ModelAttribute("command") EditModelForm form) {
		model.addAttribute("modelmfrtypes", ModelMfrType.values());
		
		List<InstrumentModelType> modelTypes = null;
		boolean isSalesCategoryModelType = false;
		if (form.getModelid() != 0) {
			InstrumentModel instModel = this.instModelServ.get(form.getModelid());
			model.addAttribute("model", instModel);
			// If model type is selectable, allow change to other selectable types; reserved type is fixed
			if (instModel.getModelType().getCanSelect()) {
				modelTypes = this.instModelTypeServ.getAllInstrumentModelTypes(CanSelect.SELECTABLE);
			}
			else {
				modelTypes = Collections.singletonList(instModel.getModelType());
			}
			isSalesCategoryModelType = instModel.getModelType().getSalescategory();
		}
		else {
			// For new model, allow selection of any model type
			modelTypes = this.instModelTypeServ.getAllInstrumentModelTypes(CanSelect.ALL);
		}
		model.addAttribute("modelTypes", getModelTypes(modelTypes, locale));
		model.addAttribute("isSalesCategoryModelType", isSalesCategoryModelType);
		model.addAttribute("listDuplicates", getDuplicateModels(form));
		return "trescal/core/instrumentmodel/editmodel";
	}
	
	private List<InstrumentModel> getDuplicateModels(EditModelForm form) {
		Integer mfrId = form.getMfrid();
		if (ModelMfrType.MFR_GENERIC.equals(form.getModelMfrType())) {
			Mfr mfr = this.mfrService.getSystemGenericMfr();
			mfrId = mfr.getMfrid();
		}
		List<InstrumentModel> listModels = this.instModelServ.searchModelSignature(mfrId, form.getModelName(), form.getDescid(), form.getInstModelTypeId(), form.getModelid());
		return listModels;
	}
	
	private List<KeyValueIntegerString> getModelTypes(List<InstrumentModelType> list, Locale locale) {
		List<KeyValueIntegerString> result = new ArrayList<>();
		for (InstrumentModelType type : list) {
			String translation = this.translationService.getCorrectTranslation(type.getModelTypeNameTranslation(), locale);
			result.add(new KeyValueIntegerString(type.getInstModelTypeId(), translation));
		}
		
		return result;
		
	}
}