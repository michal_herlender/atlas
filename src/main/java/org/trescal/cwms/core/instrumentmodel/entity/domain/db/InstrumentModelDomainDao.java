package org.trescal.cwms.core.instrumentmodel.entity.domain.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.referential.dto.TMLDomainDTO;
import org.trescal.cwms.spring.model.InstrumentModelDomainJsonDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface InstrumentModelDomainDao extends BaseDao<InstrumentModelDomain, Integer> {

	boolean existsByName(String name);

	List<InstrumentModelDomainJsonDTO> getAllDomains();

	List<InstrumentModelDomainJsonDTO> getAllDomains(Locale searchLanguage);

	int getResultCountExact(String name, Locale searchLanguage, int excludeId);

	List<KeyValueIntegerString> getAllKeyValues(String domainFragment, DomainType domainType, Locale searchLanguage,
			int maxResults);

	InstrumentModelDomain getTMLDomain(int tmlid);

	void insertTMLInstrumentModelDomain(TMLDomainDTO dto) throws Exception;

	void updateTMLDomain(TMLDomainDTO dto) throws Exception;

	void deleteTMLDomain(TMLDomainDTO dto) throws Exception;
}