package org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;
import org.trescal.cwms.core.instrumentmodel.dimensional.form.ThreadSearchForm;
import org.trescal.cwms.core.instrumentmodel.dto.ThreadSearchResultWrapper;
import org.trescal.cwms.core.tools.PagedResultSet;

@Service("ThreadService")
public class ThreadServiceImpl extends BaseServiceImpl<Thread,Integer> implements ThreadService
{
	@Autowired
	private ThreadDao threadDao;

	@Override
	public List<Thread> findAllThreadsBySizeAndType(String type, String size)
	{
		return this.threadDao.findAllThreadsBySizeAndType(type, size);
	}

	@Override
	public List<Thread> findAllThreadsByType(String type)
	{
		return this.threadDao.findAllThreadsByType(type);
	}

	@Override
	public PagedResultSet<Thread> searchThreads(ThreadSearchForm form, PagedResultSet<Thread> prs)
	{
		return this.threadDao.searchThreads(form, prs);
	}

	public List<ThreadSearchResultWrapper> searchThreadsHQL(String searchTerm, Integer typeId)
	{
		return this.threadDao.searchThreadsHQL(searchTerm, typeId);
	}

	@Override
	protected BaseDao<Thread, Integer> getBaseDao() {
		// TODO Auto-generated method stub
		return threadDao;
	}
}