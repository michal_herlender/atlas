package org.trescal.cwms.core.instrumentmodel.entity.servicecapability;

import lombok.Setter;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Setter
@Table(name = "servicecapability")
public class ServiceCapability extends Allocated<Subdiv> {
    private int id;
    private InstrumentModel instrumentModel;
    private CostType costType;
    private CalibrationType calibrationType;
    private Capability capability;
    private CalibrationProcess calibrationProcess;
    private WorkInstruction workInstruction;
    private String checksheet;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return id;
    }
	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name = "modelid", nullable = false)
    public InstrumentModel getInstrumentModel() {
        return instrumentModel;
    }

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "costtypeid", nullable = false)
    public CostType getCostType() {
        return costType;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "calibrationtypeid", nullable = false)
    public CalibrationType getCalibrationType() {
        return calibrationType;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "capabilityId", nullable = false)
    public Capability getCapability() {
        return capability;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "calibrationprocessid", nullable = true)
    public CalibrationProcess getCalibrationProcess() {
        return calibrationProcess;
    }

    @ManyToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name = "workinstructionid", nullable = true)
    public WorkInstruction getWorkInstruction() {
        return workInstruction;
    }

    @Column(name = "checksheet", nullable = true)
    public String getChecksheet() {
        return checksheet;
    }
}
