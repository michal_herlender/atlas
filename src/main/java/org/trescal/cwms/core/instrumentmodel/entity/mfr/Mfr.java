package org.trescal.cwms.core.instrumentmodel.entity.mfr;

import java.io.File;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.SortComparator;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModelComparator;
import org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource.MfrWebResource;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

@Entity
@Table(name = "mfr", uniqueConstraints={@UniqueConstraint(columnNames="name")})
public class Mfr extends Auditable implements ComponentEntity
{
	private Boolean active = false;
	private Set<Component> components;
	private File directory;
	private Boolean genericMfr = false;
	private int mfrid;
	private Set<InstrumentModel> models;
	private String name;
	private Set<MfrWebResource> webResources;	
	private Integer tmlid;

	@OneToMany(mappedBy = "mfr", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Component> getComponents()
	{
		return this.components;
	}

	@Override
	@Transient
	public Contact getDefaultContact()
	{
		return null;
	}

	@Transient
	@Override
	public File getDirectory()
	{
		return this.directory;
	}

	@Override
	@Transient
	public String getIdentifier()
	{
		return String.valueOf(this.mfrid);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "mfrid", nullable = false)
	@Type(type = "int")
	public int getMfrid()
	{
		return this.mfrid;
	}

	@OneToMany(mappedBy = "mfr", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(InstrumentModelComparator.class)
	public Set<InstrumentModel> getModels()
	{
		return this.models;
	}

	@NotNull
	@Length(min = 1, max = 100)
	@Column(name = "name", length = 100, nullable = false)
	public String getName()
	{
		return this.name;
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity()
	{
		return null;
	}

	@Override
	@Transient
	public List<Email> getSentEmails()
	{
		return null;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "mfr")
	public Set<MfrWebResource> getWebResources()
	{
		return this.webResources;
	}

	@Override
	@Transient
	public boolean isAccountsEmail()
	{
		return false;
	}

	@NotNull
	@Column(name = "active", nullable = false, columnDefinition="tinyint")
	public Boolean getActive()
	{
		return this.active;
	}

	@NotNull
	@Column(name = "genericmfr", nullable = false, columnDefinition="tinyint")
	public Boolean getGenericMfr()
	{
		return this.genericMfr;
	}

	public void setActive(Boolean active)
	{
		this.active = active;
	}

	public void setComponents(Set<Component> components)
	{
		this.components = components;
	}

	@Column(name="tmlid", nullable=true)
	public Integer getTmlid() {
		return tmlid;
	}
	public void setTmlid(Integer tmlid) {
		this.tmlid = tmlid;
	}
	
	@Override
	public void setDirectory(File file)
	{
		this.directory = file;
	}

	public void setGenericMfr(Boolean genericMfr)
	{
		this.genericMfr = genericMfr;
	}

	public void setMfrid(int id)
	{
		this.mfrid = id;
	}

	public void setModels(Set<InstrumentModel> models)
	{
		this.models = models;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public void setSentEmails(List<Email> emails)
	{

	}

	public void setWebResources(Set<MfrWebResource> webResources)
	{
		this.webResources = webResources;
	}
	
	@Transient
	public String toFullString(){
		return "Mfr [mfrid=" + mfrid + ", name=" + name
				+ ", active=" + active
				+ ", tmlid=" + tmlid + "]";
	}
}
