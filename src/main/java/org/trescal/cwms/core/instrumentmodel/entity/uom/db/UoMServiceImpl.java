package org.trescal.cwms.core.instrumentmodel.entity.uom.db;

import java.util.List;
import java.util.stream.Collectors;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.referential.dto.TMLUnitDTO;

public class UoMServiceImpl extends BaseServiceImpl<UoM, Integer> implements UoMService
{
	private UoMDao UoMDao;
	private boolean uomOrderAlphabetical;

	public void deleteUoM(UoM uom)
	{
		this.UoMDao.remove(uom);
	}

	public UoM findUoM(int id)
	{
		return this.UoMDao.find(id);
	}

	public List<UoM> getAllUoMs()
	{
		if (this.uomOrderAlphabetical)
		{
			return this.UoMDao.getAllUoMsByName();
		}
		else
		{
			List<UoM> list = this.UoMDao.findAll();
			list.sort((u1, u2) -> u1.getFormattedSymbol().toLowerCase().compareTo(u2.getFormattedSymbol().toLowerCase()));
			return list;
		}
	}
	
	@Override
	public List<UoM> getAllActiveUoMs()
	{
		if (this.uomOrderAlphabetical)
		{
			return this.UoMDao.getAllUoMsByName().stream().filter((u->u.getActive() == true)).collect(Collectors.toList());
		}
		else
		{
			List<UoM> list = this.UoMDao.findAll();
			list.sort((u1, u2) -> u1.getFormattedSymbol().toLowerCase().compareTo(u2.getFormattedSymbol().toLowerCase()));
			return list.stream().filter((u->u.getActive() == true)).collect(Collectors.toList());
		}
	}
	
	@Override
	public UoM findUoMbySymbol(String symbol) {
		return this.UoMDao.findUoMbySymbol(symbol);
	}

	public void insertUoM(UoM UoM)
	{
		this.UoMDao.persist(UoM);
	}

	public void saveOrUpdateUoM(UoM uom)
	{
		this.UoMDao.saveOrUpdate(uom);
	}

	public void setUoMDao(UoMDao UoMDao)
	{
		this.UoMDao = UoMDao;
	}

	public void setUomOrderAlphabetical(boolean uomOrderAlphabetical)
	{
		this.uomOrderAlphabetical = uomOrderAlphabetical;
	}

	public void updateUoM(UoM UoM)
	{
		this.UoMDao.update(UoM);
	}
	
	public UoM findDefaultUoM()
	{
		return this.UoMDao.findDefaultUoM();
	}

	@Override
	public UoM getTMLUoM(int tmlid) {
		return this.UoMDao.getTMLUoM(tmlid);
	}

	@Override
	public void insertTMLUoM(TMLUnitDTO dto) throws Exception {
		this.UoMDao.insertTMLUoM(dto);
	}

	@Override
	public void updateTMLUoM(TMLUnitDTO dto) throws Exception {
		this.UoMDao.updateTMLUoM(dto);
	}

	@Override
	public void deleteTMLUoM(TMLUnitDTO dto) throws Exception {
		this.UoMDao.deleteTMLUoM(dto);
	}

	@Override
	protected BaseDao<UoM, Integer> getBaseDao() {
		return this.UoMDao;
	}

}