package org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.instrument.entity.instrumentcharacteristic.InstrumentCharacteristic;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "characteristicdescription")
public class CharacteristicDescription  extends Auditable implements Comparable<CharacteristicDescription> {
	
	private int characteristicDescriptionId;
	private String name;
	private String shortName;
	private Set<Translation> translations;
	private Set<Translation> shortNameTranslations;
	private Boolean required;
	private String metadata;
	private String internalName;
	private int orderno;
	private int characteristicTypeInt;
	private List<CharacteristicLibrary> library;
	
	@SuppressWarnings("unused")
	private CharacteristicType characteristicType;
	
	private List<InstrumentCharacteristic> instrumentCharacteristics;
	
	private UoM uom;
	
	private Date log_createdon;
	
	private Boolean active;
	
	private Integer tmlid;
	
	private Description description;	
	
	private Boolean partOfKey;
	
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="characteristicdescriptiontranslation", joinColumns=@JoinColumn(name="characteristicdescriptionid"))
	public Set<Translation> getTranslations() {
		return translations;
	}
	
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="characteristicshortnametranslation", joinColumns=@JoinColumn(name="characteristicdescriptionid"))
	public Set<Translation> getShortNameTranslations() {
		return shortNameTranslations;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="characteristicdescriptionid")
	public int getCharacteristicDescriptionId() {
		return characteristicDescriptionId;
	}
	
	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}
	
	@Column(name = "shortname", nullable = true)
	public String getShortName() {
		return shortName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "descriptionid", nullable = false)
	public Description getDescription() {
		return description;
	}
	
	@Column(name = "active", nullable = false)
	public Boolean getActive() {
		return active;
	}

	@Column(name = "characteristictype", nullable = false)
	public int getCharacteristicTypeInt() {
		return characteristicTypeInt;
	}

	//Ideally we would use @Converter JPA annotation to convert Characteristic type integer values coming from TML
	//to CharacteristicType enum values but @Converter apparently only works with Hibernate 5. 
	//Having the converter in a seperate class means that we can switch to using @Converter easily if we ever move to hibernate 5
	@Transient
	public CharacteristicType getCharacteristicType() {
		return new CharacteristicTypeConverter().convertToEntityAttribute(this.characteristicTypeInt);
	}
	
	@Column(name = "tmlid", nullable = true)
	public Integer getTmlid() {
		return tmlid;
	}

	@Column(name = "required", nullable = false)
	public Boolean getRequired() {
		return required;
	}
	
	@Column(columnDefinition = "xml")
	public String getMetadata() {
		return metadata;
	}
	
	@Column(name = "internalname", nullable = true)
	public String getInternalName() {
		return internalName;
	}

	@Column(name = "orderno", nullable = false)
	public int getOrderno() {
		return orderno;
	}
	public void setOrderno(int orderno) {
		this.orderno = orderno;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "uomid", nullable = true)
	public UoM getUom() {
		return uom;
	}

	@Column(name = "log_createdon")
	public Date getLog_createdon() {
		return log_createdon;
	}

	@Column(name = "partofkey")
	public Boolean getPartOfKey() {
		return partOfKey;
	}

	public void setCharacteristicDescriptionId(int characteristicDescriptionId) {
		this.characteristicDescriptionId = characteristicDescriptionId;
	}
	public void setDescription(Description description) {
		this.description = description;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public void setCharacteristicType(CharacteristicType characteristicType) {
		this.characteristicTypeInt = new CharacteristicTypeConverter().convertToDatabaseColumn(characteristicType);
	}
	public void setTmlid(Integer tmlid) {
		this.tmlid = tmlid;
	}
	public void setRequired(Boolean required) {
		this.required = required;
	}
	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}
	public void setInternalName(String internalName) {
		this.internalName = internalName;
	}
	public void setUom(UoM uom) {
		this.uom = uom;
	}
	public void setLog_createdon(Date log_createdon) {
		this.log_createdon = log_createdon;
	}

	public void setTranslations(Set<Translation> translations) {
		this.translations = translations;
	}
	
	public void setShortNameTranslations(Set<Translation> shortNameTranslations) {
		this.shortNameTranslations = shortNameTranslations;
	}

	public void setCharacteristicTypeInt(int characteristicTypeInt) {
		this.characteristicTypeInt = characteristicTypeInt;
	}
	
	public void setPartOfKey(Boolean partOfKey) {
		this.partOfKey = partOfKey;
	}
	
	@Transient
	public String toFullString(){
		return "CharacteristicDescription [characteristicDescriptionId=" + characteristicDescriptionId + ", name=" + name
				+ ", translation=" + (translations != null ? translations : "") + ", active=" + active
				+ ", tmlid=" + tmlid + "]";
	}
	
	@OneToMany(mappedBy="characteristic", fetch=FetchType.LAZY)
	public List<InstrumentCharacteristic> getInstrumentCharacteristics() {
		return instrumentCharacteristics;
	}

	public void setInstrumentCharacteristics(List<InstrumentCharacteristic> instrumentCharacteristics) {
		this.instrumentCharacteristics = instrumentCharacteristics;
	}

	@Override
	public int compareTo(CharacteristicDescription other) {
		int result = orderno - other.orderno;
		if(result == 0) result = characteristicDescriptionId - other.characteristicDescriptionId;
		return result;
	}
	
	@Override
	public boolean equals(Object other) {
		if(other.getClass().equals(CharacteristicDescription.class))
			return characteristicDescriptionId == ((CharacteristicDescription) other).characteristicDescriptionId;
		else return false;
	}
	
	@OneToMany(mappedBy="characteristicDescription" , fetch=FetchType.LAZY)
	public List<CharacteristicLibrary> getLibrary() {
		return library;
	}
	
	public void setLibrary(List<CharacteristicLibrary> library) {
		this.library = library;
	}
}