package org.trescal.cwms.core.instrumentmodel.form;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

import lombok.Getter;
import lombok.Setter;

/**
 * Form backing object for adding and editing an {@link InstrumentModel}.
 * 
 * @author richard
 */
@Getter @Setter
public class EditModelForm
{
	@NotNull(message="{error.value.notselected}")
	private Integer descid;
	private String descname;
	@NotNull(message="{error.value.notselected}")
	private Integer instModelTypeId;
	// Nullability dependent on modelMfrType - handled in validator
	private Integer mfrid;
	private String mfrname;
	@NotNull
	private Integer modelid;
	@NotNull(message="{error.value.notselected}")
	private ModelMfrType modelMfrType;
	@Length(min = 0, max = 100)
	private String modelName;
	@NotNull
	private Boolean quarantined;
	// Optional
	private Integer salesCategoryId;
	private String salesCategoryText;
	private Boolean confirmDuplicate;
}