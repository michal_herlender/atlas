/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.entity;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;

/**
 * Enum outlining the different levels of support available to
 * {@link InstrumentModel} and {@link Instrument}.
 * 
 * @author richard
 */
public enum CalibrationSupport
{
	INHOUSE(true), OEM(false), PARTNER(false), THIRD(false), UNSUPPORTED(false);

	private boolean def;
	private ReloadableResourceBundleMessageSource messages;
	private Locale loc = null;
		
	@Component
	public static class CalibrationSupportMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (CalibrationSupport rt : EnumSet.allOf(CalibrationSupport.class))
               rt.setMessageSource(messages);
        }
	}
	
	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}
	
	CalibrationSupport(boolean def)
	{
		this.def = def;
	}

	public String getDescription()
	{
		loc = LocaleContextHolder.getLocale();
		switch (super.ordinal()) {
		case 0:
			if (messages != null) 
				return messages.getMessage("calibrationsupport.inhouse", null, "Supported in-house", loc);
			else
				return "Supported in-house";
		case 1:
			if (messages != null) 
				return messages.getMessage("calibrationsupport.oem", null, "Original manufacturer only", loc);
			else
				return "Original manufacturer only";
		case 2:
			if (messages != null) 
				return messages.getMessage("calibrationsupport.partner", null, "Supported at partner company", loc);
			else
				return "Supported at partner company";
		case 3:
			if (messages != null) 
				return messages.getMessage("calibrationsupport.third", null, "Third party only", loc);
			else
				return "Third party only";
		case 4:
			if (messages != null) 
				return messages.getMessage("calibrationsupport.unsupported", null, "No longer supported", loc);
			else
				return "No longer supported";
		}
		return this.toString();
	}

	public boolean isDef()
	{
		return this.def;
	}

	public void setDef(boolean def)
	{
		this.def = def;
	}
}
