package org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource.db;

import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfrwebresource.MfrWebResource;

public interface MfrWebResourceService
{
	/**
	 * Delete's the {@link MfrWebResource} identified by the given resourceId.
	 * 
	 * @param resourceId the id of the {@link MfrWebResource} to delete.
	 * @return {@link ResultWrapper} containing results of the operation.
	 */
	ResultWrapper deleteWebResource(Integer resourceId);

	void deleteMfrWebResource(MfrWebResource mfrwebresource);

	MfrWebResource findMfrWebResource(int id);

	/**
	 * Returns all {@link MfrWebResource} that match the given url and belong to
	 * the given {@link Mfr}.
	 * 
	 * @param url the url to match.
	 * @param mfrid the id of the {@link Mfr}.
	 * @return list of matching {@link MfrWebResource}.
	 */
	public List<MfrWebResource> findMfrWebResource(String url, int mfrid);

	List<MfrWebResource> getAllMfrWebResources();

	/**
	 * Inserts the given data as a new {@link MfrWebResource}, intended for use
	 * by DWR calls.
	 * 
	 * @param mfrid the id of the {@link Mfr}.
	 * @param description the description of the {@link MfrWebResource}.
	 * @param url the url of the {@link MfrWebResource}.
	 * @return {@link ResultWrapper} holding the results of the operation.
	 */
	ResultWrapper insert(int mfrid, String description, String url);

	void insertMfrWebResource(MfrWebResource mfrwebresource);

	/**
	 * Tests if the given url has already been recorded for the {@link Mfr}
	 * identified by the given id.
	 * 
	 * @param url the url to test.
	 * @param mfrid the id of the {@link Mfr}, not null.
	 * @return true if the url already exists for the {@link Mfr},.
	 */
	boolean isDuplicateMfrWebResource(String url, int mfrid);

	void saveOrUpdateMfrWebResource(MfrWebResource mfrwebresource);

	void updateMfrWebResource(MfrWebResource mfrwebresource);
}