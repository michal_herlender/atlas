package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.referential.dto.TMLFamilyDTO;
import org.trescal.cwms.spring.model.InstrumentModelFamilyJsonDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface InstrumentModelFamilyDao extends BaseDao<InstrumentModelFamily, Integer> {

	boolean existsByName(String name);

	List<InstrumentModelFamilyJsonDTO> getAllFamilies(Locale searchLanguage);

	List<InstrumentModelFamilyJsonDTO> getAllForDomain(Locale searchLanguage, int domainId);

	int getResultCountExact(String name, Locale searchLanguage, int excludeId);

	List<KeyValueIntegerString> getAllKeyValues(String familyFragment, DomainType domainType, Locale searchLanguage,
			int maxResults);

	InstrumentModelFamily getTMLFamily(int tmlid);

	void insertTMLInstrumentModelFamily(TMLFamilyDTO dto) throws Exception;

	void updateTMLFamily(TMLFamilyDTO dto) throws Exception;

	void deleteTMLFamily(TMLFamilyDTO dto) throws Exception;
}