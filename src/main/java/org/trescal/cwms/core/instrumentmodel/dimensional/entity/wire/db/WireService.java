package org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.Wire;

public interface WireService extends BaseService<Wire, Integer>
{
}