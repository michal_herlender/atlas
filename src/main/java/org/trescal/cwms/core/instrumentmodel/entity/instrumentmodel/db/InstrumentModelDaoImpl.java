package org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.RangeType;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelCompanyCalReqDTO;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelForNewJobItemSearchDto;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelPlantillasDTO;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelWithPriceDTO;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionDao;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.db.InstrumentModelTypeDao;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrDao;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.ModelRangeCharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.ModelRangeCharacteristicLibrary_;
import org.trescal.cwms.core.instrumentmodel.entity.option.Option;
import org.trescal.cwms.core.instrumentmodel.entity.option.db.OptionDao;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory_;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.instrumentmodel.entity.uom.db.UoMDao;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CompanyModelCalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CompanyModelCalReq_;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchByLinkedQuotationForm;
import org.trescal.cwms.core.jobs.jobitem.form.NewJobItemSearchInstrumentForm;
import org.trescal.cwms.core.login.entity.user.db.UserDao;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice_;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation_;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem_;
import org.trescal.cwms.core.referential.dto.TMLInstrumentModelDTO;
import org.trescal.cwms.core.referential.dto.TMLOptionDTO;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.PagedResultSet;

import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

import static org.trescal.cwms.core.tools.JpaUtils.calculateTypology;
import static org.trescal.cwms.core.tools.JpaUtils.entityByIdPredicate;
import static org.trescal.cwms.core.tools.StringJpaUtils.ilike;
import static org.trescal.cwms.core.tools.StringJpaUtils.trimAndConcatWithWhitespace;
import static org.trescal.cwms.core.tools.jpa.NewJobItemSearchUtils.searchByCompanyOrGroupOrEveryWherePredicate;

@Repository("InstrumentModelDao")
@Slf4j
public class InstrumentModelDaoImpl extends BaseDaoImpl<InstrumentModel, Integer> implements InstrumentModelDao {

	@Autowired
	private NewDescriptionDao instModelSubfamilyDao;
	@Autowired
	private MfrDao mfrDao;
	@Autowired
	private InstrumentModelTypeDao instrumentModelTypeDao;
	@Autowired
	private UoMDao uomDao;
	@Autowired
	private OptionDao optionDao;
	@Autowired
	private UserDao userDao;
	@Value("#{props['cwms.users.automatedusername']}")
	private String automatedusername;

	@Override
	protected Class<InstrumentModel> getEntity() {
		return InstrumentModel.class;
	}

	@Override
	public List<InstrumentModel> findInstrumentModel(Description description) {
		return getResultList(cb -> {
			CriteriaQuery<InstrumentModel> cq = cb.createQuery(InstrumentModel.class);
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);
			Join<InstrumentModel, Description> descriptionJoin = root.join(InstrumentModel_.description.getName());

			cq.where(cb.equal(descriptionJoin.get(Description_.id), description.getId()));

			return cq;
		});
	}

	public List<InstrumentModel> findInstrumentModel(SalesCategory salesCategory) {
		return getResultList(cb -> {
			CriteriaQuery<InstrumentModel> cq = cb.createQuery(InstrumentModel.class);
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);

			cq.where(cb.equal(root.get(InstrumentModel_.salesCategory), salesCategory));

			return cq;
		});
	}

	@Override
	public InstrumentModel findCapabilityModel(Description description) throws HibernateException {
		return getFirstResult(cb -> {
			CriteriaQuery<InstrumentModel> cq = cb.createQuery(InstrumentModel.class);
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);
			Join<InstrumentModel, InstrumentModelType> instModelTypeJoin = root.join(InstrumentModel_.modelType);

			cq.where(cb.and(cb.equal(root.get(InstrumentModel_.description), description),
					cb.equal(instModelTypeJoin.get(InstrumentModelType_.capability), true)));
			cq.distinct(true);

			return cq;
		}).orElse(null);
	}

	@Override
	public InstrumentModel findSalesCategoryModel(SalesCategory salesCategory) throws HibernateException {
		return getFirstResult(cb -> {
			CriteriaQuery<InstrumentModel> cq = cb.createQuery(InstrumentModel.class);
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);
			Join<InstrumentModel, InstrumentModelType> instModelTypeJoin = root.join(InstrumentModel_.modelType);

			cq.where(cb.and(cb.equal(root.get(InstrumentModel_.salesCategory), salesCategory),
					cb.equal(instModelTypeJoin.get(InstrumentModelType_.salescategory), true)));
			cq.distinct(true);

			return cq;
		}).orElse(null);
	}

	@Override
	public List<InstrumentModel> findAll() {
		return getResultList(cb -> {
			CriteriaQuery<InstrumentModel> cq = cb.createQuery(InstrumentModel.class);
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);
			root.join(InstrumentModel_.mfr);
			root.join(InstrumentModel_.description);

			return cq;
		});
	}

	@Override
	public List<InstrumentModelWithPriceDTO> getAll(DomainType domainType, Locale locale, ServiceType serviceType,
			Company allocatedCompany) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<InstrumentModelWithPriceDTO> cq = cb.createQuery(InstrumentModelWithPriceDTO.class);
		Root<InstrumentModel> model = cq.from(InstrumentModel.class);
		Join<InstrumentModel, Description> description = model.join(InstrumentModel_.description,
				javax.persistence.criteria.JoinType.INNER);
		Join<Description, InstrumentModelFamily> family = description.join(Description_.family,
				javax.persistence.criteria.JoinType.INNER);
		Join<InstrumentModelFamily, InstrumentModelDomain> domain = family.join(InstrumentModelFamily_.domain,
				javax.persistence.criteria.JoinType.INNER);
		Join<InstrumentModel, Translation> name = model.join(InstrumentModel_.nameTranslations,
				javax.persistence.criteria.JoinType.INNER);
		Join<InstrumentModel, CatalogPrice> catalogPrice = model.join(InstrumentModel_.catalogPrices,
				javax.persistence.criteria.JoinType.LEFT);
		catalogPrice.on(cb.and(cb.equal(catalogPrice.get(CatalogPrice_.serviceType), serviceType),
				cb.equal(catalogPrice.get(CatalogPrice_.organisation), allocatedCompany)));
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(domain.get(InstrumentModelDomain_.domainType), domainType));
		clauses.getExpressions().add(cb.equal(name.get(Translation_.locale), locale));
		clauses.getExpressions().add(cb.isFalse(model.get(InstrumentModel_.quarantined)));
		cq.where(clauses);
		cq.orderBy(cb.asc(name.get(Translation_.translation)));
		cq.select(cb.construct(InstrumentModelWithPriceDTO.class, model.get(InstrumentModel_.modelid),
				name.get(Translation_.translation), catalogPrice.get(CatalogPrice_.fixedPrice)));
		TypedQuery<InstrumentModelWithPriceDTO> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}

	public List<Integer> getDistinctModelidListForQuote(int quoteId) {
		return getResultList(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);
			Join<InstrumentModel, Quotationitem> quotItemJoin = root.join(InstrumentModel_.quoteItems);
			Join<Quotationitem, Quotation> quotationJoin = quotItemJoin.join(Quotationitem_.quotation);

			cq.select(root.get(InstrumentModel_.modelid));
			cq.distinct(true);

			cq.where(cb.equal(quotationJoin.get(Quotation_.id), quoteId));

			return cq;
		});
	}

	@Override
	public PagedResultSet<InstrumentModelCompanyCalReqDTO> getInstrumentModelsWithCompanyCalRequirements(int coid,
			String mfr, String model, String desc, Integer resultsPerPage, Integer currentPage, Locale locale,
			boolean onlyWithCalReqs) {

		PagedResultSet<InstrumentModelCompanyCalReqDTO> prs = new PagedResultSet<>(resultsPerPage, currentPage);

		super.completePagedResultSet(prs, InstrumentModelCompanyCalReqDTO.class, cb -> cq -> {

			Root<InstrumentModel> root = cq.from(InstrumentModel.class);
			Join<InstrumentModel, CompanyModelCalReq> calReqsJoin = root.join(InstrumentModel_.companyModelCalReqs,
					onlyWithCalReqs ? JoinType.INNER : JoinType.LEFT);
			calReqsJoin.on(cb.and(cb.equal(calReqsJoin.get(CompanyModelCalReq_.comp), coid),
					cb.equal(calReqsJoin.get(CompanyModelCalReq_.active), true)));
			Join<InstrumentModel, Mfr> mfrJoin = root.join(InstrumentModel_.mfr);
			Join<InstrumentModel, InstrumentModelType> modelTypeJoin = root.join(InstrumentModel_.modelType);
			Join<InstrumentModel, Description> descriptionJoin = root.join(InstrumentModel_.description);
			Join<Description, Translation> descriptionName = descriptionJoin.join(Description_.translations,
					JoinType.LEFT);
			descriptionName.on(cb.equal(descriptionName.get(Translation_.locale), locale));
			Join<InstrumentModel, Translation> modelName = root.join(InstrumentModel_.nameTranslations, JoinType.LEFT);
			modelName.on(cb.equal(modelName.get(Translation_.locale), locale));
			Join<InstrumentModelType, Translation> modelTypeName = modelTypeJoin
					.join(InstrumentModelType_.modelTypeNameTranslation, JoinType.LEFT);
			modelTypeName.on(cb.equal(modelTypeName.get(Translation_.locale), locale));

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(modelTypeJoin.get(InstrumentModelType_.capability), false));
			clauses.getExpressions().add(cb.equal(modelTypeJoin.get(InstrumentModelType_.salescategory), false));
			clauses.getExpressions().add(cb.equal(root.get(InstrumentModel_.quarantined), false));

			if (StringUtils.isNotBlank(mfr))
				clauses.getExpressions().add(cb.like(mfrJoin.get(Mfr_.name), mfr));
			if (StringUtils.isNotBlank(model))
				clauses.getExpressions().add(cb.like(root.get(InstrumentModel_.model), model));
			if (StringUtils.isNotBlank(desc))
				clauses.getExpressions().add(cb.like(descriptionName.get(Translation_.translation), '%' + desc + '%'));

			cq.where(clauses);

			List<Order> orders = new ArrayList<>();
			orders.add(cb.asc(mfrJoin.get(Mfr_.name)));
			orders.add(cb.asc(root.get(InstrumentModel_.model)));
			orders.add(cb.asc(descriptionJoin.get(Description_.description)));

			CompoundSelection<InstrumentModelCompanyCalReqDTO> selection = cb.construct(
					InstrumentModelCompanyCalReqDTO.class, calReqsJoin.get(CompanyModelCalReq_.id),
					descriptionName.get(Translation_.translation), mfrJoin.get(Mfr_.name),
					root.get(InstrumentModel_.model), modelName.get(Translation_.translation),
					root.get(InstrumentModel_.modelid), modelTypeName.get(Translation_.translation));

			return Triple.of(root, selection, orders);

		});

		return prs;

	}

	@Override
	public List<Tuple> getModelIdsForInstruments(Collection<Integer> instrumentIds) {
		return getResultList(cb -> {
			CriteriaQuery<Tuple> cq = cb.createQuery(Tuple.class);
			Root<Instrument> root = cq.from(Instrument.class);
			Join<Instrument, InstrumentModel> model = root.join(Instrument_.model);
			cq.where(root.get(Instrument_.plantid).in(instrumentIds));
			cq.multiselect(root.get(Instrument_.plantid), model.get(InstrumentModel_.modelid));
			return cq;
		});
	}

	@Override
	public List<InstrumentModel> getModelsByIds(Collection<Integer> modelids) {
		return getResultList(cb -> {
			CriteriaQuery<InstrumentModel> cq = cb.createQuery(InstrumentModel.class);
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);

			cq.where(root.get(InstrumentModel_.modelid).in(modelids));
			return cq;
		});
	}

	@Override
	public List<InstrumentModel> getModelsFromIds(List<Integer> modelIds) {
		List<InstrumentModel> models = new ArrayList<>();

		if (modelIds.size() > 0) {
			models = getResultList(cb -> {
				CriteriaQuery<InstrumentModel> cq = cb.createQuery(InstrumentModel.class);
				Root<InstrumentModel> root = cq.from(InstrumentModel.class);

				cq.where(root.get(InstrumentModel_.modelid).in(modelIds));
				return cq;
			});
		}

		return models;
	}

	private Pair<Root<InstrumentModel>, Selection<InstrumentModelPlantillasDTO>> createPlantillasModelQueryParts(
			CriteriaBuilder cb, CriteriaQuery<?> cq, Locale locale, Date afterLastModified) {
		Root<InstrumentModel> model = cq.from(InstrumentModel.class);
		Join<InstrumentModel, Mfr> mfr = model.join(InstrumentModel_.mfr, javax.persistence.criteria.JoinType.LEFT);
		Join<InstrumentModel, InstrumentModelType> modelType = model.join(InstrumentModel_.modelType,
				javax.persistence.criteria.JoinType.LEFT);
		Join<InstrumentModelType, Translation> modelTypeTranslation = modelType
				.join(InstrumentModelType_.modelTypeNameTranslation, javax.persistence.criteria.JoinType.LEFT);
		modelTypeTranslation.on(cb.equal(modelTypeTranslation.get(Translation_.locale), locale));
		Join<InstrumentModel, Description> subFamily = model.join(InstrumentModel_.description,
				javax.persistence.criteria.JoinType.LEFT);
		Join<Description, Translation> subFamilyTranslation = subFamily.join(Description_.translations,
				javax.persistence.criteria.JoinType.LEFT);
		subFamilyTranslation.on(cb.equal(subFamilyTranslation.get(Translation_.locale), locale));
		Join<InstrumentModel, SalesCategory> salesCategory = model.join(InstrumentModel_.salesCategory,
				javax.persistence.criteria.JoinType.LEFT);
		Join<SalesCategory, Translation> salesCategoryTranslation = salesCategory.join(SalesCategory_.translations,
				javax.persistence.criteria.JoinType.LEFT);
		salesCategoryTranslation.on(cb.equal(salesCategoryTranslation.get(Translation_.locale), locale));
		Join<Description, InstrumentModelFamily> family = subFamily.join(Description_.family,
				javax.persistence.criteria.JoinType.LEFT);
		Join<InstrumentModelFamily, Translation> familyTranslation = family.join(InstrumentModelFamily_.translation,
				javax.persistence.criteria.JoinType.LEFT);
		familyTranslation.on(cb.equal(familyTranslation.get(Translation_.locale), locale));
		Join<InstrumentModelFamily, InstrumentModelDomain> domain = family.join(InstrumentModelFamily_.domain,
				javax.persistence.criteria.JoinType.LEFT);
		Join<InstrumentModelDomain, Translation> domainTranslation = domain.join(InstrumentModelDomain_.translation,
				javax.persistence.criteria.JoinType.LEFT);
		domainTranslation.on(cb.equal(domainTranslation.get(Translation_.locale), locale));
		Selection<InstrumentModelPlantillasDTO> selection = cb.construct(InstrumentModelPlantillasDTO.class,
				model.get(InstrumentModel_.lastModified), subFamily.get(Description_.lastModified),
				salesCategory.get(SalesCategory_.lastModified), family.get(InstrumentModelFamily_.lastModified),
				domain.get(InstrumentModelDomain_.lastModified), modelType.get(InstrumentModelType_.canSelect),
				modelTypeTranslation.get(Translation_.translation), mfr.get(Mfr_.name),
				model.get(InstrumentModel_.modelMfrType), model.get(InstrumentModel_.model),
				model.get(InstrumentModel_.modelid), subFamily.get(Description_.id),
				salesCategoryTranslation.get(Translation_.translation), domainTranslation.get(Translation_.translation),
				familyTranslation.get(Translation_.translation), subFamilyTranslation.get(Translation_.translation));
		if (afterLastModified != null) {
			Predicate updated = cb.disjunction();
			updated.getExpressions().add(cb.greaterThan(model.get(InstrumentModel_.lastModified), afterLastModified));
			updated.getExpressions().add(cb.greaterThan(subFamily.get(Description_.lastModified), afterLastModified));
			updated.getExpressions()
					.add(cb.greaterThan(salesCategory.get(SalesCategory_.lastModified), afterLastModified));
			updated.getExpressions()
					.add(cb.greaterThan(family.get(InstrumentModelFamily_.lastModified), afterLastModified));
			updated.getExpressions()
					.add(cb.greaterThan(domain.get(InstrumentModelDomain_.lastModified), afterLastModified));
			cq.where(updated);
		}
		return Pair.of(model, selection);
	}

	@Override
	public InstrumentModelPlantillasDTO getPlantillasModel(Locale locale, int modelid) {
		return getFirstResult(cb -> {
			CriteriaQuery<InstrumentModelPlantillasDTO> cq = cb.createQuery(InstrumentModelPlantillasDTO.class);
			Pair<Root<InstrumentModel>, Selection<InstrumentModelPlantillasDTO>> queryParts = createPlantillasModelQueryParts(
					cb, cq, locale, null);
			Root<InstrumentModel> model = queryParts.getLeft();
			cq.where(cb.equal(model.get(InstrumentModel_.modelid), modelid));
			cq.select(queryParts.getRight());
			return cq;
		}).orElse(null);
	}

	/**
	 * lastModified parameter is optional, remaining are mandatory
	 */
	@Override
	public PagedResultSet<InstrumentModelPlantillasDTO> getPlantillasModels(Locale locale, Date afterLastModified,
			int resultsPerPage, int currentPage) {
		PagedResultSet<InstrumentModelPlantillasDTO> prs = new PagedResultSet<>(resultsPerPage, currentPage);
		completePagedResultSet(prs, InstrumentModelPlantillasDTO.class, cb -> cq -> {
			Pair<Root<InstrumentModel>, Selection<InstrumentModelPlantillasDTO>> queryParts = createPlantillasModelQueryParts(
					cb, cq, locale, afterLastModified);
			List<javax.persistence.criteria.Order> orders = new ArrayList<>();
			return Triple.of(queryParts.getLeft().get(InstrumentModel_.modelid), queryParts.getRight(), orders);
		});
		return prs;
	}

	@Override
	public PagedResultSet<InstrumentModel> searchInstrumentModelsPaged(PagedResultSet<InstrumentModel> prs,
			InstrumentAndModelSearchForm<InstrumentModel> form) {
		// TODO: add property nameTranslation to form, so that it can used
		// instead of building the name in the view

		Locale locale = LocaleContextHolder.getLocale();
		super.completePagedResultSet(prs, InstrumentModel.class, cb -> cq -> {

			Root<InstrumentModel> root = cq.from(InstrumentModel.class);
			Join<InstrumentModel, Mfr> mfrJoin = root.join(InstrumentModel_.mfr);
			Join<InstrumentModel, Description> subFamilyJoin = root.join(InstrumentModel_.description);
			Join<InstrumentModel, InstrumentModelType> modelTypeJoin = root.join(InstrumentModel_.modelType);
			Join<Description, InstrumentModelFamily> familyJoin = subFamilyJoin.join(Description_.family);

			Join<InstrumentModelFamily, InstrumentModelDomain> domainJoin = familyJoin
					.join(InstrumentModelFamily_.domain);
			Join<InstrumentModel, Translation> modelName = root.join(InstrumentModel_.nameTranslations,
					javax.persistence.criteria.JoinType.LEFT);

			Predicate disjunction = cb.disjunction();
			disjunction.getExpressions().add(cb.isNull(modelName.get(Translation_.locale)));
			disjunction.getExpressions().add(cb.equal(modelName.get(Translation_.locale), locale));

			Predicate clauses = cb.conjunction();

			// domain restriction

			if (StringUtils.isNotEmpty(form.getDomainNm())) {
				Join<InstrumentModelDomain, Translation> domainName = domainJoin
						.join(InstrumentModelDomain_.translation);
				clauses.getExpressions().add(cb.equal(domainName.get(Translation_.locale), locale));
				clauses.getExpressions().add(
						ilike(cb, domainName.get(Translation_.translation), form.getDomainNm(), MatchMode.ANYWHERE));
			} else if (form.getDomainId() != null && form.getDomainId() != 0) {
				clauses.getExpressions()
						.add(cb.equal(domainJoin.get(InstrumentModelDomain_.domainid), form.getDomainId()));
			}

			// family restriction
			if (StringUtils.isNotEmpty(form.getFamilyNm())) {
				Join<InstrumentModelFamily, Translation> familyName = familyJoin
						.join(InstrumentModelFamily_.translation);
				clauses.getExpressions().add(cb.equal(familyName.get(Translation_.locale), locale));
				clauses.getExpressions().add(
						ilike(cb, familyName.get(Translation_.translation), form.getFamilyNm(), MatchMode.ANYWHERE));
			} else if (form.getFamilyId() != null && form.getFamilyId() != 0) {
				clauses.getExpressions()
						.add(cb.equal(familyJoin.get(InstrumentModelFamily_.familyid), form.getFamilyId()));
			}

			// sub-family restrictions
			if (StringUtils.isNotEmpty(form.getDescNm())) {
				Join<Description, Translation> subFamilyName = subFamilyJoin.join(Description_.translations);
				clauses.getExpressions().add(cb.equal(subFamilyName.get(Translation_.locale), locale));
				clauses.getExpressions().add(
						ilike(cb, subFamilyName.get(Translation_.translation), form.getDescNm(), MatchMode.ANYWHERE));
			} else if ((form.getDescId() != null) && (form.getDescId() != 0)) {
				clauses.getExpressions().add(cb.equal(subFamilyJoin.get(Description_.id), form.getDescId()));
			}

			// characteristic library restriction
			if (form.getCharLibraryValues() != null) {
				for (Map.Entry<Integer, Integer> entry : form.getCharLibraryValues().entrySet()) {
					if (entry.getValue() != null) {
						Subquery<ModelRangeCharacteristicLibrary> subQuery = cq
								.subquery(ModelRangeCharacteristicLibrary.class);
						Root<ModelRangeCharacteristicLibrary> subRoot = subQuery
								.from(ModelRangeCharacteristicLibrary.class);
						Predicate clausesCharacteristic = cb.conjunction();
						String libAlias = "MRCL" + entry.getKey();
						subRoot.alias(libAlias);
						Join<ModelRangeCharacteristicLibrary, InstrumentModel> modelJoin = subRoot
								.join(ModelRangeCharacteristicLibrary_.model);
						Join<ModelRangeCharacteristicLibrary, CharacteristicLibrary> characteristicLibraryJoin = subRoot
								.join(ModelRangeCharacteristicLibrary_.characteristicLibrary);

						clausesCharacteristic.getExpressions().add(
								cb.equal(root.get(InstrumentModel_.modelid), modelJoin.get(InstrumentModel_.modelid)));
						clausesCharacteristic.getExpressions()
								.add(cb.equal(
										characteristicLibraryJoin.get(CharacteristicLibrary_.characteristiclibraryid),
										entry.getValue()));
						subQuery.select(subRoot);
						subQuery.where(clausesCharacteristic);
						clauses.getExpressions().add(cb.exists(subQuery));
					}
				}
			}

			// mfr restrictions
			if (StringUtils.isNotEmpty(form.getMfrNm())) {
				clauses.getExpressions().add(ilike(cb, mfrJoin.get(Mfr_.name), form.getMfrNm(), MatchMode.ANYWHERE));
			} else if ((form.getMfrId() != null) && (form.getMfrId() != 0)) {
				clauses.getExpressions().add(cb.equal(mfrJoin.get(Mfr_.mfrid), form.getMfrId()));
			}

			// model restrictions
			if (StringUtils.isNotEmpty(form.getModel())) {
				clauses.getExpressions()
						.add(ilike(cb, root.get(InstrumentModel_.model), form.getModel(), MatchMode.ANYWHERE));
			}

			// sales category restriction
			if (StringUtils.isNotEmpty(form.getSalesCat())) {
				Join<InstrumentModel, SalesCategory> salesCategoryJoin = root.join(InstrumentModel_.salesCategory);
				Join<SalesCategory, Translation> salesCategoryName = salesCategoryJoin
						.join(SalesCategory_.translations);
				clauses.getExpressions().add(cb.equal(salesCategoryName.get(Translation_.locale), locale));
				clauses.getExpressions().add(ilike(cb, salesCategoryName.get(Translation_.translation),
						form.getSalesCat(), MatchMode.ANYWHERE));
			} else if (form.getSalesCatId() != null && form.getSalesCatId() != 0) {
				Join<InstrumentModel, SalesCategory> salesCategoryJoin = root.join(InstrumentModel_.salesCategory);
				clauses.getExpressions().add(cb.equal(salesCategoryJoin.get(SalesCategory_.id), form.getSalesCatId()));
			}

			// model type restrictions
			if (form.getExcludeCapabilityModelTypes() != null && form.getExcludeCapabilityModelTypes()) {
				clauses.getExpressions().add(cb.or(cb.isFalse(modelTypeJoin.get(InstrumentModelType_.capability)),
						cb.isNull(modelTypeJoin.get(InstrumentModelType_.capability))));
			}
			if (form.getExcludeSalesCategoryModelTypes() != null && form.getExcludeSalesCategoryModelTypes()) {
				clauses.getExpressions().add(cb.or(cb.isFalse(modelTypeJoin.get(InstrumentModelType_.salescategory)),
						cb.isNull(modelTypeJoin.get(InstrumentModelType_.salescategory))));
			}
			if (form.getExcludeStndardModelTypes() != null && form.getExcludeStndardModelTypes()) {
				clauses.getExpressions().add(cb.or(cb.isTrue(modelTypeJoin.get(InstrumentModelType_.capability)),
						cb.isTrue(modelTypeJoin.get(InstrumentModelType_.salescategory))));
			}
			if (form.getModelTypeIds() != null && form.getModelTypeIds().size() > 0) {
				clauses.getExpressions()
						.add(modelTypeJoin.get(InstrumentModelType_.instModelTypeId).in(form.getModelTypeIds()));
			}

			// quarantined restrictions
			if (form.getExcludeQuarantined() != null && form.getExcludeQuarantined()) {
				clauses.getExpressions().add(cb.notEqual(root.get(InstrumentModel_.quarantined), true));
			}

			// manufacturer type restrictions
			if (form.getModelMfrType() != null) {
				clauses.getExpressions().add(cb.equal(root.get(InstrumentModel_.modelMfrType), form.getModelMfrType()));
			}

			clauses.getExpressions().add(disjunction);
			cq.where(clauses);

			List<Order> orders = new ArrayList<>();

			// sort by model
			if (form.getSortBy() != null && form.getSortBy().equals("Model")) {
				orders.add(cb.asc(root.get(InstrumentModel_.model)));
				orders.add(cb.asc(root.get(InstrumentModel_.modelid)));
			}
			// sort by brand then by model
			else if (form.getSortBy() != null && form.getSortBy().equals("Brand")) {
				orders.add(cb.asc(mfrJoin.get(Mfr_.name)));
				orders.add(cb.asc(root.get(InstrumentModel_.model)));
				orders.add(cb.asc(root.get(InstrumentModel_.modelid)));
			}
			// sort by instrument model then by brand then by model
			else {
				orders.add(cb.asc(modelName.get(Translation_.translation)));
				orders.add(cb.asc(subFamilyJoin.get(Description_.description)));
				orders.add(cb.asc(mfrJoin.get(Mfr_.name)));
				orders.add(cb.asc(root.get(InstrumentModel_.model)));
				orders.add(cb.asc(root.get(InstrumentModel_.modelid)));
			}

			return Triple.of(root.get(InstrumentModel_.modelid), null, orders);
		});

		return prs;
	}

	@Override
	public List<InstrumentModel> searchModelSignature(Integer mfrId, String model, Integer descriptionId,
			Integer modelTypeId, Integer excludeModelId) {
		return getResultList(cb -> {
			CriteriaQuery<InstrumentModel> cq = cb.createQuery(InstrumentModel.class);
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(InstrumentModel_.model), model));
			if (excludeModelId != null) {
				clauses.getExpressions().add(cb.notEqual(root.get(InstrumentModel_.modelid), excludeModelId));
			}
			Join<InstrumentModel, Mfr> mfrJoin = root.join(InstrumentModel_.mfr);
			if (mfrId != null && mfrId > 0) {
				clauses.getExpressions().add(cb.equal(mfrJoin.get(Mfr_.mfrid), mfrId));
			} else {
				clauses.getExpressions().add(cb.equal(mfrJoin.get(Mfr_.mfrid), 1));
			}
			Join<InstrumentModel, Description> descriptionJoin = root.join(InstrumentModel_.description);
			clauses.getExpressions().add(cb.equal(descriptionJoin.get(Description_.id), descriptionId));
			Join<InstrumentModel, InstrumentModelType> modelTypeJoin = root.join(InstrumentModel_.modelType);
			clauses.getExpressions()
					.add(cb.equal(modelTypeJoin.get(InstrumentModelType_.instModelTypeId), modelTypeId));

			cq.where(clauses);

			return cq;
		});
	}

	public InstrumentModel getByTmlId(Integer tmlModelId) {
		if (tmlModelId == null)
			throw new IllegalArgumentException("tmlModelId may not be null!");
		return super.findByUniqueProperty(InstrumentModel_.tmlid, tmlModelId);
	}

	public Long getCount() {
		return super.getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);
			cq.select(cb.count(root));
			return cq;
		});
	}
	
	public List<InstrumentModel> getPaged(int firstResult, int maxResults, boolean fetchTranslations) {
		return super.getResultList(cb -> {
			CriteriaQuery<InstrumentModel> cq = cb.createQuery(InstrumentModel.class);
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);
			if (fetchTranslations) {
				root.fetch(InstrumentModel_.nameTranslations);
			}
			return cq;
		}, firstResult, maxResults);
	}
	
	@Override
	@Deprecated
	public InstrumentModel getTMLInstrumentModel(int tmlid) {
		return getFirstResult(cb -> {
			CriteriaQuery<InstrumentModel> cq = cb.createQuery(InstrumentModel.class);
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);

			cq.where(cb.equal(root.get(InstrumentModel_.tmlid), tmlid));
			return cq;
		}).orElse(null);
	}

	@Override
	@Deprecated
	public InstrumentModel insertTMLInstrumentModel(TMLInstrumentModelDTO dto) {
		InstrumentModelType defaultModelType = this.instrumentModelTypeDao.findDefaultType();
		Contact defaultContact = this.userDao.find(automatedusername).getCon();

		Description instrumentModelSubFamily = instModelSubfamilyDao.getTMLDescription(dto.getSubFamily());
		Mfr instrumentModelManufacturer = mfrDao.findTMLMfr(dto.getManufacturer());
		if (instrumentModelManufacturer == null) {
			log.error("No Manufacturer found for id " + dto.getManufacturer());
			// throw RuntimeException, we can't
			// proceed further with a null /
			// incorrect unit.
			throw new RuntimeException("No Manufacturer found for id " + dto.getManufacturer());
		}

		InstrumentModel instModel = new InstrumentModel();
		instModel.setDescription(instrumentModelSubFamily);
		instModel.setMfr(instrumentModelManufacturer);
		instModel.setModel(dto.getModelName());
		instModel.setModelMfrType(ModelMfrType.MFR_SPECIFIC);
		instModel.setAddedBy(defaultContact);
		instModel.setAddedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		instModel.setModelType(defaultModelType);
		instModel.setTmlid(dto.getTMLID());
		instModel.setRanges(new ArrayList<>());
		instModel.setLibraryValues(new ArrayList<>());
		instModel.setNameTranslations(new java.util.HashSet<>());
		instModel.setQuarantined(!dto.isIsEnabled());

		log.debug("Inserting: " + instModel.toFullString());
		this.persist(instModel);
		// Add this to avoid locks
		log.debug("Inserted: " + instModel.toFullString());

		return this.updateTMLInstrumentModel(dto);
	}

	@Override
	@Deprecated
	public InstrumentModel updateTMLInstrumentModel(TMLInstrumentModelDTO dto) {
		try {
			InstrumentModel instrumentModel = this.getTMLInstrumentModel(dto.getTMLID());

			Description instrumentModelSubFamily = instModelSubfamilyDao.getTMLDescription(dto.getSubFamily());
			Mfr instrumentModelManufacturer = mfrDao.findTMLMfr(dto.getManufacturer());

			instrumentModel.setDescription(instrumentModelSubFamily);
			instrumentModel.setMfr(instrumentModelManufacturer);
			instrumentModel.setModel(dto.getModelName());

			UoM defaultUoM = uomDao.findDefaultUoM();
			if (defaultUoM == null) {
				log.error("Database configuration error : defaultUoM was null!");
			}

			List<ModelRange> modelRanges = new ArrayList<>();
			List<ModelRangeCharacteristicLibrary> modelRangeCharacteristicLibraries = new ArrayList<>();

			for (ModelRange mr : instrumentModel.getRanges()) {
				if (mr.getCharacteristicType() == RangeType.CHARACTERISTIC) {
					getSession().delete(mr);
				}
			}

			instrumentModel.getRanges().clear();

			for (ModelRangeCharacteristicLibrary mr : instrumentModel.getLibraryValues()) {
				getSession().delete(mr);
			}

			instrumentModel.getLibraryValues().clear();

			if (dto.getInstrumentModelCharacteristic() != null) {
				for (int i = 0; i < dto.getInstrumentModelCharacteristic().getClass().getDeclaredFields().length; i++) {
					Field field = dto.getInstrumentModelCharacteristic().getClass().getDeclaredFields()[i];
					if (log.isTraceEnabled()) {
						log.trace("Evaluating field " + i + " name " + field.getName());
					}
					field.setAccessible(true);
					ModelRange modelRange = new ModelRange();
					if (field.getName().startsWith("VarChar") || field.getName().startsWith("Float")) {
						if (field.get(dto.getInstrumentModelCharacteristic()) != null) {
							Criteria crit = getSession().createCriteria(CharacteristicDescription.class)
									.add(Restrictions.eq("internalName", field.getName()));
							Criteria descCrit = crit.createCriteria("description");
							descCrit.add(Restrictions.eq("id", instrumentModelSubFamily.getId()));

							CharacteristicDescription characteristicDescription = (CharacteristicDescription) crit
									.uniqueResult();

							if (characteristicDescription != null) {
								modelRange.setCharacteristicDescription(characteristicDescription);
								modelRange.setCharacteristicType(RangeType.CHARACTERISTIC);
								modelRange.setMaxIsInfinite(false);
								modelRange.setModel(instrumentModel);
								switch (characteristicDescription.getCharacteristicType()) {
								case FREE_FIELD: {
									modelRange.setStart((double) 0);
									modelRange.setUom(defaultUoM);
									modelRange
											.setMinvalue(field.get(dto.getInstrumentModelCharacteristic()).toString());

									modelRanges.add(modelRange);
									break;
								}
								case LIBRARY: {
									// GB 2021-02-01 : It seems that library
									// characteristics sent as "VarChar" are
									// integers
									// whereas "Float" are in scientific
									// notation, but either way represent a
									// tmlid of option within library
									int clTmlId = field.getName().startsWith("VarChar")
											? Integer.parseInt(
													field.get(dto.getInstrumentModelCharacteristic()).toString())
											: (int) Double.parseDouble(
													field.get(dto.getInstrumentModelCharacteristic()).toString());
									Criteria critLib = getSession().createCriteria(CharacteristicLibrary.class)
											.add(Restrictions.eq("tmlid", clTmlId));
									Criteria caracDescCrit = critLib.createCriteria("characteristicDescription");
									caracDescCrit.add(Restrictions.eq("characteristicDescriptionId",
											characteristicDescription.getCharacteristicDescriptionId()));

									if (log.isDebugEnabled()) {
										log.debug("Searching CharacteristicLibrary for tmlid "
												+ field.get(dto.getInstrumentModelCharacteristic())
												+ " and characteristicDescriptionId "
												+ characteristicDescription.getCharacteristicDescriptionId());
									}

									CharacteristicLibrary characteristicLibrary = (CharacteristicLibrary) critLib
											.uniqueResult();

									if (characteristicLibrary != null) {
										ModelRangeCharacteristicLibrary mrcl = new ModelRangeCharacteristicLibrary();
										mrcl.setCharacteristicLibrary(characteristicLibrary);
										mrcl.setModel(instrumentModel);

										modelRangeCharacteristicLibraries.add(mrcl);
									}

									break;
								}
								case VALUE_WITH_UNIT: {
									Field unitField = dto.getInstrumentModelCharacteristic().getClass()
											.getDeclaredField(field.getName().replace("Float", "Unit"));
									unitField.setAccessible(true);

									UoM uom = defaultUoM;

									if (unitField.get(dto.getInstrumentModelCharacteristic()) != null
											&& !unitField.get(dto.getInstrumentModelCharacteristic()).equals("")) {
										String symbol = unitField.get(dto.getInstrumentModelCharacteristic())
												.toString();
										uom = uomDao.findUoMbySymbol(symbol);
										if (uom == null) {
											log.error("No uom found for symbol/shortname " + symbol);
											// throw RuntimeException, we can't
											// proceed further with a null /
											// incorrect unit.
											throw new RuntimeException("No uom found for symbol/shortname " + symbol);
										}
									}

									modelRange.setStart(Double
											.valueOf(field.get(dto.getInstrumentModelCharacteristic()).toString()));
									modelRange.setUom(uom);

									modelRanges.add(modelRange);
									break;
								}
								}
							}
						}
					}
					field.setAccessible(false);
				}
			}

			if (dto.getOptions() != null) {

				for (TMLOptionDTO tmlOptionDTO : dto.getOptions()) {
					Option option = optionDao.getTMLOption(dto.getModel(), tmlOptionDTO.getCode());
					if (option != null) {
						Criteria modelRangeCriteria = getSession().createCriteria(ModelRange.class);

						Criteria instModelCrit = modelRangeCriteria.createCriteria("model");
						instModelCrit.add(Restrictions.eq("modelid", instrumentModel.getModelid()));

						Criteria optionCrit = modelRangeCriteria.createCriteria("modelOption");
						optionCrit.add(Restrictions.eq("optionid", option.getOptionid()));

						ModelRange mrOption = (ModelRange) modelRangeCriteria.uniqueResult();

						if (mrOption == null) {
							mrOption = new ModelRange();
						}

						mrOption.setModel(instrumentModel);
						mrOption.setStart((double) 0);
						mrOption.setUom(defaultUoM);
						mrOption.setModelOption(option);
						mrOption.setCharacteristicType(RangeType.OPTION);
						mrOption.setMaxIsInfinite(false);

						modelRanges.add(mrOption);
					}
				}
			}

			instrumentModel.setRanges(modelRanges);
			instrumentModel.setLibraryValues(modelRangeCharacteristicLibraries);
			instrumentModel.setQuarantined(!dto.isIsEnabled());

			log.debug("Updating: " + instrumentModel.toFullString());
			getSession().update(instrumentModel);
			// Add this to avoid locks
			// getSession().clear();
			log.debug("Updated: " + instrumentModel.toFullString());

			// TODO : Update the link to the Sales Category

			return instrumentModel;
		} catch (IllegalAccessException | NoSuchFieldException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public List<InstrumentModel> getOtherModelsWithMatchingSalesCategory(InstrumentModel instrumentModel) {
		return getResultList(cb -> {
			CriteriaQuery<InstrumentModel> cq = cb.createQuery(InstrumentModel.class);
			Root<InstrumentModel> root = cq.from(InstrumentModel.class);
			Join<InstrumentModel, SalesCategory> salesCategoryJoin = root.join(InstrumentModel_.salesCategory);
			Join<InstrumentModel, InstrumentModelType> modelTypeJoin = root.join(InstrumentModel_.modelType);

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(
					cb.equal(salesCategoryJoin.get(SalesCategory_.id), instrumentModel.getSalesCategory().getId()));
			clauses.getExpressions().add(cb.isFalse(root.get(InstrumentModel_.quarantined)));
			clauses.getExpressions().add(cb.isFalse(modelTypeJoin.get(InstrumentModelType_.capability)));
			clauses.getExpressions().add(cb.isFalse(modelTypeJoin.get(InstrumentModelType_.salescategory)));

			return cq;
		});
	}

	@Override
	public List<InstrumentModel> getStandAloneModelsMatchingSalesCategoryAndModelName(
			SalesCategory salesCategoryToSearch, String modelNameToFind, Locale locale) {

		return getResultList(cb -> {
			CriteriaQuery<InstrumentModel> cq = cb.createQuery(InstrumentModel.class);

			Root<InstrumentModel> rootInstModel = cq.from(InstrumentModel.class);

			Join<InstrumentModel, Translation> joinedModelNameTranslation = rootInstModel
					.join(InstrumentModel_.nameTranslations);
			Join<InstrumentModel, InstrumentModelType> joinedModelType = rootInstModel.join(InstrumentModel_.modelType);

			Predicate conjunction = cb.conjunction();

			conjunction.getExpressions()
					.add(cb.equal(rootInstModel.get(InstrumentModel_.salesCategory), salesCategoryToSearch));
			conjunction.getExpressions().add(cb.notEqual(rootInstModel.get(InstrumentModel_.quarantined), true));
			conjunction.getExpressions().add(cb.equal(joinedModelNameTranslation.get(Translation_.locale), locale));
			conjunction.getExpressions()
					.add(cb.equal(joinedModelNameTranslation.get(Translation_.translation), modelNameToFind));
			conjunction.getExpressions()
					.add(cb.notEqual(joinedModelType.get(InstrumentModelType_.salescategory), true));
			conjunction.getExpressions().add(cb.notEqual(joinedModelType.get(InstrumentModelType_.capability), true));

			cq.where(conjunction);

			return cq;
		});
	}

	@Override
	public PagedResultSet<InstrumentModelForNewJobItemSearchDto> findCompanyOrGroupInstrumentModelForNewJobItemSearch(
			NewJobItemSearchInstrumentForm form) {
		val prs = new PagedResultSet<InstrumentModelForNewJobItemSearchDto>(form.getResultsPerPage(),
				form.getCompanyModelsPageNo());
		completePagedResultSet(prs, InstrumentModelForNewJobItemSearchDto.class, cb -> cq -> {
			val instrument = cq.from(Instrument.class);
			val instrumentModel = instrument.join(Instrument_.model);
			val description = instrumentModel.join(InstrumentModel_.description);
			val company = instrument.join(Instrument_.comp);

			val predicates = Stream
					.concat(calculateInstrumentModelPredicates(instrumentModel, description, form).apply(cb),
							searchByCompanyOrGroupOrEveryWherePredicate(cb, instrument, form))
					.toArray(Predicate[]::new);
			cq.where(predicates);
			val select = getInstrumentModelForNewJobItemSearchDtoCompoundSelection(cb, instrumentModel, description,
					company);
			return Triple.of(instrumentModel, select, Collections.emptyList());
		}, true);

		return prs;
	}

	private CompoundSelection<InstrumentModelForNewJobItemSearchDto> getInstrumentModelForNewJobItemSearchDtoCompoundSelection(
			CriteriaBuilder cb, Join<Instrument, InstrumentModel> instrumentModel,
			Join<InstrumentModel, Description> description, Join<Instrument, Company> company) {
		return cb.construct(InstrumentModelForNewJobItemSearchDto.class, instrumentModel.get(InstrumentModel_.modelid),
				modelNameWithTypology(instrumentModel, description).apply(cb), company.get(Company_.coname),
				company.get(Company_.coid));
	}

	@Override
	public PagedResultSet<InstrumentModelForNewJobItemSearchDto> findAllMatchingInstrumentModelForNewJobItemSearch(
			NewJobItemSearchInstrumentForm form) {
		val prs = new PagedResultSet<InstrumentModelForNewJobItemSearchDto>(form.getResultsPerPage(),
				form.getAllModelsPageNo());

		completePagedResultSet(prs, InstrumentModelForNewJobItemSearchDto.class, cb -> cq -> {
			val instrumentModel = cq.from(InstrumentModel.class);
			val description = instrumentModel.join(InstrumentModel_.description);

			val predicates = calculateInstrumentModelPredicates(instrumentModel, description, form).apply(cb)
					.toArray(Predicate[]::new);
			cq.where(predicates);
			val select = cb.construct(InstrumentModelForNewJobItemSearchDto.class,
					instrumentModel.get(InstrumentModel_.modelid),
					modelNameWithTypology(instrumentModel, description).apply(cb), cb.literal(""), cb.literal(0));
			return Triple.of(instrumentModel, select, Collections.emptyList());
		});
		return prs;
	}

	@Override
	public PagedResultSet<InstrumentModelForNewJobItemSearchDto> findModelsByIdsForNewJobItemSearch(
			NewJobItemSearchByLinkedQuotationForm form, List<Integer> modelIds) {
		val prs = new PagedResultSet<InstrumentModelForNewJobItemSearchDto>(form.getResultsPerPage(),
				form.getAllModelsPageNo());
		completePagedResultSet(prs, InstrumentModelForNewJobItemSearchDto.class, cb -> cq -> {
			val instrumentModel = cq.from(InstrumentModel.class);
			val description = instrumentModel.join(InstrumentModel_.description);

			cq.where(instrumentModel.in(modelIds));
			val select = cb.construct(InstrumentModelForNewJobItemSearchDto.class,
					instrumentModel.get(InstrumentModel_.modelid),
					modelNameWithTypology(instrumentModel, description).apply(cb), cb.literal(""), cb.literal(0));
			return Triple.of(instrumentModel, select, Collections.emptyList());

		});
		return prs;
	}

	@Override
	public PagedResultSet<InstrumentModelForNewJobItemSearchDto> findCompanyModelsByIdsForNewJobIntemSearch(
			NewJobItemSearchByLinkedQuotationForm form, List<Integer> modelIds) {
		val prs = new PagedResultSet<InstrumentModelForNewJobItemSearchDto>(form.getResultsPerPage(),
				form.getCompanyModelsPageNo());
		completePagedResultSet(prs, InstrumentModelForNewJobItemSearchDto.class, cb -> cq -> {

			val instrument = cq.from(Instrument.class);
			val instrumentModel = instrument.join(Instrument_.model);
			val description = instrumentModel.join(InstrumentModel_.description);
			val company = instrument.join(Instrument_.comp);

			cq.where(instrumentModel.in(modelIds), cb.equal(instrument.get(Instrument_.comp), form.getCompanyId()));
			val select = getInstrumentModelForNewJobItemSearchDtoCompoundSelection(cb, instrumentModel, description,
					company);
			return Triple.of(instrumentModel, select, Collections.emptyList());
		}, true);
		return prs;
	}

	private Function<CriteriaBuilder, Stream<Predicate>> calculateInstrumentModelPredicates(
			From<?, InstrumentModel> instrumentModel, Join<InstrumentModel, Description> description,
			NewJobItemSearchInstrumentForm form) {
		val mfr = instrumentModel.join(InstrumentModel_.mfr);
		val family = description.join(Description_.family);
		val domain = family.join(InstrumentModelFamily_.domain);
		return cb -> Stream.of(entityByIdPredicate(cb, mfr, form.getMfrId()),
				entityByIdPredicate(cb, description, form.getDescId()),
				entityByIdPredicate(cb, domain, form.getDomainId()),
				entityByIdPredicate(cb, family, form.getFamilyId()),
				entityByIdPredicate(cb, instrumentModel.get(InstrumentModel_.salesCategory), form.getSalesCatId()),
				form.getExcludeQuarantined() ? Stream.of(cb.isFalse(instrumentModel.get(InstrumentModel_.quarantined)))
						: Stream.<Predicate>empty(),
				StringUtils.isNotBlank(form.getModel()) ? Stream
						.of(ilike(cb, instrumentModel.get(InstrumentModel_.model), form.getModel(), MatchMode.ANYWHERE))
						: Stream.<Predicate>empty(),
				Stream.of(cb.isTrue(instrumentModel.join(InstrumentModel_.modelType, JoinType.INNER)
						.get(InstrumentModelType_.defaultModelType))))
				.flatMap(Function.identity());
	}

	private Function<CriteriaBuilder, Expression<String>> modelNameWithTypology(
			From<?, InstrumentModel> instrumentModel, Join<InstrumentModel, Description> description) {
		return cb -> trimAndConcatWithWhitespace(
				joinTranslation(cb, instrumentModel, InstrumentModel_.nameTranslations,
						LocaleContextHolder.getLocale()),
				calculateTypology(description.get(Description_.typology)).apply(cb)).apply(cb);
	}
}