package org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.db;

import java.util.Set;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;
import org.trescal.cwms.core.referential.dto.TMLChoiceDTO;

public interface CharacteristicLibraryDao extends BaseDao<CharacteristicLibrary, Integer>
{
	CharacteristicLibrary getByCharacteristicDescriptionIdAndTmlId(Integer characteristicDescriptionId, Integer tmlId);
	public ResultWrapper getLibrariesByCharacteristicDescription(int id);
	public ResultWrapper getLibrariesByModelRange(int id);
	public void updateTMLLibraries(int characteristicDescriptionId, Set<TMLChoiceDTO> dto);
}
