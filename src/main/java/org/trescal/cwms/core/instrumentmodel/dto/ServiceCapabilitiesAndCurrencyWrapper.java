package org.trescal.cwms.core.instrumentmodel.dto;

import java.math.BigDecimal;
import java.util.List;



public class ServiceCapabilitiesAndCurrencyWrapper {

	private String currencyCode;
	private String ERSymbol;
	private BigDecimal exchangeRate;
    private List<Integer> pricedCalibrationTypeIds;
	private String message;
	private boolean success;
	


	public ServiceCapabilitiesAndCurrencyWrapper(boolean success, String message, List<Integer> pricedCalibrationTypeIds,
			String currencyCode, String ERSymbol, BigDecimal exchangeRate) {
		this.setSuccess(success);
		this.setMessage(message);
		this.setPricedCalibrationTypeIds(pricedCalibrationTypeIds);
		this.setCurrencyCode(currencyCode);
		this.setERSymbol(ERSymbol);
		this.setExchangeRate(exchangeRate);
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getERSymbol() {
		return ERSymbol;
	}

	public void setERSymbol(String eRSymbol) {
		ERSymbol = eRSymbol;
	}

	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}


	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public List<Integer> getPricedCalibrationTypeIds() {
	    return pricedCalibrationTypeIds;
	}

	public void setPricedCalibrationTypeIds(List<Integer> pricedCalibrationTypeIds) {
		this.pricedCalibrationTypeIds = pricedCalibrationTypeIds;
	}



}
