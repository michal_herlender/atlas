package org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.db;

import org.apache.commons.lang3.tuple.Triple;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.CylindricalStandard;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.CylindricalStandard_;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread_;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.ThreadType;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.ThreadType_;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.ThreadUoM;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.ThreadUoM_;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.Wire;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.Wire_;
import org.trescal.cwms.core.instrumentmodel.dimensional.form.ThreadSearchForm;
import org.trescal.cwms.core.instrumentmodel.dto.ThreadSearchResultWrapper;
import org.trescal.cwms.core.tools.PagedResultSet;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static org.trescal.cwms.core.tools.StringJpaUtils.ilike;

@Repository("ThreadDao")
public class ThreadDaoImpl extends BaseDaoImpl<Thread, Integer> implements ThreadDao {
	
	@Override
	protected Class<Thread> getEntity() {
		return Thread.class;
	}
	
	@Override
	public List<Thread> findAllThreadsBySizeAndType(String type, String size) {
		return getResultList(cb->{
			CriteriaQuery<Thread> cq = cb.createQuery(Thread.class);
			Root<Thread> thread = cq.from(Thread.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(ilike(cb, thread.get(Thread_.size), size, MatchMode.START));
			if ((type != null) && !type.equalsIgnoreCase("all")){
				Join<Thread, ThreadType> typeJoin = thread.join(Thread_.type);
				cq.where(ilike(cb,typeJoin.get(ThreadType_.type), type));
			}
			cq.orderBy(cb.asc(thread.get(Thread_.size)));
			return cq;
		});
	}
	
	@Override
	public List<Thread> findAllThreadsByType(String type) {
		return getResultList(cb-> {
			CriteriaQuery<Thread> cq = cb.createQuery(Thread.class);
			Root<Thread> thread = cq.from(Thread.class);
			if ((type != null) && !type.equalsIgnoreCase("all")){
				Join<Thread, ThreadType> typeJoin = thread.join(Thread_.type);
				cq.where(ilike(cb,typeJoin.get(ThreadType_.type), type));
			}
			cq.orderBy(cb.asc(thread.get(Thread_.size)));
			return cq;
		});
	}
	
	@Override
	public PagedResultSet<Thread> searchThreads(ThreadSearchForm form, PagedResultSet<Thread> prs) {
		
		super.completePagedResultSet(prs, Thread.class, cb -> cq -> {
			
			Root<Thread> thread = cq.from(Thread.class);
			Predicate clauses = cb.conjunction();
			if ((form.getSize() != null) && !form.getSize().trim().equals("")){
				clauses.getExpressions().add(ilike(cb, thread.get(Thread_.size) , form.getSize(), MatchMode.ANYWHERE));
			}
			
			if (form.getCylindricalStandardId() != null){
				Join<Thread, CylindricalStandard> cylindricalStandardJoin = thread.join(Thread_.cylindricalStandard);
				clauses.getExpressions().add(cb.equal(cylindricalStandardJoin.get(CylindricalStandard_.id), form.getCylindricalStandardId()));
			}
			
			if (form.getWireId() != null){
				Join<Thread, Wire> wireJoin = thread.join(Thread_.wire);
				clauses.getExpressions().add(cb.equal(wireJoin.get(Wire_.id), form.getWireId()));
			}
		
			
			if (form.getUomId() != null){
				Join<Thread, ThreadUoM> uomJoin = thread.join(Thread_.uom);
				clauses.getExpressions().add(cb.equal(uomJoin.get(ThreadUoM_.id), form.getUomId()));	
			}
			
			if (form.getTypeId() != null){
				Join<Thread, ThreadType> typeJoin = thread.join(Thread_.type);
				clauses.getExpressions().add(cb.equal(typeJoin.get(ThreadType_.id), form.getTypeId()));
			}

			cq.where(clauses);
			return Triple.of(thread.get(Thread_.id), null, new ArrayList<>());
		});

		return prs;
	}
	
	@SuppressWarnings("unchecked")
	public List<ThreadSearchResultWrapper> searchThreadsHQL(String searchTerm, Integer typeId) {
		String typeterm = "";
		if (typeId != null) typeterm = " and dimensional_thread.type.id = " + typeId;
		String hql = "select new org.trescal.cwms.core.instrumentmodel.dto.ThreadSearchResultWrapper(dimensional_thread.id, dimensional_thread.size) "
				+ "from Thread dimensional_thread where dimensional_thread.size like :size"
				+ typeterm;
		Query query = this.getSession().createQuery(hql);
		query.setString("size", searchTerm + "%");
		List<ThreadSearchResultWrapper> procs = query.list();
		return procs;
	}
}