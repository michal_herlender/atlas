package org.trescal.cwms.core.instrumentmodel.dimensional.form;

import java.util.Collection;

import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.Data;

@Data
public class ThreadSearchForm
{
	private String size;
	private Integer wireId;
	private Integer typeId;
	private Double cylindricalStandardId;
	private Integer uomId;

	// results data
	private PagedResultSet<Thread> rs;
	private Collection<org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread> threads;
	private int jobItemCount;
	private String searchCriteria;
	private int pageNo;
	private int resultsPerPage;
}
