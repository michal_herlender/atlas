package org.trescal.cwms.core.instrumentmodel.dto;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;

@Getter
@Setter
public class ServiceCapabilityForm {

	private int id;
	private String subdiv;
	private String procedure;
	private String calibrationProcess;
	private String checkSheet;
	private String procedureRef;
	
	private Integer subdivId;
	private Integer modelId;
	private Integer serviceTypeId;
	private Integer procedureId;
	private Integer processId;

	/**
	 * Constructor used for an new serviceCapability only
	 */
	public ServiceCapabilityForm(Integer subdivId, String subname, Integer modelId) {
		this.subdivId = subdivId;
		this.subdiv = subname;
		this.id = 0;
		this.modelId = modelId;
		this.serviceTypeId = 0;
	}
	
	/**
	 * Constructor used for an existing serviceCapability only
	 * @param subname
	 * @param serviceCapability
	 * @param locale
	 * @param defaultLocale
	 */
	public ServiceCapabilityForm(Integer subdivId, String subname, ServiceCapability serviceCapability) {
        id = serviceCapability.getId();
        this.subdivId = subdivId;
        subdiv = subname;

        procedure = serviceCapability.getCapability() == null ? "" : serviceCapability.getCapability().getReference() + " - " + serviceCapability.getCapability().getName();
        calibrationProcess = serviceCapability.getCalibrationProcess() == null ? "" : serviceCapability.getCalibrationProcess().getProcess();
        checkSheet = serviceCapability.getChecksheet();

        modelId = serviceCapability.getInstrumentModel().getModelid();
        serviceTypeId = serviceCapability.getCalibrationType().getServiceType().getServiceTypeId();
        procedureId = serviceCapability.getCapability().getId();
        processId = serviceCapability.getCalibrationProcess() == null ? null : serviceCapability.getCalibrationProcess().getId();

        procedureRef = serviceCapability.getCapability().getReference();
    }

}