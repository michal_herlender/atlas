package org.trescal.cwms.core.instrumentmodel.dimensional.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.cylindricalstandard.db.CylindricalStandardService;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.Thread;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.thread.db.ThreadService;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threadtype.db.ThreadTypeService;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.db.ThreadUoMService;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.wire.db.WireService;
import org.trescal.cwms.core.instrumentmodel.dimensional.form.ThreadForm;

@Controller
public class ThreadController
{
	@Autowired
	private ThreadService threadServ;
	@Autowired
	private CylindricalStandardService cylStanServ;
	@Autowired
	private WireService wireServ;
	@Autowired
	private ThreadUoMService threadUOMServ;
	@Autowired
	private ThreadTypeService threadTypeServ;

	@ModelAttribute("command")
	protected Object formBackingObject(@RequestParam(name="id", required=false, defaultValue="0") Integer threadId) throws Exception
	{
		Thread thread = this.threadServ.get(threadId);

		ThreadForm form = new ThreadForm();
		if ((threadId == 0) || (thread == null))
		{
			form.setCreate(true);
			thread = new Thread();
		}
		else
		{
			form.setCreate(false);
			form.setCylindricalStandardId(thread.getCylindricalStandard() == null ? null : thread.getCylindricalStandard().getId());
			form.setTypeId(thread.getType() == null ? null : thread.getType().getId());
			form.setWireId(thread.getWire() == null ? null : thread.getWire().getId());
			form.setUomId(thread.getUom() == null ? null : thread.getUom().getId());
		}
		form.setThread(thread);
		return form;
	}
	
	@RequestMapping(value="/thread.htm", method=RequestMethod.POST)
	protected String onSubmit(Model model,
			@Valid @ModelAttribute("command") ThreadForm form, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) {
			return referenceData(model);
		}
		Thread thread = form.getThread();

		thread.setCylindricalStandard(form.getCylindricalStandardId() == null ? null : this.cylStanServ.get(form.getCylindricalStandardId()));
		thread.setWire(form.getWireId() == null ? null : this.wireServ.get(form.getWireId()));
		thread.setUom(form.getUomId() == null ? null : this.threadUOMServ.get(form.getUomId()));
		thread.setType(form.getTypeId() == null ? null : this.threadTypeServ.get(form.getTypeId()));

		if (thread.getId() == 0) {
			this.threadServ.save(thread);
		}
		else {
			this.threadServ.merge(thread);
		}
		return "redirect:thread.htm?id="+thread.getId();
	}

	@RequestMapping(value="/thread.htm", method=RequestMethod.GET)
	protected String referenceData(Model model) throws Exception
	{
		model.addAttribute("wires", this.wireServ.getAll());
		model.addAttribute("threaduoms", this.threadUOMServ.getAll());
		model.addAttribute("cylindricalstandards", this.cylStanServ.getAll());
		model.addAttribute("threadtypes", this.threadTypeServ.getAll());

		return "trescal/core/instrumentmodel/dimensional/thread";
	}
}
