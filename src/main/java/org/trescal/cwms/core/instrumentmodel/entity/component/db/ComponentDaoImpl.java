package org.trescal.cwms.core.instrumentmodel.entity.component.db;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component;
import org.trescal.cwms.core.instrumentmodel.entity.component.Component_;
import org.trescal.cwms.core.instrumentmodel.entity.componentdescription.ComponentDescription;
import org.trescal.cwms.core.instrumentmodel.entity.componentdescription.ComponentDescription_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.jobs.repair.dto.ComponentJsonDto;

import lombok.val;

@Repository("ComponentDao")
public class ComponentDaoImpl extends BaseDaoImpl<Component, Integer> implements ComponentDao {

	@Override
	protected Class<Component> getEntity() {
		return Component.class;
	}

	@Override
	public List<Component> getComponents(int mfrid, int descid, Integer cid) {

		return getResultList(cb -> {

			CriteriaQuery<Component> cq = cb.createQuery(Component.class);
			Root<Component> root = cq.from(Component.class);
			val mfr = root.join(Component_.mfr);
			val description = root.join(Component_.description);
			Predicate clauses = cb.conjunction();
			if (cid.intValue() != 0) {
				clauses.getExpressions().add(cb.notEqual(root.get(Component_.id), cid));
			}
			clauses.getExpressions().add(cb.equal(mfr.get(Mfr_.mfrid), mfrid));
			clauses.getExpressions().add(cb.equal(description.get(ComponentDescription_.id), descid));

			cq.where(clauses);

			return cq;
		});
	}

	@Override
	public List<Component> searchSortedModelComponent(Integer mfrid, Integer descid, Boolean inStock) {

		return getResultList(cb -> {

			CriteriaQuery<Component> cq = cb.createQuery(Component.class);
			Root<Component> root = cq.from(Component.class);
			val mfr = root.join(Component_.mfr);
			val description = root.join(Component_.description);
			Predicate clauses = cb.conjunction();

			if ((mfrid != null) && (mfrid != 0)) {
				clauses.getExpressions().add(cb.equal(mfr.get(Mfr_.mfrid), mfrid));
			}

			if ((descid != null) && (descid != 0)) {
				clauses.getExpressions().add(cb.equal(description.get(ComponentDescription_.id), descid));
			}

			if ((inStock != null) && (inStock == true)) {
				clauses.getExpressions().add(cb.ge(root.get(Component_.inStock), 1));
			}

			cq.where(clauses);
			cq.orderBy(cb.asc(mfr.get(Mfr_.name)), cb.asc(description.get(ComponentDescription_.description)),
					cb.asc(root.get(Component_.id)));
			return cq;
		});

	}

	@Override
	public List<ComponentJsonDto> searchComponents(String keyword, int maxResults) {

		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ComponentJsonDto> cq = cb.createQuery(ComponentJsonDto.class);

		Root<Component> root = cq.from(Component.class);
		Join<Component, ComponentDescription> componentDescriptionJoin = root.join(Component_.description);

		cq.select(cb.construct(ComponentJsonDto.class, root.get(Component_.id),
				componentDescriptionJoin.get(ComponentDescription_.description),
				componentDescriptionJoin.get(ComponentDescription_.description)));

		Predicate clauses = cb.conjunction();
		clauses.getExpressions()
				.add(cb.like(componentDescriptionJoin.get(ComponentDescription_.description), "%" + keyword + "%"));

		cq.where(clauses);
		return getEntityManager().createQuery(cq).setMaxResults(maxResults).getResultList();
	}
}