package org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrumentmodel.dimensional.entity.threaduom.ThreadUoM;

public interface ThreadUoMService extends BaseService<ThreadUoM, Integer>
{
}