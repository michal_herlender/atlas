package org.trescal.cwms.core.instrumentmodel.controller.description;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@Controller
public class DescriptionViewController {

	@Autowired
	private NewDescriptionService descriptionService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;

	@ModelAttribute("description")
	protected Description formBackingObject(@RequestParam(value = "id", required = true, defaultValue = "0") Integer Id)
			throws Exception {
		Description description = descriptionService.findDescription(Id);
		if (description == null)
			throw new Exception("Description with ID " + Id + " could not be found");
		return description;
	}

	@RequestMapping(value = "/descriptionview.htm", method = RequestMethod.GET)
	public ModelAndView referenceData(@ModelAttribute("description") Description description) throws Exception {
		Map<String, Object> model = new HashMap<String, Object>();
		if (description != null) {
			model.put("designationdefault", "");
			Set<Translation> translations = description.getTranslations();
			for (Translation __t : translations) {
				if (__t.getLocale().equals(supportedLocaleService.getPrimaryLocale())) {
					model.put("designationdefault", __t.getTranslation());
					break;
				}
			}
			model.put("translation", translations);
			model.put("modelscount", description.getModels().size());
		}
		model.put("supportedLocales", supportedLocaleService.getSupportedLocales());
		return new ModelAndView("trescal/core/instrumentmodel/description/descriptionview", model);
	}
}