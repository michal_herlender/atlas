package org.trescal.cwms.core.instrumentmodel.form;

import java.util.SortedSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

public class EditFamilyForm {

	private InstrumentModelFamily family;
	private SortedSet<Translation> translations;
	private String designationdefault;
	
	@Autowired
	private SupportedLocaleService supportedLocaleService;

	public SortedSet<Translation> getTranslations() {
		return translations;
	}

	public void setTranslations(SortedSet<Translation> translations) {
		this.translations = translations;
	}
	
	public String getDesignationdefault() {
		return designationdefault;
	}

	public void setDesignationdefault(String designationdefault) {
		this.designationdefault = designationdefault;
	}

	public SupportedLocaleService getSupportedLocaleService() {
		return supportedLocaleService;
	}

	public void setSupportedLocaleService(SupportedLocaleService supportedLocaleService) {
		this.supportedLocaleService = supportedLocaleService;
	}

	public InstrumentModelFamily getFamily() {
		return family;
	}

	public void setFamily(InstrumentModelFamily family) {
		this.family = family;
	}

}
