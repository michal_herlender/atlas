package org.trescal.cwms.core.instrumentmodel.form.description;

import java.util.Locale;

import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.Data;

@Data
public class SearchDescriptionForm {
	private String description;
	private Locale searchLanguage;
	private Locale displayLanguage;
	private PagedResultSet<Description> rs;
	private int firstResult;
	private int pageNo;
	private int resultsPerPage;
}