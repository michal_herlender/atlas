package org.trescal.cwms.core.instrumentmodel.entity.domain.db;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily_;
import org.trescal.cwms.core.referential.db.TMLLocaleResolver;
import org.trescal.cwms.core.referential.dto.TMLDomainDTO;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.spring.model.InstrumentModelDomainJsonDTO;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Repository("InstrumentModelDomainDao")
public class InstrumentModelDomainDaoImpl extends BaseDaoImpl<InstrumentModelDomain, Integer>
		implements InstrumentModelDomainDao {

	private Log logger = LogFactory.getLog(getClass());

	@Override
	protected Class<InstrumentModelDomain> getEntity() {
		return InstrumentModelDomain.class;
	}

	@Override
	public boolean existsByName(String name) {
		Integer count = getCount(cb -> cq -> {
			Root<InstrumentModelDomain> instModelDomainRoot= cq.from(InstrumentModelDomain.class);
			cq.where(cb.equal(instModelDomainRoot.get(InstrumentModelDomain_.name.getName()), name));
			return Triple.of(instModelDomainRoot.get(InstrumentModelDomain_.domainid), null, null);
		});
		return count != null && count > 0;
	}

	/*
	 * Returns all Instrument Model Families for the given current user locale.
	 */
	@Override
	public List<InstrumentModelDomainJsonDTO> getAllDomains(Locale searchLanguage) {
		return getResultList(cb -> {
		CriteriaQuery<InstrumentModelDomainJsonDTO> cq = cb.createQuery(InstrumentModelDomainJsonDTO.class);
		Root<InstrumentModelDomain> rootInstModelDomain = cq.from(InstrumentModelDomain.class);
		Join<InstrumentModelDomain, Translation> joinTranslation = rootInstModelDomain.join(InstrumentModelDomain_.translation, JoinType.LEFT);
		
		cq.select(cb.construct(InstrumentModelDomainJsonDTO.class, 
				rootInstModelDomain.get(InstrumentModelDomain_.domainid),
				cb.selectCase().when(cb.isNotNull(joinTranslation.get(Translation_.translation)), joinTranslation.get(Translation_.translation))
				.otherwise(rootInstModelDomain.get(InstrumentModelDomain_.name)),
				rootInstModelDomain.get(InstrumentModelDomain_.active),
				rootInstModelDomain.get(InstrumentModelDomain_.tmlid))
				 );

		cq.where(cb.equal(joinTranslation.get(Translation_.locale), searchLanguage));
		cq.orderBy(cb.asc(joinTranslation.get(Translation_.translation)));
		
		return cq;
		});
	}

	@Deprecated
	@Override
	public List<InstrumentModelDomainJsonDTO> getAllDomains() {
		return getResultList(cb -> {
			CriteriaQuery<InstrumentModelDomainJsonDTO> cq = cb.createQuery(InstrumentModelDomainJsonDTO.class);
			Root<InstrumentModelDomain> rootInstModelDomain = cq.from(InstrumentModelDomain.class);
			
			cq.select(cb.construct(InstrumentModelDomainJsonDTO.class, 
					rootInstModelDomain.get(InstrumentModelDomain_.domainid),
					rootInstModelDomain.get(InstrumentModelDomain_.name),
					rootInstModelDomain.get(InstrumentModelDomain_.active),
					rootInstModelDomain.get(InstrumentModelDomain_.tmlid)));
	
			return cq;
		});
	}

	// Gets a result count for the domain (matching exactly) in the specified search
	// language
	@Override
	public int getResultCountExact(String translation, Locale searchLanguage, int excludeId) {
		return getCount(cb -> cq -> {
	    	Root<InstrumentModelDomain> rootInstModelDomain = cq.from(InstrumentModelDomain.class);
		    Join<InstrumentModelDomain, Translation> joinTranslation = rootInstModelDomain.join(InstrumentModelDomain_.translation, JoinType.LEFT);
					    
		    Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(joinTranslation.get(Translation_.locale), searchLanguage));
			clauses.getExpressions().add(cb.equal(joinTranslation.get(Translation_.translation), translation));
			clauses.getExpressions().add(cb.notEqual(rootInstModelDomain.get(InstrumentModelDomain_.domainid), excludeId));
			
		    cq.where(clauses);
			return Triple.of(rootInstModelDomain.get(InstrumentModelDomain_.domainid), null, null);
		});
	}

	@Override
	public List<KeyValueIntegerString> getAllKeyValues(String domainFragment, DomainType domainType,
			Locale searchLanguage, int maxResults) {
		return getResultList(cb -> {
			CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class);
			Root<InstrumentModelDomain> domain = cq.from(InstrumentModelDomain.class);
			Expression<String> domainName = joinTranslation(cb, domain, InstrumentModelDomain_.translation,
					searchLanguage);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.like(domainName, '%' + domainFragment + '%'));
			if (domainType != null)
				clauses.getExpressions().add(cb.equal(domain.get(InstrumentModelDomain_.domainType), domainType));
			cq.where(clauses);
			cq.orderBy(cb.asc(domainName));
			cq.select(
					cb.construct(KeyValueIntegerString.class, domain.get(InstrumentModelDomain_.domainid), domainName));
			return cq;
		}, 0, maxResults);
	}

	@Override
	public InstrumentModelDomain getTMLDomain(int tmlid) {
		return getFirstResult(cb -> {
			CriteriaQuery<InstrumentModelDomain> cq=cb.createQuery(InstrumentModelDomain.class);
			Root<InstrumentModelDomain> instModelDomainRoot= cq.from(InstrumentModelDomain.class);
			cq.where(cb.equal(instModelDomainRoot.get(InstrumentModelDomain_.tmlid), tmlid));
			cq.select(instModelDomainRoot);
			return cq;
		}).orElse(null);
	}

	public void insertTMLInstrumentModelDomain(TMLDomainDTO dto) throws Exception {
		// First try to find if a domain already exists with the same name and not
		// linked to TML
		InstrumentModelDomain existingDomain = getFirstResult(cb -> {
		CriteriaQuery<InstrumentModelDomain> cq = cb.createQuery(InstrumentModelDomain.class);
		Root<InstrumentModelDomain> rootInstModelDomain = cq.from(InstrumentModelDomain.class);
		
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(rootInstModelDomain.get(InstrumentModelDomain_.name), dto.getDefaultName()));
		
		Predicate andClause = cb.conjunction();
		andClause.getExpressions().add(cb.isNotNull(rootInstModelDomain.get(InstrumentModelDomain_.tmlid)));
		andClause.getExpressions().add(cb.notEqual(rootInstModelDomain.get(InstrumentModelDomain_.tmlid), dto.getTMLID()));
		
		Predicate orClause = cb.disjunction();
		orClause.getExpressions().add(cb.isNull(rootInstModelDomain.get(InstrumentModelDomain_.tmlid)));
		orClause.getExpressions().add(andClause);
		
		clauses.getExpressions().add(orClause);
		
		cq.where(clauses);
		return cq;
		}).orElse(null);

		if (existingDomain == null) {
			// If no domain found, do the insert
			InstrumentModelDomain domain = new InstrumentModelDomain();
			domain.setTmlid(dto.getTMLID());
			domain.setName(dto.getDefaultName());
			domain.setActive(dto.isIsEnabled());

			SortedSet<Translation> newTranslations = new TreeSet<Translation>();

			Iterator<String> iterator = dto.getRegionalNames().keySet().iterator();

			while (iterator.hasNext()) {
				String lang = iterator.next();
				String value = dto.getRegionalNames().get(lang);
				if (TMLLocaleResolver.isResolvable(lang)) {
					Locale locale = TMLLocaleResolver.resolveLocale(lang); 
					Translation t = new Translation(locale, !value.equals(null) ? value.toString() : "");
					newTranslations.add(t);
				}
			}

			domain.setTranslation(newTranslations);

			logger.debug("Inserting: " + domain.toFullString());
			this.persist(domain);
			// Add this to avoid locks
			logger.debug("Inserted: " + domain.toFullString());
		} else {
			if (existingDomain.getTmlid() != null && !existingDomain.getTmlid().equals(dto.getTMLID())) {
				String errorMessage = MessageFormat.format(
						"Error while inserting, the domain {0} already exists with a different TMLID.",
						dto.getDefaultName());
				logger.error(errorMessage);
				throw new Exception(errorMessage);
			} else {
				if (existingDomain.getTmlid() == null) {
					existingDomain.setTmlid(dto.getTMLID());
					this.merge(existingDomain);
					// Add this to avoid locks
				}

				if (dto.isIsEnabled()) {
					this.updateTMLDomain(dto);
				} else {
					this.deleteTMLDomain(dto);
				}
			}
		}
	}

	@Override
	public void updateTMLDomain(TMLDomainDTO dto) throws Exception {
		InstrumentModelDomain domain = this.getTMLDomain(dto.getTMLID());

		// First try to find if a domain already exists with the same name and not
		// linked to TML
		InstrumentModelDomain existingDomain = getFirstResult(cb -> {
		CriteriaQuery<InstrumentModelDomain> cq = cb.createQuery(InstrumentModelDomain.class);
		Root<InstrumentModelDomain> rootInstModelDomain = cq.from(InstrumentModelDomain.class);
		
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(rootInstModelDomain.get(InstrumentModelDomain_.name), dto.getDefaultName()));
		clauses.getExpressions().add(cb.equal(rootInstModelDomain.get(InstrumentModelDomain_.domainid), domain.getDomainid()));
		
		Predicate andClause = cb.conjunction();
		andClause.getExpressions().add(cb.isNotNull(rootInstModelDomain.get(InstrumentModelDomain_.tmlid)));
		andClause.getExpressions().add(cb.notEqual(rootInstModelDomain.get(InstrumentModelDomain_.tmlid), dto.getTMLID()));
		
		Predicate orClause = cb.disjunction();
		orClause.getExpressions().add(cb.isNull(rootInstModelDomain.get(InstrumentModelDomain_.tmlid)));
		orClause.getExpressions().add(andClause);
		
		clauses.getExpressions().add(orClause);
		
		cq.where(clauses);
		
		return cq;
		}).orElse(null);

		if (existingDomain == null) {
			domain.setTmlid(dto.getTMLID());
			domain.setName(dto.getDefaultName());
			domain.setActive(dto.isIsEnabled());

			SortedSet<Translation> newTranslations = new TreeSet<Translation>();

			Iterator<String> iterator = dto.getRegionalNames().keySet().iterator();

			while (iterator.hasNext()) {
				String lang = iterator.next();
				String value = dto.getRegionalNames().get(lang);
				if (TMLLocaleResolver.isResolvable(lang)) {
					Locale locale = TMLLocaleResolver.resolveLocale(lang); 
					Translation t = new Translation(locale, !value.equals(null) ? value.toString() : "");
					newTranslations.add(t);
				}
			}

			domain.setTranslation(newTranslations);

			logger.debug("Updating: " + domain.toFullString());
			this.merge(domain);
			// Add this to avoid locks
			logger.debug("Updated: " + domain.toFullString());
		} else {
			String errorMessage = MessageFormat.format("Error while updating, the domain {0} already exists with a different TMLID.",
							dto.getDefaultName());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		}
	}

	@Override
	public void deleteTMLDomain(TMLDomainDTO dto) throws Exception {
		// Check if this domain is already used by active instrument
		Integer count = getCount(cb -> cq -> {
	    	Root<Instrument> rootInst = cq.from(Instrument.class);
		    Join<Instrument, InstrumentModel> joinInstModel = rootInst.join(Instrument_.model);
		    Join<InstrumentModel, Description> joinDescription = joinInstModel.join(InstrumentModel_.description);
		    Join<Description, InstrumentModelFamily> joinInstModelFamily = joinDescription.join(Description_.family);
		    Join<InstrumentModelFamily, InstrumentModelDomain> joinInstModelDomain = joinInstModelFamily.join(InstrumentModelFamily_.domain);
					    
		    cq.where(cb.equal(joinInstModelDomain.get(InstrumentModelDomain_.tmlid), dto.getTMLID()));
			return Triple.of(rootInst.get(Instrument_.plantid), null, null);
		});

		if (count <= 0) {
			InstrumentModelDomain domain = this.getTMLDomain(dto.getTMLID());

			domain.setActive(dto.isIsEnabled());

			logger.debug("Deleting: " + domain.toFullString());
			this.merge(domain);
			// Add this to avoid locks
			logger.debug("Deleted: " + domain.toFullString());
		} else {
			String errorMessage = MessageFormat.format(
					"Error while deleting, the domain {0} is used by at least 1 active instrument.",
					dto.getDefaultName());
			logger.error(errorMessage);
			throw new Exception(errorMessage);
		}
	}
}