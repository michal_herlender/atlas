package org.trescal.cwms.core.instrumentmodel.controller.services;

import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.dto.ServiceCapabilityForm;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapabilityFormValidator;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db.ServiceCapabilityService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE })
@RequestMapping("editmodelservices.htm")
public class EditServiceCapabilityController {

	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private ServiceCapabilityService serviceCapabilityService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private CalibrationProcessService calibrationProcessService;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private ServiceCapabilityFormValidator validator;

	public static final String FORM_NAME = "form";

	@ModelAttribute("processes")
	public List<CalibrationProcess> initialiseProcesses() {
		return this.calibrationProcessService.getAll();
	}

	@ModelAttribute(FORM_NAME)
	public ServiceCapabilityForm initializeServiceCapablityDTO(
			@RequestParam(value = "id", required = false, defaultValue = "0") int id,
			@RequestParam(value = "modelid", required = false, defaultValue = "0") int modelId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		
		if (id == 0) {
			return new ServiceCapabilityForm(subdivDto.getKey(), subdivDto.getValue(), modelId);
		} 
		else {
			ServiceCapability instance = this.serviceCapabilityService.get(id);
			ServiceCapabilityForm dto = new ServiceCapabilityForm(subdivDto.getKey(), subdivDto.getValue(),
					instance);

			return dto;
		}
	}

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@RequestMapping(method = RequestMethod.GET)
	public String referenceData(Model model,
			@ModelAttribute(FORM_NAME) ServiceCapabilityForm dto) {
		Locale locale = LocaleContextHolder.getLocale();
		if (dto.getId() == 0) {
			model.addAttribute("instrumentModel", instrumentModelService.findInstrumentModel(dto.getModelId()));
		}
		else {
			ServiceCapability capability = this.serviceCapabilityService.get(dto.getId());
			model.addAttribute("instrumentModel", capability.getInstrumentModel());
			model.addAttribute("longServiceType", translationService.getCorrectTranslation(capability.getCalibrationType().getServiceType().getLongnameTranslation(), locale));
			model.addAttribute("shortServiceType", translationService.getCorrectTranslation(capability.getCalibrationType().getServiceType().getShortnameTranslation(), locale));
		}
		model.addAttribute("costType", CostType.CALIBRATION);
		model.addAttribute("serviceTypes", this.serviceTypeService.getDTOList(DomainType.INSTRUMENTMODEL, null, locale, true));
		
		return "trescal/core/instrumentmodel/services/editservicecapability";
	}

	@RequestMapping(params = "delete", method = RequestMethod.POST)
	public String processDelete(@RequestParam(value = "id", required = false, defaultValue = "0") int id) {
		ServiceCapability service = serviceCapabilityService.get(id);
		serviceCapabilityService.delete(service);
		return "redirect:instrumentmodelservices.htm?modelid=" + service.getInstrumentModel().getModelid();
	}

	@RequestMapping(params = "return", method = RequestMethod.POST)
	public String doNothing(@RequestParam(value = "modelid") int modelid) {
		return "redirect:instrumentmodelservices.htm?modelid=" + modelid;
	}

	@RequestMapping(params = "save", method = RequestMethod.POST)
	public String processSave(Model model, 
			@Valid @ModelAttribute(FORM_NAME) ServiceCapabilityForm dto, BindingResult bindingResult,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {

		if (bindingResult.hasErrors()) {
			return referenceData(model, dto);
		}
		if (dto.getId() == 0) {
			this.serviceCapabilityService.createServiceCapability(dto);
		}
		else {
			this.serviceCapabilityService.updateServiceCapability(dto);
		}

		return "redirect:instrumentmodelservices.htm?modelid=" + dto.getModelId();
	}

}