package org.trescal.cwms.core.instrumentmodel.entity.modelrange.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;

public interface ModelRangeDao extends BaseDao<ModelRange, Integer> {
	
	void deleteAll(Collection<ModelRange> ranges);
	
	ModelRange getByModelIdAndOptionId(int modelId, int optionId);
	
	List<ModelRange> loadAll(List<Integer> ids);
}