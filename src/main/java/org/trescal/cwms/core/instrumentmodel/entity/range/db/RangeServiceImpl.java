package org.trescal.cwms.core.instrumentmodel.entity.range.db;

import java.util.List;

import org.trescal.cwms.core.instrumentmodel.entity.range.Range;

public class RangeServiceImpl implements RangeService
{
	RangeDao RangeDao;

	public Range findRange(int id)
	{
		return RangeDao.find(id);
	}

	public void insertRange(Range Range)
	{
		RangeDao.persist(Range);
	}

	public void updateRange(Range Range)
	{
		RangeDao.update(Range);
	}

	public List<Range> getAllRanges()
	{
		return RangeDao.findAll();
	}

	public void setRangeDao(RangeDao RangeDao)
	{
		this.RangeDao = RangeDao;
	}

	public void deleteRange(Range range)
	{
		this.RangeDao.remove(range);
	}

	public void saveOrUpdateRange(Range range)
	{
		this.RangeDao.saveOrUpdate(range);
	}
}