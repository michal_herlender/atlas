/**
 * 
 */
package org.trescal.cwms.core.instrumentmodel.entity.modelnote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;

/**
 * Entity for recording any {@link Note}s about an {@link InstrumentModel}.
 */
@Entity
@Table(name = "modelnote", uniqueConstraints = { @UniqueConstraint(columnNames = { "modelid", "noteid" }) })
public class ModelNote extends Note
{
	public final static boolean privateOnly = true;
	
	private InstrumentModel model;
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelid", nullable = false)
	public InstrumentModel getModel() {
		return this.model;
	}
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.INSTRUMENTMODELNOTE;
	}
	
	public void setModel(InstrumentModel model) {
		this.model = model;
	}
}