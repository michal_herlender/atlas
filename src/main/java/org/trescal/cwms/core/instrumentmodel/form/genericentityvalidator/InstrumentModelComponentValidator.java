package org.trescal.cwms.core.instrumentmodel.form.genericentityvalidator;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent.InstrumentModelComponent;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelcomponent.db.InstrumentModelComponentService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

/**
 * Validator for checking {@link InstrumentModelComponent} entities.
 * 
 * @author Richard
 */
public class InstrumentModelComponentValidator extends AbstractBeanValidator
{
	private InstrumentModelComponentService instModelCompServ;

	public void setInstModelCompServ(InstrumentModelComponentService instModelCompServ)
	{
		this.instModelCompServ = instModelCompServ;
	}

	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.isAssignableFrom(InstrumentModelComponent.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		InstrumentModelComponent comp = (InstrumentModelComponent) target;
		if (errors == null)
		{
			errors = new BindException(comp, "comp");
		}

		// do the hibernate validation
		super.validate(comp, errors);

		// do the business validation
		if ((comp.getModel() != null) && (comp.getComponent() != null))
		{
			if (this.instModelCompServ.getInstrumentModelComponents(comp.getModel().getModelid(), comp.getComponent().getId(), comp.getId()).size() > 0)
			{
				errors.rejectValue("model", null, "This model already has this component type assigned");
			}
		}
	}
}
