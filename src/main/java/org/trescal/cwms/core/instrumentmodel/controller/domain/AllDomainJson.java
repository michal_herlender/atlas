package org.trescal.cwms.core.instrumentmodel.controller.domain;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.instrumentmodel.entity.domain.db.InstrumentModelDomainService;
import org.trescal.cwms.spring.model.InstrumentModelDomainJsonDTO;

@Controller
public class AllDomainJson {
@Autowired
private InstrumentModelDomainService instModDomainServ;
@Autowired
private MessageSource messages;
@RequestMapping(value="/alldomains.json", method=RequestMethod.GET)
public @ResponseBody List<InstrumentModelDomainJsonDTO> getAllDomains(Locale locale) {
Locale userLocale = LocaleContextHolder.getLocale();
List<InstrumentModelDomainJsonDTO> insts = instModDomainServ.getAllDomains(userLocale);
insts.stream().forEach(i -> {
	if(i.getActive().equals("Yes"))
		i.setActive(messages.getMessage("yes", null, locale));
	else
		i.setActive(messages.getMessage("no", null, locale));
		});
	return  insts;
}

}
