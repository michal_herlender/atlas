package org.trescal.cwms.core.instrumentmodel.controller.configuration;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditModelCharacteristicForm {

	@NotNull
	private Integer modelId;

	@Valid
	private List<EditModelCharacteristicDto> characteristics;

	// Partially implemented; reserved for future use
	private List<EditModelOptionDto> options;
}
