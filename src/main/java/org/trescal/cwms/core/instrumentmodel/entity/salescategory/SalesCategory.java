package org.trescal.cwms.core.instrumentmodel.entity.salescategory;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name="salescategory")
public class SalesCategory extends Versioned {
	private int id;
	private Set<Translation> translations;
	private Boolean active;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	public int getId()
	{
		return this.id;
	}
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="salescategorytranslation", joinColumns=@JoinColumn(name="id"))
	public Set<Translation> getTranslations() {
		return translations;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public void setTranslations(Set<Translation> translations) {
		this.translations = translations;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
}
