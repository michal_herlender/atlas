package org.trescal.cwms.core.instrumentmodel.entity.description;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Enum order corresponds to TML definition to allow future direct mapping where:   
 * 0 - UNDEFINED - not used / defined in TML (but could be used for non-TML models in Crocodile)
 * 1 - BM = Brand Model
 * 2 - SFC - Subfamily Characteristics
 * @author Galen
 */
public enum SubfamilyTypology {
	UNDEFINED,
	BM,
	SFC;
	
	public static final Logger logger = LoggerFactory.getLogger(SubfamilyTypology.class);
	
	public static SubfamilyTypology convertFromInteger(Integer tmlTypology) {
		SubfamilyTypology result = UNDEFINED;
		if (tmlTypology == null) {
			logger.warn("null tmlTypology, mapping to UNDEFINED");
		}
		else {
			switch (tmlTypology) {
			case 0:
				result = UNDEFINED;
				break;
			case 1:
				result = BM;
				break;
			case 2:
				result = SFC;
				break;
			default:
				logger.warn("tmlTypology "+tmlTypology+", mapping to UNDEFINED");
				break;
			}
		}
		return result;
	}
}
