package org.trescal.cwms.core.main.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.upcomingwork.db.UpcomingWorkService;
import org.trescal.cwms.core.workallocation.engineerallocation.db.EngineerAllocationService;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.ServletException;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashMap;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class CwmsController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EngineerAllocationService eaServ;
    @Autowired
    private JobItemService jiServ;
    @Autowired
    private UpcomingWorkService uwServ;

    @RequestMapping(value = "/home.htm")
    public String handleRequest(Model model,
                                @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws ServletException, IOException {
        LocalDate currentWeekStart = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).with(DayOfWeek.MONDAY);
        LocalDate currentWeekFinish = currentWeekStart.plusDays(6);
        LocalDate fourWeekFinish = currentWeekStart.plusDays(27);

        loadJobItemData(model, subdivDto.getKey());

        Long weeklyWorkCount = this.uwServ.countUpcomingWorkBetweenDates(currentWeekStart, currentWeekFinish, subdivDto.getKey());
        Long weeklyAllocationCount = this.eaServ.countEngineerAllocationsBetweenDates(currentWeekStart, currentWeekFinish, subdivDto.getKey());
        Long futureWorkCount = this.uwServ.countUpcomingWorkBetweenDates(currentWeekStart, fourWeekFinish, subdivDto.getKey());
        Long futureAllocationCount = this.eaServ.countEngineerAllocationsBetweenDates(currentWeekStart, fourWeekFinish, subdivDto.getKey());

        logger.debug("weeklyWorkCount : " + weeklyWorkCount);
        logger.debug("weeklyAllocationCount :" + weeklyAllocationCount);
        logger.debug("futureWorkCount : " + futureWorkCount);
        logger.debug("futureAllocationCount :" + futureAllocationCount);

        model.addAttribute("futureTotal", futureWorkCount + futureAllocationCount);
        model.addAttribute("weeklyTotal", weeklyWorkCount + weeklyAllocationCount);
        return "trescal/core/jobs/job/activejobs";
    }
	
	protected void loadJobItemData(Model model, int subdivId) {
        // Load it all into a map for the command object
        HashMap<String, Integer> pagemap = new HashMap<>();
        pagemap.put("five", this.jiServ.getCountJobItemsByDate("5", subdivId));
        pagemap.put("four", this.jiServ.getCountJobItemsByDate("4", subdivId));
        pagemap.put("three", this.jiServ.getCountJobItemsByDate("3", subdivId));
        pagemap.put("two", this.jiServ.getCountJobItemsByDate("2", subdivId));
        pagemap.put("one", this.jiServ.getCountJobItemsByDate("1", subdivId));
        pagemap.put("zero", this.jiServ.getCountJobItemsByDate("0", subdivId));
        pagemap.put("un", this.jiServ.getCountJobItemsByDate("un", subdivId));
        int total = 0;
        for (Integer value : pagemap.values()) total += value;
        pagemap.put("total", total);
        model.addAttribute("pagemap", pagemap);
    }

}