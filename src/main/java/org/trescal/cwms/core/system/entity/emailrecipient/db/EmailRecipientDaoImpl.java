package org.trescal.cwms.core.system.entity.emailrecipient.db;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.email.Email_;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipientDto;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient_;

@Repository("EmailRecipientDao")
public class EmailRecipientDaoImpl extends BaseDaoImpl<EmailRecipient, Integer> implements EmailRecipientDao {
	
	@Override
	protected Class<EmailRecipient> getEntity() {
		return EmailRecipient.class;
	}
	
	@Override
	public List<EmailRecipientDto> searchRecipientsDto(Collection<Integer> emailIds) {
		List<EmailRecipientDto> results = null;
		if (emailIds != null) {
			results = Collections.emptyList();
		}
		else {
			results = getResultList(cb -> {
				CriteriaQuery<EmailRecipientDto> cq = cb.createQuery(EmailRecipientDto.class);
				Root<EmailRecipient> root = cq.from(EmailRecipient.class);
				Join<EmailRecipient, Email> email = root.join(EmailRecipient_.email);
				Join<EmailRecipient, Contact> contact = root.join(EmailRecipient_.recipientCon, JoinType.LEFT);
				cq.where(email.get(Email_.id).in(emailIds));
				
				Selection<EmailRecipientDto> selection = cb.construct(EmailRecipientDto.class, 
						email.get(Email_.id),
						root.get(EmailRecipient_.emailAddress),
						root.get(EmailRecipient_.id),
						contact.get(Contact_.personid),
						contact.get(Contact_.firstName),
						contact.get(Contact_.lastName),
						root.get(EmailRecipient_.type));
				
				cq.select(selection);
				
				return cq;
			});
		}
		return results;
	}
}