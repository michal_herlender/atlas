package org.trescal.cwms.core.system.entity.calibrationtype.db;

import lombok.val;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.entity.jobtypecaltype.JobTypeCalType;
import org.trescal.cwms.core.jobs.job.entity.jobtypecaltype.JobTypeCalType_;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.calibrationtype.dto.CalibrationTypeOptionDto;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.tools.StringJpaUtils;

import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository("CalibrationTypeDao")
public class CalibrationTypeDaoImpl extends BaseDaoImpl<CalibrationType, Integer> implements CalibrationTypeDao {

	@Override
	protected Class<CalibrationType> getEntity() {
		return CalibrationType.class;
	}

	@Override
	public List<CalibrationType> getCalTypes(Boolean activeOnly) {
        return getResultList(cb -> {
            CriteriaQuery<CalibrationType> cq = cb.createQuery(CalibrationType.class);
            Root<CalibrationType> calType = cq.from(CalibrationType.class);

            cq.select(calType);
            if (activeOnly != null && activeOnly) {
                cq.where(cb.equal(calType.get(CalibrationType_.active), true));
            }
            cq.orderBy(cb.asc(calType.get(CalibrationType_.orderBy)));
            return cq;
        });
    }
	
	@Override
	public List<Tuple> getCalTypeAndServiceTypeIds() {
		return getResultList(cb -> {
			CriteriaQuery<Tuple> cq = cb.createTupleQuery();
			Root<CalibrationType> root = cq.from(CalibrationType.class);
			Join<CalibrationType, ServiceType> serviceType = root.join(CalibrationType_.serviceType);
			
			cq.multiselect(root.get(CalibrationType_.calTypeId),
					serviceType.get(ServiceType_.serviceTypeId));
			
			return cq;
		});
	}


	@Override
	public List<CalibrationType> getActiveCalTypes(JobType jobType) {
		return getResultList(cb ->{
			CriteriaQuery<CalibrationType> cq = cb.createQuery(CalibrationType.class);
			Root<CalibrationType> root = cq.from(CalibrationType.class);
			Join<CalibrationType, JobTypeCalType> jobTypes = root.join(CalibrationType_.jobTypes, javax.persistence.criteria.JoinType.LEFT);
			cq.where(cb.equal(jobTypes.get(JobTypeCalType_.jobType), jobType));
			cq.orderBy(cb.asc(root.get(CalibrationType_.orderBy)));
			return cq;
		});
	}

	@Override
	public List<CalibrationType> getActiveProcedureCalTypes() {
        return getResultList(cb -> {
            val cq = cb.createQuery(CalibrationType.class);
            Root<CalibrationType> calType = cq.from(CalibrationType.class);
            calType.join(CalibrationType_.accreditationLevels, javax.persistence.criteria.JoinType.INNER);
            // Needed to use in DWR (blah)
            calType.fetch(CalibrationType_.accreditationLevels, javax.persistence.criteria.JoinType.LEFT);

            cq.distinct(true);
            cq.where(cb.equal(calType.get(CalibrationType_.active), true));
            cq.orderBy(cb.asc(calType.get(CalibrationType_.orderBy)));
            return cq;
        });
    }

    @Override
	public CalibrationType getDefaultCalType() {
		CalibrationType calType = getFirstResult(cb ->{
			CriteriaQuery<CalibrationType> cq = cb.createQuery(CalibrationType.class);
			Root<CalibrationType> root = cq.from(CalibrationType.class);
			cq.orderBy(cb.asc(root.get(CalibrationType_.orderBy)));
			return cq;
		}).orElse(null);
		
		if(calType == null){
			throw new RuntimeException("Database configuration error - no cal types defined!");
		}
		return calType;
	}

	@Override
	public Boolean isCompatibleWithJobType(CalibrationType caltype, JobType jobtype) {
        return getFirstResult(cb -> {
            CriteriaQuery<JobTypeCalType> cq = cb.createQuery(JobTypeCalType.class);
            Root<JobTypeCalType> root = cq.from(JobTypeCalType.class);
            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.equal(root.get(JobTypeCalType_.jobType.getName()), jobtype));
            clauses.getExpressions().add(cb.equal(root.get(JobTypeCalType_.calType.getName()), caltype));
            cq.where(clauses);
            return cq;
        }).isPresent();
    }

    @Override
    public List<CalibrationTypeOptionDto> getActiveCalTypesOptions(JobType jobType) {
		return getResultList(cb -> {
			val cq = cb.createQuery(CalibrationTypeOptionDto.class);
			val calibrationType = cq.from(CalibrationType.class);
			val serviceType = calibrationType.join(CalibrationType_.serviceType);
			val locale = LocaleContextHolder.getLocale();
			cq.where(cb.equal(calibrationType.join(CalibrationType_.jobTypes).get(JobTypeCalType_.jobType),jobType));
			cq.select(cb.construct(CalibrationTypeOptionDto.class,
					calibrationType.get(CalibrationType_.calTypeId),
					StringJpaUtils.trimAndConcatWithSeparator(
							joinTranslation(cb,serviceType,ServiceType_.shortnameTranslation,locale),
							joinTranslation(cb,serviceType,ServiceType_.longnameTranslation,locale),
							" - "
					).apply(cb)
					));
			return cq;
		});
	}
}