package org.trescal.cwms.core.system.entity.labeltemplate.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.system.entity.labeltemplate.LabelTemplate;
import org.trescal.cwms.core.system.entity.labeltemplate.LabelTemplateType;

public interface LabelTemplateService extends BaseService<LabelTemplate, Integer> {
	LabelTemplate getLabelTemplateById(int id);
	LabelTemplate getLabelTemplateByLabelTemplateType(LabelTemplateType labeltemplatetype);
}
