package org.trescal.cwms.core.system.entity.basestatus.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.system.entity.basestatus.BaseStatus;

public interface BaseStatusService extends BaseService<BaseStatus, Integer>
{
	<Status extends BaseStatus> Status  findDefaultStatus(Class<Status> clazz);

	<Status extends BaseStatus> Status  findStatusByName(String name, Class<Status> clazz);
}