package org.trescal.cwms.core.system.entity.emailcontent.web;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.InstrumentValidationIssue;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;

@Component
public class EmailContent_InstrumentValidation {
	@Autowired
	private BusinessDetailsService bdServ;
	@Autowired
	private MessageSource messages; 
	@Autowired
	private TemplateEngine templateEngine;

	@Value("${cwms.deployed.url}")
	private String deployedURL;
	
	public static final String THYMELEAF_VIEW = "/email/portal/instrumentvalidation.html";

	public EmailContentDto getContent(InstrumentValidationIssue instValIssue, Locale locale) {
		Contact businessContact = instValIssue.getRaisedBy().getSub().getComp().getDefaultBusinessContact();
		
		BusinessDetails bdetails = this.bdServ.getAllBusinessDetails(businessContact, null, null);

		String body = getBodyThymeleaf(instValIssue, bdetails, locale);
		String subject = getSubject(instValIssue.getInstrument(), locale);
		return new EmailContentDto(body, subject);
	}

	private String getBodyThymeleaf(InstrumentValidationIssue instValIssue, BusinessDetails bdetails, Locale locale) {
		String instrumentPath = this.deployedURL
				+ "viewinstrument.htm?plantid="
				+ instValIssue.getInstrument().getPlantid() + "&loadtab=validation-tab";

		Context context = new Context(locale);
		context.setVariable("contact", instValIssue.getRaisedBy());
		context.setVariable("instrument", instValIssue.getInstrument());
		context.setVariable("instrumentPath", instrumentPath);
		context.setVariable(Constants.SESSION_ATTRIBUTE_BUSINESS_DETAILS, bdetails);
		
		String body = this.templateEngine.process(THYMELEAF_VIEW, context);
		return body;
	}

	private String getSubject(Instrument inst, Locale locale) {
		return this.messages.getMessage("email.instrumentvalidation.subject", new Object[] {inst.getPlantid()}, "Validation issue raised with instrument " + inst.getPlantid(), locale);
	}
	
}
