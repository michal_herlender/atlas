package org.trescal.cwms.core.system.entity.presetcomment.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;

public interface PresetCommentDao extends AllocatedDao<Subdiv, PresetComment, Integer> {

	List<PresetComment> getAll(PresetCommentType type);

	List<PresetComment> getAll(PresetCommentType type, Subdiv allocatedSubdiv);
}