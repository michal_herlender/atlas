package org.trescal.cwms.core.system.entity.accreditationbodyresource.db;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.accreditationbody.AccreditationBody;
import org.trescal.cwms.core.system.entity.accreditationbodyresource.AccreditationBodyResource;
import org.trescal.cwms.core.system.entity.accreditationbodyresource.AccreditationBodyResourceType;

@Repository
public class AccreditationBodyResourceDaoImpl extends BaseDaoImpl<AccreditationBodyResource, Integer>
		implements AccreditationBodyResourceDao {

	@Override
	public AccreditationBodyResource getForBodyAndType(AccreditationBody body,
			AccreditationBodyResourceType type) {
		Criteria criteria = getSession().createCriteria(AccreditationBodyResource.class);
		criteria.add(Restrictions.eq("accreditationBody", body));
		criteria.add(Restrictions.eq("type", type));
		return (AccreditationBodyResource) criteria.uniqueResult();
	}

	@Override
	protected Class<AccreditationBodyResource> getEntity() {
		return AccreditationBodyResource.class;
	}

}
