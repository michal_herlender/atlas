package org.trescal.cwms.core.system.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.presetcomment.db.PresetCommentService;
import org.trescal.cwms.core.system.entity.presetcommentcategory.db.PresetCommentCategoryService;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class PresetCommentEditController {

	@Autowired
	private PresetCommentService pscServ;
	@Autowired
	private PresetCommentCategoryService pscCaterogyService;
	@Autowired
	private SubdivService subdivService;

	@RequestMapping(value = "/editpresetcomments.htm")
	protected ModelAndView handleRequestInternal(
			@RequestParam(value = "type", required = false, defaultValue = "") PresetCommentType type,
			@RequestParam(value = "notetype", required = false, defaultValue = "") NoteType noteType,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto) throws Exception {

		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		HashMap<String, Object> model = new HashMap<String, Object>();
		// try and get the PresetComment class from the given NoteType
		if (noteType != null && !noteType.getPresetCommentType().equals(PresetCommentType.WITHOUT)) {
			model.put("comments", pscServ.getAll(noteType.getPresetCommentType(), allocatedSubdiv));
			model.put("categories", pscCaterogyService.getAllByType(noteType.getPresetCommentType()));
		} else if (type != null) {
			model.put("comments", pscServ.getAll(type, allocatedSubdiv));
			model.put("categories", pscCaterogyService.getAllByType(type));
		} else
			model.put("status", "failed");
		model.put("type", type);
		model.put("notetype", noteType);
		return new ModelAndView("trescal/core/system/presetcomments", "model", model);
	}
}