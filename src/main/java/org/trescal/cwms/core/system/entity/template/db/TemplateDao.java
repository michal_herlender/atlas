package org.trescal.cwms.core.system.entity.template.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.template.Template;

public interface TemplateDao extends BaseDao<Template, Integer> {
	
	Template findByName(String name);
}