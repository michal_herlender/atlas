package org.trescal.cwms.core.system.entity.emailcontent.core;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequest;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;

@Component
public class EmailContent_ScheduledQuoteRequestSuccess {
	@Autowired
	private MessageSource messages; 
	@Autowired
	private TemplateEngine templateEngine;

	@Value("${cwms.deployed.url}")
	private String deployedURL;
	
	public static final String THYMELEAF_VIEW = "/email/core/scheduledquoterequestsuccess.html";

	public EmailContentDto getContent(ScheduledQuotationRequest request, Locale locale) {
		String body = getBodyThymeleaf(request, locale);
		String subject = getSubject(request, locale);
		return new EmailContentDto(body, subject);
	}

	private String getBodyThymeleaf(ScheduledQuotationRequest request, Locale locale) {
        Context context = new Context(locale);
		context.setVariable("sqr", request);
		context.setVariable("baseUrl", this.deployedURL);
		
		String body = this.templateEngine.process(THYMELEAF_VIEW, context);
		return body;
	}

	private String getSubject(ScheduledQuotationRequest request, Locale locale) {
		return this.messages.getMessage("email.schedulequoterequestsuccess.subjectsuccess", new Object[] {request.getQuotation().getQno()}, 
				"Quotation you requested was created ("	+ request.getQuotation().getQno() + ")", locale);
	}

}
