package org.trescal.cwms.core.system.entity.accreditedlab.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Set;

@Service
public class AccreditedLabServiceImpl extends BaseServiceImpl<AccreditedLab, Integer> implements AccreditedLabService {
    @Autowired
    private AccreditedLabDao accreditedLabDao;

    @Override
    protected BaseDao<AccreditedLab, Integer> getBaseDao() {
        return accreditedLabDao;
    }

    @Override
    public Set<KeyValue<Integer, String>> getDtosBySubdivId(Integer subdivId) {
        return accreditedLabDao.getAllBySubdivId(subdivId);
    }

    @Override
    public Set<KeyValue<Integer, String>> getDtosBySubdiv(Subdiv allocatedSubdiv) {
        return getDtosBySubdivId(allocatedSubdiv.getSubdivid());
    }
}
