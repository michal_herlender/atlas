package org.trescal.cwms.core.system.entity.deletedcomponent.db;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.deletedcomponent.DeletedComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

import java.util.Date;
import java.util.List;

@Repository("DeletedComponentDao")
public class DeletedComponentDaoImpl extends BaseDaoImpl<DeletedComponent, Integer> implements DeletedComponentDao {

	@Override
	protected Class<DeletedComponent> getEntity() {
		return DeletedComponent.class;
	}

	@SuppressWarnings("unchecked")
	public List<Component> getDeletableComponents() {
		Criteria crit = getSession().createCriteria(DeletedComponent.class);
		crit.setProjection(Projections.distinct(Projections.projectionList().add(Projections.property("component"))));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<DeletedComponent> searchDeletedComponents(Component component, String refNo, Date dateFrom) {
		Criteria crit = getSession().createCriteria(DeletedComponent.class);
		if (component != null) crit.add(Restrictions.eq("component", component));
		if (StringUtils.isNotEmpty(refNo)) crit.add(Restrictions.ilike("refNo", refNo, MatchMode.START));
		if (dateFrom != null) crit.add(Restrictions.gt("date", dateFrom));
		crit.addOrder(Order.desc("id"));
		return crit.list();
	}
}