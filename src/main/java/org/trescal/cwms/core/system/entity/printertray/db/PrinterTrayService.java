package org.trescal.cwms.core.system.entity.printertray.db;

import java.util.List;

import javax.print.attribute.standard.MediaTray;

import org.trescal.cwms.core.system.entity.papertype.PaperType;
import org.trescal.cwms.core.system.entity.printertray.PrinterTray;

public interface PrinterTrayService
{
	void deletePrinterTray(PrinterTray printertray);

	PrinterTray findPrinterTray(int id);

	PrinterTray findPrinterTrayForPaperType(int printerId, PaperType paperType);

	List<PrinterTray> getAllPrinterTrays();

	MediaTray getMediaTrayForPaperType(int printerId, PaperType paperType);

	Integer getPaperSourceForPaperType(int printerId, PaperType paperType);

	void insertPrinterTray(PrinterTray printertray);

	void saveOrUpdatePrinterTray(PrinterTray printertray);

	void updatePrinterTray(PrinterTray printertray);
}