package org.trescal.cwms.core.system.entity.systemdefaulttypes.service;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultTypeDTO;

public interface SystemDefaultTypeService {
	
	/**
	 * Returns a list of DTOs containing hierarchical system default values for the specified Company
	 * Lookup order : Company -> Global Default
	 */
	List<SystemDefaultTypeDTO> getSystemDefaultTypeDTOs(Company company, Integer allocatedCompanyId, Locale locale);
	
	/**
	 * Returns a list of DTOs containing hierarchical system default values for the specified Subdiv
	 * Lookup order : Subdiv -> Company -> Global Default 
	 */
	List<SystemDefaultTypeDTO> getSystemDefaultTypeDTOs(Subdiv subdiv, Integer allocatedCompanyId, Locale locale);
	
	/**
	 * Returns a list of DTOs containing hierarchical system default values for the specified Contact
	 * Lookup order : Contact -> Subdiv -> Company -> Global Default
	 */
	List<SystemDefaultTypeDTO> getSystemDefaultTypeDTOs(Contact contact, Integer allocatedCompanyId, Locale locale);
}
