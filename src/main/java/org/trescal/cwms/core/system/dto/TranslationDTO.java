package org.trescal.cwms.core.system.dto;

/**
 * Generic DTO object for query results involving translations
 * Meant for use when translating many entities at once via projection to prevent an n+1 query problem
 * Created 2016-11-15
 * @author Galen
 *
 */
public class TranslationDTO {
	private Integer id;
	private String fallback;
	private String translation;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFallback() {
		return fallback;
	}
	public void setFallback(String fallback) {
		this.fallback = fallback;
	}
	public String getTranslation() {
		return translation;
	}
	public void setTranslation(String translation) {
		this.translation = translation;
	}
	/**
	 * Returns translation if available; otherwise fallback. 
	 */
	public String getBestTranslation() {
		String result = fallback;
		if ((translation != null) && (translation.length() > 0)) {
			result = translation;
		}
		return result;
	}
}
