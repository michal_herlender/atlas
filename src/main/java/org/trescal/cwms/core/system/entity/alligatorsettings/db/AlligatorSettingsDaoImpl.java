package org.trescal.cwms.core.system.entity.alligatorsettings.db;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettings;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettingsContact;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettingsContact_;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettingsSubdiv;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettingsSubdiv_;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettings_;

@Repository
public class AlligatorSettingsDaoImpl extends BaseDaoImpl<AlligatorSettings, Integer> implements AlligatorSettingsDao {

	@Override
	protected Class<AlligatorSettings> getEntity() {
		return AlligatorSettings.class;
	}
	
	@Override
	public AlligatorSettings getSettingsForContact(Contact businessContact, boolean production) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<AlligatorSettings> cq = cb.createQuery(AlligatorSettings.class);
		Root<AlligatorSettings> root = cq.from(AlligatorSettings.class);
		Join<AlligatorSettings,AlligatorSettingsContact> subdiv = null;
		if (production) {
			subdiv = root.join(AlligatorSettings_.usageContactProduction, JoinType.INNER);
		}
		else {
			subdiv = root.join(AlligatorSettings_.usageContactTest, JoinType.INNER);
		}
		cq.where(cb.equal(subdiv.get(AlligatorSettingsContact_.contact), businessContact));
		
		List<AlligatorSettings> result = getEntityManager().createQuery(cq).getResultList();
		return result.isEmpty() ? null : result.get(0);
	}

	@Override
	public AlligatorSettings getSettingsForSubdiv(Subdiv businessSubdiv, boolean production) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<AlligatorSettings> cq = cb.createQuery(AlligatorSettings.class);
		Root<AlligatorSettings> root = cq.from(AlligatorSettings.class);
		Join<AlligatorSettings,AlligatorSettingsSubdiv> subdiv = null;
		if (production) {
			subdiv = root.join(AlligatorSettings_.usageSubdivProduction, JoinType.INNER);
		}
		else {
			subdiv = root.join(AlligatorSettings_.usageSubdivTest, JoinType.INNER);
		}
		cq.where(cb.equal(subdiv.get(AlligatorSettingsSubdiv_.businessSubdiv), businessSubdiv));
		
		List<AlligatorSettings> result = getEntityManager().createQuery(cq).getResultList();
		return result.isEmpty() ? null : result.get(0);
	}
	
	@Override
	public AlligatorSettings getDefaultSettings() {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<AlligatorSettings> cq = cb.createQuery(AlligatorSettings.class);
		Root<AlligatorSettings> root = cq.from(AlligatorSettings.class);
		cq.where(cb.equal(root.get(AlligatorSettings_.globalDefault), true));
		
		List<AlligatorSettings> result = getEntityManager().createQuery(cq).getResultList();
		return result.isEmpty() ? null : result.get(0);
	}
	
}
