package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Looks up a SystemDefaultType (individual bean for resolution of system default value)
 * for a specific SystemDefaultNames enum value
 * @author Galen
 * 2017-06-05
 */
@Component
public class SystemDefaultTypeResolver implements InitializingBean {
	@Autowired
	private List<SystemDefaultType<?>> systemDefaultTypes;
	
	private Map<SystemDefaultNames,SystemDefaultType<?>> map;
	
	public static final Logger logger = LoggerFactory.getLogger(SystemDefaultTypeResolver.class);

	@Override
	public void afterPropertiesSet() throws Exception {
		map = new HashMap<>();
		for (SystemDefaultType<?> systemDefaultType : systemDefaultTypes) {
			map.put(systemDefaultType.defaultName(), systemDefaultType);
		}
		logger.info("Initialized map with size "+map.size()+" entries");
	}
	
	public SystemDefaultType<?> getSystemDefaultType(SystemDefaultNames name) {
		map.size();
		SystemDefaultType<?> type = map.get(name);
		if (type == null) {
			logger.error("No SystemDefaultType found for name "+name);
		}
		return type;
	}
}
