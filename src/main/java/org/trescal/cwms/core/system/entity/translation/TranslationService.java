package org.trescal.cwms.core.system.entity.translation;

import java.util.Locale;
import java.util.Set;

public interface TranslationService {
	String getCorrectTranslation(Set<Translation> translations, Locale locale);
}
