package org.trescal.cwms.core.system.projection.note;

import org.trescal.cwms.core.system.entity.note.NoteType;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class NoteProjectionDTO {
	private Integer noteid;
	private NoteType noteType;
	private Integer entityId;
	private Boolean active;
	private String label;
	private String note;
	private Boolean publish;
	
	public NoteProjectionDTO(Integer noteid, String noteTypeEnumName, Integer entityId, Boolean active, String label,
			String note, Boolean publish) {
		super();
		this.noteid = noteid;
		this.noteType = NoteType.valueOf(noteTypeEnumName);
		this.entityId = entityId;
		this.active = active;
		this.label = label;
		this.note = note;
		this.publish = publish;
	}
}
