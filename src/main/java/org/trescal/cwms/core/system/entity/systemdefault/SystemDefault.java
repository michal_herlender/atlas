/**
 * 
 */
package org.trescal.cwms.core.system.entity.systemdefault;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;
import org.trescal.cwms.core.system.entity.systemdefaultgroup.SystemDefaultGroup;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.enums.Scope;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * Entity representing a system default type. A system default is a property
 * whose value can be overridden and applied to {@link Company}, {@link Subdiv}
 * or {@link Contact}s. Each {@link SystemDefault} has a default value that will
 * be used if not overridden by any of the entities named above.
 *
 * @author richard
 */
@Entity
@Table(name = "systemdefault")
public class SystemDefault implements Comparable<SystemDefault>
{
	private SystemDefaultNames id;
	private Set<SystemDefaultApplication> applications;
	private String defaultDataType;
	private String defaultDescription;
	private String defaultName;
	private String defaultValue;
	/** Defines the start of a range of 'allowed' values for integer defaults. */
	private Integer start;
	/** Defines the end of a range of 'allowed' values for integer defaults. */
	private Integer end;
	private SystemDefaultGroup group;
	private Set<CompanyRole> companyRoles;
	private Set<Scope> scopes;
	private Set<Translation> defaultDescriptionTranslation;
	private Set<Translation> defaultNameTranslation;
	private Boolean groupwide;
	
	@Override
	public int compareTo(SystemDefault other) {
		return this.getDefaultName().compareTo(other.getDefaultName());
	}
	
	@Id
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "defaultid", nullable = false, unique = true)
	public SystemDefaultNames getId()
	{
		return this.id;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "systemDefault")
	public Set<SystemDefaultApplication> getApplications()
	{
		return this.applications;
	}

	@NotNull
	@Length(min = 1, max = 20)
	@Column(name = "defaultdatatype", length = 20, nullable = false)
	public String getDefaultDataType()
	{
		return this.defaultDataType;
	}

	@Length(max = 200)
	@Column(name = "defaultdescription", length = 200)
	public String getDefaultDescription()
	{
		return this.defaultDescription;
	}

	@NotNull
	@Length(min = 1, max = 50)
	@Column(name = "defaultname", length = 50, nullable = false)
	public String getDefaultName()
	{
		return this.defaultName;
	}

	@NotNull
    @Length(min = 1, max = 100)
    @Column(name = "defaultvalue", length = 100, nullable = false)
    public String getDefaultValue()
	{
		return this.defaultValue;
	}

	@NotNull
	@Type(type = "int")
	@Column(name = "endd", nullable=false)
	public Integer getEnd()
	{
		return this.end;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "groupid", unique = false, nullable = false)
	public SystemDefaultGroup getGroup()
	{
		return this.group;
	}
	
	@ElementCollection(targetClass=CompanyRole.class)
	@Enumerated(EnumType.ORDINAL)
	@CollectionTable(name="systemdefaultcorole", joinColumns=@JoinColumn(name="defaultid", referencedColumnName="defaultid"))
	@Column(name="coroleid")
	public Set<CompanyRole> getCompanyRoles()
	{
		return this.companyRoles;
	}
	
	@ElementCollection(targetClass=Scope.class)
	@Enumerated(EnumType.ORDINAL)
	@CollectionTable(name="systemdefaultscope", joinColumns=@JoinColumn(name="defaultid", referencedColumnName="defaultid"))
	@Column(name="scope")
	public Set<Scope> getScopes()
	{
		return this.scopes;
	}
	
	@NotNull
	@Type(type = "int")
	@Column(name = "start", nullable=false)
	public Integer getStart()
	{
		return this.start;
	}
	
	@NotNull
	@Column(name = "groupwide", nullable=false)
	public Boolean getGroupwide() {
		return groupwide;
	}
	
	/**
	 * @param applications the applications to set
	 */
	public void setApplications(Set<SystemDefaultApplication> applications)
	{
		this.applications = applications;
	}

	public void setDefaultDataType(String defaultDataType)
	{
		this.defaultDataType = defaultDataType;
	}

	public void setDefaultDescription(String defaultDescription)
	{
		this.defaultDescription = defaultDescription;
	}

	public void setDefaultName(String defaultName)
	{
		this.defaultName = defaultName;
	}

	public void setDefaultValue(String defaultValue)
	{
		this.defaultValue = defaultValue;
	}

	public void setEnd(Integer end)
	{
		this.end = end;
	}

	/**
	 * @param group the group to set
	 */
	public void setGroup(SystemDefaultGroup group)
	{
		this.group = group;
	}

	public void setId(SystemDefaultNames id)
	{
		this.id = id;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setCompanyRoles(Set<CompanyRole> companyRoles)
	{
		this.companyRoles = companyRoles;
	}

	public void setScopes(Set<Scope> scopes)
	{
		this.scopes = scopes;
	}

	public void setStart(Integer start)
	{
		this.start = start;
	}
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="systemdefaultdescriptiontranslation", joinColumns=@JoinColumn(name="defaultid"))
	public Set<Translation> getDefaultDescriptionTranslation() {
		return defaultDescriptionTranslation;
	}

	public void setDefaultDescriptionTranslation(
			Set<Translation> defaultDescriptionTranslation) {
		this.defaultDescriptionTranslation = defaultDescriptionTranslation;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="systemdefaultnametranslation", joinColumns=@JoinColumn(name="defaultid"))
	public Set<Translation> getDefaultNameTranslation() {
		return defaultNameTranslation;
	}

	public void setDefaultNameTranslation(Set<Translation> defaultNameTranslation) {
		this.defaultNameTranslation = defaultNameTranslation;
	}
	
	public void setGroupwide(Boolean groupwide) {
		this.groupwide = groupwide;
	}
}