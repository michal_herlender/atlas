package org.trescal.cwms.core.system.entity.businessdetails.db;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.admin.entity.BusinessEmails;
import org.trescal.cwms.core.admin.entity.db.BusinessEmailsService;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.addressprintformat.AddressPrintFormatter;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;

@Service("BusinessDetailsService")
public class BusinessDetailsServiceImpl implements BusinessDetailsService
{
	@Autowired
	private AddressPrintFormatter addressPrintFormatter; 
	@Autowired
	private BusinessEmailsService businessEmailsService;
	@Value("#{props['cwms.config.business.docCompany']}")
	private String docCompany;
	@Value("#{props['cwms.config.business.homepage']}")
	private String homepage;
	@Value("#{props['cwms.config.business.logo']}")
	private String logo;
	
	// TODO Use AddressFormatter to provide local format by country
	private String[] formatAddress(Address address) {
		String[] result = new String[] {};	// Empty address	
		if (address != null) {
			Locale locale = LocaleContextHolder.getLocale();
			boolean printCompanyName = false;
			List<String> list = this.addressPrintFormatter.getAddressText(address, locale, printCompanyName);
			result = list.toArray(new String[0]); 
		}
		return result;
	}
	
	private Address getDefaultOrFirstAddress(Subdiv subdiv) {
		Address result = null;
		if (subdiv.getDefaultAddress() != null) {
			result = subdiv.getDefaultAddress();
		}
		else {
			result = subdiv.getAddresses().iterator().next();
		}
		return result;
	}
	
	public BusinessDetails getAllBusinessDetails(Subdiv subdiv)
	{
		BusinessDetails bdetails = new BusinessDetails();
		
		initFromBusinessCompany(bdetails, subdiv.getComp());
		Address addr = getDefaultOrFirstAddress(subdiv);
		bdetails.setAddress(formatAddress(addr));
		bdetails.setCompany(subdiv.getComp().getConame());
		bdetails.setDocCompany(this.docCompany);
		bdetails.setFax(addr.getFax());
		bdetails.setHomepage(this.homepage);
		bdetails.setLogo(this.logo);
		bdetails.setTel(addr.getTelephone());
		BusinessEmails emails =  null;
		if(subdiv != null) emails = businessEmailsService.getBySubdiv(subdiv);
		if(emails == null) emails = new BusinessEmails();
		bdetails.setAccountsEmail(emails.getAccount() == null ? "" : emails.getAccount());
		bdetails.setAutoEmail(emails.getAutoService() == null ? "" : emails.getAutoService());
		bdetails.setCollectEmail(emails.getCollections() == null ? "" : emails.getCollections());
		bdetails.setGoodsinEmail(emails.getGoodsIn() == null ? "" : emails.getGoodsIn());
		bdetails.setQualityEmail(emails.getQuality() == null ? "" : emails.getQuality());
		bdetails.setQuoteEmail(emails.getQuotations() == null ? "" : emails.getQuotations());
		bdetails.setRecallEmail(emails.getRecall() == null ? "" : emails.getRecall());
		bdetails.setSalesEmail(emails.getSales() == null ? "" : emails.getSales());
		bdetails.setWebEmail(emails.getWeb() == null ? "" : emails.getWeb());
		return bdetails;
	}
	
	private void initFromBusinessCompany(BusinessDetails bdetails, Company company) {
		bdetails.setDocCompany(company.getConame());
		bdetails.setLegalIdentifier(company.getLegalIdentifier());
		bdetails.setFiscalIdentifier(company.getFiscalIdentifier());
		if (company.getBusinessSettings() != null) {
			bdetails.setLegalRegistration1(company.getBusinessSettings().getLegalRegistration1());
			bdetails.setLegalRegistration2(company.getBusinessSettings().getLegalRegistration2());
		}
	}
	
	public BusinessDetails getAllBusinessDetails(Contact contact, Address headerAddress, Company company)
	{
		BusinessDetails bdetails = this.getAllBusinessDetails(contact.getSub());

		if (contact != null) {
			bdetails.setAccountsEmail(contact.getEmail());
			bdetails.setQuoteEmail(contact.getEmail());
			bdetails.setSalesEmail(contact.getEmail());
			bdetails.setWebEmail(contact.getEmail());
			bdetails.setFax(contact.getFax());
			bdetails.setTel(contact.getTelephone());
			
			bdetails.setContactName(contact.getFirstName().concat(" ").concat(contact.getLastName()));
			bdetails.setContactPosition(contact.getPosition() == null ? "" : contact.getPosition());
		}
		if (headerAddress != null) {
			bdetails.setAddress(formatAddress(headerAddress));
		}
		if (company != null) {
			initFromBusinessCompany(bdetails, company);
		}
		return bdetails;
	}
}