package org.trescal.cwms.core.system.entity.calibrationaccreditationlevel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

/**
 * Entity defines an {@link AccreditationLevel} that is permitted for a
 * {@link CalibrationType}.
 * 
 * @author Richard
 */
@Entity
@Table(name = "calibrationaccreditationlevel", uniqueConstraints = { @UniqueConstraint(columnNames = { "accredlevel", "caltypeid" }) })
public class CalibrationAccreditationLevel
{
	private CalibrationType caltype;
	private int id;
	private AccreditationLevel level;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "caltypeid", nullable = true)
	public CalibrationType getCaltype()
	{
		return this.caltype;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "accredlevel", nullable = false)
	public AccreditationLevel getLevel()
	{
		return this.level;
	}

	public void setCaltype(CalibrationType caltype)
	{
		this.caltype = caltype;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setLevel(AccreditationLevel level)
	{
		this.level = level;
	}
}