package org.trescal.cwms.core.system.entity.upcomingwork;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "upcomingwork")
public class UpcomingWork extends Allocated<Subdiv> {
	private boolean active;
	private Company comp;
	private Contact contact;
	private Department department;
	private String description;
	private LocalDate dueDate;
	private int id;
	private LocalDate startDate;
	private String title;
	private Contact userResponsible;
	private Contact addedBy;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "addedbyid")
	public Contact getAddedBy() {
		return addedBy;
	}

	public void setAddedBy(Contact addedBy) {
		this.addedBy = addedBy;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coid")
	public Company getComp() {
		return this.comp;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid")
	public Contact getContact()
	{
		return this.contact;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deptid")
	public Department getDepartment()
	{
		return this.department;
	}

	@NotNull
	@Column(name = "description", length = 500, nullable = false)
	@Length(min = 1, max = 500)
	public String getDescription() {
		return this.description;
	}

	@Column(name = "duedate", columnDefinition = "date")
	@NotNull
	public LocalDate getDueDate() {
		return this.dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "respuserid")
	public Contact getUserResponsible() {
		return this.userResponsible;
	}

	@Column(name = "active", nullable = false, columnDefinition = "tinyint")
	public boolean isActive()
	{
		return this.active;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public void setComp(Company comp) {
		this.comp = comp;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	@NotNull
	@Column(name = "title", length = 100, nullable = false)
	@Length(min = 1, max = 100)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "startdate", columnDefinition = "date")
	public LocalDate getStartDate() {
		return this.startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public void setUserResponsible(Contact userResponsible) {
		this.userResponsible = userResponsible;
	}
}
