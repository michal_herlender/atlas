package org.trescal.cwms.core.system.entity.printer.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.printer.Printer;

public interface PrinterDao extends BaseDao<Printer, Integer>
{
	Printer findPrinterByPath(String printerPath);

	List<Printer> getPrintersAtAddress(int addrId);

	List<Printer> getBySubdiv(Subdiv subdiv);
	
	List<Printer> getBySubdivs(Collection<Subdiv> subdivs);
}