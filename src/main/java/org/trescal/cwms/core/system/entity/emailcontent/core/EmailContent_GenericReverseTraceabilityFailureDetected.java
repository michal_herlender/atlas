package org.trescal.cwms.core.system.entity.emailcontent.core;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.jobs.calibration.dto.StandardsUsageAnalysisDTO;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;

@Component
public class EmailContent_GenericReverseTraceabilityFailureDetected {

	public static final String THYMELEAF_VIEW = "/email/reversetraceability/genericreversetraceabilityfailuredetected.html";

	@Autowired
	private MessageSource messages;
	@Autowired
	private TemplateEngine templateEngine;
	@Value("${cwms.deployed.url}")
	private URL deployedURL;

	public EmailContentDto getContent(Locale locale, String date, List<StandardsUsageAnalysisDTO> dtos) throws MalformedURLException, URISyntaxException {
		String body = getBodyThymeleaf(locale, date, dtos);
		String subject = getSubject(locale, date);
		return new EmailContentDto(body, subject);
	}

	private String getSubject(Locale locale, String date) {
		return this.messages.getMessage("email.genericreversetraceability.title", new Object[] { date }, locale);
	}

	private String getBodyThymeleaf(Locale locale, String date, List<StandardsUsageAnalysisDTO> dtos) throws URISyntaxException, MalformedURLException {
		Context context = new Context(locale);
		context.setVariable("date", date);
		context.setVariable("dtos", dtos);
		return this.templateEngine.process(THYMELEAF_VIEW, context);
	}
}
