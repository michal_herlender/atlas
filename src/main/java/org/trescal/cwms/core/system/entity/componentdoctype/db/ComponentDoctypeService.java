package org.trescal.cwms.core.system.entity.componentdoctype.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.componentdoctype.ComponentDoctype;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

public interface ComponentDoctypeService
{
	ComponentDoctype findComponentDoctype(Component component, String doctypeName);

	ComponentDoctype findComponentDoctype(int id);

	List<ComponentDoctype> getAllComponentDoctypes();

	void insertComponentDoctype(ComponentDoctype componentdoctype);

	void updateComponentDoctype(ComponentDoctype componentdoctype);
}