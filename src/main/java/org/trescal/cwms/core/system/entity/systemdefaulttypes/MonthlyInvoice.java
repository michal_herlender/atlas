package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class MonthlyInvoice extends SystemDefaultBooleanType
{
	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.MONTHLY_INVOICE;
	}
}