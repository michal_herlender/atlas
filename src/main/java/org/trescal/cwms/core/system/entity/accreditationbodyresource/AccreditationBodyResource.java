package org.trescal.cwms.core.system.entity.accreditationbodyresource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.system.entity.accreditationbody.AccreditationBody;

@Entity
@Table(name = "accreditationbodyresource", uniqueConstraints={@UniqueConstraint(columnNames= {"accreditationbodyid", "type"})})
public class AccreditationBodyResource extends Versioned {
	private int id;
	private AccreditationBody accreditationBody;
	private AccreditationBodyResourceType type;
	private String resourcePath;
	
	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "accreditationbodyid", nullable=false)
	public AccreditationBody getAccreditationBody() {
		return accreditationBody;
	}
	
	@NotNull
	@Enumerated(EnumType.ORDINAL)
	@Column(name="type", nullable=false)
	public AccreditationBodyResourceType getType() {
		return type;
	}
	
	@NotNull
	@Column(name = "resourcePath", nullable = false, columnDefinition = "nvarchar(2000)")
	public String getResourcePath() {
		return resourcePath;
	}
	
	public void setAccreditationBody(AccreditationBody accreditationBody) {
		this.accreditationBody = accreditationBody;
	}
	public void setType(AccreditationBodyResourceType type) {
		this.type = type;
	}
	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}
	public void setId(int id) {
		this.id = id;
	}
}
