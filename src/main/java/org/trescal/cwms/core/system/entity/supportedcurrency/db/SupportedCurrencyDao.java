package org.trescal.cwms.core.system.entity.supportedcurrency.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface SupportedCurrencyDao extends BaseDao<SupportedCurrency, Integer>
{
	SupportedCurrency findCurrencyByCode(String currencyCode);

	SupportedCurrency findSupportedCurrency(int id);

	SupportedCurrency findSupportedCurrencyOrDefault(String currencyCode, String defaultCurrencyCode);

	List<SupportedCurrency> getAllSupportedCurrenciesWithExRates();

	List<SupportedCurrency> getAllSupportedCurrencys();
	
	List<KeyValueIntegerString> getKeyValueList(Collection<Integer> currencyIds);

	void insertSupportedCurrency(SupportedCurrency supportedcurrency);

	void updateSupportedCurrency(SupportedCurrency supportedcurrency);
	
	KeyValueIntegerString getKeyValue(Integer currencyId);
}