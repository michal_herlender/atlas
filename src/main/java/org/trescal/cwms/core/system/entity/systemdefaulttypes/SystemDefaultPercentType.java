package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.util.ArrayList;
import java.util.List;

public abstract class SystemDefaultPercentType extends SystemDefaultType<Double> {

	@Override
	public boolean isDiscreteType() {
		return false;
	}
	
	@Override
	public boolean isMultiChoice() {
		return false;
	}
	
	@Override
	public List<String> getValues() {
		return new ArrayList<>();
	}
	
	@Override
	public Double parseValue(String value) {
		// The user may enter either a percentage e.g. "10 %" as formatted 
		// or a number e.g. "10", either should be acceptable
		String numericValue = value.replace('%', ' ').trim();
		return Double.parseDouble(numericValue);
	}
	
	@Override
	public String getValue(String savedValue) {
		return savedValue + " %";
	}
}