package org.trescal.cwms.core.system.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.system.entity.email.Email;

@Builder
@Data @NoArgsConstructor @AllArgsConstructor
public class EmailResultWrapper
{
	private Email email;
	private String body;
	private String[][] attachments;
	@Builder.Default
	private Boolean send = false;

	public EmailResultWrapper(Email email, String body, String[][] attachments)
	{
		this.email = email;
		this.body = body;
		this.attachments = attachments;
	}


}