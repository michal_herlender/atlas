package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class BusinessUnitTimeZoneDefaultType extends SystemDefaultTimeZoneType {
    @Override
    public SystemDefaultNames defaultName() {
        return SystemDefaultNames.BUSINESS_UNIT_TIME_ZONE;
    }
}
