package org.trescal.cwms.core.system.entity.calibrationtype;

import lombok.Setter;
import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.jobs.job.entity.jobtypecaltype.JobTypeCalType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotecaltypedefault.QuoteCaltypeDefault;
import org.trescal.cwms.core.recall.enums.RecallRequirementType;
import org.trescal.cwms.core.system.entity.calibrationaccreditationlevel.CalibrationAccreditationLevel;
import org.trescal.cwms.core.system.entity.calibrationaccreditationlevel.CalibrationAccreditationLevelComparator;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault.TPQuotationCaltypeDefault;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Setter
@Entity
@Table(name = "calibrationtype")
public class CalibrationType {
    private int calTypeId;

    private boolean accreditationSpecific;
    private boolean active;
    private Boolean calibrationWithJudgement;
    private Integer orderBy;
    private RecallRequirementType recallRequirementType;
    private ServiceType serviceType;

    private Set<CalibrationAccreditationLevel> accreditationLevels;
    private Set<CapabilityAuthorization> authorizations;
    private Set<TPQuotationCaltypeDefault> tpQuoteCaltypeDefaults;
    private Set<TPQuotationItem> tpQuoteItems;
    private Set<JobItem> jobItems;
    private Set<QuoteCaltypeDefault> quoteCaltypeDefaults;
    private Set<Quotationitem> quoteItems;
    private Set<JobTypeCalType> jobTypes;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "caltypeid", nullable = false, unique = true)
    @Type(type = "int")
	public int getCalTypeId() {
		return this.calTypeId;
	}

	@NotNull
	@Column(name = "orderby", unique = true, nullable = false)
	@Type(type = "int")
	public Integer getOrderBy() {
		return this.orderBy;
	}

	@Column(name = "accreditationspecific", nullable = false, columnDefinition = "bit")
	public boolean getAccreditationSpecific() {
		return this.accreditationSpecific;
	}

	@Column(name = "active", nullable = false, columnDefinition = "bit")
	public boolean isActive() {
		return this.active;
	}

	@NotNull
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "servicetypeid", nullable=false)
	public ServiceType getServiceType() {
		return this.serviceType;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "recallRequirementType", nullable=false)
	public RecallRequirementType getRecallRequirementType() {
		return recallRequirementType;
	}

	@NotNull
	@Column(name = "calibrationWithJudgement", columnDefinition = "bit", nullable=false)
	public Boolean getCalibrationWithJudgement() {
		return calibrationWithJudgement;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "caltype")
	public Set<TPQuotationCaltypeDefault> getTpQuoteCaltypeDefaults() {
		return tpQuoteCaltypeDefaults;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "caltype")
	public Set<TPQuotationItem> getTpQuoteItems() {
		return tpQuoteItems;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "calType")
	public Set<JobItem> getJobItems() {
		return jobItems;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "caltype")
	public Set<QuoteCaltypeDefault> getQuoteCaltypeDefaults() {
		return quoteCaltypeDefaults;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "caltype")
	public Set<Quotationitem> getQuoteItems() {
		return quoteItems;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "caltype")
    @SortComparator(CalibrationAccreditationLevelComparator.class)
    public Set<CalibrationAccreditationLevel> getAccreditationLevels() {
        return this.accreditationLevels;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "calType")
    public Set<JobTypeCalType> getJobTypes() {
        return jobTypes;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "caltype")
    public Set<CapabilityAuthorization> getAuthorizations() {
        return this.authorizations;
    }

}
