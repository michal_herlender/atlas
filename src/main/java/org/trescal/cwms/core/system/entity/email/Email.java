package org.trescal.cwms.core.system.entity.email;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * @author jamiev
 */
@Entity
@Table(name = "email")
@Builder @AllArgsConstructor @NoArgsConstructor
public class Email extends Auditable implements ComponentEntity
{
	private String bodyXindiceKey;
	private Component component;
	private File directory;
	private Integer entityId;
	private String from;
	private int id;
	private boolean publish;
	private List<EmailRecipient> recipients;
	private Contact sentBy;
	private Date sentOn;
	private String subject;
	private String body;

	@Length(max = 100)
	@Column(name = "bodyxindicekey", length = 100)
	@Deprecated
	public String getBodyXindiceKey()
	{
		return this.bodyXindiceKey;
	}

	@Column(name = "body", columnDefinition="nvarchar(max)")
	public String getBody()
	{
		return this.body;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "component")
	public Component getComponent()
	{
		return this.component;
	}

	@Override
	@Transient
	public Contact getDefaultContact()
	{
		return this.sentBy;
	}

	@Override
	@Transient
	public File getDirectory()
	{
		return this.directory;
	}

	@Column(name = "entityid")
	public Integer getEntityId()
	{
		return this.entityId;
	}

	@NotNull
	@Length(max = 100)
	@Column(name = "fromaddress", nullable = false, length = 100)
	public String getFrom()
	{
		return this.from;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Override
	@Transient
	public String getIdentifier()
	{
		return String.valueOf(this.id);
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity()
	{
		return null;
	}

	@OneToMany(mappedBy = "email", fetch = FetchType.LAZY)
	public List<EmailRecipient> getRecipients()
	{
		return this.recipients;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sentby")
	public Contact getSentBy()
	{
		return this.sentBy;
	}

	@Override
	@Transient
	public List<Email> getSentEmails()
	{
		return null;
	}

	@Column(name = "senton", columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSentOn()
	{
		return this.sentOn;
	}

	@Length(max = 200)
	@Column(name = "subject", length = 200, columnDefinition="nvarchar(200)")
	public String getSubject()
	{
		return this.subject;
	}

	@Override
	@Transient
	public boolean isAccountsEmail()
	{
		return false;
	}

	@Column(name = "publish", nullable = false, columnDefinition="bit")
	public boolean isPublish()
	{
		return this.publish;
	}

	public void setBodyXindiceKey(String bodyXindiceKey)
	{
		this.bodyXindiceKey = bodyXindiceKey;
	}

	public void setBody(String body)
	{
		this.body = body;
	}

	public void setComponent(Component component)
	{
		this.component = component;
	}

	@Override
	public void setDirectory(File file)
	{
		this.directory = file;
	}

	public void setEntityId(Integer entityId)
	{
		this.entityId = entityId;
	}

	public void setFrom(String from)
	{
		this.from = from;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setPublish(boolean publish)
	{
		this.publish = publish;
	}

	public void setRecipients(List<EmailRecipient> recipients)
	{
		this.recipients = recipients;
	}

	public void setSentBy(Contact sentBy)
	{
		this.sentBy = sentBy;
	}

	@Override
	public void setSentEmails(List<Email> emails)
	{

	}

	public void setSentOn(Date sentOn)
	{
		this.sentOn = sentOn;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}
}