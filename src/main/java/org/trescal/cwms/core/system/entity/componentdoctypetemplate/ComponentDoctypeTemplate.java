package org.trescal.cwms.core.system.entity.componentdoctypetemplate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.system.entity.componentdoctype.ComponentDoctype;
import org.trescal.cwms.core.system.entity.template.Template;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name = "componentdoctypetemplate")
public class ComponentDoctypeTemplate
{
	private int id;
	private ComponentDoctype componentDoctype;
	private Template template;
	private boolean defaultTemplate;

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "compdoctypeid")
	public ComponentDoctype getComponentDoctype()
	{
		return this.componentDoctype;
	}

	@Id
	// @GeneratedValue(strategy = GenerationType.IDENTITY) -- identity cannot be
	// used on entities that use table-per-class inheritance type
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "templateId", nullable = false)
	public Template getTemplate()
	{
		return this.template;
	}

	@NotNull
	@Column(name = "defaulttemplate", nullable = false, columnDefinition="bit")
	public boolean isDefaultTemplate()
	{
		return this.defaultTemplate;
	}

	public void setComponentDoctype(ComponentDoctype componentDoctype)
	{
		this.componentDoctype = componentDoctype;
	}

	public void setDefaultTemplate(boolean defaultTemplate)
	{
		this.defaultTemplate = defaultTemplate;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setTemplate(Template template)
	{
		this.template = template;
	}
}
