package org.trescal.cwms.core.system.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentType;
import org.trescal.cwms.core.system.entity.emailcontent.EmailPreviewer;
import org.trescal.cwms.core.system.form.EmailPreviewForm;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
public class EmailPreviewController {
	
	@Autowired
	private EmailPreviewer emailPreviewer;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	
	private static final String VIEW_NAME = "trescal/core/admin/emailpreview";
	
	@ModelAttribute("form")
	public EmailPreviewForm formBackingObject(Locale locale) {
		EmailPreviewForm form = new EmailPreviewForm();
		form.setLocale(locale);
		form.setThymeleaf(true);
		return form;
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/emailpreview.htm")
	public String referenceData(Model model) {
		model.addAttribute("supportedLocales", supportedLocaleService.getSupportedLocales());
		model.addAttribute("contentTypes", getContentTypes());
		return VIEW_NAME;
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/emailpreview.htm")
	public String previewEmail(Model model, 
			@Valid @ModelAttribute("form") EmailPreviewForm form, BindingResult bindingResult) {
		if (!bindingResult.hasErrors()) {
			EmailContentDto content = this.emailPreviewer.getPreview(form.getContentType(), form.getEntityId(), form.getLocale(), form.getThymeleaf());
			model.addAttribute("body", content.getBody());
			model.addAttribute("subject", content.getSubject());
		}
		return referenceData(model);
	}
	
	public List<KeyValue<EmailContentType, String>> getContentTypes() {
		List<KeyValue<EmailContentType, String>> result = new ArrayList<>();
		for (EmailContentType type : EmailContentType.values()) {
			result.add(new KeyValue<EmailContentType, String>(type, type.getDescription()));
		}
		return result;
	}
}
