package org.trescal.cwms.core.system.entity.defaultnote.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.system.entity.defaultnote.DefaultNote;
import org.trescal.cwms.core.system.entity.defaultnote.DefaultNote_;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent_;

@Repository("DefaultNoteDao")
public class DefaultNoteDaoImpl extends AllocatedToCompanyDaoImpl<DefaultNote, Integer> implements DefaultNoteDao
{
	@Override
	protected Class<DefaultNote> getEntity() {
		return DefaultNote.class;
	}
	
	public List<DefaultNote> getDefaultNotesByComponent(Component component, Integer allocatedCompanyId) 
	{
		if (allocatedCompanyId == null) throw new IllegalArgumentException("allocatedCompanyId is mandatory");
		return getResultList(cb -> {
			CriteriaQuery<DefaultNote> cq = cb.createQuery(DefaultNote.class);
			Root<DefaultNote> root = cq.from(DefaultNote.class);
			Join<DefaultNote, SystemComponent> systemComponent = root.join(DefaultNote_.component);
			Join<DefaultNote, Company> company = root.join(DefaultNote_.organisation.getName());
			
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(company.get(Company_.coid), allocatedCompanyId));
			if (component != null) {
				clauses.getExpressions().add(cb.equal(systemComponent.get(SystemComponent_.component), component));
			}
			
			cq.where(clauses);
			
			cq.orderBy(cb.asc(systemComponent.get(SystemComponent_.component)), 
					cb.asc(root.get(DefaultNote_.label)), cb.asc(root.get(DefaultNote_.defaultNoteId)));
			return cq;
		});
	}
}