package org.trescal.cwms.core.system.dto;

import org.trescal.cwms.core.workallocation.engineerallocation.entity.EngineerAllocation;

public class EngineerAllocationWrapper
{
	private int endBlock;
	private EngineerAllocation engineerAllocation;
	private int startBlock;

	public EngineerAllocationWrapper(EngineerAllocation engineerAllocation, int startBlock, int endBlock)
	{
		this.engineerAllocation = engineerAllocation;
		this.startBlock = startBlock;
		this.endBlock = endBlock;
	}

	public int getEndBlock()
	{
		return this.endBlock;
	}

	public EngineerAllocation getEngineerAllocation()
	{
		return this.engineerAllocation;
	}

	public int getStartBlock()
	{
		return this.startBlock;
	}

	public void setEndBlock(int endBlock)
	{
		this.endBlock = endBlock;
	}

	public void setEngineerAllocation(EngineerAllocation engineerAllocation)
	{
		this.engineerAllocation = engineerAllocation;
	}

	public void setStartBlock(int startBlock)
	{
		this.startBlock = startBlock;
	}
}
