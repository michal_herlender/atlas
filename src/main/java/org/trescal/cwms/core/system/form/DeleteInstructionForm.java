package org.trescal.cwms.core.system.form;

import org.trescal.cwms.core.system.entity.instructionlink.InstructionEntity;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DeleteInstructionForm {
	private Integer linkid;
	private InstructionEntity entity;
	private String refererUrl;
}
