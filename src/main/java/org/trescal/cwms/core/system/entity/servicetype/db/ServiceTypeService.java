package org.trescal.cwms.core.system.entity.servicetype.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceTypeDto;
import org.trescal.cwms.spring.model.KeyValue;

public interface ServiceTypeService extends BaseService<ServiceType, Integer> {

	List<ServiceType> getAllByDomainType(DomainType domainType);
	
	List<ServiceType> getAllCalServiceTypes();
	
	ServiceType getByLongName(String longName, Locale locale);
	
	ServiceType getByShortNameAndLocal(String shortName, Locale locale);
	
	/**
	 * Returns the specified service types with information for specified locale, and fallback locale
	 * Can be used as is or formatted further
	 * @param serviceTypeIds
	 * @param locale
	 * @return
	 */
	List<ServiceTypeDto> getDTOs(Collection<Integer> serviceTypeIds, Locale locale);
	
	/**
	 * 
	 * @param domainType - OPTIONAL, restrict to service types of specified domain
	 * @param jobType - OPTIONAL, restrict to service types used for job type
	 * @param locale - mandatory
	 * @param prependNoneDto - mandatory, whether to prepend KeyValue(0,"None") 
	 * @return
	 */
	List<KeyValue<Integer, String>> getDTOList(DomainType domainType, JobType jobType, Locale locale, boolean prependNoneDto);

	List<KeyValue<Integer, String>> getDTOListWithShortNames(DomainType domainType, JobType jobType, Locale locale,
			boolean prependNoneDto);
	
	List<ServiceType> getByJobType(JobType jobType);
	
	Set<Integer> getIdsByJobType(JobType jobType);	
}