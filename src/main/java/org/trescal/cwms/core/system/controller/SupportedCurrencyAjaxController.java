package org.trescal.cwms.core.system.controller;

import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.SupportedCurrencyAjaxDTO;
import org.trescal.cwms.core.system.dto.SupportedCurrencyOption;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("supportedCurrency")
public class SupportedCurrencyAjaxController {

    private final SupportedCurrencyService supportedCurrencyService;

    private final UserService usersrv;

    public SupportedCurrencyAjaxController(SupportedCurrencyService supportedCurrencyService, UserService usersrv) {
        this.supportedCurrencyService = supportedCurrencyService;
        this.usersrv = usersrv;
    }

    @RequestMapping(value = {"supportedcurrency.json"}, method = RequestMethod.POST, produces = {
            Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8})
    public SupportedCurrencyAjaxDTO saveNewCurrencyValues(
            @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, BigDecimal exchangeRate,
            Integer currencyId) {
        Contact un = usersrv.get(username).getCon();

        return supportedCurrencyService.saveNewValues(exchangeRate, currencyId, un);
    }

    @GetMapping("companyOptions.json")
    public List<SupportedCurrencyOption> getCompanyCurrencyOptions(Integer coid) {
        return supportedCurrencyService.getCompanyCurrencyOptions(coid);
    }
}

