package org.trescal.cwms.core.system.entity.upcomingwork.db;

import lombok.val;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToSubdivDaoImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.List;

@Repository("UpcomingWorkDao")
public class UpcomingWorkDaoImpl extends AllocatedToSubdivDaoImpl<UpcomingWork, Integer> implements UpcomingWorkDao
{
	@Override
	protected Class<UpcomingWork> getEntity() {
		return UpcomingWork.class;
	}

	@Override
	public Long countUpcomingWorkBetweenDates(LocalDate startDate, LocalDate endDate, Integer allocatedSubdivId) {
		return getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<UpcomingWork> root = cq.from(UpcomingWork.class);
			Join<UpcomingWork, Subdiv> subdiv = root.join(UpcomingWork_.organisation.getName());

			// Matches long running work that may begin before the startDate AND end after the endDate
			Predicate innerClause = cb.conjunction();
			innerClause.getExpressions().add(cb.lessThan(root.get(UpcomingWork_.startDate), startDate));
			innerClause.getExpressions().add(cb.greaterThan(root.get(UpcomingWork_.dueDate), endDate));

			Predicate dateClause = cb.disjunction();
			dateClause.getExpressions().add(cb.between(root.get(UpcomingWork_.startDate), startDate, endDate));
			dateClause.getExpressions().add(cb.between(root.get(UpcomingWork_.dueDate), startDate, endDate));
			dateClause.getExpressions().add(innerClause);

			Predicate mainClause = cb.conjunction();
			mainClause.getExpressions().add(cb.isTrue(root.get(UpcomingWork_.active)));
			mainClause.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), allocatedSubdivId));
			mainClause.getExpressions().add(dateClause);

			cq.where(mainClause);

			return cq.select(cb.count(root));
		});
	}

	@Override
	public List<UpcomingWork> getUpcomingWorkBetweenDates(LocalDate startDate, LocalDate endDate, Subdiv allocatedSubdiv) {
		return getResultList(cb -> {
			val cq = cb.createQuery(UpcomingWork.class);
			val upcomingWork = cq.from(UpcomingWork.class);
			cq.where(
				cb.isTrue(upcomingWork.get(UpcomingWork_.active)),
				cb.equal(upcomingWork.get(UpcomingWork_.organisation), allocatedSubdiv),
				cb.or(
						cb.between(upcomingWork.get(UpcomingWork_.startDate), startDate, endDate),
						cb.between(upcomingWork.get(UpcomingWork_.dueDate), startDate, endDate)
					)
			);
			return cq;
		});
	}

	@Override
	public List<UpcomingWork> getUpcomingWorkForUser(int personid, Subdiv allocatedSubdiv) {
		return getResultList(cb -> {
			val cq = cb.createQuery(UpcomingWork.class);
			val upcomingWork = cq.from(UpcomingWork.class);
			cq.where(
				cb.isTrue(upcomingWork.get(UpcomingWork_.active)),
				cb.greaterThanOrEqualTo(upcomingWork.get(UpcomingWork_.dueDate), LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())),
				cb.equal(upcomingWork.get(UpcomingWork_.organisation), allocatedSubdiv),
				cb.equal(upcomingWork.get(UpcomingWork_.userResponsible), personid)
			);
			return cq;
		});
	}
}