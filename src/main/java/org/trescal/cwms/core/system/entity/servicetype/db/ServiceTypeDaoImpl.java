package org.trescal.cwms.core.system.entity.servicetype.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.entity.jobtypeservicetype.JobTypeServiceType;
import org.trescal.cwms.core.jobs.job.entity.jobtypeservicetype.JobTypeServiceType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceTypeDto;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;

@Repository
public class ServiceTypeDaoImpl extends BaseDaoImpl<ServiceType, Integer> implements ServiceTypeDao {

	@Override
	protected Class<ServiceType> getEntity() {
		return ServiceType.class;
	}

	@Override
	public List<ServiceType> getAllByDomainType(DomainType domainType) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ServiceType> cq = cb.createQuery(ServiceType.class);
		Root<ServiceType> serviceType = cq.from(ServiceType.class);
		serviceType.fetch(ServiceType_.longnameTranslation, JoinType.LEFT);
		cq.where(cb.equal(serviceType.get(ServiceType_.domainType), domainType));
		cq.orderBy(cb.asc(serviceType.get(ServiceType_.order)));
		cq.distinct(true);
		TypedQuery<ServiceType> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}

	@Override
	public List<ServiceType> getAllCalServiceTypes() {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ServiceType> cq = cb.createQuery(ServiceType.class);
		Root<ServiceType> serviceType = cq.from(ServiceType.class);
		cq.where(cb.isNotNull(serviceType.get(ServiceType_.calibrationType)));
		cq.orderBy(cb.asc(serviceType.get(ServiceType_.order)));
		TypedQuery<ServiceType> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}

	@Override
	public ServiceType getByLongName(String longName, Locale locale) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ServiceType> cq = cb.createQuery(ServiceType.class);
		Root<ServiceType> serviceType = cq.from(ServiceType.class);
		Join<ServiceType, Translation> longname = serviceType.join(ServiceType_.longnameTranslation);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(longname.get(Translation_.locale), locale));
		clauses.getExpressions().add(cb.like(longname.get(Translation_.translation), longName));
		cq.where(clauses);
		cq.orderBy(cb.asc(serviceType.get(ServiceType_.order)));
		TypedQuery<ServiceType> query = getEntityManager().createQuery(cq);
		return query.getSingleResult();
	}
	
	@Override
	public List<ServiceTypeDto> getDTOs(Collection<Integer> serviceTypeIds, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<ServiceTypeDto> cq = cb.createQuery(ServiceTypeDto.class);
			Root<ServiceType> root = cq.from(ServiceType.class);
			Join<ServiceType, Translation> shortname = root.join(ServiceType_.shortnameTranslation, JoinType.LEFT);
			shortname.on(cb.equal(shortname.get(Translation_.locale), locale));
			Join<ServiceType, Translation> longname = root.join(ServiceType_.longnameTranslation, JoinType.LEFT);
			longname.on(cb.equal(longname.get(Translation_.locale), locale));
			
			cq.where(root.get(ServiceType_.serviceTypeId).in(serviceTypeIds));
			cq.select(cb.construct(ServiceTypeDto.class, root.get(ServiceType_.serviceTypeId),
					root.get(ServiceType_.shortName), root.get(ServiceType_.longName),
					shortname.get(Translation_.translation), longname.get(Translation_.translation),
					root.get(ServiceType_.displayColour), root.get(ServiceType_.displayColourFastTrack)));
			cq.orderBy(cb.asc(root.get(ServiceType_.order)));
			return cq;
		});
	}

	/**
	 * Provides back list of service types with translations and fallbacks in a
	 * single query
	 * 
	 * @param domainType - OPTIONAL
	 * @param jobType    - OPTIONAL
	 * @param locale     - MANDATORY
	 */
	@Override
	public List<ServiceTypeDto> getAllDto(DomainType domainType, JobType jobType, Locale locale) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ServiceTypeDto> cq = cb.createQuery(ServiceTypeDto.class);
		Root<ServiceType> serviceType = cq.from(ServiceType.class);
		Join<ServiceType, Translation> shortname = serviceType.join(ServiceType_.shortnameTranslation, JoinType.LEFT);
		Join<ServiceType, Translation> longname = serviceType.join(ServiceType_.longnameTranslation, JoinType.LEFT);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(shortname.get(Translation_.locale), locale));
		clauses.getExpressions().add(cb.equal(longname.get(Translation_.locale), locale));
		if (domainType != null) {
			clauses.getExpressions().add(cb.equal(serviceType.get(ServiceType_.domainType), domainType));
		}
		if (jobType != null) {
			Join<ServiceType, JobTypeServiceType> jobTypes = serviceType.join(ServiceType_.jobTypes, JoinType.INNER);
			clauses.getExpressions().add(cb.equal(jobTypes.get(JobTypeServiceType_.jobType), jobType));
		}
		cq.where(clauses);
		cq.select(cb.construct(ServiceTypeDto.class, serviceType.get(ServiceType_.serviceTypeId),
				serviceType.get(ServiceType_.shortName), serviceType.get(ServiceType_.longName),
				shortname.get(Translation_.translation), longname.get(Translation_.translation),
				serviceType.get(ServiceType_.displayColour), serviceType.get(ServiceType_.displayColourFastTrack)));
		cq.orderBy(cb.asc(serviceType.get(ServiceType_.order)));

		TypedQuery<ServiceTypeDto> query = getEntityManager().createQuery(cq);

		return query.getResultList();
	}

	@Override
	public ServiceType getByShortNameAndLocal(String shortName, Locale locale) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<ServiceType> cq = cb.createQuery(ServiceType.class);
		Root<ServiceType> serviceType = cq.from(ServiceType.class);
		Join<ServiceType, Translation> shortname = serviceType.join(ServiceType_.shortnameTranslation);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(shortname.get(Translation_.locale), locale));
		clauses.getExpressions().add(cb.like(shortname.get(Translation_.translation), shortName));
		cq.where(clauses);
		cq.orderBy(cb.asc(serviceType.get(ServiceType_.order)));
		TypedQuery<ServiceType> query = getEntityManager().createQuery(cq);
		ServiceType st = null;
		try {
			st = query.getSingleResult();
		} catch (NoResultException nre) {
			// Ignore this because as per your logic this is ok!
		}
		return st;
	}

	@Override
	public List<ServiceType> getByJobType(JobType jobType) {
		return getResultList(cb -> {
			CriteriaQuery<ServiceType> cq = cb.createQuery(getEntity());
			Root<ServiceType> root = cq.from(getEntity());
			Join<ServiceType, JobTypeServiceType> jobTypeServiceTypes = root.join(ServiceType_.jobTypes);
			cq.where(cb.equal(jobTypeServiceTypes.get(JobTypeServiceType_.jobType), jobType));
			return cq;
		});
	}

}