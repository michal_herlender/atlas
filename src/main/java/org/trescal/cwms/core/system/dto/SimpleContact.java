package org.trescal.cwms.core.system.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public
class SimpleContact {
    private String field;
    private String email;
    private String name;
    private Integer personid;
}
