package org.trescal.cwms.core.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.PresetCommentDTO;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.presetcomment.db.PresetCommentService;
import org.trescal.cwms.core.system.entity.presetcommentcategory.db.PresetCommentCategoryService;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Controller
@JsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class PresetCommentController {

	@Autowired
	private PresetCommentService presetCommentService;
	@Autowired
	private PresetCommentCategoryService categoryService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;

	@RequestMapping("getpresetcomments.json")
	@ResponseBody
	public List<PresetCommentDTO> getPresentComments(
		@RequestParam(name = "commentType") PresetCommentType commentType,
		@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto) {
		Subdiv subdiv = subdivService.get(subdivDto.getKey());
		return presetCommentService.getAll(commentType, subdiv).stream().map(PresetCommentDTO::new)
			.collect(Collectors.toList());
	}

	@RequestMapping(value = "addpresetcomment.json")
	public ModelAndView addPresentComment(@Validated @RequestBody PresetCommentDTO comment, BindingResult bindingResult,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		if (bindingResult.hasErrors()) {
			FieldError error = bindingResult.getFieldError();
			String errorMessage = error.getField() + ": " + error.getDefaultMessage();
			throw new RuntimeException(errorMessage);

		} else {
			PresetComment presetComment = new PresetComment();
			presetComment.setComment(comment.getComment());
			PresetCommentType commentType = comment.getCommentType();
			if (commentType == null)
				commentType = PresetCommentType.WITHOUT;
			presetComment.setType(commentType);
			if (comment.getCategoryId() != null && comment.getCategoryId() != 0)
				presetComment.setCategory(categoryService.get(comment.getCategoryId()));
			presetComment.setLabel(comment.getLabel());
			presetComment.setSetby(userService.get(username).getCon());
			presetComment.setSeton(new Date());
			presetComment.setOrganisation(subdivService.get(subdivDto.getKey()));
			presetCommentService.save(presetComment);
			return new ModelAndView("trescal/core/system/presetcommentadd", "comment", presetComment);
		}
	}

	@RequestMapping(value = "editpresetcomment.json", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> editPresentComment(@Validated @RequestBody PresetCommentDTO comment,
			BindingResult bindingResult, Locale locale) {
		if (bindingResult.hasErrors()) {
			FieldError error = bindingResult.getFieldError();
			String errorMessage = error.getField() + ": " + error.getDefaultMessage();
			return ResponseEntity.badRequest().body(errorMessage);
		} else {
			PresetComment presetComment = presetCommentService.get(comment.getId());
			presetComment.setComment(comment.getComment());
			if (comment.getCategoryId() != null && comment.getCategoryId() != 0)
				presetComment.setCategory(categoryService.get(comment.getCategoryId()));
			else
				presetComment.setCategory(null);
			presetComment.setLabel(comment.getLabel());
			return ResponseEntity.ok().body("");
		}
	}

	@RequestMapping(value = "deletepresetcomment.json", method = RequestMethod.POST)
	@ResponseBody
	public boolean deletePresentComment(@RequestParam(name = "commentId") Integer commentId) {
		PresetComment presetComment = presetCommentService.get(commentId);
		presetCommentService.delete(presetComment);
		return true;
	}

	@RequestMapping(value = "presetcommentaddoverlay.htm", method = RequestMethod.GET)
	public String presetCommentAddOverlay(@RequestParam(name = "commentType", required = false) PresetCommentType type,
			@RequestParam(name = "noteType", required = false) NoteType noteType, Model model) {
		model.addAttribute("isNewComment", true);
		PresetCommentDTO presetComment = new PresetCommentDTO();
		if (type != null)
			presetComment.setCommentType(type);
		else if (noteType != null)
			presetComment.setCommentType(noteType.getPresetCommentType());
		model.addAttribute("comment", presetComment);
		model.addAttribute("categories", categoryService.getAll());
		return "trescal/core/system/presetcommentoverlay";
	}

	@RequestMapping(value = "presetcommenteditoverlay.htm", method = RequestMethod.GET)
	public String presetCommentAddOverlay(@RequestParam(name = "commentId") Integer commentId,
										  Model model) {
		model.addAttribute("isNewComment", false);
		model.addAttribute("comment", new PresetCommentDTO(presetCommentService.get(commentId)));
		model.addAttribute("categories", categoryService.getAll());
		return "trescal/core/system/presetcommentoverlay";
	}
}