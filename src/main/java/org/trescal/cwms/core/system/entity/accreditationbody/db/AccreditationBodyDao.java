package org.trescal.cwms.core.system.entity.accreditationbody.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.accreditationbody.AccreditationBody;

public interface AccreditationBodyDao extends BaseDao<AccreditationBody, Integer> {

}
