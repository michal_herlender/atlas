package org.trescal.cwms.core.system.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink;
import org.trescal.cwms.core.company.entity.companyinstructionlink.db.CompanyInstructionLinkService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.db.SubdivInstructionLinkService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.JobInstructionLink;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.db.JobInstructionLinkService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionEntity;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLink;
import org.trescal.cwms.core.system.entity.instructionlink.db.InstructionLinkService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.form.InstructionForm;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY,Constants.SESSION_ATTRIBUTE_SUBDIV})
@Slf4j
public class InstructionEditController {
	
	@Autowired
	private InstructionLinkService instructionLinkService;
	@Autowired
	CompanyInstructionLinkService companyInstructionLinkService;
	@Autowired
	SubdivInstructionLinkService subdivInstructionLinkService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private JobInstructionLinkService jobInstructionLinkService;

	public static final String FORM_NAME = "form";
	public static final String TAB_URL_FRAGMENT = "&loadtab=instructions-tab";

	@ModelAttribute(FORM_NAME)
	protected InstructionForm formBackingObject(HttpServletRequest request,
			@RequestParam(value = "linkid", required = true) Integer linkid,
			@RequestParam(value = "entity", required = true) InstructionEntity entity) throws Exception {
		InstructionForm iform = new InstructionForm();
		log.debug("Entity: " + entity);
		InstructionLink<?> link = this.instructionLinkService.get(linkid, entity);
		log.debug("Link Instruction Entity: " + link.getInstructionEntity());
		iform.setInstructionLink(link);
		iform.setIncludeOnDelNotes((link.getInstruction().getIncludeOnDelNote() != null) && link.getInstruction().getIncludeOnDelNote());
		iform.setIncludeOnSupplierDelNotes((link.getInstruction().getIncludeOnSupplierDelNote() != null) && link.getInstruction().getIncludeOnSupplierDelNote());
		iform.setInstructionType(link.getInstruction().getInstructiontype());
		iform.setRefererUrl(request.getHeader("referer"));
		if (link.getInstructionEntity().equals(InstructionEntity.COMPANY)) {
			iform.setSubdivInstruction(((CompanyInstructionLink) link).isSubdivInstruction());
		} else if (link.getInstructionEntity().equals(InstructionEntity.SUBDIV)) {
			iform.setSubdivInstruction(((SubdivInstructionLink) link).isSubdivInstruction());
		}
		return iform;
	}

	@RequestMapping(value = "/editinstruction.htm", method = RequestMethod.POST)
	protected RedirectView onSubmit(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> allocatedSubdivDto,
			@Valid @ModelAttribute(FORM_NAME) InstructionForm iform) throws Exception {
		InstructionLink<?> link = iform.getInstructionLink();
		Instruction instruction = link.getInstruction();
		InstructionType type = iform.getInstructionType();
		instruction.setInstructiontype(type);

		if (type.equals(InstructionType.CARRIAGE)){
			instruction.setIncludeOnDelNote(iform.isIncludeOnDelNotes());
			instruction.setIncludeOnSupplierDelNote(iform.isIncludeOnSupplierDelNotes());
			JobInstructionLink jobInstructionLink = (JobInstructionLink) link;
			jobInstructionLink.setInstruction(instruction);
			this.jobInstructionLinkService.merge(jobInstructionLink);
		}
		Subdiv subdiv = subdivService.get(allocatedSubdivDto.getKey());
		if (link.getInstructionEntity().equals(InstructionEntity.COMPANY)) {
			CompanyInstructionLink companyLink = (CompanyInstructionLink) link;
			//check if isSubdivInstruction checkbox is changed, and if yes change to current subdiv
			if(companyLink.isSubdivInstruction()!=iform.isSubdivInstruction()){
				companyLink.setSubdivInstruction(iform.isSubdivInstruction());
				companyLink.setBusinessSubdiv(subdiv);
			}
			companyInstructionLinkService.merge(companyLink);
		} else if (link.getInstructionEntity().equals(InstructionEntity.SUBDIV)) {
			SubdivInstructionLink subdivLink = (SubdivInstructionLink) link;
			//check if isSubdivInstruction checkbox is changed, and if yes change to current subdiv
			if(subdivLink.isSubdivInstruction()!=iform.isSubdivInstruction()){
				subdivLink.setSubdivInstruction(iform.isSubdivInstruction());
				subdivLink.setBusinessSubdiv(subdiv);
			}
			subdivInstructionLinkService.merge(subdivLink);
		}
		String viewName = iform.getRefererUrl();
		if (viewName.indexOf(TAB_URL_FRAGMENT) == -1) {
			viewName = viewName + TAB_URL_FRAGMENT;
		}
		return new RedirectView(viewName);
	}

	@RequestMapping(value = "/editinstruction.htm", method = RequestMethod.GET)
	protected String referenceData(Model model,
			@ModelAttribute(FORM_NAME) InstructionForm form) {
		List<KeyValue<InstructionType, String>> instructionTypes = InstructionType.getActiveTypes().stream()
				.map(it -> new KeyValue<>(it, it.getType()))
				.collect(Collectors.toList());
		model.addAttribute("instructionTypes", instructionTypes);
		switch (form.getInstructionLink().getInstructionEntity()) {
		case COMPANY:
			Company company = ((CompanyInstructionLink) form.getInstructionLink()).getEntity();
			model.addAttribute("instructionCompany", company);
			break;
		case SUBDIV:
			Subdiv subdiv = ((SubdivInstructionLink) form.getInstructionLink()).getEntity();
			model.addAttribute("instructionCompany", subdiv.getComp());
			model.addAttribute("instructionSubdiv", subdiv);
			break;
		case CONTACT:
			Contact contact = ((ContactInstructionLink) form.getInstructionLink()).getEntity();
			model.addAttribute("instructionCompany", contact.getSub().getComp());
			model.addAttribute("instructionSubdiv", contact.getSub());
			model.addAttribute("instructionContact", contact);
			break;
		case JOB:
			model.addAttribute("instructionJob", ((JobInstructionLink) form.getInstructionLink()).getEntity());
			break;
		default:
			throw new RuntimeException("Unsupported entity type");
		}
		return "trescal/core/system/instruction";
	}
}