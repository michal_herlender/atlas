package org.trescal.cwms.core.system.entity.closeddate.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.closeddate.ClosedDate;

public class ClosedDateServiceImpl implements ClosedDateService
{
	ClosedDateDao closedDateDao;

	public ClosedDate findClosedDate(int id)
	{
		return closedDateDao.find(id);
	}
	
	public List<ClosedDate> getFutureClosedDatesForCompany(int coid)
	{
		return closedDateDao.getFutureClosedDatesForCompany(coid);
	}

	public void insertClosedDate(ClosedDate ClosedDate)
	{
		closedDateDao.persist(ClosedDate);
	}

	public void updateClosedDate(ClosedDate ClosedDate)
	{
		closedDateDao.update(ClosedDate);
	}

	public List<ClosedDate> getAllClosedDates()
	{
		return closedDateDao.findAll();
	}

	public void setClosedDateDao(ClosedDateDao closedDateDao)
	{
		this.closedDateDao = closedDateDao;
	}

	public void deleteClosedDate(ClosedDate closeddate)
	{
		this.closedDateDao.remove(closeddate);
	}

	public void saveOrUpdateClosedDate(ClosedDate closeddate)
	{
		this.closedDateDao.saveOrUpdate(closeddate);
	}
}