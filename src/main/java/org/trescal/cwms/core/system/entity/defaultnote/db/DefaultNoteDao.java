package org.trescal.cwms.core.system.entity.defaultnote.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.system.entity.defaultnote.DefaultNote;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

public interface DefaultNoteDao extends AllocatedDao<Company, DefaultNote, Integer>
{
	List<DefaultNote> getDefaultNotesByComponent(Component component, Integer allocatedCompanyId);
}