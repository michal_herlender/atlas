package org.trescal.cwms.core.system.entity.email;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import lombok.Data;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipientDto;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Data
public class EmailDto {
	public EmailDto(Component component, Integer entityId, String from, Integer id, Boolean publish, Integer sentById,
			String sentByFirstName, String sentByLastName, Date sentOn, String subject) {
		super();
		this.component = component;
		this.entityId = entityId;
		this.from = from;
		this.id = id;
		this.publish = publish;
		this.sentById = sentById;
		this.sentByFirstName = sentByFirstName;
		this.sentByLastName = sentByLastName;
		this.sentOn = sentOn;
		this.subject = subject;
	}
	private Component component;
	private Integer entityId;
	private String from;
	private Integer id; 
	private Boolean publish;
	private Integer sentById;
	private String sentByFirstName;
	private String sentByLastName;
	private Date sentOn;
	private String subject;
	private List<EmailRecipientDto> recipients;
	
	public void addRecipient(EmailRecipientDto recipientDto) {
		if (recipients == null) recipients = new ArrayList<>();
		recipients.add(recipientDto);
	}
	
}
