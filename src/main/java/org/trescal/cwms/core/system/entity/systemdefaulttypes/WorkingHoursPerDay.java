package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class WorkingHoursPerDay extends SystemDefaultDoubleType {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.WORKING_HOURS_PER_DAY;
	}
}