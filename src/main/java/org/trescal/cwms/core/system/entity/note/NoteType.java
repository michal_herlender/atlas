package org.trescal.cwms.core.system.entity.note;

import org.trescal.cwms.core.asset.entity.asset.Asset;
import org.trescal.cwms.core.asset.entity.assetnote.AssetNote;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.DeliveryItemNote;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.DeliveryNote;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hirenote.HireNote;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrumentnote.InstrumentNote;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.modelnote.ModelNote;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certnote.CertNote;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobnote.JobNote;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnote.JobItemNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotenote.CreditNoteNote;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoicenote.InvoiceNote;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitemremark.JobCostingItemNote;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingnote.JobCostingNote;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemnote.PurchaseOrderItemNote;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseordernote.PurchaseOrderNote;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quoteitemnote.QuoteItemNote;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedulenote.ScheduleNote;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote.TPQuoteItemNote;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotenote.TPQuoteNote;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitemnote.TPQuoteRequestItemNote;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote.TPQuoteRequestNote;

/**
 * {@link Enum} that defines all of the distinct note types on the system.
 * 
 * @author Stuart
 */
public enum NoteType
{
	ASSETNOTE(Asset.class, AssetNote.class, PresetCommentType.ASSET, true),
	CERTNOTE(Certificate.class, CertNote.class, PresetCommentType.CERTIFICATE, true),
	CREDITNOTENOTE(CreditNote.class, CreditNoteNote.class, PresetCommentType.WITHOUT, false),
	DELIVERYITEMNOTE(DeliveryItem.class, DeliveryItemNote.class, PresetCommentType.WITHOUT, false),
	DELIVERYNOTE(Delivery.class, DeliveryNote.class, PresetCommentType.DELIVERY_NOTE, false),
	HIRENOTE(Hire.class, HireNote.class, PresetCommentType.WITHOUT, false),
	INSTRUMENTMODELNOTE(InstrumentModel.class, ModelNote.class, PresetCommentType.INSTRUMENT_MODEL, true),
	INSTRUMENTNOTE(Instrument.class, InstrumentNote.class, PresetCommentType.INSTRUMENT, false),
	INVOICENOTE(Invoice.class, InvoiceNote.class, PresetCommentType.INVOICE, false),
	JOBCOSTINGITEMNOTE(JobCostingItem.class, JobCostingItemNote.class, PresetCommentType.JOB_COSTING_ITEM, false),
	JOBCOSTINGNOTE(JobCosting.class, JobCostingNote.class, PresetCommentType.JOB_COSTING, false),
	JOBITEMNOTE(JobItem.class, JobItemNote.class, PresetCommentType.JOB_ITEM, true),
	JOBNOTE(Job.class, JobNote.class, PresetCommentType.JOB, true),
	PURCHASEORDERITEMNOTE(PurchaseOrderItem.class, PurchaseOrderItemNote.class, PresetCommentType.PURCHASE_ORDER_ITEM, false),
	PURCHASEORDERNOTE(PurchaseOrder.class, PurchaseOrderNote.class, PresetCommentType.WITHOUT, false),
	QUOTEITEMNOTE(Quotationitem.class, QuoteItemNote.class, PresetCommentType.QUOTATION_ITEM, false),
	QUOTENOTE(Quotation.class, QuoteNote.class, PresetCommentType.QUOTATION, false),
	SCHEDULENOTE(Schedule.class, ScheduleNote.class, PresetCommentType.WITHOUT, false),
	TPQUOTEITEMNOTE(TPQuotationItem.class, TPQuoteItemNote.class, PresetCommentType.THIRD_PARTY_INSTRUCTION, false),
	TPQUOTENOTE(TPQuotation.class, TPQuoteNote.class, PresetCommentType.THIRD_PARTY_INSTRUCTION, false),
	TPQUOTEREQUESTITEMNOTE(TPQuoteRequestItem.class, TPQuoteRequestItemNote.class, PresetCommentType.THIRD_PARTY_INSTRUCTION, false),
	TPQUOTEREQUESTNOTE(TPQuoteRequest.class, TPQuoteRequestNote.class, PresetCommentType.THIRD_PARTY_INSTRUCTION, false);
	
	private Class<? extends NoteAwareEntity> entityclazz;
	private Class<? extends Note> noteclazz;
	private PresetCommentType presetCommentType;
	private boolean privateOnly;
	
	private NoteType(Class<? extends NoteAwareEntity> entityclazz,
			Class<? extends Note> noteclazz,
			PresetCommentType presetCommentType,
			boolean privateOnly) {
		this.entityclazz = entityclazz;
		this.noteclazz = noteclazz;
		this.presetCommentType = presetCommentType;
		this.privateOnly = privateOnly;
	}
	
	public Class<? extends NoteAwareEntity> getEntityclazz() {
		return this.entityclazz;
	}

	public Class<? extends Note> getNoteclazz() {
		return this.noteclazz;
	}
	
	public PresetCommentType getPresetCommentType() {
		return presetCommentType;
	}
	
	public boolean isPrivateOnly() {
		return privateOnly;
	}
}