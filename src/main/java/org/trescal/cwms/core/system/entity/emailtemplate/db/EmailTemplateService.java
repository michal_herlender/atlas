package org.trescal.cwms.core.system.entity.emailtemplate.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.emailtemplate.EmailTemplate;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

public interface EmailTemplateService extends BaseService<EmailTemplate, Integer> {

	List<EmailTemplate> getAllByCompany(Company company);
	
	/**
	 * Finds exact matches for parameters (can also be used for duplicate detection)
	 * Company company, Locale locale, Component component - mandatory
	 * Subdiv subdiv, EmailTemplate exclude - can be null
	 * If passed Subdiv is null, query will only match null subdivs 
	 */
	EmailTemplate get(Company company, Locale locale, Component component, Subdiv subdiv, EmailTemplate exclude);
	
	/**
	 * Finds best email template for given parameters (or returns null)
	 * Subdiv businessSubdiv, Locale locale, Component component - mandatory
	 * Will return subdiv specific template if exists, otherwise company template 
	 */
	EmailTemplate findBestTemplate(Subdiv businessSubdiv, Locale locale, Component component);
}
