package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class AccreditedLabelRecallDate extends SystemDefaultType<String> {

	public static final String YES = "Yes";
	public static final String NO = "No";
	public static final String YES_WITHOUT_DATE = "Yes Without Date";
	
	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.ACCREDITED_LABEL_RECALL_DATE;
	}

	@Override
	public boolean isDiscreteType() {
		return true;
	}

	@Override
	public List<String> getValues() {
		List<String> result = new ArrayList<String>();
		result.add(YES);
		result.add(NO);
		result.add(YES_WITHOUT_DATE);
		return result;
	}

	@Override
	public String parseValue(String value) {
		return value;
	}

	@Override
	public boolean isMultiChoice() {
		return false;
	}	
}