package org.trescal.cwms.core.system.entity.upcomingwork.db;

public class UpcomingWorkException extends Exception{
    public UpcomingWorkException(String message){
        super(message);
    }
}
