package org.trescal.cwms.core.system.entity.alligatorsettings;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Stores settings that differ between Alligator installations (e.g. for different subdivisions)
 * Allows one common installer for all Alligator installations and for user to get settings upon login
 */
@Entity
@Table(name="alligatorsettings")
public class AlligatorSettings {
	private int id;
	// Configuration and usage
	private String description;
	private Boolean globalDefault;
	private Set<AlligatorSettingsContact> usageContactProduction;
	private Set<AlligatorSettingsContact> usageContactTest;
	private Set<AlligatorSettingsSubdiv> usageSubdivProduction;
	private Set<AlligatorSettingsSubdiv> usageSubdivTest;
	// Actual settings below
	private String excelLocalTempFolder;
    private Boolean excelSaveTempCopy;
    private Boolean excelRelativeTempFolder;
    private String uploadTempDataFolder;
    private String uploadRawDataFolder;
    private Boolean uploadEnabled;
    private Boolean uploadMoveUponUpload;
    private Boolean uploadUseWindowsCredentials;
    private String uploadWindowsUsername;
    private String uploadWindowsDomain;
    private String uploadWindowsPassword;
    
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}
	
	@NotNull
	@Column(name="description", length=100, nullable=false)
	public String getDescription() {
		return description;
	}
	
	@NotNull
	@Column(name="globaldefault", nullable=false, columnDefinition="bit")
	public Boolean getGlobalDefault() {
		return globalDefault;
	}
	
	@OneToMany(mappedBy="settingsProduction", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	public Set<AlligatorSettingsSubdiv> getUsageSubdivProduction() {
		return usageSubdivProduction;
	}
	
	@OneToMany(mappedBy="settingsTest", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	public Set<AlligatorSettingsSubdiv> getUsageSubdivTest() {
		return usageSubdivTest;
	}
	
	@OneToMany(mappedBy="settingsProduction", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	public Set<AlligatorSettingsContact> getUsageContactProduction() {
		return usageContactProduction;
	}
	
	@OneToMany(mappedBy="settingsTest", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	public Set<AlligatorSettingsContact> getUsageContactTest() {
		return usageContactTest;
	}
	
	@NotNull
	@Column(name="excellocaltempfolder", length=255, nullable=false)
	public String getExcelLocalTempFolder() {
		return excelLocalTempFolder;
	}
	
	@NotNull
	@Column(name="excelsavetempcopy", nullable=false, columnDefinition="bit")
	public Boolean getExcelSaveTempCopy() {
		return excelSaveTempCopy;
	}
	
	@NotNull
	@Column(name="excelrelativetempfolder", nullable=false, columnDefinition="bit")
	public Boolean getExcelRelativeTempFolder() {
		return excelRelativeTempFolder;
	}
	
	@NotNull
	@Column(name="uploadtempdatafolder", length=255, nullable=false)
	public String getUploadTempDataFolder() {
		return uploadTempDataFolder;
	}
	
	@NotNull
	@Column(name="uploadrawdatafolder", length=255, nullable=false)
	public String getUploadRawDataFolder() {
		return uploadRawDataFolder;
	}
	
	@NotNull
	@Column(name="uploadenabled", nullable=false, columnDefinition="bit")
	public Boolean getUploadEnabled() {
		return uploadEnabled;
	}
	
	@NotNull
	@Column(name="uploadmoveuponupload", nullable=false, columnDefinition="bit")
	public Boolean getUploadMoveUponUpload() {
		return uploadMoveUponUpload;
	}
	
	@NotNull
	@Column(name="uploadusewindowscredentials", nullable=false, columnDefinition="bit")
	public Boolean getUploadUseWindowsCredentials() {
		return uploadUseWindowsCredentials;
	}
	
	@NotNull
	@Column(name="uploadwindowsusername", length=255, nullable=false)
	public String getUploadWindowsUsername() {
		return uploadWindowsUsername;
	}
	
	@NotNull
	@Column(name="uploadwindowsdomain", length=255, nullable=false)
	public String getUploadWindowsDomain() {
		return uploadWindowsDomain;
	}
	
	@NotNull
	@Column(name="uploadwindowspassword", length=255, nullable=false)
	public String getUploadWindowsPassword() {
		return uploadWindowsPassword;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setGlobalDefault(Boolean globalDefault) {
		this.globalDefault = globalDefault;
	}
	public void setExcelLocalTempFolder(String excelLocalTempFolder) {
		this.excelLocalTempFolder = excelLocalTempFolder;
	}
	public void setExcelSaveTempCopy(Boolean excelSaveTempCopy) {
		this.excelSaveTempCopy = excelSaveTempCopy;
	}
	public void setExcelRelativeTempFolder(Boolean excelRelativeTempFolder) {
		this.excelRelativeTempFolder = excelRelativeTempFolder;
	}
	public void setUploadTempDataFolder(String uploadTempDataFolder) {
		this.uploadTempDataFolder = uploadTempDataFolder;
	}
	public void setUploadRawDataFolder(String uploadRawDataFolder) {
		this.uploadRawDataFolder = uploadRawDataFolder;
	}
	public void setUploadEnabled(Boolean uploadEnabled) {
		this.uploadEnabled = uploadEnabled;
	}
	public void setUploadMoveUponUpload(Boolean uploadMoveUponUpload) {
		this.uploadMoveUponUpload = uploadMoveUponUpload;
	}
	public void setUploadUseWindowsCredentials(Boolean uploadUseWindowsCredentials) {
		this.uploadUseWindowsCredentials = uploadUseWindowsCredentials;
	}
	public void setUploadWindowsUsername(String uploadWindowsUsername) {
		this.uploadWindowsUsername = uploadWindowsUsername;
	}
	public void setUploadWindowsDomain(String uploadWindowsDomain) {
		this.uploadWindowsDomain = uploadWindowsDomain;
	}
	public void setUploadWindowsPassword(String uploadWindowsPassword) {
		this.uploadWindowsPassword = uploadWindowsPassword;
	}
	public void setUsageContactProduction(Set<AlligatorSettingsContact> usageContactProduction) {
		this.usageContactProduction = usageContactProduction;
	}
	public void setUsageContactTest(Set<AlligatorSettingsContact> usageContactTest) {
		this.usageContactTest = usageContactTest;
	}
	public void setUsageSubdivProduction(Set<AlligatorSettingsSubdiv> usageSubdivProduction) {
		this.usageSubdivProduction = usageSubdivProduction;
	}
	public void setUsageSubdivTest(Set<AlligatorSettingsSubdiv> usageSubdivTest) {
		this.usageSubdivTest = usageSubdivTest;
	}
}
