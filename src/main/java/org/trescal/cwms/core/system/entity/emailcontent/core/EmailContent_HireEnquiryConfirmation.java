package org.trescal.cwms.core.system.entity.emailcontent.core;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.company.entity.addressprintformat.AddressPrintFormatter;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;

@Component
public class EmailContent_HireEnquiryConfirmation {
	@Autowired
	private AddressPrintFormatter addressFormatter;
	@Autowired
	private BusinessDetailsService bdServ;
	@Autowired
	private MessageSource messages; 
	@Autowired
	private TemplateEngine templateEngine;
	
	public static final String VELOCITY_VIEW = "/velocity/email/hire_enquiry_confirm.vm";
	public static final String THYMELEAF_VIEW = "/email/core/hire_enquiry_confirm.html";

	public EmailContentDto getContent(Hire hire, Locale locale) {
		BusinessDetails bdetails = this.bdServ.getAllBusinessDetails(hire.getOrganisation());
		
		String body = getBodyThymeleaf(hire, bdetails, locale);
		String subject = getSubject(hire, bdetails, locale);
		return new EmailContentDto(body, subject);
	}

	private String getBodyThymeleaf(Hire hire, BusinessDetails bdetails, Locale locale) {
        Context context = new Context(locale);
		context.setVariable("hire", hire);
		context.setVariable("antechCon", hire.getCreatedBy());
		context.setVariable(Constants.SESSION_ATTRIBUTE_BUSINESS_DETAILS, bdetails);
		context.setVariable("addressText", this.addressFormatter.getAddressText(hire.getAddress(), locale, false));
		
		String body = this.templateEngine.process(THYMELEAF_VIEW, context);
		return body;
	}


	private String getSubject(Hire hire, BusinessDetails bdetails, Locale locale) {
		String subject = this.messages.getMessage("email.hire_enquiry_confirm.subject",  
				new Object[] {bdetails.getDocCompany(),hire.getHireno()}, 
				bdetails.getDocCompany() + 
				" Hire Enquiry Confirmation: " + 
				hire.getHireno(), locale);
		return subject;
	}
	
}
