package org.trescal.cwms.core.system.entity.supportedcurrenyratehistory.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.supportedcurrenyratehistory.SupportedCurrencyRateHistory;

public class SupportedCurrencyRateHistoryServiceImpl implements SupportedCurrencyRateHistoryService
{
	SupportedCurrencyRateHistoryDao supportedCurrencyRateHistoryDao;

	public SupportedCurrencyRateHistory findSupportedCurrencyRateHistory(int id)
	{
		return supportedCurrencyRateHistoryDao.find(id);
	}

	public void insertSupportedCurrencyRateHistory(SupportedCurrencyRateHistory SupportedCurrencyRateHistory)
	{
		supportedCurrencyRateHistoryDao.persist(SupportedCurrencyRateHistory);
	}

	public void updateSupportedCurrencyRateHistory(SupportedCurrencyRateHistory SupportedCurrencyRateHistory)
	{
		supportedCurrencyRateHistoryDao.update(SupportedCurrencyRateHistory);
	}

	public List<SupportedCurrencyRateHistory> getAllSupportedCurrencyRateHistorys()
	{
		return supportedCurrencyRateHistoryDao.findAll();
	}

	public void setSupportedCurrencyRateHistoryDao(SupportedCurrencyRateHistoryDao supportedCurrencyRateHistoryDao)
	{
		this.supportedCurrencyRateHistoryDao = supportedCurrencyRateHistoryDao;
	}

	public void deleteSupportedCurrencyRateHistory(SupportedCurrencyRateHistory supportedcurrencyratehistory)
	{
		this.supportedCurrencyRateHistoryDao.remove(supportedcurrencyratehistory);
	}

	public void saveOrUpdateSupportedCurrencyRateHistory(SupportedCurrencyRateHistory supportedcurrencyratehistory)
	{
		this.supportedCurrencyRateHistoryDao.saveOrUpdate(supportedcurrencyratehistory);
	}
}