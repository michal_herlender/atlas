package org.trescal.cwms.core.system.entity.accreditedlabcaltype;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Entity
@Table(name = "accreditedlabcaltype")
public class AccreditedLabCalType
{
	private int id;
	private AccreditedLab accreditedLab;
	private CalibrationType calType;
	private String labelTemplatePath;

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "accreditedlabid")
	public AccreditedLab getAccreditedLab()
	{
		return this.accreditedLab;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "caltypeid")
	public CalibrationType getCalType()
	{
		return this.calType;
	}

	@Length(max = 100)
	@Column(name = "labeltemplatepath", length = 100)
	public String getLabelTemplatePath()
	{
		return this.labelTemplatePath;
	}

	public void setAccreditedLab(AccreditedLab accreditedLab)
	{
		this.accreditedLab = accreditedLab;
	}

	public void setCalType(CalibrationType calType)
	{
		this.calType = calType;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setLabelTemplatePath(String labelTemplatePath)
	{
		this.labelTemplatePath = labelTemplatePath;
	}
}