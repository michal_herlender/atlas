package org.trescal.cwms.core.system.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.note.db.NoteService;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.presetcomment.db.PresetCommentService;
import org.trescal.cwms.core.system.entity.presetcommentcategory.PresetCommentCategory;
import org.trescal.cwms.core.system.entity.presetcommentcategory.db.PresetCommentCategoryService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@JsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME })
public class NoteBoxyController {

	private static final Logger logger = LoggerFactory.getLogger(NoteBoxyController.class);

	@Autowired
	private NoteService noteService;
	@Autowired
	private PresetCommentService presetCommentService;
	@Autowired
	private PresetCommentCategoryService presetCommentCategoryService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private UserService userService;

	@ModelAttribute("presetCommentMap")
	public Map<String, List<PresetComment>> presetCommentMap(
			@RequestParam(name = "noteType", required = true) NoteType noteType,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto) {

		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		List<PresetComment> comments = presetCommentService.getAll(noteType.getPresetCommentType(), allocatedSubdiv);
		Map<String, List<PresetComment>> presetCommentMap = comments.stream()
				.collect(Collectors.groupingBy(pc -> pc.getCategory() == null ? ""
						: translationService.getCorrectTranslation(pc.getCategory().getTranslations(),
								LocaleContextHolder.getLocale())));
		return presetCommentMap;
	}

	@ModelAttribute("supportPresetComments")
	public boolean supportPresetComments(@RequestParam(name = "noteType", required = true) NoteType noteType) {
		return noteType.getPresetCommentType() != PresetCommentType.WITHOUT;
	}

	@ModelAttribute("presetCommentCategories")
	public List<KeyValue<Integer, String>> presetCommentCategories() {
		return presetCommentCategoryService.getAll().stream()
				.map(pcc -> new KeyValue<Integer, String>(pcc.getId(),
						translationService.getCorrectTranslation(pcc.getTranslations(),
								LocaleContextHolder.getLocale())))
				.sorted((kv1, kv2) -> kv1.getValue().compareTo(kv2.getValue())).collect(Collectors.toList());
	}

	@ModelAttribute("note")
	public Note getNote(@RequestParam(name = "noteType", required = true) NoteType noteType,
			@RequestParam(name = "noteId", required = false, defaultValue = "0") Integer noteId,
			@RequestParam(name = "label", required = false) String noteLabel,
			@RequestParam(name = "entityId", required = true) Integer entityId) throws Exception {
		if (noteId == 0) {
			Note note = noteService.getNewNote(noteType, entityId);
			note.setLabel(noteLabel);
			return note;
		} else
			return noteService.findNote(noteId, noteType.getNoteclazz());
	}

	@ModelAttribute("entityId")
	public Integer getEntityId(@RequestParam(name = "entityId", required = true) Integer entityId) {
		return entityId;
	}

	@ModelAttribute("privateOnlyNotes")
	public boolean isPrivateOnlyNotes(@RequestParam(name = "noteType", required = true) NoteType noteType) {
		return noteType.isPrivateOnly();
	}

	@ModelAttribute("inlineNote")
	private boolean isInlineNote(
			@RequestParam(name = "inlineNote", required = false, defaultValue = "false") boolean inlineNote) {
		return inlineNote;
	}

	@ModelAttribute("colspan")
	private Integer getColspan(@RequestParam(name = "colspan", required = false, defaultValue = "0") Integer colspan) {
		return colspan;
	}

	@RequestMapping(value = "initializenoteboxy.htm")
	public ModelAndView initializeNoteBoxy() {
		logger.debug("initializeNoteBoxy called");
		return new ModelAndView("trescal/core/system/noteboxycontent");
	}

	private void createPresetComment(String note, Integer categoryId, PresetCommentType type, Integer subdivId,
			String userName) {
		PresetCommentCategory presetCommentCategory = presetCommentCategoryService.get(categoryId);
		PresetComment presetComment = new PresetComment();
		presetComment.setCategory(presetCommentCategory);
		presetComment.setComment(note);
		presetComment.setType(type);
		Subdiv subdiv = subdivService.get(subdivId);
		presetComment.setOrganisation(subdiv);
		presetComment.setSeton(new Date());
		Contact contact = userService.get(userName).getCon();
		presetComment.setSetby(contact);
		presetCommentService.save(presetComment);
	}

	@RequestMapping(value = "insertnote.htm", method = RequestMethod.POST)
	public ModelAndView insertNote(@RequestParam(name = "noteType", required = true) NoteType noteType,
			@RequestParam(name = "entityId", required = true) Integer entityId,
			@RequestParam(name = "inlineNote", required = false, defaultValue = "false") boolean inlineNote,
			@RequestParam(name = "presetCommentCategoryId", required = false, defaultValue = "-1") Integer presetCommentCategoryId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto,
			@ModelAttribute("note") Note note) {
		logger.debug("insertNote called");
		if (note.getNoteid() == 0) {
			// initialize new note
			Contact currentContact = userService.get(userName).getCon();
			Date now = new Date();
			note.setActive(true);
			note.setSetBy(currentContact);
			note.setSetOn(now);
		}
		if (presetCommentCategoryId >= 0)
			createPresetComment(note.getNote(), presetCommentCategoryId, noteType.getPresetCommentType(),
					subdivDto.getKey(), userName);
		noteService.saveOrUpdate(note);
		return new ModelAndView(inlineNote ? "trescal/core/system/inlinenote" : "trescal/core/system/tabbednote");
	}

	@RequestMapping(value = "updatenote.htm", method = RequestMethod.POST)
	public ModelAndView updateNote(@RequestParam(name = "noteType", required = true) NoteType noteType,
			@RequestParam(name = "inlineNote", required = false, defaultValue = "false") boolean inlineNote,
			@RequestParam(name = "presetCommentCategoryId", required = false, defaultValue = "-1") Integer presetCommentCategoryId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto,
			@ModelAttribute("note") Note note) {
		logger.debug("updateNote called");
		if (presetCommentCategoryId >= 0)
			createPresetComment(note.getNote(), presetCommentCategoryId, noteType.getPresetCommentType(),
					subdivDto.getKey(), userName);
		noteService.saveOrUpdate(note);
		return new ModelAndView(inlineNote ? "trescal/core/system/inlinenote" : "trescal/core/system/tabbednote");
	}
}