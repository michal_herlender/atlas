package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.timekeeping.enums.WorkingDay;

import java.util.ArrayList;
import java.util.List;

@Component
public class WorkingDays extends SystemDefaultType<List<WorkingDay>> {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.WORKING_DAYS;
	}

	@Override
	public boolean isDiscreteType() {
		return true;
	}

	@Override
	public List<String> getValues() {
		List<String> values = new ArrayList<String>();
		for (WorkingDay wd : WorkingDay.values())
			values.add(wd.name());
		return values;
	}

	@Override
	public List<WorkingDay> parseValue(String value) {
		try {
			List<WorkingDay> values = new ArrayList<>();
			String tempValue = value.startsWith("[") ? value.substring(1, value.length()-1):value;
			if(StringUtils.isNotEmpty(tempValue)) {
				String[] valuesStr = tempValue.split(",");
				for (String v : valuesStr) {
					values.add(WorkingDay.valueOf(v.trim()));
				}
				
			}
			return values;
		}
		catch(Exception ex) {
			return new ArrayList<>();
		}
	}

	@Override
	public boolean isMultiChoice() {
		return true;
	}
}