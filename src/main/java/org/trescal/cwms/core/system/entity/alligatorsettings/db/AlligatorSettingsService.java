package org.trescal.cwms.core.system.entity.alligatorsettings.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettings;

public interface AlligatorSettingsService extends BaseService<AlligatorSettings, Integer> {
	/**
	 * Returns the settings for Alligator for the specified business contact
	 * @param businessContact
	 * @param production - true if production, false for development
	 * @return AlligatorSettings found hierarchically - for subdiv, or global default if none found for subdiv
	 */
	AlligatorSettings getSettings(Contact businessContact, boolean production);
}
