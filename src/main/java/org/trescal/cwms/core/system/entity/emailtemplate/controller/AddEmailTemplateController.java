package org.trescal.cwms.core.system.entity.emailtemplate.controller;

import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.emailtemplate.EmailTemplate;
import org.trescal.cwms.core.system.entity.emailtemplate.db.EmailTemplateService;
import org.trescal.cwms.core.system.entity.emailtemplate.form.EmailTemplateForm;
import org.trescal.cwms.core.system.entity.emailtemplate.form.EmailTemplateValidator;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
@RequestMapping(value="addemailtemplate.htm")
public class AddEmailTemplateController {
	@Autowired
	private CompanyService companyService; 
	@Autowired
	private EmailTemplateService emailTemplateService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private SystemComponentService systemComponentService;
	
	@Autowired
	private EmailTemplateValidator validator;
	
	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	public static final String VIEW_NAME = "trescal/core/system/addemailtemplate";
	public static final String REDIRECT_URL = "redirect:viewemailtemplates.htm";
	public static final String FORM_NAME = "form";
	
	@ModelAttribute(FORM_NAME)
	public EmailTemplateForm formBackingObject(Locale locale, 
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@RequestParam(name="component", required=true) Component component) {
		EmailTemplateForm form = new EmailTemplateForm();
		form.setCompid(companyDto.getKey());
		form.setComponent(component);
		form.setLanguageTag(locale.toLanguageTag());
		return form;
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public String referenceData(Model model, Locale displayLocale, @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		model.addAttribute("subdivs", this.subdivService.getAllActiveCompanySubdivsKeyValue(companyDto.getKey(), true));
		List<Locale> locales = this.supportedLocaleService.getSupportedLocales();
		model.addAttribute("locales", this.supportedLocaleService.getDTOList(locales, displayLocale));
		model.addAttribute("components", this.systemComponentService.getComponentsUsingEmailTemplates());
		
		return VIEW_NAME;
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String onSubmit(Model model, Locale displayLocale, @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@Valid @ModelAttribute(FORM_NAME) EmailTemplateForm form, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData(model, displayLocale, companyDto);
		}
		EmailTemplate template = new EmailTemplate();
		template.setComponent(form.getComponent());
		template.setLocale(Locale.forLanguageTag(form.getLanguageTag()));
		if (form.getSubdivid() != 0) template.setSubdiv(this.subdivService.get(form.getSubdivid()));
		template.setSubject(form.getSubject());
		template.setOrganisation(this.companyService.get(form.getCompid()));
		template.setTemplate(form.getTemplate());
		
		this.emailTemplateService.save(template);
		return REDIRECT_URL;
	}
}
