package org.trescal.cwms.core.system.entity.printfilerule.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.printfilerule.PrintFileRule;

public interface PrintFileRuleService
{
	void deletePrintFileRule(PrintFileRule printfilerule);

	void deletePrintFileRuleById(int id);

	PrintFileRule findPrintFileRule(int id);

	/**
	 * Returns the first {@link PrintFileRule} that applies to {@link File}s
	 * with the given file name, or null if no special rules exist.
	 * 
	 * @param fileName the file name to find rules for
	 * @return the {@link PrintFileRule}
	 */
	PrintFileRule findRuleForFileName(String fileName);

	List<PrintFileRule> getAllPrintFileRules();

	void insertPrintFileRule(PrintFileRule printfilerule);

	void saveOrUpdatePrintFileRule(PrintFileRule printfilerule);

	void updatePrintFileRule(PrintFileRule printfilerule);
}