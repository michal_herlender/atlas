package org.trescal.cwms.core.system.entity.componentdoctype;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.system.entity.componentdoctypetemplate.ComponentDoctypeTemplate;
import org.trescal.cwms.core.system.entity.supporteddoctype.SupportedDoctype;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;

@Entity
@Table(name = "componentdoctype")
public class ComponentDoctype
{
	private int id;
	private SystemComponent component;
	private SupportedDoctype doctype;
	private Set<ComponentDoctypeTemplate> templates;

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "componentId", unique = false, nullable = false)
	public SystemComponent getComponent()
	{
		return this.component;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "doctypeId", unique = false, nullable = false)
	public SupportedDoctype getDoctype()
	{
		return this.doctype;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "componentDoctype")
	public Set<ComponentDoctypeTemplate> getTemplates()
	{
		return this.templates;
	}

	public void setComponent(SystemComponent component)
	{
		this.component = component;
	}

	public void setDoctype(SupportedDoctype doctype)
	{
		this.doctype = doctype;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setTemplates(Set<ComponentDoctypeTemplate> templates)
	{
		this.templates = templates;
	}
}
