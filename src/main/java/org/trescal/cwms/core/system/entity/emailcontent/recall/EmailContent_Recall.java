package org.trescal.cwms.core.system.entity.emailcontent.recall;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.RecallCompanyConfiguration;
import org.trescal.cwms.core.recall.entity.recallcompanyconfiguration.db.RecallCompanyConfigurationService;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;

import java.util.Locale;

@Component
@Slf4j
public class EmailContent_Recall {
	@Autowired
	private MessageSource messages; 
	@Autowired
	private RecallCompanyConfigurationService rccService;
	@Autowired
	private TemplateEngine templateEngine;
	

	public static final String KEY_COMPANY = "company";
	public static final String KEY_SUBDIV = "subdiv";
	public static final String KEY_ADDRESS = "address";
	public static final String KEY_CONTACT = "contact";

	public EmailContentDto getContent(RecallDetail rdetail, Locale locale) {
        RecallCompanyConfiguration config = this.rccService.get(rdetail.getRecall().getCompany().getId());
        String body = getBodyThymeleaf(rdetail, config, locale);
        String subject = getSubject(rdetail, config);
        return new EmailContentDto(body, subject);
    }

	private String getBodyThymeleaf(RecallDetail rdetail, RecallCompanyConfiguration config, Locale locale) {
        Context context = new Context(locale);
        context.setVariable("businessCompanyName", rdetail.getRecall().getCompany().getDefaultBusinessContact().getSub().getComp().getConame());
        context.setVariable("recallDetail", rdetail);
        context.setVariable("customText", config.getCustomText().replaceAll("\\n", "<br/>"));

        log.debug("dateFrom : " + rdetail.getRecall().getDateFrom());
        log.debug("dateTo : " + rdetail.getRecall().getDateTo());

        String templatePath = config.getTemplateType().getThymeleafPath();
        return this.templateEngine.process(templatePath, context);
    }

    private String getSubject(RecallDetail rdetail, RecallCompanyConfiguration config) {
        return this.messages.getMessage(config.getSubjectKey().getMessageCode(), null, rdetail.getContact().getLocale()) +
            " - " +
            getRecallTypeDescription(rdetail);
    }

    public String getRecallTypeDescription(RecallDetail detail) {
        StringBuilder result = new StringBuilder();
        switch (detail.getRecallType()) {
            case COMPANY:
                result.append(this.messages.getMessage(KEY_COMPANY, null, detail.getContact().getLocale()));
                result.append(" - ");
                result.append(detail.getContact().getSub().getComp().getConame());
                break;
            case SUBDIV:
                result.append(this.messages.getMessage(KEY_SUBDIV, null, detail.getContact().getLocale()));
                result.append(" - ");
                result.append(detail.getContact().getSub().getSubname());
			break;
			case CONTACT:
				result.append(this.messages.getMessage(KEY_CONTACT, null, detail.getContact().getLocale()));
				result.append(" - ");
				result.append(detail.getContact().getName());
			break;	
			case ADDRESS:
				result.append(this.messages.getMessage(KEY_ADDRESS, null, detail.getContact().getLocale()));
				result.append(" - ");
				result.append(detail.getAddress().getAddr1());
				result.append(" - ");
				result.append(detail.getAddress().getTown());
			break;
		}
		return result.toString();
	}	
}
