package org.trescal.cwms.core.system.entity.instruction.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.system.dto.InstructionDTO;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

public interface InstructionService extends BaseService<Instruction, Integer> {

	List<InstructionDTO> findAllByJob(Collection<Integer> jodIds, Collection<Integer> jobItemIds,
			Integer allocatedSubdivId, InstructionType... instructionTypes);

	List<InstructionDTO> findAllByJobItems(Collection<Integer> jobItemIds, Integer allocatedSubdivId,
			InstructionType... instructionTypes);
	
	List<InstructionDTO> ajaxInstructions(Integer allocatedSubdivid, List<Integer> linkCoids, List<Integer> linkSubdivids, List<Integer> linkContactids,
			Integer linkContractid, Integer linkJobid, List<Integer> jobIds, List<Integer> jobItemIds, InstructionType[] instructionTypes,
			DeliveryType deliveryType);
}