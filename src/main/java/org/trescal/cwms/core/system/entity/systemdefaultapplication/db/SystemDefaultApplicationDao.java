package org.trescal.cwms.core.system.entity.systemdefaultapplication.db;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;
import org.trescal.cwms.core.system.enums.Scope;

import java.util.List;
import java.util.Optional;

public interface SystemDefaultApplicationDao extends AllocatedDao<Company, SystemDefaultApplication, Integer> {
    SystemDefaultApplication get(int defaultId, Scope scope, Integer entityId, Integer allocatedCompanyId);

    List<SystemDefaultApplication> find(List<SystemDefault> listSD, Company company, Subdiv subdiv, Contact contact, Integer allocatedCompanyId);

    SystemDefaultApplication findSystemDefaultApplication(int defaultId, Integer personid, Integer subdivid, Integer coid);

    Optional<SystemDefaultApplication> getOpt(int defaultId, Scope scope, Integer entityId, Integer allocatedCompanyId);

    List<SystemDefaultApplication> findApplicationWide(List<SystemDefault> listSD);
}