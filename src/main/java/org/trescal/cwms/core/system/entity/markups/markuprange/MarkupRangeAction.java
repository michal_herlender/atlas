/**
 * 
 */
package org.trescal.cwms.core.system.entity.markups.markuprange;

/**
 * Identifies the two types of behaviour allowed by {@link MarkupRange}s.
 * 
 * @author Richard
 */
public enum MarkupRangeAction
{
	PERCENTAGE, VALUE;
}
