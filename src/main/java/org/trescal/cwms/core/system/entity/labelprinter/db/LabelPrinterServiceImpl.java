package org.trescal.cwms.core.system.entity.labelprinter.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;
import org.trescal.cwms.core.system.entity.labelprinter.LabelPrinter;

@Service("LabelPrinterService")
public class LabelPrinterServiceImpl implements LabelPrinterService
{
	@Autowired
	private LabelPrinterDao labelPrinterDao;
	@Autowired
	private SessionUtilsService sessionServ;

	public void deleteLabelPrinter(LabelPrinter labelPrinter)
	{
		this.labelPrinterDao.remove(labelPrinter);
	}

	public void deleteLabelPrinterById(int id)
	{
		LabelPrinter labelPrinter = this.findLabelPrinter(id);

		// delete references from addresses (managed objects)
		for (Address addr : labelPrinter.getUsedByAddresses())
		{
			addr.setDefaultLabelPrinter(null);
		}

		// delete references from user preferences (managed objects)
		for (UserPreferences up : labelPrinter.getUsedByUsers())
		{
			up.setLabelPrinter(null);
		}

		this.deleteLabelPrinter(labelPrinter);
	}

	public LabelPrinter findLabelPrinter(int id)
	{
		return this.labelPrinterDao.find(id);
	}

	public List<LabelPrinter> getAllLabelPrinters()
	{
		return this.labelPrinterDao.findAll();
	}

	public LabelPrinter getDefaultLabelPrinterForContact(Contact con)
	{
		if ((con.getUserPreferences() != null)
				&& (con.getUserPreferences().getLabelPrinter() != null))
		{
			return con.getUserPreferences().getLabelPrinter();
		}
		else
		{
			if (con.getDefAddress() != null)
			{
				if (con.getDefAddress().getDefaultLabelPrinter() != null)
				{
					return con.getDefAddress().getDefaultLabelPrinter();
				}
			}
		}

		return null;
	}

	public LabelPrinter getDefaultLabelPrinterForCurrentContact()
	{
		return this.getDefaultLabelPrinterForContact(this.sessionServ.getCurrentContact());
	}

	public List<LabelPrinter> getLabelPrintersAtAddress(int addrId)
	{
		return this.labelPrinterDao.getLabelPrintersAtAddress(addrId);
	}

	public void insertLabelPrinter(LabelPrinter LabelPrinter)
	{
		this.labelPrinterDao.persist(LabelPrinter);
	}

	public void updateLabelPrinter(LabelPrinter LabelPrinter)
	{
		this.labelPrinterDao.update(LabelPrinter);
	}
}