package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class ClientDeliveryTotalPrice extends SystemDefaultBooleanType {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.CLIENT_DELIVERY_TOTAL_PRICE;
	}

}
