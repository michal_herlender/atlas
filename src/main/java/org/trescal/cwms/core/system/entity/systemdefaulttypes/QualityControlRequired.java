package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

/**
 * System default exported / used by external tools
 * (e.g. Calypso cal tool in USA)
 *
 */
@Component
public class QualityControlRequired extends SystemDefaultBooleanType {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.QUALITY_CONTROL_REQUIRED;
	}

}
