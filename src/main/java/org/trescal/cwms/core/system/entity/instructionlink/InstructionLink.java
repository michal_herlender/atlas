package org.trescal.cwms.core.system.entity.instructionlink;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.system.entity.instruction.Instruction;

public interface InstructionLink<T> {

	Instruction getInstruction();

	T getEntity();

	InstructionEntity getInstructionEntity();

	void setOrganisation(Company organisation);

	Company getOrganisation();
}