package org.trescal.cwms.core.system.entity.calibrationaccreditationlevel.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.calibrationaccreditationlevel.CalibrationAccreditationLevel;

public class CalibrationAccreditationLevelServiceImpl implements CalibrationAccreditationLevelService
{
	CalibrationAccreditationLevelDao CalibrationAccreditationLevelDao;

	public CalibrationAccreditationLevel findCalibrationAccreditationLevel(int id)
	{
		return CalibrationAccreditationLevelDao.find(id);
	}

	public void insertCalibrationAccreditationLevel(CalibrationAccreditationLevel CalibrationAccreditationLevel)
	{
		CalibrationAccreditationLevelDao.persist(CalibrationAccreditationLevel);
	}

	public void updateCalibrationAccreditationLevel(CalibrationAccreditationLevel CalibrationAccreditationLevel)
	{
		CalibrationAccreditationLevelDao.update(CalibrationAccreditationLevel);
	}

	public List<CalibrationAccreditationLevel> getAllCalibrationAccreditationLevels()
	{
		return CalibrationAccreditationLevelDao.findAll();
	}

	public void setCalibrationAccreditationLevelDao(CalibrationAccreditationLevelDao CalibrationAccreditationLevelDao)
	{
		this.CalibrationAccreditationLevelDao = CalibrationAccreditationLevelDao;
	}

	public void deleteCalibrationAccreditationLevel(CalibrationAccreditationLevel calibrationaccreditationlevel)
	{
		this.CalibrationAccreditationLevelDao.remove(calibrationaccreditationlevel);
	}

	public void saveOrUpdateCalibrationAccreditationLevel(CalibrationAccreditationLevel calibrationaccreditationlevel)
	{
		this.CalibrationAccreditationLevelDao.saveOrUpdate(calibrationaccreditationlevel);
	}
}