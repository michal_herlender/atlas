package org.trescal.cwms.core.system.entity.emailrecipient.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipientDto;

public interface EmailRecipientDao extends BaseDao<EmailRecipient, Integer> {
	
	public List<EmailRecipientDto> searchRecipientsDto(Collection<Integer> emailIds);
	
}