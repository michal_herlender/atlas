package org.trescal.cwms.core.system.entity.scheduledtask.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.scheduledtask.ScheduledTask;
import org.trescal.cwms.core.system.entity.scheduledtask.ScheduledTask_;

@Repository("ScheduledTaskDao")
public class ScheduledTaskDaoImpl extends BaseDaoImpl<ScheduledTask, Integer> implements ScheduledTaskDao {

	@Override
	protected Class<ScheduledTask> getEntity() {
		return ScheduledTask.class;
	}

	public ScheduledTask findScheduledTaskByClassAndMethodNames(String className, String methodName) {
		return getFirstResult(cb -> {
			CriteriaQuery<ScheduledTask> cq = cb.createQuery(ScheduledTask.class);
			Root<ScheduledTask> root = cq.from(ScheduledTask.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(ScheduledTask_.className), className));
			clauses.getExpressions().add(cb.equal(root.get(ScheduledTask_.methodName), methodName));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}
}