package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class PartDelivery extends SystemDefaultBooleanType
{
	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.PART_DELIVERY;
	}
}