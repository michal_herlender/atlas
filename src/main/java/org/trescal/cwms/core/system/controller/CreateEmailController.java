package org.trescal.cwms.core.system.controller;

import io.vavr.control.Option;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CollectionOfStringsEditor;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.EmailResultWrapper;
import org.trescal.cwms.core.system.dto.SimpleContact;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.emailrecipient.RecipientType;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.form.CreateEmailValidator;
import org.trescal.cwms.core.system.form.EmailForm;
import org.trescal.cwms.core.tools.EncryptionTools;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.net.URLDecoder;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME,
        Constants.HTTPSESS_NEW_FILE_LIST, Constants.SESSION_ATTRIBUTE_SUBDIV})
@Slf4j
public class CreateEmailController {
    @Autowired
    private BusinessDetailsService bdService;
    @Autowired
    private ContactService conService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private FileBrowserService fileBrowserService;
    @Autowired
    private JobCostingService jcService;
    @Autowired
    private UserService userService;
    @Autowired
    private SystemComponentService sysCompService;
    @Autowired
    private CreateEmailValidator validator;
    @Autowired
    private ServletContext servletContext;

    @Autowired
    private CreateEmailFormBacker formBackingCreator;

    @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
    public List<String> newFiles() {
        return new ArrayList<>();
    }


    private List<File> collectAttachments(EmailForm form, File attachment) {
        return Stream.concat(
                Stream.of(attachment).filter(Objects::nonNull),
                Stream.of(form.getAttachments()).filter(Objects::nonNull).flatMap(List::stream).map(this::verifyAndReturnFile)
        ).collect(Collectors.toList());
    }


    /**
     * If encryptedFileName is provided as bound form input (e.g. checkbox value), it's not a URL, so we need to decode first (true)
     * If encryptedFileName is provided by Spring RequestParam, it's a URL, already decoded. (false)
     */
    private File verifyAndReturnFile(String encryptedFilePath) {
        File result = null;
        try {
            String decryptionInput;
            log.debug("Decoding : " + encryptedFilePath);
            decryptionInput = StringEscapeUtils.unescapeHtml4(URLDecoder.decode(encryptedFilePath, "UTF-8"));
            log.debug("Decrypting : " + decryptionInput);
            String filePath = EncryptionTools.decrypt(decryptionInput);
            File file = new File(filePath);
            if (file.exists()) {
                log.debug("Adding file from path " + filePath);
                result = file;
            } else {
                log.error("File not found, skipping : " + filePath);
            }
        } catch (Exception e) {
            log.error("Could not decode and decrypt filepath : " + encryptedFilePath, e);
        }
        return result;
    }

    private String verifyAndReturnFileName(String encryptedFilePath) {
        File file = verifyAndReturnFile(encryptedFilePath);
        if (file != null) {
            return file.getName();
        }
        return "";
    }

    @ModelAttribute("currentContact")
    public Contact getCurrentContact(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        return userService.get(username).getCon();
    }

    private ComponentEntity getComponentEntity(Component component, Integer entityId) {
        ComponentEntity entity;
        try {
            entity = (ComponentEntity) this.sysCompService.findComponentEntity(component, entityId);
        } catch (ClassCastException e) {
            throw new ClassCastException("This class does not implement the ComponentEntity interface.");
        }
        return entity;
    }

    @ModelAttribute("form")
    public EmailForm formBackingObject(HttpServletRequest request, Locale locale,
                                       @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                       @ModelAttribute("currentContact") Contact currentContact,
                                       @RequestParam(value = "personid", required = false, defaultValue = "0") Integer personid,
                                       @RequestParam(value = "emailid", required = false, defaultValue = "0") Integer emailid,
                                       @RequestParam(value = "syscompid", required = false, defaultValue = "0") Integer syscompid,
                                       @RequestParam(value = "objid", required = false, defaultValue = "0") Integer objid,
                                       @RequestParam(value = "attach", required = false, defaultValue = "") String encryptedFilePath,
                                       @RequestParam(value = "coid", required = false, defaultValue = "0") Integer coid) {
        // set the different note types and preset comment types into the
        // session for use by dwr
        String unescapedEncodedFilePath = StringEscapeUtils.unescapeHtml4(encryptedFilePath);

        HttpSession session = request.getSession();
        session.setAttribute("emailcomment", PresetCommentType.EMAIL);
        BusinessDetails bd = this.bdService.getAllBusinessDetails(currentContact, null, null);
        EmailForm form = formBackingCreator.bake(locale, subdivDto, currentContact, personid,
            emailid, syscompid, objid, unescapedEncodedFilePath, bd, coid);
        if (form.getCoId() == null && coid != null && coid != 0) {
            form.setCoId(coid);
        }
        return form;
    }


    private List<SimpleContact> simpleContactList(List<Contact> cons, List<String> emails, String field) {
        return Stream.of(
                cons.stream()
                        .map(c -> SimpleContact.builder()
                                .field(field)
                                .email(c.getEmail())
                                .name(c.getName())
                                .personid(c.getPersonid())
                                .build()
                        ).collect(Collectors.toList())
                , emails.stream()
                        .map(e -> SimpleContact.builder()
                                .field(field)
                                .email(e).build()).collect(Collectors.toList())
        ).flatMap(List::stream).collect(Collectors.toList());
    }

    @InitBinder
    public void initBinder(ServletRequestDataBinder binder) {
        binder.registerCustomEditor(List.class, "ccs", new CollectionOfStringsEditor(ArrayList.class, String.class));
        binder.registerCustomEditor(List.class, "tos", new CollectionOfStringsEditor(ArrayList.class, String.class));
    }

    private void onBind(EmailForm form, Contact currentContact) {
        form.setFrom(currentContact.getEmail());
        Email email = new Email();
        email.setFrom(form.getFrom());
        email.setSubject(form.getSubject());
        email.setBody(form.getBody());
        email.setSentBy(currentContact);
        email.setSentOn(new Date());
        email.setEntityId(form.getEntityId());
        email.setComponent(form.getComponent());
        form.setEmail(email);
        List<Contact> toCons = new ArrayList<>();
        List<Contact> ccCons = new ArrayList<>();
        List<EmailRecipient> recips = new ArrayList<>();
        if (form.getToEmails() == null) {
            form.setToEmails(new ArrayList<>());
        }
        for (String to : form.getToEmails()) {
            EmailRecipient recip = new EmailRecipient();
            recip.setEmail(form.getEmail());
            recip.setEmailAddress(to);
            recip.setType(RecipientType.EMAIL_TO);
            recips.add(recip);
        }
        if (form.getToIds() == null) {
            form.setToIds(new ArrayList<>());
        }
        for (Integer toId : form.getToIds()) {
            Contact con = this.conService.get(toId);
            EmailRecipient recip = new EmailRecipient();
            recip.setEmail(form.getEmail());
            recip.setRecipientCon(con);
            recip.setEmailAddress(con.getEmail());
            recip.setType(RecipientType.EMAIL_TO);
            recips.add(recip);
            toCons.add(con);
        }
        if (form.getCcEmails() == null) {
            form.setCcEmails(new ArrayList<>());
        }
        for (String cc : form.getCcEmails()) {
            EmailRecipient recip = new EmailRecipient();
            recip.setEmail(form.getEmail());
            recip.setEmailAddress(cc);
            recip.setType(RecipientType.EMAIL_CC);
            recips.add(recip);
        }
        if (form.getCcIds() == null) {
            form.setCcIds(new ArrayList<>());
        }
        for (Integer ccId : form.getCcIds()) {
            Contact con = this.conService.get(ccId);
            EmailRecipient recip = new EmailRecipient();
            recip.setEmail(form.getEmail());
            recip.setRecipientCon(con);
            recip.setEmailAddress(con.getEmail());
            recip.setType(RecipientType.EMAIL_CC);
            recips.add(recip);
            ccCons.add(con);
        }
        form.setRecipients(recips);
        form.setToCons(toCons);
        form.setCcCons(ccCons);
    }

    @RequestMapping(value = "/createemail.htm", method = RequestMethod.POST)
    public String onSubmit(Model model,
                           @ModelAttribute("form") EmailForm form, BindingResult errors,
                           @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
                           @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        Contact currentContact = this.userService.get(username).getCon();

        this.onBind(form, currentContact);
        this.validator.validate(form, errors);
        if (errors.hasErrors()) {
            return referenceData(model, form, newFiles);
        }
        File tempDir = (File) servletContext.getAttribute(ServletContext.TEMPDIR);
        File tempAttachment = this.transferAttachedFileToTemp(form.getFile(), tempDir.getPath());
        List<File> allAttachments = this.collectAttachments(form, tempAttachment);
        form.getEmail().setPublish(form.isPublish());
        boolean sentSuccessfully = this.emailService.sendEmailUsingForm(form, allAttachments);
        //
        if (sentSuccessfully) {
            Component component = form.getEmail().getComponent();
            Integer entityId = form.getEmail().getEntityId();
            if (component == Component.JOB_COSTING) {
                JobCosting jobCosting = (JobCosting) this.sysCompService.findComponentEntity(component, entityId);
                // publish the JobCosting only if it is on defaultStatus which is 'CREATED'
                if(jobCosting.getStatus().getDefaultStatus())
                	this.jcService.publishJobCosting(jobCosting, currentContact);
            }
        }
        if (form.getEmail().getComponent() != null) {
            // SysCompId used by sent email
            Integer sysCompId = this.sysCompService.findComponent(form.getEmail().getComponent()).getComponentId();
            model.addAttribute("sysCompId", sysCompId);
        }
        deleteTempFile(tempAttachment);
        addAttachmentFileNames(model, form);
        return "trescal/core/system/sentemail";
    }

    @RequestMapping(value = "/createemail.htm", method = RequestMethod.GET)
    public String referenceData(Model model, @ModelAttribute("form") EmailForm form,
                                @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles) {
        // REFDATA BEGIN - only needed for system component based emails
        if (form.getComponent() != null && form.getEntityId() != null) {
            SystemComponent sysComp = this.sysCompService.findComponent(form.getComponent());
            ComponentEntity entity = getComponentEntity(form.getComponent(), form.getEntityId());

            SystemComponent compForAttachments = sysComp;
            int idForAttachments = form.getEntityId();
            ComponentEntity objForAttachments = entity;
            // if the component is a child of job, set component to parent
            // so that all attachments can be taken from anywhere within the
            // parent component dir
            if (sysComp.isUsingParentSettings() && (entity.getParentEntity() instanceof Job)) {
                Job j = (Job) entity.getParentEntity();
                compForAttachments = sysComp.getParentComponent();
                idForAttachments = j.getJobid();
                objForAttachments = j;
            }
            // get files in the root directory
            FileBrowserWrapper fileBrowserWrapper = this.fileBrowserService.getFilesForComponentRoot(compForAttachments.getComponent(), idForAttachments, newFiles);
            model.addAttribute(Constants.REFDATA_SYSTEM_COMPONENT, compForAttachments);
            model.addAttribute("entity", objForAttachments);
            model.addAttribute("id", idForAttachments);
            if (objForAttachments.getIdentifier().contains(Constants.VERSION_TEXT)) {
                try {
                    String path = objForAttachments.getIdentifier().substring(0, objForAttachments.getIdentifier().indexOf(Constants.VERSION_TEXT));
                    String ver = objForAttachments.getIdentifier().substring(objForAttachments.getIdentifier().lastIndexOf(Constants.VERSION_TEXT));
                    ver = ver.replaceAll(Constants.VERSION_TEXT, "");
                    int version = Integer.parseInt(ver);

                    model.addAttribute("identifier", path);
                    model.addAttribute("ver", version);
                } catch (Exception e) {
                    throw new RuntimeException("There was an error converting the version identifier to a directory.");
                }
            } else {
                model.addAttribute("identifier", objForAttachments.getIdentifier());
                model.addAttribute("ver", "");
            }
            model.addAttribute(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
            model.addAttribute(Constants.REFDATA_FILES_NAME, this.fileBrowserService.getFilesName(fileBrowserWrapper));
        }
        addAttachmentFileNames(model, form);
        form.setToContacts(simpleContactList(form.getToCons(), form.getToEmails(), "to"));
        form.setCcContacts(simpleContactList(form.getCcCons(), form.getCcEmails(), "cc"));
        // REFDATA END
        return "trescal/core/system/createemail";
    }

    private void addAttachmentFileNames(Model model, EmailForm form) {
        // Determine display-friendly file names from encrypted file names
        if (form.getComponentAttachment() != null) {
            model.addAttribute("componentAttachmentFileName", verifyAndReturnFileName(form.getComponentAttachment()));
        }
        // Used for all emails (e.g. resent with attachments)
        if (form.getAttachments() != null || form.getFile() != null) {
            val attachmentFileNames = Stream.concat(
                Stream.of(form.getFile()).filter(Objects::nonNull)
                    .map(MultipartFile::getOriginalFilename)
                , Stream.of(form.getAttachments()).filter(Objects::nonNull).flatMap(List::stream)
                    .map(this::verifyAndReturnFileName))
                .collect(Collectors.toList());
            model.addAttribute("attachmentFileNames", attachmentFileNames);
        }
    }

    private File transferAttachedFileToTemp(MultipartFile mpFile, String tempDir) {
        File file = null;
        if (mpFile != null) {
            String name = mpFile.getOriginalFilename();
            if (!name.equals("")) {
                file = new File(tempDir.concat(File.separator).concat(name));
                try {
                    mpFile.transferTo(file);
                } catch (Exception e) {
                    log.error("Error transferring file to temp", e);
                    file = null;
                }
            }
        }
        return file;
    }

    private void deleteTempFile(File tempFile) {
        try {
            if (tempFile != null) {
                tempFile.delete();
            }
        } catch (Exception e) {
            log.error("Error deleting temp file", e);
        }
    }
}


@lombok.Value(staticConstructor = "of")
class FormBackingConfig {
    Component component;
    ComponentEntity entity;
    Option<EmailResultWrapper> email;
}