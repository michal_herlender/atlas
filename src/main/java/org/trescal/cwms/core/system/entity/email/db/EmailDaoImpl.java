package org.trescal.cwms.core.system.entity.email.db;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.apache.commons.lang3.tuple.Triple;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.email.EmailDto;
import org.trescal.cwms.core.system.entity.email.Email_;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient_;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.form.SearchEmailForm;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;

@Repository("EmailDao")
public class EmailDaoImpl extends BaseDaoImpl<Email, Integer> implements EmailDao {
	
	@Override
	protected Class<Email> getEntity() {
		return Email.class;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Email findEagerEmail(int id)
	{
		Criteria crit = getSession().createCriteria(Email.class);
		crit.add(Restrictions.idEq(id));
		crit.setFetchMode("sentBy", FetchMode.JOIN);
		crit.setFetchMode("recipients", FetchMode.JOIN);
		crit.setFetchMode("recipients.type", FetchMode.JOIN);
		List<Email> list = crit.list();
		return list.size() > 0 ? list.get(0) : null;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	@Deprecated
	public List<Email> getAllComponentEmails(Component component, int id) {
		Criteria crit = getSession().createCriteria(Email.class);
		crit.add(Restrictions.eq("component", component));
		crit.add(Restrictions.eq("entityId", id));
		return crit.list();
	}
	
	@Override
	public void searchEmailsDto(SearchEmailForm form, PagedResultSet<EmailDto> prs) {
		this.completePagedResultSet(prs, EmailDto.class, cb -> cq -> {
			Root<Email> root = cq.from(Email.class);
			Join<Email, Contact> sentBy = root.join(Email_.sentBy);
			
			Predicate clauses = cb.conjunction();
			if (!form.getSubject().trim().isEmpty()) {
				clauses.getExpressions().add(cb.like(root.get(Email_.subject), "%"+form.getSubject().trim()+"%"));
			}

			if (form.getDate() != null) {
				Calendar calendar = new GregorianCalendar();
				calendar.setTime(form.getDate());
				DateTools.setTimeToZeros(calendar);
				Date startDate = calendar.getTime();
				calendar.add(Calendar.DATE, 1);
				Date finishDate = calendar.getTime();
				
				clauses.getExpressions().add(cb.between(root.get(Email_.sentOn), startDate, finishDate));
			}
			
			boolean hasRecipientPersonCriteria = form.getPersonid() != null && form.getPersonid() != 0; 
			boolean hasRecipientAddressCriteria = form.getToAddress().trim().isEmpty(); 
			boolean hasRecipientCriteria = hasRecipientPersonCriteria || hasRecipientAddressCriteria;

			if (hasRecipientCriteria) {
				Join<Email, EmailRecipient> recipients = root.join(Email_.recipients);
				if (hasRecipientPersonCriteria) {
					Join<EmailRecipient, Contact> recipientContact = recipients.join(EmailRecipient_.recipientCon); 
					clauses.getExpressions().add(cb.equal(recipientContact.get(Contact_.personid), form.getPersonid()));
				}

				if (hasRecipientAddressCriteria) {
					clauses.getExpressions().add(cb.like(recipients.get(EmailRecipient_.emailAddress), "%"+form.getToAddress().trim()+"%"));
				}
			}

			if (form.getPublish() != null) {
				clauses.getExpressions().add(cb.equal(root.get(Email_.publish), form.getPublish()));
			}

			if ((form.getFromPersonId() != null) && (form.getFromPersonId() != 0)) {
				Join<Email, Contact> sentByContact = root.join(Email_.sentBy); 
				clauses.getExpressions().add(cb.equal(sentByContact.get(Contact_.personid), form.getFromPersonId()));
			}			
			cq.where(clauses);
			
			Selection<EmailDto> selection = cb.construct(EmailDto.class, 
				root.get(Email_.component),
				root.get(Email_.entityId),
				root.get(Email_.from),
				root.get(Email_.id),
				root.get(Email_.publish),
				sentBy.get(Contact_.personid),
				sentBy.get(Contact_.firstName),
				sentBy.get(Contact_.lastName),
				root.get(Email_.sentOn),
				root.get(Email_.subject)); 
			
			// Most recent emails to appear first
			List<javax.persistence.criteria.Order> order = new ArrayList<>();
			order.add(cb.desc(root.get(Email_.sentOn))); 
			order.add(cb.desc(root.get(Email_.id)));
			
			return Triple.of(root, selection, order);
		});
	}
}