package org.trescal.cwms.core.system.entity.alligatorlabeltemplate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.enums.AlligatorParameterValue;

@Entity
@Table(name="alligatorlabelparameter")
public class AlligatorLabelParameter {
	private int id;
	private AlligatorLabelTemplate template;
	private String name;
	private AlligatorParameterValue value;
	private String format;		// optional
	private String text;		// optional
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}
	
	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="templateid", nullable=false, foreignKey=@ForeignKey(name="FK_alligatorlabelparameter_template"))
	public AlligatorLabelTemplate getTemplate() {
		return template;
	}
	
	@NotNull
	@Column(name = "name", length = 30, nullable = false, columnDefinition="varchar(30)")
	@Length(max = 30)
	public String getName() {
		return name;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name="value", nullable=false)
	public AlligatorParameterValue getValue() {
		return value;
	}
	
	@Column(name = "format", length = 30, nullable = true, columnDefinition="varchar(30)")
	@Length(max = 30)
	public String getFormat() {
		return format;
	}
	
	@Column(name = "text", length = 30, nullable = true, columnDefinition="varchar(30)")
	@Length(max = 30)
	public String getText() {
		return text;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public void setTemplate(AlligatorLabelTemplate template) {
		this.template = template;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setValue(AlligatorParameterValue value) {
		this.value = value;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public void setText(String text) {
		this.text = text;
	}
}
