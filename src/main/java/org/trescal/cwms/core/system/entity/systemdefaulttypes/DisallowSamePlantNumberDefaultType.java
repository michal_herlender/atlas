package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class DisallowSamePlantNumberDefaultType extends SystemDefaultBooleanType {
    @Override
    public SystemDefaultNames defaultName() {
        return SystemDefaultNames.DISALLOW_SAME_PLANT_NUMBER;
    }
}
