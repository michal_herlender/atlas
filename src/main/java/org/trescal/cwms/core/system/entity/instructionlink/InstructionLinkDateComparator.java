package org.trescal.cwms.core.system.entity.instructionlink;

import java.util.Comparator;

public class InstructionLinkDateComparator implements Comparator<InstructionLink<?>> {

	@Override
	public int compare(InstructionLink<?> link1, InstructionLink<?> link2) {
		int result = -link1.getInstruction().getLastModified().compareTo(link2.getInstruction().getLastModified());
		// Migrated / Integration Test instructions may all have same date, so compare by ID as well 
		if (result == 0) {
			result = link1.getInstruction().getInstructionid() - link2.getInstruction().getInstructionid();
		}
		return result;
	}
}