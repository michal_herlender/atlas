package org.trescal.cwms.core.system.entity.emailtemplate.db;

import java.util.Locale;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.emailtemplate.EmailTemplate;
import org.trescal.cwms.core.system.entity.emailtemplate.EmailTemplate_;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Repository
public class EmailTemplateDaoImpl extends AllocatedToCompanyDaoImpl<EmailTemplate, Integer>
		implements EmailTemplateDao {

	@Override
	protected Class<EmailTemplate> getEntity() {
		return EmailTemplate.class;
	}

	@Override
	public EmailTemplate get(Company company, Locale locale, Component component, Subdiv subdiv,
			EmailTemplate exclude) {
		try {
			return getSingleResult(cb -> {
				CriteriaQuery<EmailTemplate> cq = cb.createQuery(EmailTemplate.class);
				Root<EmailTemplate> emailTemplate = cq.from(EmailTemplate.class);
				Predicate clauses = cb.conjunction();
				clauses.getExpressions().add(cb.equal(emailTemplate.get(EmailTemplate_.component), component));
				clauses.getExpressions().add(cb.equal(emailTemplate.get(EmailTemplate_.locale), locale));
				clauses.getExpressions().add(cb.equal(emailTemplate.get(EmailTemplate_.organisation), company));
				if (subdiv == null)
					// if subdiv parameter passed is null, we only want to return "company wide"
					// templates
					clauses.getExpressions().add(cb.isNull(emailTemplate.get(EmailTemplate_.subdiv)));
				else
					clauses.getExpressions().add(cb.equal(emailTemplate.get(EmailTemplate_.subdiv), subdiv));
				if (exclude != null)
					clauses.getExpressions().add(cb.notEqual(emailTemplate, exclude));
				cq.where(clauses);
				return cq;
			});
		} catch (NoResultException ex) {
			return null;
		}
	}
}