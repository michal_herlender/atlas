package org.trescal.cwms.core.system.entity.defaultnote.db;

import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.admin.form.DefaultNoteForm;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.defaultnote.DefaultNote;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.entity.translation.TranslationServiceImpl;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Stream;

@Service("defaultNoteService")
public class DefaultNoteServiceImpl extends BaseServiceImpl<DefaultNote, Integer> implements DefaultNoteService {
    private final DefaultNoteDao defaultnoteDao;
    private final CompanyService companyService;
    private final SupportedLocaleService supportedLocaleService;
    private final SystemComponentService systemComponentService;
    private final TranslationService transService;

    @Autowired
    public DefaultNoteServiceImpl(DefaultNoteDao defaultnoteDao, CompanyService companyService, SupportedLocaleService supportedLocaleService, SystemComponentService systemComponentService) {
        this.defaultnoteDao = defaultnoteDao;
        this.companyService = companyService;
        this.supportedLocaleService = supportedLocaleService;
        this.systemComponentService = systemComponentService;
        this.transService = new TranslationServiceImpl(supportedLocaleService);
    }

    @Override
    protected BaseDao<DefaultNote, Integer> getBaseDao() {
        return defaultnoteDao;
    }

    @Override
    public int createNote(int coid, DefaultNoteForm form) {
        Company company = this.companyService.get(coid);
        SystemComponent component = this.systemComponentService.findComponent(form.getComponent());

        DefaultNote note = new DefaultNote();
		note.setLabel(form.getDefaultLabel());
		note.setNote(form.getDefaultNote());
		note.setPublish(form.getPublish());
		note.setOrganisation(company);
		note.setComponent(component);
		note.setLabelTranslation(createTranslations(form.getDefaultLabel(), form.getLabelTranslations()));
		note.setNoteTranslation(createTranslations(form.getDefaultNote(), form.getNoteTranslations()));
		
		super.save(note);
		return note.getDefaultNoteId();
	}

	private Set<Translation> createTranslations(String primaryText, Map<Locale,String> additionalText) {
        Locale primaryLocale = this.supportedLocaleService.getPrimaryLocale();
        Set<Translation> result = new HashSet<>();
		result.add(new Translation(primaryLocale, primaryText));
		for (Map.Entry<Locale,String> entry : additionalText.entrySet()) {
			result.add(new Translation(entry.getKey(), entry.getValue()));
		}
		return result;
	}

	@Override
	public void updateNote(int defaultNoteId, DefaultNoteForm form) {
		// Managed object+cascade automatically updated by JPA
		DefaultNote note = super.get(defaultNoteId);
		note.setLabel(form.getDefaultLabel());
		note.setNote(form.getDefaultNote());
		note.setPublish(form.getPublish());
		if (form.getComponent() != note.getComponent().getComponent()) {
			SystemComponent component = this.systemComponentService.findComponent(form.getComponent());
			note.setComponent(component);
		}
		updateTranslations(note.getLabelTranslation(), form.getDefaultLabel(), form.getLabelTranslations());
		updateTranslations(note.getNoteTranslation(), form.getDefaultNote(), form.getNoteTranslations());
	}
	
	private void updateTranslations(Set<Translation> translations, String primaryText, Map<Locale,String> additionalText) {
		Locale primaryLocale = this.supportedLocaleService.getPrimaryLocale();
		// Note, in case of dropped locales, the old translations are not removed.
		updateTranslation(translations, primaryLocale, primaryText);
		for (Map.Entry<Locale,String> entry : additionalText.entrySet()) {
			updateTranslation(translations, entry.getKey(), entry.getValue());
		}
	}
	
	/**
	 * Finds/updates the specified translation or creates if non-existent
	 */
	private void updateTranslation(Set<Translation> translations, Locale locale, String text) {
		Optional<Translation> existing = translations.stream()
				.filter(translation -> translation.getLocale().equals(locale))
				.findFirst();
		if (existing.isPresent()) {
			existing.get().setTranslation(text);
		} else {
            translations.add(new Translation(locale, text));
        }
    }

    @Override
    public List<DefaultNote> getDefaultNotesByComponent(Component component, Integer allocatedCompanyId) {
        return this.defaultnoteDao.getDefaultNotesByComponent(component, allocatedCompanyId);
    }

    @Override
    public <A extends Note> Stream<A> createNotesFromDefaultNotesForComponent(Component component, Class<A> noteClass, Integer companyId, Locale locale, Contact creator) {
        val dnotes = defaultnoteDao.getDefaultNotesByComponent(component, companyId);
        return dnotes.stream().map(dn -> createNoteFromDefaultNote(dn, noteClass, locale, creator))
                .filter(Objects::nonNull);
    }

    private <A extends Note> A createNoteFromDefaultNote(DefaultNote defNote, Class<A> noteClass, Locale locale, Contact creator) {
        try {
            val qn = noteClass.getConstructor().newInstance();
            qn.setActive(true);
            qn.setPublish(defNote.isPublish());

            String noteText = transService.getCorrectTranslation(defNote.getNoteTranslation(), locale);
            String noteTextHtml = noteText.replaceAll("\\n", "<br/>\n");

            if (StringUtils.isNotBlank(noteText))
                qn.setNote(noteTextHtml);
            else
                qn.setNote(defNote.getNote());

            String labeltext = transService.getCorrectTranslation(defNote.getLabelTranslation(), locale);
            if (StringUtils.isNotBlank(labeltext))
                qn.setLabel(labeltext);
            else
                qn.setLabel(defNote.getLabel());

            qn.setSetBy(creator);
            qn.setSetOn(new Date());
            return qn;
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
            return null;
        }
    }
}