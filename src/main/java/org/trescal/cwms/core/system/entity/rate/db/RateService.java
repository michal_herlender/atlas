package org.trescal.cwms.core.system.entity.rate.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.rate.Rate;

public interface RateService
{
	Rate findRate(int id);
	void insertRate(Rate rate);
	void updateRate(Rate rate);
	List<Rate> getAllRates();
	void deleteRate(Rate rate);
	void saveOrUpdateRate(Rate rate);
}