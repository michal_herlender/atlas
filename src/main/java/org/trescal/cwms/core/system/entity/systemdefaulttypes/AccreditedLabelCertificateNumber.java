package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class AccreditedLabelCertificateNumber extends SystemDefaultBooleanType {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.ACCREDITED_LABEL_CERTIFICATE_NUMBER;
	}

}
