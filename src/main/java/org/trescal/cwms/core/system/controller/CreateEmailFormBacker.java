package org.trescal.cwms.core.system.controller;

import static io.vavr.API.$;
import static io.vavr.API.Case;
import static io.vavr.API.Match;
import static io.vavr.Patterns.$Tuple2;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.addressprintformat.AddressPrintFormatter;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.jobcost.entity.addtionalcostcontact.AdditionalCostContact;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.EmailResultWrapper;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.emailrecipient.RecipientType;
import org.trescal.cwms.core.system.entity.emailtemplate.db.EmailTemplateService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.form.EmailForm;
import org.trescal.cwms.core.tools.StringTools;
import org.trescal.cwms.spring.model.KeyValue;

import io.vavr.Tuple;
import io.vavr.control.Option;
import lombok.val;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CreateEmailFormBacker {

    @Value("#{props['cwms.config.email.public']}")
    private boolean publish;
    @Autowired
    private ContactService conService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private SystemComponentService sysCompService;
    @Autowired
    private EmailTemplateService emailTemplateService;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private AddressPrintFormatter addressFormater;
    @Autowired
    private ITemplateEngine templateEngine;

    public EmailForm bake(Locale locale, KeyValue<Integer, String> subdivDto, Contact currentContact, Integer personid,
                          Integer emailId, Integer systemComponentId, Integer objectId, String encryptedFilePath, BusinessDetails bd,
                          Integer coId)
            throws IllegalArgumentException {
        return formForPersonId(personid, bd, currentContact)
                .orElse(() -> formByCompIdAndObjectId(systemComponentId, objectId, emailId, bd, currentContact,
                        subdivDto, encryptedFilePath, locale, coId))
                .getOrElseThrow(
                        () -> new IllegalArgumentException("Unsuitable parameters to create form backing object."));
    }

    private Option<EmailForm> formForPersonId(Integer personId, BusinessDetails bd, Contact currentContact) {
        return Option.when(personId != 0, () -> this.conService.get(personId))
                .map(con -> EmailForm.builder().toCon(con).from(bd.getSalesEmail()).publish(this.publish)
                        .coId(con.getSub().getComp().getCoid())
                        .includeSelfInReply(currentContact != null && currentContact.getUserPreferences() != null
                                && currentContact.getUserPreferences().isIncludeSelfInEmailReplies())
                        .build());
    }

    private Option<EmailForm> formByCompIdAndObjectId(Integer sysCompId, Integer objId, Integer emailId,
                                                      BusinessDetails bd, Contact currentContact, KeyValue<Integer, String> subdivDto, String encryptedFilePath,
                                                      Locale locale, Integer coId) {
        Option<FormBackingConfig> configuration = Option
                .when(sysCompId != 0 && objId != 0, () -> sysCompService.findComponent(sysCompId))
                .map(c -> FormBackingConfig.of(c.getComponent(), getComponentEntity(c.getComponent(), objId),
                        Option.when(emailId != 0, () -> emailService.getCompleteEmail(emailId))));

        return configuration.map(conf -> {
            val formAddress = produceFromAddress(bd, conf.getEntity(), conf.getEmail());
            val formBuilder = startBuildingForm(currentContact, conf.getComponent(), formAddress, objId, coId);
            return provideAttachments(conf.getEmail(), encryptedFilePath).andThen(defaultContactOrRecipients(conf))
                    .andThen(appendSubjectAndBody(conf, subdivDto, locale, currentContact,bd))
                    .andThen(ccConsForJobCosting(conf.getEmail(), conf.getEntity()))
                    .andThen(addBusinessCompanyId(conf.getComponent(), conf.getEntity()))
                    .apply(formBuilder).build();
        });
    }

    private UnaryOperator<EmailForm.EmailFormBuilder> defaultContactOrRecipients(FormBackingConfig conf) {
        return f -> provideTargetAddresses(f, conf.getEmail()).orElse(() -> provideDefaultContact(f, conf.getEntity()))
                .getOrElse(f);
    }

    private UnaryOperator<EmailForm.EmailFormBuilder> ccConsForJobCosting(Option<EmailResultWrapper> email,
                                                                          ComponentEntity entity) {
        // This is a one-off case where we don't want Job Costings to use mail
        // groups
        // but instead use their own additionalContacts lists.
        // Previously, mail groups could be used for ccs within subdiv (by
        // system component)
        // but are not currently configured / present in codebase.
        return form -> email.map(ignored -> form)
                .onEmpty(() -> castEntityToJobCostingContactList(entity).forEach(list -> list.forEach(form::ccCon)))
                .getOrElse(form);
    }

    private Option<List<Contact>> castEntityToJobCostingContactList(ComponentEntity entity) {
        return Option.when(entity instanceof JobCosting, () -> {
            assert entity instanceof JobCosting;
            return (JobCosting) entity;
        }).flatMap(jc -> Option.of(jc.getAdditionalContacts()))
                .map(contacts -> contacts.stream().map(AdditionalCostContact::getContact).collect(Collectors.toList()));
    }

    private UnaryOperator<EmailForm.EmailFormBuilder> appendSubjectAndBody(FormBackingConfig conf,
                                                                           KeyValue<Integer, String> subdivDto, Locale locale, Contact currentContact, BusinessDetails bd) {
        return form -> conf.getEmail().map(e -> form.subject(e.getEmail().getSubject()).body(e.getBody()))
                .getOrElse(() -> {
                    val businessSubdiv = this.subdivService.get(subdivDto.getKey());
                    val emailTemplate = this.emailTemplateService.findBestTemplate(businessSubdiv, locale,
                            conf.getComponent());
                    return (emailTemplate != null) ? form
                            .subject(this.renderTemplate(conf.getEntity(), emailTemplate.getSubject(), currentContact,bd,false))
                            .body(this.renderTemplate(conf.getEntity(), emailTemplate.getTemplate(), currentContact,bd,true))
                            : form;
                });
    }

    private Option<EmailForm.EmailFormBuilder> provideDefaultContact(EmailForm.EmailFormBuilder form,
                                                                     ComponentEntity entity) {
        return Option.of(entity.getDefaultContact()).map(con -> form.toCon(con).coId(con.getSub().getComp().getCoid()));
    }

    private Option<EmailForm.EmailFormBuilder> provideTargetAddresses(EmailForm.EmailFormBuilder form,
                                                                      Option<EmailResultWrapper> emailOpt) {
        return emailOpt.map(email -> email.getEmail().getRecipients()).map(l -> addRecipientsToForm(form, l));
    }

    private EmailForm.EmailFormBuilder addRecipientsToForm(EmailForm.EmailFormBuilder form,
                                                           List<EmailRecipient> recipients) {
        val map = recipients.stream().collect(Collectors.groupingBy(this::groupByRecipient));
        val toCons = getRecipientContact(map, "toCons");
        val ccCons = getRecipientContact(map, "ccCons");
        val toEmails = getRecipientEmail(map, "toEmails");
        val ccEmails = getRecipientEmail(map, "ccEmails");
        return form.toCons(toCons).ccCons(ccCons).toEmails(toEmails).ccEmails(ccEmails);
    }

    private List<Contact> getRecipientContact(Map<String, List<EmailRecipient>> map, String key) {
        val list = map.get(key);
        return list == null ? Collections.emptyList()
                : list.stream().map(EmailRecipient::getRecipientCon).collect(Collectors.toList());
    }

    private List<String> getRecipientEmail(Map<String, List<EmailRecipient>> map, String key) {
        val list = map.get(key);
        return list == null ? Collections.emptyList()
                : list.stream().map(EmailRecipient::getEmailAddress).collect(Collectors.toList());
    }

    private String groupByRecipient(EmailRecipient er) {
        val noContact = er.getRecipientCon() == null;
        val isTo = er.getType().equals(RecipientType.EMAIL_TO);
        return Match(Tuple.of(noContact, isTo)).of(Case($Tuple2($(false), $(true)), "toCons"),
                Case($Tuple2($(false), $(false)), "ccCons"), Case($Tuple2($(true), $(true)), "toEmails"),
                Case($Tuple2($(true), $(false)), "ccEmails"));
    }

    private UnaryOperator<EmailForm.EmailFormBuilder> provideAttachments(Option<EmailResultWrapper> emailOpt,
                                                                         String encryptedFilePath) {
        return form -> emailOpt.map(email -> {
            val attachments = Arrays.stream(email.getAttachments()).map(existingAttachments -> existingAttachments[2])
                    .collect(Collectors.toList());
            return form.attachments(attachments);
        }).getOrElse(() -> !encryptedFilePath.isEmpty()
                ? form.componentAttachment(StringTools.encodeUTF8(encryptedFilePath)) : form);
    }


    private EmailForm.EmailFormBuilder startBuildingForm(Contact currentContact, Component component,
                                                         String fromAddress, Integer objId, Integer coId) {

        return EmailForm.builder().component(component).componentEmail(true).entityId(objId).from(fromAddress)
                .publish(this.publish)
                .coId(coId)
                .includeSelfInReply(currentContact != null && currentContact.getUserPreferences() != null
                        && currentContact.getUserPreferences().isIncludeSelfInEmailReplies());
    }

    UnaryOperator<EmailForm.EmailFormBuilder> addBusinessCompanyId(Component component, ComponentEntity entity) {
        return builder -> {
            switch (component) {
                case QUOTATION:
                case INVOICE:
                case CREDIT_NOTE:
                case PURCHASEORDER:
                    builder.businessCompanyId(((Allocated<?>) entity).getOrganisation().getId());
                    break;
                case JOB:
                    builder.businessCompanyId(((Job)entity).getOrganisation().getComp().getId());
                    break;
                case DELIVERY:
                    builder.businessCompanyId(((JobDelivery) entity).getOrganisation().getComp().getCoid());
            }
            return builder;
        };
    }

    private String produceFromAddress(BusinessDetails bd, ComponentEntity entity, Option<EmailResultWrapper> email) {
        return email.map(EmailResultWrapper::getEmail).map(Email::getFrom)
                .getOrElse(Option.when(entity.isAccountsEmail(), bd::getAccountsEmail).getOrElse(bd.getSalesEmail()));
    }

    private ComponentEntity getComponentEntity(Component component, Integer entityId) {
        ComponentEntity entity;
        try {
            entity = (ComponentEntity) this.sysCompService.findComponentEntity(component, entityId);
        } catch (ClassCastException e) {
            throw new ClassCastException("This class does not implement the ComponentEntity interface.");
        }
        return entity;
    }

    private String renderTemplate(ComponentEntity obj, String template, Contact currentContact, BusinessDetails businessDetails, Boolean isBody) {
        if (template == null)
            return null;
        String temp = template.replaceAll(Constants.EMAIL_REPLACE_IDENTIFIER, obj.getIdentifier());
        if (obj.getDefaultContact() != null) {
            temp = temp.replaceAll(Constants.EMAIL_REPLACE_FIRSTNAME, obj.getDefaultContact().getFirstName());
            temp = temp.replaceAll(Constants.EMAIL_REPLACE_LASTNAME, obj.getDefaultContact().getLastName());
            temp = temp.replaceAll(Constants.EMAIL_REPLACE_CONTACT_TITLE, obj.getDefaultContact().getTitle());
        } else {
            temp = temp.replaceAll(Constants.EMAIL_REPLACE_FIRSTNAME, "Sir/Madam");
            temp = temp.replaceAll(Constants.EMAIL_REPLACE_LASTNAME, "");
            temp = temp.replaceAll(Constants.EMAIL_REPLACE_CONTACT_TITLE, "");
        }
        temp = temp.replaceAll(Constants.EMAIL_REPLACE_SENDER_FIRSTNAME, currentContact.getFirstName());
        temp = temp.replaceAll(Constants.EMAIL_REPLACE_SENDER_LASTNAME, currentContact.getLastName());
        temp = temp.replaceAll(Constants.EMAIL_REPLACE_SENDER_POSITION, (currentContact.getPosition() != null) ? currentContact.getPosition() : "");

        temp = temp.replaceAll(Constants.EMAIL_REPLACE_SENDER_ADDRESS, String.join("</br>", this.addressFormater
                .getAddressText(currentContact.getDefAddress(), LocaleContextHolder.getLocale(), true)));

        temp = temp.replaceAll(Constants.EMAIL_REPLACE_SENDER_EMAIL, currentContact.getEmail());

        temp = temp.replaceAll(Constants.EMAIL_REPLACE_SENDER_TELEPHONE, currentContact.getTelephone());
        temp = temp.replaceAll(Constants.EMAIL_REPLACE_SENDER_FAX, currentContact.getFax());
        temp = temp.replaceAll(Constants.EMAIL_REPLACE_SENDER_MOBILE, currentContact.getMobile());

        if(isBody) {
            val c = new Context(LocaleContextHolder.getLocale());
            c.setVariable("businessDetails",businessDetails);
            c.setVariable("contact",currentContact);
            val signature = this.templateEngine.process("email/fragments/email_footer.html",c);
            temp += signature;
        }
        return temp;
    }
}
