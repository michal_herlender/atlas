package org.trescal.cwms.core.system.entity.supportedcurrency;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.rate.Rate;
import org.trescal.cwms.core.system.entity.supportedcurrenyratehistory.SupportedCurrencyRateHistory;
import org.trescal.cwms.core.system.entity.supportedcurrenyratehistory.SupportedCurrencyRateHistoryComparator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "supportedcurrency")
public class SupportedCurrency
{
	private int currencyId;
	/** Obsolete code for the currency still used by some external reporting */
	private String accountsCode;

	/** ISO 4217 currency code * */
	private String currencyCode;

	/** XHTML Enitity Reference for this currency * */
	private String currencyERSymbol;

	/** Currency name in English only */
	private String currencyName;

    /**
     * Standard symbol for this currency *
     */
    private String currencySymbol;

    /**
     * Placement of currency symbol relative to value
     */
    private CurrencySymbolPosition symbolPosition;

    /**
     * The exchange rate to get 1 unit of the default currency from this
     * supportedcurrency eg. 1.234 USD to EUR*
     */
    private BigDecimal defaultRate;
    /**
     * Indicates the order to display the Supported Currency. Any null entries
     * are placed behind not null entries and sorted alphabetically
     */
    private Integer orderBy;

    /**
     * indicates when this currency's rate was last updated *
     */
    private Date rateLastSet;
    @JsonIgnore
    private Contact rateLastSetBy;
    /**
     * Indicates if this currency's exchange rate has been set *
     */
    private boolean rateSet;

    /**
     * used when invoices amounts are spelled in letters (certain currencies/configurations only, single language)
     */
    private String majorPluralName;            // e.g. dollars
    private String majorSingularName;        // e.g. dollar
    private String minorPluralName;            // e.g. cents
    private String minorSingularName;        // e.g. cent
    private Boolean minorUnit;                // e.g. true (for most currencies)
    private int minorExponent;                // e.g. 2 (for most currencies, 10^2 = 100 minor = 1 major)

    // External relationships
    @JsonIgnore
    private Set<Rate> exRates;
    @JsonIgnore
    private Set<Rate> baseRates;
    @JsonIgnore
    private Set<SupportedCurrencyRateHistory> rateHistory;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "currencyid", nullable = false, unique = true)
    @Type(type = "int")
    public int getCurrencyId() {
        return this.currencyId;
    }

    @NotNull
    @Column(name = "accountscode", nullable = false, length = 5)
    public String getAccountsCode() {
        return this.accountsCode;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "baseCurrency")
    public Set<Rate> getBaseRates()
	{
		return this.baseRates;
	}

	@NotNull
	@Column(name = "currencycode", nullable = false, length = 3)
	public String getCurrencyCode()
	{
		return this.currencyCode;
	}

	@NotNull
	@Column(name = "currencyersymbol", nullable = false, length = 10)
	public String getCurrencyERSymbol()
	{
		return this.currencyERSymbol;
	}

	@NotNull
	@Column(name = "currencyname", nullable = false, length = 100)
	public String getCurrencyName()
	{
		return this.currencyName;
	}

	@NotNull
	@Column(name = "currencysymbol", nullable = false, length = 3)
	public String getCurrencySymbol()
	{
		return this.currencySymbol;
	}

	@NotNull
	@Column(name = "exchangerate", nullable = false, precision = 20, scale = 2)
	public BigDecimal getDefaultRate()
	{
		return this.defaultRate;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "targetCurrency")
    public Set<Rate> getExRates() {
        return this.exRates;
    }

    @Column(name = "orderby", nullable = true)
    @Type(type = "int")
    public Integer getOrderBy() {
        return this.orderBy;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "currency")
    @SortComparator(SupportedCurrencyRateHistoryComparator.class)
    public Set<SupportedCurrencyRateHistory> getRateHistory() {
        return this.rateHistory;
    }

    @Column(name = "ratelastset", columnDefinition = "datetime")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getRateLastSet() {
        return this.rateLastSet;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ratelastsetby", nullable = false)
    public Contact getRateLastSetBy() {
        return this.rateLastSetBy;
    }

    @Column(name = "rateset", nullable = false, columnDefinition = "tinyint")
    public boolean isRateSet()
	{
		return this.rateSet;
	}

	@NotNull
	@Column(name = "majorpluralname", nullable = false, columnDefinition="nvarchar(50)")
	public String getMajorPluralName() {
		return majorPluralName;
	}

	@NotNull
	@Column(name = "majorsingularname", nullable = false, columnDefinition="nvarchar(50)")
	public String getMajorSingularName() {
		return majorSingularName;
	}

	@NotNull
	@Column(name = "minorpluralname", nullable = false, columnDefinition="nvarchar(50)")
	public String getMinorPluralName() {
		return minorPluralName;
	}

	@NotNull
	@Column(name = "minorsingularname", nullable = false, columnDefinition="nvarchar(50)")
	public String getMinorSingularName() {
		return minorSingularName;
	}

	@NotNull
	@Column(name = "minorunit", nullable = false, columnDefinition="bit")
	public Boolean getMinorUnit() {
		return minorUnit;
	}

	@NotNull
	@Column(name = "minorexponent", nullable = false, columnDefinition="int")
	public int getMinorExponent() {
		return minorExponent;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "symbolposition", nullable = false)
	public CurrencySymbolPosition getSymbolPosition() {
		return symbolPosition;
	}

	public void setAccountsCode(String accountsCode)
	{
		this.accountsCode = accountsCode;
	}

	public void setBaseRates(Set<Rate> baseRates)
	{
		this.baseRates = baseRates;
	}

	public void setCurrencyCode(String currencyCode)
	{
		this.currencyCode = currencyCode;
	}

	public void setCurrencyERSymbol(String currencyERSymbol)
	{
		this.currencyERSymbol = currencyERSymbol;
	}

	public void setCurrencyId(int currencyId)
	{
		this.currencyId = currencyId;
	}

	public void setCurrencyName(String currencyName)
	{
		this.currencyName = currencyName;
	}

	public void setCurrencySymbol(String currencySymbol)
	{
		this.currencySymbol = currencySymbol;
	}

	public void setDefaultRate(BigDecimal defaultRate)
	{
		this.defaultRate = defaultRate;
	}

	public void setExRates(Set<Rate> exRates)
	{
		this.exRates = exRates;
	}

	public void setOrderBy(Integer orderBy)
	{
		this.orderBy = orderBy;
	}

	public void setRateHistory(Set<SupportedCurrencyRateHistory> rateHistory)
	{
		this.rateHistory = rateHistory;
	}

	public void setRateLastSet(Date rateLastSet)
	{
		this.rateLastSet = rateLastSet;
	}

	public void setRateLastSetBy(Contact rateLastSetBy)
	{
		this.rateLastSetBy = rateLastSetBy;
	}

	public void setRateSet(boolean rateSet)
	{
		this.rateSet = rateSet;
	}

	public void setMajorPluralName(String majorPluralName) {
		this.majorPluralName = majorPluralName;
	}

	public void setMajorSingularName(String majorSingularName) {
		this.majorSingularName = majorSingularName;
	}

	public void setMinorPluralName(String minorPluralName) {
		this.minorPluralName = minorPluralName;
	}

	public void setMinorSingularName(String minorSingularName) {
		this.minorSingularName = minorSingularName;
	}

	public void setMinorUnit(Boolean minorUnit) {
		this.minorUnit = minorUnit;
	}

	public void setMinorExponent(int minorExponent) {
		this.minorExponent = minorExponent;
	}

	public void setSymbolPosition(CurrencySymbolPosition symbolPosition) {
		this.symbolPosition = symbolPosition;
	}
}
