package org.trescal.cwms.core.system.entity.calibrationtype.db;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.dto.CalibrationTypeOptionDto;
import org.trescal.cwms.core.system.entity.calibrationtype.dto.TranslatedCalibrationTypeDto;

/**
 * Interface for accessing and manipulating {@link CalibrationType} entities.
 * 
 * @author jamiev
 */
public interface CalibrationTypeService extends BaseService<CalibrationType, Integer>
{
	/**
	 * Delete the given {@link CalibrationType}.
	 * 
	 * @param caltype the {@link CalibrationType} to delete.
	 */
	void delete(CalibrationType caltype);

	/**
	 * Returns the {@link CalibrationType} entity identified by the given ID.
	 * 
	 * @param calTypeId the {@link CalibrationType} ID.
	 * @return the {@link CalibrationType}.
	 */
	CalibrationType find(int calTypeId);

	/**
	 * Returns a list of cal types that support procedure authorisations
	 * via having at least one CalibrationAccreditationLevel definition
	 * @return
	 */
	List<CalibrationType> getActiveProcedureCalTypes();
	
	/**
	 * Returns all {@link CalibrationType}(s) that are currently active.
	 * ordered during query by orderBy field
	 * 
	 * @return the {@link Set} of {@link CalibrationType}(s).
	 */
	List<CalibrationType> getActiveCalTypes();
	
	List<CalibrationTypeOptionDto> getActiveCalTypes(JobType jobType);
	
	List<TranslatedCalibrationTypeDto> getTranslatedCalibrationTypes(String language, String country);

	/**
	 * Returns all {@link CalibrationType}(s) stored in the database.
	 * ordered during query by orderBy field
	 * 
	 * @return the {@link Set} of {@link CalibrationType}(s).
	 */
	List<CalibrationType> getCalTypes();

	/**
	 * Returns the system default {@link CalibrationType} (i.e. the
	 * {@link CalibrationType} top of the list when sorted by the orderby field)
	 * 
	 * GB 2016-11-28: Use call with JobType when Job is known!
	 * 
	 * @return the {@link CalibrationType}
	 */
	CalibrationType getDefaultCalType();

	CalibrationType getDefaultCalType(JobType jobType, Subdiv allocatedSubdiv);

	/**
	 * Inserts the given {@link CalibrationType} into the database.
	 * 
	 * @param caltype the {@link CalibrationType} to insert.
	 */
	void insert(CalibrationType caltype);

	/**
	 * Updates the given {@link CalibrationType} in the database.
	 * 
	 * @param caltype the {@link CalibrationType} to update.
	 */
	void update(CalibrationType caltype);
	
	Boolean isCompatibleWithJobType(CalibrationType caltype, JobType jobtype);

	Map<Integer, String> getCachedServiceTypesByJobType(JobType jobType, Locale locale);
	
	/**
	 * Provides a Map of all calibration type ids to their corresponding service type id
	 * @return
	 */
	Map<Integer, Integer> getCalTypeToServiceTypeMap();
}
