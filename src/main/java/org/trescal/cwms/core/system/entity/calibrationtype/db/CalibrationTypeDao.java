package org.trescal.cwms.core.system.entity.calibrationtype.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.dto.CalibrationTypeOptionDto;

import javax.persistence.Tuple;
import java.util.List;

public interface CalibrationTypeDao extends BaseDao<CalibrationType, Integer> {
    List<CalibrationType> getActiveCalTypes(JobType jobType);

    List<CalibrationType> getActiveProcedureCalTypes();

    List<CalibrationType> getCalTypes(Boolean activeOnly);

    List<Tuple> getCalTypeAndServiceTypeIds();

    CalibrationType getDefaultCalType();

    Boolean isCompatibleWithJobType(CalibrationType caltype, JobType jobtype);

    List<CalibrationTypeOptionDto> getActiveCalTypesOptions(JobType jobType);
}