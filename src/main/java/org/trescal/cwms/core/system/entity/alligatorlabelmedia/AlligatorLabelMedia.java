package org.trescal.cwms.core.system.entity.alligatorlabelmedia;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name="alligatorlabelmedia")
public class AlligatorLabelMedia {
	private int id;
	private String name;
	private BigDecimal width;
	private BigDecimal height;
	private BigDecimal rotationAngle;
	private BigDecimal gap;
	private BigDecimal paddingLeft;
	private BigDecimal paddingTop;
	private BigDecimal paddingRight;
	private BigDecimal paddingBottom;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}
	
	@Column(name = "name", length = 50, nullable = false)
	@Length(max = 50)
	public String getName() {
		return name;
	}
	
    @Column(name = "width", nullable = false, precision = 15, scale = 5)
	public BigDecimal getWidth() {
		return width;
	}
    
    @Column(name = "height", nullable = false, precision = 15, scale = 5)
	public BigDecimal getHeight() {
		return height;
	}
    
    @Column(name = "rotationangle", nullable = false, precision = 15, scale = 5)
	public BigDecimal getRotationAngle() {
		return rotationAngle;
	}
    
    @Column(name = "gap", nullable = false, precision = 15, scale = 5)
	public BigDecimal getGap() {
		return gap;
	}
    
    @Column(name = "paddingleft", nullable = false, precision = 15, scale = 5)
	public BigDecimal getPaddingLeft() {
		return paddingLeft;
	}
    
    @Column(name = "paddingtop", nullable = false, precision = 15, scale = 5)
	public BigDecimal getPaddingTop() {
		return paddingTop;
	}
    
    @Column(name = "paddingright", nullable = false, precision = 15, scale = 5)
	public BigDecimal getPaddingRight() {
		return paddingRight;
	}
    
    @Column(name = "paddingbottom", nullable = false, precision = 15, scale = 5)
	public BigDecimal getPaddingBottom() {
		return paddingBottom;
	}
    
	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setWidth(BigDecimal width) {
		this.width = width;
	}
	public void setHeight(BigDecimal height) {
		this.height = height;
	}
	public void setRotationAngle(BigDecimal rotationAngle) {
		this.rotationAngle = rotationAngle;
	}
	public void setGap(BigDecimal gap) {
		this.gap = gap;
	}
	public void setPaddingLeft(BigDecimal paddingLeft) {
		this.paddingLeft = paddingLeft;
	}
	public void setPaddingTop(BigDecimal paddingTop) {
		this.paddingTop = paddingTop;
	}
	public void setPaddingRight(BigDecimal paddingRight) {
		this.paddingRight = paddingRight;
	}
	public void setPaddingBottom(BigDecimal paddingBottom) {
		this.paddingBottom = paddingBottom;
	}
}
