package org.trescal.cwms.core.system.entity.status.db;

import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import lombok.val;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.projection.StatusProjectionDTO;
import org.trescal.cwms.core.system.entity.status.Status;
import org.trescal.cwms.core.system.entity.status.Status_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;

@Repository("StatusDao")
public class StatusDaoImpl extends BaseDaoImpl<Status, Integer> implements StatusDao {
	
	private static final Logger logger = LoggerFactory.getLogger(StatusDaoImpl.class);
	
	@Override
	protected Class<Status> getEntity() {
		return Status.class;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Status findAcceptedStatus(Class<?> clazz)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(clazz);
		criteria.add(Restrictions.eq("accepted", true));
		List<Status> list = (List<Status>) criteria.getExecutableCriteria(getSession()).list();
		return list.size() > 0 ? (Status) list.get(0) : null;
	}

	@Override
	public <S extends Status> S findAcceptedStatusByClass(Class<S> clazz){
		return getFirstResult(cb -> {
			val cq = cb.createQuery(clazz);
			val status = cq.from(clazz);
			cq.where(cb.isTrue(status.get(Status_.accepted)));
			return cq;
		}).orElse(null);
	}

	@Override
	public <S extends Status> List<S> findAllAcceptedStatus(Class<S> clazz)
	{
		return getResultList(cb ->{
			val cq = cb.createQuery(clazz);
			val status = cq.from(clazz);
			cq.where(cb.isTrue(status.get("accepted")));
			cq.select(status);
			return cq;
		});
	}

	@SuppressWarnings("unchecked")
	public <T extends Status> T findDefaultStatus(Class<T> clazz)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(clazz);
		criteria.add(Restrictions.eq("defaultStatus", true));
		List<T> list = (List<T>) criteria.getExecutableCriteria(getSession()).list();
		return list.size() > 0 ? list.get(0) : null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <S extends Status> S findFollowingReceiptStatus(Class<S> clazz)
	{
		Criteria criteria = getSession().createCriteria(clazz);
		criteria.add(Restrictions.eq("followingReceipt", true));
		try {
			return (S) criteria.uniqueResult();
		} catch(HibernateException exception) {
			logger.error("There is no unique status following receipt.");
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public Status findIssuedStatus(Class<?> clazz)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(clazz);
		criteria.add(Restrictions.eq("followingIssue", true));
		List<Status> list = (List<Status>) criteria.getExecutableCriteria(getSession()).list();
		return list.size() > 0 ? (Status) list.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public Status findRejectedStatus(Class<?> clazz)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(clazz);
		criteria.add(Restrictions.eq("rejected", true));
		List<Status> list = (List<Status>) criteria.getExecutableCriteria(getSession()).list();
		return list.size() > 0 ? (Status) list.get(0) : null;
	}

	@SuppressWarnings("unchecked")
	public Status findRequiresAttentionStatus(Class<?> clazz)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(clazz);
		criteria.add(Restrictions.eq("requiringAttention", true));
		List<Status> list = (List<Status>) criteria.getExecutableCriteria(getSession()).list();
		return list.size() > 0 ? (Status) list.get(0) : null;
	}
	
	@SuppressWarnings("unchecked")
	public <S extends Status> S findStatus(int id, Class<S> clazz)
	{
		Criteria criteria = getSession().createCriteria(clazz);
		criteria.add(Restrictions.idEq(id));
		return (S) criteria.uniqueResult();
	}
	
	public Status findStatusByName(String name, Class<?> clazz)
	{
		Criteria criteria = getSession().createCriteria(clazz);
		criteria.add(Restrictions.eq("name", name));
		return (Status) criteria.uniqueResult();
	}

	@Override
	public <S extends Status> List<S> getAllStatuss(Class<S> clazz)
	{
		return getResultList(cb ->{
			val cq = cb.createQuery(clazz);
			val status = cq.from(clazz);
			cq.select(status);
			return cq;
		});
	}

	@Override
	public <S extends Status> List<StatusProjectionDTO> getAllProjectionStatuss(Class<S> clazz, Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<StatusProjectionDTO> cq = cb.createQuery(StatusProjectionDTO.class);
			Root<S> root = cq.from(clazz);
			Join<S, Translation> translationJoin = root.join("nametranslations");
			translationJoin.on(cb.equal(translationJoin.get(Translation_.locale), locale));
			
			cq.select(cb.construct(StatusProjectionDTO.class, root.get("statusid"), translationJoin.get(Translation_.translation)));
			return cq;
		});
	}
}