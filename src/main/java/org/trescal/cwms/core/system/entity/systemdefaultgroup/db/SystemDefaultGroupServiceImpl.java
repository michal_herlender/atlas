package org.trescal.cwms.core.system.entity.systemdefaultgroup.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.systemdefaultgroup.SystemDefaultGroup;
import org.trescal.cwms.core.system.enums.Scope;

public class SystemDefaultGroupServiceImpl implements SystemDefaultGroupService
{
	SystemDefaultGroupDao SystemDefaultGroupDao;

	public void deleteSystemDefaultGroup(SystemDefaultGroup systemdefaultgroup)
	{
		this.SystemDefaultGroupDao.remove(systemdefaultgroup);
	}

	public SystemDefaultGroup findSystemDefaultGroup(int id)
	{
		return this.SystemDefaultGroupDao.find(id);
	}

	@Override
	public List<SystemDefaultGroup> getAllSystemDefaultByCorole(String corole, Scope scope)
	{
		return this.SystemDefaultGroupDao.getAllSystemDefaultByCorole(corole, scope);
	}

	public List<SystemDefaultGroup> getAllSystemDefaultGroups()
	{
		return this.SystemDefaultGroupDao.findAll();
	}

	public void insertSystemDefaultGroup(SystemDefaultGroup SystemDefaultGroup)
	{
		this.SystemDefaultGroupDao.persist(SystemDefaultGroup);
	}

	public void saveOrUpdateSystemDefaultGroup(SystemDefaultGroup systemdefaultgroup)
	{
		this.SystemDefaultGroupDao.saveOrUpdate(systemdefaultgroup);
	}

	public void setSystemDefaultGroupDao(SystemDefaultGroupDao SystemDefaultGroupDao)
	{
		this.SystemDefaultGroupDao = SystemDefaultGroupDao;
	}

	public void updateSystemDefaultGroup(SystemDefaultGroup SystemDefaultGroup)
	{
		this.SystemDefaultGroupDao.update(SystemDefaultGroup);
	}
}