package org.trescal.cwms.core.system.form.genericentityvalidator;

import java.text.ParseException;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.NumberTools;

/**
 * @author JamieV
 */
public class SystemDefaultApplicationValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(SystemDefaultApplication.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		SystemDefaultApplication sda = (SystemDefaultApplication) target;

		String dataType = sda.getSystemDefault().getDefaultDataType().trim();

		// check is the correct data type
		if (dataType.equals("boolean")) {
			String value = sda.getValue();
			if (!value.equals("Y") && !value.equals("N")) {
				errors.rejectValue("value", null, "This system default must be a boolean value");
			}
		} else if (dataType.equals("integer")) {
			String value = sda.getValue();
			if (!NumberTools.isAnInteger(value)) {
				errors.rejectValue("value", null, "This system default must be an integer value");
			}
		} else if (dataType.equals("percent")) {
			String value = sda.getValue();
			if (!NumberTools.isAnInteger(value)) {
				errors.rejectValue("value", null, "This system default must be an integer value");
			} else {
				int val = Integer.parseInt(value);
				if ((val < 0) || (val > 100)) {
					errors.rejectValue("value", null,
							"This system default must be a percentage (i.e. between 0 and 100)");
				}
			}
		} else if (dataType.equals("double")) {
			String value = sda.getValue();
			if (!NumberTools.isADouble(value)) {
				errors.rejectValue("value", null, "This system default must be a decimal number (e.g. 150.00)");
			} else {
				Double d = Double.parseDouble(value);

				// round the number if necessary
				sda.setValue(((Double) NumberTools.roundDouble(d, 2)).toString());
			}

		} else if (dataType.equals("date")) {
			String value = sda.getValue();

			try {
				DateTools.df.parse(value);
			} catch (ParseException e) {
				errors.rejectValue("value", null, "Error parsing the value");
				e.printStackTrace();
			}
		}
	}
}