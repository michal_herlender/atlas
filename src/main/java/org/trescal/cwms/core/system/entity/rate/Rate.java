package org.trescal.cwms.core.system.entity.rate;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

/**
 * Defines an exchange rate between the system default {@link SupportedCurrency}
 * and another {@link SupportedCurrency}. This is currency <i>not</i> used, but
 * has been left in place for possible future use nonetheless.
 * 
 * @author richard
 */
@Entity
@Table(name = "rate")
public class Rate {
    private boolean active;
    /**
     * The base currency the rate exchanges from. (Typically the system
     * default).
     */
    private SupportedCurrency baseCurrency;
    private String description;
    private int id;
    private String name;
    /**
     * The conversion ratio used to calculate the targetCurrency from a given
     * value of the baseCurrency.
     */
    private BigDecimal rate;

    @JsonIgnore
    private Contact setBy;

    private Date setOn;

    /**
     * The currency the rate exchanges to.
     */
    private SupportedCurrency targetCurrency;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "baseid", nullable = false)
    @JsonBackReference
    public SupportedCurrency getBaseCurrency() {
        return this.baseCurrency;
    }

    @Length(max = 500)
    @Column(name = "description", length = 500)
    public String getDescription() {
        return this.description;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    @Length(max = 100)
    @Column(name = "name", length = 100)
    public String getName() {
        return this.name;
    }

    @NotNull
    @Column(name = "rate", nullable = false, precision = 10, scale = 2)
    public BigDecimal getRate() {
        return this.rate;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "personid", nullable = false)
    public Contact getSetBy() {
        return this.setBy;
    }

    @NotNull
    @Column(name = "seton", nullable = false, columnDefinition = "datetime")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getSetOn() {
        return this.setOn;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "targetid", nullable = false)
    @JsonBackReference
    public SupportedCurrency getTargetCurrency() {
        return this.targetCurrency;
    }

    @Column(name = "active", nullable = false, columnDefinition = "tinyint")
    public boolean isActive() {
        return this.active;
    }

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public void setBaseCurrency(SupportedCurrency baseCurrency)
	{
		this.baseCurrency = baseCurrency;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setRate(BigDecimal rate)
	{
		this.rate = rate;
	}

	public void setSetBy(Contact setBy)
	{
		this.setBy = setBy;
	}

	public void setSetOn(Date setOn)
	{
		this.setOn = setOn;
	}

	public void setTargetCurrency(SupportedCurrency targetCurrency)
	{
		this.targetCurrency = targetCurrency;
	}
}