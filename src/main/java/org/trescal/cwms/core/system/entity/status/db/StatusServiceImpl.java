package org.trescal.cwms.core.system.entity.status.db;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.projection.StatusProjectionDTO;
import org.trescal.cwms.core.dwr.DWRService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.status.Status;

@Service("StatusService")
public class StatusServiceImpl extends BaseServiceImpl<Status, Integer> implements StatusService
{
	@Autowired
	private DWRService ajaxServ;
	@Autowired
	private StatusDao statusDao;
	
	@Override
	protected BaseDao<Status, Integer> getBaseDao() {
		return statusDao;
	}
	
	@Override
	public List<Status> ajaxFindAllAcceptedStatus()
	{
		@SuppressWarnings("unchecked")
		Class<? extends Status> clazz = (Class<? extends Status>) this.ajaxServ.getClass(Constants.HTTPSESS_STATUS_CLASS_NAME, null);
		return this.findAllAcceptedStatus(clazz).stream().map(s -> (Status) s).collect(Collectors.toList());
	}
	
	@Override
	public Status findAcceptedStatus(Class<?> clazz)
	{
		return this.statusDao.findAcceptedStatus(clazz);
	}

	@Override
	public <S extends Status> S findAcceptedStatusByClass(Class<S> clazz) {
		return statusDao.findAcceptedStatusByClass(clazz);
	}

	@Override
	public <S extends Status> List<S> findAllAcceptedStatus(Class<S> clazz)
	{
		return this.statusDao.findAllAcceptedStatus(clazz);
	}
	
	@Override
	public <S extends Status> S findDefaultStatus(Class<S> clazz)
	{
		return this.statusDao.findDefaultStatus(clazz);
	}

	public <S extends Status> S findFollowingReceiptStatus(Class<S> clazz)
	{
		return this.statusDao.findFollowingReceiptStatus(clazz);
	}

	public Status findIssuedStatus(Class<?> clazz)
	{
		return this.statusDao.findIssuedStatus(clazz);
	}

	public Status findRejectedStatus(Class<?> clazz)
	{
		return this.statusDao.findRejectedStatus(clazz);
	}

	public Status findRequiresAttentionStatus(Class<?> clazz)
	{
		return this.statusDao.findRequiresAttentionStatus(clazz);
	}

	public <S extends Status> S findStatus(int id, Class<S> clazz)
	{
		return this.statusDao.findStatus(id, clazz);
	}

	public Status findStatusByName(String name, Class<?> clazz)
	{
		return this.statusDao.findStatusByName(name, clazz);
	}

	public <S extends Status> List<S> getAllStatuss(Class<S> clazz)
	{
		return this.statusDao.getAllStatuss(clazz);
	}

	public void insertStatus(Status status)
	{
		this.statusDao.persist(status);
	}

	public void setAjaxServ(DWRService ajaxServ)
	{
		this.ajaxServ = ajaxServ;
	}

	public void setStatusDao(StatusDao statusDao)
	{
		this.statusDao = statusDao;
	}

	public void updateStatus(Status status)
	{
		this.statusDao.update(status);
	}

	@Override
	public <S extends Status> List<StatusProjectionDTO> getAllProjectionStatuss(Class<S> clazz, Locale locale) {
		return this.statusDao.getAllProjectionStatuss(clazz, locale);
	}
}