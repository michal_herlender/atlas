package org.trescal.cwms.core.system.entity.translation;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.control.Either;
import lombok.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.system.Constants;

@Controller
@JsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_VERSION })
public class TranslationsJsonController {

	@Autowired
	private MessageSource messageSource;

	@PostMapping(value = "/getTranslations.json")
	@ResponseBody
	public Either<String,TranslationsResult> getTranslations(@ModelAttribute(Constants.SESSION_ATTRIBUTE_VERSION) String version,
								  @RequestBody List<String> codes, Locale locale) {
		Map<String,String> translations = codes.stream().map(code -> {
			try {
				return Tuple.of(code,messageSource.getMessage(code,null,locale));
			} catch (NoSuchMessageException e) {
			   return Tuple.of(code,"");
			}
		}).filter(t -> !t._2.isEmpty()).collect(Collectors.toMap(Tuple2::_1,Tuple2::_2));
		return translations.isEmpty()?Either.left("No translations founded"):
				Either.right(TranslationsResult.of(version,translations));
	}

}


@Value(staticConstructor = "of")
class TranslationsResult {
	String version;
	Map<String,String> translations;
}
