package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.tools.DateTools;

public abstract class SystemDefaultDateType extends SystemDefaultType<Date> {

	@Override
	public boolean isDiscreteType() {
		return false;
	}
	
	@Override
	public boolean isMultiChoice() {
		return false;
	}

	@Override
	public List<String> getValues() {
		return new ArrayList<>();
	}

	@Override
	public Date parseValue(String value) {
		try {
			return DateTools.df.parse(value);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getStringValue(Date date) {
		return DateTools.df.format(date);
	}

}
