package org.trescal.cwms.core.system.entity.template.db;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.template.Template;

@Repository("TemplateDao")
public class TemplateDaoImpl extends BaseDaoImpl<Template, Integer> implements TemplateDao {
	
	@Override
	protected Class<Template> getEntity() {
		return Template.class;
	}
	
	@Override
	public Template findByName(String name) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Template> query = builder.createQuery(Template.class);
		Root<Template> root = query.from(Template.class);
		query.where(builder.equal(root.get("name"), name));
		TypedQuery<Template> tq = getEntityManager().createQuery(query);
		return tq.getSingleResult();
	}
}