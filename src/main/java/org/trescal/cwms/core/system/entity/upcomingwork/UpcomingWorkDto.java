package org.trescal.cwms.core.system.entity.upcomingwork;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.UpcomingWorkJobLinkAjaxDto;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpcomingWorkDto {
    private Integer upcomingWorkId;
    private String title;
    private String desc;
    private int deptId;
    private int coId;
    private int personId;
    private int userResponsibleId;
    private String start;
    private String end;
    List<UpcomingWorkJobLinkAjaxDto> jobNoList;
}
