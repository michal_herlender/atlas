package org.trescal.cwms.core.system.entity.status.db;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.projection.StatusProjectionDTO;
import org.trescal.cwms.core.system.entity.status.Status;

public interface StatusDao extends BaseDao<Status, Integer> {
	
	Status findAcceptedStatus(Class<?> clazz);

    <S extends Status> S findAcceptedStatusByClass(Class<S> clazz);

    <S extends Status> List<S> findAllAcceptedStatus(Class<S> clazz);

	<S extends Status> S findDefaultStatus(Class<S> clazz);

	<S extends Status> S findFollowingReceiptStatus(Class<S> clazz);

	Status findIssuedStatus(Class<?> clazz);

	Status findRejectedStatus(Class<?> clazz);

	Status findRequiresAttentionStatus(Class<?> clazz);

	<S extends Status> S findStatus(int id, Class<S> clazz);

	Status findStatusByName(String name, Class<?> clazz);

	<S extends Status> List<S> getAllStatuss(Class<S> clazz);
	
	<S extends Status> List<StatusProjectionDTO> getAllProjectionStatuss(Class<S> clazz, Locale locale);
}