package org.trescal.cwms.core.system.entity.emailrecipient;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.email.Email;

/**
 * @author jamiev
 */
@Entity
@Table(name = "emailrecipient")
@Builder @AllArgsConstructor @NoArgsConstructor
public class EmailRecipient extends Auditable
{
	private Email email;
	private String emailAddress;
	private int id;
	private Contact recipientCon;
	private RecipientType type;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "emailid")
	public Email getEmail()
	{
		return this.email;
	}

	/**
	 * @Email annotation intentionally removed; best validated before email is sent
	 */
	@NotNull
	@Length(min = 1, max = 100)
	@Column(name = "emailaddress", nullable = false, length = 100)
	public String getEmailAddress()
	{
		return this.emailAddress;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid")
	public Contact getRecipientCon()
	{
		return this.recipientCon;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "type", nullable = false)
	public RecipientType getType()
	{
		return this.type;
	}

	public void setEmail(Email email)
	{
		this.email = email;
	}

	public void setEmailAddress(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setRecipientCon(Contact recipientCon)
	{
		this.recipientCon = recipientCon;
	}

	public void setType(RecipientType type)
	{
		this.type = type;
	}
}