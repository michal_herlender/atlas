package org.trescal.cwms.core.system.entity.note;

import java.util.Comparator;

/**
 * Note Comparator. Ordering hierachy is to show active before inactive, public
 * before private then sort by newest first.
 * 
 * @author Richard
 */
public class NoteComparator implements Comparator<Note>
{
	public int compare(Note q1, Note q2)
	{
		if ((q1.getNoteid() == 0) && (q2.getNoteid() == 0))
		{
			return 1;
		}
		else if (q1.isActive() && !q2.isActive())
		{
			return 1;
		}
		else if (!q1.isActive() && q2.isActive())
		{
			return -1;
		}
		else if (q1.getPublish() && !q2.getPublish())
		{
			return -1;
		}
		else if (!q1.getPublish() && q2.getPublish())
		{
			return 1;
		}
		else if ((q1.getSetOn() != null) && (q2.getSetOn() != null)
				&& (q1.getSetOn().compareTo(q2.getSetOn()) != 0))
		{
			return q2.getSetOn().compareTo(q1.getSetOn());
		}
		else
		{
			if ((q2.getNoteid() == 0) && (q1.getNoteid() == 0))
			{
				return 1;
			}
			else
			{
				return ((Integer) q2.getNoteid()).compareTo(q1.getNoteid());
			}
		}
	}
}