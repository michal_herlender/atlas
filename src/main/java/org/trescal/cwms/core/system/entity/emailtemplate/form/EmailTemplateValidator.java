package org.trescal.cwms.core.system.entity.emailtemplate.form;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.system.entity.emailtemplate.EmailTemplate;
import org.trescal.cwms.core.system.entity.emailtemplate.db.EmailTemplateService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class EmailTemplateValidator extends AbstractBeanValidator {
	@Autowired
	private CompanyService companyService;
	@Autowired
	private SubdivService subdivService;
	
	@Autowired
	private EmailTemplateService emailTemplateService;

	public static final String DUPLICATE_CODE = "error.emailtemplate.duplicate";
	public static final String DUPLICATE_MESSAGE = "There is already another email template with the same parameters.";
	
	@Override
	public boolean supports(Class<?> arg0) {
		return EmailTemplateForm.class.isAssignableFrom(arg0);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		EmailTemplateForm form = (EmailTemplateForm) target;
		Locale locale = Locale.forLanguageTag(form.getLanguageTag());
		Company company = this.companyService.get(form.getCompid());
		Subdiv subdiv = form.getSubdivid() == 0 ? null : this.subdivService.get(form.getSubdivid());
		// If editing a template, ensure no duplicates other than this one
		EmailTemplate excludeTemplate = form.getId() == 0 ? null : this.emailTemplateService.get(form.getId());
		// Ensure no duplicates
		EmailTemplate duplicate = emailTemplateService.get(company, locale, form.getComponent(), subdiv, excludeTemplate);
		if (duplicate != null) {
			errors.reject(DUPLICATE_CODE, null, DUPLICATE_MESSAGE);
		}		
	}

}
