package org.trescal.cwms.core.system.entity.instructionlink.db;

import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionEntity;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLink;

public interface InstructionLinkService {
	InstructionLink<?> get(int instructionLinkID, InstructionEntity entity);

	void merge(InstructionLink<?> link);

	List<InstructionLink<?>> getAllForJob(Job job);

	List<InstructionLink<?>> getAllForHire(Hire hire);

	void delete(InstructionLink<?> link);

	Set<Instruction> getAllForJobitem(Contract contract);

}