package org.trescal.cwms.core.system.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionEntity;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLink;
import org.trescal.cwms.core.system.entity.instructionlink.db.InstructionLinkService;
import org.trescal.cwms.core.system.form.DeleteInstructionForm;

@Controller @IntranetController
public class InstructionDeleteController {
	@Autowired
	private InstructionLinkService instructionLinkService;
	
	public static final String FORM = "command";
	public static final String INSTRUCTION_LINK = "instructionLink";
	public static final String TAB_URL_FRAGMENT = "&loadtab=instructions-tab";

	@ModelAttribute(FORM)
	public DeleteInstructionForm formBackingObject(
			@RequestParam(value="linkid") Integer linkid,
			@RequestParam(value="entity") InstructionEntity entity,
			HttpServletRequest request) {
		DeleteInstructionForm form = new DeleteInstructionForm();
		form.setEntity(entity);
		form.setLinkid(linkid);
		if(StringUtils.isEmpty(form.getRefererUrl())){
			form.setRefererUrl(request.getHeader("referer"));
		}
		return form;
	}
	
	@RequestMapping(value="/deleteinstruction.htm", method=RequestMethod.POST)
	protected RedirectView onSubmit(@ModelAttribute(FORM) DeleteInstructionForm form) {
		InstructionLink<?> link = instructionLinkService.get(form.getLinkid(), form.getEntity());
		instructionLinkService.delete(link);
		String viewName = form.getRefererUrl();
		if (viewName.indexOf(TAB_URL_FRAGMENT) == -1) viewName = viewName + TAB_URL_FRAGMENT;
		return new RedirectView(viewName);
	}
			
	@RequestMapping(value="/deleteinstruction.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute(FORM) DeleteInstructionForm form,
			HttpServletRequest request) {
		Map<String, Object> model = new HashMap<>();
		model.put(INSTRUCTION_LINK, instructionLinkService.get(form.getLinkid(), form.getEntity()));
		return new ModelAndView("/trescal/core/system/deleteinstruction", model);
	}

}
