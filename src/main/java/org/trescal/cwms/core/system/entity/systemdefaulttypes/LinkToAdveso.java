package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.system.enums.Scope;

@Component
public class LinkToAdveso extends SystemDefaultBooleanType {

	@Autowired
	private LinkToAdvesoFromDate linkToAdvesoAfterDate;
	@Autowired
	private CacheManager cacheManager;

	@Cacheable(value = "cachedIsAdvesoActiveOnSubdiv")
	public boolean isSubdivLinkedToAdvesoCached(int clientSubdivId, int businessCompanyId, Date jobItemDateIn) {
		boolean linked = getValueByScope(Scope.SUBDIV, clientSubdivId, businessCompanyId);
		if (linked) {
			Date date = linkToAdvesoAfterDate.getValueByScope(Scope.SUBDIV, clientSubdivId, businessCompanyId);
			if (date.equals(jobItemDateIn) || date.before(jobItemDateIn))
				return true;
		}
		return false;
	}

	@Cacheable(value = "cachedIsAdvesoActiveOnSubdiv")
	public boolean isAdvesoActiveOnSubdivCached(int clientSubdivId, int businessCompanyId) {
		return getValueByScope(Scope.SUBDIV, clientSubdivId, businessCompanyId);
	}

	public void evictAllCache() {
		cacheManager.getCache("cachedIsAdvesoActiveOnSubdiv").clear();
	}

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.LINK_TO_ADVESO;
	}
}