package org.trescal.cwms.core.system.entity.emailcontent;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.InstrumentValidationIssue;
import org.trescal.cwms.core.instrument.entity.instrumentvalidationissue.db.InstrumentValidationIssueService;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.job.dto.ClientFeedbackRequestDTO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequest;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.db.ScheduledQuotationRequestService;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.recall.entity.recalldetail.db.RecallDetailService;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;
import org.trescal.cwms.core.system.entity.emailcontent.autoservice.EmailContent_ChaseItems;
import org.trescal.cwms.core.system.entity.emailcontent.autoservice.EmailContent_ChaseSummary;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_ContactReminder;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_ContractReviewFeedback;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_DelayedCertificate;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_FeedbackConfirmation;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_FeedbackConfirmation.EmailType;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_HireEnquiryConfirmation;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_ScheduleConfirmation;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_ScheduledQuoteRequestFail;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_ScheduledQuoteRequestSuccess;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_UnsignedCertificateNotification;
import org.trescal.cwms.core.system.entity.emailcontent.recall.EmailContent_Recall;
import org.trescal.cwms.core.system.entity.emailcontent.web.EmailContent_ClientCollection;
import org.trescal.cwms.core.system.entity.emailcontent.web.EmailContent_Collection;
import org.trescal.cwms.core.system.entity.emailcontent.web.EmailContent_InstrumentValidation;
import org.trescal.cwms.core.system.entity.emailcontent.web.EmailContent_QuoteRequest;
import org.trescal.cwms.core.tools.autoservice.AutoServiceSummary;
import org.trescal.cwms.core.tools.autoservice.AutoServiceType;

/**
 * Admin-only component that allows us to preview emails
 * @author galen
 *
 */
@Component
public class EmailPreviewer {
	@Autowired
	private ContactService contactService;
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private HireService hireService;
	@Autowired
	private InstrumentValidationIssueService iviService;
	@Autowired
	private JobService jobService;
	@Autowired
	private QuotationService quotationService; 
	@Autowired
	private RecallDetailService recallDetailService; 
	@Autowired
	private ScheduledQuotationRequestService sqrService;
	@Autowired
	private ScheduleService scheduleService; 

	@Autowired
	private EmailContent_ChaseItems ecChaseItems;
	@Autowired
	private EmailContent_ChaseSummary ecChaseSummary;
	@Autowired
	private EmailContent_Collection ecCollection;
	@Autowired
	private EmailContent_ClientCollection ecClientCollection;
	@Autowired
	private EmailContent_ContactReminder ecContactReminder;
	@Autowired
	private EmailContent_ContractReviewFeedback ecContractReviewFeedback;
	@Autowired
	private EmailContent_DelayedCertificate ecDelayedCertificate;
	@Autowired
	private EmailContent_FeedbackConfirmation ecFeedbackConfirmation;
	@Autowired
	private EmailContent_HireEnquiryConfirmation ecHireEnquiryConfirmation;
	@Autowired
	private EmailContent_InstrumentValidation ecInstrumentValidation;
	@Autowired
	private EmailContent_QuoteRequest ecQuoteRequest;
	@Autowired
	private EmailContent_Recall ecRecall;
	@Autowired
	private EmailContent_ScheduleConfirmation ecScheduleConfirmation;
	@Autowired
	private EmailContent_ScheduledQuoteRequestFail ecScheduledQuoteRequestFail;
	@Autowired
	private EmailContent_ScheduledQuoteRequestSuccess ecScheduledQuoteRequestSuccess;
	@Autowired
	private EmailContent_UnsignedCertificateNotification ecUnsignedCertificateNotification;
		
	public EmailContentDto getPreview(EmailContentType type, Integer entityId, Locale locale, Boolean thymeleaf) {
		if (type == null) throw new IllegalArgumentException("type was null");
		if (locale == null) throw new IllegalArgumentException("type was null");
		if (thymeleaf == null) throw new IllegalArgumentException("type was null");
		
		EmailContentDto result = null;
		switch (type) {
		case AUTOSERVICE_CHASE_CLIENT_ITEMS:
		{
			Job job = this.jobService.get(entityId);
			if (job == null) throw new RuntimeException("Job not found for ID "+entityId);
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecChaseItems.getContent(job.getCon(), job.getItems(), 1, 3, locale, AutoServiceType.CLIENT);
			break;
		}
		case AUTOSERVICE_CHASE_CLIENT_SUMMARY:
		{
			Job job = this.jobService.get(entityId);
			if (job == null) throw new RuntimeException("Job not found for ID "+entityId);
			if (job.getItems().isEmpty()) throw new RuntimeException("Job must have at least one item");
			AutoServiceSummary summary = mockAutoServiceSummary(job);
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecChaseSummary.getContent(summary, locale, AutoServiceType.CLIENT);
			break;
		}
		case AUTOSERVICE_CHASE_SUPPLIER_ITEMS:
		{
			Job job = this.jobService.get(entityId);
			if (job == null) throw new RuntimeException("Job not found for ID "+entityId);
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecChaseItems.getContent(job.getCon(), job.getItems(), 1, 3, locale, AutoServiceType.SUPPLIER);
			break;
		}
		case AUTOSERVICE_CHASE_SUPPLIER_SUMMARY:
		{
			Job job = this.jobService.get(entityId);
			if (job == null) throw new RuntimeException("Job not found for ID "+entityId);
			if (job.getItems().isEmpty()) throw new RuntimeException("Job must have at least one item");
			AutoServiceSummary summary = mockAutoServiceSummary(job);
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecChaseSummary.getContent(summary, locale, AutoServiceType.SUPPLIER);
			break;
		}
		case CORE_CONTACT_REMINDER:
		{	
			Contact con = this.contactService.get(entityId);
			if (con == null) throw new RuntimeException("Contact not found for ID "+entityId);
			String newPassword = "Sample_Password";
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecContactReminder.getContent(con, newPassword, locale);
			break;
		}
		case CORE_CONTRACT_REVIEW_FEEDBACK:
		{
			Job job = this.jobService.get(entityId);
			if (job == null) throw new RuntimeException("Job not found for ID "+entityId);
			List<ClientFeedbackRequestDTO> cfritems = mockCfrItems(job);
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecContractReviewFeedback.getContent(job, cfritems, locale);
			break;
		}
		case CORE_DELAYED_CERTIFICATE:
		{
			Certificate cert = this.certificateService.findCertificate(entityId);
			if (cert == null) throw new RuntimeException("Certificate not found for ID "+entityId);
			if (cert.getCal() == null) throw new RuntimeException("Certificate not linked to a calibration, choose another ID");
			if (cert.getCal().getCompletedBy() == null) throw new RuntimeException("Linked calibration has no completedBy, choose another ID");
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecDelayedCertificate.getContent(cert, locale);
			break;
		}
		case CORE_FEEDBACK_CONFIRMATION:
		{
			Contact contact = this.contactService.get(entityId);
			if (contact == null) throw new RuntimeException("Contact not found for ID "+entityId);
			String build = "12345";
			String requestURI = "Sample request URI";
			String userAgent = "Sample user-agent";
			String description = "Sample description";
			String title = "Sample title";

			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecFeedbackConfirmation.getContent(contact, description, title, build, requestURI, userAgent, locale, EmailType.INTERNAL);
			break;
		}
		case CORE_HIRE_ENQUIRY_CONFIRMATION:
		{
			Hire hire = this.hireService.get(entityId);
			if (hire == null) throw new RuntimeException("Hire not found for ID "+entityId);
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecHireEnquiryConfirmation.getContent(hire, locale);
		}
		break;
		case RECALL_MESSAGE:
		{
			RecallDetail rdetail = this.recallDetailService.get(entityId);
			if (rdetail == null) throw new RuntimeException("RecallDetail not found for ID "+entityId);
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecRecall.getContent(rdetail, locale);
		}
		break;
		case CORE_SCHEDULE_CONFIRMATION:
		{
			Schedule schedule = this.scheduleService.get(entityId);
			if (schedule == null) throw new RuntimeException("Schedule not found for ID "+entityId);
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecScheduleConfirmation.getContent(schedule, locale);
		}
		break;
		case CORE_SCHEDULED_QUOTE_REQUEST_FAIL:	
		{
			ScheduledQuotationRequest sqr = this.sqrService.get(entityId);
			if (sqr == null) throw new RuntimeException("ScheduledQuotationRequest not found for ID "+entityId);
			Exception exception = new RuntimeException("Sample Exception");
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecScheduledQuoteRequestFail.getContent(sqr, exception, locale);
		}
		break;
		case CORE_SCHEDULED_QUOTE_REQUEST_SUCCESS:	
		{
			ScheduledQuotationRequest sqr = this.sqrService.get(entityId);
			if (sqr == null) throw new RuntimeException("ScheduledQuotationRequest not found for ID "+entityId);
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecScheduledQuoteRequestSuccess.getContent(sqr, locale);
		}
		break;
		case CORE_UNSIGNED_CERTIFICATE:
		{
			Certificate cert = this.certificateService.findCertificate(entityId);
			if (cert == null) throw new RuntimeException("Certificate not found for ID "+entityId);
			if (cert.getCal() == null) throw new RuntimeException("Certificate not linked to a calibration, choose another ID");
			if (cert.getCal().getCompletedBy() == null) throw new RuntimeException("Linked calibration has no completedBy, choose another ID");
			boolean fileExists = entityId % 2 == 0;	// true if even, false if odd - for testing purposes
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecUnsignedCertificateNotification.getContent(cert, fileExists, locale);
		}
		break;
		case PORTAL_CLIENT_COLLECTION:
		{
			Schedule schedule = this.scheduleService.get(entityId);
			if (schedule == null) throw new RuntimeException("No schedule found for ID "+entityId);
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecClientCollection.getContent(schedule, locale);
		}
		break;
		case PORTAL_COLLECTION:
		{
			Schedule schedule = this.scheduleService.get(entityId);
			if (schedule == null) throw new RuntimeException("No schedule found for ID "+entityId);
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecCollection.getContent(schedule, locale);
		}
		break;
		case PORTAL_CLIENT_QUOTE_REQUEST:
		{
			Quotation quotation = this.quotationService.get(entityId);
			if (quotation == null) throw new RuntimeException("No Quotation found for ID "+entityId);
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecQuoteRequest.getContent(quotation, locale, EmailContent_QuoteRequest.EmailType.CLIENT);
		}
		break;
		case PORTAL_INSTRUMENT_VALIDATION:
		{
			InstrumentValidationIssue issue = this.iviService.findInstrumentValidationIssue(entityId);
			if (issue == null) throw new RuntimeException("No InstrumentValidationIssue found for ID "+entityId);
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecInstrumentValidation.getContent(issue, locale);
		}
		break;
		case PORTAL_FEEDBACK:
		{
			Contact contact = this.contactService.get(entityId);
			if (contact == null) throw new RuntimeException("Contact not found for ID "+entityId);
			String build = "Sample build 12345";
			String requestURI = "Sample request URI";
			String userAgent = "Sample user-agent";
			String description = "Sample description";
			String title = "Sample title";
			
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecFeedbackConfirmation.getContent(contact, description, title, build, requestURI, userAgent, locale, EmailType.CLIENT);
			break;
		}
		case PORTAL_QUOTE_REQUEST:
		{
			Quotation quotation = this.quotationService.get(entityId);
			if (quotation == null) throw new RuntimeException("No Quotation found for ID "+entityId);
			if (!thymeleaf) throw new IllegalArgumentException("Only Thymeleaf is supported for this email type");
			result = this.ecQuoteRequest.getContent(quotation, locale, EmailContent_QuoteRequest.EmailType.INTERNAL);
		}
		break;
		default:
			throw new UnsupportedOperationException("Unsupported email content type "+type);
		}
		return result;
	}
	
	private AutoServiceSummary mockAutoServiceSummary(Job job) {
		AutoServiceSummary summary = new AutoServiceSummary(3);
		JobItem jobItem = job.getItems().iterator().next();
		Contact contact = job.getCon();
		
		summary.addChased(contact, 1, jobItem);
		summary.addFailChaseLimit(contact, jobItem);
		summary.addFailDisabledChase(contact, jobItem);
		summary.addFailNoChasing(contact, job.getItems());
		summary.addFailNoEmail(contact, job.getItems());
		summary.incrementEmailsSent();
		
		return summary;
	}

			
	private List<ClientFeedbackRequestDTO> mockCfrItems(Job job) {
		List<ClientFeedbackRequestDTO> result = new ArrayList<>();
		boolean toggle = true;
		for (JobItem ji : job.getItems()) {
			String comment = "Sample remarks for item "+ji.getItemNo();
			result.add(new ClientFeedbackRequestDTO(ji.getJobItemId(), toggle, toggle, toggle, toggle, toggle, comment));
			toggle = !toggle;
		}
		return result;
	}
}
