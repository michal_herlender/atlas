package org.trescal.cwms.core.system.entity.accreditationbodyresource.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.system.entity.accreditationbody.AccreditationBody;
import org.trescal.cwms.core.system.entity.accreditationbodyresource.AccreditationBodyResource;
import org.trescal.cwms.core.system.entity.accreditationbodyresource.AccreditationBodyResourceType;

public interface AccreditationBodyResourceService extends
		BaseService<AccreditationBodyResource, Integer> {

	AccreditationBodyResource getForBodyAndType(AccreditationBody body, AccreditationBodyResourceType type);
}
