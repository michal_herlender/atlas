package org.trescal.cwms.core.system.entity.emailtemplate;

import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

/**
 * Entity class to store email templates
 * By default, allocated by business company, can be further restricted by business subdivision
 * @author Galen
 * 
 */
@Entity
@Table(name = "emailtemplate")
public class EmailTemplate extends Allocated<Company> {
	private Component component;
	private int id;
	private Locale locale;
	private Subdiv subdiv;
	private String subject;
	private String template;
	
	@NotNull
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "component", nullable = false, unique = false)
	public Component getComponent() {
		return component;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return id;
	}
	
	@NotNull
	@Column(name = "locale", nullable = false)
	public Locale getLocale() {
		return locale;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subdivid")
	public Subdiv getSubdiv() {
		return subdiv;
	}

	@NotNull
	@Column(name="subject", nullable=false, columnDefinition="nvarchar(200)")
	public String getSubject() {
		return subject;
	}
	
	@NotNull
	@Column(name="template", nullable=false, columnDefinition="nvarchar(2000)")
	public String getTemplate() {
		return template;
	}
	
	public void setComponent(Component component) {
		this.component = component;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	public void setSubdiv(Subdiv subdiv) {
		this.subdiv = subdiv;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	
}
