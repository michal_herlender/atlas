package org.trescal.cwms.core.system.entity.userinstructiontype.db;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactDWRService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;
import org.trescal.cwms.core.login.entity.userpreferences.db.UserPreferencesService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.userinstructiontype.UserInstructionType;

@Service("UserInstructionTypeService")
public class UserInstructionTypeServiceImpl implements UserInstructionTypeService
{
	@Autowired
	private ContactService contactServ;
	@Autowired
	private ContactDWRService contactDWRServ;
	@Autowired
	private SessionUtilsService sessionServ;
	@Autowired
	private UserPreferencesService upServ;
	@Autowired
	private UserInstructionTypeDao userInstructionTypeDao;

	public void deleteUserInstructionType(UserInstructionType uit)
	{
		this.userInstructionTypeDao.remove(uit);
	}

	public ResultWrapper deleteUserInstructionTypeDWR(Integer userinstructiontypeid, HttpServletRequest req)
	{
		// find the user instruction type
		UserInstructionType uit = this.userInstructionTypeDao.find(userinstructiontypeid);
		// check user instruction type
		if (uit != null)
		{
			// get the contact from user instruction type
			Integer personid = uit.getUserPreference().getContact().getPersonid();
			// delete the user instruction type
			this.userInstructionTypeDao.remove(uit);
			// get the current contact
			Contact currentContact = this.sessionServ.getCurrentContact();
			// contact not null?
			if (currentContact != null)
			{
				// get contact using the personid from user preferences
				Contact con = this.contactServ.get(personid);
				// current contact equal to the contact being edited?
				if (currentContact.getPersonid() == con.getPersonid())
				{
					// must still be re-loaded to get new details
					Contact newsesscon = this.contactDWRServ.findDWRContact(con.getPersonid());
					this.sessionServ.setCurrentContact(req, newsesscon);
				}
			}
			// return successful result wrapper
			return new ResultWrapper(true, "", null, null);
		}
		else
		{
			// return wrapper
			return new ResultWrapper(false, "The user instruction type could not be found", null, null);
		}
	}

	public UserInstructionType findUserInstructionType(int instructiontypeid)
	{
		return this.userInstructionTypeDao.find(instructiontypeid);
	}

	public List<UserInstructionType> getAllUserInstructionTypes()
	{
		return this.userInstructionTypeDao.findAll();
	}

	public void insertUserInstructionType(UserInstructionType uit)
	{
		this.userInstructionTypeDao.persist(uit);
	}

	public ResultWrapper insertUserInstructionTypeDWR(Integer userprefsid, InstructionType instructionType, HttpServletRequest req)
	{
		// get preferences for user
		UserPreferences userprefs = this.upServ.get(userprefsid);
		// check preferences
		if (userprefs != null)
		{
			// check instruction type
			if (instructionType != null)
			{
				// create new user instruction type object
				UserInstructionType uit = new UserInstructionType();
				uit.setUserPreference(userprefs);
				uit.setInstructionType(instructionType);
				// insert new user instruction type
				this.userInstructionTypeDao.persist(uit);
				// get the current contact
				Contact currentContact = this.sessionServ.getCurrentContact();
				// contact not null?
				if (currentContact != null)
				{
					// current contact equal to the contact being edited?
					if (currentContact.getPersonid() == userprefs.getContact().getPersonid())
					{
						// must still be re-loaded to get new details
						Contact con = this.contactDWRServ.findDWRContact(userprefs.getContact().getPersonid());
						this.sessionServ.setCurrentContact(req, con);
					}
				}
				// return successful result wrapper
				return new ResultWrapper(true, "", uit, null);
			}
			else
			{
				// return wrapper
				return new ResultWrapper(false, "The instruction type could not be found", null, null);
			}
		}
		else
		{
			// return wrapper
			return new ResultWrapper(false, "The user preferences for this user could not be found", null, null);
		}
	}

	public void setContactServ(ContactService contactServ)
	{
		this.contactServ = contactServ;
	}

	public void setSessionServ(SessionUtilsService sessionServ)
	{
		this.sessionServ = sessionServ;
	}

	public void setUpServ(UserPreferencesService upServ)
	{
		this.upServ = upServ;
	}

	public void setUserInstructionTypeDao(UserInstructionTypeDao userInstructionTypeDao)
	{
		this.userInstructionTypeDao = userInstructionTypeDao;
	}

	public void updateUserInstructionType(UserInstructionType uit)
	{
		this.userInstructionTypeDao.update(uit);
	}

}
