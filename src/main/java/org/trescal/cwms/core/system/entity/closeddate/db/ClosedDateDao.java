package org.trescal.cwms.core.system.entity.closeddate.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.closeddate.ClosedDate;

public interface ClosedDateDao extends BaseDao<ClosedDate, Integer> {
	
	List<ClosedDate> getFutureClosedDatesForCompany(int coid);
}