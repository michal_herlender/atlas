package org.trescal.cwms.core.system.entity.alligatorsettings;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.contact.Contact;

@Entity
@Table(name="alligatorsettingscontact", uniqueConstraints={@UniqueConstraint(columnNames="personid")})
public class AlligatorSettingsContact extends AlligatorSettingsUsage {
	private Contact contact;
	
	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="personid", nullable=false)
	public Contact getContact() {
		return contact;
	}
	
	public void setContact(Contact contact) {
		this.contact = contact;
	}
}
