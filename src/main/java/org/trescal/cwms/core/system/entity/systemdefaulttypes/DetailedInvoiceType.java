package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class DetailedInvoiceType extends SystemDefaultBooleanType {
	
	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.DETAILED_INVOICE_TYPE;
	}
}