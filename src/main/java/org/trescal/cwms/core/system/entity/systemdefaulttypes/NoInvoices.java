package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class NoInvoices extends SystemDefaultBooleanType {
	
	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.NO_INVOICES;
	}
}