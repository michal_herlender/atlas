package org.trescal.cwms.core.system.entity.printfilerule;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
//import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.Size;
import org.trescal.cwms.core.system.entity.papertype.PaperType;
import org.trescal.cwms.core.system.entity.printer.Printer;

/**
 * @author jamiev
 */
@Entity
@Table(name = "printfilerule")
public class PrintFileRule
{
	private Integer copies;
	private String fileNameLike;
	private PaperType firstPagePaperType;
	private int id;
	private Printer printer;
	private PaperType restPagePaperType;

	@Column(name = "copies")
	public Integer getCopies()
	{
		return this.copies;
	}

	@NotNull
	//@Length(min = 1, max = 100)
	@Size(min = 1, max = 100)
	@Column(name = "filenamelike", nullable = false, length = 100)
	public String getFileNameLike()
	{
		return this.fileNameLike;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "firstpagepapertype", nullable = true)
	public PaperType getFirstPagePaperType()
	{
		return this.firstPagePaperType;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "printerid")
	public Printer getPrinter()
	{
		return this.printer;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "restpagepapertype", nullable = true)
	public PaperType getRestPagePaperType()
	{
		return this.restPagePaperType;
	}

	public void setCopies(Integer copies)
	{
		this.copies = copies;
	}

	public void setFileNameLike(String fileNameLike)
	{
		this.fileNameLike = fileNameLike;
	}

	public void setFirstPagePaperType(PaperType firstPagePaperType)
	{
		this.firstPagePaperType = firstPagePaperType;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setPrinter(Printer printer)
	{
		this.printer = printer;
	}

	public void setRestPagePaperType(PaperType restPagePaperType)
	{
		this.restPagePaperType = restPagePaperType;
	}
}