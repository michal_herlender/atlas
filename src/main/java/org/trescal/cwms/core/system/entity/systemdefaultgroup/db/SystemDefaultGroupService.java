package org.trescal.cwms.core.system.entity.systemdefaultgroup.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.corole.Corole;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefaultgroup.SystemDefaultGroup;
import org.trescal.cwms.core.system.enums.Scope;

public interface SystemDefaultGroupService
{
	void deleteSystemDefaultGroup(SystemDefaultGroup systemdefaultgroup);

	SystemDefaultGroup findSystemDefaultGroup(int id);

	/**
	 * Returns a list of {@link SystemDefaultGroup} entities that have
	 * {@link SystemDefault}s that are available to the given {@link Corole}
	 * and at the given scope.
	 * 
	 * @param corole the {@link Corole}.
	 * @param scope the {@link Scope}.
	 * @return list of {@link SystemDefaultGroup}.
	 */
	List<SystemDefaultGroup> getAllSystemDefaultByCorole(String corole, Scope scope);

	List<SystemDefaultGroup> getAllSystemDefaultGroups();

	void insertSystemDefaultGroup(SystemDefaultGroup systemdefaultgroup);

	void saveOrUpdateSystemDefaultGroup(SystemDefaultGroup systemdefaultgroup);

	void updateSystemDefaultGroup(SystemDefaultGroup systemdefaultgroup);
}