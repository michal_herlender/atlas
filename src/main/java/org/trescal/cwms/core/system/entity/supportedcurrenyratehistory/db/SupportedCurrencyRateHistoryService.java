package org.trescal.cwms.core.system.entity.supportedcurrenyratehistory.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.supportedcurrenyratehistory.SupportedCurrencyRateHistory;

public interface SupportedCurrencyRateHistoryService
{
	SupportedCurrencyRateHistory findSupportedCurrencyRateHistory(int id);
	void insertSupportedCurrencyRateHistory(SupportedCurrencyRateHistory supportedcurrencyratehistory);
	void updateSupportedCurrencyRateHistory(SupportedCurrencyRateHistory supportedcurrencyratehistory);
	List<SupportedCurrencyRateHistory> getAllSupportedCurrencyRateHistorys();
	void deleteSupportedCurrencyRateHistory(SupportedCurrencyRateHistory supportedcurrencyratehistory);
	void saveOrUpdateSupportedCurrencyRateHistory(SupportedCurrencyRateHistory supportedcurrencyratehistory);
}