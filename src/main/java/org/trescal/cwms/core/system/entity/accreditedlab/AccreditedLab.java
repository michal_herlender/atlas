package org.trescal.cwms.core.system.entity.accreditedlab;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.accreditationbody.AccreditationBody;
import org.trescal.cwms.spring.model.KeyValue;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "accreditedlab")
public class AccreditedLab extends Allocated<Subdiv> {
    private String description;
    private int id;
    private String labNo;
    private Boolean watermarkForLab;
    private AccreditationBody accreditationBody;

    public AccreditedLab() {
        this.watermarkForLab = Boolean.FALSE;
    }

    @NotNull
    @Length(min = 1, max = 50)
    @Column(name = "description", length = 50)
    public String getDescription()
	{
		return this.description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	/* Changed uniqueness from true to false 2016-04-29 to accommodate subdivs with same lab no */
    @NotNull
    @Length(min = 1, max = 20)
    @Column(name = "labno", length = 20)
    public String getLabNo()
	{
		return this.labNo;
	}
	
	@Transient
	public KeyValue<Integer, String> dto() {
        StringBuilder value = new StringBuilder();
        if (accreditationBody != null) {
            value.append(accreditationBody.getShortName());
            value.append(" : ");
        }
        value.append(labNo);
        value.append(" : ");
        value.append(description);

        return new KeyValue<>(id, value.toString());
    }
	
	@Column(name = "watermark", columnDefinition="bit default 0")
	public Boolean getWatermarkForLab()
	{
		return this.watermarkForLab;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "accreditationbodyid", nullable=false)
	public AccreditationBody getAccreditationBody() {
		return accreditationBody;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setLabNo(String labNo)
	{
		this.labNo = labNo;
	}

	public void setWatermarkForLab(Boolean watermarkForLab)
	{
		this.watermarkForLab = watermarkForLab;
	}

	public void setAccreditationBody(AccreditationBody accreditationBody) {
		this.accreditationBody = accreditationBody;
	}
}