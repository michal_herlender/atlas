package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.enums.Scope;

public class SystemDefaultTypeDTO
{
	// Data from SystemDefaultType
	private SystemDefaultNames defaultName;		// Enum representing system default
	private String localeName;					// Translated name of system default
	private SystemDefault systemDefault;		// Entity representing system default ype
	private boolean discreteType;				// Indicates whether seleted from discrete list of values
	private boolean multiChoice;				// Indicates if the system default is a multichoice of possible values
	private List<String> values;				// List of values for selection
	// Data from SystemDefault and/or SystemDefaultApplication
	private String value;						// Current value of system default
	private Integer applicationId;				// Typically only set when value comes from current scope
	private Scope scope;						// Indication of where value set at
	
	public SystemDefaultTypeDTO(SystemDefaultType<?> type, Locale locale, String savedValue, Integer applicationId, Scope scope) {
		defaultName = type.defaultName();
		localeName = type.getLocaleName(locale);
		systemDefault = type.getSystemDefault();
		discreteType = type.isDiscreteType();
		multiChoice = type.isMultiChoice();
		values = type.getValues();
		value = type.getValue(savedValue);
		this.applicationId = applicationId;
		this.scope = scope;
	}
	
	public SystemDefaultNames getDefaultName() {
		return defaultName;
	}
	
	public String getLocaleName() {
		return localeName;
	}
	
	public Scope getScope() {
		return scope;
	}
	
	public SystemDefault getSystemDefault() {
		return systemDefault;
	}
	
	public boolean isDiscreteType() {
		return discreteType;
	}
	
	public List<String> getValues() {
		return values;
	}
	
	public boolean isMultiChoice() {
		return multiChoice;
	}

	public String getValue() {
		return value;
	}
	
	public Integer getApplicationId() {
		return applicationId;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}
}