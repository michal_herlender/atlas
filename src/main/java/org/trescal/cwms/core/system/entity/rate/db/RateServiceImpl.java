package org.trescal.cwms.core.system.entity.rate.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.rate.Rate;

public class RateServiceImpl implements RateService
{
	RateDao RateDao;

	public Rate findRate(int id)
	{
		return RateDao.find(id);
	}

	public void insertRate(Rate Rate)
	{
		RateDao.persist(Rate);
	}

	public void updateRate(Rate Rate)
	{
		RateDao.update(Rate);
	}

	public List<Rate> getAllRates()
	{
		return RateDao.findAll();
	}

	public void setRateDao(RateDao RateDao)
	{
		this.RateDao = RateDao;
	}

	public void deleteRate(Rate rate)
	{
		this.RateDao.remove(rate);
	}

	public void saveOrUpdateRate(Rate rate)
	{
		this.RateDao.saveOrUpdate(rate);
	}
}