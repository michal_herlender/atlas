package org.trescal.cwms.core.system.entity.labeltemplate.db;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.labeltemplate.LabelTemplate;
import org.trescal.cwms.core.system.entity.labeltemplate.LabelTemplateType;

@Repository("LabelTemplateDao")
public class LabelTemplateDaoImpl extends BaseDaoImpl<LabelTemplate, Integer> implements LabelTemplateDao {
	@Override
	protected Class<LabelTemplate> getEntity() {
		// TODO Auto-generated method stub
		return LabelTemplate.class;
	}
	
	@Override
	public LabelTemplate getLabelTemplateById(int id) {
		Criteria crit = this.getSession().createCriteria(LabelTemplate.class);
		crit.add(Restrictions.idEq(id));
		return (LabelTemplate)crit.uniqueResult();
	}

	@Override
	public LabelTemplate getLabelTemplateByLabelTemplateType(LabelTemplateType labeltemplatetype) {
		Criteria crit = this.getSession().createCriteria(LabelTemplate.class);
		crit.add(Restrictions.eq("labelTemplateType", labeltemplatetype));
		return (LabelTemplate)crit.uniqueResult();
	}
}
