package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class LinkToAdvesoFromDate extends SystemDefaultDateType {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.LINK_TO_ADVESO_FROM_DATE;
	}
	
}
