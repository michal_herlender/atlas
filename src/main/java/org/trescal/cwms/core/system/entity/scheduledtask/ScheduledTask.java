package org.trescal.cwms.core.system.entity.scheduledtask;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;


@Entity
@Table(name = "scheduledtask", uniqueConstraints = {@UniqueConstraint(columnNames = {"classname", "methodname"})})
public class ScheduledTask {
    private boolean active;
    private String className;
    private String description;
    private int id;
    private String lastRunMessage;
    private boolean lastRunSuccess;
    private Date lastRunTime;
    private String methodName;
    private String quartzJobName;
    private String taskName;

    @NotNull
    @Column(name = "classname", length = 250, nullable = false)
    @Length(min = 1, max = 250)
    public String getClassName() {
        return this.className;
    }

    @Column(name = "description", length = 250)
    @Length(max = 250)
    public String getDescription() {
        return this.description;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    @Column(name = "lastrunmessage", length = 300)
    @Length(max = 300)
    public String getLastRunMessage() {
        return this.lastRunMessage;
    }

    @Column(name = "lastruntime")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getLastRunTime() {
        return this.lastRunTime;
    }

    @NotNull
    @Column(name = "methodname", length = 100, nullable = false)
    @Length(min = 1, max = 100)
    public String getMethodName() {
        return this.methodName;
    }

	@NotNull
	@Column(name = "quartzjobname", length = 100, nullable = false)
	@Length(min = 1, max = 100)
	public String getQuartzJobName()
	{
		return this.quartzJobName;
	}

	@NotNull
	@Column(name = "taskname", length = 50, nullable = false)
	@Length(min = 1, max = 50)
	public String getTaskName()
	{
		return this.taskName;
	}

	@Column(name = "active", nullable = false, columnDefinition="tinyint")
	public boolean isActive()
	{
		return this.active;
	}

	@Column(name = "lastrunsuccess", columnDefinition="tinyint")
	public boolean isLastRunSuccess()
	{
		return this.lastRunSuccess;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public void setClassName(String className)
	{
		this.className = className;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setLastRunMessage(String lastRunMessage)
	{
		this.lastRunMessage = lastRunMessage;
	}

	public void setLastRunSuccess(boolean lastRunSuccess)
	{
		this.lastRunSuccess = lastRunSuccess;
	}

	public void setLastRunTime(Date lastRunTime)
	{
		this.lastRunTime = lastRunTime;
	}

	public void setMethodName(String methodName)
	{
		this.methodName = methodName;
	}

	public void setQuartzJobName(String quartzJobName)
	{
		this.quartzJobName = quartzJobName;
	}

	public void setTaskName(String taskName)
	{
		this.taskName = taskName;
	}
}