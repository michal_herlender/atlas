package org.trescal.cwms.core.system.entity.alligatorlabeltemplate.enums;

public enum AlligatorTemplateType {
	ACCREDITED,
	ACCREDITED_SMALL,
	STANDARD,
	STANDARD_SMALL;
}
