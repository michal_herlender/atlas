package org.trescal.cwms.core.system.entity.status;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.entity.basestatus.BaseStatus;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "basestatus")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("simple")
public abstract class Status extends BaseStatus
{
	/**
	 * Denotes if this status represent's an accepted entity - i.e. a
	 * {@link Quotation} that has been accepted.
	 */
	private Boolean accepted;

	/**
	 * The status that the component should be set to following issue (true
	 * should be unique)
	 */
	private Boolean followingIssue;

	/**
	 * The status that a component should be set to following it's receipt
	 */
	private Boolean followingReceipt;

	/**
	 * Indicates if this status is one that has been issued - or occurs when a
	 * {@link Quotation} has been issued.
	 */
	private Boolean issued;

	/**
	 * Denotes if this status represent's a rejected entity - i.e. a
	 * {@link Hire} that has been rejected.
	 */
	private Boolean rejected;

	/**
	 * Denotes if this status requires attention from a member of staff.
	 */
	private Boolean requiringAttention;

	/**
	 * Translation for name 
	 */
	private Set<Translation> nametranslations;

	/**
	 * Translation for description 
	 */
	private Set<Translation> descriptiontranslations;
	
	/**
	 * Sets default Boolean values to false for unit/integration testing 
	 */
	public Status() {
		super();
		accepted = false;
		followingIssue = false;
		followingReceipt = false;
		issued = false;
		rejected = false;
		requiringAttention = false;
	}
	
	@Column(name = "accepted", nullable = false, columnDefinition="bit")
	//@Type(type = "boolean")
	public Boolean getAccepted()
	{
		return this.accepted;
	}

	@Column(name = "followingissue", nullable = false, columnDefinition="bit")
	public Boolean getFollowingIssue()
	{
		return this.followingIssue;
	}

	@Column(name = "followingreceipt", nullable = false, columnDefinition="bit")
	public Boolean getFollowingReceipt()
	{
		return this.followingReceipt;
	}

	@Column(name = "issued", nullable = false, columnDefinition="bit")
	public Boolean getIssued()
	{
		return this.issued;
	}

	@Column(name = "rejected", nullable = false, columnDefinition="bit")
	public Boolean getRejected()
	{
		return this.rejected;
	}

	@Column(name = "requiringattention", nullable = false, columnDefinition="bit")
	public Boolean getRequiringAttention()
	{
		return this.requiringAttention;
	}

	public void setAccepted(Boolean accepted)
	{
		this.accepted = accepted;
	}

	public void setFollowingIssue(Boolean followingIssue)
	{
		this.followingIssue = followingIssue;
	}

	public void setFollowingReceipt(Boolean followingReceipt)
	{
		this.followingReceipt = followingReceipt;
	}

	public void setIssued(Boolean issued)
	{
		this.issued = issued;
	}

	public void setRejected(Boolean rejected)
	{
		this.rejected = rejected;
	}

	public void setRequiringAttention(Boolean requiringAttention)
	{
		this.requiringAttention = requiringAttention;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="basestatusnametranslation", joinColumns=@JoinColumn(name="statusid"))
	public Set<Translation> getNametranslations() {
		return nametranslations;
	}

	public void setNametranslations(Set<Translation> nametranslations) {
		this.nametranslations = nametranslations;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="basestatusdescriptiontranslation", joinColumns=@JoinColumn(name="statusid"))
	public Set<Translation> getDescriptiontranslations() {
		return descriptiontranslations;
	}

	public void setDescriptiontranslations(Set<Translation> descriptiontranslations) {
		this.descriptiontranslations = descriptiontranslations;
	}
}
