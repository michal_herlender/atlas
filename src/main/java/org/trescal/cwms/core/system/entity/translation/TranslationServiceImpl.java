package org.trescal.cwms.core.system.entity.translation;

import org.springframework.beans.factory.annotation.Autowired;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

import java.util.Locale;
import java.util.Set;


public class TranslationServiceImpl implements TranslationService {

    private final SupportedLocaleService locServ;

    @Autowired
    public TranslationServiceImpl(SupportedLocaleService locServ) {
        this.locServ = locServ;
    }


    private String findTranslation(Set<Translation> translations, Locale locale) {
        if (translations == null) return "No Translations Loaded";

        for (Translation translation : translations) {
            if (translation.getLocale().equals(locale)) {
                return translation.getTranslation();
            }
        }
		
		return null;
	}
	
	public String getCorrectTranslation(Set<Translation> translations, Locale userLocale){
		
		Locale fallBackLocale = locServ.getPrimaryLocale();
		
		String translation = findTranslation(translations, userLocale);
		
	    if(translation == null){
	    	translation = findTranslation(translations, fallBackLocale);
	    }
		
		if(translation == null){
			translation = "No Valid Translation";
		}
		
		return translation;
			
	}

}
