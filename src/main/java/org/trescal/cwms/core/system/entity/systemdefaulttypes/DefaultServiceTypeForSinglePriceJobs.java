package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;

@Component
public class DefaultServiceTypeForSinglePriceJobs extends SystemDefaultType<ServiceType> {

	@Autowired
	private TranslationService translationService;
	@Autowired
	private ServiceTypeService serviceTypeService;

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.DEFAULT_SERVICETYPE_FORSINGLEPRICE_JOBS;
	}

	@Override
	public boolean isDiscreteType() {
		return true;
	}

	@Override
	public List<String> getValues() {
		Locale locale = LocaleContextHolder.getLocale();
		List<ServiceType> serviceTypes = serviceTypeService.getByJobType(JobType.SINGLE_PRICE);
		return serviceTypes.stream()
				.map(st -> translationService.getCorrectTranslation(st.getShortnameTranslation(), locale))
				.collect(Collectors.toList());
	}

	@Override
	public ServiceType parseValue(String value) {
		Locale locale = LocaleContextHolder.getLocale();
		ServiceType st = serviceTypeService.getByShortNameAndLocal(value, locale);
		if(st==null) {
			// fallback to en_GB
			st = serviceTypeService.getByShortNameAndLocal(value, new Locale("en_GB"));
		}
		return st;
	}
	
	/**
	 * they always saved in English, so we have to translate
	 */
	@Override
	public String getValue(String savedValue) {
		Locale locale = LocaleContextHolder.getLocale();
		ServiceType st = serviceTypeService.getByShortNameAndLocal(savedValue,  new Locale("en_GB"));
		return translationService.getCorrectTranslation(st.getShortnameTranslation(), locale);
	}

	@Override
	public boolean isMultiChoice() {
		return false;
	}


}
