package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.EnumSet;
import java.util.Locale;

/**
 * Defines the different system defaults. 2017-06-26 GB : Inactivating unused
 * system defaults 30-36 which are now not used for Plantillas. Renamed them to
 * UNUSED_xx preserving order in order to not affect other system defaults,
 * because ordinal int mapping is used in database. These unused system defaults
 * can be re-activated / renamed as new system defaults if needed.
 */
public enum SystemDefaultNames {
	/*
	 * the order of the defaults is important (ordinal int mapping), do not
	 * change it, and please don't autoformat the file!
	 */
	NOTHING(""), 															// 0
	RECALL_TO_CONTACT("systemdefault.recalltocontact"), 
	TURNAROUND("systemdefault.turnaround"), 
	MONTHLY_INVOICE("systemdefault.monthlyinvoice"), 
	PO_INVOICE("systemdefault.poinvoice"), 
	PART_DELIVERY("systemdefault.partdelivery"), 											// 5
	ACCREDITED_LABEL_RECALL_DATE("systemdefault.accreditedlabelrecalldate"), 
	THIRD_PARTY_DISCOUNT("systemdefault.thirdpartydiscount"), 
	WEEKLY_STATUS("systemdefault.weeklystatus"), 
	CHASE_CLIENT_APPROVAL("systemdefault.chaseclientapproval"), 
	CHASE_THIRD_PARTY("systemdefault.chasethirdparty"), 									// 10
	ELECTRONIC_CERTS_ONLY("systemdefault.electroniccertsonly"), 
	DISCOUNT("systemdefault.discount"), 
	PRICING_TYPE_SINGLE_JOB_ITEM("systemdefault.pricingtypesinglejobitem"), 
	CONFIRMATION_COSTING("systemdefault.confirmationcosting"), 
	APPROVED_LIMIT("systemdefault.approvedlimit"), 											// 15
	DELIVERY_APPROVAL("systemdefault.deliveryapproval"),
	AS_FOUND_CERTIFICATES("systemdefault.asfoundcertificates"), 
	SEPARATE_COSTS("systemdefault.separatecosts"), 
	STANDARD_LABEL_RECALL_DATE("systemdefault.standardlabelrecalldate"), 
	CERT_ACCEPTANCE_REPORT_AVAILABLE("systemdefault.certacceptancereport"), 				// 20
	CAL_EXTENSION("systemdefault.calextension"), 
	CUSTOMER_CERTIFICATE_ACCEPTANCE("systemdefault.customercertificateacceptance"), 
	DISPLAY_UNCERTAINTIES("systemdefault.displayuncertainties"), 
	PREMIUM_WEB_ACCESS("systemdefault.premiumwebaccess"), 
	OBSOLETE_CERTIFICATES("systemdefault.obsoletecertificates"), 							// 25
	NO_INVOICES("systemdefault.noinvoices"), 
	DISPLAY_BPO_ON_DOC("systemdefault.displaybpoondoc"), 
	BATCH_INVOICE("systemdefault.batchinvoice"), 
	CALIBRATION_LABELS("systemdefault.calibrationlabels"), 
	RECALL_LEVEL("systemdefault.recalllevel"), 												// 30
	DEVIATION("systemdefault.deviation"), 
	CERTIFICATE_CLASS("systemdefault.certificateclass"), 
	CSR_EMAILS("systemdefault.csremails"), 
	WORKING_HOURS_PER_DAY("systemdefault.workinghoursperday"), 
	WORKING_DAYS("systemdefault.workingdays"), 												// 35
	PRINT_SUBDIVISION_ON_ADDRESS("systemdefault.printsubdivisionaddress"), 
	STANDARD_LABEL_PLANT_NUMBER("systemdefault.standardlabelplantnumber"), 
	ACCREDITED_LABEL_PLANT_NUMBER("systemdefault.accreditedlabelplantnumber"), 
	STANDARD_LABEL_CERTIFICATE_NUMBER("systemdefault.standardlabelcertificatenumber"), 
	ACCREDITED_LABEL_CERTIFICATE_NUMBER("systemdefault.accreditedlabelcertificatenumber"), 	// 40
	DETAILED_INVOICE_TYPE("systemdefault.detailedinvoicetype"), 
	PRINT_DISCOUNT("systemdefault.printdiscount"), 
	PRINT_UNITARY_PRICE("systemdefault.printunitaryprice"), 
	PRINT_QUANTITY("systemdefault.printquantity"),
	LABELTEMPLATE("systemdefault.labeltemplate"), 											// 45
	CALIBRATION_VERIFICATON("systemdefault.calibrationverification"), 
	LINK_TO_ADVESO("systemdefault.linktoadveso"), 
	CALIBRATION_LABEL_DATE_FORMAT("systemdefault.calibrationlabeldateformat"), 
	CALIBRATION_LABEL_TECHNICIAN_NAME("systemdefault.calibrationlabeltechnicianname"), 
	SEND_FAULT_REPORT_TO_CLIENT("systemdefault.sendfaultreporttoclient"),					// 50 
	RESERVE_CERTIFICATE("systemdefault.reservecertificate"),
	CREATE_INSTRUMENT_WITHOUT_PLANT_NO("systemdefault.allowInstCreat"),
	DEFAULT_SERVICETYPE_FORSTANDARD_JOBS("systemdefault.defaultservicetype.forstandardjobs"),			
	DEFAULT_SERVICETYPE_FORONSITE_JOBS("systemdefault.defaultservicetype.foronsitejobs"),
	DEFAULT_SERVICETYPE_FORSINGLEPRICE_JOBS("systemdefault.defaultservicetype.forsinglepricejobs"), //55
	DISALLOW_SAME_PLANT_NUMBER("systemdefault.disallowSamePlantNumber"),
	SHOW_APPENDIX("systemdefault.showappendix"),
	LINK_TO_ADVESO_FROM_DATE("systemdefault.linktoadvesofromdate"),
    CLIENT_DELIVERY_DATE_RECEIVED("systemdefault.clientdelivery.datereceived"),
    CLIENT_DELIVERY_PURCHASE_ORDER("systemdefault.clientdelivery.purchaseorder"),            // 60
    CLIENT_DELIVERY_CERTIFICATES("systemdefault.clientdelivery.certificates"),
    CLIENT_DELIVERY_TOTAL_PRICE("systemdefault.clientdelivery.totalprice"),
    APPROVE_DN_CREATION_BY_CSR("systemdefault.approvedeliverynotecreationbycsr"),
    CALIBRATION_WARRANTY("systemdefault.calibrationwarranty"),
    REPAIR_WARRANTY("systemdefault.repairwarranty"),                                        // 65
    CLIENT_DN_DESTINATION_RECEIPT_DATE("systemdefault.clientdelivery.destinationreceiptdate"),
    BUSINESS_UNIT_TIME_ZONE("systemdefault.timezone"),
    QUALITY_CONTROL_REQUIRED("systemdefault.qualitycontrol"),
    RECALL_DATE_ADJUSTMENT("systemdefault.recalldateadjustment"),
    CREATE_INSTRUMENT_WITHOUT_SERIAL_NO("systemdefault.createInstrumentWithoutSerialNumber"); //70

    private final String messageCode;
    private ReloadableResourceBundleMessageSource messages;

    SystemDefaultNames(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessageCode() {
        return messageCode;
	}

	@Component
	public static class SystemDefaultNamesMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;

		@PostConstruct
		public void postConstruct() {
			for (SystemDefaultNames sdn : EnumSet.allOf(SystemDefaultNames.class))
				sdn.setMessageSource(messages);
		}
	}

	public void setMessageSource(ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}

	public String getValue() {
		Locale locale = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.toString(), locale);
		}
		return this.toString();
	}

	public static SystemDefaultNames getByOrdinal(int ordinal) {
		if (ordinal >= 0 && ordinal < values().length)
			return values()[ordinal];
		return null;
	}
}