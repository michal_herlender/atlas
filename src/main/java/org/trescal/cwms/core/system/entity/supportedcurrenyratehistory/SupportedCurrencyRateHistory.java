package org.trescal.cwms.core.system.entity.supportedcurrenyratehistory;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

/**
 * Entity representing the exhange rate of a {@link SupportedCurrency} over a
 * period of time.
 * 
 * @author Richard
 */
@Entity
@Table(name = "supportedcurrencyratehistory")
public class SupportedCurrencyRateHistory
{
	private SupportedCurrency currency;
	private Date dateFrom;
	private Date dateTo;
	private int id;
	private BigDecimal rate;
	private Contact setBy;

	@ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "currencyid", nullable = false)
	public SupportedCurrency getCurrency()
	{
		return this.currency;
	}

	@NotNull
	@Column(name = "datefrom", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDateFrom()
	{
		return this.dateFrom;
	}

	@NotNull
	@Column(name = "dateto", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDateTo()
	{
		return this.dateTo;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Column(name = "rate", nullable = false, precision = 10, scale = 2)
	public BigDecimal getRate()
	{
		return this.rate;
	}

	@NotNull
	@ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "personid", nullable = false)
	public Contact getSetBy()
	{
		return this.setBy;
	}

	public void setCurrency(SupportedCurrency currency)
	{
		this.currency = currency;
	}

	public void setDateFrom(Date dateFrom)
	{
		this.dateFrom = dateFrom;
	}

	public void setDateTo(Date dateTo)
	{
		this.dateTo = dateTo;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setRate(BigDecimal rate)
	{
		this.rate = rate;
	}

	public void setSetBy(Contact setBy)
	{
		this.setBy = setBy;
	}

}
