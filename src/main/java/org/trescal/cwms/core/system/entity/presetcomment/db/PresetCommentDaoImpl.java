package org.trescal.cwms.core.system.entity.presetcomment.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToSubdivDaoImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment_;

@Repository
public class PresetCommentDaoImpl extends AllocatedToSubdivDaoImpl<PresetComment, Integer> implements PresetCommentDao {

	@Override
	protected Class<PresetComment> getEntity() {
		return PresetComment.class;
	}

	@Override
	public List<PresetComment> getAll(PresetCommentType type) {
		return getResultList(cb -> {
			CriteriaQuery<PresetComment> cq = cb.createQuery(PresetComment.class);
			Root<PresetComment> presetComment = cq.from(PresetComment.class);
			cq.where(cb.equal(presetComment.get(PresetComment_.type), type));
			return cq;
		});
	}

	@Override
	public List<PresetComment> getAll(PresetCommentType type, Subdiv allocatedSubdiv) {
		return getResultList(cb -> {
			CriteriaQuery<PresetComment> cq = cb.createQuery(PresetComment.class);
			Root<PresetComment> presetComment = cq.from(PresetComment.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(presetComment.get(PresetComment_.organisation), allocatedSubdiv));
			clauses.getExpressions().add(cb.equal(presetComment.get(PresetComment_.type), type));
			cq.where(clauses);
			return cq;
		});
	}
}