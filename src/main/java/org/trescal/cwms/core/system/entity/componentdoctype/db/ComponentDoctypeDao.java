package org.trescal.cwms.core.system.entity.componentdoctype.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.componentdoctype.ComponentDoctype;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

public interface ComponentDoctypeDao extends BaseDao<ComponentDoctype, Integer> {
	
	ComponentDoctype findComponentDoctype(Component component, String doctypeName);
}