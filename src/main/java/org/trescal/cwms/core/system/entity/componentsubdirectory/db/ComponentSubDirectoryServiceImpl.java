package org.trescal.cwms.core.system.entity.componentsubdirectory.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.componentsubdirectory.ComponentSubDirectory;

public class ComponentSubDirectoryServiceImpl implements ComponentSubDirectoryService
{
	ComponentSubDirectoryDao componentSubDirectoryDao;

	public ComponentSubDirectory findComponentSubDirectory(int id)
	{
		return componentSubDirectoryDao.find(id);
	}

	public void insertComponentSubDirectory(ComponentSubDirectory ComponentSubDirectory)
	{
		componentSubDirectoryDao.persist(ComponentSubDirectory);
	}

	@Override
	public ComponentSubDirectory findComponentSubDirectory(int componentId, String name)
	{
		return componentSubDirectoryDao.findComponentSubDirectory(componentId, name);
	}
	
	public void updateComponentSubDirectory(ComponentSubDirectory ComponentSubDirectory)
	{
		componentSubDirectoryDao.update(ComponentSubDirectory);
	}

	public List<ComponentSubDirectory> getAllComponentSubDirectorys()
	{
		return componentSubDirectoryDao.findAll();
	}

	public void setComponentSubDirectoryDao(ComponentSubDirectoryDao componentSubDirectoryDao)
	{
		this.componentSubDirectoryDao = componentSubDirectoryDao;
	}
}