package org.trescal.cwms.core.system.entity.email.db;

import java.io.File;
import java.util.List;

import javax.mail.internet.MimeMessage;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.system.dto.EmailResultWrapper;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.email.EmailDto;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.system.form.EmailForm;
import org.trescal.cwms.core.system.form.SearchEmailForm;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface EmailService
{
	/**
	 * Copies the given {@link File} to the folder in the application's file
	 * system for the {@link Email} with the given ID, thus archiving a version
	 * of the file as it was when it was originally e-mailed as an attachment.
	 * 
	 * @param emailId the {@link Email} ID
	 * @param attachments the attachments to archive
	 */
	void archiveAttachments(int emailId, List<File> attachments);

	/**
	 * Deletes the given {@link Email} from the database.
	 * 
	 * @param email the {@link Email} to delete.
	 */
	void deleteEmail(Email email);

	/**
	 * Returns the {@link Email} with the given ID.
	 * 
	 * @param id the {@link Email} ID.
	 * @return the {@link Email}
	 */
	Email findEagerEmail(int id);

	/**
	 * Returns the {@link Email} with the given ID.
	 * 
	 * @param id the {@link Email} ID.
	 * @return the {@link Email}
	 */
	Email findEmail(int id);

	/**
	 * Returns a complete email in the form of a {@link EmailResultWrapper}
	 * contained within a {@link ResultWrapper}
	 * 
	 * @param id the {@link Email} ID.
	 * @return the {@link ResultWrapper}
	 */
	ResultWrapper getAjaxCompleteEmail(int id);

	/**
	 * Returns a {@link List} of {@link Email}s that were sent for the given
	 * {@link Component} entity with the given ID. For example, passing in a
	 * {@link Component} of Component.JOB and an ID of 1000 will return all
	 * {@link Email}s that were sent regarding the {@link Job} with ID 1000.
	 * 
	 * @param component the {@link Component}
	 * @param id the ID relating to the {@link Component}
	 * @return the {@link List} of {@link Email}s
	 */
	List<Email> getAllComponentEmails(Component component, int id);

	/**
	 * Returns a {@link List} of all {@link Email}s currently stored in the
	 * database.
	 * 
	 * @return the {@link List} of {@link Email}s.
	 */
	List<Email> getAllEmails();

	/**
	 * Returns a 2D String array containing both the file name and file path for
	 * each of the attachments for the {@link Email} with the given ID.
	 * 
	 * @param id the {@link Email} ID.
	 * @return the array of attachments, each containing a file name and path.
	 */
	String[][] getAttachmentsForEmail(int id);

	/**
	 * Returns the body of the {@link Email} with the given ID. The
	 * bodyXindiceKey stored in the {@link Email} is used to look up the body
	 * text from the project's xindice database.
	 * 
	 * @param id the {@link Email} ID.
	 * @return the body of the {@link Email} (as a string of HTML)
	 */
	String getBodyForEmail(int id);

	/**
	 * Returns a complete email in the form of a {@link EmailResultWrapper}
	 * 
	 * @param id the {@link Email} ID.
	 * @return the {@link EmailResultWrapper}
	 */
	EmailResultWrapper getCompleteEmail(int id);

	/**
	 * Inserts the given {@link Email} into the database.
	 * 
	 * @param email the {@link Email} to insert.
	 */
	void insertEmail(Email email);

	/**
	 * @param email
	 * @param recipients
	 */
	void persistEmailAndRecipients(Email email, List<EmailRecipient> recipients);

	/**
	 * Inserts the given {@link Email} into the database if it doesn't already
	 * exist. Otherwise, updates the given {@link Email}.
	 * 
	 * @param email the {@link Email} to save or update.
	 */
	void saveOrUpdateEmail(Email email);

	/**
	 * Returns a {@link PagedResultSet} of {@link Email}s that match the
	 * criteria specified in the {@link SearchEmailForm}
	 * 
	 * @param form the {@link SearchEmailForm} containing the search criteria.
	 * @param rs the {@link PagedResultSet}
	 * @return the {@link PagedResultSet} of {@link EmailDto}s
	 */
	void searchEmailsDto(SearchEmailForm form, PagedResultSet<EmailDto> rs, boolean populateRecipientDtos);

	/**
	 * Generic function for sending any e-mail within the application, with
	 * options for multiple To addresses, CC addresses, attachments, etc
	 * 
	 * @param subject the e-mail subject
	 * @param fromAddress the address the e-mail should appear to be sent from
	 * @param toAddresses a {@link List} of addresses to send the e-mail to
	 * @param ccAddresses a {@link List} of addresses to copy the e-mail to
	 * @param bccAddresses a {@link List} of addresses to blind copy the e-mail
	 *        to
	 * @param replyToAddresses a {@link List} of reply to addresses
	 * @param attachments a {@link List} of {@link File}s to attach to the
	 *        e-mail (if null, ignored)
	 * @param body the main body of the e-mail
	 * @param html if true sends the e-mail as an html-enabled e-mail, otherwise
	 *        sends as plain text
	 * @return true if the e-mail was sent successfully, false if the e-mail
	 *         could not be sent for any reason
	 */
	boolean sendAdvancedEmail(String subject, String fromAddress, @NotNull List<String> toAddresses,@NotNull List<String> ccAddresses,@NotNull List<String> bccAddresses,
							 @NotNull List<String> replyToAddresses,@NotNull List<File> attachments, String body, boolean html, boolean highImportance);

	/**
	 * Generic function for sending any e-mail within the application.
	 * 
	 * @param subject the e-mail subject
	 * @param fromAddress the address the e-mail should appear to be sent from
	 * @param toAddress the address to send the e-mail to
	 * @param body the main body of the e-mail, sent as html
	 * @return true if the e-mail was sent successfully, false if the e-mail
	 *         could not be sent for any reason
	 */
	boolean sendBasicEmail(String subject, String fromAddress, String toAddress, String body);

	boolean sendEmailAsComponentEmail(Component component, ComponentEntity compEntity, int compId, String subject, String fromAddress, String toAddress, Contact toContact, String body, Contact sendingContact);

	/**
	 * Sends an html-enabled e-mail using all details from the given
	 * {@link EmailForm}, attaching any {@link File}s within the form as well as
	 * an additional {@link File} if specified
	 * 
	 * @param form the {@link EmailForm} with all details (e.g. subject, body,
	 *        etc)
	 * @param attachments the additional {@link File} attachment if necessary
	 * @return true if the e-mail was sent successfully, false otherwise
	 */
	boolean sendEmailUsingForm(EmailForm form, List<File> attachments);

	/**
	 * Gets the logged in contact and uses their login credentials to store the
	 * given e-mail in their 'Sent Items' folder on the MS Exchange Server.
	 * NOTE: This requires ldap security to be enabled so that users have the
	 * same windows/exchange login details as CWMS.
	 * 
	 * @param msg the {@link MimeMessage} to store on the Exchange Server
	 * @return true if the operation was successful, false otherwise
	 */
	boolean storeEmailOnExchangeServer(MimeMessage msg);

	/**
	 * Updates the given {@link Email} in the database.
	 * 
	 * @param email the {@link Email} to update.
	 */
	void updateEmail(Email email);
}