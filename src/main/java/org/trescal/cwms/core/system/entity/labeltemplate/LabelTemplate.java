package org.trescal.cwms.core.system.entity.labeltemplate;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name = "labeltemplate")
public class LabelTemplate {
	private int id;
	private LabelTemplateType labelTemplateType;
	private String name;
	private String propertyName;
	private Set<LabelTemplateAdditionalField> additionalFields;
	
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "labeltemplatetype", nullable = false)
	public LabelTemplateType getLabelTemplateType()
	{
		return this.labelTemplateType;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "labelTemplate")    
	public Set<LabelTemplateAdditionalField> getAdditionalFields() {
		return additionalFields;
	}
	
	@Column(name = "name", nullable = false,columnDefinition = "nvarchar(50)")
	public String getName() {
		return name;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setLabelTemplateType(LabelTemplateType labelTemplateType) {
		this.labelTemplateType = labelTemplateType;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public void setAdditionalFields(
			Set<LabelTemplateAdditionalField> additionalFields) {
		this.additionalFields = additionalFields;
	}
	
	@Transient
	public String AdditionalFields()
	{
		String response = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			response = mapper.writeValueAsString(this.additionalFields);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}
}
