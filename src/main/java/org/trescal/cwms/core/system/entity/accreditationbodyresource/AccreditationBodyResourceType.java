package org.trescal.cwms.core.system.entity.accreditationbodyresource;

public enum AccreditationBodyResourceType {
	UNDEFINED,			// Ordinal 0
	CAL_LOGO_271_354	// Ordinal 1
}
