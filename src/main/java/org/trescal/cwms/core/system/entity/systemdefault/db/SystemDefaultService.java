package org.trescal.cwms.core.system.entity.systemdefault.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;
import org.trescal.cwms.core.system.enums.Scope;

public interface SystemDefaultService extends BaseService<SystemDefault, SystemDefaultNames> {

	/**
	 * Returns a {@link SystemDefault} whoose name matches the given case-sensitive
	 * name.
	 * 
	 * @param name the name of the {@link SystemDefault}.
	 * @return the matching {@link SystemDefault}
	 */
	SystemDefault getByName(String name);

	List<SystemDefault> getAllByCompanyRoleAndScope(CompanyRole companyRole, Scope scope);
}