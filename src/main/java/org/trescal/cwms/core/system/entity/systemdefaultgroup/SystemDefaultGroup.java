/**
 * 
 */
package org.trescal.cwms.core.system.entity.systemdefaultgroup;

import java.util.Comparator;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.SortNatural;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.translation.Translation;

/**
 * Entity that groups together one or more {@link SystemDefault}s.
 * 
 * @author richard
 */
@Entity
@Table(name = "systemdefaultgroup")
public class SystemDefaultGroup
{
	/**
	 * Inner {@link Comparator} implementation used to sort lazily loaded
	 * collections of {@link SystemDefaultGroup} entities.
	 * 
	 * @author richard
	 */
	public static class SystemDefaultGroupComparator implements Comparator<SystemDefaultGroup>
	{
		@Override
		public int compare(SystemDefaultGroup o1, SystemDefaultGroup o2)
		{
			return o1.getName().compareTo(o2.getName());
		}
	}

	private Set<SystemDefault> defaults;
	private String description;
	private Integer id;
	private String name;
	private Set<Translation> descriptionTranslation;
	private Set<Translation> nameTranslation;
	

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "group")
	@SortNatural
	public Set<SystemDefault> getDefaults()
	{
		return this.defaults;
	}

	@Length(max = 200)
	@Column(name = "description", length = 200)
	public String getDescription()
	{
		return this.description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getId()
	{
		return this.id;
	}

	@NotNull
	@Length(min = 1, max = 50)
	@Column(name = "dname", length = 50, nullable = false)
	public String getName()
	{
		return this.name;
	}

	/**
	 * @param defaults the defaults to set
	 */
	public void setDefaults(Set<SystemDefault> defaults)
	{
		this.defaults = defaults;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="systemdefaultgroupdescriptiontranslation", joinColumns=@JoinColumn(name="id"))
	public Set<Translation> getDescriptionTranslation() {
		return descriptionTranslation;
	}

	public void setDescriptionTranslation(Set<Translation> descriptionTranslation) {
		this.descriptionTranslation = descriptionTranslation;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="systemdefaultgroupnametranslation", joinColumns=@JoinColumn(name="id"))
	public Set<Translation> getNameTranslation() {
		return nameTranslation;
	}

	public void setNameTranslation(Set<Translation> nameTranslation) {
		this.nameTranslation = nameTranslation;
	}

}
