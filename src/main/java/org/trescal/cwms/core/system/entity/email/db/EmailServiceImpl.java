package org.trescal.cwms.core.system.entity.email.db;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetupTest;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.system.dto.EmailResultWrapper;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.email.EmailDto;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipientDto;
import org.trescal.cwms.core.system.entity.emailrecipient.RecipientType;
import org.trescal.cwms.core.system.entity.emailrecipient.db.EmailRecipientService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.system.form.EmailForm;
import org.trescal.cwms.core.system.form.SearchEmailForm;
import org.trescal.cwms.core.tools.EmailResourceProcessor;
import org.trescal.cwms.core.tools.EncryptionTools;
import org.trescal.cwms.core.tools.FileTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.spring.model.KeyValue;

import javax.annotation.PreDestroy;
import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

@Service("EmailService")
@Slf4j
public class EmailServiceImpl implements EmailService, InitializingBean {
	 enum EmailServiceMode {
		DEVELOPMENT, // DEVELOPMENT - uses Greenmail to simulate mail server
						// connection
		PRODUCTION, // PRODUCTION - connects to real email server and sends real
					// emails
		TEST // TEST - used for unit testing (TODO fully implement - needed to
				// prevent multiple Greenmail startups, one per context)
	}


	 @Autowired
	 private EmailResourceProcessor emailResourceProcessor;

	@Value("#{props['cwms.config.emailservicemode']}")
	private String emailServiceModeText;
	private GreenMail greenMail; // Initialized only for emailServiceMode =
									// DEVELOPMENT
	@Value("#{props['mail.server']}")
	private String mailServer;
	@Value("#{props['mail.username']}")
	private String mailUsername;
	@Value("#{props['mail.password']}")
	private String mailPassword;
	@Value("#{props['cwms.config.email.store_on_exchange']}")
	private boolean storeOnExchange;

	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private EmailDao emailDao;
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private EmailRecipientService recipServ;
	@Autowired
	private SessionUtilsService sessionUtilsService;
	@Autowired
	private SystemComponentService scServ;
	@Autowired
	private MessageSource messages;


	@Override
	public void archiveAttachments(int emailId, List<File> attachments) {
		if (attachments != null) {
			// find e-mail to set directory
			Email email = this.findEmail(emailId);
			String dir = email.getDirectory().getAbsolutePath();

			for (File attachment : attachments) {
				if (attachment != null) {
					String fileName = attachment.getName();
					String destination = dir.concat(File.separator).concat(fileName);
					File archiveFile = new File(destination);
					// copy the file to the e-mail's folder, by default (no moves)
					FileTools.copyFile(attachment, archiveFile);
				}
			}
		}
	}

	@Override
	public void deleteEmail(Email email) {
		this.emailDao.remove(email);
	}

	@Override
	public Email findEagerEmail(int id) {
		Email email = this.emailDao.findEagerEmail(id);
		// 2018-05-15 GB : Prevent directory lookup on repeated gets.
		if (email != null && email.getDirectory() == null) {
			email.setDirectory((this.compDirServ.getDirectory(this.scServ.findComponent(Component.EMAIL),
					String.valueOf(email.getId()))));
		}
		return email;
	}

	@Override
	public Email findEmail(int id) {
		Email email = this.emailDao.find(id);
		// 2018-05-15 GB : Prevent directory lookup on repeated gets.
		if (email != null && email.getDirectory() == null) {
			email.setDirectory((this.compDirServ.getDirectory(this.scServ.findComponent(Component.EMAIL),
					String.valueOf(email.getId()))));
		}
		return email;
	}

	@Override
	public ResultWrapper getAjaxCompleteEmail(int id) {
		EmailResultWrapper erw = this.getCompleteEmail(id);

		if (erw != null) {
			return new ResultWrapper(true, "", erw, null);
		} else {
			return new ResultWrapper(false, this.messages.getMessage("error.email.notfound", null,
					"The email requested could not be found", null), null, null);
		}
	}

	@Override
	public List<Email> getAllComponentEmails(Component component, int id) {
		return this.emailDao.getAllComponentEmails(component, id);
	}

	@Override
	public List<Email> getAllEmails() {
		return this.emailDao.findAll();
	}

	@Override
	public String[][] getAttachmentsForEmail(int id) {
		Email email = this.findEmail(id);
		List<File> list = FileTools.getFileListForDir(email.getDirectory());

		// has to be passed back as an 2D String array because DWR does not
		// support the conversion of java.io.File objects.
		String[][] results = new String[list.size()][3];
		int i = 0;
		for (File file : list) {
			results[i][0] = file.getName();
			results[i][1] = file.getAbsolutePath();
			results[i][2] = encryptFilePath(file.getAbsolutePath());

			i++;
		}
		return results;
	}

	public String encryptFilePath(String filePath) {
		try {
			return URLEncoder.encode(EncryptionTools.encrypt(filePath), "UTF-8");
		} catch (Exception ex) {
			log.error("Error encrypting filepath", ex);
			return "";
		}
	}

	/*
	 * Removed Xindice retrieval 2018-11-1, added basic error message in case
	 * data not found
	 */
	@Override
	public String getBodyForEmail(int id) {
		Email email = this.emailDao.find(id);
		if (email == null) {
			return "Error : no email found for id " + id;
		} else if (email.getBody() == null) {
			return "Error : email body was null for id " + id;
		} else {
			return email.getBody();
		}
	}

	@Override
	public EmailResultWrapper getCompleteEmail(int id) {
		EmailResultWrapper email = new EmailResultWrapper();

		email.setEmail(this.findEagerEmail(id));
		email.setAttachments(this.getAttachmentsForEmail(id));
		email.setBody(this.getBodyForEmail(id));

		return email;
	}

	@Override
	public void insertEmail(Email Email) {
		this.emailDao.persist(Email);
	}

	@Override
	public void persistEmailAndRecipients(Email email, List<EmailRecipient> recipients) {
		email.setRecipients(recipients);
		this.insertEmail(email);

		for (EmailRecipient recip : recipients) {
			this.recipServ.insertEmailRecipient(recip);
		}
	}

	@Override
	public void saveOrUpdateEmail(Email email) {
		this.emailDao.merge(email);
	}

	@Override
	public void searchEmailsDto(SearchEmailForm form, PagedResultSet<EmailDto> rs, boolean populateRecipientDtos) {
		this.emailDao.searchEmailsDto(form, rs);
		if (populateRecipientDtos && !rs.getResults().isEmpty()) {
			Collection<Integer> emailIds = rs.getResults().stream().map(EmailDto::getId).collect(Collectors.toSet());
			List<EmailRecipientDto> recipientDtos = this.recipServ.searchRecipientsDto(emailIds);
			mergeRecipientDtos(rs.getResults(), recipientDtos);
		}
	}
	
	private void mergeRecipientDtos(Collection<EmailDto> emailDtos, Collection<EmailRecipientDto> recipientDtos) {
		Map<Integer, EmailDto> idMap = new HashMap<>();
		emailDtos.forEach(emailDto -> idMap.put(emailDto.getId(), emailDto));
		for (EmailRecipientDto recipientDto : recipientDtos) {
			EmailDto emailDto = idMap.get(recipientDto.getEmailId());
			if (emailDto != null) {
				emailDto.addRecipient(recipientDto);
			}
			else {
				log.warn("EmailDto not found for id "+recipientDto.getId());
			}
		}
	}

	@Override
	public boolean sendAdvancedEmail(String subject, String fromAddress, @NotNull List<String> toAddresses,
									 @NotNull List<String> ccAddresses, @NotNull List<String> bccAddresses,
									 @NotNull List<String> replyToAddresses, @NotNull List<File> attachments,
									 String body, boolean html, boolean highImportance) {
		MimeMessage mime = this.sendEmailMimeMessage(subject, fromAddress, toAddresses, ccAddresses, bccAddresses,
				replyToAddresses, attachments, Collections.emptyList(),body, html, highImportance);
		return mime != null;
	}

	@Override
	public boolean sendBasicEmail(String subject, String fromAddress, String toAddress, String body) {
		MimeMessage mime = this.sendEmailMimeMessage(subject, fromAddress, Collections.singletonList(toAddress), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(),
				Collections.emptyList(),Collections.emptyList(), body, true, false);
		return mime != null;
	}

	@Override
	public boolean sendEmailAsComponentEmail(Component component, ComponentEntity compEntity, int compId,
			String subject, String fromAddress, String toAddress, Contact toContact, String body, Contact sendingContact) {
		EmailForm mailForm = new EmailForm(component, compId, fromAddress, toContact, toAddress, subject,
				body, sendingContact);
		EmailRecipient mailRecipient = new EmailRecipient();
		mailRecipient.setEmailAddress(StringUtils.isEmpty(toAddress) ? toContact.getEmail() : toAddress);
		mailRecipient.setRecipientCon(toContact);
		mailRecipient.setType(RecipientType.EMAIL_TO);
		mailRecipient.setEmail(mailForm.getEmail());
		mailForm.setRecipients(Collections.singletonList(mailRecipient));
		mailForm.getEmail().setRecipients(Collections.singletonList(mailRecipient));

		return this.sendEmailUsingForm(mailForm, Collections.emptyList());
	}

	private MimeMessage sendEmailMimeMessage(String subject, String fromAddress,@NotNull List<String> toAddresses,
			@NotNull List<String> ccAddresses, @NotNull List<String> bccAddresses, @NotNull List<String> replyToAddresses, @NotNull List<File> attachments,
											 List<KeyValue<String,File>> inlineResources,
			String body, boolean html, boolean highImportance) {
		try {
			MimeMessage msg = this.mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(msg, MimeMessageHelper.MULTIPART_MODE_RELATED,
					StandardCharsets.UTF_8.name());

			helper.setFrom(fromAddress);
			helper.setSubject(subject);
			val processResult = processBodyForInlineImages(body);
			helper.setText(processResult.getBody(), html);
			helper.setTo(toAddresses.toArray(new String[0]));
			processResult.resources.forEach(ir -> {
				try {
					helper.addInline(ir.getId(),ir.getResource());
				} catch (MessagingException e) {
					e.printStackTrace();
				}
			});
			msg.setReplyTo(convertAddressesToInternetAddress(replyToAddresses).toArray(new Address[0]));

			if (highImportance) {
				helper.setPriority(1);
			}

				helper.setCc(ccAddresses.toArray(new String[0]));

				helper.setBcc(bccAddresses.toArray(new String[0]));

				for (File file : attachments) {
					helper.addAttachment(file.getName(), file);
				}

			for (KeyValue<String, File> kv : inlineResources) {
				helper.addInline(kv.getKey(), kv.getValue());
			}

			this.mailSender.send(msg);

			return msg;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private ProcessBodyResult processBodyForInlineImages(String body){
		val result = ProcessBodyResult.builder();
		val document = Jsoup.parse(body);
		val images = document.select("img");
		images.forEach(e -> {
			val scr = e.attr("src");
			emailResourceProcessor.processImage(scr).ifPresent(ir -> {
					e.attr("src","cid:"+ir.getId());
					result.resource(ir);
			});
		});
		result.body(document.outerHtml());
		return result.build();
	}

	private List<Address> convertAddressesToInternetAddress(List<String> replyToAddresses) throws AddressException {
		List<Address> replyToInternetAddresses = new ArrayList<>();
		for (String replyToAddress : replyToAddresses) {
			replyToInternetAddresses.add(new InternetAddress(replyToAddress));
		}
		return replyToInternetAddresses;
	}

	@Override
	public boolean sendEmailUsingForm(EmailForm form, List<File> attachmentList) {
		List<String> toList = new ArrayList<>();
		List<String> ccList = new ArrayList<>();

		for (EmailRecipient recip : form.getRecipients()) {
			if (recip.getType() == RecipientType.EMAIL_TO) {
				toList.add(recip.getEmailAddress());
			} else {
				ccList.add(recip.getEmailAddress());
			}
		}

		List<String> replyToAddresses = new ArrayList<>();
		replyToAddresses.add(form.getFrom());
		List<String> bccAddresses = new ArrayList<>();
		if (form.isIncludeSelfInReply()) {
			appendSelfToAddressesList(replyToAddresses);
		}
		if(form.isIncludeSelfInCarbonCopy()){
			val address = getOwnAddress(sessionUtilsService.getCurrentContact());
			address.ifPresent(con ->
					form.getRecipients()
							.add(EmailRecipient.builder()
									.emailAddress(con)
									.email(form.getEmail())
									.type(RecipientType.EMAIL_CC).build())
					);
			address.ifPresent(bccAddresses::add);
		}

		MimeMessage msg = this.sendEmailMimeMessage(form.getSubject(), form.getFrom(), toList, ccList, bccAddresses,
				replyToAddresses, attachmentList,Collections.emptyList(), form.getBody(), true, false);

		if (msg != null) {
			if (this.storeOnExchange) {
				this.storeEmailOnExchangeServer(msg);
			}

			this.persistEmailAndRecipients(form.getEmail(), form.getRecipients());

			// after the e-mail has been persisted, get the new id
			int emailId = form.getEmail().getId();

			// archive all attachments (temp file cleanup is external responsibility) 
			this.archiveAttachments(emailId, attachmentList);

			return true;
		} else {
			return false;
		}
	}

	private void appendSelfToAddressesList(List<String> replyToAddresses) {
		Contact con = this.sessionUtilsService.getCurrentContact();
		val ownAddress = getOwnAddress(con);

		ownAddress.ifPresent(replyToAddresses::add);
	}

	private Optional<String> getOwnAddress(Contact con) {
		return  Optional.ofNullable(con).map(Contact::getEmail)
				.filter(StringUtils::isNotBlank);
	}

	@Override
	public boolean storeEmailOnExchangeServer(MimeMessage msg) {
		try {
			String username = "";
			String password = "";

			Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (obj instanceof UserDetails) {
				UserDetails details = (UserDetails) obj;
				username = details.getUsername();
				password = details.getPassword();
			}

			Properties props = System.getProperties();
			props.put("mail.smtp.host", this.mailServer);
			Session session = Session.getDefaultInstance(props, null);
			Store store = session.getStore("imap");
			store.connect(this.mailServer, username, password);

			Folder archive = store.getFolder("SENT ITEMS");
			archive.open(Folder.READ_WRITE);
			Message[] tosave = { msg };
			archive.appendMessages(tosave);
			archive.close(false);

			if (store.isConnected()) {
				store.close();
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void updateEmail(Email Email) {
		this.emailDao.merge(Email);
	}

	@Override
	public void afterPropertiesSet() {
		EmailServiceMode emailServiceMode = EmailServiceMode.valueOf(emailServiceModeText);
		log.info("emailServiceMode: " + emailServiceMode);
		if (emailServiceMode == EmailServiceMode.DEVELOPMENT ) {
			greenMail = new GreenMail(ServerSetupTest.SMTP_POP3_IMAP);
			greenMail.start();
			greenMail.setUser(mailUsername,mailPassword);
			log.info("Started GreenMail sandbox server for SMTP port " + ServerSetupTest.SMTP.getPort()
					+ " and IMAP port " + ServerSetupTest.IMAP.getPort() + " for development usage");
		}
	}

	@PreDestroy
	public void destroy() {
		greenMail.stop();
	}

	@Builder
	@Data
	private static class ProcessBodyResult {
		private String body;
		@Singular
		private Set<EmailResourceProcessor.ImageResource> resources;
	}
}