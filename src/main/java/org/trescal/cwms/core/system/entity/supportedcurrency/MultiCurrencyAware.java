package org.trescal.cwms.core.system.entity.supportedcurrency;

import java.math.BigDecimal;

/**
 * Interface the must be implemented by all entities that are multi-currency
 * aware.
 * 
 * @author Richard
 */
public interface MultiCurrencyAware
{
	SupportedCurrency getCurrency();

	BigDecimal getRate();

	void setCurrency(SupportedCurrency currency);

	void setRate(BigDecimal rate);
}
