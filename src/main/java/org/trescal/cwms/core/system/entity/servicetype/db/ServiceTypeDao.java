package org.trescal.cwms.core.system.entity.servicetype.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceTypeDto;

public interface ServiceTypeDao extends BaseDao<ServiceType, Integer> {
	
	List<ServiceType> getAllByDomainType(DomainType domainType);
	
	List<ServiceType> getAllCalServiceTypes();
	
	ServiceType getByLongName(String longName, Locale locale);
	
	ServiceType getByShortNameAndLocal(String shortName, Locale locale);
	
	List<ServiceTypeDto> getDTOs(Collection<Integer> serviceTypeIds, Locale locale);
	
	/**
	 * Gets service type projection with translations / defaults for specified query
	 * Avoids need for translation n+1 select when building list of service types
	 *   
	 * @param domainType - OPTIONAL 
	 * @param jobType - OPTIONAL
	 * @param locale - MANDATORY
	 * @return
	 */
	List<ServiceTypeDto> getAllDto(DomainType domainType, JobType jobType, Locale locale);

	List<ServiceType> getByJobType(JobType jobType);
}