package org.trescal.cwms.core.system.entity.presetcomment.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;

/**
 * Interface for accessing and manipulating (subclass instances of)
 * {@link PresetComment} entities.
 */
public interface PresetCommentService extends BaseService<PresetComment, Integer> {

	List<PresetComment> getAll(PresetCommentType type);

	List<PresetComment> getAll(PresetCommentType type, Subdiv allocatedSubdiv);
}