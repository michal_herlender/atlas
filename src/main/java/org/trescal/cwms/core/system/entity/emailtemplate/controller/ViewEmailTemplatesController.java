package org.trescal.cwms.core.system.entity.emailtemplate.controller;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.emailtemplate.EmailTemplate;
import org.trescal.cwms.core.system.entity.emailtemplate.EmailTemplateComparator;
import org.trescal.cwms.core.system.entity.emailtemplate.db.EmailTemplateService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class ViewEmailTemplatesController {
	
	@Autowired
	private CompanyService companyService; 
	@Autowired
	private EmailTemplateService emailTemplateService;
	@Autowired
	private SystemComponentService systemComponentService;
	
	public final static String VIEW_NAME = "trescal/core/system/viewemailtemplates";
	public final static String URL = "viewemailtemplates.htm";
	
	@RequestMapping(value=URL, method=RequestMethod.GET)
	public String referenceData(Model model, @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		Company allocatedCompany = this.companyService.get(companyDto.getKey());
		List<EmailTemplate> companyTemplates = this.emailTemplateService.getAllByCompany(allocatedCompany);
		List<Component> supportedComponents = this.systemComponentService.getComponentsUsingEmailTemplates();
		
		Set<Component> companyComponents = companyTemplates.stream().map(template -> template.getComponent()).distinct().collect(Collectors.toSet());
		Map<Component, Set<EmailTemplate>> templateMap = new TreeMap<>();
		companyComponents.stream().forEach(component -> templateMap.put(component, new TreeSet<EmailTemplate>(new EmailTemplateComparator())));
		companyTemplates.stream().forEach(template -> templateMap.get(template.getComponent()).add(template));
		
		model.addAttribute("supportedComponents", supportedComponents);
		model.addAttribute("templateMap", templateMap);
		return VIEW_NAME;
	}
}
