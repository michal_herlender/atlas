package org.trescal.cwms.core.system.entity.emailtemplate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.emailtemplate.EmailTemplate;
import org.trescal.cwms.core.system.entity.emailtemplate.db.EmailTemplateService;

@Controller @IntranetController
@RequestMapping(value="deleteemailtemplate.htm")
public class DeleteEmailTemplateController {
	@Autowired
	private EmailTemplateService emailTemplateService;
	
	public static final String REDIRECT_URL = "redirect:viewemailtemplates.htm";
	
	@RequestMapping(method={RequestMethod.POST, RequestMethod.GET})
	public String onSubmit(@RequestParam(name="id", required=true) int id) {
		EmailTemplate template = this.emailTemplateService.get(id);
		if (template == null) throw new RuntimeException("EmailTemplate with ID "+id+" does not exist");
		this.emailTemplateService.delete(template);
		return REDIRECT_URL;
	}
}
