/**
 * 
 */
package org.trescal.cwms.core.system.entity.webresource;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;

/**
 * Superclass that all entities that are used to store url's should extend.
 * 
 * @author richard
 */
@Entity
@Table(name = "webresource")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("simple")
public abstract class WebResource extends Auditable
{
	private String description;
	private int id;
	private String url;

	@Length(max = 200)
	@Column(name = "description", length = 200, nullable = true)
	public String getDescription()
	{
		return this.description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "urlid", nullable = false)
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@Length(min = 1, max = 1000)
	@Column(name = "url", length = 1000, nullable = false)
	public String getUrl()
	{
		return this.url;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}
}
