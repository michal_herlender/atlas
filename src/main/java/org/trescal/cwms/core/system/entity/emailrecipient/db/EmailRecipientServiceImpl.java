package org.trescal.cwms.core.system.entity.emailrecipient.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipientDto;

public class EmailRecipientServiceImpl implements EmailRecipientService
{
	EmailRecipientDao emailRecipientDao;

	public void deleteEmailRecipient(EmailRecipient emailrecipient)
	{
		this.emailRecipientDao.remove(emailrecipient);
	}

	public EmailRecipient findEmailRecipient(int id)
	{
		return this.emailRecipientDao.find(id);
	}

	public List<EmailRecipient> getAllEmailRecipients()
	{
		return this.emailRecipientDao.findAll();
	}

	public void insertEmailRecipient(EmailRecipient EmailRecipient)
	{
		this.emailRecipientDao.persist(EmailRecipient);
	}

	public void saveOrUpdateEmailRecipient(EmailRecipient emailrecipient)
	{
		this.emailRecipientDao.saveOrUpdate(emailrecipient);
	}
	
	public List<EmailRecipientDto> searchRecipientsDto(Collection<Integer> emailIds) {
		return this.emailRecipientDao.searchRecipientsDto(emailIds);
	}

	public void setEmailRecipientDao(EmailRecipientDao emailRecipientDao)
	{
		this.emailRecipientDao = emailRecipientDao;
	}

	public void updateEmailRecipient(EmailRecipient EmailRecipient)
	{
		this.emailRecipientDao.update(EmailRecipient);
	}
}