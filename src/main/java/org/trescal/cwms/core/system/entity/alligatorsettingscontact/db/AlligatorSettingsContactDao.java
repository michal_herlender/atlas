package org.trescal.cwms.core.system.entity.alligatorsettingscontact.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettingsContact;

public interface AlligatorSettingsContactDao extends BaseDao<AlligatorSettingsContact, Integer> {
	AlligatorSettingsContact findAlligatorSettingsContact(Contact businessContact);
}
