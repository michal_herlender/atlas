package org.trescal.cwms.core.system.dto;

import java.math.BigDecimal;

import org.trescal.cwms.core.system.entity.supportedcurrenyratehistory.SupportedCurrencyRateHistory;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SupportedCurrencyRateHistoryDTO {

	private String dateFrom;
	private String dateTo;
	private BigDecimal rate;
	private String setBy;

	public SupportedCurrencyRateHistoryDTO(String dateFrom, String dateTo, BigDecimal rate, String setBy) {
		super();
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.rate = rate;
		this.setBy = setBy;
	}

	public SupportedCurrencyRateHistoryDTO(SupportedCurrencyRateHistory history) {
		super();
		this.dateFrom = history.getDateFrom().toString();
		this.dateTo = history.getDateTo().toString();
		this.rate = history.getRate();
		this.setBy = history.getSetBy().getName();
	}

}
