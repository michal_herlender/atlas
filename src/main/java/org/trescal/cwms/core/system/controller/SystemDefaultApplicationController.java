package org.trescal.cwms.core.system.controller;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefaultDTO;
import org.trescal.cwms.core.system.entity.systemdefault.db.SystemDefaultService;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.db.SystemDefaultApplicationService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.LinkToAdveso;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultType;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class SystemDefaultApplicationController {
	private static final Log LOG = LogFactory.getLog(SystemDefaultApplicationController.class);

	@Autowired
	private SystemDefaultApplicationService applicationService;
	@Autowired
	private SystemDefaultService systemDefaultService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private List<SystemDefaultType<?>> systemDefaultTypes;
	@Autowired
	private LinkToAdveso linkToAdveso;

	@RequestMapping(value = "/deleteSystemDefaultApplication.json", method = RequestMethod.GET)
	@ResponseBody
	private SystemDefaultDTO deleteSystemDefaultApplication(
			@RequestParam(value = "applicationId", required = true) Integer applicationId) {
		try {
			SystemDefaultApplication application = applicationService.get(applicationId);
			SystemDefaultDTO systemDefaultDTO = new SystemDefaultDTO(application.getSystemDefault());
			applicationService.delete(application);
			return systemDefaultDTO;
		} catch (Exception ex) {
			LOG.error(ex.getMessage());
			throw ex;
		}
	}

	@RequestMapping(value = "/saveSystemDefaultApplication.json", method = RequestMethod.GET)
	@ResponseBody
	private SystemDefaultDTO saveSystemDefaultApplication(
			@RequestParam(value = "defaultId", required = true) Integer defaultId,
			@RequestParam(value = "newValue", required = true) String value,
			@RequestParam(value = "scope", required = true) Scope scope,
			@RequestParam(value = "scopeId", required = true) Integer scopeId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		try {
			SystemDefaultType<?> type = null;
			for (SystemDefaultType<?> t : systemDefaultTypes) {
				if (t.defaultName().ordinal() == defaultId) {
					type = t;
					break;
				}
			}
			if (type == null)
				throw new RuntimeException("Not found SystemDefaultType " + defaultId);
			String parsedValue = type.parseAndGetStringValue(value);
			Company allocatedCompany = companyService.get(companyDto.getKey());
			SystemDefaultApplication app = applicationService.get(defaultId, scope, scopeId, allocatedCompany);
			if (app != null) {
				app.setValue(parsedValue);
				applicationService.update(app);
			} else {
				app = new SystemDefaultApplication();
				switch (scope) {
				case COMPANY:
					app.setCompany(companyService.get(scopeId));
					break;
				case SUBDIV:
					app.setSubdiv(subdivService.get(scopeId));
					break;
				case CONTACT:
					app.setContact(contactService.get(scopeId));
				default:
				}
				SystemDefault systemDefault = systemDefaultService.get(SystemDefaultNames.values()[defaultId]);
				app.setSystemDefault(systemDefault);
				app.setValue(parsedValue);
				// Allocated company is set to the creating business for
				// consistency, , regardless of whether
				// the system default is groupwide or not - the query for
				// groupwide defaults ignores this field
				app.setOrganisation(allocatedCompany);
				applicationService.save(app);

			}
			
			// evit cache when linktoadveso system default is changed
			if (app.getSystemDefault().getId().equals(SystemDefaultNames.LINK_TO_ADVESO) || app.getSystemDefault()
					.getId().equals(SystemDefaultNames.LINK_TO_ADVESO_FROM_DATE)) {
				linkToAdveso.evictAllCache();
			}
			
			return new SystemDefaultDTO(app);
		} catch (Exception ex) {
			LOG.error(ex.getMessage());
			throw ex;
		}
	}
}