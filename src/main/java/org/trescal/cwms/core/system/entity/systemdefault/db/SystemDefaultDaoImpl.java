package org.trescal.cwms.core.system.entity.systemdefault.db;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault_;
import org.trescal.cwms.core.system.entity.systemdefaultgroup.SystemDefaultGroup;
import org.trescal.cwms.core.system.entity.systemdefaultgroup.SystemDefaultGroup_;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;
import org.trescal.cwms.core.system.enums.Scope;

@Repository("SystemDefaultDao")
public class SystemDefaultDaoImpl extends BaseDaoImpl<SystemDefault, SystemDefaultNames> implements SystemDefaultDao {

	@Override
	protected Class<SystemDefault> getEntity() {
		return SystemDefault.class;
	}

	@Override
	public SystemDefault getByName(String name) {
		return getSingleResult(cb -> {
			CriteriaQuery<SystemDefault> cq = cb.createQuery(SystemDefault.class);
			Root<SystemDefault> systemDefault = cq.from(SystemDefault.class);
			cq.where(cb.equal(systemDefault.get(SystemDefault_.defaultName), name));
			return cq;
		});
	}

	/**
	 * Galen Beck - 2016-06-05 Rewrote implementation using (previously unused)
	 * Criteria call to improve performance (e.g. performance impact seen on Company
	 * page) Eagerly fetching translations for default.description, and group.name
	 * only (These are the only ones used in system defaults tag currently) Consider
	 * rewriting into DTO in future
	 */
	@Override
	public List<SystemDefault> getAllByCompanyRoleAndScope(CompanyRole companyRole, Scope scope) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<SystemDefault> cq = cb.createQuery(SystemDefault.class);

		Root<SystemDefault> rootSD = cq.from(SystemDefault.class);

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(rootSD.join(SystemDefault_.companyRoles).in(companyRole));
		clauses.getExpressions().add(rootSD.join(SystemDefault_.scopes).in(scope));

		rootSD.fetch(SystemDefault_.defaultDescriptionTranslation, JoinType.LEFT);

		Fetch<SystemDefault, SystemDefaultGroup> group = rootSD.fetch(SystemDefault_.group, JoinType.INNER);
		group.fetch(SystemDefaultGroup_.nameTranslation, JoinType.LEFT);

		cq.where(clauses);
		cq.orderBy(cb.asc(rootSD.get(SystemDefault_.group).get(SystemDefaultGroup_.name)),
				cb.asc(rootSD.get(SystemDefault_.defaultName)), cb.asc(rootSD.get(SystemDefault_.id)));
		cq.distinct(true);

		List<SystemDefault> list = getEntityManager().createQuery(cq).getResultList();
		return list;
	}
}