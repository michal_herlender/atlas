package org.trescal.cwms.core.system.form;

import org.apache.commons.text.StringEscapeUtils;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLink;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

import javax.validation.Valid;
import javax.validation.constraints.Size;

/**
 * Form backing object for adding and editing {@link Instruction}s.
 *
 * @author Richard
 */
public class InstructionForm {
	private Integer coid;
	private Integer subdivid;
	private Integer personid;
	private Integer jobid;
	private boolean includeOnDelNotes;
	private boolean includeOnSupplierDelNotes;
	private InstructionLink<?> instructionLink;
	private int instructionid;
	private String instructiontext;
	private InstructionType instructionType;
	private String refererUrl;
	private boolean isSubdivInstruction;

	public Integer getCoid() {
		return this.coid;
	}

	@Valid
	public InstructionLink<?> getInstructionLink() {
		return this.instructionLink;
	}

	public int getInstructionid() {
		return this.instructionid;
	}

	@Size(min = 1, max = 1000)
	public String getInstructiontext() {
		return this.instructiontext;
	}

	public InstructionType getInstructionType() {
		return this.instructionType;
	}

	/**
	 * @return the jobid
	 */
	public Integer getJobid() {
		return this.jobid;
	}

	public Integer getPersonid() {
		return this.personid;
	}

	public String getRefererUrl() {
		return this.refererUrl;
	}

	public Integer getSubdivid() {
		return this.subdivid;
	}

	public boolean isIncludeOnDelNotes() {
		return this.includeOnDelNotes;
	}

	public boolean isIncludeOnSupplierDelNotes() {
		return this.includeOnSupplierDelNotes;
	}

	public void setCoid(Integer coid) {
		this.coid = coid;
	}

	public void setIncludeOnDelNotes(boolean includeOnDelNotes) {
		this.includeOnDelNotes = includeOnDelNotes;
	}

	public void setIncludeOnSupplierDelNotes(boolean includeOnSupplierDelNotes) {
		this.includeOnSupplierDelNotes = includeOnSupplierDelNotes;
	}

	public void setInstructionLink(InstructionLink<?> instructionLink) {
		this.instructionLink = instructionLink;
	}

	public void setInstructionid(int instructionid) {
		this.instructionid = instructionid;
	}

	public void setInstructiontext(String instructiontext) {
		this.instructiontext = instructiontext;
	}

	public void setInstructionType(InstructionType instructionType) {
		this.instructionType = instructionType;
	}

	/**
	 * @param jobid the jobid to set
	 */
	public void setJobid(Integer jobid) {
		this.jobid = jobid;
	}

	public void setPersonid(Integer personid) {
		this.personid = personid;
	}

	public void setRefererUrl(String refererUrl) {
		this.refererUrl = StringEscapeUtils.unescapeHtml4(refererUrl);
	}

	public void setSubdivid(Integer subdivid) {
		this.subdivid = subdivid;
	}

	public boolean isSubdivInstruction() {
		return isSubdivInstruction;
	}

	public void setSubdivInstruction(boolean isSubdivInstruction) {
		this.isSubdivInstruction = isSubdivInstruction;
	}

}
