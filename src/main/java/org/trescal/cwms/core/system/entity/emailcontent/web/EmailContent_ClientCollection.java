package org.trescal.cwms.core.system.entity.emailcontent.web;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.company.entity.addressprintformat.AddressPrintFormatter;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;

@Component
public class EmailContent_ClientCollection {
	@Autowired
	private BusinessDetailsService bdServ;
	@Autowired
	private MessageSource messages; 
	@Autowired
	private TemplateEngine templateEngine;
	@Autowired
    private AddressPrintFormatter addressFormatter;

	@Value("${cwms.deployed.url}")
	private String sysDeployedUrl;
	
	public static final String THYMELEAF_VIEW = "/email/portal/client_collection.html";

	public EmailContentDto getContent(Schedule schedule, Locale locale) {
		BusinessDetails bdetails = this.bdServ.getAllBusinessDetails(schedule.getOrganisation());
		String body = getBodyThymeleaf(schedule, bdetails, locale);
		String subject = getSubject(bdetails, locale);
		return new EmailContentDto(body, subject);
	}

	private String getBodyThymeleaf(Schedule schedule, BusinessDetails bdetails, Locale locale) {
        Context context = new Context(locale);
		context.setVariable("schedule", schedule);
		context.setVariable(Constants.SESSION_ATTRIBUTE_BUSINESS_DETAILS, bdetails);
		context.setVariable("addressText", this.addressFormatter.getAddressText(schedule.getAddress(), locale, false));
		String body = this.templateEngine.process(THYMELEAF_VIEW, context);
		return body;
	}

	private String getSubject(BusinessDetails bdetails, Locale locale) {
		return this.messages.getMessage("email.client_collectionrequest.subject", new Object[] {bdetails.getCompany()}, bdetails.getCompany() + " Collection Request", locale);
	}
}
