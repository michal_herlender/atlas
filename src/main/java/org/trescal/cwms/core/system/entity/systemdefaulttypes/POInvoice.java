package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class POInvoice extends SystemDefaultBooleanType
{
	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.PO_INVOICE;
	}
}