package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class CalibrationLabelDateFormat extends SystemDefaultType<String> {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.CALIBRATION_LABEL_DATE_FORMAT;
	}

	@Override
	public boolean isDiscreteType() {
		return true;
	}

	/**
	 * This defines the allowed date formats to use for calibration labels
	 * Only valid values for SimpleDateFormat should appear here, 
	 * and we should limit the list to actual confirmed requirements. 
	 * 
	 * Galen Beck = 2018-09-05
	 */
	@Override
	public List<String> getValues() {
		List<String> result = new ArrayList<>();
		result.add("dd-MM-yyyy");	// Default for most places outside US, should appear first in list 
		result.add("dd MMM yyyy");
		result.add("dd-MMM-yyyy");
		result.add("MM/dd/yyyy");	// Default for US Atlas only
		result.add("MM yyyy");
		result.add("MM-yyyy");
		result.add("MMM yyyy");
		result.add("MMM-yyyy");
		
		return result;
	}

	@Override
	public String parseValue(String value) {
		return value;
	}

	@Override
	public boolean isMultiChoice() {
		return false;
	}

}
