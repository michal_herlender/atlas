package org.trescal.cwms.core.system.entity.systemdefault.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;
import org.trescal.cwms.core.system.enums.Scope;

@Service("SystemDefaultService")
public class SystemDefaultServiceImpl extends BaseServiceImpl<SystemDefault, SystemDefaultNames>
		implements SystemDefaultService {

	@Autowired
	private SystemDefaultDao systemDefaultDao;

	@Override
	protected BaseDao<SystemDefault, SystemDefaultNames> getBaseDao() {
		return systemDefaultDao;
	}

	@Override
	public SystemDefault getByName(String name) {
		return systemDefaultDao.getByName(name);
	}

	@Override
	public List<SystemDefault> getAllByCompanyRoleAndScope(CompanyRole companyRole, Scope scope) {
		return systemDefaultDao.getAllByCompanyRoleAndScope(companyRole, scope);
	}
}