package org.trescal.cwms.core.system.entity.note;

import java.util.Set;

/**
 * Interface that all entities that have {@link Note}(s) attached to them must
 * implement.
 * 
 * @author Richard
 */
public interface NoteAwareEntity
{
	/**
	 * Gets a {@link Set} of all {@link Note}(s) belonging to this entity.
	 * 
	 * @return {@link Set} of all {@link Note}(s).
	 */
	public Set<? extends Note> getNotes();
	
	/**
	 * Get a count of all private active {@link Note}(s) on the entity.
	 * 
	 * @return count.
	 */
	public Integer getPrivateActiveNoteCount();
	
	/**
	 * Get a count of all private deactivated {@link Note}(s) on the entity.
	 * 
	 * @return count.
	 */
	public Integer getPrivateDeactivatedNoteCount();
	
	/**
	 * Get a count of all public active {@link Note}(s) on the entity.
	 * 
	 * @return count.
	 */
	public Integer getPublicActiveNoteCount();
	
	/**
	 * Get a count of all public deactivated {@link Note}(s) on the entity.
	 * 
	 * @return count.
	 */
	public Integer getPublicDeactivatedNoteCount();
}