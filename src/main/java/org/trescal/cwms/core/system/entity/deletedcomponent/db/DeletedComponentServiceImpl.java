package org.trescal.cwms.core.system.entity.deletedcomponent.db;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.system.entity.deletedcomponent.DeletedComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

public class DeletedComponentServiceImpl implements DeletedComponentService
{
	DeletedComponentDao deletedComponentDao;

	public void deleteDeletedComponent(DeletedComponent deletedcomponent)
	{
		this.deletedComponentDao.remove(deletedcomponent);
	}

	public DeletedComponent findDeletedComponent(int id)
	{
		return this.deletedComponentDao.find(id);
	}

	public List<DeletedComponent> getAllDeletedComponents()
	{
		return this.deletedComponentDao.findAll();
	}

	public List<Component> getDeletableComponents()
	{
		return this.deletedComponentDao.getDeletableComponents();
	}

	public void insertDeletedComponent(DeletedComponent DeletedComponent)
	{
		this.deletedComponentDao.persist(DeletedComponent);
	}

	public void saveOrUpdateDeletedComponent(DeletedComponent deletedcomponent)
	{
		this.deletedComponentDao.saveOrUpdate(deletedcomponent);
	}

	public List<DeletedComponent> searchDeletedComponents(Component component, String refNo, Date dateFrom)
	{
		return this.deletedComponentDao.searchDeletedComponents(component, refNo, dateFrom);
	}

	public void setDeletedComponentDao(DeletedComponentDao deletedComponentDao)
	{
		this.deletedComponentDao = deletedComponentDao;
	}

	public void updateDeletedComponent(DeletedComponent DeletedComponent)
	{
		this.deletedComponentDao.update(DeletedComponent);
	}
}