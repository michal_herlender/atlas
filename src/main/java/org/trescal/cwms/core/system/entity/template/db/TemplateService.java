package org.trescal.cwms.core.system.entity.template.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.template.Template;

public interface TemplateService
{
	Template findTemplate(int id);
	Template findByName(String name);
	void insertTemplate(Template template);
	void updateTemplate(Template template);
	List<Template> getAllTemplates();
}