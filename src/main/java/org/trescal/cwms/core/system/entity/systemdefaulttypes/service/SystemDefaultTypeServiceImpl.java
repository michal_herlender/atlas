package org.trescal.cwms.core.system.entity.systemdefaulttypes.service;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefault.db.SystemDefaultService;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.db.SystemDefaultApplicationService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultType;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultTypeDTO;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultTypeResolver;
import org.trescal.cwms.core.system.enums.Scope;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SystemDefaultTypeServiceImpl implements SystemDefaultTypeService {
	@Autowired
	private SystemDefaultTypeResolver sdtResolver;
	@Autowired
	private SystemDefaultService sdService;
	@Autowired
	private SystemDefaultApplicationService sdaService;

	/**
	 * Finds SystemDefaultApplication values for a list of specified SystemDefaults
	 * This allows the hierarchical system default values to be displayed without
	 * separate queries for each default
	 * 
	 * @param company            - mandatory Company to include in search
	 * @param subdiv             - optional Subdiv to include in search
	 * @param contact            - optional Contact to include in search
	 * @param allocatedCompanyId - mandatory company ID of allocated company
	 */
	private Map<SystemDefaultNames, SystemDefaultApplication> findSDAs(List<SystemDefault> listSD, Company company,
			Subdiv subdiv, Contact contact, Integer allocatedCompanyId) {
		val partitioned = listSD.stream().collect(Collectors.partitioningBy(SystemDefault::getGroupwide));
		List<SystemDefault> sublistAllocated = partitioned.get(false);
		List<SystemDefault> sublistGroupwide = partitioned.get(true);
		List<SystemDefaultApplication> valuesAllocated = this.sdaService.find(sublistAllocated, company, subdiv,
			contact, allocatedCompanyId);
		List<SystemDefaultApplication> valuesGroupwide = this.sdaService.find(sublistGroupwide, company, subdiv,
			contact, null);
		List<SystemDefaultApplication> valuesApplicationwide = sdaService.findApplicationWide(listSD);
		Map<SystemDefaultNames, SystemDefaultApplication> mapSDNtoSDA = new HashMap<>();
		addToMap(mapSDNtoSDA, valuesAllocated);
		addToMap(mapSDNtoSDA, valuesGroupwide);
		addToMap(mapSDNtoSDA, valuesApplicationwide);
		return mapSDNtoSDA;
	}

	/**
	 * Adds specified SystemDefaultApplication values to Map, taking precedence from
	 * Contact -> Subdiv -> Company scope as available
	 * 
	 */
	private void addToMap(Map<SystemDefaultNames, SystemDefaultApplication> mapSDNtoSDA,
			List<SystemDefaultApplication> listSDA) {
		for (SystemDefaultApplication sda : listSDA) {
			SystemDefaultNames id = sda.getSystemDefault().getId();
			SystemDefaultApplication existingSda = mapSDNtoSDA.get(id);
			boolean putInMap = true;
			// We want to keep the existingSda in map if it has a narrower scope than the
			// proposed sda
			// e.g. order (0=Contact, 2=Company)
			if (existingSda != null) {
				if (existingSda.getScope().getHierarchicalOrder() < sda.getScope().getHierarchicalOrder()) {
					putInMap = false;
				}
			}
			if (putInMap) {
				mapSDNtoSDA.put(sda.getSystemDefault().getId(), sda);
			}
		}
	}

	/**
	 * Creates a list (in order of the input listSD) of DTOs
	 * 
	 * @param listSD      - List<SystemDefault> to create DTOs for
	 * @param mapSDNtoSDA - Map of SystemDefaultApplication values to be used in DTO
	 *                    list
	 * @return List<SystemDefaultTypeDTO> of values
	 */
	private List<SystemDefaultTypeDTO> createDtos(Locale locale, List<SystemDefault> listSD,
			Map<SystemDefaultNames, SystemDefaultApplication> mapSDNtoSDA) {
		List<SystemDefaultTypeDTO> result = new ArrayList<>();
		for (SystemDefault sd : listSD) {
			SystemDefaultType<?> sdt = this.sdtResolver.getSystemDefaultType(sd.getId());
			SystemDefaultApplication sda = mapSDNtoSDA.get(sd.getId());
			if (sda != null) {
				result.add(createDTO(sdt, sda, locale));
			} else {
				result.add(createDTO(sdt, sd, locale));
			}
		}
		return result;
	}

	public SystemDefaultTypeDTO createDTO(SystemDefaultType<?> type, SystemDefaultApplication sda, Locale locale) {
		return new SystemDefaultTypeDTO(type, locale, sda.getValue(), sda.getId(), sda.getScope());
	}

	public SystemDefaultTypeDTO createDTO(SystemDefaultType<?> type, SystemDefault sd, Locale locale) {

		return new SystemDefaultTypeDTO(type, locale, sd.getDefaultValue(), null, Scope.SYSTEM);

	}

	@Override
	public List<SystemDefaultTypeDTO> getSystemDefaultTypeDTOs(Company company, Integer allocatedCompanyId,
			Locale locale) {
		List<SystemDefault> listSD = this.sdService.getAllByCompanyRoleAndScope(company.getCompanyRole(),
				Scope.COMPANY);
		Map<SystemDefaultNames, SystemDefaultApplication> mapSDNtoSDA = findSDAs(listSD, company, null, null,
				allocatedCompanyId);
		return createDtos(locale, listSD, mapSDNtoSDA);
	}

	@Override
	public List<SystemDefaultTypeDTO> getSystemDefaultTypeDTOs(Subdiv subdiv, Integer allocatedCompanyId,
			Locale locale) {
		List<SystemDefault> listSD = this.sdService.getAllByCompanyRoleAndScope(subdiv.getComp().getCompanyRole(),
				Scope.SUBDIV);
		Map<SystemDefaultNames, SystemDefaultApplication> mapSDNtoSDA = findSDAs(listSD, subdiv.getComp(), subdiv, null,
				allocatedCompanyId);
		return createDtos(locale, listSD, mapSDNtoSDA);
	}

	@Override
	public List<SystemDefaultTypeDTO> getSystemDefaultTypeDTOs(Contact contact, Integer allocatedCompanyId,
			Locale locale) {
		List<SystemDefault> listSD = this.sdService
				.getAllByCompanyRoleAndScope(contact.getSub().getComp().getCompanyRole(), Scope.CONTACT);
		Map<SystemDefaultNames, SystemDefaultApplication> mapSDNtoSDA = findSDAs(listSD, contact.getSub().getComp(),
				contact.getSub(), contact, allocatedCompanyId);
		return createDtos(locale, listSD, mapSDNtoSDA);
	}

}
