package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class CsrEmails extends SystemDefaultBooleanType {

    @Override
    public SystemDefaultNames defaultName() { return SystemDefaultNames.CSR_EMAILS; }

}
