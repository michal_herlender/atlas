package org.trescal.cwms.core.system.entity.servicetype;

import org.hibernate.annotations.SortNatural;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.jobs.job.entity.jobtypeservicetype.JobTypeServiceType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.translation.Translation;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.SortedSet;

@Entity
@Table(name = "servicetype")
public class ServiceType implements Comparable<ServiceType> {

    private int serviceTypeId;
    private String longName;
    private String shortName;
    private Set<Translation> longnameTranslation;
    private SortedSet<Translation> shortnameTranslation;
    private String displayColour;
    private String displayColourFastTrack;
    private Boolean canBePerformedByClient;
    private Boolean repair;
    private DomainType domainType;
    private CalibrationType calibrationType;
    private Integer order;
    private Set<JobTypeServiceType> jobTypes;

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "servicetypeid", nullable = false, unique = true)
	@Type(type = "int")
	public int getServiceTypeId() {
		return serviceTypeId;
	}

	@NotNull
	@Length(max = 100)
	@Column(name = "longname", nullable = false, length = 100)
	public String getLongName() {
		return longName;
	}

	@NotNull
	@Length(max = 10)
	@Column(name = "shortname", nullable = false, length = 10)
	public String getShortName() {
		return shortName;
	}

	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "servicetypelongnametranslation", joinColumns = @JoinColumn(name = "servicetypeid"))
	public Set<Translation> getLongnameTranslation() {
		return longnameTranslation;
	}

	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "servicetypeshortnametranslation", joinColumns = @JoinColumn(name = "servicetypeid"))
	@SortNatural
	public SortedSet<Translation> getShortnameTranslation() {
		return shortnameTranslation;
	}

	@NotNull
	@Length(max = 10)
	@Column(name = "displaycolour", nullable = false, length = 10)
	public String getDisplayColour() {
		return displayColour;
	}

	@NotNull
	@Length(max = 10)
	@Column(name = "displaycolourfasttrack", nullable = false, length = 10)
	public String getDisplayColourFastTrack() {
		return displayColourFastTrack;
	}

	@NotNull
	@Column(name = "canbeperformedbyclient", nullable = false)
	public Boolean getCanBePerformedByClient() {
		return canBePerformedByClient;
	}

	@NotNull
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "domaintype", nullable = false)
	public DomainType getDomainType() {
		return domainType;
	}

	@OneToOne(mappedBy = "serviceType", fetch = FetchType.LAZY)
	public CalibrationType getCalibrationType() {
		return calibrationType;
	}

	@NotNull
	@Column(name = "orderby", unique = true, nullable = false)
	@Type(type = "int")
	public Integer getOrder() {
		return order;
	}

	@NotNull
	@Column(name = "repair", nullable = false)
	public Boolean getRepair() {
		return repair;
	}

	@OneToMany(mappedBy = "serviceType", fetch = FetchType.LAZY)
	public Set<JobTypeServiceType> getJobTypes() {
		return jobTypes;
	}

	public void setDisplayColour(String displayColour) {
		this.displayColour = displayColour;
	}

	public void setDisplayColourFastTrack(String displayColourFastTrack) {
		this.displayColourFastTrack = displayColourFastTrack;
	}

	public void setCanBePerformedByClient(Boolean canBePerformedByClient) {
		this.canBePerformedByClient = canBePerformedByClient;
	}

	public void setDomainType(DomainType domainType) {
		this.domainType = domainType;
	}

	public void setCalibrationType(CalibrationType calibrationType) {
		this.calibrationType = calibrationType;
	}

	@Override
	public int compareTo(ServiceType other) {
		return this.serviceTypeId - other.serviceTypeId;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public void setJobTypes(Set<JobTypeServiceType> jobTypes) {
		this.jobTypes = jobTypes;
	}

	public void setServiceTypeId(int serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	public void setLongName(String longName) {
		this.longName = longName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public void setLongnameTranslation(Set<Translation> longnameTranslation) {
		this.longnameTranslation = longnameTranslation;
	}

	public void setShortnameTranslation(SortedSet<Translation> shortnameTranslation) {
		this.shortnameTranslation = shortnameTranslation;
	}

	public void setRepair(Boolean repair) {
		this.repair = repair;
	}

	/**
	 * used by systemdefault
	 */
	@Override
	public String toString() {
		return this.shortName;
	}
}