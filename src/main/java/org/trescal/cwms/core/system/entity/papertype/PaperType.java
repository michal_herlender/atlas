package org.trescal.cwms.core.system.entity.papertype;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.system.entity.printertray.PrinterTray;

/**
 * Enum identifying the different paper types that can be loaded into
 * {@link PrinterTray}s.
 * 
 * @author jamiev
 */
public enum PaperType
{
	A3_PLAIN, A4_PLAIN, CERT_CONTINUATION, CERT_HEADED, LETTER_CONTINUATION, LETTER_HEADED, TRACEABLE;
	
	private ReloadableResourceBundleMessageSource messages;
		
	@Component
	public static class PaperTypeMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (PaperType rt : EnumSet.allOf(PaperType.class))
               rt.setMessageSource(messages);
        }
	}
	
	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}
	
	public String getDescription() 
	{
		Locale loc = LocaleContextHolder.getLocale();
		switch (super.ordinal()) {
		case 0:
			if (messages != null) 
				return messages.getMessage("paper.a3_plain", null, this.toString(), loc);
		case 1:
			if (messages != null) 
				return messages.getMessage("paper.a4_plain", null, this.toString(), loc);
		case 2:
			if (messages != null) 
				return messages.getMessage("paper.cert_continuation", null, this.toString(), loc);
		case 3:
			if (messages != null) 
				return messages.getMessage("paper.cert_headed", null, this.toString(), loc);
		case 4:
			if (messages != null) 
				return messages.getMessage("paper.letter_continuation", null, this.toString(), loc);
		case 5:
			if (messages != null) 
				return messages.getMessage("paper.letter_headed", null, this.toString(), loc);
		case 6:
			if (messages != null) 
				return messages.getMessage("paper.traceable", null, this.toString(), loc);
		}
		return this.toString();
	}
}
