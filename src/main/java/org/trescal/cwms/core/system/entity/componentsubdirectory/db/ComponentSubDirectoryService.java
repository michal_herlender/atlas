package org.trescal.cwms.core.system.entity.componentsubdirectory.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.componentsubdirectory.ComponentSubDirectory;

public interface ComponentSubDirectoryService
{
	ComponentSubDirectory findComponentSubDirectory(int id);
	ComponentSubDirectory findComponentSubDirectory(int componentId, String name);
	void insertComponentSubDirectory(ComponentSubDirectory componentsubdirectory);
	void updateComponentSubDirectory(ComponentSubDirectory componentsubdirectory);
	List<ComponentSubDirectory> getAllComponentSubDirectorys();
}