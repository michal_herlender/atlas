package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class CustomerCertificateValidation extends SystemDefaultBooleanType {
    @Override
    public SystemDefaultNames defaultName() {
        return SystemDefaultNames.CUSTOMER_CERTIFICATE_ACCEPTANCE;
    }
}
