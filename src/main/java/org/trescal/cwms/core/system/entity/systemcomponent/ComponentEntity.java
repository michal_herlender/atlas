package org.trescal.cwms.core.system.entity.systemcomponent;

import java.io.File;
import java.util.List;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.email.Email;

/**
 * Simple interface implemented by all entites that have a
 * {@link SystemComponent} directory associated with them. Guarantees access to
 * information stored by all components.
 * 
 * @author jamiev
 */
public interface ComponentEntity
{
	public Contact getDefaultContact();

	@Deprecated
	public File getDirectory();

	public String getIdentifier();

	public ComponentEntity getParentEntity();

	public List<Email> getSentEmails();

	public boolean isAccountsEmail();

	@Deprecated
	public void setDirectory(File file);

	public void setSentEmails(List<Email> emails);
}