package org.trescal.cwms.core.system.entity.alligatorsettings.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettings;

public interface AlligatorSettingsDao extends BaseDao<AlligatorSettings, Integer> {
	AlligatorSettings getSettingsForSubdiv(Subdiv businessSubdiv, boolean production);
	AlligatorSettings getSettingsForContact(Contact businessContact, boolean production);
	AlligatorSettings getDefaultSettings();
}
