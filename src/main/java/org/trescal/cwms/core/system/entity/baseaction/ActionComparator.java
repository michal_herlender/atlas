package org.trescal.cwms.core.system.entity.baseaction;

import java.util.Comparator;

public class ActionComparator implements Comparator<BaseAction>
{
	@Override
	public int compare(BaseAction o1, BaseAction o2)
	{
		if (o2.getDate().equals(o1.getDate()))
		{
			return ((Integer) o2.getId()).compareTo(o1.getId());
		}
		return o2.getDate().compareTo(o1.getDate());
	}
}
