package org.trescal.cwms.core.system.entity.alligatorsettingscontact.db;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettingsContact;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettingsContact_;

@Repository
public class AlligatorSettingsContactDaoImpl extends BaseDaoImpl<AlligatorSettingsContact, Integer>
		implements AlligatorSettingsContactDao {

	@Override
	protected Class<AlligatorSettingsContact> getEntity() {
		return AlligatorSettingsContact.class;
	}

	@Override
	public AlligatorSettingsContact findAlligatorSettingsContact(Contact businessContact) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<AlligatorSettingsContact> cq = cb.createQuery(AlligatorSettingsContact.class);
		Root<AlligatorSettingsContact> root = cq.from(AlligatorSettingsContact.class);
		cq.where(cb.equal(root.get(AlligatorSettingsContact_.contact), businessContact));
		
		List<AlligatorSettingsContact> result = getEntityManager().createQuery(cq).getResultList();
		return result.isEmpty() ? null : result.get(0);
	}

}
