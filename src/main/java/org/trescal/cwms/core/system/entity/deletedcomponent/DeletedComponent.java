package org.trescal.cwms.core.system.entity.deletedcomponent;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Entity
@Table(name = "deletedcomponent")
public class DeletedComponent
{
	private Component component;
	private Contact contact;
	private Date date;
	private int id;
	private String reason;
	private String refNo;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "component", nullable = false, unique = false)
	public Component getComponent()
	{
		return this.component;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", unique = false, nullable = false)
	public Contact getContact()
	{
		return this.contact;
	}

	@NotNull
	@Column(name = "date", nullable = false)
	public Date getDate()
	{
		return this.date;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotEmpty
	@Length(max = 200)
	@Column(name = "reason", nullable = false, length = 200)
	public String getReason()
	{
		return this.reason;
	}

	@NotEmpty
	@Length(max = 30)
	@Column(name = "refno", nullable = false, length = 30)
	public String getRefNo()
	{
		return this.refNo;
	}

	public void setComponent(Component component)
	{
		this.component = component;
	}

	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setReason(String reason)
	{
		this.reason = reason;
	}

	public void setRefNo(String refNo)
	{
		this.refNo = refNo;
	}
}