package org.trescal.cwms.core.system.entity.accreditationbodyresource.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.system.entity.accreditationbody.AccreditationBody;
import org.trescal.cwms.core.system.entity.accreditationbodyresource.AccreditationBodyResource;
import org.trescal.cwms.core.system.entity.accreditationbodyresource.AccreditationBodyResourceType;

@Service
public class AccreditationBodyResourceServiceImpl extends
		BaseServiceImpl<AccreditationBodyResource, Integer> implements
		AccreditationBodyResourceService {

	@Autowired
	private AccreditationBodyResourceDao resourceDao;

	@Override
	public AccreditationBodyResource getForBodyAndType(AccreditationBody body,
			AccreditationBodyResourceType type) {
		return this.resourceDao.getForBodyAndType(body, type);
	}

	@Override
	protected BaseDao<AccreditationBodyResource, Integer> getBaseDao() {
		return this.resourceDao;
	}

}
