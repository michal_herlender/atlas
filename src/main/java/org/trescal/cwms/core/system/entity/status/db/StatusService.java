package org.trescal.cwms.core.system.entity.status.db;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.projection.StatusProjectionDTO;
import org.trescal.cwms.core.system.entity.status.Status;

public interface StatusService extends BaseService<Status, Integer>
{
	/**
	 * Return's a {@link List} of all {@link Status} entities that are
	 * 'accepted'. Checks the {@link HttpSession} for a parameter containing the
	 * {@link Status} class type.
	 * 
	 * @return {@link List} of all {@link Status} entities which are accepted.
	 */
	List<Status> ajaxFindAllAcceptedStatus();

	/**
	 * Returns a {@link Status} entity that is flagged as being 'accepted'. If
	 * more than one {@link Status} is found then the first in the list is
	 * returned.
	 * 
	 * @param clazz the entity type.
	 * @return {@link Status} or null if none found.
	 */
	Status findAcceptedStatus(Class<?> clazz);

	<S extends Status> S findAcceptedStatusByClass(Class<S> clazz);

	/**
	 * Return's a {@link List} of all {@link Status} entities that are
	 * 'accepted' for the given entity type.
	 * 
	 * @param clazz the entity type.
	 * @return list of {@link Status}.
	 */
	<S extends Status> List<S> findAllAcceptedStatus(Class<S> clazz);

	<S extends Status> S findDefaultStatus(Class<S> clazz);
	
	<S extends Status> S findFollowingReceiptStatus(Class<S> clazz);
	
	Status findIssuedStatus(Class<?> clazz);

	Status findRejectedStatus(Class<?> clazz);

	Status findRequiresAttentionStatus(Class<?> clazz);

	<S extends Status> S findStatus(int id, Class<S> clazz);

	Status findStatusByName(String name, Class<?> clazz);

	<S extends Status> List<S> getAllStatuss(Class<S> clazz);
	
	void insertStatus(Status status);

	void updateStatus(Status status);

	<S extends Status> List<StatusProjectionDTO> getAllProjectionStatuss(Class<S> clazz, Locale locale);
}