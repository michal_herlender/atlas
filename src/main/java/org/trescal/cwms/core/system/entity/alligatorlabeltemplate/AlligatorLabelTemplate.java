package org.trescal.cwms.core.system.entity.alligatorlabeltemplate;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.enums.AlligatorParameterStyle;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.enums.AlligatorTemplateType;

@Entity
@Table(name="alligatorlabeltemplate")
public class AlligatorLabelTemplate {
	private int id;
	private String description;
	private List<AlligatorLabelParameter> parameters;
	private AlligatorParameterStyle parameterStyle;
	private List<AlligatorLabelTemplateRule> rules;
	private AlligatorTemplateType templateType;
	private String templateXml;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}

	@NotNull
	@Column(name = "description", length = 100, nullable = false, columnDefinition="varchar(100)")
	@Length(max = 100)
	public String getDescription() {
		return description;
	}
	
	@OneToMany(mappedBy="template", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	public List<AlligatorLabelParameter> getParameters() {
		return parameters;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name="parameterstyle", nullable=false)
	public AlligatorParameterStyle getParameterStyle() {
		return parameterStyle;
	}
	
	@OneToMany(mappedBy="template", fetch=FetchType.LAZY, cascade=CascadeType.ALL, orphanRemoval=true)
	public List<AlligatorLabelTemplateRule> getRules() {
		return rules;
	}
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name="templatetype", nullable=false)
	public AlligatorTemplateType getTemplateType() {
		return templateType;
	}
	
	@NotNull
	@Column(name = "templatexml", nullable = false, columnDefinition="varchar(max)")
	public String getTemplateXml() {
		return templateXml;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setParameters(List<AlligatorLabelParameter> parameters) {
		this.parameters = parameters;
	}
	public void setParameterStyle(AlligatorParameterStyle parameterStyle) {
		this.parameterStyle = parameterStyle;
	}
	public void setRules(List<AlligatorLabelTemplateRule> rules) {
		this.rules = rules;
	}
	public void setTemplateType(AlligatorTemplateType templateType) {
		this.templateType = templateType;
	}
	public void setTemplateXml(String templateXml) {
		this.templateXml = templateXml;
	}
}
