package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class ThirdPartyDiscount extends SystemDefaultPercentType {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.THIRD_PARTY_DISCOUNT;
	}
}