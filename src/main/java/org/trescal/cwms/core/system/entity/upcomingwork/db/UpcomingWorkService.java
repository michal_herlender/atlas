package org.trescal.cwms.core.system.entity.upcomingwork.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWorkDto;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface UpcomingWorkService extends BaseService<UpcomingWork, Integer> {
	/**
	 * Adds a new {@link UpcomingWork} entry to the database with the details
	 * provided
	 *
	 * @param upcomingWorkDto
	 * @param subdivId
	 * @return a {@link ResultWrapper} indicating the success of the operation
	 */
	void addUpcomingWork(UpcomingWorkDto upcomingWorkDto, Integer subdivId) throws UpcomingWorkException;
	
	/**
	 * Deletes an existing {@link UpcomingWork} entry
	 * 
	 * @param upcomingId the id of the {@link UpcomingWork} entry
	 */
	void deleteUpcomingWork(Integer upcomingId) throws UpcomingWorkException;
	
	/**
	 * Updates an existing {@link UpcomingWork} entry with the details provided
	 * 
	 * @param upcomingWorkDto the id of the upcoming work to update
	 */
	void updateUpcomingWork(UpcomingWorkDto upcomingWorkDto) throws UpcomingWorkException;

	Long countUpcomingWorkBetweenDates(LocalDate startDate, LocalDate endDate, Integer allocatedSubdivId);

	/**
	 * Returns all active {@link UpcomingWork} entries with a start date or due
	 * date between the given {@link Date}s
	 *
	 * @param startDate the start {@link Date}
	 * @param endDate   the end {@link Date}
	 * @return the {@link List} of {@link UpcomingWork} entries
	 */
	List<UpcomingWork> getUpcomingWorkBetweenDates(LocalDate startDate, LocalDate endDate, Subdiv allocatedSubdiv);
	
	/**
	 * Returns all active and future {@link UpcomingWork} entries owned by the
	 * {@link Contact} with the given ID
	 * 
	 * @param personid the {@link Contact} ID
	 * @return the {@link List} of {@link UpcomingWork} entries
	 */
	List<UpcomingWork> getUpcomingWorkForUser(int personid, Subdiv allocatedSubdiv);
}