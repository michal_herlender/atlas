package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

public abstract class SystemDefaultBooleanType extends SystemDefaultType<Boolean>
{
	@Autowired
	private ReloadableResourceBundleMessageSource messageSource;
	
	@Override
	public boolean isDiscreteType() {
		return true;
	}
	
	@Override
	public boolean isMultiChoice() {
		return false;
	}
	
	@Override
	public List<String> getValues() {
		List<String> values = new ArrayList<>();
		values.add(getValue(false));
		values.add(getValue(true));
		return values;
	}
	
	@Override
	public Boolean parseValue(String value) {
		if (value.equals(messageSource.getMessage("false", null, "false", LocaleContextHolder.getLocale())))
			return false;
		else if (value.equals(messageSource.getMessage("true", null, "true", LocaleContextHolder.getLocale())))
			return true;
		else return Boolean.parseBoolean(value);
	}
	
	public String getValue(Boolean savedValue) {
		return savedValue ? messageSource.getMessage("true", null, "true", LocaleContextHolder.getLocale()) :
			messageSource.getMessage("false", null, "false", LocaleContextHolder.getLocale());
	}
	
	@Override
	public String getValue(String savedValue) {
		return getValue(Boolean.parseBoolean(savedValue));
	}
}