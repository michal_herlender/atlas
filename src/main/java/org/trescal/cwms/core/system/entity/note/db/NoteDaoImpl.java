package org.trescal.cwms.core.system.entity.note.db;

import lombok.val;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrumentnote.InstrumentNote;
import org.trescal.cwms.core.instrument.entity.instrumentnote.InstrumentNote_;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.dto.NoteForInstrumentTooltipDto;

import java.lang.reflect.Field;
import java.util.List;

@Repository("NoteDao")
public class NoteDaoImpl extends BaseDaoImpl<Note, Integer> implements NoteDao {

	@Override
	protected Class<Note> getEntity() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Note findNote(int id, Class<? extends  Note> clazz) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(Note.class);
			val note = cq.from(clazz);
			cq.where(cb.equal(note,id));
			cq.select(note);
			return cq;
		}).orElse(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.trescal.cwms.core.system.entity.note.db.NoteDao#findNotes(java.util
	 * .List, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Note> findNotes(List<Integer> noteids, Class<?> clazz) {
		DetachedCriteria crit = DetachedCriteria.forClass(clazz);
		crit.add(Restrictions.in("noteid", noteids));
		return (List<Note>) crit.getExecutableCriteria(getSession()).list();
	}

	public Object findObject(int id, Class<?> clazz) {
		Criteria crit = getSession().createCriteria(clazz);
		crit.add(Restrictions.idEq(id));
		return crit.uniqueResult();
	}

	@Override
	public Boolean noteExists(Class<?> clazz, Class<?> entityClazz, Integer noteTypeId, String noteContent) {
		String entityClazzName = "";
		Field[] fieldlist = clazz.getDeclaredFields();
		for (Field aField : fieldlist) {
			if (aField.getType().equals(entityClazz)) {
				entityClazzName = aField.getName();
			}
		}
		if (!entityClazzName.equals("")) {
			Criteria crit = getSession().createCriteria(clazz);
			Criteria classCrit = crit.createCriteria(entityClazzName);
			classCrit.add(Restrictions.idEq(noteTypeId));
			crit.add(Restrictions.eq("note", noteContent));
			crit.add(Restrictions.eq("active", true));
			return crit.uniqueResult() != null;
		} else {
			return false;
		}
	}

    @Override
    public List<NoteForInstrumentTooltipDto> findNotesForInstrumentTooltipByPlantId(Integer plantId) {
	    return getResultList(cb -> {
	    	val cq = cb.createQuery(NoteForInstrumentTooltipDto.class);
	    	val note = cq.from(InstrumentNote.class);
	    	cq.where(cb.equal(note.get(InstrumentNote_.instrument),plantId));
	    	cq.select(cb.construct(NoteForInstrumentTooltipDto.class,
					note.get(InstrumentNote_.label),
					note.get(InstrumentNote_.note)
					));
	    	return cq;
		});
    }

    public void insertNote(Note note) {
		getEntityManager().persist(note);
	}


}