package org.trescal.cwms.core.system.entity.accreditationbody.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.system.entity.accreditationbody.AccreditationBody;

public interface AccreditationBodyService extends BaseService<AccreditationBody, Integer> {
}
