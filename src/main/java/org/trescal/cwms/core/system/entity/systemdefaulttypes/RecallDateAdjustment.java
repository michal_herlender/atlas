package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

/**
 * System default exported / used by external tools
 * (e.g. Calypso cal tool in USA)
 * Will also use in internal recall date calculation
 */
@Component
public class RecallDateAdjustment extends SystemDefaultType<String> {

	public final String NO = "No"; 
	public final String FIRST_DAY = "First day of month"; 
	public final String LAST_DAY = "Last day of month"; 
	private final List<String> values;
	
	public RecallDateAdjustment() {
		this.values = new ArrayList<>();
		this.values.add(NO);
		this.values.add(FIRST_DAY);
		this.values.add(LAST_DAY);
	}
	
	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.RECALL_DATE_ADJUSTMENT;
	}

	@Override
	public boolean isDiscreteType() {
		return true;
	}

	@Override
	public boolean isMultiChoice() {
		return false;
	}

	@Override
	public List<String> getValues() {
		return values;
	}

	@Override
	public String parseValue(String value) {
		return value;
	}

}
