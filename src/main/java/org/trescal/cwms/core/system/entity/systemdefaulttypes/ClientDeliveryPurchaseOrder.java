package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class ClientDeliveryPurchaseOrder extends SystemDefaultBooleanType {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.CLIENT_DELIVERY_PURCHASE_ORDER;
	}

}
