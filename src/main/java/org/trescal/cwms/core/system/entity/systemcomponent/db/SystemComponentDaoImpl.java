package org.trescal.cwms.core.system.entity.systemcomponent.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent_;

@Repository("SystemComponentDao")
public class SystemComponentDaoImpl extends BaseDaoImpl<SystemComponent, Integer> implements SystemComponentDao {
	
	@Override
	protected Class<SystemComponent> getEntity() {
		return SystemComponent.class;
	}

	@Override
	public SystemComponent findComponent(Component component) {
		return getFirstResult(cb -> {
			CriteriaQuery<SystemComponent> cq = cb.createQuery(SystemComponent.class);
			Root<SystemComponent> systemComponent = cq.from(SystemComponent.class);
			cq.where(cb.equal(systemComponent.get(SystemComponent_.component), component));
			return cq;
		}).orElse(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public SystemComponent find(Integer componentId) {
		Criteria crit = getSession().createCriteria(SystemComponent.class);
		crit.add(Restrictions.idEq(componentId));
		crit.setFetchMode("subDirs", FetchMode.JOIN);
		crit.setFetchMode("fileNameRegexs", FetchMode.JOIN);
		List<SystemComponent> list = crit.list();
		return list.size() > 0 ? list.get(0) : null;
	}
	
	@SuppressWarnings("unchecked")
	public SystemComponent findComponent(String componentName) {
		DetachedCriteria scCriteria = DetachedCriteria.forClass(SystemComponent.class);
		scCriteria.add(Restrictions.eq("componentName", componentName));
		scCriteria.setFetchMode("subDirs", FetchMode.JOIN);
		List<SystemComponent> coms = (List<SystemComponent>) scCriteria.getExecutableCriteria(getSession()).list();
		return coms.size() > 0 ? coms.get(0) : null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public SystemComponent findEagerComponent(Component component) {
		Criteria crit = getSession().createCriteria(SystemComponent.class);
		crit.add(Restrictions.eq("component", component));
		crit.setFetchMode("subDirs", FetchMode.JOIN);
		List<SystemComponent> list = (List<SystemComponent>) crit.list();
		return list.size() > 0 ? list.get(0) : null;
	}
	
	@Override
	public <E> E findEntity(Class<E> clazz, int id) {
		return getEntityManager().find(clazz, id);
	}
	
	@Override
	public List<Component> getComponentsUsingEmailTemplates() {
		Criteria crit = getSession().createCriteria(SystemComponent.class);
		crit.add(Restrictions.eq("emailTemplates", true));
		crit.setProjection(Projections.distinct(Projections.projectionList().add(Projections.property("component"))));
		@SuppressWarnings("unchecked")
		List<Component> results = crit.list(); 
		return results;
	}
}