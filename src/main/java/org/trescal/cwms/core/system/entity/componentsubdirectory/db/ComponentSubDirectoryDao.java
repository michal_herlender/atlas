package org.trescal.cwms.core.system.entity.componentsubdirectory.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.componentsubdirectory.ComponentSubDirectory;

public interface ComponentSubDirectoryDao extends BaseDao<ComponentSubDirectory, Integer> {
	
	ComponentSubDirectory findComponentSubDirectory(int componentId, String name);
}