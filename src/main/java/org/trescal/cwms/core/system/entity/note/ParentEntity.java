package org.trescal.cwms.core.system.entity.note;

public interface ParentEntity<NoteEntity> {
	NoteEntity getEntity();
	void setEntity(NoteEntity entity);
}
