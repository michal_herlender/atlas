package org.trescal.cwms.core.system.entity.instruction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

@Entity
@Table(name = "baseinstruction")
public class Instruction extends Versioned {

	private Boolean includeOnDelNote;
	private Boolean includeOnSupplierDelNote;
	private String instruction;
	private int instructionid;
	private InstructionType instructiontype;

	/**
	 * Indicates whether to show the instruction on a client delivery note.
	 * Optional field, may be null for any instruction type.
	 */
	@Column(name = "includeondelnote", columnDefinition = "bit")
	public Boolean getIncludeOnDelNote() {
		return this.includeOnDelNote;
	}

	/**
	 * Indicates whether to show the instruction on a supplier (or internal) delivery note.
	 * Optional field, may be null for any instruction type.
	 */
	@Column(name = "includeonsupplierdelnote", columnDefinition = "bit")
	public Boolean getIncludeOnSupplierDelNote() {
		return this.includeOnSupplierDelNote;
	}

	@NotNull
	@Length(min = 1, max = 1000)
	@Column(name = "instruction", length = 1000)
	public String getInstruction() {
		return this.instruction;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "instructionid")
	public int getInstructionid() {
		return this.instructionid;
	}

	@NotNull
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "instructiontypeid")
	public InstructionType getInstructiontype() {
		return this.instructiontype;
	}

	public void setIncludeOnDelNote(Boolean includeOnDelNote) {
		this.includeOnDelNote = includeOnDelNote;
	}

	public void setIncludeOnSupplierDelNote(Boolean includeOnSupplierDelNote) {
		this.includeOnSupplierDelNote = includeOnSupplierDelNote;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public void setInstructionid(int instructionid) {
		this.instructionid = instructionid;
	}

	public void setInstructiontype(InstructionType instructiontype) {
		this.instructiontype = instructiontype;
	}
}