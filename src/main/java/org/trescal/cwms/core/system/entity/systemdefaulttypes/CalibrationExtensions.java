package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class CalibrationExtensions extends SystemDefaultBooleanType {

    @Override
    public SystemDefaultNames defaultName() {
        return SystemDefaultNames.CAL_EXTENSION;
    }
}
