package org.trescal.cwms.core.system.entity.instructiontype;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.system.entity.instruction.Instruction;

/**
 * Lookup entity which defines the different types of {@link Instruction}
 * available on the system. This could/should be turned into an enum?
 */
public enum InstructionType {
	UNDEFINED("instructiontype.undefined"),	// Undefined type is for enum mapping 0; not intended for use
	GENERAL("instructiontype.general"),
	PACKAGING("instructiontype.packaging"),
	CARRIAGE("instructiontype.carriage"),
	DISCOUNT("instructiontype.discount"),
	CALIBRATION("instructiontype.calibration"),
	REPAIRANDADJUSTMENT("instructiontype.repairsandadjustment"),
	COSTING("instructiontype.costing"),
	INVOICE("instructiontype.invoice"),
	HIRE("instructiontype.hire"),
	PURCHASE("instructiontype.purchase"),
	RECEIPT("instructiontype.receipt");

	private String messageCode;
	private ReloadableResourceBundleMessageSource messages;

	private InstructionType(String messageCode) {
		this.messageCode = messageCode;
	}

	@Component
	public static class InstructionTypeMessageSourceInjector {

		@Autowired
		private ReloadableResourceBundleMessageSource messages;

		@PostConstruct
		public void postConstruct() {
			for (InstructionType rt : EnumSet.allOf(InstructionType.class))
				rt.setMessageSource(messages);
		}
	}

	public void setMessageSource(ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}

	public String getType() {
		Locale locale = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.name(), locale);
		} else {
			return this.name();
		}
	}

	public String getMessageCode() {
		return messageCode;
	}
	
	public static EnumSet<InstructionType> getActiveTypes() {
		return EnumSet.complementOf(EnumSet.of(InstructionType.UNDEFINED));
	}
}