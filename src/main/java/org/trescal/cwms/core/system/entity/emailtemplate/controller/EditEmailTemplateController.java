package org.trescal.cwms.core.system.entity.emailtemplate.controller;

import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.emailtemplate.EmailTemplate;
import org.trescal.cwms.core.system.entity.emailtemplate.db.EmailTemplateService;
import org.trescal.cwms.core.system.entity.emailtemplate.form.EmailTemplateForm;
import org.trescal.cwms.core.system.entity.emailtemplate.form.EmailTemplateValidator;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@Controller @IntranetController
@RequestMapping(value="editemailtemplate.htm")
public class EditEmailTemplateController {
	@Autowired
	private EmailTemplateService emailTemplateService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private SystemComponentService systemComponentService;
	
	@Autowired
	private EmailTemplateValidator validator;
	
	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	public static final String VIEW_NAME = "trescal/core/system/addemailtemplate";
	public static final String REDIRECT_URL = "redirect:viewemailtemplates.htm";
	public static final String FORM_NAME = "form";
	
	@ModelAttribute(FORM_NAME)
	public EmailTemplateForm formBackingObject(Locale locale, @RequestParam(name="id", required=true) int id) {
		EmailTemplate template = this.emailTemplateService.get(id);
		
		EmailTemplateForm form = new EmailTemplateForm();
		form.setCompid(template.getOrganisation().getId());
		form.setComponent(template.getComponent());
		form.setId(template.getId());
		form.setLanguageTag(template.getLocale().toLanguageTag());
		if (template.getSubdiv() != null) {
			form.setSubdivid(template.getSubdiv().getSubdivid());
		}
		form.setSubject(template.getSubject());
		form.setTemplate(template.getTemplate());
		return form;
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public String referenceData(Model model, Locale displayLocale, @RequestParam(name="id", required=true) int id) {
		EmailTemplate template = this.emailTemplateService.get(id);
		
		int coid = template.getOrganisation().getCoid();
		model.addAttribute("subdivs", this.subdivService.getAllActiveCompanySubdivsKeyValue(coid, true));
		List<Locale> locales = this.supportedLocaleService.getSupportedLocales();
		model.addAttribute("locales", this.supportedLocaleService.getDTOList(locales, displayLocale));
		model.addAttribute("components", this.systemComponentService.getComponentsUsingEmailTemplates());
		
		return VIEW_NAME;
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String onSubmit(Model model, Locale displayLocale, @RequestParam(name="id", required=true) int id,
			@Valid @ModelAttribute(FORM_NAME) EmailTemplateForm form, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData(model, displayLocale, id);
		}
		EmailTemplate template = this.emailTemplateService.get(id);
		template.setComponent(form.getComponent());
		template.setLocale(Locale.forLanguageTag(form.getLanguageTag()));
		if (form.getSubdivid() == 0) {
			template.setSubdiv(null);
		}
		else {
			template.setSubdiv(this.subdivService.get(form.getSubdivid()));
		}
		template.setSubject(form.getSubject());
		template.setTemplate(form.getTemplate());
		
		this.emailTemplateService.save(template);
		return REDIRECT_URL;
	}
}
