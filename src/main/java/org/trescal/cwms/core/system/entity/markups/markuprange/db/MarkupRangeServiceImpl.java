package org.trescal.cwms.core.system.entity.markups.markuprange.db;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRange;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRangeAction;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRangeWrapper;
import org.trescal.cwms.core.system.entity.markups.markuptype.MarkupType;
import org.trescal.cwms.core.tools.MathTools;

public class MarkupRangeServiceImpl implements MarkupRangeService
{
	protected final Log logger = LogFactory.getLog(this.getClass());
	MarkupRangeDao MarkupRangeDao;

	@Override
	public ResultWrapper addMarkupRange(MarkupType type, double rangeStart, double value, MarkupRangeAction action)
	{
		// create a new results wrapper
		ResultWrapper wrapper = null;

		if (type == null)
		{
			wrapper = new ResultWrapper(false, "Could not find Markup type", null, null);
		}
		else
		{
			// make sure no other markup ranges of this type exist that start at
			// the same range
			List<MarkupRange> range = this.findMarkupRange(type, rangeStart);
			if (range.size() > 0)
			{
				wrapper = new ResultWrapper(false, "A markup range starting at "
						+ rangeStart + " already exists.", null, null);
			}
			else
			{
				MarkupRange mRange = new MarkupRange();
				mRange.setAction(action);
				mRange.setActionValue(value);
				mRange.setRangeStart(rangeStart);
				mRange.setType(type);
				this.insertMarkupRange(mRange);
				wrapper = new ResultWrapper(true, "Markup range saved.", mRange, null);
			}
		}
		return wrapper;
	}

	@Override
	public BigDecimal calculateMarkUp(BigDecimal value, MarkupType type)
	{
		MarkupRangeWrapper wrapper = this.calculateMarkUpRangeValue(value, type);
		return wrapper.getValue();
	}

	@Override
	public MarkupRangeWrapper calculateMarkUpRangeValue(BigDecimal value, MarkupType type)
	{
		MarkupRangeWrapper wrapper = new MarkupRangeWrapper();
		wrapper.setValue(new BigDecimal("0.00"));

		List<MarkupRange> ranges = this.getAllMarkupRanges(type);

		// only apply markups to actual positive (> 0) values
		if ((value != null) && (value.doubleValue() > 0.00))
		{
			// find the markup range that applies to this value
			MarkupRange thisRange = null;
			for (MarkupRange range : ranges)
			{
				thisRange = range;
				if (range.getRangeStart() <= value.doubleValue())
				{
					break;
				}
			}

			// calculate the range to be applied
			if (thisRange != null)
			{
				this.logger.info("Found matching markup range, using range action: "
						+ thisRange.getAction()
						+ " for value "
						+ value
						+ " on range type " + type.getName());

				double d = thisRange.getActionValue();

				// this is the range that we want to use
				if (thisRange.getAction() == MarkupRangeAction.PERCENTAGE)
				{
					// markup has to be calculated as a percentage of the given
					// value
					BigDecimal dec = new BigDecimal(Double.toString(d)).divide(new BigDecimal("100.00"), MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
					wrapper.setValue(value.multiply(dec).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));
					wrapper.setRange(thisRange);
				}
				else if (thisRange.getAction() == MarkupRangeAction.VALUE)
				{
					// markup is a set value for this range
					wrapper.setValue(new BigDecimal(Double.toString(d)));
					wrapper.setRange(thisRange);
				}
			}
			else
			{
				this.logger.info("No applicable markup range found, returning default values");
			}
		}

		return wrapper;
	}

	public void deleteMarkupRange(MarkupRange markuprange)
	{
		this.MarkupRangeDao.remove(markuprange);
	}

	public MarkupRange findMarkupRange(int id)
	{
		return this.MarkupRangeDao.find(id);
	}

	@Override
	public List<MarkupRange> findMarkupRange(MarkupType type, double rangestart)
	{
		return this.MarkupRangeDao.findMarkupRange(type, rangestart);
	}

	public List<MarkupRange> getAllMarkupRanges()
	{
		return this.MarkupRangeDao.findAll();
	}

	@Override
	public List<MarkupRange> getAllMarkupRanges(MarkupType type)
	{
		return this.MarkupRangeDao.getAllMarkupRanges(type);
	}

	public void insertMarkupRange(MarkupRange MarkupRange)
	{
		this.MarkupRangeDao.persist(MarkupRange);
	}

	public void saveOrUpdateMarkupRange(MarkupRange markuprange)
	{
		this.MarkupRangeDao.saveOrUpdate(markuprange);
	}

	public void setMarkupRangeDao(MarkupRangeDao MarkupRangeDao)
	{
		this.MarkupRangeDao = MarkupRangeDao;
	}

	@Override
	public ResultWrapper updateMarkupRange(int id, double rangeStart, double value)
	{
		// create a new results wrapper
		ResultWrapper wrapper = null;

		MarkupRange range = this.findMarkupRange(id);
		if (range == null)
		{
			wrapper = new ResultWrapper(false, "Could not find Markup range", null, null);
		}
		else
		{
			// make sure no other markup ranges of this type exist that start at
			// the same range
			List<MarkupRange> ranges = this.findMarkupRange(range.getType(), rangeStart);
			if (ranges.size() > 0)
			{
				wrapper = new ResultWrapper(false, "A markup range starting at "
						+ rangeStart + " already exists.", null, null);
			}
			else
			{
				range.setActionValue(value);
				range.setRangeStart(rangeStart);
				this.updateMarkupRange(range);
				wrapper = new ResultWrapper(true, "Markup range saved.", range, null);
			}
		}
		return wrapper;
	}

	public void updateMarkupRange(MarkupRange MarkupRange)
	{
		this.MarkupRangeDao.update(MarkupRange);
	}
}