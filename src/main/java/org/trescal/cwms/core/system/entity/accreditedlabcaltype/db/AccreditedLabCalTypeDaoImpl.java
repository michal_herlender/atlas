package org.trescal.cwms.core.system.entity.accreditedlabcaltype.db;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.core.system.entity.accreditedlabcaltype.AccreditedLabCalType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Repository("AccreditedLabCalTypeDao")
public class AccreditedLabCalTypeDaoImpl extends BaseDaoImpl<AccreditedLabCalType, Integer> implements AccreditedLabCalTypeDao {
	
	@Override
	protected Class<AccreditedLabCalType> getEntity() {
		return AccreditedLabCalType.class;
	}
	
	@Override
	public AccreditedLabCalType findAccreditedLabCalType(AccreditedLab accreditedLab, CalibrationType calType) {
		Criteria crit = getSession().createCriteria(AccreditedLabCalType.class);
		crit.add(Restrictions.eq("accreditedLab", accreditedLab));
		crit.add(Restrictions.eq("calType", calType));
		return (AccreditedLabCalType) crit.uniqueResult();
	}
}