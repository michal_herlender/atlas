package org.trescal.cwms.core.system.entity.basestatus.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.system.entity.basestatus.BaseStatus;

@Service("BaseStatusService")
public class BaseStatusServiceImpl extends BaseServiceImpl<BaseStatus, Integer> implements BaseStatusService
{
	@Autowired
	private BaseStatusDao baseStatusDao;
	
	@Override
	protected BaseDao<BaseStatus, Integer> getBaseDao() {
		return baseStatusDao;
	}
	
	@Override
	public <Status extends BaseStatus> Status  findDefaultStatus(Class<Status> clazz) {
		return this.baseStatusDao.findDefaultStatus(clazz);
	}
	
	@Override
	public <Status extends BaseStatus> Status  findStatusByName(String name, Class<Status> clazz) {
		return this.baseStatusDao.findStatusByName(name, clazz);
	}
}