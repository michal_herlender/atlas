package org.trescal.cwms.core.system.entity.alligatorlabeltemplate.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.AlligatorLabelTemplate;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.enums.AlligatorTemplateType;

@Service
public class AlligatorLabelTemplateServiceImpl extends BaseServiceImpl<AlligatorLabelTemplate, Integer>
		implements AlligatorLabelTemplateService {

	@Autowired
	private AlligatorLabelTemplateDao baseDao;
	
	@Override
	protected BaseDao<AlligatorLabelTemplate, Integer> getBaseDao() {
		return baseDao;
	}
	
	@Override
	public AlligatorLabelTemplate findLabelTemplate(Company businessCompany, AlligatorTemplateType type) {
		return baseDao.findLabelTemplate(businessCompany, type);
	}

}
