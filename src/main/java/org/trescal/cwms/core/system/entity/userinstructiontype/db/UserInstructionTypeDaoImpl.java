package org.trescal.cwms.core.system.entity.userinstructiontype.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.userinstructiontype.UserInstructionType;

@Repository("UserInstructionTypeDao")
public class UserInstructionTypeDaoImpl extends BaseDaoImpl<UserInstructionType, Integer> implements UserInstructionTypeDao {
	
	@Override
	protected Class<UserInstructionType> getEntity() {
		return UserInstructionType.class;
	}
}