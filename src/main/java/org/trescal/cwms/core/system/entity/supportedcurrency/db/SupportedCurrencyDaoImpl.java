package org.trescal.cwms.core.system.entity.supportedcurrency.db;

import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Repository("SupportedCurrencyDao")
public class SupportedCurrencyDaoImpl extends BaseDaoImpl<SupportedCurrency, Integer> implements SupportedCurrencyDao {
	
	@Override
	protected Class<SupportedCurrency> getEntity() {
		return SupportedCurrency.class;
	}

	public SupportedCurrency findCurrencyByCode(String currencyCode) {
		return getFirstResult(cb -> {
			CriteriaQuery<SupportedCurrency> cq = cb.createQuery(SupportedCurrency.class);
			Root<SupportedCurrency> root = cq.from(SupportedCurrency.class);
			cq.where(cb.equal(root.get(SupportedCurrency_.currencyCode), currencyCode));
			return cq;
		}).orElse(null);

	}

	public SupportedCurrency findSupportedCurrency(int id) {
		return getFirstResult(cb -> {
			CriteriaQuery<SupportedCurrency> cq = cb.createQuery(SupportedCurrency.class);
			Root<SupportedCurrency> root = cq.from(SupportedCurrency.class);
			cq.where(cb.equal(root.get(SupportedCurrency_.currencyId), id));
			return cq;
		}).orElse(null);

	}

	/**
	 * Find the currency that matches the given code else load the system
	 * default currency.
	 */
	public SupportedCurrency findSupportedCurrencyOrDefault(String currencyCode, String defaultCurrencyCode) {
		return getFirstResult(cb -> {
			CriteriaQuery<SupportedCurrency> cq = cb.createQuery(SupportedCurrency.class);
			Root<SupportedCurrency> root = cq.from(SupportedCurrency.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isTrue(root.get(SupportedCurrency_.rateSet)));
			clauses.getExpressions().add(cb.equal(root.get(SupportedCurrency_.currencyCode), currencyCode));
			cq.where(clauses);
			return cq;
		}).orElse(this.findCurrencyByCode(defaultCurrencyCode));

	}

	public List<SupportedCurrency> getAllSupportedCurrenciesWithExRates() {
		return getResultList(cb -> {
			CriteriaQuery<SupportedCurrency> cq = cb.createQuery(SupportedCurrency.class);
			Root<SupportedCurrency> root = cq.from(SupportedCurrency.class);
			cq.where(cb.isTrue(root.get(SupportedCurrency_.rateSet)));

			cq.orderBy(cb.asc(root.get(SupportedCurrency_.orderBy)), cb.asc(root.get(SupportedCurrency_.currencyCode)));
			return cq;
		});

	}

	public List<SupportedCurrency> getAllSupportedCurrencys() {
		return getResultList(cb -> {
			CriteriaQuery<SupportedCurrency> cq = cb.createQuery(SupportedCurrency.class);
			Root<SupportedCurrency> root = cq.from(SupportedCurrency.class);
			cq.orderBy(cb.asc(root.get(SupportedCurrency_.orderBy)), cb.asc(root.get(SupportedCurrency_.currencyCode)));
			return cq;
		});

	}

	@Override
	public List<KeyValueIntegerString> getKeyValueList(Collection<Integer> currencyIds) {
		if (currencyIds == null || currencyIds.isEmpty())
			throw new UnsupportedOperationException("At least one currencyId must be provided");
		return getResultList(cb -> {
			CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class);
			Root<SupportedCurrency> root = cq.from(SupportedCurrency.class);
			cq.where(root.get(SupportedCurrency_.currencyId).in(currencyIds));
			cq.select(cb.construct(KeyValueIntegerString.class, root.get(SupportedCurrency_.currencyId),
					root.get(SupportedCurrency_.currencyCode)));
			return cq;
		});
	}

	public void insertSupportedCurrency(SupportedCurrency supportedcurrency) {
		this.persist(supportedcurrency);
	}

	public void updateSupportedCurrency(SupportedCurrency supportedcurrency) {
		this.update(supportedcurrency);
	}

	@Override
	public KeyValueIntegerString getKeyValue(Integer currencyId) {
		return getFirstResult(cb -> {
			CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class);
			Root<SupportedCurrency> root = cq.from(SupportedCurrency.class);
			cq.where(cb.equal(root.get(SupportedCurrency_.currencyId), currencyId));
			cq.select(cb.construct(KeyValueIntegerString.class, root.get(SupportedCurrency_.currencyId),
					root.get(SupportedCurrency_.currencyCode)));
			return cq;
		}).orElse(null);
	}
}