package org.trescal.cwms.core.system.projection.note.service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.system.projection.note.NoteProjectionDTO;

@Service
public class NoteProjectionServiceImpl implements NoteProjectionService {
	
	@Autowired
	private NoteProjectionDao noteProjectionDao;
	
	@Override
	public List<NoteProjectionDTO> getDeliveryItemNotes(Integer deliveryId) {
		return noteProjectionDao.getDeliveryItemNotes(deliveryId);
	}
	
	@Override
	public List<NoteProjectionDTO> getDeliveryNotes(Collection<Integer> deliveryIds) {
		List<NoteProjectionDTO> result = Collections.emptyList();
		if (!deliveryIds.isEmpty()) {
			result = noteProjectionDao.getDeliveryNotes(deliveryIds); 
		}
		return result;
	}
}
