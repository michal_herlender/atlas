package org.trescal.cwms.core.system.entity.printertray;

import javax.print.attribute.standard.MediaTray;

public enum MediaTrayRef
{
	DEFAULT(MediaTray.MAIN), TOP(MediaTray.TOP), MIDDLE(MediaTray.MIDDLE), BOTTOM(MediaTray.BOTTOM);

	private MediaTray mediaTray;

	MediaTrayRef(MediaTray mediaTray)
	{
		this.mediaTray = mediaTray;
	}

	public MediaTray getMediaTray()
	{
		return this.mediaTray;
	}
}