package org.trescal.cwms.core.system.entity.accreditedlab.db;

import lombok.val;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToSubdivDaoImpl;
import org.trescal.cwms.core.system.entity.accreditationbody.AccreditationBody_;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab_;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import java.util.Set;
import java.util.TreeSet;

import static org.trescal.cwms.core.tools.StringJpaUtils.trimAndConcatWithWhitespace;

@Repository
public class AccreditedLabDaoImpl extends AllocatedToSubdivDaoImpl<AccreditedLab, Integer> implements AccreditedLabDao {
    @Override
    protected Class<AccreditedLab> getEntity() {
        return AccreditedLab.class;
    }

    @Override
    public Set<KeyValue<Integer, String>> getAllBySubdivId(Integer subdivId) {

        return new TreeSet<>(getResultList(cb -> {
            val cq = cb.createQuery(KeyValueIntegerString.class);
            val accredited = cq.from(AccreditedLab.class);
            cq.select(cb.construct(KeyValueIntegerString.class,
                accredited.get(AccreditedLab_.id),
                trimAndConcatWithWhitespace(
                    accredited.join(AccreditedLab_.accreditationBody).get(AccreditationBody_.shortName),
                    cb.literal(":")
                ).append(accredited.get(AccreditedLab_.labNo)).append(cb.literal(":"))
                    .append(accredited.get(AccreditedLab_.description)).apply(cb)

            ));
            return cq;
        }));
    }
}
