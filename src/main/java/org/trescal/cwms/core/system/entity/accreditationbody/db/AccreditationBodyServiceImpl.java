package org.trescal.cwms.core.system.entity.accreditationbody.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.system.entity.accreditationbody.AccreditationBody;

@Service
public class AccreditationBodyServiceImpl extends
		BaseServiceImpl<AccreditationBody, Integer> implements
		AccreditationBodyService {

	@Autowired
	private AccreditationBodyDao accreditationBodyDao;

	@Override
	protected BaseDao<AccreditationBody, Integer> getBaseDao() {
		return this.accreditationBodyDao;
	}
}
