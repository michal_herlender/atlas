package org.trescal.cwms.core.system.entity.baseaction;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.contact.Contact;

/**
 * Base class for entites that log actions.
 * 
 * @author Richard
 */
@MappedSuperclass
public abstract class BaseAction
{
	private int id;
	private Contact contact;
	private Date date;

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", unique = false, nullable = false)
	public Contact getContact()
	{
		return this.contact;
	}

	@NotNull
	@Column(name = "date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDate()
	{
		return this.date;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public void setId(int id)
	{
		this.id = id;
	}
}
