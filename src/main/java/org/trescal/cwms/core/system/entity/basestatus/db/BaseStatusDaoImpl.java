package org.trescal.cwms.core.system.entity.basestatus.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.basestatus.BaseStatus;
import org.trescal.cwms.core.system.entity.basestatus.BaseStatus_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository("BaseStatusDao")
public class BaseStatusDaoImpl extends BaseDaoImpl<BaseStatus, Integer> implements BaseStatusDao {

    @Override
    protected Class<BaseStatus> getEntity() {
        return BaseStatus.class;
    }

    @Override
    public <Status extends BaseStatus> Status findDefaultStatus(Class<Status> clazz) {
        return getFirstResult(cb -> {
            CriteriaQuery<Status> cq = cb.createQuery(clazz);
            final Root<Status> statusRoot = cq.from(clazz);
            cq.where(cb.isTrue(statusRoot.get(BaseStatus_.defaultStatus)));
            return cq;
        }).orElse(null);
    }

    @Override
    public <Status extends BaseStatus> Status findStatusByName(String name, Class<Status> clazz) {
        return getSingleResult(cb -> {
            CriteriaQuery<Status> cq = cb.createQuery(clazz);
            final Root<Status> statusRoot = cq.from(clazz);
            cq.where(cb.equal(statusRoot.get(BaseStatus_.name), name));
            return cq;
        });
    }
}