package org.trescal.cwms.core.system.entity.calibrationaccreditationlevel;

import java.util.Comparator;

import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;

/**
 * Implmentation of {@link Comparator} which sorts
 * {@link CalibrationAccreditationLevel} entities by their
 * {@link AccreditationLevel}.
 * 
 * @author Richard
 */
public class CalibrationAccreditationLevelComparator implements Comparator<CalibrationAccreditationLevel>
{
	@Override
	public int compare(CalibrationAccreditationLevel o1, CalibrationAccreditationLevel o2)
	{
		if ((o1.getLevel() == null) && (o2.getLevel() != null))
		{
			return 1;
		}
		else if ((o1.getLevel() != null) && (o2.getLevel() == null))
		{
			return -1;
		}
		else
		{
			if (o1.getLevel().getLevel() == o2.getLevel().getLevel())
			{
				return ((Integer) o1.getId()).compareTo(o2.getId());
			}
			else
			{
				return ((Integer) o1.getLevel().getLevel()).compareTo(o2.getLevel().getLevel());
			}
		}
	}
}
