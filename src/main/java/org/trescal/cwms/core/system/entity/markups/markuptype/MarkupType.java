/**
 * 
 */
package org.trescal.cwms.core.system.entity.markups.markuptype;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

/**
 * Defines the different types of markup rates that are in use accross the
 * appplication.
 * 
 * @author Richard
 */
public enum MarkupType
{
	SUPPLIER(1, "markuptype.supplier", "Supplier", "markuptype.descsupplier", "Markup rates applied to supplier costs."), 
	CARRIAGE(2, "markuptype.carriage", "Carriage", "markuptype.desccarriage", "Markup rates applied to carriage costs."), 
	INSPECTION(3, "markuptype.inspection", "Inspection", "markuptype.descinspection", "Calculated inspection charges.");

	private int id;
	private ReloadableResourceBundleMessageSource messages;
	private Locale loc = null;
	private String messageCodeName;
	private String messageCodeDesc;
	private String name;
	private String description;
		
	@Component
	public static class MarkupTypeMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (MarkupType rt : EnumSet.allOf(MarkupType.class))
               rt.setMessageSource(messages);
        }
	}
	
	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}

	private MarkupType(int id, String messageCodeName, String name, String messageCodeDesc, String description) {
		this.id = id;
		this.messageCodeName = messageCodeName;
		this.name = name;
		this.messageCodeDesc = messageCodeDesc;
		this.description = description;	
	}
	
	public String getDescription()
	{
		loc = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCodeDesc, null, this.description, loc);
		}
		return this.toString();
	}

	public int getId()
	{
		return this.id;
	}

	public String getName()
	{
		loc = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCodeName, null, this.name, loc);
		}
		return this.toString();
	}
	
	public static MarkupType getMarkupTypeById(int id) {
		switch (id) {
		case 1: return SUPPLIER;
		case 2: return CARRIAGE;
		case 3: return INSPECTION;
		}
		return null;
	}
	
	public String getMessageCodeName() {
		return messageCodeName;
	}
	
	public String getMessageCodeDesc() {
		return messageCodeDesc;
	}
}
