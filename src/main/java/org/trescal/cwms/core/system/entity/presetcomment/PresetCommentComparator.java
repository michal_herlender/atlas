package org.trescal.cwms.core.system.entity.presetcomment;

import java.util.Comparator;

/**
 * Sorts {@link PresetComment} in order of their category (alphabetically),
 * which uncategorised comments left until last.
 * 
 * @author Richard
 */
public class PresetCommentComparator implements Comparator<PresetComment>
{
	@Override
	public int compare(PresetComment o1, PresetComment o2)
	{
		if ((o1.getCategory() != null) && (o2.getCategory() != null))
		{
			if (o1.getCategory().getId() == o2.getCategory().getId())
			{
				return o1.getComment().toLowerCase().compareTo(o2.getComment().toLowerCase());
			}
			else
			{
				return o1.getCategory().getCategory().compareTo(o2.getCategory().getCategory());
			}
		}
		else if ((o1.getCategory() != null) && (o2.getCategory() == null))
		{
			return -1;
		}
		else if ((o1.getCategory() == null) && (o2.getCategory() != null))
		{
			return 1;
		}
		else
		{
			return o1.getComment().toLowerCase().compareTo(o2.getComment().toLowerCase());
		}
	}
}
