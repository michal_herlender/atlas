package org.trescal.cwms.core.system.entity.supportedcurrenyratehistory.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.supportedcurrenyratehistory.SupportedCurrencyRateHistory;

@Repository("SupportedCurrencyRateHistoryDao")
public class SupportedCurrencyRateHistoryDaoImpl extends BaseDaoImpl<SupportedCurrencyRateHistory, Integer> implements SupportedCurrencyRateHistoryDao {
	
	@Override
	protected Class<SupportedCurrencyRateHistory> getEntity() {
		return SupportedCurrencyRateHistory.class;
	}
}