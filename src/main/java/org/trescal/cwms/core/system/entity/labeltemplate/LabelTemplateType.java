package org.trescal.cwms.core.system.entity.labeltemplate;

public enum LabelTemplateType {
	STANDARD, 
	ALTESIS, 
	ORKLI, 
	AERO, 
	TALGO;
}
