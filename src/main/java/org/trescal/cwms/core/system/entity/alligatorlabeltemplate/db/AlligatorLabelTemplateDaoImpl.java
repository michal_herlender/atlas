package org.trescal.cwms.core.system.entity.alligatorlabeltemplate.db;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.AlligatorLabelTemplate;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.AlligatorLabelTemplateRule;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.AlligatorLabelTemplateRule_;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.AlligatorLabelTemplate_;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.enums.AlligatorTemplateType;

@Service
public class AlligatorLabelTemplateDaoImpl extends BaseDaoImpl<AlligatorLabelTemplate, Integer>
		implements AlligatorLabelTemplateDao {

	@Override
	protected Class<AlligatorLabelTemplate> getEntity() {
		return AlligatorLabelTemplate.class;
	}

	@Override
	public AlligatorLabelTemplate findLabelTemplate(Company businessCompany, AlligatorTemplateType type) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<AlligatorLabelTemplate> cq = cb.createQuery(AlligatorLabelTemplate.class);
		Root<AlligatorLabelTemplate> root = cq.from(AlligatorLabelTemplate.class);
		Join<AlligatorLabelTemplate, AlligatorLabelTemplateRule> rule = root.join(AlligatorLabelTemplate_.rules, JoinType.INNER);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(rule.get(AlligatorLabelTemplateRule_.businessCompany), businessCompany));
		clauses.getExpressions().add(cb.equal(root.get(AlligatorLabelTemplate_.templateType), type));
		cq.where(clauses);
		TypedQuery<AlligatorLabelTemplate> query = getEntityManager().createQuery(cq);
		List<AlligatorLabelTemplate> list = query.getResultList();
		return list.isEmpty() ? null : list.get(0);
	}
	
}
