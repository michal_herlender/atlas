package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class ApproveDeliveryNoteCreationByCSR extends SystemDefaultBooleanType {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.APPROVE_DN_CREATION_BY_CSR;
	}

}
