package org.trescal.cwms.core.system.entity.markups.markuprange.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRange;
import org.trescal.cwms.core.system.entity.markups.markuptype.MarkupType;

public interface MarkupRangeDao extends BaseDao<MarkupRange, Integer> {
	
	List<MarkupRange> findMarkupRange(MarkupType type, double rangestart);
	
	List<MarkupRange> getAllMarkupRanges(MarkupType type);
}