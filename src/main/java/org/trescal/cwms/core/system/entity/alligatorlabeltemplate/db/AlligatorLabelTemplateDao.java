package org.trescal.cwms.core.system.entity.alligatorlabeltemplate.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.AlligatorLabelTemplate;
import org.trescal.cwms.core.system.entity.alligatorlabeltemplate.enums.AlligatorTemplateType;

public interface AlligatorLabelTemplateDao extends BaseDao<AlligatorLabelTemplate, Integer> {
	AlligatorLabelTemplate findLabelTemplate(Company businessCompany, AlligatorTemplateType type);
}
