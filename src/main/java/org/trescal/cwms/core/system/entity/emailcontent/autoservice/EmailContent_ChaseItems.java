package org.trescal.cwms.core.system.entity.emailcontent.autoservice;

import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.tools.autoservice.AutoServiceCalculationTool;
import org.trescal.cwms.core.tools.autoservice.AutoServiceType;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@Component
@Slf4j
public class EmailContent_ChaseItems {
	@Autowired
	private BusinessDetailsService bdServ;
	@Autowired
	private MessageSource messages; 
	@Autowired
	private SupportedLocaleService localeService;
	@Autowired
	private TemplateEngine templateEngine;

	@Value("${cwms.deployed.url}")
	private String deployedURL;
	
	public static final String THYMELEAF_VIEW_CLIENT = "/email/autoservice/clientreminder.html";
	public static final String THYMELEAF_VIEW_SUMMARY = "/email/autoservice/supplierreminder.html";

	public EmailContentDto getContent(Contact con, Collection<JobItem> jobItems, int chaseNumber, int totalChases, 
			Locale locale, AutoServiceType autoServiceType) {
		Contact businessContact = con.getSub().getComp().getDefaultBusinessContact();
		BusinessDetails bdetails = this.bdServ.getAllBusinessDetails(businessContact, null, null);
		log.info("email");
		String body = getBodyThymeleaf(con, jobItems, chaseNumber, totalChases, bdetails, locale, autoServiceType);
		String subject = getSubject(bdetails, locale, autoServiceType);
		return new EmailContentDto(body, subject);
	}
	
	private String getThymeleafView(AutoServiceType type) {
		switch(type) {
			case CLIENT : return THYMELEAF_VIEW_CLIENT;
			case SUPPLIER : return THYMELEAF_VIEW_SUMMARY;
			default : throw new UnsupportedOperationException("Undefined type "+type);
		}
	}
	
	/*
	 * Prepares a Map for the view with localized model names per job item ID
	 * Key = Integer of jobItemId, Value = String with localized model name  
	 */
	private Map<Integer, String> getModelNames(Collection<JobItem> jobItems, Locale locale) {
		Map<Integer, String> result = new HashMap<>();
		jobItems.forEach(ji -> result.put(ji.getJobItemId(), InstModelTools.instrumentModelNameViaTranslations(ji.getInst(), locale, localeService.getPrimaryLocale())));
		return result;
	}

	private String getBodyThymeleaf(Contact con, Collection<JobItem> jobItems, int chaseNumber, int totalChases, 
			BusinessDetails bdetails, Locale locale, AutoServiceType autoServiceType) {
        Context context = new Context(locale);
		context.setVariable("jobItems", jobItems);
		context.setVariable("con", con);
		context.setVariable("chaseNumber", chaseNumber);
		context.setVariable("totalChases", totalChases);
		context.setVariable("antechNumberTool", new NumberTools());
		context.setVariable("antechCalculationTool", new AutoServiceCalculationTool());
		context.setVariable("deployedURL", this.deployedURL);
		context.setVariable(Constants.SESSION_ATTRIBUTE_BUSINESS_DETAILS, bdetails);
		context.setVariable("modelNames", getModelNames(jobItems, locale));
		
		String template = getThymeleafView(autoServiceType);
		String body = this.templateEngine.process(template, context);
		return body;
	}

	private String getSubject(BusinessDetails bdetails, Locale locale, AutoServiceType autoServiceType) {
		String subject = "";
		switch (autoServiceType) {
			case CLIENT:
				subject = this.messages.getMessage("email.clientreminder.subject", 
						new Object[] {bdetails.getCompany()}, bdetails.getCompany() + " Auto-Reminder: Items Requiring Your Action", locale);
				break;
			case SUPPLIER:
				subject = this.messages.getMessage("email.suppliereminder.subject", 
						new Object[] {bdetails.getCompany()}, bdetails.getCompany() + " Auto-Reminder: Items Requiring Your Action", locale);
				break;
			default :
				throw new UnsupportedOperationException("Unsupported type "+autoServiceType);
		}
		return subject;
	}

}
