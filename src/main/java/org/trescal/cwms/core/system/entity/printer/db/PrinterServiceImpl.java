package org.trescal.cwms.core.system.entity.printer.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.printer.Printer;
import org.trescal.cwms.spring.model.KeyValue;

@Service("PrinterService")
public class PrinterServiceImpl extends BaseServiceImpl<Printer, Integer> implements PrinterService
{
	@Autowired
	private AddressService addrServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private PrinterDao printerDao;

	@Override
	public void deletePrinterById(int id)
	{
		Printer printer = this.get(id);

		// delete references from addresses
		for (Address addr : printer.getUsedByAddresses())
		{
			addr.setDefaultPrinter(null);
			this.addrServ.update(addr);
		}

		// delete references from contacts
		List<Contact> contacts = new ArrayList<Contact>();
		for (Contact con : printer.getUsedByContacts())
		{
			con.setDefaultPrinter(null);
			contacts.add(con);
		}
		this.conServ.updateAll(contacts);

		this.delete(printer);
	}

	@Override
	public Printer findPrinterByPath(String printerPath)
	{
		return this.printerDao.findPrinterByPath(printerPath);
	}

	@Override
	public Printer getDefaultPrinterForContact(int personid)
	{
		Contact con = this.conServ.get(personid);

		if (con.getDefaultPrinter() != null)
		{
			return con.getDefaultPrinter();
		}
		else
		{
			if (con.getDefAddress() != null)
			{
				if (con.getDefAddress().getDefaultPrinter() != null)
				{
					return con.getDefAddress().getDefaultPrinter();
				}
			}
		}

		return null;
	}

	@Override
	public List<Printer> getPrintersAtAddress(int addrId)
	{
		return this.printerDao.getPrintersAtAddress(addrId);
	}

	@Override
	public List<KeyValue<Integer, String>> getDtosBySubdiv(Subdiv subdiv) {
		List<Printer> printers = this.printerDao.getBySubdiv(subdiv);
		
		List<KeyValue<Integer, String>> results = new ArrayList<>();
		for (Printer printer: printers) {
			StringBuffer description = new StringBuffer();
			description.append(printer.getAddr().getAddr1());
			description.append(" - ");
			description.append(printer.getDescription());
			
			results.add(new KeyValue<Integer, String>(printer.getId(), description.toString()));
		}
		return results;
	}

	@Override
	public List<KeyValue<Integer, String>> getDtosForContactByUserRole(Contact contact) {
		List<KeyValue<Integer, String>> results = new ArrayList<>();
		// If contact does not have a user account associated, return empty list
		if (contact.getUser() != null) {
			Set<Subdiv> subdivs = contact.getUser().getUserRoles().stream().map(ur -> ur.getOrganisation()).distinct().collect(Collectors.toSet());
			System.out.println(subdivs.size());
			List<Printer> printers = this.printerDao.getBySubdivs(subdivs);
			
			for (Printer printer: printers) {
				StringBuffer description = new StringBuffer();
				description.append(printer.getAddr().getSub().getComp().getConame());
				description.append(" - ");
				description.append(printer.getAddr().getSub().getSubname());
				description.append(" - ");
				description.append(printer.getAddr().getAddr1());
				description.append(" - ");
				description.append(printer.getDescription());
				
				results.add(new KeyValue<Integer, String>(printer.getId(), description.toString()));
			}
		}
		return results;
	}
	
	@Override
	protected BaseDao<Printer, Integer> getBaseDao() {
		// TODO Auto-generated method stub
		return this.printerDao;
	}
}