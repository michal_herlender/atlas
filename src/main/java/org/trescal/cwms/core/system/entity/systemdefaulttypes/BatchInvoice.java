package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class BatchInvoice extends SystemDefaultBooleanType {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.BATCH_INVOICE;
	}
}