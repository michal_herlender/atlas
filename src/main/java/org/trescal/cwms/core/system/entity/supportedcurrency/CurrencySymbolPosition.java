package org.trescal.cwms.core.system.entity.supportedcurrency;

/**
 * Defines position of currency symbol:
 * 
 * PREFIX_NO_SPACE e.g. $10
 * PREFIX_SPACE e.g. $ 1
 * POSTFIX_SPACE e.g. 1 $
 * 
 * e.g. placed on left (UK pound, US dollar) or on right (Euro) 
 *
 */
public enum CurrencySymbolPosition {
	PREFIX_NO_SPACE, PREFIX_SPACE, POSTFIX_SPACE; 
}
