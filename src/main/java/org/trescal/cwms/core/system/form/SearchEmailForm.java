package org.trescal.cwms.core.system.form;

import java.util.Date;

public class SearchEmailForm
{
	private Date date;
	private Integer fromPersonId;
	private int pageNo;
	private Integer personid;
	private Boolean publish;
	private int resultsPerPage = 20;
	private String subject;
	private String toAddress;

	public Date getDate()
	{
		return this.date;
	}

	public Integer getFromPersonId()
	{
		return this.fromPersonId;
	}

	public int getPageNo()
	{
		return this.pageNo;
	}

	public Integer getPersonid()
	{
		return this.personid;
	}

	public Boolean getPublish()
	{
		return this.publish;
	}

	public int getResultsPerPage()
	{
		return this.resultsPerPage;
	}

	public String getSubject()
	{
		return this.subject;
	}

	public String getToAddress()
	{
		return this.toAddress;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public void setFromPersonId(Integer fromPersonId)
	{
		this.fromPersonId = fromPersonId;
	}

	public void setPageNo(int pageNo)
	{
		this.pageNo = pageNo;
	}

	public void setPersonid(Integer personid)
	{
		this.personid = personid;
	}

	public void setPublish(Boolean publish)
	{
		this.publish = publish;
	}

	public void setResultsPerPage(int resultsPerPage)
	{
		this.resultsPerPage = resultsPerPage;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	public void setToAddress(String toAddress)
	{
		this.toAddress = toAddress;
	}
}