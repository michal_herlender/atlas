package org.trescal.cwms.core.system.entity.instructionlink;

import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public enum InstructionEntity {
	COMPANY("Company", "company"), 
	SUBDIV("Subdiv", "subdiv"), 
	CONTACT("Contact", "contact"), 
	JOB("Job", "job"),
	CONTRACT("Contract", "company.contract");

	private String messageCode;
	private String defaultName;
	private MessageSource messageSource;

	private InstructionEntity(String defaultName, String messageCode) {
		this.messageCode = messageCode;
		this.defaultName = defaultName;
	}

	@Component
	public static class InstructionMessageInjector {
		@Autowired
		private MessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (InstructionEntity instructionEntity : InstructionEntity.values()) {
				instructionEntity.setMessageSource(this.messageSource);
			}
		}
	}

	public String getMessageCode() {
		return messageCode;
	}

	public String getDefaultName() {
		return defaultName;
	}

	public void setDefaultName(String defaultName) {
		this.defaultName = defaultName;
	}

	public String getMessage() {
		Locale locale = LocaleContextHolder.getLocale();
		return getMessageForLocale(locale);
	}

	public String getMessageForLocale(Locale locale) {
		return this.messageSource.getMessage(messageCode, null, defaultName, locale);
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
}