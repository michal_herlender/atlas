package org.trescal.cwms.core.system.entity.supportedcurrency.db;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Currency;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.system.dto.SupportedCurrencyAjaxDTO;
import org.trescal.cwms.core.system.dto.SupportedCurrencyOption;
import org.trescal.cwms.core.system.entity.supportedcurrency.MultiCurrencyAware;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface SupportedCurrencyService extends BaseService<SupportedCurrency, Integer>
{
	/**
	 * Updates the {@link SupportedCurrency} identified by the given id with a
	 * new default exchange rate. Tests that the {@link SupportedCurrency}
	 * exists and that it is not the current system default currency. Returns a
	 * resultwrapper indicating the outcome of the operation.
	 * 
	 * @param currencyid the id of the {@link SupportedCurrency} to update.
	 * @param rate the new default rate for the {@link SupportedCurrency}. (i.e.
	 *        the value of 1 unit of the current default
	 *        {@link SupportedCurrency} in this {@link SupportedCurrency}.
	 * @return {@link ResultWrapper}.
	 */
	ResultWrapper ajaxUpdateDefaultRate(int currencyid, String rate);

	/**
	 * Get's the {@link SupportedCurrency} identified by the given ISO 4217
	 * currency code, or null if none found.
	 * 
	 * @param currencyCode the ISO 4217 currency code.
	 * @return {@link SupportedCurrency} or null.
	 */
	SupportedCurrency findCurrencyByCode(String currencyCode);

	SupportedCurrency findSupportedCurrency(int id);

	SupportedCurrency findSupportedCurrencyOrDefault(String currencyCode);

	List<SupportedCurrency> getAllSupportedCurrenciesWithExRates();

	List<SupportedCurrency> getAllSupportedCurrencys();

	/**
	 * Function prepares a number number of currency options based on the
	 * company and it's currency defaults. If a company has a default currency
	 * and a set rate then the function returns an entry for that currency +
	 * rate combo and also a list of all other currencies available on the
	 * system.
	 * 
	 * @param coid the coid.
	 * @return list of wrapper objects holding {@link SupportedCurrency}
	 *         information.
	 */
	List<SupportedCurrencyOption> getCompanyCurrencyOptions(Integer coid);

	/**
	 * Returns the system default {@link Currency}. Checks for a system default
	 * currency code, if none found then finds the first currency marked in the
	 * currency order by list.
	 * 
	 * @return {@link SupportedCurrency}.
	 */
	SupportedCurrency getDefaultCurrency();

	void insertSupportedCurrency(SupportedCurrency supportedcurrency);

	/**
	 * Returns a List of key-value pairs for the specified IDs where key is the id, value is currency code
	 */
	List<KeyValueIntegerString> getKeyValueList(Collection<Integer> currencyIds);
	
	/**
	 * Returns a List of key-value pairs for all supported currency options, intended for UI selection.
	 * Formatting uses "symbol - ISO code - name" e.g. "$ - USD - US Dollar" and is sorted by ISO code
	 */
	List<KeyValueIntegerString> getKeyValueListAll();

	void setCurrencyFromForm(MultiCurrencyAware obj, String currencyCode, Company company);

	/**
	 * Updates the given {@link SupportedCurrency} with the given default
	 * exchange rate. Also logs a history item detailing the previous rate.
	 * 
	 * @param currency the {@link SupportedCurrency} to update.
	 * @param exRate the new exchange rate.
	 */
	void updateDefaultRate(SupportedCurrency currency, double exRate);

	void updateSupportedCurrency(SupportedCurrency supportedcurrency);
	
	KeyValueIntegerString getKeyValue(Integer currencyId);
	
	SupportedCurrencyAjaxDTO saveNewValues(BigDecimal exchangeRate, Integer currencyId, Contact contact);
}