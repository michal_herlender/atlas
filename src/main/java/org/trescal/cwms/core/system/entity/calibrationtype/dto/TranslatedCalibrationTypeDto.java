package org.trescal.cwms.core.system.entity.calibrationtype.dto;

public class TranslatedCalibrationTypeDto {
	private int serviceTypeId;
	private String longName;
	private String shortName;
	
	public TranslatedCalibrationTypeDto(int serviceTypeId, String longName, String shortName) {
		super();
		this.serviceTypeId = serviceTypeId;
		this.longName = longName;
		this.shortName = shortName;
	}
	
	public int getServiceTypeId() {
		return serviceTypeId;
	}
	public void setServiceTypeId(int serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}
	public String getLongName() {
		return longName;
	}
	public void setLongName(String longName) {
		this.longName = longName;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	
}
