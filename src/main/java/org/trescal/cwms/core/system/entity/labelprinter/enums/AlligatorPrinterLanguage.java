package org.trescal.cwms.core.system.entity.labelprinter.enums;

public enum AlligatorPrinterLanguage {
	ZPL, TEC, TSC, BROTHER, DYMO
}
