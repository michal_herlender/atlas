package org.trescal.cwms.core.system.entity.alligatorsettings;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

/*
 * Base class for configuration of how AlligatorSettings are used
 */
@MappedSuperclass
public abstract class AlligatorSettingsUsage {
	private int id;
	private AlligatorSettings settingsProduction;
	private AlligatorSettings settingsTest;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}
	
	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="productionsettingsid", nullable=false)
	public AlligatorSettings getSettingsProduction() {
		return settingsProduction;
	}

	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="testsettingsid", nullable=false)
	public AlligatorSettings getSettingsTest() {
		return settingsTest;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public void setSettingsProduction(AlligatorSettings settingsProduction) {
		this.settingsProduction = settingsProduction;
	}

	public void setSettingsTest(AlligatorSettings settingsTest) {
		this.settingsTest = settingsTest;
	}

}
