package org.trescal.cwms.core.system.entity.businessdetails.db;

import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;

public interface BusinessDetailsService
{
	BusinessDetails getAllBusinessDetails(Subdiv subdiv);
	
	/**
	 * @param contact the {@link Contact} object of the business company that create the document. Contact information for header section.
 	 * @param headerAddress the {@link Address} object for the header section (if null -> defAddress from contact is used)
 	 * @param company the {@link Company} object for the footer section (if null -> defAddress from contact is used)
	 * @return BusinessDetails to use in documents
	 */
	BusinessDetails getAllBusinessDetails(Contact contact, Address headerAddress, Company company);
}