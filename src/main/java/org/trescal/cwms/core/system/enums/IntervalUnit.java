package org.trescal.cwms.core.system.enums;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.trescal.cwms.spring.model.KeyValue;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;

public enum IntervalUnit {
	YEAR("intervalunit.year", "intervalunit.pluralyear", "intervalunit.singlecharyear", x -> 365 * x),
	MONTH("intervalunit.month", "intervalunit.pluralmonth", "intervalunit.singlecharmonth", x -> 30 * x),
	WEEK("intervalunit.week", "intervalunit.pluralweek", "intervalunit.singlecharweek", x -> 7 * x),
	DAY("intervalunit.day", "intervalunit.pluralday", "intervalunit.singlecharday", x -> x);

	private final String singularNameCode; // e.g. weeks
	private final String pluralNameCode; // e.g. week
	private final String singleCharacterCode; // e.g. W
	private MessageSource messageSource;
	private final IntUnaryOperator toDaysConverter;

	IntervalUnit(String singularNameCode, String pluralNameCode, String singleCharacterCode,
				 IntUnaryOperator toDaysConvertor) {
		this.singularNameCode = singularNameCode;
		this.pluralNameCode = pluralNameCode;
		this.singleCharacterCode = singleCharacterCode;
		this.toDaysConverter = toDaysConvertor;
	}

	@Component
	public static class IntervalUnitMessageSourceInjector {
		@Autowired
		private MessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (IntervalUnit dateUnit : EnumSet.allOf(IntervalUnit.class)) {
				dateUnit.setMessageSource(messageSource);
			}
		}
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getName(Integer interval) {
		return getName(interval, LocaleContextHolder.getLocale());
	}

	public String getName(Integer interval, Locale locale) {
		if (interval == 1)
			return messageSource.getMessage(singularNameCode, null, locale);
		else
			return messageSource.getMessage(pluralNameCode, null, locale);
	}
	
	public String getSingularName(Locale locale) {
			return messageSource.getMessage(singularNameCode, null, locale);
	}

	public static List<KeyValue<String, String>> getUnitsForSelect() {
		return Arrays.stream(IntervalUnit.values()).map(u -> new KeyValue<>(u.name(), u.getName(0)))
			.collect(Collectors.toList());
	}

	public Integer toDays(Integer quantityOfThisUnit) {
		return this.toDaysConverter.applyAsInt(quantityOfThisUnit);
	}

	public String getSingularNameCode() {
		return singularNameCode;
	}

	public String getPluralNameCode() {
		return pluralNameCode;
	}

	public String getSingleCharacterCode() {
		return singleCharacterCode;
	}


}
