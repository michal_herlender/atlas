package org.trescal.cwms.core.system.entity.printertray.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.papertype.PaperType;
import org.trescal.cwms.core.system.entity.printertray.PrinterTray;

public interface PrinterTrayDao extends BaseDao<PrinterTray, Integer> {
	
	PrinterTray findPrinterTrayForPaperType(int printerId, PaperType paperType);
}