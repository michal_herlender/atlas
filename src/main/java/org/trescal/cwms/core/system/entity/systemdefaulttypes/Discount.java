package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class Discount extends SystemDefaultPercentType {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.DISCOUNT;
	}
}