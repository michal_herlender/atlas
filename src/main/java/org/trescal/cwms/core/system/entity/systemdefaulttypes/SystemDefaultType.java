package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import io.vavr.Tuple2;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.organisation.OrganisationLevel;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefault.db.SystemDefaultService;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.db.SystemDefaultApplicationService;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.spring.model.KeyValue;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;

public abstract class SystemDefaultType<ValueType> implements Comparable<SystemDefaultType<?>> {

    @Autowired
    private ReloadableResourceBundleMessageSource messageSource;
    @Autowired
    private SystemDefaultApplicationService applicationService;
    @Autowired
    private SystemDefaultService service;

    @Override
    public int compareTo(@NotNull SystemDefaultType<?> other) {
        int groupCompare = 0;
        if (getSystemDefault() != null && other.getSystemDefault() != null && getSystemDefault().getGroup() != null
            && other.getSystemDefault().getGroup() != null)
            groupCompare = getSystemDefault().getGroup().getName()
                .compareTo(other.getSystemDefault().getGroup().getName());
        return groupCompare == 0 ? defaultName().name().compareTo(other.defaultName().name()) : groupCompare;
    }

	abstract public SystemDefaultNames defaultName();

	public String getLocaleName(Locale locale) {
		return messageSource == null ? defaultName().toString()
				: messageSource.getMessage(defaultName().getMessageCode(), null, locale);
	}

	public SystemDefault getSystemDefault() {
		return service.get(this.defaultName());
	}

	public SystemDefaultApplication getSystemDefaultApplication(Scope scope, Integer organisationId,
			Integer allocatedCompanyId) {
        return applicationService.get(defaultName().ordinal(), scope, organisationId, allocatedCompanyId);
    }

    public SystemDefaultApplication getSystemDefaultApplication(Scope scope, OrganisationLevel organisation,
                                                                Company allocatedCompany) {
        return applicationService.get(defaultName().ordinal(), scope, organisation.getId(), allocatedCompany);
    }

    public KeyValue<Scope, String> getValueHierarchical(Scope scope, OrganisationLevel organisation,
                                                        Company allocatedCompany) {
        val pair =
            applicationService.getGenericValueHierachical(defaultName().ordinal(), scope, organisation.getId(),
                allocatedCompany, Function.identity());
        return new KeyValue<>(pair._1(), pair._2());
    }

    public Tuple2<Scope, ValueType> getGenericValueHierachical(Scope scope, int entityId, Company allocatedCompay) {
        return applicationService.getGenericValueHierachical(defaultName().ordinal(), scope, entityId, allocatedCompay, this::parseValue);
    }

    public ValueType parseValueHierarchical(Scope scope, OrganisationLevel organisation, Company allocatedCompany) {
        val pair = this.getGenericValueHierachical(scope, organisation.getId(), allocatedCompany);
        return pair._2();
    }

    public ValueType getValueByScope(Scope scope, Integer organisationId, Integer allocatedCompanyId) {
        SystemDefaultApplication application = getSystemDefaultApplication(scope, organisationId, allocatedCompanyId);
        if (application == null) {
			SystemDefault systemDefault = getSystemDefault();
			if (systemDefault == null)
				return null;
			else
				return parseValue(systemDefault.getDefaultValue());
		} else
			return parseValue(application.getValue());
	}

	public ValueType getValueByScope(Scope scope, OrganisationLevel organisation, Company allocatedCompany) {
		SystemDefaultApplication application = getSystemDefaultApplication(scope, organisation, allocatedCompany);
		if (application == null) {
			SystemDefault systemDefault = getSystemDefault();
			if (systemDefault == null)
				return null;
			else
				return parseValue(systemDefault.getDefaultValue());
		} else
			return parseValue(application.getValue());
	}

	abstract public boolean isDiscreteType();
	
	abstract public boolean isMultiChoice();

	abstract public List<String> getValues();

	abstract public ValueType parseValue(String value);

	public String getValue(String savedValue) {
		return savedValue;
	}

	public String getStringValue(ValueType v) {
		return v.toString();
	}

	public String parseAndGetStringValue(String v) {
		return getStringValue(parseValue(v));
	}

}