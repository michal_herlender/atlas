package org.trescal.cwms.core.system.entity.emailcontent.web;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.translation.TranslationService;

@Component
public class EmailContent_QuoteRequest {
	@Autowired
	private BusinessDetailsService bdServ;
	@Autowired
	private MessageSource messages; 
	@Autowired
	private TranslationService translationService; 
	@Autowired
	private TemplateEngine templateEngine;

	@Value("${cwms.deployed.url}")
	private String deployedURL;
	
	public static final String CLIENT_THYMELEAF_VIEW = "/email/portal/client_quoterequest.html";
	public static final String INTERNAL_THYMELEAF_VIEW = "/email/portal/quoterequest.html";
	
	public static enum EmailType {
		CLIENT, INTERNAL;
	}
	
	public EmailContentDto getContent(Quotation quotation, Locale locale, EmailType emailType) {
		Contact businessContact = quotation.getContact().getSub().getComp().getDefaultBusinessContact();
		BusinessDetails bdetails = this.bdServ.getAllBusinessDetails(businessContact, null, null);
		
		String body = getBodyThymeleaf(quotation, bdetails, locale, emailType);
		String subject = getSubject(locale);
		return new EmailContentDto(body, subject);
	}

	private String getBodyThymeleaf(Quotation quotation, BusinessDetails bdetails, Locale locale, EmailType emailType) {
        Context context = new Context(locale);
		context.setVariable("contact", quotation.getContact());
		context.setVariable(Constants.SESSION_ATTRIBUTE_BUSINESS_DETAILS, bdetails);
		context.setVariable("quotation", quotation);
		context.setVariable("quoteURL", this.deployedURL+"viewquotation.htm?id=" + quotation.getId());
		context.setVariable("serviceTypes", getServiceTypeTranslations(quotation.getQuotationitems(), locale));
		String template = emailType == EmailType.CLIENT ? CLIENT_THYMELEAF_VIEW : INTERNAL_THYMELEAF_VIEW;
		String body = this.templateEngine.process(template, context);
		return body;
	}
	
	private Map<Integer,String> getServiceTypeTranslations(Set<Quotationitem> quotationitems, Locale locale) {
		Map<Integer,String> result = new HashMap<>();
		quotationitems.forEach(qi -> 
			result.put(qi.getId(), 
					this.translationService.getCorrectTranslation(qi.getServiceType().getShortnameTranslation(), locale)));
		return result;
	}

	private String getSubject(Locale locale) {
		return this.messages.getMessage("email.quoterequest.subject", null, "Client quotation request from website", locale);
	}

}
