package org.trescal.cwms.core.system.entity.systemdefault;

import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;

public class SystemDefaultDTO
{
	private int id;
	private String defaultDataType;
	private String defaultName;
	private String defaultValue;
	
	public SystemDefaultDTO(SystemDefault systemDefault) {
		id = systemDefault.getId().ordinal();
		defaultDataType = systemDefault.getDefaultDataType();
		defaultName = systemDefault.getDefaultName();
		defaultValue = systemDefault.getDefaultValue();
	}
	
	public SystemDefaultDTO(SystemDefaultApplication application) {
		id = application.getId();
		defaultDataType = application.getSystemDefault().getDefaultDataType();
		defaultName = application.getSystemDefault().getDefaultName();
		defaultValue = application.getSystemDefault().getDefaultValue();
	}
	
	public int getId() {
		return id;
	}
	
	public String getDefaultDataType() {
		return defaultDataType;
	}
	
	public String getDefaultName() {
		return defaultName;
	}
	
	public String getDefaultValue() {
		return defaultValue;
	}
	
	public void setDefaultName(String defaultName) {
		this.defaultName = defaultName;
	}
	
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setDefaultDataType(String defaultDataType) {
		this.defaultDataType = defaultDataType;
	}
}