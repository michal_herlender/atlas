package org.trescal.cwms.core.system.entity.emailcontent.autoservice;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.tools.autoservice.AutoServiceSummary;
import org.trescal.cwms.core.tools.autoservice.AutoServiceType;

@Component
public class EmailContent_ChaseSummary {
	@Autowired
	private MessageSource messages; 
	@Autowired
	private TemplateEngine templateEngine;

	@Value("${cwms.deployed.url}")
	private String deployedUrl;
	
	public static final String THYMELEAF_VIEW_CLIENT = "/email/autoservice/clientsummary.html";
	public static final String THYMELEAF_VIEW_SUMMARY = "/email/autoservice/suppliersummary.html";

	public EmailContentDto getContent(AutoServiceSummary summary, Locale locale, AutoServiceType autoServiceType) {
		String body = getBodyThymeleaf(summary, locale, autoServiceType);
		String subject = getSubject(locale, autoServiceType);
		return new EmailContentDto(body, subject);
	}
	
	private String getThymeleafView(AutoServiceType type) {
		switch(type) {
			case CLIENT : return THYMELEAF_VIEW_CLIENT;
			case SUPPLIER : return THYMELEAF_VIEW_SUMMARY;
			default : throw new UnsupportedOperationException("Undefined type "+type);
		}
	}

	private String getBodyThymeleaf(AutoServiceSummary summary, Locale locale, AutoServiceType autoServiceType) {
        Context context = new Context(locale);
		context.setVariable("chased", summary.getChased());
		context.setVariable("failNoChasing", summary.getFailNoChasing());
		context.setVariable("failDisabledChase", summary.getFailDisabledChase());
		context.setVariable("failNoEmail", summary.getFailNoEmail());
		context.setVariable("failChaseLimit", summary.getFailChaseLimit());
		context.setVariable("totalChases", summary.getTotalChases());
		context.setVariable("deployedURL", this.deployedUrl);
		
		String template = getThymeleafView(autoServiceType);
		String body = this.templateEngine.process(template, context);
		return body;
	}

	private String getSubject(Locale locale, AutoServiceType autoServiceType) {
		String subject = "";
		switch (autoServiceType) {
			case CLIENT:
				subject = this.messages.getMessage("email.clientsummary.subject", null, "Auto-Service Client Summary", locale);
				break;
			case SUPPLIER:
				subject = this.messages.getMessage("email.suppliersummary.subject", null, "Auto-Service Supplier Summary", locale);
				break;
			default :
				throw new UnsupportedOperationException("Unsupported type "+autoServiceType);
		}
		return subject;
	}
}
