package org.trescal.cwms.core.system.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink;
import org.trescal.cwms.core.company.entity.companyinstructionlink.db.CompanyInstructionLinkService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink;
import org.trescal.cwms.core.company.entity.contactinstructionlink.db.ContactInstructionLinkService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.db.SubdivInstructionLinkService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.JobInstructionLink;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.db.JobInstructionLinkService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLink;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.form.InstructionForm;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Controller for adding new {@link Company}, {@link Subdiv} and {@link Contact}
 * {@link Instruction}s.
 */
@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY,Constants.SESSION_ATTRIBUTE_SUBDIV})
public class InstructionAddController
{
	@Autowired
	private CompanyService compServ;
	@Autowired
	private ContactService contServ;
	@Autowired
	private JobService jobServ;
	@Autowired
	private SubdivService subServ;
	@Autowired
	private SubdivInstructionLinkService subdivInstructionLinkService;
	@Autowired
	private CompanyInstructionLinkService companyInstructionLinkService;
	@Autowired
	private ContactInstructionLinkService contactInstructionLinkService;
	@Autowired
	private JobInstructionLinkService jobInstructionLinkService;
	@Autowired 
	private SubdivService subdivService;
	@Autowired
    private LocalValidatorFactoryBean validator;
	
	public static final String FORM_NAME = "form";
	public static final String TAB_URL_FRAGMENT = "&loadtab=instructions-tab";

	@ModelAttribute(FORM_NAME)
	protected InstructionForm formBackingObject(HttpServletRequest request,
			@RequestParam(value="coid", required=false, defaultValue="0") Integer coid,
			@RequestParam(value="subdivid", required=false, defaultValue="0") Integer subdivid,
			@RequestParam(value="personid", required=false, defaultValue="0") Integer personid,
			@RequestParam(value="jobid", required=false, defaultValue="0") Integer jobid) throws Exception {
		
		InstructionForm iform = new InstructionForm();
		iform.setRefererUrl(request.getHeader("referer"));
		Instruction instruction = new Instruction();
		if (coid > 0) {
			CompanyInstructionLink i = new CompanyInstructionLink();
			i.setCompany(this.compServ.get(coid));
			i.setInstruction(instruction);
			iform.setInstructionLink(i);
			iform.setCoid(coid);
		}
		else if (subdivid > 0) {
			SubdivInstructionLink i = new SubdivInstructionLink();
			i.setSubdiv(this.subServ.get(subdivid));
			iform.setInstructionLink(i);
			iform.setSubdivid(subdivid);
		}
		else if (personid > 0) {
			ContactInstructionLink i = new ContactInstructionLink();
			i.setContact(this.contServ.get(personid));
			iform.setInstructionLink(i);
			iform.setPersonid(personid);
		}
		else if (jobid > 0) {
			JobInstructionLink i = new JobInstructionLink();
			i.setJob(this.jobServ.get(jobid));
			iform.setInstructionLink(i);
			iform.setJobid(jobid);
		}
		return iform;
	}
	
	protected void onBind(InstructionForm form, int allocatedCompanyId) {
		InstructionType type = form.getInstructionType();
		InstructionLink<?> link = form.getInstructionLink(); 
		link.getInstruction().setInstructiontype(type);
		link.getInstruction().setIncludeOnDelNote(type.equals(InstructionType.CARRIAGE) ? form.isIncludeOnDelNotes() : null);
		link.getInstruction().setIncludeOnSupplierDelNote(type.equals(InstructionType.CARRIAGE) ? form.isIncludeOnSupplierDelNotes() : null);
		Company allocatedCompany = compServ.get(allocatedCompanyId); 
		link.setOrganisation(allocatedCompany);
	}
	
	private String getViewName(InstructionForm form) {
		String viewName = form.getRefererUrl();
		if (!viewName.contains(TAB_URL_FRAGMENT)) viewName = viewName + TAB_URL_FRAGMENT;
		return "redirect:"+viewName;
	}
	@ModelAttribute("businessSubdiv")
	protected Subdiv getBusinessSubdiv(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto){
		return subdivService.get(subdivDto.getKey());
	}
	
	@RequestMapping(value="/addinstruction.htm", method=RequestMethod.POST, params="coid")
	protected String onSubmitCompany(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(FORM_NAME) InstructionForm form, BindingResult bindingResult) {
		onBind(form, allocatedCompanyDto.getKey());
		// Manually validated due to allocated company
		this.validator.validate(form, bindingResult);
		if (bindingResult.hasErrors()) return referenceData(model, form);
		else {
			CompanyInstructionLink link = (CompanyInstructionLink) form.getInstructionLink();
			link.setSubdivInstruction(form.isSubdivInstruction());
			Subdiv subdiv = subdivService.get(subdivDto.getKey());
			link.setBusinessSubdiv(subdiv);
			this.companyInstructionLinkService.save(link);
			return getViewName(form);
		}
	}
	
	@RequestMapping(value="/addinstruction.htm", method=RequestMethod.POST, params="subdivid")
	protected String onSubmitSubdiv(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(FORM_NAME) InstructionForm form, BindingResult bindingResult) {
		onBind(form, allocatedCompanyDto.getKey());
		// Manually validated due to allocated company
		this.validator.validate(form, bindingResult);
		if (bindingResult.hasErrors()) return referenceData(model, form);
		else {
			SubdivInstructionLink link = (SubdivInstructionLink) form.getInstructionLink();
			link.setSubdivInstruction(form.isSubdivInstruction());
			Subdiv subdiv = subdivService.get(subdivDto.getKey());
			link.setBusinessSubdiv(subdiv);
			this.subdivInstructionLinkService.save(link);
			return getViewName(form);
		}
	}
	
	@RequestMapping(value="/addinstruction.htm", method=RequestMethod.POST, params="personid")
	protected String onSubmitContact(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto,
			@ModelAttribute(FORM_NAME) InstructionForm form, BindingResult bindingResult) {
		onBind(form, allocatedCompanyDto.getKey());
		// Manually validated due to allocated company
		this.validator.validate(form, bindingResult);
		if (bindingResult.hasErrors()) return referenceData(model, form);
		else {
			this.contactInstructionLinkService.save((ContactInstructionLink) form.getInstructionLink());
			return getViewName(form);
		}
	}
	
	@RequestMapping(value="/addinstruction.htm", method=RequestMethod.POST, params="jobid")
	protected String onSubmitJob(Model model,
			@ModelAttribute(FORM_NAME) InstructionForm form, BindingResult bindingResult) {
		onBind(form, 0);	// No allocatedCompany for Job Instructions
		this.validator.validate(form, bindingResult);
		if (bindingResult.hasErrors()) return referenceData(model, form);
		else {
			this.jobInstructionLinkService.save((JobInstructionLink) form.getInstructionLink());
			return getViewName(form);
		}
	}
	
	@RequestMapping(value="/addinstruction.htm", method=RequestMethod.GET)
	protected String referenceData(Model model,
			@ModelAttribute(FORM_NAME) InstructionForm form){
		List<KeyValue<InstructionType, String>> instructionTypes = InstructionType.getActiveTypes().stream()
				.map(it -> new KeyValue<>(it, it.getType()))
				.collect(Collectors.toList());
		model.addAttribute("instructionTypes", instructionTypes);
		switch (form.getInstructionLink().getInstructionEntity()) {
		case COMPANY:
			Company company = ((CompanyInstructionLink) form.getInstructionLink()).getEntity();
			model.addAttribute("instructionCompany", company);
			break;
		case SUBDIV:
			Subdiv subdiv = ((SubdivInstructionLink) form.getInstructionLink()).getEntity();
			model.addAttribute("instructionCompany", subdiv.getComp());
			model.addAttribute("instructionSubdiv", subdiv);
			break;
		case CONTACT:
			Contact contact = ((ContactInstructionLink) form.getInstructionLink()).getEntity();
			model.addAttribute("instructionCompany", contact.getSub().getComp());
			model.addAttribute("instructionSubdiv", contact.getSub());
			model.addAttribute("instructionContact", contact);
			break;
		case JOB:
			model.addAttribute("instructionJob", ((JobInstructionLink) form.getInstructionLink()).getEntity());
			break;
		default:
			throw new RuntimeException("Unsupported entity type");
		}
			
		return "trescal/core/system/instruction";
	}
}