package org.trescal.cwms.core.system.entity.businessdetails;

public class BusinessDetails
{
	private String accountsEmail;
	private String[] address;
	private String autoEmail;
	private String collectEmail;
	private String company;
	private String contactPosition;
	private String docCompany;
	private String fax;
	private String goodsinEmail;
	private String homepage;
	private String logo;
	private String qualityEmail;
	private String quoteEmail;
	private String recallEmail;
	private String salesEmail;
	private String tel;
	private String webEmail;
	private String contactName;
	private String legalRegistration1;
	private String legalRegistration2;
	private String legalIdentifier;
	private String fiscalIdentifier;

	public BusinessDetails()
	{

	}

	public BusinessDetails(String company, String docCompany, String address, String tel, String fax, String homepage, String accountsEmail, String salesEmail, String goodsinEmail, String recallEmail, String qualityEmail, String webEmail, String collectEmail, String quoteEmail, String autoEmail, String logo)
	{
		this.company = company;
		this.docCompany = docCompany;
		this.address = address.split(",");
		this.tel = tel;
		this.fax = fax;
		this.homepage = homepage;
		this.accountsEmail = accountsEmail;
		this.salesEmail = salesEmail;
		this.goodsinEmail = goodsinEmail;
		this.recallEmail = recallEmail;
		this.qualityEmail = qualityEmail;
		this.quoteEmail = quoteEmail;
		this.webEmail = webEmail;
		this.collectEmail = collectEmail;
		this.autoEmail = autoEmail;
		this.logo = logo;
	}

	public String getAccountsEmail()
	{
		return this.accountsEmail;
	}

	public String[] getAddress()
	{
		return this.address;
	}

	public String getAutoEmail()
	{
		return this.autoEmail;
	}

	public String getCollectEmail()
	{
		return this.collectEmail;
	}

	public String getCompany()
	{
		return this.company;
	}

	public String getDocCompany()
	{
		return this.docCompany;
	}

	public String getFax()
	{
		return this.fax;
	}

	public String getGoodsinEmail()
	{
		return this.goodsinEmail;
	}

	public String getHomepage()
	{
		return this.homepage;
	}

	public String getLogo()
	{
		return this.logo;
	}

	public String getQualityEmail()
	{
		return this.qualityEmail;
	}

	public String getQuoteEmail()
	{
		return this.quoteEmail;
	}

	public String getRecallEmail()
	{
		return this.recallEmail;
	}

	public String getSalesEmail()
	{
		return this.salesEmail;
	}

	public String getTel()
	{
		return this.tel;
	}

	public String getWebEmail()
	{
		return this.webEmail;
	}
	
	public String getContactName()
	{
		return this.contactName;
	}

	public void setAccountsEmail(String accountsEmail)
	{
		this.accountsEmail = accountsEmail;
	}

	public void setAddress(String[] address)
	{
		this.address = address;
	}

	public void setAutoEmail(String autoEmail)
	{
		this.autoEmail = autoEmail;
	}

	public void setCollectEmail(String collectEmail)
	{
		this.collectEmail = collectEmail;
	}

	public void setCompany(String company)
	{
		this.company = company;
	}

	public void setDocCompany(String docCompany)
	{
		this.docCompany = docCompany;
	}

	public void setFax(String fax)
	{
		this.fax = fax;
	}

	public void setGoodsinEmail(String goodsinEmail)
	{
		this.goodsinEmail = goodsinEmail;
	}

	public void setHomepage(String homepage)
	{
		this.homepage = homepage;
	}

	public void setLogo(String logo)
	{
		this.logo = logo;
	}

	public void setQualityEmail(String qualityEmail)
	{
		this.qualityEmail = qualityEmail;
	}

	public void setQuoteEmail(String quoteEmail)
	{
		this.quoteEmail = quoteEmail;
	}

	public void setRecallEmail(String recallEmail)
	{
		this.recallEmail = recallEmail;
	}

	public void setSalesEmail(String salesEmail)
	{
		this.salesEmail = salesEmail;
	}

	public void setTel(String tel)
	{
		this.tel = tel;
	}

	public void setWebEmail(String webEmail)
	{
		this.webEmail = webEmail;
	}
	
	public void setContactName(String contactName)
	{
		this.contactName = contactName;
	}

	public String getLegalRegistration1() {
		return legalRegistration1;
	}

	public void setLegalRegistration1(String legalRegistration1) {
		this.legalRegistration1 = legalRegistration1;
	}

	public String getLegalRegistration2() {
		return legalRegistration2;
	}

	public void setLegalRegistration2(String legalRegistration2) {
		this.legalRegistration2 = legalRegistration2;
	}

	public String getContactPosition() {
		return contactPosition;
	}

	public void setContactPosition(String contactPosition) {
		this.contactPosition = contactPosition;
	}

	public String getLegalIdentifier() {
		return legalIdentifier;
	}

	public String getFiscalIdentifier() {
		return fiscalIdentifier;
	}

	public void setLegalIdentifier(String legalIdentifier) {
		this.legalIdentifier = legalIdentifier;
	}

	public void setFiscalIdentifier(String fiscalIdentifier) {
		this.fiscalIdentifier = fiscalIdentifier;
	}
}