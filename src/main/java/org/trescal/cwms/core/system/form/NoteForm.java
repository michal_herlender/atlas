package org.trescal.cwms.core.system.form;

import javax.validation.Valid;

import org.trescal.cwms.core.system.entity.note.Note;

public class NoteForm
{
	private String entityName;

	private String href;
	private String hrefText;
	private Note note;
	private Boolean privateOnly;

	public String getEntityName()
	{
		return this.entityName;
	}

	public String getHref()
	{
		return this.href;
	}

	public String getHrefText()
	{
		return this.hrefText;
	}

	@Valid
	public Note getNote()
	{
		return this.note;
	}

	/**
	 * @return the privateOnly
	 */
	public Boolean getPrivateOnly()
	{
		return this.privateOnly;
	}

	public void setEntityName(String entityName)
	{
		this.entityName = entityName;
	}

	public void setHref(String href)
	{
		this.href = href;
	}

	public void setHrefText(String hrefText)
	{
		this.hrefText = hrefText;
	}

	public void setNote(Note note)
	{
		this.note = note;
	}

	/**
	 * @param privateOnly the privateOnly to set
	 */
	public void setPrivateOnly(Boolean privateOnly)
	{
		this.privateOnly = privateOnly;
	}
}
