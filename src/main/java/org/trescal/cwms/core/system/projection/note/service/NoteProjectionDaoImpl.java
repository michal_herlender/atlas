package org.trescal.cwms.core.system.projection.note.service;

import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AbstractDaoImpl;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery_;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem_;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.DeliveryItemNote;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.DeliveryItemNote_;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.DeliveryNote;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.DeliveryNote_;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.projection.note.NoteProjectionDTO;

@Repository
public class NoteProjectionDaoImpl extends AbstractDaoImpl implements NoteProjectionDao {
	@Override
	public List<NoteProjectionDTO> getDeliveryItemNotes(Integer deliveryId) {
		return getResultList(cb -> {
			CriteriaQuery<NoteProjectionDTO> cq = cb.createQuery(NoteProjectionDTO.class);
			Root<DeliveryItemNote> root = cq.from(DeliveryItemNote.class);
			Join<DeliveryItemNote, DeliveryItem> deliveryItem = root.join(DeliveryItemNote_.deliveryitem, JoinType.INNER);
			Join<DeliveryItem, Delivery> delivery = deliveryItem.join(DeliveryItem_.delivery, JoinType.INNER);
			cq.where(cb.equal(delivery.get(Delivery_.deliveryid), deliveryId));
			
			cq.select(cb.construct(NoteProjectionDTO.class, 
					root.get(DeliveryItemNote_.noteid),
					cb.literal(NoteType.DELIVERYITEMNOTE.name()),
					deliveryItem.get(DeliveryItem_.delitemid),
					root.get(DeliveryItemNote_.active),
					root.get(DeliveryItemNote_.label),
					root.get(DeliveryItemNote_.note),
					root.get(DeliveryItemNote_.publish)
				));
			return cq;
		});
	}
	
	@Override
	public List<NoteProjectionDTO> getDeliveryNotes(Collection<Integer> deliveryIds) {
		if (deliveryIds == null) throw new IllegalArgumentException("deliveryIds was null!");
		if (deliveryIds.isEmpty()) throw new IllegalArgumentException("deliveryIds was empty!");
		return getResultList(cb -> {
			CriteriaQuery<NoteProjectionDTO> cq = cb.createQuery(NoteProjectionDTO.class);
			Root<DeliveryNote> root = cq.from(DeliveryNote.class);
			Join<DeliveryNote, Delivery> delivery = root.join(DeliveryNote_.delivery, JoinType.INNER);
			cq.where(delivery.get(Delivery_.deliveryid).in(deliveryIds));
			
			cq.select(cb.construct(NoteProjectionDTO.class, 
					root.get(DeliveryItemNote_.noteid),
					cb.literal(NoteType.DELIVERYNOTE.name()),
					delivery.get(Delivery_.deliveryid),
					root.get(DeliveryItemNote_.active),
					root.get(DeliveryItemNote_.label),
					root.get(DeliveryItemNote_.note),
					root.get(DeliveryItemNote_.publish)
				));
			
			return cq;
		});
	}

}
