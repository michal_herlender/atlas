package org.trescal.cwms.core.system.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWorkDto;
import org.trescal.cwms.core.system.entity.upcomingwork.db.UpcomingWorkService;
import org.trescal.cwms.spring.model.KeyValue;

@RestController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
@RequestMapping("upcomingWork")
public class UpcomingWorkController {

    private final UpcomingWorkService upcomingWorkService;

    public UpcomingWorkController(UpcomingWorkService upcomingWorkService) {
        this.upcomingWorkService = upcomingWorkService;
    }

    @PostMapping(value = "/add",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createUpcomingWork(@RequestBody UpcomingWorkDto upcomingWorkDto,
                                             @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto){
        try {
            this.upcomingWorkService.addUpcomingWork( upcomingWorkDto,subdivDto.getKey());
            return ResponseEntity.status(HttpStatus.OK)
                    .body("Success" );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        }
    }

    @DeleteMapping(value = "/delete")
    public ResponseEntity deleteUpcomingWork(@RequestParam Integer upcomingWorkId){
        try {
            this.upcomingWorkService.deleteUpcomingWork(upcomingWorkId);
            return ResponseEntity.status(HttpStatus.OK)
                    .body("Success" );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        }
    }

    @PutMapping(value = "/update",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateUpcomingWork(@RequestBody UpcomingWorkDto upcomingWorkDto){
        try {
            this.upcomingWorkService.updateUpcomingWork( upcomingWorkDto);
            return ResponseEntity.status(HttpStatus.OK)
                    .body("Success" );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        }
    }
}