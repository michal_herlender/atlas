package org.trescal.cwms.core.system.entity.emailrecipient;

/**
 * Enum identifying the two main types of {@link EmailRecipient}
 * 
 * @author JamieV
 */
public enum RecipientType
{
	EMAIL_CC("CC"), EMAIL_TO("TO");

	private String type;

	RecipientType(String type)
	{
		this.type = type;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}
}