package org.trescal.cwms.core.system.entity.userinstructiontype.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.userinstructiontype.UserInstructionType;

public interface UserInstructionTypeDao extends BaseDao<UserInstructionType, Integer> {}