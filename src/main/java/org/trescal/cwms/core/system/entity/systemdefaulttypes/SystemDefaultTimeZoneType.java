package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.time.ZoneId;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

public abstract class SystemDefaultTimeZoneType extends SystemDefaultType<TimeZone> {

	@Override
	public boolean isDiscreteType() {
		return true;
	}

	@Override
	public boolean isMultiChoice() {
		return false;
	}

	@Override
	public List<String> getValues() {
		return ZoneId.getAvailableZoneIds().stream().sorted().collect(Collectors.toList());
	}

	@Override
	public TimeZone parseValue(String value) {
		return TimeZone.getTimeZone(value);
	}

	@Override
	public String getStringValue(TimeZone timeZone) {
		return timeZone.toZoneId().getId();
	}

}
