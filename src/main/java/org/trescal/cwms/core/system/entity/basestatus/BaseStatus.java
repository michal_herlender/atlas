package org.trescal.cwms.core.system.entity.basestatus;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.system.entity.status.Status;

/**
 * Base class for status tables. Most status entites will inherit from the
 * {@link Status} entity.
 * 
 * @author Richard
 */
@MappedSuperclass
public abstract class BaseStatus
{
	protected String name;
	protected String description;
	protected int statusid;
	protected Boolean defaultStatus;
	
	public BaseStatus() {
		defaultStatus = false;
	}

	@Column(name = "description", nullable = true, length = 200)
	public String getDescription()
	{
		return this.description;
	}

	@NotNull
	@Column(name = "name", nullable = false, length = 100)
	public String getName()
	{
		return this.name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "statusid", nullable = false, unique = true)
	@Type(type = "int")
	public int getStatusid()
	{
		return this.statusid;
	}

	@NotNull
	@Column(name = "defaultstatus", nullable = false, columnDefinition="bit")
	public Boolean getDefaultStatus()
	{
		return this.defaultStatus;
	}

	public void setDefaultStatus(Boolean defaultStatus)
	{
		this.defaultStatus = defaultStatus;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setStatusid(int statusid)
	{
		this.statusid = statusid;
	}
}
