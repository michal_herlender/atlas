package org.trescal.cwms.core.system.entity.closeddate;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.company.Company;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "closeddate")
public class ClosedDate {
    private LocalDate closedDate;
    private Company comp;
    private int id;
    private String reason;

    @Column(name = "closeddate", columnDefinition = "date")
    public LocalDate getClosedDate() {
        return this.closedDate;
    }

    public void setClosedDate(LocalDate closedDate) {
        this.closedDate = closedDate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coid")
    public Company getComp() {
        return this.comp;
    }

    public void setComp(Company comp) {
        this.comp = comp;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Length(max = 100)
    @Column(name = "reason", length = 100)
    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}