package org.trescal.cwms.core.system.entity.userinstructiontype.db;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.userinstructiontype.UserInstructionType;

public interface UserInstructionTypeService
{
	void deleteUserInstructionType(UserInstructionType uit);

	/**
	 * this method deletes the {@link UserInstructionType} identified by the id
	 * passed and so disabling the viewing of this instruction type for the
	 * user.
	 * 
	 * @param userinstructiontypeid id of the user instruction type to be
	 *        deleted
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper deleteUserInstructionTypeDWR(Integer userinstructiontypeid, HttpServletRequest req);

	UserInstructionType findUserInstructionType(int instructiontypeid);

	List<UserInstructionType> getAllUserInstructionTypes();

	void insertUserInstructionType(UserInstructionType uit);

	/**
	 * this method inserts a {@link UserInstructionType} which links the current
	 * {@link Contact}'s {@link UserPreferences} to a {@link InstructionType}.
	 * This means they wish to view the type of instruction.
	 * 
	 * @param userprefsid the id of the contacts user preferences
	 * @param instructiontypeid the id of the instruction type the contact
	 *        wishes to view
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper insertUserInstructionTypeDWR(Integer userprefsid, InstructionType instructionType, HttpServletRequest req);

	void updateUserInstructionType(UserInstructionType uit);
}
