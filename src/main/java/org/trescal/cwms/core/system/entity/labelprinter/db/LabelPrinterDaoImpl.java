package org.trescal.cwms.core.system.entity.labelprinter.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.labelprinter.LabelPrinter;

@Repository("LabelPrinterDao")
public class LabelPrinterDaoImpl extends BaseDaoImpl<LabelPrinter, Integer> implements LabelPrinterDao {
	
	@Override
	protected Class<LabelPrinter> getEntity() {
		return LabelPrinter.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<LabelPrinter> getLabelPrintersAtAddress(int addrId) {
		Criteria criteria = getSession().createCriteria(LabelPrinter.class);
		criteria.createCriteria("addr").add(Restrictions.idEq(addrId));
		return criteria.list();
	}
}