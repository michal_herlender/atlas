package org.trescal.cwms.core.system.entity.accreditationbody;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.country.Country;
import org.trescal.cwms.core.system.entity.accreditationbodyresource.AccreditationBodyResource;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;

@Entity
@Table(name = "accreditationbody", uniqueConstraints={@UniqueConstraint(columnNames= {"countryid", "shortName"})})
public class AccreditationBody extends Versioned {
	private int id;
	private String shortName;
	private Country country;
	private Set<AccreditedLab> accreditedLabs;
	private Set<AccreditationBodyResource> accreditationBodyResources;
	
	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return id;
	}

	@NotNull
	@Column(name = "shortName", nullable = false, columnDefinition = "nvarchar(25)")
	public String getShortName() {
		return shortName;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "countryid", nullable=false)
	public Country getCountry() {
		return country;
	}
	
	@OneToMany(mappedBy = "accreditationBody", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<AccreditedLab> getAccreditedLabs() {
		return accreditedLabs;
	}
	
	@OneToMany(mappedBy = "accreditationBody", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<AccreditationBodyResource> getAccreditationBodyResources() {
		return accreditationBodyResources;
	}
	
	public void setShortName(String name) {
		this.shortName = name;
	}
	public void setCountry(Country country) {
		this.country = country;
	}
	public void setAccreditedLabs(Set<AccreditedLab> accreditedLabs) {
		this.accreditedLabs = accreditedLabs;
	}
	public void setAccreditationBodyResources(
			Set<AccreditationBodyResource> accreditationBodyImages) {
		this.accreditationBodyResources = accreditationBodyImages;
	}
	public void setId(int id) {
		this.id = id;
	}
}
