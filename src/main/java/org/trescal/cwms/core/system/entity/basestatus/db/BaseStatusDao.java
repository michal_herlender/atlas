package org.trescal.cwms.core.system.entity.basestatus.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.basestatus.BaseStatus;
import org.trescal.cwms.core.system.entity.status.Status;

public interface BaseStatusDao extends BaseDao<BaseStatus, Integer>
{
	<Status extends BaseStatus> Status findDefaultStatus(Class<Status> clazz);

	<Status extends BaseStatus> Status  findStatusByName(String name, Class<Status> clazz);
}