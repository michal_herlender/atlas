package org.trescal.cwms.core.system.entity.accreditedlabcaltype.db;

import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.core.system.entity.accreditedlabcaltype.AccreditedLabCalType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

public class AccreditedLabCalTypeServiceImpl implements AccreditedLabCalTypeService
{
	private AccreditedLabCalTypeDao accreditedLabCalTypeDao;
	
	private static String DEFAULT_LABEL_TEMPLATE = "/labels/birt/birt_label_standard.rptdesign";

	@Override
	public AccreditedLabCalType findAccreditedLabCalType(AccreditedLab accreditedLab, CalibrationType calType)
	{
		return this.accreditedLabCalTypeDao.findAccreditedLabCalType(accreditedLab, calType);
	}

	@Override
	public String getLabelTemplate(AccreditedLab accreditedLab, CalibrationType calType)
	{
		AccreditedLabCalType alct = this.findAccreditedLabCalType(accreditedLab, calType);
		String ltp = alct != null ? alct.getLabelTemplatePath() : null;

		// return label template path if specified, but fallback on default if necessary
		return ((ltp != null) && !ltp.trim().isEmpty()) ? ltp : DEFAULT_LABEL_TEMPLATE;
	}

	public void setAccreditedLabCalTypeDao(AccreditedLabCalTypeDao accreditedLabCalTypeDao)
	{
		this.accreditedLabCalTypeDao = accreditedLabCalTypeDao;
	}
}
