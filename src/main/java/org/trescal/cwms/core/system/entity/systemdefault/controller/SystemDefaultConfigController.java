package org.trescal.cwms.core.system.entity.systemdefault.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultType;

@Controller @IntranetController
public class SystemDefaultConfigController
{
	private static final String View = "trescal/core/company/systemdefaultconfig";
	
	@Autowired
	private List<SystemDefaultType<?>> systemDefaultTypes;
	
	@ModelAttribute("systemDefaultTypes")
	public List<SystemDefaultType<?>> getSystemDefaultTypes() {
		return systemDefaultTypes;
	}
	
	@RequestMapping(value="systemdefaultconfig.htm", method=RequestMethod.GET)
	public String onRequest() {
		return View;
	}
}