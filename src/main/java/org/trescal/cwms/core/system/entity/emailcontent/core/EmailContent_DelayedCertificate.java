package org.trescal.cwms.core.system.entity.emailcontent.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;

import java.time.LocalDate;
import java.util.Locale;

/**
 * Extracted from CertificateServiceImpl.delayCertificate(...) so we can preview/test content
 * TODO : Once templates done, we can remove the velocity part
 *
 * @author galen
 */
@Component
public class EmailContent_DelayedCertificate {
	@Autowired
	private BusinessDetailsService bdServ;
	@Autowired
	private MessageSource messages; 
	@Autowired
	private TemplateEngine templateEngine;

	@Value("${cwms.deployed.url}")
	private String deployedURL;

	public static final String THYMELEAF_VIEW = "/email/core/delayedcertificatenotification.html";
	
	public EmailContentDto getContent(Certificate cert, Locale locale) {
		// A delayed certificate is expected to always have a calibration attached, 
		// however accommodate other types of certificates in case used for testing, etc... 
		Subdiv businessSubdiv = cert.getCal() != null ? cert.getCal().getOrganisation() : null; 
		BusinessDetails bdetails = this.bdServ.getAllBusinessDetails(businessSubdiv);
		String body = getBodyThymeleaf(cert, bdetails, locale);
		String subject = getSubject(cert, locale);
		return new EmailContentDto(body, subject);
	}
	
	private String getBodyThymeleaf(Certificate cert, BusinessDetails bdetails, Locale locale) {
		Contact engineer = cert.getCal().getCompletedBy();
		// get current date

		Context context = new Context(locale);
		context.setVariable("cert", cert);
		context.setVariable(Constants.SESSION_ATTRIBUTE_BUSINESS_DETAILS, bdetails);
		context.setVariable("engineer", engineer);
		context.setVariable("date", LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		context.setVariable("jiCertPath", this.deployedURL + "jicertificates.htm?jobitemid="
			+ cert.getLinks().iterator().next().getJobItem().getJobItemId());

		String body = this.templateEngine.process(THYMELEAF_VIEW, context);
		return body;
	}

	private String getSubject(Certificate cert, Locale locale) {
		// create subject string
		return this.messages.getMessage("email.delaycert.subject", new Object[] { cert.getCertno() },
				"Delayed certificate signing notification (".concat(cert.getCertno()).concat(")"), locale);
	}
}