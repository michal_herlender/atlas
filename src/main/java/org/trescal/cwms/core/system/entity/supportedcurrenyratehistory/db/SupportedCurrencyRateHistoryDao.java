package org.trescal.cwms.core.system.entity.supportedcurrenyratehistory.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.supportedcurrenyratehistory.SupportedCurrencyRateHistory;

public interface SupportedCurrencyRateHistoryDao extends BaseDao<SupportedCurrencyRateHistory, Integer> {}