package org.trescal.cwms.core.system.entity.alligatorsettings.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettings;

@Service
public class AlligatorSettingsServiceImpl extends BaseServiceImpl<AlligatorSettings, Integer>
		implements AlligatorSettingsService {
	
	@Autowired
	private AlligatorSettingsDao alligatorSettingsDao; 
	
	private final static Logger logger = LoggerFactory.getLogger(AlligatorSettingsServiceImpl.class);

	@Override
	protected BaseDao<AlligatorSettings, Integer> getBaseDao() {
		return alligatorSettingsDao;
	}
	
	@Override
	public AlligatorSettings getSettings(Contact businessContact, boolean production) {
		logger.debug("Production system : "+production);
		// Step 1 - Get settings for contact, if available
		AlligatorSettings result = this.alligatorSettingsDao.getSettingsForContact(businessContact, production);
		if (result == null) {
			// Step 2 - Get default settings for contact's business subdivision, if available
			result = this.alligatorSettingsDao.getSettingsForSubdiv(businessContact.getSub(), production);
			if (result == null) {
				// Step 3 - Get global default settings as fallback
				result = this.alligatorSettingsDao.getDefaultSettings();
				logger.debug("Retrived default settings for contact id "+businessContact.getPersonid());
			}
			else {
				logger.debug("Retrived subdiv settings with id "+result.getId()+" for contact id "+businessContact.getPersonid());
			}
		}
		else {
			logger.debug("Retrived contact settings with id "+result.getId()+" for contact id "+businessContact.getPersonid());
		}
		return result;
	}

}
