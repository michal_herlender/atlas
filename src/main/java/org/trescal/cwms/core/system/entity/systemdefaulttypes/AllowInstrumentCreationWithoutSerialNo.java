package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class AllowInstrumentCreationWithoutSerialNo extends SystemDefaultBooleanType {

    @Override
    public SystemDefaultNames defaultName() {
        return SystemDefaultNames.CREATE_INSTRUMENT_WITHOUT_SERIAL_NO;
    }

}
