package org.trescal.cwms.core.system.entity.systemdefaultapplication.db;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.control.Option;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefault.db.SystemDefaultService;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;
import org.trescal.cwms.core.system.enums.Scope;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Service("SystemDefaultApplicationService")
public class SystemDefaultApplicationServiceImpl extends BaseServiceImpl<SystemDefaultApplication, Integer>
		implements SystemDefaultApplicationService {

	@Autowired
	private ContactService conServ;
	@Autowired
	private SubdivService subServ;
	@Autowired
	private SystemDefaultApplicationDao systemDefaultApplicationDao;
	@Autowired
	private SystemDefaultService systemDefServ;

	@Override
	protected BaseDao<SystemDefaultApplication, Integer> getBaseDao() {
		return systemDefaultApplicationDao;
	}

	@Override
	public SystemDefaultApplication get(int defaultId, Scope scope, int entityId, Integer allocatedCompanyId) {
		return this.systemDefaultApplicationDao.get(defaultId, scope, entityId, allocatedCompanyId);
	}

	@Override
	public SystemDefaultApplication get(int defaultId, Scope scope, int entityId, Company allocatedCompany) {
		return this.get(defaultId, scope, entityId, allocatedCompany == null ? null : allocatedCompany.getCoid());
	}


	private Optional<SystemDefaultApplication> getOpt(int defaultId, Scope scope, Integer entityId, Company allocatedCompany) {
		return systemDefaultApplicationDao.getOpt(defaultId, scope, entityId, allocatedCompany == null ? null : allocatedCompany.getCoid());
	}

	private Option<SystemDefaultApplication> getOptHierarchical(Integer defaultId, Scope scope, Integer entityId, Company allocatedCompay) {
		Option<SystemDefaultApplication> maybeDefault = Option.ofOptional(getOpt(defaultId, scope, entityId, allocatedCompay));
		return maybeDefault.orElse(() -> {
			switch (scope) {
				case CONTACT:
					int subdivid = conServ.get(entityId).getSub().getSubdivid();
					return getOptHierarchical(defaultId, Scope.SUBDIV, subdivid, allocatedCompay);
				case SUBDIV:
					int companyId = subServ.get(entityId).getComp().getCoid();
					return getOptHierarchical(defaultId, Scope.COMPANY, companyId, allocatedCompay);
				case COMPANY:
					return getOptHierarchical(defaultId, Scope.SYSTEM, null, allocatedCompay);
				case SYSTEM:
				default:
					return Option.none();
			}
		});
	}

	@Override
	public <V> Tuple2<Scope, V> getGenericValueHierachical(int defaultId, Scope scope, int entityId, Company allocatedCompay, Function<String, V> parser) {
		val maybeDefault = getOptHierarchical(defaultId, scope, entityId, allocatedCompay)
			.toJavaOptional();
		return maybeDefault.map(sda -> Tuple.of(sda.getScope(), parser.apply(sda.getValue())))
			.orElseGet(() -> {
				val sd = systemDefServ.get(SystemDefaultNames.getByOrdinal(defaultId));
				return Tuple.of(Scope.SYSTEM, parser.apply(sd.getDefaultValue()));
			})
			;
	}


	@Override
	public SystemDefaultApplication findSystemDefaultApplication(int defaultId, Integer personid, Integer subdivid,
																 Integer coid) {
		return this.systemDefaultApplicationDao.findSystemDefaultApplication(defaultId, personid, subdivid, coid);
	}

	@Override
	public List<SystemDefaultApplication> find(List<SystemDefault> listSD, Company company, Subdiv subdiv,
											   Contact contact, Integer allocatedCompanyId) {
		if (listSD.isEmpty()) {
			// No need for query (no system defaults), and query requires at least 1 system
			// default to look up
			return Collections.emptyList();
		} else {
			return this.systemDefaultApplicationDao.find(listSD, company, subdiv, contact, allocatedCompanyId);
		}
	}

	@Override
	public List<SystemDefaultApplication> findApplicationWide(List<SystemDefault> listSD) {
		return systemDefaultApplicationDao.findApplicationWide(listSD);
	}
}