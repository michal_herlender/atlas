package org.trescal.cwms.core.system.entity.printertray.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.papertype.PaperType;
import org.trescal.cwms.core.system.entity.printertray.PrinterTray;

@Repository("PrinterTrayDao")
public class PrinterTrayDaoImpl extends BaseDaoImpl<PrinterTray, Integer> implements PrinterTrayDao {
	
	@Override
	protected Class<PrinterTray> getEntity() {
		return PrinterTray.class;
	}
	
	@SuppressWarnings("unchecked")
	public PrinterTray findPrinterTrayForPaperType(int printerId, PaperType paperType) {
		Criteria crit = getSession().createCriteria(PrinterTray.class);
		crit.createCriteria("printer").add(Restrictions.idEq(printerId));
		crit.add(Restrictions.eq("paperType", paperType));
		List<PrinterTray> trays = crit.list();
		return trays.size() > 0 ? trays.get(0) : null;
	}
}