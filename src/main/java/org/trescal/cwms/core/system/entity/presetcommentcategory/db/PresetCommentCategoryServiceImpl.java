package org.trescal.cwms.core.system.entity.presetcommentcategory.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.presetcommentcategory.PresetCommentCategory;

@Service("PresetCommentCategoryService")
public class PresetCommentCategoryServiceImpl extends BaseServiceImpl<PresetCommentCategory, Integer>
		implements PresetCommentCategoryService {

	@Autowired
	private PresetCommentCategoryDao presetCommentCategoryDao;

	@Override
	protected BaseDao<PresetCommentCategory, Integer> getBaseDao() {
		return presetCommentCategoryDao;
	}

	@Override
	public PresetCommentCategory getByName(String category) {
		return presetCommentCategoryDao.getByName(category);
	}

	@Override
	public List<PresetCommentCategory> getAllByType(PresetCommentType commentType) {
		return presetCommentCategoryDao.getAllByType(commentType);
	}
}