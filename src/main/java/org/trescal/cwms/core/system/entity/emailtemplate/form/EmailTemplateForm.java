package org.trescal.cwms.core.system.entity.emailtemplate.form;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

public class EmailTemplateForm {
	private int id;				// If 0, we are adding a new template, otherwise edit (used in validator)
	private Component component;
	private String languageTag;
	private int compid;			// Used in validator
	private int subdivid;		// If 0, not subdiv specific
	private String template;
	private String subject;
	
	public int getId() {
		return id;
	}
	@NotNull(message="{error.value.notselected}")
	public String getLanguageTag() {
		return languageTag;
	}
	public Component getComponent() {
		return component;
	}
	public int getCompid() {
		return compid;
	}
	public int getSubdivid() {
		return subdivid;
	}
	@NotEmpty
	public String getTemplate() {
		return template;
	}
	@NotEmpty
	public String getSubject() {
		return subject;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setLanguageTag(String languageTag) {
		this.languageTag = languageTag;
	}
	public void setSubdivid(int subdivid) {
		this.subdivid = subdivid;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public void setComponent(Component component) {
		this.component = component;
	}
	public void setCompid(int compid) {
		this.compid = compid;
	}
}
