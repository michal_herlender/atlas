package org.trescal.cwms.core.system.entity.emailcontent;

/**
 * Enumerates the different types of emails that can be previewed / generated via this framework
 */
public enum EmailContentType {
	AUTOSERVICE_CHASE_CLIENT_ITEMS("Autoservice - Chase Client Items (Entity : Job)", true),
	AUTOSERVICE_CHASE_CLIENT_SUMMARY("Autoservice - Chase Client Summary (Entity : Job)", true),
	AUTOSERVICE_CHASE_SUPPLIER_ITEMS("Autoservice - Chase Supplier Items (Entity : Job)", true),
	AUTOSERVICE_CHASE_SUPPLIER_SUMMARY("Autoservice - Chase Supplier Summary (Entity : Job)", true),
	
	CORE_CONTACT_REMINDER("Core - Contact Reminder (Entity : Contact)", true),
	CORE_CONTRACT_REVIEW_FEEDBACK("Core - Contract Review Feedback (Entity : Job)", true),
	CORE_DELAYED_CERTIFICATE("Core - Delayed Certificate (Entity : Certificate)", true),
	CORE_FEEDBACK_CONFIRMATION("Core - Feedback Confirmation (Entity : Contact)", true),
	CORE_HIRE_ENQUIRY_CONFIRMATION("Core - Hire Enquiry Confirmation (Entity : Hire)", true),
	CORE_SCHEDULE_CONFIRMATION("Core - Schedule Confirmation (Entity : Schedule)", true),
	CORE_SCHEDULED_QUOTE_REQUEST_FAIL("Core - Scheduled Quote Request Fail (Entity : ScheduledQuotationRequest)", true),
	CORE_SCHEDULED_QUOTE_REQUEST_SUCCESS("Core - Scheduled Quote Request Success (Entity : ScheduledQuotationRequest)", true),
	CORE_UNSIGNED_CERTIFICATE("Core - Unsigned Certificate (Entity : Certificate)", true),

	RECALL_MESSAGE("Recall - Recall Message (Entity : RecallDetail)", true),	
	
	PORTAL_CLIENT_COLLECTION("Portal - Client Collection (Entity : Schedule)", true),
	PORTAL_CLIENT_QUOTE_REQUEST("Portal - Client Quote Request (Entity : Quotation)", true),
	PORTAL_COLLECTION("Portal - Collection (Entity : Schedule)", true),
	PORTAL_INSTRUMENT_VALIDATION("Portal - Instrument Validation (Entity : InstrumentValidationIssue)", true),
	PORTAL_FEEDBACK("Portal - Feedback (Entity : Contact)", true),
	PORTAL_QUOTE_REQUEST("Portal - Quote Request (Entity : Quotation)", true);
	
	private String description;
	private Boolean requiresEntity;
	
	private EmailContentType(String description, Boolean requiresEntity) {
		this.description = description;
		this.requiresEntity = requiresEntity;
	}

	public String getDescription() {
		return description;
	}

	public Boolean getRequiresEntity() {
		return requiresEntity;
	}
}
