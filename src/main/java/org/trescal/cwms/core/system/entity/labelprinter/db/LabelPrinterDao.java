package org.trescal.cwms.core.system.entity.labelprinter.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.labelprinter.LabelPrinter;

public interface LabelPrinterDao extends BaseDao<LabelPrinter, Integer> {
	
	List<LabelPrinter> getLabelPrintersAtAddress(int addrId);
}