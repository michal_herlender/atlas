package org.trescal.cwms.core.system.entity.upcomingwork.db;

import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.UpcomingWorkJobLinkAjaxDto;
import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.db.UpcomingWorkJobLinkException;
import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.db.UpcomingWorkJobLinkService;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWorkDto;
import org.trescal.cwms.core.tools.DateTools;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service("UpcomingWorkService")
public class UpcomingWorkServiceImpl extends BaseServiceImpl<UpcomingWork, Integer> implements UpcomingWorkService {
	private static final String DATE_IN_THE_FORM = "Incorrect dates provided. Please provide at least a due date in the form ";
	private final CompanyService compServ;
	private final ContactService contServ;
	private final DepartmentService deptServ;
	private final SessionUtilsService sessionServ;
	private final SubdivService subdivService;
	private final UpcomingWorkDao upcomingWorkDao;
	private final UpcomingWorkJobLinkService upcomingWorkJobLinkService;
	private final JobService jobService;

	public UpcomingWorkServiceImpl(CompanyService compServ, ContactService contServ, DepartmentService deptServ, SessionUtilsService sessionServ,
								   SubdivService subdivService, UpcomingWorkDao upcomingWorkDao, UpcomingWorkJobLinkService upcomingWorkJobLinkService,
								   JobService jobService) {
		this.compServ = compServ;
		this.contServ = contServ;
		this.deptServ = deptServ;
		this.sessionServ = sessionServ;
		this.subdivService = subdivService;
		this.upcomingWorkDao = upcomingWorkDao;
		this.upcomingWorkJobLinkService = upcomingWorkJobLinkService;
		this.jobService = jobService;
	}

	@Override
	protected BaseDao<UpcomingWork, Integer> getBaseDao() {
		return upcomingWorkDao;
	}

	public void addUpcomingWork(UpcomingWorkDto upcomingWorkDto,Integer subdivId) throws UpcomingWorkException {
		Subdiv allocatedSubdiv = subdivService.get(subdivId);
		if (allocatedSubdiv == null){
			throw new UpcomingWorkException("Subdivision "+subdivId+" not found");
		}
		UpcomingWork upcomingWork = new UpcomingWork();

		saveOrUpdateUpcomingWork(upcomingWorkDto, upcomingWork);
		upcomingWork.setAddedBy(this.sessionServ.getCurrentContact());
		upcomingWork.setOrganisation(allocatedSubdiv);
		List<Job> jobs = verifyJobs(upcomingWorkDto.getJobNoList(), allocatedSubdiv);
		UpcomingWork work = this.merge(upcomingWork);
		try {
			this.upcomingWorkJobLinkService.addJobToUpcomingWork(jobs,work);
		} catch (UpcomingWorkJobLinkException e) {
			throw new UpcomingWorkException(e.getMessage());
		}
	}

	public void updateUpcomingWork(UpcomingWorkDto upcomingWorkDto) throws UpcomingWorkException {
		UpcomingWork upcomingWork = this.get(upcomingWorkDto.getUpcomingWorkId());
		if (upcomingWork != null) {

			saveOrUpdateUpcomingWork(upcomingWorkDto, upcomingWork);
			this.merge(upcomingWork);


		} else {
			throw new UpcomingWorkException("The upcoming work could not be found");
		}
	}

	private void saveOrUpdateUpcomingWork(UpcomingWorkDto upcomingWorkDto, UpcomingWork upcomingWork) throws UpcomingWorkException {
		if ((upcomingWorkDto.getEnd() != null) && !upcomingWorkDto.getEnd().trim().isEmpty()) {
			try {
				upcomingWork.setStartDate(((upcomingWorkDto.getStart() != null) && !upcomingWorkDto.getStart().trim().isEmpty()) ? LocalDate.parse(upcomingWorkDto.getStart(), DateTimeFormatter.ISO_DATE) : null);
				upcomingWork.setDueDate(!upcomingWorkDto.getEnd().trim().isEmpty() ? LocalDate.parse(upcomingWorkDto.getEnd(), DateTimeFormatter.ISO_DATE) : null);
			} catch (Exception e) {
				throw new UpcomingWorkException(DATE_IN_THE_FORM + DateTools.df.toPattern().toLowerCase());
			}
			if (upcomingWorkDto.getPersonId() != 0) {
				Contact contact = this.contServ.get(upcomingWorkDto.getPersonId());
				if (contact != null) {
					upcomingWork.setContact(contact);
					upcomingWork.setComp(contact.getSub().getComp());
				} else {
					throw new UpcomingWorkException("The contact selected could not be found");
				}
			} else {
				Company comp = this.compServ.get(upcomingWorkDto.getCoId());
				if (comp != null) {
					upcomingWork.setComp(comp);
					upcomingWork.setContact(null);
				} else {
					throw new UpcomingWorkException("The company selected could not be found");
				}
			}

			Department dept = this.deptServ.get(upcomingWorkDto.getDeptId());
			if (dept != null) {
				upcomingWork.setDepartment(dept);
			} else {
				throw new UpcomingWorkException("The department selected could not be found");
			}

			Contact userResponsible = this.contServ.get(upcomingWorkDto.getUserResponsibleId());
			if (userResponsible != null) {
				upcomingWork.setUserResponsible(userResponsible);
			} else {
				throw new UpcomingWorkException("The user responsible selected could not be found");
			}

			upcomingWork.setActive(true);
			upcomingWork.setTitle(((upcomingWorkDto.getTitle() == null) || upcomingWorkDto.getTitle().trim().isEmpty()) ? "- No title -" : upcomingWorkDto.getTitle());
			upcomingWork.setDescription(((upcomingWorkDto.getDesc() == null) || upcomingWorkDto.getDesc().trim().isEmpty()) ? "- No description -" : upcomingWorkDto.getDesc());


		}
		else {
			throw new UpcomingWorkException(DATE_IN_THE_FORM + DateTools.df.toPattern().toLowerCase());
		}
	}

	public void deleteUpcomingWork(Integer upcomingId) throws UpcomingWorkException {
		UpcomingWork upcomingWork = this.get(upcomingId);
		if (upcomingWork != null) {
			this.upcomingWorkJobLinkService.deleteAllUpcomingWorkJobLinkByUpcomingWork(upcomingWork);
			this.delete(upcomingWork);
		}
		else {
			throw new UpcomingWorkException("The upcoming work could not be found");
		}
	}
	


	public Long countUpcomingWorkBetweenDates(LocalDate startDate, LocalDate endDate, Integer allocatedSubdivId) {
		return this.upcomingWorkDao.countUpcomingWorkBetweenDates(startDate, endDate, allocatedSubdivId);
	}

	public List<UpcomingWork> getUpcomingWorkBetweenDates(LocalDate startDate, LocalDate endDate, Subdiv allocatedSubdiv) {
		return this.upcomingWorkDao.getUpcomingWorkBetweenDates(startDate, endDate, allocatedSubdiv);
	}

	public List<UpcomingWork> getUpcomingWorkForUser(int personid, Subdiv allocatedSubdiv) {
		return this.upcomingWorkDao.getUpcomingWorkForUser(personid, allocatedSubdiv);
	}

	List<Job> verifyJobs(List<UpcomingWorkJobLinkAjaxDto> upcomingWorkJobLinkAjaxDtos,Subdiv subdiv) throws UpcomingWorkException {
		List<Job> jobs = new ArrayList<>();
		for (UpcomingWorkJobLinkAjaxDto dto : upcomingWorkJobLinkAjaxDtos) {

			Job job = this.jobService.findByJobNo(dto.getJobNo());
			if (job == null) {
				throw new UpcomingWorkException("The job " + dto.getJobNo() + "could not be found");
			}

			if (job.getType() != JobType.SITE) {
				throw new UpcomingWorkException("The Job " + job.getIdentifier() + " is not site job");
			}

			if (job.getOrganisation().getSubdivid() != subdiv.getSubdivid()) {
				throw new UpcomingWorkException("The job " + dto.getJobNo() + " do not have good subdivision");
			}
			jobs.add(job);
		}
		return jobs;
	}
}