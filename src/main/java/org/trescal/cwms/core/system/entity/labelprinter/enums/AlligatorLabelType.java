package org.trescal.cwms.core.system.entity.labelprinter.enums;

public enum AlligatorLabelType {
	REGULAR, SMALL;
}
