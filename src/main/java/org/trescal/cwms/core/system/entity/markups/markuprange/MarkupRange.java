package org.trescal.cwms.core.system.entity.markups.markuprange;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.system.entity.markups.markuptype.MarkupType;

/**
 * Defines a value range and a markup action that will be applied to any values
 * within that range. There are two actions defined by the
 * {@link MarkupRangeAction} percentage and value.
 * 
 * @author Richard
 */
@Entity
@Table(name = "markuprange")
public class MarkupRange
{
	private MarkupRangeAction action;
	/**
	 * If the {@link MarkupRangeAction} is percentage then this attribute
	 * represents the percentage to be applied otherwise it represents the
	 * markup value.
	 */
	private double actionValue;
	private int id;
	private double rangeStart;
	private MarkupType type;

	@NotNull
	@Column(name = "action", nullable = false)
	public MarkupRangeAction getAction()
	{
		return this.action;
	}

	@NotNull
	@Column(name = "actionvalue", nullable = false)
	public double getActionValue()
	{
		return this.actionValue;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@Column(name = "rangestart", nullable = false)
	public double getRangeStart()
	{
		return this.rangeStart;
	}

	@NotNull
	@Column(name = "typeid", nullable=false)
	public Integer getTypeId()
	{
		if (this.type != null)
			return this.type.getId();
		
		return 0;
	}
	
	@Transient
	public MarkupType getType()
	{
		return this.type;
	}

	public void setAction(MarkupRangeAction action)
	{
		this.action = action;
	}

	/**
	 * @param actionValue the actionValue to set
	 */
	public void setActionValue(double actionValue)
	{
		this.actionValue = actionValue;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setRangeStart(double rangeStart)
	{
		this.rangeStart = rangeStart;
	}

	/**
	 * @param type the type to set
	 */
	public void setTypeId(Integer type)
	{
		if (type != null)
			setType(MarkupType.getMarkupTypeById(type));
	}
	
	public void setType(MarkupType type)
	{
		this.type = type;
	}
}
