package org.trescal.cwms.core.system.projection.note.service;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.system.projection.note.NoteProjectionDTO;

public interface NoteProjectionService {

	/*
	 * Returns all the notes attached to DeliveryItems on a specific Delivery
	 */
	List<NoteProjectionDTO> getDeliveryItemNotes(Integer deliveryId);
	
	/*
	 * Returns all the notes attached to specified IDs of Delivery 
	 */
	List<NoteProjectionDTO> getDeliveryNotes(Collection<Integer> deliveryIds);
}
