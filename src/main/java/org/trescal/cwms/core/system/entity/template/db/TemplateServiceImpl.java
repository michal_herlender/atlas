package org.trescal.cwms.core.system.entity.template.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.template.Template;

public class TemplateServiceImpl implements TemplateService
{
	TemplateDao templateDao;

	public Template findTemplate(int id)
	{
		return templateDao.find(id);
	}

	@Override
	public Template findByName(String name)
	{
		return templateDao.findByName(name);
	}
	
	public void insertTemplate(Template template)
	{
		templateDao.persist(template);
	}

	public void updateTemplate(Template template)
	{
		templateDao.update(template);
	}

	public List<Template> getAllTemplates()
	{
		return templateDao.findAll();
	}
	public TemplateDao getTemplateDao()
	{
		return templateDao;
	}

	public void setTemplateDao(TemplateDao templateDao)
	{
		this.templateDao = templateDao;
	}
}