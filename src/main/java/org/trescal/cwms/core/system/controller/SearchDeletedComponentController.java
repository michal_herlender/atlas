package org.trescal.cwms.core.system.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.entity.deletedcomponent.db.DeletedComponentService;
import org.trescal.cwms.core.system.form.SearchDeletedComponentForm;
import org.trescal.cwms.core.tools.DateTools;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Controller
@IntranetController
@RequestMapping("/deletedcomponent.htm")
public class SearchDeletedComponentController {
	@Autowired
	private DeletedComponentService delCompServ;

	public static final String FORM_NAME = "searchdeletedcomponentform";

	@InitBinder
	public void initBinder(WebDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
	}

	@RequestMapping(method = RequestMethod.GET)
	public String displayForm(@ModelAttribute(FORM_NAME) SearchDeletedComponentForm searchDeletedComponentForm)
			throws Exception {
		searchDeletedComponentForm.setDeletableComponents(this.delCompServ.getDeletableComponents());
		return "trescal/core/system/deletedcomponent";
	}

	@RequestMapping(method = RequestMethod.POST)
	protected String onSubmit(@RequestParam(value = "from", required = false) String from,
			@ModelAttribute(FORM_NAME) SearchDeletedComponentForm searchDeletedComponentForm, BindingResult result)
			throws Exception {
		if (result.hasErrors()) {
			return "trescal/core/system/deletedcomponent";
		} else {
			// if no inputs set then just default to show all deleted components
			// from last month
			if (StringUtils.isEmpty(searchDeletedComponentForm.getRefNo()) && StringUtils.isEmpty(from)) {
				GregorianCalendar cal = new GregorianCalendar();
				cal.add(Calendar.MONTH, -1);
				searchDeletedComponentForm.setFrom(cal.getTime());
			}
			searchDeletedComponentForm.setDeletableComponents(this.delCompServ.getDeletableComponents());
			searchDeletedComponentForm
					.setComponents(this.delCompServ.searchDeletedComponents(searchDeletedComponentForm.getComponent(),
							searchDeletedComponentForm.getRefNo(), searchDeletedComponentForm.getFrom()));
			return "trescal/core/system/deletedcomponent";
		}
	}
}
