package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.failurereport.enums.SentFaultReportSettingEnum;

@Component
public class SendFaultReportToClient extends SystemDefaultType<SentFaultReportSettingEnum> {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.SEND_FAULT_REPORT_TO_CLIENT;
	}

	@Override
	public boolean isDiscreteType() {
		return true;
	}

	@Override
	public List<String> getValues() {
		List<String> values = new ArrayList<String>();
		for (SentFaultReportSettingEnum ww : SentFaultReportSettingEnum.values())
			values.add(ww.name());
		return values;
	}

	@Override
	public SentFaultReportSettingEnum parseValue(String value) {
		try {
			return SentFaultReportSettingEnum.valueOf(value);
		}
		catch(Exception ex) {
			return SentFaultReportSettingEnum.CSR_DESICION;
		}
	}

	@Override
	public boolean isMultiChoice() {
		return false;
	}
}
