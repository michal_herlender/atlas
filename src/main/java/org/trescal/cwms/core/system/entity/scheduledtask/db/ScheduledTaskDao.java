package org.trescal.cwms.core.system.entity.scheduledtask.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.scheduledtask.ScheduledTask;

public interface ScheduledTaskDao extends BaseDao<ScheduledTask, Integer> {
	
	ScheduledTask findScheduledTaskByClassAndMethodNames(String className, String methodName);
}