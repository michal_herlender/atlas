package org.trescal.cwms.core.system.form;

import java.util.Locale;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.system.entity.emailcontent.EmailContentType;

public class EmailPreviewForm {
	private EmailContentType contentType;
	private Integer entityId;
	private Locale locale;
	private Boolean thymeleaf;
	
	@NotNull
	public EmailContentType getContentType() {
		return contentType;
	}
	@NotNull
	@Min(value=1)
	public Integer getEntityId() {
		return entityId;
	}
	@NotNull
	public Locale getLocale() {
		return locale;
	}
	@NotNull
	public Boolean getThymeleaf() {
		return thymeleaf;
	}
	public void setContentType(EmailContentType contentType) {
		this.contentType = contentType;
	}
	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	public void setThymeleaf(Boolean thymeleaf) {
		this.thymeleaf = thymeleaf;
	}
}
