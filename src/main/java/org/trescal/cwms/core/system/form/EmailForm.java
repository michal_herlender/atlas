package org.trescal.cwms.core.system.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.interceptor.UnescapeString;
import org.trescal.cwms.core.system.dto.EmailResultWrapper;
import org.trescal.cwms.core.system.dto.SimpleContact;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Form backing Object for handling e-mails
 *
 * @author jamiev
 */
@Data
@Builder
@AllArgsConstructor
public class EmailForm {
    private List<String> attachments;        // encryptedFilePath to file
    private String body;
    @Singular
    private List<Contact> ccCons;
    @Singular
    private List<String> ccEmails;
    private List<Integer> ccIds;
    private Integer coId;
    private Integer businessCompanyId;
    private Component component;
    private String componentAttachment;        // encryptedFilePath to file
    private boolean componentEmail;
    private org.trescal.cwms.core.system.entity.email.Email email;
    private EmailResultWrapper emailrw;
    private Integer entityId;
    private MultipartFile file;
    private String from;
    private boolean includeSelfInReply;
    private boolean includeSelfInCarbonCopy;
    private boolean publish;
    private List<EmailRecipient> recipients;
    private String subject;
    @Singular
    private List<Contact> toCons;
    @Singular
    private List<String> toEmails;
    private List<Integer> toIds;
    @Singular
    private List<SimpleContact> toContacts;
    @Singular
    private List<SimpleContact> ccContacts;

    public EmailForm(Component component, int compId, String from, Contact toCon, String to, String subject, String body, Contact sentBy) {
        this(component, from);
        this.recipients = new ArrayList<>();
        if (toCon != null) {
            this.toCons.add(toCon);
        }
        if (StringUtils.isNotEmpty(to)) {
            this.toEmails.add(to);
        }
        this.subject = subject;
        this.body = body;

        Email email = new Email();
        email.setFrom(this.getFrom());
        email.setSubject(this.getSubject());
        email.setSentBy(sentBy);
        email.setSentOn(new Date());
        email.setPublish(true);
        email.setEntityId(compId);
        email.setComponent(this.getComponent());
        email.setBody(body);
        this.setEmail(email);
    }

    public EmailForm(Component component, String fromAddress) {
        toCons = new ArrayList<>();
        this.toCons = new ArrayList<>();
        toEmails = new ArrayList<>();
        this.toEmails = new ArrayList<>();
        this.ccCons = new ArrayList<>();
        this.ccEmails = new ArrayList<>();
        this.from = fromAddress;
        this.componentEmail = true;
        this.component = component;
        this.toContacts = new ArrayList<>();
        this.ccContacts = new ArrayList<>();
    }

    public List<Integer> getCoIds(){
        return Stream.of(coId,businessCompanyId).filter(Objects::nonNull).filter(i -> i>0)
                .collect(Collectors.toList());
    }

    public static class EmailFormBuilder{
        private String subject;

        @UnescapeString
        public EmailFormBuilder subject(String subject){
            this.subject = subject;
            return this;
        }
    }
}