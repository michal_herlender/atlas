package org.trescal.cwms.core.system.entity.accreditedlab.db;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Set;

public interface AccreditedLabDao extends AllocatedDao<Subdiv, AccreditedLab, Integer>
{
    Set<KeyValue<Integer, String>> getAllBySubdivId(Integer subdivId);
}
