package org.trescal.cwms.core.system.entity.defaultnote;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;
import org.trescal.cwms.core.system.entity.translation.Translation;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table (name="defaultnote")
public class DefaultNote extends Allocated<Company>
{
	private int defaultNoteId;
	private String label;
	private String note;
	private boolean publish;
	private Set<Translation> labelTranslation;
	private Set<Translation> noteTranslation;
	
	private SystemComponent component;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column (name="defaultnoteid", nullable=false, unique = true)
	@Type (type = "int")
	public int getDefaultNoteId() {
		return defaultNoteId;
	}

	public void setDefaultNoteId(int defaultNoteId) {
		this.defaultNoteId = defaultNoteId;
	}
	
	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "componentid", nullable = false)
	public SystemComponent getComponent() {
		return component;
	}

	public void setComponent(SystemComponent component) {
		this.component = component;
	}

	@Column (name="label", nullable=true, length=50)
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Column (name="note", nullable=true, unique=false, length=5000)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	@Column (name="publish", nullable=false, columnDefinition="bit")
	@Type (type="boolean")
	public boolean isPublish() {
		return publish;
	}

	public void setPublish(boolean publish) {
		this.publish = publish;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="defaultnotelabeltranslation", joinColumns=@JoinColumn(name="defaultnoteid"))
	public Set<Translation> getLabelTranslation() {
		return labelTranslation;
	}

	public void setLabelTranslation(Set<Translation> labelTranslation) {
		this.labelTranslation = labelTranslation;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="defaultnotenotetranslation", joinColumns=@JoinColumn(name="defaultnoteid"))
	public Set<Translation> getNoteTranslation() {
		return noteTranslation;
	}

	public void setNoteTranslation(Set<Translation> noteTranslation) {
		this.noteTranslation = noteTranslation;
	}
}
