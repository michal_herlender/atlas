package org.trescal.cwms.core.system.entity.supportedcurrency.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.SupportedCurrencyAjaxDTO;
import org.trescal.cwms.core.system.dto.SupportedCurrencyOption;
import org.trescal.cwms.core.system.entity.supportedcurrency.MultiCurrencyAware;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrenyratehistory.SupportedCurrencyRateHistory;
import org.trescal.cwms.core.system.entity.supportedcurrenyratehistory.SupportedCurrencyRateHistoryComparator;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;

import static org.trescal.cwms.core.system.Constants.COMPANY_DEFAULT_CURRENCY;

@Service("SupportedCurrencyService")
public class SupportedCurrencyServiceImpl extends BaseServiceImpl<SupportedCurrency, Integer>
		implements SupportedCurrencyService {
	@Value("#{props['default.currency']}")
	private String defaultCurrencyCode;
	@Autowired
	private CompanyService companyServ;
	@Autowired
	private MessageSource messages;
	@Autowired
	private SessionUtilsService sessionServ;
	@Autowired
	private SupportedCurrencyDao supportedCurrencyDao;

	@Override
	protected BaseDao<SupportedCurrency, Integer> getBaseDao() {
		return this.supportedCurrencyDao;
	}

	@Override
	public ResultWrapper ajaxUpdateDefaultRate(int currencyid, String rate) {
		// validate the rate is valid (positive) number
		if ((rate != null) && NumberTools.isADouble(rate)) {
			double exRate = Double.parseDouble(rate);

			// try and find the currency
			SupportedCurrency currency = this.findSupportedCurrency(currencyid);
			if ((currencyid != 0) && (currency != null)) {
				// make sure the currency is not the system default
				SupportedCurrency defaultCurrency = this.getDefaultCurrency();
				if (currency.getCurrencyId() != defaultCurrency.getCurrencyId()) {
					this.updateDefaultRate(currency, exRate);
					return new ResultWrapper(true, "", currency, null);
				} else {
					return new ResultWrapper(false, this.messages.getMessage("error.currency.exchangerate.default",
							null, "The exchange rate for the default currency cannot be updated", null));
				}
			} else {
				return new ResultWrapper(false, this.messages.getMessage("error.currency.currencycode.notfound2", null,
						"The currency you requested could not be found", null));
			}
		} else {
			return new ResultWrapper(false,
					this.messages.getMessage("error.currency.invalidrate", null, "You must enter a valid rate", null));
		}
	}

	@Override
	public SupportedCurrency findCurrencyByCode(String currencyCode) {
		return this.supportedCurrencyDao.findCurrencyByCode(currencyCode);
	}

	public SupportedCurrency findSupportedCurrency(int id) {
		return this.supportedCurrencyDao.findSupportedCurrency(id);
	}

	public SupportedCurrency findSupportedCurrencyOrDefault(String currencyCode) {
		return this.supportedCurrencyDao.findSupportedCurrencyOrDefault(currencyCode, this.defaultCurrencyCode);
	}

	public List<SupportedCurrency> getAllSupportedCurrenciesWithExRates() {
		return this.supportedCurrencyDao.getAllSupportedCurrenciesWithExRates();
	}

	public List<SupportedCurrency> getAllSupportedCurrencys() {
		return this.supportedCurrencyDao.getAllSupportedCurrencys();
	}

	@Override
	public List<SupportedCurrencyOption> getCompanyCurrencyOptions(Integer coid) {
		LinkedList<SupportedCurrencyOption> options = new LinkedList<>();

		// get the company
		Company comp = this.companyServ.get(coid);

		if ((comp != null) && (coid != null)) {
			// get a list of all current currencies
			List<SupportedCurrency> currencies = this.getAllSupportedCurrenciesWithExRates();

			// get the current default currency
			SupportedCurrency defaultCurrency = this.getDefaultCurrency();

			SupportedCurrency compDefCur = null;
			BigDecimal compDefRate;

			// check if the company has a default currency and default rate for
			// that
			// currency, if so set this as
			// a separate currency entry
			boolean compDefRateSet = false;
			if (comp.getCurrency() != null) {
				compDefCur = comp.getCurrency();
				if (comp.getRate() != null) {
					compDefRate = comp.getRate();
					compDefRateSet = true;

					SupportedCurrencyOption opt = new SupportedCurrencyOption();
					opt.setCompanyDefault(true);
					opt.setCurrency(comp.getCurrency());
					opt.setRate(compDefRate);
					opt.setSystemDefault(false);
					opt.setSelected(true);
					opt.setOptionValue(Constants.COMPANY_DEFAULT_CURRENCY);
					opt.setDefaultCurrencySymbol(defaultCurrency.getCurrencySymbol());
					options.add(opt);
				}
			}

			for (SupportedCurrency cur : currencies) {
				SupportedCurrencyOption opt = new SupportedCurrencyOption();

				// if this is the company default && no company default rate set
				// then mark as company default
				if (compDefCur != null) {
					if (!compDefRateSet && (compDefCur.getCurrencyId() == cur.getCurrencyId())) {
						opt.setCompanyDefault(true);
						opt.setSelected(true);
					}
				}
				opt.setCurrency(cur);
				opt.setRate(cur.getDefaultRate());
				opt.setSystemDefault(cur.getCurrencyId() == defaultCurrency.getCurrencyId());
				opt.setOptionValue(cur.getCurrencyCode());
				opt.setDefaultCurrencySymbol(defaultCurrency.getCurrencySymbol());

				// if no company default currency then make the default currency
				// the
				// selected currency
				if (comp.getCurrency() == null) {
					if (cur.getCurrencyId() == defaultCurrency.getCurrencyId()) {
						opt.setSelected(true);
					}
				}

				options.add(opt);
			}
		}
		return options;
	}

	@Override
	public SupportedCurrency getDefaultCurrency() {
		return findCurrencyByCode(defaultCurrencyCode);
	}

	@Override
	public KeyValueIntegerString getKeyValue(Integer currencyId) {
		return this.supportedCurrencyDao.getKeyValue(currencyId);
	}

	@Override
	public List<KeyValueIntegerString> getKeyValueList(Collection<Integer> currencyIds) {
		List<KeyValueIntegerString> result = Collections.emptyList();
		if (!currencyIds.isEmpty()) {
			result = this.supportedCurrencyDao.getKeyValueList(currencyIds);
		}
		return result;
	}

	/**
	 * Returns a List of key-value pairs for all supported currency options,
	 * intended for UI selection. Formatting uses "symbol - ISO code - name"
	 * e.g. "$ - USD - US Dollar"
	 */
	@Override
	public List<KeyValueIntegerString> getKeyValueListAll() {
		List<KeyValueIntegerString> result = new ArrayList<>();
		List<SupportedCurrency> options = getAllSupportedCurrencys();
		for (SupportedCurrency currency : options) {
			String value = currency.getCurrencySymbol() +
					" - " +
					currency.getCurrencyCode() +
					" - " +
					currency.getCurrencyName();
			result.add(new KeyValueIntegerString(currency.getCurrencyId(), value));
		}
		return result;
	}

	public void insertSupportedCurrency(SupportedCurrency supportedcurrency) {
		this.supportedCurrencyDao.insertSupportedCurrency(supportedcurrency);
	}

	public void setCompanyServ(CompanyService companyServ) {
		this.companyServ = companyServ;
	}

	@Override
	public void setCurrencyFromForm(MultiCurrencyAware obj, String currencyCode, Company company) {
		// if the currency code is blank, this indicates to not perform an
		// update (no rate change)
		if (!currencyCode.isEmpty()) {
			// check if the company default currency has been selected
			if (currencyCode.equals(COMPANY_DEFAULT_CURRENCY)) {
				// use the company defaults to set currency and rate
				obj.setCurrency(company.getCurrency());
				obj.setRate(company.getRate());
			} else {
				// otherwise use the select option
				obj.setCurrency(this.findSupportedCurrencyOrDefault(currencyCode));
				obj.setRate(obj.getCurrency().getDefaultRate());
			}
		}
	}

	public void updateDefaultRate(SupportedCurrency currency, double exRate) {
		Contact prevRateSetBy = currency.getRateLastSetBy();
		Date prevRateSetOn = currency.getRateLastSet();
		BigDecimal prevRate = currency.getDefaultRate();

		// all ok then save the currency and return to the wrapper
		currency.setRateSet(true);
		currency.setDefaultRate(new BigDecimal(exRate));
		currency.setRateLastSet(new Date());
		currency.setRateLastSetBy(this.sessionServ.getCurrentContact());
		/*
		 * System.out.println("MY TEST " +
		 * this.sessionServ.getCurrentContact());
		 */

		// add a new history item for the currency
		SupportedCurrencyRateHistory historyItem = new SupportedCurrencyRateHistory();
		historyItem.setCurrency(currency);
		historyItem.setDateTo(new Date());
		historyItem.setSetBy(prevRateSetBy);
		historyItem.setDateFrom(prevRateSetOn);
		historyItem.setRate(prevRate);
		if (currency.getRateHistory() == null) {
			currency.setRateHistory(
					new TreeSet<SupportedCurrencyRateHistory>(new SupportedCurrencyRateHistoryComparator()));
		}
		currency.getRateHistory().add(historyItem);

		// persist the updates
		this.updateSupportedCurrency(currency);
	}

	public void updateSupportedCurrency(SupportedCurrency supportedcurrency) {
		this.supportedCurrencyDao.updateSupportedCurrency(supportedcurrency);
	}

	@Override
	public SupportedCurrencyAjaxDTO saveNewValues(BigDecimal exchangeRate, Integer currencyId,
			Contact contact/* , Set<SupportedCurrencyRateHistory> rateHistory */) {

		SupportedCurrency sc = this.supportedCurrencyDao.find(currencyId);

		Contact prevRateSetBy = sc.getRateLastSetBy();
		Date prevRateSetOn = sc.getRateLastSet();
		BigDecimal prevRate = sc.getDefaultRate();

		sc.setDefaultRate(exchangeRate);
		sc.setRateLastSetBy(contact);
		sc.setRateLastSet(new Date());

		SupportedCurrencyRateHistory historyItem = new SupportedCurrencyRateHistory();
		historyItem.setCurrency(sc);
		historyItem.setDateTo(new Date());
		historyItem.setSetBy(prevRateSetBy);
		historyItem.setDateFrom(prevRateSetOn);
		historyItem.setRate(prevRate);
		if (sc.getRateHistory() == null) {
			sc.setRateHistory(new TreeSet<SupportedCurrencyRateHistory>(new SupportedCurrencyRateHistoryComparator()));
		}
		sc.getRateHistory().add(historyItem);

		supportedCurrencyDao.merge(sc);

		Locale locale = LocaleContextHolder.getLocale();
		DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, locale);
		String date = dateFormat.format(sc.getRateLastSet());

		SupportedCurrencyAjaxDTO scaDTO = new SupportedCurrencyAjaxDTO(date, contact.getName(), historyItem);
		return scaDTO;

	}
}