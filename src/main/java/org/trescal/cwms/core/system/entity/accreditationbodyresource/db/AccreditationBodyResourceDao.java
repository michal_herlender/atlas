package org.trescal.cwms.core.system.entity.accreditationbodyresource.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.accreditationbody.AccreditationBody;
import org.trescal.cwms.core.system.entity.accreditationbodyresource.AccreditationBodyResource;
import org.trescal.cwms.core.system.entity.accreditationbodyresource.AccreditationBodyResourceType;

public interface AccreditationBodyResourceDao extends BaseDao<AccreditationBodyResource, Integer> {
	AccreditationBodyResource getForBodyAndType(AccreditationBody body, AccreditationBodyResourceType type);
}
