package org.trescal.cwms.core.system.entity.note.db;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.GenericPricingEntity;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.note.ParentEntity;
import org.trescal.cwms.core.system.entity.note.dto.NoteForInstrumentTooltipDto;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;

public interface NoteService {
    /**
     * this method changes the active status of a note.
     *
     * @param noteid the id of the note to be updated.
     * @param active the active state to be applied.
     * @param personid the id of the user updating the note.
     * @param noteType the type of note to be updated.
	 */
	Note changeNoteStatus(Integer noteid, boolean active, Integer personid, NoteType noteType, HttpSession session);

	/**
	 * Get a {@link Note} that is identified by the given id and match the given
	 * classtype.
	 * 
	 * @param id the id of a {@link Note}.
	 * @param clazz {@link Class} of the {@link Note} to return, not null.
	 * @return {@link Note}
	 */
	Note findNote(int id, Class<? extends Note> clazz);

	/**
	 * Get a list of all {@link Note}(s) that are identified by the given id(s)
	 * and match the given classtype.
	 * 
	 * @param noteids list of {@link Note} ids, not null.
	 * @param clazz {@link Class} of the {@link Note} to return, not null.
	 * @return list of {@link Note}(s).
	 */
	List<Note> findNotes(List<Integer> noteids, Class<?> clazz);
	
	Note getNewNote(NoteType noteType, Integer entityId);

	/**
	 * Inserts a {@link Note} into the database using values provided from user.
	 * 
	 * @param label the label text of the {@link Note} to be inserted.
	 * @param note the main body text of the {@link Note} to be inserted.
	 * @param publish indicates if the note should be published.
	 * @param personId the id of the {@link Contact} inserting the note.
	 * @param noteType the type of {@link Note} to be inserted.
	 * @param noteTypeId the id of the entity we are attaching the note to.
	 * @param savecomment if true function will also attempt to save note as a
	 *        {@link PresetComment}.
	 * @param categoryid the category to save the comment to, null indicates
	 *        uncategorised.
	 * @return {@link ResultWrapper} containing {@link Note}
	 */
	ResultWrapper insertNoteType(String label, String note, boolean publish, Integer personId, NoteType noteType, Integer noteTypeId, boolean savecomment, Integer categoryid);

	/**
	 * this method updates the published status of a {@link Note}
	 * 
	 * @param noteid the id of the note to be updated.
	 * @param publish indicates whether to publish the note (true) or make it
	 *        private (false)
	 * @param personid the id of the user updating the note.
	 * @param noteType the type of note being updated.
	 * @return {@link Note}
	 */
	Note publish(Integer noteid, boolean publish, Integer personid, NoteType noteType, HttpSession session);

	void saveOrUpdate(Note note);

	/**
	 * Checks for existence of a note with identical content already registered against a 
	 * particular entity.
	 * 
	 * @param noteType noteType the type of {@link Note} to be checked.
	 * @param noteTypeId the id of the entity to check against.
     * @param noteContent the note content to check for.
     * @return Boolean
     */

    Boolean isNoteExists(NoteType noteType, Integer noteTypeId, String noteContent);

    List<NoteForInstrumentTooltipDto> findNotesForInstrumentTooltipByPlantId(Integer plantId);


    void createHistoryForQuotationItem(NoteType noteType, Integer entityId, String username, String noteLabel, String note);

    <E extends GenericPricingEntity<Company>, NE extends Note & ParentEntity<E>> Set<NE> initalizeNotes(E entity, Class<NE> noteClass, Locale locale);
}