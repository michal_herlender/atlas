package org.trescal.cwms.core.system.entity.componentsubdirectory.db;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.componentsubdirectory.ComponentSubDirectory;

@Repository("ComponentSubDirectoryDao")
public class ComponentSubDirectoryDaoImpl extends BaseDaoImpl<ComponentSubDirectory, Integer> implements ComponentSubDirectoryDao {
	
	@Override
	protected Class<ComponentSubDirectory> getEntity() {
		return ComponentSubDirectory.class;
	}
	
	@Override
	public ComponentSubDirectory findComponentSubDirectory(int componentId, String name) {
		DetachedCriteria crit = DetachedCriteria.forClass(ComponentSubDirectory.class);
		crit.add(Restrictions.eq("dirName", name));
		DetachedCriteria comCrit = crit.createCriteria("component");
		comCrit.add(Restrictions.idEq(componentId));
		@SuppressWarnings("unchecked")
		List<ComponentSubDirectory> list = crit.getExecutableCriteria(getSession()).list();
		return list.size() > 0 ? list.get(0) : null;
	}
}