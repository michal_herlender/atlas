package org.trescal.cwms.core.system.entity.emailcontent.core;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;

/**
 * Extracted from ContactServiceImpl.sendPasswordReminder(...) so we can preview/test content
 * TODO : Once templates done, we can remove the velocity part 
 * @author galen
 *
 */
@Component
public class EmailContent_ContactReminder {
	@Autowired
	private BusinessDetailsService bdServ;
	@Autowired
	private MessageSource messages; 
	@Autowired
	private TemplateEngine templateEngine;

	@Value("${cwms.deployed.url}")
	private String deployedURL;
	
	public static final String THYMELEAF_VIEW = "/email/core/contactreminder.html";
	
	public EmailContentDto getContent(Contact userContact, String newPassword, Locale locale) {
		// get business details (from the primary business contact of customer)
		Contact businessContact = userContact.getSub().getComp().getDefaultBusinessContact();
		Subdiv businessSubdiv = businessContact.getSub() != null ? businessContact.getSub() : null;  
		BusinessDetails bdetails = this.bdServ.getAllBusinessDetails(businessSubdiv);
		String body = getBodyThymeleaf(userContact, newPassword, bdetails, locale);
		String subject = getSubject(bdetails, locale);
		return new EmailContentDto(body, subject);
	}
	
	private String getBodyThymeleaf(Contact userContact, String newPassword, BusinessDetails bdetails, Locale locale) {
        Context context = new Context(locale);
		context.setVariable("contact", userContact);
		context.setVariable(Constants.SESSION_ATTRIBUTE_BUSINESS_DETAILS, bdetails);
		context.setVariable("deployedURL", this.deployedURL);
		context.setVariable("newPassword", newPassword);
		
		String body = this.templateEngine.process(THYMELEAF_VIEW, context);
		return body;
	}
	
	private String getSubject(BusinessDetails bdetails, Locale locale) {
		return this.messages.getMessage("email.contactreminder.subject",
				new Object[] { bdetails.getDocCompany() },
				bdetails.getDocCompany() + " Client Website: Login Details", 
				locale);
	}
}
