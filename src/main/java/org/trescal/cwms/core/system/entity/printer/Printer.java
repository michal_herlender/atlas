package org.trescal.cwms.core.system.entity.printer;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.printertray.PrinterTray;

/**
 * @author jamiev
 */
@Entity
@Table(name = "printer")
public class Printer extends AbstractPrinter
{
	private Set<PrinterTray> trays;
	private Set<Address> usedByAddresses;
	private Set<Contact> usedByContacts;

	@OneToMany(mappedBy = "printer", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@OrderBy("trayNo")
	public Set<PrinterTray> getTrays()
	{
		return this.trays;
	}

	@OneToMany(mappedBy = "defaultPrinter", fetch = FetchType.LAZY)
	public Set<Address> getUsedByAddresses()
	{
		return this.usedByAddresses;
	}

	@OneToMany(mappedBy = "defaultPrinter", fetch = FetchType.LAZY)
	public Set<Contact> getUsedByContacts()
	{
		return this.usedByContacts;
	}

	public void setTrays(Set<PrinterTray> trays)
	{
		this.trays = trays;
	}

	public void setUsedByAddresses(Set<Address> usedByAddresses)
	{
		this.usedByAddresses = usedByAddresses;
	}

	public void setUsedByContacts(Set<Contact> usedByContacts)
	{
		this.usedByContacts = usedByContacts;
	}
}
