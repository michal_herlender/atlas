package org.trescal.cwms.core.system.entity.instruction;

import java.util.Comparator;

/**
 * Instruction Comparator. Ordering hierachy is to show newest first.
 * 
 * @author Stuart
 */
public class InstructionComparator implements Comparator<Instruction>
{
	public int compare(Instruction i1, Instruction i2)
	{
		if ((i1.getInstructionid() == 0) && (i2.getInstructionid() == 0))
			return 1;
		else if ((i1.getLastModified() != null)
				&& (i2.getLastModified() != null)
				&& (i1.getLastModified().compareTo(i2.getLastModified()) != 0))
		{
			return i2.getLastModified().compareTo(i1.getLastModified());
		}
		else
		{
			if ((i1.getInstructionid() == 0) && (i2.getInstructionid() == 0))
			{
				return 1;
			}
			else
			{
				return ((Integer) i1.getInstructionid()).compareTo(i2.getInstructionid());
			}
		}
	}
}