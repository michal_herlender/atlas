package org.trescal.cwms.core.system.entity.accreditedlabcaltype.db;

import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.core.system.entity.accreditedlabcaltype.AccreditedLabCalType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

public interface AccreditedLabCalTypeService
{
	AccreditedLabCalType findAccreditedLabCalType(AccreditedLab accreditedLab, CalibrationType calType);

	String getLabelTemplate(AccreditedLab accreditedLab, CalibrationType calType);
}
