package org.trescal.cwms.core.system.entity.emailtemplate.db;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.emailtemplate.EmailTemplate;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Service
public class EmailTemplateServiceImpl extends
		BaseServiceImpl<EmailTemplate, Integer> implements 
		EmailTemplateService {

	@Autowired
	private EmailTemplateDao emailTemplateDao;
	
	@Override
	protected BaseDao<EmailTemplate, Integer> getBaseDao() {
		return this.emailTemplateDao;
	}
	
	@Override
	public List<EmailTemplate> getAllByCompany(Company company) {
		return this.emailTemplateDao.getAllByCompany(company);
	}
	
	@Override
	public EmailTemplate get(Company company, Locale locale, Component component, Subdiv subdiv, EmailTemplate exclude) {
		return this.emailTemplateDao.get(company, locale, component, subdiv, exclude);
	}
	
	@Override
	public EmailTemplate findBestTemplate(Subdiv businessSubdiv, Locale locale, Component component) {
		// Prefer subdiv specific template if available
		EmailTemplate result = this.emailTemplateDao.get(businessSubdiv.getComp(), locale, component, businessSubdiv, null);  
		if (result == null) {
			// Fallback to company-wide template if available
			result = this.emailTemplateDao.get(businessSubdiv.getComp(), locale, component, null, null);
		}
		return result;
	}
}
