package org.trescal.cwms.core.system.entity.emailcontent.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.jobs.job.dto.ClientFeedbackRequestDTO;
import org.trescal.cwms.core.jobs.job.dto.ItemFeedbackRequestDTO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

/**
 * Extracted from JobServiceImpl.generateClientFeedbackRequest(...) so we can preview/test content
 * TODO : Once templates done, we can remove the velocity part 
 * @author galen
 *
 */
@Component
public class EmailContent_ContractReviewFeedback {
	@Autowired
	private BusinessDetailsService bdServ;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private MessageSource messages; 
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private TemplateEngine templateEngine;
	
	public static final String THYMELEAF_VIEW = "/email/core/con_rev_feedback.html";

	public EmailContentDto getContent(Job job, List<ClientFeedbackRequestDTO> cfritems, Locale locale) {
		BusinessDetails bdetails = this.bdServ.getAllBusinessDetails(job.getOrganisation());

		// get locale of the contact of the job
		Locale contactLocale = job.getCon().getLocale() != null ? job.getCon().getLocale() : Locale.getDefault();
		// new item feedback request dto list
		List<ItemFeedbackRequestDTO> ifritems = new ArrayList<ItemFeedbackRequestDTO>();
		// create item feedback request dto's
		for (ClientFeedbackRequestDTO cfrdto : cfritems)
		{
			ItemFeedbackRequestDTO ifrdto = new ItemFeedbackRequestDTO();
			JobItem jobItem = this.jiServ.findJobItem(cfrdto.getJobItemId());
			ifrdto.setJi(jobItem);
			ifrdto.setCfrDTO(cfrdto);
			ifrdto.setModelName(InstModelTools.modelNameViaTranslations(jobItem.getInst().getModel(), contactLocale, supportedLocaleService.getPrimaryLocale()));
			// add to list
			ifritems.add(ifrdto);
		}
		
		String body = getBodyThymeleaf(job, ifritems, bdetails, locale);
		String subject = getSubject(job, bdetails, locale);
		EmailContentDto result = new EmailContentDto(body, subject);
		return result;
	}

	private String getBodyThymeleaf(Job job, List<ItemFeedbackRequestDTO> ifritems, BusinessDetails bdetails, Locale locale) {
        Context context = new Context(locale);
		context.setVariable("job", job);
		context.setVariable("ifritems", ifritems);
		context.setVariable(Constants.SESSION_ATTRIBUTE_BUSINESS_DETAILS, bdetails);
		
		String body = this.templateEngine.process(THYMELEAF_VIEW, context);
		return body;
	}

	private String getSubject(Job job, BusinessDetails bdetails, Locale locale) {
		return this.messages.getMessage("email.con_rev_feedback.subject",  new Object[] {bdetails.getDocCompany(), job.getJobno()}, 
				bdetails.getDocCompany() + " Information Request For Job No " + job.getJobno(), locale);
	}
}
