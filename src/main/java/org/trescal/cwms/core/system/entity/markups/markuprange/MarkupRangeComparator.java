/**
 * 
 */
package org.trescal.cwms.core.system.entity.markups.markuprange;

import java.util.Comparator;

/**
 * Custom {@link Comparator} implementation for {@link MarkupRange}. Sorts
 * {@link MarkupRange} by the value of the rangestart field, lowest first.
 * 
 * @author Richard
 */
public class MarkupRangeComparator implements Comparator<MarkupRange>
{
	@Override
	public int compare(MarkupRange o1, MarkupRange o2)
	{
		return ((Double) o1.getRangeStart()).compareTo(o2.getRangeStart());
	}
}
