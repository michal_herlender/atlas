package org.trescal.cwms.core.system.entity.supporteddoctype;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.system.entity.componentdoctype.ComponentDoctype;

@Entity
@Table (name="supporteddoctype")
public class SupportedDoctype 
{
	private Integer id;
	private String name;
	private String extension;
	
	private String imagePath;
	private File image;
	
	private Set<ComponentDoctype> componentDoctypes = new HashSet<ComponentDoctype>(0);
	
	public SupportedDoctype(){}

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column (name="doctypeId", nullable=false, unique = true)
	@Type (type = "int")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Transient
	public File getImage() {
		return image;
	}

	public void setImage(File image) {
		this.image = image;
	}

	@Column (name="name", nullable=false, unique=true, length=50)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column (name="imagepath", nullable=true, length=200, unique=true)
	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) 
	{
		this.imagePath = imagePath;
		if(this.imagePath != null && !this.imagePath.equals(""))
		{
			this.image = new File(this.imagePath);
		}
	}
	
	@Column (name="extension", nullable=false, length=10, unique=true)
	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "doctype")
	public Set<ComponentDoctype> getComponentDoctypes() {
		return componentDoctypes;
	}

	public void setComponentDoctypes(Set<ComponentDoctype> componentDoctypes) {
		this.componentDoctypes = componentDoctypes;
	}
}
