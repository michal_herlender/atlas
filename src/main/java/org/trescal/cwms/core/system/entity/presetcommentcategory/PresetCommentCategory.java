package org.trescal.cwms.core.system.entity.presetcommentcategory;

import java.util.Date;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "presetcommentcategory")
public class PresetCommentCategory
{
	private int id;
	private String category;
	private Date setOn;
	private Contact setBy;
	private Set<PresetComment> comments;
	private Set<Translation> translations;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}
	
	@NotNull
	@NotEmpty
	@Column(name = "category", nullable = false, length = 50, unique = true)
	public String getCategory() {
		return category;
	}
	
	@OneToMany(mappedBy = "category")
	public Set<PresetComment> getComments() {
		return comments;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setbyid")
	public Contact getSetBy() {
		return setBy;
	}
	
	@NotNull
	@Column(name = "seton")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSetOn() {
		return setOn;
	}
	
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="presetcommentcategorytranslation", joinColumns=@JoinColumn(name="id"))
	public Set<Translation> getTranslations() {
		return translations;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	public void setComments(Set<PresetComment> comments) {
		this.comments = comments;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setSetBy(Contact setBy) {
		this.setBy = setBy;
	}
	
	public void setSetOn(Date setOn) {
		this.setOn = setOn;
	}
	
	public void setTranslations(Set<Translation> translations) {
		this.translations = translations;
	}
}