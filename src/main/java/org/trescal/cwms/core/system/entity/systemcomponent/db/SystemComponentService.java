package org.trescal.cwms.core.system.entity.systemcomponent.db;

import java.util.List;

import org.trescal.cwms.core.exception.EntityNotFoundException;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;

public interface SystemComponentService
{
	/**
	 * Return's the {@link SystemComponent} entity matching the given
	 * {@link Component} type.
	 * 
	 * @param component the {@link Component}.
	 * @return {@link SystemComponent}, null if not found.
	 */
	SystemComponent findComponent(Component component);

	/**
	 * Find the {@link SystemComponent} identified by the given id.
	 * 
	 * @param componentId the id of the {@link SystemComponent} entity.
	 * @return the {@link SystemComponent}.
	 */
	SystemComponent findComponent(int componentId);

	/**
	 * Find the {@link SystemComponent} identified by the given name.
	 * 
	 * @param componentName the {@link SystemComponent} name.
	 * @return the {@link SystemComponent}.
	 * @deprecated #see findComponent(Component component)
	 */
	@Deprecated
	SystemComponent findComponent(String componentName);

	/**
	 * Returns the entity that matches the {@link Component} type and the given
	 * entity id. Null if none found.
	 * 
	 * @param component the {@link Component} type.
	 * @param id the entity id, not null.
	 * @return the entity or null.
	 */
	Object findComponentEntity(Component component, int id) throws EntityNotFoundException;

	/**
	 * Return's the {@link SystemComponent} entity matching the given
	 * {@link Component} type.
	 * 
	 * @param component the {@link Component}.
	 * @return {@link SystemComponent}, null if not found.
	 */
	SystemComponent findEagerComponent(Component component);

	/**
	 * Returns the entity of the given {@link Class} type with the matching id
	 * or null if not found.
	 * 
	 * @param clazz the {@link Class}.
	 * @param id the entity id.
	 * @return the entity or null if not found.
	 */
	<Entity> Entity findEntity(Class<Entity> clazz, int id);
	
	/**
	 * Returns a List of Components (not SystemComponents) that use email templates 
	 */
	List<Component> getComponentsUsingEmailTemplates();
}
