package org.trescal.cwms.core.system.entity.deletedcomponent.db;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.system.entity.deletedcomponent.DeletedComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

public interface DeletedComponentService
{
	void deleteDeletedComponent(DeletedComponent deletedcomponent);

	DeletedComponent findDeletedComponent(int id);

	List<DeletedComponent> getAllDeletedComponents();

	List<Component> getDeletableComponents();

	void insertDeletedComponent(DeletedComponent deletedcomponent);

	void saveOrUpdateDeletedComponent(DeletedComponent deletedcomponent);

	List<DeletedComponent> searchDeletedComponents(Component component, String refNo, Date dateFrom);

	void updateDeletedComponent(DeletedComponent deletedcomponent);
}