package org.trescal.cwms.core.system.entity.instruction.db;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink_;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.Contract_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.JobInstructionLink;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.JobInstructionLink_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.system.dto.InstructionDTO;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instruction.Instruction_;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionEntity;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

/**
 * In all public Dao methods, we first check that a query is necessary, both to
 * prevent an unnecessary query and to prevent SQL exception of no items in an
 * IN clause.
 */
@Repository
public class InstructionDaoImpl extends BaseDaoImpl<Instruction, Integer> implements InstructionDao {

	@Override
	protected Class<Instruction> getEntity() {
		return Instruction.class;
	}

	private List<InstructionDTO> findAllLinkedToCompanyByItemRestriction(
			Function<CriteriaBuilder, Function<Path<JobItem>, Expression<Boolean>>> itemRestriction,
			InstructionType... instructionTypes) {
		return getResultList(cb -> {
			CriteriaQuery<InstructionDTO> cq = cb.createQuery(InstructionDTO.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			Join<Job, Subdiv> allocatedSubdiv = job.join("organisation");
			Join<Subdiv, Company> allocatedCompany = allocatedSubdiv.join(Subdiv_.comp);
			Join<Job, Contact> contact = job.join(Job_.con);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Join<Company, CompanyInstructionLink> companyInstructionLink = company.join(Company_.instructions);
			companyInstructionLink.on(
					cb.and(cb.equal(companyInstructionLink.get(CompanyInstructionLink_.organisation), allocatedCompany),
							cb.or(cb.isFalse(companyInstructionLink.get(CompanyInstructionLink_.subdivInstruction)),
									cb.equal(companyInstructionLink.get(CompanyInstructionLink_.businessSubdiv),
											job.get(Job_.organisation)))));
			Join<CompanyInstructionLink, Instruction> instruction = companyInstructionLink
					.join(CompanyInstructionLink_.instruction);
			Join<CompanyInstructionLink, Subdiv> businessSubdiv = companyInstructionLink
					.join(CompanyInstructionLink_.businessSubdiv, JoinType.LEFT);
			Join<Instruction, Contact> lastModifiedBy = instruction.join(Instruction_.lastModifiedBy);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(itemRestriction.apply(cb).apply(jobItem));
			clauses.getExpressions()
					.add(instruction.get(Instruction_.instructiontype).in(Arrays.asList(instructionTypes)));
			cq.where(clauses);
			cq.distinct(true);
			cq.select(cb.construct(InstructionDTO.class, instruction.get(Instruction_.instructionid),
					instruction.get(Instruction_.instruction), instruction.get(Instruction_.instructiontype),
					instruction.get(Instruction_.lastModified), lastModifiedBy.get(Contact_.firstName),lastModifiedBy.get(Contact_.lastName),
					cb.literal(InstructionEntity.COMPANY.name()), company.get(Company_.coname), 
					companyInstructionLink.get(CompanyInstructionLink_.subdivInstruction),
					businessSubdiv.get(Subdiv_.subname), instruction.get(Instruction_.includeOnDelNote),
					instruction.get(Instruction_.includeOnSupplierDelNote)));
			return cq;
		});
	}

	private List<InstructionDTO> findAllLinkedToCompanyByJobRestriction(
			Function<CriteriaBuilder, Function<Path<Job>, Expression<Boolean>>> jobRestriction,
			InstructionType... instructionTypes) {
		return getResultList(cb -> {
			CriteriaQuery<InstructionDTO> cq = cb.createQuery(InstructionDTO.class);
			Root<Job> job = cq.from(Job.class);
			Join<Job, Subdiv> allocatedSubdiv = job.join("organisation");
			Join<Subdiv, Company> allocatedCompany = allocatedSubdiv.join(Subdiv_.comp);
			Join<Job, Contact> contact = job.join(Job_.con);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
			Join<Company, CompanyInstructionLink> companyInstructionLink = company.join(Company_.instructions);
			companyInstructionLink.on(
					cb.and(cb.equal(companyInstructionLink.get(CompanyInstructionLink_.organisation), allocatedCompany),
							cb.or(cb.isFalse(companyInstructionLink.get(CompanyInstructionLink_.subdivInstruction)),
									cb.equal(companyInstructionLink.get(CompanyInstructionLink_.businessSubdiv),
											job.get(Job_.organisation)))));
			Join<CompanyInstructionLink, Subdiv> businessSubdiv = companyInstructionLink
					.join(CompanyInstructionLink_.businessSubdiv, JoinType.LEFT);
			Join<CompanyInstructionLink, Instruction> instruction = companyInstructionLink
					.join(CompanyInstructionLink_.instruction);
			Join<Instruction, Contact> lastModifiedBy = instruction.join(Instruction_.lastModifiedBy);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(jobRestriction.apply(cb).apply(job));
			clauses.getExpressions()
					.add(instruction.get(Instruction_.instructiontype).in(Arrays.asList(instructionTypes)));
			cq.where(clauses);
			cq.distinct(true);
			cq.select(cb.construct(InstructionDTO.class, instruction.get(Instruction_.instructionid),
					instruction.get(Instruction_.instruction), instruction.get(Instruction_.instructiontype),
					instruction.get(Instruction_.lastModified), lastModifiedBy.get(Contact_.firstName),lastModifiedBy.get(Contact_.lastName),
					cb.literal(InstructionEntity.COMPANY.name()), company.get(Company_.coname), 
					companyInstructionLink.get(CompanyInstructionLink_.subdivInstruction),
					businessSubdiv.get(Subdiv_.subname), instruction.get(Instruction_.includeOnDelNote), 
					instruction.get(Instruction_.includeOnSupplierDelNote)));
			return cq;
		});
	}

	@Override
	public List<InstructionDTO> findAllLinkedToCompanyByItem(Integer jobItemId, InstructionType... instructionTypes) {
		List<InstructionDTO> result = null;
		if (instructionTypes.length > 0) {
			result = findAllLinkedToCompanyByItemRestriction(
					cb -> jobItem -> cb.equal(jobItem.get(JobItem_.jobItemId), jobItemId), instructionTypes);
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	@Override
	public List<InstructionDTO> findAllLinkedToCompanyByItems(Collection<Integer> jobItemIds,
			InstructionType... instructionTypes) {
		List<InstructionDTO> result = null;
		if (instructionTypes.length > 0 && !jobItemIds.isEmpty()) {
			result = findAllLinkedToCompanyByItemRestriction(cb -> jobItem -> jobItem.in(jobItemIds), instructionTypes);
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	@Override
	public List<InstructionDTO> findAllLinkedToCompanyByJobs(Collection<Integer> jobIds,
			InstructionType... instructionTypes) {
		List<InstructionDTO> result = null;
		if (instructionTypes != null && instructionTypes.length > 0 && !jobIds.isEmpty()) {
			result = findAllLinkedToCompanyByJobRestriction(cb -> job -> job.in(jobIds), instructionTypes);
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	private List<InstructionDTO> findAllLinkedToContactByItemRestriction(
			Function<CriteriaBuilder, Function<Path<JobItem>, Expression<Boolean>>> itemRestriction,
			InstructionType... instructionTypes) {
		return getResultList(cb -> {
			CriteriaQuery<InstructionDTO> cq = cb.createQuery(InstructionDTO.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			Join<Job, Subdiv> allocatedSubdiv = job.join("organisation");
			Join<Subdiv, Company> allocatedCompany = allocatedSubdiv.join(Subdiv_.comp);
			Join<Job, Contact> contact = job.join(Job_.con);
			Join<Contact, ContactInstructionLink> contactInstructionLink = contact.join(Contact_.instructions);
			contactInstructionLink
					.on(cb.equal(contactInstructionLink.get(ContactInstructionLink_.organisation), allocatedCompany));
			Join<ContactInstructionLink, Instruction> instruction = contactInstructionLink
					.join(ContactInstructionLink_.instruction);
			Join<Instruction, Contact> lastModifiedBy = instruction.join(Instruction_.lastModifiedBy);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(itemRestriction.apply(cb).apply(jobItem));
			clauses.getExpressions()
					.add(instruction.get(Instruction_.instructiontype).in(Arrays.asList(instructionTypes)));
			cq.where(clauses);
			cq.distinct(true);
			cq.select(cb.construct(InstructionDTO.class, instruction.get(Instruction_.instructionid),
					instruction.get(Instruction_.instruction), instruction.get(Instruction_.instructiontype),
					instruction.get(Instruction_.lastModified), lastModifiedBy.get(Contact_.firstName),lastModifiedBy.get(Contact_.lastName),
					cb.literal(InstructionEntity.CONTACT.name()), cb.concat(cb.concat(contact.get(Contact_.firstName),
					cb.literal(" ")), contact.get(Contact_.lastName)), cb.literal(false), cb.literal(""),
					instruction.get(Instruction_.includeOnDelNote), instruction.get(Instruction_.includeOnSupplierDelNote)));
			return cq;
		});
	}

	private List<InstructionDTO> findAllLinkedToContactByJobRestriction(
			Function<CriteriaBuilder, Function<Path<Job>, Expression<Boolean>>> jobRestriction,
			InstructionType... instructionTypes) {
		return getResultList(cb -> {
			CriteriaQuery<InstructionDTO> cq = cb.createQuery(InstructionDTO.class);
			Root<Job> job = cq.from(Job.class);
			Join<Job, Subdiv> allocatedSubdiv = job.join("organisation");
			Join<Subdiv, Company> allocatedCompany = allocatedSubdiv.join(Subdiv_.comp);
			Join<Job, Contact> contact = job.join(Job_.con);
			Join<Contact, ContactInstructionLink> contactInstructionLink = contact.join(Contact_.instructions);
			contactInstructionLink
					.on(cb.equal(contactInstructionLink.get(ContactInstructionLink_.organisation), allocatedCompany));
			Join<ContactInstructionLink, Instruction> instruction = contactInstructionLink
					.join(ContactInstructionLink_.instruction);
			Join<Instruction, Contact> lastModifiedBy = instruction.join(Instruction_.lastModifiedBy);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(jobRestriction.apply(cb).apply(job));
			clauses.getExpressions()
					.add(instruction.get(Instruction_.instructiontype).in(Arrays.asList(instructionTypes)));
			cq.where(clauses);
			cq.distinct(true);
			cq.select(cb.construct(InstructionDTO.class, instruction.get(Instruction_.instructionid),
					instruction.get(Instruction_.instruction), instruction.get(Instruction_.instructiontype),
					instruction.get(Instruction_.lastModified), lastModifiedBy.get(Contact_.firstName),lastModifiedBy.get(Contact_.lastName),
					cb.literal(InstructionEntity.CONTACT.name()), cb.concat(cb.concat(contact.get(Contact_.firstName),
					cb.literal(" ")), contact.get(Contact_.lastName)), cb.literal(false), cb.literal(""),
					instruction.get(Instruction_.includeOnDelNote), instruction.get(Instruction_.includeOnSupplierDelNote)));
			return cq;
		});
	}

	@Override
	public List<InstructionDTO> findAllLinkedToContactByItem(Integer jobItemId, InstructionType... instructionTypes) {
		List<InstructionDTO> result = null;
		if (instructionTypes != null && instructionTypes.length > 0) {
			result = findAllLinkedToContactByItemRestriction(
					cb -> jobItem -> cb.equal(jobItem.get(JobItem_.jobItemId), jobItemId), instructionTypes);
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	@Override
	public List<InstructionDTO> findAllLinkedToContactByItems(Collection<Integer> jobItemIds,
			InstructionType... instructionTypes) {
		List<InstructionDTO> result = null;
		if (instructionTypes != null && instructionTypes.length > 0 && !jobItemIds.isEmpty()) {
			result = findAllLinkedToContactByItemRestriction(cb -> jobItem -> jobItem.in(jobItemIds), instructionTypes);
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	@Override
	public List<InstructionDTO> findAllLinkedToContactByJobs(Collection<Integer> jobIds,
			InstructionType... instructionTypes) {
		List<InstructionDTO> result = null;
		if (instructionTypes != null && instructionTypes.length > 0 && !jobIds.isEmpty()) {
			result = findAllLinkedToContactByJobRestriction(cb -> job -> job.in(jobIds), instructionTypes);
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	private List<InstructionDTO> findAllLinkedToContractByItemRestriction(
			Function<CriteriaBuilder, Function<Path<JobItem>, Expression<Boolean>>> itemRestriction,
			InstructionType... instructionTypes) {
		return getResultList(cb -> {
			CriteriaQuery<InstructionDTO> cq = cb.createQuery(InstructionDTO.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Contract> contract = jobItem.join(JobItem_.contract);
			Join<Contract, Instruction> instruction = contract.join(Contract_.instructions);
			Join<Instruction, Contact> lastModifiedBy = instruction.join(Instruction_.lastModifiedBy);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(itemRestriction.apply(cb).apply(jobItem));
			clauses.getExpressions()
					.add(instruction.get(Instruction_.instructiontype).in(Arrays.asList(instructionTypes)));
			cq.where(clauses);
			cq.distinct(true);
			cq.select(cb.construct(InstructionDTO.class, instruction.get(Instruction_.instructionid),
					instruction.get(Instruction_.instruction), instruction.get(Instruction_.instructiontype),
					instruction.get(Instruction_.lastModified), lastModifiedBy.get(Contact_.firstName),lastModifiedBy.get(Contact_.lastName),
				    cb.literal(InstructionEntity.CONTRACT.name()), contract.get(Contract_.contractNumber), cb.literal(false), cb.literal(""),
					instruction.get(Instruction_.includeOnDelNote), instruction.get(Instruction_.includeOnSupplierDelNote)));
			return cq;
		});
	}

	@Override
	public List<InstructionDTO> findAllLinkedToContractByItem(Integer jobItemId, InstructionType... instructionTypes) {
		List<InstructionDTO> result = null;
		if (instructionTypes != null && instructionTypes.length > 0) {
			result = findAllLinkedToContractByItemRestriction(
					cb -> jobItem -> cb.equal(jobItem.get(JobItem_.jobItemId), jobItemId), instructionTypes);
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	@Override
	public List<InstructionDTO> findAllLinkedToContractByItems(Collection<Integer> jobItemIds,
			InstructionType... instructionTypes) {
		List<InstructionDTO> result = null;
		if (instructionTypes != null && instructionTypes.length > 0 && !jobItemIds.isEmpty()) {
			result = findAllLinkedToContractByItemRestriction(cb -> jobItem -> jobItem.in(jobItemIds),
					instructionTypes);
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	@Override
	public List<InstructionDTO> findAllLinkedToJob(Integer jobId, InstructionType... instructionTypes) {
		List<InstructionDTO> result = null;
		if (instructionTypes != null && instructionTypes.length > 0) {
			result = getResultList(cb -> {
				CriteriaQuery<InstructionDTO> cq = cb.createQuery(InstructionDTO.class);
				Root<Job> job = cq.from(Job.class);
				Join<Job, JobInstructionLink> jobInstructionLink = job.join(Job_.insts);
				Join<JobInstructionLink, Instruction> instruction = jobInstructionLink
						.join(JobInstructionLink_.instruction);
				Join<Instruction, Contact> lastModifiedBy = instruction.join(Instruction_.lastModifiedBy);
				Predicate clauses = cb.conjunction();
				clauses.getExpressions().add(cb.equal(job.get(Job_.jobid), jobId));
				clauses.getExpressions()
						.add(instruction.get(Instruction_.instructiontype).in(Arrays.asList(instructionTypes)));
				cq.where(clauses);
				cq.distinct(true);
				cq.select(cb.construct(InstructionDTO.class, instruction.get(Instruction_.instructionid),
						instruction.get(Instruction_.instruction), instruction.get(Instruction_.instructiontype),
						instruction.get(Instruction_.lastModified), lastModifiedBy.get(Contact_.firstName),lastModifiedBy.get(Contact_.lastName),
						cb.literal(InstructionEntity.JOB.name()), job.get(Job_.jobno), cb.literal(false), cb.literal(""), 
						instruction.get(Instruction_.includeOnDelNote), instruction.get(Instruction_.includeOnSupplierDelNote)));
				return cq;
			});
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	private List<InstructionDTO> findAllLinkedToJobByItemRestriction(
			Function<CriteriaBuilder, Function<Path<JobItem>, Expression<Boolean>>> itemRestriction,
			InstructionType... instructionTypes) {
		List<InstructionDTO> result = null;
		if (instructionTypes != null && instructionTypes.length > 0) {
			result = getResultList(cb -> {
				CriteriaQuery<InstructionDTO> cq = cb.createQuery(InstructionDTO.class);
				Root<JobItem> jobItem = cq.from(JobItem.class);
				Join<JobItem, Job> job = jobItem.join(JobItem_.job);
				Join<Job, JobInstructionLink> jobInstructionLink = job.join(Job_.insts);
				Join<JobInstructionLink, Instruction> instruction = jobInstructionLink
						.join(JobInstructionLink_.instruction);
				Join<Instruction, Contact> lastModifiedBy = instruction.join(Instruction_.lastModifiedBy);
				Predicate clauses = cb.conjunction();
				clauses.getExpressions().add(itemRestriction.apply(cb).apply(jobItem));
				clauses.getExpressions()
						.add(instruction.get(Instruction_.instructiontype).in(Arrays.asList(instructionTypes)));
				cq.where(clauses);
				cq.distinct(true);
				cq.select(cb.construct(InstructionDTO.class, instruction.get(Instruction_.instructionid),
						instruction.get(Instruction_.instruction), instruction.get(Instruction_.instructiontype),
						instruction.get(Instruction_.lastModified), lastModifiedBy.get(Contact_.firstName),lastModifiedBy.get(Contact_.lastName),
						cb.literal(InstructionEntity.JOB.name()), job.get(Job_.jobno), cb.literal(false), cb.literal(""),
						instruction.get(Instruction_.includeOnDelNote), instruction.get(Instruction_.includeOnSupplierDelNote)));
				return cq;
			});
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	@Override
	public List<InstructionDTO> findAllLinkedToJobByItem(Integer jobItemId, InstructionType... instructionTypes) {
		List<InstructionDTO> result = null;
		if (instructionTypes != null && instructionTypes.length > 0) {
			result = findAllLinkedToJobByItemRestriction(
					cb -> jobItem -> cb.equal(jobItem.get(JobItem_.jobItemId), jobItemId), instructionTypes);
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	@Override
	public List<InstructionDTO> findAllLinkedToJobByItems(Collection<Integer> jobItemIds,
			InstructionType... instructionTypes) {
		List<InstructionDTO> result = null;
		if (instructionTypes != null && instructionTypes.length > 0 && !jobItemIds.isEmpty()) {
			result = findAllLinkedToJobByItemRestriction(cb -> jobItem -> jobItem.in(jobItemIds), instructionTypes);
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	private List<InstructionDTO> findAllLinkedToSubdivByItemRestriction(
			Function<CriteriaBuilder, Function<Path<JobItem>, Expression<Boolean>>> itemRestriction,
			InstructionType... instructionTypes) {
		return getResultList(cb -> {
			CriteriaQuery<InstructionDTO> cq = cb.createQuery(InstructionDTO.class);
			Root<JobItem> jobItem = cq.from(JobItem.class);
			Join<JobItem, Job> job = jobItem.join(JobItem_.job);
			Join<Job, Subdiv> allocatedSubdiv = job.join("organisation");
			Join<Subdiv, Company> allocatedCompany = allocatedSubdiv.join(Subdiv_.comp);
			Join<Job, Contact> contact = job.join(Job_.con);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			Join<Subdiv, SubdivInstructionLink> subdivInstructionLink = subdiv.join(Subdiv_.instructions);
			subdivInstructionLink.on(
					cb.and(cb.equal(subdivInstructionLink.get(SubdivInstructionLink_.organisation), allocatedCompany),
							cb.or(cb.isFalse(subdivInstructionLink.get(SubdivInstructionLink_.subdivInstruction)),
									cb.equal(subdivInstructionLink.get(SubdivInstructionLink_.businessSubdiv),
											job.get(Job_.organisation)))));
			Join<SubdivInstructionLink, Instruction> instruction = subdivInstructionLink
					.join(SubdivInstructionLink_.instruction);
			Join<SubdivInstructionLink, Subdiv> businessSubdiv = subdivInstructionLink
					.join(SubdivInstructionLink_.businessSubdiv, JoinType.LEFT);
			Join<Instruction, Contact> lastModifiedBy = instruction.join(Instruction_.lastModifiedBy);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(itemRestriction.apply(cb).apply(jobItem));
			clauses.getExpressions()
					.add(instruction.get(Instruction_.instructiontype).in(Arrays.asList(instructionTypes)));
			cq.where(clauses);
			cq.distinct(true);
			cq.select(cb.construct(InstructionDTO.class, instruction.get(Instruction_.instructionid),
					instruction.get(Instruction_.instruction), instruction.get(Instruction_.instructiontype),
					instruction.get(Instruction_.lastModified), lastModifiedBy.get(Contact_.firstName),lastModifiedBy.get(Contact_.lastName),
					cb.literal(InstructionEntity.SUBDIV.name()), subdiv.get(Subdiv_.subname), 
					subdivInstructionLink.get(SubdivInstructionLink_.subdivInstruction),
					businessSubdiv.get(Subdiv_.subname), instruction.get(Instruction_.includeOnDelNote),
					instruction.get(Instruction_.includeOnSupplierDelNote)));
			return cq;
		});
	}

	private List<InstructionDTO> findAllLinkedToSubdivByJobRestriction(
			Function<CriteriaBuilder, Function<Path<Job>, Expression<Boolean>>> jobRestriction,
			InstructionType... instructionTypes) {
		return getResultList(cb -> {
			CriteriaQuery<InstructionDTO> cq = cb.createQuery(InstructionDTO.class);
			Root<Job> job = cq.from(Job.class);
			Join<Job, Subdiv> allocatedSubdiv = job.join("organisation");
			Join<Subdiv, Company> allocatedCompany = allocatedSubdiv.join(Subdiv_.comp);
			Join<Job, Contact> contact = job.join(Job_.con);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			Join<Subdiv, SubdivInstructionLink> subdivInstructionLink = subdiv.join(Subdiv_.instructions);
			subdivInstructionLink.on(
					cb.and(cb.equal(subdivInstructionLink.get(SubdivInstructionLink_.organisation), allocatedCompany),
							cb.or(cb.isFalse(subdivInstructionLink.get(SubdivInstructionLink_.subdivInstruction)),
									cb.equal(subdivInstructionLink.get(SubdivInstructionLink_.businessSubdiv),
											job.get(Job_.organisation)))));
			Join<SubdivInstructionLink, Instruction> instruction = subdivInstructionLink
					.join(SubdivInstructionLink_.instruction);
			Join<SubdivInstructionLink, Subdiv> businessSubdiv = subdivInstructionLink
					.join(SubdivInstructionLink_.businessSubdiv, JoinType.LEFT);
			Join<Instruction, Contact> lastModifiedBy = instruction.join(Instruction_.lastModifiedBy);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(jobRestriction.apply(cb).apply(job));
			clauses.getExpressions()
					.add(instruction.get(Instruction_.instructiontype).in(Arrays.asList(instructionTypes)));
			cq.where(clauses);
			cq.distinct(true);
			cq.select(cb.construct(InstructionDTO.class, instruction.get(Instruction_.instructionid),
					instruction.get(Instruction_.instruction), instruction.get(Instruction_.instructiontype),
					instruction.get(Instruction_.lastModified), lastModifiedBy.get(Contact_.firstName),lastModifiedBy.get(Contact_.lastName),
					cb.literal(InstructionEntity.SUBDIV.name()), subdiv.get(Subdiv_.subname), 
					subdivInstructionLink.get(SubdivInstructionLink_.subdivInstruction),
					businessSubdiv.get(Subdiv_.subname), instruction.get(Instruction_.includeOnDelNote),
					instruction.get(Instruction_.includeOnSupplierDelNote)));
			return cq;
		});
	}

	@Override
	public List<InstructionDTO> findAllLinkedToSubdivByItem(Integer jobItemId, InstructionType... instructionTypes) {
		List<InstructionDTO> result = null;
		if (instructionTypes != null && instructionTypes.length > 0) {
			result = findAllLinkedToSubdivByItemRestriction(
					cb -> jobItem -> cb.equal(jobItem.get(JobItem_.jobItemId), jobItemId), instructionTypes);
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	@Override
	public List<InstructionDTO> findAllLinkedToSubdivByItems(Collection<Integer> jobItemIds,
			InstructionType... instructionTypes) {
		List<InstructionDTO> result = null;
		if (instructionTypes != null && instructionTypes.length > 0 && !jobItemIds.isEmpty()) {
			result = findAllLinkedToSubdivByItemRestriction(cb -> jobItem -> jobItem.in(jobItemIds), instructionTypes);
		} else {
			result = Collections.emptyList();
		}
		return result;
	}

	@Override
	public List<InstructionDTO> findAllLinkedToSubdivByJobs(Collection<Integer> jobIds,
			InstructionType... instructionTypes) {
		List<InstructionDTO> result = null;
		if (instructionTypes != null && instructionTypes.length > 0 && !jobIds.isEmpty()) {
			result = findAllLinkedToSubdivByJobRestriction(cb -> job -> job.in(jobIds), instructionTypes);
		} else {
			result = Collections.emptyList();
		}
		return result;
	}
}