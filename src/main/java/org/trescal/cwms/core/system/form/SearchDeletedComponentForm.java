package org.trescal.cwms.core.system.form;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.system.entity.deletedcomponent.DeletedComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

public class SearchDeletedComponentForm
{
	private Component component;
	private List<DeletedComponent> components;
	private List<Component> deletableComponents;
	private Date from;
	private String refNo;

	public Component getComponent()
	{
		return this.component;
	}

	public List<DeletedComponent> getComponents()
	{
		return this.components;
	}

	public List<Component> getDeletableComponents()
	{
		return this.deletableComponents;
	}

	public Date getFrom()
	{
		return this.from;
	}

	public String getRefNo()
	{
		return this.refNo;
	}

	public void setComponent(Component component)
	{
		this.component = component;
	}

	public void setComponents(List<DeletedComponent> components)
	{
		this.components = components;
	}

	public void setDeletableComponents(List<Component> deletableComponents)
	{
		this.deletableComponents = deletableComponents;
	}

	public void setFrom(Date from)
	{
		this.from = from;
	}

	public void setRefNo(String refNo)
	{
		this.refNo = refNo;
	}
}
