package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class ObsoleteCertificateFunctionality extends SystemDefaultBooleanType {


    @Override
    public SystemDefaultNames defaultName() { return SystemDefaultNames.OBSOLETE_CERTIFICATES; }
}
