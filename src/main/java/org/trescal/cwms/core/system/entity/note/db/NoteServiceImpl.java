package org.trescal.cwms.core.system.entity.note.db;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.asset.entity.asset.Asset;
import org.trescal.cwms.core.asset.entity.asset.db.AssetService;
import org.trescal.cwms.core.asset.entity.assetnote.AssetNote;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.deliverynote.entity.delivery.Delivery;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.DeliveryItem;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.db.DeliveryItemService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitemnote.DeliveryItemNote;
import org.trescal.cwms.core.deliverynote.entity.deliverynote.DeliveryNote;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.hire.entity.hire.db.HireService;
import org.trescal.cwms.core.hire.entity.hirenote.HireNote;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentnote.InstrumentNote;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.modelnote.ModelNote;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.certificate.entity.certnote.CertNote;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobnote.JobNote;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemnote.JobItemNote;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.db.CreditNoteService;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnotenote.CreditNoteNote;
import org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.GenericPricingEntity;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.db.InvoiceService;
import org.trescal.cwms.core.pricing.invoice.entity.invoicenote.InvoiceNote;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.db.JobCostingService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.db.JobCostingItemService;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitemremark.JobCostingItemNote;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingnote.JobCostingNote;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.db.PurchaseOrderItemService;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitemnote.PurchaseOrderItemNote;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseordernote.PurchaseOrderNote;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteitemnote.QuoteItemNote;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.schedule.entity.schedule.Schedule;
import org.trescal.cwms.core.schedule.entity.schedule.db.ScheduleService;
import org.trescal.cwms.core.schedule.entity.schedulenote.ScheduleNote;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.defaultnote.db.DefaultNoteService;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.note.ParentEntity;
import org.trescal.cwms.core.system.entity.note.dto.NoteForInstrumentTooltipDto;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.presetcomment.db.PresetCommentService;
import org.trescal.cwms.core.system.entity.presetcommentcategory.db.PresetCommentCategoryService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.form.genericentityvalidator.NoteValidator;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.db.TPQuotationItemService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote.TPQuoteItemNote;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotenote.TPQuoteNote;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.db.TPQuoteRequestItemService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitemnote.TPQuoteRequestItemNote;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote.TPQuoteRequestNote;

@Service("NoteService")
public class NoteServiceImpl implements NoteService {
	@Autowired
	private AssetService assetService;
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private ContactService conServ;
	@Autowired
	private CreditNoteService creditNoteService;
	@Autowired
	private DeliveryService deliveryService;
	@Autowired
	private DeliveryItemService deliveryItemService;
	@Autowired
	private HireService hireService;
	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private JobCostingItemService jobCostingItemService;
	@Autowired
	private JobCostingService jobCostingService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private JobService jobService;
	@Autowired
	private NoteDao noteDao;
	@Autowired
	private PresetCommentCategoryService presetCommentCatServ;
	@Autowired
	private PresetCommentService presetCommentServ;
	@Autowired
	private PurchaseOrderItemService purchaseOrderItemService;
	@Autowired
	private PurchaseOrderService purchaseOrderService;
	@Autowired
	private QuotationItemService quotationItemService;
	@Autowired
	private QuotationService quotationService;
	@Autowired
	private ScheduleService scheduleService;
	@Autowired
	private TPQuotationItemService tpQuotationItemService;
	@Autowired
    private TPQuotationService tpQuotationService;
    @Autowired
    private TPQuoteRequestItemService tpQuoteRequestItemService;
    @Autowired
    private TPQuoteRequestService tpQuoteRequestService;
    @Autowired
    private UserService userService;
    @Autowired
    private NoteValidator validator;

    @Autowired
    private DefaultNoteService defaultNoteService;

    @Override
    public Note changeNoteStatus(Integer noteid, boolean active, Integer personid, NoteType noteType,
                                 HttpSession session) {
        Note note = this.findNote(noteid, noteType.getNoteclazz());
        Date date = new Date();
        note.setDeactivatedOn(date);
        note.setActive(active);
        Contact con = personid == 0
                ? userService.get((String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME)).getCon()
				: this.conServ.get(personid);
		note.setSetBy(con);
		if (!active) {
			note.setDeactivatedBy(con);
			note.setDeactivatedOn(date);
		} else {
			note.setSetBy(con);
			note.setSetOn(date);
		}

		this.noteDao.saveOrUpdate(note);
		return note;
	}

	@Override
	public Note findNote(int id, Class<? extends Note> clazz) {
		return this.noteDao.findNote(id, clazz);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.trescal.cwms.core.system.entity.note.db.NoteService#findNotes(java
	 * .util.List, java.lang.Class)
	 */
	@Override
	public List<Note> findNotes(List<Integer> noteids, Class<?> clazz) {
		return this.noteDao.findNotes(noteids, clazz);
	}

	@Override
	public Note getNewNote(NoteType noteType, Integer entityId) {
		switch (noteType) {
		case ASSETNOTE:
			AssetNote assetNote = new AssetNote();
			assetNote.setAsset(assetService.findAsset(entityId));
			return assetNote;
		case CERTNOTE:
			CertNote certificateNote = new CertNote();
			certificateNote.setCert(certificateService.findCertificate(entityId));
			return certificateNote;
		case CREDITNOTENOTE:
			CreditNoteNote crediteNoteNote = new CreditNoteNote();
			crediteNoteNote.setCreditNote(creditNoteService.get(entityId));
			return crediteNoteNote;
		case DELIVERYITEMNOTE:
			DeliveryItemNote deliveryItemNote = new DeliveryItemNote();
			deliveryItemNote.setDeliveryitem(deliveryItemService.findDeliveryItem(entityId));
			return deliveryItemNote;
		case DELIVERYNOTE:
			DeliveryNote deliveryNote = new DeliveryNote();
			deliveryNote.setDelivery(deliveryService.get(entityId));
			return deliveryNote;
		case HIRENOTE:
			HireNote hireNote = new HireNote();
			hireNote.setHire(hireService.get(entityId));
			return hireNote;
		case INSTRUMENTMODELNOTE:
			ModelNote modelNote = new ModelNote();
			modelNote.setModel(instrumentModelService.findInstrumentModel(entityId));
			return modelNote;
		case INSTRUMENTNOTE:
			InstrumentNote instrumentNote = new InstrumentNote();
			instrumentNote.setInstrument(instrumentService.get(entityId));
			return instrumentNote;
		case INVOICENOTE:
			InvoiceNote invoiceNote = new InvoiceNote();
			invoiceNote.setInvoice(invoiceService.findInvoice(entityId));
			return invoiceNote;
		case JOBCOSTINGITEMNOTE:
			JobCostingItemNote jobCostingItemNote = new JobCostingItemNote();
			jobCostingItemNote.setJobCostingItem(jobCostingItemService.get(entityId));
			return jobCostingItemNote;
		case JOBCOSTINGNOTE:
			JobCostingNote jobCostingNote = new JobCostingNote();
			jobCostingNote.setJobcosting(jobCostingService.get(entityId));
			return jobCostingNote;
		case JOBITEMNOTE:
			JobItemNote jobItemNote = new JobItemNote();
			jobItemNote.setJi(jobItemService.findJobItem(entityId));
			return jobItemNote;
		case JOBNOTE:
			JobNote jobNote = new JobNote();
			jobNote.setJob(jobService.get(entityId));
			return jobNote;
		case PURCHASEORDERITEMNOTE:
			PurchaseOrderItemNote purchaseOrderItemNote = new PurchaseOrderItemNote();
			purchaseOrderItemNote.setItem(purchaseOrderItemService.findPurchaseOrderItem(entityId));
			return purchaseOrderItemNote;
		case PURCHASEORDERNOTE:
			PurchaseOrderNote purchaseOrderNote = new PurchaseOrderNote();
			purchaseOrderNote.setOrder(purchaseOrderService.find(entityId));
			return purchaseOrderNote;
		case QUOTEITEMNOTE:
			QuoteItemNote quoteItemNote = new QuoteItemNote();
			quoteItemNote.setQuotationitem(quotationItemService.findQuotationItem(entityId));
			return quoteItemNote;
		case QUOTENOTE:
			QuoteNote quoteNote = new QuoteNote();
			quoteNote.setQuotation(quotationService.get(entityId));
			return quoteNote;
		case SCHEDULENOTE:
			ScheduleNote scheduleNote = new ScheduleNote();
			scheduleNote.setSchedule(scheduleService.get(entityId));
			return scheduleNote;
		case TPQUOTEITEMNOTE:
			TPQuoteItemNote tpQuoteItemNote = new TPQuoteItemNote();
			tpQuoteItemNote.setTpquotationitem(tpQuotationItemService.get(entityId));
			return tpQuoteItemNote;
		case TPQUOTENOTE:
			TPQuoteNote tpQuoteNote = new TPQuoteNote();
			tpQuoteNote.setTpquotation(tpQuotationService.get(entityId));
			return tpQuoteNote;
		case TPQUOTEREQUESTITEMNOTE:
			TPQuoteRequestItemNote tpQuoteRequestItemNote = new TPQuoteRequestItemNote();
			tpQuoteRequestItemNote.setTpQuoteRequestItem(tpQuoteRequestItemService.findTPQuoteRequestItem(entityId));
			return tpQuoteRequestItemNote;
		case TPQUOTEREQUESTNOTE:
			TPQuoteRequestNote tpQuoteRequestNote = new TPQuoteRequestNote();
			tpQuoteRequestNote.setTpQuoteRequest(tpQuoteRequestService.get(entityId));
			return tpQuoteRequestNote;
		default:
			return null;
		}
	}

	@Override
	public ResultWrapper insertNoteType(String label, String note, boolean publish, Integer personId, NoteType noteType,
			Integer noteTypeId, boolean savecomment, Integer categoryid) {
		Note newnote = null;

		switch (noteType) {
		case ASSETNOTE:
			AssetNote anote = new AssetNote();
			anote.setAsset((Asset) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = anote;
			break;
		case CERTNOTE:
			CertNote cnote = new CertNote();
			cnote.setCert((Certificate) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = cnote;
			break;
		case DELIVERYNOTE:
			DeliveryNote dnote = new DeliveryNote();
			dnote.setDelivery((Delivery) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = dnote;
			break;
		case DELIVERYITEMNOTE:
			DeliveryItemNote dinote = new DeliveryItemNote();
			dinote.setDeliveryitem((DeliveryItem) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = dinote;
			break;
		case INSTRUMENTNOTE:
			InstrumentNote inote = new InstrumentNote();
			inote.setInstrument((Instrument) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = inote;
			break;
		case INSTRUMENTMODELNOTE:
			ModelNote mnote = new ModelNote();
			mnote.setModel((InstrumentModel) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = mnote;
			break;
		case INVOICENOTE:
			InvoiceNote invnote = new InvoiceNote();
			invnote.setInvoice((Invoice) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = invnote;
			break;
		case JOBCOSTINGNOTE:
			JobCostingNote jcnote = new JobCostingNote();
			jcnote.setJobcosting((JobCosting) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = jcnote;
			break;
		case JOBCOSTINGITEMNOTE:
			JobCostingItemNote jcinote = new JobCostingItemNote();
			jcinote.setJobCostingItem((JobCostingItem) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = jcinote;
			break;
		case JOBNOTE:
			JobNote jnote = new JobNote();
			jnote.setJob((Job) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = jnote;
			break;
		case JOBITEMNOTE:
			JobItemNote jinote = new JobItemNote();
			jinote.setJi((JobItem) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = jinote;
			break;
		case PURCHASEORDERNOTE:
			PurchaseOrderNote ponote = new PurchaseOrderNote();
			ponote.setOrder((PurchaseOrder) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = ponote;
			break;
		case PURCHASEORDERITEMNOTE:
			PurchaseOrderItemNote poinote = new PurchaseOrderItemNote();
			poinote.setItem((PurchaseOrderItem) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = poinote;
			break;
		case QUOTENOTE:
			QuoteNote qnote = new QuoteNote();
			qnote.setQuotation((Quotation) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = qnote;
			break;
		case QUOTEITEMNOTE:
			QuoteItemNote qinote = new QuoteItemNote();
			qinote.setQuotationitem((Quotationitem) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = qinote;
			break;
		case SCHEDULENOTE:
			ScheduleNote snote = new ScheduleNote();
			snote.setSchedule((Schedule) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = snote;
			break;
		case TPQUOTENOTE:
			TPQuoteNote tpqnote = new TPQuoteNote();
			tpqnote.setTpquotation((TPQuotation) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = tpqnote;
			break;
		case TPQUOTEITEMNOTE:
			TPQuoteItemNote tpqinote = new TPQuoteItemNote();
			tpqinote.setTpquotationitem(
					(TPQuotationItem) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = tpqinote;
			break;
		case TPQUOTEREQUESTNOTE:
			TPQuoteRequestNote tpqrnote = new TPQuoteRequestNote();
			tpqrnote.setTpQuoteRequest((TPQuoteRequest) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = tpqrnote;
			break;
		case TPQUOTEREQUESTITEMNOTE:
			TPQuoteRequestItemNote tpqrinote = new TPQuoteRequestItemNote();
			tpqrinote.setTpQuoteRequestItem(
					(TPQuoteRequestItem) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = tpqrinote;
			break;
		case CREDITNOTENOTE:
			CreditNoteNote cnnote = new CreditNoteNote();
			cnnote.setCreditNote((CreditNote) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = cnnote;
			break;
		case HIRENOTE:
			HireNote hirenote = new HireNote();
			hirenote.setHire((Hire) this.noteDao.findObject(noteTypeId, noteType.getEntityclazz()));
			newnote = hirenote;
			break;
		}
		newnote.setActive(true);
		newnote.setLabel(label);
		newnote.setNote(note);
		newnote.setPublish(publish);
		Contact contact = this.conServ.get(personId);
		newnote.setSetBy(contact);
		newnote.setSetOn(new Date());
		BindException errors = new BindException(newnote, "newnote");
		this.validator.validate(newnote, null);
		if (!errors.hasErrors()) {
			this.noteDao.insertNote(newnote);
			// user has opted to save the comment, go do it
			// TODO: decide, whether we'd like to create a new
			// PresetCommentCategory if newCategory is not null or empty
			if (savecomment && !newnote.getNoteType().getPresetCommentType().equals(PresetCommentType.WITHOUT)) {
				PresetComment comment = new PresetComment();
				comment.setType(newnote.getNoteType().getPresetCommentType());
				comment.setSetby(contact);
				comment.setSeton(new Date());
				comment.setComment(note);
				if (categoryid != null && categoryid != 0)
					comment.setCategory(presetCommentCatServ.get(categoryid));
				this.presetCommentServ.save(comment);
			}
		}
		return new ResultWrapper(errors, newnote);
	}

	@Override
	public Boolean isNoteExists(NoteType noteType, Integer noteTypeId, String noteContent) {
		return this.noteDao.noteExists(noteType.getNoteclazz(), noteType.getEntityclazz(), noteTypeId, noteContent);
	}

	@Override
	public List<NoteForInstrumentTooltipDto> findNotesForInstrumentTooltipByPlantId(Integer plantId) {
		return noteDao.findNotesForInstrumentTooltipByPlantId(plantId);
	}

	@Override
	public Note publish(Integer noteid, boolean publish, Integer personid, NoteType noteType, HttpSession session) {
		Note note = this.findNote(noteid, noteType.getNoteclazz());
		note.setPublish(publish);
		note.setSetOn(new Date());
		note.setSetBy(personid == 0
				? userService.get((String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME)).getCon()
				: this.conServ.get(personid));
		this.noteDao.saveOrUpdate(note);
		return note;
	}

	@Override
	public void saveOrUpdate(Note note) {
		this.noteDao.saveOrUpdate(note);
	}

	@Override
	public void createHistoryForQuotationItem(NoteType noteType, Integer entityId, String username, String noteLabel,
			String note) {
		
		
		Note newnote = getNewNote(noteType, entityId);
        newnote.setPublish(false);
        newnote.setSetOn(new Date());
        newnote.setSetBy(this.userService.get(username).getCon());
        newnote.setActive(true);
        newnote.setLabel(noteLabel);
        newnote.setNote(note);

        this.saveOrUpdate(newnote);
    }

    @Override
    public <E extends GenericPricingEntity<Company>, NE extends Note & ParentEntity<E>> Set<NE> initalizeNotes(E entity, Class<NE> noteClass, Locale locale) {
        return Component.componentForClazz(entity.getClass())
                .map(c -> defaultNoteService.createNotesFromDefaultNotesForComponent(c, noteClass,
                                entity.getOrganisation().getCoid(), locale, entity.getCreatedBy())
                        .peek(n -> n.setEntity(entity))
                        .collect(Collectors.toCollection(() -> new TreeSet<>(new NoteComparator())))
                ).orElseGet(() -> new TreeSet<>(new NoteComparator()));
    }

}