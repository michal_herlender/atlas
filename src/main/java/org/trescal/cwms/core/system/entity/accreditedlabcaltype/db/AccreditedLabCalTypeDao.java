package org.trescal.cwms.core.system.entity.accreditedlabcaltype.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.core.system.entity.accreditedlabcaltype.AccreditedLabCalType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

public interface AccreditedLabCalTypeDao extends BaseDao<AccreditedLabCalType, Integer> {
	
	AccreditedLabCalType findAccreditedLabCalType(AccreditedLab accreditedLab, CalibrationType calType);
}