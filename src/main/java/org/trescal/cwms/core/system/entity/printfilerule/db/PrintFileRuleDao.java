package org.trescal.cwms.core.system.entity.printfilerule.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.printfilerule.PrintFileRule;

public interface PrintFileRuleDao extends BaseDao<PrintFileRule, Integer> {}