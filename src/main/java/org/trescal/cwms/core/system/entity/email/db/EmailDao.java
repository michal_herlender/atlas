package org.trescal.cwms.core.system.entity.email.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.email.EmailDto;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.form.SearchEmailForm;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface EmailDao extends BaseDao<Email, Integer> {
	
	Email findEagerEmail(int id);
	
	List<Email> getAllComponentEmails(Component component, int id);
	
	void searchEmailsDto(SearchEmailForm form, PagedResultSet<EmailDto> prs);
}