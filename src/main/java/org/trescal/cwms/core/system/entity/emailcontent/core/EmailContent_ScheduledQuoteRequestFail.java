package org.trescal.cwms.core.system.entity.emailcontent.core;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequest;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;

@Component
public class EmailContent_ScheduledQuoteRequestFail {
	@Autowired
	private MessageSource messages; 
	@Autowired
	private TemplateEngine templateEngine;
	
	public static final String THYMELEAF_VIEW = "/email/core/scheduledquoterequestfail.html";

	public EmailContentDto getContent(ScheduledQuotationRequest request, Exception exception, Locale locale) {
		String body = getBodyThymeleaf(request, exception, locale);
		String subject = getSubject(locale);
		return new EmailContentDto(body, subject);
	}

	private String getBodyThymeleaf(ScheduledQuotationRequest request, Exception exception, Locale locale) {
        Context context = new Context(locale);
		context.setVariable("sqr", request);
		context.setVariable("exception", exception);
		context.setVariable("stacktrace", this.getStackTrace(exception));
		
		String body = this.templateEngine.process(THYMELEAF_VIEW, context);
		return body;
	}

	private String getSubject(Locale locale) {
		return this.messages.getMessage("email.schedulequoterequestsuccess.subjectfail", null, "Scheduled quotation request failed", locale);
	}

	protected String getStackTrace(Exception ex) {
		StackTraceElement[] stackTraceElement = ex.getStackTrace();
		StringBuffer stackTrace = new StringBuffer();
		for (StackTraceElement element : stackTraceElement) {
			stackTrace.append(element.toString());
			stackTrace.append("<br/>");
		}
		return stackTrace.toString();
	}
	
}
