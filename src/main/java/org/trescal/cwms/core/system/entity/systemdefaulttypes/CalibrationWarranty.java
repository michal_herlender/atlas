package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class CalibrationWarranty extends SystemDefaultDoubleType {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.CALIBRATION_WARRANTY;
	}

}
