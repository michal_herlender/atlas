package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.system.entity.labeltemplate.LabelTemplateType;

@Component
public class DefaultLabelTemplateType extends SystemDefaultType<String> {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.LABELTEMPLATE;
	}
	
	@Override
	public boolean isDiscreteType() {
		return true;
	}
	
	@Override
	public List<String> getValues() {
		List<String> result = new ArrayList<String>();
		for (LabelTemplateType item : LabelTemplateType.values()) {
			result.add("'" + item + "'");
		}
		return result;
	}
	
	@Override
	public String parseValue(String value) {
		return value;
	}

	@Override
	public boolean isMultiChoice() {
		return false;
	}
}
