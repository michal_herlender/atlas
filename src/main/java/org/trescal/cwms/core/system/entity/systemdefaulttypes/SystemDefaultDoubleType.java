package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.util.ArrayList;
import java.util.List;

public abstract class SystemDefaultDoubleType extends SystemDefaultType<Double> {
	
	@Override
	public boolean isDiscreteType() {
		return false;
	}
	
	@Override
	public boolean isMultiChoice() {
		return false;
	}
	
	@Override
	public List<String> getValues() {
		return new ArrayList<String>();
	}
	
	@Override
	public Double parseValue(String value) {
		return Double.parseDouble(value);
	}
}