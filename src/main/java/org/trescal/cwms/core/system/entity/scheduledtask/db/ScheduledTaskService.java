package org.trescal.cwms.core.system.entity.scheduledtask.db;

import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.system.entity.scheduledtask.ScheduledTask;

public interface ScheduledTaskService {
    void deleteScheduledTask(ScheduledTask scheduledtask);

    ScheduledTask findScheduledTask(int id);

    ScheduledTask findScheduledTaskByClassAndMethodNames(String className, String methodName);

    List<ScheduledTask> getAllScheduledTasks();

    void insertScheduledTask(ScheduledTask scheduledtask);

    /**
     * Method used to report the completion of a {@link ScheduledTask}, whether
     * it was a success and an optional message.
     *
     * @param className className the {@link ScheduledTask} className
     * @param ste       the {@link StackTraceElement} array taken at the time of
     *                  method execution
     * @param success   the success of the task run
     * @param message   an optional message with any relevant information of the
     *                  task run
     */
    void reportOnScheduledTaskRun(String className, StackTraceElement[] ste, boolean success, String message);

    void saveOrUpdateScheduledTask(ScheduledTask scheduledtask);

    /**
     * Returns true if a {@link ScheduledTask} can be found using the given
     * class name and a method name calculated from the stack trace, and that
     * {@link ScheduledTask} is currently active. Returns false if a matching
     * {@link ScheduledTask} cannot be found or if the task is found but is
     * currently inactive.
     *
     * @param className the {@link ScheduledTask} className
     * @param ste       the {@link StackTraceElement} array taken at the time of
     *                  method execution
     * @return true if the task should continue, false otherwise
     */
    boolean scheduledTaskIsActive(String className, StackTraceElement ste[]);

    boolean scheduledTaskIsActive(String className, String methodName);

    void switchScheduledTaskOnOrOff(int id, boolean active);

    /**
     * Manually triggers the quartz job with the given name after a given delay
     * from the time this method is called. NOTE: Do not use this method to
     * trigger scheduled tasks that you wish to run again as scheduled
     * afterwards. This method is only to be called for tasks that will be
     * triggered manually every time they are required to run.
     *
     * @param jobName        the job name in quartz
     * @param delayInSeconds the delay in seconds until job fires
     * @return a {@link ResultWrapper} indicating the the success of SCHEDULING
     * the task - note: not the success of the task itself
     */
    ResultWrapper triggerScheduledTaskAfterDelay(String jobName, int delayInSeconds);

    /**
     * Manually triggers the quartz job with the given name immediately.
     *
     * @param jobName the job name in quartz
     * @return a {@link ResultWrapper} indicating the the success of TRIGGERING
     * the task - note: not the success of the task itself
     */
    ResultWrapper triggerScheduledTaskNow(String jobName);

    void updateScheduledTask(ScheduledTask scheduledtask);
}