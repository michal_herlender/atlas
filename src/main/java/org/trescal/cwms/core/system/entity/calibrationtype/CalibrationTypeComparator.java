package org.trescal.cwms.core.system.entity.calibrationtype;

import java.util.Comparator;

public class CalibrationTypeComparator implements Comparator<CalibrationType>
{
	public int compare(CalibrationType c1, CalibrationType c2)
	{
		return c1.getOrderBy().compareTo(c2.getOrderBy());
	}
}