package org.trescal.cwms.core.system.entity.printer.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.printer.Printer;
import org.trescal.cwms.spring.model.KeyValue;

public interface PrinterService extends BaseService<Printer, Integer>
{
	void deletePrinterById(int id);

	Printer findPrinterByPath(String printerPath);

	Printer getDefaultPrinterForContact(int personid);

	List<Printer> getPrintersAtAddress(int addrId);
	
	List<KeyValue<Integer, String>> getDtosBySubdiv(Subdiv subdiv);
	
	/*
	 * Returns dto list of printers that the contact has access to via user role
	 */
	List<KeyValue<Integer, String>> getDtosForContactByUserRole(Contact contact);
}