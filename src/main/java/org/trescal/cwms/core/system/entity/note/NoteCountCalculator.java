package org.trescal.cwms.core.system.entity.note;

public class NoteCountCalculator {
	
	public static int getPrivateActiveNoteCount(NoteAwareEntity noteAwareEntity) {
		int result = 0;
		// Entities returned without eager fetching or lazy loading (e.g. DWR) may not have access to notes 
		if (noteAwareEntity.getNotes() != null) {
			result = (int) noteAwareEntity.getNotes().stream().filter(n -> n.isActive() && !n.getPublish()).count();
		}
		return result;
	}
	
	public static int getPrivateDeactivedNoteCount(NoteAwareEntity noteAwareEntity) {
		int result = 0;
		// Entities returned without eager fetching or lazy loading (e.g. DWR) may not have access to notes 
		if (noteAwareEntity.getNotes() != null) {
			result = (int) noteAwareEntity.getNotes().stream().filter(n -> !n.isActive() && !n.getPublish()).count();
		}
		return result;
	}
	
	public static int getPublicActiveNoteCount(NoteAwareEntity noteAwareEntity) {
		int result = 0;
		// Entities returned without eager fetching or lazy loading (e.g. DWR) may not have access to notes 
		if (noteAwareEntity.getNotes() != null) {
			result = (int) noteAwareEntity.getNotes().stream().filter(n -> n.isActive() && n.getPublish()).count();
		}
		return result;
	}
	
	public static int getPublicDeactivedNoteCount(NoteAwareEntity noteAwareEntity) {
		int result = 0;
		// Entities returned without eager fetching or lazy loading (e.g. DWR) may not have access to notes 
		if (noteAwareEntity.getNotes() != null) {
			result = (int) noteAwareEntity.getNotes().stream().filter(n -> !n.isActive() && n.getPublish()).count();
		}
		return result;
	}
}