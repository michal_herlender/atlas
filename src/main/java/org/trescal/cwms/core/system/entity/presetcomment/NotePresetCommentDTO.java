package org.trescal.cwms.core.system.entity.presetcomment;

import org.trescal.cwms.core.system.entity.presetcommentcategory.PresetCommentCategory;


/**
 * Wrapper class used for individual preset comments
 * 
 * Changes: Galen 2016-10-04 - Re-designed completely to properly carry abbreviated + HTML trimmed text
 * comment - the original comment as stored in database (with HTML markup)
 * textComment - the original comment but with all HTML tags removed out 
 * trimmedTextComment - a shortened version of textComment suitable for select boxes
 * category - the category (may be null if uncategorized)
 *
 * @author Richard
 */
public class NotePresetCommentDTO
{
	private String comment;
	private String textComment;
	private String trimmedTextComment;
	private PresetCommentCategory category;
	
	public PresetCommentCategory getCategory() {
		return category;
	}
	public String getComment() {
		return comment;
	}
	public String getTextComment() {
		return textComment;
	}
	public String getTrimmedTextComment() {
		return trimmedTextComment;
	}
	
	public void setCategory(PresetCommentCategory category) {
		this.category = category;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public void setTextComment(String textComment) {
		this.textComment = textComment;
	}
	public void setTrimmedTextComment(String trimmedTextComment) {
		this.trimmedTextComment = trimmedTextComment;
	}
}
