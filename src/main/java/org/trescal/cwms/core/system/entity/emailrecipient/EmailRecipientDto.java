package org.trescal.cwms.core.system.entity.emailrecipient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor @RequiredArgsConstructor
public class EmailRecipientDto {
	private Integer emailId;
	private final String emailAddress;
	private Integer id;
	private Integer recipientContactId;
	private String recipientContactFirstName;
	private String recipientContactLastName;
	private final RecipientType type;
	
}
