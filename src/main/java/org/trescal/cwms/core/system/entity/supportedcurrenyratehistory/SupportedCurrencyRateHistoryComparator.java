package org.trescal.cwms.core.system.entity.supportedcurrenyratehistory;

import java.util.Comparator;

import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrencyComparator;

/**
 * Sorts {@link SupportedCurrencyRateHistory} in descending order (with newest
 * first)
 * 
 * @author Richard
 */
public class SupportedCurrencyRateHistoryComparator implements Comparator<SupportedCurrencyRateHistory>
{
	@Override
	public int compare(SupportedCurrencyRateHistory o1, SupportedCurrencyRateHistory o2)
	{
		if (o1.getCurrency().getCurrencyId() == o2.getCurrency().getCurrencyId())
		{
			return o2.getDateTo().compareTo(o1.getDateTo());
		}
		else
		{
			return new SupportedCurrencyComparator().compare(o1.getCurrency(), o2.getCurrency());
		}

	}
}
