package org.trescal.cwms.core.system.entity.systemcomponent.db;

import java.util.List;

import org.trescal.cwms.core.exception.EntityNotFoundException;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;

public class SystemComponentServiceImpl implements SystemComponentService
{
	private SystemComponentDao scDao;

	@Override
	public SystemComponent findComponent(Component component)
	{
		return this.scDao.findComponent(component);
	}

	public SystemComponent findComponent(int componentId)
	{
		return this.scDao.find(componentId);
	}

	public SystemComponent findComponent(String componentName)
	{
		return this.scDao.findComponent(componentName);
	}

	@Override
	public Object findComponentEntity(Component component, int id) throws EntityNotFoundException
	{
		Object obj = this.findEntity(component.getClazz(), id);
		if (obj == null)
		{
			throw new EntityNotFoundException("The requested entity could not be found");
		}
		return obj;
	}

	@Override
	public SystemComponent findEagerComponent(Component component)
	{
		return this.scDao.findEagerComponent(component);
	}

	@Override
	public <Entity> Entity findEntity(Class<Entity> clazz, int id)
	{
		return this.scDao.findEntity(clazz, id);
	}
	
	@Override
	public List<Component> getComponentsUsingEmailTemplates() {
		return this.scDao.getComponentsUsingEmailTemplates();
	}

	public void setScDao(SystemComponentDao scDao)
	{
		this.scDao = scDao;
	}
}
