package org.trescal.cwms.core.system.entity.emailcontent.batch;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;

import org.springframework.batch.core.JobExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.batch.config.entities.backgroundtask.BackgroundTask;
import org.trescal.cwms.batch.controller.BgTasksController;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;

@Component
public class EmailContent_GenericBackgroundTaskCompletion {

	public static final String THYMELEAF_VIEW = "/email/batch/genericbackgroundtaskcompletion.html";

	@Autowired
	private MessageSource messages;
	@Autowired
	private TemplateEngine templateEngine;
	@Value("${cwms.deployed.url}")
	private URL deployedURL;

	public EmailContentDto getContent(Locale locale, BackgroundTask bgTask, JobExecution jobExecution)
			throws URISyntaxException, MalformedURLException {
		String body = getBodyThymeleaf(locale, bgTask, jobExecution);
		String subject = getSubject(locale, bgTask, jobExecution);
		return new EmailContentDto(body, subject);
	}

	private String getBodyThymeleaf(Locale locale, BackgroundTask bgTask, JobExecution jobExecution)
			throws URISyntaxException, MalformedURLException {
		Context context = new Context(locale);
		context.setVariable("bgTask", bgTask);
		context.setVariable("jobExecution", jobExecution);

		// construct details URL
		UriComponentsBuilder ub = UriComponentsBuilder.fromUri(deployedURL.toURI());
		ub.path(BgTasksController.GET_URL).queryParam("id", bgTask.getId());
		context.setVariable("detailsURL", ub.build().toUri().toURL());

		return this.templateEngine.process(THYMELEAF_VIEW, context);
	}

	private String getSubject(Locale locale, BackgroundTask bgTask, JobExecution jobExecution) {
		return this.messages.getMessage("email.genericbackgroundtaskcompletion.title",
				new Object[] { jobExecution.getJobInstance().getJobName(), bgTask.getId() }, locale);
	}

}
