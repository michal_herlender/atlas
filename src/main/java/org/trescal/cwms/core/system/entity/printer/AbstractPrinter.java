package org.trescal.cwms.core.system.entity.printer;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.location.Location;

/**
 * @author jamiev
 */
@MappedSuperclass
public abstract class AbstractPrinter
{
	private Address addr;
	private String description;
	private int id;
	private Location loc;
	private String path;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "addrid")
	public Address getAddr()
	{
		return this.addr;
	}

	@Column(name = "description", length = 200, nullable = true)
	@Length(max = 200)
	public String getDescription()
	{
		return this.description;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "locid")
	public Location getLoc()
	{
		return this.loc;
	}

	@NotNull
	@Column(name = "path", length = 100, nullable = false)
	@Length(min = 1, max = 100)
	public String getPath()
	{
		return this.path;
	}

	public void setAddr(Address addr)
	{
		this.addr = addr;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setLoc(Location loc)
	{
		this.loc = loc;
	}

	public void setPath(String path)
	{
		this.path = path;
	}
}