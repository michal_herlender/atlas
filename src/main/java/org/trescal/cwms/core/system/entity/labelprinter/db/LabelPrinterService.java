package org.trescal.cwms.core.system.entity.labelprinter.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.labelprinter.LabelPrinter;

public interface LabelPrinterService
{
	void deleteLabelPrinter(LabelPrinter labelLabelPrinter);

	void deleteLabelPrinterById(int id);

	LabelPrinter findLabelPrinter(int id);

	List<LabelPrinter> getAllLabelPrinters();

	LabelPrinter getDefaultLabelPrinterForContact(Contact con);

	LabelPrinter getDefaultLabelPrinterForCurrentContact();

	List<LabelPrinter> getLabelPrintersAtAddress(int addrId);

	void insertLabelPrinter(LabelPrinter labelLabelPrinter);

	void updateLabelPrinter(LabelPrinter labelLabelPrinter);
}