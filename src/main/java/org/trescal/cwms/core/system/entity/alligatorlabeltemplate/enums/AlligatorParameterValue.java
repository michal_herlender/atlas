package org.trescal.cwms.core.system.entity.alligatorlabeltemplate.enums;

/**
 * Defines different sources of values for use in a label template 
 *
 */
public enum AlligatorParameterValue {
	CERT_ACCREDITATION_NUMBER,
	CERT_CAL_DATE,
	CERT_NUMBER,
	CERT_NUMBER_LEFT_PART,
	CERT_NUMBER_RIGHT_PART,
	CERT_NUMBER_RIGHT_PART_TRIMMED,
	CERT_TECHNCIAN_NAME,
	INST_CLIENT_ID,
	INST_DUE_DATE,
	INST_SERIAL_NUMBER,
	INST_TRESCAL_ID,
	TEXT,
	TP_CERT_NUMBER;
}
