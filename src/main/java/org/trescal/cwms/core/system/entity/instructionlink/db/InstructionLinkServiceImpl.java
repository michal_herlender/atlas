package org.trescal.cwms.core.system.entity.instructionlink.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink;
import org.trescal.cwms.core.company.entity.companyinstructionlink.db.CompanyInstructionLinkService;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink;
import org.trescal.cwms.core.company.entity.contactinstructionlink.db.ContactInstructionLinkService;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.db.SubdivInstructionLinkService;
import org.trescal.cwms.core.contract.dtos.ContractInstructionDto;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.db.ContractService;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.JobInstructionLink;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.db.JobInstructionLinkService;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionEntity;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLink;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLinkDateComparator;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

@Service
public class InstructionLinkServiceImpl implements InstructionLinkService {

	@Autowired
	private CompanyInstructionLinkService companyInstructionLinkService;
	@Autowired
	private SubdivInstructionLinkService subdivInstructionLinkService;
	@Autowired
	private ContactInstructionLinkService contactInstructionLinkService;
	@Autowired
	private JobInstructionLinkService jobInstructionLinkService;

	@Autowired
	private ContractService contractService;

	@Override
	public InstructionLink<?> get(int id, InstructionEntity entity) {
		switch (entity) {
		case COMPANY: {
			return companyInstructionLinkService.get(id);
		}
		case SUBDIV: {
			return subdivInstructionLinkService.get(id);
		}
		case CONTACT: {
			return contactInstructionLinkService.get(id);
		}
		case JOB: {
			return jobInstructionLinkService.get(id);
		}
		default: {
			return null;
		}
		}
	}

	@Override
	public void merge(InstructionLink<?> link) {
		switch (link.getInstructionEntity()) {
		case COMPANY: {
			companyInstructionLinkService.merge((CompanyInstructionLink) link);
			break;
		}
		case SUBDIV: {
			subdivInstructionLinkService.merge((SubdivInstructionLink) link);
			break;
		}
		case CONTACT: {
			contactInstructionLinkService.merge((ContactInstructionLink) link);
			break;
		}
		case JOB: {
			jobInstructionLinkService.merge((JobInstructionLink) link);
			break;
		}
		default:
			break;
		}
	}

	public List<InstructionLink<?>> getAllForJob(Job job) {
		// We now include just the instructions for the owning subdiv of the job

		List<InstructionLink<?>> result = new ArrayList<InstructionLink<?>>();
		result.addAll(this.jobInstructionLinkService.getAllForJob(job));
		result.addAll(this.companyInstructionLinkService.getAllForCompany(job.getCon().getSub().getComp(),
				job.getOrganisation()));
		result.addAll(this.subdivInstructionLinkService.getAllForSubdiv(job.getCon().getSub(), job.getOrganisation()));
		result.addAll(
				this.contactInstructionLinkService.getAllForContact(job.getCon(), job.getOrganisation().getComp()));
		Collections.sort(result, new InstructionLinkDateComparator());
		return result;
	}

	@Override
	public Set<Instruction> getAllForJobitem(Contract contract) {
		Set<Instruction> result = new HashSet<>();
		result.addAll(contract.getInstructions());
		return result;
	}

	@Override
	public List<InstructionLink<?>> getAllForHire(Hire hire) {
		List<InstructionLink<?>> result = new ArrayList<InstructionLink<?>>();

		result.addAll(this.companyInstructionLinkService.getForCompanyAndTypes(hire.getContact().getSub().getComp(),
				hire.getOrganisation(), InstructionType.HIRE));
		result.addAll(this.subdivInstructionLinkService.getForSubdivAndTypes(hire.getContact().getSub(),
				hire.getOrganisation(), InstructionType.HIRE));
		result.addAll(this.contactInstructionLinkService.getForContactAndTypes(hire.getContact(),
				hire.getOrganisation().getComp(), InstructionType.HIRE));
		Collections.sort(result, new InstructionLinkDateComparator());
		return result;
	}

	@Override
	public void delete(InstructionLink<?> link) {
		switch (link.getInstructionEntity()) {
		case COMPANY: {
			companyInstructionLinkService.delete((CompanyInstructionLink) link);
			break;
		}
		case SUBDIV: {
			subdivInstructionLinkService.delete((SubdivInstructionLink) link);
			break;
		}
		case CONTACT: {
			contactInstructionLinkService.delete((ContactInstructionLink) link);
			break;
		}
		case JOB: {
			jobInstructionLinkService.delete((JobInstructionLink) link);
			break;
		}
		default:
			break;
		}
	}
}
