package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class DisplayBpoOnDoc extends SystemDefaultBooleanType {
	
	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.DISPLAY_BPO_ON_DOC;
	}
}