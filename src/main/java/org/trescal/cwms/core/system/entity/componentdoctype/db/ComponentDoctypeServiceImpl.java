package org.trescal.cwms.core.system.entity.componentdoctype.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.componentdoctype.ComponentDoctype;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

public class ComponentDoctypeServiceImpl implements ComponentDoctypeService
{
	ComponentDoctypeDao componentDoctypeDao;

	public ComponentDoctype findComponentDoctype(Component component, String doctypeName)
	{
		return this.componentDoctypeDao.findComponentDoctype(component, doctypeName);
	}

	public ComponentDoctype findComponentDoctype(int id)
	{
		return this.componentDoctypeDao.find(id);
	}

	public List<ComponentDoctype> getAllComponentDoctypes()
	{
		return this.componentDoctypeDao.findAll();
	}

	public ComponentDoctypeDao getComponentDoctypeDao()
	{
		return this.componentDoctypeDao;
	}

	public void insertComponentDoctype(ComponentDoctype componentdoctype)
	{
		this.componentDoctypeDao.persist(componentdoctype);
	}

	public void setComponentDoctypeDao(ComponentDoctypeDao componentdoctypeDao)
	{
		this.componentDoctypeDao = componentdoctypeDao;
	}

	public void updateComponentDoctype(ComponentDoctype componentdoctype)
	{
		this.componentDoctypeDao.update(componentdoctype);
	}
}