package org.trescal.cwms.core.system.entity.alligatorsettings;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.subdiv.Subdiv;

@Entity
@Table(name="alligatorsettingssubdiv", uniqueConstraints={@UniqueConstraint(columnNames="subdivid")})
public class AlligatorSettingsSubdiv extends AlligatorSettingsUsage {
	private Subdiv businessSubdiv;
	
	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="subdivid", nullable=false)
	public Subdiv getBusinessSubdiv() {
		return businessSubdiv;
	}
	
	public void setBusinessSubdiv(Subdiv businessSubdiv) {
		this.businessSubdiv = businessSubdiv;
	}
	
}
