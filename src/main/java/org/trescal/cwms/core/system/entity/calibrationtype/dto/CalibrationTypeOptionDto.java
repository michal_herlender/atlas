package org.trescal.cwms.core.system.entity.calibrationtype.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CalibrationTypeOptionDto {
    private Integer id;
    private String label;
}
