package org.trescal.cwms.core.system.entity.systemdefaultgroup.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.systemdefaultgroup.SystemDefaultGroup;
import org.trescal.cwms.core.system.enums.Scope;

@Repository("SystemDefaultGroupDao")
public class SystemDefaultGroupDaoImpl extends BaseDaoImpl<SystemDefaultGroup, Integer> implements SystemDefaultGroupDao {
	
	@Override
	protected Class<SystemDefaultGroup> getEntity() {
		return SystemDefaultGroup.class;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SystemDefaultGroup> getAllSystemDefaultByCorole(String corole, Scope scope) {
		Criteria crit = getSession().createCriteria(SystemDefaultGroup.class);
		Criteria sysDef = crit.createCriteria("defaults");
		Criteria sysRole = sysDef.createCriteria("roles");
		sysRole.createCriteria("corole").add(Restrictions.eq("corole", corole));
		sysDef.createCriteria("scopes").add(Restrictions.eq("scope", scope));
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return crit.list();
	}
}