package org.trescal.cwms.core.system.entity.supportedcurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.system.dto.SupportedCurrencyOption;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.spring.model.KeyValue;

@Component
public class SupportedCurrencyFormatter {
	
	@Autowired
	private MessageSource messages;
	@Autowired
	private SupportedCurrencyService currencyService; 
	
	public final static String CODE_COMPANY_DEFAULT_RATE = "viewjob.compdefrate";
	public final static String MESSAGE_COMPANY_DEFAULT_RATE = "Company Default Rate";
	
	public final static String CODE_CURRENT_RATE = "viewjob.currentrate";
	public final static String MESSAGE_CURRENT_RATE = "Current Rate";
	
	public final static String CODE_SYSTEM_DEFAULT_RATE = "viewjob.systdefrate";
	public final static String MESSAGE_SYSTEM_DEFAULT_RATE = "System Default Rate";
	
	/**
	 * Returns List of formatted currency options as KeyValue pairs
	 * delegating to getCompanyCurrencyOptions() for the generation of
	 * the list
	 * 
	 * Note, there is the possibility that an entity (e.g. Job has its rate/currency set), but 
	 * that the rate in the SupportedCurrency entry is different or has changed.  
	 * 
	 * For this reason, when a MultiCurrencyAware entity is provided with a currency and rate, 
	 * we prepend the list with an entry with an empty String key and a value describing the existing rate.
	 * 
	 * An empty String may then be used to indicate "no change" to the rate in UI binding. 
	 * 
	 * @param coid mandatory id of the company for which the 
	 * @param entity optional entity that may have an existing rate and currency set 
	 * @return list of @link KeyValue<Integer, String> options
	 */
	public List<KeyValue<String, String>> getCompanyCurrencyDTOs(Integer coid, MultiCurrencyAware entity, Locale locale) {
		List<KeyValue<String, String>> result = new ArrayList<>();
		List<SupportedCurrencyOption> options = this.currencyService.getCompanyCurrencyOptions(coid);
		
		if (entity != null && entity.getCurrency() != null && entity.getRate() != null) {
			String defaultCurrencySymbol = this.currencyService.getDefaultCurrency().getCurrencySymbol();
			
			StringBuffer value = new StringBuffer();
			value.append(entity.getCurrency().getCurrencyCode());
			value.append(" @ ");
			value.append(defaultCurrencySymbol);
			value.append("1 = ");
			value.append(entity.getCurrency().getCurrencySymbol());
			value.append(entity.getRate());
			value.append(" [");
			value.append(messages.getMessage(CODE_CURRENT_RATE, null, MESSAGE_CURRENT_RATE, locale));
			value.append("]");
			result.add(new KeyValue<>("", value.toString()));
		}
		for (SupportedCurrencyOption curopt : options) {
			StringBuffer value = new StringBuffer();
			value.append(curopt.getCurrency().getCurrencyCode());
			value.append(" @ ");
			value.append(curopt.getDefaultCurrencySymbol());
			value.append("1 = ");
			value.append(curopt.getCurrency().getCurrencySymbol());
			value.append(curopt.getRate());
			if (curopt.isCompanyDefault()) {
				value.append(" [");
				value.append(messages.getMessage(CODE_COMPANY_DEFAULT_RATE, null, MESSAGE_COMPANY_DEFAULT_RATE, locale));
				value.append("]");
			}
			if (curopt.isSystemDefault()) {
				value.append(" [");
				value.append(messages.getMessage(CODE_SYSTEM_DEFAULT_RATE, null, MESSAGE_SYSTEM_DEFAULT_RATE, locale));
				value.append("]");
			}
			result.add(new KeyValue<>(curopt.getCurrency().getCurrencyCode(), value.toString()));
		}
		
		return result;
	}

}
