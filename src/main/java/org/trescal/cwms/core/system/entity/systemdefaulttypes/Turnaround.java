package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.spring.model.KeyValue;

@Component
public class Turnaround extends SystemDefaultRangeType
{
	@Override
	protected Integer getMin() {
		return 0;
	}
	
	@Override
	protected Integer getMax() {
		return 28;
	}
	
	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.TURNAROUND;
	}
	
	/**
	 * Gets turnaround from system default, or business subdivision if system default is 0  
	 * @return Integer turnaround
	 */
	public Integer getTurnaround(Contact jobContact, Subdiv businessSubdiv) {
		// look up default turnaround
		Integer result = null;
		KeyValue<Scope, String> tnt = super.getValueHierarchical(Scope.CONTACT, jobContact, businessSubdiv.getComp());
		if(tnt != null) {
			result = super.parseValue(tnt.getValue());
		}
		if (((result == null) || result == 0) && businessSubdiv.getBusinessSettings() != null) {
			result = businessSubdiv.getBusinessSettings().getDefaultTurnaround();
		}
		if (result == null) {
			// Fallback case if business settings have not been configured correctly
			result = 0;
		}
		return result;
	}
}