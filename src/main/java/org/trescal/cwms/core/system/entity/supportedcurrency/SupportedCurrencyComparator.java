package org.trescal.cwms.core.system.entity.supportedcurrency;

import java.util.Comparator;

public class SupportedCurrencyComparator implements Comparator<SupportedCurrency>
{
	public int compare(SupportedCurrency sc1, SupportedCurrency sc2)
	{
		if ((sc1.getOrderBy() != null) && (sc2.getOrderBy() != null))
		{
			return sc1.getOrderBy().compareTo(sc2.getOrderBy());
		}
		else if ((sc1.getOrderBy() != null) && (sc2 == null))
		{
			return 1;
		}
		else if ((sc1.getOrderBy() == null) && (sc2.getOrderBy() != null))
		{
			return -1;
		}
		else
		{
			return sc1.getCurrencyCode().compareTo(sc2.getCurrencyCode());
		}
	}
}
