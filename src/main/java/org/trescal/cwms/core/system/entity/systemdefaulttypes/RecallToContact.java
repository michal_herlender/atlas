package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class RecallToContact extends SystemDefaultBooleanType
{
	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.RECALL_TO_CONTACT;
	}
}