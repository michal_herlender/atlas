package org.trescal.cwms.core.system.entity.presetcommentcategory.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.presetcommentcategory.PresetCommentCategory;

public interface PresetCommentCategoryService extends BaseService<PresetCommentCategory, Integer> {

	PresetCommentCategory getByName(String category);

	/**
	 * Returns a distinct list of all {@link PresetCommentCategory} categories that
	 * belong to the {@link PresetComment} type.
	 * 
	 * @param commentClass the type of {@link PresetComment} class.
	 * @return list of matching {@link PresetCommentCategory}s.
	 */
	List<PresetCommentCategory> getAllByType(PresetCommentType commentType);
}