package org.trescal.cwms.core.system.entity.note;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@MappedSuperclass
public abstract class Note extends Auditable {
	/**
	 * @deprecated use NoteType.isPrivateOnly
	 */
	public final static boolean privateOnly = false;

	private boolean active;
	private Contact deactivatedBy;
	private Date deactivatedOn;
	private String label;
	private String note;
	private int noteid;
	private Boolean publish;
	private Contact setBy;
	private Date setOn;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deactivatedby")
	public Contact getDeactivatedBy() {
		return this.deactivatedBy;
	}

	@Column(name = "deactivatedon")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDeactivatedOn() {
		return this.deactivatedOn;
	}

	@Length(max = 50)
	@Column(name = "label", length = 50)
	public String getLabel() {
		return this.label;
	}

	@NotNull
	@Length(min = 1, max = 2000)
	@Column(name = "note", columnDefinition = "nvarchar(2000)")
	public String getNote() {
		return this.note;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "noteid")
	@Type(type = "int")
	public int getNoteid() {
		return this.noteid;
	}

	@Transient
	public abstract NoteType getNoteType();

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setby", nullable = false)
	public Contact getSetBy() {
		return this.setBy;
	}

	@Column(name = "seton", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSetOn() {
		return this.setOn;
	}

	@Column(name = "active", nullable = false, columnDefinition = "tinyint")
	public boolean isActive() {
		return this.active;
	}

	@Column(name = "publish", nullable = false, columnDefinition = "tinyint")
	public Boolean getPublish() {
		return this.publish;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setDeactivatedBy(Contact deactivatedBy) {
		this.deactivatedBy = deactivatedBy;
	}

	public void setDeactivatedOn(Date deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setNoteid(int noteid) {
		this.noteid = noteid;
	}

	public void setPublish(Boolean publish) {
		this.publish = publish;
	}

	public void setSetBy(Contact setBy) {
		this.setBy = setBy;
	}

	public void setSetOn(Date setOn) {
		this.setOn = setOn;
	}
}
