package org.trescal.cwms.core.system.dto;

import org.trescal.cwms.core.system.entity.supportedcurrenyratehistory.SupportedCurrencyRateHistory;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SupportedCurrencyAjaxDTO {

	private String rateLastSet;

	private String rateLastSetBy;

	private SupportedCurrencyRateHistoryDTO rateHistory;

	public SupportedCurrencyAjaxDTO(String rateLastSet, String rateLastSetBy, SupportedCurrencyRateHistory rateHistory) {
		super();
		this.rateLastSet = rateLastSet;
		this.rateLastSetBy = rateLastSetBy;
		this.rateHistory = new SupportedCurrencyRateHistoryDTO(rateHistory);
	}

}
