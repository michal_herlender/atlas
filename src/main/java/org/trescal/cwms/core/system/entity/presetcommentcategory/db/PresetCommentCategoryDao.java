package org.trescal.cwms.core.system.entity.presetcommentcategory.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.presetcommentcategory.PresetCommentCategory;

public interface PresetCommentCategoryDao extends BaseDao<PresetCommentCategory, Integer> {

	PresetCommentCategory getByName(String category);

	List<PresetCommentCategory> getAllByType(PresetCommentType commentType);
}