package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class ElectronicCertsOnly extends SystemDefaultBooleanType {
	
	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.ELECTRONIC_CERTS_ONLY;
	}
}