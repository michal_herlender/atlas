package org.trescal.cwms.core.system.entity.systemdefaultapplication.db;

import io.vavr.Tuple2;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;
import org.trescal.cwms.core.system.enums.Scope;

import java.util.List;
import java.util.function.Function;

public interface SystemDefaultApplicationService extends BaseService<SystemDefaultApplication, Integer> {

	SystemDefaultApplication get(int defaultId, Scope scope, int entityId, Integer allocatedCompanyId);

	/**
	 * Returns a {@link SystemDefaultApplication} for the {@link SystemDefault}
	 * identified by the given entityId, at the given {@link Scope} and for the
	 * entity represented by the given entityId (entity can be one of
	 * {@link Company}, {@link Subdiv} or {@link Contact}.
	 *
	 * @param defaultId          the id of the {@link SystemDefault}.
	 * @param scope              the {@link Scope} of the default.
	 * @param entityId           the id of the entity.
	 * @return a {@link SystemDefaultApplication}.
	 */
	SystemDefaultApplication get(int defaultId, Scope scope, int entityId, Company allocatedCompany);


	<V> Tuple2<Scope, V> getGenericValueHierachical(int defaultId, Scope scope, int entityId, Company allocatedCompay, Function<String, V> parser);

	/**
	 * Gets a {@link SystemDefaultApplication} for the {@link SystemDefault}
	 * identified by the given defaultId and that belongs to any of the given entity
	 * ids. If no default application is found for this default then a
	 * {@link SystemDefault} value is returned instead.,
	 *
	 * @param defaultId the id of the {@link SystemDefault}, not null.
	 * @param personid  id of a {@link Contact}.
	 * @param subdivid  id of a {@link Subdiv}.
	 * @param coid      id of a {@link Company}.
	 * @return a {@link SystemDefaultApplication}.
	 */
	SystemDefaultApplication findSystemDefaultApplication(int defaultId, Integer personid, Integer subdivid,
														  Integer coid);

	/**
	 * Finds SystemDefaultApplication for multiple SystemDefault values and multiple
	 * scopes within one query
	 */
	List<SystemDefaultApplication> find(List<SystemDefault> listSD, Company company, Subdiv subdiv, Contact contact,
										Integer allocatedCompanyId);

	List<SystemDefaultApplication> findApplicationWide(List<SystemDefault> listSD);
}