package org.trescal.cwms.core.system.form;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.emailrecipient.RecipientType;
import org.trescal.cwms.core.validation.AbstractBeanValidator;


/**
 * Custom Validator for validating a new {@link Email} and it's
 * {@link EmailRecipient}s.
 * 
 * @author jamiev
 */
@Component
public class CreateEmailValidator extends AbstractBeanValidator
{
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(EmailForm.class);
	}

	public void validate(Object command, Errors errors)
	{
		EmailForm form = (EmailForm) command;
		Email email = form.getEmail();
		List<EmailRecipient> recips = form.getRecipients();

		super.validate(form, errors);
		
		Set<ConstraintViolation<Email>> violations = super.getConstraintViolations(email);
		for (ConstraintViolation<Email> value : violations)
		{
			String prop = value.getPropertyPath().toString();
			if (prop.equals("from") || prop.equals("subject")
					|| prop.equals("body"))
			{
				errors.rejectValue(prop, null, value.getMessage());
			}
			else
			{
				errors.rejectValue("email." + value.getPropertyPath(), null, value.getMessage());
			}
		}

		if (recips.size() > 0)
		{
			for (EmailRecipient recip : recips)
			{
				Set<ConstraintViolation<EmailRecipient>> violations1 = super.getConstraintViolations(recip);
				for (ConstraintViolation<EmailRecipient> value : violations1)
				{
					if (value.getPropertyPath().toString().equals("emailAddress"))
					{
						if (recip.getType().equals(RecipientType.EMAIL_TO))
						{
							if (recip.getRecipientCon() == null)
							{
								errors.rejectValue("toEmails", null, "'"
										+ value.getInvalidValue() + "' : "
										+ value.getMessage());
							}
							else
							{
								errors.rejectValue("toIds", null, "'"
										+ value.getInvalidValue() + "' : "
										+ value.getMessage());
							}
						}
						else
						{
							if (recip.getRecipientCon() == null)
							{
								errors.rejectValue("ccEmails", null, "'"
										+ value.getInvalidValue() + "' : "
										+ value.getMessage());
							}
							else
							{
								errors.rejectValue("ccIds", null, "'"
										+ value.getInvalidValue() + "' : "
										+ value.getMessage());
							}
						}
					}
					else
					{
						errors.rejectValue("recipients[0]."
								+ value.getPropertyPath(), null, value.getMessage());
					}
				}
			}
		}
		else
		{
			errors.rejectValue("recipients", null, "An e-mail must have at least one recipient.");
		}
	}
}