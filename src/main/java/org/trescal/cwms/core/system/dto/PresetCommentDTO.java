package org.trescal.cwms.core.system.dto;

import java.util.Date;

import javax.validation.constraints.Size;

import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;

public class PresetCommentDTO {

	private Integer id;
	private Integer categoryId;
	private String label;
	private String comment;
	private Date seton;
	private String setby;
	private PresetCommentType commentType;
	private NoteType noteType;

	public PresetCommentDTO() {
	}

	public PresetCommentDTO(PresetComment presetComment) {
		this.id = presetComment.getId();
		this.categoryId = presetComment.getCategory() == null ? null : presetComment.getCategory().getId();
		this.label = presetComment.getLabel();
		this.comment = presetComment.getComment();
		this.seton = presetComment.getSeton();
		this.setby = presetComment.getSetby().getName();
		this.commentType = presetComment.getType();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Size(max = 2000)
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getSeton() {
		return seton;
	}

	public void setSeton(Date seton) {
		this.seton = seton;
	}

	public String getSetby() {
		return setby;
	}

	public void setSetby(String setby) {
		this.setby = setby;
	}

	public PresetCommentType getCommentType() {
		return commentType;
	}

	public void setCommentType(PresetCommentType commentType) {
		this.commentType = commentType;
	}

	public NoteType getNoteType() {
		return noteType;
	}

	public void setNoteType(NoteType noteType) {
		this.noteType = noteType;
	}
}