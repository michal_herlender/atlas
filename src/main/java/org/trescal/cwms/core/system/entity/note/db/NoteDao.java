package org.trescal.cwms.core.system.entity.note.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.dto.NoteForInstrumentTooltipDto;

public interface NoteDao
{
	Note findNote(int id, Class<? extends Note> clazz);

	List<Note> findNotes(List<Integer> noteids, Class<?> clazz);

	Object findObject(int id, Class<?> clazz);

	void insertNote(Note note);

	void saveOrUpdate(Note note);

	Boolean noteExists(Class<?> clazz, Class<?> entityClazz, Integer noteTypeId, String noteContent);

    List<NoteForInstrumentTooltipDto> findNotesForInstrumentTooltipByPlantId(Integer plantId);
}