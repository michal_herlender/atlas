package org.trescal.cwms.core.system.entity.calibrationaccreditationlevel.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.calibrationaccreditationlevel.CalibrationAccreditationLevel;

@Repository("CalibrationAccreditationLevelDao")
public class CalibrationAccreditationLevelDaoImpl extends BaseDaoImpl<CalibrationAccreditationLevel, Integer> implements CalibrationAccreditationLevelDao {
	
	@Override
	protected Class<CalibrationAccreditationLevel> getEntity() {
		return CalibrationAccreditationLevel.class;
	}
}