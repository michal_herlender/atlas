package org.trescal.cwms.core.system.entity.printfilerule.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.printfilerule.PrintFileRule;

@Repository("PrintFileRuleDao")
public class PrintFileRuleDaoImpl extends BaseDaoImpl<PrintFileRule, Integer> implements PrintFileRuleDao {
	
	@Override
	protected Class<PrintFileRule> getEntity() {
		return PrintFileRule.class;
	}
}