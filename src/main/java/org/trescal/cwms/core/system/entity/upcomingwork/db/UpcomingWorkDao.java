package org.trescal.cwms.core.system.entity.upcomingwork.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork;

import java.time.LocalDate;
import java.util.List;

public interface UpcomingWorkDao extends BaseDao<UpcomingWork, Integer> {
    Long countUpcomingWorkBetweenDates(LocalDate startDate, LocalDate endDate, Integer allocatedSubdivId);

    List<UpcomingWork> getUpcomingWorkBetweenDates(LocalDate startDate, LocalDate endDate, Subdiv allocatedSubdiv);

    List<UpcomingWork> getUpcomingWorkForUser(int personid, Subdiv allocatedSubdiv);
}