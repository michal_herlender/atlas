package org.trescal.cwms.core.system.entity.emailrecipient.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipient;
import org.trescal.cwms.core.system.entity.emailrecipient.EmailRecipientDto;

public interface EmailRecipientService
{
	/**
	 * Deletes the given {@link EmailRecipient} from the database.
	 * 
	 * @param emailrecipient the {@link EmailRecipient} to delete.
	 */
	void deleteEmailRecipient(EmailRecipient emailrecipient);

	/**
	 * Returns the {@link EmailRecipient} with the given ID.
	 * 
	 * @param id the {@link EmailRecipient} ID.
	 * @return the {@link EmailRecipient}
	 */
	EmailRecipient findEmailRecipient(int id);

	List<EmailRecipientDto> searchRecipientsDto(Collection<Integer> emailIds);
	
	/**
	 * Returns a {@link List} of all {@link EmailRecipient}s currently stored
	 * in the database.
	 * 
	 * @return the {@link List} of {@link EmailRecipient}s
	 */
	List<EmailRecipient> getAllEmailRecipients();

	/**
	 * Inserts the given {@link EmailRecipient} into the database.
	 * 
	 * @param emailrecipient the {@link EmailRecipient} to insert.
	 */
	void insertEmailRecipient(EmailRecipient emailrecipient);

	/**
	 * Inserts the given {@link EmailRecipient} into the database if it doesn't
	 * already exist. Otherwise, updates the given {@link EmailRecipient}.
	 * 
	 * @param emailrecipient the {@link EmailRecipient} to save or update.
	 */
	void saveOrUpdateEmailRecipient(EmailRecipient emailrecipient);

	/**
	 * Updates the given {@link EmailRecipient} in the database.
	 * 
	 * @param emailrecipient the {@link EmailRecipient} to update.
	 */
	void updateEmailRecipient(EmailRecipient emailrecipient);
}