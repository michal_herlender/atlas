package org.trescal.cwms.core.system.entity.userinstructiontype;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

/**
 * Lookup entity which defines the different types of {@link Instruction}
 * available on the system. This could/should be turned into an enum?
 */
@Entity
@Table(name = "userinstructiontype", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "instructiontypeid", "userprefid" }) })
public class UserInstructionType {

	private int id;
	private InstructionType instructionType;
	private UserPreferences userPreference;

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "instructiontypeid")
	public InstructionType getInstructionType() {
		return this.instructionType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userprefid")
	public UserPreferences getUserPreference() {
		return this.userPreference;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setInstructionType(InstructionType instructionType) {
		this.instructionType = instructionType;
	}

	public void setUserPreference(UserPreferences userPreference) {
		this.userPreference = userPreference;
	}
}