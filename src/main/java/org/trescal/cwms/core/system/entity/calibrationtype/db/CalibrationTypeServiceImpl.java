package org.trescal.cwms.core.system.entity.calibrationtype.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.Tuple;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.dto.CalibrationTypeOptionDto;
import org.trescal.cwms.core.system.entity.calibrationtype.dto.TranslatedCalibrationTypeDto;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.DefaultServiceTypeForOnsiteJobs;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.DefaultServiceTypeForSinglePriceJobs;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.DefaultServiceTypeForStandardJobs;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.Scope;

@Service("caltypeService")
public class CalibrationTypeServiceImpl extends BaseServiceImpl<CalibrationType, Integer>
		implements CalibrationTypeService {

	@Autowired
	private CalibrationTypeDao ctDao;
	@Autowired
	private DefaultServiceTypeForOnsiteJobs defaultServiceTypeForOnsiteJobs;
	@Autowired
	private DefaultServiceTypeForStandardJobs defaultServiceTypeForStandardJobs;
	@Autowired
	private DefaultServiceTypeForSinglePriceJobs defaultServiceTypeForSinglePriceJobs;
	@Autowired
	private TranslationService translationService;

	@Override
	protected BaseDao<CalibrationType, Integer> getBaseDao() {
		return ctDao;
	}

	@Override
	public List<TranslatedCalibrationTypeDto> getTranslatedCalibrationTypes(String language, String country) {
		Locale locale = new Locale(language, country);
		List<TranslatedCalibrationTypeDto> dtoList = new ArrayList<>();
		List<CalibrationType> calTypes = this.getActiveCalTypes();
		for (CalibrationType calType : calTypes) {
			dtoList.add(new TranslatedCalibrationTypeDto(calType.getCalTypeId(),
					translationService.getCorrectTranslation(calType.getServiceType().getLongnameTranslation(), locale),
					translationService.getCorrectTranslation(calType.getServiceType().getShortnameTranslation(),
							locale)));
		}
		return dtoList;
	}

	@Override
	public CalibrationType getDefaultCalType(JobType jobType, Subdiv allocatedSubdiv) {

		ServiceType st = null;
		if (jobType.equals(JobType.STANDARD))
			st = defaultServiceTypeForStandardJobs.parseValueHierarchical(Scope.SUBDIV, allocatedSubdiv,
					allocatedSubdiv.getComp());
		else if (jobType.equals(JobType.SITE))
			st = defaultServiceTypeForOnsiteJobs.parseValueHierarchical(Scope.SUBDIV, allocatedSubdiv,
					allocatedSubdiv.getComp());
		else if (jobType.equals(JobType.SINGLE_PRICE))
			st = defaultServiceTypeForSinglePriceJobs.parseValueHierarchical(Scope.SUBDIV, allocatedSubdiv,
					allocatedSubdiv.getComp());

		if (st != null)
			return st.getCalibrationType();
		else
			return null;

	}

	@Override
	public void delete(CalibrationType caltype) {
		this.ctDao.remove(caltype);
	}

	@Override
	public CalibrationType find(int calTypeId) {
		return this.ctDao.find(calTypeId);
	}

	@Override
	public List<CalibrationType> getActiveProcedureCalTypes() {
		return this.ctDao.getActiveProcedureCalTypes();
	}

	@Override
	public List<CalibrationType> getActiveCalTypes() {
		return this.ctDao.getCalTypes(true);
	}

	@Override
	public List<CalibrationTypeOptionDto> getActiveCalTypes(JobType jobType) {
		return this.ctDao.getActiveCalTypesOptions(jobType);
	}

	@Override
	@Cacheable("cachedServiceTypesByJobTypeShortName")
	public Map<Integer, String> getCachedServiceTypesByJobType(JobType jobType, Locale locale) {
		return this.ctDao.getActiveCalTypes(jobType).stream()
				.collect(Collectors.toMap(ct -> ct.getServiceType().getServiceTypeId(), ct -> {
					return translationService.getCorrectTranslation(ct.getServiceType().getShortnameTranslation(),
							locale);
				}));
	}

	@Override
	public List<CalibrationType> getCalTypes() {
		return this.ctDao.getCalTypes(null);
	}

	@Override
	public Map<Integer, Integer> getCalTypeToServiceTypeMap() {
		List<Tuple> queryResults = this.ctDao.getCalTypeAndServiceTypeIds();
		Map<Integer, Integer> results = new HashMap<>();
		for (Tuple tuple : queryResults) {
			Integer calTypeId = (Integer) tuple.get(0);
			Integer serviceTypeId = (Integer) tuple.get(1);
			results.put(calTypeId, serviceTypeId);
		}
		return results;
	}

	@Override
	public CalibrationType getDefaultCalType() {

		return this.ctDao.getDefaultCalType();
	}

	@Override
	public void insert(CalibrationType caltype) {
		this.ctDao.persist(caltype);
	}

	@Override
	public void update(CalibrationType caltype) {
		this.ctDao.update(caltype);
	}

	@Override
	public Boolean isCompatibleWithJobType(CalibrationType caltype, JobType jobtype) {
		return this.ctDao.isCompatibleWithJobType(caltype, jobtype);
	}

}