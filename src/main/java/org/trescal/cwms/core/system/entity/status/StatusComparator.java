package org.trescal.cwms.core.system.entity.status;

import java.util.Comparator;

/**
 * Generic Comparator for comparing system status's
 * @author Richard
 *
 */
public class StatusComparator implements Comparator<Status>
{
	public int compare(Status qs1, Status qs2)
	{
		return qs1.getName().compareTo(qs2.getName());
	}
}
