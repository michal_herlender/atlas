package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class AsFoundCertificates extends SystemDefaultBooleanType {
	
	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.AS_FOUND_CERTIFICATES;
	}
}