package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.recall.entity.recall.RecallType;

@Component
public class RecallLevel extends SystemDefaultType<RecallType> {
	
	private static final Logger logger = LoggerFactory.getLogger(RecallLevel.class);

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.RECALL_LEVEL;
	}

	@Override
	public boolean isDiscreteType() {
		return true;
	}

	@Override
	public List<String> getValues() {
		List<String> result = new ArrayList<>();
		for (RecallType type : RecallType.values()) {
			result.add(type.name());			
		}
		
		return result;
	}

	/**
	 * Returns RecallType.CONTACT as a default (logging issue) if the value is incorrectly specified
	 * (as there are no database constraints on value)
	 */
	@Override
	public RecallType parseValue(String value) {
		RecallType result = RecallType.CONTACT;
		if (value != null) {
			try {
				result = RecallType.valueOf(value);
			}
			catch (Exception e) {
				logger.error("Incorrect system default value "+value+", defaulting to RecallType.CONTACT");
			}
		}
		else {
			logger.error("Null system default value, defaulting to RecallType.CONTACT");
		}
		return result;
	}

	@Override
	public boolean isMultiChoice() {
		return false;
	}
}
