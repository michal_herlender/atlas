package org.trescal.cwms.core.system.entity.emailtemplate.db;

import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.emailtemplate.EmailTemplate;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

public interface EmailTemplateDao extends AllocatedDao<Company, EmailTemplate, Integer> {
	EmailTemplate get(Company company, Locale locale, Component component, Subdiv subdiv, EmailTemplate exclude);
}
