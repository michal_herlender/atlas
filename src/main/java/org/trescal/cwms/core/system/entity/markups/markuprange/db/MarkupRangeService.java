package org.trescal.cwms.core.system.entity.markups.markuprange.db;

import java.math.BigDecimal;
import java.util.List;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRange;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRangeAction;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRangeWrapper;
import org.trescal.cwms.core.system.entity.markups.markuptype.MarkupType;

public interface MarkupRangeService
{
	/**
	 * Creates a new {@link MarkupRange} for the {@link MarkupType} identified
	 * by the given typeid. Checks if a {@link MarkupRange} for the given
	 * rangestart already exists.
	 * 
	 * @param type id of the {@link MarkupRange}, not null.
	 * @param rangeStart the value of the range start.
	 * @param value the value of the {@link MarkupRange}, not null.
	 * @param action the {@link MarkupRangeAction}, not null.
	 * @return wrapped results.
	 */
	ResultWrapper addMarkupRange(MarkupType type, double rangeStart, double value, MarkupRangeAction action);

	/**
	 * Calculates the markup value that should be applied to the given value for
	 * the given markup type. If no range can be found or no value ascertained
	 * then a new {@link BigDecimal} set to 0.00 is returned.
	 * 
	 * @param value the value to apply the markup to.
	 * @param type the {@link MarkupType}.
	 * @return the value of any markup to be applied.
	 */
	BigDecimal calculateMarkUp(BigDecimal value, MarkupType type);

	/**
	 * Calculates the markup value that should be applied to the given value for
	 * the given markup type and inserts the values into a
	 * {@link MarkupRangeWrapper}. If no range can be found or no value
	 * ascertained then a new {@link BigDecimal} set to 0.00 is returned.
	 * 
	 * @param value the value to apply the markup to.
	 * @param type the {@link MarkupType}.
	 * @return the value of any markup to be applied.
	 */
	MarkupRangeWrapper calculateMarkUpRangeValue(BigDecimal value, MarkupType type);

	void deleteMarkupRange(MarkupRange markuprange);

	MarkupRange findMarkupRange(int id);

	/**
	 * Gets a list of all {@link MarkupRange} of the given type whoose
	 * rangestart matches the given rangestart.
	 * 
	 * @param type the {@link MarkupType}, not null.
	 * @param rangestart the range start.
	 * @return list of {@link MarkupRange}.
	 */
	List<MarkupRange> findMarkupRange(MarkupType type, double rangestart);

	List<MarkupRange> getAllMarkupRanges();

	/**
	 * Returns a list of all {@link MarkupRange} of the given {@link MarkupType}
	 * name in order of rangeStart descending.
	 * 
	 * @param type is the {@link MarkupType}.
	 * @return list of matching {@link MarkupRange}.
	 */
	List<MarkupRange> getAllMarkupRanges(MarkupType type);

	void insertMarkupRange(MarkupRange markuprange);

	void saveOrUpdateMarkupRange(MarkupRange markuprange);

	/**
	 * Saves the {@link MarkupRange} identified by the given id with the new
	 * rangestart and value.
	 * 
	 * @param id the id of the {@link MarkupRange}.
	 * @param rangeStart the rangestart.
	 * @param value the value.
	 * @return wrapped results.
	 */
	ResultWrapper updateMarkupRange(int id, double rangeStart, double value);

	void updateMarkupRange(MarkupRange markuprange);
}