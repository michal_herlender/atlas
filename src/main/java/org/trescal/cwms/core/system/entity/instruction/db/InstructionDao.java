package org.trescal.cwms.core.system.entity.instruction.db;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.dto.InstructionDTO;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

public interface InstructionDao extends BaseDao<Instruction, Integer> {

	List<InstructionDTO> findAllLinkedToCompanyByItem(Integer jobItemId, InstructionType... instructionTypes);

	List<InstructionDTO> findAllLinkedToCompanyByItems(Collection<Integer> jobItemIds,
			InstructionType... instructionTypes);

	List<InstructionDTO> findAllLinkedToCompanyByJobs(Collection<Integer> jobIds, InstructionType... instructionTypes);

	List<InstructionDTO> findAllLinkedToContactByItem(Integer jobItemId, InstructionType... instructionTypes);

	List<InstructionDTO> findAllLinkedToContactByItems(Collection<Integer> jobItemIds,
			InstructionType... instructionTypes);

	List<InstructionDTO> findAllLinkedToContactByJobs(Collection<Integer> jobIds, InstructionType... instructionTypes);

	List<InstructionDTO> findAllLinkedToContractByItem(Integer jobItemId, InstructionType... instructionTypes);

	List<InstructionDTO> findAllLinkedToContractByItems(Collection<Integer> jobItemIds,
			InstructionType... instructionTypes);

	List<InstructionDTO> findAllLinkedToJob(Integer jobId, InstructionType... instructionTypes);

	List<InstructionDTO> findAllLinkedToJobByItem(Integer jobItemId, InstructionType... instructionTypes);

	List<InstructionDTO> findAllLinkedToJobByItems(Collection<Integer> jobItemIds, InstructionType... instructionTypes);

	List<InstructionDTO> findAllLinkedToSubdivByItem(Integer jobItemId, InstructionType... instructionTypes);

	List<InstructionDTO> findAllLinkedToSubdivByItems(Collection<Integer> jobItemIds,
			InstructionType... instructionTypes);

	List<InstructionDTO> findAllLinkedToSubdivByJobs(Collection<Integer> jobIds, InstructionType... instructionTypes);
}