package org.trescal.cwms.core.system.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.spring.model.KeyValue;


@RestController
@RequestMapping("currencylookup")
public class CurrencyLookupController {

    private CompanyService companyService;

    public CurrencyLookupController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("currencysymbolforcompany/{id}")
    public KeyValue<Integer, String> getCurrencies(@PathVariable("id") Integer companyID) {
        Company company = companyService.get(companyID);
        return new KeyValue<>(company.getCurrency().getCurrencyId(),company.getCurrency().getCurrencyERSymbol());
    }
}
