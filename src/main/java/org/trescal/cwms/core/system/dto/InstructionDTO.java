package org.trescal.cwms.core.system.dto;

import lombok.Data;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink;
import org.trescal.cwms.core.jobs.job.entity.jobinstructionlink.JobInstructionLink;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionEntity;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLink;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.tools.DateTools;

import java.util.Date;

@Data
public class InstructionDTO {

	private Integer id;
	private String instruction;
	private InstructionType type;
	private String color;
	private InstructionEntity linkedToType;
	private String linkedToName;
	private Integer linkId;
	private Boolean isSubdivInstruction;
	private String businessSubdiv;
	private Boolean includeOnDelNote;
	private Boolean includeOnSupplierDelNote;
	private String typeTranslation;
	private String linkedTypeMessageCode;
	private String lastModifiedBy;
	private Date lastModified;

	/**
	 * Constructor used by JPA queries
	 */
	public InstructionDTO(Integer id, String instruction, InstructionType type, Date lastModified, String lastModifiedByFirstName,
						  String lastModifiedByLastName, String linkedToType, String linkedToName, Boolean isSubdivInstruction, String businessSubdiv,
						  Boolean includeOnDelNote, Boolean includeOnSupplierDelNote) {
		super();
		this.id = id;
		this.instruction = instruction;
		this.type = type;
		this.color = DateTools.isDateAfterXTimescale(-1, "m", lastModified) ? "red"
			: DateTools.isDateAfterXTimescale(-3, "m", lastModified) ? "amber" : "brown";
		this.linkedToType = InstructionEntity.valueOf(linkedToType);
		this.linkedToName = linkedToName;
		this.isSubdivInstruction = isSubdivInstruction;
		this.businessSubdiv = businessSubdiv;
		this.includeOnDelNote = includeOnDelNote;
		this.includeOnSupplierDelNote = includeOnSupplierDelNote;
		this.typeTranslation = type.getType();
		this.linkedTypeMessageCode = this.linkedToType.getMessageCode();
		this.lastModifiedBy = lastModifiedByFirstName + " " + lastModifiedByLastName;
		this.lastModified = lastModified;
	}
	
	public InstructionDTO(InstructionLink<?> instructionLink) {
		this.id = instructionLink.getInstruction().getInstructionid();
		this.instruction = instructionLink.getInstruction().getInstruction();
		this.type = instructionLink.getInstruction().getInstructiontype();
		this.color = DateTools.isDateAfterXTimescale(-1, "m", instructionLink.getInstruction().getLastModified()) ? "red"
				: DateTools.isDateAfterXTimescale(-3, "m", instructionLink.getInstruction().getLastModified()) ? "amber" : "brown";
		this.linkedToType = instructionLink.getInstructionEntity();
		if(this.linkedToType != null && this.linkedToType.equals(InstructionEntity.COMPANY)) {
			this.linkedToName = ((CompanyInstructionLink) instructionLink).getCompany().getConame();
			this.linkId =  ((CompanyInstructionLink) instructionLink).getId();
			this.isSubdivInstruction = ((CompanyInstructionLink) instructionLink).isSubdivInstruction();
			if(((CompanyInstructionLink) instructionLink).getBusinessSubdiv() != null)
				this.businessSubdiv = ((CompanyInstructionLink) instructionLink).getBusinessSubdiv().getSubname();
		}
		else if(this.linkedToType != null && this.linkedToType.equals(InstructionEntity.SUBDIV)) {
			this.linkedToName = ((SubdivInstructionLink) instructionLink).getSubdiv().getSubname();
			this.linkId =  ((SubdivInstructionLink) instructionLink).getId();
			this.isSubdivInstruction = ((SubdivInstructionLink) instructionLink).isSubdivInstruction();
			if(((SubdivInstructionLink) instructionLink).getBusinessSubdiv() != null)
				this.businessSubdiv = ((SubdivInstructionLink) instructionLink).getBusinessSubdiv().getSubname();
		}
		else if(this.linkedToType != null && this.linkedToType.equals(InstructionEntity.CONTACT)) {
			this.linkedToName = ((ContactInstructionLink) instructionLink).getContact().getName();
			this.linkId =  ((ContactInstructionLink) instructionLink).getId();
		} else if (this.linkedToType != null && this.linkedToType.equals(InstructionEntity.JOB)) {
			this.linkedToName = ((JobInstructionLink) instructionLink).getJob().getJobno();
			this.linkId = ((JobInstructionLink) instructionLink).getId();
		}
		this.includeOnDelNote = instructionLink.getInstruction().getIncludeOnDelNote();
		this.includeOnSupplierDelNote = instructionLink.getInstruction().getIncludeOnSupplierDelNote();
		this.typeTranslation = type.getType();
		this.linkedTypeMessageCode = this.linkedToType.getMessageCode();
		this.lastModifiedBy = instructionLink.getInstruction().getLastModifiedBy().getName();
		this.lastModified = instructionLink.getInstruction().getLastModified();
	}
	
	
	public InstructionDTO(Instruction instruction) {
		this.id = instruction.getInstructionid();
		this.instruction = instruction.getInstruction();
		this.type = instruction.getInstructiontype();
		this.color = DateTools.isDateAfterXTimescale(-1, "m", instruction.getLastModified()) ? "red"
			: DateTools.isDateAfterXTimescale(-3, "m", instruction.getLastModified()) ? "amber" : "brown";
		this.isSubdivInstruction = false;
		this.includeOnDelNote = instruction.getIncludeOnDelNote();
		this.includeOnSupplierDelNote = instruction.getIncludeOnSupplierDelNote();
		this.typeTranslation = type.getType();
		this.lastModifiedBy = instruction.getLastModifiedBy().getName();
		this.lastModified = instruction.getLastModified();
	}
}