package org.trescal.cwms.core.system.entity.accreditedlab.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Set;

public interface AccreditedLabService extends BaseService<AccreditedLab, Integer> {
    Set<KeyValue<Integer, String>> getDtosBySubdivId(Integer subdivId);

    Set<KeyValue<Integer, String>> getDtosBySubdiv(Subdiv allocatedSubdiv);
}
