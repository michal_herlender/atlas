package org.trescal.cwms.core.system.entity.systemdefaultapplication.db;

import lombok.val;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault_;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication;
import org.trescal.cwms.core.system.entity.systemdefaultapplication.SystemDefaultApplication_;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;
import org.trescal.cwms.core.system.enums.Scope;

import javax.persistence.criteria.*;
import java.util.List;
import java.util.Optional;

@Repository("SystemDefaultApplicationDao")
public class SystemDefaultApplicationDaoImpl extends AllocatedToCompanyDaoImpl<SystemDefaultApplication, Integer>
	implements SystemDefaultApplicationDao {

	@Override
	protected Class<SystemDefaultApplication> getEntity() {
		return SystemDefaultApplication.class;
	}

	@Override
	public SystemDefaultApplication get(int defaultId, Scope scope, Integer entityId, Integer allocatedCompanyId) {
		return getOpt(defaultId, scope, entityId, allocatedCompanyId).orElse(null);
	}

	@Override
	public List<SystemDefaultApplication> find(List<SystemDefault> listSD, Company company, Subdiv subdiv,
			Contact contact, Integer allocatedCompanyId) {
		if (listSD == null)
			throw new IllegalArgumentException("Parameter was null: List<SystemDefault> listSD");
		// Must have at least one systemdefault to search for; SQL fails otherwise
		// (empty list returned by service layer)
		if (listSD.isEmpty())
			throw new IllegalArgumentException("Parameter was empty: List<SystemDefault listSD>");
		if (company == null)
			throw new IllegalArgumentException("Parameter was null: Company company");

		return getResultList(cb -> {
			CriteriaQuery<SystemDefaultApplication> cq = cb.createQuery(SystemDefaultApplication.class);

			Root<SystemDefaultApplication> rootSDA = cq.from(SystemDefaultApplication.class);
			Join<SystemDefaultApplication, SystemDefault> joinSD = rootSDA.join(SystemDefaultApplication_.systemDefault,
				JoinType.INNER);

			Predicate conjunction = cb.conjunction();
			conjunction.getExpressions().add(joinSD.in(listSD));
			if (allocatedCompanyId != null) {
				Join<SystemDefaultApplication, Company> joinAllocated = rootSDA
					.join(SystemDefaultApplication_.organisation.getName(), JoinType.INNER); // Generic attribute type
																								// mismatch
			conjunction.getExpressions().add(joinAllocated.get(Company_.coid).in(allocatedCompanyId));
		}

		Predicate disjunction = cb.disjunction();
			disjunction.getExpressions().add(cb.equal(rootSDA.get(SystemDefaultApplication_.company), company));
			if (subdiv != null) {
				disjunction.getExpressions().add(cb.equal(rootSDA.get(SystemDefaultApplication_.subdiv), subdiv));
			}
			if (contact != null) {
				disjunction.getExpressions().add(cb.equal(rootSDA.get(SystemDefaultApplication_.contact), contact));
			}
			conjunction.getExpressions().add(disjunction);

			cq.where(conjunction);
			return cq;
		});
	}

	@Override
	public SystemDefaultApplication findSystemDefaultApplication(int defaultId, Integer personid, Integer subdivid,
			Integer coid) {
		return getFirstResult(cb -> {
			CriteriaQuery<SystemDefaultApplication> cq = cb.createQuery(SystemDefaultApplication.class);
			Root<SystemDefaultApplication> sda = cq.from(SystemDefaultApplication.class);
			Predicate clauses = cb.conjunction();
			if (personid != null) {
				Join<SystemDefaultApplication, Contact> contact = sda.join(SystemDefaultApplication_.contact);
				clauses.getExpressions().add(cb.equal(contact.get(Contact_.personid), personid));
			}
			if (subdivid != null) {
				Join<SystemDefaultApplication, Subdiv> subdiv = sda.join(SystemDefaultApplication_.subdiv);
				clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), subdivid));
			}
			if (coid != null) {
				Join<SystemDefaultApplication, Company> company = sda.join(SystemDefaultApplication_.company);
				clauses.getExpressions().add(cb.equal(company.get(Company_.coid), coid));
			}
			return cq;
		}).orElse(null);
	}

	@Override
	public Optional<SystemDefaultApplication> getOpt(int defaultId, Scope scope, Integer entityId, Integer allocatedCompanyId) {
		SystemDefaultNames defaultName = SystemDefaultNames.getByOrdinal(defaultId);
		return getFirstResult(cb -> {
			CriteriaQuery<SystemDefaultApplication> cq = cb.createQuery(SystemDefaultApplication.class);
			Root<SystemDefaultApplication> root = cq.from(SystemDefaultApplication.class);
			Join<SystemDefaultApplication, SystemDefault> systemDefault = root
				.join(SystemDefaultApplication_.systemDefault);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(systemDefault.get(SystemDefault_.id), defaultName));
			clauses.getExpressions().add(cb.or(cb.isTrue(systemDefault.get(SystemDefault_.groupwide)),
				cb.equal(root.get(SystemDefaultApplication_.organisation), allocatedCompanyId)));
			switch (scope) {
				case COMPANY:
					Join<SystemDefaultApplication, Company> company = root.join(SystemDefaultApplication_.company);
					clauses.getExpressions().add(cb.equal(company.get(Company_.coid), entityId));
					break;
				case SUBDIV:
					Join<SystemDefaultApplication, Subdiv> subdiv = root.join(SystemDefaultApplication_.subdiv);
					clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), entityId));
					break;
				case CONTACT:
					Join<SystemDefaultApplication, Contact> contact = root.join(SystemDefaultApplication_.contact);
					clauses.getExpressions().add(cb.equal(contact.get(Contact_.personid), entityId));
					break;
				case SYSTEM:
					clauses.getExpressions().add(cb.isNull(root.get(SystemDefaultApplication_.contact)));
					clauses.getExpressions().add(cb.isNull(root.get(SystemDefaultApplication_.subdiv)));
					clauses.getExpressions().add(cb.isNull(root.get(SystemDefaultApplication_.company)));
					break;
			}
			cq.where(clauses);
			return cq;
		});
	}

	@Override
	public List<SystemDefaultApplication> findApplicationWide(List<SystemDefault> listSD) {
		return getResultList(cb -> {
			val cq = cb.createQuery(SystemDefaultApplication.class);
			val sda = cq.from(SystemDefaultApplication.class);
			cq.where(sda.get(SystemDefaultApplication_.systemDefault).in(listSD),
				cb.isNull(sda.get(SystemDefaultApplication_.company)),
				cb.isNull(sda.get(SystemDefaultApplication_.contact)),
				cb.isNull(sda.get(SystemDefaultApplication_.subdiv))
			);

			return cq;
		});
	}
}