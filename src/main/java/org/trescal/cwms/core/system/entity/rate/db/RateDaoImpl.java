package org.trescal.cwms.core.system.entity.rate.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.rate.Rate;

@Repository("RateDao")
public class RateDaoImpl extends BaseDaoImpl<Rate, Integer> implements RateDao {
	
	@Override
	protected Class<Rate> getEntity() {
		return Rate.class;
	}
}