package org.trescal.cwms.core.system.controller;

import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.InstructionDTO;
import org.trescal.cwms.core.system.entity.instruction.db.InstructionService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.List;

@RestController
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class AjaxInstructionController {
    private final InstructionService instructionService;

    public AjaxInstructionController(InstructionService instructionService) {
        this.instructionService = instructionService;
    }

    @RequestMapping(value = "/getInstructions.json", method = RequestMethod.GET, produces = Constants.MEDIATYPE_APPLICATION_JSON_UTF8)
    protected List<InstructionDTO> onSubmitSubdiv(
            @RequestParam(value = "allocatedSubdivid", required = false) Integer allocatedSubdivid,
            @RequestParam(value = "linkCoids", required = false) List<Integer> linkCoids,
            @RequestParam(value = "linkSubdivids", required = false) List<Integer> linkSubdivids,
            @RequestParam(value = "linkContactids", required = false) List<Integer> linkContactids,
            @RequestParam(value = "linkContractid", required = false) Integer linkContractid,
            @RequestParam(value = "linkJobid", required = false) Integer linkJobid,
            @RequestParam(value = "jobIds", required = false) List<Integer> jobIds,
            @RequestParam(value = "jobitemIds", required = false) List<Integer> jobItemIds,
            @RequestParam(value = "instructionTypes", required = false) InstructionType[] instructionTypes,
            @RequestParam(value = "deliveryType", required = false) DeliveryType deliveryType,
            @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {

        return this.instructionService.ajaxInstructions(allocatedSubdivid != null ? allocatedSubdivid : subdivDto.getKey(), linkCoids, linkSubdivids, linkContactids, linkContractid, linkJobid, jobIds, jobItemIds, instructionTypes != null ? instructionTypes : InstructionType.values(), deliveryType);

    }


}