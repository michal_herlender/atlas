package org.trescal.cwms.core.system.entity.alligatorsettingscontact.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettings;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettingsContact;
import org.trescal.cwms.core.system.entity.alligatorsettings.db.AlligatorSettingsService;

@Service
public class AlligatorSettingsContactServiceImpl 
	extends BaseServiceImpl<AlligatorSettingsContact, Integer> 
	implements AlligatorSettingsContactService {

	@Autowired
	private AlligatorSettingsService asService;
	@Autowired
	private AlligatorSettingsContactDao ascDao;
	
	@Override
	protected BaseDao<AlligatorSettingsContact, Integer> getBaseDao() {
		return ascDao;
	}
	
	@Override
	public void editAlligatorSettingsContact(Contact businessContact, boolean useContactSettings, int productionSettingsId, int testSettingsId) {
		AlligatorSettingsContact asc = findAlligatorSettingsContact(businessContact);
		AlligatorSettings settingsProduction = productionSettingsId == 0 ? null : asService.get(productionSettingsId);
		AlligatorSettings settingsTest = testSettingsId == 0 ? null : asService.get(testSettingsId);
		
		if (useContactSettings) {
			if (asc == null) {
				// Case 1 - create settings
				asc = new AlligatorSettingsContact();
				asc.setContact(businessContact);
				asc.setSettingsProduction(settingsProduction);
				asc.setSettingsTest(settingsTest);
				super.save(asc);
			}
			else {
				// Case 2 - update settings
				asc.setSettingsProduction(settingsProduction);
				asc.setSettingsTest(settingsTest);
			}
		}
		else if (asc != null) {
			// Case 3 - delete existing settings (contact will then use defaults)
			super.delete(asc);
		}
		else {
			// Case 4- no existing settings, none to create, nothing to do
		}
	}
	
	@Override
	public AlligatorSettingsContact findAlligatorSettingsContact(Contact businessContact) {
		return this.ascDao.findAlligatorSettingsContact(businessContact);
	}

}
