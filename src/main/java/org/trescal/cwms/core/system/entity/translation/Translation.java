package org.trescal.cwms.core.system.entity.translation;

import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;


/*
 * Removed unique constraints 2015-06-18 as they were not combining together as expected.  Was:
 * @Table(uniqueConstraints= {@UniqueConstraint(columnNames = { "translation", "locale" })})
 */

@Embeddable
@Table
public class Translation implements Comparable<Translation> {
	
	private String translation;
	private Locale locale;
	
	public Translation() {}
	
	public Translation(Locale locale, String translation) {
		this.locale = locale;
		this.translation = translation;
	}
	
	@NotNull
	@Length(min = 1, max = 1000)
	//@Column(name = "translation", nullable = false, length = 1000)
	@Column(name = "translation", nullable = false, columnDefinition="nvarchar(1000)")
	public String getTranslation() {
		return translation;
	}
	@NotNull
	@Column(name = "locale", nullable = false)
	public Locale getLocale() {
		return locale;
	}
	public void setTranslation(String translation) {
		this.translation = translation;
	}
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	@Override
	public int compareTo(Translation arg0) {
		//logger.debug("Arg: " + arg0.getTranslation() + " - " + "Translation: " + translation + "\n");
		//int translationResult = arg0.getTranslation().compareTo(translation);
		//if (translationResult != 0) return translationResult;
		int localeResult = arg0.getLocale().toString().compareTo(this.locale.toString());
		if(localeResult != 0) return localeResult;
		int translationResult = arg0.getTranslation().compareTo(translation);
		return translationResult;
	}
	
	@Override
	public boolean equals(Object other) {
		return this == other || other != null && this.getClass() == other.getClass() && compareTo((Translation) other) == 0;
	}
	
	@Override
	public int hashCode() {
		int result = 0;
		if (locale != null) result = 31 * result + locale.hashCode();
		if (translation != null) result = 31 * result + translation.hashCode();
		return result;
	}
	
	@Override
	public String toString() {
		return "Translation [translation=" + translation + ", locale=" + locale
				+ "]";
	}
}