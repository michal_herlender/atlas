package org.trescal.cwms.core.system.entity.systemcomponent.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;

public interface SystemComponentDao extends BaseDao<SystemComponent, Integer> {
	
	SystemComponent findComponent(Component component);
	
	SystemComponent findComponent(String componentName);
	
	SystemComponent findEagerComponent(Component component);
	
	<Entity> Entity findEntity(Class<Entity> clazz, int id);
	
	List<Component> getComponentsUsingEmailTemplates();
}