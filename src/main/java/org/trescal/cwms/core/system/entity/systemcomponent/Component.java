package org.trescal.cwms.core.system.entity.systemcomponent;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.generaldelivery.GeneralDelivery;
import org.trescal.cwms.core.deliverynote.entity.jobdelivery.JobDelivery;
import org.trescal.cwms.core.documents.images.image.Image;
import org.trescal.cwms.core.hire.entity.hire.Hire;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.jobs.calibration.entity.standardsusageanalysis.StandardsUsageAnalysis;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.repair.repaircompletionreport.RepairCompletionReport;
import org.trescal.cwms.core.jobs.repair.repairinspectionreport.RepairInspectionReport;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.pricing.creditnote.entity.creditnote.CreditNote;
import org.trescal.cwms.core.pricing.invoice.entity.invoice.Invoice;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcosting.JobCosting;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest;
import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * {@link Enum} that defines all of the distinct system component types on the
 * system. This relates explicity to the {@link SystemComponent} entity on a
 * one-to-one basis, but allows us to rigidly define the various possible
 * components available to the system whilst the {@link SystemComponent} link
 * allows facets of these to be updatable (folder paths etc). This enum also
 * makes it much more reliable to query for {@link SystemComponent}s.
 *
 * @author Richard
 */
public enum Component {
    // SystemComponent maps to Component using the EnumeratedType Ordinal - it
    // is important therefore that the **ORDER OF THE ENUMS BELOW IS NOT
    // CHANGED**, any
    // new enums must be appended to the end of this list.
    JOB(Job.class),                                            // 0
    JOB_COSTING(JobCosting.class),
    QUOTATION(Quotation.class),
    THIRD_PARTY_QUOTATION(TPQuotation.class),
    THIRD_PARTY_QUOTATION_REQUEST(TPQuoteRequest.class),
    DELIVERY(JobDelivery.class),                            // 5
    RECALL(Recall.class),
    INSTRUMENT(Instrument.class),
    INSTRUMENTMODEL(InstrumentModel.class),
    MFR(Mfr.class),
    COMPANY(Company.class),                                    // 10
    PROCEDURE(Capability.class),
    @Deprecated PRICELIST(Object.class),
    USERS(User.class),
    EMAIL(Email.class),
    PURCHASEORDER(PurchaseOrder.class),                        // 15
    IMAGE(Image.class),
    GENERAL_DELIVERY(GeneralDelivery.class),
    INVOICE(Invoice.class),
    CERTIFICATE(Certificate.class),
    CLIENT_CERTIFICATE(Certificate.class),                    // 20
    QUOTATION_REQUEST(QuotationRequest.class),
    CREDIT_NOTE(CreditNote.class),
    HIRE(Hire.class),
    SUBDIV(Subdiv.class),
    FAILURE_REPORT(FaultReport.class),                        // 25
    REPAIR_INSPECTION_REPORT(RepairInspectionReport.class),
    REPAIR_COMPLETION_REPORT(RepairCompletionReport.class),
    GENERAL_SERVICE_OPERATION(GeneralServiceOperation.class),
    STANDARDS_USAGE_ANALYSIS(StandardsUsageAnalysis.class);

    private Class<? extends Object> clazz;

    Component(Class<? extends Object> clazz) {
        this.clazz = clazz;
    }

    public Class<? extends Object> getClazz() {
        return this.clazz;
    }

    public static Optional<Component> componentForClazz(Class<?> clazz) {
        return Stream.of(Component.values()).filter(c -> c.clazz.equals(clazz)).findFirst();
    }
}
