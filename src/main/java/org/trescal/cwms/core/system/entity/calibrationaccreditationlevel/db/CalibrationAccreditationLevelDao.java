package org.trescal.cwms.core.system.entity.calibrationaccreditationlevel.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.calibrationaccreditationlevel.CalibrationAccreditationLevel;

public interface CalibrationAccreditationLevelDao extends BaseDao<CalibrationAccreditationLevel, Integer> {}