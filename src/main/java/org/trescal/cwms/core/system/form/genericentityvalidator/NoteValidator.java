package org.trescal.cwms.core.system.form.genericentityvalidator;

import org.springframework.validation.Errors;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

public class NoteValidator extends AbstractBeanValidator
{

	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(Note.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		Note note = (Note) target;

		super.validate(note, errors);

		// check contact is not null
		if (note.getSetBy() == null)
		{
			errors.rejectValue("setBy", null, "The requested contact could not be found");
		}

	}
}
