package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class StandardLabelCertificateNumber extends SystemDefaultBooleanType {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.STANDARD_LABEL_CERTIFICATE_NUMBER;
	}

}
