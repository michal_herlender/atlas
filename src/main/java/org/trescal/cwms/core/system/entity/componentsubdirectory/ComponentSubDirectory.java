package org.trescal.cwms.core.system.entity.componentsubdirectory;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.system.entity.systemcomponent.SystemComponent;

@Entity
@Table (name="systemsubdirectory")
public class ComponentSubDirectory 
{
	private int dirId;
	private String dirName;
	
	private SystemComponent component;
	
	public ComponentSubDirectory(){}
	
	public ComponentSubDirectory(int dirId, String dirName, SystemComponent component) {
		super();
		this.dirId = dirId;
		this.dirName = dirName;
		this.component = component;
	}

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column (name="dirid", nullable=false, unique = true)
	@Type (type = "int")
	public int getDirId() {
		return dirId;
	}
	
	public void setDirId(int dirId) {
		this.dirId = dirId;
	}
	
	@Column (name="dirname", nullable=false, length=50)
	public String getDirName() {
		return dirName;
	}

	public void setDirName(String dirName) {
		this.dirName = dirName;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="componentid", nullable=false)
	public SystemComponent getComponent() {
		return component;
	}
	
	public void setComponent(SystemComponent component) {
		this.component = component;
	}
}
