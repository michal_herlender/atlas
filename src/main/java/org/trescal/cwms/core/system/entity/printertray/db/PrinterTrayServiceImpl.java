package org.trescal.cwms.core.system.entity.printertray.db;

import java.util.List;

import javax.print.attribute.standard.MediaTray;

import org.trescal.cwms.core.system.entity.papertype.PaperType;
import org.trescal.cwms.core.system.entity.printer.db.PrinterService;
import org.trescal.cwms.core.system.entity.printertray.PrinterTray;

public class PrinterTrayServiceImpl implements PrinterTrayService
{
	PrinterService printerServ;
	PrinterTrayDao printerTrayDao;

	public void deletePrinterTray(PrinterTray printertray)
	{
		this.printerTrayDao.remove(printertray);
	}

	public PrinterTray findPrinterTray(int id)
	{
		return this.printerTrayDao.find(id);
	}

	public PrinterTray findPrinterTrayForPaperType(int printerId, PaperType paperType)
	{
		return this.printerTrayDao.findPrinterTrayForPaperType(printerId, paperType);
	}

	public List<PrinterTray> getAllPrinterTrays()
	{
		return this.printerTrayDao.findAll();
	}

	public MediaTray getMediaTrayForPaperType(int printerId, PaperType paperType)
	{
		PrinterTray tray = this.findPrinterTrayForPaperType(printerId, paperType);

		return ((tray == null) || (tray.getMediaTrayRef() == null)) ? null : tray.getMediaTrayRef().getMediaTray();
	}

	public Integer getPaperSourceForPaperType(int printerId, PaperType paperType)
	{
		PrinterTray tray = this.findPrinterTrayForPaperType(printerId, paperType);

		return (tray == null) ? null : tray.getPaperSource();
	}

	public void insertPrinterTray(PrinterTray PrinterTray)
	{
		this.printerTrayDao.persist(PrinterTray);
	}

	public void saveOrUpdatePrinterTray(PrinterTray printertray)
	{
		this.printerTrayDao.saveOrUpdate(printertray);
	}

	public void setPrinterServ(PrinterService printerServ)
	{
		this.printerServ = printerServ;
	}

	public void setPrinterTrayDao(PrinterTrayDao printerTrayDao)
	{
		this.printerTrayDao = printerTrayDao;
	}

	public void updatePrinterTray(PrinterTray PrinterTray)
	{
		this.printerTrayDao.update(PrinterTray);
	}
}