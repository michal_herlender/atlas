package org.trescal.cwms.core.system;

import org.trescal.cwms.core.system.entity.papertype.PaperType;
import org.trescal.cwms.core.system.entity.status.Status;
import org.trescal.cwms.core.system.entity.supportedcurrency.MultiCurrencySupport;

import javax.servlet.http.HttpSession;

/**
 * Class containing system-wide constant values.
 * 
 * @author richard
 */
public class Constants
{
	/**
	 * Values for keys in CalReq map
	 */
	public static final String CALREQ_MAP_COMPANYMODEL = "companymodel";

	public static final String CALREQ_MAP_COMPANYDESCRIPTION = "companydescription";

	public static final String CALREQ_MAP_INSTRUMENT = "instrument";

	public static final String CALREQ_MAP_JOBITEM = "jobitem";
	public static final String CALREQ_MAP_MODEL = "model";
	public static final String COMPANY = "Company";
	public static final String COMPANY_DEFAULT_CURRENCY = "companydefaultcurrency";
	public static final String CONTACT = "Contact";

	public static final String CURRENT_SUBDIV = "currentSubdiv"; 
	/**
	 * Values for keys in Days Map
	 */
	public static final String DAYS_MAP_1_DAYS = "workfordepttype.1day";
	public static final String DAYS_MAP_2_DAYS = "workfordepttype.2days";

	public static final String DAYS_MAP_3_DAYS = "workfordepttype.3days";

	public static final String DAYS_MAP_4_DAYS = "workfordepttype.4days";
	public static final String DAYS_MAP_5_DAYS = "workfordepttype.5days";
	public static final String DAYS_MAP_ALL = "all";
	public static final String DAYS_MAP_BUSINESS = "workfordepttype.inhouse";
	public static final String DAYS_MAP_OVERDUE = "activejobs.OVERDUE";
	public static final String DEFAULT_CHECKSHEET = "Dims Worksheet Generic.vm";
	public static final String DEFAULT_LIMIT_CHART = "img/chartplaceholder.png";
	public static final PaperType DEFAULT_PAPER_TYPE = PaperType.A4_PLAIN;

	/**
	 * document tool used in documents
	 */
	public static final String DOCUMENTTOOL = "documenttool";

	public static final String EMAIL_REPLACE_FIRSTNAME = "_REPLACE_FIRSTNAME_";
	public static final String EMAIL_REPLACE_IDENTIFIER = "_REPLACE_IDENTIFIER_";
	public static final String EMAIL_REPLACE_LASTNAME = "_REPLACE_LASTNAME_";
	public static final String EMAIL_REPLACE_SENDER_FIRSTNAME = "_REPLACE_SENDER_FIRSTNAME_";
	public static final String EMAIL_REPLACE_SENDER_LASTNAME = "_REPLACE_SENDER_LASTNAME_";
	public static final String EMAIL_REPLACE_SENDER_POSITION = "_REPLACE_SENDER_POSITION_";
	public static final String EMAIL_REPLACE_SENDER_TELEPHONE ="_REPLACE_SENDER_TELEPHONE_";
	public static final String EMAIL_REPLACE_SENDER_FAX ="_REPLACE_SENDER_FAX_";
	public static final String EMAIL_REPLACE_SENDER_MOBILE ="_REPLACE_SENDER_MOBILE_";
	public static final String EMAIL_REPLACE_SENDER_ADDRESS ="_REPLACE_SENDER_ADDRESS_";
	public static final String EMAIL_REPLACE_SENDER_EMAIL="_REPLACE_SENDER_EMAIL_";
	public static final String EMAIL_REPLACE_CONTACT_TITLE="_REPLACE_TITLE_";
	public static final String EXCEL_ESCAPE_TOKEN = "_EXT_";

	public static final String FAST_TRACK_TURN = "fastTrackTurn";
	public static final String FAX_PREFIX = "[fax:";

	public static final int FORM_CANCEL = 2;
	public static final int FORM_COMPLETE = 0;
	public static final int FORM_RESUME = 1;

	public static final int FUTURE_ALLOCATION_DAYS = 28;

	/**
	 * Key for {@link HttpSession} attribute containing a  of
	 * recently created file names.
	 */
	public static final String HTTPSESS_NEW_FILE_LIST = "newFileList";

	/**
	 * Key for a {@link HttpSession} attribute containing a
	 * {@link MultiCurrencySupport} class.
	 */
	public static final String HTTPSESS_PRICING_CLASS_NAME = "pricingClass";

	/**
	 * Key for {@link HttpSession} attribute containing a {@link Status} class
	 * name.
	 */
	public static final String HTTPSESS_STATUS_CLASS_NAME = "statusClass";
	public static final int IMAGE_LARGE_SIZE = 500;

	public static final int IMAGE_MEDIUM_SIZE = 250;
	public static final int IMAGE_THUMB_SIZE = 75;

	public static final String MEDIATYPE_APPLICATION_JSON_UTF8 = "application/json;charset=UTF-8"; 
	public static final String MEDIATYPE_TEXT_JSON_UTF8 = "text/json;charset=UTF-8";
	public static final String MEDIATYPE_APPLICATION_XML_UTF8 = "application/xml;charset=UTF-8"; 
	public static final String MEDIATYPE_TEXT_XML_UTF8 = "text/xml;charset=UTF-8";
	
	public static final String NUMBERTOOL = "numbertool";

	public static final String PDF_OWNER_PASSWORD = "Secur3d4ntechD0cument19!88";
	/**
	 * If this is changed, the variable name must be updated on all VM pages as
	 * this is the name of a variable used in the reference data
	 */
	public static final String REFDATA_SC_ROOT_FILES = "scRootFiles";
	public static final String REFDATA_FILES_NAME = "filesName";
	public static final String REFDATA_SYSTEM_COMPONENT = "sc";

	public static final int RESULTS_PER_PAGE = 25;

	public static final int SALES_REPORT_DEFAULT_YEARS = 5;
	/*
	 * Session Attributes
	 */

	public static final String SESSION_ATTRIBUTE_AVAILABLE_SUBDIVS = "availableSubdivs";
	public static final String SESSION_ATTRIBUTE_BUSINESS_DETAILS = "businessDetails";
	public static final String SESSION_ATTRIBUTE_CONNECTION_TYPE = "connectionType";
	public static final String SESSION_ATTRIBUTE_CONTACT = "currentContact";
	public static final String SESSION_ATTRIBUTE_COMPANY = "allocatedCompany";        // Discuss rename?
	public static final String SESSION_ATTRIBUTE_DEFAULT_LOCALE = "defaultlocale";
    public static final String SESSION_ATTRIBUTE_LOCALE = "cwmsSessionLocale";
    public static final String SESSION_ATTRIBUTE_TIME_ZONE = "cwmsSessionTimeZone";
    public static final String SESSION_ATTRIBUTE_SUBDIV = "allocatedSubdiv";        // Discuss rename?
    public static final String SESSION_ATTRIBUTE_SYSTEM_NAME = "systemName";
    public static final String SESSION_ATTRIBUTE_USERNAME = "username";
    public static final String SESSION_ATTRIBUTE_USER_READ_RULE = "userReadRule";
    public static final String SESSION_ATTRIBUTE_USER_UPDATE_RULE = "userUpdateRule";
    public static final String SESSION_ATTRIBUTE_USER_CERTVAL_RULE = "userCertificateValidationRule";
    public static final String SESSION_ATTRIBUTE_USER_ADD_INST_RULE = "userAddInstRule";
    public static final String SESSION_ATTRIBUTE_USER_ADD_CAL_RULE = "userAddCalRule";
    public static final String SESSION_ATTRIBUTE_MESSAGES = "messageSource";
    public static final String SESSION_ATTRIBUTE_VERSION = "version";
    public static final String SESSION_ATTRIBUTE_SYSTEM_TYPE = "systemType";

    public static final String SQLSERVER = "sqlserver";
    public static final String STRINGTOOL = "stringtool";

    public static final String SUBDIV = "Subdiv";

    public static final String SYMBOLTOOL = "sym";
    public static final String SYSTEM_AUTHENTICATION_ERROR_MESSAGE = "system_authentication_error_message";

	public static final String USER_PERMISSION_ADDINST = "Add Instrument";
	public static final String USER_PERMISSION_READ = "View Level";
	public static final String USER_PERMISSION_UPDATE = "Update Instrument";
	public static final String USER_PERMISSION_CERTVAL = "Certificate Validation";
	public static final String VERSION_TEXT = " version ";
	public static final String WEB_LOG_FILEPATH = "logPath";
	public static final String DOCUMENT_LOGO = "classpath:logo.png";

	/*
	 * File naming constants
	 */
	public static final String FILE_NAMING_VERSION = "vers";
	public static final String FILE_NAMING_REVISION = "rev";

	public static final String LOGING_TIMEZONE_PARAM = "timezone";
}