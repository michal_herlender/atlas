package org.trescal.cwms.core.system.entity.presetcomment.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;

@Service("PresetCommentService")
public class PresetCommentServiceImpl extends BaseServiceImpl<PresetComment, Integer> implements PresetCommentService {

	@Autowired
	private PresetCommentDao presetCommentDao;

	@Override
	protected BaseDao<PresetComment, Integer> getBaseDao() {
		return presetCommentDao;
	}

	@Override
	public List<PresetComment> getAll(PresetCommentType type) {
		return presetCommentDao.getAll(type);
	}

	@Override
	public List<PresetComment> getAll(PresetCommentType type, Subdiv allocatedSubdiv) {
		return presetCommentDao.getAll(type, allocatedSubdiv);
	}
}