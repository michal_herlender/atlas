package org.trescal.cwms.core.system.entity.deletedcomponent.db;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.deletedcomponent.DeletedComponent;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

public interface DeletedComponentDao extends BaseDao<DeletedComponent, Integer> {
	
	List<Component> getDeletableComponents();
	
	List<DeletedComponent> searchDeletedComponents(Component component, String refNo, Date dateFrom);
}