package org.trescal.cwms.core.system.entity.template;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.system.entity.componentdoctypetemplate.ComponentDoctypeTemplate;

@Entity
@Table(name = "template")
public class Template
{
	private Integer templateId;
	private String name;
	private String templatePath;
	private String description;

	private Set<ComponentDoctypeTemplate> componentDoctypeTemplates;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "template")
	public Set<ComponentDoctypeTemplate> getComponentDoctypeTemplates()
	{
		return this.componentDoctypeTemplates;
	}

	@Column(name = "description", length = 200)
	public String getDescription()
	{
		return this.description;
	}

	@Column(name = "name", nullable = false, unique = true, length = 50)
	public String getName()
	{
		return this.name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "templateid", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getTemplateId()
	{
		return this.templateId;
	}

	@Column(name = "templatepath", nullable = false, unique = true, length = 100)
	public String getTemplatePath()
	{
		return this.templatePath;
	}

	public void setComponentDoctypeTemplates(Set<ComponentDoctypeTemplate> componentDoctypeTemplates)
	{
		this.componentDoctypeTemplates = componentDoctypeTemplates;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setTemplateId(Integer id)
	{
		this.templateId = id;
	}

	public void setTemplatePath(String templatePath)
	{
		this.templatePath = templatePath;
	}
}
