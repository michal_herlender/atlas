package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import java.util.ArrayList;
import java.util.List;

public abstract class SystemDefaultRangeType extends SystemDefaultType<Integer>
{
	abstract protected Integer getMin();
	
	abstract protected Integer getMax();
	
	@Override
	public boolean isDiscreteType() {
		return true;
	}
	
	@Override
	public boolean isMultiChoice() {
		return true;
	}
	
	@Override
	public List<String> getValues() {
		List<String> values = new ArrayList<String>();
		for(Integer i=getMin(); i<=getMax(); i++) values.add(i.toString());
		return values;
	}
	
	@Override
	public Integer parseValue(String value) {
		Integer i = value == null ? null : Integer.parseInt(value);
		return getMin() <= i && i <= getMax() ? i : null;
	}
}