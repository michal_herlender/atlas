package org.trescal.cwms.core.system.entity.note.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NoteForInstrumentTooltipDto {

    private String label;
    private String note;
}
