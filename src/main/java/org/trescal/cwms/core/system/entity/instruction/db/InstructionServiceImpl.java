package org.trescal.cwms.core.system.entity.instruction.db;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.companyinstructionlink.CompanyInstructionLink;
import org.trescal.cwms.core.company.entity.companyinstructionlink.db.CompanyInstructionLinkService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.contactinstructionlink.ContactInstructionLink;
import org.trescal.cwms.core.company.entity.contactinstructionlink.db.ContactInstructionLinkService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.SubdivInstructionLink;
import org.trescal.cwms.core.company.entity.subdivinstructionlink.db.SubdivInstructionLinkService;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.db.ContractService;
import org.trescal.cwms.core.deliverynote.entity.delivery.DeliveryType;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.system.dto.InstructionDTO;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructionlink.InstructionLink;
import org.trescal.cwms.core.system.entity.instructionlink.db.InstructionLinkService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Service
public class InstructionServiceImpl extends BaseServiceImpl<Instruction, Integer> implements InstructionService {

	@Autowired
	private InstructionDao instructionDao;
	@Autowired
	private CompanyInstructionLinkService companyInstructionLinkService;
	@Autowired
	private SubdivInstructionLinkService subdivInstructionLinkService;
	@Autowired
	private ContactInstructionLinkService contactInstructionLinkService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private ContractService contractService;
	@Autowired
	private InstructionLinkService instructionLinkService;
	@Autowired
	private JobService jobService;

	@Override
	protected BaseDao<Instruction, Integer> getBaseDao() {
		return instructionDao;
	}

	@Override
	public List<InstructionDTO> findAllByJob(Collection<Integer> jobIds, Collection<Integer> jobItemIds,
											 Integer allocatedSubdivId, InstructionType... instructionTypes) {
		List<InstructionDTO> instructions = new ArrayList<>();
		instructions.addAll(instructionDao.findAllLinkedToContractByItems(jobItemIds, instructionTypes));
		instructions.addAll(instructionDao.findAllLinkedToJobByItems(jobItemIds, instructionTypes));
		instructions.addAll(instructionDao.findAllLinkedToContactByJobs(jobIds, instructionTypes));
		instructions.addAll(instructionDao.findAllLinkedToSubdivByJobs(jobIds, instructionTypes));
		instructions.addAll(instructionDao.findAllLinkedToCompanyByJobs(jobIds, instructionTypes));
		return instructions;
	}

	@Override
	public List<InstructionDTO> findAllByJobItems(Collection<Integer> jobItemIds, Integer allocatedSubdivId,
												  InstructionType... instructionTypes) {
		List<InstructionDTO> instructions = new ArrayList<>();
		instructions.addAll(instructionDao.findAllLinkedToContractByItems(jobItemIds, instructionTypes));
		instructions.addAll(instructionDao.findAllLinkedToJobByItems(jobItemIds, instructionTypes));
		instructions.addAll(instructionDao.findAllLinkedToContactByItems(jobItemIds, instructionTypes));
		instructions.addAll(instructionDao.findAllLinkedToSubdivByItems(jobItemIds, instructionTypes));
		instructions.addAll(instructionDao.findAllLinkedToCompanyByItems(jobItemIds, instructionTypes));
		return instructions;
	}

	@Override
	public List<InstructionDTO> ajaxInstructions(Integer allocatedSubdivid, List<Integer> linkCoids, List<Integer> linkSubdivids, List<Integer> linkContactids,
												 Integer linkContractid, Integer linkJobid, List<Integer> jobIds, List<Integer> jobItemIds, InstructionType[] instructionTypes,
												 DeliveryType deliveryType) {

		List<InstructionDTO> result = new ArrayList<>();
		Subdiv allocatedSubdiv = this.subdivService.get(allocatedSubdivid);
		Company allocatedCompany = allocatedSubdiv.getComp();

		// Instructions linked to the JOB
		if(linkJobid != null) {
			Job job = this.jobService.get(linkJobid);
			List<InstructionLink<?>> jobInstructions = this.instructionLinkService.getAllForJob(job);
			List<InstructionType> types = Arrays.asList(instructionTypes);
			for (InstructionLink<?> instructionLink : jobInstructions) {
				if(types.contains(instructionLink.getInstruction().getInstructiontype()) && addToInstructionsList(instructionLink.getInstruction().getInstructiontype(), deliveryType, instructionLink.getInstruction().getIncludeOnDelNote(), instructionLink.getInstruction().getIncludeOnSupplierDelNote()))
					result.add(new InstructionDTO(instructionLink));
			}

		}

		// Instructions linked to the COMPANY and COMPANY_CONTRACTS
		if(CollectionUtils.isNotEmpty(linkCoids)) {
			linkCoids.forEach(linkCoid -> {
						Company company = this.companyService.get(linkCoid);
						if(company != null) {
							List<CompanyInstructionLink> filteredCompnayInstructions = this.companyInstructionLinkService.getForCompanyAndTypes(company, allocatedSubdiv, instructionTypes);
							filteredCompnayInstructions.forEach(i -> {
								if(addToInstructionsList(i.getInstruction().getInstructiontype(), deliveryType, i.getInstruction().getIncludeOnDelNote(), i.getInstruction().getIncludeOnSupplierDelNote()))
									result.add(new InstructionDTO(i));
							});
						}
					}
			);
		}

		// Instructions linked to the SUBDIV
		if(CollectionUtils.isNotEmpty(linkSubdivids)) {
			linkSubdivids.forEach(linkSubdivid -> {
				Subdiv subdiv = this.subdivService.get(linkSubdivid);
				if(subdiv != null) {
					List<SubdivInstructionLink> filteredSubdivInstructions = this.subdivInstructionLinkService.getForSubdivAndTypes(subdiv, allocatedSubdiv, instructionTypes);
					filteredSubdivInstructions.forEach(i -> {
						if(addToInstructionsList(i.getInstruction().getInstructiontype(), deliveryType, i.getInstruction().getIncludeOnDelNote(), i.getInstruction().getIncludeOnSupplierDelNote()))
							result.add(new InstructionDTO(i));
					});
				}
			});
		}

		// Instructions linked to the CONTACT
		if(CollectionUtils.isNotEmpty(linkContactids)) {
			linkContactids.forEach(linkContactid -> {
				Contact contact = this.contactService.get(linkContactid);
				if(contact != null) {
					List<ContactInstructionLink> filteredContactInstructions = this.contactInstructionLinkService.getForContactAndTypes(contact, allocatedCompany, instructionTypes);
					filteredContactInstructions.forEach(i -> {
						if(addToInstructionsList(i.getInstruction().getInstructiontype(), deliveryType, i.getInstruction().getIncludeOnDelNote(), i.getInstruction().getIncludeOnSupplierDelNote()))
							result.add(new InstructionDTO(i));
					});
				}
			});
		}

		// Instructions linked to the CONTRACT
		if(linkContractid != null) {
			Contract contract = this.contractService.get(linkContractid);
			if(contract != null) {
				List<InstructionType> types = Arrays.asList(instructionTypes);
				contract.getInstructions().forEach(i -> {
					if(types.contains(i.getInstructiontype()) && addToInstructionsList(i.getInstructiontype(), deliveryType, i.getIncludeOnDelNote(), i.getIncludeOnSupplierDelNote()))
						result.add(new InstructionDTO(i));
				});
			}
		}

		// get instructions for contract case
		if(CollectionUtils.isNotEmpty(jobItemIds) && CollectionUtils.isEmpty(jobIds)){
			List<InstructionDTO> jobitemsInstructions = instructionDao.findAllLinkedToContractByItems(jobItemIds, instructionTypes != null ? instructionTypes:InstructionType.values());
			for (InstructionDTO instructionDTO : jobitemsInstructions) {
				if(addToInstructionsList(instructionDTO.getType(), deliveryType, instructionDTO.getIncludeOnDelNote(), instructionDTO.getIncludeOnSupplierDelNote()))
					result.add(instructionDTO);
			}
		}

		if(CollectionUtils.isNotEmpty(jobItemIds) && CollectionUtils.isNotEmpty(jobIds)){
			List<InstructionDTO> jobInstructions = this.findAllByJob(jobIds, jobItemIds,
					allocatedSubdivid, instructionTypes);
			for (InstructionDTO instructionDTO : jobInstructions) {
				if(addToInstructionsList(instructionDTO.getType(), deliveryType, instructionDTO.getIncludeOnDelNote(), instructionDTO.getIncludeOnSupplierDelNote()))
					result.add(instructionDTO);
			}
		}

		return result;
	}

	private boolean addToInstructionsList(InstructionType instructionType, DeliveryType deliveryType, Boolean includeOnDelNote, Boolean includeOnSupplierDelNote) {
		return !instructionType.equals(InstructionType.CARRIAGE) || deliveryType == null
				|| (includeOnDelNote == null && includeOnSupplierDelNote == null)
				|| (includeOnDelNote == null && BooleanUtils.isFalse(includeOnSupplierDelNote))
				|| (BooleanUtils.isFalse(includeOnDelNote) && includeOnSupplierDelNote == null)
				|| (BooleanUtils.isFalse(includeOnDelNote) && BooleanUtils.isFalse(includeOnSupplierDelNote))
				|| (deliveryType.equals(DeliveryType.CLIENT) && BooleanUtils.isTrue(includeOnDelNote))
				|| (!deliveryType.equals(DeliveryType.CLIENT) && BooleanUtils.isTrue(includeOnSupplierDelNote));
	}

}