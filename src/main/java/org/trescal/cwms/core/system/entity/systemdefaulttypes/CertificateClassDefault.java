package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class CertificateClassDefault extends SystemDefaultBooleanType {

    @Override
    public SystemDefaultNames defaultName() {
        return SystemDefaultNames.CERTIFICATE_CLASS;
    }
}
