package org.trescal.cwms.core.system.entity.servicetype;

import lombok.Getter;

/**
 * Query result DTO containing id, short name, long name translation
 * as well as default fallback values if no translation populated
 *
 */
@Getter
public class ServiceTypeDto {
	private Integer id;
	private String shortNameDefault;
	private String longNameDefault;
	private String shortNameTranslation;
	private String longNameTranslation;
	private String displayColour;
	private String displayColourFastTrack;

	public ServiceTypeDto(Integer id, String shortNameDefault, String longNameDefault, 
			String shortNameTranslation, String longNameTranslation, String displayColour,
			String displayColourFastTrack) {
		super();
		this.id = id;
		this.shortNameDefault = shortNameDefault;
		this.longNameDefault = longNameDefault;
		this.shortNameTranslation = shortNameTranslation;
		this.longNameTranslation = longNameTranslation;
		this.displayColour = displayColour;
		this.displayColourFastTrack = displayColourFastTrack;
	}

	/**
	 * Can be used directly in selection lists 
	 */
	public String getShortName() {
		return shortNameTranslation != null ? shortNameTranslation : shortNameDefault;
	}
	/**
	 * Can be used directly in selection lists 
	 */
	public String getLongName() {
		return longNameTranslation != null ? longNameTranslation : longNameDefault;
	}
	/**
	 * Can be used directly in selection lists 
	 */
	public String getBothNames() {
		StringBuffer result = new StringBuffer();
		result.append(getShortName());
		result.append(" - ");
		result.append(getLongName());
		return result.toString();
	}

}
