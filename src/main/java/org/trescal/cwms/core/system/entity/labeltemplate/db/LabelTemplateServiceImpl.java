package org.trescal.cwms.core.system.entity.labeltemplate.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.system.entity.labeltemplate.LabelTemplate;
import org.trescal.cwms.core.system.entity.labeltemplate.LabelTemplateType;

@Service("LabelTemplateService")
public class LabelTemplateServiceImpl extends BaseServiceImpl<LabelTemplate, Integer> implements LabelTemplateService {
	@Autowired
	private LabelTemplateDao labelTemplateDao;
	
	@Override
	public LabelTemplate getLabelTemplateById(int id) {
		return this.labelTemplateDao.getLabelTemplateById(id);
	}

	@Override
	public LabelTemplate getLabelTemplateByLabelTemplateType(
			LabelTemplateType labeltemplatetype) {
		return this.labelTemplateDao.getLabelTemplateByLabelTemplateType(labeltemplatetype);
	}

	public LabelTemplateDao getLabelTemplateDao() {
		return labelTemplateDao;
	}

	public void setLabelTemplateDao(LabelTemplateDao labelTemplateDao) {
		this.labelTemplateDao = labelTemplateDao;
	}

	@Override
	protected BaseDao<LabelTemplate, Integer> getBaseDao() {
		return this.labelTemplateDao;
	}
}
