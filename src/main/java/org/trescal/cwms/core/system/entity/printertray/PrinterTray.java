package org.trescal.cwms.core.system.entity.printertray;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.system.entity.papertype.PaperType;
import org.trescal.cwms.core.system.entity.printer.Printer;

/**
 * @author jamiev
 */
@Entity
@Table(name = "printertray", uniqueConstraints = { @UniqueConstraint(columnNames = { "printerid", "trayno" }) })
public class PrinterTray
{
	private int id;
	private MediaTrayRef mediaTrayRef;
	private int paperSource;
	private PaperType paperType;
	private Printer printer;
	private int trayNo;

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "mediatrayref", nullable = false)
	public MediaTrayRef getMediaTrayRef()
	{
		return this.mediaTrayRef;
	}

	@Column(name = "papersource", nullable = false)
	@Type(type = "int")
	public int getPaperSource()
	{
		return this.paperSource;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "papertype", nullable = false)
	public PaperType getPaperType()
	{
		return this.paperType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "printerid")
	public Printer getPrinter()
	{
		return this.printer;
	}

	@Column(name = "trayno", nullable = false)
	@Type(type = "int")
	public int getTrayNo()
	{
		return this.trayNo;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setMediaTrayRef(MediaTrayRef mediaTrayRef)
	{
		this.mediaTrayRef = mediaTrayRef;
	}

	public void setPaperSource(int paperSource)
	{
		this.paperSource = paperSource;
	}

	public void setPaperType(PaperType paperType)
	{
		this.paperType = paperType;
	}

	public void setPrinter(Printer printer)
	{
		this.printer = printer;
	}

	public void setTrayNo(int trayNo)
	{
		this.trayNo = trayNo;
	}
}
