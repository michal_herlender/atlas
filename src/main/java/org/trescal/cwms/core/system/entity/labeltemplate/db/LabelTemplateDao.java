package org.trescal.cwms.core.system.entity.labeltemplate.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.labeltemplate.LabelTemplate;
import org.trescal.cwms.core.system.entity.labeltemplate.LabelTemplateType;

public interface LabelTemplateDao extends BaseDao<LabelTemplate, Integer> {
	LabelTemplate getLabelTemplateById(int id);
	LabelTemplate getLabelTemplateByLabelTemplateType(LabelTemplateType labeltemplatetype);
}
