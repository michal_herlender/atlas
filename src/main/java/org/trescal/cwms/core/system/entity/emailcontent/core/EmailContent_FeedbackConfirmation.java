package org.trescal.cwms.core.system.entity.emailcontent.core;

import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;

/**
 * Extracted from AddFeedbackController so we can preview/test content
 * TODO : Once templates done, we can remove the velocity part 
 * @author galen
 *
 */
@Component
public class EmailContent_FeedbackConfirmation {
	@Autowired
	private BusinessDetailsService bdServ;
	@Autowired
	private MessageSource messages; 
	@Autowired
	private TemplateEngine templateEngine;
	
	public static enum EmailType {
		CLIENT, INTERNAL;
	}
	
	public static final String THYMELEAF_VIEW_INTERNAL = "/email/core/feedbackconfirmation.html";
	
	public static final String THYMELEAF_VIEW_CLIENT = "/email/portal/feedback.html";

	public EmailContentDto getContent(Contact contact, String description, String title, String build, String requestURI, 
			String userAgent, Locale locale, EmailType emailType) {
		String body = getBodyThymeleaf(contact, description, title, build, requestURI, userAgent, locale, emailType);
		String subject = getSubject(contact, title, locale, emailType);
		return new EmailContentDto(body, subject);
	}

	private String getBodyThymeleaf(Contact contact, String description, String title, String build, String requestURI, 
			String userAgent, Locale locale, EmailType emailType) {
		BusinessDetails bdetails = this.bdServ.getAllBusinessDetails(contact, null, null);
		
		Context context = new Context(locale);
		context.setVariable("description", description);
		context.setVariable("title", title);
		context.setVariable("contact", contact);
		context.setVariable("date", new Date());
		context.setVariable("build", build);
		context.setVariable("requesturl", requestURI);
		context.setVariable("useragent", userAgent);
		context.setVariable(Constants.SESSION_ATTRIBUTE_BUSINESS_DETAILS, bdetails);
		
		String viewName = (emailType == EmailType.INTERNAL) ? THYMELEAF_VIEW_INTERNAL : THYMELEAF_VIEW_CLIENT;
		String body = this.templateEngine.process(viewName, context);
		return body;
	}

	private String getSubject(Contact contact, String title, Locale locale, EmailType emailType) {
		String subject = null;
		switch(emailType) {
		case CLIENT:
			subject = this.messages.getMessage("email.feedback.subject", new Object[] {contact.getName(), contact.getSub().getComp().getConame()}, "Website Feedback Supplied by " + contact.getName()
			+ " at " + contact.getSub().getComp().getConame(), locale);
			break;
		case INTERNAL:
			subject = this.messages.getMessage("email.feedbackconfirmation.subject", 
					new Object[] {title,contact.getName()}, 
					"ERP Feedback : " + title + " : " + contact.getName(), locale);
			break;
		}
		
		return subject;
	}

}
