package org.trescal.cwms.core.system.entity.rate.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.rate.Rate;

public interface RateDao extends BaseDao<Rate, Integer> {}