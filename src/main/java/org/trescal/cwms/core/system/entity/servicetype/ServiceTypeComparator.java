package org.trescal.cwms.core.system.entity.servicetype;

import java.util.Comparator;

/**
 * Comparator for service types that uses the defined order
 * (order field in database) to compare service types  
 * @author galen
 *
 */
public class ServiceTypeComparator implements Comparator<ServiceType> {

	@Override
	public int compare(ServiceType st0, ServiceType st1) {
		int result = st0.getOrder().compareTo(st1.getOrder());
		if (result == 0) {
			result = st0.getServiceTypeId() - st1.getServiceTypeId();
		}
		return result;
	}

}
