package org.trescal.cwms.core.system.entity.emailcontent.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.BusinessDetails;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;

import java.time.LocalDate;
import java.util.Locale;

@Component
public class EmailContent_UnsignedCertificateNotification {
	@Autowired
	private BusinessDetailsService bdServ;
	@Autowired
	private MessageSource messages;
	@Autowired
	private TemplateEngine templateEngine;

	@Value("${cwms.deployed.url}")
	private String deployedUrl;
	
	public static final String THYMELEAF_VIEW = "/email/core/unsignedcertificatenotification.html";

	public EmailContentDto getContent(Certificate c, boolean fileExists, Locale locale) {
		BusinessDetails bdetails = this.bdServ.getAllBusinessDetails(c.getCal().getOrganisation());
		
		String body = getBodyThymeleaf(c, fileExists, bdetails, locale);
		String subject = getSubject(c, fileExists, locale);
		return new EmailContentDto(body, subject);
	}

	private String getBodyThymeleaf(Certificate c, boolean fileExists, BusinessDetails bdetails, Locale locale) {
		// find job contact
		Contact jobContact = c.getLinks().iterator().next().getJobItem().getJob().getCon();
		// the path of the job
		String jiCertPath = "";
		// cert has links?
		if ((c.getLinks() != null) && (c.getLinks().size() > 0))
		{
			jiCertPath = this.deployedUrl
					+ "jicertificates.htm?jobitemid="
				+ c.getLinks().iterator().next().getJobItem().getJobItemId();
		}
		// find engineer
		Contact engineer = c.getCal().getCompletedBy();

		Context context = new Context(locale);
		context.setVariable("cert", c);
		context.setVariable("jobcontact", jobContact);
		context.setVariable("engineer", engineer);
		context.setVariable(Constants.SESSION_ATTRIBUTE_BUSINESS_DETAILS, bdetails);
		context.setVariable("date", LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		context.setVariable("jiCertPath", jiCertPath);
		context.setVariable("fileExists", fileExists);

		String body = this.templateEngine.process(THYMELEAF_VIEW, context);
		return body;
	}


	private String getSubject(Certificate c, boolean fileExists, Locale locale) {
		String subject = fileExists 
				? this.messages.getMessage("email.unsignedcert.subject1", 
						new Object[] {c.getCertno()}, "Certificate "	+ c.getCertno()	+ " Remains Unsigned - Please Sign A.S.A.P", locale)
				: this.messages.getMessage("email.unsignedcert.subject2", 
						new Object[] {c.getCertno()}, "Certificate "	+ c.getCertno()	+ " Has No Existing Document - Please Create A.S.A.P", locale);
		return subject;
	}
}
