package org.trescal.cwms.core.system.entity.markups.markuprange;

import java.math.BigDecimal;

/**
 * Contains the results of a {@link MarkupRange} application.
 * 
 * @author Richard
 */
public class MarkupRangeWrapper
{
	private BigDecimal value;
	private MarkupRange range;

	public MarkupRange getRange()
	{
		return this.range;
	}

	public BigDecimal getValue()
	{
		return this.value;
	}

	public void setRange(MarkupRange range)
	{
		this.range = range;
	}

	public void setValue(BigDecimal value)
	{
		this.value = value;
	}
}