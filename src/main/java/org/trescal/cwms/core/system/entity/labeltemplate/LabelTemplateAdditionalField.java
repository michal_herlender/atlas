package org.trescal.cwms.core.system.entity.labeltemplate;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.system.entity.translation.Translation;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="labeltemplateadditionalfield")
public class LabelTemplateAdditionalField {
	private int id;
	private LabelTemplate labelTemplate;
	private String fieldName;
	private String name;
	private Set<Translation> translations;
		
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY, targetEntity=LabelTemplate.class)
	@JoinColumn(name = "labeltemplateid", nullable = false)
	@JsonIgnore
	public LabelTemplate getLabelTemplate() {
		return labelTemplate;
	}
	
	@Column(name = "fieldname", nullable = false,columnDefinition = "nvarchar(50)")
	public String getFieldName() {
		return fieldName;
	}

	@Column(name = "name", nullable = false,columnDefinition = "nvarchar(50)")
	public String getName() {
		return name;
	}
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="labeltemplateadditionalfieldtranslation", joinColumns=@JoinColumn(name="id"))
	public Set<Translation> getTranslations() {
		return translations;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setLabelTemplate(LabelTemplate labelTemplate) {
		this.labelTemplate = labelTemplate;
	}
	
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setTranslations(Set<Translation> translations) {
		this.translations = translations;
	}
}
