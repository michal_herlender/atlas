package org.trescal.cwms.core.system.entity.scheduledtask.db;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.impl.triggers.SimpleTriggerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.system.entity.scheduledtask.ScheduledTask;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.GenericTools;
import org.trescal.cwms.core.tools.StringTools;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Service("ScheduledTaskService")
@Slf4j
public class ScheduledTaskServiceImpl implements ScheduledTaskService {
	@Autowired
	private ScheduledTaskDao scheduledTaskDao;
	@Autowired
	private SchedulerFactoryBean schedulerFactoryBean;
	@Value("${cwms.config.scheduledtask.globalactive}")
	private boolean globalActive;

	private static final Logger logger = LoggerFactory.getLogger(ScheduledTaskServiceImpl.class);

	public void deleteScheduledTask(ScheduledTask scheduledtask) {
		this.scheduledTaskDao.remove(scheduledtask);
	}

	public ScheduledTask findScheduledTask(int id) {
		return this.scheduledTaskDao.find(id);
	}

	public ScheduledTask findScheduledTaskByClassAndMethodNames(String className, String methodName) {
		return this.scheduledTaskDao.findScheduledTaskByClassAndMethodNames(className, methodName);
	}

	public List<ScheduledTask> getAllScheduledTasks() {
		return this.scheduledTaskDao.findAll();
	}

	public void insertScheduledTask(ScheduledTask ScheduledTask) {
		this.scheduledTaskDao.persist(ScheduledTask);
	}

	public void reportOnScheduledTaskRun(String className, StackTraceElement[] ste, boolean success, String message) {
		String methodName = GenericTools.getMethodNameFromStackTrace(ste);
		ScheduledTask task = this.findScheduledTaskByClassAndMethodNames(className, methodName);
		String trimmedMessage = StringTools.trimToLength(message, 300);

		if (task != null) {
			task.setLastRunTime(new Date());
			task.setLastRunSuccess(success);
			task.setLastRunMessage(trimmedMessage);

			this.updateScheduledTask(task);
		} else {
			logger.error("ERROR: Attempt to report results of an un-found scheduled task");
		}
	}

	public void saveOrUpdateScheduledTask(ScheduledTask scheduledtask) {
		this.scheduledTaskDao.saveOrUpdate(scheduledtask);
	}

	public boolean scheduledTaskIsActive(String className, StackTraceElement[] ste) {
		String methodName = GenericTools.getMethodNameFromStackTrace(ste);
		return this.scheduledTaskIsActive(className, methodName);
	}

	public boolean scheduledTaskIsActive(String className, String methodName) {
        log.debug("className = " + className);
        log.debug("methodName = " + methodName);
        ScheduledTask task = this.findScheduledTaskByClassAndMethodNames(className, methodName);
        boolean result = false;
        if (task != null) {
            if (this.globalActive) {
                if (task.isActive()) {
                    result = true;
                    logger.debug("Scheduled task is active");
                } else {
                    logger.debug("Scheduled task is inactive");
                }
			} else {
				logger.debug("Scheduled tasks are globally inactive");
			}
		} else {
			logger.error("No scheduled task found for class " + className + " and method " + methodName);
		}
		return result;
	}

	public void switchScheduledTaskOnOrOff(int id, boolean active) {
		ScheduledTask task = this.findScheduledTask(id);
		task.setActive(active);
		this.updateScheduledTask(task);
	}

	public ResultWrapper triggerScheduledTaskAfterDelay(String jobName, int delayInSeconds) {
		try {
			JobKey jobKey = new JobKey(jobName, Scheduler.DEFAULT_GROUP);
			List<? extends Trigger> trigs = this.schedulerFactoryBean.getScheduler().getTriggersOfJob(jobKey);

			Calendar cal = new GregorianCalendar();
			cal.add(Calendar.SECOND, delayInSeconds);
			String randomName = "myTrigger-" + new Date().toString() + "-" + RandomStringUtils.randomAlphanumeric(20);
			logger.debug(randomName);
			SimpleTriggerImpl trigger = new SimpleTriggerImpl(randomName, Scheduler.DEFAULT_GROUP, cal.getTime(), null,
					0, 0L);
			trigger.setJobName(jobName);

			if (!trigs.isEmpty()) {
				this.schedulerFactoryBean.getScheduler().rescheduleJob(trigs.get(0).getKey(), trigger);
			} else {
				this.schedulerFactoryBean.getScheduler().scheduleJob(trigger);
			}

			return new ResultWrapper(true,
					"Scheduled task successfully scheduled for " + DateTools.dtf.format(cal.getTime()));
		} catch (Exception e) {
			logger.error("ERROR: ", e);

			return new ResultWrapper(false, "There was an error scheduling the task");
		}
	}

	public ResultWrapper triggerScheduledTaskNow(String jobName) {
		try {
			JobKey jobKey = new JobKey(jobName, Scheduler.DEFAULT_GROUP);
			this.schedulerFactoryBean.getScheduler().triggerJob(jobKey);

			return new ResultWrapper(true, "Scheduled task successfully triggered with immediate effect");
		} catch (Exception e) {
			logger.error("ERROR: ", e);

			return new ResultWrapper(false, "There was an error triggering the task");
		}
	}

	public void updateScheduledTask(ScheduledTask ScheduledTask) {
		this.scheduledTaskDao.update(ScheduledTask);
	}
}