package org.trescal.cwms.core.system.entity.alligatorlabeltemplate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.company.Company;

@Entity
@Table(name="alligatorlabeltemplaterule")
public class AlligatorLabelTemplateRule {
	private int id;
	private AlligatorLabelTemplate template;
	private Company businessCompany;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}
	
	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="coid", nullable=false)
	public Company getBusinessCompany() {
		return businessCompany;
	}

	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="templateid", nullable=false)
	public AlligatorLabelTemplate getTemplate() {
		return template;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setBusinessCompany(Company businessCompany) {
		this.businessCompany = businessCompany;
	}

	public void setTemplate(AlligatorLabelTemplate template) {
		this.template = template;
	}
}
