package org.trescal.cwms.core.system.entity.labelprinter;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;
import org.trescal.cwms.core.system.entity.alligatorlabelmedia.AlligatorLabelMedia;
import org.trescal.cwms.core.system.entity.labelprinter.enums.AlligatorPrinterLanguage;
import org.trescal.cwms.core.system.entity.printer.AbstractPrinter;

@Entity
@Table(name = "labelprinter")
public class LabelPrinter extends AbstractPrinter
{
	private Integer darknessLevel;
	private Integer resolution;
	private String ipv4;
	private AlligatorPrinterLanguage language;
	private AlligatorLabelMedia media;
	private Set<Address> usedByAddresses;
	private Set<UserPreferences> usedByUsers;

	@OneToMany(mappedBy = "defaultLabelPrinter", fetch = FetchType.LAZY)
	public Set<Address> getUsedByAddresses()
	{
		return this.usedByAddresses;
	}

	@OneToMany(mappedBy = "labelPrinter", fetch = FetchType.LAZY)
	public Set<UserPreferences> getUsedByUsers()
	{
		return this.usedByUsers;
	}

	@NotNull
	@Column(name="darknesslevel", nullable = false)
	public Integer getDarknessLevel() {
		return darknessLevel;
	}

	@NotNull
	@Column(name="resolution", nullable = false)
	public Integer getResolution() {
		return resolution;
	}

	@NotNull
	@Column(name = "ipv4", nullable = false, length=15)
	public String getIpv4() {
		return ipv4;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "language", nullable = false)
	public AlligatorPrinterLanguage getLanguage() {
		return language;
	}

	@NotNull
	@ManyToOne(cascade={}, fetch=FetchType.LAZY)
	@JoinColumn(name="media", nullable = false)
	public AlligatorLabelMedia getMedia() {
		return media;
	}

	public void setUsedByAddresses(Set<Address> usedByAddresses)
	{
		this.usedByAddresses = usedByAddresses;
	}

	public void setUsedByUsers(Set<UserPreferences> usedByUsers)
	{
		this.usedByUsers = usedByUsers;
	}

	public void setLanguage(AlligatorPrinterLanguage language) {
		this.language = language;
	}

	public void setMedia(AlligatorLabelMedia media) {
		this.media = media;
	}

	public void setDarknessLevel(int darknessLevel) {
		this.darknessLevel = darknessLevel;
	}

	public void setResolution(int resolution) {
		this.resolution = resolution;
	}

	public void setIpv4(String ipv4) {
		this.ipv4 = ipv4;
	}
}