package org.trescal.cwms.core.system.entity.printfilerule.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.printfilerule.PrintFileRule;

public class PrintFileRuleServiceImpl implements PrintFileRuleService
{
	PrintFileRuleDao printFileRuleDao;

	public void deletePrintFileRule(PrintFileRule printfilerule)
	{
		this.printFileRuleDao.remove(printfilerule);
	}

	public void deletePrintFileRuleById(int id)
	{
		this.deletePrintFileRule(this.findPrintFileRule(id));
	}

	public PrintFileRule findPrintFileRule(int id)
	{
		return this.printFileRuleDao.find(id);
	}

	public PrintFileRule findRuleForFileName(String fileName)
	{
		// get list of all stored rules
		List<PrintFileRule> rules = this.getAllPrintFileRules();

		// convert filename to lower case
		String name = fileName.toLowerCase();

		// for every stored rule
		for (PrintFileRule rule : rules)
		{
			// convert the match to lower case
			String like = rule.getFileNameLike().toLowerCase();

			// if filename contains match
			if (name.contains(like))
			{
				// return the rule
				return rule;
			}
		}

		// else no matches, so return null
		return null;
	}

	public List<PrintFileRule> getAllPrintFileRules()
	{
		return this.printFileRuleDao.findAll();
	}

	public void insertPrintFileRule(PrintFileRule PrintFileRule)
	{
		this.printFileRuleDao.persist(PrintFileRule);
	}

	public void saveOrUpdatePrintFileRule(PrintFileRule printfilerule)
	{
		this.printFileRuleDao.saveOrUpdate(printfilerule);
	}

	public void setPrintFileRuleDao(PrintFileRuleDao printFileRuleDao)
	{
		this.printFileRuleDao = printFileRuleDao;
	}

	public void updatePrintFileRule(PrintFileRule PrintFileRule)
	{
		this.printFileRuleDao.update(PrintFileRule);
	}
}