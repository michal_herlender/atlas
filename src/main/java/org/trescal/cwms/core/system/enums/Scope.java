/**
 * 
 */
package org.trescal.cwms.core.system.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.Locale;

/**
 * Enum which lists the company scope structure available to
 * {@link SystemDefault}. The basic scope hierarchy relates to {@link Company},
 * {@link Subdiv} and {@link Contact}.
 * 
 * @author richard
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Scope implements Serializable
{
	COMPANY(2,"Company","company"), CONTACT(0,"Contact","contact"), SUBDIV(1,"Subdiv","subdiv"), SYSTEM(3,"System","system");
	
	private final int hierarchicalOrder;
	private final String messageCode;
	private final String defaultName;
	private MessageSource messageSource; 
	
	Scope(int hierarchicalOrder, String defaultName, String messageCode) {
		this.hierarchicalOrder = hierarchicalOrder;
		this.messageCode = messageCode;
		this.defaultName = defaultName;	
	}
	
	@Component
	public static class ScopeMessageInjector {
		@Autowired
		private MessageSource messageSource;
		@PostConstruct
		public void postConstruct() {
			for (Scope scope : Scope.values()) {
				scope.setMessageSource(this.messageSource);
			}
		}
	}
	/**
	 * As the scope values are sorted by hierarchical order and stored by ordinal mapping in database,
	 * this establishes a ranking by hierarchical order (0=CONTACT, 1=SUBDIV, 2=COMPANY, 3=SYSTEM)
	 */
	public int getHierarchicalOrder() {
		return hierarchicalOrder;
	}
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public String getDefaultName() {
		return defaultName;
	}
	public String getMessage() {
		Locale locale = LocaleContextHolder.getLocale();
		return getMessageForLocale(locale);
	}
	
	public String getMessageForLocale(Locale locale) {
		return this.messageSource.getMessage(messageCode, null, defaultName, locale);
	}

	public String getName(){
		return name();
	}
}
