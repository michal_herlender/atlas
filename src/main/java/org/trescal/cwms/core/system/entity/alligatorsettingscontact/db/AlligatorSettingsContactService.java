package org.trescal.cwms.core.system.entity.alligatorsettingscontact.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettingsContact;

public interface AlligatorSettingsContactService extends BaseService<AlligatorSettingsContact, Integer> {
	/**
	 * Creates/Updates/Deletes AlligatorSettingsContact as necessary 
	 * @param businessContact - the Contact to edit settings for
	 * @param useContactSettings - whether the contact should have settings at contact level  
	 * @param productionSettingsId - the id of the AlligatorSettings to use for PRODUCTION (if set at contact level)
	 * @param testSettingsId - the id of the AlligatorSettings to use for TEST (if set at contact level) 
	 * @return void 
	 */
	void editAlligatorSettingsContact(Contact businessContact, boolean useContactSettings, int productionSettingsId, int testSettingsId);

	/**
	 * Returns AlligatorSettingsContact for the specified contact, if it exists
	 * @param businessContact
	 * @return AlligatorSettingContact (or null if none) 
	 */
	AlligatorSettingsContact findAlligatorSettingsContact(Contact businessContact);

}
