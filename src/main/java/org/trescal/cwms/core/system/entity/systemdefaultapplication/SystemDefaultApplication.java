/**
 * 
 */
package org.trescal.cwms.core.system.entity.systemdefaultapplication;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.enums.Scope;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Defines a {@link SystemDefault} that has been overridden and applied to
 * either a {@link Company}, {@link Subdiv} or {@link Contact}. This application
 * now takes predecence over it's {@link SystemDefault} default value. There are
 * strict precedence rules within this though, a {@link Contact} default takes
 * priority over a {@link Subdiv} default, which in turn overrides a
 * {@link Company} default which overrides the {@link SystemDefault} default
 * value.
 * 
 * @author richard
 */
@Entity
@Table(name = "systemdefaultapplication", uniqueConstraints = { @UniqueConstraint(columnNames = { "personid", "subdivid", "coid", "defaultid", "orgid" }) })
public class SystemDefaultApplication extends Allocated<Company>
{
	private Company company;
	private Contact contact;
	private int id;
	private Subdiv subdiv;
	private SystemDefault systemDefault;
	private String value;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coid", unique = false, nullable = true)
	public Company getCompany()
	{
		return this.company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", unique = false, nullable = true)
	public Contact getContact()
	{
		return this.contact;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "subdivid", unique = false, nullable = true)
	public Subdiv getSubdiv()
	{
		return this.subdiv;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "defaultid", unique = false, nullable = false)
	public SystemDefault getSystemDefault()
	{
		return this.systemDefault;
	}

	@NotNull
	@Length(min = 1, max = 100)
	@Column(name = "defaultvalue", length = 100, nullable = false)
	public String getValue()
	{
		return this.value;
	}
	
	@Transient
	public Scope getScope() {
		return getCompany() != null ? Scope.COMPANY :
			getSubdiv() != null ? Scope.SUBDIV :
				getContact() != null ? Scope.CONTACT : Scope.SYSTEM;
	}
	
	public void setCompany(Company company)
	{
		this.company = company;
	}

	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setSubdiv(Subdiv subdiv)
	{
		this.subdiv = subdiv;
	}

	public void setSystemDefault(SystemDefault systemDefault)
	{
		this.systemDefault = systemDefault;
	}
	
	/**
	 * @param value the value to set
	 */
	public void setValue(String value)
	{
		this.value = value;
	}
}