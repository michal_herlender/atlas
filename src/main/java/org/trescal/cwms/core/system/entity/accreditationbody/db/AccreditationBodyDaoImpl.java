package org.trescal.cwms.core.system.entity.accreditationbody.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.accreditationbody.AccreditationBody;

@Repository
public class AccreditationBodyDaoImpl extends
		BaseDaoImpl<AccreditationBody, Integer> implements AccreditationBodyDao {

	@Override
	protected Class<AccreditationBody> getEntity() {
		return AccreditationBody.class;
	}
}
