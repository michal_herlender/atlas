package org.trescal.cwms.core.system.entity.systemdefaulttypes.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.DisallowSamePlantNumberDefaultType;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.spring.model.KeyValue;

@RestController
@RequestMapping("settings")
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class SystemDefaultAjaxController {

    @Autowired
    private DisallowSamePlantNumberDefaultType disallowSamePlantNumberDefaultType;

    @GetMapping("disallowSamePlantNumberForCompany.json")
    Boolean disallowSamePlantNumberForCompany(Integer coid, @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto) {
        return disallowSamePlantNumberDefaultType.getValueByScope(Scope.COMPANY, coid, allocatedCompanyDto.getKey());
    }
}
