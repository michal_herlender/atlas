package org.trescal.cwms.core.system.entity.supportedcurrency;

import java.math.BigDecimal;

import org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.GenericPricingEntity;

/**
 * Interface that defines multiple currency support for classes that implement
 * it and that actually have costs associated with them which may need to be
 * displayed in different currencies.
 * 
 * @author richard
 */
public interface MultiCurrencySupport extends MultiCurrencyAware
{
	/**
	 * Gets the cost of this {@link GenericPricingEntity} converted into the
	 * system default currency.
	 * 
	 * @return final cost in the base currency.
	 */
	BigDecimal getBaseFinalCost();

	String getBaseFinalCostFormatted();
}