package org.trescal.cwms.core.system.entity.supportedcurrency;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/*
 * Helper class with static method to obtain formatter with variable fraction rate,
 * used for VAT and tax rate formatting;
 */
public class PercentFormatter {
    /*
     * Returns a NumberFormat with minDigits set to 1 and maxDigits set to 3
     */
    public static NumberFormat getTaxPercentFormat(Locale locale) {
        NumberFormat format = DecimalFormat.getPercentInstance(locale);
        format.setMinimumFractionDigits(1);
        format.setMaximumFractionDigits(3);
        return format;
    }
}
