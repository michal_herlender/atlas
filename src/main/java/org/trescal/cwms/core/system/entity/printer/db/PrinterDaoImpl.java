package org.trescal.cwms.core.system.entity.printer.db;

import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.printer.Printer;

@Repository
public class PrinterDaoImpl extends BaseDaoImpl<Printer, Integer> implements PrinterDao
{
	@Override
	public Printer findPrinterByPath(String printerPath)
	{
		if (printerPath != null)
		{
			Criteria criteria = this.getSession().createCriteria(Printer.class);
			criteria.add(Restrictions.eq("path", printerPath.trim()));
			return (Printer) criteria.uniqueResult();
		}
		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Printer> getPrintersAtAddress(int addrId)
	{
		Criteria criteria = this.getSession().createCriteria(Printer.class);
		criteria.createCriteria("addr").add(Restrictions.idEq(addrId));
		return criteria.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Printer> getBySubdiv(Subdiv subdiv) {
		Criteria criteria = this.getSession().createCriteria(Printer.class);
		Criteria criteriaAddr = criteria.createCriteria("addr");
		criteriaAddr.add(Restrictions.eq("sub", subdiv));

		criteriaAddr.addOrder(Order.asc("addr1"));
		criteria.addOrder(Order.asc("description"));
		
		return criteria.list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Printer> getBySubdivs(Collection<Subdiv> subdivs) {
		Criteria criteria = this.getSession().createCriteria(Printer.class);
		Criteria criteriaAddr = criteria.createCriteria("addr");
		Criteria criteriaSub = criteriaAddr.createCriteria("sub");
		Criteria criteriaComp = criteriaSub.createCriteria("comp");
		if(!subdivs.isEmpty()) criteriaAddr.add(Restrictions.in("sub", subdivs));
		criteriaComp.addOrder(Order.asc("coname"));
		criteriaSub.addOrder(Order.asc("subname"));
		criteriaAddr.addOrder(Order.asc("addr1"));
		criteria.addOrder(Order.asc("description"));
		return criteria.list();
	}
	
	
	@Override
	protected Class<Printer> getEntity() {
		return Printer.class;
	}
}