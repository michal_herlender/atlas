package org.trescal.cwms.core.system.entity.emailtemplate;

import java.util.Comparator;

public class EmailTemplateComparator implements Comparator<EmailTemplate> {

	@Override
	public int compare(EmailTemplate template0, EmailTemplate template1) {
		// 1 - Compare by component
		int result = template0.getComponent().ordinal() - template1.getComponent().ordinal();
		if (result == 0) {
			// Organisation could potentially be null (migrated templates etc...)
			// 2 - Compare by organisation
			if (template0.getOrganisation() != null && template1.getOrganisation() != null) {
				result = template0.getOrganisation().getCoid() - template1.getOrganisation().getCoid();
			}
			if (result == 0) {
				// 3 - Compare by locale
				result = template0.getLocale().toLanguageTag().compareTo(template1.getLocale().toLanguageTag());
				if (result == 0) {
					// 4 - Compare by whether template has a subdivision
					if ((template0.getSubdiv() == null) && (template1.getSubdiv() != null)) result = -1;
					else if ((template0.getSubdiv() != null) && (template1.getSubdiv() == null)) result = 1;
					else if ((template0.getSubdiv() != null) && (template1.getSubdiv() != null)) {
						result = template0.getSubdiv().getSubdivid() - template1.getSubdiv().getSubdivid(); 
					}
					// 5 - Compare by ID
					if (result == 0) result = template0.getId() - template1.getId();
				}
			}
		}
		return result;
	}
	
}
