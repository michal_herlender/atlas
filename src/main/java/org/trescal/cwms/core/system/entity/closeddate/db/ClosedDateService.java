package org.trescal.cwms.core.system.entity.closeddate.db;

import java.util.List;

import org.trescal.cwms.core.system.entity.closeddate.ClosedDate;

public interface ClosedDateService
{
	void deleteClosedDate(ClosedDate closeddate);

	ClosedDate findClosedDate(int id);

	List<ClosedDate> getAllClosedDates();

	List<ClosedDate> getFutureClosedDatesForCompany(int coid);

	void insertClosedDate(ClosedDate closeddate);

	void saveOrUpdateClosedDate(ClosedDate closeddate);

	void updateClosedDate(ClosedDate closeddate);
}