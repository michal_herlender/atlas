package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

@Component
public class ApprovedLimit extends SystemDefaultDoubleType {

	@Autowired
	private ReloadableResourceBundleMessageSource messageSource;

	public static final String NO_LIMIT_DEFINED = "No limit defined";
	public static final String MESSAGE_CODE = "systemdefault.nolimit";

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.APPROVED_LIMIT;
	}

	@Override
	public Double parseValue(String value) {

		// Note - the system default value may come across in English (if from
		// database) or in the user's locale.
		if (value.equals(NO_LIMIT_DEFINED) || value.equals(
				messageSource.getMessage(MESSAGE_CODE, null, NO_LIMIT_DEFINED, LocaleContextHolder.getLocale()))) {
			return 0d;
		} else {
			try {
				return Double.parseDouble(value);
			} catch (Exception e) {
				return 0d;
			}
		}
	}

	@Override
	public String getValue(String parsedValue) {
		try {
			Double val = Double.parseDouble(parsedValue);
			return val == 0
					? messageSource.getMessage(MESSAGE_CODE, null, NO_LIMIT_DEFINED, LocaleContextHolder.getLocale())
					: parsedValue;
		} catch (Exception ex) {
			return parsedValue;
		}
	}
}