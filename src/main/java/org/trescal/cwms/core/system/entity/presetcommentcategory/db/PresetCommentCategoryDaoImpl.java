package org.trescal.cwms.core.system.entity.presetcommentcategory.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment;
import org.trescal.cwms.core.system.entity.presetcomment.PresetCommentType;
import org.trescal.cwms.core.system.entity.presetcomment.PresetComment_;
import org.trescal.cwms.core.system.entity.presetcommentcategory.PresetCommentCategory;
import org.trescal.cwms.core.system.entity.presetcommentcategory.PresetCommentCategory_;

@Repository
public class PresetCommentCategoryDaoImpl extends BaseDaoImpl<PresetCommentCategory, Integer>
		implements PresetCommentCategoryDao {

	@Override
	protected Class<PresetCommentCategory> getEntity() {
		return PresetCommentCategory.class;
	}

	@Override
	public PresetCommentCategory getByName(String categoryName) {
		return getFirstResult(cb -> {
			CriteriaQuery<PresetCommentCategory> cq = cb.createQuery(PresetCommentCategory.class);
			Root<PresetCommentCategory> category = cq.from(PresetCommentCategory.class);
			cq.where(cb.equal(category.get(PresetCommentCategory_.category), categoryName));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<PresetCommentCategory> getAllByType(PresetCommentType commentType) {
		return getResultList(cb -> {
			CriteriaQuery<PresetCommentCategory> cq = cb.createQuery(PresetCommentCategory.class);
			Root<PresetCommentCategory> category = cq.from(PresetCommentCategory.class);
			Join<PresetCommentCategory, PresetComment> presetComment = category.join(PresetCommentCategory_.comments,
					JoinType.INNER);
			presetComment.on(cb.equal(presetComment.get(PresetComment_.type), commentType));
			cq.distinct(true);
			return cq;
		});
	}
}