package org.trescal.cwms.core.system.entity.emailcontent;

/**
 * Wrapper to hold body and subject of email for preview / sending purposes
 * @author galen
 *
 */
public class EmailContentDto {
	
	private String body;
	private String subject;

	public EmailContentDto(String body, String subject) {
		super();
		this.body = body;
		this.subject = subject;
	}
	
	public String getBody() {
		return body;
	}
	public String getSubject() {
		return subject;
	}
}
