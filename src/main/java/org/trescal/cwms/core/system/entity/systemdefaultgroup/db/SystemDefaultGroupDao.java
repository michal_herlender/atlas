package org.trescal.cwms.core.system.entity.systemdefaultgroup.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.system.entity.systemdefaultgroup.SystemDefaultGroup;
import org.trescal.cwms.core.system.enums.Scope;

public interface SystemDefaultGroupDao extends BaseDao<SystemDefaultGroup, Integer> {
	
	List<SystemDefaultGroup> getAllSystemDefaultByCorole(String corole, Scope scope);
}