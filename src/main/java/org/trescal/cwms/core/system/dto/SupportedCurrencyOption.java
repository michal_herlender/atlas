package org.trescal.cwms.core.system.dto;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

import java.io.IOException;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SupportedCurrencyOption {
	private String optionValue;
	@JsonSerialize(using = SupportedCurrencySerializer.class)
	private SupportedCurrency currency;
	private boolean systemDefault;
	private boolean companyDefault;
	private BigDecimal rate;
	private boolean selected;
	private String defaultCurrencySymbol;

	public String getLabel() {
		val appendix = companyDefault ? "[Company Default Rate]" : systemDefault ? "[System Default Rate]" : "";
		return String.format("%s @ %s1 = %s%s %s", currency.getCurrencyCode(), defaultCurrencySymbol, currency.getCurrencySymbol(), rate, appendix);
	}

	static class SupportedCurrencySerializer extends StdSerializer<SupportedCurrency> {

		public SupportedCurrencySerializer() {
			super(SupportedCurrency.class);
		}

		@Override
		public void serialize(SupportedCurrency value, JsonGenerator gen, SerializerProvider provider) throws IOException {
			gen.writeString(value.getCurrencyCode());
		}
	}
}
