package org.trescal.cwms.core.system.dto;

import org.trescal.cwms.core.jobs.job.entity.upcomingworkjoblink.UpcomingWorkJobLinkAjaxDto;
import org.trescal.cwms.core.system.entity.upcomingwork.UpcomingWork;

import java.util.List;

public class UpcomingWorkWrapper
{
	private int endBlock;
	private int startBlock;
	private UpcomingWork upcomingWork;

	public List<UpcomingWorkJobLinkAjaxDto> getUpcomingWorkJobLinkAjaxDtos() {
		return upcomingWorkJobLinkAjaxDtos;
	}

	public void setUpcomingWorkJobLinkAjaxDtos(List<UpcomingWorkJobLinkAjaxDto> upcomingWorkJobLinkAjaxDtos) {
		this.upcomingWorkJobLinkAjaxDtos = upcomingWorkJobLinkAjaxDtos;
	}

	private List<UpcomingWorkJobLinkAjaxDto> upcomingWorkJobLinkAjaxDtos;

	public UpcomingWorkWrapper(UpcomingWork upcomingWork, int startBlock, int endBlock,List<UpcomingWorkJobLinkAjaxDto> upcomingWorkJobLinkAjaxDtos)
	{
		this.upcomingWork = upcomingWork;
		this.startBlock = startBlock;
		this.endBlock = endBlock;
		this.upcomingWorkJobLinkAjaxDtos = upcomingWorkJobLinkAjaxDtos;
	}

	public int getEndBlock()
	{
		return this.endBlock;
	}

	public int getStartBlock()
	{
		return this.startBlock;
	}

	public UpcomingWork getUpcomingWork()
	{
		return this.upcomingWork;
	}

	public void setEndBlock(int endBlock)
	{
		this.endBlock = endBlock;
	}

	public void setStartBlock(int startBlock)
	{
		this.startBlock = startBlock;
	}

	public void setUpcomingWork(UpcomingWork upcomingWork)
	{
		this.upcomingWork = upcomingWork;
	}
}