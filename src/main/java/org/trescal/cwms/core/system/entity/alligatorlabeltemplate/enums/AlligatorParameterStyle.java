package org.trescal.cwms.core.system.entity.alligatorlabeltemplate.enums;

/**
 * Defines how parameters are managed for an aspect of communication with Alligator
 * (e.g. label template values)
 * AUTOMATIC : The parameters are determined dynamically by Atlas 
 * FIXED : The parameters are defined beforehand
 *
 */
public enum AlligatorParameterStyle {
	AUTOMATIC, 
	FIXED;
}
