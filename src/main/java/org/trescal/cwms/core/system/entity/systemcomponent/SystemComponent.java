package org.trescal.cwms.core.system.entity.systemcomponent;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.system.entity.componentdoctype.ComponentDoctype;
import org.trescal.cwms.core.system.entity.componentsubdirectory.ComponentSubDirectory;
import org.trescal.cwms.core.system.entity.defaultnote.DefaultNote;
import org.trescal.cwms.core.tools.filebrowser.RegularExpression;

@SuppressWarnings("serial")
@Entity
@Table(name = "systemcomponent")
public class SystemComponent implements Serializable
{
	/**
	 * Defines an additional path to append to the directory path that is
	 * generated for this {@link SystemComponent}s parent.
	 */
	private String appendToParentPath;
	/**
	 * Defines any children {@link SystemComponent} that might inherit their
	 * properties from this {@link SystemComponent}.
	 */
	private Set<SystemComponent> childrenComponents;

	private Component component;
	private int componentId;

	private String componentName;
	@Deprecated
	private String defaultEmailBodyReference;
	@Deprecated
	private String defaultEmailSubject;

	private Set<DefaultNote> defaultNotes = new HashSet<DefaultNote>(0);
	private Set<ComponentDoctype> docTypes = new HashSet<ComponentDoctype>(0);
	
	/**
	 * Indicates if the component supports email templates, 
	 * i.e. EmailTemplate should be managed for entity 
	 */
	private boolean emailTemplates;

	private List<RegularExpression> fileNameRegexs;

	/**
	 * If a component is set to split folders, this defines whether the range
	 * boundaries should be decremented by 1. For example, the Procedure ranges
	 * should go 10000-19999 instead of 10001-20000!
	 */
	private boolean foldersDropBoundaries;

	private boolean foldersPerItem;

	private boolean foldersSplit;

	private int foldersSplitRange;

	private boolean foldersYear;
	
	private String numberedSubFolderDivider;

	private boolean numberedSubFolders;
	/**
	 * Defines the {@link SystemComponent} that this {@link SystemComponent}
	 * uses to get it's path information.
	 */
	private SystemComponent parentComponent;
	private Character prependToId;
	private String rootDir;
	private Set<ComponentSubDirectory> subDirs;

	/**
	 * If this is a child {@link SystemComponent} this setting defines which set
	 * of properties to use.
	 */
	private boolean usingParentSettings;

	@Length(min = 0, max = 200)
	@Column(name = "appendtoparentpath", nullable = true, length = 200)
	public String getAppendToParentPath()
	{
		return this.appendToParentPath;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "parentComponent")
	public Set<SystemComponent> getChildrenComponents()
	{
		return this.childrenComponents;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "component", nullable = false, unique = true)
	public Component getComponent()
	{
		return this.component;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "componentid", nullable = false, unique = true)
	@Type(type = "int")
	public int getComponentId()
	{
		return this.componentId;
	}

	@Column(name = "componentname", length = 30, nullable = false, unique = true)
	public String getComponentName()
	{
		return this.componentName;
	}

	@Deprecated
	@Column(name = "defaultemailbodyreference", length = 100)
	public String getDefaultEmailBodyReference()
	{
		return this.defaultEmailBodyReference;
	}

	@Deprecated
	@Column(name = "defaultemailsubject", length = 200)
	public String getDefaultEmailSubject()
	{
		return this.defaultEmailSubject;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "component")
	public Set<DefaultNote> getDefaultNotes()
	{
		return this.defaultNotes;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "component")
	public Set<ComponentDoctype> getDocTypes()
	{
		return this.docTypes;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "sc")
	public List<RegularExpression> getFileNameRegexs()
	{
		return this.fileNameRegexs;
	}

	@NotNull
	@Column(name = "foldersperitem", nullable = false, columnDefinition="tinyint")
	public boolean getFoldersPerItem()
	{
		return this.foldersPerItem;
	}

	@NotNull
	@Column(name = "folderssplit", nullable = false, columnDefinition="tinyint")
	public boolean getFoldersSplit()
	{
		return this.foldersSplit;
	}

	@NotNull
	@Column(name = "folderssplitrange", nullable = false)
	@Type(type = "int")
	public int getFoldersSplitRange()
	{
		return this.foldersSplitRange;
	}

	@NotNull
	@Column(name = "foldersyear", nullable = false, columnDefinition="tinyint")
	public boolean getFoldersYear()
	{
		return this.foldersYear;
	}

	@Column(name = "numberedsubfolderdivider", length = 50)
	public String getNumberedSubFolderDivider()
	{
		return this.numberedSubFolderDivider;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.EAGER)
	@JoinColumn(name = "parentid", nullable = true)
	public SystemComponent getParentComponent()
	{
		return this.parentComponent;
	}

	@Column(name = "prependtoid", nullable = true)
	@Type(type = "char")
	public Character getPrependToId()
	{
		return this.prependToId;
	}

	@NotNull
	@Column(name = "rootdir", length = 30, nullable = false, unique = true)
	public String getRootDir()
	{
		return this.rootDir;
	}

	@OneToMany(mappedBy = "component", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<ComponentSubDirectory> getSubDirs()
	{
		return this.subDirs;
	}

	// TODO add @NotNull / constraint
	@Column(name = "emailtemplates", columnDefinition="bit default 0")
	public boolean isEmailTemplates() {
		return emailTemplates;
	}

	@Column(name = "foldersdropboundaries", nullable = false, columnDefinition="tinyint")
	//@Type(type = "boolean")
	public boolean isFoldersDropBoundaries()
	{
		return this.foldersDropBoundaries;
	}

	@Column(name = "numberedsubfolders", nullable = false, columnDefinition="tinyint")
	//@Type(type = "boolean")
	public boolean isNumberedSubFolders()
	{
		return this.numberedSubFolders;
	}

	@Column(name = "usingparentsettings", nullable = false, columnDefinition="tinyint")
	//@Type(type = "boolean")
	public boolean isUsingParentSettings()
	{
		return this.usingParentSettings;
	}

	public void setAppendToParentPath(String appendToParentPath)
	{
		this.appendToParentPath = appendToParentPath;
	}

	public void setChildrenComponents(Set<SystemComponent> childrenComponents)
	{
		this.childrenComponents = childrenComponents;
	}

	public void setComponent(Component component)
	{
		this.component = component;
	}

	public void setComponentId(int componentId)
	{
		this.componentId = componentId;
	}

	public void setComponentName(String componentName)
	{
		this.componentName = componentName;
	}

	@Deprecated
	public void setDefaultEmailBodyReference(String defaultEmailBodyReference)
	{
		this.defaultEmailBodyReference = defaultEmailBodyReference;
	}

	@Deprecated
	public void setDefaultEmailSubject(String defaultEmailSubject)
	{
		this.defaultEmailSubject = defaultEmailSubject;
	}

	public void setDefaultNotes(Set<DefaultNote> defaultNotes)
	{
		this.defaultNotes = defaultNotes;
	}

	public void setDocTypes(Set<ComponentDoctype> docTypes)
	{
		this.docTypes = docTypes;
	}

	public void setFileNameRegexs(List<RegularExpression> fileNameRegexs)
	{
		this.fileNameRegexs = fileNameRegexs;
	}

	public void setFoldersDropBoundaries(boolean foldersDropBoundaries)
	{
		this.foldersDropBoundaries = foldersDropBoundaries;
	}

	public void setFoldersPerItem(boolean foldersPerItem)
	{
		this.foldersPerItem = foldersPerItem;
	}

	public void setFoldersSplit(boolean foldersSplit)
	{
		this.foldersSplit = foldersSplit;
	}

	public void setFoldersSplitRange(int foldersSplitRange)
	{
		this.foldersSplitRange = foldersSplitRange;
	}

	public void setFoldersYear(boolean foldersYear)
	{
		this.foldersYear = foldersYear;
	}

	public void setNumberedSubFolderDivider(String numberedSubFolderDivider)
	{
		this.numberedSubFolderDivider = numberedSubFolderDivider;
	}

	public void setNumberedSubFolders(boolean numberedSubFolders)
	{
		this.numberedSubFolders = numberedSubFolders;
	}

	public void setParentComponent(SystemComponent parentComponent)
	{
		this.parentComponent = parentComponent;
	}

	public void setPrependToId(Character prependToId)
	{
		this.prependToId = prependToId;
	}

	public void setRootDir(String rootDir)
	{
		this.rootDir = rootDir;
	}

	public void setSubDirs(Set<ComponentSubDirectory> subDirs)
	{
		this.subDirs = subDirs;
	}

	public void setUsingParentSettings(boolean usingParentSettings)
	{
		this.usingParentSettings = usingParentSettings;
	}

	public void setEmailTemplates(boolean emailTemplates) {
		this.emailTemplates = emailTemplates;
	}
}
