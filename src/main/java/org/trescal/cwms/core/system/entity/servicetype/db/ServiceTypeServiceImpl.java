package org.trescal.cwms.core.system.entity.servicetype.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceTypeDto;
import org.trescal.cwms.spring.model.KeyValue;

@Service
public class ServiceTypeServiceImpl extends BaseServiceImpl<ServiceType, Integer> implements ServiceTypeService {

	@Autowired
	private ServiceTypeDao serviceTypeDao;
	@Autowired
	private MessageSource messageSource;

	@Override
	protected BaseDao<ServiceType, Integer> getBaseDao() {
		return serviceTypeDao;
	}

	@Override
	public List<ServiceType> getAllByDomainType(DomainType domainType) {
		return serviceTypeDao.getAllByDomainType(domainType);
	}

	@Override
	public List<ServiceType> getAllCalServiceTypes() {
		return serviceTypeDao.getAllCalServiceTypes();
	}

	@Override
	public ServiceType getByLongName(String longName, Locale locale) {
		return serviceTypeDao.getByLongName(longName, locale);
	}
	
	@Override
	public List<ServiceTypeDto> getDTOs(Collection<Integer> serviceTypeIds, Locale locale) {
		List<ServiceTypeDto> result = new ArrayList<>();
		if (!serviceTypeIds.isEmpty()) {
			result = this.serviceTypeDao.getDTOs(serviceTypeIds, locale);
		}
		return result;
	}

	@Override
	public List<KeyValue<Integer, String>> getDTOList(DomainType domainType, JobType jobType, Locale locale,
			boolean prependNoneDto) {
		List<KeyValue<Integer, String>> results = new ArrayList<>();
		if (prependNoneDto)
			results.add(new KeyValue<>(0, messageSource.getMessage("company.none", null, locale)));
		this.serviceTypeDao.getAllDto(domainType, jobType, locale).stream().forEach(stDto -> results
				.add(new KeyValue<>(stDto.getId(), stDto.getShortName() + " - " + stDto.getLongName())));
		return results;
	}

	@Override
	public List<KeyValue<Integer, String>> getDTOListWithShortNames(DomainType domainType, JobType jobType,
			Locale locale, boolean prependNoneDto) {
		List<KeyValue<Integer, String>> results = new ArrayList<>();
		if (prependNoneDto)
			results.add(new KeyValue<>(0, messageSource.getMessage("company.none", null, locale)));
		this.serviceTypeDao.getAllDto(domainType, jobType, locale).stream()
				.forEach(stDto -> results.add(new KeyValue<>(stDto.getId(), stDto.getShortName())));
		return results;
	}

	@Override
	public ServiceType getByShortNameAndLocal(String shortName, Locale locale) {
		return serviceTypeDao.getByShortNameAndLocal(shortName, locale);
	}

	@Override
	public List<ServiceType> getByJobType(JobType jobType) {
		return serviceTypeDao.getByJobType(jobType);
	}

	@Override
	public Set<Integer> getIdsByJobType(JobType jobType) {
		List<ServiceType> serviceTypes = this.serviceTypeDao.getByJobType(jobType);
		return serviceTypes.stream().map(st -> st.getServiceTypeId()).collect(Collectors.toSet());
	}


}