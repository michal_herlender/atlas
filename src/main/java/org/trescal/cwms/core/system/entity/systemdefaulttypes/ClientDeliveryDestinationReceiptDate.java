package org.trescal.cwms.core.system.entity.systemdefaulttypes;

import org.springframework.stereotype.Component;

@Component
public class ClientDeliveryDestinationReceiptDate extends SystemDefaultBooleanType {

	@Override
	public SystemDefaultNames defaultName() {
		return SystemDefaultNames.CLIENT_DN_DESTINATION_RECEIPT_DATE;
	}

}
