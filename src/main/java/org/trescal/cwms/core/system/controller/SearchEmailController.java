package org.trescal.cwms.core.system.controller;

import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.EmailDto;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.form.SearchEmailForm;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@RequestMapping("searchemail.htm")
@SessionAttributes(Constants.SESSION_ATTRIBUTE_COMPANY)
public class SearchEmailController
{
	@Autowired
	private ContactService conServ;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private MessageSource messages;
	
	@ModelAttribute("form")
	protected SearchEmailForm createSearcEmailForm() throws Exception
	{
		return new SearchEmailForm();
	}
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) throws Exception
	{
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public String referenceData(Model model, Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute("form") SearchEmailForm form) {
		
		ContactKeyValue defaultDto = new ContactKeyValue(0, "-- "+messages.getMessage("system.selectcontact", null, locale)+" --");
		model.addAttribute("fromContacts", this.conServ.getContactDtoList(companyDto.getKey(), null, true, defaultDto));
		return "trescal/core/system/searchemail";
	}
	
	@RequestMapping(method=RequestMethod.POST)
	protected String onSubmit(Model model, 
			@ModelAttribute("form") SearchEmailForm form, BindingResult result) throws Exception
	{
		if ( result.hasErrors()) {
			return "trescal/core/system/searchemail";
		}
		else {
			PagedResultSet<EmailDto> prs = new PagedResultSet<EmailDto>(form.getResultsPerPage(), form.getPageNo());
			this.emailServ.searchEmailsDto(form, prs, true);
			model.addAttribute("pagedResultSet", prs);
			return "trescal/core/system/emailsearchresults";
		}
	}
}