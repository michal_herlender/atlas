package org.trescal.cwms.core.system.entity.markups.markuprange.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.markups.markuprange.MarkupRange;
import org.trescal.cwms.core.system.entity.markups.markuptype.MarkupType;

@Repository("MarkupRangeDao")
public class MarkupRangeDaoImpl extends BaseDaoImpl<MarkupRange, Integer> implements MarkupRangeDao {
	
	@Override
	protected Class<MarkupRange> getEntity() {
		return MarkupRange.class;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MarkupRange> findMarkupRange(MarkupType type, double rangestart) {
		Criteria crit = getSession().createCriteria(MarkupRange.class);
		crit.add(Restrictions.eq("rangeStart", rangestart));
		crit.add(Restrictions.eq("typeId",type.getId()));
		return (List<MarkupRange>) crit.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MarkupRange> getAllMarkupRanges(MarkupType type) {
		Criteria crit = getSession().createCriteria(MarkupRange.class);
		if (type != null)
		crit.add(Restrictions.eq("typeId",type.getId()));
		crit.addOrder(Order.desc("rangeStart"));
		return (List<MarkupRange>) crit.list();
	}
}