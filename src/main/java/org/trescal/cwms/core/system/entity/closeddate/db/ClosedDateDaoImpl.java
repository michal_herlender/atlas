package org.trescal.cwms.core.system.entity.closeddate.db;

import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.system.entity.closeddate.ClosedDate;
import org.trescal.cwms.core.system.entity.closeddate.ClosedDate_;
import org.trescal.cwms.core.tools.DateTools;

@Repository("ClosedDateDao")
public class ClosedDateDaoImpl extends BaseDaoImpl<ClosedDate, Integer> implements ClosedDateDao {

	@Override
	protected Class<ClosedDate> getEntity() {
		return ClosedDate.class;
	}

	@Override
	public List<ClosedDate> getFutureClosedDatesForCompany(int coid) {

		return getResultList(cb -> {
			CriteriaQuery<ClosedDate> cq = cb.createQuery(ClosedDate.class);
			Root<ClosedDate> root = cq.from(ClosedDate.class);
			Join<ClosedDate, Company> comp = root.join(ClosedDate_.comp);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(comp.get(Company_.coid), coid));
			clauses.getExpressions().add(
					cb.greaterThanOrEqualTo(root.get(ClosedDate_.closedDate), DateTools.dateToLocalDate(new Date())));
			cq.where(clauses);
			return cq;
		});
	}
}