package org.trescal.cwms.core.system.entity.supportedcurrency;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

import lombok.extern.slf4j.Slf4j;

/**
 * Helper class to format currency value with symbol, per local conventions
 * 
 * Typically to be used for document output, where we display a currency symbol e.g. $
 * 
 * The currency code (USD, EUR, SGD) is intentionally not included here
 */
@Slf4j
public class CurrencyValueFormatter {
	
	private static char NON_BREAKING_SPACE = '\u00a0';
	
	/**
	 * Formats currency value with symbol using the specified locale for decimal format
	 * and referring to the supportedCurrency for all other information
	 */
	public static String format(BigDecimal value, SupportedCurrency supportedCurrency, Locale locale) {
		return format(value, supportedCurrency.getCurrencySymbol(), supportedCurrency.getSymbolPosition(), locale);
	}

	/**
	 * Formats currency value with symbol using the specified locale for decimal format
	 * and using the specified currencySymbol and symbolPosition
	 * 
	 * Supports null value of currency to aid in troubleshooting of bad data (so page / document still formatted) but logs as an error. 
	 */
	public static String format(BigDecimal value, String currencySymbol, CurrencySymbolPosition symbolPosition, Locale locale) {
		if (currencySymbol == null) throw new IllegalArgumentException("currencySymbol must not be null");
		if (symbolPosition == null) throw new IllegalArgumentException("symbolPosition must not be null");
		if (locale == null) throw new IllegalArgumentException("symbolPosition must not be null");
		
		// Note, currencies are assumed to have 2 decimal places; if adding support for 3/0 etc... implement here.
		NumberFormat nf = NumberFormat.getInstance(locale);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		
		StringBuffer result = new StringBuffer();
		if (value == null) {
			log.error("null value provided for currency formatting, returning empty string");
		}
		else {
			switch (symbolPosition) {
			case POSTFIX_SPACE:
				result.append(nf.format(value.doubleValue()));
				result.append(NON_BREAKING_SPACE);
				result.append(currencySymbol);
				break;
			case PREFIX_NO_SPACE:
				result.append(currencySymbol);
				result.append(nf.format(value.doubleValue()));
				break;
			case PREFIX_SPACE:
				result.append(currencySymbol);
				result.append(NON_BREAKING_SPACE);
				result.append(nf.format(value.doubleValue()));
				break;
			default:
				throw new UnsupportedOperationException("Symbol position not supported : "+symbolPosition);
			}
		}
		
		return result.toString();
	}
}
