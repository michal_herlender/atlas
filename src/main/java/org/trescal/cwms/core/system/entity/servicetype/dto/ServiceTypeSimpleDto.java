package org.trescal.cwms.core.system.entity.servicetype.dto;

import lombok.AllArgsConstructor;
import lombok.Value;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

import static org.trescal.cwms.core.tools.TranslationUtils.getBestTranslation;

@Value
@AllArgsConstructor
public class ServiceTypeSimpleDto {
    Integer id;
    String shortName;
    String longName;

    public static ServiceTypeSimpleDto fromServiceType(ServiceType serviceType) {
        return new ServiceTypeSimpleDto(serviceType.getServiceTypeId(), serviceType.getShortName(), serviceType.getLongName());
    }

    public static ServiceTypeSimpleDto fromServiceTypeWithTranslatedNames(ServiceType serviceType) {
        return new ServiceTypeSimpleDto(serviceType.getServiceTypeId(), getBestTranslation(serviceType.getShortnameTranslation()).orElse(""), getBestTranslation(serviceType.getLongnameTranslation()).orElse(""));
    }
}
