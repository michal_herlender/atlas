package org.trescal.cwms.core.system.entity.defaultnote.db;

import org.trescal.cwms.core.admin.form.DefaultNoteForm;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.defaultnote.DefaultNote;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

public interface DefaultNoteService extends BaseService<DefaultNote, Integer> {
    /**
     * Creates and saves new note using the form which must be validated & correct
     *
     * @param coid - company id for which the note should be created
     * @param form - details of new note
     * @return id of the note that has been created
     */
    int createNote(int coid, DefaultNoteForm form);

	/**
	 * Updates a specified note with new details
	 * @param defaultNoteId - id of the note
	 * @param form - details of updated note
     */
    void updateNote(int defaultNoteId, DefaultNoteForm form);

    /**
     * Returns a sorted list of notes (first by component, then by default label, then by id)
     *
     * @param component          (optional) component - can be null
     * @param allocatedCompanyId (mandatory) Integer ID of the business company for which notes are being searched for
     * @return
     */
    List<DefaultNote> getDefaultNotesByComponent(Component component, Integer allocatedCompanyId);

    <A extends Note> Stream<A> createNotesFromDefaultNotesForComponent(Component component, Class<A> noteClass, Integer companyId, Locale locale, Contact creator);
}