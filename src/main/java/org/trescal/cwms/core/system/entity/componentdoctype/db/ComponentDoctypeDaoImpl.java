package org.trescal.cwms.core.system.entity.componentdoctype.db;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.system.entity.componentdoctype.ComponentDoctype;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Repository("ComponentDoctypeDao")
public class ComponentDoctypeDaoImpl extends BaseDaoImpl<ComponentDoctype, Integer> implements ComponentDoctypeDao {
	
	@Override
	protected Class<ComponentDoctype> getEntity() {
		return ComponentDoctype.class;
	}
	
	@SuppressWarnings("unchecked")
	public ComponentDoctype findComponentDoctype(Component component, String doctypeName) {
		DetachedCriteria crit = DetachedCriteria.forClass(ComponentDoctype.class);
		DetachedCriteria componentCrit = crit.createCriteria("component");
		componentCrit.add(Restrictions.eq("component", component));
		DetachedCriteria docCrit = crit.createCriteria("doctype");
		docCrit.add(Restrictions.eq("name", doctypeName));
		List<ComponentDoctype> list = (List<ComponentDoctype>) crit.getExecutableCriteria(getSession()).list();
		return list.size() > 0 ? list.get(0) : null;
	}
}