package org.trescal.cwms.core.system.entity.presetcomment;

import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.system.entity.presetcommentcategory.PresetCommentCategory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "presetcomment")
public class PresetComment extends Allocated<Subdiv> {

	private int id;
	private PresetCommentType type;
	private PresetCommentCategory category;
	private String label;
	private String comment;
	private Contact setby;
	private Date seton;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return this.id;
	}

	@Enumerated(EnumType.STRING)
	public PresetCommentType getType() {
		return type;
	}

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "categoryid")
    public PresetCommentCategory getCategory() {
		return this.category;
	}

	@Size(max = 50)
	@Column(name = "label", length = 50)
	public String getLabel() {
		return label;
	}

	@NotNull
	@Size(min = 1, max = 2000)
	@Column(name = "comment", length = 2000, columnDefinition = "nvarchar(2000)")
	public String getComment() {
		return this.comment;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setbyid")
	public Contact getSetby() {
		return this.setby;
	}

	@NotNull
	@Column(name = "seton")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSeton() {
		return this.seton;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCategory(PresetCommentCategory category) {
		this.category = category;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setSetby(Contact setby) {
		this.setby = setby;
	}

	public void setSeton(Date seton) {
		this.seton = seton;
	}

	public void setType(PresetCommentType type) {
		this.type = type;
	}
}