package org.trescal.cwms.core.system.entity.systemdefault.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.system.entity.systemdefault.SystemDefault;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultNames;
import org.trescal.cwms.core.system.enums.Scope;

public interface SystemDefaultDao extends BaseDao<SystemDefault, SystemDefaultNames> {

	SystemDefault getByName(String name);

	List<SystemDefault> getAllByCompanyRoleAndScope(CompanyRole corole, Scope scope);
}