package org.trescal.cwms.core.system.projection.note.service;

import java.util.Collection;
import java.util.List;

import org.trescal.cwms.core.system.projection.note.NoteProjectionDTO;

public interface NoteProjectionDao {

	List<NoteProjectionDTO> getDeliveryItemNotes(Integer deliveryId);
	
	List<NoteProjectionDTO> getDeliveryNotes(Collection<Integer> deliveryIds);

}
