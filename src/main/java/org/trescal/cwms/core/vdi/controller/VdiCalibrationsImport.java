package org.trescal.cwms.core.vdi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller @IntranetController
public class VdiCalibrationsImport {
    @GetMapping("vdiCalibrationsImport.htm")
    public String vdiCalibrationsImport(){
        return "trescal/core/jobs/calibration/vdiCalibrationsImport";
    }
}
