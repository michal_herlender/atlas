package org.trescal.cwms.core.vdi.projection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor @NoArgsConstructor
public class OrderProjection {
    private Integer deliveryId;
    private String identifier;
    private Integer companyId;
    private String companyName;
    private String addr1;
    private String addr2;
    private String addr3;
    private String city;
    private String country;
    private String contact;
    private String zip;
    private String phone;
    private String fax;
    private String email;
    private LocalDate deliveryDate;
    private String organizationSubdiv;
    private String organizationCompany;
    private String organizationAddr1;
    private String organizationAddr2;
    private String organizationAddr3;
    private String organizationCity;
    private String organizationCountry;
    private String organizationZip;
    private String jobNumber;


}
