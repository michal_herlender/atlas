package org.trescal.cwms.core.vdi.service;

import io.vavr.control.Either;
import lombok.Value;
import lombok.val;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.batch.jobs.operationsimport.ImportedOperationItem;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.deliverynote.entity.deliveryitem.db.DeliveryItemService;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.db.CalibrationService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.calibration.form.ImportedOperationSynthesisForm;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.vdi.data.Orders;
import org.trescal.cwms.core.vdi.projection.OrderProjection;
import org.trescal.cwms.core.vdi.projection.OrdersArchiveNameAndCertificateFiles;
import org.trescal.cwms.core.vdi.projection.PositionProjection;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class VdiService {

	@Autowired
	private JobService jobService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private DeliveryService deliveryService;
	@Autowired
	private DeliveryItemService deliveryItemService;
	@Autowired
	private VdiProjectionExporter projectionExporter;
	@Autowired
	private VdiImporter importer;
	@Autowired
	private CertificateService certificateService;
	@Autowired
	private CalibrationService calibrationService;
	@Autowired
	private CalibrationProcessService calibrationProcessService;
	@Autowired
	private UserService userService;
	@Autowired
	private SubdivService subdivService;

	private Orders createOrderByIdAndRetrievers(Integer id, Function<Integer, Optional<OrderProjection>> orderRetriever,
			Function<Integer, List<PositionProjection>> positionsRetriever) {
		return orderRetriever.apply(id).map(orderProjection -> {
			val positions = positionsRetriever.apply(id);
			val order = projectionExporter.createOrderFromProjections(orderProjection, positions);
			return new Orders(Collections.singletonList(order));
		}).orElseGet(Orders::new);
	}

	public Orders transformDeliveryById(Integer deliveryId) {
		return createOrderByIdAndRetrievers(deliveryId, deliveryService::getOrderProjectionForDeliveryNote,
				deliveryItemService::getPositionProjectionForDeliveryNote);
	}

	public Optional<OrdersArchiveNameAndCertificateFiles> orderWithFilesByDelivery(Integer deliveryId) {
		return orderWithFilesByIdAndRetrievers(deliveryId, deliveryService::getOrderProjectionForDeliveryNote,
				deliveryItemService::getPositionProjectionForDeliveryNote);
	}

	private Optional<OrdersArchiveNameAndCertificateFiles> orderWithFilesByIdAndRetrievers(Integer id,
			Function<Integer, Optional<OrderProjection>> orderRetriever,
			Function<Integer, List<PositionProjection>> positionsRetriever) {
		val orderProjection = orderRetriever.apply(id);
		return orderProjection.map(orderP -> {
			val positions = positionsRetriever.apply(id);
			val certificatesNumbers = positions.stream().map(PositionProjection::getCertificateNumber)
					.collect(Collectors.toList());
			val certificatesFiles = certificateService.certificateFilesForNumbers(orderP.getJobNumber(),
					certificatesNumbers);
			val order = projectionExporter.createOrderFromProjections(orderP, positions);
			val orders = new Orders(Collections.singletonList(order));
			return OrdersArchiveNameAndCertificateFiles.of(orders, orderP.getIdentifier(), certificatesFiles);
		});
	}

	public Optional<OrdersArchiveNameAndCertificateFiles> orderWithFilesByJob(Integer jobId) {
		return orderWithFilesByIdAndRetrievers(jobId, jobService::getOrderProjectionForJob,
				jobItemService::getPositionProjectionForJob);
	}

	public Orders transformJobById(Integer jobId) {
		return createOrderByIdAndRetrievers(jobId, jobService::getOrderProjectionForJob,
				jobItemService::getPositionProjectionForJob);
	}

	public List<Either<String, ImportedJob>> parseOrdersToImportCalibrationsForm(Orders orders,
			List<MultipartFile> certificates, String userName, Integer subdivId) {
		val user = userService.get(userName);
		val sub = subdivService.get(subdivId);
		val con = user.getCon();

		return orders.getOrder().stream().flatMap(importer::parseOrderToImportForm)
				.map(either -> either.flatMap(form -> {
					try {
						List<List<ImportedOperationItem>> groups = this.calibrationService
								.groupImportedOperations(form.getRows());
						calibrationService.importOnsiteOperations(groups, sub.getSubdivid(), con.getPersonid(),
								LocaleContextHolder.getLocale(), calibrationProcessService.findByName("VDI IMPORT"));
					} catch (Exception e) {
						e.printStackTrace();
						val message = StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : "Unknown Exception";
						return Either.left(message);
					}
					return Either.right(form);
				})).map(either -> either.map(form -> {
					val ij = toImportedJob(form);
					uploadCertificateForParsedJob(certificates, userName,
							JobWithCertificates.of(ij.getJobId(), ij.instruments.stream()
									.map(ImportedInstrument::getCertificateNumber).collect(Collectors.toList())));
					return ij;
				})).collect(Collectors.toList());
	}

	private ImportedJob toImportedJob(ImportedOperationSynthesisForm synthesisForm) {
		return ImportedJob.of(synthesisForm.getJobId(),
				synthesisForm.getRows().stream().map(this::toImportedInstrument).collect(Collectors.toList()));
	}

	private ImportedInstrument toImportedInstrument(ImportedOperationItem row) {
		return ImportedInstrument.of(row.getPlantid(), row.getDocumentNumber());
	}

	public List<JobWithCertificates> parseOrdersAndImportCertificates(Orders orders, List<MultipartFile> certificates,
			String userName) {
		Stream<JobWithCertificates> jobWithCertificate = orders.getOrder().stream()
				.map(importer::readCertificatesFromOrder).flatMap(e -> e.fold(s -> Stream.empty(), Stream::of));
		return jobWithCertificate.peek(jw -> uploadCertificateForParsedJob(certificates, userName, jw))
				.collect(Collectors.toList());
	}

	private void uploadCertificateForParsedJob(List<MultipartFile> certificates, String userName,
			JobWithCertificates jw) {
		val files = certificates.stream()
				.filter(f -> jw.getCertNames().contains(FilenameUtils.getBaseName(f.getOriginalFilename())))
				.collect(Collectors.toList());
		try {
			certificateService.uploadOnSiteCertificate(jw.getJobid(), files, LocaleContextHolder.getLocale(),
					userService.get(userName).getCon());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Value(staticConstructor = "of")
	public static class JobWithCertificates {
		Integer jobid;
		List<String> certNames;
	}

	@Value(staticConstructor = "of")
	static public class ImportedJob {
		Integer jobId;
		List<ImportedInstrument> instruments;
	}

	@Value(staticConstructor = "of")
	static public class ImportedInstrument {
		Integer plantId;
		String certificateNumber;
	}
}
