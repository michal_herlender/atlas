package org.trescal.cwms.core.vdi.service;

import java.util.function.Function;

public interface Parser<A> extends Function<String, A> {
}
