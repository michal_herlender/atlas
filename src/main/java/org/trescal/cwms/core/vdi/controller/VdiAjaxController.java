package org.trescal.cwms.core.vdi.controller;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import lombok.val;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.vdi.data.Orders;
import org.trescal.cwms.core.vdi.projection.OrdersArchiveNameAndCertificateFiles;
import org.trescal.cwms.core.vdi.service.VdiService;
import org.trescal.cwms.spring.model.KeyValue;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@RestController
@RequestMapping("vdi/")
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME,
        Constants.SESSION_ATTRIBUTE_COMPANY})
public class VdiAjaxController {

    @Autowired
    private VdiService vdiService;

    @GetMapping(value = "exportDeliveryNote",produces = "application/xml")
    public  Orders exportDeliveryNote(@RequestParam Integer deliveryId){
        return vdiService.transformDeliveryById(deliveryId);
    }

    private ResponseEntity<StreamingResponseBody> getExportWithFiles(Integer id, Function<Integer,Optional<OrdersArchiveNameAndCertificateFiles>> retriever){
        val maybeOrdersNameFiles = retriever.apply(id);
        return maybeOrdersNameFiles
                .<ResponseEntity<StreamingResponseBody>>map(ordersNameFiles -> ResponseEntity.ok()
                        .header("Content-Disposition", "attachment; filename=" + ordersNameFiles.getArchiveName() + ".zip")
                        .body(out -> {
                            val zipOutputStream = new ZipOutputStream(out);
                            val orders = ordersNameFiles.getOrders();
                            copyOrdersToInputStream(orders, ordersNameFiles.getArchiveName() + ".xml", zipOutputStream);
                            val files = ordersNameFiles.getFiles();
                            files.forEach(f -> this.putFileToArchive(f, zipOutputStream));
                            zipOutputStream.close();
                        }))
                .orElseGet(()-> ResponseEntity.badRequest().build());
    }

    @GetMapping(value = "downloadVdi2326ForDelivery.zip", produces = "application/zip")
    public ResponseEntity<StreamingResponseBody> getDeliveryWithFiles(@RequestParam Integer deliveryId){
        return getExportWithFiles(deliveryId, vdiService::orderWithFilesByDelivery);
    }

    @GetMapping(value = "getOrder", produces = "application/xml")
    public Orders getOrder(@RequestParam Integer id) {
        return vdiService.transformJobById(id);
    }


    @GetMapping(value = "downloadJob.zip", produces = "application/zip")
    public ResponseEntity<StreamingResponseBody> getFiles(@RequestParam Integer jobId){
        return getExportWithFiles(jobId,vdiService::orderWithFilesByJob);
    }

    private void putFileToArchive(File file, ZipOutputStream zipOutputStream){
        try {
            zipOutputStream.putNextEntry(new ZipEntry(file.getName()));
            val fis = new FileInputStream(file);
            IOUtils.copy(fis,zipOutputStream);
            fis.close();
            zipOutputStream.closeEntry();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void copyOrdersToInputStream(Orders orders, String entryName, ZipOutputStream zipOutputStream)  {
        try {
            zipOutputStream.putNextEntry(new ZipEntry(entryName));
            val context =JAXBContext.newInstance(Orders.class);
            val marshall = context.createMarshaller();
            marshall.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
            marshall.marshal(orders,zipOutputStream);
            zipOutputStream.closeEntry();
        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }

    }

    @PostMapping("importCertificates.json")
    public List<VdiService.JobWithCertificates> importCertificates(@RequestParam MultipartFile file,
                                                                   @RequestParam(required = false) List<MultipartFile> certificates,
                                                                   @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        try {
            val context = JAXBContext.newInstance(Orders.class);
            val marshall = context.createUnmarshaller();
            val orders = marshall.unmarshal(new StreamSource(file.getInputStream()), Orders.class);
            return vdiService.parseOrdersAndImportCertificates(orders.getValue(), certificates, username);
        } catch (JAXBException | IOException e) {
            return Collections.emptyList();
        }
    }

    @PostMapping("importCalibrations.json")
    public List<Tuple2<String, VdiService.ImportedJob>> importCalibrations(@RequestParam MultipartFile file,
                                                                           @RequestParam(required = false) List<MultipartFile> certificates,
                                                                           @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                                                           @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
        try {
            val context = JAXBContext.newInstance(Orders.class);
            val marshall = context.createUnmarshaller();
            val orders = marshall.unmarshal(new StreamSource(file.getInputStream()), Orders.class);
            return vdiService.parseOrdersToImportCalibrationsForm(orders.getValue(), certificates, username, subdivDto.getKey())
                    .stream()
                    .map(e -> e.<Tuple2<String, VdiService.ImportedJob>>fold(l -> Tuple.of(l, null), r -> Tuple.of(null, r))).collect(Collectors.toList());
        } catch (JAXBException | IOException e) {
            return Collections.emptyList();
        }
    }

}
