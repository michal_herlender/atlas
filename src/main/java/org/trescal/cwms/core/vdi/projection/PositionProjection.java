package org.trescal.cwms.core.vdi.projection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import java.time.LocalDate;
import java.util.Date;

@Data
@AllArgsConstructor @NoArgsConstructor
public class PositionProjection {
    private Integer deliveryId;
    private String idNumber;
    private Integer procedureId;
    private WorkRequirementType workRequirementType;
    private String remark;
    private Integer plantId;
    private String formerBarcode;
    private String serialNumber;
    private String type;
    private String designation;
    private Integer calibrationInterval;
    private IntervalUnit calibrationIntervalUnit;
    private LocalDate lastCalibrationDate;
    private LocalDate nextCalibrationDate;
    private String manufacturer;
    private LocalDate deliveryDate;
    private Integer calibrationId;
    private Integer certificateId;
    private Date certificateDate;
    private String certificateNumber;
    private String certificateRemark;
    private LocalDate calibrationDate;
    private String calibrationPrinciple;
    private String jobItemNote;
    private String personInCharge;
    private CalibrationVerificationStatus calibrationVerificationStatus;
    private Boolean restriction;
}