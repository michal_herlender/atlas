package org.trescal.cwms.core.vdi.projection;

import lombok.Value;
import org.trescal.cwms.core.vdi.data.Orders;

import java.io.File;
import java.util.List;

@Value(staticConstructor = "of")
public class OrdersArchiveNameAndCertificateFiles {
    Orders orders;
    String archiveName;
    List<File> files;
}
