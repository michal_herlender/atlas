package org.trescal.cwms.core.vdi.service;

import io.vavr.Tuple;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.OptionalsUtils;
import org.trescal.cwms.core.vdi.data.*;
import org.trescal.cwms.core.vdi.projection.OrderProjection;
import org.trescal.cwms.core.vdi.projection.PositionProjection;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.vavr.API.*;
import static io.vavr.Patterns.$Tuple2;
import static org.trescal.cwms.core.instrument.CalibrationVerificationStatus.*;
import static org.trescal.cwms.core.tools.GenericTools.applyIfNonEmpty;
import static org.trescal.cwms.core.tools.GenericTools.applyWithFormaterIfNonEmpty;

@Component
public class VdiProjectionExporter {
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private final SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
    @Value("#{props['cwms.config.vdi.version']}")
    private String version;

    public Order createOrderFromProjections(OrderProjection order, List<PositionProjection> positions) {
        return Order.builder().headerSection(projectionToHeader(order))
                .technicalSection(projectionsToTechnical(positions))
                .build();
    }

    private TechnicalSection projectionsToTechnical(List<PositionProjection> positions) {
        val transformedPositions = positions.stream()
                .collect(Collectors.groupingBy(PositionProjection::getPlantId))
                .values().stream().map(this::projectionsToPosition).collect(Collectors.toList());
        return new TechnicalSection(transformedPositions);
    }

    private Position projectionsToPosition(List<PositionProjection> positionProjections) {
        val referential = positionProjections.stream().findFirst();
        return referential.map(projection -> Position.builder()
                .masterData(projectionToPositionMasterData(projection))
                .buyerData(projectionsToBuyerData(positionProjections))
                .supplierData(projectionsToSupplierData(positionProjections))
                .build()).orElse(null);
    }

    private SupplierData projectionsToSupplierData(List<PositionProjection> positionProjections) {
        val calibrations = positionProjections.stream()
                .filter(p -> p.getCalibrationId() != null)
                .filter(p -> p.getCertificateId() != null)
                .map(this::projectionToCalibration)
                .collect(Collectors.toList());
        return calibrations.isEmpty() ? new SupplierData() : SupplierData.builder().calibrations(new Calibrations(calibrations)).build();
    }

    private VdiCalibration projectionToCalibration(PositionProjection projection) {
        val result = new Result();
        result.setResultsDirectives(createResultDirectives(projection));

        applyWithFormaterIfNonEmpty(projection.getCertificateDate(), result::setCalibrationCertificateCreatingDate, dateFormat::format);
        result.setCertificateNumber(projection.getCertificateNumber());
        result.setRemark(projection.getCertificateRemark());
        result.setCondition(calibrationVerificationStatusToVdi(projection.getCalibrationVerificationStatus(),projection.getRestriction()));
        applyIfNonEmpty(projection.getPersonInCharge(), result::setPersonInCharge);
        applyWithFormaterIfNonEmpty(projection.getCalibrationDate(), result::setCalibrationDate, DateTimeFormatter.ISO_DATE::format);
//        There is currently no time component on calibration date
//        applyWithFormaterIfNonEmpty(projection.getCalibrationDate(), result::setCalibrationTime, DateTimeFormatter.ISO_TIME::format);
        applyIfNonEmpty(projection.getCalibrationPrinciple(), result::setCalibrationPrinciple);

        return VdiCalibration.builder().result(result).build();
    }

    private String calibrationVerificationStatusToVdi(CalibrationVerificationStatus calibrationVerificationStatus, Boolean restriction) {
        return Match(Tuple.of(calibrationVerificationStatus,restriction))
                .of(
                        Case($Tuple2($(NOT_APPLICABLE),$()),"000"),
                        Case($Tuple2($(ACCEPTABLE),$(false)),"001"),
                        Case($Tuple2($(NOT_ACCEPTABLE),$()),"002"),
                        Case($Tuple2($(PROBABLY_PASS),$(false)),"003"),
                        Case($Tuple2($(PROBABLY_PASS),$(true)),"005"),
                        Case($Tuple2($(PROBABLY_FAIL),$(false)),"004"),
                        Case($Tuple2($(PROBABLY_FAIL),$(true)),"005"),
                        Case($Tuple2($(ACCEPTABLE),$(true)),"005"),
                        Case($(), "")
                        );
    }

    private ResultsDirectives createResultDirectives(PositionProjection projection) {
        val resultsDirective = new ResultsDirective();
        val dataInstruction = new DataInstruction();
        dataInstruction.setInstruction("000");
        dataInstruction.setValue(projection.getCertificateNumber() + ".pdf");
        val dataFormat = new DataFormat("000", "PDF");
        val dataFormats = new DataFormats(Collections.singletonList(dataFormat));
        dataInstruction.setDataFormats(dataFormats);
        resultsDirective.setRemark(projection.getCertificateRemark());
        resultsDirective.setServiceInstruction("000");
        resultsDirective.setDataInstructions(new DataInstructions(Collections.singletonList(dataInstruction)));
        return new ResultsDirectives(Collections.singletonList(resultsDirective));
    }

    private BuyerData projectionsToBuyerData(List<PositionProjection> positionProjections) {
        val procedureDirectives = positionProjections.stream().filter(p -> p.getRemark() != null).collect(Collectors.groupingBy(PositionProjection::getProcedureId))
                .keySet().stream()
                .map(procedureIdToDirective(positionProjections))
                .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
                ;
        val noteDirectives = positionProjections.stream().filter(p -> StringUtils.isNotBlank(p.getJobItemNote())).collect(Collectors.groupingBy(PositionProjection::getJobItemNote))
                .keySet().stream()
                .map(this.stringToDirective("006"));
        val directives = Stream.concat(procedureDirectives, noteDirectives).collect(Collectors.toList());
        return directives.isEmpty()?new BuyerData():
        BuyerData.builder()
                .buyerDirectives(new BuyerDirectives(directives))
                .build();
    }

    private Function<Integer, Optional<BuyerDirective>> procedureIdToDirective(List<PositionProjection> positionProjections) {
        return id -> {
            val reference = positionProjections.stream().filter(p  -> p.getProcedureId().equals(id)).findFirst();
          val remark = reference
                  .map(PositionProjection::getRemark);
          val workRequirementType = reference.map(PositionProjection::getWorkRequirementType).map(this::workRequirementTypeToVdiCode);
          return remark.flatMap(r -> workRequirementType.map(wt -> BuyerDirective.builder()
          .remark(r).serviceInstruction(wt).build()));
        };
    }

    private String workRequirementTypeToVdiCode(WorkRequirementType type) {
        switch (type){
            case THIRD_PARTY:
            case ONSITE:
                return null;
            case CALIBRATION:
                return "007";
            case REPAIR: return "005";
            case ADJUSTMENT: return "004";
            case GENERAL_SERVICE_OPERATION: return "006";
            default:return null;
        }
    }

    private Function<String, BuyerDirective> stringToDirective(String instruction) {
        return remark -> BuyerDirective.builder().remark(remark).serviceInstruction(instruction).build();
    }

    private MasterData projectionToPositionMasterData(PositionProjection projection) {
        val masterData = new MasterData();
        masterData.setIdNumber(projection.getIdNumber());
        masterData.setSerialnumber(projection.getSerialNumber());
        masterData.setAdditionalIdNumbers(projectionToAdditionalIds(projection));
        masterData.setType(projection.getType());
        masterData.setDesignation(projection.getDesignation());
        if (projection.getCalibrationIntervalUnit() != null) {
            masterData.setCalibrationIntervalUnit(formatFrequencyUnit(projection.getCalibrationIntervalUnit()));
            masterData.setCalibrationInterval(projection.getCalibrationInterval().longValue());
        }

        applyWithFormaterIfNonEmpty(projection.getLastCalibrationDate(), masterData::setLastCalibrationDate, DateTimeFormatter.ISO_DATE::format);
        applyWithFormaterIfNonEmpty(projection.getNextCalibrationDate(), masterData::setNextCalibrationDate, DateTimeFormatter.ISO_DATE::format);
        applyIfNonEmpty(projection.getManufacturer(), masterData::setManufacturer);
        applyWithFormaterIfNonEmpty(projection.getDeliveryDate(), masterData::setDeliveryDate, DateTimeFormatter.ISO_DATE::format);
        return masterData;
    }

    private AdditionalIdNumbers projectionToAdditionalIds(PositionProjection projection) {
        val ids = Stream.of(
                createAdditionalId("Z01", projection.getPlantId().toString(), "plantId"),
                createAdditionalId("Z02", projection.getFormerBarcode(), "formerBarcode")
        ).flatMap(OptionalsUtils::optionalToStream)
                .collect(Collectors.toList());
        return new AdditionalIdNumbers(ids);
    }

    private Optional<AdditionalIdNumber> createAdditionalId(String type, String value, String remark) {
        return value == null || value.isEmpty() ? Optional.empty() : Optional.of(AdditionalIdNumber.builder().idType(type).value(value).remark(remark).build());
    }

    private String formatFrequencyUnit(IntervalUnit unit) {
        switch (unit) {
            case MONTH:
                return "MON";
            case DAY:
                return "DAY";
            case WEEK:
                return "WEE";
            case YEAR:
                return "ANN";
            default:
                return "";
        }
    }

    private HeaderSection projectionToHeader(OrderProjection order) {
        val now = new Date();
        return HeaderSection.builder()
                .id(order.getIdentifier())
                .creator("atlas")
                .date(dateFormat.format(now))
                .time(timeFormat.format(now))
                .language(Vdi2623Language.fromValue(LocaleContextHolder.getLocale().getLanguage()))
                .version(version)
                .buyer(projectionToBuyer(order))
                .supplier(projectionToSupplier(order))
                .build();
    }

    private Supplier projectionToSupplier(OrderProjection order) {
        val supplier = new Supplier();
        supplier.setId(order.getOrganizationSubdiv());
        supplier.setCompany(order.getOrganizationCompany());
        supplier.setStreet(streetFromProjection(order.getOrganizationAddr1(), order.getOrganizationAddr2(), order.getOrganizationAddr3()));
        supplier.setZipcode(order.getOrganizationZip());
        supplier.setCity(order.getOrganizationCity());
        supplier.setCountry(order.getOrganizationCountry());
        return supplier;
    }

    private Buyer projectionToBuyer(OrderProjection order) {
        val buyer = new Buyer();
        buyer.setId(order.getIdentifier());
        buyer.setCompany(order.getCompanyName());
        buyer.setStreet(streetFromProjection(order.getAddr1(), order.getAddr2(), order.getAddr3()));
        buyer.setCity(order.getCity());
        buyer.setCountry(order.getCountry());
        buyer.setContact(order.getContact());
        applyIfNonEmpty(order.getZip(), buyer::setZipcode);
        applyIfNonEmpty(order.getPhone(), buyer::setPhone);
        applyIfNonEmpty(order.getFax(), buyer::setFax);
        applyIfNonEmpty(order.getEmail(), buyer::setEmail);
        return buyer;
    }

    private String streetFromProjection(String addr1, String addr2, String addr3) {
        return Stream.of(addr3, addr2, addr1)
                .filter(StringUtils::isNoneBlank).findFirst().orElse("");
    }
}
