package org.trescal.cwms.core.vdi.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.batch.jobs.operationsimport.ImportedOperationItem;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatOperationTypeEnum;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.calibration.enums.CalibrationClass;
import org.trescal.cwms.core.jobs.calibration.form.ImportedOperationSynthesisForm;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.OptionalsUtils;
import org.trescal.cwms.core.vdi.data.AdditionalIdNumber;
import org.trescal.cwms.core.vdi.data.Order;
import org.trescal.cwms.core.vdi.data.Position;
import org.trescal.cwms.core.vdi.data.Result;
import org.trescal.cwms.core.vdi.data.VdiCalibration;
import org.trescal.cwms.core.workflow.entity.actionoutcometype.ActionOutcomeValue_Calibration;

import io.vavr.control.Either;
import lombok.val;

@Service
public class VdiImporter {

	@Autowired
	private JobService jobService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private ContactService contactService;

	private final SimpleDateFormat dateTimeParser = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");

	private Function<String, Either<String, JobItem>> findByBarcode() {
		return barcode -> OptionalsUtils
				.<String, JobItem>optionalToEither(() -> "Unable to load the active instrument for the barcode " + barcode)
				.apply(jobItemService.findActiveJobItemFromBarcodeOrFormerBarcode(barcode));
	}

	Either<String, VdiService.JobWithCertificates> readCertificatesFromOrder(Order order) {
		val job = jobService.findJobByJobNumber(order.getHeaderSection().getId())
				.<Either<String,Job>>map(Either::right)
				.orElse(Either.left("Filed to load the job of the id " + order.getHeaderSection().getId()));
		val certificates = order.getTechnicalSection().getPosition().stream().flatMap(this::readCertificateFromPosition)
				.map(Certificate::getCertno).collect(Collectors.toList());
		return job.map(j -> VdiService.JobWithCertificates.of(j.getJobid(), certificates));
	}

	private Stream<Certificate> readCertificateFromPosition(Position position) {
		val item = readPositionBarcode(position).flatMap(this.findByBarcode());
		return item
				.map(i -> position.getSupplierData().getCalibrations().getCalibration().stream()
						.map(this::readCertificateFromCalibration).flatMap(this::eitherToStream))
				.getOrElseGet(s -> Stream.empty());
	}

	private Either<String, Certificate> readCertificateFromCalibration(VdiCalibration vdiCalibration) {
		val personInCharge = contactService.searchContactByFullName(vdiCalibration.getResult().getPersonInCharge(),
				true);
		return personInCharge.map(con -> this.parseCertificate(vdiCalibration.getResult(), con));
	}

	private <T> Stream<T> eitherToStream(Either<?, T> o) {
		return o.fold(x -> Stream.empty(), Stream::of);
	}

	private Either<String, String> readPositionBarcode(Position position) {
		return position.getMasterData().getAdditionalIdNumbers().getAdditionalIdNumber().stream()
				.filter(i -> i.getIdType().equals("Z01") || i.getIdType().equals("Z02"))
				.map(AdditionalIdNumber::getValue).filter(StringUtils::isNotBlank)
				.findFirst().<Either<String, String>>map(Either::right)
				.orElseGet(() -> Either.left("Unable to find barcode of former barcode for position "
						+ position.getMasterData().getIdNumber()));
	}

	private Certificate parseCertificate(Result result, Contact con) {
		val cert = new Certificate();
		cert.setRegisteredBy(con);
		cert.setCertno(result.getCertificateNumber());
		cert.setRemarks(result.getRemark());
		cert.setThirdCertNo(result.getCertificateNumber());
		parseDate(result.getCalibrationCertificateCreatingDate()).peek(cert::setCertDate);
		return cert;
	}

	private Either<ParseException, Date> parseDateAndTime(String date, String time) {
		try {
			return Either.right(dateTimeParser.parse(date + 'T' + time));
		} catch (ParseException e) {
			return Either.left(e);
		}
	}

	private Either<ParseException, Date> parseDate(String date) {
		return parseDateAndTime(date, "00:00:00");
	}

	public Stream<Either<String, ImportedOperationSynthesisForm>> parseOrderToImportForm(Order order) {
		val rows = order.getTechnicalSection().getPosition().stream().flatMap(this::parsePositionToImportFormRows);
		return rows.map(e -> e.map(row -> {
			val builder = ImportedOperationSynthesisForm.builder();
			builder.jobId(row.getJobitem().getJob().getJobid());
			builder.rows(Collections.singletonList(row));
			return builder.build();
		}));
	}

	private Stream<Either<String, ImportedOperationItem>> parsePositionToImportFormRows(Position position) {
		val item = readPositionBarcode(position).flatMap(this.findByBarcode());
		val calibrations = position.getSupplierData().getCalibrations();
		return calibrations == null ? Stream.empty()
				: calibrations.getCalibration().stream().map(this::parseCalibrationToFormRow)
						.map(rowOrMessage -> item.flatMap(ji -> {
							rowOrMessage.peek(row -> {
								row.plantid(ji.getInst().getPlantid());
								row.jobitem(ji);
								row.intervalUnit(parseCalibrationIntervalUnit(position.getMasterData().getCalibrationIntervalUnit()));
								row.calibrationFrequency(position.getMasterData().getCalibrationInterval().intValue());
								row.contractReviewed(ji.getLastContractReviewBy()!=null);
								row.automaticContractReview(true);
								row.jobitemEquivalentId(String.valueOf(ji.getJobItemId()));
								row.jobitemId(ji.getJobItemId());
								row.operationType(ExchangeFormatOperationTypeEnum.CAL);
								if (ji.getNextWorkReq() != null)
									row.jiWrId(ji.getNextWorkReq().getId());

							});
							return rowOrMessage.map(ImportedOperationItem.ImportedOperationItemBuilder::build);
						}));
	}

	private IntervalUnit parseCalibrationIntervalUnit(String calibrationIntervalUnit) {
		switch (calibrationIntervalUnit){
			case "DAY":
				return IntervalUnit.DAY;
			case "WEE": return IntervalUnit.WEEK;
			case "MON": return IntervalUnit.MONTH;
			case "ANN": return IntervalUnit.YEAR;
			default: return null;
		}
	}

	private Either<String, ImportedOperationItem.ImportedOperationItemBuilder> parseCalibrationToFormRow(
			VdiCalibration vdiCalibration) {
		val personInCharge = contactService
				.searchContactByFullName(vdiCalibration.getResult().getPersonInCharge(), true)
				.mapLeft(msg -> msg + "Which is responsible for the calibration: "
						+ vdiCalibration.getResult().getCertificateNumber());
		val rowBuilder = ImportedOperationItem.builder().checked(true)
				.calibrationOutcome(ActionOutcomeValue_Calibration.SUCCESSFUL);

		return personInCharge.map(person -> {
			rowBuilder.operationByHrId(person.getHrid());
			rowBuilder.operationBy(person);
			rowBuilder.operationValidatedByHrId(person.getHrid());
			val result = vdiCalibration.getResult();
			rowBuilder.calibrationClass(CalibrationClass.AS_FOUND_AND_AS_LEFT).operationDuration(0);
			parseDateAndTime(result.getCalibrationDate(), result.getCalibrationTime())
					.peek(rowBuilder::operationStartDate);
			rowBuilder.documentNumber(result.getCertificateNumber());
			val status = parseCalibrationVerificationStatus(result.getCondition());
			status.ifPresent(s -> {
				rowBuilder.calibrationVerificationStatus( s._1() );
				rowBuilder.restiction(s._2());
			});
			parseDate(result.getCalibrationCertificateCreatingDate()).peek(rowBuilder::operationDate);
			return rowBuilder;
		});

	}

	private Optional<Tuple2<CalibrationVerificationStatus,Boolean>>  parseCalibrationVerificationStatus(String condition) {
		Function<String,Tuple2<CalibrationVerificationStatus,Boolean>> innerSwitchFunction =
		(c) -> {switch (c){
			case "000": return Tuple.of(CalibrationVerificationStatus.NOT_APPLICABLE,false);
			case "001": return Tuple.of(CalibrationVerificationStatus.ACCEPTABLE,false);
			case "002": return Tuple.of(CalibrationVerificationStatus.NOT_ACCEPTABLE,false);
			case "003":
				return Tuple.of(CalibrationVerificationStatus.PROBABLY_PASS,false);
			case "004": return Tuple.of(CalibrationVerificationStatus.PROBABLY_FAIL,false);
			case "005": return Tuple.of(CalibrationVerificationStatus.ACCEPTABLE,true);
			default:return null;
		}};
		return Optional.ofNullable(innerSwitchFunction.apply(condition));
	}
}
