package org.trescal.cwms.core.vdi.service;

import java.util.function.Function;

public interface Formatter<A> extends Function<A,String> {
}
