/*package org.trescal.cwms.core.events.change.queue.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;
import org.trescal.cwms.core.events.change.queue.ChangeQueue;
import org.trescal.cwms.core.events.change.queue.enums.ChangeQueueDTO;

public interface ChangeQueueService extends BaseService<ChangeQueue, Integer> {
	
	public List<ChangeQueueDTO> findNNotProcessedChangeQueueItems(int size, List<ChangeEntity> changeEntities);
	
	public List<ChangeQueue> findNNotProcessedChangeQueue(int size);
}
*/