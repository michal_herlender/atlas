
package org.trescal.cwms.core.events.repository;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscriber;


/**
 * @Author shazil.khan
 *
 */
public interface EventSubscriberDao extends BaseDao<EventSubscriber, Integer>
{
/**
	 * @param name
	 * @return
	 */

	EventSubscriber findByName(String name);
	
	List<EventSubscriber> getAllSubscribersEager();
	

}

