package org.trescal.cwms.core.events.change.queue;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.events.change.enums.ChangeEntity;
import org.trescal.cwms.core.events.change.enums.ChangeType;

import lombok.Setter;

/**
 * Defines a master queue record which contains changes recorded in sequential order  
 * 
 * This is a prototype design not intended for production use; subject to modification / review / deletion
 *
 * TODO : Indexes
 * TODO : Revisit identifier generation strategy for concurrent use (sequence?) 
 * TODO : Queue design may change based on the change capture strategy chosen
 */
@Entity
@Table(name="change_queue", indexes={@Index(columnList="processed", name="IDX_change_queue_processed")})
@Setter
public class ChangeQueue {
	private Integer id;
	private ChangeEntity entityType;
	private Boolean processed;
	private String payload;
	private Integer changeType;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	
	@NotNull
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "entitytype", nullable = false)
	public ChangeEntity getEntityType() {
		return entityType;
	}
	
	@NotNull
	@Column(name = "processed", nullable = false, columnDefinition="bit")
	public Boolean getProcessed() {
		return processed;
	}
	
	@NotNull
	@Column(name = "payload", nullable = false, columnDefinition="varchar(max)")
	public String getPayload() {
		return payload;
	}

	@Column(name = "changetype", nullable = false)
	public Integer getChangeType() {
		return changeType;
	}
	
	@Transient
	public ChangeType getChangeTypeEnum() {
		// changeType stored in ChangeQueue corresponds to CDC operation type
		switch (changeType) {
		case 1: return ChangeType.DELETE;
		case 2: return ChangeType.PUSH;
		default: return ChangeType.UPDATE;
		}
	}
	
	 
	
}
