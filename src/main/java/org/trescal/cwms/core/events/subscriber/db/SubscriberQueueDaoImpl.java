/*package org.trescal.cwms.core.events.subscriber.db;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CompoundSelection;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.events.change.queue.ChangeQueue;
import org.trescal.cwms.core.events.change.queue.ChangeQueue_;
import org.trescal.cwms.core.events.subscriber.dto.SubscriberEventDTO;
import org.trescal.cwms.core.events.subscriber.queue.SubscriberQueue;
import org.trescal.cwms.core.events.subscriber.queue.SubscriberQueue_;
import org.trescal.cwms.core.tools.PagedResultSet;

@Repository
public class SubscriberQueueDaoImpl extends BaseDaoImpl<SubscriberQueue, Integer> implements SubscriberQueueDao {

	@Override
	protected Class<SubscriberQueue> getEntity() {
		return SubscriberQueue.class;
	}

	@Override
	public void getEventDTOs(Integer subscriberId, Boolean processed, PagedResultSet<SubscriberEventDTO> prs) {
		super.completePagedResultSet(prs, SubscriberEventDTO.class, cb -> cq -> {
			Root<SubscriberQueue> root = cq.from(SubscriberQueue.class);
			Join<SubscriberQueue, ChangeQueue> change = root.join(SubscriberQueue_.queueEvent, JoinType.INNER);
			
			cq.where(cb.equal(root.get(SubscriberQueue_.subscriber), subscriberId), cb.equal(root.get(SubscriberQueue_.processed), processed));
			
			CompoundSelection<SubscriberEventDTO> selection = cb.construct(SubscriberEventDTO.class,
					root.get(SubscriberQueue_.id), change.get(ChangeQueue_.id), change.get(ChangeQueue_.entityType));
			
			// Integer changeEventId, ChangeType eventType, ChangeEntity entityType

			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(SubscriberQueue_.id)));
			
			return Triple.of(root, selection, order);
		});
	}

	@Override
	public List<SubscriberQueue> getSubscriberQueueEventsById(List<Integer> ids) {
		return getResultList(cb -> {
			CriteriaQuery<SubscriberQueue> cq = cb.createQuery(SubscriberQueue.class);
			Root<SubscriberQueue> root = cq.from(SubscriberQueue.class);
			cq.where(root.get(SubscriberQueue_.id).in(ids));
			
			return cq;
		});
	}
	
}
*/