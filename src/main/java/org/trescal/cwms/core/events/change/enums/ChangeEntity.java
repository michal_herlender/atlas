package org.trescal.cwms.core.events.change.enums;

import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.cdc.entity.certificate.CertLinkCT;
import org.trescal.cwms.core.cdc.entity.certificate.CertificateCT;
import org.trescal.cwms.core.cdc.entity.certificate.InstCertLinkCT;
import org.trescal.cwms.core.cdc.entity.company.AddressCT;
import org.trescal.cwms.core.cdc.entity.company.CompanyCT;
import org.trescal.cwms.core.cdc.entity.company.CompanySettingsCT;
import org.trescal.cwms.core.cdc.entity.company.ContactCT;
import org.trescal.cwms.core.cdc.entity.company.DepartmentCT;
import org.trescal.cwms.core.cdc.entity.company.DepartmentMemberCT;
import org.trescal.cwms.core.cdc.entity.company.LocationCT;
import org.trescal.cwms.core.cdc.entity.company.SubdivCT;
import org.trescal.cwms.core.cdc.entity.company.SubdivSettingsCT;
import org.trescal.cwms.core.cdc.entity.defaultdata.SystemDefaultCT;
import org.trescal.cwms.core.cdc.entity.defaultdata.SystemdDefaultApplicationCT;
import org.trescal.cwms.core.cdc.entity.instruction.BaseInstructionCT;
import org.trescal.cwms.core.cdc.entity.instruction.CompanyInstructionLinkCT;
import org.trescal.cwms.core.cdc.entity.instruction.ContactInstructionLinkCT;
import org.trescal.cwms.core.cdc.entity.instruction.JobInstructionLinkCT;
import org.trescal.cwms.core.cdc.entity.instruction.SubdivInstructionLinkCT;
import org.trescal.cwms.core.cdc.entity.instrument.CalReqCT;
import org.trescal.cwms.core.cdc.entity.instrument.InstrumentCT;
import org.trescal.cwms.core.cdc.entity.instrument.InstrumentRangeCT;
import org.trescal.cwms.core.cdc.entity.instrument.InstrumentUsageTypeCT;
import org.trescal.cwms.core.cdc.entity.invoice.CreditNoteCT;
import org.trescal.cwms.core.cdc.entity.invoice.InvoiceCT;
import org.trescal.cwms.core.cdc.entity.job.EngineerAllocationCT;
import org.trescal.cwms.core.cdc.entity.job.ItemStateCT;
import org.trescal.cwms.core.cdc.entity.job.JobCT;
import org.trescal.cwms.core.cdc.entity.job.JobItemCT;
import org.trescal.cwms.core.cdc.entity.job.JobItemGroupCT;
import org.trescal.cwms.core.cdc.entity.job.OnBehalfItemCT;
import org.trescal.cwms.core.cdc.entity.job.UpcomingWorkCT;
import org.trescal.cwms.core.cdc.entity.job.UpcomingWorkJobLinkCT;
import org.trescal.cwms.core.cdc.entity.referential.BrandCT;
import org.trescal.cwms.core.cdc.entity.referential.CountryCT;
import org.trescal.cwms.core.cdc.entity.referential.InstrumentModelCT;
import org.trescal.cwms.core.cdc.entity.referential.InstrumentModelTranslationCT;
import org.trescal.cwms.core.cdc.entity.referential.ServiceTypeCT;
import org.trescal.cwms.core.cdc.entity.referential.SubFamilyCT;
import org.trescal.cwms.core.cdc.entity.referential.UomCT;
import org.trescal.cwms.core.cdc.entity.workinstruction.InstrumentWorkInstructionCT;
import org.trescal.cwms.core.cdc.entity.workinstruction.WorkInstructionCT;

/**
 * Defines different types of entities which we track CDC (change data capture data) for 
 *
 * IMPORTANT NOTE : Enum order is important and should typically not be modified; 
 * ordinal mappings are used for performance/size, e.g. in ChangeQueue record
 *
 */
public enum ChangeEntity {
	
	// GROUP 1 - REFRENTIAL DATA
	BRAND("dbo_mfr", BrandCT.class),									// 0
	COUNTRY("dbo_country", CountryCT.class),							// 1
	SERVICE_TYPE("dbo_servicetype", ServiceTypeCT.class),				// 2
	SUB_FAMILY("dbo_description", SubFamilyCT.class),					// 3
	INSTRUMENT_MODEL("dbo_instmodel", InstrumentModelCT.class),			// 4
	INSTRUMENT_MODEL_TRANSLATION("dbo_instmodeltranslation", InstrumentModelTranslationCT.class),	// 5
	
	// GROUP 2 - COMPANY DATA
	COMPANY("dbo_company", CompanyCT.class),							// 6
	COMPANY_SETTINGS("dbo_companysettingsforallocatedcompany", CompanySettingsCT.class),	// 7
	SUBDIV("dbo_subdiv", SubdivCT.class),								// 8
	ADDRESS("dbo_address", AddressCT.class),							// 9
	LOCATION("dbo_location", LocationCT.class),							// 10
	CONTACT("dbo_contact", ContactCT.class),							// 11
	DEPARTMENT("dbo_department", DepartmentCT.class),					// 12
	DEPARTMENT_MEMBER("dbo_departmentmember", DepartmentMemberCT.class),// 13
	
	// GROUP 3 - INSTRUMENT DATA
	INSTRUMENT("dbo_instrument", InstrumentCT.class),					// 14
	
	// GROUP 4 - JOB DATA
	ITEM_STATE("dbo_itemstate", ItemStateCT.class),						// 15
	JOB("dbo_job", JobCT.class),										// 16
	JOB_ITEM("dbo_jobitem", JobItemCT.class),							// 17
	ENGINEER_ALLOCATION("dbo_engineerallocation", EngineerAllocationCT.class),	// 18
	CAL_REQ("dbo_calreq", CalReqCT.class),								// 19
	UPCOMING_WORK("dbo_upcomingwork",UpcomingWorkCT.class),				// 20
	UPCOMING_WORK_JOB_LINK("dbo_upcomingworkjoblink", UpcomingWorkJobLinkCT.class),	// 21
	
	// GROUP 5 -INSTRUCTION DATA
	BASE_INSTRUCTION("dbo_baseinstruction", BaseInstructionCT.class),	// 22
	COMPANY_INSTRUCTION_LINK("dbo_companyinstructionlink", CompanyInstructionLinkCT.class),	// 23
	SUBDIV_INSTRUCTION_LINK("dbo_subdivinstructionlink", SubdivInstructionLinkCT.class),	// 24
	CONTACT_INSTRUCTION_LINK("dbo_contactinstructionlink", ContactInstructionLinkCT.class),	// 25
	JOB_INSTRUCTION_LINK("dbo_jobinstructionlink", JobInstructionLinkCT.class),				// 26
	
	// GROUP 6 - SYSTEM DEFAULT DATA
	SYSTEM_DEFAULT("dbo_systemdefault", SystemDefaultCT.class ),		// 27
	SYSTEM_DEFAULT_APPLICATION("dbo_systemdefaultapplication",SystemdDefaultApplicationCT.class),	// 28
	
	// GROUP 7 - WORK INSTRUCTION DATA
	WORK_INSTRUCTION("dbo_workinstruction", WorkInstructionCT.class),	// 29
	INSTRUMENT_WORK_INSTRUCTION("dbo_instrumentworkinstruction",InstrumentWorkInstructionCT.class),	// 30
	
	// GROUP 8 - CERTIFICATE
	CERTIFICATE("dbo_certificate",CertificateCT.class), 				// 31
	INST_CERT_LINK("dbo_instcertlink",InstCertLinkCT.class),			// 32
	CERT_LINK("dbo_certlink",CertLinkCT.class),							// 33
	
	// Additional tables added after original release, before invoice development
	UOM("dbo_uom",UomCT.class),														// 34
	INSTRUMENT_RANGE("dbo_instrumentrange",InstrumentRangeCT.class),				// 35
	JOB_ITEM_GROUP("dbo_jobitemgroup",JobItemGroupCT.class),						// 36
	ON_BEHALF_OF_ITEM("dbo_onbehalfitem",OnBehalfItemCT.class), 					// 37
	INSTRUMENT_USAGE_TYPE("dbo_instrumentusagetype",InstrumentUsageTypeCT.class),	// 38
	SUBDIV_SETTINGS("dbo_subdivsettingsforallocatedsubdiv",SubdivSettingsCT.class),	// 39
	
	// GROUP 9 - INVOICE (added after all above)
	INVOICE("dbo_invoice",InvoiceCT.class),											// 40
	CREDIT_NOTE("dbo_creditnote",CreditNoteCT.class);								// 41
	
	// Add any new CDC tables below and not inserted above, to preserve enum ordinal mapping!

	private String tableName;
	private Class<? extends EntityCT> clazz;

	private ChangeEntity(String tableName, Class<? extends EntityCT> clazz) {
		this.tableName = tableName;
		this.clazz = clazz;
	}

	public String getTableName() {
		return tableName;
	}

	public Class<? extends EntityCT> getClazz()
	{
		return this.clazz;
	}
}
