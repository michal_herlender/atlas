/*package org.trescal.cwms.core.events.change.queue.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;
import org.trescal.cwms.core.events.change.queue.ChangeQueue;
import org.trescal.cwms.core.events.change.queue.ChangeQueue_;
import org.trescal.cwms.core.events.change.queue.enums.ChangeQueueDTO;

import lombok.val;

@Repository("ChangeQueueDao")
public class ChangeQueueDaoImpl extends BaseDaoImpl<ChangeQueue, Integer> implements ChangeQueueDao {

	@Override
	protected Class<ChangeQueue> getEntity() {
		return ChangeQueue.class;
	}

	@Override
	public List<ChangeQueueDTO> findNNotProcessedChangeQueueItems(int size, List<ChangeEntity> changeEntities) {
		return getResultList(cb -> {
			val cq = cb.createQuery(ChangeQueueDTO.class);
			val root = cq.from(ChangeQueue.class);
			
			val clauses = cb.conjunction();
			clauses.getExpressions().add(cb.isFalse(root.get(ChangeQueue_.processed)));
			if(CollectionUtils.isNotEmpty(changeEntities))
				clauses.getExpressions().add(root.get(ChangeQueue_.entityType).in(changeEntities));
			cq.where(clauses);
			cq.select(cb.construct(ChangeQueueDTO.class, root.get(ChangeQueue_.id),
					root.get(ChangeQueue_.entityType),
					root.get(ChangeQueue_.payload),
					root.get(ChangeQueue_.changeType)));
			cq.orderBy(cb.asc(root.get(ChangeQueue_.id)));
			return cq;
		}, 0, size);
	}
	
	@Override
	public List<ChangeQueue> findNNotProcessedChangeQueue(int size) {
		return getResultList(cb -> {
			CriteriaQuery<ChangeQueue> cq = cb.createQuery(ChangeQueue.class);
			Root<ChangeQueue> root = cq.from(ChangeQueue.class);
			
			cq.where(cb.isFalse(root.get(ChangeQueue_.processed)));
			cq.orderBy(cb.asc(root.get(ChangeQueue_.id)));
			return cq;
		}, 0, size);
	}

}
*/