/**
 * 
 */
package org.trescal.cwms.core.events.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.events.repository.EventSubscriptionDao;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscription;

/**
 * @Author shazil.khan
 *
 */
@Service("EventSubscriptionService")
public class EventSubscriptionServiceImpl extends BaseServiceImpl<EventSubscription, Integer> implements EventSubscriptionService {

	
	
	@Autowired
	private EventSubscriptionDao eventSubscriptionDao;
	/* 
	 * @see org.trescal.cwms.core.audit.entity.db.BaseServiceImpl#getBaseDao()
	 */
	@Override
	protected BaseDao<EventSubscription, Integer> getBaseDao() {
		return eventSubscriptionDao;
	}

	
}
