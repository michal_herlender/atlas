/**
 * 
 */
package org.trescal.cwms.core.events.service;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscription;

/**
 * @Author shazil.khan
 *
 */
public interface EventSubscriptionService extends BaseService<EventSubscription, Integer> {

}
