/*package org.trescal.cwms.core.events.subscriber.repository.subscriber;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscriber;

public interface SubscriberService extends BaseService<EventSubscriber, Integer> {
	
	*//**
	 * Returns all EventSubscriber entities with their EventSubscriptions eagerly fetched 
	 * This prevents a lazy load for each subscriber's EventSubscriptions, when needed
	 * @return
	 *//*
	public List<EventSubscriber> getAllSubscribersEager();
}
*/