/**
 * 
 */
package org.trescal.cwms.core.events.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;
import org.trescal.cwms.core.events.change.queue.ChangeQueue;
import org.trescal.cwms.core.events.change.queue.enums.ChangeQueueDTO;
import org.trescal.cwms.core.events.repository.ChangeQueueDao;

@Service
public class ChangeQueueServiceImpl extends BaseServiceImpl<ChangeQueue, Integer> implements ChangeQueueService {

	@Autowired
	private ChangeQueueDao changeQueueDao;

	@Override
	protected BaseDao<ChangeQueue, Integer> getBaseDao() {
		return this.changeQueueDao;
	}

	@Override
	public List<ChangeQueueDTO> findNNotProcessedChangeQueueItems(int size, List<ChangeEntity> changeEntities) {
		return this.changeQueueDao.findNNotProcessedChangeQueueItems(size, changeEntities);
	}

	public List<ChangeQueue> findNNotProcessedChangeQueue(int size) {
		return this.changeQueueDao.findNNotProcessedChangeQueue(size);
	}

	@Override
	public List<ChangeQueue> findNNotProcessedElements(int size, List<ChangeEntity> changeEntities) {
		// TODO Auto-generated method stub
		return changeQueueDao.findNNotProcessedElements(size, changeEntities);
	}
}
