package org.trescal.cwms.core.events.subscriber.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.events.change.enums.ChangeEntity;

import lombok.Setter;

@Entity
@Table(name="event_subscription")
@Setter
public class EventSubscription {
	private Integer id;
	private EventSubscriber subscriber;
	private ChangeEntity entityType;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	
	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="subscriberid", nullable=false, foreignKey=@ForeignKey(name="FK_event_subscription_subscriberid"))
	public EventSubscriber getSubscriber() {
		return subscriber;
	}
	
	@NotNull
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "entitytype", nullable = false)
	public ChangeEntity getEntityType() {
		return entityType;
	}
	
	
}
