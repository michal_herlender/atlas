package org.trescal.cwms.core.events.repository;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.events.subscriber.dto.SubscriberEventDTO;
import org.trescal.cwms.core.events.subscriber.dto.SubscriberQueueDTO;
import org.trescal.cwms.core.events.subscriber.entity.SubscriberQueue;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface SubscriberQueueDao extends BaseDao<SubscriberQueue, Integer> {
	void getEventDTOs(Integer subscriberId, Boolean processed, PagedResultSet<SubscriberEventDTO> prs);
	List<SubscriberQueue> getSubscriberQueueEventsById(List<Integer> ids);
	List<SubscriberQueueDTO> getSubscriberQueueEventsProjectionById(List<Integer> ids);
}
