/*package org.trescal.cwms.core.events.subscriber.repository.subscriber;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscriber;

@Service
public class SubscriberServiceImpl extends BaseServiceImpl<EventSubscriber, Integer>
		implements SubscriberService {
	@Autowired
	private SubscriberDao baseDao;
	
	@Override
	protected BaseDao<EventSubscriber, Integer> getBaseDao() {
		return baseDao;
	}
	
	@Override
	public List<EventSubscriber> getAllSubscribersEager() {
		return baseDao.getAllSubscribersEager();
	}
	
}
*/