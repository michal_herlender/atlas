/*package org.trescal.cwms.core.events.subscriber.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.events.subscriber.dto.SubscriberEventDTO;
import org.trescal.cwms.core.events.subscriber.queue.SubscriberQueue;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.events.dto.SubscriberEventUpdateInput;

public interface SubscriberQueueService extends BaseService<SubscriberQueue, Integer> {
	
	*//**
	 * Returns a paged result set (oldest events first) for the specific subscriber indicated
	 * 
	 * @param subscriberId (the subscriber to retrieve events for - currently just an ID)
	 * @param processed (optional - value of the "processed" attribute) 
	 * @param prs (the paged result set)
	 * @param populateEventDetails (whether to populate event details for result page based on the event content)
	 *//*
	void getEvents(Integer subscriberId, Boolean processed, PagedResultSet<SubscriberEventDTO> prs, Boolean populateEventDetails);
	
	List<SubscriberQueue> getSubscriberQueueEventsById(List<Integer> ids);
	
	void performUpdate (SubscriberEventUpdateInput input);
}
*/