package org.trescal.cwms.core.events.change.queue.enums;

import java.io.Serializable;

import org.trescal.cwms.core.events.change.enums.ChangeEntity;
import org.trescal.cwms.core.events.change.enums.ChangeType;
import org.trescal.cwms.rest.events.tools.PayloadSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ChangeQueueDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private ChangeEntity entityType;
	@JsonSerialize(using = PayloadSerializer.class)
	private String payload;
	private ChangeType changeType;
	
	public ChangeQueueDTO(Integer id, ChangeEntity entityType, String payload, ChangeType changeType) {
		super();
		this.id = id;
		this.entityType = entityType;
		this.payload = payload;
		this.changeType = changeType;
	}

}
