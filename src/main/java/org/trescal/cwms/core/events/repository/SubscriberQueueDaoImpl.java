package org.trescal.cwms.core.events.repository;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.events.change.queue.ChangeQueue;
import org.trescal.cwms.core.events.change.queue.ChangeQueue_;
import org.trescal.cwms.core.events.subscriber.dto.SubscriberEventDTO;
import org.trescal.cwms.core.events.subscriber.dto.SubscriberQueueDTO;
import org.trescal.cwms.core.events.subscriber.entity.SubscriberQueue;
import org.trescal.cwms.core.events.subscriber.entity.SubscriberQueue_;
import org.trescal.cwms.core.tools.PagedResultSet;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SubscriberQueueDaoImpl extends BaseDaoImpl<SubscriberQueue, Integer> implements SubscriberQueueDao {

	@Override
	protected Class<SubscriberQueue> getEntity() {
		return SubscriberQueue.class;
	}

	@Override
	public void getEventDTOs(Integer subscriberId, Boolean processed, PagedResultSet<SubscriberEventDTO> prs) {
		super.completePagedResultSet(prs, SubscriberEventDTO.class, cb -> cq -> {
			Root<SubscriberQueue> root = cq.from(SubscriberQueue.class);
			Join<SubscriberQueue, ChangeQueue> change = root.join(SubscriberQueue_.queueEvent, JoinType.INNER);

			cq.where(cb.equal(root.get(SubscriberQueue_.subscriber), subscriberId),
					cb.equal(root.get(SubscriberQueue_.processed), processed));

			CompoundSelection<SubscriberEventDTO> selection = cb.construct(SubscriberEventDTO.class,
					root.get(SubscriberQueue_.id), 
					change.get(ChangeQueue_.id),
					change.get(ChangeQueue_.changeType),
					change.get(ChangeQueue_.entityType),
					change.get(ChangeQueue_.payload));

			// Integer eventId, Integer changeEventId, ChangeType changeType, Integer entityType, String payload

			List<Order> order = new ArrayList<>();
			order.add(cb.asc(root.get(SubscriberQueue_.id)));

			return Triple.of(root, selection, order);
		});
	}

	@Override
	public List<SubscriberQueue> getSubscriberQueueEventsById(List<Integer> ids) {
		return getResultList(cb -> {
			CriteriaQuery<SubscriberQueue> cq = cb.createQuery(SubscriberQueue.class);
			Root<SubscriberQueue> root = cq.from(SubscriberQueue.class);
			cq.where(root.get(SubscriberQueue_.id).in(ids));

			return cq;
		});
	}

	@Override
	public List<SubscriberQueueDTO> getSubscriberQueueEventsProjectionById(List<Integer> ids) {
		return getResultList(cb -> {
			CriteriaQuery<SubscriberQueueDTO> cq = cb.createQuery(SubscriberQueueDTO.class);
			Root<SubscriberQueue> root = cq.from(SubscriberQueue.class);
			cq.where(root.get(SubscriberQueue_.id).in(ids));
			cq.select(cb.construct(SubscriberQueueDTO.class, root.get(SubscriberQueue_.id),
					root.get(SubscriberQueue_.queueEvent).get(ChangeQueue_.id)));
			return cq;
		});
	}

}
