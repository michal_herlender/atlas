package org.trescal.cwms.core.events.service;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscriber;


/**
 * @Author shazil.khan
 *
 */
public interface EventSubscriberService extends BaseService<EventSubscriber, Integer>
{

/**
	 * @param name
	 * @return
	 */

	EventSubscriber findByName(String name);
	
	/**
	 * Returns all EventSubscriber entities with their EventSubscriptions eagerly fetched 
	 * This prevents a lazy load for each subscriber's EventSubscriptions, when needed
	 * @return
	 */
	public List<EventSubscriber> getAllSubscribersEager();
	

}

