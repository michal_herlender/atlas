package org.trescal.cwms.core.events.restcontrollers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.events.change.queue.ChangeQueue;
import org.trescal.cwms.core.events.service.ChangeQueueService;
import org.trescal.cwms.core.events.service.EventSubscriberService;
import org.trescal.cwms.core.events.service.SubscriberQueueService;
/*import org.trescal.cwms.core.events.change.queue.db.ChangeQueueService;*/
import org.trescal.cwms.core.events.subscriber.dto.SubscriberQueueDTO;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscriber;
import org.trescal.cwms.core.events.subscriber.entity.SubscriberQueue;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@Controller
public class SubscriberEventConfirmReceiptController {
	
	@Autowired
	private EventSubscriberService subscriberService;
	@Autowired
	private ChangeQueueService changeQueueService;
	@Autowired
	private SubscriberQueueService subscriberQueueService;

	@RequestMapping(path="/api/events/confirmreceipt", method=RequestMethod.POST)
	@ResponseBody
	public RestResultDTO<List<SubscriberQueueDTO>> confirmReceiptEvents(
			@RequestParam(name="subscriberId", required=true) Integer subscriberId,
			@RequestParam(name="eventIds", required=true) List<Integer> eventIds) {
		
		EventSubscriber es = this.subscriberService.get(subscriberId);
		if(es == null)
			return new RestResultDTO<List<SubscriberQueueDTO>>(false, "Subscriber not found");
		
		if(CollectionUtils.isEmpty(eventIds))
			return new RestResultDTO<List<SubscriberQueueDTO>>(false, "Subscriber not found");
		
		List<Integer> subsciberQueueIds = new ArrayList<>();
		eventIds.forEach(e -> {
			ChangeQueue cq = this.changeQueueService.get(e);
			if(cq != null) {
				cq.setProcessed(true);
				this.changeQueueService.merge(cq);
				
				SubscriberQueue sq = new SubscriberQueue();
				sq.setProcessed(false);
				sq.setQueueEvent(cq);
				sq.setSubscriber(es);
				this.subscriberQueueService.save(sq);
				subsciberQueueIds.add(sq.getId());
			}
		});
		
		RestResultDTO<List<SubscriberQueueDTO>> result = new RestResultDTO<List<SubscriberQueueDTO>>(true, "");
		result.addResult(this.subscriberQueueService.getSubscriberQueueEventsProjectionById(subsciberQueueIds));
		
		return result;
	}
}
