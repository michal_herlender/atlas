package org.trescal.cwms.core.events.change.enums;

/**
 * Defines different types of changes which may be registered  
 * 
 * This is a prototype design not intended for production use; subject to modification / review / deletion
 *
 * Note : Enum order is important and should typically not be modified; ordinal mappings are used for performance/size
 */
public enum ChangeType {
	PUSH, //Remove
	DELETE,
	CREATE,
	UPDATE
}
