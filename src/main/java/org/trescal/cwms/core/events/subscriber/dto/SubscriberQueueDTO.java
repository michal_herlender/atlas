package org.trescal.cwms.core.events.subscriber.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SubscriberQueueDTO {
	
	private Integer queueId;
	private Integer changeQueueId;
	
	public SubscriberQueueDTO(Integer queueId, Integer changeQueueId) {
		super();
		this.queueId = queueId;
		this.changeQueueId = changeQueueId;
	}
	
	
}
