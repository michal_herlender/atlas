/**
 * 
 */
package org.trescal.cwms.core.events.repository;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscription;

/**
 * @Author shazil.khan
 *
 */
public interface EventSubscriptionDao extends BaseDao<EventSubscription, Integer> {

}
