package org.trescal.cwms.core.events.subscriber.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.events.change.queue.ChangeQueue;

import lombok.Setter;

@Entity
@Table(name="subscriber_queue", indexes={@Index(columnList="processed, subscriberid", name="IDX_subscriber_queue_processed_subscriberid")})
@Setter
public class SubscriberQueue {
	private Integer id;
	private EventSubscriber subscriber;
	private ChangeQueue queueEvent;
	private Boolean processed;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	@NotNull
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="subscriberid", nullable=false, foreignKey=@ForeignKey(name="FK_subscriber_queue_subscriberid"))
	public EventSubscriber getSubscriber() {
		return subscriber;
	}
	
	/**
	 * Intentionally not bidirectional. 
	 */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="changeid", nullable=false, foreignKey=@ForeignKey(name="FK_subscriber_queue_changeid"))
	public ChangeQueue getQueueEvent() {
		return queueEvent;
	}
	
	@NotNull
	@Column(name = "processed", nullable = false, columnDefinition="bit")
	public Boolean getProcessed() {
		return processed;
	}
	
}
