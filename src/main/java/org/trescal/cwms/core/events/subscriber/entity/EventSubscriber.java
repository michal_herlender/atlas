package org.trescal.cwms.core.events.subscriber.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Setter;

/*@Data*/
@Entity
@Table(name="event_subscriber")
@Setter
//1.Entity
public class EventSubscriber {
	private Integer id;
	private String description;
	private List<EventSubscription> eventSubscriptions;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	
	@NotNull
	@Length(min = 1, max = 100)
	@Column(name = "model", length = 100, nullable = false, columnDefinition="varchar(100)")
	public String getDescription() {
		return description;
	}
	
	@OneToMany(cascade = { CascadeType.ALL },fetch = FetchType.LAZY, mappedBy = "subscriber")
	public List<EventSubscription> getEventSubscriptions(){
		return eventSubscriptions;
	} 
}
