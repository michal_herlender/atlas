package org.trescal.cwms.core.events.service;
/*


package org.trescal.cwms.core.events.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.events.repository.SubscriberRepository;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscriber;



/**
 * @Author shazil.khan
 */

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.events.repository.EventSubscriberDao;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscriber;

//3.Implementation
@Service("SubscriberService")
public class EventSubscriberServiceImpl extends BaseServiceImpl<EventSubscriber, Integer> implements EventSubscriberService {

	@Autowired
	private EventSubscriberDao subscriberdao;
	
/*	@Autowired
	private EventSubscriberService eventSubscriberService;*/
	
	@Override
	public EventSubscriber findByName(String name) {
		return subscriberdao.findByName(name);
	}

	@Override
	protected BaseDao<EventSubscriber, Integer> getBaseDao() {
		return subscriberdao;
	}
	
	public void save(EventSubscriber subscriber){
		 subscriberdao.persist(subscriber);
	}
	
	@Override
	public List<EventSubscriber> getAllSubscribersEager() {
		return subscriberdao.getAllSubscribersEager();
	}
	
	

	/* (
	 * @see org.trescal.cwms.core.events.service.SubscriberService#get(int)
	 */
/*	@Override
	public EventSubscriber get(int id) {
		// TODO Auto-generated method stub
		return subscriberService.get(id);
		
	}*/

	/*@Autowired
	private SubscriberService eventSubscriberRepository;
		
	@Autowired
	private SubscriberServiceImpl subscriberService;
	

	
	
	public EventSubscriber getEventSubscriberById(int id){
		return eventSubscriberRepository.findOne(id);
	}
	
	public List<EventSubscriber> getEventSubscribers(){
		return eventSubscriberRepository.findAll();
	}
	
	public EventSubscriber getEventSubscriberByName(String name){
		return eventSubscriberRepository.findByName(name);
	}
	
	public String deleteEventSubscriber(int id){
		 eventSubscriberRepository.delete(id);
		 return "Product is deleted from repository !" +""+id;
	}
	
	public EventSubscriber updateSubscriber(EventSubscriber subscriber){
		EventSubscriber existingSubscriber = eventSubscriberRepository.findOne(subscriber.getId());
		existingSubscriber.setDescription(subscriber.getDescription());
		return eventSubscriberRepository.save(existingSubscriber);
	}
	
	*/
	
}


