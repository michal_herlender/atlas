package org.trescal.cwms.core.events.service;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.events.repository.SubscriberQueueDao;
import org.trescal.cwms.core.events.subscriber.dto.SubscriberEventDTO;
import org.trescal.cwms.core.events.subscriber.dto.SubscriberQueueDTO;
import org.trescal.cwms.core.events.subscriber.entity.SubscriberQueue;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.events.dto.SubscriberEventUpdateInput;

@Service
public class SubscriberQueueServiceImpl extends BaseServiceImpl<SubscriberQueue, Integer>
		implements SubscriberQueueService {
	@Autowired
	private SubscriberQueueDao baseDao;
	
	// For now set a limit of no more than 100 events
	public static int RESULTS_PER_PAGE_LIMIT = 100;

	@Override
	protected BaseDao<SubscriberQueue, Integer> getBaseDao() {
		return baseDao;
	}
	
	@Override
	public void getEvents(Integer subscriberId, Boolean processed, PagedResultSet<SubscriberEventDTO> prs) {
		if (prs.getResultsPerPage() > RESULTS_PER_PAGE_LIMIT) {
			// To limit the JSON result size, prevent IN clause limit, and prevent (statistically) too many different event types 
			throw new UnsupportedOperationException("Results per page must be no more than "+RESULTS_PER_PAGE_LIMIT+ " but was "+prs.getResultsPerPage());
		}
		baseDao.getEventDTOs(subscriberId, processed, prs);
	}
	
	@Override
	public List<SubscriberQueue> getSubscriberQueueEventsById(List<Integer> ids) {
		List<SubscriberQueue> dtos = Collections.emptyList();
		if (!ids.isEmpty()) {
			dtos = baseDao.getSubscriberQueueEventsById(ids);
		}
		return dtos;
	}

	@Override
	public void performUpdate(SubscriberEventUpdateInput input) {
        // These should (and are) also be checked in the validator!
        if (input == null)
            throw new IllegalArgumentException("Input must not be null");
        if (input.getProcessed() == null)
            throw new IllegalArgumentException("Processed must not be null");
        if (input.getEventIds() == null || input.getEventIds().isEmpty())
            throw new IllegalArgumentException("Events must not be null or empty");
        if (input.getEventIds().size() > RESULTS_PER_PAGE_LIMIT)
            throw new IllegalArgumentException("Event size must not exceed " + RESULTS_PER_PAGE_LIMIT + " but was " + input.getEventIds().size());

        // TODO could update to use a single query to update all event IDs provided (more efficient)
        List<Integer> eventIds = input.getEventIds();
        List<SubscriberQueue> queueRecords = getSubscriberQueueEventsById(eventIds);
        queueRecords.forEach(event -> event.setProcessed(input.getProcessed()));
    }

	@Override
	public List<SubscriberQueueDTO> getSubscriberQueueEventsProjectionById(List<Integer> ids) {
		return this.baseDao.getSubscriberQueueEventsProjectionById(ids);
	}
	
	
}
