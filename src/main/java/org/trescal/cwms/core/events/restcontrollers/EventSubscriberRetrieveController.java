
package org.trescal.cwms.core.events.restcontrollers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;
import org.trescal.cwms.core.events.change.queue.ChangeQueue;
import org.trescal.cwms.core.events.service.ChangeQueueService;
import org.trescal.cwms.core.events.service.EventSubscriberService;
import org.trescal.cwms.core.events.service.EventSubscriptionService;
import org.trescal.cwms.core.events.service.SubscriberQueueService;
import org.trescal.cwms.core.events.subscriber.dto.SubscriberEventDTO;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscriber;
import org.trescal.cwms.core.events.subscriber.entity.SubscriberQueue;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.common.dto.output.RestPagedResultDTO;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;


/**
 * @Author shazil.khan
 */
@RestController
@RequestMapping("/events")
public class EventSubscriberRetrieveController {

	private String EXT_DN_EXIST = "EVENT SUBSCRIBER DOESNOT EXIST";
	private String EXT_EXIST ="EVENT SUBSCRIBER EXIST";

	@Autowired  
	private EventSubscriberService subscriberService;

	@Autowired
	private EventSubscriptionService subscriptionService;

	@Autowired
	private ChangeQueueService changeQueueService;

	@Autowired
	private SubscriberQueueService subscriberQueueService;

	private List<ChangeQueue> resultsDetails =new ArrayList<ChangeQueue>();

	private List<ChangeQueue> errorsDetails = null;


	/*
	 * For saving one subscriber
	 */
	@PostMapping("/addEventSubscriber")
	public void addEventSubscriber(@RequestBody EventSubscriber eventsubscriber){
		subscriberService.save(eventsubscriber);
	}

	/*
	 * Searching a Subscriber by Id
	 */
	@GetMapping("/subscriberById/{id}")
	public EventSubscriber findEventSubscriberById(@PathVariable int id){
		return subscriberService.get(id)  ;	
	}

	@GetMapping("/allSubscribers")
	public List<EventSubscriber> findAllEventSubscribers(){
		return subscriberService.getAll();
	}
	
	/*	@GetMapping("/eventqueue/{id}")
	public ChangeQueue findChangeeventsById(@PathVariable int id){
		return cdcEntityService.get(id);
	}
	 */

/*	@GetMapping("/subscriptionsById/{id}")
	public EventSubscription findAllEventSubscriptionById(@PathVariable int id){
		return subscriptionService.get(id);
	}*/

/*	@GetMapping("/allSubscription")
	public List<EventSubscription> findAllEventSubscriptions(){
		return subscriptionService.getAll();
	}*/
	
	
/*	@RequestMapping(path="/api/events/retrieve", method=RequestMethod.GET)*/
	@RequestMapping(path="/retrieve", method=RequestMethod.GET)
	@ResponseBody
	public RestPagedResultDTO<SubscriberEventDTO> getEvents(
			@RequestParam(name="subscriberId", required=true) Integer subscriberId) {
		RestPagedResultDTO<SubscriberEventDTO> result = new RestPagedResultDTO<SubscriberEventDTO>(true, "", 100, 1);
		boolean processed = false;
		PagedResultSet<SubscriberEventDTO> prs = new PagedResultSet<>(100, 1);
		subscriberQueueService.getEvents(subscriberId, processed, prs);
		
		prs.getResults().forEach(dto -> result.addResult(dto));
		result.setResultsCount(prs.getResultsCount());
		return result;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { "/datatoretrieve" }, method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<List<ChangeQueue>>  findAllDataEvents(
			@RequestParam(required = true) @NotNull Integer size,
			@RequestParam(required = true) @NotNull Integer id) {	

		EventSubscriber eventSubscriber = subscriberService.get(id);

		if(eventSubscriber == null)
		{
			return new RestResultDTO<List<ChangeQueue>>(true, EXT_DN_EXIST);
		}

		List<ChangeEntity> changeEntities = new ArrayList<>();
		if(eventSubscriber.getEventSubscriptions()!=null)
			eventSubscriber.getEventSubscriptions().forEach(e -> {
				changeEntities.add(e.getEntityType());
			});

		RestResultDTO<List<ChangeQueue>> resultDTO = new RestResultDTO<List<ChangeQueue>>(true, EXT_EXIST);
		resultDTO.addResult(changeQueueService.findNNotProcessedElements(size, changeEntities));

		return resultDTO;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = { "/dataretrieved" }, method = RequestMethod.GET, produces = {
			Constants.MEDIATYPE_APPLICATION_JSON_UTF8, Constants.MEDIATYPE_TEXT_JSON_UTF8 })
	public RestResultDTO<List<ChangeQueue>>  findAllEvents(
			@RequestParam(required = true) @NotNull Integer size,
			@RequestParam(required = true) @NotNull Integer id) {	

		EventSubscriber eventSubscriber = subscriberService.get(id);

		if(eventSubscriber == null)
		{
			return new RestResultDTO<List<ChangeQueue>>(true, EXT_DN_EXIST);
		}

		List<ChangeEntity> changeEntities = new ArrayList<>();
		if(eventSubscriber.getEventSubscriptions()!=null)
		{
			eventSubscriber.getEventSubscriptions().forEach(e -> {
				changeEntities.add(e.getEntityType());
			});	
		}   

		List<ChangeQueue> result = changeQueueService.findNNotProcessedElements(size, changeEntities);
		result.forEach(r -> {
			r.setProcessed(true);
			changeQueueService.merge(r);
		});

		//New method for updating subscriber_table in the database.
		SubscriberQueue subscriberQueue = new SubscriberQueue() ;

		RestResultDTO<List<ChangeQueue>> resultDTO = new RestResultDTO<List<ChangeQueue>>(true, EXT_EXIST);
		resultDTO.addResult(result);
		return resultDTO;}

	@GetMapping("/SubscriberByName/{name}")
	public EventSubscriber findEventSubscriberByName(@PathVariable String name){
		return subscriberService.findByName(name);
	}

	/*	@PutMapping("/updateEventSubscriber")
	public EventSubscriber updateEventSubscriber(@RequestBody EventSubscriber eventsubscriber){
		return subscriberService.updateSubscriber(eventsubscriber);
	}*/

	/*	@DeleteMapping("/deleteEventSubscriber/{id}")
	public String deleteEventSubscriber(@PathVariable int id){
		return subscriberService.deleteEventSubscriber(id);
	}*/
}

