/**
 * 
 */
package org.trescal.cwms.core.events.service;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;
import org.trescal.cwms.core.events.change.queue.ChangeQueue;
import org.trescal.cwms.core.events.change.queue.enums.ChangeQueueDTO;
/**
 * @Author shazil.khan
 *
 */
public interface ChangeQueueService extends BaseService<ChangeQueue, Integer> {
	
	public List<ChangeQueueDTO> findNNotProcessedChangeQueueItems(int size, List<ChangeEntity> changeEntities);
	
	public List<ChangeQueue> findNNotProcessedChangeQueue(int size);
	
	List<ChangeQueue> findNNotProcessedElements(int size, List<ChangeEntity> changeEntities );
	
}
