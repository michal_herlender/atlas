package org.trescal.cwms.core.events.repository;



import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscription;

/**
 * @Author shazil.khan
 *
 */
@Repository("EventSubscriptionDao")
public class EventSubscriptionDaoImpl extends BaseDaoImpl<EventSubscription, Integer> implements EventSubscriptionDao {

	/* 
	 * @see org.trescal.cwms.core.audit.entity.db.BaseDaoImpl#getEntity()
	 */
	@Override
	protected Class<EventSubscription> getEntity() {
		return EventSubscription.class;
	}

	

}
 