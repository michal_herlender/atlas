package org.trescal.cwms.core.events.subscriber.dto;

import org.trescal.cwms.core.events.change.enums.ChangeEntity;
import org.trescal.cwms.core.events.change.enums.ChangeType;
import org.trescal.cwms.rest.events.tools.PayloadSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SubscriberEventDTO {
	private Integer eventId;
	private Integer changeEventId;
	private ChangeType eventType;
	private ChangeEntity entityType;
	@JsonSerialize(using=PayloadSerializer.class)
	private String payload;

	/**
	 * Used by JPA constructor in this order; intentionally not using AllArgsConstructor as it easily breaks JPA query
	 */
	public SubscriberEventDTO(Integer eventId, Integer changeEventId, Integer changeType, ChangeEntity entityType, String payload) {
		super();
		this.eventId = eventId;
		this.changeEventId = changeEventId;
		this.eventType = initEventType(changeType);
		this.entityType = entityType;
		this.payload = payload;
	}
	
	public ChangeType initEventType(Integer changeType) {
		// changeType stored in ChangeQueue corresponds to CDC operation type
		// TODO revisit and confirm / refactor this
		switch (changeType) {
			case 1: return ChangeType.DELETE;
			case 2: return ChangeType.CREATE;
			default: return ChangeType.UPDATE;
		}
	}
	
}
