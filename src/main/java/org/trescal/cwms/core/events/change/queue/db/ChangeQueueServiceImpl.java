/*package org.trescal.cwms.core.events.change.queue.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;
import org.trescal.cwms.core.events.change.queue.ChangeQueue;
import org.trescal.cwms.core.events.change.queue.enums.ChangeQueueDTO;

@Service
public class ChangeQueueServiceImpl extends BaseServiceImpl<ChangeQueue, Integer> implements ChangeQueueService {
	
	@Autowired
	private ChangeQueueDao changeQueueDao;

	@Override
	protected BaseDao<ChangeQueue, Integer> getBaseDao() {
		return this.changeQueueDao;
	}

	@Override
	public List<ChangeQueueDTO> findNNotProcessedChangeQueueItems(int size, List<ChangeEntity> changeEntities) {
		return this.changeQueueDao.findNNotProcessedChangeQueueItems(size, changeEntities);
	}
	
	public List<ChangeQueue> findNNotProcessedChangeQueue(int size) {
		return this.changeQueueDao.findNNotProcessedChangeQueue(size);
	}
}
*/