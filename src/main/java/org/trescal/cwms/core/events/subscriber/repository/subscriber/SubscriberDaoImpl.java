/*package org.trescal.cwms.core.events.subscriber.repository.subscriber;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscriber;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscriber_;

@Repository
public class SubscriberDaoImpl extends BaseDaoImpl<EventSubscriber, Integer> implements SubscriberDao {

	@Override
	protected Class<EventSubscriber> getEntity() {
		return EventSubscriber.class;
	}
	
	@Override
	public List<EventSubscriber> getAllSubscribersEager() {
		return getResultList(cb -> {
			CriteriaQuery<EventSubscriber> cq = cb.createQuery(EventSubscriber.class);
			Root<EventSubscriber> root = cq.from(EventSubscriber.class);
			root.fetch(EventSubscriber_.subscriptions, JoinType.LEFT);
			cq.distinct(true);
					
			return cq;
		});
	}
	
}
*/