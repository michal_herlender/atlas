package org.trescal.cwms.core.events.repository;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscriber;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscriber_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * @author shazil.khan
 */
@Repository("SubscriberDao")
public class EventSubscriberDaoImpl extends BaseDaoImpl<EventSubscriber, Integer> implements EventSubscriberDao {

	@Autowired
	private EventSubscriberDao eventSubscriberDao;

	@Override
	public EventSubscriber findByName(String name) {
		return getFirstResult(cb ->
		{
			val cq = cb.createQuery(EventSubscriber.class);
			val eventSub = cq.from(EventSubscriber.class);
			cq.where(cb.equal(eventSub.get(EventSubscriber_.description),name));
			return cq;
		}).orElse(null);
	}
	
	@Override
	protected Class<EventSubscriber> getEntity() {
		return EventSubscriber.class ;
	}
	
	@Override
	public List<EventSubscriber> getAllSubscribersEager() {
		return getResultList(cb -> {
			CriteriaQuery<EventSubscriber> cq = cb.createQuery(EventSubscriber.class);
			Root<EventSubscriber> root = cq.from(EventSubscriber.class);
			root.fetch(EventSubscriber_.eventSubscriptions, JoinType.LEFT);
			cq.distinct(true);

			return cq;
		});
	}
	

	public EventSubscriber getEventSubscriberByName(String name){
		return eventSubscriberDao.findByName(name);
	}


}


