/*package org.trescal.cwms.core.events.subscriber.db;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.events.change.db.ChangeEventService;
import org.trescal.cwms.core.events.change.dto.ChangeMfrDTO;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;
import org.trescal.cwms.core.events.subscriber.dto.SubscriberEventDTO;
import org.trescal.cwms.core.events.subscriber.queue.SubscriberQueue;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.rest.events.dto.SubscriberEventUpdateInput;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SubscriberQueueServiceImpl extends BaseServiceImpl<SubscriberQueue, Integer>
		implements SubscriberQueueService {
	
	@Autowired
	private SubscriberQueueDao baseDao;
	@Autowired
	private ChangeEventService changeEventService;
	
	// For now set a limit of no more than 100 events
	public static int RESULTS_PER_PAGE_LIMIT = 100;

	@Override
	protected BaseDao<SubscriberQueue, Integer> getBaseDao() {
		return baseDao;
	}
	
	@Override
	public void getEvents(Integer subscriberId, Boolean processed, PagedResultSet<SubscriberEventDTO> prs, Boolean populateEventDetails) {
		if (prs.getResultsPerPage() > RESULTS_PER_PAGE_LIMIT) {
			// To limit the JSON result size, prevent IN clause limit, and prevent (statistically) too many different event types 
			throw new UnsupportedOperationException("Results per page must be no more than "+RESULTS_PER_PAGE_LIMIT+ " but was "+prs.getResultsPerPage());
		}
		baseDao.getEventDTOs(subscriberId, processed, prs);
		if (populateEventDetails != null && populateEventDetails) {
			Collection<SubscriberEventDTO> events = prs.getResults();
			MultiValuedMap<ChangeEntity, Integer> mapChangeIdsByType = new HashSetValuedHashMap<>();
			Map<Integer, SubscriberEventDTO> mapEventsByChangeId = events.stream()
					.collect(Collectors.toMap(SubscriberEventDTO::getChangeEventId, event -> event));
			
			events.forEach(eventDto -> mapChangeIdsByType.put(eventDto.getEntityType(), eventDto.getChangeEventId()));
			for (ChangeEntity entityType : mapChangeIdsByType.keySet()) {
				// TODO generalize to other types
				List<ChangeMfrDTO> dtos = getEventDetails(entityType, mapChangeIdsByType.get(entityType));
				log.info("dtos.size : "+dtos.size()); 
				dtos.forEach(dto -> mapEventsByChangeId.get(dto.getChangeid()).setData(dto));
				
				System.out.println("Test Debug Here");
			}
		}
	}
	
	private List<ChangeMfrDTO> getEventDetails(ChangeEntity entityType, Collection<Integer> changeIds) {
		// TODO generalize to other types
		List<ChangeMfrDTO> dtos = null;
		switch (entityType) {
		case BRAND:
			dtos = this.changeEventService.getMfrDTOs(changeIds);
			break;
		default:
			throw new UnsupportedOperationException("Result type "+entityType+" not yet supported");
		}
		return dtos;
	}
	
	@Override
	public List<SubscriberQueue> getSubscriberQueueEventsById(List<Integer> ids) {
		List<SubscriberQueue> dtos = Collections.emptyList();
		if (!ids.isEmpty()) {
			dtos = baseDao.getSubscriberQueueEventsById(ids);
		}
		return dtos;
	}

	@Override
	public void performUpdate(SubscriberEventUpdateInput input) {
		// These should also be checked in the validator
		if (input == null) throw new IllegalArgumentException("Input must not be null");
		if (input.getProcessed() == null) throw new IllegalArgumentException("Processed must not be null");
		if (input.getEventIds() == null || input.getEventIds().isEmpty()) throw new IllegalArgumentException("Processed must not be null");
		
		// TODO generalize to other types
		List<Integer> eventIds = input.getEventIds();
		List<SubscriberQueue> queueRecords = getSubscriberQueueEventsById(eventIds);
		queueRecords.forEach(event -> event.setProcessed(input.getProcessed()));
		
	}
}
*/