package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.calibration.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.calibration.CustomCalibrationCondition;

@Service("CustomCalibrationConditionService")
public class CustomCalibrationConditionServiceImpl extends BaseServiceImpl<CustomCalibrationCondition, Integer> implements CustomCalibrationConditionService
{
	@Autowired
	private CustomCalibrationConditionDao customCalibrationConditionDao;
	
	@Override
	protected BaseDao<CustomCalibrationCondition, Integer> getBaseDao() {
		return customCalibrationConditionDao;
	}
	
	@Override
	public List<CustomCalibrationCondition> getListCustomCalibrationConditions(List<Integer> ids) {
		return this.customCalibrationConditionDao.getListCustomCalibrationConditions(ids);
	}
}