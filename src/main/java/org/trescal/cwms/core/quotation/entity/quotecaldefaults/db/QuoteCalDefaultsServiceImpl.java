package org.trescal.cwms.core.quotation.entity.quotecaldefaults.db;

import java.util.List;

import org.trescal.cwms.core.quotation.entity.quotecaldefaults.QuoteCalDefaults;

public class QuoteCalDefaultsServiceImpl implements QuoteCalDefaultsService
{
	QuoteCalDefaultsDao quoteCalDefaultsDao;

	public QuoteCalDefaults findQuoteCalDefaults(int id)
	{
		return quoteCalDefaultsDao.find(id);
	}

	public void insertQuoteCalDefaults(QuoteCalDefaults quotecaldefaults)
	{
		quoteCalDefaultsDao.persist(quotecaldefaults);
	}

	public void updateQuoteCalDefaults(QuoteCalDefaults quotecaldefaults)
	{
		quoteCalDefaultsDao.update(quotecaldefaults);
	}

	public void deleteQuoteCalDefaults(QuoteCalDefaults quotecaldefaults) 
	{
		this.quoteCalDefaultsDao.remove(quotecaldefaults);
	}
	
	public List<QuoteCalDefaults> getAllQuoteCalDefaultss()
	{
		return quoteCalDefaultsDao.findAll();
	}
	
	public void setQuoteCalDefaultsDao(QuoteCalDefaultsDao quoteCalDefaultsDao) 
	{
		this.quoteCalDefaultsDao = quoteCalDefaultsDao;
	}
}