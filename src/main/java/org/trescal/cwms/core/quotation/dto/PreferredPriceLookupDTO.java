package org.trescal.cwms.core.quotation.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PreferredPriceLookupDTO {

    Integer modelId;
    List<Integer> calTypeIds;
    Integer businessCoId;
    String currencyCode;
    Integer clientCoId;
    Integer excludeQuotationId;

}
