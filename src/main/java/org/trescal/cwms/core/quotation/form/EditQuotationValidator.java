package org.trescal.cwms.core.quotation.form;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import java.time.LocalDate;

@Component
public class EditQuotationValidator extends AbstractBeanValidator {
	protected final Log logger = LogFactory.getLog(this.getClass());
	@Autowired
	private StatusService statusServ;

	public boolean supports(Class<?> clazz) {
		return clazz.equals(EditQuotationForm.class);
	}

	public void validate(Object obj, Errors errors)
	{
		super.validate(obj, errors);
		EditQuotationForm quoteData = (EditQuotationForm) obj;

		if (quoteData == null)
		{
			errors.reject("error.nullpointer", "Null data received");
		}
		else {
			if (quoteData.getQuotation().getContact() == null) {
				errors.rejectValue("contact", "error.quotation.contact", null, "Please select a contact");
			}
			if (quoteData.getQuotation().getVatRate() == null) {
				errors.rejectValue("quotation.vatRate", "error.vatrate.invalid", null, "Invalid VAT rate selection");
			}

			LocalDate date = quoteData.getQuotation().getReqdate();

			if (date != null) {

				LocalDate now = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
				if (date.isAfter(now)) {
					errors.rejectValue("reqDate", "error.quotation.reqdate.future", null, "Request date cannot be in the future");
				}
			}
			
			if(quoteData.getStatusid() != null) {
				QuoteStatus qsNew = this.statusServ.findStatus(quoteData.getStatusid(), QuoteStatus.class);
				if (qsNew.getAccepted() && quoteData.getClientAcceptanceOn() == null)
					errors.rejectValue("clientAcceptanceOn", "error.quotation.clientacceptance", null, "Please select a client acceptance date");
			}
		}
	}
}
