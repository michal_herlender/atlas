package org.trescal.cwms.core.quotation.form;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class AddInstrumentToQuoteValidator extends AbstractBeanValidator {

	@Autowired
	private QuotationService quotationService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(AddInstrumentToQuoteForm.class);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		super.validate(obj, errors);
		AddInstrumentToQuoteForm form = (AddInstrumentToQuoteForm) obj;
		// check barcode entered is a number
		if ((form.getSearch().getBarcode() != null) && !form.getSearch().getBarcode().trim().isEmpty()
				&& !NumberTools.isAnInteger(form.getSearch().getBarcode().trim())) {
			errors.rejectValue("search.barcode", "search.barcode", null,
					"The barcode '" + form.getSearch().getBarcode() + "' specified is not valid.");
		}
		// check if one of the instruments we want to add already exist in the
		// quotation with the same service type
		int count = 0;
		Quotation quotation = quotationService.get(form.getQuotationId());
		if (form.getSubmitType().equals("addToQuote") && form.getBasketIds() != null) {
			for (Integer instId : form.getBasketIds()) {
				if (seenInstWithSameCalTypes(quotation).get(instId).contains(form.getBasketCaltypes().get(count))) {
					errors.rejectValue("basketIds[" + count + "]", "quotaddinst.onquote", "On Quote");
				}
				count++;
			}
		}

	}

	/**
	 * Returns all instruments already exist in a quotation with the associated
	 * Cal Type
	 */
	private MultiValuedMap<Integer, Integer> seenInstWithSameCalTypes(Quotation quotation) {
		MultiValuedMap<Integer, Integer> instOnQuoteWithServiceTypeUsed = new HashSetValuedHashMap<>();
		quotation.getQuotationitems().stream().forEach(qi -> {
			if (qi.getInst() != null) {
				instOnQuoteWithServiceTypeUsed.put(qi.getInst().getPlantid(), qi.getCaltype().getCalTypeId());
			}
		});

		return instOnQuoteWithServiceTypeUsed;
	}
}