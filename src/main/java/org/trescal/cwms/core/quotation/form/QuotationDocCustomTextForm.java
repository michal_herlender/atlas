package org.trescal.cwms.core.quotation.form;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class QuotationDocCustomTextForm
{
	private Integer quotationId;
	private String subject;
	private String body;
	private Boolean useCustomQuoteText;

	public static interface UseCustomTextGroup {};
	
	@NotNull
	public Integer getQuotationId()
	{
		return this.quotationId;
	}

	@NotNull
	@Length(min=1, max=100, groups=UseCustomTextGroup.class)
	public String getSubject()
	{
		return this.subject;
	}

	@NotNull
	@Length(min=1, max=2000, groups=UseCustomTextGroup.class)
	public String getBody()
	{
		return this.body;
	}

	@NotNull
	public Boolean getUseCustomQuoteText()
	{
		return this.useCustomQuoteText;
	}

	public void setQuotationId(Integer quotationId)
	{
		this.quotationId = quotationId;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	public void setBody(String body)
	{
		this.body = body;
	}

	public void setUseCustomQuoteText(Boolean useCustomQuoteText)
	{
		this.useCustomQuoteText = useCustomQuoteText;
	}

}
