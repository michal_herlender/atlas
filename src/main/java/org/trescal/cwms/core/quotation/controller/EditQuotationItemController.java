package org.trescal.cwms.core.quotation.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.pricing.entity.costs.base.db.CalCostService;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostMarkupSource;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostSource;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteheading.db.QuoteHeadingService;
import org.trescal.cwms.core.quotation.form.EditQuotationItemForm;
import org.trescal.cwms.core.quotation.form.EditQuotationItemValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost.TPQuotationCalibrationCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.db.TPQuotationItemService;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Displays a form which allows the user to edit a {@link Quotationitem}.
 */
@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class EditQuotationItemController {
	@Value("${cwms.config.quotation.size.restrictedititem}")
	private long sizeRestrictEditItem;
	@Autowired
	private QuoteHeadingService qhServ;
	@Autowired
	private QuotationItemService qiServ;
	@Autowired
	private TPQuotationItemService tpItemServ;
	@Autowired
	private CalCostService calCostService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private EditQuotationItemValidator validator;
	
	@InitBinder("command")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("command")
	protected Object formBackingObject(@RequestParam(value = "id") Integer id) {
		EditQuotationItemForm form = new EditQuotationItemForm();
		Quotationitem item = this.qiServ.findQuotationItem(id);

		if (item == null) {
			throw new RuntimeException("The requested quotation item could not be found");
		} else {
			long itemCount = this.qiServ.getItemCount(item.getQuotation().getId());
			form.setEditable(itemCount < sizeRestrictEditItem);
			form.setQuotationitem(item);
			form.setCopyIds(new ArrayList<>());
			form.setServiceTypeId(item.getServiceType().getServiceTypeId());
			return form;
		}
	}

	/**
	 * Result of saving a quotation item
	 */
	@RequestMapping(value = "/editquotationitem.htm", method = RequestMethod.POST)
	public String onSubmit(
		@RequestParam(value = "action", required = false, defaultValue = "save") String submitType,
		@ModelAttribute("command") @Validated EditQuotationItemForm form, BindingResult bindingResult,
		@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
		@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		if (bindingResult.hasErrors()) {
			return showView();
		}
		Quotationitem qi = form.getQuotationitem();
		if(form.getCalibrationCostId() != null){
			TPQuotationCalibrationCost tpQuoteCalCost = (TPQuotationCalibrationCost) calCostService.findCalCost(form.getCalibrationCostId());
			qi.getCalibrationCost().setLinkedCostSrc(tpQuoteCalCost);
		}
		form.setAllocatedSubdivId(subdivDto.getKey());
		// edit the quotation item
		this.qiServ.editQuotationItem(qi, form, username);
		Quotation q = qi.getQuotation();
		if (submitType.equalsIgnoreCase("save"))
		{
			// return the user to the same quotationitem page
			return "redirect:/editquotationitem.htm?id="+qi.getId();
		}
		else
		{
			// return the user to the quotation page
			return "redirect:/viewquotation.htm?id="+q.getId();
		}
	}
	
	protected int getModelId(Quotationitem qi) {
		// model id variable
		return qi.getModel() == null ? qi.getInst().getModel().getModelid() : qi.getModel().getModelid();
	}

	@ModelAttribute("serviceTypes")
	protected List<KeyValue<Integer,String>> getServiceTypes(Locale locale) {
		// load the different available calibration types
		return this.serviceTypeService.getDTOList(null, null, locale, false);
	}

	@ModelAttribute("nearbyItems")
	protected List<Quotationitem> getNearbyItems(@RequestParam(value = "id") Integer id) {
		Quotationitem qi = this.qiServ.findQuotationItem(id);
		// For the display of 
		return this.qiServ.findNearbyQuotationItems(qi.getHeading().getHeadingId(), qi.getItemno() - 10, qi.getItemno() + 10);
	}

	@ModelAttribute("headings")
	protected Set<QuoteHeading> getQuotationHeadings(@RequestParam(value = "id") Integer id) {
		// load the different quotation headings
		Quotationitem qi = this.qiServ.findQuotationItem(id);
		return this.qhServ.findQuotationHeadings(qi.getQuotation().getId());
	}

	@ModelAttribute("availablemodules")
	protected Set<Quotationitem> getAvailableModules(@RequestParam(value = "id") Integer id) {
		// get a list of modules on the quotation that are available to be
		// paired up to this base unit
		Quotationitem qi = this.qiServ.findQuotationItem(id);
		int modelid = getModelId(qi);
		return this.qiServ.findAvailableModules(qi.getQuotation().getId(), modelid);
	}

	@ModelAttribute("availablebaseunits")
	protected Set<Quotationitem> getAvailableBaseUnits(@RequestParam(value = "id") Integer id) {
		Quotationitem qi = this.qiServ.findQuotationItem(id);
		int modelid = getModelId(qi);
		return this.qiServ.findAvailableBaseUnits(qi.getQuotation().getId(), modelid);
	}

	@ModelAttribute("tpquote")
	protected List<TPQuotationItem> getTPQuotationItems(@RequestParam(value = "id") Integer id) {
		Quotationitem qi = this.qiServ.findQuotationItem(id);
		int modelid = getModelId(qi);
		return this.tpItemServ.getMatchingItems("CALIBRATION", modelid);
	}
	
	@ModelAttribute("defaultCurrency")
	protected SupportedCurrency getDefaultCurrency(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		Subdiv subdiv = this.subdivService.get(subdivDto.getKey());
		return subdiv.getComp().getCurrency();
	}

	@ModelAttribute("identicalmodels")
	protected List<Quotationitem> getIdenticalModels(@RequestParam(value = "id") Integer id) {
		// add any other identical quotationitem models
		Quotationitem qi = this.qiServ.findQuotationItem(id);
		int modelid = getModelId(qi);
		return this.qiServ.findIdenticalModelsOnQuotation(qi.getId(), modelid, qi.getId(), qi.getQuotation().getSortType());
	}
	
	@ModelAttribute("tpcostsrc")
	protected ThirdCostSource[] getThirdCostSource() {
		return ThirdCostSource.values();
	}
	
	@ModelAttribute("tpcostmarkupsrc")
	protected ThirdCostMarkupSource[] getThirdCostMarkupSource() {
		return ThirdCostMarkupSource.values();
	}
	
	@ModelAttribute("privateOnlyNotes")
	protected boolean getPrivateOnlyNotes() {
		return NoteType.QUOTEITEMNOTE.isPrivateOnly();
	}

	/*
	 * Moved reference data to own attributes to prevent flush/attempted commit failure when JPA validation fails
	 */
	@RequestMapping(value = "/editquotationitem.htm", method = RequestMethod.GET)
	protected String showView() {
		return "trescal/core/quotation/editquotationitem";
	}
}
