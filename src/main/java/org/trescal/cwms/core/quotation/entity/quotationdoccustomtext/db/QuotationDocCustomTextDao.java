package org.trescal.cwms.core.quotation.entity.quotationdoccustomtext.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.quotation.entity.quotationdoccustomtext.QuotationDocCustomText;

public interface QuotationDocCustomTextDao extends BaseDao<QuotationDocCustomText, Integer> {}