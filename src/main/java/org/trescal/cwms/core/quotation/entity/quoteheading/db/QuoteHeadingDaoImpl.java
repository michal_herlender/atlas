package org.trescal.cwms.core.quotation.entity.quoteheading.db;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation_;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeadingComparator;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading_;

@Repository("QuoteHeadingDao")
public class QuoteHeadingDaoImpl extends BaseDaoImpl<QuoteHeading, Integer> implements QuoteHeadingDao {
	
	@Override
	protected Class<QuoteHeading> getEntity() {
		return QuoteHeading.class;
	}
	
	public List<QuoteHeading> findAll(List<Integer> ids) {
		return getResultList(cb ->{
			CriteriaQuery<QuoteHeading> cq = cb.createQuery(QuoteHeading.class);
			Root<QuoteHeading> root = cq.from(QuoteHeading.class);
			cq.where(root.get(QuoteHeading_.headingId).in(ids));
			return cq;
		});
	}
	
	public QuoteHeading findDefaultQuoteHeading(int quoteId) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<QuoteHeading> cq = cb.createQuery(QuoteHeading.class);
		Root<QuoteHeading> heading = cq.from(QuoteHeading.class);
		Join<QuoteHeading, Quotation> quotation = heading.join(QuoteHeading_.quotation, JoinType.LEFT);
		Predicate conjunction = cb.and(
				cb.equal(quotation.get(Quotation_.id), quoteId),
				cb.equal(heading.get(QuoteHeading_.systemDefault), true));
		cq.where(conjunction);
		TypedQuery<QuoteHeading> query = getEntityManager().createQuery(cq);
		List<QuoteHeading> results = query.getResultList();
		// Expected there should be one and only one system default, if multiple (e.g. migration error), return first for use.
		return results.isEmpty() ? null : results.get(0);
	}
	
	public Set<QuoteHeading> findQuotationHeadings(int quoteid) throws DataAccessException {
		List<QuoteHeading> list = getResultList(cb ->{
			CriteriaQuery<QuoteHeading> cq = cb.createQuery(QuoteHeading.class);
			Root<QuoteHeading> root = cq.from(QuoteHeading.class);
			Join<QuoteHeading, Quotation> quotationJoin = root.join(QuoteHeading_.quotation);
			cq.where(cb.equal(quotationJoin.get(Quotation_.id), quoteid));
			return cq;
		});
		Set<QuoteHeading> qhSet = new TreeSet<QuoteHeading>(new QuoteHeadingComparator());
		qhSet.addAll(list);
		return qhSet;
	}
	
	@Override
	public Set<QuoteHeading> findQuotationHeadings(int quoteid, Integer excludeHeaderId) {
		List<QuoteHeading> list = getResultList(cb ->{
			CriteriaQuery<QuoteHeading> cq =  cb.createQuery(QuoteHeading.class);
			Root<QuoteHeading> root = cq.from(QuoteHeading.class);
			Join<QuoteHeading, Quotation> quotationJoin = root.join(QuoteHeading_.quotation);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(quotationJoin.get(Quotation_.id), quoteid));
			if(excludeHeaderId != null){
				clauses.getExpressions().add(cb.notEqual(root.get(QuoteHeading_.headingId), excludeHeaderId));
			}
			cq.where(clauses);
			return cq;
		});
		Set<QuoteHeading> qhSet = new TreeSet<QuoteHeading>(new QuoteHeadingComparator());
		qhSet.addAll(list);
		return qhSet;
	}
	
	public QuoteHeading findQuoteheadingByItemNo(int quotationId, int headingNo) {
		return getFirstResult(cb ->{
			CriteriaQuery<QuoteHeading> cq = cb.createQuery(QuoteHeading.class);
			Root<QuoteHeading> root = cq.from(QuoteHeading.class);
			Join<QuoteHeading, Quotation> quotationJoin = root.join(QuoteHeading_.quotation);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(quotationJoin.get(Quotation_.id), quotationId));
			clauses.getExpressions().add(cb.equal(root.get(QuoteHeading_.headingNo), headingNo));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}
	
	public QuoteHeading findSystemDefaultQuoteHeading(int quoteid) {
		return getFirstResult(cb ->{
			CriteriaQuery<QuoteHeading> cq = cb.createQuery(QuoteHeading.class);
			Root<QuoteHeading> root = cq.from(QuoteHeading.class);
			Join<QuoteHeading, Quotation> quotationJoin = root.join(QuoteHeading_.quotation);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(quotationJoin.get(Quotation_.id), quoteid));
			clauses.getExpressions().add(cb.isTrue(root.get(QuoteHeading_.systemDefault)));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}
}