package org.trescal.cwms.core.quotation.entity.priorquotestatus.db;

import java.util.Set;
import java.util.TreeSet;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.quotation.entity.priorquotestatus.PriorQuoteStatus;
import org.trescal.cwms.core.quotation.entity.priorquotestatus.PriorQuoteStatusComparator;

@Repository("PriorQuoteStatusDao")
public class PriorQuoteStatusDaoImpl extends BaseDaoImpl<PriorQuoteStatus, Integer> implements PriorQuoteStatusDao {
	
	@Override
	protected Class<PriorQuoteStatus> getEntity() {
		return PriorQuoteStatus.class;
	}
	
	@SuppressWarnings("unchecked")
	public Set<PriorQuoteStatus> getQuotationPriorStatusList(int quoteid) {
		Criteria priorQuoteCriteria = getSession().createCriteria(PriorQuoteStatus.class);
		priorQuoteCriteria.add(Restrictions.eq("quotation.id", quoteid));
		priorQuoteCriteria.addOrder(Order.desc("setOn"));
		Set<PriorQuoteStatus> pqSet = new TreeSet<PriorQuoteStatus>(new PriorQuoteStatusComparator());
		pqSet.addAll(priorQuoteCriteria.list());
		return pqSet;
	}
}