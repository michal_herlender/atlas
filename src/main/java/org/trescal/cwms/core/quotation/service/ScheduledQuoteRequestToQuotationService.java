package org.trescal.cwms.core.quotation.service;

import java.util.Set;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequest;

public interface ScheduledQuoteRequestToQuotationService
{
	public void notifyFailure(ScheduledQuotationRequest request, Exception e);

	public void notifySuccess(ScheduledQuotationRequest request);

	public Quotation prepareAndSaveScheduledQuoteToQuotation(ScheduledQuotationRequest request, boolean emailNotification, Contact currentContact, Subdiv allocatedSubdiv, Company allocatedCompany);
	
	public Set<Quotationitem> prepareCostPerInstrumentQuotationitems(ScheduledQuotationRequest request, Quotation quotation);
}