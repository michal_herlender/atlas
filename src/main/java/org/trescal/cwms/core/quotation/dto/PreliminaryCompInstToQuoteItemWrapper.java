package org.trescal.cwms.core.quotation.dto;

import java.util.ArrayList;
import java.util.List;

import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.dto.ContractReviewCalibrationCostDTO;
import org.trescal.cwms.core.pricing.catalogprice.dto.CatalogPriceDTO;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.dto.CostJobItemDto;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutputInvoiceItem;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PreliminaryCompInstToQuoteItemWrapper
{
	private Integer calTypeId;
	private String calTypeName;
	private boolean instrumentModelCase;
	
	private String finalCost;
	private CostSource source;
	private Integer plantid;
	private String serialno;
	private String plantno;
	private String fullInstrumentModelName;
	private String currency;
	private String comments;
	private List<CostJobItemDto> jobCostings = new ArrayList<CostJobItemDto>();
	private List<QuoteItemOutputDto> quotationItemOuputs = new ArrayList<QuoteItemOutputDto>();
	private List<PriceLookupOutputInvoiceItem> invoiceItemOuputs = new ArrayList<PriceLookupOutputInvoiceItem>();
	private List<CatalogPriceDTO> catalogPrices = new ArrayList<CatalogPriceDTO>();
	private ContractReviewCalibrationCostDTO contractReviewCost;
	

}
