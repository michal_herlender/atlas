package org.trescal.cwms.core.quotation.entity.quotecaltypedefault.db;

import java.util.List;

import org.trescal.cwms.core.quotation.entity.quotecaltypedefault.QuoteCaltypeDefault;

public class QuoteCaltypeDefaultServiceImpl implements QuoteCaltypeDefaultService
{
	QuoteCaltypeDefaultDao quoteCaltypeDefaultDao;

	public QuoteCaltypeDefault findQuoteCaltypeDefault(int id)
	{
		return quoteCaltypeDefaultDao.find(id);
	}
	
	public void deleteQuoteCaltypeDefault(QuoteCaltypeDefault quotecaltypedefault) 
	{
		quoteCaltypeDefaultDao.remove(quotecaltypedefault);
	}
	
	public void insertQuoteCaltypeDefault(QuoteCaltypeDefault QuoteCaltypeDefault)
	{
		quoteCaltypeDefaultDao.persist(QuoteCaltypeDefault);
	}

	public void updateQuoteCaltypeDefault(QuoteCaltypeDefault QuoteCaltypeDefault)
	{
		quoteCaltypeDefaultDao.update(QuoteCaltypeDefault);
	}

	public List<QuoteCaltypeDefault> getAllQuoteCaltypeDefaults()
	{
		return quoteCaltypeDefaultDao.findAll();
	}

	public void setQuoteCaltypeDefaultDao(QuoteCaltypeDefaultDao quoteCaltypeDefaultDao) 
	{
		this.quoteCaltypeDefaultDao = quoteCaltypeDefaultDao;
	}
}