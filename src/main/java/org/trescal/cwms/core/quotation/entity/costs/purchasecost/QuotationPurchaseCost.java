/**
 * 
 */
package org.trescal.cwms.core.quotation.entity.costs.purchasecost;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewLinkedPurchaseCost;
import org.trescal.cwms.core.pricing.entity.costs.thirdparty.TPSupportedPurchaseCost;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;

@Entity
@DiscriminatorValue("quotation")
public class QuotationPurchaseCost extends TPSupportedPurchaseCost
{
	private Set<ContractReviewLinkedPurchaseCost> conRevLinkedPurchaseCosts;

	private Quotationitem quoteItem;

	public QuotationPurchaseCost()
	{
	}

	/**
	 * Copy Constructor.
	 * 
	 * @param cost {@link QuotationPurchaseCost} to copy.
	 */
	public QuotationPurchaseCost(QuotationPurchaseCost cost)
	{

		this.quoteItem = cost.getQuoteItem();
		this.active = cost.isActive();
		this.costType = cost.getCostType();
		this.customOrderBy = cost.getCustomOrderBy();
		this.discountRate = new BigDecimal(cost.getDiscountRate().toPlainString());
		this.discountValue = new BigDecimal(cost.getDiscountValue().toPlainString());
		this.finalCost = new BigDecimal(cost.getFinalCost().toPlainString());
		this.totalCost = new BigDecimal(cost.getTotalCost().toPlainString());
		this.setHouseCost(cost.getHouseCost());
		this.setLinkedCostSrc(cost.getLinkedCostSrc());
		this.setThirdCostSrc(cost.getThirdCostSrc());
		this.setThirdCostTotal(new BigDecimal(cost.getThirdCostTotal().toPlainString()));
		this.setThirdManualPrice(new BigDecimal(cost.getThirdManualPrice().toPlainString()));
		this.setThirdMarkupRate(new BigDecimal(cost.getThirdMarkupRate().toPlainString()));
		this.setThirdMarkupSrc(cost.getThirdMarkupSrc());
		this.setThirdMarkupValue(new BigDecimal(cost.getThirdMarkupValue().toPlainString()));

	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "quotationPurchaseCost")
	public Set<ContractReviewLinkedPurchaseCost> getConRevLinkedPurchaseCosts()
	{
		return this.conRevLinkedPurchaseCosts;
	}

	@OneToOne(fetch=FetchType.LAZY, mappedBy = "purchaseCost")
	public Quotationitem getQuoteItem()
	{
		return this.quoteItem;
	}

	public void setConRevLinkedPurchaseCosts(Set<ContractReviewLinkedPurchaseCost> conRevLinkedPurchaseCosts)
	{
		this.conRevLinkedPurchaseCosts = conRevLinkedPurchaseCosts;
	}

	public void setQuoteItem(Quotationitem quoteItem)
	{
		this.quoteItem = quoteItem;
	}
}
