package org.trescal.cwms.core.quotation.entity;

import java.time.LocalDate;

/**
 * Interface implemented by quote entities that should have a request date
 * (allows merging and sorting of different entities which share request date
 * fields).
 *
 * @author Richard
 */
public interface QuoteRequested {
	int getId();

	QuoteRequestType getQuoteType();

	LocalDate getReqdate();

	boolean isQuote();
}
