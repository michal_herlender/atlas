package org.trescal.cwms.core.quotation.form;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.form.QuoteCompanyInstrumentsForm;
import org.trescal.cwms.core.quotation.enums.QuoteCreationType;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class AddQuotationForm {
    @NotNull
    @Length(max = 100)
    private String clientRef;

    @NotNull(message = "error.quotation.contact")
    private Contact contact;
    private QuoteCreationType creationType;
    private String currencyCode;
    private QuoteCompanyInstrumentsForm qcif;

    private Integer quoteFromJobId;
    private Integer quoteFromPriceListId;
    private Integer quoteFromRecallId;
    private LocalDate reqDate;

    @NotNull
    @Min(value = 1, message = "{error.value.notselected}")
    private Integer sourcedBy;

}
