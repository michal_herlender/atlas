package org.trescal.cwms.core.quotation.entity.quotationitem.comparators;

import java.util.Comparator;

import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;

public class QuotationItemBarcodeComparator implements Comparator<Quotationitem>
{
	@Override
	public int compare(Quotationitem o1, Quotationitem o2)
	{
		// both and instrument?
		if ((o1.getInst() != null) && (o2.getInst() != null))
		{
			if (o1.getInst().getPlantid() == o2.getInst().getPlantid())
			{
				// this should never happen but compare just in case
				return ((Integer) o1.getId()).compareTo(o2.getId());
			}
			else
			{
				return ((Integer) o1.getInst().getPlantid()).compareTo(o2.getInst().getPlantid());
			}
		}
		else if ((o1.getInst() == null) && (o2.getInst() != null))
		{
			return 1;
		}
		else if ((o1.getInst() != null) && (o2.getInst() == null))
		{
			return -1;
		}
		else
		{
			return ((Integer) o1.getId()).compareTo(o2.getId());
		}
	}
}
