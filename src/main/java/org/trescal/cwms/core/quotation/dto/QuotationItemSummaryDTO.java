package org.trescal.cwms.core.quotation.dto;

import java.math.BigDecimal;

import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

public class QuotationItemSummaryDTO {

	private ServiceType serviceType;
	private QuoteHeading heading;
	private Long countItems;
	private BigDecimal totalFinalCost;

	public QuotationItemSummaryDTO() {
	}

	public QuotationItemSummaryDTO(Long countItems, BigDecimal totalFinalCost) {
		this.setCountItems(countItems);
		this.setTotalFinalCost(totalFinalCost);
	}

	public QuotationItemSummaryDTO(ServiceType serviceType, Long countItems, BigDecimal totalFinalCost) {
		this.serviceType = serviceType;
		this.setCountItems(countItems);
		this.setTotalFinalCost(totalFinalCost);
	}

	public QuotationItemSummaryDTO(QuoteHeading heading, Long countItems, BigDecimal totalFinalCost) {
		this.heading = heading;
		this.setCountItems(countItems);
		this.setTotalFinalCost(totalFinalCost);
	}

	public QuotationItemSummaryDTO(ServiceType serviceType, QuoteHeading heading, Long countItems,
			BigDecimal totalFinalCost) {
		this.serviceType = serviceType;
		this.heading = heading;
		this.setCountItems(countItems);
		this.setTotalFinalCost(totalFinalCost);
	}

	public ServiceType getServiceType() {
		return serviceType;
	}

	public void setServiceType(ServiceType serviceType) {
		this.serviceType = serviceType;
	}

	public QuoteHeading getHeading() {
		return heading;
	}

	public void setHeading(QuoteHeading heading) {
		this.heading = heading;
	}

	public Long getCountItems() {
		return countItems;
	}

	public void setCountItems(Long countItems) {
		this.countItems = countItems;
	}

	public BigDecimal getTotalFinalCost() {
		return totalFinalCost;
	}

	public void setTotalFinalCost(BigDecimal totalFinalCost) {
		this.totalFinalCost = totalFinalCost;
	}
}