package org.trescal.cwms.core.quotation.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.form.AddInstModelToQuoteForm;

public interface AddInstModelToQuotationService {
	void addToQuote(Quotation quotation, AddInstModelToQuoteForm aqi, Subdiv subdiv, Contact contact, Locale locale);

	void saveQuotationDefaults(AddInstModelToQuoteForm aqi, Contact currentContact);

	Set<Quotationitem> importQuotationItemsUsingInstModel(Integer allocatedSubdivId, Integer contactId, List<Integer> modelindex,
			Map<Integer, Integer> modelIds, Map<Integer, Integer> serviceTypeIds, Map<Integer, BigDecimal> finalPrices,
			Map<Integer, BigDecimal> discountPrices, Map<Integer, BigDecimal> catalogPrices,
			Map<Integer, String> publicNotes, Map<Integer, String> privateNotes, Integer quoteId, Locale locale);
}
