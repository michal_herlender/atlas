package org.trescal.cwms.core.quotation.entity.quotationdocdefaulttext.db;

import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.quotation.entity.quotationdocdefaulttext.QuotationDocDefaultText;

public interface QuotationDocDefaultTextDao extends BaseDao<QuotationDocDefaultText, Integer> {
	QuotationDocDefaultText findDefaultText(Locale locale);
}
