package org.trescal.cwms.core.quotation.entity.quotationitem.comparators;

import java.util.Comparator;

import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;

public class QuotationItemCustomComparator implements Comparator<Quotationitem>
{
	@Override
	public int compare(Quotationitem o1, Quotationitem o2)
	{
		return (o1.getItemno()).compareTo(o2.getItemno());
	}
}