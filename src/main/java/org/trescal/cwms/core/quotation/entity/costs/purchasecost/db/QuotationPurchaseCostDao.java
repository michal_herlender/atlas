package org.trescal.cwms.core.quotation.entity.costs.purchasecost.db;

import org.trescal.cwms.core.quotation.entity.costs.QuotationCostDaoSupport;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;

public interface QuotationPurchaseCostDao extends QuotationCostDaoSupport<QuotationPurchaseCost>
{
}