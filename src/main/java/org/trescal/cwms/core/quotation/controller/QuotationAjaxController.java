package org.trescal.cwms.core.quotation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.quotation.dto.QuotationSearchResultWrapper;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class QuotationAjaxController {

	@Autowired
	private CompanyService companyService;
	@Autowired
	private QuotationService quotationService;
	@Autowired
	private StatusService statusService;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private UserService userService;
	@Value("#{props['cwms.config.quotation.resultsyearfilter']}")
	private Integer resultsYearFilter;
	
	@RequestMapping(value="searchcompanyquotations.htm", method=RequestMethod.GET)
	public ModelAndView getQuotations(
		@RequestParam(name = "coId") Integer coId,
		@RequestParam(name = "jobId", required = false) Integer jobId,
		@RequestParam(name = "years", required = false) Integer years,
		@RequestParam(name = "showExpired", required = false, defaultValue = "false") Boolean showExpired,
		@RequestParam(name = "showUnIssued", required = false, defaultValue = "false") Boolean showUnIssued,
		@RequestParam(name = "linkTo", required = false, defaultValue = "job") String linkTo,
		Locale locale) {
		if (years == null) years = resultsYearFilter;
		Company company = this.companyService.get(coId);
		List<QuotationSearchResultWrapper> quotations = quotationService.searchCompanyQuotations(coId, showExpired, showUnIssued, years, jobId);
		Map<String, Object> model = new HashMap<>();
		model.put("quotations", quotations);
		model.put("coId", coId);
		model.put("coName", company.getConame());
		model.put("jobId", jobId);
		model.put("linkTo", linkTo);
		List<KeyValue<Integer, String>> acceptStatus = statusService.findAllAcceptedStatus(QuoteStatus.class).stream()
			.map(s -> new KeyValue<>(s.getStatusid(), translationService.getCorrectTranslation(s.getNametranslations(), locale)))
			.collect(Collectors.toList());
		model.put("acceptStatus", acceptStatus);
		return new ModelAndView("trescal/core/jobs/job/companyquotations", model);
	}

	@RequestMapping(path="acceptquote.htm", method=RequestMethod.GET)
	public @ResponseBody String updateQuoteStatus(@RequestParam(name="quoteId") Integer quoteId,
								  	@RequestParam(name="acceptStatus", required=false, defaultValue="0") Integer acceptStatusId,
									@SessionAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username){

		Quotation quotation = quotationService.get(quoteId);
		Contact currentContact = userService.get(username).getCon();

		if(acceptStatusId > 0 && quotation.getQuotestatus().getIssued() && !quotation.getQuotestatus().getAccepted()) {
			QuoteStatus acceptStatus = (QuoteStatus) statusService.get(acceptStatusId);
			quotation.setQuotestatus(acceptStatus);
			quotation.setAccepted(true);
			quotation.setAcceptedBy(currentContact);
			quotation.setAcceptedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		}

		quotationService.merge(quotation);

		return "";
	}
}