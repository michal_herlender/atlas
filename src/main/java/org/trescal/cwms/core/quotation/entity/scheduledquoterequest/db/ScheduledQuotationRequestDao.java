package org.trescal.cwms.core.quotation.entity.scheduledquoterequest.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequest;

public interface ScheduledQuotationRequestDao extends BaseDao<ScheduledQuotationRequest, Integer>
{
	List<ScheduledQuotationRequest> getOutstanding(Subdiv allocatedSubdiv);

	List<ScheduledQuotationRequest> getTopXMostRecentlyProcessed(Subdiv allocatedSubdiv, int x);
	
	List<ScheduledQuotationRequest> scheduledQuotationRequestFromQuotation(Integer quotationId);
}