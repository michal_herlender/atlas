package org.trescal.cwms.core.quotation.enums;

public enum NewQuoteInstSortType
{
	BARCODE("Barcode"), CALDUEDATE("Calibration Due Date"), PLANTNO("Plant No"), SERIALNO("Serial No");

	private String displayName;

	private NewQuoteInstSortType(String displayName)
	{
		this.displayName = displayName;
	}

	public String getDisplayName()
	{
		return this.displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

}
