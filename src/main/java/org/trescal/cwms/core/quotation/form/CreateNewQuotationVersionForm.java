package org.trescal.cwms.core.quotation.form;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.quotation.entity.quotation.Quotation;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CreateNewQuotationVersionForm
{
	private boolean createAsNewVersion;
	private List<Integer> cusCalConIds;
	private Integer cusGenConIds;
	private List<Integer> headingids;
	private boolean includeCustomConditions;
	private boolean includeDefaultQuoteNotes;
	private List<Integer> itemnos;
	@Valid
	private Quotation newQuotation;
	private List<Integer> noteids;
	private boolean noteThis;
	private boolean preserveModules;
	@NotNull(message="{error.value.notselected}")
	private Integer sourcedById;
	@NotNull(message="{error.value.notselected}")
	private Integer personId;
	@NotNull
	private Integer businessCompanyId;

}