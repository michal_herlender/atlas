package org.trescal.cwms.core.quotation.entity.quoteheading.db;

import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.QuotationItemComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;

@Service("QuoteHeadingService")
public class QuoteHeadingServiceImpl implements QuoteHeadingService
{
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private QuoteHeadingDao qhDao;
	@Autowired
	private QuotationService quoteServ;

	@Override
	public void delete(QuoteHeading qh)
	{
		this.qhDao.remove(qh);
	}

	@Override
	public List<QuoteHeading> findAll(List<Integer> ids)
	{
		return this.qhDao.findAll(ids);
	}

	@Override
	public QuoteHeading findDefaultQuoteHeading(int quoteid)
	{
		return this.qhDao.findDefaultQuoteHeading(quoteid);
	}

	@Override
	public Set<QuoteHeading> findQuotationHeadings(int quoteid)
	{
		return this.qhDao.findQuotationHeadings(quoteid);
	}

	@Override
	public Set<QuoteHeading> findQuotationHeadings(int quoteid, Integer excludeHeaderId)
	{
		return this.qhDao.findQuotationHeadings(quoteid, excludeHeaderId);
	}

	@Override
	public QuoteHeading findQuoteHeading(int headingId)
	{
		return this.qhDao.find(headingId);
	}

	@Override
	public QuoteHeading findQuoteheadingByItemNo(int quotationId, int headingNo)
	{
		return this.qhDao.findQuoteheadingByItemNo(quotationId, headingNo);
	}

	@Override
	public QuoteHeading findSystemDefaultQuoteHeading(int quoteid)
	{
		return this.qhDao.findSystemDefaultQuoteHeading(quoteid);
	}

	@Override
	public QuoteHeading createDefaultHeading(Quotation q, Locale locale)
	{
		QuoteHeading heading = new QuoteHeading();
		heading.setHeadingDescription(messageSource.getMessage("createquot.defaultheadingdescription", null, "Default heading for quotation items", locale));
		heading.setHeadingNo(1);
		heading.setHeadingName(messageSource.getMessage("systemdefault.systemdefault", null, "System Default", locale));
		heading.setQuotation(q);
		heading.setSystemDefault(true);
		heading.setQuoteitems(new TreeSet<Quotationitem>(new QuotationItemComparator()));
		return heading;
	}
	
	@Override
	public QuoteHeading copyDefaultHeading(Quotation quote, Quotation newQuote) {
		QuoteHeading source = this.findSystemDefaultQuoteHeading(quote.getId());
		
		QuoteHeading heading = new QuoteHeading();
		heading.setHeadingDescription(source == null ? "" : source.getHeadingDescription());
		heading.setHeadingNo(1);
		heading.setHeadingName(source == null ? "" : source.getHeadingDescription());
		heading.setQuotation(newQuote);
		heading.setSystemDefault(true);
		return heading;
	}
	
	/**
	 * Updates the QuoteHeadings attached to the quotation to set the specified
	 * heading as being the (potentially updated) default heading
	 * @param quotation
	 * @param headingId
	 */
	@Override
	public void selectNewDefaultHeading(Quotation quotation, int headingId) {
		for (QuoteHeading heading : quotation.getQuoteheadings()) {
			if (heading.getHeadingId() == headingId) {
				heading.setSystemDefault(true);
			}
			else {
				heading.setSystemDefault(false);
			}
			
		}
	}

	@Override
	public void insert(QuoteHeading qh)
	{
		this.qhDao.persist(qh);
	}

	@Override
	public ResultWrapper moveQuoteHeading(int oldPosId, int newPosId, int quotationId) throws Exception
	{
		// first find the quotation heading to be moved
		QuoteHeading oldPosQH = this.qhDao.find(oldPosId);
		// then find the quotation heading where the new heading needs to be
		// moved to
		QuoteHeading newPosQH = this.qhDao.find(newPosId);
		// check that these items are not null?
		if ((oldPosQH != null) && (newPosQH != null))
		{
			// get numbers of headings
			int oldPos = oldPosQH.getHeadingNo();
			int newPos = newPosQH.getHeadingNo();
			// check that two headings with same heading number have not been
			// passed?
			if (oldPos != newPos)
			{
				// set the position initially to 0 so that move other quotation
				// heading into
				// empty positions
				oldPosQH.setHeadingNo(0);
				// update quote heading
				this.update(oldPosQH);
				// initialise temp quote heading
				QuoteHeading temp = null;
				// the quote heading is moving up the list
				if (oldPos > newPos)
				{
					// get each quote heading in between the new position and
					// the
					// old
					// position and
					// move them down one place
					for (int i = oldPos; i != newPos; i--)
					{
						temp = this.findQuoteheadingByItemNo(quotationId, (i - 1));
						temp.setHeadingNo(i);
						this.update(temp);
					}
				}
				// the quote heading is moving down the list
				else
				{
					// get each quote heading in between the new position and
					// the
					// old
					// position and
					// move them up one place
					for (int i = oldPos + 1; i != newPos + 1; i++)
					{
						temp = this.findQuoteheadingByItemNo(quotationId, i);
						temp.setHeadingNo(i - 1);
						this.update(temp);
					}
				}

				// set the position of the quote heading that has been moved
				// from 0
				// (it's
				// temporary position) to it's proper (new) position because the
				// position will now be vacant
				oldPosQH.setHeadingNo(newPos);
				this.update(oldPosQH);
				// get the quotation object
				Quotation q = this.quoteServ.get(quotationId);
				// update the quotation object
				this.quoteServ.update(q);
				// return successful wrapper
				return new ResultWrapper(true, null, null, null);
			}
			else
			{
				// return un-successful result wrapper
				return new ResultWrapper(false, "You cannot select the same position for both headings", null, null);
			}
		}
		else
		{
			// return un-successful result wrapper
			return new ResultWrapper(false, "One of the quotation headings could not be found", null, null);
		}
	}

	@Override
	public void saveOrUpdateAll(List<QuoteHeading> headings)
	{
		this.qhDao.saveOrUpdateAll(headings);
	}

	@Override
	public Quotation sortQuoteHeadingNos(Quotation q)
	{
		// new empty set of quote headings sorted with latest comparator
		TreeSet<QuoteHeading> headings = new TreeSet<QuoteHeading>(q.getHeadingSortType().getComparator());
		// add in all current quote headings
		headings.addAll(q.getQuoteheadings());
		// initialise count for new heading numbers
		int headingno = 1;
		// create new heading numbers for all headings on quote
		for (QuoteHeading qh : headings)
		{
			// set heading number of this quote heading
			qh.setHeadingNo(headingno);
			// increment heading number
			headingno++;
		}
		// set new set back into quote
		q.getQuoteheadings().addAll(headings);
		// merge quote
		this.quoteServ.merge(q);
		// return updated quote
		return q;
	}

	@Override
	public void update(QuoteHeading qh)
	{
		this.qhDao.update(qh);
	}
}
