package org.trescal.cwms.core.quotation.entity.priorquotestatus;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Richard
 */
@Entity
@Table(name = "priorquotestatus")
public class PriorQuoteStatus
{
	private Integer pqsId;
	private Quotation quotation;
	private Contact revokedBy;
	private Date revokedOn;
	private QuoteStatus revokedTo;
	private Contact setBy;
	private Date setOn;
	private QuoteStatus status;

	/**
	 *  
	 */
	public PriorQuoteStatus()
	{

	}

	public PriorQuoteStatus(QuoteStatus status, Contact setBy, Date setOn, QuoteStatus revokedTo, Contact revokedBy, Date revokedOn, Quotation quotation)
	{
		super();
		this.status = status;
		this.setBy = setBy;
		this.setOn = setOn;
		this.revokedTo = revokedTo;
		this.revokedBy = revokedBy;
		this.revokedOn = revokedOn;
		this.quotation = quotation;
	}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pqsid", nullable = false, unique = true)
    @Type(type = "int")
    public Integer getPqsId() {
        return this.pqsId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quoteid", nullable = false)
    public Quotation getQuotation() {
        return this.quotation;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "revokedby", nullable = false)
    public Contact getRevokedBy() {
        return this.revokedBy;
    }

    @Column(name = "revokedon")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getRevokedOn() {
        return this.revokedOn;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "revokedto", nullable = false)
    public QuoteStatus getRevokedTo() {
        return this.revokedTo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "setby", nullable = false)
    public Contact getSetBy() {
        return this.setBy;
    }

    @Column(name = "seton")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getSetOn() {
        return this.setOn;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "statusid", nullable = false)
    public QuoteStatus getStatus() {
        return this.status;
    }

    public void setPqsId(Integer pqsId) {
        this.pqsId = pqsId;
    }

	public void setQuotation(Quotation quotation)
	{
		this.quotation = quotation;
	}

	public void setRevokedBy(Contact revokedBy)
	{
		this.revokedBy = revokedBy;
	}

	public void setRevokedOn(Date to)
	{
		this.revokedOn = to;
	}

	public void setRevokedTo(QuoteStatus revokedTo)
	{
		this.revokedTo = revokedTo;
	}

	public void setSetBy(Contact setBy)
	{
		this.setBy = setBy;
	}

	public void setSetOn(Date setOn)
	{
		this.setOn = setOn;
	}

	public void setStatus(QuoteStatus status)
	{
		this.status = status;
	}
}
