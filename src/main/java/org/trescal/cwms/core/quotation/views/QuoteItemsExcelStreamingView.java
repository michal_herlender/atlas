package org.trescal.cwms.core.quotation.views;

import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxStreamingView;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.reports.view.XlsxCellStyleHolder;
import org.trescal.cwms.core.tools.reports.view.XlsxViewDecorator;

@Component
public class QuoteItemsExcelStreamingView extends AbstractXlsxStreamingView {

	private static boolean autosize = true;
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		@SuppressWarnings("unchecked")
		Set<Quotationitem> items = (Set<Quotationitem>) model.get("items");
	
		TranslationService tranServ = (TranslationService) Objects.requireNonNull(getApplicationContext()).getBean("translationService");

		Locale userLocale = LocaleContextHolder.getLocale();
		
		// create a new Excel
				Sheet sheet = workbook.createSheet("Quotation Items");
				sheet.setDefaultColumnWidth(30);
				
				SXSSFWorkbook wb = (SXSSFWorkbook) workbook;
				XlsxCellStyleHolder styleHolder = new XlsxCellStyleHolder(wb);
				XlsxViewDecorator decorator = new XlsxViewDecorator(sheet, wb, autosize, styleHolder);

				// create header row
				int colIndex = 0;
				int rowIndex = 0;
				CellStyle styleHeader = decorator.getStyleHolder().getHeaderStyle();
				
				decorator.createCell(rowIndex, colIndex++, "Barcode", styleHeader);
				decorator.createCell(rowIndex, colIndex++, "Plant Number", styleHeader);
				decorator.createCell(rowIndex, colIndex++, "Make", styleHeader);
				decorator.createCell(rowIndex, colIndex++, "Model", styleHeader);
				decorator.createCell(rowIndex, colIndex++, "Description", styleHeader);
				decorator.createCell(rowIndex, colIndex++, "Serial Number", styleHeader);
				decorator.createCell(rowIndex, colIndex++, "Service Type", styleHeader);
				decorator.createCell(rowIndex, colIndex++, "Cal Price", styleHeader);
				decorator.createCell(rowIndex, colIndex++, "Quantity", styleHeader);
				decorator.createCell(rowIndex, colIndex++, "Discount", styleHeader);
				decorator.createCell(rowIndex, colIndex++, "Total Price", styleHeader);

				int rowCount = 1;       int colIndex1 = 0;
				for (Quotationitem anItem : items) {
					if (rowCount == 1) {
						String quote = anItem.getQuotation().getIdentifier();
						response.setHeader("Content-Disposition", "attachment; filename=\"Quotation_" + quote + ".xlsx\"");
					}
					CellStyle styleText = decorator.getStyleHolder().getTextStyle();
					CellStyle styleInteger = decorator.getStyleHolder().getIntegerStyle();
					int rowIndex1 = 1;
					
					if (anItem.getInst() != null) {
						
						decorator.createCell(rowIndex1, colIndex1++, anItem.getInst().getPlantid(), styleInteger);
						decorator.createCell(rowIndex1, colIndex1++, anItem.getInst().getPlantno(), styleText);
						decorator.createCell(rowIndex1, colIndex1++, anItem.getInst().getModel().getMfr().getName(), styleText);
						decorator.createCell(rowIndex1, colIndex1++, anItem.getInst().getModel().getModel(), styleText);
						
						Set<Translation> translations = anItem.getInst().getModel().getDescription().getTranslations();
						decorator.createCell(rowIndex1, colIndex1++, tranServ.getCorrectTranslation(translations, userLocale), styleText);
						decorator.createCell(rowIndex1, colIndex1++, anItem.getInst().getSerialno(), styleText);
						
					} else if (anItem.getModel() != null) {
						
						decorator.createCell(rowIndex1, colIndex1++, anItem.getModel().getMfr().getName(), styleText);
						decorator.createCell(rowIndex1, colIndex1++, anItem.getModel().getModel(), styleText);
						decorator.createCell(rowIndex1, colIndex1++, tranServ.getCorrectTranslation(anItem.getModel().getDescription().getTranslations(), userLocale), styleText);
						
					}
					decorator.createCell(rowIndex1, colIndex1++, tranServ.getCorrectTranslation(anItem.getServiceType().getShortnameTranslation(), userLocale), styleText);
					decorator.createCell(rowIndex1, colIndex1++, anItem.getPriorDiscountCalPrice(), styleInteger);
					decorator.createCell(rowIndex1, colIndex1++, anItem.getQuantity(), styleInteger);
					decorator.createCell(rowIndex1, colIndex1++, anItem.getCalibrationCost().getDiscountValue(), styleInteger);
					decorator.createCell(rowIndex1, colIndex1++, anItem.getFinalCost(), styleInteger);
				}
				
			}
		}
