package org.trescal.cwms.core.quotation.entity.quotation;

/**
 * Wrapper object that holds data collected from a Quotation metrics query.
 * 
 * @author Richard
 */
public class QuotationMetricFormatter
{
	private int quotationCount;
	private int issued;
	private double percentIssued;
	private int notIssued;
	private double percentNotIssued;
	private int accepted;
	private double percentAccepted;
	private int notAccepted;
	private double percentNotAccepted;

	public int getAccepted()
	{
		return this.accepted;
	}

	public int getIssued()
	{
		return this.issued;
	}

	public int getNotAccepted()
	{
		return this.notAccepted;
	}

	public int getNotIssued()
	{
		return this.notIssued;
	}

	public double getPercentAccepted()
	{
		return this.percentAccepted;
	}

	public double getPercentIssued()
	{
		return this.percentIssued;
	}

	public double getPercentNotAccepted()
	{
		return this.percentNotAccepted;
	}

	public double getPercentNotIssued()
	{
		return this.percentNotIssued;
	}

	public int getQuotationCount()
	{
		return this.quotationCount;
	}

	public void setAccepted(int accepted)
	{
		this.accepted = accepted;
	}

	public void setIssued(int issued)
	{
		this.issued = issued;
	}

	public void setNotAccepted(int notAccepted)
	{
		this.notAccepted = notAccepted;
	}

	public void setNotIssued(int notIssued)
	{
		this.notIssued = notIssued;
	}

	public void setPercentAccepted(double percentAccepted)
	{
		this.percentAccepted = percentAccepted;
	}

	public void setPercentIssued(double percentIssued)
	{
		this.percentIssued = percentIssued;
	}

	public void setPercentNotAccepted(double percentNotAccepted)
	{
		this.percentNotAccepted = percentNotAccepted;
	}

	public void setPercentNotIssued(double percentNotIssued)
	{
		this.percentNotIssued = percentNotIssued;
	}

	public void setQuotationCount(int quotationCount)
	{
		this.quotationCount = quotationCount;
	}

}