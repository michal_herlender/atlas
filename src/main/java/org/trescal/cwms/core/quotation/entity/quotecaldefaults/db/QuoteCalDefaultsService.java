package org.trescal.cwms.core.quotation.entity.quotecaldefaults.db;

import java.util.List;

import org.trescal.cwms.core.quotation.entity.quotecaldefaults.QuoteCalDefaults;

public interface QuoteCalDefaultsService
{
	QuoteCalDefaults findQuoteCalDefaults(int id);
	void insertQuoteCalDefaults(QuoteCalDefaults quotecaldefaults);
	void updateQuoteCalDefaults(QuoteCalDefaults quotecaldefaults);
	void deleteQuoteCalDefaults(QuoteCalDefaults quotecaldefaults);
	List<QuoteCalDefaults> getAllQuoteCalDefaultss();
}