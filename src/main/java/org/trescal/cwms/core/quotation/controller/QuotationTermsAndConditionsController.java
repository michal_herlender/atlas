package org.trescal.cwms.core.quotation.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.calibration.CustomCalibrationCondition;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.calibration.db.CustomCalibrationConditionService;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.general.CustomGeneralCondition;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.DefaultCalibrationCondition;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.db.DefaultCalibrationConditionService;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general.DefaultGeneralCondition;
import org.trescal.cwms.core.quotation.form.CalConditionDto;
import org.trescal.cwms.core.quotation.form.QuoteTermsAndConditionsForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.spring.model.KeyValue;

import lombok.val;
import lombok.extern.slf4j.Slf4j;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
@Slf4j
public class QuotationTermsAndConditionsController {

	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private CustomCalibrationConditionService cusCalConServ;
	@Autowired
	private DefaultCalibrationConditionService defCalConService;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private SubdivService subdivService;

	@ModelAttribute("qtcform")
	protected QuoteTermsAndConditionsForm formBackingObject(
			@RequestParam(name = "id", required = false, defaultValue = "0") Integer id,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		log.debug("Entering QuotationTermsAndConditionsController formBackingObject method ....");
		QuoteTermsAndConditionsForm tcForm = new QuoteTermsAndConditionsForm();
		Quotation quote = this.quoteServ.get(id);
		tcForm.setQuotation(quote);
		// load the terms and conditions for the quotation
		tcForm.setConditions(this.quoteServ.findQuotationConditions(quote));
		Company company = subdivService.get(subdivDto.getKey()).getComp();
		DefaultCalibrationCondition defaultCalCondition = defCalConService.getLatest(id, company);
		tcForm.setGeneralCalCondition(defaultCalCondition == null ? "" : defaultCalCondition.getConditionText());
		List<CalibrationType> caltypes = this.calTypeServ.getCalTypes();
		val conditions = caltypes.stream()
				.map(ct -> CalConditionDto.builder().calTypeId(ct.getCalTypeId()).serviceType(ct.getServiceType())
						.condition(tcForm.getGeneralCalCondition()).build())
				.collect(Collectors.toMap(CalConditionDto::getCalTypeId, Function.identity()));
		val customConditions = quote.getCalConditions().stream()
				.map(cc -> CalConditionDto.builder().calTypeId(cc.getCaltype().getCalTypeId())
						.condition(cc.getConditionText()).serviceType(cc.getCaltype().getServiceType()).build())
				.collect(Collectors.toMap(CalConditionDto::getCalTypeId, Function.identity()));
		conditions.putAll(customConditions);
		tcForm.setCalConditions(new ArrayList<>(conditions.values()));
		val generalConditions = Optional.ofNullable(quote.getGeneralConditions())
				.map(CustomGeneralCondition::getConditionText).orElseGet(() -> Optional
						.ofNullable(quote.getDefaultterms()).map(DefaultGeneralCondition::getConditionText).orElse(""));
		tcForm.setGeneralConditions(generalConditions);
		return tcForm;
	}

	@RequestMapping(value = "/qtcform.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(@ModelAttribute("qtcform") QuoteTermsAndConditionsForm tcForm) throws Exception {
		log.debug("Entering QuotationTermsAndConditionsController ModelAndView method ....");
		Quotation quote = tcForm.getQuotation();
		quote.setUsingDefaultTermsAndConditions(tcForm.isUsingDefaultTermsAndConditions());
		// Save the quotation
		this.quoteServ.merge(quote);
		// if the custom terms and conditions flag is set then insert/update any
		// custom conditions
		if (!tcForm.isUsingDefaultTermsAndConditions()) {
			// get and save the custom general conditions
			CustomGeneralCondition cg = getCustomGeneralConditionOrNew(quote);
			cg.setConditionText(tcForm.getGeneralConditions() == null ? "" : tcForm.getGeneralConditions());
			cg.setQuotation(quote);
			// get and save the custom calibration conditions
			// iterate over each calibration type available to this
			// quotation
			tcForm.getCalConditions().forEach(cc -> {
				Integer caltypeId = cc.getCalTypeId();
				String customCalCondition = cc.getCondition();
				CustomCalibrationCondition cusCal = Optional.ofNullable(quote.getCalConditions()).flatMap(
						c -> c.stream().filter(cu -> caltypeId.equals(cu.getCaltype().getCalTypeId())).findFirst())
						.orElseGet(CustomCalibrationCondition::new);
				cusCal.setCaltype(this.calTypeServ.find(caltypeId));
				cusCal.setConditionText(customCalCondition);
				cusCal.setQuotation(quote);
				this.cusCalConServ.merge(cusCal);
			});
		} else {
			// set general and custom conditions back to null so re-populated on
			// custom page
			quote.setGeneralConditions(null);
			quote.setCalConditions(null);
		}
		return new ModelAndView(new RedirectView("/viewquotation.htm?id=" + quote.getId(), true));
	}

	private CustomGeneralCondition getCustomGeneralConditionOrNew(Quotation quote) {
		if (quote.getGeneralConditions() == null) {
			// if the quotation has no custom general conditions set already
			// then create a new one
			CustomGeneralCondition cg = new CustomGeneralCondition();
			quote.setGeneralConditions(cg);
			cg.setOrganisation(quote.getOrganisation());
		}
		return quote.getGeneralConditions();
	}

	@RequestMapping(value = "/qtcform.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute("qtcform") QuoteTermsAndConditionsForm tcForm) {
		// reference to quoteform
		Quotation quote = tcForm.getQuotation();
		// load all general default conditions
		Map<String, Object> model = new HashMap<>();
		model.put("general", quote.getDefaultterms());
		return new ModelAndView("trescal/core/quotation/qtcform", model);
	}
}
