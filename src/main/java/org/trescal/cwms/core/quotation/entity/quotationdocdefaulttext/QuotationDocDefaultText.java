package org.trescal.cwms.core.quotation.entity.quotationdocdefaulttext;

import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.quotation.entity.quotationdoccustomtext.QuotationDocText;

@Entity
@Table(name="quotationdocdefaulttext")
public class QuotationDocDefaultText extends Auditable implements QuotationDocText {
	private Integer id;
	private Locale locale;
	private String subject;
	private String body;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	
	@NotNull
	@Column(name = "locale", nullable = false)
	public Locale getLocale() {
		return locale;
	}
	
	@Override
	@NotNull
	@Length(min=1, max=100)
	@Column(name = "subject", nullable = false, columnDefinition="nvarchar(100)")
	public String getSubject() {
		return subject;
	}
	
	@Override
	@NotNull
	@Length(min=1, max=2000)
	@Column(name = "body", nullable = false, columnDefinition="nvarchar(2000)")
	public String getBody() {
		return body;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public void setBody(String body) {
		this.body = body;
	}
}
