package org.trescal.cwms.core.quotation.entity.quotationrequest;

public enum QuotationRequestDetailsSource
{
	EXISTING, FREEHAND;
}
