package org.trescal.cwms.core.quotation.entity.quotecaltypedefault.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.quotation.entity.quotecaltypedefault.QuoteCaltypeDefault;

@Repository("QuoteCaltypeDefaultDao")
public class QuoteCaltypeDefaultDaoImpl extends BaseDaoImpl<QuoteCaltypeDefault, Integer> implements QuoteCaltypeDefaultDao {
	
	@Override
	protected Class<QuoteCaltypeDefault> getEntity() {
		return QuoteCaltypeDefault.class;
	}
}