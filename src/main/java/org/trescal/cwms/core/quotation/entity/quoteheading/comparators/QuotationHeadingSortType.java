package org.trescal.cwms.core.quotation.entity.quoteheading.comparators;

import java.util.Comparator;
import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;

public enum QuotationHeadingSortType
{
	ALPHA("quoteheadingsorttype.alphabetical", "Alphabetical", new QuotationHeadingAlphaComparator()), 
	CUSTOM("quoteheadingsorttype.custom", "Custom", new QuotationHeadingCustomComparator()), 
	ID("quoteheadingsorttype.asadded", "As Added", new QuotationHeadingIdComparator());

	private ReloadableResourceBundleMessageSource messages;
	private Locale loc = null;
	private String messageCode;
	private Comparator<QuoteHeading> comparator;
	private String displayName;

	@Component
	public static class QuotationHeadingSortTypeMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (QuotationHeadingSortType qhst : EnumSet.allOf(QuotationHeadingSortType.class))
            	qhst.setMessageSource(messages);
        }
	}
	
	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}

	
	QuotationHeadingSortType(String messageCode, String displayName, Comparator<QuoteHeading> comparator)
	{
		this.messageCode = messageCode;
		this.displayName = displayName;
		this.comparator = comparator;
	}

	public Comparator<QuoteHeading> getComparator()
	{
		return this.comparator;
	}

	public String getDisplayName()
	{
		loc = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.displayName, loc);
		}
		return this.toString();
	}

	public void setComparator(Comparator<QuoteHeading> comparator)
	{
		this.comparator = comparator;
	}

	/*public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}*/
	
	public String getMessageCode() {
		return messageCode;
	}
}
