package org.trescal.cwms.core.quotation.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.companysettings.db.CompanySettingsForAllocatedCompanyService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.quotation.dto.CompanySettingsForAllocatedCompanyDTO;
import org.trescal.cwms.core.quotation.dto.QuotationProjectionDTO;
import org.trescal.cwms.core.quotation.dto.QuotationRequestProjectionDTO;
import org.trescal.cwms.core.quotation.entity.QuoteRequested;
import org.trescal.cwms.core.quotation.entity.QuoteRequestedComparator;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequestStatus;
import org.trescal.cwms.core.quotation.entity.quotationrequest.db.QuotationRequestService;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.quotation.entity.quotestatusgroup.QuoteStatusGroup;
import org.trescal.cwms.core.quotation.form.NewQuotationSearchForm;
import org.trescal.cwms.core.quotation.form.NewQuotationSearchValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.userright.entity.limitcompany.db.LimitCompanyService;
import org.trescal.cwms.spring.model.KeyValue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME,
		Constants.SESSION_ATTRIBUTE_COMPANY })
public class NewQuotationSearchController {
	private static final String FORM_VIEW = "trescal/core/quotation/newcreatequote";

	@Autowired
	private ContactService contactService;
	@Autowired
	private QuotationService quotationService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private StatusService statusService;
	@Autowired
	private QuotationRequestService quotationRequestService;
	@Value("#{props['cwms.config.quotation.resultsyearfilter']}")
	private Integer resultsYearFilter;
	@Autowired
	private NewQuotationSearchValidator validator;
	@Autowired
	private LimitCompanyService limitCompanyService;
	@Autowired
	private CompanySettingsForAllocatedCompanyService companySettingsService;

	@ModelAttribute("command")
	protected NewQuotationSearchForm formBackingObject() throws ServletException {
		NewQuotationSearchForm form = new NewQuotationSearchForm();
		form.setResultsYearFilter(this.resultsYearFilter);
		form.setQuoteRequestedList(new PagedResultSet<QuoteRequested>(Constants.RESULTS_PER_PAGE, 1));
		form.setQuotationsRequiringAction(new PagedResultSet<QuotationProjectionDTO>(Constants.RESULTS_PER_PAGE, 1));
		form.setTabIndex(0);
		// grab lists of interesting status's
		return form;
	}

	// @InitBinder
	// protected void initBinder(ServletRequestDataBinder binder) throws
	// Exception {
	// MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
	// binder.registerCustomEditor(Date.class, editor);
	// }

	@RequestMapping(value = "/createquote.htm", method = RequestMethod.POST)
	public ModelAndView onSubmit(
			@ModelAttribute(value = Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdiv,
			@ModelAttribute("command") NewQuotationSearchForm qsf, BindingResult bindingResult, Locale locale)
			throws Exception {
		validator.validate(qsf, bindingResult);
		if (bindingResult.hasErrors())
			return referenceData(locale, allocatedCompanyDto, subdiv, qsf);
		else {
			qsf.setAllocatedSubdivId(subdiv.getKey());
			PagedResultSet<QuotationProjectionDTO> rs = this.quotationService.queryQuotationNew(qsf,
					new PagedResultSet<QuotationProjectionDTO>(
							qsf.getResultsPerPage() == 0 ? Constants.RESULTS_PER_PAGE : qsf.getResultsPerPage(),
							qsf.getPageNo() == 0 ? 1 : qsf.getPageNo()),
					locale);
			// two collections to collect client/allocated company Ids
			Set<Integer> clientCompnayIds = new HashSet<Integer>();
			Set<Integer> allocatedCompnayIds = new HashSet<Integer>();
			collectCompanyIds(rs.getResults(), clientCompnayIds, allocatedCompnayIds);
			if (!clientCompnayIds.isEmpty() && !allocatedCompnayIds.isEmpty()) {
				List<CompanySettingsForAllocatedCompanyDTO> companySettings = companySettingsService
						.getAllSettingsDtosForAllocatedCompany(clientCompnayIds, allocatedCompnayIds);
				// set Companies settings
				setCompanySettingsForQuote(rs.getResults(), companySettings);
			}
			qsf.setRs(rs);
			qsf.setQuotes((Collection<QuotationProjectionDTO>) rs.getResults());
			return new ModelAndView("trescal/core/quotation/newquoteresults", "command", qsf);
		}
	}

	@RequestMapping(value = "/createquote.htm", params = "forced=forced", method = RequestMethod.GET)
	public ModelAndView forceSubmit(Locale locale,
			@ModelAttribute(value = Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdiv,
			@ModelAttribute("command") NewQuotationSearchForm qsf, BindingResult bindingResult,
			@RequestParam(value = "mid", required = false, defaultValue = "-1") Integer modelId,
			@RequestParam(value = "compid", required = false, defaultValue = "-1") Integer companyId,
			@RequestParam(value = "subdivid", required = false, defaultValue = "-1") Integer subdivId,
			@RequestParam(value = "contactid", required = false, defaultValue = "-1") Integer contactId,
			@RequestParam(value = "alltime", required = false, defaultValue = "false") Boolean allTime,
			@RequestParam(value = "yearfilter", required = false, defaultValue = "0") Integer yearFilter)
			throws Exception {
		if (modelId >= 0)
			qsf.setModelid(modelId);
		if (companyId >= 0){
			qsf.setCoid(companyId);
			qsf.setAllocatedCompanyId(allocatedCompanyDto.getKey());
		}
		if (subdivId >= 0){
			qsf.setSubdivid(subdivId);
			qsf.setAllocatedCompanyId(allocatedCompanyDto.getKey());
		}	
		if (contactId >= 0){
			qsf.setPersonid(contactId);
			qsf.setAllocatedCompanyId(allocatedCompanyDto.getKey());
		}
			
		if (allTime)
			qsf.setResultsYearFilter(null);
		if (yearFilter > 0)
			qsf.setResultsYearFilter(yearFilter);
		return onSubmit(subdiv, allocatedCompanyDto, qsf, bindingResult, locale);
	}

	@RequestMapping(value = "/createquote.htm", method = RequestMethod.POST, params = { "pagination", "search" })
	protected ModelAndView referenceDataPaginationAndSearch(Locale locale,
			@ModelAttribute(value = Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute("command") NewQuotationSearchForm qsf) throws Exception {
		if (qsf.getTabIndex() == 0) {
			qsf.getQuoteRequestedList().setStartResultsFrom(1);
			qsf.getQuoteRequestedList().setCurrentPage(1);
		} else if (qsf.getTabIndex() == 1) {
			qsf.getQuotationsRequiringAction().setStartResultsFrom(1);
			qsf.getQuotationsRequiringAction().setCurrentPage(1);
		}
		return referenceData(locale, allocatedCompanyDto, subdivDto, qsf);
	}

	@RequestMapping(value = "/createquote.htm", method = RequestMethod.POST, params = { "pagination", "!search" })
	protected ModelAndView referenceDataPagination(Locale locale,
			@ModelAttribute(value = Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute("command") NewQuotationSearchForm qsf) throws Exception {
		return referenceData(locale, allocatedCompanyDto, subdivDto, qsf);
	}

	@RequestMapping(value = "/createquote.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(Locale locale,
			@ModelAttribute(value = Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompanyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute("command") NewQuotationSearchForm qsf) throws Exception {
		// get allocatedSubdivision
		Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
		// two collections to collect client/allocated company Ids
		Set<Integer> clientCompnayIds = new HashSet<Integer>();
		Set<Integer> allocatedCompnayIds = new HashSet<Integer>();
		// all requiring action (i.e. new/waiting issue) && awaiting update and
		// collecting company ids from result
		PagedResultSet<QuotationProjectionDTO> quotesAwaitingAction = new PagedResultSet<QuotationProjectionDTO>(
				qsf.getQuotationsRequiringAction().getResultsPerPage(),
				qsf.getQuotationsRequiringAction().getCurrentPage());
		quotesAwaitingAction.setStartResultsFrom(qsf.getQuotationsRequiringAction().getStartResultsFrom());
		this.quotationService.getPaginationAllQuotesInStatusGroupNew(QuoteStatusGroup.REQUIRINGATTENTION,
				subdivDto.getKey(), quotesAwaitingAction, qsf, locale);
		collectCompanyIds(quotesAwaitingAction.getResults(), clientCompnayIds, allocatedCompnayIds);
		// get 40 most recent quotations issued and collecting company ids from
		// result
		List<QuotationProjectionDTO> mostRecentQuotes = this.quotationService.getMostRecentIssuedQuotationsNew(40,
				subdivDto.getKey(), locale);
		collectCompanyIds(mostRecentQuotes, clientCompnayIds, allocatedCompnayIds);
		// mash the web quotations and web quote requests together to make one
		// 'web' list and collecting company ids from result
		List<QuotationProjectionDTO> webQuotesRequiringAction = this.quotationService
				.getAllQuotesInStatusGroupNew(QuoteStatusGroup.WEBREQUESTS, subdivDto.getKey(), locale);
		collectCompanyIds(webQuotesRequiringAction, clientCompnayIds, allocatedCompnayIds);

		List<QuotationRequestProjectionDTO> quotationRequests = this.quotationRequestService
				.getQuotationRequestByStatusNew(QuotationRequestStatus.REQUESTED, subdivDto.getKey());
		collectComapnyIds(quotationRequests, clientCompnayIds, allocatedCompnayIds);

		if (!clientCompnayIds.isEmpty() && !allocatedCompnayIds.isEmpty()) {
			// get all company settings
			List<CompanySettingsForAllocatedCompanyDTO> companySettings = companySettingsService
					.getAllSettingsDtosForAllocatedCompany(clientCompnayIds, allocatedCompnayIds);

			// set all company settings
			setCompanySettingsForQuote(quotesAwaitingAction.getResults(), companySettings);
			setCompanySettingsForQuote(mostRecentQuotes, companySettings);
			setCompanySettingsForQuote(webQuotesRequiringAction, companySettings);
			setCompanySettingsForQuoteRequest(quotationRequests, companySettings);
		}

		// Update NewQuotationSearchForm
		qsf.setQuotationsRequiringAction(quotesAwaitingAction);
		// get 40 most recent quotations issued
		qsf.setQuotationsIssued(mostRecentQuotes);
		List<QuoteRequested> quotesRequested = new ArrayList<QuoteRequested>();

		// set the web quotations and web quote requests filters
		quotesRequested.addAll(webQuotesRequiringAction
				.stream().filter(e -> (qsf.getQuoteRequesteContactId() == 0 && qsf.getQuoteRequesteSubdivId() == 0)
						|| (qsf.getQuoteRequesteContactId() != 0
								&& qsf.getQuoteRequesteSubdivId() != 0
								&& e.getSourcedBy().getSubdiv().getSubdivid().intValue() == qsf
										.getQuoteRequesteSubdivId()
								&& e.getSourcedBy().getPersonid().intValue() == qsf.getQuoteRequesteContactId())
						|| (qsf.getQuoteRequesteSubdivId() != 0 && qsf.getQuoteRequesteContactId() == 0
								&& e.getSourcedBy().getSubdiv().getSubdivid().intValue() == qsf
										.getQuoteRequesteSubdivId())
						|| (qsf.getQuoteRequesteContactId() != 0 && qsf.getQuoteRequesteSubdivId() == 0
								&& e.getSourcedBy().getPersonid().intValue() == qsf.getQuoteRequesteContactId()))
				.collect(Collectors.toList()));
		quotesRequested.addAll(quotationRequests
				.stream().filter(e -> (qsf.getQuoteRequesteContactId() == 0 && qsf.getQuoteRequesteSubdivId() == 0)
						|| (qsf.getQuoteRequesteContactId() != 0
								&& qsf.getQuoteRequesteSubdivId() != 0
								&& e.getLoggedBy().getSubdiv().getSubdivid().intValue() == qsf
										.getQuoteRequesteSubdivId()
								&& e.getLoggedBy().getPersonid().intValue() == qsf.getQuoteRequesteContactId())
						|| (qsf.getQuoteRequesteSubdivId() != 0 && qsf.getQuoteRequesteContactId() == 0
								&& e.getLoggedBy().getSubdiv().getSubdivid().intValue() == qsf
										.getQuoteRequesteSubdivId())
						|| (qsf.getQuoteRequesteContactId() != 0 && qsf.getQuoteRequesteSubdivId() == 0
								&& e.getLoggedBy().getPersonid().intValue() == qsf.getQuoteRequesteContactId()))
				.collect(Collectors.toList()));

		// sort the quotesRequested and set QuoteRequestedList results and
		// resultsCount
		Collections.sort(quotesRequested, new QuoteRequestedComparator());
		qsf.getQuoteRequestedList().setResultsCount(quotesRequested.size());
		qsf.getQuoteRequestedList()
				.setResults(quotesRequested.stream().skip(qsf.getQuoteRequestedList().getStartResultsFrom())
						.limit(qsf.getQuoteRequestedList().getResultsPerPage()).collect(Collectors.toList()));

		Map<String, Object> refData = new HashMap<String, Object>();
		refData.put("resultsyears", Arrays.asList(1, 2, 3, 4, 5));
		refData.put("businesscons", this.contactService.getAllCompanyContacts(allocatedSubdiv.getComp().getCoid()));
		refData.put("quotestatus", this.statusService.getAllProjectionStatuss(QuoteStatus.class, locale));

		refData.put("sourcedByContBySubdivRequest",
				getContactsBySubdivsAsJsonRequest(quotationRequests, webQuotesRequiringAction));
		refData.put("sourcedByContBySubdivAC", qsf.getSourcedByContBySubdivAC());
		refData.put("sourcedByContBySubdivIssued", getContactsBySubdivsAsJson(qsf.getQuotationsIssued()));
		refData.put("quotesAllowIssue", limitCompanyService.getQuotesDTOUnderLimit(quotesAwaitingAction.getResults(),
				SecurityContextHolder.getContext().getAuthentication().getAuthorities(), allocatedSubdiv.getComp()));

		refData.put("subdivsOfLoggedinCompany",
				this.subdivService.getSubdivProjectionDTOsByCompany(allocatedCompanyDto.getKey()));
		refData.put("currentSubdiv", subdivDto);

		return new ModelAndView(FORM_VIEW, refData);
	}

	private void collectComapnyIds(Collection<QuotationRequestProjectionDTO> collection, Set<Integer> clientCompnayIds,
			Set<Integer> allocatedCompnayIds) {
		Set<Integer> temp;
		clientCompnayIds.addAll(collection.stream().map(e -> e.getCon().getSubdiv().getCompany().getCoid())
				.collect(Collectors.toSet()));
		temp = collection.stream()
				.filter(t -> t.getLoggedBy() != null && t.getLoggedBy().getSubdiv() != null
						&& t.getLoggedBy().getSubdiv().getCompany() != null)
				.map(e -> e.getLoggedBy().getSubdiv().getCompany().getCoid()).collect(Collectors.toSet());
		if (temp != null)
			allocatedCompnayIds.addAll(temp);
		temp = collection.stream().filter(t -> t.getComp() != null).map(e -> e.getComp().getCoid())
				.collect(Collectors.toSet());
		if (temp != null)
			clientCompnayIds.addAll(temp);
		clientCompnayIds.remove(null);
		allocatedCompnayIds.remove(null);
	}

	private void collectCompanyIds(Collection<QuotationProjectionDTO> collection, Set<Integer> clientCompnayIds,
			Set<Integer> allocatedCompnayIds) {
		clientCompnayIds.addAll(collection.stream().map(e -> e.getContactDto().getSubdiv().getCompany().getCoid())
				.collect(Collectors.toSet()));
		Set<Integer> temp = collection.stream()
				.filter(t -> t.getSourcedBy() != null && t.getSourcedBy().getSubdiv() != null
						&& t.getSourcedBy().getSubdiv().getCompany() != null)
				.map(e -> e.getSourcedBy().getSubdiv().getCompany().getCoid()).collect(Collectors.toSet());
		if (temp != null)
			allocatedCompnayIds.addAll(temp);
		temp = collection.stream()
				.filter(t -> t.getIssueby() != null && t.getIssueby().getSubdiv() != null
						&& t.getIssueby().getSubdiv().getCompany() != null)
				.map(e -> e.getIssueby().getSubdiv().getCompany().getCoid()).collect(Collectors.toSet());
		if (temp != null)
			allocatedCompnayIds.addAll(temp);
	}

	private void setCompanySettingsForQuote(Collection<QuotationProjectionDTO> collection,
			List<CompanySettingsForAllocatedCompanyDTO> companySettings) {
		collection.stream().forEach(e -> {
			CompanySettingsForAllocatedCompanyDTO eSettingDtoForContact = companySettings.stream()
					.filter(i -> i.getAllocatedcompanyid().equals(e.getOrganisationId())
							&& i.getCompanyid().equals(e.getContactDto().getSubdiv().getCompany().getCoid()))
					.findFirst().orElse(null);
			CompanySettingsForAllocatedCompanyDTO eSettingDtoForSourcedBy = null;
			CompanySettingsForAllocatedCompanyDTO eSettingDtoForIssuedBy = null;
			if (e.getSourcedBy() != null)
				eSettingDtoForSourcedBy = companySettings.stream().filter(
						i -> i.getAllocatedcompanyid().equals(e.getSourcedBy().getSubdiv().getCompany().getCoid())
								&& i.getCompanyid().equals(e.getContactDto().getSubdiv().getCompany().getCoid()))
						.findFirst().orElse(null);
			if (e.getIssueby() != null)
				eSettingDtoForIssuedBy = companySettings.stream()
						.filter(i -> i.getAllocatedcompanyid().equals(e.getIssueby().getSubdiv().getCompany().getCoid())
								&& i.getCompanyid().equals(e.getContactDto().getSubdiv().getCompany().getCoid()))
						.findFirst().orElse(null);
			if (eSettingDtoForContact != null) {
				e.getContactDto().getSubdiv().getCompany().setActive(eSettingDtoForContact.getActive());
				e.getContactDto().getSubdiv().getCompany().setOnstop(eSettingDtoForContact.getOnstop());
				;
			}
			if (eSettingDtoForSourcedBy != null) {
				e.getSourcedBy().getSubdiv().getCompany().setActive(eSettingDtoForSourcedBy.getActive());
				e.getSourcedBy().getSubdiv().getCompany().setOnstop(eSettingDtoForSourcedBy.getOnstop());
				;
			}
			if (eSettingDtoForIssuedBy != null) {
				e.getIssueby().getSubdiv().getCompany().setActive(eSettingDtoForIssuedBy.getActive());
				e.getIssueby().getSubdiv().getCompany().setOnstop(eSettingDtoForIssuedBy.getOnstop());
				;
			}

		});
	}

	private void setCompanySettingsForQuoteRequest(Collection<QuotationRequestProjectionDTO> collection,
			List<CompanySettingsForAllocatedCompanyDTO> companySettings) {
		collection.stream().forEach(e -> {
			CompanySettingsForAllocatedCompanyDTO eSettingDtoForCon = companySettings.stream()
					.filter(i -> i.getAllocatedcompanyid().equals(e.getOrganisationId())
							&& i.getCompanyid().equals(e.getCon().getSubdiv().getCompany().getCoid()))
					.findFirst().orElse(null);
			CompanySettingsForAllocatedCompanyDTO eSettingDtoForLoggedBy = null;
			CompanySettingsForAllocatedCompanyDTO eSettingDtoForComp = null;
			if (e.getLoggedBy() != null)
				eSettingDtoForLoggedBy = companySettings.stream().filter(
						i -> i.getAllocatedcompanyid().equals(e.getLoggedBy().getSubdiv().getCompany().getCoid())
								&& i.getCompanyid().equals(e.getCon().getSubdiv().getCompany().getCoid()))
						.findFirst().orElse(null);
			if (e.getComp() != null)
				eSettingDtoForComp = companySettings.stream()
						.filter(i -> i.getAllocatedcompanyid().equals(e.getOrganisationId())
								&& i.getCompanyid().equals(e.getComp().getCoid()))
						.findFirst().orElse(null);

			if (eSettingDtoForCon != null) {
				e.getCon().getSubdiv().getCompany().setActive(eSettingDtoForCon.getActive());
				e.getCon().getSubdiv().getCompany().setOnstop(eSettingDtoForCon.getOnstop());
				;
			}
			if (eSettingDtoForLoggedBy != null) {
				e.getLoggedBy().getSubdiv().getCompany().setActive(eSettingDtoForLoggedBy.getActive());
				e.getLoggedBy().getSubdiv().getCompany().setOnstop(eSettingDtoForLoggedBy.getOnstop());
				;
			}
			if (eSettingDtoForComp != null) {
				e.getComp().setActive(eSettingDtoForComp.getActive());
				e.getComp().setOnstop(eSettingDtoForComp.getOnstop());
				;
			}

		});
	}

	/* return json list of quotations issuers grouped by subdiv */
	private String getContactsBySubdivsAsJson(Collection<QuotationProjectionDTO> collection)
			throws JsonProcessingException {

		// get all issuers(contacts) from fetched quotations
		Set<ContactProjectionDTO> issuers = new HashSet<>();
		issuers.addAll(collection.stream().map(QuotationProjectionDTO::getSourcedBy).collect(Collectors.toSet()));

		// return keyvalue VOs (id,name) of issuers grouped by subdivs
		Map<KeyValue<Integer, String>, List<KeyValue<Integer, String>>> m = issuers.stream()
				.collect(Collectors.groupingBy(con -> {
					return new KeyValue<Integer, String>(con.getSubdiv().getSubdivid(), con.getSubdiv().getSubname());
				}, Collectors.mapping(c -> {
					return new KeyValue<Integer, String>(c.getPersonid(), c.getName());
				}, Collectors.toList())));

		// convert to json
		return new ObjectMapper().writeValueAsString(m);
	}

	/*
	 * return json list of quotationRequest and quotations issuers grouped by
	 * subdiv
	 */
	private String getContactsBySubdivsAsJsonRequest(Collection<QuotationRequestProjectionDTO> collection,
			Collection<QuotationProjectionDTO> collection2) throws JsonProcessingException {

		// get all issuers(contacts) from fetched quotationRequest/quotations
		Set<ContactProjectionDTO> issuers = new HashSet<>();

		issuers.addAll(collection.stream().map(QuotationRequestProjectionDTO::getLoggedBy).collect(Collectors.toSet()));
		issuers.addAll(collection2.stream().map(QuotationProjectionDTO::getSourcedBy).collect(Collectors.toSet()));

		// return key value VOs (id,name) of issuers grouped by subdiv
		Map<KeyValue<Integer, String>, List<KeyValue<Integer, String>>> m = issuers.stream()
				.collect(Collectors.groupingBy(con -> {
					return new KeyValue<Integer, String>(con.getSubdiv().getSubdivid(), con.getSubdiv().getSubname());
				}, Collectors.mapping(c -> {
					return new KeyValue<Integer, String>(c.getPersonid(), c.getName());
				}, Collectors.toList())));

		// convert to json
		return new ObjectMapper().writeValueAsString(m);
	}

}