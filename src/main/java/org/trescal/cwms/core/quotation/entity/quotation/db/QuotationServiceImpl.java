package org.trescal.cwms.core.quotation.entity.quotation.db;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.validation.BindException;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.vatrate.VatRate;
import org.trescal.cwms.core.company.entity.vatrate.db.VatRateService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelWithPriceDTO;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModelComparator;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.pricing.TotalDiscountCalculator;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.quotation.dto.*;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.QuotationMetricFormatter;
import org.trescal.cwms.core.quotation.entity.quotationitem.QuotationItemComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.comparators.QuotationItemSortType;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequestComparator;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.QuotationConditions;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.DefaultCalibrationCondition;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.db.DefaultCalibrationConditionService;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general.db.DefaultGeneralConditionService;
import org.trescal.cwms.core.quotation.entity.quotecaldefaults.QuoteCalDefaults;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeadingComparator;
import org.trescal.cwms.core.quotation.entity.quoteheading.comparators.QuotationHeadingSortType;
import org.trescal.cwms.core.quotation.entity.quoteheading.db.QuoteHeadingService;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.quotation.entity.quotestatusgroup.QuoteStatusGroup;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequest;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.db.ScheduledQuotationRequestService;
import org.trescal.cwms.core.quotation.form.ImportedQuotationItemsSynthesisForm.ImportedQuotationItemsSynthesisRowDTO;
import org.trescal.cwms.core.quotation.form.NewQuotationSearchForm;
import org.trescal.cwms.core.quotation.form.QuotationMetricForm;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.note.db.NoteService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.web.quotations.dto.WebQuoteFieldWrapper;
import org.trescal.cwms.web.quotations.form.genericentityvalidator.WebQuoteValidator;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

@Service("QuotationService")
@Slf4j
public class QuotationServiceImpl extends BaseServiceImpl<Quotation, Integer> implements QuotationService {
    @Value("${cwms.config.quotation.size.restrictdeletequote}")
    private long sizeRestrictDeleteQuote;

    @Autowired
    private CalibrationTypeService calibrationTypeService;
    @Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private ContactService contactService;
	@Autowired
	private DefaultCalibrationConditionService defCalConServ;
	@Autowired
	private DefaultGeneralConditionService defGenConServ;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private QuoteHeadingService qhServ;
	@Autowired
	private QuotationItemService qiServ;
	@Autowired
	private QuotationDao quoteDao;
	@Autowired
	private SystemComponentService scServ;
	@Autowired
	private StatusService statusService;
	@Autowired
	private WebQuoteValidator webQuoteValidator;
	@Autowired
	private StatusService statusServ;
    @Autowired
    private VatRateService vatRateService;
    @Autowired
    private ExchangeFormatService exchangeFormatService;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private ScheduledQuotationRequestService scheduledQuotationRequestService;
	@Autowired
	private TaxCalculator taxCalculator;
	@Autowired
	private CompanyService companyService;
    @Value("${cwms.config.avalara.aware}")
    private Boolean avalaraAware;
    @Autowired
    private NoteService noteService;

    @Override
    protected BaseDao<Quotation, Integer> getBaseDao() {
        return this.quoteDao;
    }

    @Override
    public ResultWrapper ajaxDeleteQuotation(int quoteId) {
		// boolean to indicate that all items are ok to delete
		boolean okToDelete = true;
		String allReasons = "";
		Quotation quote = null;

		long totalItemCount = qiServ.getItemCount(quoteId);
		// 2016-02-22 - Added check of number of quotation items until
		// performance issues fully resolved
		if (totalItemCount >= sizeRestrictDeleteQuote) {
			okToDelete = false;
			allReasons = "This quotation has " + totalItemCount
					+ " items and due to temporary performance restrictions, quotations with " + sizeRestrictDeleteQuote
					+ " items or more cannot be deleted.";
		}
		if (okToDelete) {
			// get quotation
			quote = this.get(quoteId);
			// quotation not null?
			if (quote == null) {
				okToDelete = false;
				allReasons = "Cannot find the quote you requested to delete";
			}
			// check all items
			if (okToDelete) {
				for (Quotationitem qi : quote.getQuotationitems()) {
					ResultWrapper rw = this.qiServ.checkQuotationItemIsSafeToDelete(qi);

					if (!rw.isSuccess()) {
						okToDelete = false;
						allReasons = allReasons.concat(rw.getMessage() + "<br/><br/>");
					}
					
				}
			}
		}
		// ok to delete quote?
		if (okToDelete) {
			// delete scheduled Quotation Request if exist
			List<ScheduledQuotationRequest> scheduledQuotationRequestList = this.scheduledQuotationRequestService
					.scheduledQuotationRequestFromQuotation(quote.getId());
			for (ScheduledQuotationRequest request :  scheduledQuotationRequestList) {
					request.setQuotation(null);
					this.scheduledQuotationRequestService.delete(request);
			}		
			// delete this quotation
			this.delete(quote);
			// return successful result
			return new ResultWrapper(true, "Quotation successfully deleted");
		} else {
			// return un-successful result
			return new ResultWrapper(false, allReasons);
		}
	}

	@Override
	public Quotation copyQuotation(Quotation quote) {
		Quotation newQuote = new Quotation();
		newQuote.setQno(quote.getQno());
		newQuote.setVer(quote.getVer());
		newQuote.setCreatedBy(quote.getCreatedBy());
		newQuote.setShowTotalQuoteValue(quote.isShowTotalQuoteValue());
		newQuote.setShowHeadingTotal(quote.isShowHeadingTotal());
		newQuote.setShowHeadingCaltypeTotal(quote.isShowHeadingCaltypeTotal());
		newQuote.setShowQuoteCaltypeTotal(quote.isShowQuoteCaltypeTotal());
		newQuote.setHeadingTotalVisible(quote.isHeadingTotalVisible());
		newQuote.setHeadingCaltypeTotalVisible(quote.isHeadingCaltypeTotalVisible());
		newQuote.setQuoteCaltypeTotalVisible(quote.isQuoteCaltypeTotalVisible());
		newQuote.setContact(quote.getContact());
		newQuote.setClientref(quote.getClientref());
		newQuote.setDuration(quote.getDuration());
		newQuote.setReqdate(quote.getReqdate());
		newQuote.setRegdate(quote.getRegdate());
		newQuote.setSourcedBy(quote.getSourcedBy());
		newQuote.setQuotestatus(quote.getQuotestatus());
		newQuote.setCurrency(quote.getCurrency());
		newQuote.setRate(quote.getRate());
		newQuote.setFinalCost(quote.getFinalCost());
		newQuote.setVatRate(quote.getVatRate());
		newQuote.setVatValue(quote.getVatValue());
		newQuote.setTotalCost(quote.getTotalCost());
		newQuote.setDefaultSetBy(quote.getDefaultSetBy());
		newQuote.setDefaultSetOn(quote.getDefaultSetOn());
		newQuote.setSortType(quote.getSortType());
		newQuote.setHeadingSortType(quote.getHeadingSortType());
		newQuote.setOrganisation(quote.getOrganisation());
		newQuote.setSourceAddress(quote.getSourceAddress());
		return newQuote;
	}

	@Override
	public Long countByCompanyAndAllocatedCompany(Company company, Company allocatedCompany) {
		return quoteDao.countByCompanyAndAllocatedCompany(company, allocatedCompany);
	}

	@Override
	public Long countActualByCompanyAndAllocatedCompany(Integer yearsBack, Company company, Company allocatedCompany) {
		return quoteDao.countActualByCompanyAndAllocatedCompany(yearsBack, company, allocatedCompany);
	}

	@Override
	public Long countByContactAndAllocatedSubdiv(Contact contact, Subdiv allocatedSubdiv) {
		return quoteDao.countByContactAndAllocatedSubdiv(contact, allocatedSubdiv);
	}

	@Override
	public Long countActualByContactAndAllocatedSubdiv(Integer yearsBack, Contact contact, Subdiv allocatedSubdiv) {
		return quoteDao.countActualByContactAndAllocatedSubdiv(yearsBack, contact, allocatedSubdiv);
	}

	@Override
	public Long countBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv) {
		return quoteDao.countBySubdivAndAllocatedSubdiv(subdiv, allocatedSubdiv);
	}

	@Override
	public Long countActualBySubdivAndAllocatedSubdiv(Integer yearsBack, Subdiv subdiv, Subdiv allocatedSubdiv) {
		return quoteDao.countActualBySubdivAndAllocatedSubdiv(yearsBack, subdiv, allocatedSubdiv);
	}

	@Override
	public Quotation createTemplateNewQuotation(Contact currentContact, Contact quoteContact, Subdiv allocatedSubdiv, Company allocatedCompany,
			Locale locale) {
		Quotation quotation = new Quotation();
		String qno = numerationService.generateNumber(NumerationType.QUOTATION, allocatedCompany, null);
		quotation.setQno(qno);
		quotation.setVer(1);
		quotation.setRegdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		quotation.setClientref("");
		quotation.setContact(quoteContact);
		quotation.setCreatedBy(currentContact);
		quotation.setDuration(allocatedCompany.getBusinessSettings().getDefaultQuotationDuration());
		quotation.setUsingDefaultTermsAndConditions(true);
		quotation.setDefaultterms(this.defGenConServ.getLatest(allocatedCompany));
		quotation.setTotalCost(new BigDecimal("0.00"));
		quotation.setVatValue(new BigDecimal("0.00"));
		quotation.setFinalCost(new BigDecimal("0.00"));
		quotation.setSortType(QuotationItemSortType.ID);
		quotation.setShowTotalQuoteValue(true);
		quotation.setShowHeadingTotal(false);
		quotation.setShowHeadingCaltypeTotal(false);
		quotation.setShowQuoteCaltypeTotal(false);
		quotation.setHeadingTotalVisible(false);
		quotation.setHeadingCaltypeTotalVisible(false);
		quotation.setQuoteCaltypeTotalVisible(false);
		quotation.setReqdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		quotation.setHeadingSortType(QuotationHeadingSortType.ID);
		quotation.setOrganisation(allocatedCompany);
		quotation.setSourceAddress(allocatedSubdiv.getDefaultAddress());

		// Get a QuoteStatus for the quotation
		QuoteStatus qs = this.statusService.findDefaultStatus(QuoteStatus.class);
		quotation.setQuotestatus(qs);

		// set the add quotation item defaults
		quotation.setDefaultAddModules(true);
		quotation.setDefaultQty(1);
		quotation.setDefaultSetBy(currentContact);
		quotation.setDefaultSetOn(new Date());
		quotation.setIssued(false);
		quotation.setQuotationitems(new HashSet<>());
		quotation.setCurrency(allocatedCompany.getCurrency());
		quotation.setRate(quotation.getCurrency().getDefaultRate());
		VatRate vatRate = this.vatRateService.getForAddress(quoteContact.getDefAddress(), allocatedCompany);
		if (vatRate != null)
			quotation.setVatRate(vatRate.getRate());

		// create default quote heading
		quotation.setQuoteheadings(new TreeSet<>(new QuoteHeadingComparator()));
		quotation.getQuoteheadings().add(qhServ.createDefaultHeading(quotation, locale));

		// for each calibration type get the latest note
		List<CalibrationType> calTypes = this.calibrationTypeService.getCalTypes();
		Set<QuoteCalDefaults> tempCalDefaults = new HashSet<>();

		for (CalibrationType calType : calTypes) {
            // get a refernce to latest condition for this calibration type
            DefaultCalibrationCondition defCal = this.defCalConServ.getLatest(calType.getCalTypeId(), allocatedCompany);
            // create a new object in the intersection table to reference this
            // quote
            QuoteCalDefaults quoteCalDefault = new QuoteCalDefaults();
            quoteCalDefault.setCondition(defCal);
            quoteCalDefault.setQuotation(quotation);
        }

        quotation.setDefaultcalterms(tempCalDefaults);
        quotation.setNotes(noteService.initalizeNotes(quotation, QuoteNote.class, locale));

        quotation.setQuotationitems(new TreeSet<>(new QuotationItemComparator()));
        CostCalculator.updatePricingTotalCost(quotation, quotation.getQuotationitems());
		taxCalculator.calculateAndSetVATaxAndFinalCost(quotation);

        return quotation;
    }

	@Override
	public QuoteNote createTemplateNewQuoteNote(Quotation quotation, boolean active, String label, String note,
			boolean publish, Contact contact) {
		QuoteNote quoteNote = new QuoteNote();
		quoteNote.setActive(active);
		quoteNote.setLabel(label);
		quoteNote.setNote(note);
		quoteNote.setPublish(publish);
		quoteNote.setQuotation(quotation);
		quoteNote.setSetBy(contact);
		quoteNote.setSetOn(new Date());

		return quoteNote;
	}

	@Override
	public void delete(Quotation q) {
		for (QuoteHeading qh : q.getQuoteheadings()) {
			qh.setQuotation(null);
			this.qhServ.delete(qh);
		}
		q.setQuoteheadings(null);
		this.quoteDao.remove(q);
	}

	@Override
	public int findNextVersion(String qno) {
		Integer version = this.quoteDao.findMaxQuotationVersion(qno);
		version = version + 1;
		return version;
	}

	@Override
	public Quotation get(Integer quoteid) {
		Quotation q = this.quoteDao.find(quoteid);
		// 2018-05-15 GB : Prevent directory lookup on repeated gets.
		if (q != null && q.getDirectory() == null) {
			q.setDirectory(this.compDirServ.getDirectory(this.scServ.findComponent(Component.QUOTATION), q.getQno(),
					q.getVer()));
		}
		return q;
	}

	@Override
	public QuotationConditions findQuotationConditions(Quotation q) {
		QuotationConditions qc = new QuotationConditions();
		// if the quotation is using default terms & conditions then load these
		if (q.isUsingDefaultTermsAndConditions()) {
            log.debug("usingDefaultTermsAndConditions = true");
			Set<DefaultCalibrationCondition> defCalConditions = new HashSet<>();

			Set<CalibrationType> calSet = this.getCaltypesPerQuotation(q.getId());

			// if quote is bound to particular versions fetch them
			Set<QuoteCalDefaults> conditionLinks = q.getDefaultcalterms();
			if (conditionLinks.isEmpty()) {
                log.debug("defaultcalterms.isEmpty() = true");
				// if not get the latest default conditions by type
				// get a list of calibration type default conditions for each
				// caltype for this quotation
				for (CalibrationType ct : calSet) {
					DefaultCalibrationCondition dcc = this.defCalConServ.getLatest(ct.getCalTypeId(),
							q.getOrganisation());
					if (dcc == null) {
                        log.debug("No default cal condition for calTypeId: " + ct.getCalTypeId() + " and coid: "
                                + q.getOrganisation().getCoid());
					} else {
                        log.debug("Adding default cal condition " + dcc.getDefCalConId());
                        defCalConditions.add(dcc);
					}
				}
			} else {
                log.debug("defaultcalterms.size() = " + conditionLinks.size());
				// retrieve and add the specifically bound default conditions if
				// the quotation is using this form of calibration type
				for (QuoteCalDefaults qcd : conditionLinks) {
					DefaultCalibrationCondition condition = qcd.getCondition();
					if (calSet.contains(condition.getCaltype()))
						defCalConditions.add(condition);
				}
			}
			qc.setCalCon(defCalConditions);
			// load the default calibration conditions
			qc.setGeneralCon(this.defGenConServ.getLatest(q.getOrganisation()));
		} else {
			// the quotation has custom conditions so just load these from the
			// quotation itself
            log.debug("usingDefaultTermsAndConditions = false");
            qc.setCalCon(q.getCalConditions());
			qc.setGeneralCon(q.getGeneralConditions());
		}

		return qc;
	}

	@Override
	public ResultWrapper findQuotationDWR(int quoteid) {
		// find eagerly loaded quotation
		Quotation quote = this.quoteDao.findEagerQuotation(quoteid);
		// check for null quotation
		if (quote != null) {
			return new ResultWrapper(true, "", quote, null);
		} else {
			return new ResultWrapper(false, "The quotation could not be found", null, null);
		}
	}

	@Override
	public List<Quotation> findQuotations(String qno) {
		return this.quoteDao.findQuotations(qno);
	}

	@Override
	public List<QuotationKeyValue> findLinkedQuotationsWithServiceModels(Job job) {
		return quoteDao.findLinkedQuotationsWithServiceModels(job);
	}

	@Override
	public List<InstrumentModelWithPriceDTO> findServiceModelsOnLinkedQuotations(Integer jobId) {
		return quoteDao.findServiceModelsOnLinkedQuotations(jobId);
	}

	@Override
	public List<InstrumentModelWithPriceDTO> findServiceModelsOnQuotation(Integer quoteId) {
		return quoteDao.findServiceModelsOnQuotation(quoteId);
	}
	
	@Override
	public boolean getAvalaraAware() {
		return avalaraAware;
	}
	
	// Intentionally exposed for integration testing of documents
	@Override
	public void setAvalaraAware(boolean avalaraAware) {
		this.avalaraAware = avalaraAware;
	}

	@Override
	public Set<CalibrationType> getCaltypesPerQuotation(int quoteid) {
		return this.quoteDao.getCaltypesPerQuotation(quoteid);
	}


	@Override
	public Set<InstrumentModel> getDistinctModels(Quotation quotation) {
		Set<InstrumentModel> models = new TreeSet<>(new InstrumentModelComparator());
		for (Quotationitem qi : quotation.getQuotationitems()) {
			if (qi.getInst() != null) {
				models.add(qi.getInst().getModel());
			} else {
				models.add(qi.getModel());
			}
		}
		return models;
	}

	@Override
	public LocalDate getExpiryDate(Quotation q) {
		if (q.getIssuedate() != null) {
			// update the expiry date
			return q.getIssuedate().plusDays(q.getDuration());
		} else {
			return null;
		}
	}

	@Override
	public List<Quotation> getIssuedQuotationsForContactFromIssuedate(int personId, LocalDate issuedate, boolean accepted) {
		return this.quoteDao.getIssuedQuotationsForContactFromIssuedate(personId, issuedate, accepted);
	}

	public Quotation getQuotationByExactQuoteNo(String quoteNo) {
		return this.quoteDao.getQuotationByExactQuoteNo(quoteNo);
	}

	@Override
	public List<Quotation> getQuotationsRequestForContactFromReqdate(int personId, LocalDate reqdate, boolean accepted
			, boolean issued) {
		return this.quoteDao.getQuotationsRequestForContactFromReqdate(personId, reqdate, accepted, issued);
	}

	@Override
	public QuotationSummaryDTO getQuotationSummary(Quotation quotation) {
		QuotationSummaryDTO result = new QuotationSummaryDTO(quotation.getId());
		// If required, we could selectively build based on whether document or
		// internal use
		result.setSummaryByServiceType(this.qiServ.findSummaryDTOs(quotation.getId(), false, true));
		result.setSummaryByHeading(this.qiServ.findSummaryDTOs(quotation.getId(), true, false));
		result.setSummaryByHeadingAndServiceType(this.qiServ.findSummaryDTOs(quotation.getId(), true, true));
		return result;
	}

	/*
	 * Removing due to poor performance on large quotes
	 */
	@Override
	@Deprecated
	public TotalDiscountCalculator getQuotationTotalDiscount(Set<Quotationitem> qitems) {
		return new TotalDiscountCalculator(qitems);
	}

	@Override
	public void save(Quotation q) {
		this.quoteDao.persist(q);
		// create a new quotation directory
		this.compDirServ.getDirectory(this.scServ.findComponent(Component.QUOTATION), q.getQno(), q.getVer());
	}

	@Override
	public Quotation insertQuotationFromRequest(QuotationRequest quotationRequest, int personid,
			boolean addRequestInfoAsNote, Contact currentContact, Subdiv allocatedSubdiv) {
		Contact quoteContact = this.contactService.get(personid);
		Quotation quotation = this.createTemplateNewQuotation(currentContact, quoteContact, allocatedSubdiv, allocatedSubdiv.getComp(),
			currentContact.getLocale());
		quotation.setSourcedBy(currentContact);
		quotation.setClientref(quotationRequest.getClientref());
		quotation.setReqdate(quotationRequest.getReqdate());
		quotation.setDuedate(quotationRequest.getDuedate());
		// make sure the quotation request is associated with this quotation
		quotation.setQuotationRequests(new TreeSet<>(new QuotationRequestComparator()));
		quotation.getQuotationRequests().add(quotationRequest);

		if (addRequestInfoAsNote) {
			QuoteNote qn = new QuoteNote();
			qn.setActive(true);
			qn.setPublish(false);
			qn.setNote(quotationRequest.getRequestInfo());
			qn.setLabel("Quotation Request Info");
			qn.setSetBy(quotationRequest.getLoggedBy());
			qn.setSetOn(quotationRequest.getLoggedOn());
			qn.setQuotation(quotation);
			quotation.getNotes().add(qn);
		}

		// persist the quotation
		this.save(quotation);

		return quotation;
	}

	@Override
	public boolean isAllItemsOnQuotationUnique(Quotation q) {
		// create new set for unique quote item strings
		Set<String> uniqueQIs = new TreeSet<>();
		// counter for items in quote
		int countItems = 0;
		// create unique string for each item in each heading
		for (QuoteHeading qh : q.getQuoteheadings()) {
			for (Quotationitem qi : qh.getQuoteitems()) {
				String itemno = String.valueOf(qi.getItemno());
				String quoteid = String.valueOf(q.getId());
				String caltypeid = String.valueOf(qi.getServiceType().getCalibrationType().getCalTypeId());
				String headingid = String.valueOf(qi.getHeading().getHeadingId());
				String uniqueQI = itemno.concat("-").concat(quoteid).concat("-").concat(caltypeid).concat("-")
					.concat(headingid);
				// add unique string to set
				uniqueQIs.add(uniqueQI);
				// increment quote item count
				countItems++;
			}
		}
		// both counts match? - if two strings the same in set then they will be
		// merged
		return countItems == uniqueQIs.size();
	}


	@Override
	public QuotationMetricFormatter loadMetrics(QuotationMetricForm metricForm) {
		QuotationMetricFormatter m = new QuotationMetricFormatter();

		// remove any null values (caused when the 'any' option is selected
		if (metricForm.getCreatedByIds() != null) {
			metricForm.getCreatedByIds().remove(null);
		}
		if (metricForm.getSourcedByIds() != null) {
			metricForm.getSourcedByIds().remove(null);
		}
		if (metricForm.getIssuedByIds() != null) {
			metricForm.getIssuedByIds().remove(null);
		}

		m = this.quoteDao.loadMetrics(metricForm, m);

		// the quotecount, issued and accepted fields have now been populated
		m.setNotIssued(m.getQuotationCount() - m.getIssued());
		m.setPercentIssued(NumberTools.getPct(m.getIssued(), m.getQuotationCount(), 0.0));
		m.setPercentNotIssued(NumberTools.getPct(m.getNotIssued(), m.getQuotationCount(), 0.0));

		m.setNotAccepted(m.getIssued() - m.getAccepted());
		m.setPercentAccepted(NumberTools.getPct(m.getAccepted(), m.getIssued(), 0.0));
		m.setPercentNotAccepted(NumberTools.getPct(m.getNotAccepted(), m.getIssued(), 0.0));

		return m;
	}

	@Override
	public List<QuotationSearchResultWrapper> searchCompanyQuotations(Integer coid, Boolean showExpired,
			Boolean showUnIssued, Integer years, Integer jobid) {
		return this.quoteDao.searchCompanyQuotations(coid, showExpired != null ? showExpired : false,
				showUnIssued != null ? showUnIssued : false, years, jobid);
	}


	@Override
	public ResultWrapper updateQuotationCaltypeTotalVisibility(boolean show, int quoteid) {
		Quotation quote = this.get(quoteid);
		if (quote == null) {
			return new ResultWrapper(false, "No quotation could be found for the given id", null, null);
		} else {
			quote.setShowQuoteCaltypeTotal(show);
			this.merge(quote);
			return new ResultWrapper(true, "", quote, null);
		}
	}

	@Override
	public Quotation updateQuotationCosts(Quotation quotation) {
		CostCalculator.updatePricingTotalCost(quotation, quotation.getQuotationitems());
		taxCalculator.calculateAndSetVATaxAndFinalCost(quotation);
		this.merge(quotation);
		return quotation;
	}

	@Override
	public ResultWrapper updateQuotationHeadingCaltypeTotalVisibility(boolean show, int quoteid) {
		Quotation quote = this.get(quoteid);
		if (quote == null) {
			return new ResultWrapper(false, "No quotation could be found for the given id", null, null);
		} else {
			quote.setShowHeadingCaltypeTotal(show);
			this.merge(quote);
			return new ResultWrapper(true, "", quote, null);
		}
	}

	@Override
	public ResultWrapper updateQuotationHeadingTotalVisibility(boolean show, int quoteid) {
		Quotation quote = this.get(quoteid);
		if (quote == null) {
			return new ResultWrapper(false, "No quotation could be found for the given id", null, null);
		} else {
			quote.setShowHeadingTotal(show);
			this.merge(quote);
			return new ResultWrapper(true, "", quote, null);
		}
	}

	@Override
	public ResultWrapper updateQuotationTotalVisibility(boolean show, int quoteid) {
		Quotation quote = this.get(quoteid);
		if (quote == null) {
			return new ResultWrapper(false, "No quotation could be found for the given id", null, null);
		} else {
			quote.setShowTotalQuoteValue(show);
			this.merge(quote);
			return new ResultWrapper(true, "", quote, null);
		}
	}

	@Override
	public ResultWrapper webValidateQuoteRequest(String poNumber) {
		WebQuoteFieldWrapper quoteFields = new WebQuoteFieldWrapper(poNumber);

		// validate quoteFields for data type
		BindException errors = new BindException(quoteFields, "quote");
		this.webQuoteValidator.validate(quoteFields, errors);

		return new ResultWrapper(errors, quoteFields);
	}

	@Override
	public List<Quotation> getQuotationListByJobitemId(Integer jobitemid) {
		return this.quoteDao.getQuotationListByJobitemId(jobitemid);
	}

	@Override
	public List<Quotation> getIssuedAndAcceptedQuotationListByJobitemId(Integer jobitemid) {
		ArrayList<QuoteStatus> quoteStatus = new ArrayList<>();
		quoteStatus.add((QuoteStatus) this.statusServ.findIssuedStatus(QuoteStatus.class));
		quoteStatus.add((QuoteStatus) this.statusServ.findAcceptedStatus(QuoteStatus.class));
		return this.quoteDao.getIssuedAndAcceptedQuotationListByJobitemId(jobitemid, quoteStatus);
	}

	@Override
	public List<ImportedQuotationItemsSynthesisRowDTO> convertFileContentToQuotationItemsDTO(Integer exchangeFormatId,
																							 List<LinkedCaseInsensitiveMap<String>> fileContent) {

		ExchangeFormat exchangeFormat = exchangeFormatService.get(exchangeFormatId);
		List<ServiceType> serviceTypes = serviceTypeService.getAllCalServiceTypes();

		List<ImportedQuotationItemsSynthesisRowDTO> myList = new ArrayList<>();
		for (LinkedCaseInsensitiveMap<String> row : fileContent) {
			ImportedQuotationItemsSynthesisRowDTO dto = new ImportedQuotationItemsSynthesisRowDTO();
			for (String key : row.keySet()) {
				ExchangeFormatFieldNameEnum efFieldName = exchangeFormat.getExchangeFormatFieldNameDetails().stream()
					.filter(e -> (e.getTemplateName() != null
						&& e.getTemplateName().trim().equalsIgnoreCase(key.trim()))
						|| (e.getFieldName().getValue().trim().equalsIgnoreCase(key.trim())))
					.map(ExchangeFormatFieldNameDetails::getFieldName).findFirst().orElse(null);
				if (efFieldName != null && StringUtils.isNoneBlank(row.get(key)))
					switch (efFieldName) {

					case ID_TRESCAL:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setPlantId(Integer.valueOf(row.get(key)));
						break;

					case PLANT_NO:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setPlantNo(row.get(key));
						break;

					case CATALOG_PRICE:
						dto.setCatalogPrice(Double.valueOf(row.get(key)));
						break;

					case DISCOUNT_RATE:
						dto.setDiscountRate(Double.valueOf(row.get(key)));
						break;

					case FINAL_PRICE:
						dto.setFinalPrice(Double.valueOf(row.get(key)));
						break;

					case PUBLIC_NOTE:
						dto.setPublicNote(row.get(key));
						break;

					case PRIVATE_NOTE:
						dto.setPrivateNote(row.get(key));
						break;

					case EXPECTED_SERVICE:
						dto.setServiceType(row.get(key));
						for (ServiceType st : serviceTypes) {
							boolean shortNamematches = st.getShortnameTranslation().stream().anyMatch(s -> s.getTranslation().toLowerCase().trim().equals(row.get(key).toLowerCase()));
							if (shortNamematches)
								dto.setServiceTypeId(st.getServiceTypeId());
							else {
								boolean longNamematches = st.getLongnameTranslation().stream().anyMatch(s -> s.getTranslation().toLowerCase().trim().equals(row.get(key).toLowerCase()));
								if (longNamematches)
									dto.setServiceTypeId(st.getServiceTypeId());
							}
						}
						break;
					default:
						break;
					}
			}
			myList.add(dto);
		}
		return myList;
	}

	@Override
	public List<QuotationProjectionDTO> getAllQuotesInStatusGroupNew(QuoteStatusGroup group, Integer allocatedSubdivId, Locale locale) {
		return this.quoteDao.getAllQuotesInStatusGroupNew(group, allocatedSubdivId, locale);
	}

	@Override
	public List<QuotationProjectionDTO> getMostRecentIssuedQuotationsNew(int resultSize, Integer allocatedSubdivId, Locale locale) {
		return this.quoteDao.getMostRecentIssuedQuotationsNew(resultSize, allocatedSubdivId, locale);
	}

	@Override
	public PagedResultSet<QuotationProjectionDTO> queryQuotationNew(NewQuotationSearchForm qsf,
			PagedResultSet<QuotationProjectionDTO> rs, Locale locale) {
		return this.quoteDao.queryQuotationNew(qsf, rs, locale);
	}

	@Override
	public void getPaginationAllQuotesInStatusGroupNew(QuoteStatusGroup webrequests, Integer allocatedSubdivId,
			PagedResultSet<QuotationProjectionDTO> webQuotesRequiringAction, NewQuotationSearchForm qsf, Locale locale) {
		this.quoteDao.getPaginationAllQuotesInStatusGroupNew(webrequests, allocatedSubdivId, webQuotesRequiringAction, qsf, locale);
	}
	
	public List<Integer> getPlantIdsFromQuotation(Integer quoteId) {
		Quotation quotation = this.get(quoteId);
		// create list for quotation instruments
		List<Integer> qinstIds = new ArrayList<>();
		// add all existing quote instruments to list on form
		for (Quotationitem qi : quotation.getQuotationitems()) {
			// instrument?
			if (qi.getInst() != null) {
				// add to list
				qinstIds.add(qi.getInst().getPlantid());
			}
		}
		return qinstIds;
	}
		
	public MultiValuedMap<Integer, Integer> quotationItemsFromInstrument(Integer quoteId) {
		Quotation quotation = this.get(quoteId);
		MultiValuedMap<Integer, Integer> qitemsaddedByInst = new HashSetValuedHashMap<>();
		quotation.getQuotationitems().forEach(qi -> {
			if (qi.getInst() != null) {
				qitemsaddedByInst.put(qi.getInst().getPlantid(), qi.getServiceType().getServiceTypeId());
			}
		});
		return qitemsaddedByInst;
	}
	
	@Override
	public List<QuoteItemOutputDto> findQuotationItemsFromModel(Integer modelId, Integer servicetypeid, Integer businessCompany){
		Company allocatedCompany = this.companyService.get(businessCompany);
		return this.quoteDao.findQuotationItemsFromModel(modelId, servicetypeid, allocatedCompany);
	}
	
}