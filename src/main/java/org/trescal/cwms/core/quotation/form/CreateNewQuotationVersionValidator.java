package org.trescal.cwms.core.quotation.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class CreateNewQuotationVersionValidator extends AbstractBeanValidator {
	@Autowired
	private ContactService contactService;

	@Override
	public boolean supports(Class<?> clazz) {
		return CreateNewQuotationVersionForm.class.isAssignableFrom(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		// Continue only if basic validation passed
		if (!errors.hasErrors()) {
			CreateNewQuotationVersionForm form = (CreateNewQuotationVersionForm) target;
			Contact clientContact = this.contactService.get(form.getPersonId());
			Contact businessContact = this.contactService.get(form.getSourcedById());
			
			// Client contact must be active
			if (!clientContact.isActive()) {
				errors.rejectValue("personId", null, "The contact is inactive");
			}
			
			// Business contact must be active, and belong to the same business company (catches migrated data, etc...)
			if (!businessContact.isActive()) {
				errors.rejectValue("sourcedById", null, "The contact is inactive");
			}
			if (businessContact.getSub().getComp().getCoid() != form.getBusinessCompanyId()) {
				errors.rejectValue("sourcedById", null, "The contact belongs to another company");
			}
			
		}
	}
}
