package org.trescal.cwms.core.quotation.entity.quotationdocdefaulttext.db;

import java.util.Locale;
import java.util.Optional;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.quotation.entity.quotationdocdefaulttext.QuotationDocDefaultText;
import org.trescal.cwms.core.quotation.entity.quotationdocdefaulttext.QuotationDocDefaultText_;

@Repository
public class QuotationDocDefaultTextDaoImpl extends BaseDaoImpl<QuotationDocDefaultText, Integer>
		implements QuotationDocDefaultTextDao {

	@Override
	protected Class<QuotationDocDefaultText> getEntity() {
		return QuotationDocDefaultText.class;
	}
	
	@Override
	public QuotationDocDefaultText findDefaultText(Locale locale) {
		Optional<QuotationDocDefaultText> result = getFirstResult(cb -> {
			CriteriaQuery<QuotationDocDefaultText> cq = cb.createQuery(QuotationDocDefaultText.class);
			Root<QuotationDocDefaultText> root = cq.from(QuotationDocDefaultText.class);
			cq.where(cb.equal(root.get(QuotationDocDefaultText_.locale), locale));
			return cq;
		});
		return result.orElse(null);
	}

}
