package org.trescal.cwms.core.quotation.enums;

public enum ScheduledQuoteRequestScope
{
	COMPANY, SUBDIV, CONTACT;
}
