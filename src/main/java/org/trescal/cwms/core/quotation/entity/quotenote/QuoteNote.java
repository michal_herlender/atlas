package org.trescal.cwms.core.quotation.entity.quotenote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.note.ParentEntity;

@Entity
@Table(name = "quotenote")
public class QuoteNote extends Note implements ParentEntity<Quotation> {
    private Quotation quotation;

    @Override
    @Transient
    public NoteType getNoteType() {
        return NoteType.QUOTENOTE;
    }

    @ManyToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name = "quoteid", unique = false, nullable = false, insertable = true, updatable = true)
    public Quotation getQuotation() {
        return quotation;
    }

    public void setQuotation(Quotation quotation) {
        this.quotation = quotation;
    }

    @Override
    public void setEntity(Quotation quotation) {
        setQuotation(quotation);
    }

    @Override
    @Transient
    public Quotation getEntity() {
        return getQuotation();
    }
}