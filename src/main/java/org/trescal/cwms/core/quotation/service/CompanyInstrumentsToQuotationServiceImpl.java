package org.trescal.cwms.core.quotation.service;

import static org.trescal.cwms.core.tools.InstModelTools.instrumentModelNameWithTypology;
import static org.trescal.cwms.core.tools.TranslationUtils.getBestTranslation;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.db.ContractReviewCalibrationCostService;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.dto.ContractReviewCalibrationCostDTO;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.pricing.catalogprice.dto.CatalogPriceDTO;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.dto.CostJobItemDto;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.invoice.entity.invoiceitem.db.InvoiceItemService;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.db.JobCostingCalibrationCostService;
import org.trescal.cwms.core.pricing.lookup.PriceLookupRequirements;
import org.trescal.cwms.core.pricing.lookup.PriceLookupService;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupInput;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupInputFactory;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutput;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutputInvoiceItem;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.quotation.dto.PreliminaryCompInstToQuoteItemWrapper;
import org.trescal.cwms.core.quotation.dto.QuoteItemOutputDto;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.QuotationItemComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.form.AddQuotationForm;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import lombok.val;

@Service("CompanyInstrumentsToQuotationService")
public class CompanyInstrumentsToQuotationServiceImpl implements CompanyInstrumentsToQuotationService {
    @Autowired
    private CalibrationTypeService calTypeServ;
    @Autowired
    private ConvertToQuotationUtils convertToQuotationUtils;
    @Autowired
    private InstrumService instServ;
    @Autowired
    private JobItemService jobItemServ;
    @Autowired
    private QuotationItemService quotationItemServ;
    @Autowired
    private QuotationService quotationServ;
    @Autowired
    private CatalogPriceService priceServ;
    @Autowired
    private SubdivService subdivServ;
    @Autowired
    private SupportedLocaleService supportedLocaleService;
    @Autowired
    private InvoiceItemService invoiceItemService;
    @Autowired
    private JobCostingCalibrationCostService jobCostingCalibrationCostService;
    @Autowired
    private ContractReviewCalibrationCostService calCostServ;
    @Autowired
    private CatalogPriceService catalogPriceService;
    @Autowired
    private PriceLookupService priceLookupService;
    @Autowired
    private InstrumentModelService instrumentModelService;
    @Autowired
    private TaxCalculator taxCalculator;
    @Value("${cwms.config.quotation.resultsyearfilter}")
    private Integer quotationResultsYearFilter;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Quotationitem getNewQuotationItemForInstrument(Instrument instrument, Quotation quotation,
                                                          QuoteHeading heading, Subdiv subdiv, JobItem ji) {
        // get the most recent jobitem for this
        CalibrationType caltype = null;
        // look for the most recent jobitem for this instrument (there may or
        // may not be a jobitem - instruments can be recalled without having
        // ever appeared on a job)
        if (ji == null) {
            // no previous jobitem, chose the default cal type
            caltype = this.calTypeServ.getDefaultCalType();
        } else {
            // previous jobitem, use this cal type
            caltype = ji.getCalType();
        }
        // create new quote item from template
        Quotationitem qItem = this.quotationItemServ.createTemplateNewQuotationItem(quotation, heading, instrument,
                null, caltype.getCalTypeId(), subdiv.getSubdivid());
        // work out the cost of the item based on it's most recent cal cost (or
        // model defaults if no previous costing)
        this.prepareCalibrationCost(instrument, ji, qItem.getCalibrationCost(), subdiv);
        // update item costs
        CostCalculator.updateItemCostWithRunningTotal(qItem);
        return qItem;
    }

    @Override
    public Quotation prepareAndSaveCompanyInstrumentsToQuotation(AddQuotationForm aqf, Quotation quotation,
                                                                 Subdiv subdiv, Contact currentContact) {
        // create instrument list from the plantids
        List<Instrument> quoteCompInsts = this.instServ.getInstruments(aqf.getQcif().getPlantids());
        // get all calibration types currently active
        List<CalibrationType> caltypes = this.calTypeServ.getActiveCalTypes();
        // convert list of recall instruments to quotation items
        quotation.setQuotationitems(this.getNewQuotationItemsForInstruments(quoteCompInsts, caltypes, quotation, subdiv,
                aqf.getQcif().getPlantids()));
        this.quotationItemServ.createNotesForNewQuotation(quotation, subdiv, currentContact);
        CostCalculator.updatePricingTotalCost(quotation, quotation.getQuotationitems());
        taxCalculator.calculateAndSetVATaxAndFinalCost(quotation);
        return quotation;
    }

    @Override
    public void prepareCalibrationCost(Instrument inst, JobItem ji, QuotationCalibrationCost qCalCost, Subdiv subDiv) {
        if (ji != null) {
            this.convertToQuotationUtils.prepareCalibrationCostFromJobItem(ji, qCalCost);
        } else {
            // prepare from instrument
            this.convertToQuotationUtils.prepareCalibrationCostFromModel(inst, qCalCost, subDiv);
        }
    }

    @Override
    public Set<Quotationitem> getNewQuotationItemsForInstruments(List<Instrument> toQuoteInsts,
                                                                 List<CalibrationType> caltypes, Quotation quotation, Subdiv subdiv, List<Integer> plantIds) {
        // New quotation, so should just have one default heading.
        QuoteHeading heading = quotation.getQuoteheadings().iterator().next();
        TreeSet<Quotationitem> qItems = new TreeSet<Quotationitem>(new QuotationItemComparator());
        // add all items for each calibration type
        // initialize item count
        int itemcount = quotationItemServ.getMaxItemno(quotation.getId()) + 1;
        Map<Instrument, JobItem> mostRecentJobItems = jobItemServ.findMostRecentJobItem(plantIds);
        for (Instrument inst : toQuoteInsts) {
            // start creating the quotation item
            Quotationitem qItem = this.getNewQuotationItemForInstrument(inst, quotation, heading, subdiv,
                    mostRecentJobItems.get(inst));
            // caltypes match
            if (caltypes.stream().anyMatch(cal -> cal.getCalTypeId() == qItem.getCaltype().getCalTypeId())) {
                qItem.setItemno(itemcount);
                itemcount++;
                qItems.add(qItem);
            }
        }

        this.logger.info("Added " + qItems.size() + " quotation items from instrument");

        return qItems;
    }

    @Override
    public ResultWrapper sourceCompanyInstrumentToQIValues(int[] plantIds, int quoteId, int subDivId) {
        Locale locale = LocaleContextHolder.getLocale();
        // create list for instruments to be sourced
        List<Instrument> instsToSource = new ArrayList<Instrument>();
        // get all instruments with plantIds passed
        for (int plid : plantIds) {
            // find the instrument
            Instrument i = this.instServ.get(plid);
            // null?
            if (i != null) {
                // add to list
                instsToSource.add(i);
            }
        }
        // all instruments loaded
        if (plantIds.length == instsToSource.size()) {
            // find the quotation
            Quotation quote = this.quotationServ.get(quoteId);
            // quotation null?
            if (quote != null) {
                // create list of wrappers
                List<PreliminaryCompInstToQuoteItemWrapper> prelWraps = new ArrayList<PreliminaryCompInstToQuoteItemWrapper>();
                // add all instruments in list
                for (Instrument inst : instsToSource) {
                    // create new wrapper to hold return values
                    PreliminaryCompInstToQuoteItemWrapper wrap = new PreliminaryCompInstToQuoteItemWrapper();
                    wrap = this.getDefaultPrices(inst, null, null, quote, subDivId, locale);
                    prelWraps.add(wrap);
                }
                // return success wrapper
                return new ResultWrapper(true, "", prelWraps, null);
            } else {
                // return error message
                return new ResultWrapper(false, "Quotation could not be found", null, null);
            }
        } else {
            // return error message
            return new ResultWrapper(false, "One or more of the instruments could not be found", null, null);
        }
    }

    @Override
    public BigDecimal getCatalogPriceForInstrument(Integer plantId, Integer calTypeId, Integer subDivId) {
        Integer modelId = instServ.get(plantId).getModel().getModelid();
        return priceServ.ajaxFind(modelId, calTypeId, subDivId).getValue();
    }

    @Override
    public PreliminaryCompInstToQuoteItemWrapper getDefaultPrices(Instrument inst, InstrumentModel model,
                                                                  CalibrationType calibrationType, Quotation quote, Integer subdivid, Locale locale) {
        Subdiv subdiv = this.subdivServ.get(subdivid);
        // create new wrapper to hold return values
        PreliminaryCompInstToQuoteItemWrapper wrap = new PreliminaryCompInstToQuoteItemWrapper();
        // create calibration type variable
        CalibrationType caltype = calibrationType;
        // Case 1 : add Instrument to Quotation
        if (inst != null) {
            model = inst.getModel();
            // look for the most recent jobitem for this instrument
            JobItem ji = this.jobItemServ.findMostRecentJobItem(inst.getPlantid());
            if (ji != null) {
                val clientCompanyId = ji.getJob().getCon().getSub().getComp().getCoid();
                val businessCompanyId = ji.getJob().getOrganisation().getComp().getCoid();
                if (caltype == null) {
                    caltype = ji.getServiceType().getCalibrationType();
                }
                val jobCostings = this.jobCostingCalibrationCostService.findMatchingCosts(
                                ji.getInst().getPlantid(), clientCompanyId, ji.getInst().getModel().getModelid(),
                                caltype.getCalTypeId(), 1, 10)
                        .stream().map(CostJobItemDto::fromJobCostingCalibrationCost).collect(Collectors.toList());
                ContractReviewCalibrationCostDTO contractReviewCost = this.calCostServ
                        .findContractReviewCalibrationCost(ji.getCalibrationCost().getCostid(), caltype.getServiceType().getServiceTypeId(), locale);
                // get catalog prices from the instrument
                val prices = getPriceLookupOutputs(businessCompanyId, clientCompanyId,
                        caltype.getServiceType().getServiceTypeId(),
                        ji.getInst().getPlantid(), null, getQuotationIds(ji));
				val partitions = splitPrices(prices);
                val catalogPrices = collectPriceCatalog(prices);
                wrap.setJobCostings(jobCostings);
                wrap.setQuotationItemOuputs(partitions.get(false));
                wrap.setContractReviewCost(contractReviewCost);
                wrap.setCatalogPrices(catalogPrices);
            } else {
                if (caltype == null) {
                    if (inst.getDefaultServiceType() != null
                            && inst.getDefaultServiceType().getCalibrationType() != null) {
                        caltype = inst.getDefaultServiceType().getCalibrationType();
                    } else {
                        caltype = this.calTypeServ.getDefaultCalType(JobType.STANDARD, subdiv);
                    }
                }
                // get catalog prices from both the instrument and the model
                val prices = getPriceLookupOutputs(quote.getOrganisation().getCoid(), quote.getContact().getSub().getComp().getCoid(),
                        caltype.getServiceType().getServiceTypeId(), inst.getPlantid(),
                        model.getModelid(), Collections.emptyList());
                logger.info("Retrieved "+prices.size()+" prices");
                val catalogPrices = collectPriceCatalog(prices);
                wrap.setCatalogPrices(catalogPrices);
                wrap.setQuotationItemOuputs(getQuoteItemOutputDtos(prices));
                logger.info("Retrieved "+wrap.getQuotationItemOuputs().size()+" quotation items");
                if (wrap.getCatalogPrices().isEmpty()) {
                    this.findSingleCatalogPrice(model, caltype.getServiceType(), subdiv.getComp(), wrap);
                }
            }
            List<PriceLookupOutputInvoiceItem> invoiceItemOuputs = this.invoiceItemService.findInvoiceItems(inst.getPlantid(),
                    caltype.getServiceType().getServiceTypeId(), quote.getContact().getSub().getComp().getCoid());
            wrap.setInvoiceItemOuputs(invoiceItemOuputs);
            // add instrument to wrapper
            wrap.setPlantid(inst.getPlantid());
            wrap.setPlantno(inst.getPlantno());
            wrap.setSerialno(inst.getSerialno());
            // Case 2 : add Instrument Model To Quotation
        } else if (model != null) {
            wrap.setInstrumentModelCase(true);
            Integer businessCompany = quote.getOrganisation().getCoid();
            List<QuoteItemOutputDto> quoteItems = this.quotationServ.findQuotationItemsFromModel(model.getModelid(),
                    caltype.getServiceType().getServiceTypeId(), businessCompany);
            wrap.setQuotationItemOuputs(quoteItems);
            val prices = getPriceLookupOutputs(businessCompany, quote.getContact().getSub().getComp().getCoid(),
                    caltype.getServiceType().getServiceTypeId(), null,
                    model.getModelid(), Collections.emptyList());
            val catalogPrices = collectPriceCatalog(prices);
            wrap.setCatalogPrices(catalogPrices);
            if (wrap.getCatalogPrices().isEmpty()) {
                this.findSingleCatalogPrice(model, calibrationType.getServiceType(), subdiv.getComp(), wrap);
            }
        }
        // add full instrument model
        wrap.setFullInstrumentModelName(InstModelTools.modelNameViaTranslations(model,
                LocaleContextHolder.getLocale(), supportedLocaleService.getPrimaryLocale()));
        // add currency to wrapper
        wrap.setCurrency(quote.getCurrency().getCurrencyERSymbol());
        // add calibration type to wrapper
        wrap.setCalTypeId(caltype.getCalTypeId());
        wrap.setCalTypeName(caltype.getServiceType().getShortName());
        // add comments to wrapper
        wrap.setComments("");
        // set default price and source
        if (!wrap.getInvoiceItemOuputs().isEmpty()) {
            wrap.setSource(CostSource.INVOICE);
            wrap.setFinalCost(wrap.getInvoiceItemOuputs().get(0).getFinalPriceForInvoiceItem().toString());
        } else if (!wrap.getJobCostings().isEmpty()) {
            wrap.setSource(CostSource.JOB_COSTING);
            wrap.setFinalCost(wrap.getJobCostings().get(0).getFinalCost().toString());
        } else if (wrap.getContractReviewCost() != null) {
            wrap.setSource(CostSource.CONTRACT_REVIEW);
            wrap.setFinalCost(wrap.getContractReviewCost().getFinalCost().toString());
        } else if (!wrap.getQuotationItemOuputs().isEmpty()) {
            wrap.setSource(CostSource.QUOTATION);
            wrap.setFinalCost(wrap.getQuotationItemOuputs().get(0).getFinalPrice().toString());
        } else if (!wrap.getCatalogPrices().isEmpty()) {
            wrap.setSource(CostSource.MODEL);
            Optional<CatalogPriceDTO> standalonePrice = wrap.getCatalogPrices().stream().filter(dto -> !dto.getIsSalesCategory()).findAny();
            //the price of the stand-alone instrument model has the highest priority
            if (standalonePrice.isPresent()) {
                wrap.setFinalCost(standalonePrice.get().getPrice().toString());
                wrap.setComments(standalonePrice.get().getComments());
            } else {
                wrap.setFinalCost(wrap.getCatalogPrices().get(0).getPrice().toString());
                wrap.setComments(wrap.getCatalogPrices().get(0).getComments());
            }
        } else {
            wrap.setSource(CostSource.MANUAL);
            wrap.setFinalCost(new BigDecimal(0).toString());
        }
        // Remove self-reference
        wrap.getQuotationItemOuputs().removeIf(quoteItemOutputDto -> quoteItemOutputDto.getQuoteId().equals(quote.getId()));
        return wrap;
    }

    private SortedSet<PriceLookupOutput> getPriceLookupOutputs(Integer businessCompanyId, Integer clientCompanyId,
                                                               Integer serviceTypeId, Integer instrumentId, Integer modelId, List<Integer> includeQuotationIds) {

        PriceLookupRequirements reqs = new PriceLookupRequirements(businessCompanyId, clientCompanyId, true);
        reqs.setFirstMatchSearch(false);
        reqs.setIncludeQuotationIds(includeQuotationIds);
        reqs.setQuotationAccepted(null);
        reqs.setQuotationIssued(null);
        reqs.setQuotationExpiryDateAfter(null);
        reqs.setQuotationRegDateAfter(getQuotationRegDateAfter());

        PriceLookupInputFactory factory = new PriceLookupInputFactory(reqs);
        PriceLookupInput input = new PriceLookupInput();
        if (instrumentId != null) {
            input = factory.addInstrument(serviceTypeId, instrumentId);
        }
        else if (modelId != null) {
            input = factory.addInstrumentModel(serviceTypeId, modelId);
        }

        reqs.addInput(input);
        this.priceLookupService.findPrices(reqs);
        return input.getOutputs();
    }

    private LocalDate getQuotationRegDateAfter() {
        return LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())
                .minusYears(quotationResultsYearFilter);
    }

    private List<Integer> getQuotationIds(JobItem jobItem) {
        val streamFromContract = jobItem.getContract() != null && jobItem.getContract().getQuotation() != null ?
                Stream.of(jobItem.getContract().getQuotation().getId()) : Stream.<Integer>empty();
        val streamIds = jobItem.getJob().getLinkedQuotes().stream().map(JobQuoteLink::getQuotation)
                .map(Quotation::getId);
        val stream = Stream.of(streamIds, streamFromContract).flatMap(Function.identity());
        return stream.collect(Collectors.toList());
    }

    private List<CatalogPriceDTO> collectPriceCatalog(Set<PriceLookupOutput> pricees) {
        val locale = LocaleContextHolder.getLocale();
        List<CatalogPriceDTO> dtos = new ArrayList<>();
        pricees.stream().filter(p -> p.getCatalogPriceId() != null).forEach(p -> {
            CatalogPrice catalog = this.catalogPriceService.get(p.getCatalogPriceId());
            CatalogPriceDTO dto = new CatalogPriceDTO();
            dto.setSource(p.getQueryType().getMessage());
            dto.setModelTypeName(getBestTranslation(catalog.getInstrumentModel()
                    .getModelType().getModelTypeNameTranslation()).orElse(""));
            dto.setModelId(catalog.getInstrumentModel().getModelid());
            dto.setModelName(instrumentModelNameWithTypology(catalog.getInstrumentModel(), locale));
            dto.setServiceType(getBestTranslation(catalog.getServiceType().getShortnameTranslation()).orElse(""));
            dto.setPrice(catalog.getFixedPrice());
            dto.setId(catalog.getId());
            dto.setComments(catalog.getComments());
            dto.setIsSalesCategory(catalog.getInstrumentModel().getModelType().getSalescategory());
            dtos.add(dto);
        });
        return dtos;
    }

    private Map<Boolean, List<QuoteItemOutputDto>> splitPrices(Set<PriceLookupOutput> prices) {
        return prices.stream().filter(p -> p.getQuotationItemId() != null)
                .map(p -> Tuple.of(p, quotationItemServ.findQuotationItem(p.getQuotationItemId())))
                .map(this::processPriceQuotationPair).collect(Collectors.partitioningBy(QuoteItemOutputDto::getIsOther));
    }

    private List<QuoteItemOutputDto> getQuoteItemOutputDtos(Set<PriceLookupOutput> prices) {
        return prices.stream().filter(p -> p.getQuotationItemId() != null)
                .map(p -> Tuple.of(p, quotationItemServ.findQuotationItem(p.getQuotationItemId())))
                .map(this::processPriceQuotationPair).collect(Collectors.toList());
    }

    private QuoteItemOutputDto processPriceQuotationPair(Tuple2<PriceLookupOutput, Quotationitem> tuple) {
        val today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
        val plo = tuple._1;
        val qi = tuple._2;
        return QuoteItemOutputDto.builder()
                .isOther(!(plo.getPreferredQuotation() ||
                        (plo.getQuotationAccepted() && plo.getQuotationIssued() &&
                                plo.getQuotationExpiryDate() != null && plo.getQuotationExpiryDate().isAfter(today))))
                .prefered(plo.getPreferredQuotation())
                .statusName(getBestTranslation(qi.getQuotation().getQuotestatus().getNametranslations()).orElse(""))
                .source(plo.getQueryType().getMessage())
                .issued(plo.getQuotationIssued())
                .accepted(plo.getQuotationAccepted())
                .exiryDate(plo.getQuotationExpiryDate())
                .expired(qi.getQuotation().isExpired())
                .quoteId(qi.getQuotation().getId())
                .quoteNo(qi.getQuotation().getQno())
                .quotationVer(qi.getQuotation().getVer())
                .itemId(qi.getId())
                .itemNo(qi.getItemno())
                .price(plo.getQuotationCalCostTotalCost())
                .discountRate(plo.getQuotationCalCostDiscountRate())
                .discount(qi.getCalibrationCost().getDiscountValue())
                .finalPrice(qi.getCalibrationCost().getFinalCost())
                .currencyCode(qi.getQuotation().getCurrency().getCurrencyCode())
                .costId(plo.getQuotationCalCostId())
                .build();
    }

    private void findSingleCatalogPrice(InstrumentModel model, ServiceType serviceType, Company company,
                                        PreliminaryCompInstToQuoteItemWrapper wrap) {
        CatalogPrice catalogPrice = priceServ.findSingle(model, serviceType,
                CostType.CALIBRATION, company);
        if (catalogPrice != null) {
            CatalogPriceDTO dto = new CatalogPriceDTO();
            dto.setModelTypeName(getBestTranslation(catalogPrice.getInstrumentModel()
                    .getModelType().getModelTypeNameTranslation()).orElse(""));
            dto.setModelId(catalogPrice.getInstrumentModel().getModelid());
            dto.setModelName(catalogPrice.getInstrumentModel().getModel());
            dto.setServiceType(getBestTranslation(catalogPrice.getServiceType().getShortnameTranslation()).orElse(""));
            dto.setPrice(catalogPrice.getFixedPrice());
            dto.setId(catalogPrice.getId());
            dto.setComments(catalogPrice.getComments());
            wrap.getCatalogPrices().add(dto);
        }
    }


    @Override
    public List<PreliminaryCompInstToQuoteItemWrapper> sourceCompanyInstrumentModelToQIValues(List<Integer> calTypeIds, Integer modelId,
                                                                                              Integer coid, Integer quoteId) {
        Locale locale = LocaleContextHolder.getLocale();
        // create list of wrappers
        List<PreliminaryCompInstToQuoteItemWrapper> prelWraps = new ArrayList<PreliminaryCompInstToQuoteItemWrapper>();
        // get Instrument Model
        InstrumentModel model = this.instrumentModelService.get(modelId);
        // get current quotation
        Quotation quote = this.quotationServ.get(quoteId);
        for (Integer calTypeId : calTypeIds) {
            PreliminaryCompInstToQuoteItemWrapper wrap = this.getDefaultPrices(null, model, calTypeServ.get(calTypeId), quote,
                    quote.getCreatedBy().getSub().getSubdivid(), locale);
            prelWraps.add(wrap);
        }
        return prelWraps;
    }

}
