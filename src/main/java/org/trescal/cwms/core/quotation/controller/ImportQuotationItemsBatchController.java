package org.trescal.cwms.core.quotation.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.batch.config.AutoSubmitPolicyEnum;
import org.trescal.cwms.batch.config.entities.backgroundtask.BackgroundTask;
import org.trescal.cwms.batch.config.entities.backgroundtask.BackgroundTaskService;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.exchangeformat.dto.ExchangeFormatDTO;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.ExchangeFormatFile;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db.ExchangeFormatFileService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatTypeEnum;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.form.ImportQuotationItemsForm;
import org.trescal.cwms.core.quotation.form.validator.ImportQuotationItemsBatchFormValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV,
		Constants.SESSION_ATTRIBUTE_COMPANY })
public class ImportQuotationItemsBatchController {

	public static final String IMPORT_QUOTATION_FORM = "importQuotationItemsBatch";

	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private ExchangeFormatService exchangeService;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;
	@Autowired
	private BackgroundTaskService bgTaskService;
	@Autowired
	private ExchangeFormatFileService efFileService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private ImportQuotationItemsBatchFormValidator validator;

	@ModelAttribute(IMPORT_QUOTATION_FORM)
	public ImportQuotationItemsForm submitImportQuotationIntems(
			@RequestParam(name = "id", required = true) int quotationid) {
		ImportQuotationItemsForm form = new ImportQuotationItemsForm();
		Quotation quotation = quoteServ.get(quotationid);
		form.setCoid(quotation.getContact().getSub().getComp().getCoid());
		form.setPersonid(quotation.getContact().getPersonid());
		form.setQuotationId(quotationid);
		form.setSubdivid(quotation.getContact().getSub().getSubdivid());
		form.setAddrid(quotation.getContact().getSub().getComp().getLegalAddress().getAddrid());
		return form;
	}

	@ModelAttribute("autoSubmitPolicies")
	protected List<KeyValue<String, String>> getAutoSubmitPolicies() {
		List<AutoSubmitPolicyEnum> list = new ArrayList<>();
		for (AutoSubmitPolicyEnum value : AutoSubmitPolicyEnum.values()) {
			if (!value.equals(AutoSubmitPolicyEnum.SUBMIT_ONLY_ITEMS_WITHOUT_ERRORS_AND_WITHOUT_WARNINGS)
					&& !value.equals(AutoSubmitPolicyEnum.SUBMIT_IF_ALL_WITHOUT_ERRORS_AND_WITHOUT_WARNINGS)) {
				list.add(value);
			}
		}
		return list.stream().map(v -> new KeyValue<>(v.name(), v.getValue())).collect(Collectors.toList());
	}

	@ModelAttribute("efImportQuotationItems")
	public List<ExchangeFormatDTO> exchangeFormatImportQuotationItems(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@RequestParam(name = "id", required = true) int quotationid) {
		List<ExchangeFormatDTO> ef = new ArrayList<>();
		Company clientCompany = quoteServ.get(quotationid).getContact().getSub().getComp();

		ef.addAll(this.exchangeService.getExchangeFormats(ExchangeFormatTypeEnum.QUOTATION_ITEMS_IMPORTATION,
				compServ.get(companyDto.getKey()), subdivService.get(subdivDto.getKey()), clientCompany));

		return ef;
	}

	@GetMapping(value = "/importquotationitemsbatch.htm")
	public String doGet(Model model, Locale locale,
			@ModelAttribute(IMPORT_QUOTATION_FORM) ImportQuotationItemsForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@RequestParam(name = "id", required = true) int quotationid) {
		model.addAttribute("quotation", quoteServ.get(quotationid));
		model.addAttribute("errors", form.getErrorMessage());
		model.addAttribute("bgtaskId", form.getBgTaskId());
		model.addAttribute("personid", userService.get(username).getCon().getPersonid());
		return "trescal/core/quotation/importquotationitemsbatch";
	}

	@PostMapping(value = "/importquotationitemsbatch.htm")
	public String onSubmit(@Valid @ModelAttribute(IMPORT_QUOTATION_FORM) ImportQuotationItemsForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyDto,
			BindingResult bindingResult, final RedirectAttributes redirectAttributes, Model model, Locale locale)
			throws IOException {

		validator.validate(form);
		if (!form.getErrorMessage().isEmpty()) {
			return doGet(model, locale, form, username, form.getQuotationId());
		}

		User user = userService.get(username);
		BackgroundTask bgTask = null;

		try {

			ExchangeFormatFile efFile = efFileService.createEfFile(user.getCon(), form.getFile(),
					new ExchangeFormatFile());

			bgTask = bgTaskService.submitQuotationItemsImportBgTask(form, user.getCon().getId(), user.getCon().getId(),
					subdivDto.getKey(), efFile, locale.toString());
			form.setBgTaskId(bgTask.getId());

		} catch (JobExecutionAlreadyRunningException e) {
			form.getErrorMessage().add(
					messageSource.getMessage("importcalibrations.jobalreadyrunning", new Object[] {}, null, locale));
		} catch (JobRestartException e) {
			form.getErrorMessage()
					.add(messageSource.getMessage("importcalibrations.jobrestarterror", new Object[] {}, null, locale));
		} catch (JobInstanceAlreadyCompleteException e) {
			form.getErrorMessage().add(
					messageSource.getMessage("importcalibrations.jobalreadycompleted", new Object[] {}, null, locale));
		} catch (JobParametersInvalidException e) {
			form.getErrorMessage().add(
					messageSource.getMessage("importcalibrations.invalidjobparametres", new Object[] {}, null, locale));
		}

		return doGet(model, locale, form, username, form.getQuotationId());
	}
}
