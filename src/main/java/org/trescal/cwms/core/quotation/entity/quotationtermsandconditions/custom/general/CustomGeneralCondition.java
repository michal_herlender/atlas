package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.general;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.QuotationGeneralCondition;

@Entity
@Table(name="customquotationgeneralcondition")
public class CustomGeneralCondition extends QuotationGeneralCondition
{
	private Quotation quotation;
	
	public CustomGeneralCondition()
	{
		super();
	}
	
	@OneToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name="quoteid", unique = false, nullable = false, insertable = true, updatable = true)
	public Quotation getQuotation() {
		return quotation;
	}

	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}
	
	/*
	 * @JoinColumns
		( 
			{
				@JoinColumn(name = "qno", unique = false, nullable = false, insertable = true, updatable = true),
				@JoinColumn(name = "ver", unique = false, nullable = false, insertable = true, updatable = true),
			}
		)
	 */
}
