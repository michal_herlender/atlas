package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general.DefaultGeneralCondition;

public interface DefaultGeneralConditionService extends BaseService<DefaultGeneralCondition, Integer>
{
	/**
	 * @return latest terms and conditions
	 */
	DefaultGeneralCondition getLatest(Company allocatedCompany);
}