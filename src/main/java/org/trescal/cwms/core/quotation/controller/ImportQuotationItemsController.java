package org.trescal.cwms.core.quotation.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.documents.excel.utils.ExcelFileReaderUtil;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.exchangeformat.dto.ExchangeFormatDTO;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatTypeEnum;
import org.trescal.cwms.core.exchangeformat.utils.ExchangeFormatGeneralValidator;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.form.ImportQuotationItemsForm;
import org.trescal.cwms.core.quotation.form.validator.ImportQuotationItemsFormValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.HTTPSESS_NEW_FILE_LIST, Constants.SESSION_ATTRIBUTE_SUBDIV,
		Constants.SESSION_ATTRIBUTE_COMPANY })
public class ImportQuotationItemsController {

	public static final String IMPORT_QUOTATION_FORM = "importQuotationItems";

	@Autowired
	private ImportQuotationItemsFormValidator validator;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private ExchangeFormatService exchangeService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private ExcelFileReaderUtil excelFileReaderUtil;
	@Autowired
	private ExchangeFormatGeneralValidator efValidator;

	@InitBinder(IMPORT_QUOTATION_FORM)
	protected void initBinder(ServletRequestDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute(IMPORT_QUOTATION_FORM)
	public ImportQuotationItemsForm submitImportQuotationIntems(
			@RequestParam(name = "id", required = true) int quotationid) {
		ImportQuotationItemsForm form = new ImportQuotationItemsForm();
		Quotation quotation = quoteServ.get(quotationid);
		form.setCoid(quotation.getContact().getSub().getComp().getCoid());
		form.setPersonid(quotation.getContact().getPersonid());
		form.setQuotationId(quotationid);
		form.setSubdivid(quotation.getContact().getSub().getSubdivid());
		form.setAddrid(quotation.getContact().getSub().getComp().getLegalAddress().getAddrid());
		return form;
	}

	@ModelAttribute("efImportQuotationItems")
	public List<ExchangeFormatDTO> exchangeFormatImportQuotationItems(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@RequestParam(name = "id", required = true) int quotationid) {
		List<ExchangeFormatDTO> ef = new ArrayList<>();
		Company clientCompany = quoteServ.get(quotationid).getContact().getSub().getComp();

		ef.addAll(this.exchangeService.getExchangeFormats(ExchangeFormatTypeEnum.QUOTATION_ITEMS_IMPORTATION,
				compServ.get(companyDto.getKey()), subdivService.get(subdivDto.getKey()), clientCompany));

		return ef;
	}

	@RequestMapping(value = "/importquotationitems.htm", method = RequestMethod.GET)
	protected String referenceData(Model model, Locale locale,
			@ModelAttribute(IMPORT_QUOTATION_FORM) ImportQuotationItemsForm form,
			@RequestParam(name = "id", required = true) int quotationid) {
		model.addAttribute("quotation", quoteServ.get(quotationid));
		return "trescal/core/quotation/importquotationitems";
	}

	@RequestMapping(value = "/importquotationitems.htm", method = RequestMethod.POST)
	public String onSubmit(@Valid @ModelAttribute(IMPORT_QUOTATION_FORM) ImportQuotationItemsForm form,
			BindingResult bindingResult, final RedirectAttributes redirectAttributes, Model model, Locale locale,
			HttpSession session, @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyDto) throws Exception {

		if (bindingResult.hasErrors()) {
			return referenceData(model, locale, form, form.getQuotationId());
		}

		ExchangeFormat ef = exchangeService.get(form.getExchangeFormatId());
		// get File Content
		List<LinkedCaseInsensitiveMap<String>> fileContent = excelFileReaderUtil
				.readExcelFile(form.getFile().getInputStream(), ef.getSheetName(), ef.getLinesToSkip());

		if (efValidator.validate(ef, fileContent, bindingResult).hasErrors()) {
			return referenceData(model, locale, form, form.getQuotationId());
		}

		redirectAttributes.addFlashAttribute("fileContent", fileContent);
		redirectAttributes.addFlashAttribute("importQuotationItems", form);

		return "redirect:/importedquotationitemssynthesis.htm";
	}

}
