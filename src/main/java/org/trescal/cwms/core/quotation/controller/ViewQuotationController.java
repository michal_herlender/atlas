package org.trescal.cwms.core.quotation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.db.ContractService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.db.JobQuoteLinkService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.comparators.QuotationItemCustomComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeadingComparator;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.quotation.form.QuotationItemSearchForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.dto.InstructionDTO;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.instruction.db.InstructionService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceTypeComparator;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.core.userright.entity.limitcompany.db.LimitCompanyService;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;

/**
 * Form that displays a {@link Quotation} and allows some of it's basic
 * properties to be updated.
 * 
 * @author Richard
 */
@Controller
@IntranetController
@SessionAttributes({ Constants.HTTPSESS_NEW_FILE_LIST, Constants.SESSION_ATTRIBUTE_USERNAME,
		Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY })
public class ViewQuotationController {
	private static final int linkedJobResults = 5;

	@Value("${cwms.config.avalara.aware}")
	private Boolean avalaraAware;
	@Value("${cwms.config.quotation.size.restrictview}")
	private long sizeRestrictView;
	@Autowired
	private CalReqService calReqServ;
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private JobQuoteLinkService jqlServ;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private QuotationItemService quotationItemService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private SystemComponentService systemCompServ;
	@Autowired
	private LimitCompanyService limitCompanyService;
	@Autowired
	private ContractService contractService;

	@Autowired
	private InstructionService instructionService;
	
	public static final String FORM_NAME = "command";
	
	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}
	
	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
		return new ArrayList<>();
	}

	private Quotation getQuotation(int id) {
		Quotation q = this.quoteServ.get(id);
		if ((id == 0) || (q == null)) {
			throw new RuntimeException("Quotation could not be found");
		}
		return q;
	}

	@ModelAttribute(FORM_NAME)
	public QuotationItemSearchForm formBackingObject(@RequestParam(name = "id") int quotationid) {
		QuotationItemSearchForm form = new QuotationItemSearchForm();
		form.setQuotationid(quotationid);
		return form;
	}

	@RequestMapping(value = "/viewquotation.htm")
	protected String referenceData(HttpSession session, Model model,
								   @ModelAttribute(FORM_NAME) QuotationItemSearchForm form,
								   @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
								   @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyDto,
								   @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV)SubdivKeyValue subdivDto,
								   @RequestParam(name = "id") int quotationid) {
		Company comp = compServ.get(companyDto.getKey());
		// the pricing type class name
		session.setAttribute(Constants.HTTPSESS_PRICING_CLASS_NAME, Quotation.class);
		// set the status type class name
		session.setAttribute(Constants.HTTPSESS_STATUS_CLASS_NAME, QuoteStatus.class);
		model.addAttribute("showVAT", !this.avalaraAware);
		Quotation q = getQuotation(form.getQuotationid());
		long totalItemCount = this.quotationItemService.getItemCount(form.getQuotationid());
		model.addAttribute("totalItemCount", totalItemCount);
		// 2016-02-22 - Added check of number of quotation items until
		// performance
		// issues fully resolved
		if (totalItemCount >= sizeRestrictView) {
			throw new RuntimeException("This quotation has " + totalItemCount
					+ " items and due to temporary performance restrictions, quotations with " + sizeRestrictView
					+ " items or more cannot currently be displayed.");
		}
		// Display subset of quotation items for performance / usability reasons
		PagedResultSet<Quotationitem> prs = this.quotationItemService.findQuotationItems(form,
				new PagedResultSet<>(form.getResultsPerPage(), form.getPageNo()));
		model.addAttribute("prs", prs);
		Collection<Quotationitem> pagedQItems = prs.getResults();
		q.setSentEmails(this.emailServ.getAllComponentEmails(Component.QUOTATION, form.getQuotationid()));
		model.addAttribute("quotation", q);
		// get files in the root directory
		FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ.getFilesForComponentRoot(Component.QUOTATION, q.getId(), newFiles);
		// calculate the total discounts for the quotation
		// model.addAttribute("discountCalculator",
		// this.quoteServ.getQuotationTotalDiscount(q.getQuotationitems()));
		model.addAttribute("discountCalculator", this.quotationItemService.getTotalDiscountSummaryDTO(q.getId()));
		// load the terms and conditions for the quotation
		model.addAttribute("conditions", this.quoteServ.findQuotationConditions(q));
		// get count of all job quote links
		model.addAttribute("jobQuoteLinkCount", this.jqlServ.getCountJobQuoteLinks(q.getId()));
		// get first five jobs linked to quote
		model.addAttribute("jobQuoteLinks", this.jqlServ.findJobQuoteLinksResultset(q.getId(), 1, linkedJobResults));
		// add the rows per page token
		model.addAttribute("resPerPage", linkedJobResults);
		// get a list of all other quotations that match this quotation number
		model.addAttribute("quotelist", this.quoteServ.findQuotations(q.getQno()));
		// private only notes
		model.addAttribute("privateOnlyNotes", NoteType.QUOTENOTE.isPrivateOnly());
		model.addAttribute(Constants.REFDATA_SYSTEM_COMPONENT, this.systemCompServ.findComponent(Component.QUOTATION));
		model.addAttribute("defaultCurrency", q.getOrganisation().getCurrency());
		model.addAttribute(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
		model.addAttribute(Constants.REFDATA_FILES_NAME, fileBrowserServ.getFilesName(fileBrowserWrapper));
		// find applicable calibration requirements that may have been set
		Map<Integer, CalReq> calReqs = this.calReqServ.findCalReqsForQuotationItems(q, pagedQItems);
		model.addAttribute("calReqs", calReqs);
		// add all active calibration types
		model.addAttribute("caltypes", this.calTypeServ.getCalTypes());
		// add all active calibration types
		model.addAttribute("serviceTypes", this.serviceTypeService.getAll());
		// add pricing tools to the page (replaced with quotation summary)
		model.addAttribute("pricingtool", this.quoteServ.getQuotationSummary(q));
		// if the user has expanded all notes on the quotation then this session
		// attribute will have been set to maintain state
		model.addAttribute("sessQuoteItemNoteVisibility",
				session.getAttribute("sessQuoteItemNoteVisibility" + q.getId()));
		// if the user has marked a quote item on the quotation then this
		// session attribute will have been set to maintain state
		model.addAttribute("sessSetScrollTopMarker", session.getAttribute("sessSetScrollTopMarker" + q.getId()));
		// Establish a map of headings to cal types to quotation items (just for
		// the
		// items in result set)
		model.addAttribute("quotationItemMap", getQuotationItemMap(prs));
		Collection<? extends GrantedAuthority> auth = SecurityContextHolder.getContext().getAuthentication()
				.getAuthorities();
		// Add the limit allow for issuing this quotation
		model.addAttribute("limit", limitCompanyService.getQuotationLimit(auth, comp));

		model.addAttribute("isWellAllocated", (q.getOrganisation().getCoid() == companyDto.getKey()));
		model.addAttribute("isUnderLimit", limitCompanyService.getQuotationUnderlimit(q.getOrganisation().getId(), q.getFinalCost(), auth, comp));
		// Add contract id
		String instructionLinkContractid = null;
		Contract quotationContrat = this.contractService.findContractByQuotation(q);
		Integer contractId = null;
		if(quotationContrat != null){
			contractId = quotationContrat.getId();
			instructionLinkContractid = String.valueOf(contractId);
		}
		model.addAttribute("instructionLinkContractid", instructionLinkContractid);


		InstructionType[] instructionType = new InstructionType[]{InstructionType.DISCOUNT, InstructionType.COSTING};
		Contact contact = q.getContact();
		Integer linkedJobId = q.getLinkedJobs().stream().findAny().map(JobQuoteLink::getJob).map(Job::getJobid).orElse(null);
		Subdiv sub = contact.getSub();

		List<InstructionDTO> instructionDTOS = this.instructionService.ajaxInstructions(subdivDto.getKey(), singletonList(sub.getComp().getCoid()), singletonList(sub.getSubdivid()), singletonList(contact.getPersonid()), contractId, linkedJobId, null, null, instructionType, null);

		model.addAttribute("instructionsCount", instructionDTOS.size());
		return "trescal/core/quotation/viewquotation";
	}

	// For just the items in the paged result set only
	private Map<QuoteHeading, Map<ServiceType, Set<Quotationitem>>> getQuotationItemMap(
			PagedResultSet<Quotationitem> prs) {
		Collection<Quotationitem> results = prs.getResults();
		Map<QuoteHeading, Map<ServiceType, Set<Quotationitem>>> map = results.stream().map(Quotationitem::getHeading).distinct().collect(
				Collectors.toMap(Function.identity(),x -> new TreeMap<>(new ServiceTypeComparator()),(x1, x2)->x1,()->new TreeMap<>(new QuoteHeadingComparator()) )
		);
		for (Quotationitem qi : results) {
			Map<ServiceType, Set<Quotationitem>> subMap = map.get(qi.getHeading());
			subMap.putIfAbsent(qi.getServiceType(), new TreeSet<>(new QuotationItemCustomComparator()));
			subMap.get(qi.getServiceType()).add(qi);
		}
		return map;
	}
}