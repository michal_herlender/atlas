/**
 * 
 */
package org.trescal.cwms.core.quotation.entity.costs.calcost;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewLinkedCalibrationCost;
import org.trescal.cwms.core.pricing.entity.costs.thirdparty.TPSupportedCalCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingLinkedCalibrationCost;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;

@Entity
@DiscriminatorValue("quotation")
public class QuotationCalibrationCost extends TPSupportedCalCost
{
	private Set<ContractReviewLinkedCalibrationCost> conRevLinkedCalibrationCosts;

	private Set<JobCostingLinkedCalibrationCost> jobCostLinkedCalibrationCosts;
	private Quotationitem quoteItem;

	public QuotationCalibrationCost() {
		super();
	}
	
	public QuotationCalibrationCost(JobCostingCalibrationCost jcCalCost) {
		super(jcCalCost);
	}
	
	/**
	 * Copy constructor.
	 * 
	 * @param cost the {@link QuotationCalibrationCost} to copy.
	 */
	public QuotationCalibrationCost(QuotationCalibrationCost cost)
	{
		this.quoteItem = cost.getQuoteItem();
		this.active = cost.isActive();
		this.costType = cost.getCostType();
		this.customOrderBy = cost.getCustomOrderBy();
		this.discountRate = new BigDecimal(cost.getDiscountRate().toPlainString());
		this.discountValue = new BigDecimal(cost.getDiscountValue().toPlainString());
		this.finalCost = new BigDecimal(cost.getFinalCost().toPlainString());
		this.totalCost = new BigDecimal(cost.getTotalCost().toPlainString());
		this.setHouseCost(cost.getHouseCost());
		this.setLinkedCostSrc(cost.getLinkedCostSrc());
		this.setThirdCostSrc(cost.getThirdCostSrc());
		this.setThirdCostTotal(new BigDecimal(cost.getThirdCostTotal().toPlainString()));
		this.setThirdManualPrice(new BigDecimal(cost.getThirdManualPrice().toPlainString()));
		this.setThirdMarkupRate(new BigDecimal(cost.getThirdMarkupRate().toPlainString()));
		this.setThirdMarkupSrc(cost.getThirdMarkupSrc());
		this.setThirdMarkupValue(new BigDecimal(cost.getThirdMarkupValue().toPlainString()));
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "quotationCalibrationCost")
	public Set<ContractReviewLinkedCalibrationCost> getConRevLinkedCalibrationCosts()
	{
		return this.conRevLinkedCalibrationCosts;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "quotationCalibrationCost")
	public Set<JobCostingLinkedCalibrationCost> getJobCostLinkedCalibrationCosts()
	{
		return this.jobCostLinkedCalibrationCosts;
	}

	@OneToOne(mappedBy = "calibrationCost")
	public Quotationitem getQuoteItem()
	{
		return this.quoteItem;
	}

	public void setConRevLinkedCalibrationCosts(Set<ContractReviewLinkedCalibrationCost> conRevLinkedCalibrationCosts)
	{
		this.conRevLinkedCalibrationCosts = conRevLinkedCalibrationCosts;
	}

	public void setJobCostLinkedCalibrationCosts(Set<JobCostingLinkedCalibrationCost> jobCostLinkedCalibrationCosts)
	{
		this.jobCostLinkedCalibrationCosts = jobCostLinkedCalibrationCosts;
	}

	public void setQuoteItem(Quotationitem quoteItem)
	{
		this.quoteItem = quoteItem;
	}
}
