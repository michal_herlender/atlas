package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.db;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.DefaultCalibrationCondition;

@Repository("DefaultCalibrationConditionDao")
public class DefaultCalibrationConditionDaoImpl extends AllocatedToCompanyDaoImpl<DefaultCalibrationCondition, Integer> implements DefaultCalibrationConditionDao
{
	@Override
	protected Class<DefaultCalibrationCondition> getEntity() {
		return DefaultCalibrationCondition.class;
	}
	
	@Override
	public DefaultCalibrationCondition getLatest(int type, Company company) {
		Criteria criteria = getSession().createCriteria(DefaultCalibrationCondition.class);
		criteria.createCriteria("caltype").add(Restrictions.idEq(type));
		criteria.add(this.getCompanyCrit(company));
		criteria.addOrder(Order.desc("lastModified"));
		criteria.setFirstResult(0);
		criteria.setMaxResults(1);
		return (DefaultCalibrationCondition) criteria.uniqueResult();
	}
}