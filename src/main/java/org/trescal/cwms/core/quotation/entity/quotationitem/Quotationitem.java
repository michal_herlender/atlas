package org.trescal.cwms.core.quotation.entity.quotationitem;

import java.math.BigDecimal;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.*;

import org.hibernate.annotations.SortComparator;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.pricing.entity.costs.CostOrderComparator;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.ThirdPartyPricingItem;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteitemnote.QuoteItemNote;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationTypeComparator;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItemComparator;

import lombok.Setter;

/**
 * Entity representing an item on a {@link Quotation}.
 * 
 * @author Richard
 */
@Entity
@Table(name = "quotationitem", indexes = {
		@Index(columnList = "quoteid", name = "IDX_quotationitem_quoteid"),
		@Index(columnList = "heading_headingid", name = "IDX_quotationitem_headingid"),
		@Index(columnList = "plantid", name = "IDX_quotationitem_plantid"),
		@Index(columnList = "modelid", name = "IDX_quotationitem_modelid"),
		@Index(columnList = "servicetypeid", name = "IDX_quotationitem_servicetypeid")
})
@Setter
public class Quotationitem extends ThirdPartyPricingItem implements NoteAwareEntity {

	private Integer id;
	/**
	 * Defines this module's parent base unit
	 */
	private Quotationitem baseUnit;
	private QuotationCalibrationCost calibrationCost;
	@Deprecated
	private CalibrationType caltype;
	private ServiceType serviceType;
	private QuoteHeading heading;
	protected BigDecimal inspection;
	private Instrument inst;
	private InstrumentModel model;
	/**
	 * Defines a list of all modules belonging to this quotationitem.
	 */
	private Set<Quotationitem> modules;
	private Set<QuoteItemNote> notes;
	private String plantno;
	protected QuotationPurchaseCost purchaseCost;
	private Quotation quotation;
	private String referenceNo;
	private Set<TPQuoteRequestItem> tpQuoteItemRequests;

	/** default constructor */
	public Quotationitem() {
		super();
		this.notes = new TreeSet<>(new NoteComparator());
	}

	public Quotationitem(Quotationitem qi) {
		super(qi);
		this.caltype = qi.getCaltype();
		this.serviceType = qi.getServiceType();
		this.plantno = qi.getPlantno();
		this.model = qi.getModel();
		this.inst = qi.getInst();
		this.inspection = qi.getInspection();
		this.calibrationCost = new QuotationCalibrationCost(qi.getCalibrationCost());
		this.purchaseCost = new QuotationPurchaseCost(qi.getPurchaseCost());
	}

	@Override
	@Id
	@GeneratedValue(generator = "quotation_item_id_sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "quotation_item_id_sequence", sequenceName = "quotation_item_id_sequence")
	public Integer getId() {
		return id;
	}

	@Override
	@Transient
	public SortedSet<Cost> getCosts() {
		SortedSet<Cost> costs = new TreeSet<>(new CostOrderComparator());
		if (calibrationCost != null)
			costs.add(calibrationCost);
		if (purchaseCost != null)
			costs.add(purchaseCost);
		return costs;
	}

	// Changed EAGER to LAZY 2019-06-02, we don't currently have modules/base units
	// in model database
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "baseid", foreignKey = @ForeignKey(name = "FK_quotationitem_baseid"))
	@Override
	public Quotationitem getBaseUnit() {
		return this.baseUnit;
	}

	/**
	 * @return the calCost
	 */
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "calcost_id", foreignKey = @ForeignKey(name = "FK_quotationitem_calcost"))
	public QuotationCalibrationCost getCalibrationCost() {
		return this.calibrationCost;
	}

	/**
	 * @deprecated Use getServiceType()!
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "caltype_caltypeid", foreignKey = @ForeignKey(name = "FK_quotationitem_caltype"))
	@SortComparator(CalibrationTypeComparator.class)
	public CalibrationType getCaltype() {
		return this.caltype;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "heading_headingid", foreignKey = @ForeignKey(name = "FK_quotationitem_heading"))
	public QuoteHeading getHeading() {
		return this.heading;
	}

	@Column(name = "inspection", nullable = false, precision = 10, scale = 2)
	public BigDecimal getInspection() {
		return this.inspection;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid", foreignKey = @ForeignKey(name = "FK_quotationitem_plantid"))
	public Instrument getInst() {
		return this.inst;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelid", foreignKey = @ForeignKey(name = "FK_quotationitem_modelid"))
	public InstrumentModel getModel() {
		return this.model;
	}

	// Changed EAGER to LAZY 2019-06-02, we don't currently have modules/base units
	// in model database
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "baseUnit")
	@SortComparator(QuotationItemComparator.class)
	@Override
	public Set<Quotationitem> getModules() {
		return this.modules;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "quotationitem")
	@SortComparator(NoteComparator.class)
	public Set<QuoteItemNote> getNotes() {
		return this.notes;
	}

	@Column(name = "plantno", length = 50)
	public String getPlantno() {
		return this.plantno;
	}

	@Transient
	public BigDecimal getPriorDiscountCalPrice() {
		return this.calibrationCost.getFinalCost().add(this.calibrationCost.getDiscountValue());
	}

	@Transient
	public Integer getPrivateActiveNoteCount() {
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount() {
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount() {
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount() {
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "purchasecost_id", foreignKey = @ForeignKey(name = "FK_quotationitem_purchasecost"))
	public QuotationPurchaseCost getPurchaseCost() {
		return this.purchaseCost;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quoteid", nullable = false, foreignKey = @ForeignKey(name = "FK_quotationitem_quoteid"))
	public Quotation getQuotation() {
		return this.quotation;
	}

	@Transient
	public String getReferenceNo() {
		// set initial value
		this.referenceNo = "";
		// instrument quote item?
		if (this.inst != null) {
			if ((this.inst.getPlantno() != null) && (!this.inst.getPlantno().equalsIgnoreCase(""))) {
				this.referenceNo = "(P) " + this.inst.getPlantno();
			} else {
				this.referenceNo = "(S) " + this.inst.getSerialno();
			}
		}
		// has reference number been assigned?
		// could not find on instrument, maybe still use quote item?
		if (this.referenceNo.equalsIgnoreCase("")) {
			if ((this.getPlantno() != null) && (!this.getPlantno().equalsIgnoreCase(""))) {
				this.referenceNo = "(QIP) " + this.getPlantno();
			}
		}
		// return ref no
		return this.referenceNo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "servicetypeid", foreignKey = @ForeignKey(name = "FK_quotationitem_servicetype"))
	public ServiceType getServiceType() {
		return serviceType;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "requestFromQuoteItem")
	@SortComparator(TPQuoteRequestItemComparator.class)
	public Set<TPQuoteRequestItem> getTpQuoteItemRequests() {
		return this.tpQuoteItemRequests;
	}

	/**
	 * Calculates the unit discounts provided on this quotation item (not including
	 * modules)
	 */
	@Transient
	public BigDecimal getUnitDiscountValue() {
		BigDecimal result = new BigDecimal("0.00");
		result = result.add(this.getGeneralDiscountValue());
		if (this.calibrationCost != null)
			result = result.add(this.calibrationCost.getDiscountValue());
		if (this.purchaseCost != null)
			result = result.add(this.purchaseCost.getDiscountValue());
		return result;
	}

	/**
	 * Calculates the cumulative cost of the base unit and it's modules.
	 * 
	 * @return the cost of the unit.
	 */
	@Transient
	public BigDecimal getUnitsTotalCost() {
		BigDecimal unitCalpriceTotal = BigDecimal.valueOf(this.calibrationCost.getFinalCost().doubleValue()).setScale(2,
				4);
		if (this.modules != null) {
			for (Quotationitem qi : this.modules) {
				unitCalpriceTotal = unitCalpriceTotal.add(qi.getCalibrationCost().getFinalCost());
			}
		}
		return unitCalpriceTotal;
	}

	@Override
	public String toString() {
		return "Quotationitem id:" + this.getId() + ", itemno:" + this.getItemno();
	}
}