package org.trescal.cwms.core.quotation.entity.quotationdocdefaulttext.db;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.quotation.entity.quotationdocdefaulttext.QuotationDocDefaultText;

@Service
public class QuotationDocDefaultTextServiceImpl extends BaseServiceImpl<QuotationDocDefaultText, Integer>
		implements QuotationDocDefaultTextService {
	
	@Autowired
	private QuotationDocDefaultTextDao baseDao;

	@Override
	protected BaseDao<QuotationDocDefaultText, Integer> getBaseDao() {
		return baseDao;
	}
	
	@Override
	public QuotationDocDefaultText findDefaultText(Locale locale) {
		return baseDao.findDefaultText(locale);
	}

}
