package org.trescal.cwms.core.quotation.form;

import java.util.Set;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteheading.comparators.QuotationHeadingSortType;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class QuoteHeadingForm
{
	private Integer assignToHeading;
	@NotNull
	private String formfunction;
	private QuoteHeading header;
	@NotNull(message="{error.value.notselected}")
	private String itemInsertOption;
	private Set<Integer> items;
	private Quotation quotation;
	private boolean quoteDefault;
	private QuotationHeadingSortType sortType;


	@Transient
	public boolean isQuoteDefault()
	{
		return this.quoteDefault;
	}

	@Transient
	public void setQuoteDefault(boolean quoteDefault)
	{
		this.quoteDefault = quoteDefault;
	}

}
