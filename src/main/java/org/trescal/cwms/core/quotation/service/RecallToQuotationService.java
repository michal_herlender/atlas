package org.trescal.cwms.core.quotation.service;

import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.exception.EntityNotFoundException;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.form.RecallToQuotationForm;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;

public interface RecallToQuotationService
{
	Quotation prepareAndSaveRecallToQuotation(List<Integer> toQuotePlantIds, RecallDetail rdetail, Contact currentContact, Subdiv allocatedSubdiv) throws EntityNotFoundException;

	// Following seem exposed just for testing
	
	void prepareCalibrationCost(Instrument inst, JobItem ji, QuotationCalibrationCost qCalCost, Subdiv subDiv);

	Set<Quotationitem> prepareRecallInstrumentsToQuotationItems(List<Instrument> toquoteitems, Quotation quotation, int subdivId);

	Quotation prepareRecallToQuotation(List<Integer> toQuotePlantIds, RecallDetail rdetail, Contact currentContact, Subdiv allocatedSubdiv) throws EntityNotFoundException;

	Quotationitem recallInstrumentToQuotationitem(Instrument instrument, Quotation quotation, QuoteHeading qh, int subdivId); 
	
	Quotation createQuotationFromRecall(RecallToQuotationForm form, Subdiv allocatedSubdiv, Contact currentContact);
}
