package org.trescal.cwms.core.quotation.entity.quoteheading.comparators;

import java.util.Comparator;

import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;

public class QuotationHeadingAlphaComparator implements Comparator<QuoteHeading>
{
	@Override
	public int compare(QuoteHeading qh1, QuoteHeading qh2)
	{
		if (qh1.isSystemDefault() && !qh2.isSystemDefault())
		{
			return -1;
		}
		else if (!qh1.isSystemDefault() && qh2.isSystemDefault())
		{
			return 1;
		}
		else
		{
			// heading names are not exactly the same
			if (!qh1.getHeadingName().equalsIgnoreCase(qh2.getHeadingName()))
			{
				return (qh1.getHeadingName().compareToIgnoreCase(qh2.getHeadingName()));
			}
			else
			{
				return ((Integer) qh1.getHeadingId()).compareTo(qh2.getHeadingId());
			}
		}
	}
}