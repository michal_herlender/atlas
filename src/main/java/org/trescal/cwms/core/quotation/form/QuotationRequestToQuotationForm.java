package org.trescal.cwms.core.quotation.form;

import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class QuotationRequestToQuotationForm
{
	private QuotationRequest quotationRequest;
	private Integer coid;
	private Integer subdivid;
	private Integer personid;
	private boolean addRequestInfoAsNote;

}
