package org.trescal.cwms.core.quotation.entity.quotation;

import java.util.Comparator;

/**
 * Basic built in comparator which sorts quotations by registration date (desc)
 * 
 * @author Richard
 */
public class QuotationComparator implements Comparator<Quotation>
{
	public int compare(Quotation q1, Quotation q2)
	{
		return q2.getRegdate().compareTo(q1.getRegdate());
	}
}
