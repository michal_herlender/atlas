package org.trescal.cwms.core.quotation.controller;

import io.vavr.control.Either;
import lombok.Data;
import lombok.Value;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.quotation.entity.priorquotestatus.db.PriorQuoteStatusService;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.userright.entity.limitcompany.db.LimitCompanyService;

import java.time.LocalDate;
import java.util.Collection;

/**
 * Controller to perform status update to quotation Split from
 * ViewQuotationController 2017-02-11 as part of performance improvements
 * 
 * @author Galen
 *
 */

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY })
public class QuotationStatusController {

	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private PriorQuoteStatusService pqsServ;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private UserService userService;
	@Autowired
	private LimitCompanyService limitCompanyService;
	@Autowired
	private CompanyService companyServ;
	@Autowired
	private SessionUtilsService sessionService;

	private Quotation getQuotation(int id) {
		Quotation q = this.quoteServ.get(id);
		if ((id == 0) || (q == null)) {
			throw new RuntimeException("Quotation could not be found");
		}
		return q;
	}

	@RequestMapping(value = "/updatequotationstatus.htm", method = RequestMethod.GET)
	protected String forcedPost(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
								@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyDto,
								@RequestParam(name = "id") int id,
								@RequestParam(value = "issuing", required = false, defaultValue = "false") Boolean issuing,
								@RequestParam(value = "accepting", required = false, defaultValue = "false") Boolean accepting,
								@RequestParam(value = "statusid", required = false, defaultValue = "0") Integer statusid,
								@RequestParam(value = "clientacceptedon", required = false) @DateTimeFormat(pattern = "dd.MM.yyyy") LocalDate clientAcceptedOn) {
		Quotation q = getQuotation(id);
		Contact currentContact = this.userService.get(username).getCon();
		Collection<? extends GrantedAuthority> auth = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		Company comp = companyServ.get(companyDto.getKey());
		int originalStatusId = q.getQuotestatus().getStatusid();
		if (issuing && !q.isIssued()) {
			if (!limitCompanyService.getQuotationUnderlimit(q.getOrganisation().getId(), q.getFinalCost(), auth, comp))
				throw new RuntimeException("Has no right or is under limit allow");
			q.setQuotestatus((QuoteStatus) this.statusServ.findIssuedStatus(QuoteStatus.class));
		} else if (accepting && !q.isAccepted()) {
			// look for a user selected statusid otherwise use a default
			// accepted status
			q.setQuotestatus(statusid != 0 ? this.statusServ.findStatus(statusid, QuoteStatus.class)
				: (QuoteStatus) this.statusServ.findAcceptedStatus(QuoteStatus.class));
			if (clientAcceptedOn != null)
				q.setClientAcceptedOn(clientAcceptedOn);
		}
		// log the status change if one has occured
		this.pqsServ.updateQuoteStatus(q, originalStatusId, q.getQuotestatus().getStatusid(), currentContact);
		// persist the quotation
		this.quoteServ.merge(q);
		return "redirect:viewquotation.htm?id=" + q.getId();
	}

	@PostMapping("/updatequotationstatus.json")
    @ResponseBody
	Either<String,AcceptedOnDto> updateStatus(@RequestBody AcceptedOnForm form){
		val quote = quoteServ.get(form.getId());
		if(quote == null)
			return Either.left("Cannot find quote for Id: "+form.getId());
		quote.setClientAcceptedOn(form.getAcceptedOn());
		updateQuoteStatus(quote,statusServ.findAcceptedStatusByClass(QuoteStatus.class));
		quoteServ.merge(quote);
		val acceptedBy = quote.getAcceptedBy();
		return Either.right(AcceptedOnDto.of(acceptedBy!=null?acceptedBy.getName():"",quote.getAcceptedOn()));
	}

	private void updateQuoteStatus(Quotation quotation,QuoteStatus status){
		val originalStatus = quotation.getQuotestatus();
		quotation.setQuotestatus(statusServ.findAcceptedStatusByClass(QuoteStatus.class));
		val currentContact = sessionService.getCurrentContact();
		pqsServ.updateQuoteStatus(quotation,originalStatus.getStatusid(),status.getStatusid(),currentContact);
	}


}

@Data
class AcceptedOnForm {
	private Integer id;
	private LocalDate acceptedOn;
}

@Value(staticConstructor = "of")
class AcceptedOnDto {
	String acceptedBy;
	LocalDate acceptedOn;
}