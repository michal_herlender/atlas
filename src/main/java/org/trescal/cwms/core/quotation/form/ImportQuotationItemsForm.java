package org.trescal.cwms.core.quotation.form;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.batch.config.AutoSubmitPolicyEnum;

import lombok.Data;

@Data
public class ImportQuotationItemsForm {

	private MultipartFile file;
	private Integer subdivid;
	private Integer personid;
	private Integer addrid;
	private Integer exchangeFormatId;
	private Integer quotationId;
	private List<Integer> quoteItemsIds;
	private Integer coid;
	private AutoSubmitPolicyEnum autoSubmitPolicy;
	private List<String> errorMessage;
	private Integer bgTaskId;
	private Boolean priceAndServiceTypeFromFile;
	
}
