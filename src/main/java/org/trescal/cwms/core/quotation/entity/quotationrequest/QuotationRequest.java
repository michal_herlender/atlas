package org.trescal.cwms.core.quotation.entity.quotationrequest;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.quotation.entity.QuoteRequestType;
import org.trescal.cwms.core.quotation.entity.QuoteRequested;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "quotationrequest")
public class QuotationRequest extends Allocated<Company> implements QuoteRequested, ComponentEntity {
    private String addr1;
    private String addr2;
    private String calibrationType;
    private String clientOrderNo;
    private String clientref;
    private Company comp;
    private String company;
    private Contact con;
    private String contact;
    private String country;
    private String county;
    private QuotationRequestDetailsSource detailsSource;
    private File directory;
    private LocalDate duedate;
    private String email;
    private String fax;
    private int id;
    private Contact loggedBy;
    private Date loggedOn;
    private QuoteRequestLogSource logSource;
    private String phone;
    private String postcode;
    private Quotation quotation;
    private QuoteRequestType quoteType;
    private LocalDate reqdate;
    private String requestInfo;
    private String requestNo;
    private QuotationRequestStatus status;
    private String town;

    @Length(max = 50)
    @Column(name = "addr1", length = 50)
    public String getAddr1() {
        return this.addr1;
    }

    @Length(max = 50)
    @Column(name = "addr2", length = 50)
    public String getAddr2() {
        return this.addr2;
    }

    @Length(max = 30)
    @Column(name = "calibrationtype", length = 30)
    public String getCalibrationType() {
        return this.calibrationType;
    }

    @Column(name = "clientorderno", length = 100)
    public String getClientOrderNo() {
        return this.clientOrderNo;
	}

	@Column(name = "clientref", length = 100)
	public String getClientref()
	{
		return this.clientref;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coid")
	public Company getComp()
	{
		return this.comp;
	}

	@Length(max = 100)
	@Column(name = "company", length = 100)
	public String getCompany()
	{
		return this.company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid")
	public Contact getCon()
	{
        return this.con;
    }

    @Length(max = 100)
    @Column(name = "contact", length = 100)
    public String getContact() {
        return this.contact;
    }

    @Length(max = 50)
    @Column(name = "country", length = 50)
    public String getCountry() {
        return this.country;
    }

    @Length(max = 30)
    @Column(name = "county", length = 30)
    public String getCounty() {
        return this.county;
    }

    @Override
    @Transient
    public Contact getDefaultContact() {
		return this.con;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "detailssource", nullable = false)
	public QuotationRequestDetailsSource getDetailsSource()
	{
		return this.detailsSource;
	}

	@Override
	@Transient
	public File getDirectory()
	{
		return this.directory;
	}

    @Column(name = "duedate", columnDefinition = "date")
    public LocalDate getDuedate() {
        return this.duedate;
    }

	@Length(max = 200)
	@Column(name = "email", length = 200)
	public String getEmail()
	{
		return this.email;
	}

	@Length(max = 50)
	@Column(name = "fax", length = 50)
	public String getFax()
	{
		return this.fax;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@Override
	@Transient
	public String getIdentifier()
	{
		return this.requestNo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "loggedby", nullable = false)
	public Contact getLoggedBy()
	{
		return this.loggedBy;
	}

	@Column(name = "loggedon", nullable = false, length = 19)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLoggedOn()
	{
		return this.loggedOn;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "logsource", nullable = false)
	public QuoteRequestLogSource getLogSource()
	{
		return this.logSource;
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity()
	{
        return null;
    }

    @Length(max = 50)
    @Column(name = "phone", length = 50)
    public String getPhone() {
        return this.phone;
    }

    public void setDuedate(LocalDate duedate) {
        this.duedate = duedate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quoteid")
    public Quotation getQuotation() {
        return this.quotation;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "quotetype", nullable = false)
    public QuoteRequestType getQuoteType() {
        return this.quoteType;
    }

    @Length(max = 10)
    @Column(name = "postcode", length = 10)
    public String getPostcode() {
        return this.postcode;
    }

    @Length(max = 1000)
    @Column(name = "requestinfo", length = 1000)
    public String getRequestInfo() {
        return this.requestInfo;
    }

	@Column(name = "requestno", nullable = false, length = 20)
	public String getRequestNo()
	{
		return this.requestNo;
	}

	@Override
	@Transient
	public List<Email> getSentEmails()
	{
        return null;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    public QuotationRequestStatus getStatus() {
        return this.status;
    }

	@NotNull
	@Column(name = "reqdate", nullable = false, columnDefinition = "date")
    public LocalDate getReqdate() {
        return this.reqdate;
    }

    @Override
    @Transient
    public boolean isAccountsEmail() {
        return false;
    }

	public void setAddr1(String addr1)
	{
		this.addr1 = addr1;
	}

	public void setAddr2(String addr2)
	{
		this.addr2 = addr2;
	}

	public void setCalibrationType(String calibrationType)
	{
		this.calibrationType = calibrationType;
	}

	public void setClientOrderNo(String clientOrderNo)
	{
		this.clientOrderNo = clientOrderNo;
	}

	public void setClientref(String clientref)
	{
		this.clientref = clientref;
	}

	public void setComp(Company comp)
	{
		this.comp = comp;
	}

	public void setCompany(String company)
	{
		this.company = company;
	}

	public void setCon(Contact con)
	{
		this.con = con;
	}

	public void setContact(String contact)
	{
		this.contact = contact;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public void setCounty(String county)
	{
		this.county = county;
	}

	public void setDetailsSource(QuotationRequestDetailsSource detailsSource) {
        this.detailsSource = detailsSource;
    }

    @Override
    public void setDirectory(File file) {
        this.directory = file;
    }

    public void setReqdate(LocalDate reqdate) {
        this.reqdate = reqdate;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFax(String fax)
	{
		this.fax = fax;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setLoggedBy(Contact loggedBy)
	{
		this.loggedBy = loggedBy;
	}

	public void setLoggedOn(Date loggedOn)
	{
		this.loggedOn = loggedOn;
	}

	public void setLogSource(QuoteRequestLogSource logSource)
	{
		this.logSource = logSource;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public void setPostcode(String postcode)
	{
		this.postcode = postcode;
	}

    public void setQuotation(Quotation quotation) {
        this.quotation = quotation;
    }

    public void setQuoteType(QuoteRequestType quoteType) {
        this.quoteType = quoteType;
    }

    @Length(max = 30)
    @Column(name = "town")
    public String getTown() {
        return this.town;
    }

    public void setRequestInfo(String requestInfo) {
        this.requestInfo = requestInfo;
    }

    public void setRequestNo(String requestNo)
	{
		this.requestNo = requestNo;
	}

	@Override
	public void setSentEmails(List<Email> emails)
	{

	}

	public void setStatus(QuotationRequestStatus status)
	{
		this.status = status;
	}

	public void setTown(String town)
	{
		this.town = town;
	}
	
	@Override
	@Transient
	public boolean isQuote() {
		return false;
	}
}
