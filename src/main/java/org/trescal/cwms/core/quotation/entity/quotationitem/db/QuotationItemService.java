package org.trescal.cwms.core.quotation.entity.quotationitem.db;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.MultiValuedMap;
import org.springframework.ui.Model;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.calibration.entity.calibration.Calibration;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.entity.costs.CostLookUp;
import org.trescal.cwms.core.pricing.lookup.PriceLookupRequirements;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutputQuotationItem;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupQueryType;
import org.trescal.cwms.core.quotation.dto.DWRQuoteItemDTO;
import org.trescal.cwms.core.quotation.dto.DiscountSummaryDTO;
import org.trescal.cwms.core.quotation.dto.ModuleToBaseUnitDTO;
import org.trescal.cwms.core.quotation.dto.QuotationItemCostDTO;
import org.trescal.cwms.core.quotation.dto.QuotationItemSummaryDTO;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.comparators.QuotationItemSortType;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteitemnote.QuoteItemNote;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.quotation.form.EditQuotationItemForm;
import org.trescal.cwms.core.quotation.form.QuotationItemSearchForm;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.tools.PagedResultSet;

/**
 * Interface for accessing and manipulating {@link QuotationItem} entities.
 * 
 * @author Richard
 */
public interface QuotationItemService {
	/**
	 * Adds the {@link Quotationitem}s identified by the moduleItemNos ids array
	 * as modules of the {@link Quotationitem}s identified by the baseItemId.
	 * 
	 * @param baseItemId
	 *            the id of the {@link Quotationitem} base unit, not null.
	 * @param moduleItemNos
	 *            the id(s) of the {@link Quotationitem}s, not null.
	 * @return list of {@link Quotationitem} modules assigned to the base unit.
	 */
	Quotationitem addModules(int baseItemId, int[] moduleItemNos);

	ResultWrapper addModulesToQuotationItemBaseUnit(int qiid, ModuleToBaseUnitDTO[] dtoList, int subdivId)
			throws Exception;

	ResultWrapper ajaxDeleteQuotationItems(Integer quoteId, String listOfIds, Integer personId, Integer subDivId)
			throws Exception;

	ResultWrapper checkQuotationItemIsSafeToDelete(Quotationitem qi);

	/**
	 * Copies all of the costs from the source {@link Quotationitem} and
	 * overwrites the costs in the desintation {@link Quotationitem}s. IMPORTANT
	 * NOTE: this method does not persist these changes, nor does it update the
	 * cost totals of the {@link Quotation} as a whole. It is up to the callee
	 * to facilitate these additional processes.
	 * 
	 * @param sourceItem
	 *            the source {@link Quotationitem} to copy costs from.
	 * @param destItems
	 *            the destination {@link Quotationitem} to copy costs to.
	 */
	void copyCosts(Quotationitem sourceItem, Collection<Quotationitem> destItems);

	ResultWrapper copyQuotationItemsToHeadingAndCaltype(String copyIds, int headingId, int calTypeId, int subdivId)
			throws Exception;

	/**
	 * Creates a template basic {@link Quotationitem} bean with it's properties
	 * set to system default values. These properties can be overridden by the
	 * callee. Quotation is nullable.
	 * 
	 * @param quotation
	 *            the {@link Quotation} this item should belong to, nullable.
	 * @param inst
	 *            {@link Instrument} to be added as a quotation item
	 * @param modelid
	 *            id of the instrument model to be added as a quotation item if
	 *            inst is null
	 * @return a template instance of a {@link Quotationitem}.
	 */
	Quotationitem createTemplateNewQuotationItem(Quotation quotation, QuoteHeading heading, Instrument inst,
			Integer modelid, Integer caltypeid, Integer subdivId);

	QuoteItemNote createTemplateNewQuotationItemNote(Quotationitem item, boolean active, String label, String note,
			boolean publish, Contact contact);

	void createNotesForNewQuotation(Quotation quotation, Subdiv allocatedSubdiv, Contact currentContact);

	/**
	 * Remove the {@link Quotationitem} items as modules from their base units.
	 * 
	 * @param quoteid
	 *            the id of the {@link Quotation} that the modules belong to,
	 *            not null.
	 * @param qiids
	 *            list of ids that identify {@link Quotationitem} items to reset
	 *            to being regular units, not null.
	 * @param modelid
	 *            the id of the base unit {@link Model}
	 * @return list of {@link Quotationitem} modules no longer assigned to the
	 *         base unit.
	 */
	Set<Quotationitem> deleteByModuleAvailableGetBaseUnits(int quoteid, int[] qiids, int modelid);

	/**
	 * Delete the {@link Quotationitem} identified by the given id.
	 * 
	 * @param qiid
	 *            the id of the {@link Quotationitem}, not null.
	 */
	void deleteQuotationItem(int qiid);

	/**
	 * Delete a {@link Quotationitem}
	 * 
	 * @param qi
	 *            the {@link Quotationitem} to delete, not null.
	 */
	void deleteQuotationItem(Quotationitem qi);

	/**
	 * Delete the {@link Quotationitem}(s) identified by the array of ids.
	 * 
	 * @param qiids
	 *            ids identifying the {@link Quotationitem}(s) to delete, not
	 *            null.
	 */
	void deleteQuotationItems(int[] qiids);

	/**
	 * Update the {@link Quotationitem}(s) identified by the array of ids so
	 * that they are standalone units (i.e. not modules of a base unit).
	 * 
	 * @param qiids
	 *            ids identifying the {@link Quotationitem}(s) to set as
	 *            standalone, not null.
	 */
	void detachModule(int[] qiids);

	/**
	 * Returns a set of {@link Quotationitem}(s) that are base units and are
	 * available as a base unit for the given {@link Model} on the given
	 * {@link Quotation}.
	 * 
	 * @param quoteid
	 *            the id of the {@link Quotation} that the base units must
	 *            belong to, not null.
	 * @param modelid
	 *            the id of the {@link Model} that must be available as a module
	 *            of the base units returned, not null.
	 * @return set of available {@link Quotationitem}
	 */
	Set<Quotationitem> findAvailableBaseUnits(int quoteid, int modelid);

	/**
	 * Returns a set of {@link Quotationitem}(s) that are modules and are
	 * available as a modules for the given {@link Model} on the given
	 * {@link Quotation}.
	 * 
	 * @param quoteid
	 *            the id of the {@link Quotation} that the modules must belong
	 *            to, not null.
	 * @param modelid
	 *            the id of the {@link Model} that must be available as a base
	 *            unit of the modules returned, not null.
	 * @return set of available {@link Quotationitem}
	 */
	Set<Quotationitem> findAvailableModules(int quoteid, int modelid);

	/**
	 * Returns a set of {@link Quotationitem} belonging to the given
	 * {@link Quotation} of the given {@link InstrumentModel} type.
	 * 
	 * @param quoteid
	 *            the {@link Quotation} id.
	 * @param modelid
	 *            the {@link InstrumentModel} id.
	 * @param excludeQIId
	 *            id of a {@link Quotationitem} to optionally ignore.
	 * @return set matching {@link Quotationitem}.
	 */
	List<Quotationitem> findIdenticalModelsOnQuotation(int quoteid, int modelid, Integer excludeQIId,
			QuotationItemSortType sortType);

	/**
	 * Returns quotation items on the specified heading between the specified
	 * values
	 * 
	 * @param headingid
	 * @param minItemNo
	 * @param maxItemNo
	 * @return list sorted by itemno
	 */
	List<Quotationitem> findNearbyQuotationItems(int headingid, int minItemNo, int maxItemNo);

	/**
	 * Get the {@link Quotationitem} identified by the given id.
	 * 
	 * @param id
	 *            the id of the {@link Quotationitem}, not null.
	 * @return {@link Quotationitem}.
	 */
	Quotationitem findQuotationItem(int id);

	/**
	 * This method finds a quotation item within the specified heading and
	 * calibration type with the given item number
	 * 
	 * @param headingId
	 *            the id of the heading the item resides within
	 * @param caltypeId
	 *            the id of the calibration type the item resides within
	 * @param itemNo
	 *            the item number of the item
	 * @return {@link Quotationitem}
	 */
	Quotationitem findQuotationitemByItemNo(int headingId, int caltypeId, int itemNo);

	/**
	 * Performs query for multiple quotation items at once:
	 * 
	 * Three variations are supported: (a) Instrument : serviceTypeMap is of
	 * serviceTypeId to Sets of Instrument ids (b) Instrument Model :
	 * serviceTypeMap is of serviceTypeId to Sets of InstrumentModel ids (c)
	 * Sales Category : serviceTypeMap is of serviceTypeId to Sets of
	 * SalesCategory ids
	 * 
	 * @param reqs
	 *            general requirements for the query
	 * @param queryType
	 *            type of query (see above)
	 * @param serviceTypeMap
	 *            MultiValuedMap of entity parameters (see above)
	 * @return
	 */
	List<PriceLookupOutputQuotationItem> findQuotationItems(PriceLookupRequirements requirements,
			PriceLookupQueryType queryType, MultiValuedMap<Integer, Integer> serviceTypeMap);

	/**
	 * Eagerly load Quotationitems for display purposes
	 * 
	 * @param form
	 * @param prs
	 * @return
	 */
	PagedResultSet<Quotationitem> findQuotationItems(QuotationItemSearchForm form, PagedResultSet<Quotationitem> prs);

	/**
	 * Get a list of {@link Quotationitem}(s) identified by the list of ids.
	 * 
	 * @param qiids
	 *            list of {@link Integer} ids, not null.
	 * @return list of {@link Quotationitem}(s)
	 */
	List<Quotationitem> findQuotationItems(List<Integer> qiids);

	/**
	 * Finds quotation items related to the selected instruments, returning a
	 * map of plantIds to the related quotation items. Uses linked quotes to
	 * job.
	 * 
	 * @return
	 */
	Map<Integer, Set<Quotationitem>> findQuotationItemsForInstruments(List<Instrument> instruments, Job job,
			boolean defaultServiceTypeUsage);

    Map<Integer, Map<Integer, String>> findQuotationItemsOptionsForPlantIdsJobIdAndDefaultUsage(List<Integer> plantIds, Integer jobId, Boolean defaultServiceTypeUsage);

	Map<Integer, Map<Integer, String>> findQuotationItemsOptionsForModelIdsJobIdAndDefaultUsage(List<Integer> modelIdIds, Integer jobId, Boolean defaultServiceTypeUsage);

	/**
	 * Finds quotation items related to the selected models, returning a map of
	 * plantIds to the related quotation items. Uses linked quotes to job.
	 * 
	 * @return
	 */
	Map<Integer, Set<Quotationitem>> findQuotationItemsForModels(List<InstrumentModel> instruments, Job job,
			boolean defaultServiceTypeUsage);

	List<QuotationItemSummaryDTO> findSummaryDTOs(int quoteid, boolean groupByHeading, boolean groupByServiceType);

	/**
	 * See getAlternativeCalibrationCosts.
	 */
	ResultWrapper getAjaxAlternativeCalibrationCosts(int modelid, int calTypeId, int coid, int businessCoId,
			Integer excludeQuotationId);

	/**
	 * Loads a {@link CostLookUp} entity with different possible alternative
	 * costs for the model, calibration and company combination from a variety
	 * of different sources: other quoatations, model defaults etc
	 * 
	 * @param modelid
	 *            the id of the {@link InstrumentModel}.
	 * @param calTypeId
	 *            the id of the {@link Calibration} type.
	 * @param coid
	 *            the id of the {@link Company}.
	 * @return
	 */
	CostLookUp getAlternativeCalibrationCosts(int modelid, int calTypeId, int coid, int businessCoId,
			Integer excludeQuotationId);

	QuotationItemCostDTO getItemCostsByInstrument(Integer quotationId, Integer jobItemId, Integer serviceTypeId);

	/**
	 * Returns all {@link Quotationitem}(s) for the {@link Quotation} with the
	 * given ID.
	 * 
	 * @param quoteid
	 *            the {@link Quotation} ID.
	 * @return the {@link List} of {@link Quotationitem}(s).
	 */
	Set<Quotationitem> getItemsForQuotation(int quoteid, Integer page, Integer resPerPage);

	List<DWRQuoteItemDTO> getDWRItemsForQuotation(int quoteid);

	/**
	 * Get the maximum {@link Quotationitem} itemno on the {@link Quotation}
	 * identified by the given id.
	 * 
	 * @param quoteid
	 *            the id of the {@link Quotation}, not null.
	 * @return the maximum itemno.
	 */
	Integer getMaxItemno(int quoteid);

	/**
	 * DTO for just the general discount on the quotation items Exposed for
	 * testing purposes, use
	 * 
	 * @param quoteid
	 * @return
	 */
	DiscountSummaryDTO getGeneralDiscountDTO(int quoteid);

	/**
	 * Returns the number of items on the specified quotation (this will be
	 * different than the maximum itemno as the itemno is heading/caltype
	 * specific)
	 */
	Long getItemCount(int quoteid);

	/**
	 * Assembles a map via iteration of all existing QuoteNote with service
	 * capability labels for the specified locale (notes may be inactive or
	 * active) key = comment, value = QuoteNote
	 * 
	 * @param quotation
	 * @param locale
	 * @return
	 */
	Map<String, QuoteNote> getServiceCapabilityNoteMap(Quotation quotation, Locale locale);

	/**
	 * Assembles a translated comment regarding a matching capabilityServiceType/matching ServiceCapability , if
	 * it exists If no service capability exists, or no procedure specified,
	 * null is returned for the caller to handle
	 */
	String getServiceCapabilityComment(InstrumentModel model, CalibrationType calType, Subdiv subdiv, Locale locale);

	/**
	 * Replacement for TotalDiscountCalculator
	 * 
	 * @param quoteid
	 * @return DiscountSummaryDTO containing count and value
	 */
	DiscountSummaryDTO getTotalDiscountSummaryDTO(int quoteid);

	/**
	 * this method uses the quote item id of a base unit to fetch a list of pos
	 * 
	 * @param baseQuoteItemId
	 * @return
	 */
	ResultWrapper getModulesAvailableForBaseUnitDWR(int baseQuoteItemId, int subdivId);

	/**
	 * Inserts the given {@link Quotationitem} into the database.
	 * 
	 * @param qi
	 *            the {@link Quotationitem} to insert, not null.
	 */
	void insertQuotationItem(Quotationitem qi);

	/**
	 * Move a quotation item within a heading and a calibration type
	 * 
	 * @param oldPosId
	 *            the id of the item to be moved
	 * @param newPosId
	 *            the id of the item where the moved item is to be moved to
	 * @param headingId
	 *            id of the heading in which the item is to be moved
	 * @param calTypeId
	 *            id of the calibration type in which the item is to be moved
	 * @return {@link ResultWrapper}
	 * @throws Exception
	 */
	ResultWrapper moveQuotationItem(int oldPosId, int newPosId, int headingId, int calTypeId) throws Exception;

	/**
	 * Persists or saves the given collection of {@link Quotationitem} items.
	 * 
	 * @param items
	 */
	void saveOrUpdateAll(Collection<Quotationitem> items);

	/**
	 * Updates the list of {@link Quotationitem}(s) identified by the ids array
	 * to either be (true) or not (false) modules. This merely updates the
	 * 'partofbaseunit' status of the module and does not change what base unit
	 * a module may or may not belong to.
	 * 
	 * @param qiids
	 *            list of ids that identify {@link Quotationitem}(s)
	 * @param flag
	 *            indicates if the items are or are not modules on a
	 *            {@link Quotationitem} base unit.
	 */
	void setModuleStatus(int[] qiids, boolean flag);

	/**
	 * Sorts the {@link Quotationitem} on the given {@link Quotation} using the
	 * {@link Comparator} implementation specified by the quote's
	 * {@link Sort Type}. Sorting is done in groups of calibration types per
	 * heading, i.e. all UKAS items under one heading will be sorted and
	 * numbered from 1, then all standard calibration items under the same
	 * heading will be sorted again from 1 and so on. Note, this function does
	 * not explicitly save the results of this operation, instead it updates the
	 * {@link Quotationitem} entities belonging to the {@link Quotation}, which
	 * should then itself be saved.
	 * 
	 * @param Quotation
	 *            the {@link Quotation} to sort.
	 */
	Quotation sortQuotationItems(Quotation quotation);

	/**
	 * Performs cleanup of quotation notes after quotation items, are removed or
	 * changed
	 */
	void tidyQuoteNotes(Quotation quotation, Contact currentContact, Subdiv subdiv);

	Quotationitem updateItemCosts(Quotationitem quotationitem);

	/**
	 * Saves the given {@link Quotationitem} back to the database.
	 * 
	 * @param qi
	 *            the {@link Quotationitem} to save, not null.
	 */
	void updateQuotationItem(Quotationitem qi);

	ResultWrapper updateQuotationItemsPriceAndDiscountAndCaltyp(String copyIds, int calTypId, String price, BigDecimal discount,
			int quoteId,String username);
	
	Quotationitem editQuotationItem(Quotationitem qi, EditQuotationItemForm form, String username);
}
