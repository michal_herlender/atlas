package org.trescal.cwms.core.quotation.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentDefinitions;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.filenamingservice.FileNamingService;
import org.trescal.cwms.core.documents.filenamingservice.QuotationFileNamingService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;

@Controller
@IntranetController
@SessionAttributes(Constants.HTTPSESS_NEW_FILE_LIST)
public class QuotationBirtDocumentController {
	@Autowired
	private QuotationService quotationService;
	@Autowired
	private DocumentService documentService;

	@RequestMapping(value = "/quotebirtdocs.htm")
	public String handleRequest(Model model, Locale locale, @RequestParam(value = "id", required = true) Integer quoteId,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList) throws Exception {

		Quotation quotation = this.quotationService.get(quoteId);
		FileNamingService fnService = new QuotationFileNamingService(quotation);
		
		Map<String, Object> additionalParameters = new HashMap<>();
		additionalParameters.put(BaseDocumentDefinitions.PARAMETER_FACSIMILE, !quotation.isIssued());

		Document doc = documentService.createBirtDocument(quoteId, BaseDocumentType.QUOTATION, locale,
				Component.QUOTATION, fnService, additionalParameters);
		documentService.addFileNameToSession(doc, sessionNewFileList);
		// Prevent new file name from appearing as URL parameter after redirect
		model.asMap().clear();
		return "redirect:viewquotation.htm?id=" + quoteId + "&loadtab=quotedocs-tab";
	}
}