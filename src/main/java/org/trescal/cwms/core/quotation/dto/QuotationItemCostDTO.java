package org.trescal.cwms.core.quotation.dto;

import java.math.BigDecimal;

import org.trescal.cwms.core.pricing.entity.costtype.CostType;

public class QuotationItemCostDTO {

	private Integer itemId;
	private Integer itemNo;
	private CostType costType;
	private Integer costId;
	private BigDecimal totalCost;
	private BigDecimal discountRate;
	private BigDecimal discountValue;
	private BigDecimal finalCost;

	public QuotationItemCostDTO(Integer itemId, Integer itemNo, CostType costType, Integer costId, BigDecimal totalCost,
			BigDecimal discountRate, BigDecimal discountValue, BigDecimal finalCost) {
		super();
		this.itemId = itemId;
		this.itemNo = itemNo;
		this.costType = costType;
		this.costId = costId;
		this.totalCost = totalCost;
		this.discountRate = discountRate;
		this.discountValue = discountValue;
		this.finalCost = finalCost;
	}

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public Integer getItemNo() {
		return itemNo;
	}

	public void setItemNo(Integer itemNo) {
		this.itemNo = itemNo;
	}

	/**
	 * @return the costType
	 */
	public CostType getCostType() {
		return costType;
	}

	/**
	 * @param costType the costType to set
	 */
	public void setCostType(CostType costType) {
		this.costType = costType;
	}

	/**
	 * @return the costId
	 */
	public Integer getCostId() {
		return costId;
	}

	/**
	 * @param costId the costId to set
	 */
	public void setCostId(Integer costId) {
		this.costId = costId;
	}

	public BigDecimal getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	/**
	 * @return the discountRate
	 */
	public BigDecimal getDiscountRate() {
		return discountRate;
	}

	/**
	 * @param discountRate the discountRate to set
	 */
	public void setDiscountRate(BigDecimal discountRate) {
		this.discountRate = discountRate;
	}

	/**
	 * @return the discountValue
	 */
	public BigDecimal getDiscountValue() {
		return discountValue;
	}

	/**
	 * @param discountValue the discountValue to set
	 */
	public void setDiscountValue(BigDecimal discountValue) {
		this.discountValue = discountValue;
	}

	/**
	 * @return the finalCost
	 */
	public BigDecimal getFinalCost() {
		return finalCost;
	}

	/**
	 * @param finalCost the finalCost to set
	 */
	public void setFinalCost(BigDecimal finalCost) {
		this.finalCost = finalCost;
	}
}