package org.trescal.cwms.core.quotation.entity.priorquotestatus.db;

import java.util.Set;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.quotation.entity.priorquotestatus.PriorQuoteStatus;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;

/**
 * Interface for accessing and manipulating {@link PriorQuoteStatus} entities.
 * 
 * @author Richard
 */
public interface PriorQuoteStatusService
{
	/**
	 * Deletes the given {@link PriorQuoteStatus}.
	 * 
	 * @param pqs the {@link PriorQuoteStatus} to delete, not null.
	 */
	void deletePriorQuotationStatus(PriorQuoteStatus pqs);

	/**
	 * Gets the {@link PriorQuoteStatus} identified by the given id.
	 * 
	 * @param id the id of the {@link PriorQuoteStatus}, not null.
	 * @return {@link PriorQuoteStatus}.
	 */
	PriorQuoteStatus findPriorQuotationStatus(int id);

	/**
	 * Gets a set of all {@link PriorQuoteStatus} that belong to the
	 * {@link Quotation} identified by the given quoteid.
	 * 
	 * @param quoteid the id of the {@link Quotation}, not null.
	 * @return {@link Set} of {@link PriorQuoteStatus}.
	 */
	Set<PriorQuoteStatus> getQuotationPriorStatusList(int quoteid);

	/**
	 * Inserts the {@link PriorQuoteStatus} into the database.
	 * 
	 * @param pqs {@link PriorQuoteStatus} to insert, not null.
	 */
	void insertPriorQuotationStatus(PriorQuoteStatus pqs);

	/**
	 * Saves the given {@link PriorQuoteStatus} back to the database.
	 * 
	 * @param pqs the {@link PriorQuoteStatus} to save.
	 */
	void updatePriorQuotationStatus(PriorQuoteStatus pqs);

	/**
	 * Checks if the {@link Quotation}'s old status matches the
	 * {@link QuoteStatus} identified by the new id. If not then a new
	 * {@link PriorQuoteStatus} is created and placed into the {@link Quotation}
	 * 's prior quote status collection. Also checks if the status means that
	 * the {@link Quotation} has been issued or accepted and if so updates these
	 * fields on the {@link Quotation} accordingly. It is up to the caller to
	 * persist the quotation to force a cascade save.
	 * 
	 * @param oldStatusId id of the {@link Quotation}s old status.
	 * @param newQuoteStatusId id of the {@link Quotation}'s new
	 *        {@link QuoteStatus}. not null.
	 * @param user the {@link Contact} creating this {@link PriorQuoteStatus},
	 *        not null.
	 */
	void updateQuoteStatus(Quotation q, int oldStatusId, int newQuoteStatusId, Contact user);
}
