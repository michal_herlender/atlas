package org.trescal.cwms.core.quotation.entity.quotecaltypedefault;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SortComparator;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationTypeComparator;

@Entity
@Table(name = "quotecaltypedefault")
public class QuoteCaltypeDefault implements Comparable<QuoteCaltypeDefault>
{
	private CalibrationType caltype;
	private Quotation quotation;
	private int quotecaltypeid;

	@Override
	public int compareTo(QuoteCaltypeDefault o)
	{
		return ((Integer) this.caltype.getCalTypeId()).compareTo(o.getCaltype().getCalTypeId());
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name="caltype_caltypeid")
	@SortComparator(CalibrationTypeComparator.class)
	public CalibrationType getCaltype()
	{
		return this.caltype;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name="quotation_id")
	public Quotation getQuotation()
	{
		return this.quotation;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getQuotecaltypeid()
	{
		return this.quotecaltypeid;
	}

	public void setCaltype(CalibrationType caltype)
	{
		this.caltype = caltype;
	}

	public void setQuotation(Quotation quotation)
	{
		this.quotation = quotation;
	}

	public void setQuotecaltypeid(int quotecaltypeid)
	{
		this.quotecaltypeid = quotecaltypeid;
	}
}
