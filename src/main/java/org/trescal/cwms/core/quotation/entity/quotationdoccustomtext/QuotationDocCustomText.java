package org.trescal.cwms.core.quotation.entity.quotationdoccustomtext;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;

@Entity
@Table(name = "quotationdoccustomtext")
public class QuotationDocCustomText extends Auditable implements QuotationDocText
{
	private int id;
	private Quotation quotation;
	private String body;
	private String subject;

	/*
	 * Intentionally not bidirectional to avoid lazy loading on quotation
	 */
	@MapsId
	@OneToOne(fetch = FetchType.LAZY)
	public Quotation getQuotation()
	{
		return this.quotation;
	}

	/** 
	 * Populated with Quotation ID using MapsId, not generated ID 
	 */ 
	@Id
	@Column(name = "id")
	public int getId()
	{
		return this.id;
	}
	
	@Override
	@NotNull
	@Length(min=1, max=2000)
	@Column(name = "body", nullable = false, columnDefinition="nvarchar(2000)")
	public String getBody() {
		return body;
	}
	
	@Override
	@NotNull
	@Length(min=1, max=100)
	@Column(name = "subject", nullable = false, columnDefinition="nvarchar(100)")
	public String getSubject() {
		return subject;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
}
