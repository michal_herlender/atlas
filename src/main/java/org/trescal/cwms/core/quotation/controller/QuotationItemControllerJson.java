package org.trescal.cwms.core.quotation.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.system.Constants;

@Controller
@JsonController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME })
public class QuotationItemControllerJson {

	@Autowired
	private QuotationItemService qiSerivce;

	@RequestMapping(value = "/updateQuotationItems.json", method = RequestMethod.POST)
	@ResponseBody
	protected ResultWrapper updateQuotationItems(@RequestParam(name = "copyIds", required = false) String copyIds,
			@RequestParam(name = "calTypId", required = false) int calTypId,
			@RequestParam(name = "price", required = false) String price,
			@RequestParam(name = "discount", required = false) BigDecimal discount,
			@RequestParam(name = "quoteId", required = false) int quoteId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {

		return this.qiSerivce.updateQuotationItemsPriceAndDiscountAndCaltyp(copyIds, calTypId, price, discount, quoteId,
				username);
	}

}
