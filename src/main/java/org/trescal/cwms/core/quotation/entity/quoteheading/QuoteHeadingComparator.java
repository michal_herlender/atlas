package org.trescal.cwms.core.quotation.entity.quoteheading;

import java.util.Comparator;

/**
 * Comparator class for sorting {@link QuoteHeading} entities. System default
 * heading is always first and then sorts by heading number.
 * 
 * @author Richard
 */
public class QuoteHeadingComparator implements Comparator<QuoteHeading>
{
	@Override
	public int compare(QuoteHeading qh1, QuoteHeading qh2)
	{
		if (qh1.isSystemDefault() && !qh2.isSystemDefault())
		{
			return -1;
		}
		else if (!qh1.isSystemDefault() && qh2.isSystemDefault())
		{
			return 1;
		}
		else
		{

			if (qh1.getHeadingNo() != qh2.getHeadingNo())
			{
				return (qh1.getHeadingNo()).compareTo(qh2.getHeadingNo());
			}
			else
			{
				return ((Integer) qh1.getHeadingId()).compareTo(qh2.getHeadingId());
			}
		}
	}
}
