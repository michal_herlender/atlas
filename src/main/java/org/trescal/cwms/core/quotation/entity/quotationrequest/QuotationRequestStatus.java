package org.trescal.cwms.core.quotation.entity.quotationrequest;

public enum QuotationRequestStatus
{
	REQUESTED, LOGGED, DISCARDED;
}
