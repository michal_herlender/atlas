package org.trescal.cwms.core.quotation.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.EntityNotFoundException;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db.ServiceCapabilityService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.db.JobQuoteLinkService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.QuotationItemComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.note.db.NoteService;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;

import java.time.LocalDate;
import java.util.*;

@Service("JobToQuotationService")
@Slf4j
public class JobToQuotationServiceImpl implements JobToQuotationService {

    @Autowired
    private CalibrationTypeService calTypeServ;
    @Autowired
    private ConvertToQuotationUtils convertToQuotationUtils;
    @Autowired
    private JobItemService jobItemService;
    @Autowired
    private JobService jobService;
    @Autowired
    private QuotationItemService quotationItemService;
    @Autowired
    private QuotationService quoteServ;
    @Autowired
    protected MessageSource messageSource;
    @Autowired
    protected SubdivService subServ;
    @Autowired
    protected NoteService noteService;
    @Autowired
    protected CatalogPriceService catalogPriceService;
    @Autowired
    protected ServiceCapabilityService serviceCapabilityService;
    @Autowired
    protected SupportedCurrencyService supportedCurrencyService;
    @Autowired
    private TaxCalculator taxCalculator;
    @Autowired
    protected TranslationService translationService;
    @Autowired
    private JobQuoteLinkService jobQuoteLinkService;

    @Override
    public Quotationitem jobitemToQuotationitem(JobItem ji, Quotation quotation, int subdivId) {
        QuoteHeading heading = quotation.getQuoteheadings().iterator().next();
        Quotationitem qItem = this.quotationItemService.createTemplateNewQuotationItem(quotation, heading, ji.getInst(),
                null, ji.getServiceType().getCalibrationType().getCalTypeId(), subdivId);

        // work out the cost of the item based on it's most recent cal cost
        this.prepareCalibrationCost(ji, qItem.getCalibrationCost());
        // update item costs
        CostCalculator.updateItemCostWithRunningTotal(qItem);

        return qItem;
    }

    @Override
    public void prepareCalibrationCost(JobItem ji, QuotationCalibrationCost qCalCost) {
        this.convertToQuotationUtils.prepareCalibrationCostFromJobItem(ji, qCalCost);
    }

    @Override
    public Set<Quotationitem> prepareJobItemsToQuotationItems(Collection<JobItem> jobitems, Quotation quotation,
                                                              int subdivId) {
        // create new set of quote items
        TreeSet<Quotationitem> qItems = new TreeSet<>(new QuotationItemComparator());
        // get all calibration types currently active
        List<CalibrationType> caltypes = this.calTypeServ.getActiveCalTypes();
        // initialise item count
        int itemcount = 1;
        // add all items for each calibration type
        for (CalibrationType caltype : caltypes) {
            // check all job items
            for (JobItem ji : jobitems) {
                // caltypes match
                if (ji.getServiceType().getCalibrationType().getCalTypeId() == caltype.getCalTypeId()) {
                    Quotationitem qItem = this.jobitemToQuotationitem(ji, quotation, subdivId);
                    qItem.setItemno(itemcount);
                    itemcount++;
                    qItems.add(qItem);
                }
            }
        }
        // log items added
        log.info("Added " + qItems.size() + " quotation items");

        return qItems;
    }

    @Override
    public Set<Quotationitem> prepareJobItemsToQuotationItems(List<Integer> toQuoteJobItemIds, Quotation quotation,
                                                              int subdivId) {
        return this.prepareJobItemsToQuotationItems(this.jobItemService.getAllItems(toQuoteJobItemIds), quotation,
                subdivId);
    }

    @Override
    public int prepareAndSaveJobToQuotation(List<Integer> toQuoteJobItemIds, int jobid, Contact contact,
                                            Contact currentContact, Contact sourcedBy, LocalDate reqDate, Subdiv allocatedSubdiv, String clientRef,
                                            String currencyCode, boolean linkToJob) throws EntityNotFoundException {
        log.info("Preparing quotation from job " + jobid);

        // load job
        Job job = this.jobService.get(jobid);
        // job null?
        if (job == null) {
            throw new EntityNotFoundException();
        }
        // create new quotation
        Locale locale = LocaleContextHolder.getLocale();
        Quotation quotation = this.quoteServ.createTemplateNewQuotation(currentContact, contact,
                allocatedSubdiv, allocatedSubdiv.getComp(), locale);

        this.supportedCurrencyService.setCurrencyFromForm(quotation, currencyCode,
                quotation.getContact().getSub().getComp());
        quotation.setClientref(clientRef);
        quotation.setRegdate(reqDate);
        quotation.setCreatedBy(currentContact);
        quotation.setSourcedBy(sourcedBy);

        // convert jobitems to quotation items
        quotation.setQuotationitems(
                this.prepareJobItemsToQuotationItems(toQuoteJobItemIds, quotation, allocatedSubdiv.getSubdivid()));

        CostCalculator.updatePricingTotalCost(quotation, quotation.getQuotationitems());
        taxCalculator.calculateAndSetVATaxAndFinalCost(quotation);
        // Now that quotation items are added, create the notes for this
        // quotation
        this.quotationItemService.createNotesForNewQuotation(quotation, allocatedSubdiv, currentContact);

        QuoteNote qn = new QuoteNote();
        qn.setActive(true);
        qn.setPublish(false);
        qn.setNote(messageSource.getMessage("createquot.createdfromjob", new Object[]{job.getJobno()},
                "Created from job " + job.getJobno(), locale));
        qn.setLabel("");
        qn.setSetBy(currentContact);
        qn.setSetOn(new Date());
        qn.setQuotation(quotation);
        quotation.getNotes().add(qn);
        this.quoteServ.save(quotation);
        int quotationId = quotation.getId();

        // link to Job
        if (linkToJob)
            jobQuoteLinkService.linkJobToQuote(quotation, job, currentContact);

        log.info("Saved new quotation with id " + quotationId);
        return quotationId;
    }
}