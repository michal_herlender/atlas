package org.trescal.cwms.core.quotation.form;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequestDetailsSource;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import javax.validation.ConstraintViolation;
import java.util.Set;

@Component
public class QuotationRequestValidator extends AbstractBeanValidator {
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(QuotationRequestForm.class);
	}
	
	@Override
	public void validate(Object target, Errors errors)
	{
		super.validate(target, errors);
		QuotationRequestForm form = (QuotationRequestForm) target;
		if (form.getRequest().getDetailsSource().equals(QuotationRequestDetailsSource.EXISTING))
		{
			if (form.getPersonid() == null)
			{
				errors.rejectValue("personid", null, "Please select a contact");
			}
			if (form.getCoid() == null)
			{
				errors.rejectValue("coid", null, "Please select a company");
			}
			Set<ConstraintViolation<QuotationRequest>> viols = super.getConstraintViolations(form.getRequest());
			for (ConstraintViolation<QuotationRequest> viol : viols)
				if (viol.getPropertyPath().equals("requestInfo") || viol.getPropertyPath().equals("reqdate"))
					errors.rejectValue("request." + viol.getPropertyPath(), null, viol.getMessage());
		}
		else
		{
			// company & contact are mandatory in this case
			if ((form.getRequest().getCompany() == null)
					|| form.getRequest().getCompany().trim().equals(""))
			{
				errors.rejectValue("request.company", null, "Please provide a company");
			}
			if ((form.getRequest().getContact() == null)
					|| form.getRequest().getContact().trim().equals(""))
			{
				errors.rejectValue("request.contact", null, "Please provide a contact");
			}
			Set<ConstraintViolation<QuotationRequest>> viols = super.getConstraintViolations(form.getRequest());
			for (ConstraintViolation<QuotationRequest> viol : viols)
				if (viol.getPropertyPath().equals("requestInfo") || viol.getPropertyPath().equals("reqdate") ||
						viol.getPropertyPath().equals("phone") || viol.getPropertyPath().equals("email") ||
						viol.getPropertyPath().equals("fax"))
					errors.rejectValue("request." + viol.getPropertyPath(), null, viol.getMessage());
			// make sure that one of phone/email/fax is present
			if (StringUtils.isEmpty(form.getRequest().getPhone())
					&& StringUtils.isEmpty(form.getRequest().getEmail())
					&& StringUtils.isEmpty(form.getRequest().getFax()))
			{
				errors.rejectValue("request.phone", null, "Please provide one of a phone/fax/email");
			}
		}
	}
}