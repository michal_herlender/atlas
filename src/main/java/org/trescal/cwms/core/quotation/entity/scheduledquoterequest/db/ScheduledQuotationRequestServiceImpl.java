package org.trescal.cwms.core.quotation.entity.scheduledquoterequest.db;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequest;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequestDateRange;
import org.trescal.cwms.core.quotation.enums.ScheduledQuoteRequestType;
import org.trescal.cwms.core.quotation.form.ScheduledQuotationRequestForm;
import org.trescal.cwms.core.quotation.service.ScheduledQuoteRequestToQuotationService;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;

@Service("ScheduledQuotationRequestService")
public class ScheduledQuotationRequestServiceImpl extends BaseServiceImpl<ScheduledQuotationRequest, Integer> implements ScheduledQuotationRequestService
{
	@Autowired
	private ScheduledQuotationRequestDao baseDao;
	@Autowired
	private ScheduledQuoteRequestToQuotationService scheduledQuoteRequestToQuotationService;
	@Autowired
	private ScheduledTaskService schTaskServ;
	@Autowired
	private UserService userService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private CalibrationTypeService calibrationTypeService;
	@Autowired
	private SubdivService subdivService;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public List<ScheduledQuotationRequest> getOutstanding(Subdiv allocatedSubdiv)
	{
		return this.baseDao.getOutstanding(allocatedSubdiv);
	}

	@Override
	public List<ScheduledQuotationRequest> getTopXMostRecentlyProcessed(Subdiv allocatedSubdiv, int x)
	{
		return this.baseDao.getTopXMostRecentlyProcessed(allocatedSubdiv, x);
	}

	@Override
	protected BaseDao<ScheduledQuotationRequest, Integer> getBaseDao() {
		return baseDao;
	}
	
	@Override
	public List<ScheduledQuotationRequest> scheduledQuotationRequestFromQuotation(Integer quoteId) {
		return this.baseDao.scheduledQuotationRequestFromQuotation(quoteId);
	}
	
	@Override
	public void createScheduledQuotationRequest(ScheduledQuotationRequestForm form, 
			String username, Integer subdivid){
		ScheduledQuotationRequest sqr = new ScheduledQuotationRequest();
		sqr.setQuoteContact(this.contactService.get(form.getContactid()));
		sqr.setRequestBy(this.userService.get(username).getCon());
		sqr.setRequestOn(new Date());
		sqr.setRequestType(form.getType());
		sqr.setScope(form.getScope());
		sqr.setStrategy(form.getStrategy());
		sqr.setDefaultCalType(this.calibrationTypeService.find(form.getCalTypeId()));
		sqr.setOrganisation(this.subdivService.get(subdivid));
		if (form.getType().equals(ScheduledQuoteRequestType.DATE_RANGE_INSTRUMENTS)) {
			ScheduledQuotationRequestDateRange dateRange = new ScheduledQuotationRequestDateRange();
			dateRange.setFinishDate(form.getFinishDate());
			dateRange.setIncludeEmptyDates(form.getIncludeEmptyDates());
			dateRange.setRequest(sqr);
			dateRange.setStartDate(form.getStartDate());
			sqr.setDateRange(dateRange);
		}
		sqr.setLocale(Locale.forLanguageTag(form.getLanguageTag()));
		this.save(sqr);
	}
}