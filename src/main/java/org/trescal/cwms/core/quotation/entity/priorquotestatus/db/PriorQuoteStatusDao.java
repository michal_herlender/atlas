package org.trescal.cwms.core.quotation.entity.priorquotestatus.db;

import java.util.Set;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.quotation.entity.priorquotestatus.PriorQuoteStatus;


public interface PriorQuoteStatusDao extends BaseDao<PriorQuoteStatus, Integer> {
	
	Set<PriorQuoteStatus> getQuotationPriorStatusList(int quoteid);
}