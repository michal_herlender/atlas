/**
 * 
 */
package org.trescal.cwms.core.quotation.form;

import java.util.List;

import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost.TPQuotationCalibrationCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.purchasecost.TPQuotationPurchaseCost;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditQuotationItemForm {
	/**
	 * The id of a linked {@link TPQuotationCalibrationCost}.
	 */
	private Integer calibrationCostId;
	private Integer serviceTypeId;
	private List<Integer> copyIds;

	private Integer headingId;
	private String plantNo;
	/**
	 * The id of a linked {@link TPQuotationPurchaseCost}.
	 */
	private Integer purchaseCostId;
	private Quotationitem quotationitem;
	private Boolean editable;
	private Integer allocatedSubdivId;

}
