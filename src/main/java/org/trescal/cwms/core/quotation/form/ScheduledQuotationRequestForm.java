package org.trescal.cwms.core.quotation.form;

import org.hibernate.validator.constraints.NotEmpty;
import org.trescal.cwms.core.quotation.enums.QuoteGenerationStrategy;
import org.trescal.cwms.core.quotation.enums.ScheduledQuoteRequestScope;
import org.trescal.cwms.core.quotation.enums.ScheduledQuoteRequestType;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter @Setter
public class ScheduledQuotationRequestForm {
	
	@NotNull(message = "{error.value.notselected}")
    private Integer contactid;
    private Integer subdivid;
    private Integer coid;
    @NotNull(message = "{error.value.notselected}")
    private ScheduledQuoteRequestScope scope;
    @NotNull(message = "{error.value.notselected}")
    private ScheduledQuoteRequestType type;
    @NotNull(message = "{error.value.notselected}")
    private QuoteGenerationStrategy strategy;
    @NotNull(message = "{error.value.notselected}")
    private Integer calTypeId;
    private LocalDate startDate;
    private LocalDate finishDate;
    private Boolean includeEmptyDates;
    @NotEmpty(message = "{error.value.notselected}")
    private String languageTag;

}
