package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.db;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.DefaultCalibrationCondition;

public interface DefaultCalibrationConditionDao extends AllocatedDao<Company, DefaultCalibrationCondition, Integer>
{
	DefaultCalibrationCondition getLatest(int type, Company company);
}