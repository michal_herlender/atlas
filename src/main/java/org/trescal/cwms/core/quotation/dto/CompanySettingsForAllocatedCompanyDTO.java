package org.trescal.cwms.core.quotation.dto;

public class CompanySettingsForAllocatedCompanyDTO {

	private Integer allocatedcompanyid;
	private Integer companyid;
	private Boolean active;
	private Boolean onstop;

	public CompanySettingsForAllocatedCompanyDTO(Integer allocatedcompanyid, Integer companyid, Boolean active, Boolean onstop) {
		super();
		this.allocatedcompanyid = allocatedcompanyid;
		this.companyid = companyid;
		this.active = active;
		this.onstop = onstop;
	}

	public Integer getAllocatedcompanyid() {
		return allocatedcompanyid;
	}

	public void setAllocatedcompanyid(Integer allocatedcompanyid) {
		this.allocatedcompanyid = allocatedcompanyid;
	}

	public Integer getCompanyid() {
		return companyid;
	}

	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}

	public Boolean getActive() {
		return active;
	}

	public Boolean getOnstop() {
		return onstop;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public void setOnstop(Boolean onstop) {
		this.onstop = onstop;
	}
}
