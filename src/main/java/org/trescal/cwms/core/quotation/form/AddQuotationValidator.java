package org.trescal.cwms.core.quotation.form;

import lombok.val;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import java.time.LocalDate;

@Component
public class AddQuotationValidator extends AbstractBeanValidator
{
	protected final Log logger = LogFactory.getLog(this.getClass());

	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(AddQuotationForm.class);
	}

	public void validate(Object obj, Errors errors)
	{
		AddQuotationForm quoteData = (AddQuotationForm) obj;
		super.validate(quoteData, errors);

		if (quoteData.getReqDate() != null) {
			// check that the date is a real date and not in the future
			val reqDate = quoteData.getReqDate();
			val now = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
			if (now.isBefore(reqDate)) {
				errors.rejectValue("reqDate", "error.quotation.reqdate.future", null, "Request date cannot be in the future");
			}
		}

		if (quoteData.getClientRef().length() > 100)
		{
			errors.rejectValue("clientRef", null, "Client reference should be no more than 100 characters");
		}
	}
}
