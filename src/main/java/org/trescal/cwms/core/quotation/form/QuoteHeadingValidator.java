package org.trescal.cwms.core.quotation.form;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class QuoteHeadingValidator extends AbstractBeanValidator
{
protected final Log logger = LogFactory.getLog(getClass());
	
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(QuoteHeadingForm.class);
	}

	public void validate(Object target, Errors errors)
	{
		super.validate(target, errors);
		QuoteHeadingForm headingData = (QuoteHeadingForm)target;

		if (headingData == null)
		{
			errors.reject("error.nullpointer", "Null data received");
		}
		else if(headingData.getFormfunction() == null)
		{
			errors.reject("error.nullpointer", "Null data received");
		}
		else if(headingData.getFormfunction().equalsIgnoreCase("newheading") || headingData.getFormfunction().equalsIgnoreCase("editquoteheading"))
		{
			String headingName = headingData.getHeader().getHeadingName();
			String headingDesc = headingData.getHeader().getHeadingDescription();
			
			// TODO - these could/should be changed to validation groups
			if(headingName == null || headingName.trim().equals(""))
			{
				// blank headers are allowed if they are system defaults
				if(!headingData.getHeader().isSystemDefault())
				{
					errors.rejectValue("header.headingName", "error.quotation.heading.headingName", null, "Please enter a heading name");
				}
			}
			else if(headingName.length() > 50)
			{
				errors.rejectValue("header.headingName", "error.quotation.heading.headingName.length", null, "The heading name was too long");
			}
			
			if(headingDesc != null)
			{
				if(headingDesc.length() > 200)
				{
					errors.rejectValue("header.headingDescription", "error.quotation.heading.headingDescription.length", null, "The heading description was too long");
				}
			}
		}
	}
}