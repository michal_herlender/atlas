package org.trescal.cwms.core.quotation.entity.quotation.db;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.DomainType;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelWithPriceDTO;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink_;
import org.trescal.cwms.core.quotation.dto.QuotationKeyValue;
import org.trescal.cwms.core.quotation.dto.QuotationProjectionDTO;
import org.trescal.cwms.core.quotation.dto.QuotationSearchResultWrapper;
import org.trescal.cwms.core.quotation.dto.QuoteItemOutputDto;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.QuotationMetricFormatter;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation_;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem_;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus_;
import org.trescal.cwms.core.quotation.entity.quotestatusgroup.QuoteStatusGroup;
import org.trescal.cwms.core.quotation.entity.quotestatusgroupmember.QuoteStatusGroupMember;
import org.trescal.cwms.core.quotation.entity.quotestatusgroupmember.QuoteStatusGroupMember_;
import org.trescal.cwms.core.quotation.form.NewQuotationSearchForm;
import org.trescal.cwms.core.quotation.form.QuotationMetricForm;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.vavr.Function2;
import lombok.val;

@Repository("QuotationDao")
public class QuotationDaoImpl extends AllocatedToCompanyDaoImpl<Quotation, Integer> implements QuotationDao {
	@Override
	protected Class<Quotation> getEntity() {
		return Quotation.class;
	}

	@Override
	public Long countByCompanyAndAllocatedCompany(Company company, Company allocatedCompany) {
		return countActualByCompanyAndAllocatedCompany(null, company, allocatedCompany);
	}

	/*
	 * Supports null as a parameter for yearsBack in which case expression is
	 * not added, avoiding duplicate code for
	 * countByCompanyAndAllocatedCompany() above
	 */
	@Override
	public Long countActualByCompanyAndAllocatedCompany(Integer yearsBack, Company company, Company allocatedCompany) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Quotation> root = cq.from(Quotation.class);
		cq.select(cb.count(root));

		Join<Contact, Subdiv> joinClientSubdiv = root.join(Quotation_.contact).join(Contact_.sub);

		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(root.get(Quotation_.organisation.getName()), allocatedCompany));
		clauses.getExpressions().add(cb.equal(joinClientSubdiv.get(Subdiv_.comp), company));
		if (yearsBack != null) {
			val past = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).minusYears(yearsBack);
			clauses.getExpressions().add(cb.greaterThan(root.get(Quotation_.regdate), past));
		}
		cq.where(clauses);

		return getEntityManager().createQuery(cq).getSingleResult();
	}

	public Long countByContactAndAllocatedSubdiv(Contact contact, Subdiv allocatedSubdiv) {
		return getCount(cb -> cq -> {
			val quotation = cq.from(Quotation.class);
			cq.where(cb.and(cb.equal(quotation.get(Quotation_.organisation), allocatedSubdiv.getComp()),
					cb.equal(quotation.get(Quotation_.contact), contact)));
			return Triple.of(quotation, quotation, Collections.emptyList());
		}).longValue();
	}

	public Long countActualByContactAndAllocatedSubdiv(Integer yearsBack, Contact contact, Subdiv allocatedSubdiv) {
		return getCount(cb -> cq -> {
			val quotation = cq.from(Quotation.class);
			val yearsAgo = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).minusYears(yearsBack);
			cq.where(cb.and(cb.equal(quotation.get(Quotation_.organisation), allocatedSubdiv.getComp()),
					cb.equal(quotation.get(Quotation_.contact), contact),
					cb.greaterThan(quotation.get(Quotation_.regdate), yearsAgo)));
			return Triple.of(quotation, quotation, Collections.emptyList());
		}).longValue();
	}

	@Override
	public Long countBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv) {
		return getCount(cb -> cq -> {
			val quotation = cq.from(Quotation.class);
			cq.where(cb.and(cb.equal(quotation.get(Quotation_.organisation), allocatedSubdiv.getComp()),
					cb.equal(quotation.get(Quotation_.contact).get(Contact_.sub), subdiv)));
			return Triple.of(quotation, quotation, Collections.emptyList());
		}).longValue();
	}

	@Override
	public Long countActualBySubdivAndAllocatedSubdiv(Integer yearsBack, Subdiv subdiv, Subdiv allocatedSubdiv) {
		return getCount(cb -> cq -> {
			val quotation = cq.from(Quotation.class);
			val yearsAgo = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).minusYears(yearsBack);
			cq.where(cb.and(cb.equal(quotation.get(Quotation_.organisation), allocatedSubdiv.getComp()),
					cb.equal(quotation.get(Quotation_.contact).get(Contact_.sub), subdiv),
					cb.greaterThan(quotation.get(Quotation_.regdate), yearsAgo)));
			return Triple.of(quotation, quotation, Collections.emptyList());
		}).longValue();
	}

	/**
	 * Used by DWR in tooltips only, not exposed in service layer
	 */
	@Override
	public Quotation findEagerQuotation(int id) {
		return getSingleResult(cb -> {
			val cq = cb.createQuery(Quotation.class);
			val quotation = cq.from(Quotation.class);
			cq.where(cb.equal(quotation.get(Quotation_.id), id));
			quotation.fetch(Quotation_.contact, JoinType.INNER).fetch(Contact_.sub, JoinType.INNER).fetch(Subdiv_.comp,
					JoinType.INNER);
			quotation.fetch(Quotation_.currency);
			quotation.fetch(Quotation_.sourcedBy);
			quotation.fetch(Quotation_.quotestatus);
			return cq;
		});
	}

	@Override
	public Integer findMaxQuotationVersion(String qno) {
		return getSingleResult(cb -> {
			CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
			Root<Quotation> quote = cq.from(Quotation.class);
			cq.where(cb.equal(quote.get(Quotation_.qno), qno));
			cq.select(cb.max(quote.get(Quotation_.ver)));
			return cq;
		});
	}

	@Override
	public List<Quotation> findQuotations(String qno) {
		return getResultList(cb -> {
			CriteriaQuery<Quotation> cq = cb.createQuery(Quotation.class);
			Root<Quotation> quote = cq.from(Quotation.class);
			cq.where(cb.equal(quote.get(Quotation_.qno), qno));
			cq.orderBy(cb.asc(quote.get(Quotation_.ver)));
			return cq;
		});
	}

	@Override
	public List<QuotationKeyValue> findLinkedQuotationsWithServiceModels(Job job) {
		return getResultList(cb -> {
			CriteriaQuery<QuotationKeyValue> cq = cb.createQuery(QuotationKeyValue.class);
			Root<Quotation> quote = cq.from(Quotation.class);
			Join<Quotation, JobQuoteLink> linkedJobs = quote.join(Quotation_.linkedJobs);
			linkedJobs.on(cb.equal(linkedJobs.get(JobQuoteLink_.job), job));
			Join<Quotation, Quotationitem> item = quote.join(Quotation_.quotationitems);
			Join<Quotationitem, InstrumentModel> model = item.join(Quotationitem_.model);
			Join<InstrumentModel, Description> subFamily = model.join(InstrumentModel_.description);
			Join<Description, InstrumentModelFamily> family = subFamily.join(Description_.family);
			Join<InstrumentModelFamily, InstrumentModelDomain> domain = family.join(InstrumentModelFamily_.domain);
			cq.where(cb.equal(domain.get(InstrumentModelDomain_.domainType), DomainType.SERVICE));
			cq.distinct(true);
			cq.select(cb.construct(QuotationKeyValue.class, quote.get(Quotation_.id), quote.get(Quotation_.qno)));
			return cq;
		});
	}

	@Override
	public List<InstrumentModelWithPriceDTO> findServiceModelsOnLinkedQuotations(Integer jobId) {
		return getResultList(cb -> {
			CriteriaQuery<InstrumentModelWithPriceDTO> cq = cb.createQuery(InstrumentModelWithPriceDTO.class);
			Root<Quotation> quote = cq.from(Quotation.class);
			Join<Quotation, JobQuoteLink> linkedJobs = quote.join(Quotation_.linkedJobs);
			Join<JobQuoteLink, Job> job = linkedJobs.join(JobQuoteLink_.job);
			job.on(cb.equal(job.get(Job_.jobid), jobId));
			Join<Quotation, Quotationitem> item = quote.join(Quotation_.quotationitems);
			Join<Quotationitem, InstrumentModel> model = item.join(Quotationitem_.model);
			Join<InstrumentModel, Translation> modelName = model.join(InstrumentModel_.nameTranslations,
					javax.persistence.criteria.JoinType.LEFT);
			modelName.on(cb.equal(modelName.get(Translation_.locale), LocaleContextHolder.getLocale()));
			Join<InstrumentModel, Description> subFamily = model.join(InstrumentModel_.description);
			Join<Description, InstrumentModelFamily> family = subFamily.join(Description_.family);
			Join<InstrumentModelFamily, InstrumentModelDomain> domain = family.join(InstrumentModelFamily_.domain);
			cq.where(cb.equal(domain.get(InstrumentModelDomain_.domainType), DomainType.SERVICE));
			cq.select(cb.construct(InstrumentModelWithPriceDTO.class, model.get(InstrumentModel_.modelid),
					modelName.get(Translation_.translation), item.get(Quotationitem_.finalCost),
					item.get(Quotationitem_.plantno)));
			return cq;
		});
	}

	@Override
	public List<InstrumentModelWithPriceDTO> findServiceModelsOnQuotation(Integer quoteId) {
		return getResultList(cb -> {
			CriteriaQuery<InstrumentModelWithPriceDTO> cq = cb.createQuery(InstrumentModelWithPriceDTO.class);
			Root<Quotation> quote = cq.from(Quotation.class);
			Join<Quotation, Quotationitem> item = quote.join(Quotation_.quotationitems);
			Join<Quotationitem, InstrumentModel> model = item.join(Quotationitem_.model);
			Join<InstrumentModel, Translation> modelName = model.join(InstrumentModel_.nameTranslations,
					javax.persistence.criteria.JoinType.LEFT);
			modelName.on(cb.equal(modelName.get(Translation_.locale), LocaleContextHolder.getLocale()));
			Join<InstrumentModel, Description> subFamily = model.join(InstrumentModel_.description);
			Join<Description, InstrumentModelFamily> family = subFamily.join(Description_.family);
			Join<InstrumentModelFamily, InstrumentModelDomain> domain = family.join(InstrumentModelFamily_.domain);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(quote.get(Quotation_.id), quoteId));
			clauses.getExpressions().add(cb.equal(domain.get(InstrumentModelDomain_.domainType), DomainType.SERVICE));
			cq.where(clauses);
			cq.select(cb.construct(InstrumentModelWithPriceDTO.class, model.get(InstrumentModel_.modelid),
					modelName.get(Translation_.translation), item.get(Quotationitem_.finalCost),
					item.get(Quotationitem_.plantno)));
			return cq;
		});
	}

	@Override
	public List<QuotationProjectionDTO> getAllQuotesInStatusGroupNew(QuoteStatusGroup group, Integer allocatedSubdivId,
			Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<QuotationProjectionDTO> cq = cb.createQuery(QuotationProjectionDTO.class);
			Root<Quotation> quote = cq.from(Quotation.class);
			quote.alias("quote");
			Join<Quotation, Company> companyJoin = quote.join("organisation");
			companyJoin.alias("companyJoin");
			Join<Company, Subdiv> subdivJoin = companyJoin.join(Company_.subdivisions);
			subdivJoin.alias("subdivJoin");
			Join<Quotation, QuoteStatus> quotStatuJoin = quote.join(Quotation_.quotestatus);
			quotStatuJoin.alias("quotStatuJoin");
			Join<QuoteStatus, QuoteStatusGroupMember> quoteStatusGroupMemberJoin = quotStatuJoin
					.join(QuoteStatus_.statusGroups);
			quoteStatusGroupMemberJoin.alias("quoteStatusGroupMemberJoin");
			Join<QuoteStatus, Translation> quoteStatusTranslationJoin = quotStatuJoin
					.join(QuoteStatus_.nametranslations);
			quoteStatusTranslationJoin.alias("quoteStatusTranslationJoin");

			Join<Quotation, Contact> contactContactJoin = quote.join(Quotation_.contact);
			contactContactJoin.alias("contactContactJoin");
			Join<Contact, Subdiv> contactSubdivJoin = contactContactJoin.join(Contact_.sub);
			contactSubdivJoin.alias("contactSubdivJoin");
			Join<Subdiv, Company> contactCompanyJoin = contactSubdivJoin.join(Subdiv_.comp);
			contactCompanyJoin.alias("contactCompanyJoin");

			Join<Quotation, Contact> sourcedContactJoin = quote.join(Quotation_.sourcedBy,
					javax.persistence.criteria.JoinType.LEFT);
			sourcedContactJoin.alias("sourcedContactJoin");
			Join<Contact, Subdiv> sourcedSubdivJoin = sourcedContactJoin.join(Contact_.sub,
					javax.persistence.criteria.JoinType.LEFT);
			sourcedSubdivJoin.alias("sourcedSubdivJoin");
			Join<Subdiv, Company> sourcedCompanyJoin = sourcedSubdivJoin.join(Subdiv_.comp,
					javax.persistence.criteria.JoinType.LEFT);
			sourcedCompanyJoin.alias("sourcedCompanyJoin");

			Join<Quotation, Contact> issuebyContactJoin = quote.join(Quotation_.issueby,
					javax.persistence.criteria.JoinType.LEFT);
			issuebyContactJoin.alias("issuebyContactJoin");
			Join<Contact, Subdiv> issuebySubdivJoin = issuebyContactJoin.join(Contact_.sub,
					javax.persistence.criteria.JoinType.LEFT);
			issuebySubdivJoin.alias("issuebySubdivJoin");
			Join<Subdiv, Company> issuebyCompanyJoin = issuebySubdivJoin.join(Subdiv_.comp,
					javax.persistence.criteria.JoinType.LEFT);
			issuebyCompanyJoin.alias("issuebyCompanyJoin");

			Predicate and1 = cb.and(
					cb.equal(quoteStatusGroupMemberJoin.get(QuoteStatusGroupMember_.groupId), group.getId()),
					cb.equal(subdivJoin.get(Subdiv_.subdivid), allocatedSubdivId));
			Predicate and2 = cb.and(
					cb.or(cb.isNull(quote.get(Quotation_.duedate)), cb.isNotNull(quote.get(Quotation_.duedate))),
					cb.equal(quoteStatusTranslationJoin.get(Translation_.locale), locale));
			cq.where(cb.and(and1, and2));
			Calendar maxDate = Calendar.getInstance();
			maxDate.add(Calendar.YEAR, 1);
			cq.orderBy(cb.asc(cb.coalesce(quote.get(Quotation_.duedate), maxDate.getTime())),
					cb.desc(quote.get(Quotation_.regdate)));

			cq.select(cb.construct(QuotationProjectionDTO.class, quote.get(Quotation_.id), quote.get(Quotation_.qno),
					quote.get(Quotation_.ver), quoteStatusTranslationJoin.get(Translation_.translation),
					quote.get(Quotation_.clientref), quote.get(Quotation_.issuedate),

					sourcedContactJoin.get(Contact_.personid), sourcedContactJoin.get(Contact_.firstName),
					sourcedContactJoin.get(Contact_.lastName), sourcedContactJoin.get(Contact_.email),
					sourcedContactJoin.get(Contact_.telephone), sourcedContactJoin.get(Contact_.active),
					sourcedSubdivJoin.get(Subdiv_.subdivid), sourcedSubdivJoin.get(Subdiv_.subname),
					sourcedSubdivJoin.get(Subdiv_.subdivCode), sourcedSubdivJoin.get(Subdiv_.active),
					sourcedCompanyJoin.get(Company_.coid), sourcedCompanyJoin.get(Company_.coname),

					contactContactJoin.get(Contact_.personid), contactContactJoin.get(Contact_.firstName),
					contactContactJoin.get(Contact_.lastName), contactContactJoin.get(Contact_.email),
					contactContactJoin.get(Contact_.telephone), contactContactJoin.get(Contact_.active),
					contactSubdivJoin.get(Subdiv_.subdivid), contactSubdivJoin.get(Subdiv_.subname),
					contactSubdivJoin.get(Subdiv_.subdivCode), contactSubdivJoin.get(Subdiv_.active),
					contactCompanyJoin.get(Company_.coid), contactCompanyJoin.get(Company_.coname),

					issuebyContactJoin.get(Contact_.personid), issuebyContactJoin.get(Contact_.firstName),
					issuebyContactJoin.get(Contact_.lastName), issuebyContactJoin.get(Contact_.email),
					issuebyContactJoin.get(Contact_.telephone), issuebyContactJoin.get(Contact_.active),
					issuebySubdivJoin.get(Subdiv_.subdivid), issuebySubdivJoin.get(Subdiv_.subname),
					issuebySubdivJoin.get(Subdiv_.subdivCode), issuebySubdivJoin.get(Subdiv_.active),
					issuebyCompanyJoin.get(Company_.coid), issuebyCompanyJoin.get(Company_.coname),

					companyJoin.get(Company_.coid), quote.get(Quotation_.finalCost), quote.get(Quotation_.totalCost), quote.get(Quotation_.duedate),
					quote.get(Quotation_.regdate),quote.get(Quotation_.expiryDate),issuebySubdivJoin.get(Subdiv_.subdivCode)));
			return cq;
		});
	}

	@Override
	public Set<CalibrationType> getCaltypesPerQuotation(int quoteid) {
		return new HashSet<>(getResultList(cb -> {
			val cq = cb.createQuery(CalibrationType.class);
			val calType = cq.from(CalibrationType.class);
			cq.where(cb.equal(calType.join(CalibrationType_.quoteItems).get(Quotationitem_.quotation), quoteid));
			return cq;
		}));
	}

	@Override
	public List<Quotation> getIssuedQuotationsForContactFromIssuedate(int personId, LocalDate issuedate, boolean accepted) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Quotation.class);
			val quotation = cq.from(Quotation.class);
			cq.where(cb.equal(quotation.get(Quotation_.issuedate), issuedate),
					cb.equal(quotation.get(Quotation_.accepted), accepted),
					cb.equal(quotation.get(Quotation_.contact), personId));
			cq.orderBy(cb.desc(quotation.get(Quotation_.qno)));
			return cq;
		});
	}

	@Override
	public List<QuotationProjectionDTO> getMostRecentIssuedQuotationsNew(int resultSize, Integer allocatedSubdivId,
			Locale locale) {
		return getResultList(cb -> {
			CriteriaQuery<QuotationProjectionDTO> cq = cb.createQuery(QuotationProjectionDTO.class);
			Root<Quotation> quote = cq.from(Quotation.class);
			quote.alias("quote");
			Join<Quotation, Company> companyJoin = quote.join("organisation");
			companyJoin.alias("companyJoin");
			Join<Company, Subdiv> subdivJoin = companyJoin.join(Company_.subdivisions);
			subdivJoin.alias("subdivJoin");
			Join<Quotation, QuoteStatus> quotStatuJoin = quote.join(Quotation_.quotestatus);
			quotStatuJoin.alias("quotStatuJoin");
			Join<QuoteStatus, Translation> quoteStatusTranslationJoin = quotStatuJoin
					.join(QuoteStatus_.nametranslations);
			quoteStatusTranslationJoin.alias("quoteStatusTranslationJoin");

			Join<Quotation, Contact> contactContactJoin = quote.join(Quotation_.contact);
			contactContactJoin.alias("contactContactJoin");
			Join<Contact, Subdiv> contactSubdivJoin = contactContactJoin.join(Contact_.sub);
			contactSubdivJoin.alias("contactSubdivJoin");
			Join<Subdiv, Company> contactCompanyJoin = contactSubdivJoin.join(Subdiv_.comp);
			contactCompanyJoin.alias("contactCompanyJoin");

			Join<Quotation, Contact> sourcedContactJoin = quote.join(Quotation_.sourcedBy);
			sourcedContactJoin.alias("sourcedContactJoin");
			Join<Contact, Subdiv> sourcedSubdivJoin = sourcedContactJoin.join(Contact_.sub);
			sourcedSubdivJoin.alias("sourcedSubdivJoin");
			Join<Subdiv, Company> sourcedCompanyJoin = sourcedSubdivJoin.join(Subdiv_.comp);
			sourcedCompanyJoin.alias("sourcedCompanyJoin");

			Join<Quotation, Contact> issuebyContactJoin = quote.join(Quotation_.issueby,
					javax.persistence.criteria.JoinType.LEFT);
			issuebyContactJoin.alias("issuebyContactJoin");
			Join<Contact, Subdiv> issuebySubdivJoin = issuebyContactJoin.join(Contact_.sub,
					javax.persistence.criteria.JoinType.LEFT);
			issuebySubdivJoin.alias("issuebySubdivJoin");
			Join<Subdiv, Company> issuebyCompanyJoin = issuebySubdivJoin.join(Subdiv_.comp,
					javax.persistence.criteria.JoinType.LEFT);
			issuebyCompanyJoin.alias("issuebyCompanyJoin");

			Predicate andPre = cb.and(cb.equal(quotStatuJoin.get(QuoteStatus_.name), "Quote Issued to client"),
					cb.equal(subdivJoin.get(Subdiv_.subdivid), allocatedSubdivId));
			cq.where(cb.and(cb.equal(quoteStatusTranslationJoin.get(Translation_.locale), locale), andPre));
			cq.orderBy(cb.desc(quote.get(Quotation_.issuedate)));

			cq.select(cb.construct(QuotationProjectionDTO.class, quote.get(Quotation_.id), quote.get(Quotation_.qno),
					quote.get(Quotation_.ver), quoteStatusTranslationJoin.get(Translation_.translation),
					quote.get(Quotation_.clientref), quote.get(Quotation_.issuedate),

					sourcedContactJoin.get(Contact_.personid), sourcedContactJoin.get(Contact_.firstName),
					sourcedContactJoin.get(Contact_.lastName), sourcedContactJoin.get(Contact_.email),
					sourcedContactJoin.get(Contact_.telephone), sourcedContactJoin.get(Contact_.active),
					sourcedSubdivJoin.get(Subdiv_.subdivid), sourcedSubdivJoin.get(Subdiv_.subname),
					sourcedSubdivJoin.get(Subdiv_.subdivCode), sourcedSubdivJoin.get(Subdiv_.active),
					sourcedCompanyJoin.get(Company_.coid), sourcedCompanyJoin.get(Company_.coname),

					contactContactJoin.get(Contact_.personid), contactContactJoin.get(Contact_.firstName),
					contactContactJoin.get(Contact_.lastName), contactContactJoin.get(Contact_.email),
					contactContactJoin.get(Contact_.telephone), contactContactJoin.get(Contact_.active),
					contactSubdivJoin.get(Subdiv_.subdivid), contactSubdivJoin.get(Subdiv_.subname),
					contactSubdivJoin.get(Subdiv_.subdivCode), contactSubdivJoin.get(Subdiv_.active),
					contactCompanyJoin.get(Company_.coid), contactCompanyJoin.get(Company_.coname),

					issuebyContactJoin.get(Contact_.personid), issuebyContactJoin.get(Contact_.firstName),
					issuebyContactJoin.get(Contact_.lastName), issuebyContactJoin.get(Contact_.email),
					issuebyContactJoin.get(Contact_.telephone), issuebyContactJoin.get(Contact_.active),
					issuebySubdivJoin.get(Subdiv_.subdivid), issuebySubdivJoin.get(Subdiv_.subname),
					issuebySubdivJoin.get(Subdiv_.subdivCode), issuebySubdivJoin.get(Subdiv_.active),
					issuebyCompanyJoin.get(Company_.coid), issuebyCompanyJoin.get(Company_.coname),

					companyJoin.get(Company_.coid), quote.get(Quotation_.finalCost), quote.get(Quotation_.totalCost), quote.get(Quotation_.duedate),
					quote.get(Quotation_.regdate),quote.get(Quotation_.expiryDate),issuebySubdivJoin.get(Subdiv_.subdivCode)));
			cq.distinct(true);
			return cq;
		}, 0, resultSize);
	}

	@Override
	public Quotation getQuotationByExactQuoteNo(String quoteNo) {
		return getFirstResult(cb -> {
			val cq = cb.createQuery(Quotation.class);
			val quotaion = cq.from(Quotation.class);
			cq.where(cb.equal(quotaion.get(Quotation_.qno), quoteNo));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<Quotation> getQuotationsRequestForContactFromReqdate(int personId, LocalDate reqdate,
			boolean accepted, boolean issued) {
		
		return getResultList(cb -> {
            val cq = cb.createQuery(Quotation.class);
            val quotation = cq.from(Quotation.class);
            val con = quotation.join(Quotation_.contact);
            cq.where(cb.greaterThanOrEqualTo(quotation.get(Quotation_.regdate), reqdate),
                    cb.equal(quotation.get(Quotation_.issued), issued),
                    cb.equal(quotation.get(Quotation_.accepted), accepted),
                    cb.equal(con.get(Contact_.personid), personId));
            cq.orderBy(cb.desc(quotation.get(Quotation_.qno)));
            return cq;
        });
//		return getResultList(cb -> {
//			val cq = cb.createQuery(QuotationRequest.class);
//			val quotationRequest = cq.from(QuotationRequest.class);
//			val con = quotationRequest.join(QuotationRequest_.con);
//			cq.where(cb.greaterThanOrEqualTo(quotationRequest.get(QuotationRequest_.reqdate), reqdate),
//					cb.equal(con.get(Contact_.personid), personId),
//					cb.equal(quotationRequest.get(QuotationRequest_.status), QuotationRequestStatus.REQUESTED));
//			cq.orderBy(cb.desc(quotationRequest.get(QuotationRequest_.requestNo)));
//			return cq;
//		});
	}

	private Stream<Predicate> ifNonEmptyInPredicate(Path<?> path, Collection<Integer> ids) {
		return !ids.isEmpty() ? Stream.of(path.in(ids)) : Stream.empty();
	}

	private Stream<Predicate> inBetweenPredicate(CriteriaBuilder cb, Path<LocalDate> path, LocalDate date1,
			LocalDate date2) {
		if (date1 == null)
			return Stream.empty();
		val dateTo = date2 == null ? LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()) : date2;
		return Stream.of(cb.between(path, date1, dateTo));
	}

	@Override
	public QuotationMetricFormatter loadMetrics(QuotationMetricForm metricForm, QuotationMetricFormatter formatter) {
		// build a search query from the metricForm
		Function2<Boolean, Boolean, Function<CriteriaBuilder, CriteriaQuery<Long>>> criteriaGenerator = (issued,
				accepted) -> cb -> {
					val cq = cb.createQuery(Long.class);
					val quotation = cq.from(Quotation.class);
					val predicates = Stream.<Stream<Predicate>>of(
							ifNonEmptyInPredicate(quotation.get(Quotation_.createdBy), metricForm.getCreatedByIds()),
							ifNonEmptyInPredicate(quotation.get(Quotation_.issueby), metricForm.getIssuedByIds()),
							ifNonEmptyInPredicate(quotation.get(Quotation_.sourcedBy), metricForm.getSourcedByIds()),
							inBetweenPredicate(cb, quotation.get(Quotation_.regdate), metricForm.getRegFrom(),
									metricForm.getRegTo()),
							issued ? Stream.of(cb.isTrue(quotation.get(Quotation_.issued))) : Stream.empty(),
							accepted ? Stream.of(cb.isTrue(quotation.get(Quotation_.accepted))) : Stream.empty())
							.flatMap(Function.identity()).toArray(Predicate[]::new);
					cq.where(predicates);
					cq.select(cb.countDistinct(quotation));
					return cq;
				};

		// run 3 queries to get the total number quotes, those issued, those
		// accepted

		// total number quotes
		formatter.setQuotationCount(this.<Long>getSingleResult(criteriaGenerator.apply(false, false)).intValue());
		formatter.setIssued(this.<Long>getSingleResult(criteriaGenerator.apply(true, false)).intValue());
		formatter.setAccepted(this.<Long>getSingleResult(criteriaGenerator.apply(true, true)).intValue());
		return formatter;
	}

	@Override
	public PagedResultSet<QuotationProjectionDTO> queryQuotationNew(NewQuotationSearchForm qsf,
			PagedResultSet<QuotationProjectionDTO> rs, Locale locale) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<QuotationProjectionDTO> cq = cb.createQuery(QuotationProjectionDTO.class);
		Root<Quotation> quote = cq.from(Quotation.class);
		Join<Quotation, Company> companyJoin = quote.join("organisation");
		Join<Company, Subdiv> subdivJoin = companyJoin.join(Company_.subdivisions);
		Join<Quotation, QuoteStatus> quoteStatusJoin = quote.join(Quotation_.quotestatus);
		Join<QuoteStatus, Translation> quoteStatusTranslationJoin = quoteStatusJoin.join(QuoteStatus_.nametranslations);

		Join<Quotation, Contact> contactContactJoin = quote.join(Quotation_.contact);
		Join<Contact, Subdiv> contactSubdivJoin = contactContactJoin.join(Contact_.sub);
		Join<Subdiv, Company> contactCompanyJoin = contactSubdivJoin.join(Subdiv_.comp);

		Join<Quotation, Contact> sourcedContactJoin = quote.join(Quotation_.sourcedBy);
		Join<Contact, Subdiv> sourcedSubdivJoin = sourcedContactJoin.join(Contact_.sub);
		Join<Subdiv, Company> sourcedCompanyJoin = sourcedSubdivJoin.join(Subdiv_.comp);

		Join<Quotation, Contact> issuebyContactJoin = quote.join(Quotation_.issueby,
				javax.persistence.criteria.JoinType.LEFT);
		issuebyContactJoin.alias("issuebyContactJoin");
		Join<Contact, Subdiv> issuebySubdivJoin = issuebyContactJoin.join(Contact_.sub,
				javax.persistence.criteria.JoinType.LEFT);
		issuebySubdivJoin.alias("issuebySubdivJoin");
		Join<Subdiv, Company> issuebyCompanyJoin = issuebySubdivJoin.join(Subdiv_.comp,
				javax.persistence.criteria.JoinType.LEFT);
		issuebyCompanyJoin.alias("issuebyCompanyJoin");

		Join<Quotation, Quotationitem> quoteItemJoin = quote.join(Quotation_.quotationitems,
				javax.persistence.criteria.JoinType.LEFT);
		quoteItemJoin.alias("quoteItemJoin");
		Join<Quotationitem, Instrument> instrumentJoin = quoteItemJoin.join(Quotationitem_.inst,
				javax.persistence.criteria.JoinType.LEFT);
		instrumentJoin.alias("instrumentJoin");

		Predicate clauses = cb.conjunction();

		if (qsf.getAllocatedCompanyId() != null) {
			clauses.getExpressions().add(cb.equal(companyJoin.get(Company_.coid), qsf.getAllocatedCompanyId()));
		} else if (qsf.getAllocatedSubdivId() != null) {
			clauses.getExpressions().add(cb.equal(subdivJoin.get(Subdiv_.subdivid), qsf.getAllocatedSubdivId()));
		}
		// company id restriction
		if (qsf.getCoid() != null) {
			clauses.getExpressions().add(cb.equal(contactCompanyJoin.get(Company_.coid), qsf.getCoid()));
		} else if ((qsf.getConame() != null) && !qsf.getConame().trim().equals("")) {
			clauses.getExpressions().add(cb.like(contactCompanyJoin.get(Company_.coname), qsf.getConame() + "%"));
		}

		// contact id restriction
		if (qsf.getPersonid() != null) {
			clauses.getExpressions().add(cb.equal(contactContactJoin.get(Contact_.personid), qsf.getPersonid()));
		} else if ((qsf.getContact() != null) && !qsf.getContact().trim().equals("")) {
			clauses.getExpressions()
					.add(cb.or(cb.like(contactContactJoin.get(Contact_.firstName), qsf.getContact() + "%"),
							cb.like(contactContactJoin.get(Contact_.lastName), qsf.getContact() + "%")));
		}

		// subdiv (contact wins)
		else if (qsf.getSubdivid() != null) {
			clauses.getExpressions().add(cb.equal(contactSubdivJoin.get(Subdiv_.subdivid), qsf.getSubdivid()));
		}

		// client reference
		if ((qsf.getClientref() != null) && !qsf.getClientref().trim().isEmpty()) {
			clauses.getExpressions().add(cb.like(quote.get(Quotation_.clientref), "%" + qsf.getClientref() + "%"));
		}

		// quotation number restriction
		if ((qsf.getQno() != null) && !qsf.getQno().trim().equals("")) {
			clauses.getExpressions().add(cb.like(quote.get(Quotation_.qno), "%" + qsf.getQno() + "%"));
		}
		if ((qsf.getSerialno() != null) && (!qsf.getSerialno().trim().equals(""))) {
			clauses.getExpressions()
					.add(cb.like(instrumentJoin.get(Instrument_.serialno), "%" + qsf.getSerialno() + "%"));
		}
		if ((qsf.getPlantno() != null) && (!qsf.getPlantno().trim().equals(""))) {
			clauses.getExpressions()
					.add(cb.or(cb.like(instrumentJoin.get(Instrument_.plantno), "%" + qsf.getPlantno() + "%"),
							cb.like(quoteItemJoin.get(Quotationitem_.plantno), "%" + qsf.getPlantno() + "%")));
		}
		if ((qsf.getPlantid() != null) && (!qsf.getPlantid().trim().equals(""))) {
			try {
				if (NumberTools.isAnInteger(qsf.getPlantid())) {
					clauses.getExpressions().add(
							cb.or(cb.equal(instrumentJoin.get(Instrument_.plantid), Integer.parseInt(qsf.getPlantid())),
									cb.equal(instrumentJoin.get(Instrument_.formerBarCode), qsf.getPlantid())));
				} else {
					clauses.getExpressions()
							.add(cb.equal(instrumentJoin.get(Instrument_.formerBarCode), qsf.getPlantid()));
				}
			} catch (NumberFormatException ignored) {
			}
		}

		// issue date
		if (qsf.getIssueDate1() != null) {
			LocalDate date1, date2;
			date1 = qsf.getIssueDate1();

			date2 = (qsf.isIssueDateBetween() ? qsf.getIssueDate2() : qsf.getIssueDate1()).plusDays(1);
			clauses.getExpressions().add(cb.between(quote.get(Quotation_.issuedate), date1, date2));
		}

		// expiry date
		if (qsf.getExpiryDate1() != null) {

			LocalDate date4 = qsf.isExpiryDateBetween() ? qsf.getExpiryDate2() : qsf.getExpiryDate1();
			clauses.getExpressions().add(cb.between(quote.get(Quotation_.expiryDate), qsf.getExpiryDate1(), date4));
		}

		// years critiera
		if ((qsf.getResultsYearFilter() != null) && (qsf.getResultsYearFilter() > 0)) {
			val past = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())
					.minusYears(qsf.getResultsYearFilter());
			clauses.getExpressions().add(cb.greaterThan(quote.get(Quotation_.regdate), past));
		}

		// search created by
		if (qsf.getCreatedByIds() != null) {
			qsf.getCreatedByIds().remove(null);
			if (qsf.getCreatedByIds().size() > 0) {
				Join<Quotation, Contact> createdByContactJoin = quote.join(Quotation_.createdBy);
				clauses.getExpressions().add(createdByContactJoin.get(Contact_.personid).in(qsf.getCreatedByIds()));
			}
		}

		// search issued by
		if (qsf.getIssuedByIds() != null) {
			qsf.getIssuedByIds().remove(null);
			if (qsf.getIssuedByIds().size() > 0) {
				clauses.getExpressions().add(issuebyContactJoin.get(Contact_.personid).in(qsf.getIssuedByIds()));
			}
		}

		// search sourced by
		if (qsf.getSourcedByIds() != null) {
			qsf.getSourcedByIds().remove(null);
			if (qsf.getSourcedByIds().size() > 0) {
				clauses.getExpressions().add(sourcedContactJoin.get(Contact_.personid).in(qsf.getSourcedByIds()));
			}
		}

		// search quotestatus
		if (qsf.getQuotestatus() != null && qsf.getQuotestatus() > 0) {
			clauses.getExpressions().add(cb.equal(quoteStatusJoin.get(QuoteStatus_.statusid), qsf.getQuotestatus()));
		}

		// manufacturer
		if (((qsf.getMfr() != null) && !qsf.getMfr().trim().equals(""))
				|| ((qsf.getModel() != null) && !qsf.getModel().trim().equals(""))
				|| ((qsf.getDescription() != null) && !qsf.getDescription().trim().equals(""))
				|| ((qsf.getModelid() != null) && !qsf.getModelid().equals(0))
				|| ((qsf.getMfrid() != null) && !qsf.getMfrid().equals(0))
				|| ((qsf.getDescid() != null) && !qsf.getDescid().equals(0))) {

			Join<Quotationitem, InstrumentModel> qiModelJoin = quoteItemJoin.join(Quotationitem_.model,
					javax.persistence.criteria.JoinType.LEFT);
			Join<Instrument, InstrumentModel> instModelJoin = instrumentJoin.join(Instrument_.model,
					javax.persistence.criteria.JoinType.LEFT);

			Join<InstrumentModel, Description> qiModelDescriptionJoin = qiModelJoin.join(InstrumentModel_.description,
					javax.persistence.criteria.JoinType.LEFT);
			Join<InstrumentModel, Description> instModelDescriptionJoin = instModelJoin
					.join(InstrumentModel_.description, javax.persistence.criteria.JoinType.LEFT);

			Join<InstrumentModel, Mfr> qiMfrJoin = qiModelJoin.join(InstrumentModel_.mfr,
					javax.persistence.criteria.JoinType.LEFT);
			Join<InstrumentModel, Mfr> instModelMfrJoin = instModelJoin.join(InstrumentModel_.mfr,
					javax.persistence.criteria.JoinType.LEFT);
			Join<Instrument, Mfr> instMfrJoin = instrumentJoin.join(Instrument_.mfr,
					javax.persistence.criteria.JoinType.LEFT);

			// add model filtering
			if ((qsf.getModelid() != null) && !qsf.getModelid().equals(0)) {
				clauses.getExpressions()
						.add(cb.or(cb.equal(qiModelJoin.get(InstrumentModel_.modelid), qsf.getModelid()),
								cb.equal(instModelJoin.get(InstrumentModel_.modelid), qsf.getModelid())));
			} else if ((qsf.getModel() != null) && !qsf.getModel().trim().equals("")) {

				clauses.getExpressions()
						.add(cb.or(cb.like(qiModelJoin.get(InstrumentModel_.model), "%" + qsf.getModel() + "%"),
								cb.like(instModelJoin.get(InstrumentModel_.model), "%" + qsf.getModel() + "%"),
								cb.like(instrumentJoin.get(Instrument_.modelname), "%" + qsf.getModel() + "%")));
			}

			// add description filtering
			if ((qsf.getDescid() != null) && !qsf.getDescid().equals(0)) {
				clauses.getExpressions()
						.add(cb.or(cb.equal(qiModelDescriptionJoin.get(Description_.id), qsf.getDescid()),
								cb.equal(instModelDescriptionJoin.get(Description_.id), qsf.getDescid())));
			} else if ((qsf.getDescription() != null) && !qsf.getDescription().trim().equals("")) {
				clauses.getExpressions()
						.add(cb.or(
								cb.like(qiModelDescriptionJoin.get(Description_.description),
										"%" + qsf.getDescription() + "%"),
								cb.like(instModelDescriptionJoin.get(Description_.description),
										"%" + qsf.getDescription() + "%")));
			}

			// add mfr filtering
			if (qsf.getMfrid() != null) {
				clauses.getExpressions()
						.add(cb.or(cb.equal(qiMfrJoin.get(Mfr_.mfrid), qsf.getMfrid()),
								cb.or(cb.equal(instModelMfrJoin.get(Mfr_.mfrid), qsf.getMfrid()),
										cb.equal(instMfrJoin.get(Mfr_.mfrid), qsf.getMfrid()))));
			} else if ((qsf.getMfr() != null) && !qsf.getMfr().trim().equals("")) {
				clauses.getExpressions()
						.add(cb.or(cb.equal(qiMfrJoin.get(Mfr_.name), qsf.getMfr()),
								cb.or(cb.equal(instModelMfrJoin.get(Mfr_.name), qsf.getMfr()),
										cb.equal(instMfrJoin.get(Mfr_.name), qsf.getMfr()))));
			}
		}

		if (qsf.getSourcedBySubdivId() != null) {
			clauses.getExpressions()
					.add(cb.equal(quote.get(Quotation_.sourceAddress).get(Address_.sub).get(Subdiv_.SUBDIVID),
							qsf.getSourcedBySubdivId()));
		}

		clauses.getExpressions().add(cb.equal(quoteStatusTranslationJoin.get(Translation_.locale), locale));
		cq.where(clauses);
		cq.orderBy(cb.desc(quote.get(Quotation_.regdate)), cb.desc(quote.get(Quotation_.qno)),
				cb.desc(quote.get(Quotation_.ver)));

		cq.select(cb.construct(QuotationProjectionDTO.class, quote.get(Quotation_.id), quote.get(Quotation_.qno),
				quote.get(Quotation_.ver), quoteStatusTranslationJoin.get(Translation_.translation),
				quote.get(Quotation_.clientref), quote.get(Quotation_.issuedate),

				sourcedContactJoin.get(Contact_.personid), sourcedContactJoin.get(Contact_.firstName),
				sourcedContactJoin.get(Contact_.lastName), sourcedContactJoin.get(Contact_.email),
				sourcedContactJoin.get(Contact_.telephone), sourcedContactJoin.get(Contact_.active),
				sourcedSubdivJoin.get(Subdiv_.subdivid), sourcedSubdivJoin.get(Subdiv_.subname),
				sourcedSubdivJoin.get(Subdiv_.subdivCode), sourcedSubdivJoin.get(Subdiv_.active),
				sourcedCompanyJoin.get(Company_.coid), sourcedCompanyJoin.get(Company_.coname),

				contactContactJoin.get(Contact_.personid), contactContactJoin.get(Contact_.firstName),
				contactContactJoin.get(Contact_.lastName), contactContactJoin.get(Contact_.email),
				contactContactJoin.get(Contact_.telephone), contactContactJoin.get(Contact_.active),
				contactSubdivJoin.get(Subdiv_.subdivid), contactSubdivJoin.get(Subdiv_.subname),
				contactSubdivJoin.get(Subdiv_.subdivCode), contactSubdivJoin.get(Subdiv_.active),
				contactCompanyJoin.get(Company_.coid), contactCompanyJoin.get(Company_.coname),

				issuebyContactJoin.get(Contact_.personid), issuebyContactJoin.get(Contact_.firstName),
				issuebyContactJoin.get(Contact_.lastName), issuebyContactJoin.get(Contact_.email),
				issuebyContactJoin.get(Contact_.telephone), issuebyContactJoin.get(Contact_.active),
				issuebySubdivJoin.get(Subdiv_.subdivid), issuebySubdivJoin.get(Subdiv_.subname),
				issuebySubdivJoin.get(Subdiv_.subdivCode), issuebySubdivJoin.get(Subdiv_.active),
				issuebyCompanyJoin.get(Company_.coid), issuebyCompanyJoin.get(Company_.coname),

				companyJoin.get(Company_.coid), quote.get(Quotation_.finalCost),quote.get(Quotation_.totalCost), quote.get(Quotation_.duedate),
				quote.get(Quotation_.regdate), quote.get(Quotation_.expiryDate),issuebySubdivJoin.get(Subdiv_.subdivCode)));
		cq.distinct(true);

		TypedQuery<QuotationProjectionDTO> query = getEntityManager().createQuery(cq);
		rs.setResultsCount(query.getResultList().size());
		query.setFirstResult(rs.getStartResultsFrom());
		query.setMaxResults(rs.getResultsPerPage());
		rs.setResults(query.getResultList());
		return rs;
	}

	@Override
	public List<QuotationSearchResultWrapper> searchCompanyQuotations(int coid, boolean showExpired,
			boolean showUnIssued, Integer years, Integer jobid) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<QuotationSearchResultWrapper> cq = cb.createQuery(QuotationSearchResultWrapper.class);
		Root<Quotation> quotation = cq.from(Quotation.class);
		Join<Quotation, Contact> contact = quotation.join(Quotation_.contact);
		Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
		Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
		Join<Quotation, Quotationitem> items = quotation.join(Quotation_.quotationitems,
				javax.persistence.criteria.JoinType.LEFT);
		cq.select(cb.construct(QuotationSearchResultWrapper.class, quotation.get(Quotation_.id),
				quotation.get(Quotation_.qno), quotation.get(Quotation_.ver), quotation.get(Quotation_.issued),
				quotation.get(Quotation_.accepted),
				cb.selectCase()
						.when(cb.lessThanOrEqualTo(quotation.get(Quotation_.expiryDate),
								cb.currentDate().as(LocalDate.class)), true)
						.otherwise(false),
				company.get(Company_.coname), company.get(Company_.coid),
				cb.concat(cb.concat(contact.get(Contact_.firstName), " "), contact.get(Contact_.lastName)),
				contact.get(Contact_.personid), cb.count(items)));
		cq.distinct(true);
		Predicate clauses = cb.conjunction();
		List<Expression<Boolean>> expr = clauses.getExpressions();
		expr.add(cb.equal(company.get(Company_.coid), coid));
		if (!showExpired) {
			Predicate notExpired = cb.disjunction();
			notExpired.getExpressions().add(cb.isNull(quotation.get(Quotation_.expiryDate)));
			notExpired.getExpressions()
					.add(cb.greaterThan(quotation.get(Quotation_.expiryDate), cb.currentDate().as(LocalDate.class)));
			expr.add(notExpired);
		}
		if (!showUnIssued)
			expr.add(cb.isTrue(quotation.get(Quotation_.issued)));
		if (years != null) {
			val past = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).minusYears(years);
			expr.add(cb.greaterThanOrEqualTo(quotation.get(Quotation_.regdate), past));
		}
		if (jobid != null) {
			Subquery<JobQuoteLink> sq = cq.subquery(JobQuoteLink.class);
			Root<JobQuoteLink> link = sq.from(JobQuoteLink.class);
			Path<Job> job = link.get(JobQuoteLink_.job);
			Predicate sqClauses = cb.conjunction();
			sqClauses.getExpressions().add(cb.equal(job.get(Job_.jobid), jobid));
			sqClauses.getExpressions().add(cb.equal(link.get(JobQuoteLink_.quotation), quotation));
			sq.select(link);
			sq.where(sqClauses);
			expr.add(cb.not(cb.exists(sq)));
		}
		cq.where(clauses);
		cq.groupBy(quotation.get(Quotation_.id), quotation.get(Quotation_.qno), quotation.get(Quotation_.ver),
				quotation.get(Quotation_.issued), quotation.get(Quotation_.accepted),
				quotation.get(Quotation_.expiryDate), company.get(Company_.coname), company.get(Company_.coid),
				contact.get(Contact_.firstName), contact.get(Contact_.lastName), contact.get(Contact_.personid));
		TypedQuery<QuotationSearchResultWrapper> query = getEntityManager().createQuery(cq);
		return query.getResultList();
	}

	@Override
	public List<Quotation> getQuotationListByJobitemId(Integer jobitemid) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Quotation.class);
			val quotation = cq.from(Quotation.class);
			cq.where(cb.equal(quotation.join(Quotation_.linkedJobs).join(JobQuoteLink_.job).join(Job_.items),
					jobitemid));
			return cq;
		});
	}

	@Override
	public List<Quotation> getIssuedAndAcceptedQuotationListByJobitemId(Integer jobitemid,
			ArrayList<QuoteStatus> quoteStatus) {
		return getResultList(cb -> {
			val cq = cb.createQuery(Quotation.class);
			val quotation = cq.from(Quotation.class);
			val status = quotation.get(Quotation_.quotestatus);
			cq.where(
					cb.equal(quotation.join(Quotation_.linkedJobs).join(JobQuoteLink_.job).join(Job_.items), jobitemid),
					status.get(QuoteStatus_.name).in(quoteStatus));
			return cq;
		});
	}

	@Override
	public void getPaginationAllQuotesInStatusGroupNew(QuoteStatusGroup group, Integer allocatedSubdivId,
			PagedResultSet<QuotationProjectionDTO> webQuotesRequiringAction, NewQuotationSearchForm qsf,
			Locale locale) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();

		CriteriaQuery<QuotationProjectionDTO> cq = cb.createQuery(QuotationProjectionDTO.class);
		Root<Quotation> quote = cq.from(Quotation.class);
		quote.alias("quote");
		Join<Quotation, Company> companyJoin = quote.join("organisation");
		companyJoin.alias("companyJoin");
		Join<Company, Subdiv> subdivJoin = companyJoin.join(Company_.subdivisions);
		subdivJoin.alias("subdivJoin");
		Join<Quotation, QuoteStatus> quotStatuJoin = quote.join(Quotation_.quotestatus);
		quotStatuJoin.alias("quotStatuJoin");
		Join<QuoteStatus, QuoteStatusGroupMember> quoteStatusGroupMemberJoin = quotStatuJoin
				.join(QuoteStatus_.statusGroups);
		quoteStatusGroupMemberJoin.alias("quoteStatusGroupMemberJoin");
		Join<QuoteStatus, Translation> quoteStatusTranslationJoin = quotStatuJoin.join(QuoteStatus_.nametranslations);
		quoteStatusTranslationJoin.alias("quoteStatusTranslationJoin");

		Join<Quotation, Contact> contactContactJoin = quote.join(Quotation_.contact);
		contactContactJoin.alias("contactContactJoin");
		Join<Contact, Subdiv> contactSubdivJoin = contactContactJoin.join(Contact_.sub);
		contactSubdivJoin.alias("contactSubdivJoin");
		Join<Subdiv, Company> contactCompanyJoin = contactSubdivJoin.join(Subdiv_.comp);
		contactCompanyJoin.alias("contactCompanyJoin");

		Join<Quotation, Contact> sourcedContactJoin = quote.join(Quotation_.sourcedBy,
				javax.persistence.criteria.JoinType.LEFT);
		sourcedContactJoin.alias("sourcedContactJoin");
		Join<Contact, Subdiv> sourcedSubdivJoin = sourcedContactJoin.join(Contact_.sub,
				javax.persistence.criteria.JoinType.LEFT);
		sourcedSubdivJoin.alias("sourcedSubdivJoin");
		Join<Subdiv, Company> sourcedCompanyJoin = sourcedSubdivJoin.join(Subdiv_.comp,
				javax.persistence.criteria.JoinType.LEFT);
		sourcedCompanyJoin.alias("sourcedCompanyJoin");

		Join<Quotation, Contact> issuebyContactJoin = quote.join(Quotation_.issueby,
				javax.persistence.criteria.JoinType.LEFT);
		issuebyContactJoin.alias("issuebyContactJoin");
		Join<Contact, Subdiv> issuebySubdivJoin = issuebyContactJoin.join(Contact_.sub,
				javax.persistence.criteria.JoinType.LEFT);
		issuebySubdivJoin.alias("issuebySubdivJoin");
		Join<Subdiv, Company> issuebyCompanyJoin = issuebySubdivJoin.join(Subdiv_.comp,
				javax.persistence.criteria.JoinType.LEFT);
		issuebyCompanyJoin.alias("issuebyCompanyJoin");

		Predicate clauses1 = cb.and(
				cb.equal(quoteStatusGroupMemberJoin.get(QuoteStatusGroupMember_.groupId), group.getId()),
				cb.equal(subdivJoin.get(Subdiv_.subdivid), allocatedSubdivId));
		Predicate clauses2 = cb.and(
				cb.or(cb.isNull(quote.get(Quotation_.duedate)), cb.isNotNull(quote.get(Quotation_.duedate))),
				cb.equal(quoteStatusTranslationJoin.get(Translation_.locale), locale));
		cq.where(cb.and(clauses1, clauses2));
		Calendar maxDate = Calendar.getInstance();
		maxDate.add(Calendar.YEAR, 1);
		cq.orderBy(cb.asc(cb.coalesce(quote.get(Quotation_.duedate), maxDate.getTime())),
				cb.desc(quote.get(Quotation_.regdate)));

		cq.select(cb.construct(QuotationProjectionDTO.class, quote.get(Quotation_.id), quote.get(Quotation_.qno),
				quote.get(Quotation_.ver), quoteStatusTranslationJoin.get(Translation_.translation),
				quote.get(Quotation_.clientref), quote.get(Quotation_.issuedate),

				sourcedContactJoin.get(Contact_.personid), sourcedContactJoin.get(Contact_.firstName),
				sourcedContactJoin.get(Contact_.lastName), sourcedContactJoin.get(Contact_.email),
				sourcedContactJoin.get(Contact_.telephone), sourcedContactJoin.get(Contact_.active),
				sourcedSubdivJoin.get(Subdiv_.subdivid), sourcedSubdivJoin.get(Subdiv_.subname),
				sourcedSubdivJoin.get(Subdiv_.subdivCode), sourcedSubdivJoin.get(Subdiv_.active),
				sourcedCompanyJoin.get(Company_.coid), sourcedCompanyJoin.get(Company_.coname),

				contactContactJoin.get(Contact_.personid), contactContactJoin.get(Contact_.firstName),
				contactContactJoin.get(Contact_.lastName), contactContactJoin.get(Contact_.email),
				contactContactJoin.get(Contact_.telephone), contactContactJoin.get(Contact_.active),
				contactSubdivJoin.get(Subdiv_.subdivid), contactSubdivJoin.get(Subdiv_.subname),
				contactSubdivJoin.get(Subdiv_.subdivCode), contactSubdivJoin.get(Subdiv_.active),
				contactCompanyJoin.get(Company_.coid), contactCompanyJoin.get(Company_.coname),

				issuebyContactJoin.get(Contact_.personid), issuebyContactJoin.get(Contact_.firstName),
				issuebyContactJoin.get(Contact_.lastName), issuebyContactJoin.get(Contact_.email),
				issuebyContactJoin.get(Contact_.telephone), issuebyContactJoin.get(Contact_.active),
				issuebySubdivJoin.get(Subdiv_.subdivid), issuebySubdivJoin.get(Subdiv_.subname),
				issuebySubdivJoin.get(Subdiv_.subdivCode), issuebySubdivJoin.get(Subdiv_.active),
				issuebyCompanyJoin.get(Company_.coid), issuebyCompanyJoin.get(Company_.coname),

				companyJoin.get(Company_.coid), quote.get(Quotation_.finalCost), quote.get(Quotation_.totalCost), quote.get(Quotation_.duedate),
				quote.get(Quotation_.regdate),quote.get(Quotation_.expiryDate),issuebySubdivJoin.get(Subdiv_.subdivCode)));

		TypedQuery<QuotationProjectionDTO> query = getEntityManager().createQuery(cq);
		List<QuotationProjectionDTO> result = query.getResultList();
		webQuotesRequiringAction.setResultsCount(query.getResultList().size());
		try {
			if (QuoteStatusGroup.REQUIRINGATTENTION.equals(group))
				qsf.setSourcedByContBySubdivAC(getQuotationContactsBySubdivsAsJsonRequest(result));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		if (qsf.getQuoteSubdivId() != 0)
			clauses1.getExpressions().add(cb.equal(sourcedSubdivJoin.get(Subdiv_.subdivid), qsf.getQuoteSubdivId()));
		if (qsf.getQuoteContactId() != 0)
			clauses1.getExpressions().add(cb.equal(sourcedContactJoin.get(Contact_.personid), qsf.getQuoteContactId()));
		cq.where(cb.and(clauses1, clauses2));
		query = getEntityManager().createQuery(cq);
		webQuotesRequiringAction.setResultsCount(query.getResultList().size());
		query.setFirstResult(webQuotesRequiringAction.getStartResultsFrom());
		query.setMaxResults(webQuotesRequiringAction.getResultsPerPage());
		webQuotesRequiringAction.setResults(query.getResultList());
	}

	private String getQuotationContactsBySubdivsAsJsonRequest(Collection<QuotationProjectionDTO> collection)
			throws JsonProcessingException {

		// get all issuers(contacts) from fetched quotationRequest/quotations

		Set<ContactProjectionDTO> issuers = collection.stream().map(QuotationProjectionDTO::getSourcedBy)
				.collect(Collectors.toSet());

		// return key value VOs (id,name) of issuers grouped by subdiv
		Map<KeyValue<Integer, String>, List<KeyValue<Integer, String>>> m = issuers.stream().collect(Collectors
				.groupingBy(con -> new KeyValue<>(con.getSubdiv().getSubdivid(), con.getSubdiv().getSubname()),
						Collectors.mapping(c -> new KeyValue<>(c.getPersonid(), c.getName()), Collectors.toList())));

		// convert to json
		return new ObjectMapper().writeValueAsString(m);
	}
	
	@Override
	public List<QuoteItemOutputDto> findQuotationItemsFromModel(Integer modelId, Integer servicetypeid, Company allocatedCompany){
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<QuoteItemOutputDto> cq = cb.createQuery(QuoteItemOutputDto.class);
		Root<Quotationitem> root = cq.from(Quotationitem.class);
		Join<Quotationitem, Quotation> quotation = root.join(Quotationitem_.quotation);
		Join<Quotationitem, InstrumentModel> model = root.join(Quotationitem_.model);
		Join<Quotationitem, ServiceType> serviceType = root.join(Quotationitem_.serviceType);
		Join<Quotationitem, Instrument> instrument = root.join(Quotationitem_.inst, JoinType.LEFT);
		Predicate clauses = cb.conjunction();
		clauses.getExpressions().add(cb.equal(model.get(InstrumentModel_.modelid), modelId));
		clauses.getExpressions().add(cb.isTrue(quotation.get(Quotation_.issued)));
		clauses.getExpressions().add(cb.equal(serviceType.get(ServiceType_.serviceTypeId), servicetypeid));
		clauses.getExpressions().add(cb.equal(quotation.get(Quotation_.organisation.getName()), allocatedCompany));
		clauses.getExpressions().add(cb.isNull(instrument));
		cq.where(clauses);
		cq.orderBy(cb.desc(quotation.get(Quotation_.issuedate)));
		cq.select(cb.construct(QuoteItemOutputDto.class,
				quotation.get(Quotation_.expiryDate),
				quotation.get(Quotation_.id),
				quotation.get(Quotation_.qno),
				root.get(Quotationitem_.id),
				root.get(Quotationitem_.itemno),
				root.get(Quotationitem_.finalCost)
				));
		TypedQuery<QuoteItemOutputDto> query = getEntityManager().createQuery(cq);
		query.setMaxResults(10);
		return query.getResultList();
	}
}