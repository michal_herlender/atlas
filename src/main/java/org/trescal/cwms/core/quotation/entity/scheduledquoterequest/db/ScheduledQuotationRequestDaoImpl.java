package org.trescal.cwms.core.quotation.entity.scheduledquoterequest.db;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToSubdivDaoImpl;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation_;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequest;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequest_;

@Repository("ScheduledQuotationRequestDao")
public class ScheduledQuotationRequestDaoImpl extends AllocatedToSubdivDaoImpl<ScheduledQuotationRequest, Integer> implements ScheduledQuotationRequestDao
{
	@Override
	protected Class<ScheduledQuotationRequest> getEntity() {
		return ScheduledQuotationRequest.class;
	}

	@Override
	public List<ScheduledQuotationRequest> getOutstanding(Subdiv allocatedSubdiv)
	{
		return getResultList(cb ->{
			CriteriaQuery<ScheduledQuotationRequest> cq = cb.createQuery(ScheduledQuotationRequest.class);
			Root<ScheduledQuotationRequest> root = cq.from(ScheduledQuotationRequest.class);
			Predicate clauses = cb.conjunction();
			if(allocatedSubdiv != null){
				clauses.getExpressions().add(cb.equal(root.get(ScheduledQuotationRequest_.organisation), allocatedSubdiv));
			}
			clauses.getExpressions().add(cb.isFalse(root.get(ScheduledQuotationRequest_.processed)));
			cq.where(clauses);
			cq.orderBy(cb.desc(root.get(ScheduledQuotationRequest_.id)));
			return cq;
		});
	}

	@Override
	public List<ScheduledQuotationRequest> getTopXMostRecentlyProcessed(Subdiv allocatedSubdiv, int x)
	{
		x = x < 1 ? 10 : x;

			CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<ScheduledQuotationRequest> cq = cb.createQuery(ScheduledQuotationRequest.class);
			Root<ScheduledQuotationRequest> root = cq.from(ScheduledQuotationRequest.class);
			Predicate clauses = cb.conjunction();
			if(allocatedSubdiv != null){
				clauses.getExpressions().add(cb.equal(root.get(ScheduledQuotationRequest_.organisation), allocatedSubdiv));
			}
			clauses.getExpressions().add(cb.isTrue(root.get(ScheduledQuotationRequest_.processed)));
			cq.where(clauses);
			TypedQuery<ScheduledQuotationRequest> query = getEntityManager().createQuery(cq);
			query.setMaxResults(x);
			List<ScheduledQuotationRequest> result = query.getResultList();
			
			return result;
	}
	
	@Override
	public List<ScheduledQuotationRequest> scheduledQuotationRequestFromQuotation(Integer quotationId) {
		return getResultList(cb ->{
			CriteriaQuery<ScheduledQuotationRequest> cq = cb.createQuery(ScheduledQuotationRequest.class);
			Root<ScheduledQuotationRequest> root = cq.from(ScheduledQuotationRequest.class);
			Join<ScheduledQuotationRequest, Quotation> quotation = root.join(ScheduledQuotationRequest_.quotation, JoinType.LEFT);
			cq.where(cb.equal(quotation.get(Quotation_.id), quotationId));
			return cq;
		});
	}
}