package org.trescal.cwms.core.quotation.entity.quoteheading;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.SortComparator;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.QuotationItemComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;

@Entity
@Table(name = "quoteheading", uniqueConstraints = { @UniqueConstraint(columnNames = { "headingid", "quoteid" }) })
public class QuoteHeading extends Auditable implements Cloneable
{
	private String headingDescription;
	private int headingId;
	private String headingName;
	private Integer headingNo;
	private Quotation quotation;
	private Set<Quotationitem> quoteitems;
	private boolean systemDefault;

	public QuoteHeading()
	{
	}

	public QuoteHeading(int headingId, int headingNo, String headingName, String headingDescription, Quotation quotation, Set<Quotationitem> quoteitems, boolean defHeading)
	{
		super();
		this.headingId = headingId;
		this.headingNo = headingNo;
		this.headingName = headingName;
		this.headingDescription = headingDescription;
		this.quotation = quotation;
		this.quoteitems = quoteitems;
	}

	/**
	 * Copy Constructor - only copies non-key parameters
	 * 
	 * @param qh
	 */
	public QuoteHeading(QuoteHeading qh)
	{
		this.headingNo = qh.headingNo;
		this.headingName = qh.headingName;
		this.headingDescription = qh.headingDescription;
	}

	@NotNull
	@Column(name = "headingdescription", nullable = false, length = 200)
	public String getHeadingDescription()
	{
		return this.headingDescription;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "headingid", nullable = false, unique = true)
	@Type(type = "int")
	public int getHeadingId()
	{
		return this.headingId;
	}

	@NotNull
	@Column(name = "headingname", nullable = false, length = 50)
	public String getHeadingName()
	{
		return this.headingName;
	}

	@NotNull
	@Column(name = "headingno", nullable = false)
	@Type(type = "int")
	public Integer getHeadingNo()
	{
		return this.headingNo;
	}

	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "quoteid", nullable = false)
	public Quotation getQuotation()
	{
		return this.quotation;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "heading")
	@SortComparator(QuotationItemComparator.class)
	public Set<Quotationitem> getQuoteitems()
	{
		return this.quoteitems;
	}

	@NotNull
	@Column(name = "systemdefault", nullable = false, columnDefinition="tinyint")
	public boolean isSystemDefault()
	{
		return this.systemDefault;
	}

	public void setHeadingDescription(String longName)
	{
		this.headingDescription = longName;
	}

	public void setHeadingId(int calTypeId)
	{
		this.headingId = calTypeId;
	}

	public void setHeadingName(String shortName)
	{
		this.headingName = shortName;
	}

	public void setHeadingNo(Integer headingNo)
	{
		this.headingNo = headingNo;
	}

	public void setQuotation(Quotation quotation)
	{
		this.quotation = quotation;
	}

	public void setQuoteitems(Set<Quotationitem> quoteitems)
	{
		this.quoteitems = quoteitems;
	}

	public void setSystemDefault(boolean systemDefault)
	{
		this.systemDefault = systemDefault;
	}
}
