package org.trescal.cwms.core.quotation.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.quotation.entity.quotation.QuotationMetricFormatter;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuotationMetricForm {
	private LocalDate regFrom;
	private LocalDate regTo;

	private List<Integer> createdByIds = Collections.emptyList();
	private List<Integer> issuedByIds = Collections.emptyList();
	private List<Integer> sourcedByIds = Collections.emptyList();

	private List<ContactKeyValue> businessContacts;

	private QuotationMetricFormatter metrics;

	private boolean exists;


}
