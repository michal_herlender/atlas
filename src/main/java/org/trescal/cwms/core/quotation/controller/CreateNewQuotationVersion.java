package org.trescal.cwms.core.quotation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.QuotationItemComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.calibration.CustomCalibrationCondition;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.calibration.db.CustomCalibrationConditionService;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.general.CustomGeneralCondition;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.general.db.CustomGeneralConditionService;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general.db.DefaultGeneralConditionService;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeadingComparator;
import org.trescal.cwms.core.quotation.entity.quoteheading.db.QuoteHeadingService;
import org.trescal.cwms.core.quotation.entity.quoteitemnote.QuoteItemNote;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.quotation.form.CreateNewQuotationVersionForm;
import org.trescal.cwms.core.quotation.form.CreateNewQuotationVersionValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.db.NoteService;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Controller class to copy a Quotation into a new version of the same Quotation
 * number or as an entirely new quotation. This function allows the user to
 * selectively copy most quotation attributes, quotation items, quotation notes
 * and quotation headings. Additionally the copying process allows users to
 * select =if they want to preserve the moduler relationships of units on the
 * original quotation.
 *
 * @author Richard
 */
@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY })
public class CreateNewQuotationVersion {
	@Value("${cwms.config.quotation.size.restrictcopyquote}")
	private long sizeRestrictCopy;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private ContactService conServ;
	@Autowired
	private CustomCalibrationConditionService custCalConServ;
	@Autowired
	private CustomGeneralConditionService custGenConServ;
	@Autowired
	private DefaultGeneralConditionService defGenConServ;
	@Autowired
	private NoteService noteServ;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private QuoteHeadingService qhServ;
	@Autowired
	private QuotationItemService qiServ;
    @Autowired
    private QuotationService quoteServ;
    @Autowired
    private UserService userService;
    @Autowired
    private StatusService statusServ;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private CreateNewQuotationVersionValidator validator;

    public static final String FORM_NAME = "newquoteversion";

    @InitBinder(FORM_NAME)
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
        binder.setValidator(validator);
    }

    @ModelAttribute(FORM_NAME)
	protected CreateNewQuotationVersionForm formBackingObject(@RequestParam(name = "id") int quoteid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) {
		long totalItemCount = qiServ.getItemCount(quoteid);
		// 2016-02-22 - Added check of number of quotation items until
		// performance issues fully resolved
		if (totalItemCount >= sizeRestrictCopy) {
			throw new RuntimeException("This quotation has " + totalItemCount
					+ " items and due to temporary performance restrictions, quotations with " + sizeRestrictCopy
					+ " items or more cannot be copied to new quotations.");
		}
		Quotation quote = this.quoteServ.get(quoteid);
		if (quote == null) {
			throw new RuntimeException("The requested quotation could not be found");
		} else {
			CreateNewQuotationVersionForm form = new CreateNewQuotationVersionForm();
			// set form defaults
			form.setCreateAsNewVersion(true);
			form.setPreserveModules(true);
			form.setIncludeCustomConditions(true);
			form.setNoteThis(true);
			// Create a new Quotation based on the existing Quote and reset some
			// of the values
			Contact con = this.userService.get(username).getCon();
			Quotation newQuote = this.quoteServ.copyQuotation(quote);
			newQuote.setCreatedBy(con);
			newQuote.setReqdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			newQuote.setRegdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			newQuote.setQuotestatus(this.statusServ.findDefaultStatus(QuoteStatus.class));
			newQuote.setDefaultSetBy(con);
			newQuote.setDefaultSetOn(new Date());
			form.setPersonId(newQuote.getContact().getPersonid());
			form.setNewQuotation(newQuote);
			form.setSourcedById(quote.getSourcedBy().getPersonid());
			form.setBusinessCompanyId(companyDto.getKey());
			return form;
		}
	}

	private void copyCustomTermsAndConditions(CreateNewQuotationVersionForm form, Quotation newQuote) {
		if ((form.getCusCalConIds() != null) && (form.getCusGenConIds() != null)) {
			List<CustomCalibrationCondition> conditions = this.custCalConServ
					.getListCustomCalibrationConditions(form.getCusCalConIds());
			// copy any custom calibration conditions
			for (CustomCalibrationCondition condition : conditions) {
				CustomCalibrationCondition con = new CustomCalibrationCondition();
				con.setQuotation(newQuote);
				con.setCaltype(condition.getCaltype());
				con.setConditionText(condition.getConditionText());
				newQuote.getCalConditions().add(con);
			}
			CustomGeneralCondition oldGen = this.custGenConServ.get(form.getCusGenConIds());
			// copy custom general conditions
			CustomGeneralCondition gen = new CustomGeneralCondition();
			gen.setConditionText(oldGen.getConditionText());
			gen.setQuotation(newQuote);
			newQuote.setGeneralConditions(gen);
		}
	}

	/**
	 * Copies any requested headings from the old quotation to the new
	 * quotation. A system default heading is created if the newly created
	 * quotation would not have one of its own. A map
	 * 
	 */
	private HashMap<Integer, QuoteHeading> copyHeadings(CreateNewQuotationVersionForm form, Quotation oldQuote,
			Quotation newQuote, Locale locale) {
		// HashMap used to map old quotation heading ids to their new 'copied'
		// QuoteHeading objects
		HashMap<Integer, QuoteHeading> heads = new HashMap<>(0);

		int oldDefaultHeadingId = this.qhServ.findDefaultQuoteHeading(oldQuote.getId()).getHeadingId();
		QuoteHeading defaultHeading = null;
		if ((form.getHeadingids() != null) && (form.getHeadingids().size() > 0)) {
			// load up the list of headings the user has requested to copy
			List<QuoteHeading> headings = this.qhServ.findAll(form.getHeadingids());
			// iterate over each of the headings, copy into new headings, attach
			// to the new quotation and persist.
			for (QuoteHeading qh : headings) {
				QuoteHeading newHead = new QuoteHeading(qh);
				newHead.setQuotation(newQuote);
				newHead.setSystemDefault(qh.isSystemDefault());
				// initialise quote items
				newHead.setQuoteitems(new TreeSet<>(new QuotationItemComparator()));
				newQuote.getQuoteheadings().add(newHead);
				// add the new heading to a hashmap mapping old headingId
				// onto new heading
				heads.put(qh.getHeadingId(), newHead);
				if (qh.isSystemDefault())
					defaultHeading = newHead;
			}
		}
		if (defaultHeading == null) {
			defaultHeading = this.qhServ.createDefaultHeading(newQuote, locale);
			newQuote.getQuoteheadings().add(defaultHeading);
			heads.put(oldDefaultHeadingId, defaultHeading);
		}
		return heads;
	}

	private QuoteHeading getNewDefaultHeading(Map<Integer, QuoteHeading> heads) {
		QuoteHeading newDefaultHeading = null;
		for (QuoteHeading qh : heads.values()) {
			if (qh.isSystemDefault()) {
				newDefaultHeading = qh;
			}
		}
		return newDefaultHeading;
	}

	/**
	 * Copies any requested items from the old quotation into the new quotation.
	 * If the user has opted to preserve any 'part-or' module to base unit
	 * relationships then these are also copied over.
	 */
	private void copyItems(CreateNewQuotationVersionForm form, Quotation newQuote, Map<Integer, QuoteHeading> heads) {
		QuoteHeading newDefaultHeading = getNewDefaultHeading(heads);
		// HashMap used to map old quotation item itemnos to their new 'copied'
		// Quotationitem objects
		HashMap<Integer, Quotationitem> qitems = new HashMap<>(0);

		if ((form.getItemnos() != null) && (form.getItemnos().size() > 0)) {
			List<Quotationitem> qItems = this.qiServ.findQuotationItems(form.getItemnos());
			int itemno = 1;
			for (Quotationitem qi : qItems) {
				// create a 'copy' of the item - this cascades down to the
				// different Costs for the QuotationItem
				Quotationitem newItem = new Quotationitem(qi);
				newItem.setQuotation(newQuote);
				newItem.setItemno(itemno);
				itemno++;
				if (heads.get(qi.getHeading().getHeadingId()) == null) {
					newItem.setHeading(newDefaultHeading);
					// add item to heading
					newDefaultHeading.getQuoteitems().add(newItem);
				} else {
					QuoteHeading qh = heads.get(qi.getHeading().getHeadingId());
					newItem.setHeading(qh);
					// check that items have been initialised
					if (heads.get(qi.getHeading().getHeadingId()).getQuoteitems() == null) {
						// initialise quote items
						heads.get(qi.getHeading().getHeadingId())
								.setQuoteitems(new TreeSet<>(new QuotationItemComparator()));
					}
					// add item to heading
					heads.get(qi.getHeading().getHeadingId()).getQuoteitems().add(newItem);
				}
				if (qi.isPartOfBaseUnit() && form.isPreserveModules())
					newItem.setPartOfBaseUnit(true);
				newItem.setModules(null);
				newItem.setPartOfBaseUnit(false);
				newItem.setBaseUnit(null);
				// copy the cost totals straight over - these will remain
				// unchanged
				// from an item to item basis
				newItem.setTotalCost(qi.getTotalCost());
				newItem.setFinalCost(qi.getFinalCost());
				newItem.setGeneralDiscountRate(qi.getGeneralDiscountRate());
				newItem.setGeneralDiscountValue(qi.getGeneralDiscountValue());
				if (newItem.getNotes() == null)
					newItem.setNotes(new TreeSet<>(new NoteComparator()));
				if (newQuote.getQuotationitems() == null)
					newQuote.setQuotationitems(new TreeSet<>(new QuotationItemComparator()));
				newQuote.getQuotationitems().add(newItem);
				qitems.put(qi.getId(), newItem);
				if (qi.getNotes().size() > 0) {
					for (Note note : qi.getNotes()) {
						if (note.isActive()) {
							QuoteItemNote newNote = new QuoteItemNote();
							newNote.setQuotationitem(newItem);
							newNote.setNote(note.getNote());
							newNote.setLabel(note.getLabel());
							newNote.setPublish(note.getPublish());
							newNote.setSetBy(note.getSetBy());
							newNote.setSetOn(note.getSetOn());
							newNote.setActive(true);
							if (newItem.getNotes() == null)
								newItem.setNotes(new TreeSet<>(new NoteComparator()));
							newItem.getNotes().add(newNote);
						}
					}
				}
			}
			if (form.isPreserveModules()) {
				for (Quotationitem qi : qItems) {
					if (qi.isPartOfBaseUnit() && (qi.getBaseUnit() != null)) {
						Quotationitem mod = qitems.get(qi.getId());
						Quotationitem base = qitems.get(qi.getBaseUnit().getId());
						mod.setPartOfBaseUnit(true);
						mod.setBaseUnit(base);
					}
				}
			}
		}
	}

	/**
	 * Copy over any requested old quotation notes into the new quotation.
	 * 
	 */
	private void copyQuotationNotes(CreateNewQuotationVersionForm form, Quotation newQuote) {
		if ((form.getNoteids() != null) && (form.getNoteids().size() > 0)) {
			List<Note> qNotes = this.noteServ.findNotes(form.getNoteids(), QuoteNote.class);

			for (Note note : qNotes) {
				if (note.isActive()) {
					QuoteNote newNote = new QuoteNote();
					newNote.setQuotation(newQuote);
					newNote.setNote(note.getNote());
					newNote.setLabel(note.getLabel());
					newNote.setPublish(note.getPublish());
					newNote.setSetBy(note.getSetBy());
					newNote.setSetOn(note.getSetOn());
					newNote.setActive(true);
					if (newQuote.getNotes() == null) {
						newQuote.setNotes(new TreeSet<>(new NoteComparator()));
					}
					newQuote.getNotes().add(newNote);
				}
			}
		}
	}

	@RequestMapping(value = "/newquoteversionform.htm", method = RequestMethod.POST)
	protected String onSubmit(Locale locale, Model model, @RequestParam(name = "id") int quoteid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(FORM_NAME) @Validated CreateNewQuotationVersionForm form, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData(model, quoteid, companyDto);
		}
		Company allocatedCompany = companyService.get(companyDto.getKey());
		// create a new Quotation using the fields copied from the form
		Quotation oldQuote = this.quoteServ.get(quoteid);
		// fields used for copying
		String oldQuoteNo = oldQuote.getQno();
		int oldVersion = oldQuote.getVer();
		// create a new Quotation
		Quotation newQuote = form.getNewQuotation();
		// Get the version for this quotation
		String newQuoteNo;
		int version;
		// create the quotation as a new version of the existing quotation
		if (form.isCreateAsNewVersion()) {
			newQuoteNo = oldQuote.getQno();
			version = this.quoteServ.findNextVersion(newQuoteNo);
		}
		// create the quotation as an entirely new quotation
		else {
			// Get the next quotation number
			newQuoteNo = numerationService.generateNumber(NumerationType.QUOTATION, allocatedCompany, null);
			version = 1;
		}
		if (form.getPersonId() != newQuote.getContact().getPersonid()) {
			Contact newContact = conServ.get(form.getPersonId());
			newQuote.setContact(newContact);
		}
		newQuote.setSortType(oldQuote.getSortType());
		newQuote.setHeadingSortType(oldQuote.getHeadingSortType());
		newQuote.setQno(newQuoteNo);
		newQuote.setVer(version);
		newQuote.setUsingDefaultTermsAndConditions(form.isIncludeCustomConditions());
		newQuote.setIssued(false);
		newQuote.setAccepted(false);
		newQuote.setSourcedBy(this.conServ.get(form.getSourcedById()));
		newQuote.setOrganisation(allocatedCompany);
		newQuote.setNotes(new TreeSet<>(new NoteComparator()));
		newQuote.setQuoteheadings(new TreeSet<>(new QuoteHeadingComparator()));
		newQuote.setQuotationitems(new TreeSet<>(new QuotationItemComparator()));
		newQuote.setCalConditions(new HashSet<>());

		// add all headings to the new quotation (this returns the new default
		// heading)
		Map<Integer, QuoteHeading> heads = this.copyHeadings(form, oldQuote, newQuote, locale);

		// add any items to the new quotation
		this.copyItems(form, newQuote, heads);

		// update the total cost of the quotation
		CostCalculator.updatePricingFinalCost(newQuote, newQuote.getQuotationitems());

		if (!oldQuote.isUsingDefaultTermsAndConditions() && form.isIncludeCustomConditions()) {
			newQuote.setUsingDefaultTermsAndConditions(false);
			newQuote.setDefaultterms(this.defGenConServ.getLatest(allocatedCompany));
			this.copyCustomTermsAndConditions(form, newQuote);
		} else {
			newQuote.setUsingDefaultTermsAndConditions(true);
			newQuote.setDefaultterms(oldQuote.getDefaultterms());
		}

		// include default notes?
		if (form.isIncludeDefaultQuoteNotes()) {
            newQuote.getNotes().addAll(noteServ.initalizeNotes(newQuote, QuoteNote.class, locale));
        }

		// user requested to record a note detailing that this change has
		// occurred
		if (form.isNoteThis()) {
			QuoteNote note = new QuoteNote();
			note.setActive(true);
			note.setNote(
					messageSource.getMessage("createquot.newversionquotation", new Object[] { oldQuoteNo, oldVersion },
							"Quotation created as a new version of quotation " + oldQuoteNo + " version " + oldVersion,
							locale));
			note.setSetOn(new Date());
			note.setSetBy(this.userService.get(username).getCon());
			note.setQuotation(newQuote);
			note.setPublish(false);
			newQuote.getNotes().add(note);
		}

		// add any notes to the new quotation
		this.copyQuotationNotes(form, newQuote);
		// persist the new quotation
		this.quoteServ.save(newQuote);

		if (form.isCreateAsNewVersion()) {
			oldQuote.setQuotestatus(
					(QuoteStatus) this.statusServ.findStatusByName("Replaced with new version", QuoteStatus.class));
		}

		this.quoteServ.merge(oldQuote);

		// Remove attributes from model to prevent inclusion as redirect
		// parameters
		model.asMap().clear();
		return "redirect:viewquotation.htm?id=" + newQuote.getId();
	}

	@RequestMapping(value = "/newquoteversionform.htm", method = RequestMethod.GET)
	protected String referenceData(Model model, @RequestParam(name = "id") int quoteid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto) {
		Quotation oldQuotation = this.quoteServ.get(quoteid);
		model.addAttribute("oldQuotation", oldQuotation);

		// Get list of active client contacts (note existing contact may be no
		// longer active, checked in validator)
		ContactKeyValue clientContactDto = new ContactKeyValue(oldQuotation.getContact().getPersonid(),
				oldQuotation.getContact().getName());
		Integer clientCoid = oldQuotation.getContact().getSub().getComp().getCoid();
		List<ContactKeyValue> clientContactList = this.conServ.getContactDtoList(clientCoid, null, true,
				clientContactDto);
		model.addAttribute("clientContactList", clientContactList);
		// Get list of active business contacts (note existing sourcedBy may be
		// no longer active, checked in validator)
		ContactKeyValue businessContactDto = new ContactKeyValue(oldQuotation.getSourcedBy().getPersonid(),
				oldQuotation.getSourcedBy().getName());
		Integer businessCoid = companyDto.getKey();
		List<ContactKeyValue> businessContactList = this.conServ.getContactDtoList(businessCoid, null, true,
				businessContactDto);
		model.addAttribute("businessContactList", businessContactList);
		// load a list of quotestatus
		model.addAttribute("quoteStatusList", this.statusServ.getAllStatuss(QuoteStatus.class));

		return "trescal/core/quotation/newquoteversionform";
	}
}