package org.trescal.cwms.core.quotation.dto;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.company.projection.CompanyProjectionDTO;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.quotation.entity.QuoteRequestType;
import org.trescal.cwms.core.quotation.entity.QuoteRequested;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequestDetailsSource;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuoteRequestLogSource;

import java.time.LocalDate;

@Getter
@Setter
public class QuotationRequestProjectionDTO implements QuoteRequested {

	private int id;
	private String quotestatus;
	private LocalDate reqdate;
	private LocalDate duedate;
	private ContactProjectionDTO loggedBy;
	private String requestNo;
	private QuotationRequestDetailsSource detailsSource;
	private CompanyProjectionDTO comp;
	private String company;
	private ContactProjectionDTO con;
	private String contact;
	private QuoteRequestLogSource logSource;
	private QuoteRequestType quoteType;
	private Integer organisationId;

	public QuotationRequestProjectionDTO(Integer id, LocalDate reqdate, LocalDate duedate,
										 String requestNo, QuotationRequestDetailsSource detailsSource,
										 String company, String contact,
										 QuoteRequestLogSource logSource, QuoteRequestType quoteType,

										 Integer loggedByPersonid, String loggedByFirstName, String loggedByLastName,
										 String loggedByEmail, String loggedByTelephone, Boolean loggedByContactActive,
										 Integer loggedBySubdivid, String loggedBySubname, Boolean loggedBySubdivActive,
										 Integer loggedByCoid, String loggedByConame,

										 Integer conPersonid, String conFirstName, String conLastName,
										 String conEmail, String conTelephone, Boolean conContactActive,
										 Integer conSubdivid, String conSubname, Boolean conSubdivActive,
										 Integer conCoid, String conConame,

										 Integer coid, String coname,
										 Integer organisationId) {
		super();
		this.id = id;
		this.reqdate = reqdate;
		this.duedate = duedate;
		this.loggedBy = new ContactProjectionDTO(loggedByPersonid, loggedByFirstName, loggedByLastName, loggedByEmail, loggedByTelephone, loggedByContactActive, loggedBySubdivid, loggedBySubname, loggedBySubdivActive, loggedByCoid, loggedByConame, null, null);
		this.requestNo = requestNo;
		this.detailsSource = detailsSource;
		this.comp = new CompanyProjectionDTO(coid, coname, null, null);
		this.company = company;
		this.con = new ContactProjectionDTO(conPersonid, conFirstName, conLastName, conEmail, conTelephone, conContactActive, conSubdivid, conSubname, conSubdivActive, conCoid, conConame, null, null);
		this.contact = contact;
		this.logSource = logSource;
		this.quoteType = quoteType;
		this.organisationId = organisationId;
	}


	@Override
	public boolean isQuote() {
		return false;
	}
}