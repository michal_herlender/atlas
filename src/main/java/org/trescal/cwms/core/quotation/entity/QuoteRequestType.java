package org.trescal.cwms.core.quotation.entity;

/**
 * This was previously in an external web service 'beans' package that there was actually no source code for! 
 * Recreating inside project 2017-12-11
 */
public enum QuoteRequestType {
	CALIBRATION("Calibration"), HIRE("Hire"), SALES("Sales"); 
	
	private QuoteRequestType(String displayName) {
		this.displayName = displayName;
	}
	
	private String displayName;

	public String getDisplayName() {
		return displayName;
	}
}
