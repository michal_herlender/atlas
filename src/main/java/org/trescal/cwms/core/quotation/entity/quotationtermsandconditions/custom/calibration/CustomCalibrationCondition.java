package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.calibration;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.QuotationCalibrationCondition;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

@Entity
@Table(name="customquotationcalibrationcondition", uniqueConstraints = {@UniqueConstraint(columnNames={"caltypeid", "quoteid"})})
public class CustomCalibrationCondition extends Versioned implements QuotationCalibrationCondition
{
	private Quotation quotation;
	private CalibrationType caltype;
	private String conditionText;
	private int defCalConId;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "defcalconid", nullable = false, unique = true)
	@Type(type = "int")
	public int getDefCalConId() {
		return this.defCalConId;
	}
	
	@Column(name = "conditiontext", nullable = true, unique = false, length = 2000)
	public String getConditionText() {
		return this.conditionText;
	}
	
	public void setDefCalConId(int defCalConId) {
		this.defCalConId = defCalConId;
	}
	
	public void setConditionText(String conditionText) {
		this.conditionText = conditionText;
	}
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY) 
	@JoinColumn(name = "caltypeid", unique = false, nullable = false, insertable = true, updatable = true)
	public CalibrationType getCaltype() {
		return caltype;
	}
	
	public void setCaltype(CalibrationType caltype) {
		this.caltype = caltype;
	}
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name="quoteid", unique = false, nullable = false, insertable = true, updatable = true)
	public Quotation getQuotation() {
		return quotation;
	}
	
	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}
}