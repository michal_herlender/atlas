package org.trescal.cwms.core.quotation.entity.quotenote.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;

public interface QuoteNoteDao extends BaseDao<QuoteNote, Integer> {

}
