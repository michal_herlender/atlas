package org.trescal.cwms.core.quotation.entity.costs.calcost.db;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.quotation.entity.costs.QuotationCostDaoSupportImpl;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost_;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation_;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem_;

@Repository("QuotationCalibrationCostDao")
public class QuotationCalibrationCostDaoImpl extends QuotationCostDaoSupportImpl<QuotationCalibrationCost>
		implements QuotationCalibrationCostDao {

	public QuotationCalibrationCost findEagerQuotationCalibrationCost(int id) {
		return getSingleResult(cb -> {
			CriteriaQuery<QuotationCalibrationCost> cq = cb.createQuery(QuotationCalibrationCost.class);
			Root<QuotationCalibrationCost> calCost = cq.from(QuotationCalibrationCost.class);
			calCost.fetch(QuotationCalibrationCost_.quoteItem).fetch(Quotationitem_.quotation)
					.fetch(Quotation_.currency);
			calCost.fetch(QuotationCalibrationCost_.quoteItem);
			cq.where(cb.equal(calCost.get(QuotationCalibrationCost_.costid), id));
			return cq;
		});
	}

	@Override
	protected Class<QuotationCalibrationCost> getEntity() {
		return QuotationCalibrationCost.class;
	}
}