package org.trescal.cwms.core.quotation.entity.quotestatusgroupmember;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.quotation.entity.quotestatusgroup.QuoteStatusGroup;

@Entity
@Table(name = "quotestatusgroupmember")
public class QuoteStatusGroupMember
{
	private int id;
	private QuoteStatus status;
	private QuoteStatusGroup group;

	@Column(name = "groupid", nullable = false)
	public int getGroupId()
	{
		if (this.group != null)
			return this.group.getId();
		
		return 0;
	}
	
	@Transient
	public QuoteStatusGroup getGroup()
	{
		return this.group;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid", nullable = false)
	public QuoteStatus getStatus()
	{
		return this.status;
	}

	public void setGroupId(int group)
	{
		setGroup(QuoteStatusGroup.getQuoteStatusGroupById(group));
	}
	
	public void setGroup(QuoteStatusGroup group)
	{
		this.group = group;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setStatus(QuoteStatus status)
	{
		this.status = status;
	}
}
