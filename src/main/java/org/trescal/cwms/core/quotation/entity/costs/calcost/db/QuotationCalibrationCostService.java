package org.trescal.cwms.core.quotation.entity.costs.calcost.db;

import org.trescal.cwms.core.quotation.entity.costs.QuotationCostService;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;

public interface QuotationCalibrationCostService extends QuotationCostService<QuotationCalibrationCost>
{
	/**
	 * Returns the {@link QuotationCalibrationCost} entity with the given ID.
	 * 
	 * @param id id of the {@link QuotationCalibrationCost}
	 * @return {@link QuotationCalibrationCost}
	 */
	QuotationCalibrationCost findEagerQuotationCalibrationCost(int id);
}