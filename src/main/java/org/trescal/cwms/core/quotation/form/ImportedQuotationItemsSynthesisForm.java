package org.trescal.cwms.core.quotation.form;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.trescal.cwms.batch.config.ItemAnalysis;
import org.trescal.cwms.batch.config.entities.itemanalysisresult.ItemAnalysisResult;

import lombok.Data;

public class ImportedQuotationItemsSynthesisForm {
	
	private Integer subdivId;
	private Integer contactId;
	private Integer addressId;
	private Integer coid;
	private Integer exchangeFormatId;
	private Integer businessCompanyId;
	private Integer quotationId;
	private boolean identifiedItemsExist;

	@Valid
	private List<ImportedQuotationItemsSynthesisRowDTO> rows;
	
	private List<ImportedQuotationItemsSynthesisRowDTO> identifiedItems;
	private List<ImportedQuotationItemsSynthesisRowDTO> nonIdentifiedItems;
	
	public Integer getSubdivId() {
		return subdivId;
	}
	public void setSubdivId(Integer subdivId) {
		this.subdivId = subdivId;
	}
	public Integer getCoid() {
		return coid;
	}
	public void setCoid(Integer coid) {
		this.coid = coid;
	}
	public Integer getContactId() {
		return contactId;
	}
	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}
	public Integer getAddressId() {
		return addressId;
	}
	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}
	public Integer getExchangeFormatId() {
		return exchangeFormatId;
	}
	public void setExchangeFormatId(Integer exchangeFormatId) {
		this.exchangeFormatId = exchangeFormatId;
	}
	public Integer getBusinessCompanyId() {
		return businessCompanyId;
	}
	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}
	public boolean isIdentifiedItemsExist() {
		return identifiedItemsExist;
	}
	public void setIdentifiedItemsExist(boolean identifiedItemsExist) {
		this.identifiedItemsExist = identifiedItemsExist;
	}
	public List<ImportedQuotationItemsSynthesisRowDTO> getRows() {
		return rows;
	}
	public void setRows(List<ImportedQuotationItemsSynthesisRowDTO> rows) {
		this.rows = rows;
	}
	public Integer getQuotationId() {
		return quotationId;
	}
	public void setQuotationId(Integer quotationId) {
		this.quotationId = quotationId;
	}
	public List<ImportedQuotationItemsSynthesisRowDTO> getIdentifiedItems() {
		return identifiedItems;
	}
	public void setIdentifiedItems(List<ImportedQuotationItemsSynthesisRowDTO> identifiedItems) {
		this.identifiedItems = identifiedItems;
	}
	public List<ImportedQuotationItemsSynthesisRowDTO> getNonIdentifiedItems() {
		return nonIdentifiedItems;
	}
	public void setNonIdentifiedItems(List<ImportedQuotationItemsSynthesisRowDTO> nonIdentifiedItems) {
		this.nonIdentifiedItems = nonIdentifiedItems;
	}

	@Data
	public static class ImportedQuotationItemsSynthesisRowDTO implements ItemAnalysis{
		
		/* input data */
		private Integer plantId;
		private String plantNo;
		private Integer modelid;
		private Integer tmlid;
		private String serviceType;
		private Integer serviceTypeId;
		private Double catalogPrice;
		private Double discountRate;
		private Double finalPrice;
		private String publicNote;
		private String privateNote;
		private List<String> errorMessages;
		private Integer index;
		private List<ItemAnalysisResult> itemAnalysisResult;
		
		public ImportedQuotationItemsSynthesisRowDTO() {
			this.itemAnalysisResult = new ArrayList<ItemAnalysisResult>();
		}
	}
}
