package org.trescal.cwms.core.quotation.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.views.QuoteItemsExcelStreamingView;
import org.trescal.cwms.core.quotation.views.QuoteItemsExcelView;

/* 
 * Controller to produce an Excel file containing details of all quotation items 
 * relating to a particular quotation, uses QuoteItemsExcelView to create Excel file
 */

@Controller @IntranetController
public class QuotationExcelDownload {

	@Autowired
	private QuotationItemService quoteItemServ;
	@Autowired
	private QuoteItemsExcelStreamingView quoteItemsExcelStreamingView;
	@Autowired
	private QuoteItemsExcelView quoteItemsExcelView;
	@Value("${cwms.config.quotation.size.restrictexport}")
	private Long sizeRestrictView;

	@RequestMapping(method = RequestMethod.GET, path = "/quoteitemdownload.xlsx")
	protected ModelAndView generateExcel(@RequestParam("quoteid") int quoteid) {
		Long totalItemCount = this.quoteItemServ.getItemCount(quoteid);
		if (totalItemCount > sizeRestrictView) {
			throw new RuntimeException("This quotation has " + totalItemCount
					+ " items and due to performance restrictions, quotations with more than " + sizeRestrictView
					+ " items cannot currently be exported.");
		}
		
		Set<Quotationitem> items = quoteItemServ.getItemsForQuotation(quoteid, null, null);
		return new ModelAndView(quoteItemsExcelView, "items", items);
	}
	@RequestMapping(method = RequestMethod.GET, path = "/quoteitemStreamingdownload.xlsx")
	protected ModelAndView generateExceldownload(@RequestParam("quoteid") int quoteid) {
		Long totalItemCount = this.quoteItemServ.getItemCount(quoteid);
		if (totalItemCount > sizeRestrictView) {
			throw new RuntimeException("This quotation has " + totalItemCount
					+ " items and due to performance restrictions, quotations with more than " + sizeRestrictView
					+ " items cannot currently be exported.");
		}
		
		Set<Quotationitem> items = quoteItemServ.getItemsForQuotation(quoteid, null, null);
		return new ModelAndView(quoteItemsExcelStreamingView, "items", items);
	}
}
