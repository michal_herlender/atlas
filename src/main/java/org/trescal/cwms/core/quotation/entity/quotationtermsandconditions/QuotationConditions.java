package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions;

import java.util.Set;

import javax.persistence.Transient;

/**
 * Convenience transient object to hold the current terms and conditions for a
 * quotation. Both general terms and conditions and calibration terms and
 * conditions can be system defaults or custom set by users. This should only to
 * be used for ajax calls.
 * 
 * @author Richard
 */
public class QuotationConditions
{
	private QuotationGeneralCondition generalCon;
	private Set<? extends QuotationCalibrationCondition> calCon;

	@Transient
	public Set<? extends QuotationCalibrationCondition> getCalCon() {
		return calCon;
	}
	
	public void setCalCon(Set<? extends QuotationCalibrationCondition> calCon) {
		this.calCon = calCon;
	}
	
	@Transient
	public QuotationGeneralCondition getGeneralCon() {
		return generalCon;
	}
	
	public void setGeneralCon(QuotationGeneralCondition generalCon) {
		this.generalCon = generalCon;
	}
}