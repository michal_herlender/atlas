package org.trescal.cwms.core.quotation.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quotecaltypedefault.QuoteCaltypeDefault;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteheading.db.QuoteHeadingService;
import org.trescal.cwms.core.quotation.entity.quoteitemnote.QuoteItemNote;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.quotation.form.AddInstModelToQuoteForm;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.note.db.NoteService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.tools.MathTools;

@Service
public class AddInstModelToQuotationServiceImpl implements AddInstModelToQuotationService {
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private CatalogPriceService catalogPriceService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private QuoteHeadingService qhServ;
	@Autowired
	private QuotationItemService qiServ;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private InstrumentModelService instModelService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private MessageSource messages;
	@Autowired
	private ContactService conService;
	@Autowired
	private SubdivService subServ;
	@Autowired
	private NoteService noteService;
	@Autowired
	TaxCalculator taxCalculator;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * Put inside one service method as controller was having flush/commit with
	 * each service call
	 */
	public void addToQuote(Quotation quotation, AddInstModelToQuoteForm aqi, Subdiv subdiv, Contact contact,
			Locale locale) {
		Map<String, QuoteNote> existingServiceCapabilityNotes = this.qiServ.getServiceCapabilityNoteMap(quotation,
				locale);
		String quoteItemLabel = messageSource.getMessage("quoteautonote.catalogpricenote", null, locale);
		String quoteLabel = messageSource.getMessage("quoteautonote.procedurenote", null, locale);

		if ((aqi.getChosenModels() != null) && (aqi.getChosenModels().size() > 0)) {
			for (Quotationitem qi : aqi.getChosenModels()) {
				// add new quotation item
				quotation.getQuotationitems().add(qi);
				// Add catalog price comment as a note
				CatalogPrice catalogPrice = catalogPriceService.findSingle(qi.getModel(), qi.getServiceType(), null,
						subdiv.getComp());
				if (catalogPrice != null) {
					String comments = catalogPrice.getComments();
					if (comments != null && !comments.isEmpty()) {
						QuoteItemNote qiNote = this.qiServ.createTemplateNewQuotationItemNote(qi, true, quoteItemLabel,
								comments, true, contact);
						qi.getNotes().add(qiNote);
					}
				}
				// Add default Procedure description as a note to the quote (not
				// quote item!)
				String comment = this.qiServ.getServiceCapabilityComment(qi.getModel(),
						qi.getServiceType().getCalibrationType(), subdiv, locale);
				if (comment != null) {
					QuoteNote existingQuoteNote = existingServiceCapabilityNotes.get(comment);
					if (existingQuoteNote != null) {
						existingQuoteNote.setActive(true);
						existingQuoteNote.setPublish(true);
					} else {
						QuoteNote qNote = this.quoteServ.createTemplateNewQuoteNote(quotation, true, quoteLabel,
								comment, true, contact);
						quotation.getNotes().add(qNote);
					}
				}
				// get the quote heading
				QuoteHeading qh = this.qhServ.findQuoteHeading(qi.getHeading().getHeadingId());
				// set quote item into heading
				qh.getQuoteitems().add(qi);
				// any modules?
				if (qi.getModules() != null) {
					for (Quotationitem qItemModule : qi.getModules()) {
						qItemModule.setBaseUnit(qi);
						qItemModule.setPartOfBaseUnit(true);
						qh.getQuoteitems().add(qItemModule);
						// add all new items to quotation items
						quotation.getQuotationitems().add(qItemModule);
					}
				}
			}
			// update the final cost of the quotation as a whole
			// update the costs
			CostCalculator.updatePricingTotalCost(quotation, quotation.getQuotationitems());
			taxCalculator.calculateAndSetVATaxAndFinalCost(quotation);
			// update the numbering of the items on the quotation
			this.qiServ.sortQuotationItems(quotation);
			// update quotation
			this.quoteServ.merge(quotation);
		}
	}

	private Set<QuoteCaltypeDefault> getQuoteCaltypeDefaults(AddInstModelToQuoteForm aqi) {
		Set<QuoteCaltypeDefault> quoteCaltypeDefaults = new HashSet<QuoteCaltypeDefault>();
		if (aqi.getDefaultCaltypes() != null) {
			for (Integer caltypeid : aqi.getDefaultCaltypes()) {
				CalibrationType ct = this.calTypeServ.find(caltypeid);
				QuoteCaltypeDefault qct = new QuoteCaltypeDefault();
				qct.setCaltype(ct);
				qct.setQuotation(aqi.getQuotation());
				quoteCaltypeDefaults.add(qct);
			}
		}
		return quoteCaltypeDefaults;
	}

	@Override
	public void saveQuotationDefaults(AddInstModelToQuoteForm aqi, Contact currentContact) {
		boolean updateQuotation = false;
		Quotation quote = aqi.getQuotation();
		if (quote.isDefaultAddModules() != aqi.isDefaultAddModules()) {
			quote.setDefaultAddModules(aqi.isDefaultAddModules());
			updateQuotation = true;
		}
		this.qhServ.selectNewDefaultHeading(quote, aqi.getDefaultQuoteHeading());
		if (quote.getDefaultQty() != aqi.getDefaultQuoteQty()) {
			quote.setDefaultQty(aqi.getDefaultQuoteQty());
			updateQuotation = true;
		}

		// sort out the calibration types
		if ((aqi.getDefaultCaltypes() != null) && !aqi.getDefaultCaltypes().isEmpty()) {
			// Set new quoteCaltypeDefaults for this quotation (others will
			// delete by cascade)
			quote.getDefaultCaltypes().clear();
			quote.getDefaultCaltypes().addAll(this.getQuoteCaltypeDefaults(aqi));
			updateQuotation = true;
		} else {
			quote.getDefaultCaltypes().clear();
			updateQuotation = true;
		}

		if (updateQuotation) {
			quote.setDefaultSetOn(new Date());
			quote.setDefaultSetBy(currentContact);
		}
	}

	public Set<Quotationitem> importQuotationItemsUsingInstModel(Integer allocatedSubdivId, Integer contactId, List<Integer> modelindex,
			Map<Integer, Integer> modelIds, Map<Integer, Integer> serviceTypeIds, Map<Integer, BigDecimal> finalPrices,
			Map<Integer, BigDecimal> discountPrices, Map<Integer, BigDecimal> catalogPrices,
			Map<Integer, String> publicNotes, Map<Integer, String> privateNotes, Integer quoteId, Locale locale) {

		Quotation quotation = quoteServ.get(quoteId);
		Subdiv subDiv = this.subServ.get(allocatedSubdivId);
		Contact person = this.conService.get(contactId);
		// get all calibration types currently active
		List<CalibrationType> caltypes = this.calTypeServ.getActiveCalTypes();
		// create instrument model list from the modelIds
		Set<QuoteNote> newQuoteNotes = new HashSet<>();
		Set<String> newQuoteNotesText = new HashSet<>();
		Set<Quotationitem> qItems = new HashSet<Quotationitem>();
		// initialize item count
		int itemcount = this.qiServ.getMaxItemno(quotation.getId()) + 1;

		for (Integer index : modelindex) {
			ServiceType servicetype = this.serviceTypeService.get(serviceTypeIds.get(index));
			CalibrationType caltype = servicetype.getCalibrationType();
			Quotationitem qitem = qiServ.createTemplateNewQuotationItem(quotation,
					quotation.getQuoteheadings().iterator().next(), null, modelIds.get(index),
					caltype.getCalTypeId(), allocatedSubdivId);
			qitem.setQuotation(quotation);
			qitem.setModel(instModelService.get(modelIds.get(index)));
			qitem.setNotes(new TreeSet<>(new NoteComparator()));
			qitem.setPlantno("");
			// set Service type
			qitem.setServiceType(servicetype);
			// set Calibration type
			qitem.setCaltype(caltype);
			//
			if (caltypes.stream().anyMatch(cal -> cal.getCalTypeId() == qitem.getCaltype().getCalTypeId())) {
				qitem.setItemno(itemcount);
				itemcount++;
			}

			// set Final price from file
			if (finalPrices.get(index) != null) {
				qitem.setFinalCost(finalPrices.get(index));
				qitem.getCalibrationCost().setFinalCost(finalPrices.get(index));
				// we will overwrite the total cost, total cost of
				// calibration and house cost of calibration if we have
				// catalog price(non mandatory field)
				qitem.getCalibrationCost().setTotalCost(finalPrices.get(index));
				qitem.getCalibrationCost().setHouseCost(finalPrices.get(index));
				qitem.setTotalCost(finalPrices.get(index));
			}

			// set Discount rate from file
			if (discountPrices.get(index) != null) {
				qitem.getCalibrationCost().setDiscountRate(discountPrices.get(index));
			}

			// set total price from file
			if (catalogPrices.get(index) != null) {
				qitem.setTotalCost(catalogPrices.get(index));
				qitem.getCalibrationCost().setHouseCost(catalogPrices.get(index));
				qitem.getCalibrationCost().setTotalCost(catalogPrices.get(index));
			}

			// calculate discount value using discountRate and totalCost
			if (qitem.getTotalCost() != null && qitem.getCalibrationCost().getDiscountRate().doubleValue() > 0) {
				BigDecimal up = qitem.getCalibrationCost().getDiscountRate().divide(new BigDecimal("100"),
						MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
				BigDecimal discountValue = up.multiply(qitem.getTotalCost()).setScale(MathTools.SCALE_FIN,
						MathTools.ROUND_MODE_FIN);
				qitem.getCalibrationCost().setDiscountValue(discountValue);
			}

			// set Public note
			if (!StringUtils.isEmpty(publicNotes.get(index))) {
				QuoteItemNote notePublic = new QuoteItemNote();
				String publicLabel = messages.getMessage("exchangeformat.fieldname.publicnote", null, locale);
				notePublic.setQuotationitem(qitem);
				notePublic.setSetBy(person);
				notePublic.setSetOn(new Date());
				notePublic.setActive(true);
				notePublic.setPublish(true);
				notePublic.setNote(publicNotes.get(index));
				notePublic.setLabel(publicLabel);
				qitem.getNotes().add(notePublic);
			}

			// set Private note
			if (!StringUtils.isEmpty(privateNotes.get(index))) {
				QuoteItemNote notePrivate = new QuoteItemNote();
				String privateLabel = messages.getMessage("exchangeformat.fieldname.privatenote", null, locale);
				notePrivate.setQuotationitem(qitem);
				notePrivate.setSetBy(person);
				notePrivate.setSetOn(new Date());
				notePrivate.setActive(true);
				notePrivate.setPublish(false);
				notePrivate.setNote(privateNotes.get(index));
				notePrivate.setLabel(privateLabel);
				qitem.getNotes().add(notePrivate);
			}

			// Add catalog price comment as a note
			CatalogPrice catalogPrice = catalogPriceService.findSingle(qitem.getModel(), qitem.getServiceType(), null,
					subDiv.getComp());
			if (catalogPrice != null) {
				String comments = catalogPrice.getComments();
				if (comments != null && !comments.isEmpty()) {
					String label = messages.getMessage("quoteautonote.catalogpricenote", null, locale);
					QuoteItemNote note = new QuoteItemNote();
					note.setActive(true);
					note.setLabel(label);
					note.setNote(comments);
					note.setPublish(true);
					note.setQuotationitem(qitem);
					note.setSetBy(person);
					note.setSetOn(new Date());
					qitem.getNotes().add(note);
				}
			}

			// Add default Procedure description as a note if it isn't
			// already added against this quote.
			String comments = this.qiServ.getServiceCapabilityComment(qitem.getModel(), qitem.getCaltype(), subDiv,
					locale);
			if (comments != null) {
				if (!newQuoteNotesText.contains(comments)
						&& !noteService.isNoteExists(NoteType.QUOTENOTE, quotation.getId(), comments)) {
					String label = messages.getMessage("quoteautonote.procedurenote", null, locale);
					QuoteNote note = new QuoteNote();
					note.setActive(true);
					note.setLabel(label);
					note.setNote(comments);
					note.setPublish(true);
					note.setQuotation(quotation);
					note.setSetBy(person);
					note.setSetOn(new Date());
					newQuoteNotes.add(note);
					newQuoteNotesText.add(comments);
				}
			}
			qItems.add(qitem);
		}

		this.logger.info("Added " + qItems.size()
				+ " quotation items using instrument model / get prices and service types from the file");
		quotation.getNotes().addAll(newQuoteNotes);

		return qItems;
	}

}
