package org.trescal.cwms.core.quotation.entity.quotecaldefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.DefaultCalibrationCondition;

@Entity
@Table(name = "quotecaldefaults", uniqueConstraints = { @UniqueConstraint(columnNames = { "conditionid", "quotationid" }) })
/**
 * Default text values mapped to a quotation.
 */
public class QuoteCalDefaults
{
	private DefaultCalibrationCondition condition;
	private int id;
	private Quotation quotation;

	@ManyToOne(cascade = {}, fetch = FetchType.EAGER)
	@JoinColumn(name = "conditionid", unique = false, nullable = false, insertable = true, updatable = true)
	public DefaultCalibrationCondition getCondition()
	{
		return this.condition;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.EAGER)
	@JoinColumn(name = "quotationid", unique = false, nullable = false, insertable = true, updatable = true)
	public Quotation getQuotation()
	{
		return this.quotation;
	}

	public void setCondition(DefaultCalibrationCondition condition)
	{
		this.condition = condition;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setQuotation(Quotation quotation)
	{
		this.quotation = quotation;
	}

}
