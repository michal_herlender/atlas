package org.trescal.cwms.core.quotation.entity.costs;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.quotation.dto.DiscountSummaryDTO;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

/**
 * Super interface that defines functionality to be shared amongst Quotation
 * Cost services.
 * 
 * @author richard
 */
public interface QuotationCostService<Entity extends Cost> extends BaseService<Entity, Integer>
{
	/**
	 * Return's a list of Costs of the given type entities that belong to
	 * {@link Quotation}s that are linked to the {@link Job} identified by the
	 * given id and items of the matching {@link InstrumentModel} and
	 * {@link CalibrationType}. Sorted by most recent first.
	 * 
	 * @param jobid the id of the {@link Job}.
	 * @param modelid the id of the {@link InstrumentModel} the
	 *        {@link Quotationitem} must belong to.
	 * @param calTypeId the id of the {@link CalibrationType} of the
	 *        {@link Quotationitem}.
	 * @return list of matching {@link QuotationCalibrationCost} entities.
	 */
	List<Entity> findMatchingCostOnLinkedQuotation(int jobid, int modelid, int calTypeId);

	/**
	 * Return's a list of {@link Cost} entities of the given type that belong to
	 * {@link Quotation}s of the matching {@link Company} and items of the
	 * matching {@link InstrumentModel} and {@link CalibrationType}. Sorted by
	 * most recent first. Notes, restrictions are put in place so that only
	 * active costs belonging to {@link Quotation}s from within the last x years
	 * are returned, where x is the (injected) system default property number of
	 * years to search back over quotations.
	 * 
	 * @param coid the id of the {@link Company} the {@link Quotation} must
	 *        belong to.
	 * @param modelid the id of the {@link InstrumentModel} the
	 *        {@link Quotationitem} must belong to.
	 * @param calTypeId the id of the {@link CalibrationType} of the
	 *        {@link Quotationitem}.
	 * @param page the current page of the results, null will not page results.
	 * @param resPerPage the number of results to diplay per page, null will
	 *        default to {@link Constants} system default.
	 * @return list of matching {@link QuotationCalibrationCost} entities.
	 */
	List<Entity> findMatchingCosts(Integer coid, int modelid, Integer calTypeId, Integer page, Integer resPerPage);

	DiscountSummaryDTO getDiscountDTO(int quoteid);
	
	/**
	 * Recalculates the total cost, discount and final cost for the given
	 * {@link Cost}. Different cost types have different rules on how a cost
	 * total is calculatated, therefore, it is up to each specific cost type's
	 * service to provide an implementation for this function that is specific
	 * to how costs are aggregated for this cost type. Does not persist the
	 * resulting object to the database.
	 * 
	 * @param cost the {@link Cost} to update.
	 * @return the {@link Cost}.
	 */
	public Entity updateCosts(Entity cost);
}
