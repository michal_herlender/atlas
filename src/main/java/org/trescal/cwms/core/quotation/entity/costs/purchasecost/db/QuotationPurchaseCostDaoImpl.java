package org.trescal.cwms.core.quotation.entity.costs.purchasecost.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.quotation.entity.costs.QuotationCostDaoSupportImpl;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;

@Repository("QuotationPurchaseCostDao")
public class QuotationPurchaseCostDaoImpl extends QuotationCostDaoSupportImpl<QuotationPurchaseCost> implements QuotationPurchaseCostDao
{
	@Override
	protected Class<QuotationPurchaseCost> getEntity() {
		return QuotationPurchaseCost.class;
	}
}