package org.trescal.cwms.core.quotation.controller;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.QuotationItemComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteheading.comparators.QuotationHeadingSortType;
import org.trescal.cwms.core.quotation.entity.quoteheading.db.QuoteHeadingService;
import org.trescal.cwms.core.quotation.form.QuoteHeadingForm;
import org.trescal.cwms.core.quotation.form.QuoteHeadingValidator;

/**
 * Displays a form to allow creation of a new {@link QuoteHeading}.
 */
@Controller @IntranetController
public class QuoteHeadingController
{
	@Value("${cwms.config.quotation.size.restrictheading}")
	private long sizeRestrictHeading;
	@Autowired
	private QuoteHeadingService qhServ;
	@Autowired
	private QuotationItemService qiServ;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private QuoteHeadingValidator validator;
	@Autowired
	private MessageSource messageSource;
	
	@InitBinder("quoteheadingform")
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	@ModelAttribute("quoteheadingform")
	protected QuoteHeadingForm formBackingObject(
			@RequestParam(name="id", required=true) Integer id) throws Exception
	{
		QuoteHeadingForm qhForm = new QuoteHeadingForm();
		long totalItemCount = qiServ.getItemCount(id);
		// 2016-02-22 - Added check of number of quotation items until performance issues fully resolved 
		if (totalItemCount >= sizeRestrictHeading) {
			throw new RuntimeException("This quotation has "+totalItemCount+" items and due to temporary performance restrictions, quotations with "+sizeRestrictHeading+" items or more cannot have their headings edited.");
		}
		Quotation q = this.quoteServ.get(id);
		qhForm.setQuotation(q);
		QuoteHeading qh = new QuoteHeading();
		qh.setQuotation(q);
		qhForm.setHeader(qh);
		qhForm.setItemInsertOption("none");
		qhForm.setSortType(q.getHeadingSortType());
		return qhForm;
	}
	
	@RequestMapping(value="/quoteheadingform.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(
			@Validated @ModelAttribute("quoteheadingform") QuoteHeadingForm qhf, BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) throws Exception
	{
		if (bindingResult.hasErrors()) {
			return referenceData();
		}
		Quotation quotation = qhf.getQuotation();
		// new heading added?
		if (qhf.getFormfunction().equalsIgnoreCase("newheading")) {
			// get new quote heading from form
			QuoteHeading qh = qhf.getHeader();
			// temporarily set quote heading number
			qh.setHeadingNo(quotation.getQuoteheadings().size() + 1);
			// insert quote heading
			this.qhServ.insert(qh);
			// add quote heading to quotation
			quotation.getQuoteheadings().add(qh);
			// update quotation
			this.quoteServ.update(quotation);
			// this header is being set as default
			if (qhf.isQuoteDefault()) {
				// set this heading as the quote default
				this.qhServ.selectNewDefaultHeading(quotation, qh.getHeadingId());
			}
			// sort quote headings
			this.qhServ.sortQuoteHeadingNos(quotation);
			// update heading items
			this.updateHeadingItems(qhf.getQuotation().getId(), qh, qhf.getItemInsertOption());
		}
		else {
			// sort type on form and quotation different?
			if (qhf.getSortType() != quotation.getHeadingSortType()) {
				// if sorttype changed then add to session so can revert back
				redirectAttributes.addFlashAttribute("prevSortTypeQH", quotation.getHeadingSortType());
				// set sort type
				quotation.setHeadingSortType(qhf.getSortType());
				// update quote
				this.quoteServ.update(quotation);
				// sort quote headings
				this.qhServ.sortQuoteHeadingNos(quotation);
			}
		}
		return new ModelAndView(new RedirectView("quoteheadingform.htm?id=" + qhf.getQuotation().getId()));
	}
	
	@RequestMapping(value="/quoteheadingform.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData() throws Exception
	{
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, String> newHeaderItemOptions = new LinkedHashMap<String, String>();
		newHeaderItemOptions.put("all", this.messageSource.getMessage("quotheadform.optall", null, "Place all items under this heading", LocaleContextHolder.getLocale()));
		newHeaderItemOptions.put("noheader", this.messageSource.getMessage("quotheadform.optnoheader", null, "Place all units currently with no heading under this heading", LocaleContextHolder.getLocale()));
		newHeaderItemOptions.put("none", this.messageSource.getMessage("quotheadform.optnone", null, "Do not place any units under this heading", LocaleContextHolder.getLocale()));
		m.put("newHeaderItemOptions", newHeaderItemOptions);
		m.put("sorttypes", QuotationHeadingSortType.values());
		return new ModelAndView("trescal/core/quotation/quoteheadingform", m);
	}
	
	private void updateHeadingItems(int quoteId, QuoteHeading heading, String toUpdate) throws Exception
	{
		// reload the quotation - can't use the Quotation in the form as the
		// session is closed
		Quotation thisQuote = this.quoteServ.get(quoteId);
		for (Quotationitem qi : thisQuote.getQuotationitems())
			if (toUpdate.equalsIgnoreCase("noheader")) {
				if (qi.getHeading().isSystemDefault()) {
					qi.setHeading(heading);
					if (heading.getQuoteitems() == null)
						heading.setQuoteitems(new TreeSet<Quotationitem>(new QuotationItemComparator()));
					heading.getQuoteitems().add(qi);
				}
			}
			else if (toUpdate.equalsIgnoreCase("all")) {
				qi.setHeading(heading);
				if (heading.getQuoteitems() == null)
					heading.setQuoteitems(new TreeSet<Quotationitem>(new QuotationItemComparator()));
				heading.getQuoteitems().add(qi);
			}
		this.qhServ.update(heading);
		this.quoteServ.update(thisQuote);
		// update the sort order of the items on the quotation
		if (toUpdate.equalsIgnoreCase("all") || toUpdate.equalsIgnoreCase("noheader"))
			this.qiServ.sortQuotationItems(thisQuote);
	}
}