package org.trescal.cwms.core.quotation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.quotation.entity.QuoteRequestType;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequestDetailsSource;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequestStatus;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuoteRequestLogSource;
import org.trescal.cwms.core.quotation.entity.quotationrequest.db.QuotationRequestService;
import org.trescal.cwms.core.quotation.form.QuotationRequestForm;
import org.trescal.cwms.core.quotation.form.QuotationRequestValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.FormFunction;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class QuotationRequestController {
	@Autowired
	private CompanyService companyService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private QuotationRequestService quotationRequestService;
	@Autowired
	private SessionUtilsService sessionUtilsService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private QuotationRequestValidator validator;
	
	public static final String COMMAND_NAME = "command"; 

	@ModelAttribute(COMMAND_NAME)
	protected QuotationRequestForm formBackingObject(
			@RequestParam(name="id", required=false, defaultValue="0") Integer id,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception
	{
		QuotationRequestForm form = new QuotationRequestForm();
		QuotationRequest quotationRequest = this.quotationRequestService.get(id);
		if ((id == 0) || (quotationRequest == null)) {
			Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
			quotationRequest = new QuotationRequest();
			// set defaults
			quotationRequest.setDetailsSource(QuotationRequestDetailsSource.EXISTING);
			quotationRequest.setLogSource(QuoteRequestLogSource.INTERNAL);
			quotationRequest.setStatus(QuotationRequestStatus.REQUESTED);
			quotationRequest.setReqdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			quotationRequest.setOrganisation(allocatedSubdiv.getComp());
			form.setFormFunction(FormFunction.CREATE);
		}
		else form.setFormFunction(FormFunction.EDIT);
		form.setRequest(quotationRequest);
		return form;
	}

	@InitBinder(COMMAND_NAME)
	protected void initBinder(WebDataBinder binder) throws Exception {
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}

	@RequestMapping(value="/quotationrequest.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(COMMAND_NAME) @Validated QuotationRequestForm form, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) {
			return referenceData();
		}
		QuotationRequest quotationRequest = form.getRequest();
		if (quotationRequest.getDetailsSource().equals(QuotationRequestDetailsSource.EXISTING))
		{
			quotationRequest.setCon(this.contactService.get(form.getPersonid()));
			quotationRequest.setComp(this.companyService.get(form.getCoid()));

			// blank out all of the freehand fields
			quotationRequest.setContact("");
			quotationRequest.setCompany("");
			quotationRequest.setEmail("");
			quotationRequest.setFax("");
			quotationRequest.setPhone("");
		}
		int quotationId = quotationRequest.getId();
		if (form.getFormFunction().equals(FormFunction.CREATE)) {
			Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
			String quotationRequestNo = numerationService.generateNumber(NumerationType.QUOTATION_REQUEST, allocatedSubdiv.getComp(), allocatedSubdiv);
			quotationRequest.setRequestNo(quotationRequestNo);
			quotationRequest.setLoggedBy(this.sessionUtilsService.getCurrentContact());
			quotationRequest.setLoggedOn(new Date());
			this.quotationRequestService.save(quotationRequest);
			quotationId = quotationRequest.getId();
		}
		else this.quotationRequestService.merge(quotationRequest);
		return new ModelAndView(new RedirectView("/viewquotationrequest.htm?id="
				+ quotationId + "", true));
	}

	@RequestMapping(value="/quotationrequest.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData() throws Exception {
		Map<String, Object> refData = new HashMap<>();

		// add quote request type values
		refData.put("quotereqtypes", QuoteRequestType.values());

		return new ModelAndView("/trescal/core/quotation/quotationrequest", refData);
	}
}
