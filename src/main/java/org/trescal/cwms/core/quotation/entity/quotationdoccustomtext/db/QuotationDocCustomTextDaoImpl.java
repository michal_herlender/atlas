package org.trescal.cwms.core.quotation.entity.quotationdoccustomtext.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.quotation.entity.quotationdoccustomtext.QuotationDocCustomText;

@Repository
public class QuotationDocCustomTextDaoImpl extends BaseDaoImpl<QuotationDocCustomText, Integer> implements QuotationDocCustomTextDao {
	
	@Override
	protected Class<QuotationDocCustomText> getEntity() {
		return QuotationDocCustomText.class;
	}
}