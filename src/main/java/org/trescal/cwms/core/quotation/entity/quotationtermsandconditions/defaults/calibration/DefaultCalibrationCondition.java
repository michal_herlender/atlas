package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.QuotationCalibrationCondition;
import org.trescal.cwms.core.quotation.entity.quotecaldefaults.QuoteCalDefaults;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "defaultquotationcalibrationcondition")
public class DefaultCalibrationCondition extends Allocated<Company> implements QuotationCalibrationCondition
{
	private CalibrationType caltype;
	private Set<QuoteCalDefaults> defaultcalterms;
	private Set<Translation> conditiontextTranslation;
	private String conditionText;
	private int defCalConId;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "defcalconid", nullable = false, unique = true)
	@Type(type = "int")
	public int getDefCalConId() {
		return this.defCalConId;
	}
	
	@Column(name = "conditiontext", nullable = true, unique = false, length = 2000)
	public String getConditionText() {
		return this.conditionText;
	}
	
	public void setDefCalConId(int defCalConId) {
		this.defCalConId = defCalConId;
	}
	
	public void setConditionText(String conditionText) {
		this.conditionText = conditionText;
	}
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "caltypeid", unique = true, nullable = false, insertable = true, updatable = true)
	public CalibrationType getCaltype() {
		return this.caltype;
	}
	
	@OneToMany(cascade = {}, fetch = FetchType.LAZY, mappedBy = "condition")
	public Set<QuoteCalDefaults> getDefaultcalterms() {
		return this.defaultcalterms;
	}
	
	public void setCaltype(CalibrationType caltype) {
		this.caltype = caltype;
	}
	
	public void setDefaultcalterms(Set<QuoteCalDefaults> defaultcalterms) {
		this.defaultcalterms = defaultcalterms;
	}
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="defaultquotationcalibrationconditiontranslation", joinColumns=@JoinColumn(name="defcalconid"))
	public Set<Translation> getConditiontextTranslation() {
		return conditiontextTranslation;
	}
	
	public void setConditiontextTranslation(Set<Translation> conditiontextTranslation) {
		this.conditiontextTranslation = conditiontextTranslation;
	}
}