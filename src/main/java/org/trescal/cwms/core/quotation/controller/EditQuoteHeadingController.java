package org.trescal.cwms.core.quotation.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteheading.db.QuoteHeadingService;
import org.trescal.cwms.core.quotation.form.QuoteHeadingForm;
import org.trescal.cwms.core.quotation.form.QuoteHeadingValidator;

/**
 * Displays a form to allow editing of a {@link QuoteHeading}.
 * 
 * @author richard
 */
@Controller @IntranetController
public class EditQuoteHeadingController
{
	@Value("${cwms.config.quotation.size.restrictheading}")
	private long sizeRestrictHeading;
	@Autowired
	private QuoteHeadingService qhServ;
	@Autowired
	private QuotationItemService qiServ;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private QuoteHeadingValidator validator;
	
	@InitBinder("quoteheadingform")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("quoteheadingform")
	protected Object formBackingObject(HttpServletRequest req) throws Exception
	{
		QuoteHeadingForm eqf = new QuoteHeadingForm();
		int headingId = ServletRequestUtils.getIntParameter(req, "headingId", 0);

		QuoteHeading heading = this.qhServ.findQuoteHeading(headingId);
		if ((heading == null) || (headingId == 0))
		{
			throw new Exception("Quotation Heading not found");
		}
		eqf.setHeader(heading);
		eqf.setQuoteDefault(heading.isSystemDefault());
		eqf.setFormfunction("editquoteheading");
		return eqf;
	}

	@RequestMapping(value="/editquoteheadingform.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest arg0, @ModelAttribute("quoteheadingform") @Validated QuoteHeadingForm eqf, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) {
			return referenceData(arg0, eqf);
		}
		Quotation q = this.quoteServ.get(eqf.getHeader().getQuotation().getId());

		// has this heading been set as default?
		if (eqf.isQuoteDefault())
		{
			this.qhServ.selectNewDefaultHeading(q, eqf.getHeader().getHeadingId());
		}

		// save the quote heading
		this.qhServ.update(eqf.getHeader());

		// check if any items are being reassigned and if so update their header
		if (eqf.getItems() != null)
		{
			if (eqf.getItems().size() > 0)
			{
				for (QuoteHeading qhead : q.getQuoteheadings())
				{
					Set<Quotationitem> newSortedSet = new TreeSet<Quotationitem>(q.getSortType().getComparator());
					newSortedSet.addAll(qhead.getQuoteitems());
					qhead.setQuoteitems(newSortedSet);
				}

				for (Integer i : eqf.getItems())
				{
					// find the quotation item to move
					Quotationitem qi = this.qiServ.findQuotationItem(i);
					// find old and new headings
					QuoteHeading oldqh = this.qhServ.findQuoteHeading(qi.getHeading().getHeadingId());
					QuoteHeading newqh = this.qhServ.findQuoteHeading(eqf.getAssignToHeading());
					// remove qi from old heading
					oldqh.getQuoteitems().remove(qi);
					// set new heading on quote item
					qi.setHeading(newqh);
					// add qoute item to new heading set of quote items
					newqh.getQuoteitems().add(qi);
					// find any modules attached to this base unit and update
					// their headings as well
					if (!qi.isPartOfBaseUnit())
					{
						if (qi.getModules() != null)
						{
							for (Quotationitem qitem : qi.getModules())
							{
								// remove qi from old heading
								oldqh.getQuoteitems().remove(qitem);
								// set new heading on quote item
								qitem.setHeading(newqh);
								// add qoute item to new heading set of quote
								// items
								newqh.getQuoteitems().add(qitem);
								// update quote heading
								this.qhServ.update(newqh);
								// update quotation item
								this.qiServ.updateQuotationItem(qi);
							}
						}
					}
					// update quote heading
					this.qhServ.update(newqh);
					// update quotation item
					this.qiServ.updateQuotationItem(qi);
				}

				// requires a re-load of the items here - this is a nasty hack
				// put in to
				// prevent errors where the quote was coming back with only 1
				// item (most
				// likely a comparator issue with fields lazily initialised!)
				Set<Quotationitem> theItems = this.qiServ.getItemsForQuotation(q.getId(), null, null);
				q.setQuotationitems(theItems);

				// items have been moved around so update their sort order
				this.qiServ.sortQuotationItems(q);
			}
		}

		return new ModelAndView(new RedirectView("quoteheadingform.htm?id="
				+ eqf.getHeader().getQuotation().getId()));
	}

	@RequestMapping(value="/editquoteheadingform.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(HttpServletRequest req, @ModelAttribute("quoteheadingform") QuoteHeadingForm eqf) throws Exception
	{
		QuoteHeading heading = eqf.getHeader();

		Map<String, Object> m = new HashMap<String, Object>();

		Integer thisHeaderId = null;
		if (heading != null)
		{
			thisHeaderId = heading.getHeadingId();
		}

		// get all the other headings for this quotation except the current
		// heading
		m.put("headings", this.qhServ.findQuotationHeadings(eqf.getHeader().getQuotation().getId(), thisHeaderId));

		return new ModelAndView("trescal/core/quotation/editquoteheading", m);
	}

	public void setQhServ(QuoteHeadingService qhServ)
	{
		this.qhServ = qhServ;
	}

	public void setQiServ(QuotationItemService qiServ)
	{
		this.qiServ = qiServ;
	}

	public void setQuoteServ(QuotationService quoteServ)
	{
		this.quoteServ = quoteServ;
	}
}
