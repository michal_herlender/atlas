package org.trescal.cwms.core.quotation.entity.quotationitem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class QuotationItemOptionDto {
    Integer id;
    Integer quotationItemId;
    String value;
}
