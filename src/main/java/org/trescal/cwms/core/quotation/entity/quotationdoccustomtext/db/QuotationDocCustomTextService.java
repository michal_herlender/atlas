package org.trescal.cwms.core.quotation.entity.quotationdoccustomtext.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.quotation.entity.quotationdoccustomtext.QuotationDocCustomText;
import org.trescal.cwms.core.quotation.form.QuotationDocCustomTextForm;

public interface QuotationDocCustomTextService extends BaseService<QuotationDocCustomText, Integer>
{
	public void editCustomText(QuotationDocCustomTextForm form);
}
