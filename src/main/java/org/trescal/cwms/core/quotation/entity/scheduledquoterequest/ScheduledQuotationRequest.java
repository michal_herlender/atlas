package org.trescal.cwms.core.quotation.entity.scheduledquoterequest;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.enums.QuoteGenerationStrategy;
import org.trescal.cwms.core.quotation.enums.ScheduledQuoteRequestScope;
import org.trescal.cwms.core.quotation.enums.ScheduledQuoteRequestType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Locale;

@Entity
@Table(name = "scheduledquotationrequest")
public class ScheduledQuotationRequest extends Allocated<Subdiv> {
    private boolean cancelled;
    private CalibrationType defaultCalType;
    private int id;
    private boolean processed;
    private Date processedOn;
    private Quotation quotation;
    private Contact quoteContact;
    private Contact requestBy;
    private Date requestOn;
    private ScheduledQuoteRequestType requestType;
    private ScheduledQuoteRequestScope scope;
    private QuoteGenerationStrategy strategy;
    private ScheduledQuotationRequestDateRange dateRange;
    private Locale locale;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "caltypeid")
    public CalibrationType getDefaultCalType() {
        return this.defaultCalType;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    @Column(name = "processedon", columnDefinition = "datetime")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getProcessedOn() {
        return this.processedOn;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quoteid")
    public Quotation getQuotation() {
        return this.quotation;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quotepersonid", nullable = false)
    public Contact getQuoteContact() {
        return this.quoteContact;
    }

    @NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "requestby", nullable = false)
	public Contact getRequestBy()
	{
		return this.requestBy;
	}

	@NotNull
	@Column(name = "requestedon", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getRequestOn()
	{
		return this.requestOn;
	}

	@NotNull
	@Column(name = "requesttype", nullable = false)
	@Enumerated(EnumType.STRING)
	public ScheduledQuoteRequestType getRequestType()
	{
		return this.requestType;
	}

	@NotNull
	@Column(name = "requestscope", nullable = false)
	@Enumerated(EnumType.STRING)
	public ScheduledQuoteRequestScope getScope()
	{
		return this.scope;
	}

	@NotNull
	@Column(name = "strategy", nullable = false, length = 100)
	@Enumerated(EnumType.STRING)
	public QuoteGenerationStrategy getStrategy()
	{
		return this.strategy;
	}

	@Column(name = "cancelled", nullable = false, columnDefinition="tinyint")
	public boolean isCancelled()
	{
		return this.cancelled;
	}

	@Column(name = "processed", nullable = false, columnDefinition="tinyint")
	public boolean isProcessed()
	{
		return this.processed;
	}

	// Stores optional date range (therefore may not exist)
	@OneToOne(mappedBy="request", cascade = CascadeType.ALL, orphanRemoval=true, fetch=FetchType.EAGER)
	public ScheduledQuotationRequestDateRange getDateRange() {
		return dateRange;
	}

	// TODO add @NotNull and database constraint 
	@Column(name = "locale")
	public Locale getLocale() {
		return locale;
	}

	public void setCancelled(boolean cancelled)
	{
		this.cancelled = cancelled;
	}

	public void setDefaultCalType(CalibrationType defaultCalType)
	{
		this.defaultCalType = defaultCalType;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setProcessed(boolean processed)
	{
		this.processed = processed;
	}

	public void setProcessedOn(Date processedOn)
	{
		this.processedOn = processedOn;
	}

	public void setQuotation(Quotation quotation)
	{
		this.quotation = quotation;
	}

	public void setQuoteContact(Contact quoteContact)
	{
		this.quoteContact = quoteContact;
	}

	public void setRequestBy(Contact requestBy)
	{
		this.requestBy = requestBy;
	}

	public void setRequestOn(Date requestOn)
	{
		this.requestOn = requestOn;
	}

	public void setRequestType(ScheduledQuoteRequestType requestType)
	{
		this.requestType = requestType;
	}

	public void setScope(ScheduledQuoteRequestScope scope)
	{
		this.scope = scope;
	}

	public void setStrategy(QuoteGenerationStrategy strategy)
	{
		this.strategy = strategy;
	}

	public void setDateRange(ScheduledQuotationRequestDateRange dateRange) {
		this.dateRange = dateRange;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}
}
