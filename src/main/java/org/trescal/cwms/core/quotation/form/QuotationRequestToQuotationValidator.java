package org.trescal.cwms.core.quotation.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class QuotationRequestToQuotationValidator implements Validator
{
	@Override
	public boolean supports(Class<?> clazz)
	{
		return clazz.isAssignableFrom(QuotationRequestToQuotationForm.class);
	}

	@Override
	public void validate(Object target, Errors errors)
	{
		QuotationRequestToQuotationForm form = (QuotationRequestToQuotationForm) target;
		if (form.getPersonid() == null)
		{
			errors.rejectValue("personid", null, "Please select a contact");
		}
		if (form.getCoid() == null)
		{
			errors.rejectValue("coid", null, "Please select a company");
		}
	}
}
