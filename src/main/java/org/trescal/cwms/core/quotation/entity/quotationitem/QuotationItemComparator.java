package org.trescal.cwms.core.quotation.entity.quotationitem;

import java.util.Comparator;

import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

/**
 * Comparator class for sorting {@link Quotationitem} entities. Sorts by
 * {@link CalibrationType} and then item number.
 * 
 * @author Richard
 */
public class QuotationItemComparator implements Comparator<Quotationitem> {

	@Override
	public int compare(Quotationitem q1, Quotationitem q2) {
		// Currently, the service type is not being set everywhere - TODO change to
		// service type when implementation complete
		if (!q1.getServiceType().getShortName().equals(q2.getServiceType().getShortName()))
			return q1.getServiceType().getShortName().compareTo(q2.getServiceType().getShortName());
		else {
			Integer item1 = q1.getItemno();
			Integer item2 = q2.getItemno();
			if (item1 != item2)
				return item1.compareTo(item2);
			else
				return q1.getId() == null ? -1 : q1.getId().compareTo(q2.getId());
		}
	}
}