package org.trescal.cwms.core.quotation.form;

import java.util.List;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.quotation.enums.NewQuoteInstSortType;
import org.trescal.cwms.spring.model.TriState;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AddInstrumentToQuoteForm
{
	private Integer quotationId;
	private Integer addressid;
	private boolean ascOrder;
	private TriState outOfCal;
	private Integer personid;
	private InstrumentAndModelSearchForm<Instrument> search;
	private NewQuoteInstSortType sortType;
	private Integer subdivid;
	private String submitType;
	// Basket items
	private List<Integer> basketCaltypes;
	private List<String> basketCosts;
	private List<String> basketDefInsts;
	private List<Integer> basketHeadings;
	private List<Integer> basketIds;
	private List<String> basketPlants;
	private List<String> basketSerials;
	private List<String> basketSources;

}