package org.trescal.cwms.core.quotation.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.quotation.entity.priorquotestatus.db.PriorQuoteStatusService;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.quotation.form.EditQuotationForm;
import org.trescal.cwms.core.quotation.form.EditQuotationValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME})
public class EditQuotationController {

	@Autowired
	private AddressService addressService;
	@Autowired
	private ContactService conServ;
	@Autowired
	private PriorQuoteStatusService pqsServ;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private SupportedCurrencyService supportedCurrencyServ;
	@Autowired
	private TaxCalculator taxCalculator;
	@Autowired
	private UserService userService;
	@Autowired
	private EditQuotationValidator validator;

	public static final String FORM_NAME = "quotationform";
	
	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}

	private Quotation getQuotation(int id) {
		Quotation q = this.quoteServ.get(id);
		if ((id == 0) || (q == null)) {
			throw new RuntimeException("Quotation could not be found");
		}
		return q;
	}

	@ModelAttribute(FORM_NAME)
	protected EditQuotationForm formBackingObject(@RequestParam(name = "id") int id) {
		EditQuotationForm quotationform = new EditQuotationForm();
		Quotation q = getQuotation(id);
		quotationform.setStatusid(q.getQuotestatus().getStatusid());
		quotationform.setClientAcceptanceOn(q.getClientAcceptedOn());
		quotationform.setQuotation(q);
		quotationform.setCurrencyCode(q.getCurrency().getCurrencyCode());
		quotationform.setSourcedByPersonid(q.getSourcedBy().getPersonid());
		return quotationform;
	}

	@RequestMapping(value = "editquotation.htm", method = RequestMethod.GET)
	public String referenceData(Model model, @RequestParam(name = "id") int id) {
		Quotation q = getQuotation(id);
		int orgid = q.getOrganisation().getCoid();

		// load a list of contacts to source quotation.  Add existing sourced-by to list, as value may not be active!
		ContactKeyValue currentDto = new ContactKeyValue(q.getSourcedBy().getId(), q.getSourcedBy().getName());
		List<ContactKeyValue> companyContacts = this.conServ.getContactDtoList(orgid, null, true, currentDto);
		model.addAttribute("businessContactList", companyContacts);
		// load a list of quotestatus
		model.addAttribute("quoteStatusList", this.statusServ.getAllStatuss(QuoteStatus.class));
		// load a list of addresses of the business company that the quotation belongs to 
		model.addAttribute("quoteAdresseList", addressService.getAllCompanyAddresses(orgid, AddressType.WITHOUT, true));
		// add a list of currency options for the company the quote is being generated for
		model.addAttribute("currencyopts", this.supportedCurrencyServ
				.getCompanyCurrencyOptions(q.getContact().getSub().getComp().getCoid()));
		return "trescal/core/quotation/editquotation";
	}
	
	@RequestMapping(value="editquotation.htm", method = RequestMethod.POST)
	public String onSubmit(Model model,
						   @RequestParam(name = "id") int id,
						   @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
						   @ModelAttribute("quotationform") @Validated EditQuotationForm eqf, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData(model, id);
		}
		Quotation q = eqf.getQuotation();
		q.setSourcedBy(this.conServ.get(eqf.getSourcedByPersonid()));
		q.setSourceAddress(addressService.get(eqf.getSourceAddressid()));
		// get the user performing the update
		Contact currentContact = this.userService.get(username).getCon();
		// get the current and new Quotation Status
		QuoteStatus qs = this.statusServ.findStatus(q.getQuotestatus().getStatusid(), QuoteStatus.class);
		QuoteStatus qsNew = this.statusServ.findStatus(eqf.getStatusid(), QuoteStatus.class);
		// update the issued - if it is issued...
		if (!q.isIssued() && qs.getIssued()) {
            q.setIssueby(currentContact);
            q.setIssued(true);
            q.setIssuedate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
        }

		if (qsNew.getAccepted())
			q.setClientAcceptedOn(eqf.getClientAcceptanceOn());
			// when the status of the quotation is 'lost' the user wants
		// to record the date of the response of his customer 
		// (that's why we keep the value of clientAcceptdOn)
		else if (qsNew.getName().equals("Lost") && eqf.getClientAcceptanceOn() != null ){
			q.setAcceptedOn(eqf.getClientAcceptanceOn());
		} else 
			q.setClientAcceptedOn(null);
		
		// update the currency
		this.supportedCurrencyServ.setCurrencyFromForm(q, eqf.getCurrencyCode(), q.getContact().getSub().getComp());
		// user is applying a global discount
		if (eqf.isApplyDiscount()) {
			/*
			 * @TODO we need a set of generic methods to apply item level
			 *       discounts, calc costs etc i.e. here we need to get the
			 *       total, work out and set the discount value, update the
			 *       total cost to minus this. Should be a service or
			 *       PricingItem service method. Can we extend a Service?
			 */
			for (Quotationitem qi : q.getQuotationitems()) {
				qi.setGeneralDiscountRate(eqf.getGlobalDiscount());
				CostCalculator.updateItemCostWithRunningTotal(qi);
			}
        }
        // check to see if the contact has been changed
        if (eqf.getPersonid() != null) {
            // set the new contact
            q.setContact(this.conServ.get(eqf.getPersonid()));
        }
        // calculate vat and final amount, as vat rate may have changed
        CostCalculator.updatePricingTotalCost(q, q.getQuotationitems());
		taxCalculator.calculateAndSetVATaxAndFinalCost(q);

        // always update the expiry date
        q.setExpiryDate(this.quoteServ.getExpiryDate(q));
        // update the quotation status history - i.e. create a new
        // priorquotestatus entry if this quotation's status has changed
        this.pqsServ.updateQuoteStatus(q, q.getQuotestatus().getStatusid(), eqf.getStatusid(), currentContact);
        // actually perform the quotation status update
        q.setQuotestatus(qsNew);
        // persist the quotation
        this.quoteServ.merge(q);
        return "redirect:viewquotation.htm?id=" + q.getId();
    }
}