package org.trescal.cwms.core.quotation.entity.quoteheading.db;

import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;

/**
 * Interface for accessing and manipulating {@link QuoteHeading} entities.
 * 
 * @author Richard
 */
public interface QuoteHeadingService
{
	/**
	 * Delete the given {@link QuoteHeading}.
	 * 
	 * @param qh the {@link QuoteHeading} to delete.
	 */
	void delete(QuoteHeading qh);

	/**
	 * Returns a list of all {@link QuoteHeading}(s) identified by the given
	 * list of heading ids.
	 * 
	 * @param ids list of heading ids, not null.
	 * @return list of {@link QuoteHeading}(s)
	 */
	List<QuoteHeading> findAll(List<Integer> ids);

	/**
	 * Gets the default {@link QuoteHeading} for the {@link Quotation}
	 * identified by the given id.
	 * 
	 * @param quoteid the id of the {@link Quotation}, not null.
	 * @return the default {@link QuoteHeading}
	 */
	QuoteHeading findDefaultQuoteHeading(int quoteid);

	/**
	 * Returns a set of all {@link QuoteHeading}(s) belonging to the
	 * {@link Quotation} identified by the given id.
	 * 
	 * @param quoteid the id of the {@link Quotation}.
	 * @return list of {@link QuoteHeading}(s)
	 */
	Set<QuoteHeading> findQuotationHeadings(int quoteid);

	/**
	 * Returns all {@link QuoteHeading} belonging to the given {@link Quotation}
	 * id with the exception of the {@link QuoteHeading} identified by the
	 * excludeHeaderId.
	 * 
	 * @param quoteid the {@link Quotation} id.
	 * @param excludeHeaderId id of {@link QuoteHeading} to exclude from results
	 *        (nullable).
	 * @return {@link Set} of {@link QuoteHeading}.
	 */
	Set<QuoteHeading> findQuotationHeadings(int quoteid, Integer excludeHeaderId);

	/**
	 * Get the {@link QuoteHeading} identified by the given id.
	 * 
	 * @param headingId the id of the {@link QuoteHeading}, not null.
	 * @return {@link QuoteHeading}
	 */
	QuoteHeading findQuoteHeading(int headingId);

	/**
	 * This method finds a quote heading for a quotation using the heading
	 * number
	 * 
	 * @param quotationId id of the {@link Quotation}
	 * @param headingNo id of the {@link QuoteHeading}
	 * @return {@link ResultWrapper}
	 */
	QuoteHeading findQuoteheadingByItemNo(int quotationId, int headingNo);

	/**
	 * Get the system default {@link QuoteHeading}(s) belonging to the
	 * {@link Quotation} identified by the given id.
	 * 
	 * @param quoteid the id of the {@link Quotation}, not null.
	 * @return the system default {@link QuoteHeading}(s)
	 */
	QuoteHeading findSystemDefaultQuoteHeading(int quoteid);

	/**
	 * Creates a default heading for the quotation, but does NOT save it
	 * (the calling quotation may not be saved yet.)
	 * The calling quotation owns the heading and should cascade save / persist it. 
	 * @param q
	 * @return
	 */
	QuoteHeading createDefaultHeading(Quotation q, Locale locale);

	/**
	 * Creates a default heading for the quotation, based on the old one, but does NOT save it
	 * The calling quotation owns the heading and should cascade save / persist it. 
	 * @param quote
	 * @param newQuote
	 */
	QuoteHeading copyDefaultHeading(Quotation quote, Quotation newQuote);
	
	/**
	 * Inserts the given {@link QuoteHeading}.
	 * 
	 * @param qh {@link QuoteHeading}.
	 */
	void insert(QuoteHeading qh);

	/**
	 * Move a quotation heading within a quotation
	 * 
	 * @param oldPosId the id of the heading to be moved
	 * @param newPosId the id of the heading where the moved heading is to be
	 *        moved to
	 * @param quotationId id of the quotation in which the heading is to be
	 *        moved
	 * @return {@link ResultWrapper}
	 * @throws Exception
	 */
	ResultWrapper moveQuoteHeading(int oldPosId, int newPosId, int quotationId) throws Exception;

	/**
	 * Saves or updates all {@link QuoteHeading}(s) in the given list.
	 * 
	 * @param headings list of {@link QuoteHeading}, not null.
	 */
	void saveOrUpdateAll(List<QuoteHeading> headings);
	
	/**
	 * Updates the QuoteHeadings attached to the quotation to set the specified
	 * heading as being the (potentially updated) system default heading
	 * @param quotation
	 * @param headingId
	 */
	void selectNewDefaultHeading(Quotation quotation, int headingId);

	/**
	 * This method sorts the headings on a {@link Quotation} using the latest
	 * sort type comparator
	 * 
	 * @param q the {@link Quotation} to sort quote headings
	 * @return {@link Quotation}
	 */
	Quotation sortQuoteHeadingNos(Quotation q);

	/**
	 * Updates the given {@link QuoteHeading}.
	 * 
	 * @param qh
	 */
	void update(QuoteHeading qh);
}
