package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general.db;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general.DefaultGeneralCondition;

public interface DefaultGeneralConditionDao extends AllocatedDao<Company, DefaultGeneralCondition, Integer>
{
	/**
	 * @return latest terms and conditions
	 */
	DefaultGeneralCondition getLatest(Company allocatedCompany);
}