package org.trescal.cwms.core.quotation.entity.quotecaltypedefault.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.quotation.entity.quotecaltypedefault.QuoteCaltypeDefault;

public interface QuoteCaltypeDefaultDao extends BaseDao<QuoteCaltypeDefault, Integer> {}