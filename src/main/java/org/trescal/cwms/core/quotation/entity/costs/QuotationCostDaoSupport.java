package org.trescal.cwms.core.quotation.entity.costs;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.quotation.dto.DiscountSummaryDTO;

public interface QuotationCostDaoSupport<Entity extends Cost> extends BaseDao<Entity, Integer>
{
	List<Entity> findMatchingCostOnLinkedQuotation(int jobid, int modelid, int calTypeId);

	List<Entity> findMatchingCosts(Integer coid, int modelid, Integer calTypeId, Integer resultsYearFilter, Integer page, Integer resPerPage);

	DiscountSummaryDTO getDiscountDTO(int quoteid);
}
