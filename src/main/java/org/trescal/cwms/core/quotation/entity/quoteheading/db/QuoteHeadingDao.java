package org.trescal.cwms.core.quotation.entity.quoteheading.db;

import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;

public interface QuoteHeadingDao extends BaseDao<QuoteHeading, Integer> {
	
	List<QuoteHeading> findAll(List<Integer> ids);
	
	QuoteHeading findDefaultQuoteHeading(int quoteid);
	
	Set<QuoteHeading> findQuotationHeadings(int quoteid);
	
	Set<QuoteHeading> findQuotationHeadings(int quoteid, Integer excludeHeaderId);
	
	QuoteHeading findQuoteheadingByItemNo(int quotationId, int headingNo);
	
	QuoteHeading findSystemDefaultQuoteHeading(int quoteid);
}