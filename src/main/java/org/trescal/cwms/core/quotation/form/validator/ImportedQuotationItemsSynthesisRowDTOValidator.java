package org.trescal.cwms.core.quotation.form.validator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;
import org.trescal.cwms.core.instrument.entity.instrument.PossibleInstrumentDTO;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.form.ImportedQuotationItemsSynthesisForm.ImportedQuotationItemsSynthesisRowDTO;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class ImportedQuotationItemsSynthesisRowDTOValidator extends AbstractBeanValidator implements SmartValidator {

	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private MessageSource messageSource;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(ImportedQuotationItemsSynthesisRowDTO.class);
	}

	@Override
	public void validate(Object target, Errors errors, Object... validationHints) {

		ImportedQuotationItemsSynthesisRowDTO dto = (ImportedQuotationItemsSynthesisRowDTO) target;

		Quotation quotation = (Quotation) validationHints[0];
		// all possible instrument from db
		List<PossibleInstrumentDTO> possibleInstruments = (List<PossibleInstrumentDTO>) validationHints[1];

		boolean duplicatePlantNoInDatabase = possibleInstruments.stream()
				.filter(p -> p.getPlantno().equals(dto.getPlantNo())).collect(Collectors.toList()).size() > 1;

		List<String> plantnos = new ArrayList<>();
		List<Integer> plantids = new ArrayList<>();
		// add to list possible plant no
		possibleInstruments.stream().forEach(p -> plantnos.add(p.getPlantno()));
		// add to list possible plant id
		possibleInstruments.stream().forEach(p -> plantids.add(p.getPlantid()));

		dto.setErrorMessages(new ArrayList<>());

		if (dto.getPlantId() != null) {
			if (instrumentService.get(dto.getPlantId()) == null) {
				dto.getErrorMessages().add(messageSource.getMessage("error.instrument.plantid.notfound",
						new Object[] {String.valueOf(dto.getPlantId())}, LocaleContextHolder.getLocale()));
			} else if (!plantids.contains(dto.getPlantId())) {
				dto.getErrorMessages().add(messageSource.getMessage("error.instrument.notavailable", null,
						LocaleContextHolder.getLocale()));
			} else if (getQuoteItemIds(quotation).contains(dto.getPlantId())) {
				dto.getErrorMessages().add(
						messageSource.getMessage("error.instrument.onquote", null, LocaleContextHolder.getLocale()));
			}

		} else if (StringUtils.isNoneBlank(dto.getPlantNo())) {
			if (!plantnos.contains(dto.getPlantNo())) {
				dto.getErrorMessages().add(messageSource.getMessage("error.instrument.notavailable", null,
						LocaleContextHolder.getLocale()));
			} else if (duplicatePlantNoInDatabase) {
				dto.getErrorMessages().add(messageSource.getMessage("error.instrument.duplicateplantno", null,
						LocaleContextHolder.getLocale()));
			} else if (getQuoteItemIds(quotation).contains(possibleInstruments.stream()
					.filter(p -> p.getPlantno().equals(dto.getPlantNo())).findFirst().get().getPlantid())) {
				dto.getErrorMessages().add(
						messageSource.getMessage("error.instrument.onquote", null, LocaleContextHolder.getLocale()));
			}
			// set plant id if no error found
			else if (!possibleInstruments.isEmpty())
				dto.setPlantId(possibleInstruments.stream().filter(p -> p.getPlantno().equals(dto.getPlantNo()))
						.findFirst().get().getPlantid());
		}
	}

	private List<Integer> getQuoteItemIds(Quotation quotation) {
		// create list for quotation instruments
		List<Integer> qinstIds = new ArrayList<Integer>();
		// add all existing quote instruments to list on form
		for (Quotationitem qi : quotation.getQuotationitems()) {
			// instrument?
			if (qi.getInst() != null) {
				// add to list
				qinstIds.add(qi.getInst().getPlantid());
			}
		}
		return qinstIds;
	}
}
