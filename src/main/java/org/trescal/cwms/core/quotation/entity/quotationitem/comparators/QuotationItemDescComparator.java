package org.trescal.cwms.core.quotation.entity.quotationitem.comparators;

import java.util.Comparator;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;

public class QuotationItemDescComparator implements Comparator<Quotationitem>
{
	@Override
	public int compare(Quotationitem o1, Quotationitem o2)
	{
		// quote item 1
		InstrumentModel qi1model = null;
		Mfr qi1mfr = null;
		// quote item 2 model
		InstrumentModel qi2model = null;
		Mfr qi2mfr = null;
		// first qi has instrument?
		if (o1.getInst() != null)
		{
			// get model from instrument
			qi1model = o1.getInst().getModel();
			// check mfr type
			if (qi1model.getModelMfrType().isMfrRequiredForModel())
			{
				qi1mfr = qi1model.getMfr();
			}
			else
			{
				qi1mfr = o1.getInst().getMfr();
			}
		}
		else
		{
			// get model and mfr from model on quote item
			qi1model = o1.getModel();
			qi1mfr = qi1model.getMfr();
		}
		// second qi has instrument?
		if (o2.getInst() != null)
		{
			// get model from instrument
			qi2model = o2.getInst().getModel();
			// check mfr type
			if (qi2model.getModelMfrType().isMfrRequiredForModel())
			{
				qi2mfr = qi2model.getMfr();
			}
			else
			{
				qi2mfr = o2.getInst().getMfr();
			}
		}
		else
		{
			// get model and mfr from model on quote item
			qi2model = o2.getModel();
			qi2mfr = qi2model.getMfr();
		}
		// compare items
		if (qi1model.getModelid() == qi2model.getModelid())
		{
			// models are identical so just order by the id
			return ((Integer) o1.getId()).compareTo(o2.getId());
		}
		else
		{
			if (!qi1model.getDescription().getDescription().equalsIgnoreCase(qi2model.getDescription().getDescription()))
			{
				return qi1model.getDescription().getDescription().compareToIgnoreCase(qi2model.getDescription().getDescription());
			}
			else if ((qi1mfr != null) && (qi2mfr != null)
					&& !qi1mfr.getName().equalsIgnoreCase(qi2mfr.getName()))
			{
				return qi1mfr.getName().compareToIgnoreCase((qi2mfr.getName()));
			}
			else if (!qi1model.getModel().equalsIgnoreCase(qi2model.getModel()))
			{
				return qi1model.getModel().compareToIgnoreCase(qi2model.getModel());
			}
			else
			{
				return ((Integer) qi1model.getModelid()).compareTo(qi2model.getModelid());
			}
		}
	}
}
