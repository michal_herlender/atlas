package org.trescal.cwms.core.quotation.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.comparators.QuotationItemSortType;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.form.EditQuotationItemSortControllerForm;

@Controller @IntranetController
public class EditQuotationItemSortController
{
	@Value("${cwms.config.quotation.size.restrictsorting}")
	private long sizeRestrictSorting;
	@Autowired
	private QuotationItemService qiServ;
	@Autowired
	private QuotationService quoteServ;
	
	protected final Logger logger = LoggerFactory.getLogger(EditQuotationItemSortController.class);

	@ModelAttribute("command")
	protected EditQuotationItemSortControllerForm formBackingObject(@RequestParam(name="quoteid", required=true) int quoteId) throws Exception
	{
		long totalItemCount = qiServ.getItemCount(quoteId);
		// 2016-02-22 - Added check of number of quotation items until performance issues fully resolved 
		if (totalItemCount >= sizeRestrictSorting) {
			throw new RuntimeException("This quotation has "+totalItemCount+" items and due to temporary performance restrictions, quotations with "+sizeRestrictSorting+" items or more cannot have their sorting changed.");
		}
		
		Quotation quotation = this.quoteServ.get(quoteId);

		if (quotation == null)
		{
			throw new RuntimeException("Could not find quotation");
		}

		EditQuotationItemSortControllerForm form = new EditQuotationItemSortControllerForm();
		form.setQuotation(quotation);
		return form;
	}

	@RequestMapping(value="/quotesort.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(HttpSession session, @ModelAttribute("command") EditQuotationItemSortControllerForm form, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) {
			return new ModelAndView("trescal/core/quotation/quotesort", "command", form);
		}
		// reload the quotation so we don't get any lazy-init errors
		Quotation quotation = this.quoteServ.get(form.getQuotation().getId());

		// if sorttype changed then add to session so can revert back
		session.setAttribute(quotation.getId() + "_prevSortTypeQuotation", quotation.getSortType());
		quotation.setSortType(form.getSortType());

		this.quoteServ.merge(quotation);
		this.qiServ.sortQuotationItems(quotation);

		return new ModelAndView(new RedirectView("/quotesort.htm?quoteid="
				+ quotation.getId(), true));
	}

	@RequestMapping(value="/quotesort.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(HttpServletRequest request) throws Exception
	{
		Map<String, Object> refData = new HashMap<String, Object>();
		refData.put("sorttypes", QuotationItemSortType.values());
		return new ModelAndView("trescal/core/quotation/quotesort", refData);
	}
}
