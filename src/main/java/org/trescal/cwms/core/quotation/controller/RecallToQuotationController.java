package org.trescal.cwms.core.quotation.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.form.RecallToQuotationForm;
import org.trescal.cwms.core.quotation.form.RecallToQuotationValidator;
import org.trescal.cwms.core.quotation.service.RecallToQuotationService;
import org.trescal.cwms.core.recall.entity.recall.RecallType;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.recall.entity.recalldetail.db.RecallDetailService;
import org.trescal.cwms.core.recall.entity.recallitem.db.RecallItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class RecallToQuotationController
{
	@Autowired
	private ContactService contactService;
	@Autowired
	private RecallDetailService rdServ;
	@Autowired
	private RecallItemService recallitemServ;
	@Autowired
	private RecallToQuotationService recallToQuotationService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SupportedCurrencyService supportedCurrencyServ;
	@Autowired
	private UserService userService;
	@Autowired
	private RecallToQuotationValidator validator;
	
	public static final String FORM_NAME = "form";
	
	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	@ModelAttribute(FORM_NAME)
	protected RecallToQuotationForm formBackingObject(@RequestParam(name = "rdid") int rdid,
                                                      @RequestParam(name = QuotationAddFormController.REDIRECT_ATTRIBUTE_CLIENTREF, required = false) String clientRef,
                                                      @RequestParam(name = QuotationAddFormController.REDIRECT_ATTRIBUTE_CONTACT_ID, required = false, defaultValue = "0") Integer contactid,
                                                      @RequestParam(name = QuotationAddFormController.REDIRECT_ATTRIBUTE_CURRENCYCODE, required = false) String currencyCode,
                                                      @RequestParam(name = QuotationAddFormController.REDIRECT_ATTRIBUTE_REQDATE, required = false)  LocalDate reqdate,
                                                      @RequestParam(name = QuotationAddFormController.REDIRECT_ATTRIBUTE_SOURCEDBY_ID, required = false, defaultValue = "0") Integer sourcedbyid
    ) throws Exception
	{
		// create new form object
		RecallToQuotationForm form = new RecallToQuotationForm();
		// values present indicating redirect?
		if (contactid != 0)
		{
			// set from recall to false as we have come from create quote
			form.setFromRecall(false);
			form.setClientRef(clientRef);
			form.setContact(contactService.get(contactid));
			form.setCurrencyCode(currencyCode);
			form.setReqDate(reqdate);
			if (sourcedbyid != 0) form.setSourcedBy(contactService.get(sourcedbyid));
		}
		else {
            // directed from view recalls
            form.setFromRecall(true);
            // set todays date
            form.setReqDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
        }

		// find the recall detail
		RecallDetail rdetail = this.rdServ.get(rdid);
		// check recall detail can be loaded
		if ((rdid == 0) || (rdetail == null))
		{
			throw new Exception("Recall details could not be found");
		}
		// set recall detail on form
		form.setRdetail(rdetail);
		// what type of recall is this?
		if (rdetail.getRecallType().equals(RecallType.CONTACT))
		{
			// get all instruments recalled for contact and add recall items to
			// form
			form.setRecallitems(this.recallitemServ.getRecallItemsForContact(rdetail.getRecall().getId(), rdetail.getContact().getPersonid()));
		}
		else
		{
			// get all instruments recalled for address and add recall items to
			// form
			form.setRecallitems(this.recallitemServ.getRecallItemsForAddress(rdetail.getRecall().getId(), rdetail.getAddress().getAddrid()));
		}

		return form;
	}

	@RequestMapping(value="/recalltoquotation.htm", method=RequestMethod.POST)
	public ModelAndView onSubmit(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto,
			@ModelAttribute(FORM_NAME) @Validated RecallToQuotationForm form, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) return referenceData(form);
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Contact currentContact = this.userService.get(username).getCon();
		Quotation quotation = this.recallToQuotationService.createQuotationFromRecall(form, allocatedSubdiv, currentContact);
		return new ModelAndView(new RedirectView("viewquotation.htm?id="
				+ quotation.getId(), true));
	}

	@RequestMapping(value="/recalltoquotation.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute(FORM_NAME) RecallToQuotationForm form) throws Exception
	{
        HashMap<String, Object> map = new HashMap<>();

        map.put("currencyopts", this.supportedCurrencyServ.getCompanyCurrencyOptions(form.getRdetail().getContact().getSub().getComp().getCoid()));
        map.put("today", LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
        return new ModelAndView("trescal/core/quotation/recalltoquotation", map);
    }
}