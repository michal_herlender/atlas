package org.trescal.cwms.core.quotation.entity.costs.calcost.db;

import org.trescal.cwms.core.quotation.entity.costs.QuotationCostDaoSupport;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;

public interface QuotationCalibrationCostDao extends QuotationCostDaoSupport<QuotationCalibrationCost>
{
	QuotationCalibrationCost findEagerQuotationCalibrationCost(int id);
}