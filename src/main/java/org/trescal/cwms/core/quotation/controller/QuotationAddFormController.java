package org.trescal.cwms.core.quotation.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.form.QuoteCompanyInstrumentsForm;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.form.JobSearchForm;
import org.trescal.cwms.core.jobs.job.projection.JobProjectionDTO;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.enums.QuoteCreationType;
import org.trescal.cwms.core.quotation.form.AddQuotationForm;
import org.trescal.cwms.core.quotation.form.AddQuotationValidator;
import org.trescal.cwms.core.quotation.service.CompanyInstrumentsToQuotationService;
import org.trescal.cwms.core.recall.entity.recalldetail.db.RecallDetailService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

import lombok.val;

/**
 * Displays a form allowing users to select the client reference, sourced-by and
 * request date properties for a new {@link Quotation} before saving the new
 * {@link Quotation} to the database. If the save is successful then the user is
 * redirected to the {@link ViewQuotationController} view.
 */
@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_SUBDIV})
public class QuotationAddFormController
{
	@Autowired
	private CompanyInstrumentsToQuotationService companyInstrumentsToQuotationServ;
	@Autowired
	private CompanyService compServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private JobService jobService;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private RecallDetailService rdServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SupportedCurrencyService supportedCurrencyServ;
	@Autowired
	private UserService userService;
	
	@Autowired
	private AddQuotationValidator validator;
	@Autowired
	protected MessageSource messageSource;
	
	public static final String REDIRECT_ATTRIBUTE_CLIENTREF = "clientref";
	public static final String REDIRECT_ATTRIBUTE_CONTACT_ID = "contactid";
	public static final String REDIRECT_ATTRIBUTE_CURRENCYCODE = "currencycode";
	public static final String REDIRECT_ATTRIBUTE_SOURCEDBY_ID = "sourcedbyid";
	public static final String REDIRECT_ATTRIBUTE_REQDATE = "reqdate";
	public static final String FORM_NAME = "addquoteform";
	
	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

	protected int createQuote(AddQuotationForm aqf, boolean saveInstruments, Contact sourcedBy, Subdiv allocatedSubdiv, Contact currentContact, Locale locale) {
		// create an empty quote
		Contact quoteContact = aqf.getContact();
		Quotation quotation = this.quoteServ.createTemplateNewQuotation(currentContact, quoteContact, allocatedSubdiv, allocatedSubdiv.getComp(), locale);
		quotation.setClientref(aqf.getClientRef());
		quotation.setReqdate(aqf.getReqDate());
		quotation.setSourcedBy(sourcedBy);
		this.supportedCurrencyServ.setCurrencyFromForm(quotation, aqf.getCurrencyCode(), quotation.getContact().getSub().getComp());
		if (saveInstruments) {
			this.companyInstrumentsToQuotationServ.prepareAndSaveCompanyInstrumentsToQuotation(aqf, quotation, allocatedSubdiv, currentContact);
		}
		this.quoteServ.save(quotation);
		return quotation.getId();
	}

	protected void 	createQuoteFromJobOrRecall(RedirectAttributes redirectAttributes, AddQuotationForm aqf) {
		// Regular redirect attributes used instead of flash attributes
		// because validation / rebinding upon validation errors is easier in second controller this way
		redirectAttributes.addAttribute(REDIRECT_ATTRIBUTE_CLIENTREF, aqf.getClientRef());
		redirectAttributes.addAttribute(REDIRECT_ATTRIBUTE_CONTACT_ID, aqf.getContact().getId());
		redirectAttributes.addAttribute(REDIRECT_ATTRIBUTE_CURRENCYCODE, aqf.getCurrencyCode());
		redirectAttributes.addAttribute(REDIRECT_ATTRIBUTE_REQDATE, aqf.getReqDate());
		redirectAttributes.addAttribute(REDIRECT_ATTRIBUTE_SOURCEDBY_ID, aqf.getSourcedBy());
	}
	
	@ModelAttribute(FORM_NAME)
	protected AddQuotationForm formBackingObject(HttpSession session,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer,String> companyDto,
			@RequestParam(name="personid") Integer personid,
			@RequestParam(name="formid", required=false, defaultValue="") String formid) {

		AddQuotationForm aqf = new AddQuotationForm();
		Contact currentContact = this.userService.get(username).getCon();
		Contact clientContact = this.conServ.get(personid);
		Company allocatedCompany = this.compServ.get(companyDto.getKey());

		aqf.setContact(clientContact);
		aqf.setCurrencyCode(allocatedCompany.getCurrency().getCurrencyCode());
		aqf.setReqDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		aqf.setSourcedBy(currentContact.getSub().getComp().getCoid() == companyDto.getKey() ? currentContact.getId() : 0);
		// check for formid passed when creating quotation from company
		// instrument list
		if (!formid.equals("")) {
			aqf.setCreationType(QuoteCreationType.FROM_COMPINSTLIST);
			// retrieve update instruments form from session using form id
			// passed in
			// url
			QuoteCompanyInstrumentsForm qciform = (QuoteCompanyInstrumentsForm) session.getAttribute(formid);
			// set onto quote add form
			aqf.setQcif(qciform);
		}
		else
		{
			aqf.setCreationType(QuoteCreationType.EMPTY);
		}

		return aqf;
	}

	@RequestMapping(value="/quoteform.htm", method=RequestMethod.POST)
	public ModelAndView onSubmit(Locale locale,
			RedirectAttributes redirectAttributes,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(FORM_NAME) @Validated AddQuotationForm aqf, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) {
			return referenceData(locale, companyDto, aqf);
		}
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());

		Contact sourcedBy = this.conServ.get(aqf.getSourcedBy());
		Contact currentContact = this.userService.get(username).getCon();
		if (aqf.getCreationType() == null) aqf.setCreationType(QuoteCreationType.EMPTY);
		int quoteId = 0;
		switch (aqf.getCreationType()) {
			case EMPTY:
				quoteId = this.createQuote(aqf, false, sourcedBy, allocatedSubdiv, currentContact, locale);
				break;
			case FROM_COMPINSTLIST:
				quoteId = this.createQuote(aqf, true, sourcedBy, allocatedSubdiv, currentContact, locale);
				break;
			case FROM_JOB:
				// transfer attributes using redirect attributes
				this.createQuoteFromJobOrRecall(redirectAttributes, aqf);
				return new ModelAndView(new RedirectView("/jobtoquotation.htm?jobid=" + aqf.getQuoteFromJobId(), true));
			case FROM_RECALL:
				// transfer attributes using redirect attributes
				this.createQuoteFromJobOrRecall(redirectAttributes, aqf);
				return new ModelAndView(new RedirectView("/recalltoquotation.htm?rdid=" + aqf.getQuoteFromRecallId(), true));
		}
		return new ModelAndView(new RedirectView("viewquotation.htm?id=" + quoteId, true));
	}
	
	@RequestMapping(value="/quoteform.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(FORM_NAME) AddQuotationForm aqf) throws Exception {
		Map<String, Object> refData = new HashMap<>();
		ContactKeyValue noneDto = new ContactKeyValue(0, this.messageSource.getMessage("company.none", null, "None", locale));
		refData.put("businessContactList", this.conServ.getContactDtoList(companyDto.getKey(), null, true, noneDto));
		refData.put("currencyopts", this.supportedCurrencyServ.getCompanyCurrencyOptions(companyDto.getKey()));
		int RES_PER_PAGE = 50;
		refData.put("resultsperpage", RES_PER_PAGE);
		// get the 50 most recent jobs for this subdiv
		JobSearchForm jsf = new JobSearchForm();
		jsf.setCoid(String.valueOf(aqf.getContact().getSub().getComp().getCoid()));
		jsf.setResultsPerPage(RES_PER_PAGE);
		jsf.setPageNo(1);
		val prs = new PagedResultSet<JobProjectionDTO>(jsf.getResultsPerPage(), jsf.getPageNo());
		this.jobService.queryJobJPANew(jsf, LocaleContextHolder.getLocale(), prs);
		refData.put("jobs", prs);
		// size as it's rare this will ever even have any results
		// grab all distinct recalls for this contact
		refData.put("rdetails", this.rdServ.getRecallDetailsForContact(aqf.getContact().getPersonid()));
		refData.put("today", LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		return new ModelAndView("trescal/core/quotation/quoteform", refData);
	}
}