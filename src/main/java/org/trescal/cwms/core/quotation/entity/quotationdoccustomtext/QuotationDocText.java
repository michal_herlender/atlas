package org.trescal.cwms.core.quotation.entity.quotationdoccustomtext;

public interface QuotationDocText {
	public String getSubject();
	public String getBody();
}
