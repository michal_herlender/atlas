package org.trescal.cwms.core.quotation.entity.quotationitem.comparators;

import java.util.Comparator;

import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeadingComparator;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationTypeComparator;

public class QuotationItemHeadingCaltypeOrderbyComparator implements Comparator<Quotationitem>
{
	private QuotationItemSortType sortType;

	public QuotationItemHeadingCaltypeOrderbyComparator(QuotationItemSortType sortType)
	{
		this.sortType = sortType;
	}

	@Override
	public int compare(Quotationitem item1, Quotationitem item2)
	{
		if (item1.getHeading().getHeadingId() != item2.getHeading().getHeadingId())
		{
			// delegate to a quotation heading comparator to sort
			return new QuoteHeadingComparator().compare(item1.getHeading(), item2.getHeading());
		}
		else if (item1.getCaltype().getCalTypeId() != item2.getCaltype().getCalTypeId())
		{
			// delegate to a calibration type comparator
			return new CalibrationTypeComparator().compare(item1.getCaltype(), item2.getCaltype());
		}
		else
		{
			// otherwise use the default comparaotr for this quotation
			return this.sortType.getComparator().compare(item1, item2);
		}
	}
}
