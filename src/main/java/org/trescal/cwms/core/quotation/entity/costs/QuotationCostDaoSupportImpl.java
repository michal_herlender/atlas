package org.trescal.cwms.core.quotation.entity.costs;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.quotation.dto.DiscountSummaryDTO;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;

/**
 * Convenience superclass that all {@link Quotation} cost dao's inherit from.
 * Bundles common functionality into one resource.
 * 
 * @author richard
 */
public abstract class QuotationCostDaoSupportImpl<Entity extends Cost> extends BaseDaoImpl<Entity, Integer> implements QuotationCostDaoSupport<Entity>
{
	@SuppressWarnings("unchecked")
	public List<Entity> findMatchingCostOnLinkedQuotation(int jobid, int modelid, int calTypeId)
	{
		// 3 main sets of joins
		// qcalcost-->qitem-->quote-->quotejoblink-->job
		// qcalcost-->qitem-->model
		// qcalcost-->qitem-->caltype
		
 
		Criteria crit = this.getSession().createCriteria(getEntity());
		Criteria qItemCrit = crit.createCriteria("quoteItem");
		Criteria qCrit = qItemCrit.createCriteria("quotation");
		qItemCrit.setFetchMode("quotation", FetchMode.JOIN);
		qItemCrit.setFetchMode("quotation.currency", FetchMode.JOIN);

		// active costs only
		crit.add(Restrictions.eq("active", true));

		// company restriction
		qCrit.createCriteria("linkedJobs").createCriteria("job").add(Restrictions.idEq(jobid));

		// model restriction
		qItemCrit.createCriteria("model", "model", JoinType.LEFT_OUTER_JOIN);
		qItemCrit.createCriteria("inst", "inst", JoinType.LEFT_OUTER_JOIN);
		
		DetachedCriteria salesCatSubqueryCriteria = DetachedCriteria.forClass(InstrumentModel.class);
		salesCatSubqueryCriteria.add(Restrictions.idEq(modelid));
		salesCatSubqueryCriteria.setProjection(Property.forName("salesCategory.id"));
		
		qItemCrit.createAlias("model.modelType", "mt", JoinType.LEFT_OUTER_JOIN);
		
		Disjunction or = Restrictions.disjunction();
		or.add(Restrictions.eq("model.modelid", modelid));
		or.add(Restrictions.eq("inst.model.modelid", modelid));
		or.add(Restrictions.and(Subqueries.propertyEq("model.salesCategory.id", salesCatSubqueryCriteria),Restrictions.eq("mt.salescategory", true)));
		qItemCrit.add(or);

		// caltype restriction
		qItemCrit.createCriteria("caltype").add(Restrictions.idEq(calTypeId));

		// ordering
		//qItemCrit.addOrder(Order.asc("model.modelType.instModelTypeId"));
		qCrit.addOrder(Order.desc("id"));
		
		// Added restriction to just return 1 page (25 records) of results, large quotations ~1000 results!
		crit.setFirstResult(0);
		crit.setMaxResults(Constants.RESULTS_PER_PAGE);

		return (List<Entity>) crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Entity> findMatchingCosts(Integer coid, int modelid, Integer calTypeId, Integer resultsYearFilter, Integer page, Integer resPerPage)
	{
		// 3 main sets of joins
		// qcalcost-->qitem-->quote-->con-->sub-->comp
		// qcalcost-->qitem-->model
		// qcalcost-->qitem-->caltype
		
		Criteria crit = this.getSession().createCriteria(getEntity());
		Criteria qItemCrit = crit.createCriteria("quoteItem");
		Criteria qCrit = qItemCrit.createCriteria("quotation");
		qItemCrit.setFetchMode("quotation", FetchMode.JOIN);
		qItemCrit.setFetchMode("quotation.currency", FetchMode.JOIN);

		// active costs only
		crit.add(Restrictions.eq("active", true));

		// company restriction
		if (coid != null)
		{
			qCrit.createCriteria("contact").createCriteria("sub").createCriteria("comp").add(Restrictions.idEq(coid));
		}

		// add eager loading for company & other details
		crit.setFetchMode("quoteItem.caltype", FetchMode.JOIN);
		crit.setFetchMode("quoteItem.quotation.contact", FetchMode.JOIN);
		crit.setFetchMode("quoteItem.quotation.contact.sub", FetchMode.JOIN);
		crit.setFetchMode("quoteItem.quotation.contact.sub.comp", FetchMode.JOIN);

		crit.setFetchMode("quoteItem.quotation.createdBy", FetchMode.JOIN);

		// model restriction
		qItemCrit.createCriteria("model", "model", JoinType.LEFT_OUTER_JOIN);
		qItemCrit.createCriteria("inst", "inst", JoinType.LEFT_OUTER_JOIN);

		Criterion lhs = Restrictions.eq("model.modelid", modelid);
		Criterion rhs = Restrictions.eq("inst.model.modelid", modelid);
		qItemCrit.add(Restrictions.or(lhs, rhs));

		// caltype restriction
		if (calTypeId != null)
		{
			qItemCrit.createCriteria("caltype").add(Restrictions.idEq(calTypeId));
		}
		// results year filter (injected property)
		if (resultsYearFilter != null)
		{
			GregorianCalendar cal = new GregorianCalendar();
			cal.add(Calendar.YEAR, 0 - resultsYearFilter);
			qCrit.add(Restrictions.gt("regdate", DateTools.dateToLocalDate(cal.getTime())));
		}
		// ordering
		qCrit.addOrder(Order.desc("id"));
		// apply paging if requested
		if (page != null)
		{
			resPerPage = resPerPage == null ? Constants.RESULTS_PER_PAGE : resPerPage;
			crit.setFirstResult((resPerPage * page) - resPerPage);
			crit.setMaxResults(resPerPage);
		}
		return (List<Entity>) crit.list();
	}
	
	@Override
	public DiscountSummaryDTO getDiscountDTO(int quoteid) {
		Criteria crit = this.getSession().createCriteria(getEntity());
		crit.createCriteria("quoteItem").createCriteria("quotation").add(Restrictions.idEq(quoteid));
		crit.add(Restrictions.gt("discountValue", new BigDecimal("0.00")));
		
		ProjectionList projection = Projections.projectionList();
		projection.add(Projections.rowCount(), "discounts");
		projection.add(Projections.sum("discountValue"), "discountValue");
		
		crit.setProjection(projection);
		crit.setResultTransformer(Transformers.aliasToBean(DiscountSummaryDTO.class));
		return (DiscountSummaryDTO) crit.uniqueResult();
	}

}
