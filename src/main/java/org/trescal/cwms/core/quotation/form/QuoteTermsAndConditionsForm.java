package org.trescal.cwms.core.quotation.form;

import java.util.Collections;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.QuotationConditions;

@Data
@AllArgsConstructor @NoArgsConstructor
public class QuoteTermsAndConditionsForm
{
	private String generalCalCondition;
	
	private List<CalConditionDto> calConditions;

	private QuotationConditions conditions;

	private String generalConditions;
	private Quotation quotation;

	private boolean usingDefaultTermsAndConditions;

	public List<CalConditionDto> getCalConditions(){
		return this.calConditions!=null ? this.calConditions: Collections.emptyList();
	}
}