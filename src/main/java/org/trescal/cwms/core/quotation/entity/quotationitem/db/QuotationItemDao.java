package org.trescal.cwms.core.quotation.entity.quotationitem.db;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.MultiValuedMap;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.lookup.PriceLookupRequirements;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutputQuotationItem;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupQueryType;
import org.trescal.cwms.core.quotation.dto.DiscountSummaryDTO;
import org.trescal.cwms.core.quotation.dto.QuotationItemCostDTO;
import org.trescal.cwms.core.quotation.dto.QuotationItemSummaryDTO;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.comparators.QuotationItemSortType;
import org.trescal.cwms.core.quotation.entity.quotationitem.dto.QuotationItemOptionDto;
import org.trescal.cwms.core.quotation.form.QuotationItemSearchForm;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface QuotationItemDao extends BaseDao<Quotationitem, Integer> {

	void delete(int qiid);

	void deleteQuotationItems(int[] qiids);

	void detachModule(int[] qiid);

	Set<Quotationitem> findAvailableBaseUnits(int quoteid, int modelid);

	Set<Quotationitem> findAvailableModules(int quoteid, int modelid);

	List<Quotationitem> findIdenticalModelsOnQuotation(int quoteid, int modelid, Integer excludeQIId,
			QuotationItemSortType sortType);

	List<Quotationitem> findNearbyQuotationItems(int headingid, int minItemNo, int maxItemNo);

	Quotationitem findQuotationitemByItemNo(int headingId, int caltypeId, int itemNo);

	List<PriceLookupOutputQuotationItem> findQuotationItems(PriceLookupRequirements requirements,
			PriceLookupQueryType queryType, MultiValuedMap<Integer, Integer> serviceTypeMap);

	PagedResultSet<Quotationitem> findQuotationItems(QuotationItemSearchForm form, PagedResultSet<Quotationitem> prs);

	List<Quotationitem> findQuotationItems(int[] qiids);

	List<Quotationitem> findQuotationItems(List<Integer> qiids);

	List<Quotationitem> findQuotationItemsForPlantIds(List<Integer> plantIds, Integer jobId, boolean defaultServiceTypeUsage);

	List<Quotationitem> findQuotationItemsForModelIds(List<Integer> modelIds, Integer jobId, boolean defaultServiceTypeUsage);

	List<QuotationItemSummaryDTO> findSummaryDTOs(int quoteid, boolean groupByHeading, boolean groupByServiceType);

	QuotationItemCostDTO getItemCostsByInstrument(Integer quotationId, Integer jobItemId, Integer serviceTypeId);

	DiscountSummaryDTO getGeneralDiscountDTO(int quoteid);

	Long getItemCount(int quoteid);

	Set<Quotationitem> getItemsForQuotation(int quoteid, Integer page, Integer resPerPage);

	Integer getMaxItemno(int quoteid);

	void saveOrUpdateAll(Collection<Quotationitem> items);

    List<QuotationItemOptionDto> findQuotationItemsOptionsForPlantIdsJobIdAndDefaultUsage(List<Integer> plantIds, Integer jobId, Boolean defaultServiceTypeUsage);

	List<QuotationItemOptionDto> findQuotationItemsOptionsForModelIdsJobIdAndDefaultUsage(List<Integer> modelIdIds, Integer jobId, Boolean defaultServiceTypeUsage);
}