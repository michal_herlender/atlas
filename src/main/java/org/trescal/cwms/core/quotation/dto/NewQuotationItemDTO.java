package org.trescal.cwms.core.quotation.dto;

import java.math.BigDecimal;

import org.trescal.cwms.core.pricing.entity.costs.CostSource;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class NewQuotationItemDTO {

	private Integer plantId;
	private String modelName;
	private String serialNo;
	private String plantNo;
	private Integer defaultCalTypeId;
	private CostSource priceSource;
	private BigDecimal finalPrice;

}