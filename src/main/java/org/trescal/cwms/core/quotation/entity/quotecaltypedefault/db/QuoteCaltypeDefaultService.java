package org.trescal.cwms.core.quotation.entity.quotecaltypedefault.db;

import java.util.List;

import org.trescal.cwms.core.quotation.entity.quotecaltypedefault.QuoteCaltypeDefault;

public interface QuoteCaltypeDefaultService
{
	QuoteCaltypeDefault findQuoteCaltypeDefault(int id);
	void insertQuoteCaltypeDefault(QuoteCaltypeDefault quotecaltypedefault);
	void updateQuoteCaltypeDefault(QuoteCaltypeDefault quotecaltypedefault);
	void deleteQuoteCaltypeDefault(QuoteCaltypeDefault quotecaltypedefault);
	List<QuoteCaltypeDefault> getAllQuoteCaltypeDefaults();
}