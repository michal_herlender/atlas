package org.trescal.cwms.core.quotation.service;

import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequest;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.db.ScheduledQuotationRequestService;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;

import java.util.List;

@Service("ScheduledQuotationGenerator")
@Log
public class ScheduledQuotationGenerator {

    private ScheduledQuotationRequestService scheduledQuotationRequestService;
    private ScheduledQuoteRequestToQuotationService scheduledQuoteRequestToQuotationService;
    private ScheduledTaskService scheduledTaskService;

    public ScheduledQuotationGenerator(ScheduledQuotationRequestService scheduledQuotationRequestService, ScheduledQuoteRequestToQuotationService scheduledQuoteRequestToQuotationService, ScheduledTaskService scheduledTaskService) {
        this.scheduledQuotationRequestService = scheduledQuotationRequestService;
        this.scheduledQuoteRequestToQuotationService = scheduledQuoteRequestToQuotationService;
        this.scheduledTaskService = scheduledTaskService;
    }

    public void createQuotationsFromScheduledQuotationRequests() {
        if (this.scheduledTaskService.scheduledTaskIsActive(this.getClass().getName(), "createQuotationsFromScheduledQuotationRequests")) {
            List<ScheduledQuotationRequest> outstanding = scheduledQuotationRequestService.getOutstanding(null);
            log.info("Found " + outstanding.size() + " oustanding scheduled quotation requests");
            for (ScheduledQuotationRequest request : outstanding) {
                Contact requestingContact = request.getRequestBy();
                this.scheduledQuoteRequestToQuotationService.prepareAndSaveScheduledQuoteToQuotation(request, true, requestingContact, request.getOrganisation(), request.getOrganisation().getComp());
            }
            // report results of task run
            String message = outstanding.size() + " outstanding quote requests processed";
            this.scheduledTaskService.reportOnScheduledTaskRun(this.getClass().getName(), Thread.currentThread().getStackTrace(), true, message);
        } else
            log.info("Scheduled Quote Generator not running: scheduled task cannot be found or is turned off");
    }
}