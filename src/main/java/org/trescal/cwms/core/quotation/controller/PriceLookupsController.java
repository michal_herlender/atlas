package org.trescal.cwms.core.quotation.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.quotation.dto.PreferredPriceLookupDTO;
import org.trescal.cwms.core.quotation.dto.PreliminaryCompInstToQuoteItemWrapper;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.service.CompanyInstrumentsToQuotationService;

@RestController
@RequestMapping("quotepricelookup")
public class PriceLookupsController {

	@Autowired
    QuotationItemService quoteItemService;
	@Autowired
    InstrumentModelService instrumentModelService;
    @Autowired
    private CompanyInstrumentsToQuotationService companyInstrumentsToQuotationService;

    @PostMapping("/preferredcalprice")
    public List<PreliminaryCompInstToQuoteItemWrapper> getPreferedPrice(@RequestBody PreferredPriceLookupDTO dto) {
    	return this.companyInstrumentsToQuotationService.sourceCompanyInstrumentModelToQIValues(dto.getCalTypeIds(),
    			dto.getModelId(), dto.getClientCoId(), dto.getExcludeQuotationId());
    }

}
