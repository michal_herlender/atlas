package org.trescal.cwms.core.quotation.form;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class JobToQuotationValidator implements Validator
{
	protected final Log logger = LogFactory.getLog(this.getClass());
	
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(JobToQuotationForm.class);
	}

	public void validate(Object obj, Errors errors)
	{
		JobToQuotationForm jtoqData = (JobToQuotationForm) obj;

		if (jtoqData == null)
		{
			errors.reject("error.nullpointer", "Null data received");
		}
		else
		{
			if ((jtoqData.getToQuoteJobItemIds() == null)
					|| (jtoqData.getToQuoteJobItemIds().size() == 0))
			{
				errors.rejectValue("toQuoteJobItemIds", null, "Please select at least one job item to quote");
			}

			if ((jtoqData.getClientRef() == null)
					|| (jtoqData.getClientRef().length() > 100))
			{
				errors.rejectValue("clientRef", null, "Client reference should be no more than 100 characters");
			}
		}
	}
}
