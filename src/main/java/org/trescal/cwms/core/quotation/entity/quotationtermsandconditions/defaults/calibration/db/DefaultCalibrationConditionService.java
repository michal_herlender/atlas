package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.DefaultCalibrationCondition;

public interface DefaultCalibrationConditionService extends BaseService<DefaultCalibrationCondition, Integer>
{
	DefaultCalibrationCondition getLatest(int type, Company company);
}