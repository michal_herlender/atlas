package org.trescal.cwms.core.quotation.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.form.ImportQuotationItemsForm;
import org.trescal.cwms.core.quotation.form.ImportedQuotationItemsSynthesisForm;
import org.trescal.cwms.core.quotation.form.ImportedQuotationItemsSynthesisForm.ImportedQuotationItemsSynthesisRowDTO;
import org.trescal.cwms.core.quotation.form.validator.ImportedQuotationItemsSynthesisFormValidator;
import org.trescal.cwms.core.quotation.service.AddInstrumentsToQuotationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME })
public class ImportedQuotationItemsSynthesisController {

	public static final String FORM = "form";

	@Autowired
	private ImportedQuotationItemsSynthesisFormValidator validator;
	@Autowired
	private QuotationService quotationService;
	@Autowired
	private QuotationItemService qiService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private UserService userService;
	@Autowired
	private AddInstrumentsToQuotationService addInstrumentsToQuoteService;
	@Autowired
	private TaxCalculator taxCalculator;

	@InitBinder(FORM)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute(FORM)
	protected ImportedQuotationItemsSynthesisForm formBackingObject(
			@ModelAttribute("importQuotationItems") ImportQuotationItemsForm inputForm,
			@ModelAttribute(FORM) ImportedQuotationItemsSynthesisForm form,
			@ModelAttribute("fileContent") ArrayList<LinkedCaseInsensitiveMap<String>> fileContent, Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws ParseException {

		if (form.getSubdivId() == null) {
			form.setAddressId(inputForm.getAddrid());
			form.setContactId(inputForm.getPersonid());
			form.setCoid(inputForm.getCoid());
			form.setExchangeFormatId(inputForm.getExchangeFormatId());
			form.setSubdivId(inputForm.getSubdivid());
			form.setQuotationId(inputForm.getQuotationId());
			form.setRows(
					quotationService.convertFileContentToQuotationItemsDTO(form.getExchangeFormatId(), fileContent));
		}

		form.setBusinessCompanyId(this.userService.get(username).getCon().getSub().getComp().getId());
		Contact con = contactService.get(form.getContactId());
		Address addr = addressService.get(form.getAddressId());
		model.addAttribute("contact", con);
		model.addAttribute("address", addr);
		model.addAttribute("subdiv", addr.getSub());
		model.addAttribute("company", addr.getSub().getComp());
		model.addAttribute("quotation", quotationService.get(form.getQuotationId()));
			
		return form;

	}

	@RequestMapping(value = "/importedquotationitemssynthesis.htm", method = RequestMethod.GET)
	public ModelAndView doGet(Model model, @Valid @ModelAttribute(FORM) ImportedQuotationItemsSynthesisForm form,
			BindingResult result) throws Exception {
		
		analysisData(form);
		
		if (result.hasErrors())
			return newanalysis(form, result, model);
		return new ModelAndView("trescal/core/quotation/importedquotationitemssynthesis");
	}

	@RequestMapping(value = { "/importedquotationitemssynthesis.htm" }, params = { "!save",
			"newanalysis" }, method = RequestMethod.POST)
	protected ModelAndView newanalysis(@Valid @ModelAttribute(FORM) ImportedQuotationItemsSynthesisForm form,
			BindingResult result, Model model) throws Exception {
		analysisData(form);
		return new ModelAndView("trescal/core/quotation/importedquotationitemssynthesis");
	}

	@RequestMapping(value = { "/importedquotationitemssynthesis.htm" }, params = { "save",
			"!newanalysis" }, method = RequestMethod.POST)
	protected ModelAndView submit(@Valid @ModelAttribute(FORM) ImportedQuotationItemsSynthesisForm form,
			BindingResult result, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Model model,
			Locale locale) throws Exception {

            Integer quoteId = form.getQuotationId();
            Integer contactId = this.userService.get(username).getCon().getPersonid();
    		List<Integer> plantIds = new ArrayList<Integer>();
    		Map<Integer, Integer> serviceTypeIds = new HashMap<>();
    		Map<Integer, BigDecimal> finalPrices = new HashMap<>();
    		Map<Integer, BigDecimal> discountPrices = new HashMap<>();
    		Map<Integer, BigDecimal> catalogPrices = new HashMap<>();
    		Map<Integer, String> publicNotes = new HashMap<>();
    		Map<Integer, String> privateNotes = new HashMap<>();

    		form.getIdentifiedItems().stream().forEach(row -> {
    			plantIds.add(row.getPlantId());
    			serviceTypeIds.put(row.getPlantId(), row.getServiceTypeId());
    			finalPrices.put(row.getPlantId(), BigDecimal.valueOf(row.getFinalPrice()));
    			if (row.getDiscountRate() != null)
    				discountPrices.put(row.getPlantId(), BigDecimal.valueOf(row.getDiscountRate()));
    			if (row.getCatalogPrice() != null)
    				catalogPrices.put(row.getPlantId(), BigDecimal.valueOf(row.getCatalogPrice()));
    			if (!StringUtils.isEmpty(row.getPublicNote()))
    				publicNotes.put(row.getPlantId(), row.getPublicNote());
    			if (!StringUtils.isEmpty(row.getPrivateNote()))
    				privateNotes.put(row.getPlantId(), row.getPrivateNote());
    		}); 
    
			Set<Quotationitem> list = addInstrumentsToQuoteService.importQuotationItemsUsingInst__PricesFromFile(subdivDto.getKey(), contactId, plantIds, serviceTypeIds,
					finalPrices, discountPrices, catalogPrices, publicNotes, privateNotes, quoteId, locale);
			
			if(!list.isEmpty()){
				Quotation quotation = quotationService.get(quoteId);
				quotation.getQuotationitems().addAll(list);
				// update the costs
				CostCalculator.updatePricingTotalCost(quotation, quotation.getQuotationitems());
				taxCalculator.calculateAndSetVATaxAndFinalCost(quotation);
				// save implicitly needed before sort; as sort currently rebuilds
				// quotation item set.
				this.quotationService.save(quotation);
				// update the numbering of the items on the quotation
				this.qiService.sortQuotationItems(quotation);
			}

			return new ModelAndView(new RedirectView("/viewquotation.htm?id=" + form.getQuotationId(), true));
	}
	
	private void analysisData(ImportedQuotationItemsSynthesisForm form){
		form.setIdentifiedItems(new ArrayList<>());
		form.setNonIdentifiedItems(new ArrayList<>());
		for (ImportedQuotationItemsSynthesisRowDTO dto: form.getRows()){
			if (dto.getErrorMessages()!=null && dto.getErrorMessages().size()>0){
				form.getNonIdentifiedItems().add(dto);
			}
			else
				form.getIdentifiedItems().add(dto);
			}
		
		if(form.getIdentifiedItems().size()>0){
			form.setIdentifiedItemsExist(true);
		}
		else
			form.setIdentifiedItemsExist(false);
	}

}
