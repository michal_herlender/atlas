package org.trescal.cwms.core.quotation.controller;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteheading.db.QuoteHeadingService;

@Controller @IntranetController
public class DeleteQuoteHeadingController
{
	@Autowired
	private QuoteHeadingService qhServ;
	@Autowired
	private QuotationItemService qiServ;
	
	@RequestMapping(value="/deletequoteheading.htm", method=RequestMethod.GET)
	protected ModelAndView handle(@RequestParam(value="headingId", required=true) int headingId) throws Exception
	{
		QuoteHeading qh = this.qhServ.findQuoteHeading(headingId);

		// don't allow deletions of the default heading - this currently will have
		// no effect apart from to redirect to the editquoteheading page
		// TODO - change to use a form and proper binding / validation.
		//errors.rejectValue("headingId", "error.quotationheading.nodelete", null, "You cannot delete the default heading");
		if (!qh.isSystemDefault()) {
			// load the default heading
			QuoteHeading defaultHeading = this.qhServ.findDefaultQuoteHeading(qh.getQuotation().getId());

			// if any items are assigned to this heading then reassign all to
			// the default heading
			if (qh.getQuoteitems().size() > 0)
			{
				for (Quotationitem qi : qh.getQuoteitems())
				{
					qi.setHeading(defaultHeading);
					this.qiServ.updateQuotationItem(qi);
				}

				// set quotation items to null to make sure no items are deleted
				qh.setQuoteitems(new HashSet<Quotationitem>());
			}

			// delete the heading
			this.qhServ.delete(qh);
		}

		// you could include errors.getModel() as part of the return here
		// if you were displaying a normal ModelAndView.
		return new ModelAndView(new RedirectView("quoteheadingform.htm?id="
				+ qh.getQuotation().getId()));
	}
}