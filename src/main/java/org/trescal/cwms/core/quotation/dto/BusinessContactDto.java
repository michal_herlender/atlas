package org.trescal.cwms.core.quotation.dto;

import lombok.Value;

@Value(staticConstructor = "of")
public class BusinessContactDto {
    String id;
    String name;
}
