package org.trescal.cwms.core.quotation.enums;

public enum ScheduledQuoteRequestType
{
	ALL_INSTRUMENTS,
	ALL_OUT_OF_CAL_INSTRUMENTS,
	DATE_RANGE_INSTRUMENTS
}
