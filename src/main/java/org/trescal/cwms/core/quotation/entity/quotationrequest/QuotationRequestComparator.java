package org.trescal.cwms.core.quotation.entity.quotationrequest;

import java.util.Comparator;

public class QuotationRequestComparator implements Comparator<QuotationRequest>
{
	@Override
	public int compare(QuotationRequest qr1, QuotationRequest qr2)
	{
		if ((qr1.getId() == 0) && (qr2.getId() == 0))
		{
			return 1;
		}
		else
		{
			return ((Integer) qr1.getId()).compareTo(qr2.getId());
		}
	}
}
