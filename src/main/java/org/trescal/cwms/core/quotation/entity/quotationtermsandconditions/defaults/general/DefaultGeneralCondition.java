package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.QuotationGeneralCondition;
import org.trescal.cwms.core.system.entity.translation.Translation;

@Entity
@Table(name = "defaultquotationgeneralcondition")
public class DefaultGeneralCondition extends QuotationGeneralCondition
{
	private Set<Quotation> Quotations;
	private Set<Translation> conditiontextTranslation;
	
	public DefaultGeneralCondition() {
		super();
	}
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "defaultterms")
	public Set<Quotation> getQuotations() {
		return this.Quotations;
	}
	
	public void setQuotations(Set<Quotation> quotations) {
		this.Quotations = quotations;
	}
	
	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="defaultquotationgeneralconditiontranslation", joinColumns=@JoinColumn(name="defgenconid"))
	public Set<Translation> getConditiontextTranslation() {
		return conditiontextTranslation;
	}
	
	public void setConditiontextTranslation(Set<Translation> conditiontextTranslation) {
		this.conditiontextTranslation = conditiontextTranslation;
	}
}