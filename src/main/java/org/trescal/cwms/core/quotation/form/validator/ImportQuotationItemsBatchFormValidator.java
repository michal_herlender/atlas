package org.trescal.cwms.core.quotation.form.validator;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.documents.excel.utils.ExcelFileReaderUtil;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.quotation.form.ImportQuotationItemsForm;

@Component
public class ImportQuotationItemsBatchFormValidator {

	@Autowired
	private ExchangeFormatService efService;
	@Autowired
	private ExcelFileReaderUtil excelFileReaderUtil;
	@Autowired
	private MessageSource messageSource;

	// 50MB
	// private static final long MAX_IN_BYTES = 52428800;
	private static final String EXCEL_EXTENSION_1 = "xlsx";
	private static final String EXCEL_EXTENSION_2 = "xls";

	public void validate(ImportQuotationItemsForm form) {

		MultipartFile file = form.getFile();
		form.setErrorMessage(new ArrayList<>());

		// validate file
		if (file.isEmpty()) {
			form.getErrorMessage().add(messageSource.getMessage("error.instrument.upload.file.empty", null,
					LocaleContextHolder.getLocale()));
		} else if (!EXCEL_EXTENSION_1.equalsIgnoreCase((FilenameUtils.getExtension(file.getOriginalFilename())))
				&& !EXCEL_EXTENSION_2.equalsIgnoreCase((FilenameUtils.getExtension(file.getOriginalFilename())))) {
			form.getErrorMessage().add(messageSource.getMessage("error.instrument.upload.file.notexcel", null,
					LocaleContextHolder.getLocale()));
		} else {
			// try to open the file
			try {
				// For .xlsx
				if (POIXMLDocument.hasOOXMLHeader(file.getInputStream())) {
					XSSFWorkbook wb = new XSSFWorkbook(file.getInputStream());
				}
				// For .xls
				else if (POIFSFileSystem.hasPOIFSHeader(file.getInputStream())) {
					HSSFWorkbook wb = new HSSFWorkbook(file.getInputStream());
				} else {
					form.getErrorMessage().add(messageSource.getMessage("error.instrument.upload.file.cannotbeopened",
							null, LocaleContextHolder.getLocale()));
				}
			} catch (IOException e) {
				e.printStackTrace();
				form.getErrorMessage().add(messageSource.getMessage("error.instrument.upload.file.cannotbeopened", null,
						LocaleContextHolder.getLocale()));
			}
		}

		// validate exchange Format
		if (form.getExchangeFormatId() == null) {
			form.getErrorMessage().add(messageSource.getMessage("error.instrument.exchangeformat.notnull", null,
					LocaleContextHolder.getLocale()));
		} else {
			ExchangeFormat ef = efService.get(form.getExchangeFormatId());
			if (ef == null) {
				form.getErrorMessage().add(messageSource.getMessage("error.instrument.exchangeformat.notnull", null,
						LocaleContextHolder.getLocale()));
			}
		}

		ExchangeFormat ef = efService.get(form.getExchangeFormatId());
		// can read file ?
		try {
			if (!excelFileReaderUtil.canReadFile(file.getInputStream(), ef.getSheetName(), ef.getLinesToSkip())) {
				form.getErrorMessage().add(messageSource.getMessage("error.instrument.upload.file.cannotbeopened", null,
						LocaleContextHolder.getLocale()));
			}
		} catch (IOException e) {
			e.printStackTrace();
			form.getErrorMessage().add(messageSource.getMessage("error.instrument.upload.file.cannotbeopened", null,
					LocaleContextHolder.getLocale()));
		}

		// validate contact
		if (form.getPersonid() == null) {
			form.getErrorMessage().add(messageSource.getMessage("error.instrument.contact.notnull", null,
					LocaleContextHolder.getLocale()));
		}

		if (form.getAutoSubmitPolicy() == null) {
			form.getErrorMessage().add(
					messageSource.getMessage("error.batch.autosubmitpolicy", null, LocaleContextHolder.getLocale()));
		}
	}

}
