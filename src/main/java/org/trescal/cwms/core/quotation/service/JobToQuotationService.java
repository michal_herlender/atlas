package org.trescal.cwms.core.quotation.service;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.exception.EntityNotFoundException;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface JobToQuotationService {
    Quotationitem jobitemToQuotationitem(JobItem ji, Quotation quotation, int subdivId);

    void prepareCalibrationCost(JobItem ji, QuotationCalibrationCost qCalCost);

    Set<Quotationitem> prepareJobItemsToQuotationItems(Collection<JobItem> jobitems, Quotation quotation,
                                                       int subdivId);

    Set<Quotationitem> prepareJobItemsToQuotationItems(List<Integer> jobitemids, Quotation quotation,
                                                       int subdivId);

    /**
     * Uses the specified job to create a new quotation. The quotation is saved
     * and the ID of the newly created entity returned.
     *
     * @param reqDate
     * @param linkToJob
     */
    int prepareAndSaveJobToQuotation(List<Integer> toQuoteJobItemIds, int jobid, Contact contact,
                                     Contact currentContact, Contact sourcedBy, LocalDate reqDate, Subdiv allocatedSubdiv, String clientRef,
                                     String currencyCode, boolean linkToJob) throws EntityNotFoundException;
}
