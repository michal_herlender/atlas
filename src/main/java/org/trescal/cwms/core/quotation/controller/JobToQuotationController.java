package org.trescal.cwms.core.quotation.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.quotation.form.JobToQuotationForm;
import org.trescal.cwms.core.quotation.form.JobToQuotationValidator;
import org.trescal.cwms.core.quotation.service.JobToQuotationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.tools.DateTools;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class JobToQuotationController {
    @Autowired
    private ContactService conServ;
    @Value("#{props['cwms.config.jobitem.fast_track_turn']}")
    private int fastTrackTurn;
    @Autowired
	private JobService jobServ;
	@Autowired
	private JobToQuotationService jobToQuotationService;
	@Autowired
	private SupportedCurrencyService supportedCurrencyServ;
	@Autowired
	private JobToQuotationValidator validator;
	@Autowired
	private UserService userService;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}

    @ModelAttribute("form")
    protected JobToQuotationForm formBackingObject(@RequestParam(name = "jobid") int jobid,
                                                   @RequestParam(name = QuotationAddFormController.REDIRECT_ATTRIBUTE_CLIENTREF, required = false) String clientRef,
                                                   @RequestParam(name = QuotationAddFormController.REDIRECT_ATTRIBUTE_CONTACT_ID, required = false, defaultValue = "0") Integer contactid,
                                                   @RequestParam(name = QuotationAddFormController.REDIRECT_ATTRIBUTE_CURRENCYCODE, required = false) String currencyCode,
                                                   @RequestParam(name = QuotationAddFormController.REDIRECT_ATTRIBUTE_REQDATE, required = false)  LocalDate reqdate,
                                                   @RequestParam(name = QuotationAddFormController.REDIRECT_ATTRIBUTE_SOURCEDBY_ID, required = false, defaultValue = "0") Integer sourcedbyid)
        throws Exception {
        JobToQuotationForm form = new JobToQuotationForm();
        // find the job
     	Job job = this.jobServ.get(jobid);
     	// check job can be loaded
     	if ((jobid == 0) || (job == null)) {
     		throw new Exception("Job details could not be found");
     	}
        // values present indicating redirect?
        if (contactid != 0) {
            // set from job to false as we have come from create quote
			form.setFromJob(false);
			form.setClientRef(clientRef);
			form.setContact(conServ.get(contactid));
			form.setCurrencyCode(currencyCode);
			form.setReqDate(reqdate);
			if (sourcedbyid != 0)
				form.setSourcedBy(conServ.get(sourcedbyid));
		} else {
            // directed from view job
            form.setFromJob(true);
            form.setClientRef(job.getClientRef());
            form.setContact(job.getCon());
            form.setCurrencyCode(job.getCurrency().getCurrencyCode());
            // set todays date
            form.setReqDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
        }
		
		// set job on form
		form.setJob(job);

		return form;
	}

	@RequestMapping(value = "/jobtoquotation.htm", method = RequestMethod.POST)
	public ModelAndView onSubmit(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("form") @Validated JobToQuotationForm form, BindingResult bindingResult) throws Exception {
        if (bindingResult.hasErrors()) {
            return referenceData(form);
        }
        Subdiv subdiv = form.getJob().getOrganisation();
        Contact currentContact = this.userService.get(username).getCon();
        Contact contact;
        Contact sourcedBy;
        // only update these fields if not coming from job
        if (!form.isFromJob()) {
            contact = form.getContact();
            sourcedBy = form.getSourcedBy();
        } else {
            contact = this.conServ.get(form.getPersonid());
            sourcedBy = currentContact;
        }
        int quotationId = this.jobToQuotationService.prepareAndSaveJobToQuotation(form.getToQuoteJobItemIds(),
            form.getJob().getJobid(), contact, currentContact, sourcedBy, form.getReqDate(), subdiv, form.getClientRef(),
				form.getCurrencyCode(), form.isLinkToJob());

		return new ModelAndView(new RedirectView("viewquotation.htm?id=" + quotationId, true));
	}

	@RequestMapping(value = "/jobtoquotation.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute("form") JobToQuotationForm form) throws Exception {
        HashMap<String, Object> map = new HashMap<>();

        map.put(Constants.FAST_TRACK_TURN, this.fastTrackTurn);
        map.put("currencyopts", this.supportedCurrencyServ
            .getCompanyCurrencyOptions(form.getJob().getCon().getSub().getComp().getCoid()));
        map.put("today", LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));

        return new ModelAndView("trescal/core/quotation/jobtoquotation", map);
    }
}
