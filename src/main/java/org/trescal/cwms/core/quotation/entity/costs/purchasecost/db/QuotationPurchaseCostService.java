package org.trescal.cwms.core.quotation.entity.costs.purchasecost.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.quotation.entity.costs.QuotationCostService;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;

public interface QuotationPurchaseCostService extends QuotationCostService<QuotationPurchaseCost>, BaseService<QuotationPurchaseCost, Integer>
{
}