package org.trescal.cwms.core.quotation.entity.scheduledquoterequest;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "scheduledquotationrequestdaterange")
public class ScheduledQuotationRequestDateRange {
    private int requestid;
    private ScheduledQuotationRequest request;
    private LocalDate startDate;
    private LocalDate finishDate;
    private Boolean includeEmptyDates;

    @Id
    @GeneratedValue(generator = "sqrgen")
    @GenericGenerator(strategy = "foreign", name = "sqrgen",
        parameters = @Parameter(name = "property", value = "request"))
    @Column(name = "requestid", nullable = false, unique = true)
    @Type(type = "int")
    public int getRequestid() {
        return requestid;
    }

    public void setRequestid(int requestid) {
        this.requestid = requestid;
    }

    @NotNull
    @OneToOne
    @JoinColumn(name = "requestid", nullable = false)
    public ScheduledQuotationRequest getRequest() {
        return request;
    }

    public void setRequest(ScheduledQuotationRequest request) {
        this.request = request;
    }

    @Column(name = "startdate", columnDefinition = "date")
    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    @NotNull
    @Column(name = "includeempty", nullable = false, columnDefinition = "bit default 0")
    public Boolean getIncludeEmptyDates() {
        return includeEmptyDates;
    }

    public void setIncludeEmptyDates(Boolean includeEmptyDates) {
        this.includeEmptyDates = includeEmptyDates;
    }

    @Column(name = "finishdate", columnDefinition = "date")
    public LocalDate getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(LocalDate finishDate) {
        this.finishDate = finishDate;
    }
}
