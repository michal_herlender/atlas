package org.trescal.cwms.core.quotation.entity.quotecaldefaults.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.quotation.entity.quotecaldefaults.QuoteCalDefaults;

public interface QuoteCalDefaultsDao extends BaseDao<QuoteCalDefaults, Integer> {}