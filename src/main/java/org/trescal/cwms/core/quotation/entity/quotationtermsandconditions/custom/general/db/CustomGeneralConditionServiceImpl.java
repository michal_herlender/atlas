package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.general.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.general.CustomGeneralCondition;

@Service("CustomGeneralConditionService")
public class CustomGeneralConditionServiceImpl extends BaseServiceImpl<CustomGeneralCondition, Integer> implements CustomGeneralConditionService
{
	@Autowired
	private CustomGeneralConditionDao customGeneralConditionDao;
	
	@Override
	protected BaseDao<CustomGeneralCondition, Integer> getBaseDao() {
		return customGeneralConditionDao;
	}
}