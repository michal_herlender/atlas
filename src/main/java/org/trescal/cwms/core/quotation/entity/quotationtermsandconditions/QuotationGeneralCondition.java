package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.annotation.AllowXHTML;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;

@MappedSuperclass
public abstract class QuotationGeneralCondition extends Allocated<Company>
{
	private int genConId;
	private String conditionText;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "defgenconid", nullable = false, unique = true)
	@Type(type = "int")
	public int getGenConId() {
		return genConId;
	}
	
	public void setGenConId(int defGenConId) {
		this.genConId = defGenConId;
	}
	
	@Column(name = "conditiontext", nullable = true, unique = false, length = 5000)
	@AllowXHTML (allow=true)
	public String getConditionText() {
		return conditionText;
	}
	
	public void setConditionText(String conditionText) {
		this.conditionText = conditionText;
	}
}