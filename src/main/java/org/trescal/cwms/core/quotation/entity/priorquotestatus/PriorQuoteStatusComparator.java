package org.trescal.cwms.core.quotation.entity.priorquotestatus;

import java.util.Comparator;

public class PriorQuoteStatusComparator implements Comparator<PriorQuoteStatus>
{
	@Override
	public int compare(PriorQuoteStatus p1, PriorQuoteStatus p2)
	{
		int q1 = p1.getQuotation().getId();
		int q2 = p2.getQuotation().getId();

		if (q2 != q1)
		{
			return ((Integer) q2).compareTo(q1);
		}
		else
		{
			if (p2.getSetOn().compareTo(p1.getSetOn()) == 0)
			{
				return p2.getPqsId().compareTo(p1.getPqsId());
			}
			else
			{
				return p2.getSetOn().compareTo(p1.getSetOn());
			}
		}
	}
}
