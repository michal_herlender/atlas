package org.trescal.cwms.core.quotation.entity.quotenote.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;

@Repository
public class QuoteNoteDaoImpl extends BaseDaoImpl<QuoteNote, Integer> implements QuoteNoteDao {

	@Override
	protected Class<QuoteNote> getEntity() {
		return QuoteNote.class;
	}

}
