package org.trescal.cwms.core.quotation.entity.quotationrequest;

public enum QuoteRequestLogSource
{
	INTERNAL("Logged internally"),
	MEMBERS_WEB("Logged from members website"),
	PUBLIC_WEB("Logged from public website");

	private String viewName;

	private QuoteRequestLogSource(String viewName)
	{
		this.viewName = viewName;
	}

	public String getViewName()
	{
		return this.viewName;
	}

	public void setViewName(String viewName)
	{
		this.viewName = viewName;
	}

}
