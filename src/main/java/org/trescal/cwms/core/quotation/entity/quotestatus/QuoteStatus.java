package org.trescal.cwms.core.quotation.entity.quotestatus;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotestatusgroupmember.QuoteStatusGroupMember;
import org.trescal.cwms.core.system.entity.status.Status;

@Entity
@DiscriminatorValue("quotation")
public class QuoteStatus extends Status
{
	private Set<Quotation> quotations = new HashSet<Quotation>(0);
	private Set<QuoteStatusGroupMember> statusGroups;

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "quotestatus")
	public Set<Quotation> getQuotations()
	{
		return this.quotations;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "status")
	public Set<QuoteStatusGroupMember> getStatusGroups()
	{
		return this.statusGroups;
	}

	public void setQuotations(Set<Quotation> quotations)
	{
		this.quotations = quotations;
	}

	public void setStatusGroups(Set<QuoteStatusGroupMember> statusGroups)
	{
		this.statusGroups = statusGroups;
	}
}
