package org.trescal.cwms.core.quotation.entity.quotationitem.db;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.collections4.MultiValuedMap;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.*;
import org.hibernate.transform.Transformers;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType_;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory;
import org.trescal.cwms.core.instrumentmodel.entity.salescategory.SalesCategory_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink_;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.job.entity.jobtypeservicetype.JobTypeServiceType;
import org.trescal.cwms.core.jobs.job.entity.jobtypeservicetype.JobTypeServiceType_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.lookup.PriceLookupRequirements;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutputQuotationItem;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupQueryType;
import org.trescal.cwms.core.quotation.dto.DiscountSummaryDTO;
import org.trescal.cwms.core.quotation.dto.QuotationItemCostDTO;
import org.trescal.cwms.core.quotation.dto.QuotationItemSummaryDTO;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost_;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation_;
import org.trescal.cwms.core.quotation.entity.quotationitem.QuotationItemComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem_;
import org.trescal.cwms.core.quotation.entity.quotationitem.TempQuotationItemComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.comparators.QuotationItemHeadingCaltypeOrderbyComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.comparators.QuotationItemSortType;
import org.trescal.cwms.core.quotation.entity.quotationitem.dto.QuotationItemOptionDto;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading_;
import org.trescal.cwms.core.quotation.form.QuotationItemSearchForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency_;
import org.trescal.cwms.core.tools.GenericTools;
import org.trescal.cwms.core.tools.PagedResultSet;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

import static org.trescal.cwms.core.tools.StringJpaUtils.trimAndConcatWithSeparator;
import static org.trescal.cwms.core.tools.StringJpaUtils.trimAndConcatWithWhitespace;

@Repository("QuoteItemDao")
@Slf4j
public class QuotationItemDaoImpl extends BaseDaoImpl<Quotationitem, Integer> implements QuotationItemDao {


	@Override
	protected Class<Quotationitem> getEntity() {
		return Quotationitem.class;
	}

	@Override
	public void delete(int qiid) {
		Quotationitem qi = find(qiid);
		remove(qi);
	}

	@Override
	public void deleteQuotationItems(int[] qiids) {
		// get the items to delete as a list
		List<Quotationitem> list = this.findQuotationItems(qiids);
		// delete the items
		list.forEach(this::remove);
	}

	@Override
	public void detachModule(int[] qiids) {
		ArrayList<Integer> itemNos = GenericTools.convertIntArrayToArrayList(qiids);
		DetachedCriteria ds = DetachedCriteria.forClass(Quotationitem.class).add(Restrictions.in("id", itemNos));
		@SuppressWarnings("unchecked")
		List<Quotationitem> qitems = (List<Quotationitem>) ds.getExecutableCriteria(getSession()).list();
		if (qitems != null)
			for (Quotationitem qi : qitems) {
				qi.setBaseUnit(null);
				qi.setPartOfBaseUnit(false);
				saveOrUpdate(qi);
			}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Quotationitem> findAvailableBaseUnits(int quoteid, int modelid) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Quotationitem.class)
				.add(Restrictions.eq("quotation.id", quoteid));
		// eagerly load the calibration type
		criteria.setFetchMode("caltype", FetchMode.JOIN);
		criteria.setFetchMode("model", FetchMode.JOIN);
		criteria.setFetchMode("model.model", FetchMode.JOIN);
		criteria.setFetchMode("model.description", FetchMode.JOIN);
		criteria.setFetchMode("model.mfr", FetchMode.JOIN);
		// all items that are allowed able to have modules
		DetachedCriteria instCrit = criteria.createCriteria("model");
		DetachedCriteria modeDetCrit = instCrit.createCriteria("modelType");
		modeDetCrit.add(Restrictions.eq("modules", true));
		// all items that are allowed to be base units for this module type
		DetachedCriteria partOfCrit = instCrit.createCriteria("thisBaseUnitsModules");
		DetachedCriteria moduleCrit = partOfCrit.createCriteria("module");
		moduleCrit.add(Restrictions.eq("id", modelid));
		List<Quotationitem> list = (List<Quotationitem>) criteria.getExecutableCriteria(getSession()).list();
		Set<Quotationitem> qi = new TreeSet<>(new QuotationItemComparator());
		qi.addAll(list);
		return qi;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Quotationitem> findAvailableModules(int quoteid, int modelid) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Quotationitem.class)
				.add(Restrictions.eq("quotation.id", quoteid))
				// all items that are not part of other base units on this quotation
				.add(Restrictions.eq("partOfBaseUnit", false));
		// all items that are allowed to have base units (i.e. modules or
		// functions)
		DetachedCriteria instCrit = criteria.createCriteria("model");
		DetachedCriteria modeDetCrit = instCrit.createCriteria("modelType");
		modeDetCrit.add(Restrictions.eq("baseUnits", true));
		// all items that are allowed to be attacehed to this base unit type
		DetachedCriteria partOfCrit = instCrit.createCriteria("thisModulesBaseUnits");
		DetachedCriteria moduleCrit = partOfCrit.createCriteria("base");
		moduleCrit.add(Restrictions.eq("id", modelid));
		List<Quotationitem> list = (List<Quotationitem>) criteria.getExecutableCriteria(getSession()).list();
		Set<Quotationitem> qi = new TreeSet<>(new QuotationItemComparator());
		qi.addAll(list);
		return qi;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Quotationitem> findIdenticalModelsOnQuotation(int quoteid, int modelid, Integer excludeQIId,
			QuotationItemSortType sortType) {
		Criteria crit = getSession().createCriteria(Quotationitem.class);
		crit.createCriteria("quotation").add(Restrictions.idEq(quoteid));
		crit.createCriteria("model").add(Restrictions.idEq(modelid));
		if (excludeQIId != null)
			crit.add(Restrictions.ne("id", excludeQIId));
		List<Quotationitem> items = crit.list();
		items.sort(new QuotationItemHeadingCaltypeOrderbyComparator(sortType));
		return items;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Quotationitem> findNearbyQuotationItems(int headingid, int minItemNo, int maxItemNo) {
		Criteria criteria = getSession().createCriteria(Quotationitem.class);
		criteria.setFetchMode("inst", FetchMode.JOIN);
		criteria.setFetchMode("inst.model", FetchMode.JOIN);
		criteria.setFetchMode("inst.model.description", FetchMode.JOIN);
		criteria.setFetchMode("model", FetchMode.JOIN);
		criteria.setFetchMode("model.description", FetchMode.JOIN);
		criteria.setFetchMode("model.mfr", FetchMode.JOIN);
		criteria.setFetchMode("modules", FetchMode.JOIN);
		criteria.createCriteria("heading").add(Restrictions.idEq(headingid));
		criteria.add(Restrictions.between("itemno", minItemNo, maxItemNo));
		criteria.addOrder(Order.asc("itemno"));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return (List<Quotationitem>) criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Quotationitem find(Integer id) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Quotationitem.class).add(Restrictions.eq("id", id));
		criteria.setFetchMode("model", FetchMode.JOIN);
		criteria.setFetchMode("model.model", FetchMode.JOIN);
		criteria.setFetchMode("model.description", FetchMode.JOIN);
		criteria.setFetchMode("model.mfr", FetchMode.JOIN);
		List<Quotationitem> list = (List<Quotationitem>) criteria.getExecutableCriteria(getSession()).list();
		return list.size() > 0 ? list.get(0) : null;
	}

	@Override
	public Quotationitem findQuotationitemByItemNo(int headingId, int caltypeId, int itemNo) {
		Criteria crit = getSession().createCriteria(Quotationitem.class);
		crit.createCriteria("heading").add(Restrictions.idEq(headingId));
		crit.createCriteria("caltype").add(Restrictions.idEq(caltypeId));
		crit.add(Restrictions.eq("itemno", itemNo));
		return (Quotationitem) crit.uniqueResult();
	}

	public List<PriceLookupOutputQuotationItem> findQuotationItems(PriceLookupRequirements reqs,
			PriceLookupQueryType queryType, MultiValuedMap<Integer, Integer> serviceTypeMap) {
		List<PriceLookupOutputQuotationItem> results;
		if (reqs == null || queryType == null || serviceTypeMap == null) {
			throw new IllegalArgumentException("One or more method arguments were null");
		} else if (serviceTypeMap.isEmpty()) {
			log.warn("Returning empty quotation results, as input map is null/empty");
			results = Collections.emptyList();
		} else {
			results = getResultList(cb -> {
				CriteriaQuery<PriceLookupOutputQuotationItem> cq = cb.createQuery(PriceLookupOutputQuotationItem.class);
				Root<Quotationitem> root = cq.from(Quotationitem.class);
				Join<Quotationitem, ServiceType> serviceType = root.join(Quotationitem_.serviceType);
				Join<Quotationitem, Quotation> quotation = root.join(Quotationitem_.quotation);
				Join<Quotationitem, QuotationCalibrationCost> quotationCalCost = root
						.join(Quotationitem_.calibrationCost);
				Join<Quotation, Contact> clientContact = quotation.join(Quotation_.contact);
				Join<Contact, Subdiv> clientSubdiv = clientContact.join(Contact_.sub);
				Join<Quotationitem, Instrument> instrument = root.join(Quotationitem_.inst, JoinType.LEFT);
				Join<Quotationitem, InstrumentModel> model = root.join(Quotationitem_.model, JoinType.LEFT);
				Join<InstrumentModel, SalesCategory> salesCategory = model.join(InstrumentModel_.salesCategory,
						JoinType.LEFT);
				Join<Quotation, SupportedCurrency> currency = quotation.join(Quotation_.currency);

				Predicate clauses = cb.conjunction();
				clauses.getExpressions()
						.add(cb.equal(quotation.get(Quotation_.organisation), reqs.getBusinessCompanyId()));
				clauses.getExpressions().add(cb.equal(clientSubdiv.get(Subdiv_.comp), reqs.getClientCompanyId()));

				Predicate quotationClauses = cb.disjunction();
				
				// We want to be able to include specific quotation ids independent of the general quotation search parameters 
				if (reqs.isIncludeQuotationIds()) {
					quotationClauses.getExpressions().add(root.get(Quotationitem_.quotation).in(reqs.getIncludeQuotationIds()));
				}
				if ((reqs.getQuotationAccepted() != null) ||
					(reqs.getQuotationIssued() != null) ||
					(reqs.getQuotationExpiryDateAfter() != null) ||
					(reqs.getQuotationRegDateAfter() != null)) {
					Predicate quotationParams = cb.conjunction();
					if (reqs.getQuotationAccepted() != null) {
						quotationParams.getExpressions().add(
							cb.equal(quotation.get(Quotation_.accepted), 
								reqs.getQuotationAccepted()));
					}
					if (reqs.getQuotationIssued() != null) {
						quotationParams.getExpressions().add(
							cb.equal(quotation.get(Quotation_.issued), 
								reqs.getQuotationIssued()));
					}
					if (reqs.getQuotationExpiryDateAfter() != null) {
						quotationParams.getExpressions().add(
							cb.greaterThan(quotation.get(Quotation_.expiryDate), 
								reqs.getQuotationExpiryDateAfter()));						
					}
					if (reqs.getQuotationRegDateAfter() != null) {
						quotationParams.getExpressions().add(
							cb.greaterThan(quotation.get(Quotation_.regdate), 
								reqs.getQuotationRegDateAfter()));						
					}
					quotationClauses.getExpressions().add(quotationParams);
				}
				if (!quotationClauses.getExpressions().isEmpty()) {
					clauses.getExpressions().add(quotationClauses);
				}
				
				Predicate itemClauses = cb.disjunction();

				switch (queryType) {
				case QUOTATION_ITEM_INSTRUMENT:
					// Search for quotation items for specific instrument id / service type id
					// combinations
					// serviceTypeMap is of serviceTypeId to Set of instrumentIds
					for (Integer serviceTypeId : serviceTypeMap.keySet()) {
						Collection<Integer> instrumentIds = serviceTypeMap.get(serviceTypeId);

						Predicate serviceTypeClauses = cb.conjunction();
						serviceTypeClauses.getExpressions()
								.add(cb.equal(root.get(Quotationitem_.serviceType), serviceTypeId));
						serviceTypeClauses.getExpressions().add(instrument.get(Instrument_.plantid).in(instrumentIds));
						itemClauses.getExpressions().add(serviceTypeClauses);
					}
					break;

				case QUOTATION_ITEM_INSTRUMENT_MODEL:
					// Search for quotation items for specific instrument model id / service type id
					// combinations
					// serviceTypeMap is of serviceTypeId to Set of instrumentModelIds
					for (Integer serviceTypeId : serviceTypeMap.keySet()) {
						Collection<Integer> modelIds = serviceTypeMap.get(serviceTypeId);

						Predicate serviceTypeClauses = cb.conjunction();
						serviceTypeClauses.getExpressions()
								.add(cb.equal(root.get(Quotationitem_.serviceType), serviceTypeId));
						serviceTypeClauses.getExpressions().add(model.get(InstrumentModel_.modelid).in(modelIds));
						itemClauses.getExpressions().add(serviceTypeClauses);
					}
					break;

				case QUOTATION_ITEM_SALES_CATEGORY_MODEL:
					// Search for quotation items for sales category model types ONLY,
					// matching the sales category id/service type id
					// serviceTypeMap is of serviceTypeId to Set of salesCategoryIds
					Join<InstrumentModel, InstrumentModelType> modelType = model.join(InstrumentModel_.modelType);
					clauses.getExpressions().add(cb.isTrue(modelType.get(InstrumentModelType_.salescategory)));

					// Search for quotation items for specific sales category id / service type id
					// combinations
					for (Integer serviceTypeId : serviceTypeMap.keySet()) {
						Collection<Integer> salesCategoryIds = serviceTypeMap.get(serviceTypeId);

						Predicate serviceTypeClauses = cb.conjunction();
						serviceTypeClauses.getExpressions()
								.add(cb.equal(root.get(Quotationitem_.serviceType), serviceTypeId));
						serviceTypeClauses.getExpressions()
								.add(salesCategory.get(SalesCategory_.id).in(salesCategoryIds));
						itemClauses.getExpressions().add(serviceTypeClauses);
					}
					break;

				default:
					throw new IllegalArgumentException("Unsupported query type " + queryType);
				}

				clauses.getExpressions().add(itemClauses);

				cq.where(clauses);
				cq.select(cb.construct(PriceLookupOutputQuotationItem.class,
						serviceType.get(ServiceType_.serviceTypeId), instrument.get(Instrument_.plantid),
						model.get(InstrumentModel_.modelid), salesCategory.get(SalesCategory_.id),
						root.get(Quotationitem_.id), quotationCalCost.get(QuotationCalibrationCost_.costid),
						quotationCalCost.get(QuotationCalibrationCost_.discountRate),
						quotationCalCost.get(QuotationCalibrationCost_.totalCost), quotation.get(Quotation_.id),
						quotation.get(Quotation_.accepted), quotation.get(Quotation_.expiryDate),
						quotation.get(Quotation_.issued), quotation.get(Quotation_.issuedate), 
						currency.get(SupportedCurrency_.currencyId)));
				return cq;
			});
		}
		// Workaround to set query type (setting enum as literal throws NPE in Hibernate
		// 5.2.17)
		// https://hibernate.atlassian.net/browse/HHH-12184
		// https://hibernate.atlassian.net/browse/HHH-13016
		results.forEach(output -> output.setQueryType(queryType));
		return results;
	}

	public PagedResultSet<Quotationitem> findQuotationItems(QuotationItemSearchForm form,
			PagedResultSet<Quotationitem> prs) {

		prs.setResultsCount(getItemCount(form.getQuotationid()).intValue());
		prs.setResults(getResultList(cb -> {
			CriteriaQuery<Quotationitem> cq = cb.createQuery(Quotationitem.class);
			Root<Quotationitem> root = cq.from(Quotationitem.class);
			Join<Quotationitem, Quotation> quotation = root.join(Quotationitem_.quotation);
			Join<Quotationitem, ServiceType> serviceType = root.join(Quotationitem_.serviceType);
			Join<Quotationitem, QuoteHeading> heading = root.join(Quotationitem_.heading);

			root.fetch(Quotationitem_.inst, JoinType.LEFT);
			root.fetch(Quotationitem_.model, JoinType.LEFT);
			root.fetch(Quotationitem_.calibrationCost, JoinType.LEFT);
			root.fetch(Quotationitem_.purchaseCost, JoinType.LEFT);
			// Fetch heading and service type (always present) so we can sort
			root.fetch(Quotationitem_.heading);
			root.fetch(Quotationitem_.serviceType);

			cq.where(cb.equal(quotation.get(Quotation_.id), form.getQuotationid()));

			List<javax.persistence.criteria.Order> order = new ArrayList<>();
			order.add(cb.asc(heading.get(QuoteHeading_.headingNo)));
			order.add(cb.asc(serviceType.get(ServiceType_.order)));
			order.add(cb.asc(root.get(Quotationitem_.itemno)));
			cq.orderBy(order);

//	    cq.distinct(true);
			return cq;
		}, prs.getStartResultsFrom(), prs.getResultsPerPage()));

		// Following for discussion / comparison - unused joins have no effect on
		// fetching, fetching rejected by count.
		/*
		 * super.completePagedResultSet(prs, Quotationitem.class, cb -> cq -> {
		 * Root<Quotationitem> root = cq.from(Quotationitem.class); Join<Quotationitem,
		 * Quotation> quotation = root.join(Quotationitem_.quotation); // Note, fetching
		 * doesn't work with count query, changed to join Join<Quotationitem,
		 * Instrument> inst = root.join(Quotationitem_.inst, JoinType.LEFT);
		 * 
		 * inst.join(Instrument_.instrumentComplementaryField, JoinType.LEFT);
		 * modules (not used) but tested in view root.join(Quotationitem_.modules,
		 * JoinType.LEFT); root.join(Quotationitem_.baseUnit, JoinType.LEFT);
		 * 
		 * cq.where(cb.equal(quotation.get(Quotation_.id), form.getQuotationid()));
		 * 
		 * List<javax.persistence.criteria.Order> order = new
		 * ArrayList<javax.persistence.criteria.Order>();
		 * order.add(cb.asc(root.get(Quotationitem_.itemno))); cq.distinct(true);
		 * 
		 * return Triple.of(root.get(Quotationitem_.id), null, order); });
		 */
		return prs;
	}

	@Override
	public List<Quotationitem> findQuotationItems(int[] qiids) {
		ArrayList<Integer> items = GenericTools.convertIntArrayToArrayList(qiids);
		return this.findQuotationItems(items);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Quotationitem> findQuotationItems(List<Integer> qiids) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Quotationitem.class).add(Restrictions.in("id", qiids));
		criteria.setFetchMode("model", FetchMode.JOIN);
		criteria.setFetchMode("model.model", FetchMode.JOIN);
		criteria.setFetchMode("model.description", FetchMode.JOIN);
		criteria.setFetchMode("model.mfr", FetchMode.JOIN);
		return (List<Quotationitem>) criteria.getExecutableCriteria(getSession()).list();
	}

	@Override
	public List<Quotationitem> findQuotationItemsForPlantIds(List<Integer> plantIds, Integer jobId, boolean defaultServiceTypeUsage) {
		return getResultList(cb -> {
			CriteriaQuery<Quotationitem> cq = cb.createQuery(Quotationitem.class);
			Root<Quotationitem> root = cq.from(Quotationitem.class);
			Join<Quotationitem, Instrument> instrument = root.join(Quotationitem_.inst);
			Join<Quotationitem, ServiceType> serviceType = root.join(Quotationitem_.serviceType);
			Join<Quotationitem, Quotation> quotation = root.join(Quotationitem_.quotation);
			Join<Quotation, JobQuoteLink> linkedJobs = quotation.join(Quotation_.linkedJobs);
			Join<JobQuoteLink, Job> jobs = linkedJobs.join(JobQuoteLink_.job);

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(instrument.get(Instrument_.plantid).in(plantIds));
			clauses.getExpressions().add(cb.equal(jobs.get(Job_.jobid), jobId));

			if (defaultServiceTypeUsage) {
				// Restrict to only default service type of job
				Subquery<ServiceType> sq = cq.subquery(ServiceType.class);
				Root<Job> sq_job = sq.from(Job.class);
				Join<Job, CalibrationType> sq_caltype = sq_job.join(Job_.defaultCalType);
				sq.where(cb.equal(sq_job.get(Job_.jobid), jobId));
				sq.select(sq_caltype.get(CalibrationType_.serviceType));
				
				clauses.getExpressions().add(cb.equal(serviceType, sq.getSelection()));
			}
			else {
				// Restrict to all compatible service types of job
				Subquery<JobType> sq = cq.subquery(JobType.class);
				Root<Job> sq_job = sq.from(Job.class);
				sq.where(cb.equal(sq_job.get(Job_.jobid), jobId));
				sq.select(sq_job.get(Job_.type));

				Join<ServiceType, JobTypeServiceType> jobTypes = serviceType.join(ServiceType_.jobTypes);
				clauses.getExpressions().add(cb.equal(jobTypes.get(JobTypeServiceType_.jobType), sq.getSelection()));
			}
			
			cq.where(clauses);

			return cq;
		});
	}

	@Override
	/*
	 * Returns an summary of quotation item data for aggregation of totals by
	 * heading and/or cal type. Designed to accommodate 1000s of quotation items
	 * without having to load all
	 */
	public List<QuotationItemSummaryDTO> findSummaryDTOs(int quoteid, boolean groupByHeading,
			boolean groupByServiceType) {
		return getResultList(cb -> {
			CriteriaQuery<QuotationItemSummaryDTO> cq = cb.createQuery(QuotationItemSummaryDTO.class);
			Root<Quotationitem> item = cq.from(Quotationitem.class);
			Join<Quotationitem, Quotation> quotation = item.join(Quotationitem_.quotation);
			List<Expression<?>> groupingList = new ArrayList<>();
			if (groupByHeading)
				groupingList.add(item.get(Quotationitem_.heading));
			if (groupByServiceType)
				groupingList.add(item.get(Quotationitem_.serviceType));
			cq.groupBy(groupingList);
			cq.where(cb.equal(quotation.get(Quotation_.id), quoteid));
			if (groupByServiceType)
				if (groupByHeading)
					cq.select(cb.construct(QuotationItemSummaryDTO.class, item.get(Quotationitem_.serviceType),
							item.get(Quotationitem_.heading), cb.count(item),
							cb.sum(item.get(Quotationitem_.finalCost))));
				else
					cq.select(cb.construct(QuotationItemSummaryDTO.class, item.get(Quotationitem_.serviceType),
							cb.count(item), cb.sum(item.get(Quotationitem_.finalCost))));
			else if (groupByHeading)
				cq.select(cb.construct(QuotationItemSummaryDTO.class, item.get(Quotationitem_.heading), cb.count(item),
						cb.sum(item.get(Quotationitem_.finalCost))));
			else
				cq.select(cb.construct(QuotationItemSummaryDTO.class, cb.count(item),
						cb.sum(item.get(Quotationitem_.finalCost))));
			return cq;
		});
	}

	@Override
	public QuotationItemCostDTO getItemCostsByInstrument(Integer quotationId, Integer jobItemId, Integer serviceTypeId) {
		return getFirstResult(cb -> {
			CriteriaQuery<QuotationItemCostDTO> cq = cb.createQuery(QuotationItemCostDTO.class);
			Root<Quotationitem> item = cq.from(Quotationitem.class);
			Join<Quotationitem, Instrument> instrument = item.join(Quotationitem_.inst);
			Join<Instrument, JobItem> jobItem = instrument.join(Instrument_.jobItems);
			jobItem.on(cb.equal(jobItem, jobItemId));
			Join<Quotationitem, ServiceType> serviceType = item.join(Quotationitem_.serviceType);
			Join<Quotationitem, QuotationCalibrationCost> calCost = item.join(Quotationitem_.calibrationCost);
			cq.where(cb.and(cb.equal(item.get(Quotationitem_.quotation), quotationId), cb.equal(serviceType.get(ServiceType_.serviceTypeId), serviceTypeId)));
			cq.select(cb.construct(QuotationItemCostDTO.class, item.get(Quotationitem_.id),
					item.get(Quotationitem_.itemno), calCost.get(QuotationCalibrationCost_.costType),
					calCost.get(QuotationCalibrationCost_.costid), calCost.get(QuotationCalibrationCost_.totalCost),
					calCost.get(QuotationCalibrationCost_.discountRate),
					calCost.get(QuotationCalibrationCost_.discountValue),
					calCost.get(QuotationCalibrationCost_.finalCost)));
			return cq;
		}).orElse(null);
	}

	@Override
	public DiscountSummaryDTO getGeneralDiscountDTO(int quoteid) {
		Criteria crit = getSession().createCriteria(Quotationitem.class);
		crit.createCriteria("quotation").add(Restrictions.idEq(quoteid));
		crit.add(Restrictions.gt("generalDiscountValue", new BigDecimal("0.00")));
		ProjectionList projection = Projections.projectionList();
		projection.add(Projections.rowCount(), "discounts");
		projection.add(Projections.sum("generalDiscountValue"), "discountValue");
		crit.setProjection(projection);
		crit.setResultTransformer(Transformers.aliasToBean(DiscountSummaryDTO.class));
		return (DiscountSummaryDTO) crit.uniqueResult();
	}

	@Override
	public Long getItemCount(int quoteid) {
		Criteria criteria = getSession().createCriteria(Quotationitem.class);
		criteria.createCriteria("quotation").add(Restrictions.idEq(quoteid));
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Quotationitem> getItemsForQuotation(int quoteid, Integer page, Integer resPerPage) {
		Criteria crit = getSession().createCriteria(Quotationitem.class);
		crit.createCriteria("quotation").add(Restrictions.idEq(quoteid));
		crit.setFetchMode("heading", FetchMode.JOIN);
		crit.setFetchMode("caltype", FetchMode.JOIN);
		crit.setFetchMode("model", FetchMode.JOIN);
		crit.setFetchMode("model.model", FetchMode.JOIN);
		crit.setFetchMode("model.description", FetchMode.JOIN);
		crit.setFetchMode("model.mfr", FetchMode.JOIN);
		crit.setFetchMode("inst", FetchMode.JOIN);
		crit.setFetchMode("inst.model", FetchMode.JOIN);
		crit.setFetchMode("inst.model.model", FetchMode.JOIN);
		crit.setFetchMode("inst.model.description", FetchMode.JOIN);
		crit.setFetchMode("inst.model.mfr", FetchMode.JOIN);
		crit.addOrder(Order.asc("heading"));
		crit.addOrder(Order.asc("caltype"));
		crit.addOrder(Order.asc("itemno"));
		// apply paging if requested
		if (page != null) {
			resPerPage = resPerPage == null ? Constants.RESULTS_PER_PAGE : resPerPage;
			crit.setFirstResult((resPerPage * page) - resPerPage);
			crit.setMaxResults(resPerPage);
		}
		List<Quotationitem> items = crit.list();
		Set<Quotationitem> set = new TreeSet<>(new TempQuotationItemComparator());
		set.addAll(items);
		return set;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer getMaxItemno(int quoteid) {
		List<Integer> list = getSession().createCriteria(Quotationitem.class)
				.add(Restrictions.eq("quotation.id", quoteid))
				.setProjection(Projections.projectionList().add(Projections.max("itemno"))).list();
		return list.get(0) == null ? 0 : list.get(0);
	}

	@Override
	public void saveOrUpdateAll(Collection<Quotationitem> items) {
		for (Quotationitem quotationitem : items)
			saveOrUpdate(quotationitem);
	}

	@Override
	public List<QuotationItemOptionDto> findQuotationItemsOptionsForPlantIdsJobIdAndDefaultUsage(List<Integer> plantIds, Integer jobId, Boolean defaultServiceTypeUsage) {
		return findQuotationItemsOptionsForIdListJobIdAndDefaultUsage(plantIds,jobId,defaultServiceTypeUsage,qi -> qi.get(Quotationitem_.inst).get(Instrument_.plantid));
    }

	private List<QuotationItemOptionDto> findQuotationItemsOptionsForIdListJobIdAndDefaultUsage(List<Integer> ids, Integer jobId, Boolean defaultServiceTypeUsage, Function<From<?,Quotationitem>,Path<Integer>> idPathProducer) {
		return getResultList(cb -> {
			val cq = cb.createQuery(QuotationItemOptionDto.class);
			val quotationItem = cq.from(Quotationitem.class);
			val instrument = quotationItem.join(Quotationitem_.inst, JoinType.LEFT);
			val quotation = quotationItem.join(Quotationitem_.quotation);
			val linkedJobs = quotation.join(Quotation_.linkedJobs);
			val job = linkedJobs.join(JobQuoteLink_.job);
			val serviceType = quotationItem.join(Quotationitem_.serviceType);

			val defaultServiceTypePredicateGenerator = defaultServiceTypeUsage?
					serviceTypePredicateGenerator(jobId,serviceType): jobTypesPredicateGenerator(jobId,serviceType);

			val idPath = idPathProducer.apply(quotationItem);
			cq.where(
					idPath.in(ids),
					cb.equal(job,jobId),
					defaultServiceTypePredicateGenerator.apply(cb,cq)
			);


			val valueExpression = trimAndConcatWithSeparator(quotation.get(Quotation_.qno),
					quotationItem.get(Quotationitem_.itemno).as(String.class),".")
					.appendWithSeparator(joinTranslation(cb,serviceType,ServiceType_.shortnameTranslation, LocaleContextHolder.getLocale())," - ")
					.appendWithSeparator(referenceNumberExpression(cb,quotationItem,instrument)," - ")
					.appendWithSeparator(quotation.join(Quotation_.currency).get(SupportedCurrency_.currencySymbol)," - ")
					.append(quotationItem.get(Quotationitem_.finalCost).as(String.class))
					.apply(cb)
					;

			cq.select(cb.construct(QuotationItemOptionDto.class,
					idPath,
					quotationItem.get(Quotationitem_.id),
                    valueExpression
					));

			return cq;
		});
	}

	@Override
	public List<QuotationItemOptionDto> findQuotationItemsOptionsForModelIdsJobIdAndDefaultUsage(List<Integer> modelIdIds, Integer jobId, Boolean defaultServiceTypeUsage) {
		return findQuotationItemsOptionsForIdListJobIdAndDefaultUsage(modelIdIds,jobId,defaultServiceTypeUsage,qi ->
				qi.get(Quotationitem_.model).get(InstrumentModel_.modelid));
	}

	private Expression<String> referenceNumberExpression(CriteriaBuilder cb, From<?,Quotationitem> quotationitem, From<?,Instrument> instrument){
		return cb.<String>selectCase()
				.when(cb.and(cb.isNotNull(instrument),cb.isNotNull(instrument.get(Instrument_.plantno)),cb.notEqual(instrument.get(Instrument_.plantno),"")),trimAndConcatWithWhitespace(cb.literal("(P)"),instrument.get(Instrument_.plantno)).apply(cb))
				.when(cb.and(cb.isNotNull(instrument),cb.isNotNull(instrument.get(Instrument_.serialno)),cb.notEqual(instrument.get(Instrument_.serialno),"")),trimAndConcatWithWhitespace(cb.literal("(S)"),instrument.get(Instrument_.serialno)).apply(cb))
				.when(cb.and(cb.isNotNull(quotationitem.get(Quotationitem_.plantno)),cb.notEqual(quotationitem.get(Quotationitem_.plantno),"")),
						trimAndConcatWithWhitespace(cb.literal("(QIP)"),quotationitem.get(Quotationitem_.plantno)).apply(cb))
				.otherwise("");

	}

	private BiFunction<CriteriaBuilder,CriteriaQuery<?>,Predicate> serviceTypePredicateGenerator(Integer jobId, From<?,ServiceType> serviceType){
		return (cb,cq) -> {
			val sq = cq.subquery(ServiceType.class);
			val job = sq.from(Job.class);
			sq.where(cb.equal(job,jobId));
			sq.select(job.join(Job_.defaultCalType).get(CalibrationType_.serviceType));
			return cb.equal(serviceType,sq.getSelection());
		};
	}

	private BiFunction<CriteriaBuilder,CriteriaQuery<?>,Predicate> jobTypesPredicateGenerator(Integer jobId, From<?,ServiceType> serviceType){
	    return (cb,cq) -> {
	    	val sq = cq.subquery(JobType.class);
	    	val job = sq.from(Job.class);
	    	sq.where(cb.equal(job,jobId));
	    	sq.select(job.get(Job_.type));
	    	return cb.equal(serviceType.join(ServiceType_.jobTypes).get(JobTypeServiceType_.jobType),sq.getSelection());
		};
	}



	@Override
	public List<Quotationitem> findQuotationItemsForModelIds(List<Integer> modelIds, Integer jobId, boolean defaultServiceTypeUsage) {
		return getResultList(cb -> {
			CriteriaQuery<Quotationitem> cq = cb.createQuery(Quotationitem.class);
			Root<Quotationitem> root = cq.from(Quotationitem.class);
			Join<Quotationitem, InstrumentModel> model = root.join(Quotationitem_.model);
			Join<Quotationitem, ServiceType> serviceType = root.join(Quotationitem_.serviceType);
			Join<Quotationitem, Quotation> quotation = root.join(Quotationitem_.quotation);
			Join<Quotation, JobQuoteLink> linkedJobs = quotation.join(Quotation_.linkedJobs);
			
			Join<JobQuoteLink, Job> jobs = linkedJobs.join(JobQuoteLink_.job);
			jobs.on(cb.equal(jobs.get(Job_.jobid), jobId));

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(model.get(InstrumentModel_.modelid).in(modelIds));
			
			if (defaultServiceTypeUsage) {
				// Restrict to only quotation items matching default service type of job
				Subquery<ServiceType> sq = cq.subquery(ServiceType.class);
				Root<Job> sq_job = sq.from(Job.class);
				Join<Job, CalibrationType> sq_caltype = sq_job.join(Job_.defaultCalType);
				sq.where(cb.equal(sq_job.get(Job_.jobid), jobId));
				sq.select(sq_caltype.get(CalibrationType_.serviceType));
				
				clauses.getExpressions().add(cb.equal(serviceType, sq.getSelection()));
			}
			else {
				// Restrict to quotation items matching all compatible service types of job
				Subquery<JobType> sq = cq.subquery(JobType.class);
				Root<Job> sq_job = sq.from(Job.class);
				sq.where(cb.equal(sq_job.get(Job_.jobid), jobId));
				sq.select(sq_job.get(Job_.type));

				Join<ServiceType, JobTypeServiceType> jobTypes = serviceType.join(ServiceType_.jobTypes);
				clauses.getExpressions().add(cb.equal(jobTypes.get(JobTypeServiceType_.jobType), sq.getSelection()));
			}

			cq.where(clauses);
			return cq;
		});
	}
}