package org.trescal.cwms.core.quotation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.quotation.service.CompanyInstrumentsToQuotationService;

@Controller
@JsonController
public class AddInstrumentToQuoteJsonController {

	@Autowired
	private CompanyInstrumentsToQuotationService companyInstrumentsToQuotationService;

	@RequestMapping(value = "addinstrumenttoquotebasket.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultWrapper sourceCompanyInstrumentToQIValues(@RequestParam(name = "quoteid", required = true) int quoteid,
			@RequestParam(name = "subdivid", required = true) int subdivid,
			@RequestParam(name = "instArray[]", required = true) int[] instArray) {
		return this.companyInstrumentsToQuotationService.sourceCompanyInstrumentToQIValues(instArray, quoteid,
				subdivid);
	}
}
