package org.trescal.cwms.core.quotation.entity.quotationrequest.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.quotation.dto.QuotationRequestProjectionDTO;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequestStatus;

public interface QuotationRequestService extends BaseService<QuotationRequest, Integer>
{
	ResultWrapper deleteQR(int id);
	
	List<QuotationRequestProjectionDTO> getQuotationRequestByStatusNew(QuotationRequestStatus status, Integer allocatedSubdivId);

}