package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general.db;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general.DefaultGeneralCondition;

@Repository("DefaultGeneralConditionDao")
public class DefaultGeneralConditionDaoImpl extends AllocatedToCompanyDaoImpl<DefaultGeneralCondition, Integer> implements DefaultGeneralConditionDao
{
	@Override
	protected Class<DefaultGeneralCondition> getEntity() {
		return DefaultGeneralCondition.class;
	}
	
	@Override
	public DefaultGeneralCondition getLatest(Company allocatedCompany) {
		Criteria criteria = getSession().createCriteria(DefaultGeneralCondition.class);
		criteria.add(getCompanyCrit(allocatedCompany));
		criteria.addOrder(Order.desc("lastModified"));
		criteria.setFirstResult(0);
		criteria.setMaxResults(1);
		return (DefaultGeneralCondition) criteria.uniqueResult(); 
	}
}