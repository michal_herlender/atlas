package org.trescal.cwms.core.quotation.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.recall.entity.recallitem.RecallItem;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RecallToQuotationForm {
    private String clientRef;
    private Contact contact;
    private String currencyCode;

    private boolean fromRecall;
    private RecallDetail rdetail;
    private List<RecallItem> recallitems;
    private LocalDate reqDate;

    private Contact sourcedBy;
    private List<Integer> toQuotePlantIds;


}
