package org.trescal.cwms.core.quotation.entity.quotecaldefaults.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.quotation.entity.quotecaldefaults.QuoteCalDefaults;

@Repository("QuoteCalDefaultsDao")
public class QuoteCalDefaultsDaoImpl extends BaseDaoImpl<QuoteCalDefaults, Integer> implements QuoteCalDefaultsDao {
	
	@Override
	protected Class<QuoteCalDefaults> getEntity() {
		return QuoteCalDefaults.class;
	}
}