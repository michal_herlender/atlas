package org.trescal.cwms.core.quotation.entity.quotationrequest.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.quotation.dto.QuotationRequestProjectionDTO;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequestStatus;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;

//import uk.org.antech.ws.beans.QuoteRequest;

@Service("QuotationRequestService")
public class QuotationRequestServiceImpl extends BaseServiceImpl<QuotationRequest, Integer> implements QuotationRequestService {

	@Value("#{props['cwms.users.automatedusername']}")
	private String automatedUserName;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private QuotationRequestDao quotationRequestDao;
	@Autowired
	private SystemComponentService scServ;
	@Override
	protected BaseDao<QuotationRequest, Integer> getBaseDao() {
		return quotationRequestDao;
	}
	
	@Override
	public ResultWrapper deleteQR(int id)
	{
		QuotationRequest quotationRequest = this.get(id);
		if (quotationRequest != null) {
			this.delete(quotationRequest);
			return new ResultWrapper(true, "");
		}
		else {
			return new ResultWrapper(false, "The quote request could not be found");
		}
	}
	
	@Override
	public QuotationRequest get(Integer id)
	{
		QuotationRequest request = this.quotationRequestDao.find(id);
		// 2018-12-12 GB : Prevent directory lookup on repeated gets.
		if (request != null && request.getDirectory() == null)
			request.setDirectory(this.compDirServ.getDirectory(this.scServ.findComponent(Component.QUOTATION_REQUEST), request.getRequestNo()));
		return request;
	}

	@Override
	public List<QuotationRequestProjectionDTO> getQuotationRequestByStatusNew(QuotationRequestStatus status,
			Integer allocatedSubdivId) {
		return this.quotationRequestDao.getQuotationRequestByStatusNew(status, allocatedSubdivId);
	}
	
}