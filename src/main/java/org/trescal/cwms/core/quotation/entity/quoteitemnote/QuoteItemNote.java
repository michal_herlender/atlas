package org.trescal.cwms.core.quotation.entity.quoteitemnote;

import javax.persistence.*;

import lombok.Setter;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;

@Entity
@Table(name = "quoteitemnote")
@Setter
public class QuoteItemNote extends Note {

    private Quotationitem quotationitem;

    @Override
    @Transient
    public NoteType getNoteType() {
        return NoteType.QUOTEITEMNOTE;
    }

    @ManyToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name = "id", unique = false, nullable = false, insertable = true, updatable = true, foreignKey = @ForeignKey(name = "FK_quoteitemnote_quotationitem"))
    public Quotationitem getQuotationitem() {
        return quotationitem;
    }
}