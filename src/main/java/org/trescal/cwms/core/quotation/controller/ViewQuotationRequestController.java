package org.trescal.cwms.core.quotation.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest;
import org.trescal.cwms.core.quotation.entity.quotationrequest.db.QuotationRequestService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;

@Controller @IntranetController
@SessionAttributes(Constants.HTTPSESS_NEW_FILE_LIST)
public class ViewQuotationRequestController
{
	@Autowired
	private QuotationRequestService quotationRequestService;
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private SystemComponentService systemCompServ;
	
	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
		return new ArrayList<String>();
	}
	
	@RequestMapping(value="/viewquotationrequest.htm")
	protected ModelAndView handleRequestInternal(
			@RequestParam(value="id", required=false, defaultValue="0") Integer id,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles) throws Exception
	{
		QuotationRequest qr = this.quotationRequestService.get(id);
		if ((qr == null) || (id == 0)) {
			throw new Exception("Quotation request could not be found");
		}
		FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ.getFilesForComponentRoot(Component.QUOTATION_REQUEST, qr.getId(), newFiles);
		Map<String, Object> modelMap = new HashMap<String, Object>();
		modelMap.put("request", qr);
		modelMap.put(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
		modelMap.put(Constants.REFDATA_FILES_NAME, this.fileBrowserServ.getFilesName(fileBrowserWrapper));
		modelMap.put(Constants.REFDATA_SYSTEM_COMPONENT, this.systemCompServ.findComponent(Component.QUOTATION_REQUEST));
		return new ModelAndView("trescal/core/quotation/viewquotationrequest", "command", modelMap);
	}
}
