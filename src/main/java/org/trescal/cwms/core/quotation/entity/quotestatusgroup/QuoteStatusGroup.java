package org.trescal.cwms.core.quotation.entity.quotestatusgroup;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

public enum QuoteStatusGroup
{
	REQUIRINGATTENTION(1, "quotestatusgroup.requiringattention", "Requiring Attention"), 
	WEBREQUESTS(2, "quotestatusgroup.webrequests", "Web Requests");
	
	private int id;
	private ReloadableResourceBundleMessageSource messages;
	private Locale loc = null;
	private String messageCode;
	private String name;
	
	@Component
	public static class QuoteStatusGroupMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (QuoteStatusGroup rt : EnumSet.allOf(QuoteStatusGroup.class))
               rt.setMessageSource(messages);
        }
	}
	
	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}

	private QuoteStatusGroup(int id, String messageCode, String name) {
		this.id = id;
		this.messageCode = messageCode;
		this.name = name;
	}
	
	public String getDescription()
	{
		return this.toString();
	}

	public int getId()
	{
		return this.id;
	}

	public String getName()
	{
		loc = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.name, loc);
		}
		return this.toString();
	}
	
	public static QuoteStatusGroup getQuoteStatusGroupById(int id) {
		switch (id) {
		case 1: return REQUIRINGATTENTION;
		case 2: return WEBREQUESTS;
		}
		return null;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
}
