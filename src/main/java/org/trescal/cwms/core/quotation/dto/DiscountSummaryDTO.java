package org.trescal.cwms.core.quotation.dto;

import java.math.BigDecimal;

public class DiscountSummaryDTO {
	private long discounts;
	private BigDecimal discountValue;
	
	public DiscountSummaryDTO() {
		discounts = 0;
		discountValue = new BigDecimal("0.00");
	}
	
	public void add(DiscountSummaryDTO dto) {
		this.discounts += dto.discounts;
		// If no results found, sum may be null
		if (dto.discountValue != null) this.discountValue = this.discountValue.add(dto.discountValue);
	}
	
	public long getDiscounts() {
		return discounts;
	}
	public BigDecimal getDiscountValue() {
		return discountValue == null ? new BigDecimal("0.00") : discountValue;
	}
	public void setDiscounts(long discounts) {
		this.discounts = discounts;
	}
	public void setDiscountValue(BigDecimal totalValue) {
		this.discountValue = totalValue;
	}
}
