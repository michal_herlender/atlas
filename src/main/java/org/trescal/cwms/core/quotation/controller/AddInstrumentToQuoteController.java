package org.trescal.cwms.core.quotation.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.addresstype.AddressType;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteheading.db.QuoteHeadingService;
import org.trescal.cwms.core.quotation.enums.NewQuoteInstSortType;
import org.trescal.cwms.core.quotation.form.AddInstrumentToQuoteForm;
import org.trescal.cwms.core.quotation.form.AddInstrumentToQuoteValidator;
import org.trescal.cwms.core.quotation.service.AddInstrumentsToQuotationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.TriState;

/**
 * Displays a page listing the {@link Instrument} entities associated to a
 * {@link Company}. The page can be submitted back to itself to narrow or widen
 * the list of results. By default no instruments are listed, but a total count
 * is given.
 */
@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME })
public class AddInstrumentToQuoteController {
	@Value("${cwms.config.quotation.size.restrictaddinstrument}")
	private long sizeRestrictAddInstrument;

	@Autowired
	private AddInstrumentsToQuotationService addInstrumentsToQuotationService;
	@Autowired
	private AddressService addServ;
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private InstrumService instrumentServ;
	@Autowired
	private MessageSource messages;
	@Autowired
	private QuoteHeadingService qhServ;
	@Autowired
	private QuotationItemService qiServ;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private SubdivService subServ;
	@Autowired
	private TranslationService translationService;
	@Autowired
	private AddInstrumentToQuoteValidator validator;

	protected final Logger logger = LoggerFactory.getLogger(AddInstrumentToQuoteController.class);

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("form")
	protected AddInstrumentToQuoteForm formBackingObject(@RequestParam(value = "id", required = true) Integer quoteid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		long totalItemCount = qiServ.getItemCount(quoteid);
		// 2016-02-22 - Added check of number of quotation items until
		// performance issues fully resolved
		if (totalItemCount >= sizeRestrictAddInstrument) {
			throw new RuntimeException("This quotation has " + totalItemCount
					+ " items and due to temporary performance restrictions, quotations with "
					+ sizeRestrictAddInstrument + " items or more cannot have instruments added.");
		}

		AddInstrumentToQuoteForm form = new AddInstrumentToQuoteForm();
		form.setQuotationId(quoteid);
		Quotation quotation = this.getQuotation(quoteid);
		Company company = quotation.getContact().getSub().getComp();
		// initialise form search
		InstrumentAndModelSearchForm<Instrument> search = new InstrumentAndModelSearchForm<Instrument>();

		search.setPageNo(1);
		search.setResultsPerPage(Constants.RESULTS_PER_PAGE);
		search.setCompId(company.getCoid());

		form.setSubdivid(quotation.getContact().getSub().getSubdivid());
		form.setSearch(search);
		// set initial search orientation
		form.setAscOrder(true);

		return form;
	}

	private Quotation getQuotation(int quoteid) {
		Quotation q = this.quoteServ.get(quoteid);
		if ((quoteid == 0) || (q == null)) {
			throw new RuntimeException("The quotation requested couldn't be found.");
		}
		return q;
	}

	/**
	 * Returns the number of not-null elements in the basket (different than
	 * size when elements deleted)
	 */
	private long getBasketCount(AddInstrumentToQuoteForm form) {
		return form.getBasketIds() == null ? 0
				: form.getBasketIds().stream().filter(plantid -> plantid != null).count();
	}

	/**
	 * Returns list of choices for "out of cal" options
	 */
	private List<KeyValue<TriState, String>> getOutOfCalList(Locale locale) {
		List<KeyValue<TriState, String>> result = new ArrayList<>();
		result.add(new KeyValue<>(TriState.UNDEFINED,
				this.messages.getMessage("searchinstrument.either", null, "Either", locale)));
		result.add(new KeyValue<>(TriState.TRUE, this.messages.getMessage("searchinstrument.outofcalibrationonly", null,
				"Out of calibration only", locale)));
		result.add(new KeyValue<>(TriState.FALSE,
				this.messages.getMessage("searchinstrument.incalibrationonly", null, "In calibration only", locale)));
		return result;
	}

	/**
	 * Returns list of choices for "out of cal" options
	 */
	private List<KeyValue<Boolean, String>> getAscOrderList(Locale locale) {
		List<KeyValue<Boolean, String>> result = new ArrayList<>();
		result.add(new KeyValue<>(Boolean.TRUE,
				this.messages.getMessage("quotaddinst.ascending", null, "Ascending", locale)));
		result.add(new KeyValue<>(Boolean.FALSE,
				this.messages.getMessage("quotaddinst.descending", null, "Descending", locale)));
		return result;
	}

	/**
	 * TODO needs translations in enum
	 * 
	 * @return
	 */
	private List<KeyValue<NewQuoteInstSortType, String>> getSortTypeList() {
		List<KeyValue<NewQuoteInstSortType, String>> result = new ArrayList<>();
		for (NewQuoteInstSortType sortType : NewQuoteInstSortType.values()) {
			result.add(new KeyValue<>(sortType, sortType.getDisplayName()));
		}

		return result;
	}

	/**
	 * TODO replace with service types - can use 1 query
	 * 
	 * @param locale
	 * @return
	 */
	private List<KeyValue<Integer, String>> getDTOList(Locale locale) {
		return this.calTypeServ.getCalTypes().stream()
				.map(ct -> new KeyValue<Integer, String>(ct.getCalTypeId(), translationService.getCorrectTranslation(
						ct.getServiceType().getShortnameTranslation(), LocaleContextHolder.getLocale())))
				.collect(Collectors.toList());
	}

	@RequestMapping(value = "/addinstrumenttoquote.htm", method = RequestMethod.GET)
	protected String referenceData(Model model, Locale locale, @ModelAttribute("form") AddInstrumentToQuoteForm form,
			Boolean searchbasketTabSelected) {
		// Moved all reference data from form to here
		Quotation quotation = getQuotation(form.getQuotationId());
		Company company = quotation.getContact().getSub().getComp();
		model.addAttribute("basketCount", getBasketCount(form));
		model.addAttribute("outOfCalList", getOutOfCalList(locale));
		model.addAttribute("sortTypeList", getSortTypeList());
		model.addAttribute("ascOrderList", getAscOrderList(locale));
		model.addAttribute("totalinstrumentcount", this.instrumentServ.getCompanyInstrumentCount(company.getCoid()));
		model.addAttribute("sublist", this.subServ.getAllSubdivFromCompany(company.getCoid(), true));
		if (form.getSubdivid() != null && form.getSubdivid() != 0) {
			model.addAttribute("addlist", this.addServ.getAllActiveSubdivAddressesKeyValue(form.getSubdivid(), AddressType.WITHOUT, false));
			model.addAttribute("conlist", this.conServ.getContactDtoList(null, form.getSubdivid(), true));
		}
		model.addAttribute("caltypes", this.getDTOList(locale));
		model.addAttribute("currencyERSymbol", quotation.getCurrency().getCurrencyERSymbol());
		model.addAttribute("headings", this.qhServ.findQuotationHeadings(form.getQuotationId()));
		model.addAttribute("quotation", quotation);
		model.addAttribute("searchbasketTabSelected", searchbasketTabSelected);
		
		return "trescal/core/quotation/addinstrumenttoquote";
	}

	@RequestMapping(value = "/addinstrumenttoquote.htm", method = RequestMethod.POST)
	protected String onSubmit(Model model, Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute("form") @Validated AddInstrumentToQuoteForm form, BindingResult bindingResult)
			throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData(model, locale, form, true);
		}
		// check submit type
		if (form.getSubmitType().equalsIgnoreCase("")) {
			// use coid of quotation contact for search
			int resultsPerPage = form.getSearch().getResultsPerPage();
			int currentPage = form.getSearch().getPageNo();
			Boolean outOfCal = form.getOutOfCal().getBooleanValue();
			logger.info("company wide : " + form.getSearch().getSearchAllCompanies());
			PagedResultSet<Instrument> rs = this.instrumentServ.searchInstrumentsPaged(
					new PagedResultSet<Instrument>(resultsPerPage, currentPage), form.getSearch(), form.getSubdivid(),
					form.getPersonid(), form.getAddressid(), outOfCal, form.getSortType(), form.isAscOrder(), locale);
			form.getSearch().setRs(rs);
			// return search results
			return referenceData(model, locale, form, false);
		} else {
			if ((form.getBasketIds() != null) && (form.getBasketIds().size() > 0)) {
				this.addInstrumentsToQuotationService.addInstruments(subdivDto.getKey(), username, form, locale);
			}
			return "redirect:viewquotation.htm?id=" + form.getQuotationId();
		}
	}
}