package org.trescal.cwms.core.quotation.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.form.AddInstrumentToQuoteForm;

public interface AddInstrumentsToQuotationService {

	void addInstruments(Integer allocatedSubdivId, String username, AddInstrumentToQuoteForm form, Locale locale);

	/**
	 * Import quotation items using instrument / get prices and service types
	 * from the file
	 * 
	 * @param allocatedSubdivId
	 *            the {@link Instrument} ID.
	 * @param contactId
	 *            the {@link Contact} ID.
	 * @param plantIds
	 *            the {@link Instrument} IDs.
	 * @param serviceTypeIds
	 *            the {@link Service type} IDs.
	 * @param finalPrices
	 *            the {@link Instrument} IDs.
	 * @param discountPrices.
	 * @param catalogPrices.
	 * @param publicNotes.
	 * @param publicNotes.
	 * @param quoteId
	 *            the {@link Quotation} ID.
	 * @param locale
	 *            the {@link Locale} .
	 */
	Set<Quotationitem> importQuotationItemsUsingInst__PricesFromFile(Integer allocatedSubdivId, Integer contactId,
			List<Integer> plantIds, Map<Integer, Integer> serviceTypeIds, Map<Integer, BigDecimal> finalPrices,
			Map<Integer, BigDecimal> discountPrices, Map<Integer, BigDecimal> catalogPrices,
			Map<Integer, String> publicNotes, Map<Integer, String> privateNotes, Integer quoteId, Locale locale);

	/**
	 * Import quotation items using instrument / get prices and service types
	 * from the most recent job items
	 * 
	 * @param allocatedSubdivId
	 *            the {@link Instrument} ID.
	 * @param contactId
	 *            the {@link Contact} ID.
	 * @param plantIds
	 *            the {@link Instrument} IDs.
	 * @param discountPrices.
	 * @param catalogPrices.
	 * @param publicNotes.
	 * @param publicNotes.
	 * @param quoteId
	 *            the {@link Quotation} ID.
	 * @param locale
	 *            the {@link Locale} .
	 */
	Set<Quotationitem> importQuotationItemsUsingInst_PricesFromLatestJobItem(Integer allocatedSubdivId, Integer contactId,
			List<Integer> plantIds, Map<Integer, BigDecimal> discountPrices, Map<Integer, BigDecimal> catalogPrices,
			Map<Integer, String> publicNotes, Map<Integer, String> privateNotes, Integer quoteId, Locale locale);
	/**
	 * Import quotation items using instrument / get prices and service types
	 * from the file in batch mode
	 * 
	 * @param allocatedSubdivId
	 *            the {@link Instrument} ID.
	 * @param contactId
	 *            the {@link Contact} ID.
	 * @param instIndex
	 *            the index of each item
	 * @param plantIds
	 *            the {@link Instrument} IDs.
	 * @param instWithIndex
	 * 		    instrument/index
	 * @param serviceTypeIds
	 *            the {@link Service type} IDs.
	 * @param finalPrices
	 *            the {@link Instrument} IDs.
	 * @param discountPrices.
	 * @param catalogPrices.
	 * @param publicNotes.
	 * @param publicNotes.
	 * @param quoteId
	 *            the {@link Quotation} ID.
	 * @param locale
	 *            the {@link Locale} .
	 */
	Set<Quotationitem> importQuotationItemsUsingInst_PricesFromFile_BatchMode(Integer allocatedSubdivId, Integer contactId, List<Integer> instIndex, 
			List<Integer> plantIds, Map<Integer, Integer> instWithIndex, Map<Integer, Integer> serviceTypeIds,
			Map<Integer, BigDecimal> finalPrices, Map<Integer, BigDecimal> discountPrices, Map<Integer, BigDecimal> catalogPrices,
			Map<Integer, String> publicNotes, Map<Integer, String> privateNotes, Integer quoteId, Locale locale);
}
