package org.trescal.cwms.core.quotation.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.GeneralServiceOperation;
import org.trescal.cwms.core.jobs.generalserviceoperation.entity.gso.db.GeneralServiceOperationService;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostMarkupSource;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostSource;
import org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.db.ThirdPartyPricingItemService;
import org.trescal.cwms.core.pricing.jobcost.dto.ModelJobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.db.JobCostingCalibrationCostService;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.QuotationItemComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.comparators.QuotationItemSortType;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteitemnote.QuoteItemNote;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequest;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.db.ScheduledQuotationRequestService;
import org.trescal.cwms.core.quotation.enums.QuoteGenerationStrategy;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_ScheduledQuoteRequestFail;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_ScheduledQuoteRequestSuccess;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service("ScheduledQuoteRequestToQuotationService")
public class ScheduledQuoteRequestToQuotationServiceImpl implements ScheduledQuoteRequestToQuotationService {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("#{props['cwms.admin.email']}")
	private String adminEmail;
	@Autowired
	private CatalogPriceService catalogPriceService;
	@Autowired
	private EmailContent_ScheduledQuoteRequestFail ecScheduledQuoteRequestFail;
	@Autowired
	private EmailContent_ScheduledQuoteRequestSuccess ecScheduledQuoteRequestSuccess;
	@Autowired
	private EmailService emailService;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private JobCostingCalibrationCostService jobCostingCalibrationCostService;
	@Autowired
	private QuotationItemService quoteItemService;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private ScheduledQuotationRequestService scheduledQuotationRequestService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private ThirdPartyPricingItemService tpPricingItemService;
	@Autowired
	private MessageSource messages;	
	@Autowired
	private GeneralServiceOperationService gsoService;
	@Autowired
	private TaxCalculator taxCalculator;
	
	@Override
	public void notifyFailure(ScheduledQuotationRequest request, Exception exception)
	{
		Locale locale = supportedLocaleService.getPrimaryLocale();
		
		EmailContentDto contentDto = this.ecScheduledQuoteRequestFail.getContent(request, exception, locale);
		
		String docString = contentDto.getBody();
		String subject = contentDto.getSubject();

		this.emailService.sendAdvancedEmail(subject, this.adminEmail, Collections.singletonList(this.adminEmail), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), docString, true, false);
	}

	@Override
	public void notifySuccess(ScheduledQuotationRequest request)
	{
		if (!StringUtils.isEmpty(request.getRequestBy().getEmail()))
		{
			Locale locale = request.getRequestBy().getLocale();
			
			EmailContentDto contentDto = this.ecScheduledQuoteRequestSuccess.getContent(request, locale);
			
			String docString = contentDto.getBody();
			String subject = contentDto.getSubject();
			this.emailService.sendAdvancedEmail(subject, this.adminEmail, Collections.singletonList(request.getRequestBy().getEmail()), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), docString, true, false);
		}

	}
	
	@Override
	public Quotation prepareAndSaveScheduledQuoteToQuotation(ScheduledQuotationRequest request, boolean emailNotification, Contact currentContact, Subdiv allocatedSubdiv, Company allocatedCompany)
	{
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Quotation quotation = null;
		try {
			logger.info("Preparing quotation from scheduled request "
					+ request.getId() + " - "
					+ request.getQuoteContact().getName() + " ("
					+ request.getQuoteContact().getSub().getComp().getConame()
					+ ") :" + request.getScope() + ", "
					+ request.getRequestType());
			Contact quoteContact = request.getQuoteContact();
			quotation = this.quoteServ.createTemplateNewQuotation(currentContact, quoteContact, allocatedSubdiv, allocatedCompany, request.getLocale());
			logger.info("template created");
			// the template quote notes will be created with getCurrentContact()
			// contacts - but this will be null because this is being run by a
			// scheduled task. Instead override and set the quote request
			// requester as the contact
			for (QuoteNote qn : quotation.getNotes()) qn.setSetBy(request.getRequestBy());
			quotation.setContact(request.getQuoteContact());
			if(quotation.getCreatedBy() == null) quotation.setCreatedBy(request.getRequestBy());
			quotation.setSourcedBy(request.getRequestBy());
			// add items using the chosen generation strategy
			stopWatch.stop();
			logger.info("start quotation items generation: " + stopWatch.getTotalTimeSeconds() + ", strategy: " + request.getStrategy());
			stopWatch.start();
			switch (request.getStrategy())
			{
				case SINGLE_QITEM_PER_MODEL:
					quotation.setQuotationitems(this.prepareCostPerInstrumentQuotationitems(request, quotation));
					break;
				case SINGLE_QITEM_PER_INSTRUMENT:
					quotation.setQuotationitems(this.prepareCostPerInstrumentQuotationitems(request, quotation));
					break;
				case SINGLE_QITEM_PER_INSTRUMENT_WITH_PLANTNOS:
					quotation.setQuotationitems(this.prepareCostPerInstrumentQuotationitems(request, quotation));
					break;
				case SINGLE_QITEM_PER_MODEL_WITH_INSTRUMENT_QTY:
					quotation.setQuotationitems(this.prepareCostPerInstrumentQuotationitems(request, quotation));
					break;
			}
			stopWatch.stop();
			logger.info("finish quotation items generation: " + stopWatch.getTotalTimeSeconds() + "s");
			stopWatch.start();
			CostCalculator.updatePricingTotalCost(quotation, quotation.getQuotationitems());
			taxCalculator.calculateAndSetVATaxAndFinalCost(quotation);
			QuoteNote qn = new QuoteNote();
			qn.setActive(true);
			qn.setPublish(false);
			qn.setNote(messages.getMessage("createquot.createdfromscheduledquotationrequest", null, "Created from scheduled quotation request", request.getLocale()));
			qn.setLabel("");
			qn.setSetBy(request.getRequestBy());
			qn.setSetOn(new Date());
			qn.setQuotation(quotation);
			quotation.getNotes().add(qn);
			this.quoteServ.save(quotation);
			request.setProcessed(true);
			request.setProcessedOn(new Date());
			request.setQuotation(quotation);
			request = this.scheduledQuotationRequestService.merge(request);
			stopWatch.stop();
			logger.info("Completed prepartion of quotation " + request.getQuotation().getQno() + " in " + stopWatch.getTotalTimeSeconds() + "s");
			if (emailNotification)
				this.notifySuccess(request);
		}
		catch (Exception e) {
			e.printStackTrace();
			if (emailNotification)
				this.notifyFailure(request, e);
		}
		return quotation;
	}
	
	@Override
	public Set<Quotationitem> prepareCostPerInstrumentQuotationitems(ScheduledQuotationRequest request, Quotation quotation) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		quotation.setSortType(QuotationItemSortType.MODEL);
		Subdiv allocatedSubdiv = request.getRequestBy().getSub();
		Company company = null;
		Subdiv subdiv = null;
		Contact contact = null;
		switch (request.getScope()) {
		case COMPANY:
			company = request.getQuoteContact().getSub().getComp();
			break;
		case CONTACT:
			contact = request.getQuoteContact();
			break;
		case SUBDIV:
			subdiv = request.getQuoteContact().getSub();
			break;
		default:
			break;
		}
		List<Instrument> instruments = instrumentService.getAll(company, subdiv, contact, request.getRequestType(), request.getDateRange(), true);
		stopWatch.stop();
		logger.info("load " + instruments.size() + " instruments in " + stopWatch.getTotalTimeSeconds() + "s");
		stopWatch.start();
		Map<Instrument, ServiceType> serviceTypeMap = new HashMap<Instrument, ServiceType>();
		instruments.forEach(i -> {
			GeneralServiceOperation lastInstrumentGso = this.gsoService.getLastInstrumentGSO(i);
			serviceTypeMap.put(i, i.getLastCal() == null || i.getLastCal().getCalType() == null ?
				(lastInstrumentGso == null || lastInstrumentGso.getCalType() == null ?
				(i.getDefaultServiceType() == null ? request.getDefaultCalType().getServiceType() : i.getDefaultServiceType()):
					lastInstrumentGso.getCalType().getServiceType()) : 
					i.getLastCal().getCalType().getServiceType());});
		stopWatch.stop();
		logger.info("compute service types after " + stopWatch.getTotalTimeSeconds() + "s");
		stopWatch.start();
		Map<ServiceType, List<Instrument>> instrumentMap = instruments.stream().collect(Collectors.groupingBy(i -> serviceTypeMap.get(i)));
		stopWatch.stop();
		logger.info("instrument map generated after " + stopWatch.getTotalTimeSeconds() + "s");
		stopWatch.start();
		Map<ServiceType, List<InstrumentModel>> modelMap = new HashMap<ServiceType, List<InstrumentModel>>();
		instrumentMap.forEach((s,i) -> modelMap.put(s, i.stream().map(Instrument::getModel).distinct().collect(Collectors.toList())));
		stopWatch.stop();
		logger.info("model map generated after " + stopWatch.getTotalTimeSeconds() + "s");
		stopWatch.start();
		Map<ServiceType, Map<Integer, JobCostingCalibrationCost>> latestJobCostingCost = new HashMap<ServiceType, Map<Integer, JobCostingCalibrationCost>>();
		for(ServiceType serviceType: modelMap.keySet()) {
			CalibrationType calType = serviceType.getCalibrationType();
			if(calType != null) {
				List<ModelJobCostingCalibrationCost> jcCalCosts = this.jobCostingCalibrationCostService.findMostRecentCostForModels(
						modelMap.get(serviceType).stream().map(InstrumentModel::getModelid).collect(Collectors.toList()), calType, company);
				Map<Integer, JobCostingCalibrationCost> modelCosts = new HashMap<Integer, JobCostingCalibrationCost>();
				jcCalCosts.forEach(modelCost -> modelCosts.put(modelCost.getModelid(), modelCost.getCalibrationCost()));
				latestJobCostingCost.put(serviceType, modelCosts);
			}
		}
		stopWatch.stop();
		logger.info("job costing costs generated after " + stopWatch.getTotalTimeSeconds() + "s");
		stopWatch.start();
		Map<ServiceType, Map<Integer, CatalogPrice>> catalogPrices = new HashMap<ServiceType, Map<Integer, CatalogPrice>>();
		// GB 2018-02-06 Expanded / changed this to handle duplicate CatalogPrice entries for one model/serviceType/company;
		// reducing map via stream would previously throw duplicate key exception.
		for(ServiceType serviceType: modelMap.keySet()) {
			List<InstrumentModel> modelList = modelMap.get(serviceType);
			List<CatalogPrice> priceList = catalogPriceService.findAll(serviceType, modelList, allocatedSubdiv.getComp());
			Map<Integer, CatalogPrice> priceMap = new HashMap<>();
			priceList.stream().forEach(p -> priceMap.put(p.getInstrumentModel().getModelid(), p));
			catalogPrices.put(serviceType, priceMap);
		}
		stopWatch.stop();
		logger.info("catalog prices generated after " + stopWatch.getTotalTimeSeconds() + "s");
		stopWatch.start();
		TreeSet<Quotationitem> quotationItems = new TreeSet<Quotationitem>(new QuotationItemComparator());
		// There should be exactly one (default) heading
		QuoteHeading qh = quotation.getQuoteheadings().stream().findFirst().orElse(null);
		// now create an item per instrument
		Integer itemCount = 0;
		for(Instrument instrument: instruments) {
			InstrumentModel model = instrument.getModel();
			ServiceType serviceType = serviceTypeMap.get(instrument);
			Quotationitem qItem = new Quotationitem();
			qItem.setQuotation(quotation);
			qItem.setItemno(++itemCount);
			qItem.setQuantity(1);
			qItem.setPartOfBaseUnit(false);
			qItem.setInst(instrument);
			qItem.setCaltype(serviceType.getCalibrationType());
			qItem.setServiceType(serviceType);
			tpPricingItemService.setThirdPartyDefaultValues(qItem);
			qItem.setGeneralDiscountRate(new BigDecimal(0.00));
			qItem.setInspection(new BigDecimal(0.00));
			CostCalculator.updateItemCostWithRunningTotal(qItem);
			qItem.setNotes(new TreeSet<QuoteItemNote>(new NoteComparator()));
			qItem.setHeading(qh);
			qh.getQuoteitems().add(qItem);
			// generate calibration cost
			if(latestJobCostingCost.get(serviceType).containsKey(model.getModelid())) {
				JobCostingCalibrationCost jcCalCost = latestJobCostingCost.get(serviceType).get(model.getModelid());
				QuotationCalibrationCost calCost = new QuotationCalibrationCost(jcCalCost);
				calCost.setQuoteItem(qItem);
				calCost.setActive(true);
				qItem.setCalibrationCost(calCost);
				Integer costingItemId = jcCalCost.getJobCostingItem().getId();
				qItem.getNotes().add(quoteItemService.createTemplateNewQuotationItemNote(qItem, true, "",
						"Created from cost id " + costingItemId
						+ " link <a href=\"editjobcostingitem.htm?id= "
						+ costingItemId + "\" " + costingItemId + "></a>", false, quotation.getCreatedBy()));
				CostCalculator.updateTPItemCostWithRunningTotal(qItem);
				logger.info("CAL COST: " + calCost + " " + calCost.getFinalCost());
			}
			else {
				QuotationCalibrationCost calCost = new QuotationCalibrationCost();
				calCost.setQuoteItem(qItem);
				calCost.setActive(true);
				BigDecimal price;
				if(catalogPrices.get(serviceType).containsKey(model.getModelid())) {
					price = catalogPrices.get(serviceType).get(model.getModelid()).getFixedPrice();
					qItem.getNotes().add(quoteItemService.createTemplateNewQuotationItemNote(qItem, true, "",
							"Created using model defaults", false, quotation.getCreatedBy()));
				}
				else {
					price = BigDecimal.valueOf(0, 2);
					qItem.getNotes().add(quoteItemService.createTemplateNewQuotationItemNote(qItem, true, "",
							"No prices available", false, quotation.getCreatedBy()));
				}
				calCost.setHouseCost(price);
				calCost.setFinalCost(price);
				calCost.setTotalCost(price);
				calCost.setDiscountRate(BigDecimal.valueOf(0, 2));
				calCost.setDiscountValue(BigDecimal.valueOf(0, 2));
				calCost.setCostType(CostType.CALIBRATION);
				qItem.setCalibrationCost(calCost);
				CostCalculator.updateTPItemCostWithRunningTotal(qItem);
			}
			// set default purchase cost
			QuotationPurchaseCost salesCost = new QuotationPurchaseCost();
			salesCost.setHouseCost(new BigDecimal(0.00));
			salesCost.setDiscountRate(new BigDecimal(0.00));
			salesCost.setDiscountValue(new BigDecimal(0.00));
			salesCost.setTotalCost(new BigDecimal(0.00));
			salesCost.setFinalCost(new BigDecimal(0.00));
			salesCost.setCostType(CostType.PURCHASE);
			salesCost.setActive(false);
			salesCost.setThirdManualPrice(new BigDecimal(0.00));
			salesCost.setThirdMarkupSrc(ThirdCostMarkupSource.SYSTEM_DEFAULT);
			salesCost.setThirdMarkupRate(new BigDecimal(0.00));
			salesCost.setThirdCostSrc(ThirdCostSource.MANUAL);
			salesCost.setThirdMarkupValue(new BigDecimal(0.00));
			salesCost.setThirdCostTotal(new BigDecimal(0.00));
			salesCost.setThirdMarkupSrc(ThirdCostMarkupSource.SYSTEM_DEFAULT);
			salesCost.setThirdCostSrc(ThirdCostSource.MANUAL);
			salesCost.setQuoteItem(qItem);
			qItem.setPurchaseCost(salesCost);
			if (request.getStrategy().equals(QuoteGenerationStrategy.SINGLE_QITEM_PER_INSTRUMENT_WITH_PLANTNOS)) {
				StringBuffer serialPlant = new StringBuffer();
				if (!StringUtils.isEmpty(instrument.getPlantno()))
					serialPlant.append(instrument.getPlantno());
				else if (!StringUtils.isEmpty(instrument.getSerialno())) {
					if (!StringUtils.isEmpty(serialPlant.toString()))
						serialPlant.append(" ");
					serialPlant.append("s/n: ").append(instrument.getSerialno());
				}
				qItem.setPlantno(serialPlant.toString());
			}
			quotationItems.add(qItem);
		}
		stopWatch.stop();
		logger.info("Finished after " + stopWatch.getTotalTimeSeconds() + "s");
		return quotationItems;
	}
}