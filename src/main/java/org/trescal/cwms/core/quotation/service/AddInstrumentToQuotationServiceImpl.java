package org.trescal.cwms.core.quotation.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionDTO;
import org.trescal.cwms.core.jobs.jobitem.projection.JobItemProjectionTools;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.QuotationItemComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteheading.db.QuoteHeadingService;
import org.trescal.cwms.core.quotation.entity.quoteitemnote.QuoteItemNote;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.quotation.form.AddInstrumentToQuoteForm;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.note.db.NoteService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.tools.MathTools;

@Service
public class AddInstrumentToQuotationServiceImpl implements AddInstrumentsToQuotationService {

	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private CatalogPriceService catalogPriceService;
	@Autowired
	private CompanyInstrumentsToQuotationService companyInstsToQuoteServ;
	@Autowired
	private InstrumService instrumentServ;
	@Autowired
	private MessageSource messages;
	@Autowired
	private NoteService noteService;
	@Autowired
	private QuotationService quotationService;
	@Autowired
	private QuoteHeadingService qhServ;
	@Autowired
	private QuotationItemService qiServ;
	@Autowired
	private UserService userService;
	@Autowired
	private SubdivService subServ;
	@Autowired
	private ContactService conService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private JobItemService jobItemServ;
	@Autowired
	private SupportedCurrencyService currencyService;
	@Autowired
	private TaxCalculator taxCalculator;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void addInstruments(Integer allocatedSubdivId, String username, AddInstrumentToQuoteForm form,
			Locale locale) {
		Quotation quotation = this.quotationService.get(form.getQuotationId());
		Subdiv subDiv = this.subServ.get(allocatedSubdivId);
		Contact person = this.userService.get(username).getCon();
		// get all calibration types currently active
		List<CalibrationType> caltypes = this.calTypeServ.getActiveCalTypes();
		// create instrument list from the plantids
		List<Instrument> quoteCompInsts = this.instrumentServ.getInstruments(form.getBasketIds());
		// convert instruments into new quotation items
		Set<Quotationitem> newItems = this.companyInstsToQuoteServ.getNewQuotationItemsForInstruments(quoteCompInsts,
				caltypes, quotation, subDiv, form.getBasketIds());
		Set<QuoteNote> newQuoteNotes = new HashSet<>();
		Set<String> newQuoteNotesText = new HashSet<>();
		// set heading on all quotation items
		for (Quotationitem qi : newItems) {
			// get quote item plant id
			Integer qiplantid = qi.getInst().getPlantid();
			int count = 0;
			// find matching plantids
			for (Integer plid : form.getBasketIds()) {
				// matching plant id (some may be null if deleted from basket)
				if ((plid != null) && qiplantid.intValue() == plid.intValue()) {
					// get correct heading id
					QuoteHeading qh = this.qhServ.findQuoteHeading(form.getBasketHeadings().get(count));
					// set quote heading on quote item (item not added to
					// heading - persist items all at end via quotation)
					qi.setHeading(qh);
					// qh.getQuoteitems().add(qi);

					// set cal type
					CalibrationType calibrationType = this.calTypeServ.find(form.getBasketCaltypes().get(count));
					qi.setCaltype(calibrationType);
					if (calibrationType != null)
						qi.setServiceType(calibrationType.getServiceType());

					// Add catalog price comment as a note
					CatalogPrice catalogPrice = catalogPriceService.findSingle(qi.getInst().getModel(),
							qi.getServiceType(), null, subDiv.getComp());
					if (catalogPrice != null) {
						String comments = catalogPrice.getComments();
						if (comments != null && !comments.isEmpty()) {
							String label = messages.getMessage("quoteautonote.catalogpricenote", null, locale);
							QuoteItemNote note = new QuoteItemNote();
							note.setActive(true);
							note.setLabel(label);
							note.setNote(comments);
							note.setPublish(true);
							note.setQuotationitem(qi);
							note.setSetBy(person);
							note.setSetOn(new Date());
							qi.getNotes().add(note);
						}
					}

					// set Price
					BigDecimal finalPrice = new BigDecimal(form.getBasketCosts().get(count));
					qi.setTotalCost(finalPrice);
					qi.setFinalCost(finalPrice);
					// set calibration cost
					if(qi.getCalibrationCost() != null){
						qi.getCalibrationCost().setFinalCost(finalPrice);
						qi.getCalibrationCost().setTotalCost(finalPrice);
						qi.getCalibrationCost().setHouseCost(finalPrice);
						qi.getCalibrationCost().setCostSrc(CostSource.valueOf(form.getBasketSources().get(count)));
					}
					// Add default Procedure description as a note if it isn't
					// already added against this quote.
					String comments = this.qiServ.getServiceCapabilityComment(qi.getInst().getModel(), qi.getCaltype(),
							subDiv, locale);
					if (comments != null) {
						if (!newQuoteNotesText.contains(comments)
								&& !noteService.isNoteExists(NoteType.QUOTENOTE, quotation.getId(), comments)) {
							String label = messages.getMessage("quoteautonote.procedurenote", null, locale);
							QuoteNote note = new QuoteNote();
							note.setActive(true);
							note.setLabel(label);
							note.setNote(comments);
							note.setPublish(true);
							note.setQuotation(quotation);
							note.setSetBy(person);
							note.setSetOn(new Date());
							newQuoteNotes.add(note);
							newQuoteNotesText.add(comments);
						}
					}
				}
				// increment count
				count++;
			}
		}
		// add all new items to quotation items
		quotation.getQuotationitems().addAll(newItems);
		quotation.getNotes().addAll(newQuoteNotes);
		// update the costs
		CostCalculator.updatePricingTotalCost(quotation, quotation.getQuotationitems());
		taxCalculator.calculateAndSetVATaxAndFinalCost(quotation);
		/*
		 * Comments 2019-08-07: we had previously been doing a merge when this
		 * code was in controller, but not needed any longer. this could be
		 * potentially avoided by doing all lookups first, and then
		 * creates/updates There were sometimes transient value issues, which
		 * were isolated to being due to multiple browser form submits (multiple
		 * submit buttons + jQuery -> multiple form submits!)
		 */
		// save implicitly needed before sort; as sort currently rebuilds
		// quotation item set.
		this.quotationService.save(quotation);
		// update the numbering of the items on the quotation
		this.qiServ.sortQuotationItems(quotation);
	}

	@Override
	public Set<Quotationitem> importQuotationItemsUsingInst__PricesFromFile(Integer allocatedSubdivId, Integer contactId, List<Integer> plantIds, Map<Integer, Integer> serviceTypeIds,
			Map<Integer, BigDecimal> finalPrices, Map<Integer, BigDecimal> discountPrices, Map<Integer, BigDecimal> catalogPrices,
			Map<Integer, String> publicNotes, Map<Integer, String> privateNotes, Integer quoteId, Locale locale){

		Quotation quotation = this.quotationService.get(quoteId);
		Subdiv subDiv = this.subServ.get(allocatedSubdivId);
		Contact person = this.conService.get(contactId);
		// create instrument list from the plantids
		List<Instrument> quoteCompInsts = this.instrumentServ.getInstruments(plantIds);
		// convert instruments into new quotation items
		Set<Quotationitem> newItems = new HashSet<>();
		quoteCompInsts.stream().forEach(inst->newItems.add(qiServ.createTemplateNewQuotationItem(quotation, quotation.getQuoteheadings().iterator().next(), inst, 
				inst.getModel().getModelid(), this.calTypeServ.getDefaultCalType().getCalTypeId(), allocatedSubdivId)));
		// initialize item count
		int itemcount = this.qiServ.getMaxItemno(quotation.getId()) + 1;
		Set<QuoteNote> newQuoteNotes = new HashSet<>();
		Set<String> newQuoteNotesText = new HashSet<>();
		for (Quotationitem qi : newItems) {
			
			ServiceType servicetype = this.serviceTypeService.get(serviceTypeIds.get(qi.getInst().getPlantid()));
			if(servicetype != null){
				// set Service type
				qi.setServiceType(servicetype);
				// set Calibration type
				qi.setCaltype(servicetype.getCalibrationType());
			}
			
			// set item no
			qi.setItemno(itemcount);
			itemcount++;
			
			// set Final price
			if(finalPrices.get(qi.getInst().getPlantid()) != null) {
				qi.setFinalCost(finalPrices.get(qi.getInst().getPlantid()));
				qi.getCalibrationCost().setFinalCost(finalPrices.get(qi.getInst().getPlantid()));
				// we will overwrite the total cost, total cost of
				// calibration and house cost of calibration if we have
				// catalog price(non mandatory field)
				qi.getCalibrationCost().setTotalCost(finalPrices.get(qi.getInst().getPlantid()));
				qi.getCalibrationCost().setHouseCost(finalPrices.get(qi.getInst().getPlantid()));
				qi.setTotalCost(finalPrices.get(qi.getInst().getPlantid()));
			}
			
			// set Discount rate
			if( discountPrices.get(qi.getInst().getPlantid()) != null){
				qi.getCalibrationCost().setDiscountRate(discountPrices.get(qi.getInst().getPlantid()));
			}
			
			// set Catalog price
			if(catalogPrices.get(qi.getInst().getPlantid()) != null){
				qi.setTotalCost(catalogPrices.get(qi.getInst().getPlantid()));
				qi.getCalibrationCost().setHouseCost(catalogPrices.get(qi.getInst().getPlantid()));
				qi.getCalibrationCost().setTotalCost(catalogPrices.get(qi.getInst().getPlantid()));
			}
			
			// calculate discount value using discountRate and totalCost
			if(qi.getTotalCost() != null && qi.getCalibrationCost().getDiscountRate().doubleValue()>0){
				BigDecimal up = qi.getCalibrationCost().getDiscountRate().divide(new BigDecimal("100"), MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
				BigDecimal discountValue = up.multiply(qi.getTotalCost()).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN);
				qi.getCalibrationCost().setDiscountValue(discountValue);
			}		
			
			// set Public note
			if(!StringUtils.isEmpty(publicNotes.get(qi.getInst().getPlantid()))){
				QuoteItemNote notePublic = new QuoteItemNote();
				String publicLabel = messages.getMessage("exchangeformat.fieldname.publicnote", null, locale);
				notePublic.setQuotationitem(qi);
				notePublic.setSetBy(person);
				notePublic.setSetOn(new Date());
				notePublic.setActive(true);
				notePublic.setPublish(true);
				notePublic.setNote(publicNotes.get(qi.getInst().getPlantid()));
				notePublic.setLabel(publicLabel);
				qi.getNotes().add(notePublic);
			}
	
			// set Private note
			if(!StringUtils.isEmpty(privateNotes.get(qi.getInst().getPlantid()))){
				QuoteItemNote notePrivate = new QuoteItemNote();
				String privateLabel = messages.getMessage("exchangeformat.fieldname.privatenote", null, locale);
				notePrivate.setQuotationitem(qi);
				notePrivate.setSetBy(person);
				notePrivate.setSetOn(new Date());
				notePrivate.setActive(true);
				notePrivate.setPublish(false);
				notePrivate.setNote(privateNotes.get(qi.getInst().getPlantid()));
				notePrivate.setLabel(privateLabel);
				qi.getNotes().add(notePrivate);
			}
			
			// Add catalog price comment as a note
			CatalogPrice catalogPrice = catalogPriceService.findSingle(qi.getInst().getModel(),
			qi.getServiceType(), null, subDiv.getComp());
			if (catalogPrice != null) {
				String comments = catalogPrice.getComments();
				if (comments != null && !comments.isEmpty()) {
					String label = messages.getMessage("quoteautonote.catalogpricenote", null, locale);
					QuoteItemNote note = new QuoteItemNote();
					note.setActive(true);
					note.setLabel(label);
					note.setNote(comments);
					note.setPublish(true);
					note.setQuotationitem(qi);
					note.setSetBy(person);
					note.setSetOn(new Date());
					qi.getNotes().add(note);
				}
			}
			
			// Add default Procedure description as a note if it isn't
			// already added against this quote.
			String comments = this.qiServ.getServiceCapabilityComment(qi.getInst().getModel(), qi.getCaltype(),
					subDiv, locale);
			if (comments != null) {
				if (!newQuoteNotesText.contains(comments)
					&& !noteService.isNoteExists(NoteType.QUOTENOTE, quotation.getId(), comments)) {
						String label = messages.getMessage("quoteautonote.procedurenote", null, locale);
						QuoteNote note = new QuoteNote();
						note.setActive(true);
						note.setLabel(label);
						note.setNote(comments);
						note.setPublish(true);
						note.setQuotation(quotation);
						note.setSetBy(person);
						note.setSetOn(new Date());
						newQuoteNotes.add(note);
						newQuoteNotesText.add(comments);
				}
			}
		}
		
		// add all new items to quotation items
		this.logger.info("Added " + newItems.size() + " quotation items from instrument");
		quotation.getNotes().addAll(newQuoteNotes);
		return newItems;
	}
	
	@Override
	public Set<Quotationitem> importQuotationItemsUsingInst_PricesFromLatestJobItem(Integer allocatedSubdivId, Integer contactId,
			List<Integer> plantIds, Map<Integer, BigDecimal> discountPrices, Map<Integer, BigDecimal> catalogPrices,
			Map<Integer, String> publicNotes, Map<Integer, String> privateNotes, Integer quoteId, Locale locale) {
		
		Quotation quotation = this.quotationService.get(quoteId);
		Subdiv subDiv = this.subServ.get(allocatedSubdivId);
		Contact person = this.conService.get(contactId);
		
		// New quotation, so should just have one default heading.
		QuoteHeading heading = quotation.getQuoteheadings().iterator().next();
		TreeSet<Quotationitem> qItems = new TreeSet<Quotationitem>(new QuotationItemComparator());
		// create instrument list from the plantids
		List<Instrument> quoteCompInsts = this.instrumentServ.getInstruments(plantIds);
		Map<Integer, JobItemProjectionDTO> mostRecentJobItems = jobItemServ.findMostRecentJobItemDTO(plantIds);

		Set<QuoteNote> newQuoteNotes = new HashSet<>();
		Set<String> newQuoteNotesText = new HashSet<>();
		// initialize item count
		int itemcount = this.qiServ.getMaxItemno(quotation.getId()) + 1;
		
		for (Instrument inst : quoteCompInsts) {
			CalibrationType caltype = null;
			if (mostRecentJobItems.get(inst.getPlantid()) != null && mostRecentJobItems.get(inst.getPlantid()).getServiceTypeId() != null) {
				caltype = serviceTypeService.get(mostRecentJobItems.get(inst.getPlantid()).getServiceTypeId()).getCalibrationType();
			} else
				caltype = this.calTypeServ.getDefaultCalType();
	
			Quotationitem qItem = this.qiServ.createTemplateNewQuotationItem(quotation, heading, inst,
					null, caltype.getCalTypeId(), subDiv.getSubdivid());
			
			// set item no
			qItem.setItemno(itemcount);
			itemcount++;
			
			if(mostRecentJobItems.get(inst.getPlantid()) != null){
				JobItemProjectionDTO jidto = mostRecentJobItems.get(inst.getPlantid());
				// set currency 
				jidto.setCurrency(currencyService.getKeyValue(jidto.getCurrencyId()));
				BigDecimal finalPrice = JobItemProjectionTools.getPriceAndCurrency(mostRecentJobItems.get(inst.getPlantid())).getPrice();
				qItem.setFinalCost(finalPrice);
				qItem.getCalibrationCost().setFinalCost(finalPrice);
				// we will overwrite the total cost, total cost of
				// calibration and house cost of calibration if we have
				// catalog price(non mandatory field)
				qItem.getCalibrationCost().setTotalCost(finalPrice);
				qItem.getCalibrationCost().setHouseCost(finalPrice);
				qItem.setTotalCost(finalPrice);	
			}
			
			// set Discount rate
			if( discountPrices.get(qItem.getInst().getPlantid()) != null){
				qItem.getCalibrationCost().setDiscountRate(discountPrices.get(qItem.getInst().getPlantid()));
			}
						
			// set Catalog price
			if(catalogPrices.get(qItem.getInst().getPlantid()) != null){
				qItem.setTotalCost(catalogPrices.get(qItem.getInst().getPlantid()));
				qItem.getCalibrationCost().setHouseCost(catalogPrices.get(qItem.getInst().getPlantid()));
				qItem.getCalibrationCost().setTotalCost(catalogPrices.get(qItem.getInst().getPlantid()));
			}
						
			// calculate discount value using discountRate and totalCost
			if(qItem.getTotalCost() != null && qItem.getCalibrationCost().getDiscountRate().doubleValue()>0){
				BigDecimal up = qItem.getCalibrationCost().getDiscountRate().divide(new BigDecimal("100"), MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
				BigDecimal discountValue = up.multiply(qItem.getTotalCost()).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN);
				qItem.getCalibrationCost().setDiscountValue(discountValue);
			}		
						
			// set Public note
			if(!StringUtils.isEmpty(publicNotes.get(qItem.getInst().getPlantid()))){
				QuoteItemNote notePublic = new QuoteItemNote();
				String publicLabel = messages.getMessage("exchangeformat.fieldname.publicnote", null, locale);
				notePublic.setQuotationitem(qItem);
				notePublic.setSetBy(person);
				notePublic.setSetOn(new Date());
				notePublic.setActive(true);
				notePublic.setPublish(true);
				notePublic.setNote(publicNotes.get(qItem.getInst().getPlantid()));
				notePublic.setLabel(publicLabel);
				qItem.getNotes().add(notePublic);
			}
				
			// set Private note
			if(!StringUtils.isEmpty(privateNotes.get(qItem.getInst().getPlantid()))){
				QuoteItemNote notePrivate = new QuoteItemNote();
				String privateLabel = messages.getMessage("exchangeformat.fieldname.privatenote", null, locale);
				notePrivate.setQuotationitem(qItem);
				notePrivate.setSetBy(person);
				notePrivate.setSetOn(new Date());
				notePrivate.setActive(true);
				notePrivate.setPublish(false);
				notePrivate.setNote(privateNotes.get(qItem.getInst().getPlantid()));
				notePrivate.setLabel(privateLabel);
				qItem.getNotes().add(notePrivate);
			}
						
			// Add catalog price comment as a note
			CatalogPrice catalogPrice = catalogPriceService.findSingle(qItem.getInst().getModel(),
					qItem.getServiceType(), null, subDiv.getComp());
			if (catalogPrice != null) {
				String comments = catalogPrice.getComments();
				if (comments != null && !comments.isEmpty()) {
					String label = messages.getMessage("quoteautonote.catalogpricenote", null, locale);
					QuoteItemNote note = new QuoteItemNote();
					note.setActive(true);
					note.setLabel(label);
					note.setNote(comments);
					note.setPublish(true);
					note.setQuotationitem(qItem);
					note.setSetBy(person);
					note.setSetOn(new Date());
					qItem.getNotes().add(note);
				}
			}
						
			// Add default Procedure description as a note if it isn't
			// already added against this quote.
			String comments = this.qiServ.getServiceCapabilityComment(qItem.getInst().getModel(), qItem.getCaltype(),
								subDiv, locale);
				if (comments != null) {
					if (!newQuoteNotesText.contains(comments)
					&& !noteService.isNoteExists(NoteType.QUOTENOTE, quotation.getId(), comments)) {
						String label = messages.getMessage("quoteautonote.procedurenote", null, locale);
						QuoteNote note = new QuoteNote();
						note.setActive(true);
						note.setLabel(label);
						note.setNote(comments);
						note.setPublish(true);
						note.setQuotation(quotation);
						note.setSetBy(person);
						note.setSetOn(new Date());
						newQuoteNotes.add(note);
						newQuoteNotesText.add(comments);
					}
				}
				
			qItems.add(qItem);
		}
		
		this.logger.info("Added " + qItems.size() + " quotation items from instrument / the price and service type"
				+ "from the last job item");
		
		quotation.getNotes().addAll(newQuoteNotes);
		
		return qItems;
	}
	
	@Override
	public Set<Quotationitem> importQuotationItemsUsingInst_PricesFromFile_BatchMode(Integer allocatedSubdivId, Integer contactId, List<Integer> instIndex,
			List<Integer> plantIds, Map<Integer, Integer> instWithIndex,  Map<Integer, Integer> serviceTypeIds,
			Map<Integer, BigDecimal> finalPrices, Map<Integer, BigDecimal> discountPrices, Map<Integer, BigDecimal> catalogPrices,
			Map<Integer, String> publicNotes, Map<Integer, String> privateNotes, Integer quoteId, Locale locale){

		Quotation quotation = this.quotationService.get(quoteId);
		Subdiv subDiv = this.subServ.get(allocatedSubdivId);
		Contact person = this.conService.get(contactId);
		// convert instruments into new quotation items
		Set<Quotationitem> newItems = new HashSet<>();
		// initialize item count
		int itemcount = this.qiServ.getMaxItemno(quotation.getId()) + 1;
		Integer calTypeId = this.calTypeServ.getDefaultCalType().getCalTypeId();
		// create instrument list from the plantids
		Map<Integer, Instrument> quoteCompInsts = this.instrumentServ.getInstruments(plantIds).stream()
				.collect(Collectors.toMap(inst -> inst.getPlantid(), inst -> inst));
		Set<QuoteNote> newQuoteNotes = new HashSet<>();
		Set<String> newQuoteNotesText = new HashSet<>();
		for (Integer index : instIndex) {	
			Instrument inst = quoteCompInsts.get(instWithIndex.get(index));
			Quotationitem qi = qiServ.createTemplateNewQuotationItem(quotation, quotation.getQuoteheadings().iterator().next(), inst, inst.getModel().getModelid(), calTypeId, allocatedSubdivId);
			ServiceType servicetype = this.serviceTypeService.get(serviceTypeIds.get(index));
			if(servicetype != null){
				// set Service type
				qi.setServiceType(servicetype);
				// set Calibration type
				qi.setCaltype(servicetype.getCalibrationType());
			}
			
			// set item no
			qi.setItemno(itemcount);
			itemcount++;
			
			// set Final price
			if(finalPrices.get(index) != null) {
				qi.setFinalCost(finalPrices.get(index));
				qi.getCalibrationCost().setFinalCost(finalPrices.get(index));
				// we will overwrite the total cost, total cost of
				// calibration and house cost of calibration if we have
				// catalog price(non mandatory field)
				qi.getCalibrationCost().setTotalCost(finalPrices.get(index));
				qi.getCalibrationCost().setHouseCost(finalPrices.get(index));
				qi.setTotalCost(finalPrices.get(index));
			}
			
			// set Discount rate
			if( discountPrices.get(index) != null){
				qi.getCalibrationCost().setDiscountRate(discountPrices.get(index));
			}
			
			// set Catalog price
			if(catalogPrices.get(index) != null){
				qi.setTotalCost(catalogPrices.get(index));
				qi.getCalibrationCost().setHouseCost(catalogPrices.get(index));
				qi.getCalibrationCost().setTotalCost(catalogPrices.get(index));
			}
			
			// calculate discount value using discountRate and totalCost
			if(qi.getTotalCost() != null && qi.getCalibrationCost().getDiscountRate().doubleValue()>0){
				BigDecimal up = qi.getCalibrationCost().getDiscountRate().divide(new BigDecimal("100"), MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
				BigDecimal discountValue = up.multiply(qi.getTotalCost()).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN);
				qi.getCalibrationCost().setDiscountValue(discountValue);
			}		
			
			// set Public note
			if(!StringUtils.isEmpty(publicNotes.get(index))){
				QuoteItemNote notePublic = new QuoteItemNote();
				String publicLabel = messages.getMessage("exchangeformat.fieldname.publicnote", null, locale);
				notePublic.setQuotationitem(qi);
				notePublic.setSetBy(person);
				notePublic.setSetOn(new Date());
				notePublic.setActive(true);
				notePublic.setPublish(true);
				notePublic.setNote(publicNotes.get(index));
				notePublic.setLabel(publicLabel);
				qi.getNotes().add(notePublic);
			}
	
			// set Private note
			if(!StringUtils.isEmpty(privateNotes.get(index))){
				QuoteItemNote notePrivate = new QuoteItemNote();
				String privateLabel = messages.getMessage("exchangeformat.fieldname.privatenote", null, locale);
				notePrivate.setQuotationitem(qi);
				notePrivate.setSetBy(person);
				notePrivate.setSetOn(new Date());
				notePrivate.setActive(true);
				notePrivate.setPublish(false);
				notePrivate.setNote(privateNotes.get(index));
				notePrivate.setLabel(privateLabel);
				qi.getNotes().add(notePrivate);
			}
			
			// Add catalog price comment as a note
			CatalogPrice catalogPrice = catalogPriceService.findSingle(qi.getInst().getModel(),
			qi.getServiceType(), null, subDiv.getComp());
			if (catalogPrice != null) {
				String comments = catalogPrice.getComments();
				if (comments != null && !comments.isEmpty()) {
					String label = messages.getMessage("quoteautonote.catalogpricenote", null, locale);
					QuoteItemNote note = new QuoteItemNote();
					note.setActive(true);
					note.setLabel(label);
					note.setNote(comments);
					note.setPublish(true);
					note.setQuotationitem(qi);
					note.setSetBy(person);
					note.setSetOn(new Date());
					qi.getNotes().add(note);
				}
			}
			
			// Add default Procedure description as a note if it isn't
			// already added against this quote.
			String comments = this.qiServ.getServiceCapabilityComment(qi.getInst().getModel(), qi.getCaltype(),
					subDiv, locale);
			if (comments != null) {
				if (!newQuoteNotesText.contains(comments)
					&& !noteService.isNoteExists(NoteType.QUOTENOTE, quotation.getId(), comments)) {
						String label = messages.getMessage("quoteautonote.procedurenote", null, locale);
						QuoteNote note = new QuoteNote();
						note.setActive(true);
						note.setLabel(label);
						note.setNote(comments);
						note.setPublish(true);
						note.setQuotation(quotation);
						note.setSetBy(person);
						note.setSetOn(new Date());
						newQuoteNotes.add(note);
						newQuoteNotesText.add(comments);
				}
			}
		newItems.add(qi);
		}
		
		// add all new items to quotation items
		this.logger.info("Added " + newItems.size() + " quotation items from instrument");
		quotation.getNotes().addAll(newQuoteNotes);
		return newItems;
	}
}
