package org.trescal.cwms.core.quotation.entity.quotationitem.comparators;

import java.util.Comparator;

import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;

public class QuotationItemReferenceNoComparator implements Comparator<Quotationitem>
{
	@Override
	public int compare(Quotationitem o1, Quotationitem o2)
	{
		// get plant nos from quote items
		String qi1refno = o1.getPlantno();
		String qi2refno = o2.getPlantno();
		// first qi has instrument?
		if (o1.getInst() != null)
		{
			// check plant no value
			if ((o1.getInst().getPlantno() != null)
					&& !o1.getInst().getPlantno().equalsIgnoreCase(""))
			{
				qi1refno = o1.getInst().getPlantno();
			}
			else
			{
				qi1refno = o1.getInst().getSerialno();
			}
		}
		// second qi has instrument?
		if (o2.getInst() != null)
		{
			// check plant no value
			if ((o2.getInst().getPlantno() != null)
					&& !o2.getInst().getPlantno().equalsIgnoreCase(""))
			{
				qi2refno = o2.getInst().getPlantno();
			}
			else
			{
				qi2refno = o2.getInst().getSerialno();
			}
		}
		// compare reference numbers
		if ((qi1refno == null) && (qi2refno != null))
		{
			return -1;
		}
		else if ((qi1refno != null) && (qi2refno == null))
		{
			return 1;
		}
		else if (((qi1refno == null) && (qi2refno == null))
				|| qi1refno.trim().equals(qi2refno.trim()))
		{
			// if both null or both identical then compare the item no - makes
			// sure that identical plantnos don't get swallowed up in TreeSets
			return ((Integer) o1.getId()).compareTo(o2.getId());
		}
		else
		{
			return qi1refno.compareTo(qi2refno);
		}
	}
}
