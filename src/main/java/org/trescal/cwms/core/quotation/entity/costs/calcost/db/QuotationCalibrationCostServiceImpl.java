package org.trescal.cwms.core.quotation.entity.costs.calcost.db;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.thirdparty.ThirdPartyCostSupportService;
import org.trescal.cwms.core.quotation.dto.DiscountSummaryDTO;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;

@Service("QuotationCalibrationCostService")
public class QuotationCalibrationCostServiceImpl extends BaseServiceImpl<QuotationCalibrationCost, Integer> implements QuotationCalibrationCostService
{
	@Autowired
	private QuotationCalibrationCostDao baseDao;
	// injected properties
	@Value("#{props['cwms.config.quotation.resultsyearfilter']}")
	private Integer resultsYearFilter;
	@Value("#{props['cwms.config.costs.costings.roundupnearest50']}")
	private boolean roundUpFinalCosts;
	@Autowired
	private ThirdPartyCostSupportService thirdCostService;

	@Override
	public QuotationCalibrationCost findEagerQuotationCalibrationCost(int id)
	{
		return this.baseDao.findEagerQuotationCalibrationCost(id);
	}

	@Override
	public List<QuotationCalibrationCost> findMatchingCostOnLinkedQuotation(int jobid, int modelid, int calTypeId)
	{
		return this.baseDao.findMatchingCostOnLinkedQuotation(jobid, modelid, calTypeId);
	}

	@Override
	public List<QuotationCalibrationCost> findMatchingCosts(Integer coid, int modelid, Integer calTypeId, Integer page, Integer resPerPage)
	{
		return this.baseDao.findMatchingCosts(coid, modelid, calTypeId, this.resultsYearFilter, page, resPerPage);
	}

	@Override
	public QuotationCalibrationCost updateCosts(QuotationCalibrationCost cost)
	{
		if (cost.getHouseCost() == null)
		{
			cost.setHouseCost(new BigDecimal("0.00"));
		}

		// calculate TP costs
		cost = (QuotationCalibrationCost) this.thirdCostService.recalculateThirdPartyCosts(cost);

		cost.setTotalCost(cost.getHouseCost().add(cost.getThirdCostTotal()));

		// delegate to CostCalculator to apply any discounts and set the final
		// cost
		CostCalculator.updateSingleCost(cost, this.roundUpFinalCosts);
		return cost;
	}

	@Override
	public DiscountSummaryDTO getDiscountDTO(int quoteid) {
		return this.baseDao.getDiscountDTO(quoteid);
	}

	@Override
	protected BaseDao<QuotationCalibrationCost, Integer> getBaseDao() {
		return this.baseDao;
	}
}