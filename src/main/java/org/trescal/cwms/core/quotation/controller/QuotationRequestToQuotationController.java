package org.trescal.cwms.core.quotation.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.businessarea.db.BusinessAreaService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.country.db.CountryService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.deliverynote.entity.transportoption.db.TransportOptionService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequestStatus;
import org.trescal.cwms.core.quotation.entity.quotationrequest.db.QuotationRequestService;
import org.trescal.cwms.core.quotation.form.QuotationRequestToQuotationForm;
import org.trescal.cwms.core.quotation.form.QuotationRequestToQuotationValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV})
@Slf4j
public class QuotationRequestToQuotationController {
	@Autowired
	private QuotationRequestService quotationRequestService;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private QuotationRequestToQuotationValidator validator;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;
	@Autowired
	private CountryService countryService;
	@Autowired
	private BusinessAreaService businessAreaService;
	@Autowired
	private TranslationService translationService;
	@Autowired
    private TransportOptionService transportOptionService;
	@Autowired
    private SupportedCurrencyService supportedCurrencyService;
	@Autowired
	private MessageSource messages;

	private List<KeyValueIntegerString> getBusinessAreas(Locale locale) {
		List<KeyValueIntegerString> result = new ArrayList<>();
		result.add(new KeyValueIntegerString(0, messages.getMessage("company.none", null, "None", locale)));
		this.businessAreaService.getAll().stream()
			.map(ba -> new KeyValueIntegerString(ba.getBusinessareaid(),
				translationService.getCorrectTranslation(ba.getNametranslation(), LocaleContextHolder.getLocale())))
			.forEach(result::add);
		return result;
	}

	private List<KeyValue<String,String>> getRelevantCompanyRoles() {
		return Arrays.asList(new KeyValue<>(CompanyRole.PROSPECT.name(), CompanyRole.PROSPECT.getMessage()),
                new KeyValue<>(CompanyRole.CLIENT.name(), CompanyRole.CLIENT.getMessage()));
	}

	private List<KeyValue<Integer,String>> getCurrencies() {
		return supportedCurrencyService.getAllSupportedCurrencys().stream()
                .map(c -> new KeyValue<>(c.getCurrencyId(), c.getCurrencyName()))
                .collect(Collectors.toList());
	}

	private List<KeyValue<Integer,String>> getTransportOptions(
            @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
       return transportOptionService.getTransportOptionsFromSubdiv(subdivDto.getKey()).stream()
               .map(to -> new KeyValue<>(to.getId(),
                       translationService.getCorrectTranslation(to.getMethod().getMethodTranslation(), LocaleContextHolder.getLocale()) +
                               (to.getSub() != null ? " - " + to.getSub().getSubname() : "") +
                               (to.getDayText() != null ? " (" + to.getDayText() + ")" : "")))
               .collect(Collectors.toList());
    }

	@InitBinder("command")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("command")
	protected QuotationRequestToQuotationForm formBackingObject(@RequestParam(name = "id") Integer qrid) {
		QuotationRequestToQuotationForm form = new QuotationRequestToQuotationForm();

		QuotationRequest quotationRequest = this.quotationRequestService.get(qrid);
		if ((quotationRequest == null) || (qrid == 0)) {
			throw new RuntimeException("Unable to find quotation request");
		} else if (!quotationRequest.getStatus().equals(QuotationRequestStatus.REQUESTED)) {
			log.debug(quotationRequest.getStatus().toString());
			throw new RuntimeException("Cannot convert quotation request at this status - "
				+ quotationRequest.getStatus().toString());
		} else {
			if (quotationRequest.getComp() != null) {
				form.setCoid(quotationRequest.getComp().getCoid());
			}
			if (quotationRequest.getCon() != null)
			{
				form.setPersonid(quotationRequest.getCon().getPersonid());
				form.setSubdivid(quotationRequest.getCon().getSub().getSubdivid());
			}

			form.setQuotationRequest(quotationRequest);
		}
		return form;
	}

	@RequestMapping(value="/quotationrequesttoquotation.htm", method=RequestMethod.GET)
	public String referenceData(Locale locale, Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) {
		Contact currentContact = userService.get(username).getCon();
		
		model.addAttribute("defaultBusinessAreaId", 0);
		model.addAttribute("userCountryId", currentContact.getSub().getComp().getCountry().getCountryid());
		model.addAttribute("userCurrencyId", currentContact.getSub().getComp().getCurrency().getCurrencyId());
		model.addAttribute("transportOptions", getTransportOptions(subdivDto));
		model.addAttribute("currencies", getCurrencies());
		model.addAttribute("countries", this.countryService.getCountries());
		model.addAttribute("coroles", getRelevantCompanyRoles());
		model.addAttribute("businessAreas", getBusinessAreas(locale));
		
		return "/trescal/core/quotation/quotationrequesttoquotation";
	}
	
	@RequestMapping(value="/quotationrequesttoquotation.htm", method=RequestMethod.POST)
	protected String onSubmit(Locale locale, Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute("command") @Validated QuotationRequestToQuotationForm form, 
			BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) {
			return referenceData(locale, model, username, subdivDto);
		}
		Contact currentContact = this.userService.get(username).getCon();
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		QuotationRequest quotationRequest = form.getQuotationRequest();
		// insert the quotation
		Quotation quotation = this.quoteServ.insertQuotationFromRequest(quotationRequest, form.getPersonid(), form.isAddRequestInfoAsNote(), currentContact, allocatedSubdiv);
		// update the quotation request
		quotationRequest.setStatus(QuotationRequestStatus.LOGGED);
		quotationRequest.setQuotation(quotation);
		this.quotationRequestService.merge(quotationRequest);
		model.asMap().clear();
		return "redirect:viewquotation.htm?id=" + quotation.getId();
	}
}