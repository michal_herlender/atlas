package org.trescal.cwms.core.quotation.form;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;

import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
public class EditQuotationForm {
    // view fields
    private Quotation quotation;

    // springbound update fields
    private Integer personid;
    private String clientref;
    @Min(value = 1, message = "{error.value.notselected}")
    private Integer statusid;
    private String reqDate;
    @Min(value = 1, message = "{error.value.notselected}")
    private Integer sourcedByPersonid;
    private String currencyCode;
    private Integer sourceAddressid;
    private LocalDate clientAcceptanceOn;

    private BigDecimal globalDiscount = new BigDecimal("0.00");
    private boolean applyDiscount = false;

}