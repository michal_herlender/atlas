package org.trescal.cwms.core.quotation.entity.quotationrequest.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.quotation.dto.QuotationRequestProjectionDTO;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequestStatus;

public interface QuotationRequestDao extends AllocatedDao<Company, QuotationRequest, Integer>
{
	List<QuotationRequestProjectionDTO> getQuotationRequestByStatusNew(QuotationRequestStatus status, Integer allocatedSubdivId);

}