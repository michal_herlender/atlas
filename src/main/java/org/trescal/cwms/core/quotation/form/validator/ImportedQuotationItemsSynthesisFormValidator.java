package org.trescal.cwms.core.quotation.form.validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.instrument.entity.instrument.PossibleInstrumentDTO;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.form.ImportedQuotationItemsSynthesisForm;
import org.trescal.cwms.core.quotation.form.ImportedQuotationItemsSynthesisForm.ImportedQuotationItemsSynthesisRowDTO;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class ImportedQuotationItemsSynthesisFormValidator extends AbstractBeanValidator {
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ImportedQuotationItemsSynthesisRowDTOValidator rowValidator;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private QuotationService quotationService;
	@Autowired
	private MessageSource messageSource;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(ImportedQuotationItemsSynthesisForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		super.validate(target, errors);

		ImportedQuotationItemsSynthesisForm form = (ImportedQuotationItemsSynthesisForm) target;

		Subdiv subdiv = subdivService.get(form.getSubdivId());

		Locale locale = LocaleContextHolder.getLocale();

		List<String> plantnos = new ArrayList<>();
		List<String> serialnos = new ArrayList<>();
		List<Integer> plantids = new ArrayList<>();
		// add to plantnos list all plant no from file
	   if(form.getRows() == null) {
		   form.setRows(new ArrayList<>());
		   if(form.getIdentifiedItems()!=null) {
			   form.getRows().addAll(form.getIdentifiedItems());
		   }
		   if(form.getNonIdentifiedItems()!=null) {
			   form.getRows().addAll(form.getNonIdentifiedItems());
		   }   
	   }
		form.getRows().stream().forEach(row -> plantnos.add(row.getPlantNo()));
		// get all possible instrument from db
		List<PossibleInstrumentDTO> possibleInstruments = instrumentService.lookupPossibleInstruments(
				form.getCoid().intValue(), subdiv.getSubdivid(), 0, locale, plantids, plantnos, serialnos);
		// add to plantids list possible plant id
		possibleInstruments.stream().forEach(p -> plantids.add(p.getPlantid()));

		// validate rows
		for (int i = 0; i < form.getRows().size(); i++) {
			ImportedQuotationItemsSynthesisRowDTO row = form.getRows().get(i);

			try {
				errors.pushNestedPath("rows[" + i + "]");
				ValidationUtils.invokeValidator(this.rowValidator, row, errors,
						quotationService.get(form.getQuotationId()), possibleInstruments);
				// add to list the plant no already entered in the row in
				// order to detect the duplicate fields(plant no) in the file
				List<String> plantnosRow = new ArrayList<>();
				if (i > 0)
					for (int index = 0; index <= i - 1; index++) {
						plantnosRow.add(form.getRows().get(index).getPlantNo());
					}

				for (int j = 0; j < plantnosRow.size(); j++) {
					if (plantnosRow.contains(row.getPlantNo())) {
						row.getErrorMessages().add(messageSource.getMessage("exchangeformat.import.itemstatus.duplicatedintable", null,
								LocaleContextHolder.getLocale()));
						break;
					}
				}

			} finally {
				errors.popNestedPath();
			}

		}
	}

}
