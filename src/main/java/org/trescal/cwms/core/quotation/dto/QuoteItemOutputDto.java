package org.trescal.cwms.core.quotation.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuoteItemOutputDto {
	private Boolean isOther;
    private Boolean prefered;
    private Boolean accepted;
    private Boolean issued;
    private String statusName;
    private String source;
    private Boolean expired;
    private LocalDate exiryDate;
    private Integer quoteId;
    private String quoteNo;
    private Integer quotationVer;
    private Integer itemNo;
    private Integer itemId;
    private BigDecimal price;
    private BigDecimal discountRate;
    private BigDecimal discount;
    private BigDecimal finalPrice;
    private String currencyCode;
    private Integer costId;
	
    public QuoteItemOutputDto(LocalDate exiryDate, Integer quoteId, String quoteNo, Integer itemId,
    		Integer itemNo, BigDecimal finalPrice) {
		super();
		this.exiryDate = exiryDate;
		this.quoteId = quoteId;
		this.quoteNo = quoteNo;
		this.itemId = itemId;
		this.itemNo = itemNo;
		this.finalPrice = finalPrice;
	}

}
