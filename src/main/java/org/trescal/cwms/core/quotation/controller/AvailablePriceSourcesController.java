package org.trescal.cwms.core.quotation.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.quotation.dto.PreliminaryCompInstToQuoteItemWrapper;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.service.CompanyInstrumentsToQuotationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV})
public class AvailablePriceSourcesController {
	@Autowired
	private QuotationService quotationService;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private CalibrationTypeService calibrationTypeService;
	@Autowired
	private ServiceTypeService serviceTypeService;
    @Autowired
    private CompanyInstrumentsToQuotationService companyInstrumentsToQuotationService;
	
	@RequestMapping(value = "/availablepricesources.htm", method = RequestMethod.GET)
	public ModelAndView availablePriceSources(Model model, Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivKeyValue,
			@RequestParam(name = "plantid", required = false) Integer plantid,
			@RequestParam(name = "modelid", required = false) Integer modelid,
			@RequestParam(name = "caltypeid", required = false) Integer caltypeid,
			@RequestParam(name = "servicetypeid", required = false) Integer servicetypeid,
			@RequestParam(name = "quoteid", required = true) Integer quoteid) {
		
		PreliminaryCompInstToQuoteItemWrapper wrap = this.companyInstrumentsToQuotationService.getDefaultPrices
				(plantid != null ? this.instrumentService.get(plantid) : null, 
				 modelid != null ? this.instrumentModelService.get(modelid) : null, 
				 servicetypeid != null ? serviceTypeService.get(servicetypeid).getCalibrationType() : 
					 caltypeid != null ? this.calibrationTypeService.get(caltypeid) : null,
				 quoteid != null ? this.quotationService.get(quoteid) : null, subdivKeyValue.getKey(), locale);
		
		model.addAttribute("wrap", wrap);
		return new ModelAndView("trescal/core/quotation/availablepricesources");
	}	
	
	
	/**
	 * it used to update the cost price and the cost source when the user change the service type 
	 * in add instrument to quote screen
	 */
	@RequestMapping(value = "/availablepricesource.json", method = RequestMethod.GET)
	public @ResponseBody PreliminaryCompInstToQuoteItemWrapper availablePriceSource(Model model, Locale locale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivKeyValue,
			@RequestParam(name = "plantid", required = false) Integer plantid,
			@RequestParam(name = "modelid", required = false) Integer modelid,
			@RequestParam(name = "caltypeid", required = false) Integer caltypeid,
			@RequestParam(name = "quoteid", required = true) Integer quoteid) {
		
		PreliminaryCompInstToQuoteItemWrapper wrap = this.companyInstrumentsToQuotationService.getDefaultPrices
				(plantid != null ? this.instrumentService.get(plantid) : null, 
				 modelid != null ? this.instrumentModelService.get(modelid) : null, 
				 caltypeid != null ? this.calibrationTypeService.get(caltypeid) : null,
				 quoteid != null ? this.quotationService.get(quoteid) : null, subdivKeyValue.getKey(), locale);
		return wrap;
	}	
    
}
	