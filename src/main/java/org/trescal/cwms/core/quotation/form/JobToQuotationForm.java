package org.trescal.cwms.core.quotation.form;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.job.Job;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class JobToQuotationForm {
    private String clientRef;
    private Contact contact;
    private String currencyCode;
    private boolean fromJob;
    private Job job;
    private Integer personid;
    private LocalDate reqDate;
    private Contact sourcedBy;
    private List<Integer> toQuoteJobItemIds;
    private boolean linkToJob;

}
