package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.general.db;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.general.CustomGeneralCondition;

public interface CustomGeneralConditionDao extends AllocatedDao<Company, CustomGeneralCondition, Integer> {}