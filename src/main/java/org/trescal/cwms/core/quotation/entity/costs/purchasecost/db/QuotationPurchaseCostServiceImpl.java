package org.trescal.cwms.core.quotation.entity.costs.purchasecost.db;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.thirdparty.ThirdPartyCostSupportService;
import org.trescal.cwms.core.quotation.dto.DiscountSummaryDTO;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;

@Service("QuotationPurchaseCostService")
public class QuotationPurchaseCostServiceImpl extends BaseServiceImpl<QuotationPurchaseCost, Integer> implements QuotationPurchaseCostService
{
	@Autowired
	QuotationPurchaseCostDao baseDao;
	// injected properties
	@Value("#{props['cwms.config.quotation.resultsyearfilter']}")
	private Integer resultsYearFilter;
	@Value("#{props['cwms.config.costs.costings.roundupnearest50']}")
	boolean roundUpFinalCosts;
	@Autowired
	ThirdPartyCostSupportService thirdCostService;

	@Override
	public List<QuotationPurchaseCost> findMatchingCostOnLinkedQuotation(int jobid, int modelid, int calTypeId)
	{
		return this.baseDao.findMatchingCostOnLinkedQuotation(jobid, modelid, calTypeId);
	}

	@Override
	public List<QuotationPurchaseCost> findMatchingCosts(Integer coid, int modelid, Integer calTypeId, Integer page, Integer resPerPage)
	{
		return this.baseDao.findMatchingCosts(coid, modelid, calTypeId, this.resultsYearFilter, null, null);
	}

	@Override
	public DiscountSummaryDTO getDiscountDTO(int quoteid) {
		return this.baseDao.getDiscountDTO(quoteid);
	}

	@Override
	public QuotationPurchaseCost updateCosts(QuotationPurchaseCost cost)
	{
		if (cost.getHouseCost() == null)
		{
			cost.setHouseCost(new BigDecimal("0.00"));
		}

		// calculate TP costs
		cost = (QuotationPurchaseCost) this.thirdCostService.recalculateThirdPartyCosts(cost);

		cost.setTotalCost(cost.getHouseCost().add(cost.getThirdCostTotal()));

		// delegate to CostCalculator to apply any discounts and set the final
		// cost
		CostCalculator.updateSingleCost(cost, this.roundUpFinalCosts);
		return cost;
	}

	@Override
	protected BaseDao<QuotationPurchaseCost, Integer> getBaseDao() {
		return baseDao;
	}
}