package org.trescal.cwms.core.quotation.dto;

import org.trescal.cwms.core.quotation.entity.quotation.Quotation;

/**
 * Simple DTO class to wrap the results of a {@link Quotation} hql query see
 * QuotationService.searchCompanyQuotations()
 * 
 * @author Richard
 */
public class QuotationSearchResultWrapper
{
	private Integer qid;
	private String qno;
	private Integer ver;
	private Boolean issued;
	private Boolean accepted;
	private String coname;
	private Integer coid;
	private String conname;
	private Integer personid;
	private Boolean expired;
	private Long itemCount;

	public QuotationSearchResultWrapper()
	{

	}

	public QuotationSearchResultWrapper(Integer qid, String qno, Integer ver, Boolean issued, Boolean accepted, Boolean expired, String coname, Integer coid, String conname, Integer personid)
	{
		super();
		this.qid = qid;
		this.qno = qno;
		this.ver = ver;
		this.issued = issued;
		this.accepted = accepted;
		this.expired = expired;
		this.coname = coname;
		this.coid = coid;
		this.conname = conname;
		this.personid = personid;
	}

	public QuotationSearchResultWrapper(Integer qid, String qno, Integer ver, Boolean issued, Boolean accepted, Boolean expired, String coname, Integer coid, String conname, Integer personid, Long itemCount)
	{
		super();
		this.qid = qid;
		this.qno = qno;
		this.ver = ver;
		this.issued = issued;
		this.accepted = accepted;
		this.expired = expired;
		this.coname = coname;
		this.coid = coid;
		this.conname = conname;
		this.personid = personid;
		this.itemCount = itemCount;
	}

	public Boolean getAccepted()
	{
		return this.accepted;
	}

	public Integer getCoid()
	{
		return this.coid;
	}

	public String getConame()
	{
		return this.coname;
	}

	public String getConname()
	{
		return this.conname;
	}
	
	public Boolean getExpired()
	{
		return this.expired;
	}

	public Boolean getIssued()
	{
		return this.issued;
	}

	public Long getItemCount()
	{
		return this.itemCount;
	}

	public Integer getPersonid()
	{
		return this.personid;
	}

	public Integer getQid()
	{
		return this.qid;
	}

	public String getQno()
	{
		return this.qno;
	}

	public Integer getVer()
	{
		return this.ver;
	}

	public void setAccepted(Boolean accepted)
	{
		this.accepted = accepted;
	}

	public void setCoid(Integer coid)
	{
		this.coid = coid;
	}

	public void setConame(String coname)
	{
		this.coname = coname;
	}

	public void setConname(String conname)
	{
		this.conname = conname;
	}

	public void setExpired(Boolean expired)
	{
		this.expired = expired;
	}

	public void setIssued(Boolean issued)
	{
		this.issued = issued;
	}

	public void setItemCount(Long itemCount)
	{
		this.itemCount = itemCount;
	}

	public void setPersonid(Integer personid)
	{
		this.personid = personid;
	}

	public void setQid(Integer qid)
	{
		this.qid = qid;
	}

	public void setQno(String qno)
	{
		this.qno = qno;
	}

	public void setVer(Integer ver)
	{
		this.ver = ver;
	}

}
