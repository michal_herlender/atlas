package org.trescal.cwms.core.quotation.entity.quoteheading.comparators;

import java.util.Comparator;

import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;

public class QuotationHeadingIdComparator implements Comparator<QuoteHeading>
{
	@Override
	public int compare(QuoteHeading qh1, QuoteHeading qh2)
	{
		if (qh1.isSystemDefault() && !qh2.isSystemDefault())
		{
			return -1;
		}
		else if (!qh1.isSystemDefault() && qh2.isSystemDefault())
		{
			return 1;
		}
		else
		{
			return ((Integer) qh1.getHeadingId()).compareTo(qh2.getHeadingId());
		}
	}
}