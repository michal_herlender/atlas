package org.trescal.cwms.core.quotation.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.EntityNotFoundException;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.QuotationItemComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.quotation.form.RecallToQuotationForm;
import org.trescal.cwms.core.recall.entity.recall.Recall;
import org.trescal.cwms.core.recall.entity.recalldetail.RecallDetail;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;


@Service("RecallQuotationService")
public class RecallToQuotationServiceImpl implements RecallToQuotationService
{
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private ConvertToQuotationUtils convertToQuotationUtils;
	@Autowired
	private InstrumService instServ;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private QuotationItemService quotationItemService;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private SupportedCurrencyService supportedCurrencyService;
	@Autowired
	private SubdivService subdivServ;
	@Autowired
	private TaxCalculator taxCalculator;
	@Autowired
	protected MessageSource messageSource;	
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public Quotation prepareAndSaveRecallToQuotation(List<Integer> toQuotePlantIds, RecallDetail rdetail, Contact currentContact, Subdiv allocatedSubdiv) throws EntityNotFoundException
	{
		Quotation quotation = this.prepareRecallToQuotation(toQuotePlantIds, rdetail, currentContact, allocatedSubdiv);
		this.quoteServ.save(quotation);
		return quotation;
	}

	@Override
	public void prepareCalibrationCost(Instrument inst, JobItem ji, QuotationCalibrationCost qCalCost, Subdiv subDiv)
	{
		if (ji != null)
		{
			this.convertToQuotationUtils.prepareCalibrationCostFromJobItem(ji, qCalCost);
		}
		else
		{
			// prepare from instrument
			this.convertToQuotationUtils.prepareCalibrationCostFromModel(inst, qCalCost, subDiv);
		}
	}

	@Override
	public Set<Quotationitem> prepareRecallInstrumentsToQuotationItems(List<Instrument> toQuoteInsts, Quotation quotation, int subdivId)
	{
		QuoteHeading qh = quotation.getQuoteheadings().iterator().next();
		TreeSet<Quotationitem> qItems = new TreeSet<Quotationitem>(new QuotationItemComparator());
		// get all calibration types currently active
		List<CalibrationType> caltypes = this.calTypeServ.getActiveCalTypes();
		// add all items for each calibration type
		for (CalibrationType caltype : caltypes)
		{
			// initialise item count
			int itemcount = 1;
			// check all recall insts
			for (Instrument inst : toQuoteInsts)
			{
				// start creating the quotation item
				Quotationitem qItem = this.recallInstrumentToQuotationitem(inst, quotation, qh, subdivId);
				// caltypes match
				if (qItem.getCaltype().getCalTypeId() == caltype.getCalTypeId())
				{
					qItem.setItemno(itemcount);
					itemcount++;
					qItems.add(qItem);
				}
			}
		}

		this.logger.info("Added " + qItems.size() + " quotation items");

		return qItems;
	}

	@Override
	public Quotation prepareRecallToQuotation(List<Integer> toQuotePlantIds, RecallDetail rdetail, Contact currentContact, Subdiv allocatedSubdiv) throws EntityNotFoundException
	{
		Recall recall = rdetail.getRecall();
		if (recall == null) throw new EntityNotFoundException();
		Contact quoteContact = rdetail.getContact();
		if (quoteContact == null) throw new EntityNotFoundException();
		Locale locale = LocaleContextHolder.getLocale();
		Quotation quotation = this.quoteServ.createTemplateNewQuotation(currentContact, quoteContact, allocatedSubdiv, allocatedSubdiv.getComp(), locale);
		quotation.setCreatedBy(currentContact);
		quotation.setSourcedBy(currentContact);
		// get instruments for plant id list
		List<Instrument> toQuoteInsts = this.instServ.getInstruments(toQuotePlantIds);
		// convert list of recall instruments to quotation items
		quotation.setQuotationitems(this.prepareRecallInstrumentsToQuotationItems(toQuoteInsts, quotation, allocatedSubdiv.getSubdivid()));
		CostCalculator.updatePricingTotalCost(quotation, quotation.getQuotationitems());
		taxCalculator.calculateAndSetVATaxAndFinalCost(quotation);
		QuoteNote qn = new QuoteNote();
		qn.setActive(true);
		qn.setPublish(false);
		qn.setNote(messageSource.getMessage("createquot.createdfromrecall", new Object[] {recall.getRecallNo()}, "Created from recall " + recall.getRecallNo(), locale));
		qn.setLabel("");
		qn.setSetBy(currentContact);
		qn.setSetOn(new Date());
		qn.setQuotation(quotation);
		quotation.getNotes().add(qn);
		return quotation;
	}
	
	@Override
	public Quotationitem recallInstrumentToQuotationitem(Instrument instrument, Quotation quotation, QuoteHeading heading, int subdivId)
	{
		// get the most recent jobitem for this
		CalibrationType caltype = null;

		// look for the most recent jobitem for this instrument (there may or
		// may not be a jobitem - instruments can be recalled without having
		// ever appeared on a job)
		JobItem ji = this.jobItemService.findMostRecentJobItem(instrument.getPlantid());

		if (ji == null)
		{
			// no previous jobitem, chose the default cal type
			caltype = this.calTypeServ.getDefaultCalType();
		}
		else
		{
			// previous jobitem, use this cal type
			caltype = ji.getCalType();
		}
		// create new quote item from template
		Quotationitem qItem = this.quotationItemService.createTemplateNewQuotationItem(quotation, heading, instrument, null, caltype.getCalTypeId(),subdivId);
		// work out the cost of the item based on it's most recent cal cost (or
		// model defaults if no previous costing)
		Subdiv subDiv = subdivServ.get(subdivId);
		this.prepareCalibrationCost(instrument, ji, qItem.getCalibrationCost(), subDiv);
		// update item costs
		CostCalculator.updateItemCostWithRunningTotal(qItem);

		return qItem;
	}
	
	@Override
	public Quotation createQuotationFromRecall(RecallToQuotationForm form, Subdiv allocatedSubdiv, Contact currentContact){
		Quotation quotation = this.prepareAndSaveRecallToQuotation(form.getToQuotePlantIds(), form.getRdetail(), currentContact, allocatedSubdiv);
		// only update these fields if not coming from recall
		if (!form.isFromRecall()) {
			quotation.setContact(form.getContact());
			quotation.setSourcedBy(form.getSourcedBy());
		}
		quotation.setClientref(form.getClientRef());
		quotation.setReqdate(form.getReqDate());
		this.supportedCurrencyService.setCurrencyFromForm(quotation, form.getCurrencyCode(), quotation.getContact().getSub().getComp());
		this.quoteServ.merge(quotation);
		return quotation;
	}
}
