package org.trescal.cwms.core.quotation.entity.quotation.db;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.collections4.MultiValuedMap;
import org.springframework.ui.Model;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelWithPriceDTO;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.TotalDiscountCalculator;
import org.trescal.cwms.core.quotation.dto.QuotationKeyValue;
import org.trescal.cwms.core.quotation.dto.QuotationProjectionDTO;
import org.trescal.cwms.core.quotation.dto.QuotationSearchResultWrapper;
import org.trescal.cwms.core.quotation.dto.QuotationSummaryDTO;
import org.trescal.cwms.core.quotation.dto.QuoteItemOutputDto;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.QuotationMetricFormatter;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.QuotationConditions;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.quotation.entity.quotestatusgroup.QuoteStatusGroup;
import org.trescal.cwms.core.quotation.form.ImportedQuotationItemsSynthesisForm.ImportedQuotationItemsSynthesisRowDTO;
import org.trescal.cwms.core.quotation.form.NewQuotationSearchForm;
import org.trescal.cwms.core.quotation.form.QuotationMetricForm;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.tools.PagedResultSet;

/**
 * Interface for accessing and manipulating {@link Quotation} entities.
 *
 * @author Richard
 */
public interface QuotationService extends BaseService<Quotation, Integer> {

	ResultWrapper ajaxDeleteQuotation(int quoteId);

	/**
	 * Creates a new {@link Quotation} entity and copies the properties of the
	 * argument {@link Quotation} into the new {@link Quotation}. Note, only direct
	 * fields of the {@link Quotation} are copied, lists of {@link Quotationitem},
	 * {@link QuoteHeading} etc are disregarded.
	 * 
	 * @param q
	 *            the {@link Quotation} to copy.
	 * @return the new {@link Quotation}.
	 */
	Quotation copyQuotation(Quotation q);

	Long countByCompanyAndAllocatedCompany(Company company, Company allocatedCompany);

	Long countActualByCompanyAndAllocatedCompany(Integer yearsBack, Company company, Company allocatedCompany);

	Long countByContactAndAllocatedSubdiv(Contact contact, Subdiv allocatedSubdiv);

	Long countActualByContactAndAllocatedSubdiv(Integer yearsBack, Contact contact, Subdiv allocatedSubdiv);

	Long countBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv);

	Long countActualBySubdivAndAllocatedSubdiv(Integer yearsBack, Subdiv subdiv, Subdiv allocatedSubdiv);

	/**
	 * Creates a new {@link Quotation} object with properties initialised to their
	 * default values (This includes grabbing and updating the ids table). Methods
	 * calling this should override any of the default properties set on the
	 * Quotation entity prior to persisting.
	 * 
	 * @return
	 */
	Quotation createTemplateNewQuotation(Contact currentContact, Contact quoteContact, Subdiv allocatedSubdiv, Company allocatedCompany, Locale locale);

	QuoteNote createTemplateNewQuoteNote(Quotation quotation, boolean active, String label, String note,
			boolean publish, Contact contact);

	/**
	 * Returns the next version of the given {@link Quotation} if a new one were to
	 * be created.
	 * 
	 * @param qno
	 *            the {@link Quotation} number.
	 * @return the next version number.
	 */
	int findNextVersion(String qno);

	/**
	 * Returns a {@link QuotationConditions} object representing the current terms
	 * and conditions applied to the given {@link Quotation}.
	 * 
	 * @param quotation
	 *            the {@link Quotation}.
	 * @return {@link QuotationConditions} applied to this {@link Quotation}.
	 */
	QuotationConditions findQuotationConditions(Quotation quotation);

	/**
	 * Returns a {@link ResultWrapper} with an eagerly loaded {@link Quotation}
	 * entity identified by the given id.
	 * 
	 * @param quoteid
	 *            the {@link Quotation} id.
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper findQuotationDWR(int quoteid);

	/**
	 * Return all {@link Quotation}(s) that have the matching qno (i.e. all versions
	 * of the same {@link Quotation}.
	 * 
	 * @param qno
	 *            the {@link Quotation} number.
	 * @return list of matching {@link Quotation}(s).
	 */
	List<Quotation> findQuotations(String qno);

	List<InstrumentModelWithPriceDTO> findServiceModelsOnQuotation(Integer quoteId);
	
	List<InstrumentModelWithPriceDTO> findServiceModelsOnLinkedQuotations(Integer jobId);

	List<QuotationKeyValue> findLinkedQuotationsWithServiceModels(Job job);

	boolean getAvalaraAware();
	
	// Intentionally exposed for integration testing of documents
	void setAvalaraAware(boolean avalaraAware);
	
	/**
	 * Get a {@link Set} of distinct {@link CalibrationType}(s) that are presant on
	 * the {@link Quotation} identified by the given id.
	 * 
	 * @param quoteid
	 *            the id of the {@link Quotation}.
	 * @return {@link Set} of {@link CalibrationType}(s).
	 */
	Set<CalibrationType> getCaltypesPerQuotation(int quoteid);

	/**
	 * Get a distinct list of {@link Model}(s) that belong to the {@link Quotation}.
	 * 
	 * @param quotation
	 *            the {@link Quotation} to get distinct models from.
	 * @return {@link Set}{@link Model}(s).
	 */
	Set<InstrumentModel> getDistinctModels(Quotation quotation);

	/**
	 * Returns the calculated expiry date for the given {@link Quotation} (Issuedate
	 * + duration).
	 *
	 * @param q
	 *            the {@link Quotation}.
	 * @return {@link Date} or null if not possible to calculate.
	 */
	LocalDate getExpiryDate(Quotation q);

	/**
	 * get a list of issued {@link Quotation}'s for a contact using the issue date
	 * supplied
	 * 
	 * @param personId
	 *            the id of the contact we are searching quotes for
	 * @param issuedate
	 *            a date provided from which we should get quotations for contact
	 * @param accepted
	 *            indicates if we require accepted quotes
	 * @return list of {@link Quotation}'s
	 */
	List<Quotation> getIssuedQuotationsForContactFromIssuedate(int personId, LocalDate issuedate, boolean accepted);

	/**
	 * Returns the {@link Quotation} with the given Quotation No or null if there
	 * are no quotations with the given Quotation No.
	 * 
	 * @param quoteNo
	 *            the quoteNo to match.
	 * @return the {@link Quotation}
	 */
	Quotation getQuotationByExactQuoteNo(String quoteNo);

	/**
	 * get a list of {@link Quotation}'s for a contact
	 * 
	 * @param personId
	 *            the id of the contact we are searching quotes for
	 * @return list of {@link Quotation}'s
	 */
	List<Quotation> getQuotationsRequestForContactFromReqdate(int personId, LocalDate reqdate,
			boolean accepted, boolean issued);

	/**
	 * Builds a quotation summary to be used in display and / or document Replaces
	 * TotalDiscountCalculator and PricingTools which had poor performance for 1000s
	 * of quotation items
	 * 
	 * @param quotation
	 * @return summary DTO
	 */
	QuotationSummaryDTO getQuotationSummary(Quotation quotation);

	/**
	 * Calculates the total discount number + value applied to the
	 * {@link Quotationitem}(s).
	 * 
	 * @param qitems
	 *            the {@link Quotationitem}(s) to calculate the discount for.
	 * @return {@link TotalDiscountCalculator} containing the applied discounts.
	 */
	@Deprecated
	TotalDiscountCalculator getQuotationTotalDiscount(Set<Quotationitem> qitems);

	Quotation insertQuotationFromRequest(QuotationRequest quotationRequest, int personid, boolean addRequestInfoAsNote,
			Contact currentContact, Subdiv allocatedSubdiv);

	/**
	 * Checks that all items on a quote are unique by concatenating the itemno,
	 * quoteid, caltypeid and headingid
	 * 
	 * @param q
	 *            the {@link Quotation} to check
	 * @return boolean
	 */
	boolean isAllItemsOnQuotationUnique(Quotation q);


	/**
	 * Loads metric data into a {@link QuotationMetricFormatter} and generates basic
	 * % reports on various {@link Quotation} related aspects (conversion rate etc).
	 * The criteria for the data ranges is taken from the
	 * {@link QuotationMetricForm}.
	 * 
	 * @param metricForm
	 *            form object containing information about the data range - e.g.
	 *            date ranges.
	 * @return {@link QuotationMetricFormatter}.
	 */
	QuotationMetricFormatter loadMetrics(QuotationMetricForm metricForm);

	/**
	 * Returns a list of arrays representing {@link Quotation} that match the given
	 * criteria.
	 * 
	 * @param coid
	 *            the id of a {@link Company}, not null.
	 * @param showExpired
	 *            if true expired {@link Quotation} are shown in the results.
	 * @param showUnIssued
	 *            if true then non-issued quotes will also be shown.
	 * @param years
	 *            defines how many years the results should cover, null searches
	 *            all.
	 * @param jobid
	 *            if not null then results will not include any quotations linked to
	 *            the {@link Job} identified by the jobid, if null then all are
	 *            returned.
	 * @return {@link QuotationSearchResultWrapper}
	 */
	List<QuotationSearchResultWrapper> searchCompanyQuotations(Integer coid, Boolean showExpired, Boolean showUnIssued,
			Integer years, Integer jobid);

	/**
	 * Updates the showTotalQuoteCaltypeValue setting of the {@link Quotation}, true
	 * displays, false hides.
	 * 
	 * @param show
	 *            toggle to display or hide the total value of the
	 *            {@link Quotation}.
	 * @param quoteid
	 *            the id of the {@link Quotation} to update.
	 * @return {@link ResultWrapper} indicating if this operation was a success.
	 */
	ResultWrapper updateQuotationCaltypeTotalVisibility(boolean show, int quoteid);

	Quotation updateQuotationCosts(Quotation q);

	/**
	 * Updates the showTotalHeadingCaltypeValue setting of the {@link Quotation} ,
	 * true displays, false hides.
	 * 
	 * @param show
	 *            toggle to display or hide the total heading value of the
	 *            {@link Quotation}.
	 * @param quoteid
	 *            the id of the {@link Quotation} to update.
	 * @return {@link ResultWrapper} indicating if this operation was a success.
	 */
	ResultWrapper updateQuotationHeadingCaltypeTotalVisibility(boolean show, int quoteid);

	/**
	 * Updates the showTotalHeadingValue setting of the {@link Quotation}, true
	 * displays, false hides.
	 * 
	 * @param show
	 *            toggle to display or hide the total heading value of the
	 *            {@link Quotation}.
	 * @param quoteid
	 *            the id of the {@link Quotation} to update.
	 * @return {@link ResultWrapper} indicating if this operation was a success.
	 */
	ResultWrapper updateQuotationHeadingTotalVisibility(boolean show, int quoteid);

	/**
	 * Updates the showTotalQuoteValue setting of the {@link Quotation}, true
	 * displays, false hides.
	 * 
	 * @param show
	 *            toggle to display or hide the total value of the
	 *            {@link Quotation}.
	 * @param quoteid
	 *            the id of the {@link Quotation} to update.
	 * @return {@link ResultWrapper} indicating if this operation was a success.
	 */
	ResultWrapper updateQuotationTotalVisibility(boolean show, int quoteid);

	/**
	 * this method validates a quotation requested via the website.
	 * 
	 * @param poNumber
	 *            the purchase order number supplied
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper webValidateQuoteRequest(String poNumber);

	List<Quotation> getQuotationListByJobitemId(Integer jobitemid);

	List<Quotation> getIssuedAndAcceptedQuotationListByJobitemId(Integer jobitemid);
	
	List<ImportedQuotationItemsSynthesisRowDTO> convertFileContentToQuotationItemsDTO(Integer exchangeFormat,
			List<LinkedCaseInsensitiveMap<String>> fileContent) throws ParseException;
	
	List<QuotationProjectionDTO> getAllQuotesInStatusGroupNew(QuoteStatusGroup group, Integer allocatedSubdivId, Locale locale);
	List<QuotationProjectionDTO> getMostRecentIssuedQuotationsNew(int resultSize, Integer allocatedSubdivId, Locale locale);
	PagedResultSet<QuotationProjectionDTO> queryQuotationNew(NewQuotationSearchForm qsf, PagedResultSet<QuotationProjectionDTO> rs, Locale locale);

	void getPaginationAllQuotesInStatusGroupNew(QuoteStatusGroup webrequests, Integer allocatedSubdivId,
			PagedResultSet<QuotationProjectionDTO> webQuotesRequiringAction, NewQuotationSearchForm qsf, Locale locale);
	
    List<Integer> getPlantIdsFromQuotation(Integer quoteId);
        
    /**
	 * this method get for each instrument of 
	 * a quotation item their service types 
	 * @param quoteId {@link  Quotation}
	 * 
	 * @return {@link  MultiValuedMap}
	 */
    MultiValuedMap<Integer, Integer> quotationItemsFromInstrument(Integer quoteId);
    
    List<QuoteItemOutputDto> findQuotationItemsFromModel(Integer modelId, Integer servicetypeid, Integer coid);
    
}