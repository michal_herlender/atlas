package org.trescal.cwms.core.quotation.dto;

import org.trescal.cwms.spring.model.KeyValue;

public class QuotationKeyValue extends KeyValue<Integer, String> {

	public QuotationKeyValue() {
		super();
	}

	public QuotationKeyValue(Integer quoteId, String quoteNo) {
		super(quoteId, quoteNo);
	}
}