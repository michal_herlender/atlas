package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.general.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.general.CustomGeneralCondition;

public interface CustomGeneralConditionService extends BaseService<CustomGeneralCondition, Integer> {}