package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.calibration.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.calibration.CustomCalibrationCondition;

public interface CustomCalibrationConditionDao extends BaseDao<CustomCalibrationCondition, Integer>
{
	List<CustomCalibrationCondition> getListCustomCalibrationConditions(List<Integer> ids);
}