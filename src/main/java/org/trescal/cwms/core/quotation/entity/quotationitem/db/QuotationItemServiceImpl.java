package org.trescal.cwms.core.quotation.entity.quotationitem.db;

import io.vavr.control.Option;
import lombok.val;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.InstrumentModelPartOf;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.db.InstrumentModelPartOfService;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.db.ServiceCapabilityService;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewLinkedCalibrationCost;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.purchasecost.ContractReviewLinkedPurchaseCost;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.CostLookUp;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costs.CostSourceSupportService;
import org.trescal.cwms.core.pricing.entity.costs.base.db.CalCostService;
import org.trescal.cwms.core.pricing.entity.costs.base.db.PurchaseCostService;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostMarkupSource;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostSource;
import org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.db.ThirdPartyPricingItemService;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingLinkedCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.pricing.lookup.PriceLookupRequirements;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupOutputQuotationItem;
import org.trescal.cwms.core.pricing.lookup.dto.PriceLookupQueryType;
import org.trescal.cwms.core.pricing.tax.TaxCalculator;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.quotation.dto.*;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.calcost.db.QuotationCalibrationCostService;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.db.QuotationPurchaseCostService;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.QuotationItemComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.comparators.QuotationItemSortType;
import org.trescal.cwms.core.quotation.entity.quotationitem.dto.QuotationItemOptionDto;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteheading.db.QuoteHeadingService;
import org.trescal.cwms.core.quotation.entity.quoteitemnote.QuoteItemNote;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.quotation.form.EditQuotationItemForm;
import org.trescal.cwms.core.quotation.form.QuotationItemSearchForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.note.db.NoteService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.tools.GenericTools;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.MathTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost.TPQuotationCalibrationCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.purchasecost.TPQuotationPurchaseCost;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service("QuotationItemService")
public class QuotationItemServiceImpl implements QuotationItemService {
	@Value("${cwms.config.quotation.size.restrictcopyitem}")
	private long sizeRestrictCopyItem;
	@Value("${cwms.config.quotation.size.restrictdeleteitem}")
	private long sizeRestrictDeleteItem;
	@Value("${cwms.config.costs.calibration.lookup.quotationitem}")
	private String lookupSources;
	@Value("${cwms.config.costs.quotations.roundupnearest50}")
	private boolean roundUpFinalCosts;

	@Autowired
	private CalCostService calCostServ;
	@Autowired
	private CalibrationTypeService caltypeServ;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private CatalogPriceService priceServ;
	@Autowired
	private ContactService contactService;
	@Autowired
	private InstrumentModelPartOfService impoServ;
	@Autowired
	private InstrumentModelService modelService;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private NoteService noteService;
	@Autowired
	private QuoteHeadingService qhServ;
	@Autowired
	private QuotationItemDao qiDao;
	@Autowired
	private QuotationCalibrationCostService quotationCalibrationCostService;
	@Autowired
	private QuotationPurchaseCostService quotationPurchaseCostService;
    @Autowired
    private QuotationService quoteServ;
    @Autowired
    private ServiceCapabilityService serviceCapabilityService;
    @Autowired
    private SubdivService divServ;
    @Autowired
    private SupportedLocaleService supportedLocaleService;
	@Autowired
	private TaxCalculator taxCalculator;
    @Autowired
    private ThirdPartyPricingItemService thirdPartyPricingItemService;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private CapabilityService capabilityService;
    @Autowired
    private PurchaseCostService purCostServ;
    @Autowired
    private UserService userService;
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Override
    public Quotationitem addModules(int baseItemId, int[] moduleItemNos) {
        // convert the array of modules to an arraylist
        ArrayList<Integer> modules = GenericTools.convertIntArrayToArrayList(moduleItemNos);

        // get the base unit
		Quotationitem baseItem = this.findQuotationItem(baseItemId);

		// get a list of quotation item modules
		List<Quotationitem> modItems = this.findQuotationItems(modules);

		// create a new partof object for each and persist it
		for (Quotationitem mod : modItems) {
			mod.setBaseUnit(baseItem);
			mod.setPartOfBaseUnit(true);
			this.updateQuotationItem(mod);
		}

		return baseItem;
	}

	public ResultWrapper addModulesToQuotationItemBaseUnit(int qiid, ModuleToBaseUnitDTO[] dtoList, int subdivId) {
		// find quotation item
		Quotationitem baseItem = this.findQuotationItem(qiid);
		// quotation item null?
		if (baseItem != null) {
			// check module list
			if (dtoList.length > 0) {
				// create reference variable for quote
				Quotation quotation = baseItem.getQuotation();
				// add each module
				for (ModuleToBaseUnitDTO dto : dtoList) {
					// create new quote item
					Quotationitem newItem = this.createTemplateNewQuotationItem(baseItem.getQuotation(),
							baseItem.getHeading(), null, dto.getModelId(), baseItem.getCaltype().getCalTypeId(),
							subdivId);
					// set qty and heading
					newItem.setQuantity(dto.getQty());
					newItem.setBaseUnit(baseItem);
					newItem.setPartOfBaseUnit(true);
					newItem.setPlantno(dto.getPlantNo());
					newItem.setReferenceNo(dto.getPlantNo());

					newItem.getCalibrationCost().setHouseCost(new BigDecimal(dto.getUnitCost()));
					newItem.getCalibrationCost().setTotalCost(new BigDecimal(dto.getUnitCost()));
					newItem.getCalibrationCost().setFinalCost(new BigDecimal(dto.getUnitCost()));

					// insert new quotation item
					this.insertQuotationItem(newItem);

					CostCalculator.updateItemCostWithRunningTotal(newItem);

					// add new quote item to quotation
					quotation.getQuotationitems().add(newItem);
					// update quotation
					quotation = this.quoteServ.merge(quotation);
				}
				// now update quotation totals
				CostCalculator.updatePricingTotalCost(quotation, quotation.getQuotationitems());
				taxCalculator.calculateAndSetVATaxAndFinalCost(quotation);
				// finally update the numbering of the items
				this.sortQuotationItems(quotation);
				// return successful wrapper
				return new ResultWrapper(true, "", null, null);
			} else {
				// return un-successful wrapper
				return new ResultWrapper(false, "No modules have been supplied to add to base unit", null, null);
			}
		} else {
			// return un-successful wrapper
			return new ResultWrapper(false, "Quotation item could not be found", null, null);
		}
	}

	/*
	 * public ResultWrapper addNewModulesToBaseUnitQI(int baseItemId, int[]
	 * moduleModelIds) { // first get base item Quotationitem baseItem =
	 * this.qiDao.findQuotationItem(baseItemId); // base item null? if (baseItem
	 * != null) { for (int mmId : moduleModelIds) { // find the module model } }
	 * else { // return wrapper return new ResultWrapper(false,
	 * "The base item to be updated could not be found", null, null); } }
	 */
	@Override
	public ResultWrapper ajaxDeleteQuotationItems(Integer quoteId, String listOfIds, Integer personId,
			Integer subDivId) {
		String[] qiStringIds = listOfIds.split(",");
		Integer[] qiIds = new Integer[qiStringIds.length];
		int i = 0;
		while (i < qiStringIds.length) {
			qiIds[i] = Integer.parseInt(qiStringIds[i]);
			i++;
		}

		List<Quotationitem> qis = new ArrayList<>();
		boolean okToDelete = true;
		String allReasons = "";

		long totalItemCount = this.getItemCount(quoteId);
		if (totalItemCount >= sizeRestrictDeleteItem) {
			okToDelete = false;
			allReasons = "This quotation has " + sizeRestrictDeleteItem
					+ " items and due to temporary performance restrictions, quotations with " + sizeRestrictDeleteItem
					+ " or more items cannot have their items deleted.";
		}

		if (okToDelete) {
			for (Integer id : qiIds) {
				// get quote item
				Quotationitem qitem = this.findQuotationItem(id);
				qis.add(qitem);
				ResultWrapper rw = this.checkQuotationItemIsSafeToDelete(qitem);

				if (!rw.isSuccess()) {
					okToDelete = false;
					allReasons = allReasons.concat(rw.getMessage() + "<br/><br/>");
				}
			}
		}

		if (okToDelete) {
			Quotation quotation = this.quoteServ.get(quoteId);
			for (Quotationitem qitem : qis) {
				Set<Quotationitem> modules = qitem.getModules();
				qitem.setModules(null);

				// first delete any modules if this is a base unit
				for (Quotationitem mod : modules) {
					this.deleteQuoteItem(quotation, mod);
				}

				// remove quote item from references and delete
				this.deleteQuoteItem(quotation, qitem);

				// update the total cost of the quotation
				CostCalculator.updatePricingTotalCost(quotation, quotation.getQuotationitems());
				taxCalculator.calculateAndSetVATaxAndFinalCost(quotation);
			}

			Contact currentContact = this.contactService.get(personId);
			Subdiv subdiv = this.divServ.get(subDivId);
			this.tidyQuoteNotes(quotation, currentContact, subdiv);
			this.sortQuotationItems(quotation);
			this.quoteServ.merge(quotation);

			return new ResultWrapper(true, "Quotation items successfully deleted");
		} else {
			return new ResultWrapper(false, allReasons);
		}
	}

	/**
	 * Assembles a translated comment regarding a matching ServiceCapability, if
	 * it exists If no service capability exists, or no procedure specified,
	 * null is returned for the caller to handle 2019-01-30 - description, if
	 * present is placed on a new line with html <br/>
	 * tags as it becomes an html note
	 */
	@Override
	public String getServiceCapabilityComment(InstrumentModel model, CalibrationType calType, Subdiv subdiv,
			Locale locale) {
        val procOpt = Option.ofOptional(capabilityService.getActiveCapabilityUsingCapabilityFilter(
                model.getDescription().getId(), calType.getServiceType().getServiceTypeId(), subdiv.getSubdivid()))
            .orElse(() -> {
                val serviceCapability = Option.of(serviceCapabilityService.getCalService(model, calType, subdiv));
                return serviceCapability.map(ServiceCapability::getCapability);
            }).toJavaOptional();

        return procOpt.map(proc -> {
            StringBuilder comment = new StringBuilder();
            comment.append(subdiv.getSubname());
            comment.append(" / ");
            comment.append(
                this.translationService.getCorrectTranslation(model.getDescription().getTranslations(), locale));
            comment.append(" / ");
            comment.append(this.translationService
                .getCorrectTranslation(calType.getServiceType().getLongnameTranslation(), locale));
			comment.append(" / ");
			comment.append(proc.getReference());
			comment.append(" / ");
			String description = proc.getDescription();
			if (description != null && !description.isEmpty()) {
				comment.append("<br/>\n");
				comment.append(description.replaceAll("\\n", "<br/>\n"));
			}
			return comment.toString();
		}).orElse(null);
	}

	/**
	 * Assembles a map via iteration of all existing QuoteNote with service
	 * capability labels for the specified locale (notes may be inactive or
	 * active) key = comment, value = QuoteNote
	 */
	@Override
	public Map<String, QuoteNote> getServiceCapabilityNoteMap(Quotation quotation, Locale locale) {
		String label = messageSource.getMessage("quoteautonote.procedurenote", null, locale);
		Map<String, QuoteNote> result = new HashMap<>();
		for (QuoteNote note : quotation.getNotes()) {
			// Some notes (if manually entered) may not have a label
			if ((note.getLabel() != null) && note.getLabel().equals(label)) {
				result.put(note.getNote(), note);
			}
		}

		return result;
	}

	/**
	 * Method to make sure no irrelevant procedure notes are left behind when a
	 * quote item is modified / deleted. Updates: 2016-10-07 - Galen - made
	 * public, reuse inactivated notes rather than creating new ones
	 * 
	 */
	@Override
	public void tidyQuoteNotes(Quotation quotation, Contact currentContact, Subdiv subdiv) {

		Locale locale = LocaleContextHolder.getLocale();

		Map<String, QuoteNote> existingNotes = getServiceCapabilityNoteMap(quotation, locale);
		String label = messageSource.getMessage("quoteautonote.procedurenote", null, locale);

		// Disable all procedure quote notes - only works if person deleting
		// quote item
		// has same language as person who added quote item
		// Further changes will be needed if this proves to be an issue. (e.g.
		// add
		// boolean to QuoteNote?)
		for (QuoteNote note : quotation.getNotes()) {
			// Some notes (if manually entered) may not have a label
			if ((note.getLabel() != null) && note.getLabel().equals(label)) {
				noteService.changeNoteStatus(note.getNoteid(), false, currentContact.getPersonid(), NoteType.QUOTENOTE,
						null);
			}
		}

		// Re insert the correct procedure quote notes
		for (Quotationitem qi : quotation.getQuotationitems()) {
			InstrumentModel model = (qi.getInst() != null) ? qi.getInst().getModel() : qi.getModel();
			String comment = this.getServiceCapabilityComment(model, qi.getCaltype(), subdiv, locale);
			if (comment != null) {
				QuoteNote matchingNote = existingNotes.get(comment);
				if (matchingNote == null) {
					QuoteNote newNote = this.quoteServ.createTemplateNewQuoteNote(quotation, true, label, comment, true,
							currentContact);
					quotation.getNotes().add(newNote);
					existingNotes.put(comment, newNote);

				} else {
					matchingNote.setActive(true);
					matchingNote.setPublish(true);
				}
			}
		}
	}

	@Override
	public ResultWrapper checkQuotationItemIsSafeToDelete(Quotationitem qi) {
		int i = qi.getCalibrationCost().getConRevLinkedCalibrationCosts().size();
		i += qi.getCalibrationCost().getJobCostLinkedCalibrationCosts().size();
		i += qi.getPurchaseCost().getConRevLinkedPurchaseCosts().size();

		if (i > 0) {
			String reason = "Item " + qi.getItemno()
					+ " cannot be deleted because it has been used as a cost source on the following:<br/>";

			for (ContractReviewLinkedCalibrationCost crlcc : qi.getCalibrationCost()
					.getConRevLinkedCalibrationCosts()) {
				if ((crlcc.getCalibrationCost() != null) && (crlcc.getCalibrationCost().getJobitem() != null)) {
					JobItem ji = crlcc.getCalibrationCost().getJobitem();
					reason = reason.concat("Contract Review calibration cost for item " + ji.getItemNo() + " on Job "
							+ ji.getJob().getJobno() + ".<br/>");
				}
			}

			for (JobCostingLinkedCalibrationCost jclcc : qi.getCalibrationCost().getJobCostLinkedCalibrationCosts()) {
				if ((jclcc.getCalibrationCost() != null) && (jclcc.getCalibrationCost().getJobCostingItem() != null)) {
					JobCostingItem jci = jclcc.getCalibrationCost().getJobCostingItem();
					reason = reason.concat("Job costing calibration cost for item " + jci.getItemno() + " on costing "
							+ jci.getJobCosting().getIdentifier() + ".<br/>");
				}
			}

			for (ContractReviewLinkedPurchaseCost crlpc : qi.getPurchaseCost().getConRevLinkedPurchaseCosts()) {
				if ((crlpc.getPurchaseCost() != null) && (crlpc.getPurchaseCost().getJobitem() != null)) {
					JobItem ji = crlpc.getPurchaseCost().getJobitem();
					reason = reason.concat("Contract Review purchase cost for item " + ji.getItemNo() + " on Job "
							+ ji.getJob().getJobno() + ".<br/>");
				}
			}

			return new ResultWrapper(false, reason);
		} else {
			return new ResultWrapper(true, null);
		}
	}

	@Override
	public void copyCosts(Quotationitem sourceItem, Collection<Quotationitem> destItems) {
		for (Quotationitem qItem : destItems) {
			// copy calibration costs
			QuotationCalibrationCost srcCalCost = sourceItem.getCalibrationCost();
			QuotationCalibrationCost destCalCost = qItem.getCalibrationCost();

			// sequentially copy the cal cost fields one-by-one
			destCalCost.setActive(srcCalCost.isActive());
			destCalCost.setCustomOrderBy(srcCalCost.getCustomOrderBy());
			destCalCost.setDiscountRate(srcCalCost.getDiscountRate());
			destCalCost.setDiscountValue(srcCalCost.getDiscountValue());
			destCalCost.setFinalCost(srcCalCost.getFinalCost());
			destCalCost.setHourlyRate(srcCalCost.getHourlyRate());
			destCalCost.setHouseCost(srcCalCost.getHouseCost());
			destCalCost.setLinkedCostSrc(srcCalCost.getLinkedCostSrc());
			destCalCost.setThirdCostSrc(srcCalCost.getThirdCostSrc());
			destCalCost.setThirdCostTotal(srcCalCost.getThirdCostTotal());
			destCalCost.setThirdManualPrice(srcCalCost.getThirdManualPrice());
			destCalCost.setThirdMarkupRate(srcCalCost.getThirdMarkupRate());
			destCalCost.setThirdMarkupSrc(srcCalCost.getThirdMarkupSrc());
			destCalCost.setThirdMarkupValue(srcCalCost.getThirdMarkupValue());

			destCalCost.setTpCarriageIn(srcCalCost.getTpCarriageIn());
			destCalCost.setTpCarriageInMarkupValue(srcCalCost.getTpCarriageInMarkupValue());
			destCalCost.setTpCarriageMarkupRate(srcCalCost.getTpCarriageMarkupRate());
			destCalCost.setTpCarriageMarkupSrc(srcCalCost.getTpCarriageMarkupSrc());
			destCalCost.setTpCarriageOut(srcCalCost.getTpCarriageOut());
			destCalCost.setTpCarriageOutMarkupValue(srcCalCost.getTpCarriageOutMarkupValue());
			destCalCost.setTpCarriageTotal(srcCalCost.getTpCarriageTotal());

			destCalCost.setTotalCost(srcCalCost.getTotalCost());

			// update the totals to make sure everything is tallied up properly

			this.quotationCalibrationCostService.updateCosts(destCalCost);
			// destCalCost = (QuotationCalibrationCost)
			// CostCalculator.updateThirdPartyCost(destCalCost);
			destCalCost.setTotalCost(destCalCost.getThirdCostTotal().add(destCalCost.getHouseCost()));
			CostCalculator.updateSingleCost(destCalCost, this.roundUpFinalCosts);

			qItem.setCalibrationCost(destCalCost);

			// copy sales costs
			QuotationPurchaseCost srcPurCost = sourceItem.getPurchaseCost();
			QuotationPurchaseCost destPurCost = qItem.getPurchaseCost();

			// sequentially copy the cal cost fields one-by-one
			destPurCost.setActive(srcPurCost.isActive());
			destPurCost.setCustomOrderBy(srcPurCost.getCustomOrderBy());
			destPurCost.setDiscountRate(srcPurCost.getDiscountRate());
			destPurCost.setDiscountValue(srcPurCost.getDiscountValue());
			destPurCost.setFinalCost(srcPurCost.getFinalCost());
			destPurCost.setHourlyRate(srcPurCost.getHourlyRate());
			destPurCost.setHouseCost(srcPurCost.getHouseCost());
			destPurCost.setLinkedCostSrc(srcPurCost.getLinkedCostSrc());
			destPurCost.setThirdCostSrc(srcPurCost.getThirdCostSrc());
			destPurCost.setThirdCostTotal(srcPurCost.getThirdCostTotal());
			destPurCost.setThirdManualPrice(srcPurCost.getThirdManualPrice());
			destPurCost.setThirdMarkupRate(srcPurCost.getThirdMarkupRate());
			destPurCost.setThirdMarkupSrc(srcPurCost.getThirdMarkupSrc());
			destPurCost.setThirdMarkupValue(srcPurCost.getThirdMarkupValue());
			destPurCost.setTotalCost(srcPurCost.getTotalCost());

			destPurCost.setTpCarriageIn(srcPurCost.getTpCarriageIn());
			destPurCost.setTpCarriageInMarkupValue(srcPurCost.getTpCarriageInMarkupValue());
			destPurCost.setTpCarriageMarkupRate(srcPurCost.getTpCarriageMarkupRate());
			destPurCost.setTpCarriageMarkupSrc(srcPurCost.getTpCarriageMarkupSrc());
			destPurCost.setTpCarriageOut(srcPurCost.getTpCarriageOut());
			destPurCost.setTpCarriageOutMarkupValue(srcPurCost.getTpCarriageOutMarkupValue());
			destPurCost.setTpCarriageTotal(srcPurCost.getTpCarriageTotal());

			// update the totals to make sure everything is tallied up properly
			this.quotationPurchaseCostService.updateCosts(destPurCost);
			// destPurCost = (QuotationPurchaseCost)
			// CostCalculator.updateThirdPartyCost(destPurCost);
			destPurCost.setTotalCost(destPurCost.getThirdCostTotal().add(destPurCost.getHouseCost()));
			CostCalculator.updateSingleCost(destPurCost, this.roundUpFinalCosts);

			qItem.setPurchaseCost(destPurCost);

			// copy the third party carriage costs
			qItem.setTpCarriageIn(sourceItem.getTpCarriageIn());
			qItem.setTpCarriageOut(sourceItem.getTpCarriageOut());
			qItem.setTpCarriageMarkupRate(sourceItem.getTpCarriageMarkupRate());
			qItem.setTpCarriageInMarkupValue(sourceItem.getTpCarriageInMarkupValue());
			qItem.setTpCarriageOutMarkupValue(sourceItem.getTpCarriageOutMarkupValue());
			qItem.setTpCarriageMarkupSrc(sourceItem.getTpCarriageMarkupSrc());
			qItem.setTpCarriageTotal(sourceItem.getTpCarriageTotal());

			this.thirdPartyPricingItemService.updateThirdPartyCarriageCosts(qItem);

			// copy the quotation item level costs
			qItem.setGeneralDiscountRate(sourceItem.getGeneralDiscountRate());
			qItem.setGeneralDiscountValue(sourceItem.getGeneralDiscountValue());
			qItem.setTotalCost(sourceItem.getTotalCost());
			qItem.setFinalCost(sourceItem.getFinalCost());

			CostCalculator.updateTPItemCostWithRunningTotal(qItem);

			// update totals
			CostCalculator.updateItemCost(qItem);
		}
	}

	public ResultWrapper copyQuotationItemsToHeadingAndCaltype(String copyIds, int headingId, int calTypeId,
			int subdivId) {
		// first find the heading
		QuoteHeading qhead = this.qhServ.findQuoteHeading(headingId);
		// quote heading null?
		if (qhead != null) {
			long totalItemCount = this.qiDao.getItemCount(qhead.getQuotation().getId());
			// 2016-02-22 - Added check of number of quotation items until
			// performance
			// issues fully resolved
			if (totalItemCount < sizeRestrictCopyItem) {
				// find the calibration type
				CalibrationType ctype = this.caltypeServ.find(calTypeId);
				// calibration type null?
				if (ctype != null) {
					String[] qiStringIds = copyIds.split(",");
					Integer[] qiIds = new Integer[qiStringIds.length];
					int i = 0;
					while (i < qiStringIds.length) {
						qiIds[i] = Integer.parseInt(qiStringIds[i]);
						i++;
					}
					// initialise reference variable and array
					Quotation quotation = null;
					List<Quotationitem> qis = new ArrayList<>();
					// find all quotation items using ids
					for (Integer id : qiIds) {
						// get quote item
						Quotationitem qitem = this.findQuotationItem(id);
						// check item is not an instrument
						if (qitem.getInst() != null) {
							return new ResultWrapper(false,
									"You have selected one or more items that are linked to an instrument", null, null);
						} else {
							// only add item if not a module
							if (qitem.getBaseUnit() == null) {
								qis.add(qitem);
							}
							// quotation is null? get quotation
							if (quotation == null) {
								quotation = qitem.getQuotation();
							}
						}
					}
					// create new quotation items for all items to copy
					for (Quotationitem qi : qis) {
						// create new quotation item
						Quotationitem newItem = this.createTemplateNewQuotationItem(quotation, qhead, qi.getInst(),
								qi.getModel().getModelid(), ctype.getCalTypeId(), subdivId);
						// base unit?
						if (qi.getBaseUnit() == null) {
							// set part of base unit false
							newItem.setPartOfBaseUnit(false);
						}
						// copy costs from original item
						this.copyCosts(qi, Collections.singletonList(newItem));
						// insert new quotation item
						this.insertQuotationItem(newItem);
						// add new quote item to quotation
						quotation.getQuotationitems().add(newItem);
						// update quotation
						quotation = this.quoteServ.merge(quotation);
						// is this a base unit?
						if (qi.getModules().size() > 0) {
							// create new quote item for each module
							for (Quotationitem qim : qi.getModules()) {
								// create new quotation item
								Quotationitem newModItem = this.createTemplateNewQuotationItem(quotation, qhead,
										qim.getInst(), qim.getModel().getModelid(), ctype.getCalTypeId(), subdivId);
								// set base unit
								newModItem.setBaseUnit(newItem);
								newModItem.setPartOfBaseUnit(true);
								// copy costs from original item
								this.copyCosts(qim, Collections.singletonList(newModItem));
								// insert new quotation item
								this.insertQuotationItem(newModItem);
								// add new quote item to quotation
								quotation.getQuotationitems().add(newModItem);
								// update quotation
								this.quoteServ.merge(quotation);
							}
						}
					}
					// now update quotation totals
					assert quotation != null;
					CostCalculator.updatePricingTotalCost(quotation, quotation.getQuotationitems());
					taxCalculator.calculateAndSetVATaxAndFinalCost(quotation);
					// finally update the numbering of the items
					this.sortQuotationItems(quotation);
					// return successful wrapper
					return new ResultWrapper(true, "", null, null);
				} else {
					// return un-successful wrapper
					return new ResultWrapper(false, "The calibration type could not be found", null, null);
				}
			} else {
				return new ResultWrapper(false,
						"This quotation has " + totalItemCount
								+ " items and due to temporary performance restrictions, quotations with "
								+ sizeRestrictCopyItem + " items or more cannot have items copied.");
			}
		} else {
			// return un-successful wrapper
			return new ResultWrapper(false, "The quote heading could not be found", null, null);
		}
	}

	@Override
	public Quotationitem createTemplateNewQuotationItem(Quotation quotation, QuoteHeading heading, Instrument inst,
			Integer modelid, Integer caltypeid, Integer subdivId) {
		Quotationitem item = new Quotationitem();
		item.setQuotation(quotation);
		item.setItemno(1);
		item.setQuantity(1);
		item.setPlantno(null);
		item.setPartOfBaseUnit(false);
		item.setNotes(new TreeSet<>(new NoteComparator()));
		item.setHeading(heading);
		// instrument null?
		if (inst != null) {
			// add instrument to quotation item
			item.setInst(inst);
			item.setModel(null);
		} else {
			// add instrument model to quotation item
			item.setModel(this.modelService.findInstrumentModel(modelid));
			item.setInst(null);
		}
		CalibrationType caltype = caltypeid == null ? this.caltypeServ.getDefaultCalType()
				: this.caltypeServ.find(caltypeid);
		item.setCaltype(caltype);
		if (caltype != null)
			item.setServiceType(caltype.getServiceType());
		// set the default third party costs
		this.thirdPartyPricingItemService.setThirdPartyDefaultValues(item);
		double calcost = 0d;
		if (caltypeid != null && quotation != null) {
			// get correct model
			InstrumentModel instmod;
			// instrument not null?
			if (inst != null) {
				instmod = inst.getModel();
			} else {
				instmod = item.getModel();
			}
			// no calcost set so get the default cost for this model for this
			// quotation's currency
			Subdiv subdiv = divServ.get(subdivId);
			BigDecimal cost = this.modelService.getModelDefaultCalCost(instmod, item.getCaltype(),
					item.getQuotation().getCurrency().getDefaultRate(), subdiv.getComp());
			calcost = cost == null ? 0.00 : cost.doubleValue();
		}

		QuotationCalibrationCost calCost = new QuotationCalibrationCost();
		calCost.setHouseCost(new BigDecimal(calcost, MathTools.mc));

		// third party costs
		calCost.setThirdManualPrice(new BigDecimal("0.00"));
		calCost.setThirdMarkupSrc(ThirdCostMarkupSource.SYSTEM_DEFAULT);
		calCost.setThirdMarkupRate(new BigDecimal("0.00"));
		calCost.setThirdCostSrc(ThirdCostSource.MANUAL);
		calCost.setThirdMarkupValue(new BigDecimal("0.00"));
		calCost.setThirdCostTotal(new BigDecimal("0.00"));

		calCost.setDiscountValue(new BigDecimal("0.00"));
		calCost.setDiscountRate(new BigDecimal("0.00"));

		calCost.setTotalCost(new BigDecimal(calcost).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));
		calCost.setFinalCost(new BigDecimal(calcost).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));
		calCost.setCostType(CostType.CALIBRATION);
		calCost.setActive(true);
		calCost.setQuoteItem(item);
		item.setCalibrationCost(calCost);

		QuotationPurchaseCost salesCost = new QuotationPurchaseCost();
		salesCost.setHouseCost(new BigDecimal("0.00"));
		salesCost.setDiscountRate(new BigDecimal("0.00"));
		salesCost.setDiscountValue(new BigDecimal("0.00"));
		salesCost.setTotalCost(new BigDecimal("0.00"));
		salesCost.setFinalCost(new BigDecimal("0.00"));
		salesCost.setCostType(CostType.PURCHASE);
		salesCost.setActive(false);

		salesCost.setThirdManualPrice(new BigDecimal("0.00"));
		salesCost.setThirdMarkupSrc(ThirdCostMarkupSource.SYSTEM_DEFAULT);
		salesCost.setThirdMarkupRate(new BigDecimal("0.00"));
		salesCost.setThirdCostSrc(ThirdCostSource.MANUAL);
		salesCost.setThirdMarkupValue(new BigDecimal("0.00"));
		salesCost.setThirdCostTotal(new BigDecimal("0.00"));
		salesCost.setThirdMarkupSrc(ThirdCostMarkupSource.SYSTEM_DEFAULT);
		salesCost.setThirdCostSrc(ThirdCostSource.MANUAL);
		salesCost.setQuoteItem(item);
		item.setPurchaseCost(salesCost);

		item.setGeneralDiscountRate(new BigDecimal("0.00"));
		item.setInspection(new BigDecimal("0.00"));

		CostCalculator.updateItemCostWithRunningTotal(item);

		return item;
	}

	@Override
	public QuoteItemNote createTemplateNewQuotationItemNote(Quotationitem item, boolean active, String label,
			String note, boolean publish, Contact contact) {
		QuoteItemNote qin = new QuoteItemNote();
		qin.setActive(active);
		qin.setLabel(label);
		qin.setNote(note);
		qin.setPublish(publish);
		qin.setQuotationitem(item);
		qin.setSetBy(contact);
		qin.setSetOn(new Date());

		// TODO remove and test - likely unnecessary
		if (item.getNotes() == null) {
			item.setNotes(new TreeSet<>(new NoteComparator()));
		}

		return qin;
	}

	/*
	 * Iterates through all existing quotation items on a new (can be unsaved)
	 * and adds: QuoteItemNote notes for catalog price comments QuoteNote notes
	 * for service capability comments
	 */
	@Override
	public void createNotesForNewQuotation(Quotation quotation, Subdiv allocatedSubdiv, Contact currentContact) {
		Locale locale = LocaleContextHolder.getLocale();
		// To prevent duplicate notes
		Map<String, QuoteNote> existingQuoteNotes = new HashMap<>();

		String quoteItemLabel = messageSource.getMessage("quoteautonote.catalogpricenote", null, locale);
		String quoteLabel = messageSource.getMessage("quoteautonote.procedurenote", null, locale);

		// Add catalog price comment as a note for each quotation item and
		// procedure
		// description note where neccessary
		for (Quotationitem qi : quotation.getQuotationitems()) {
			CatalogPrice catalogPrice = this.priceServ.findSingle(qi.getInst().getModel(), qi.getServiceType(), null,
					allocatedSubdiv.getComp());
			if (catalogPrice != null) {
				String comments = catalogPrice.getComments();
				if (comments != null && !comments.isEmpty()) {
					QuoteItemNote note = this.createTemplateNewQuotationItemNote(qi, true, quoteItemLabel, comments,
							true, currentContact);
					qi.getNotes().add(note);
				}
			}
			String serviceComment = this.getServiceCapabilityComment(qi.getInst().getModel(), qi.getCaltype(),
					allocatedSubdiv, locale);
			if (serviceComment != null) {
				QuoteNote note = existingQuoteNotes.get(serviceComment);
				if (note == null) {
					note = this.quoteServ.createTemplateNewQuoteNote(quotation, true, quoteLabel, serviceComment, true,
							currentContact);
					quotation.getNotes().add(note);
					existingQuoteNotes.put(serviceComment, note);
				}
			}
		}
	}

	@Override
	public Set<Quotationitem> deleteByModuleAvailableGetBaseUnits(int quoteid, int[] qiids, int modelid) {
		// now update all modules to show that they are now regular quotation
		// items
		this.setModuleStatus(qiids, false);

		// delete the partof relationships
		this.detachModule(qiids);

		return this.findAvailableBaseUnits(quoteid, modelid);
	}

	@Override
	public void deleteQuotationItem(int qiid) {
		this.qiDao.delete(qiid);
	}

	@Override
	public void deleteQuotationItem(Quotationitem qi) {
		this.qiDao.remove(qi);
	}

	@Override
	public void deleteQuotationItems(int[] qiids) {
		this.qiDao.deleteQuotationItems(qiids);
	}

	/**
	 * Only called within this class as a sub-method of the deleteQuotationItems
	 * method (which is the only deletion method safe to be called from outside
	 * of this class)
	 */
	private void deleteQuoteItem(Quotation quotation, Quotationitem qitem) {
		// remove the module from it's base unit
		if (qitem.isPartOfBaseUnit()) {
			Quotationitem base = qitem.getBaseUnit();

			if ((base.getModules() != null) && (base.getModules().size() > 0)) {
				base.getModules().remove(qitem);
				this.updateQuotationItem(base);
			}
		}

		// explicity remove this item from the Quotation set of QuotationItems
		// (otherwise hibernate chokes)
		quotation.getQuotationitems().remove(qitem);

		// remove the item from the headings so that it doesn't get re-saved
		// following the re-sorting
		for (QuoteHeading heading : quotation.getQuoteheadings()) {
			heading.getQuoteitems().remove(qitem);
		}

		// delete the item
		this.deleteQuotationItem(qitem);
	}

	@Override
	public void detachModule(int[] qiids) {
		this.qiDao.detachModule(qiids);
	}

	@Override
	public Set<Quotationitem> findAvailableBaseUnits(int quoteid, int modelid) {
		return this.qiDao.findAvailableBaseUnits(quoteid, modelid);
	}

	@Override
	public Set<Quotationitem> findAvailableModules(int quoteid, int modelid) {
		return this.qiDao.findAvailableModules(quoteid, modelid);
	}

	@Override
	public List<Quotationitem> findIdenticalModelsOnQuotation(int quoteid, int modelid, Integer excludeQIId,
			QuotationItemSortType sortType) {
		return this.qiDao.findIdenticalModelsOnQuotation(quoteid, modelid, excludeQIId, sortType);
	}

	@Override
	public List<Quotationitem> findNearbyQuotationItems(int headingid, int minItemNo, int maxItemNo) {
		return this.qiDao.findNearbyQuotationItems(headingid, minItemNo, maxItemNo);
	}

	@Override
	public Quotationitem findQuotationItem(int id) {
		return this.qiDao.find(id);
	}

	public Quotationitem findQuotationitemByItemNo(int headingId, int caltypeId, int itemNo) {
		return this.qiDao.findQuotationitemByItemNo(headingId, caltypeId, itemNo);
	}

	@Override
	public List<PriceLookupOutputQuotationItem> findQuotationItems(PriceLookupRequirements requirements,
			PriceLookupQueryType queryType, MultiValuedMap<Integer, Integer> serviceTypeMap) {
		return this.qiDao.findQuotationItems(requirements, queryType, serviceTypeMap);
	}

	@Override
	public PagedResultSet<Quotationitem> findQuotationItems(QuotationItemSearchForm form,
			PagedResultSet<Quotationitem> prs) {
		return this.qiDao.findQuotationItems(form, prs);
	}

	@Override
	public List<Quotationitem> findQuotationItems(List<Integer> qiids) {
		return this.qiDao.findQuotationItems(qiids);
	}

	/**
	 * Find all quotation items for specified instruments, where the
	 * quotation(s) are linked to the job
	 */
	@Override
	public Map<Integer, Set<Quotationitem>> findQuotationItemsForInstruments(List<Instrument> instruments, Job job,
			boolean defaultServiceTypeUsage) {
		// TODO Implement "related" quotationitems (by model, by sales
		// category...)
		// We now restrict quote items by service types for job.
		Map<Integer, Set<Quotationitem>> map = new HashMap<>();
		List<Integer> plantIds = instruments.stream().map(Instrument::getPlantid).collect(Collectors.toList());
		List<Quotationitem> qitems = this.qiDao.findQuotationItemsForPlantIds(plantIds, job.getJobid(),
				defaultServiceTypeUsage);
		for (Quotationitem qitem : qitems) {
			populateMap(map, qitem.getInst().getPlantid(), qitem);
		}
		return map;
	}

	@Override
	public Map<Integer, Map<Integer, String>> findQuotationItemsOptionsForPlantIdsJobIdAndDefaultUsage(
			List<Integer> plantIds, Integer jobId, Boolean defaultServiceTypeUsage) {
		val items = qiDao.findQuotationItemsOptionsForPlantIdsJobIdAndDefaultUsage(plantIds, jobId,
				defaultServiceTypeUsage);
		return items.stream().collect(Collectors.groupingBy(QuotationItemOptionDto::getId,
				Collectors.toMap(QuotationItemOptionDto::getQuotationItemId, QuotationItemOptionDto::getValue)));
	}

	@Override
	public Map<Integer, Map<Integer, String>> findQuotationItemsOptionsForModelIdsJobIdAndDefaultUsage(
			List<Integer> modelIdIds, Integer jobId, Boolean defaultServiceTypeUsage) {
		val items = qiDao.findQuotationItemsOptionsForModelIdsJobIdAndDefaultUsage(modelIdIds, jobId,
				defaultServiceTypeUsage);
		return items.stream().collect(Collectors.groupingBy(QuotationItemOptionDto::getId,
				Collectors.toMap(QuotationItemOptionDto::getQuotationItemId, QuotationItemOptionDto::getValue)));
	}

	@Override
	public Map<Integer, Set<Quotationitem>> findQuotationItemsForModels(List<InstrumentModel> models, Job job,
			boolean defaultServiceTypeUsage) {
		// We now restrict quote items by service types for job.
		Map<Integer, Set<Quotationitem>> map = new HashMap<>();
		List<Integer> modelIds = models.stream().map(InstrumentModel::getModelid).collect(Collectors.toList());
		List<Quotationitem> qitems = this.qiDao.findQuotationItemsForModelIds(modelIds, job.getJobid(),
				defaultServiceTypeUsage);
		for (Quotationitem qitem : qitems) {
			populateMap(map, qitem.getModel().getModelid(), qitem);
		}
		return map;
	}

	@Override
	public List<QuotationItemSummaryDTO> findSummaryDTOs(int quoteid, boolean groupByHeading,
			boolean groupByServiceType) {
		return this.qiDao.findSummaryDTOs(quoteid, groupByHeading, groupByServiceType);
	}

	private void populateMap(Map<Integer, Set<Quotationitem>> map, Integer key, Quotationitem qitem) {
		if (!map.containsKey(key))
			map.put(key, new TreeSet<>(new QuotationItemComparator()));
		map.get(key).add(qitem);
	}

	@Override
	public ResultWrapper getAjaxAlternativeCalibrationCosts(int modelid, int calTypeId, int coid, int businessCoid,
			Integer excludeQuotationId) {
		return new ResultWrapper(true, "",
				this.getAlternativeCalibrationCosts(modelid, calTypeId, coid, businessCoid, excludeQuotationId), null);
	}

	@Override
	public CostLookUp getAlternativeCalibrationCosts(int modelid, int calTypeId, int coid, int businessCoId,
			Integer excludeQuotationId) {
		List<CostSource> costSources = CostSourceSupportService.resolveCostSourceList(this.lookupSources);
		CostLookUp lu = this.calCostServ.lookupAlternativeCosts(new CostLookUp(), null, modelid, calTypeId, coid, null,
				costSources, businessCoId);
		// messy way to exclude a quotation but when i started to add this
		// restriction
		// in the dao layer i ended up with
		// an enormous number of changes in the tangled mess of the different
		// types of
		// cost in the system!
		if (excludeQuotationId != null) {
			if (lu.getLinkedQuotationCosts() != null && lu.getLinkedQuotationCosts().size() > 0) {
				List<QuotationCalibrationCost> linkedQuotationCostsWithQuoteExcluded = lu.getLinkedQuotationCosts()
						.stream().filter(qc -> qc.getQuoteItem().getQuotation().getId() != excludeQuotationId)
						.collect(Collectors.toList());
				lu.setLinkedQuotationCosts(linkedQuotationCostsWithQuoteExcluded);
			}
			if (lu.getUnLinkedQuotationCosts() != null && lu.getUnLinkedQuotationCosts().size() > 0) {
				List<QuotationCalibrationCost> unlinkedQuotationCostsWithQuoteExcluded = lu.getUnLinkedQuotationCosts()
						.stream().filter(qc -> qc.getQuoteItem().getQuotation().getId() != excludeQuotationId)
						.collect(Collectors.toList());
				lu.setUnLinkedQuotationCosts(unlinkedQuotationCostsWithQuoteExcluded);
			}
		}

		return lu;
	}

	@Override
	public QuotationItemCostDTO getItemCostsByInstrument(Integer quotationId, Integer jobItemId,
			Integer serviceTypeId) {
		return qiDao.getItemCostsByInstrument(quotationId, jobItemId, serviceTypeId);
	}

	@Override
	public Set<Quotationitem> getItemsForQuotation(int quoteid, Integer page, Integer resPerPage) {
		return this.qiDao.getItemsForQuotation(quoteid, page, resPerPage);
	}

	@Override
	public List<DWRQuoteItemDTO> getDWRItemsForQuotation(int quoteid) {
		Set<Quotationitem> quoteItems = this.qiDao.getItemsForQuotation(quoteid, 1, Constants.RESULTS_PER_PAGE);
		List<DWRQuoteItemDTO> dtos = new ArrayList<>();
		for (Quotationitem quoteItem : quoteItems) {
			DWRQuoteItemDTO dto = new DWRQuoteItemDTO();
			dto.setCalTypeShortName(quoteItem.getCaltype().getServiceType().getShortName());
			dto.setCalTypeDisplayColor(quoteItem.getCaltype().getServiceType().getDisplayColour());
			dto.setHeadingName(quoteItem.getHeading().getHeadingName());
			dto.setItemNo(quoteItem.getItemno());
			if (quoteItem.getInst() != null) {
				dto.setPlantId(quoteItem.getInst().getPlantid() + " - ");
				dto.setFullModelName(InstModelTools.modelNameViaTranslations(quoteItem.getInst().getModel(),
						LocaleContextHolder.getLocale(), supportedLocaleService.getPrimaryLocale()));
			} else {
				dto.setPlantId("");
				dto.setFullModelName(InstModelTools.modelNameViaTranslations(quoteItem.getModel(),
						LocaleContextHolder.getLocale(), supportedLocaleService.getPrimaryLocale()));
			}
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public DiscountSummaryDTO getGeneralDiscountDTO(int quoteid) {
		return this.qiDao.getGeneralDiscountDTO(quoteid);
	}

	@Override
	public Long getItemCount(int quoteid) {
		return this.qiDao.getItemCount(quoteid);
	}

	@Override
	public Integer getMaxItemno(int quoteid) {
		return this.qiDao.getMaxItemno(quoteid);
	}

	public ResultWrapper getModulesAvailableForBaseUnitDWR(int baseQuoteItemId, int subdivId) {
		// check that this is is valid quote item
		Quotationitem baseQI = this.qiDao.find(baseQuoteItemId);
		// quote item not null?
		if (baseQI != null) {
			// create new list of module wrappers
			List<QuoteItemModuleWrapper> impomList = new ArrayList<>();
			// get model default cost for each module
			for (InstrumentModelPartOf impo : this.impoServ
					.getModulesAvailableForBaseUnit(baseQI.getModel().getModelid())) {
				// create new wrapper
				QuoteItemModuleWrapper qimodwrap = new QuoteItemModuleWrapper();
				qimodwrap.setImpo(impo);
				qimodwrap.setModelCalCost(new BigDecimal("0.00"));
				Subdiv subDiv = divServ.get(subdivId);
				CatalogPrice catalogPrice = priceServ.findSingle(impo.getModule(), baseQI.getServiceType(),
						CostType.CALIBRATION, subDiv.getComp());
				if (catalogPrice != null) {
					qimodwrap.setModelCalCost(catalogPrice.getFixedPrice());
				}
				impomList.add(qimodwrap);
			}
			// return successful wrapper
			return new ResultWrapper(true, "", impomList, null);
		} else {
			// return failure
			return new ResultWrapper(false, "The base unit quote item could not be found", null, null);
		}
	}

	@Override
	public DiscountSummaryDTO getTotalDiscountSummaryDTO(int quoteid) {
		DiscountSummaryDTO result = new DiscountSummaryDTO();
		result.add(getGeneralDiscountDTO(quoteid));
		result.add(this.quotationCalibrationCostService.getDiscountDTO(quoteid));
		result.add(this.quotationPurchaseCostService.getDiscountDTO(quoteid));

		return result;
	}

	@Override
	public void insertQuotationItem(Quotationitem qi) {
		this.qiDao.persist(qi);
	}

	public ResultWrapper moveQuotationItem(int oldPosId, int newPosId, int headingId, int calTypeId) throws Exception {
		// first find the quotation item to be moved
		Quotationitem oldPosQI = this.qiDao.find(oldPosId);
		// then find the quotation item where the new item needs to be moved to
		Quotationitem newPosQI = this.qiDao.find(newPosId);
		// check that these items are not null?
		if ((oldPosQI != null) && (newPosQI != null)) {
			// get item numbers of items
			int oldPos = oldPosQI.getItemno();
			int newPos = newPosQI.getItemno();
			// check that two items with same item number have not been passed?
			if (oldPos != newPos) {
				// set the position initially to 0 so that move other quotation
				// items into
				// empty positions
				oldPosQI.setItemno(0);
				// update quote item
				this.updateQuotationItem(oldPosQI);
				// initialise temp quote item
				Quotationitem temp;
				// the quote item is moving up the list
				if (oldPos > newPos) {
					// get each quote item in between the new position and the
					// old
					// position and
					// move them down one place
					for (int i = oldPos; i != newPos; i--) {
						temp = this.findQuotationitemByItemNo(headingId, calTypeId, (i - 1));
						temp.setItemno(i);
						this.updateQuotationItem(temp);
					}
				}
				// the quote item is moving down the list
				else {
					// get each quote item in between the new position and the
					// old
					// position and
					// move them up one place
					for (int i = oldPos + 1; i != newPos + 1; i++) {
						temp = this.findQuotationitemByItemNo(headingId, calTypeId, i);
						temp.setItemno(i - 1);
						this.updateQuotationItem(temp);
					}
				}

				// set the position of the quote item that has been moved from 0
				// (it's
				// temporary position) to it's proper (new) position because the
				// position will now be vacant
				oldPosQI.setItemno(newPos);
				this.updateQuotationItem(oldPosQI);
				// get the quotation object
				Quotation q = oldPosQI.getQuotation();
				// update the quotation object
				this.quoteServ.merge(q);
				// check that all quote items are unique
				if (!this.quoteServ.isAllItemsOnQuotationUnique(q)) {
					throw new Exception("Sorting the items on quotation (id: " + q.getId() + " qno: " + q.getQno()
							+ ") has resulted in a non unique item (itemno, quoteid, caltypeid, headingid)");
				} else {
					return new ResultWrapper(true, null, null, null);
				}
			} else {
				// return un-successful result wrapper
				return new ResultWrapper(false, "You cannot select the same position for both items", null, null);
			}
		} else {
			// return un-successful result wrapper
			return new ResultWrapper(false, "One of the quotation items could not be found", null, null);
		}
	}

	@Override
	public void saveOrUpdateAll(Collection<Quotationitem> items) {
		this.qiDao.saveOrUpdateAll(items);
	}

	@Override
	public void setModuleStatus(int[] qiids, boolean flag) {
		List<Quotationitem> quoteItems = this.qiDao.findQuotationItems(qiids);

		for (Quotationitem qi : quoteItems) {
			qi.setPartOfBaseUnit(flag);
			this.qiDao.merge(qi);
		}
	}

	@Override
	public Quotation sortQuotationItems(Quotation q) {
		// this is slightly nuts, but we have to set all the itemnos to be much
		// greater then they could ever actually be, then flush to the db.
		// Otherwise if not then when we try to just swop the item numbers
		// around and then save to the db then we'll get SQL
		// exceptions because occasionally a
		// new itemno will be set that is identical to another (that will be
		// updated later on in the batch). If we make the numbers
		// really big and then force a flush and then do
		// the updates then we avoid this problem.

		int count = 1;
		for (QuoteHeading qh : q.getQuoteheadings()) {
			for (Quotationitem qi : qh.getQuoteitems()) {
				qi.setItemno(100000 + count++);
			}
		}

		q.setQuotationitems(new TreeSet<>(q.getSortType().getComparator()));

		for (QuoteHeading qh : q.getQuoteheadings()) {
			q.getQuotationitems().addAll(this.sortQuoteHeadingItems(qh, q));
		}

		// this.quoteServ.merge(q);

		// check that all quote items are unique
		if (!this.quoteServ.isAllItemsOnQuotationUnique(q)) {
			throw new RuntimeException("Sorting the items on quotation (id: " + q.getId() + " qno: " + q.getQno()
					+ ") has resulted in a non unique item (itemno, quoteid, caltypeid, headingid)");
		}

		return q;
	}

	public TreeSet<Quotationitem> sortQuoteHeadingItems(QuoteHeading qh, Quotation q) {
		Set<CalibrationType> calTypes = this.quoteServ.getCaltypesPerQuotation(q.getId());

		TreeSet<Quotationitem> headingItems = new TreeSet<>(q.getSortType().getComparator());

		int itemno = 1;
		int moduleno = 1;
		for (CalibrationType calType : calTypes) {
			// build up a distinct list of quotationitems for this caltype
			// for this heading
			this.logger.info("Sorting with " + q.getSortType().getComparator());

			TreeSet<Quotationitem> sortedItems = new TreeSet<>(q.getSortType().getComparator());

			if (qh.getQuoteitems() != null) {
				for (Quotationitem qi : qh.getQuoteitems()) {
					if (qi.getServiceType().equals(calType.getServiceType())) {
						sortedItems.add(qi);
					}
				}
			}

			if (sortedItems.size() > 0) {

				for (Quotationitem qi : sortedItems) {
					if (qi.isPartOfBaseUnit()) {
						// if this is a module then set the itemno using a
						// separate count and the heading id, the itemno for
						// the module will not be displayed anywhere and this
						// stops it interfering with other item's numbers
						qi.setItemno(moduleno + qh.getHeadingId());
						moduleno++;
					} else {
						qi.setItemno(itemno);
						itemno++;
					}
				}

				headingItems.addAll(sortedItems);
			}
		}

		return headingItems;
	}

	@Override
	public Quotationitem updateItemCosts(Quotationitem quotationitem) {
		this.quotationCalibrationCostService.updateCosts(quotationitem.getCalibrationCost());
		if (quotationitem.getPurchaseCost() != null)
			this.quotationPurchaseCostService.updateCosts(quotationitem.getPurchaseCost());
		return quotationitem;
	}

	@Override
	public void updateQuotationItem(Quotationitem qi) {
		this.qiDao.merge(qi);
	}

	@Override
	public ResultWrapper updateQuotationItemsPriceAndDiscountAndCaltyp(String copyIds, int calTypId, String price,
			BigDecimal discount, int quoteId, String username) {

		long totalItemCount = this.qiDao.getItemCount(quoteId);
		if (totalItemCount < sizeRestrictCopyItem) {

			CalibrationType calTyp = this.caltypeServ.get(calTypId);

			String[] qiStringIds = copyIds.split(",");
			Integer[] qiIds = new Integer[qiStringIds.length];
			int i = 0;
			while (i < qiStringIds.length) {
				qiIds[i] = Integer.parseInt(qiStringIds[i]);
				i++;
			}

			// find all quotation items using ids
			for (Integer id : qiIds) {

				// get quote item
				Quotationitem qitem = this.findQuotationItem(id);
				String note = " ";

				if (discount != null) {
					note += " "
							+ this.messageSource.getMessage("quotationitem.OldDiscoutValue", null,
									LocaleContextHolder.getLocale())
							+ " " + qitem.getCalibrationCost().getDiscountRate() + "\n";
					qitem.getCalibrationCost().setDiscountRate(discount);
				}

				if (price != null && StringUtils.isNoneBlank(price)) {
					if (price.contains("%")) {
						if (price.contains("-")) {

							BigDecimal pricePercentageCalcu = (new BigDecimal(price.replaceAll("%", "")))
									.multiply(qitem.getCalibrationCost().getHouseCost())
									.divide(new BigDecimal(100), MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);

							BigDecimal priceFinal = qitem.getCalibrationCost().getHouseCost()
									.subtract(pricePercentageCalcu)
									.setScale(MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);

							note += " "
									+ this.messageSource.getMessage("quotationitem.Oldprice", null,
											LocaleContextHolder.getLocale())
									+ " " + qitem.getCalibrationCost().getHouseCost() + "\n";

							qitem.getCalibrationCost().setHouseCost(priceFinal);

						} else {

							BigDecimal pricePercentageCal = (new BigDecimal(price.replaceAll("%", "")))
									.multiply(qitem.getCalibrationCost().getHouseCost())
									.divide(new BigDecimal(100), MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);

							BigDecimal priceFinal = qitem.getCalibrationCost().getHouseCost().add(pricePercentageCal)
									.setScale(MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);

							note += " "
									+ this.messageSource.getMessage("quotationitem.Oldprice", null,
											LocaleContextHolder.getLocale())
									+ " " + qitem.getCalibrationCost().getHouseCost() + "\n";

							qitem.getCalibrationCost().setHouseCost(priceFinal);
						}
					} else {

						note += " "
								+ this.messageSource.getMessage("quotationitem.Oldprice", null,
										LocaleContextHolder.getLocale())
								+ " " + qitem.getCalibrationCost().getHouseCost() + "\n";

						qitem.getCalibrationCost().setHouseCost(new BigDecimal(price));
					}
				}

				if (calTyp != null) {

					note += " "
							+ this.messageSource.getMessage("quotationitem.OldeServiceType", null,
									LocaleContextHolder.getLocale())
							+ " " + qitem.getServiceType().getShortName() + "\n";
					qitem.setCaltype(calTyp);
					qitem.setServiceType(calTyp.getServiceType());
					this.updateQuotationItem(qitem);
				}

				if (StringUtils.isNotEmpty(note) ){
					this.noteService.createHistoryForQuotationItem(NoteType.QUOTEITEMNOTE, id, username, "Quotation Items ",
							note);
				}
				
				this.updateItemCosts(qitem);
				CostCalculator.updateItemCostWithRunningTotal(qitem);
				Quotation quote = this.quoteServ.get(quoteId);
				CostCalculator.updatePricingTotalCost(quote, quote.getQuotationitems());
				taxCalculator.calculateAndSetVATaxAndFinalCost(quote);
			}

			return new ResultWrapper(true, null, null, null);
		}

		else
			return new ResultWrapper(false, null, null, null);
	}

	@Override
	public Quotationitem editQuotationItem(Quotationitem qi, EditQuotationItemForm form, String username) {
		Quotation q = qi.getQuotation();

		for (QuoteHeading qhead : q.getQuoteheadings()) {
			Set<Quotationitem> newSortedSet = new TreeSet<>(q.getSortType().getComparator());
			newSortedSet.addAll(qhead.getQuoteitems());
			qhead.setQuoteitems(newSortedSet);
		}

		String note = "";

		boolean requiresResorting = false;
		boolean requiresNoteTidying = false;
		if ((form.getServiceTypeId() != null) && (form.getServiceTypeId() != qi.getServiceType().getServiceTypeId())) {
			ServiceType serviceType = serviceTypeService.get(form.getServiceTypeId());

			note += " " + this.messageSource.getMessage("quotationitem.OldeServiceType", null,
					LocaleContextHolder.getLocale()) + " " + qi.getServiceType().getShortName() + "\n";

			qi.setServiceType(serviceType);
			qi.setCaltype(serviceType.getCalibrationType());
			// force item no back to 0 - stops non-unique errors on the first
			// save. All numbering is then updated by a forced sort further down
			qi.setItemno(0);
			requiresResorting = true;
			requiresNoteTidying = true;
		}

		if ((form.getHeadingId() != null) && (form.getHeadingId() != qi.getHeading().getHeadingId())) {
			int oldHeadId = qi.getHeading().getHeadingId();
			int formHeadId = form.getHeadingId();
			for (QuoteHeading qhead : q.getQuoteheadings()) {
				if (qhead.getHeadingId() == formHeadId) {
					// force item no back to 0 - stops non-unique errors on the
					// first
					// save. All numbering is then updated by a forced sort
					// further down
					qi.setItemno(0);

					note += " " + this.messageSource.getMessage("quotationitem.OldQuoteHeading", null,
							LocaleContextHolder.getLocale()) + " " + qi.getHeading() + "\n";

					// set heading onto quote item
					qi.setHeading(qhead);
					// set quote item into quote heading items
					qhead.getQuoteitems().add(qi);
					// update the quote heading
					this.qhServ.update(qhead);
				} else if (qhead.getHeadingId() == oldHeadId) {
					qhead.getQuoteitems().remove(qi);
				}
			}

			requiresResorting = true;
		}
		// plant number of quote item has been updated?
		if (((form.getPlantNo() != null) && !form.getPlantNo().equalsIgnoreCase(""))
				&& (!Objects.equals(form.getPlantNo(), qi.getPlantno()))) {

			note += " "
					+ this.messageSource.getMessage("quotationitem.OldeplantNo", null, LocaleContextHolder.getLocale())
					+ " " + qi.getPlantno() + "\n";

			qi.setPlantno(form.getPlantNo());
			// force item no back to 0 - stops non-unique errors on the first
			// save. All numbering is then updated by a forced sort further down
			qi.setItemno(0);
			requiresResorting = true;
		}

		// validate the costs input on the quotation
		this.updateQuotationItemCosts(qi, form.getCalibrationCostId(), form.getPurchaseCostId());

		// update the quotation item
		this.updateQuotationItem(qi);
		if (StringUtils.isNotEmpty(note) ){
			this.noteService.createHistoryForQuotationItem(NoteType.QUOTEITEMNOTE, qi.getId(), username, "Quotation Items ",
					note);
		}
		
		// requires a re-load of the items here - this is a nasty hack put in to
		// prevent errors where the quote was coming back with only 1 item (most
		// likely a comparator issue with fields lazily initialised!)
		Set<Quotationitem> theItems = this.getItemsForQuotation(q.getId(), null, null);
		q.setQuotationitems(theItems);

		// copy over any requested costs
		if ((form.getCopyIds() != null) && (form.getCopyIds().size() > 0)) {
			this.copyCosts(qi, this.findQuotationItems(form.getCopyIds()));
		}

		if (requiresNoteTidying) {
			User user = this.userService.get(username);
			Subdiv subdiv = this.divServ.get(form.getAllocatedSubdivId());
			this.tidyQuoteNotes(q, user.getCon(), subdiv);
		}

		// either the caltype or the heading was updated so we need to resort
		// the items on this quotation
		if (requiresResorting) {
			q = this.sortQuotationItems(q);
		} else {
			this.quoteServ.merge(q);
		}
		// requires a re-load of the items here - this is a nasty hack put in to
		// prevent errors where the quote was coming back with only 1 item (most
		// likely a comparator issue with fields lazily initialised!)

		theItems = this.getItemsForQuotation(q.getId(), null, null);
		q.setQuotationitems(theItems);
		this.quoteServ.updateQuotationCosts(q);
		return qi;
	}

	private void updateQuotationItemCosts(Quotationitem qi, Integer calCostId, Integer salesCostId) {
		QuotationCalibrationCost calCost = qi.getCalibrationCost();
		if (calCostId != null) {
			TPQuotationCalibrationCost tpqcc = (TPQuotationCalibrationCost) this.calCostServ.findCalCost(calCostId);
			if (qi.getQuotation().getCurrency().getCurrencyId() != tpqcc.getTpQuoteItem().getTpquotation().getCurrency()
					.getCurrencyId()) {
				logger.debug("DIFFERENT CURRENCIES!!");
			}
			calCost.setLinkedCostSrc(tpqcc);
		} else {
			calCost.setLinkedCostSrc(null);
		}

		QuotationPurchaseCost salesCost = qi.getPurchaseCost();
		if (salesCost != null)
			salesCost.setLinkedCostSrc(salesCostId == null ? null
					: (TPQuotationPurchaseCost) this.purCostServ.findPurchaseCost(salesCostId));

		this.updateItemCosts(qi);

		CostCalculator.updateItemCostWithRunningTotal(qi);
	}
}
