package org.trescal.cwms.core.quotation.form;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class RecallToQuotationValidator implements Validator
{
	protected final Log logger = LogFactory.getLog(this.getClass());

	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(RecallToQuotationForm.class);
	}

	public void validate(Object obj, Errors errors)
	{
		RecallToQuotationForm rtoqData = (RecallToQuotationForm) obj;

		if (rtoqData == null)
		{
			errors.reject("error.nullpointer", "Null data received");
		}
		else
		{
			if ((rtoqData.getToQuotePlantIds() == null)
					|| (rtoqData.getToQuotePlantIds().size() == 0))
			{
				errors.rejectValue("toQuotePlantIds", null, "Please select at least one recall item to quote");
			}

			if ((rtoqData.getClientRef() == null)
					|| (rtoqData.getClientRef().length() > 100))
			{
				errors.rejectValue("clientRef", null, "Client reference should be no more than 100 characters");
			}
		}
	}
}
