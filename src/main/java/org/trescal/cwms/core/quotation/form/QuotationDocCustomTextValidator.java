package org.trescal.cwms.core.quotation.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class QuotationDocCustomTextValidator extends AbstractBeanValidator {

	@Override
	public boolean supports(Class<?> clazz) {
		return QuotationDocCustomTextForm.class.isAssignableFrom(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		QuotationDocCustomTextForm form = (QuotationDocCustomTextForm) target;
		if (form.getUseCustomQuoteText()) {
			// Validate fields required for custom text creation
			super.validate(target, errors, QuotationDocCustomTextForm.UseCustomTextGroup.class);
		}
	}

}
