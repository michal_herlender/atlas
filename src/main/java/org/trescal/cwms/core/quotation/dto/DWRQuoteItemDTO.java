package org.trescal.cwms.core.quotation.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by scottchamberlain on 12/04/2016.
 */
@Getter @Setter
public class DWRQuoteItemDTO {

    Integer itemNo;
    String headingName;
    String plantId;
    String calTypeDisplayColor;
    String calTypeShortName;
    String fullModelName;

}
