package org.trescal.cwms.core.quotation.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class NewQuotationSearchValidator extends AbstractBeanValidator {
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(NewQuotationSearchForm.class);
	}
	
	@Override
	public void validate(Object obj, Errors errors) {
		super.validate(obj, errors);
		NewQuotationSearchForm form = (NewQuotationSearchForm) obj;
		/*
		 * BUSINESS VALIDATION
		 */
		if (form.isIssueDateBetween()) {
			// if first date is given
			if (form.getIssueDate1() != null) {
				// check second date is given
				if (form.getIssueDate2() == null) {
					errors.rejectValue("issueDate2", "issueDate2", null, "Second issue date has not been specified.");
				}
				else
				{
					// check second date is chronologically after the first date
					if (!form.getIssueDate1().isAfter(form.getIssueDate2())) {
						errors.rejectValue("issueDate2", "issueDate2", null, "Second issue date must be after the first issue date.");
					}
				}
			}
		}
		
		if(form.isExpiryDateBetween()){
			
			if (form.getExpiryDate1() != null)
			{
				// check second date is given
				if (form.getExpiryDate2() == null)
				{
					errors.rejectValue("expiryDate2", "expiryDate2", null, "Second expiry date has not been specified.");
				}
				else {
					// check second date is chronologically after the first date
					if (!form.getExpiryDate2().isAfter(form.getExpiryDate1())) {
						errors.rejectValue("expiryDate2", "expiryDate2", null, "Second expiry date must be after the first expiry date.");
					}
				}
			}
			
			
		}
	}
}