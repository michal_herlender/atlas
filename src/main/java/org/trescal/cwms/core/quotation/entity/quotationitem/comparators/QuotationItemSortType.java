package org.trescal.cwms.core.quotation.entity.quotationitem.comparators;

import java.util.Comparator;
import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;

public enum QuotationItemSortType
{
	DESC("quoteitemsorttype.description", "Description", new QuotationItemDescComparator()), 
	ID("quoteitemsorttype.asadded", "As Added", new QuotationItemIdComparator()), 
	MODEL("quoteitemsorttype.mfr", "Manufacturer", new QuotationItemModelComparator()), 
	REFERENCENO("quoteitemsorttype.refno", "Reference No", new QuotationItemReferenceNoComparator()), 
	CUSTOM("quoteitemsorttype.custom", "Custom", new QuotationItemCustomComparator()), 
	BARCODE("quoteitemsorttype.barcode", "Barcode", new QuotationItemBarcodeComparator());

	private ReloadableResourceBundleMessageSource messages;
	private Locale loc = null;
	private String messageCode;
	private Comparator<Quotationitem> comparator;
	private String displayName;

	@Component
	public static class QuotationHeadingSortTypeMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (QuotationItemSortType qist : EnumSet.allOf(QuotationItemSortType.class))
            	qist.setMessageSource(messages);
        }
	}
	
	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}
	
	QuotationItemSortType(String messageCode, String displayName, Comparator<Quotationitem> comparator)
	{
		this.messageCode = messageCode;
		this.displayName = displayName;
		this.comparator = comparator;
	}

	public Comparator<Quotationitem> getComparator()
	{
		return this.comparator;
	}

	public String getDisplayName()
	{
		loc = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.displayName, loc);
		}
		return this.toString();
	}

	public void setComparator(Comparator<Quotationitem> comparator)
	{
		this.comparator = comparator;
	}

	/*public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}*/
}
