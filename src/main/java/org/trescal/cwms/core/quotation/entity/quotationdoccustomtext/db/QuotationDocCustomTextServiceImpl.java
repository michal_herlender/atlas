package org.trescal.cwms.core.quotation.entity.quotationdoccustomtext.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationdoccustomtext.QuotationDocCustomText;
import org.trescal.cwms.core.quotation.form.QuotationDocCustomTextForm;

@Service
public class QuotationDocCustomTextServiceImpl 
	extends BaseServiceImpl<QuotationDocCustomText, Integer> 
	implements QuotationDocCustomTextService
{
	@Autowired
	private QuotationDocCustomTextDao quoteDCTDao;
	@Autowired
	private QuotationService quotationService; 

	@Override
	protected BaseDao<QuotationDocCustomText, Integer> getBaseDao() {
		return quoteDCTDao;
	}
	
	@Override
	public void editCustomText(QuotationDocCustomTextForm form) {
		// Find existing custom text, if any
		QuotationDocCustomText customText = this.get(form.getQuotationId());
		// do we want to use custom text?
		if (form.getUseCustomQuoteText())
		{
			// does custom text already exist?
			if (customText != null)
			{
				customText.setBody(form.getBody());
				customText.setSubject(form.getSubject());
			}
			else
			{
				Quotation quotation = this.quotationService.get(form.getQuotationId());
				// create new quote document custom text
				customText = new QuotationDocCustomText();
				customText.setBody(form.getBody());
				customText.setSubject(form.getSubject());
				customText.setQuotation(quotation);
				this.save(customText);
			}
		}
		else
		{
			// is there already custom text for this quote?
			if (customText != null)
			{
				this.delete(customText);
			}
		}
	}
}
