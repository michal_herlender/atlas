package org.trescal.cwms.core.quotation.form;

import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest;
import org.trescal.cwms.core.tools.FormFunction;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class QuotationRequestForm
{
	private QuotationRequest request;
	private FormFunction formFunction;
	private Integer coid;
	private Integer personid;

}
