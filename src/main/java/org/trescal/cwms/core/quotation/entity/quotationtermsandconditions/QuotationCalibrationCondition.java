package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions;

public interface QuotationCalibrationCondition {
	
	int getDefCalConId();
	
	String getConditionText();
	
	void setDefCalConId(int defCalConId);
	
	void setConditionText(String conditionText);
}