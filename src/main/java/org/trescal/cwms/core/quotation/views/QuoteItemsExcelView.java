package org.trescal.cwms.core.quotation.views;

import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsxView;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.TranslationService;

@Component
public class QuoteItemsExcelView extends AbstractXlsxView {


	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, @NotNull HttpServletRequest request, @NotNull HttpServletResponse response) throws Exception {
		@SuppressWarnings("unchecked")
		Set<Quotationitem> items = (Set<Quotationitem>) model.get("items");

		TranslationService tranServ = (TranslationService) Objects.requireNonNull(getApplicationContext()).getBean("translationService");


		Locale userLocale = LocaleContextHolder.getLocale();


		// create a new Excel sheet
		Sheet sheet = workbook.createSheet("Quotation Items");
		sheet.setDefaultColumnWidth(30);

		// create style for header cells
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setFontName("Arial");
		style.setFillForegroundColor(HSSFColor.GREY_50_PERCENT.index);
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		font.setColor(HSSFColor.WHITE.index);
		style.setFont(font);

		// create header row
		Row header = sheet.createRow(0);

		header.createCell(0).setCellValue("Barcode");
		header.getCell(0).setCellStyle(style);

		header.createCell(1).setCellValue("Plant Number");
		header.getCell(1).setCellStyle(style);

		header.createCell(2).setCellValue("Make");
		header.getCell(2).setCellStyle(style);

		header.createCell(3).setCellValue("Model");
		header.getCell(3).setCellStyle(style);

		header.createCell(4).setCellValue("Description");
		header.getCell(4).setCellStyle(style);

		header.createCell(5).setCellValue("Serial Number");
		header.getCell(5).setCellStyle(style);

		header.createCell(6).setCellValue("Service Type");
		header.getCell(6).setCellStyle(style);
		
		header.createCell(7).setCellValue("Cal Price");
		header.getCell(7).setCellStyle(style);
		
		header.createCell(8).setCellValue("Quantity");
		header.getCell(8).setCellStyle(style);
		
		header.createCell(9).setCellValue("Discount");
		header.getCell(9).setCellStyle(style);

		header.createCell(10).setCellValue("Total Price");
		header.getCell(10).setCellStyle(style);

		// create data rows
		int rowCount = 1;

		for (Quotationitem anItem : items) {
			if (rowCount == 1) {
				String quote = anItem.getQuotation().getIdentifier();
				response.setHeader("Content-Disposition", "attachment; filename=\"Quotation_" + quote + ".xlsx\"");
			}
			Row aRow = sheet.createRow(rowCount++);
			if (anItem.getInst() != null) {
				aRow.createCell(0).setCellValue(anItem.getInst().getPlantid());
				aRow.createCell(1).setCellValue(anItem.getInst().getPlantno());
				aRow.createCell(2).setCellValue(anItem.getInst().getModel().getMfr().getName());
				aRow.createCell(3).setCellValue(anItem.getInst().getModel().getModel());
				Set<Translation> translations = anItem.getInst().getModel().getDescription().getTranslations();
				aRow.createCell(4).setCellValue(tranServ.getCorrectTranslation(translations, userLocale));
				aRow.createCell(5).setCellValue(anItem.getInst().getSerialno());
			} else if (anItem.getModel() != null) {
				aRow.createCell(2).setCellValue(anItem.getModel().getMfr().getName());
				aRow.createCell(3).setCellValue(anItem.getModel().getModel());
				aRow.createCell(4).setCellValue(tranServ.getCorrectTranslation(anItem.getModel().getDescription().getTranslations(), userLocale));
			}
			aRow.createCell(6).setCellValue(tranServ.getCorrectTranslation(anItem.getServiceType().getShortnameTranslation(), userLocale));
			aRow.createCell(7).setCellValue(anItem.getPriorDiscountCalPrice().doubleValue());
			aRow.createCell(8).setCellValue(anItem.getQuantity());
			aRow.createCell(9).setCellValue(anItem.getCalibrationCost().getDiscountValue().doubleValue());
			aRow.createCell(10).setCellValue(anItem.getFinalCost().doubleValue());
		}
		
	}
}
