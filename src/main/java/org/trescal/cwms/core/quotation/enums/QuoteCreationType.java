package org.trescal.cwms.core.quotation.enums;

public enum QuoteCreationType
{
	EMPTY, FROM_COMPINSTLIST, FROM_JOB, FROM_RECALL;
}