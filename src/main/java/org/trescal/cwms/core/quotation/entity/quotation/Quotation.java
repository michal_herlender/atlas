package org.trescal.cwms.core.quotation.entity.quotation;

import lombok.Setter;
import lombok.val;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.jobquotelink.JobQuoteLink;
import org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing;
import org.trescal.cwms.core.quotation.entity.QuoteRequestType;
import org.trescal.cwms.core.quotation.entity.QuoteRequested;
import org.trescal.cwms.core.quotation.entity.priorquotestatus.PriorQuoteStatus;
import org.trescal.cwms.core.quotation.entity.priorquotestatus.PriorQuoteStatusComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.comparators.QuotationItemSortType;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.calibration.CustomCalibrationCondition;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.general.CustomGeneralCondition;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general.DefaultGeneralCondition;
import org.trescal.cwms.core.quotation.entity.quotecaldefaults.QuoteCalDefaults;
import org.trescal.cwms.core.quotation.entity.quotecaltypedefault.QuoteCaltypeDefault;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeadingComparator;
import org.trescal.cwms.core.quotation.entity.quoteheading.comparators.QuotationHeadingSortType;
import org.trescal.cwms.core.quotation.entity.quotenote.QuoteNote;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URL;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Setter
@Table(name = "quotation", uniqueConstraints = {@UniqueConstraint(columnNames = {"qno", "ver"})})
public class Quotation extends Pricing<Company> implements NoteAwareEntity, ComponentEntity, QuoteRequested {
	private boolean accepted;
	private Contact acceptedBy;
	private LocalDate acceptedOn;
	private LocalDate clientAcceptedOn;
	private Set<CustomCalibrationCondition> calConditions;
	private boolean defaultAddModules;
	private Set<QuoteCalDefaults> defaultcalterms;
	private Set<QuoteCaltypeDefault> defaultCaltypes;
	private int defaultQty;
	private Contact defaultSetBy;
	private Date defaultSetOn;
	private DefaultGeneralCondition defaultterms;
	private File directory;
	private LocalDate duedate;
	private LocalDate contractStartDate;
	private CustomGeneralCondition generalConditions;
	private boolean headingCaltypeTotalVisible;
	private QuotationHeadingSortType headingSortType;
	private boolean headingTotalVisible;
	private Set<JobQuoteLink> linkedJobs;
	private Set<QuoteNote> notes;
	private Set<PriorQuoteStatus> priorStatusList;
	private String qno;
	private Set<Quotationitem> quotationitems;
	private Set<QuotationRequest> quotationRequests;
	private boolean quoteCaltypeTotalVisible;
	private Set<QuoteHeading> quoteheadings;
	private QuoteStatus quotestatus;
	private List<Email> sentEmails;
	private boolean showHeadingCaltypeTotal;
	private boolean showHeadingTotal;
	private boolean showQuoteCaltypeTotal;
	private boolean showTotalQuoteValue;
	private QuotationItemSortType sortType;
	private Contact sourcedBy;
	private Set<TPQuoteRequest> tpQuoteRequests;
	private boolean usingDefaultTermsAndConditions;
	private int ver;
	private BigDecimal contractInvoiceThreshold;
	private boolean showDiscounts;
	private Address sourceAddress;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "acceptedby")
	public Contact getAcceptedBy() {
		return this.acceptedBy;
	}

	@Column(name = "acceptedon", columnDefinition = "date")
	public LocalDate getAcceptedOn() {
		return this.acceptedOn;
	}

	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "quotation")
	public Set<CustomCalibrationCondition> getCalConditions() {
		return this.calConditions;
	}

	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "quotation")
	public Set<QuoteCalDefaults> getDefaultcalterms()
	{
		return this.defaultcalterms;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "quotation", orphanRemoval=true)
	public Set<QuoteCaltypeDefault> getDefaultCaltypes()
	{
		return this.defaultCaltypes;
	}

	@Override
	@Transient
	public Contact getDefaultContact() {
		return this.contact;
	}

	@Column(name = "defaultqty", nullable = false)
	public int getDefaultQty() {
		return this.defaultQty;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "defaultsetby")
	public Contact getDefaultSetBy() {
		return this.defaultSetBy;
	}

	@NotNull
	@Column(name = "defaultseton", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDefaultSetOn() {
		return this.defaultSetOn;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "defaulttermsid")
	public DefaultGeneralCondition getDefaultterms() {
		return this.defaultterms;
	}

	@Transient
	public File getDirectory() {
		return this.directory;
	}

	@Column(name = "duedate", nullable = false, columnDefinition = "date")
	public LocalDate getDuedate() {
		return this.duedate;
	}

	@Transient
	public String getFilePath() {
		return this.directory == null ? "" : this.directory.toString();
	}

	@Transient
	public URI getFileURI()
	{
		return this.directory == null ? null : this.directory.toURI();
	}

	@Transient
	public URL getFileURL()
	{
		URL url = null;
		try {
			url = this.directory.toURI().toURL();
		} catch (Exception ignored) {

		}
		return url;
	}

	@OneToOne(mappedBy = "quotation")
	@Cascade(org.hibernate.annotations.CascadeType.ALL)
	public CustomGeneralCondition getGeneralConditions()
	{
		return this.generalConditions;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "headingsorttype", nullable = false)
	public QuotationHeadingSortType getHeadingSortType()
	{
		return this.headingSortType;
	}

	@Override
	@Transient
	public String getIdentifier()
	{
		return this.qno + Constants.VERSION_TEXT + this.ver;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "quotation")
	public Set<JobQuoteLink> getLinkedJobs()
	{
		return this.linkedJobs;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "quotation")
	@SortComparator(NoteComparator.class)
	public Set<QuoteNote> getNotes()
	{
		return this.notes;
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity()
	{
		return null;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "quotation")
	@SortComparator(PriorQuoteStatusComparator.class)
	public Set<PriorQuoteStatus> getPriorStatusList()
	{
		return this.priorStatusList;
	}

	@Transient
	public Integer getPrivateActiveNoteCount()
	{
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount()
	{
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount()
	{
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount()
	{
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}
	
	@NotNull
	@Column(name = "qno", nullable = false, length = 30)
	public String getQno()
	{
		return this.qno;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "quotation")
//	2019-07-17 removed comparator (unnecessary) as it does not deal well with entities without IDs 
//	@SortComparator(QuotationItemComparator.class)
	public Set<Quotationitem> getQuotationitems()
	{
		return this.quotationitems;
	}

	@OneToMany(mappedBy = "quotation", cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	public Set<QuotationRequest> getQuotationRequests()
	{
		return this.quotationRequests;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "quotation")
	@SortComparator(QuoteHeadingComparator.class)
	public Set<QuoteHeading> getQuoteheadings()
	{
		return this.quoteheadings;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid", nullable = false)
	public QuoteStatus getQuotestatus()
	{
		return this.quotestatus;
	}

	@Transient
	public QuoteRequestType getQuoteType()
	{
		return QuoteRequestType.CALIBRATION;
	}

	@Override
	@Transient
	public List<Email> getSentEmails()
	{
		return this.sentEmails;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "sorttype", nullable = false)
	public QuotationItemSortType getSortType()
	{
		return this.sortType;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sourcedby", nullable = false)
	public Contact getSourcedBy()
	{
		return this.sourcedBy;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "requestFromQuote")
	@SortComparator(TPQuoteRequest.TPQuoteRequestComparator.class)
	public Set<TPQuoteRequest> getTpQuoteRequests()
	{
		return this.tpQuoteRequests;
	}

	@Column(name = "ver", nullable = false)
	@Type(type = "int")
	public int getVer()
	{
		return this.ver;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sourceaddressid", nullable = false, foreignKey = @ForeignKey(name = "FK_quotation_sourceaddressid"))
	public Address getSourceAddress() {
		return sourceAddress;
	}

	@Column(name = "accepted", nullable = false, columnDefinition="tinyint")
	public boolean isAccepted()
	{
		return this.accepted;
	}

	@Override
	@Transient
	public boolean isAccountsEmail()
	{
		return false;
	}

	@Column(name = "defaultaddmodules", nullable = false, columnDefinition="tinyint")
	public boolean isDefaultAddModules()
	{
		return this.defaultAddModules;
	}

	@Transient
	public boolean isExpired() {
		// has quote been issued?
		if (this.isIssued())
			// is expiry date before current date
			return !this.isValid();
		else {
			// find out date quotation will expire
			val exipiryDate = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())
				.plusDays(getDuration());
			// is expiry date is before current date
			return this.getReqdate().isBefore(exipiryDate);
		}
	}

	@Column(name = "total_headingcaltype", nullable = false, columnDefinition="tinyint")
	public boolean isHeadingCaltypeTotalVisible()
	{
		return this.headingCaltypeTotalVisible;
	}

	@Column(name = "total_heading", nullable = false, columnDefinition="tinyint")
	public boolean isHeadingTotalVisible()
	{
		return this.headingTotalVisible;
	}

	@Column(name = "total_quotecaltype", nullable = false, columnDefinition="tinyint")
	public boolean isQuoteCaltypeTotalVisible()
	{
		return this.quoteCaltypeTotalVisible;
	}

	@Column(name = "doc_total_headingcaltype", nullable = false, columnDefinition="tinyint")
	public boolean isShowHeadingCaltypeTotal()
	{
		return this.showHeadingCaltypeTotal;
	}

	@Column(name = "doc_total_heading", nullable = false, columnDefinition="tinyint")
	public boolean isShowHeadingTotal()
	{
		return this.showHeadingTotal;
	}

	@Column(name = "doc_total_quotecaltype", nullable = false, columnDefinition="tinyint")
	public boolean isShowQuoteCaltypeTotal()
	{
		return this.showQuoteCaltypeTotal;
	}

	@Column(name = "showtotalquotevalue", nullable = false, columnDefinition="tinyint")
	public boolean isShowTotalQuoteValue()
	{
		return this.showTotalQuoteValue;
	}

	@Column(name = "usingdefaulttermsandconditions", nullable = false, columnDefinition="tinyint")
	public boolean isUsingDefaultTermsAndConditions() {
		return this.usingDefaultTermsAndConditions;
	}

	@Column(name = "doc_discounts", nullable = false, columnDefinition = "tinyint default 0")
	public boolean isShowDiscounts() {
		return this.showDiscounts;
	}

	@Column(name = "clientacceptedon")
	public LocalDate getClientAcceptedOn() {
		return clientAcceptedOn;
	}

	@Column(name = "contractstartdate", columnDefinition = "date")
	public LocalDate getContractStartDate() {
		return contractStartDate;
	}

	@Column(name = "contractinvoicethreshold")
	public BigDecimal getContractInvoiceThreshold() {
		return contractInvoiceThreshold;
	}

	@Override
	@Transient
	public boolean isQuote() {
		return true;
	}
}
