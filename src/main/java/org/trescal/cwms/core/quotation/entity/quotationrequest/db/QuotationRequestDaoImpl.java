package org.trescal.cwms.core.quotation.entity.quotationrequest.db;

import java.util.Calendar;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.quotation.dto.QuotationRequestProjectionDTO;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequestStatus;
import org.trescal.cwms.core.quotation.entity.quotationrequest.QuotationRequest_;

@Repository("QuotationRequestDao")
public class QuotationRequestDaoImpl extends AllocatedToCompanyDaoImpl<QuotationRequest, Integer> implements QuotationRequestDao
{
	@Override
	protected Class<QuotationRequest> getEntity() {
		return QuotationRequest.class;
	}
	
	@Override
	public List<QuotationRequestProjectionDTO> getQuotationRequestByStatusNew(QuotationRequestStatus status, Integer allocatedSubdivId)
	{
		return getResultList(cb -> {
			
			CriteriaQuery<QuotationRequestProjectionDTO> cq = cb.createQuery(QuotationRequestProjectionDTO.class);
			Root<QuotationRequest> quote = cq.from(QuotationRequest.class);
			Join<QuotationRequest, Company> companyJoin = quote.join("organisation");
			Join<Company, Subdiv> subdivJoin = companyJoin.join(Company_.subdivisions);
			
			Join<Quotation, Contact> conContactJoin = quote.join(QuotationRequest_.con.getName(), javax.persistence.criteria.JoinType.LEFT);
			Join<Contact, Subdiv> conSubdivJoin = conContactJoin.join(Contact_.sub, javax.persistence.criteria.JoinType.LEFT);
			Join<Subdiv, Company> conCompanyJoin = conSubdivJoin.join(Subdiv_.comp, javax.persistence.criteria.JoinType.LEFT);
			
			Join<Quotation, Contact> loggedContactJoin = quote.join(QuotationRequest_.loggedBy.getName(), javax.persistence.criteria.JoinType.LEFT);
			Join<Contact, Subdiv> loggedSubdivJoin = loggedContactJoin.join(Contact_.sub.getName(), javax.persistence.criteria.JoinType.LEFT);
			Join<Subdiv, Company> loggedCompanyJoin = loggedSubdivJoin.join(Subdiv_.comp.getName(), javax.persistence.criteria.JoinType.LEFT);
			
			Join<QuotationRequest, Company> quoteRequestCompanyJoin = quote.join(QuotationRequest_.comp.getName(), javax.persistence.criteria.JoinType.LEFT);
			
			Predicate and = cb.and(cb.equal(quote.get(QuotationRequest_.status.getName()), status), 
					cb.equal(subdivJoin.get(Subdiv_.subdivid.getName()), allocatedSubdivId));
			Predicate or = cb.or(cb.isNotNull(quote.get(QuotationRequest_.duedate.getName())), cb.isNull(quote.get(QuotationRequest_.duedate.getName())));
			cq.where(cb.and(and, or));
			Calendar maxDate = Calendar.getInstance();
			maxDate.add(Calendar.YEAR, 1);
			cq.orderBy(cb.asc(cb.coalesce(quote.get(QuotationRequest_.duedate), maxDate.getTime())), cb.asc(quote.get(QuotationRequest_.reqdate.getName())));
			
			cq.select(cb.construct(QuotationRequestProjectionDTO.class, quote.get(QuotationRequest_.id),
					quote.get(QuotationRequest_.reqdate), quote.get(QuotationRequest_.duedate),quote.get(QuotationRequest_.requestNo),
					quote.get(QuotationRequest_.detailsSource), quote.get(QuotationRequest_.company),
					quote.get(QuotationRequest_.contact), quote.get(QuotationRequest_.logSource),
					quote.get(QuotationRequest_.quoteType), 
					
					loggedContactJoin.get(Contact_.personid), loggedContactJoin.get(Contact_.firstName),
					loggedContactJoin.get(Contact_.lastName), loggedContactJoin.get(Contact_.email),
					loggedContactJoin.get(Contact_.telephone), loggedContactJoin.get(Contact_.active),
					loggedSubdivJoin.get(Subdiv_.subdivid), loggedSubdivJoin.get(Subdiv_.subname),
					loggedSubdivJoin.get(Subdiv_.active), loggedCompanyJoin.get(Company_.coid),
					loggedCompanyJoin.get(Company_.coname), 
					
					conContactJoin.get(Contact_.personid), conContactJoin.get(Contact_.firstName),
					conContactJoin.get(Contact_.lastName), conContactJoin.get(Contact_.email),
					conContactJoin.get(Contact_.telephone), conContactJoin.get(Contact_.active),
					conSubdivJoin.get(Subdiv_.subdivid), conSubdivJoin.get(Subdiv_.subname),
					conSubdivJoin.get(Subdiv_.active), conCompanyJoin.get(Company_.coid),
					conCompanyJoin.get(Company_.coname), 
					
					quoteRequestCompanyJoin.get(Company_.coid),quoteRequestCompanyJoin.get(Company_.coname), 
					companyJoin.get(Company_.coid)
					));
			return cq;
		});
	}
}