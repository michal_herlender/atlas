package org.trescal.cwms.core.quotation.entity.scheduledquoterequest.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequest;
import org.trescal.cwms.core.quotation.form.ScheduledQuotationRequestForm;

public interface ScheduledQuotationRequestService extends BaseService<ScheduledQuotationRequest, Integer> {

    /**
     * Gets a list of all outstanding {@link ScheduledQuotationRequest}s, orderd
     * by most recent first.
     */
    List<ScheduledQuotationRequest> getOutstanding(Subdiv allocatedSubdiv);

    /**
     * Gets the top x most recently processed {@link ScheduledQuotationRequest}
     * s.
     *
     * @param x the number of resutls to return. If negative or 0 then defaults
     *          to 10.
     */
    List<ScheduledQuotationRequest> getTopXMostRecentlyProcessed(Subdiv allocatedSubdiv, int x);

    List<ScheduledQuotationRequest> scheduledQuotationRequestFromQuotation(Integer quotationId);

    void createScheduledQuotationRequest(ScheduledQuotationRequestForm form, String username, Integer subdivid);
}