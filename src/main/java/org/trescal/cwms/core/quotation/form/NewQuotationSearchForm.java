package org.trescal.cwms.core.quotation.form;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.trescal.cwms.core.quotation.dto.QuotationProjectionDTO;
import org.trescal.cwms.core.quotation.entity.QuoteRequested;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@Getter @Setter
public class NewQuotationSearchForm
{
	private Integer allocatedSubdivId;
	private Integer allocatedCompanyId;
	private String clientref;
	private Integer coid;
	private String coname;
	private String contact;
	private List<Integer> createdByIds;
	private Integer descid;
	private String description;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate issueDate1;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate issueDate2;
	private boolean issueDateBetween;

	private List<Integer> issuedByIds;
	private String mfr;
	private Integer mfrid;
	private String model;
	private Integer modelid;
	private int pageNo;

	private Integer personid;
	private String plantid;
	private String plantno;
	private String qno;
	
	private List<QuotationProjectionDTO> quotationsIssued;
	private PagedResultSet<QuotationProjectionDTO> quotationsRequiringAction;
	private PagedResultSet<QuoteRequested> quoteRequestedList;
	private Collection<QuotationProjectionDTO> quotes;

	private int resultsPerPage = 50;
	private Integer resultsYearFilter;
	private PagedResultSet<QuotationProjectionDTO> rs;
	private String serialno;
	private Integer subdivid;
	
	private Integer quotestatus;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate expiryDate1;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate expiryDate2;
	private boolean expiryDateBetween;
	
	private List<Integer> sourcedByIds;
	private int tabIndex;
	private String sourcedByContBySubdivAC;
	
	private int quoteRequesteSubdivId;
	private int quoteRequesteContactId;
	private int quoteSubdivId;
	private int quoteContactId;
	
	private Integer sourcedBySubdivId;
	
	
}