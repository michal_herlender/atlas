package org.trescal.cwms.core.quotation.form;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class QuotationItemSearchForm {
	private Integer quotationid;
	private int pageNo;
	private int resultsPerPage = 100;
}
