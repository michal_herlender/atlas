package org.trescal.cwms.core.quotation.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

@Component("ConvertToQuotationUtils")
public class ConvertToQuotationUtils
{
	@Autowired
	private CalibrationTypeService calibrationTypeService;
	@Autowired
	private QuotationItemService quotationItemService;
	@Autowired
	private CatalogPriceService priceServ;
	
	/**
	 * Takes the most recent {@link JobCostingCalibrationCost} and applies these
	 * costs to the given {@link QuotationCalibrationCost}, if no job costing
	 * costs exist for the jobitem then the contract review cost (which must
	 * exist) are used instead.
	 * 
	 * @param ji the {@link JobItem}
	 * @param qCalCost the {@link QuotationCalibrationCost}.
	 */
	public void prepareCalibrationCostFromJobItem(JobItem ji, QuotationCalibrationCost qCalCost)
	{
		boolean jobCostFound = false;
		if (ji.getJobCostingItems() != null)
		{
			for (JobCostingItem jci : ji.getJobCostingItems())
			{
				// make sure the cost is active and is not on a site costing
				if (jci.getCalibrationCost().isActive()
						&& !jci.getJobCosting().getType().equals(PricingType.SITE))
				{
					this.prepateCalibrationCostFromJobCostingCalibrationCost(jci.getCalibrationCost(), qCalCost);
					jobCostFound = true;
					break;
				}
			}
		}

		// no active costing cal costs for this jobitem so just use the contract
		// review cal cost - which should probably be correct, even for site
		// jobs
		if (!jobCostFound)
		{
			ContractReviewCalibrationCost ccost = ji.getCalibrationCost();
			qCalCost.setDiscountRate(ccost.getDiscountRate());
			qCalCost.setDiscountValue(ccost.getDiscountValue());
			qCalCost.setFinalCost(ccost.getFinalCost());
			qCalCost.setHouseCost(ccost.getTotalCost());
			qCalCost.setTotalCost(ccost.getTotalCost());
		}
	}

	/**
	 * Takes the default calibration cost for the instrument and sets this as
	 * the quotation calibration cost. If no default cost is found then no costs
	 * are set.
	 * 
	 * @param inst the {@link Instrument}.
	 * @param qCalCost the {@link QuotationCalibrationCost}.
	 */
	public void prepareCalibrationCostFromModel(Instrument inst, QuotationCalibrationCost qCalCost, Subdiv subDiv)
	{
		this.prepareCalibrationCostFromModel(inst.getModel(), qCalCost, subDiv);
	}

	public void prepareCalibrationCostFromModel(InstrumentModel model, QuotationCalibrationCost qCalCost, Subdiv subDiv)
	{
		BigDecimal fixedPrice = new BigDecimal(0);
		CatalogPrice catalogPrice = priceServ.findSingle(model, qCalCost.getQuoteItem().getServiceType(), CostType.CALIBRATION, subDiv.getComp());
		if(catalogPrice != null)
			fixedPrice = catalogPrice.getFixedPrice();
		qCalCost.setHouseCost(fixedPrice);
		qCalCost.setTotalCost(fixedPrice);
		qCalCost.setFinalCost(fixedPrice);

	}

	public Quotationitem prepareItemToQuotationitem(InstrumentModel model, Quotation quotation, QuoteHeading heading, CalibrationType defaultCalType, Subdiv subdiv)
	{
		CalibrationType caltype = defaultCalType == null ? this.calibrationTypeService.getDefaultCalType() : defaultCalType;
		Quotationitem qItem = this.quotationItemService.createTemplateNewQuotationItem(quotation, heading, null, model.getModelid(), caltype.getCalTypeId(), subdiv.getSubdivid());
		this.prepareCalibrationCostFromModel(model, qItem.getCalibrationCost(), subdiv);
		qItem.getNotes().add(this.quotationItemService.createTemplateNewQuotationItemNote(qItem, true, "", "Created using model defaults", false, quotation.getCreatedBy()));
		CostCalculator.updateTPItemCostWithRunningTotal(qItem);
		return qItem;
	}

	public Quotationitem prepareItemToQuotationitem(JobCostingCalibrationCost cost, ServiceType serviceType, InstrumentModel model, Quotation quotation, QuoteHeading heading, Subdiv subdiv)
	{
		CalibrationType caltype = serviceType.getCalibrationType();
		Quotationitem qItem = this.quotationItemService.createTemplateNewQuotationItem(quotation, heading, null, model.getModelid(), caltype.getCalTypeId(), subdiv.getSubdivid());
		this.prepateCalibrationCostFromJobCostingCalibrationCost(cost, qItem.getCalibrationCost());
		int costingItemId = cost.getJobCostingItem().getId();
		qItem.getNotes().add(this.quotationItemService.createTemplateNewQuotationItemNote(qItem, true, "", "Created from cost id "
				+ costingItemId
				+ " link <a href=\"editjobcostingitem.htm?id= "
				+ costingItemId + "\" " + costingItemId + "></a>", false, quotation.getCreatedBy()));
		CostCalculator.updateTPItemCostWithRunningTotal(qItem);
		return qItem;
	}
	
	public void prepateCalibrationCostFromJobCostingCalibrationCost(JobCostingCalibrationCost ccost, QuotationCalibrationCost qCalCost)
	{
		qCalCost.setDiscountRate(ccost.getDiscountRate());
		qCalCost.setDiscountValue(ccost.getDiscountValue());
		qCalCost.setFinalCost(ccost.getFinalCost());
		qCalCost.setHouseCost(ccost.getHouseCost());
		qCalCost.setLinkedCostSrc(ccost.getLinkedCostSrc());
		qCalCost.setThirdCostSrc(ccost.getThirdCostSrc());
		qCalCost.setThirdCostTotal(ccost.getThirdCostTotal());
		qCalCost.setThirdManualPrice(ccost.getThirdManualPrice());
		qCalCost.setThirdMarkupRate(ccost.getThirdMarkupRate());
		qCalCost.setThirdMarkupSrc(ccost.getThirdMarkupSrc());
		qCalCost.setThirdMarkupValue(ccost.getThirdMarkupValue());
		qCalCost.setTotalCost(ccost.getTotalCost());
	}
}