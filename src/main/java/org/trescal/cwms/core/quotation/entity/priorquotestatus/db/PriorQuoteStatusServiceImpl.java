package org.trescal.cwms.core.quotation.entity.priorquotestatus.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.quotation.entity.priorquotestatus.PriorQuoteStatus;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.system.entity.status.db.StatusDao;

import java.time.LocalDate;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDate;

@Service("PriorQuoteStatusService")
public class PriorQuoteStatusServiceImpl implements PriorQuoteStatusService {
	@Autowired
	private PriorQuoteStatusDao pqsDao;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private StatusDao statusDao;

	public void deletePriorQuotationStatus(PriorQuoteStatus pqs) {
		this.pqsDao.remove(pqs);
	}

	public PriorQuoteStatus findPriorQuotationStatus(int id) {
		return this.pqsDao.find(id);
	}

	public Set<PriorQuoteStatus> getQuotationPriorStatusList(int quoteid) {
		return this.pqsDao.getQuotationPriorStatusList(quoteid);
	}

	public void insertPriorQuotationStatus(PriorQuoteStatus pqs) {
		this.pqsDao.persist(pqs);
	}

	public void updatePriorQuotationStatus(PriorQuoteStatus pqs) {
		this.pqsDao.merge(pqs);
	}

	public void updateQuoteStatus(Quotation q, int oldStatusId, int newQuoteStatusId, Contact user) {
		if (oldStatusId != newQuoteStatusId) {
			TreeSet<PriorQuoteStatus> pqsList = (TreeSet<PriorQuoteStatus>) this.getQuotationPriorStatusList(q.getId());

			Date setOn;
			Contact setBy;

			if (pqsList.size() > 0) {
				// get most recent
				setOn = pqsList.first().getRevokedOn();
				setBy = pqsList.first().getSetBy();
			} else {
				setOn = dateFromLocalDate(q.getRegdate());
				setBy = q.getCreatedBy();
			}

			// set the quotation fields to issued / accepted if the status
			// requires this
			if (q.getQuotestatus().getAccepted() && !q.isAccepted()) {
				q.setAcceptedBy(user);
				q.setAccepted(true);
				q.setAcceptedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			} else if (q.getQuotestatus().getIssued() && !q.isIssued()) {
				q.setIssueby(user);
				q.setIssued(true);
				q.setIssuedate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
				q.setExpiryDate(this.quoteServ.getExpiryDate(q));
			}

			QuoteStatus oldS = this.statusDao.findStatus(oldStatusId, QuoteStatus.class);
			QuoteStatus newS = this.statusDao.findStatus(newQuoteStatusId, QuoteStatus.class);

			// was accepted but has been set back to issued?
			if (oldS.getAccepted() && newS.getIssued()) {
				q.setAcceptedBy(null);
				q.setAccepted(false);
				q.setAcceptedOn(null);
				q.setIssueby(user);
				q.setIssued(true);
				q.setIssuedate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
				q.setExpiryDate(quoteServ.getExpiryDate(q));
			}
			// was issued and has been set to accepted
			else if(oldS.getIssued() && newS.getAccepted()) {
				q.setAcceptedBy(user);
				q.setAccepted(true);
				q.setAcceptedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			}
			// was accepted and has been set to other status 
			else if(oldS.getAccepted() && !newS.getIssued() && !newS.getAccepted()) {
				q.setAcceptedBy(null);
				q.setAccepted(false);
				q.setAcceptedOn(null);
			}


			PriorQuoteStatus pqs = new PriorQuoteStatus(oldS, setBy, setOn, newS, user, new Date(), q);
			this.pqsDao.persist(pqs);	// Added to assign ID to prevent comparator NPE GB 2014-12-18
			pqsList.add(pqs);
			q.setPriorStatusList(pqsList);
		}
	}
}
