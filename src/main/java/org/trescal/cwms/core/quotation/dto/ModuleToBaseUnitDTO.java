package org.trescal.cwms.core.quotation.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ModuleToBaseUnitDTO
{
	private Integer modelId;
	private String plantNo;
	private Integer qty;
	private String unitCost;

	public ModuleToBaseUnitDTO()
	{

	}

	public ModuleToBaseUnitDTO(Integer modelId, String plantNo, Integer qty, String unitCost)
	{
		this.modelId = modelId;
		this.plantNo = plantNo;
		this.qty = qty;
		this.unitCost = unitCost;
	}

}
