package org.trescal.cwms.core.quotation.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.quotation.dto.PreliminaryCompInstToQuoteItemWrapper;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.quotation.form.AddQuotationForm;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

public interface CompanyInstrumentsToQuotationService {
	
	Quotationitem getNewQuotationItemForInstrument(Instrument instrument, Quotation quotation,
			QuoteHeading heading, Subdiv subdiv, JobItem ji);

	Quotation prepareAndSaveCompanyInstrumentsToQuotation(AddQuotationForm aqf, Quotation quotation,
			Subdiv subdiv, Contact currentContact);

	void prepareCalibrationCost(Instrument inst, JobItem ji, QuotationCalibrationCost qCalCost, Subdiv subDiv);

	Set<Quotationitem> getNewQuotationItemsForInstruments(List<Instrument> toquoteitems,
			List<CalibrationType> caltypes, Quotation quotation, Subdiv subdiv, List<Integer> plantIds);

	ResultWrapper sourceCompanyInstrumentToQIValues(int[] plantIds, int quoteId, int subDivId);

	BigDecimal getCatalogPriceForInstrument(Integer plantId, Integer calTypeId, Integer subDivId);
	
	PreliminaryCompInstToQuoteItemWrapper getDefaultPrices(Instrument inst, 
			InstrumentModel model , CalibrationType calibrationType, Quotation quote, 
			Integer subdivid, Locale locale);
	
	List<PreliminaryCompInstToQuoteItemWrapper> sourceCompanyInstrumentModelToQIValues(List<Integer> calTypeIds, Integer modelId, Integer coid, 
			 Integer quoteId);
}
