package org.trescal.cwms.core.quotation.entity.quotation.db;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrumentmodel.dto.InstrumentModelWithPriceDTO;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.quotation.dto.QuotationKeyValue;
import org.trescal.cwms.core.quotation.dto.QuotationProjectionDTO;
import org.trescal.cwms.core.quotation.dto.QuotationSearchResultWrapper;
import org.trescal.cwms.core.quotation.dto.QuoteItemOutputDto;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.QuotationMetricFormatter;
import org.trescal.cwms.core.quotation.entity.quotestatus.QuoteStatus;
import org.trescal.cwms.core.quotation.entity.quotestatusgroup.QuoteStatusGroup;
import org.trescal.cwms.core.quotation.form.NewQuotationSearchForm;
import org.trescal.cwms.core.quotation.form.QuotationMetricForm;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.tools.PagedResultSet;

public interface QuotationDao extends AllocatedDao<Company, Quotation, Integer> {

	Long countByCompanyAndAllocatedCompany(Company company, Company allocatedCompany);

	Long countActualByCompanyAndAllocatedCompany(Integer yearsBack, Company company, Company allocatedCompany);

	Long countByContactAndAllocatedSubdiv(Contact contact, Subdiv allocatedSubdiv);

	Long countActualByContactAndAllocatedSubdiv(Integer yearsBack, Contact contact, Subdiv allocatedSubdiv);

	Long countBySubdivAndAllocatedSubdiv(Subdiv subdiv, Subdiv allocatedSubdiv);

	Long countActualBySubdivAndAllocatedSubdiv(Integer yearsBack, Subdiv subdiv, Subdiv allocatedSubdiv);

	Quotation findEagerQuotation(int id);

	Integer findMaxQuotationVersion(String qno);

	List<Quotation> findQuotations(String qno);

	List<QuotationKeyValue> findLinkedQuotationsWithServiceModels(Job job);

	List<InstrumentModelWithPriceDTO> findServiceModelsOnLinkedQuotations(Integer jobId);
	
	List<InstrumentModelWithPriceDTO> findServiceModelsOnQuotation(Integer quoteId);

	Set<CalibrationType> getCaltypesPerQuotation(int quoteid);

	List<Quotation> getIssuedQuotationsForContactFromIssuedate(int personId, LocalDate issuedate, boolean accepted);

	Quotation getQuotationByExactQuoteNo(String quoteNo);

	List<Quotation> getQuotationsRequestForContactFromReqdate(int personId, LocalDate reqdate, boolean accepted, boolean issued);

	QuotationMetricFormatter loadMetrics(QuotationMetricForm metricForm, QuotationMetricFormatter formatter);

	List<QuotationSearchResultWrapper> searchCompanyQuotations(int coid, boolean showExpired, boolean showUnIssued,
			Integer years, Integer jobid);

	List<Quotation> getQuotationListByJobitemId(Integer jobitemid);

	List<Quotation> getIssuedAndAcceptedQuotationListByJobitemId(Integer jobitemid, ArrayList<QuoteStatus> quoteStatus);
	
	List<QuotationProjectionDTO> getAllQuotesInStatusGroupNew(QuoteStatusGroup group, Integer allocatedSubdivId, Locale locale);
	List<QuotationProjectionDTO> getMostRecentIssuedQuotationsNew(int resultSize, Integer allocatedSubdivId, Locale locale);
	PagedResultSet<QuotationProjectionDTO> queryQuotationNew(NewQuotationSearchForm qsf, PagedResultSet<QuotationProjectionDTO> rs, Locale locale);
	
	void getPaginationAllQuotesInStatusGroupNew(QuoteStatusGroup group, Integer allocatedSubdivId,
			PagedResultSet<QuotationProjectionDTO> webQuotesRequiringAction, NewQuotationSearchForm qsf, Locale locale);
	
	List<QuoteItemOutputDto> findQuotationItemsFromModel(Integer modelId, Integer servicetypeid, Company allocatedCompany);
}