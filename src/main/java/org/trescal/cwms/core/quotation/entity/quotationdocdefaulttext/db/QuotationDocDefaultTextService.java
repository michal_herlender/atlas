package org.trescal.cwms.core.quotation.entity.quotationdocdefaulttext.db;

import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.quotation.entity.quotationdocdefaulttext.QuotationDocDefaultText;

public interface QuotationDocDefaultTextService extends BaseService<QuotationDocDefaultText, Integer> {
	QuotationDocDefaultText findDefaultText(Locale locale);
}
