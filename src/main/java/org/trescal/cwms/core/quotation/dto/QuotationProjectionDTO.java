package org.trescal.cwms.core.quotation.dto;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.company.projection.ContactProjectionDTO;
import org.trescal.cwms.core.quotation.entity.QuoteRequestType;
import org.trescal.cwms.core.quotation.entity.QuoteRequested;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;


@Getter
@Setter
public class QuotationProjectionDTO implements QuoteRequested, Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private ContactProjectionDTO sourcedBy;
    private String qno;
    private Integer ver;
    private ContactProjectionDTO contactDto;
    private String quotestatus;
    private String clientref;
    private ContactProjectionDTO issueby;
    private LocalDate issuedate;
    private Integer organisationId;
    private BigDecimal finalCost;
    private LocalDate duedate;
    private LocalDate regdate;
    private String sourcedSubCode;
    private String contactSubCode;
    private String issuebySubCode;
    private LocalDate expiryDate;
    private String subdivCode;
    private BigDecimal totalCost;

    public QuotationProjectionDTO(Integer id, String qno, Integer ver, String quotestatus, String clientref,
                                  LocalDate issuedate,

                                  Integer sourcedPersonid, String sourcedFirstName, String sourcedLastName,
                                  String sourcedEmail, String sourcedTelephone, Boolean sourcedContactActive,
                                  Integer sourcedSubdivid, String sourcedSubname, String sourcedSubCode,
                                  Boolean sourcedSubdivActive, Integer sourcedCoid, String sourcedConame,

                                  Integer contactPersonid, String contactFirstName, String contactLastName,
                                  String contactEmail, String contactTelephone, Boolean contactContactActive,
                                  Integer contactSubdivid, String contactSubname, String contactSubCode,
                                  Boolean contactSubdivActive, Integer contactCoid, String contactConame,

                                  Integer issuebyPersonid, String issuebyFirstName, String issuebyLastName,
                                  String issuebyEmail, String issuebyTelephone, Boolean issuebyContactActive,
                                  Integer issuebySubdivid, String issuebySubname, String issuebySubCode,
                                  Boolean issuebySubdivActive, Integer issuebyCoid, String issuebyConame,

                                  Integer organisationId, BigDecimal finalCost,BigDecimal totalCost,
                                  LocalDate duedate, LocalDate regdate,LocalDate expiryDate,String subdivCode
    ) {
        super();
        this.id = id;
        this.sourcedBy = new ContactProjectionDTO(sourcedPersonid, sourcedFirstName, sourcedLastName, sourcedEmail, sourcedTelephone, sourcedContactActive, sourcedSubdivid, sourcedSubname, sourcedSubdivActive, sourcedCoid, sourcedConame, null, null);
        this.qno = qno;
        this.ver = ver;
        this.contactDto = new ContactProjectionDTO(contactPersonid, contactFirstName, contactLastName, contactEmail, contactTelephone, contactContactActive, contactSubdivid, contactSubname, contactSubdivActive, contactCoid, contactConame, null, null);
        this.quotestatus = quotestatus;
        this.clientref = clientref;
        this.issueby = new ContactProjectionDTO(issuebyPersonid, issuebyFirstName, issuebyLastName, issuebyEmail, issuebyTelephone, issuebyContactActive, issuebySubdivid, issuebySubname, issuebySubdivActive, issuebyCoid, issuebyConame, null, null);
        this.issuedate = issuedate;
        this.organisationId = organisationId;
        this.finalCost = finalCost;
        this.duedate = duedate;
        this.regdate = regdate;
        this.sourcedSubCode = sourcedSubCode;
        this.contactSubCode = contactSubCode;
        this.issuebySubCode = issuebySubCode;
        this.expiryDate = expiryDate;
        this.subdivCode = subdivCode;
        this.totalCost = totalCost;
    }


    @Override
    public QuoteRequestType getQuoteType() {
        return QuoteRequestType.CALIBRATION;
    }

    @Override
    public LocalDate getReqdate() {
        return null;
    }

    @Override
    public boolean isQuote() {
        return true;
    }
}