package org.trescal.cwms.core.quotation.dto;

import java.math.BigDecimal;

import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelpartof.InstrumentModelPartOf;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class QuoteItemModuleWrapper
{
	private InstrumentModelPartOf impo;
	private BigDecimal modelCalCost;

}
