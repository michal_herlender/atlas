package org.trescal.cwms.core.quotation.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.ScheduledQuotationRequest;
import org.trescal.cwms.core.quotation.entity.scheduledquoterequest.db.ScheduledQuotationRequestService;
import org.trescal.cwms.core.quotation.enums.QuoteGenerationStrategy;
import org.trescal.cwms.core.quotation.enums.ScheduledQuoteRequestScope;
import org.trescal.cwms.core.quotation.enums.ScheduledQuoteRequestType;
import org.trescal.cwms.core.quotation.form.ScheduledQuotationRequestForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class ScheduledQuotationRequestController
{
	@Autowired
	private ContactService contactService;
	@Autowired
	private CalibrationTypeService calibrationTypeService;
	@Autowired
	private ScheduledQuotationRequestService scheduledQuotationRequestService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	
	public static final String VIEW_NAME = "trescal/core/quotation/scheduledquotationrequest";
	public static final String FORM_NAME = "form";

	@InitBinder
	public void initBinder(ServletRequestDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}
	
	@ModelAttribute(FORM_NAME)
	public ScheduledQuotationRequestForm formBackingObject(Locale locale,
			@RequestParam(name="personid", required=false, defaultValue="0") Integer personid) {
		ScheduledQuotationRequestForm form = new ScheduledQuotationRequestForm();
		// look for a default contact that may have been specified in call
		if (personid > 0)
		{
			Contact person = this.contactService.get(personid);
			form.setContactid(person.getId());
			form.setSubdivid(person.getSub().getSubdivid());
			form.setCoid(person.getSub().getComp().getCoid());
		}
		
		form.setIncludeEmptyDates(false);
		form.setCalTypeId(this.calibrationTypeService.getDefaultCalType().getCalTypeId());
		form.setLanguageTag(locale.toLanguageTag());
		form.setScope(ScheduledQuoteRequestScope.COMPANY);
		form.setType(ScheduledQuoteRequestType.ALL_INSTRUMENTS);
		form.setStrategy(QuoteGenerationStrategy.SINGLE_QITEM_PER_INSTRUMENT_WITH_PLANTNOS);
		return form;
	}

	@RequestMapping(value = "/deletescheduledquoterequest.htm")
	public String delete(@RequestParam(name = "id") Integer sqrid) {
		ScheduledQuotationRequest sqr = this.scheduledQuotationRequestService.get(sqrid);
		this.scheduledQuotationRequestService.delete(sqr);

		return "redirect:/scheduledquoterequest.htm";
	}
	
	@RequestMapping(value="/scheduledquoterequest.htm", method=RequestMethod.POST)
	public String onSubmit(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto,
			@Valid @ModelAttribute(FORM_NAME) ScheduledQuotationRequestForm form,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return VIEW_NAME;
		}
		this.scheduledQuotationRequestService.createScheduledQuotationRequest(form, username, subdivDto.getKey());
		// Prevent parameters from polluting redirect url
		model.asMap().clear();
		return "redirect:scheduledquoterequest.htm";
	}
	
	@ModelAttribute("supportedLocales")
	public List<KeyValue<String,String>> getSupportedLocales(Locale displayLocale) {
		List<Locale> locales = supportedLocaleService.getSupportedLocales();
		return supportedLocaleService.getDTOList(locales, displayLocale);
	}
	
	@ModelAttribute("outstanding")
	public List<ScheduledQuotationRequest> getOutstanding(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto) {
		Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
		return this.scheduledQuotationRequestService.getOutstanding(allocatedSubdiv);
	}

	@ModelAttribute("mostrecent")
	public List<ScheduledQuotationRequest> getMostRecent(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto) {
		Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
		return this.scheduledQuotationRequestService.getTopXMostRecentlyProcessed(allocatedSubdiv, 20);
	}

	@ModelAttribute("allcaltypes")
	public List<CalibrationType> getAllCalTypes() {
		return this.calibrationTypeService.getCalTypes();
	}

	@ModelAttribute("quotegenerationstrategies")
	public QuoteGenerationStrategy[] getQuoteGenerationStrategies() {
		return QuoteGenerationStrategy.values();
	}

	@RequestMapping(value="/scheduledquoterequest.htm", method=RequestMethod.GET)
	public String showView() {
		return VIEW_NAME;
	}
}
