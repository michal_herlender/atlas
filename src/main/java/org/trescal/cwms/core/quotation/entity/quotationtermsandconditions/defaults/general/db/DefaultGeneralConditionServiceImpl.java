package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.general.DefaultGeneralCondition;

@Service("DefaultGeneralConditionService")
public class DefaultGeneralConditionServiceImpl extends BaseServiceImpl<DefaultGeneralCondition, Integer> implements DefaultGeneralConditionService
{
	@Autowired
	private DefaultGeneralConditionDao defaultGeneralConditionDao;
	
	@Override
	protected BaseDao<DefaultGeneralCondition, Integer> getBaseDao() {
		return defaultGeneralConditionDao;
	}
	
	@Override
	public DefaultGeneralCondition getLatest(Company allocatedCompany) {
		return this.defaultGeneralConditionDao.getLatest(allocatedCompany);
	}
}