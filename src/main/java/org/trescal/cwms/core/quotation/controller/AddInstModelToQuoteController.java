package org.trescal.cwms.core.quotation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostMarkupSource;
import org.trescal.cwms.core.pricing.entity.enums.ThirdCostSource;
import org.trescal.cwms.core.pricing.entity.pricingitems.thirdpartypricingitem.db.ThirdPartyPricingItemService;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.QuotationItemComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.quotation.entity.quoteheading.db.QuoteHeadingService;
import org.trescal.cwms.core.quotation.form.AddInstModelToQuoteForm;
import org.trescal.cwms.core.quotation.service.AddInstModelToQuotationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.Discount;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.MathTools;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Comparator.comparing;

/**
 * Form controller which allows users to add items to a {@link Quotation}. Users
 * can search for items to add from the {@link Model} database, add selected
 * items into a basket, search again for more items whilst maintaining the
 * already selected items in the basket, update basket item properties between
 * searches and finally save the basket items back to the {@link Quotation} as
 * {@link Quotationitem} objects. Each successive search will submit back to the
 * same form, so much of the processing in the onSubmit() method deals with
 * maintaining the state of the items already added to the basket.
 */
@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME })
public class AddInstModelToQuoteController {
	@Value("${cwms.config.quotation.size.restrictaddmodel}")
	private long sizeRestrictAddModel;
	@Value("#{props['cwms.config.costs.quotations.roundupnearest50']}")
	private boolean roundUpFinalCosts;
	@Autowired
	private AddInstModelToQuotationService addInstModelToQuotationService;
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private Discount discount;
	@Autowired
	private InstrumentModelService modelServ;
	@Autowired
	private QuoteHeadingService qhServ;
	@Autowired
	private QuotationItemService qiServ;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private ThirdPartyPricingItemService thirdPricingItemServ;
	@Autowired
	private SubdivService subDivServ;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private UserService userService;
	@Autowired
	private MessageSource messageSource;

	@ModelAttribute("modelTypeSelections")
	private List<KeyValue<Integer, String>> createModelTypeSelections() {
		List<KeyValue<Integer, String>> selections = new ArrayList<>();
		selections.add(new KeyValue<Integer, String>(0,
				messageSource.getMessage("instmod.all", null, LocaleContextHolder.getLocale())));
		selections.add(new KeyValue<Integer, String>(1,
				messageSource.getMessage("instmod.salescatonly", null, LocaleContextHolder.getLocale())));
		selections.add(new KeyValue<Integer, String>(2,
				messageSource.getMessage("instmod.excludesalescat", null, LocaleContextHolder.getLocale())));
		return selections;
	}

	@ModelAttribute("addquoteitemsearchform")
	protected AddInstModelToQuoteForm formBackingObject(HttpSession session,
			@RequestParam(name = "id", required = true) Integer quoteid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
		long totalItemCount = qiServ.getItemCount(quoteid);
		// 2016-02-22 - Added check of number of quotation items until
		// performance
		// issues fully resolved
		if (totalItemCount >= sizeRestrictAddModel) {
			throw new RuntimeException("This quotation has " + totalItemCount
					+ " items and due to temporary performance restrictions, quotations with " + sizeRestrictAddModel
					+ " items or more cannot have instrument models added.");
		}
		// find quotation
		Quotation q = this.quoteServ.get(quoteid);
		// create new quotation item search form
		AddInstModelToQuoteForm aqi = new AddInstModelToQuoteForm();
		// add quote, quoteid, quoteno and version
		aqi.setQuotation(q);
		// added this value to the form because when retrieved on the velocity
		// page a lazy initialisation exception is encountered
		aqi.setCoid(q.getContact().getSub().getComp().getCoid());
		aqi.setId(q.getId());
		aqi.setQno(q.getQno());
		aqi.setVer(q.getVer());
		// set all available caltypes and max quantity
		List<CalibrationType> calTypes = this.calTypeServ.getCalTypes();
		calTypes.sort((c1, c2) -> c1.getOrderBy().compareTo(c2.getOrderBy()));
		aqi.setCaltypes(calTypes);
		// set quote headings
		aqi.setHeadings(this.qhServ.findQuotationHeadings(quoteid));
		// set max item quantity
		aqi.setMaxQuantity(50);
		List<Integer> range = IntStream.rangeClosed(1, 50).boxed().collect(Collectors.toList());
		aqi.setQuantityRange(range);
		// get the current maximum quotationitem number currently on the quote
		aqi.setMaxItemNo(this.qiServ.getMaxItemno(q.getId()) + 1);
		// make the logged in user's currency available
		aqi.setDefaultCurrency(this.subDivServ.get(subdivDto.getKey()).getComp().getCurrency());
		// the pricing type class name
		session.setAttribute(Constants.HTTPSESS_PRICING_CLASS_NAME, Quotation.class);

		// initialise form search
		aqi.setSearch(new InstrumentAndModelSearchForm<InstrumentModel>());

		return aqi;
	}

	private Double getCalPrice(String calprice) {
		Double cal = null;
		if (calprice != null) {
			if (NumberTools.isADouble(calprice)) {
				cal = Double.parseDouble(calprice);
			}
		}
		return cal;
	}

	private String getPlantNo(String plantno) {
		if (plantno != null) {
			plantno = plantno.trim();
			if (plantno.length() > 50) {
				plantno = plantno.substring(0, 50);
			}
		} else
			plantno = "";
		return plantno;
	}

	@RequestMapping(value = "/addinstmodeltoquote.htm", method = RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/quotation/addinstmodeltoquote";
	}

	@RequestMapping(value = "/addinstmodeltoquote.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(@ModelAttribute("addquoteitemsearchform") AddInstModelToQuoteForm aqi,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		Subdiv subDiv = subDivServ.get(subdivDto.getKey());
		Contact contact = userService.get(username).getCon();
		Quotation quotation = this.quoteServ.get(aqi.getId());
		Locale locale = LocaleContextHolder.getLocale();
		aqi.setQuotation(quotation);
		// always add all quotation items on the form to the chosenModels
		// arraylist
		if (aqi.getIds() != null) {
			// arraylist to hold the current list of quotationitems in the
			// quotation item basket
			ArrayList<Quotationitem> aList = new ArrayList<Quotationitem>();
			// iterate over all of the items in the basket and convert the
			// fields in the form to actual quotation items
			for (int i = 0; i < aqi.getIds().size(); i++) {
				Quotationitem qitem = new Quotationitem();
				qitem.setQuotation(aqi.getQuotation());
				qitem.setItemno(aqi.getItemnos().get(i));
				qitem.setQuantity(aqi.getQuantities().get(i));
				if (aqi.getPlantnos().size() >= i && aqi.getPlantnos().size() != 0)
					qitem.setPlantno(this.getPlantNo(aqi.getPlantnos().get(i)));
				else
					qitem.setPlantno("");
				CalibrationType calType = this.calTypeServ.find(Integer.parseInt(aqi.getCaltype().get(i)));
				qitem.setCaltype(calType);
				if (calType != null)
					qitem.setServiceType(calType.getServiceType());
				qitem.setHeading(this.qhServ.findQuoteHeading(aqi.getHeadingids().get(i)));
				qitem.setPartOfBaseUnit(false);
				qitem.setModel(this.modelServ.findInstrumentModel(aqi.getIds().get(i)));
				qitem.setNotes(new TreeSet<>(new NoteComparator()));
				// set the default third party costs
				this.thirdPricingItemServ.setThirdPartyDefaultValues(qitem);
				aqi.setMaxItemNo(aqi.getMaxItemNo() + 1);
				this.setQuotationItemPriceDefaults(qitem, this.getCalPrice(aqi.getCalcosts().get(i)), subDiv, contact);
				// set cost source
				if (qitem.getCalibrationCost() != null) {
					qitem.getCalibrationCost().setCostSrc(CostSource.valueOf(aqi.getSourceCost().get(i)));
				}
				// check for any modules that are being added to this
				// quotaitonitem
				if (aqi.getModulemodelids() != null) {
					for (int m = 0; m < aqi.getModulemodelids().size(); m++) {
						// this module has been added as part of this base unit
						if (aqi.getModulepartofs().get(m).intValue() == qitem.getItemno().intValue()) {
							// create the module as a new quotationitem using
							// the baseunit's defaults (for qty, plantno etc)
							Quotationitem modItem = new Quotationitem();
							modItem.setQuotation(aqi.getQuotation());
							modItem.setItemno(aqi.getModuleitemnos().get(m));
							modItem.setQuantity(aqi.getQuantities().get(i));
							if (aqi.getPlantnos().size() >= i && aqi.getPlantnos().size() != 0)
								modItem.setPlantno(this.getPlantNo(aqi.getPlantnos().get(i)));
							else
								modItem.setPlantno("");
							modItem.setCaltype(this.calTypeServ.find(Integer.parseInt(aqi.getCaltype().get(i))));
							modItem.setHeading(this.qhServ.findQuoteHeading(aqi.getHeadingids().get(i)));
							modItem.setModel(this.modelServ.findInstrumentModel(aqi.getModulemodelids().get(m)));
							modItem.setBaseUnit(qitem);
							// set the default third party costs
							this.thirdPricingItemServ.setThirdPartyDefaultValues(modItem);
							// flag that this instrument is part of another on
							// the quotationitem
							modItem.setPartOfBaseUnit(true);
							aqi.setMaxItemNo(aqi.getMaxItemNo() + 1);
							this.setQuotationItemPriceDefaults(modItem, null, subDiv, contact);
							// add the module quotationitem as a module of the
							// current quotation item
							if (qitem.getModules() == null)
								qitem.setModules(new TreeSet<Quotationitem>(new QuotationItemComparator()));
							qitem.getModules().add(modItem);
						}
					}
				}
				aList.add(qitem);
			}
			aqi.setChosenModels(aList);
		}
		if (aqi.getSubmitted().equals("addtoquote")) {
			this.addInstModelToQuotationService.addToQuote(quotation, aqi, subDiv, contact, locale);
			return new ModelAndView(new RedirectView("viewquotation.htm?id=" + aqi.getId()));
		} else {
			this.addInstModelToQuotationService.saveQuotationDefaults(aqi, contact);
			// Set required instrument model types
			switch (aqi.getModelTypeSelectorChoice()) {
			case 1:
				aqi.getSearch().setExcludeStndardModelTypes(true);
				break;
			case 2:
				aqi.getSearch().setExcludeSalesCategoryModelTypes(true);
				break;
			default:
				break;
			}

			aqi.getSearch().setExcludeCapabilityModelTypes(true);
			aqi.getSearch().setExcludeQuarantined(true);

			aqi.getSearch().setSalesCat(aqi.getSalesCat());
			aqi.getSearch().setSalesCatId(aqi.getSalesCatId());
			aqi.getSearch().setDomainId(aqi.getDomainId());
			aqi.getSearch().setDomainNm(aqi.getDomainNm());
			aqi.getSearch().setFamilyId(aqi.getFamilyId());
			aqi.getSearch().setFamilyNm(aqi.getFamilyNm());
			aqi.getSearch().setMfrId(aqi.getMfrId());
			aqi.getSearch().setMfrNm(aqi.getMfrtext());

			List<InstrumentModel> searchMods = this.modelServ.searchInstrumentModels(aqi.getSearch());

			// TODO - changed from sorting by assembled model name to stored
			// translation,
			// but better to do this via database.
			final Locale userLocale = locale;
			final Locale defaultLocale = this.supportedLocaleService.getPrimaryLocale();
			final Function<InstrumentModel, String> byFullName = model -> InstModelTools.modelNameViaTranslations(model,
					userLocale, defaultLocale);

			List<InstrumentModel> searchModelsSorted = searchMods.stream().sorted(comparing(byFullName))
					.collect(Collectors.toList());

			aqi.setSearchModels(searchModelsSorted);

			return new ModelAndView("trescal/core/quotation/addinstmodeltoquote", "addquoteitemsearchform", aqi);
		}
	}

	private void setQuotationItemPriceDefaults(Quotationitem qi, Double calcost, Subdiv subDiv, Contact contact) {
		if ((calcost == null) || (NumberTools.doubleIsZero(calcost))) {
			// no calcost set so get the default cost for this model for this
			// quotation's currency
            BigDecimal cost = this.modelServ.getModelDefaultCalCost(qi.getModel(), qi.getServiceType().getCalibrationType(),
                    qi.getQuotation().getCurrency().getDefaultRate(), subDiv.getComp());
			calcost = cost == null ? 0.00 : cost.doubleValue();
		}

		QuotationCalibrationCost calCost = new QuotationCalibrationCost();
		calCost.setHouseCost(new BigDecimal(calcost.doubleValue(), MathTools.mc));

		// third party costs
		calCost.setThirdManualPrice(new BigDecimal(0.00));
		calCost.setThirdMarkupSrc(ThirdCostMarkupSource.SYSTEM_DEFAULT);
		calCost.setThirdMarkupRate(new BigDecimal(0.00));
		calCost.setThirdCostSrc(ThirdCostSource.MANUAL);
		calCost.setThirdMarkupValue(new BigDecimal(0.00));
		calCost.setThirdCostTotal(new BigDecimal(0.00));
		// calibration costs
		Contact client = qi.getQuotation().getContact();
		Double rate = discount
				.parseValue(discount.getValueHierarchical(Scope.CONTACT, client, subDiv.getComp()).getValue());
		calCost.setDiscountRate(new BigDecimal(rate));
		calCost.setDiscountValue(new BigDecimal(0.00));
		calCost.setTotalCost(
				new BigDecimal(calcost.doubleValue()).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));
		calCost.setFinalCost(
				new BigDecimal(calcost.doubleValue()).setScale(MathTools.SCALE_FIN, MathTools.ROUND_MODE_FIN));
		calCost.setCostType(CostType.CALIBRATION);
		calCost.setActive(true);
		CostCalculator.updateSingleCost(calCost, this.roundUpFinalCosts);
		qi.setCalibrationCost(calCost);

		QuotationPurchaseCost salesCost = new QuotationPurchaseCost();
		// SalesCost salesCost = new SalesCost();
		salesCost.setHouseCost(new BigDecimal(0.00));
		salesCost.setDiscountRate(new BigDecimal(0.00));
		salesCost.setDiscountValue(new BigDecimal(0.00));
		salesCost.setTotalCost(new BigDecimal(0.00));
		salesCost.setFinalCost(new BigDecimal(0.00));
		salesCost.setCostType(CostType.PURCHASE);
		salesCost.setActive(false);

		salesCost.setThirdManualPrice(new BigDecimal(0.00));
		salesCost.setThirdMarkupSrc(ThirdCostMarkupSource.SYSTEM_DEFAULT);
		salesCost.setThirdMarkupRate(new BigDecimal(0.00));
		salesCost.setThirdCostSrc(ThirdCostSource.MANUAL);
		salesCost.setThirdMarkupValue(new BigDecimal(0.00));
		salesCost.setThirdCostTotal(new BigDecimal(0.00));
		salesCost.setThirdMarkupSrc(ThirdCostMarkupSource.SYSTEM_DEFAULT);
		salesCost.setThirdCostSrc(ThirdCostSource.MANUAL);
		CostCalculator.updateSingleCost(salesCost, this.roundUpFinalCosts);
		qi.setPurchaseCost(salesCost);

		qi.setGeneralDiscountRate(new BigDecimal(0.00));
		qi.setInspection(new BigDecimal(0.00));

		CostCalculator.updateTPItemCostWithRunningTotal(qi);
	}
}
