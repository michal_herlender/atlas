package org.trescal.cwms.core.quotation.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.ContractReviewCalibrationCost;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.pricing.PricingType;
import org.trescal.cwms.core.pricing.catalogprice.entity.CatalogPrice;
import org.trescal.cwms.core.pricing.catalogprice.entity.db.CatalogPriceService;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.quotation.dto.NewQuotationItemDTO;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
public class QuotationController {

	@Autowired
	private CalibrationTypeService calibrationTypeService;
	@Autowired
	private CatalogPriceService catalogPriceService;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private QuotationService quotationService;
	@Autowired
	private TranslationService translationService;

	@RequestMapping(value = "/toggleInstrumentInQuotationBasket.htm", method = RequestMethod.POST)
	public String toggleInstrumentInQuotationBasket(
			@RequestParam(name = "plantIds[]", required = true) List<Integer> plantIds,
			@RequestParam(name = "quoteId", required = true) Integer quoteId, Model model) {
		Quotation quotation = quotationService.get(quoteId);
		Locale locale = LocaleContextHolder.getLocale();
		List<NewQuotationItemDTO> quotationItems = plantIds.stream().map(plantId -> {
			Instrument instrument = instrumentService.get(plantId);
			NewQuotationItemDTO quotationItem = new NewQuotationItemDTO();
			quotationItem.setPlantId(plantId);
			quotationItem.setModelName(
					translationService.getCorrectTranslation(instrument.getModel().getNameTranslations(), locale));
			quotationItem.setSerialNo(instrument.getSerialno());
			quotationItem.setPlantNo(instrument.getPlantno());
			JobItem jobItem = jobItemService.findMostRecentJobItem(plantId);
			boolean costFound = false;
			if (jobItem != null) {
				quotationItem.setDefaultCalTypeId(jobItem.getCalType().getCalTypeId());
				for (JobCostingItem jobCostingItem : jobItem.getJobCostingItems()) {
					if (jobCostingItem.getCalibrationCost().isActive()
							&& !jobCostingItem.getJobCosting().getType().equals(PricingType.SITE)) {
						quotationItem.setFinalPrice(jobCostingItem.getFinalCost());
						quotationItem.setPriceSource(CostSource.JOB_COSTING);
						costFound = true;
						break;
					}
				}
				if (!costFound) {
					ContractReviewCalibrationCost contractReviewCost = jobItem.getCalibrationCost();
					if (contractReviewCost != null) {
						quotationItem.setFinalPrice(contractReviewCost.getFinalCost());
						quotationItem.setPriceSource(CostSource.CONTRACT_REVIEW);
						costFound = true;
					}
				}
			}
			if (!costFound) {
				ServiceType serviceType = instrument.getDefaultServiceType();
				if (serviceType == null)
					serviceType = calibrationTypeService.getDefaultCalType().getServiceType();
				quotationItem.setDefaultCalTypeId(serviceType.getCalibrationType().getCalTypeId());
				Company company = quotation.getOrganisation();
				CatalogPrice price = catalogPriceService.findSingle(instrument.getModel(), serviceType,
						CostType.CALIBRATION, company);
				if (price == null) {
					quotationItem.setFinalPrice(BigDecimal.valueOf(0, 2));
					quotationItem.setPriceSource(CostSource.MANUAL);
				} else {
					quotationItem.setFinalPrice(price.getFixedPrice());
					quotationItem.setPriceSource(CostSource.MODEL);
				}
			}
			return quotationItem;
		}).collect(Collectors.toList());
		model.addAttribute("quotationItems", quotationItems);
		model.addAttribute("quoteId", quoteId);
		model.addAttribute("calTypes",
				calibrationTypeService.getCalTypes().stream()
						.map(ct -> new KeyValue<Integer, String>(ct.getCalTypeId(), translationService
								.getCorrectTranslation(ct.getServiceType().getShortnameTranslation(), locale)))
						.collect(Collectors.toList()));
		model.addAttribute("headings", quotation.getQuoteheadings());
		model.addAttribute("currencySymbol", quotation.getCurrency().getCurrencyERSymbol());
		/* TODO: replace subdivision by company in "reevaluate price" */
		model.addAttribute("subdivId", quotation.getOrganisation().getDefaultSubdiv().getSubdivid());
		return "trescal/core/quotation/toggleinstrumentinbasket";
	}
}