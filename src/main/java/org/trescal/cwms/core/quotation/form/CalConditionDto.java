package org.trescal.cwms.core.quotation.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CalConditionDto {
    private Integer calTypeId;
	private ServiceType serviceType;
	private String condition;
}
