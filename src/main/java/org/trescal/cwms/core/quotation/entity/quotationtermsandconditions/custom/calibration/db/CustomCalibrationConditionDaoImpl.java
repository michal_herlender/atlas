package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.calibration.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.calibration.CustomCalibrationCondition;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.calibration.CustomCalibrationCondition_;

@Repository("CustomCalibrationConditionDao")
public class CustomCalibrationConditionDaoImpl extends BaseDaoImpl<CustomCalibrationCondition, Integer>
		implements CustomCalibrationConditionDao {
	@Override
	protected Class<CustomCalibrationCondition> getEntity() {
		return CustomCalibrationCondition.class;
	}

	@Override
	public List<CustomCalibrationCondition> getListCustomCalibrationConditions(List<Integer> ids) {
		return getResultList(cb -> {
			CriteriaQuery<CustomCalibrationCondition> cq = cb.createQuery(CustomCalibrationCondition.class);
			Root<CustomCalibrationCondition> root = cq.from(CustomCalibrationCondition.class);
			cq.where(root.get(CustomCalibrationCondition_.defCalConId).in(ids));
			return cq;
		});
	}
}