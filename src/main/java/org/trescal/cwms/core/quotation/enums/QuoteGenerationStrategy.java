package org.trescal.cwms.core.quotation.enums;

public enum QuoteGenerationStrategy
{
	SINGLE_QITEM_PER_INSTRUMENT,
	SINGLE_QITEM_PER_INSTRUMENT_WITH_PLANTNOS,
	SINGLE_QITEM_PER_MODEL,
	SINGLE_QITEM_PER_MODEL_WITH_INSTRUMENT_QTY;
}
