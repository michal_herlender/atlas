package org.trescal.cwms.core.quotation.entity.quotationitem.comparators;

import java.util.Comparator;

import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;

public class QuotationItemIdComparator implements Comparator<Quotationitem>
{
	@Override
	public int compare(Quotationitem o1, Quotationitem o2)
	{
		return ((Integer) o1.getId()).compareTo(o2.getId());
	}
}