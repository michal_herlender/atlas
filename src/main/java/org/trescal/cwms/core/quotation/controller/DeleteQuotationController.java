package org.trescal.cwms.core.quotation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;

@Controller
@JsonController
public class DeleteQuotationController {

	@Autowired
	private QuotationService quotationService;

	@RequestMapping(value = "/deleteQuotation.json", method = RequestMethod.POST)
	@ResponseBody
	protected ResultWrapper updateQuotationItems(
			@RequestParam(name = "quoteId", required = true) Integer quoteId) throws Exception {

		return this.quotationService.ajaxDeleteQuotation(quoteId);
	}
}
