package org.trescal.cwms.core.quotation.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationdoccustomtext.QuotationDocCustomText;
import org.trescal.cwms.core.quotation.entity.quotationdoccustomtext.db.QuotationDocCustomTextService;
import org.trescal.cwms.core.quotation.entity.quotationdocdefaulttext.QuotationDocDefaultText;
import org.trescal.cwms.core.quotation.entity.quotationdocdefaulttext.db.QuotationDocDefaultTextService;
import org.trescal.cwms.core.quotation.form.QuotationDocCustomTextForm;
import org.trescal.cwms.core.quotation.form.QuotationDocCustomTextValidator;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@Controller @IntranetController
public class QuotationDocCustomTextController
{
	@Autowired
	private QuotationDocCustomTextService customTextService;
	@Autowired
	private QuotationDocDefaultTextService defaultTextService;
	@Autowired
	private QuotationService quotationService;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private QuotationDocCustomTextValidator validator;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	@ModelAttribute("form")
	protected Object formBackingObject(Locale locale,
		@RequestParam(value="id", required=true) Integer quoteId) {
		// create new quote doc custom text form
		QuotationDocCustomTextForm form = new QuotationDocCustomTextForm();
		form.setQuotationId(quoteId);
		// Find existing custom text, if any
		QuotationDocCustomText customText = this.customTextService.get(quoteId);
		if (customText != null) {
			form.setUseCustomQuoteText(true);
			form.setSubject(customText.getSubject());
			form.setBody(customText.getBody());
		}
		else {
			form.setUseCustomQuoteText(false);
			// Populate defaults from global defaults for current locale
			QuotationDocDefaultText defaultText = this.defaultTextService.findDefaultText(locale);
			if (defaultText == null) {
				Locale primaryLocale = this.supportedLocaleService.getPrimaryLocale();
				defaultText = this.defaultTextService.findDefaultText(primaryLocale);
			}
			form.setBody(defaultText != null ? defaultText.getBody() : "");
			form.setSubject(defaultText != null ? defaultText.getSubject() : "");
		}
		return form;
	}

	@RequestMapping(value="/quotedocumenttext.htm", method=RequestMethod.GET)
	public String referenceData(Model model,
			@RequestParam(value="id", required=true) Integer quoteId) {
		
		model.addAttribute("customText", this.customTextService.get(quoteId));
		model.addAttribute("quotation", this.quotationService.get(quoteId));
		
		return "trescal/core/quotation/quotedocumenttext";
	}
	
	@RequestMapping(value="/quotedocumenttext.htm", method=RequestMethod.POST)
	public String onSubmit(Model model,
			@RequestParam(value="id", required=true) Integer quoteId,
			@Validated @ModelAttribute("form") QuotationDocCustomTextForm form,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return referenceData(model, quoteId);
		}
		this.customTextService.editCustomText(form);

		return "redirect:viewquotation.htm?id="+ form.getQuotationId();
	}
}
