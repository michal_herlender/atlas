package org.trescal.cwms.core.quotation.dto;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;

public class QuotationSummaryDTO {
	
	private int quoteid;
	private List<QuotationItemSummaryDTO> summaryByHeading;
	private List<QuotationItemSummaryDTO> summaryByServiceType;
	private List<QuotationItemSummaryDTO> summaryByHeadingAndServiceType;

	public final static Logger logger = LoggerFactory.getLogger(QuotationSummaryDTO.class);

	public QuotationSummaryDTO(int quoteid) {
		this.quoteid = quoteid;
	}

	public void setSummaryByHeading(List<QuotationItemSummaryDTO> summaryByHeading) {
		this.summaryByHeading = summaryByHeading;
		printDebug("summaryByHeading", this.summaryByHeading);
	}

	public void setSummaryByServiceType(List<QuotationItemSummaryDTO> summaryByServiceType) {
		this.summaryByServiceType = summaryByServiceType;
		printDebug("summaryByServiceType", this.summaryByServiceType);
	}

	public void setSummaryByHeadingAndServiceType(List<QuotationItemSummaryDTO> summaryByHeadingAndServiceType) {
		this.summaryByHeadingAndServiceType = summaryByHeadingAndServiceType;
		printDebug("summaryByHeadingAndServiceType", this.summaryByHeadingAndServiceType);
	}

	private void printDebug(String listName, List<QuotationItemSummaryDTO> list) {
		logger.debug(listName + ".size(): " + list.size());
		for (QuotationItemSummaryDTO dto : list) {
			logger.debug("Service Type: " + dto.getServiceType() + " Heading: "
					+ dto.getHeading() + "Count Items: " + dto.getCountItems());
			if (dto.getTotalFinalCost() != null)
				logger.debug(dto.getTotalFinalCost().toString());
		}
	}

	public QuotationItemSummaryDTO getSummaryForHeading(int headingid) {
		if (this.summaryByHeading == null)
			throw new RuntimeException("No summaryByHeading for quoteid " + quoteid);
		QuotationItemSummaryDTO result = null;
		for (QuotationItemSummaryDTO dto : this.summaryByHeading) {
			if (dto.getHeading().getHeadingId() == headingid) {
				result = dto;
				break;
			}
		}
		return result;
	}

	public QuotationItemSummaryDTO getSummaryForHeadingAndServiceType(int headingid, int serviceTypeId) {
		if (this.summaryByHeadingAndServiceType == null)
			throw new RuntimeException("No summaryByHeadingAndServiceType for quoteid " + quoteid);
		QuotationItemSummaryDTO result = null;
		for (QuotationItemSummaryDTO dto : this.summaryByHeadingAndServiceType) {
			if ((dto.getHeading().getHeadingId() == headingid) && (dto.getServiceType().getServiceTypeId() == serviceTypeId)) {
				result = dto;
				break;
			}
		}
		return result;
	}

	public QuotationItemSummaryDTO getSummaryForServiceType(int serviceTypeId) {
		if (this.summaryByServiceType == null)
			throw new RuntimeException("No summaryByServiceType for quoteid " + quoteid);
		QuotationItemSummaryDTO result = null;
		for (QuotationItemSummaryDTO dto : this.summaryByServiceType) {
			if (dto.getServiceType().getServiceTypeId() == serviceTypeId) {
				result = dto;
				break;
			}
		}
		return result;
	}

	public boolean isServiceTypeInQuoteHeading(int headingId, int serviceTypeId) {
		QuotationItemSummaryDTO summary = getSummaryForHeadingAndServiceType(headingId, serviceTypeId);
		return (summary != null) && (summary.getCountItems() > 0);
	}

	public boolean isServiceTypeInQuotation(int serviceTypeId) {
		QuotationItemSummaryDTO summary = getSummaryForServiceType(serviceTypeId);
		return (summary != null) && (summary.getCountItems() > 0);
	}

	// Drop in replacement methods for pricingtool
	public BigDecimal calculateQuoteHeadingServiceTypeTotal(QuoteHeading heading, int serviceTypeId) {
		QuotationItemSummaryDTO summary = getSummaryForHeadingAndServiceType(heading.getHeadingId(), serviceTypeId);
		return summary == null ? new BigDecimal("0.00") : summary.getTotalFinalCost();
	}

	public BigDecimal calculateQuoteHeadingTotal(QuoteHeading qh) {
		QuotationItemSummaryDTO summary = getSummaryForHeading(qh.getHeadingId());
		return summary == null ? new BigDecimal("0.00") : summary.getTotalFinalCost();
	}

	public BigDecimal calculateQuoteServiceTypeTotal(Quotation q, int serviceTypeId) {
		QuotationItemSummaryDTO summary = getSummaryForServiceType(serviceTypeId);
		return summary == null ? new BigDecimal("0.00") : summary.getTotalFinalCost();
	}
}