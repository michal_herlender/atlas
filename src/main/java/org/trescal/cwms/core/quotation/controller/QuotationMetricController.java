package org.trescal.cwms.core.quotation.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.form.QuotationMetricForm;

@Controller
@IntranetController
public class QuotationMetricController {
    @Autowired
    private ContactService contactServ;
    @Autowired
    private QuotationService quoteServ;
    
    public static final String FORM_NAME = "command";

    @ModelAttribute(FORM_NAME)
	protected QuotationMetricForm formBackingObject() throws Exception
	{
		QuotationMetricForm form = new QuotationMetricForm();
		form.setBusinessContacts(this.contactServ.getBusinessContacts(false));
		// this is the first visit to this form, set default range as 31 days
		if (!form.isExists()) {
            LocalDate today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
            form.setRegTo(today);
            form.setRegFrom(today.minusMonths(1));
            form.setExists(true);
        }
		return form;
	}
	
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) throws Exception
	{
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
	}
	
	@RequestMapping(value="/quotationmetric.htm")
	protected ModelAndView onSubmit(@ModelAttribute(FORM_NAME) QuotationMetricForm form) throws Exception
	{
		form.setMetrics(this.quoteServ.loadMetrics(form));
		return new ModelAndView("trescal/core/quotation/quotationmetric", FORM_NAME, form);
	}

}