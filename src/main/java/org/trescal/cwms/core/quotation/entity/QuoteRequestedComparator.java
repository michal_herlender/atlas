package org.trescal.cwms.core.quotation.entity;

import java.util.Comparator;

public class QuoteRequestedComparator implements Comparator<QuoteRequested>
{
	@Override
	public int compare(QuoteRequested qr1, QuoteRequested qr2)
	{
		if (qr1.getQuoteType().equals(qr2.getQuoteType()))
		{
			if ((qr1.getReqdate() == null) && (qr2.getReqdate() == null))
			{
				return ((Integer) qr1.getId()).compareTo(qr2.getId());
			}
			else if ((qr1.getReqdate() != null) && (qr2.getReqdate() == null))
			{
				return 1;
			}
			else if ((qr1.getReqdate() == null) && (qr2.getReqdate() != null))
			{
				return -1;
			}
			else
			{
				if (qr1.getReqdate().compareTo(qr2.getReqdate()) == 0)
				{
					return ((Integer) qr1.getId()).compareTo(qr2.getId());
				}
				else
				{
					return qr1.getReqdate().compareTo(qr2.getReqdate());
				}
			}
		}
		else
		{
			return ((Integer) qr1.getQuoteType().ordinal()).compareTo(qr2.getQuoteType().ordinal());
		}
	}
}
