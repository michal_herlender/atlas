package org.trescal.cwms.core.quotation.form;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quoteheading.QuoteHeading;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AddInstModelToQuoteForm {
	private List<String> calcosts;
	private List<String> caltype;
	private List<CalibrationType> caltypes;
	private List<String> sourceCost = new ArrayList<String>();
	private List<Quotationitem> chosenModels;
	// added this value to the form because when retrieved on the velocity page
	// a lazy initialisation exception is encountered
	private Integer coid;
	private boolean defaultAddModules;

	private String salesCat;
	private Integer salesCatId;
	private String domainNm;
	private Integer domainId;
	private String familyNm;
	private Integer familyId;

	private List<Integer> defaultCaltypes;

	private SupportedCurrency defaultCurrency;
	private int defaultQuoteHeading;
	private int defaultQuoteQty;
	private String desc;
	private Integer descid;
	private List<Integer> headingids;

	private Set<QuoteHeading> headings;
	private Integer id;
	private List<Integer> ids;

	private String instmodel;

	// basketitem variable lists
	private List<Integer> itemnos;

	private int maxItemNo;
	private int maxQuantity;
	private List<Integer> quantityRange;

	private String mfrtext;

	private Integer mfrId;

	// baseketitem module lists
	private List<String> moduleclasstypes;
	private List<Integer> moduleitemnos;
	private List<Integer> modulemodelids;

	private List<Integer> modulepartofs;
	private List<String> plantnos;
	private String qno;
	private List<Integer> quantities;
	private Quotation quotation;

	private InstrumentAndModelSearchForm<InstrumentModel> search;
	private List<InstrumentModel> searchModels;
	private boolean showmodeltypes;
	private int startRow;

	private String submitted;

	private Integer ver;

	private Integer modelTypeSelectorChoice;

}