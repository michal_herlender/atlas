package org.trescal.cwms.core.quotation.entity.quotationitem;

import java.util.Comparator;

public class TempQuotationItemComparator implements Comparator<Quotationitem>
{
	public int compare(Quotationitem q1, Quotationitem q2)
	{
		// if (q1.getCaltype().getOrderBy() != q2.getCaltype().getOrderBy())
		// {
		// return
		// q1.getCaltype().getOrderBy().compareTo(q2.getCaltype().getOrderBy());
		// }
		// else
		// {
		// int item1 = q1.getItemno();
		// int item2 = q2.getItemno();
		//
		// if (item1 != item2)
		// {
		// return ((Integer) item1).compareTo(item2);
		// }
		// else
		// {
		// return ((Integer) q1.getId()).compareTo(q2.getId());
		// }
		// }

		return ((Integer) q1.getId()).compareTo(q2.getId());
	}
}
