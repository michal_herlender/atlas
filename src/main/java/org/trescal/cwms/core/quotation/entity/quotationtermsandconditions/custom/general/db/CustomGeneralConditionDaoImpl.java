package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.general.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.custom.general.CustomGeneralCondition;

@Repository("CustomGeneralConditionDao")
public class CustomGeneralConditionDaoImpl extends AllocatedToCompanyDaoImpl<CustomGeneralCondition, Integer> implements CustomGeneralConditionDao
{
	@Override
	protected Class<CustomGeneralCondition> getEntity() {
		return CustomGeneralCondition.class;
	}
}