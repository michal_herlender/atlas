package org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.quotation.entity.quotationtermsandconditions.defaults.calibration.DefaultCalibrationCondition;

@Service("DefaultCalibrationConditionService")
public class DefaultCalibrationConditionServiceImpl extends BaseServiceImpl<DefaultCalibrationCondition, Integer> implements DefaultCalibrationConditionService
{
	@Autowired
	private DefaultCalibrationConditionDao defaultCalibrationConditionDao;
	
	@Override
	protected BaseDao<DefaultCalibrationCondition, Integer> getBaseDao() {
		return defaultCalibrationConditionDao;
	}
	
	@Override
	public DefaultCalibrationCondition getLatest(int type, Company company) {
		return this.defaultCalibrationConditionDao.getLatest(type, company);
	}
}