package org.trescal.cwms.core.quotation.form;

import java.math.BigDecimal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class EditQuotationItemValidator extends AbstractBeanValidator
{
	protected final Log logger = LogFactory.getLog(this.getClass());
	private final int MAXPRICE = 10000000;
	private final int MAXQUANTITY = 2000;

	public boolean supports(Class<?> clazz)
	{
		return EditQuotationItemForm.class.isAssignableFrom(clazz);
	}

	public void validate(Object obj, Errors errors)
	{
		EditQuotationItemForm form = (EditQuotationItemForm) obj;
		Quotationitem quoteItemData = form.getQuotationitem();
		if (!form.getEditable()) errors.reject("error.performancerestrictions", "This operation cannot be performed due to performance restrictions.");

		if (quoteItemData == null)
		{
			errors.reject("error.nullpointer", "Null data received");
		}
		else
		{
			// validate the calibration cost fields, if present
			if (quoteItemData.getCalibrationCost() != null) {
				errors.pushNestedPath("quotationitem.calibrationCost");
				super.validate(quoteItemData.getCalibrationCost(), errors);
				errors.popNestedPath();
			}
			
			// validate the purchase cost fields, if present
			if (quoteItemData.getPurchaseCost() != null) {
				errors.pushNestedPath("quotationitem.purchaseCost");
				super.validate(quoteItemData.getPurchaseCost(), errors);
				errors.popNestedPath();
			}
			
			// validate the plantno length
			if ((quoteItemData.getPlantno() != null)
					&& (quoteItemData.getPlantno().length() > 50))
			{
				errors.rejectValue("quotationitem.plantno", "error.field-length-too-long", new Object[] { new Integer(50) }, "This the text you entered was too long");
			}

			// validate the quantity
			if (quoteItemData.getQuantity() == null)
			{
				errors.rejectValue("quotationitem.quantity", "error.quotationitem.qty.notnull", null, "You must set a value for the quantity");
			}
			else
			{
				if (!quoteItemData.getQuantity().toString().matches("[0-9]*"))
				{
					errors.rejectValue("quotationitem.quantity", "error.quotationitem.quantity.letters", null, "The quantity cannot contain letters");
				}
				else if (quoteItemData.getQuantity() > this.MAXQUANTITY)
				{
					errors.rejectValue("quotationitem.quantity", "error.quotationitem.quantity.size", new Object[] { new Integer(this.MAXQUANTITY) }, "The quantity cannot excede "
							+ this.MAXQUANTITY);
				}
			}

			// in-house calprice
			if (quoteItemData.getTotalCost() == null)
			{
				quoteItemData.setTotalCost(new BigDecimal(0.00));
			}
			else
			{
				if ((quoteItemData.getTotalCost().doubleValue() > this.MAXPRICE)
						|| (quoteItemData.getTotalCost().doubleValue() < -this.MAXPRICE))
				{
					errors.rejectValue("quotationitem.houseCalPrice", "error.quotationitem.price.size", new Object[] { new Integer(this.MAXPRICE) }, "The calibration price is too large");
				}
				if (quoteItemData.getTotalCost().scale() > 2)
				{
					errors.rejectValue("quotationitem.houseCalPrice", "error.quotationitem.price.decimal", new Object[] { new Integer(2) }, "The price is to two decimal places only");
				}
			}

			/*
			 * // inspection if(quoteItemData.getInspection() == null) {
			 * quoteItemData.setInspection(new BigDecimal(0.00)); } else {
			 * if(quoteItemData.getInspection().doubleValue() > MAXPRICE ||
			 * quoteItemData.getInspection().doubleValue() < -MAXPRICE) {
			 * errors.rejectValue("inspection",
			 * "error.quotationitem.price.size", new Object[] {new
			 * Integer(MAXPRICE)}, "The inspection charge is too large"); }
			 * if(quoteItemData.getInspection().scale() > 2) {
			 * errors.rejectValue("inspection",
			 * "error.quotationitem.price.decimal", new Object[] {new
			 * Integer(2)}, "The inspection charge is two decimal places only");
			 * } }
			 */
			// discount
			if (quoteItemData.getGeneralDiscountRate() == null)
			{
				quoteItemData.setGeneralDiscountRate(new BigDecimal(0.00));
			}
			else
			{
				if (quoteItemData.getGeneralDiscountRate().scale() > 2)
				{
					errors.rejectValue("quotationitem.discount", "error.quotationitem.price.decimal", new Object[] { new Integer(2) }, "The discount is to two decimal places only");
				}
			}

			/*
			 * // third party manual calibration
			 * if(quoteItemData.getThirdCalPrice() == null) {
			 * quoteItemData.setThirdCalPrice(new BigDecimal(0.00)); } else {
			 * if(quoteItemData.getThirdCalPrice().doubleValue() > MAXPRICE ||
			 * quoteItemData.getThirdCalPrice().doubleValue() < -MAXPRICE) {
			 * errors.rejectValue("thirdCalPrice",
			 * "error.quotationitem.price.size", new Object[] {new
			 * Integer(MAXPRICE)}, "The third party calibration price is too
			 * large"); } if(quoteItemData.getThirdCalPrice().scale() > 2) {
			 * errors.rejectValue("thirdCalPrice",
			 * "error.quotationitem.price.decimal", new Object[] {new
			 * Integer(2)}, "The price is to two decimal places only"); } } //
			 * third party calibration markup rate
			 * if(quoteItemData.getThirdCalPriceMarkupRate() == null) {
			 * quoteItemData.setThirdCalPriceMarkupRate(new BigDecimal(0.00)); }
			 * else { if(quoteItemData.getThirdCalPriceMarkupRate().scale() > 2)
			 * { errors.rejectValue("thirdCalPriceMarkupRate",
			 * "error.quotationitem.price.decimal", new Object[] {new
			 * Integer(2)}, "The markup rate is to two decimal places only"); }
			 * }
			 */
		}
	}
}

/*
 * if(quoteItemData.getDiscount().doubleValue() > MAXPRICE ||
 * quoteItemData.getDiscount().doubleValue() < -MAXPRICE) {
 * errors.rejectValue("discount", "error.quotationitem.discount.size", new
 * Object[] {new Integer(MAXPRICE)}, "The discount is too large"); }
 */
