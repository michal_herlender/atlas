package org.trescal.cwms.core.quotation.form;

import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotationitem.comparators.QuotationItemSortType;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditQuotationItemSortControllerForm
{
	private Quotation quotation;
	private QuotationItemSortType sortType;

}
