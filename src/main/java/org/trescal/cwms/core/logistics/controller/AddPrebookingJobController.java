package org.trescal.cwms.core.logistics.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.documents.excel.utils.ExcelFileReaderUtil;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.utils.ExchangeFormatGeneralValidator;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.logistics.form.PrebookingJobForm;
import org.trescal.cwms.core.logistics.form.validator.AddPrebookingJobFormValidator;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes(value = { Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME })
public class AddPrebookingJobController {

	@Value("#{props['cwms.config.address.use_locations']}")
	private boolean useLocations;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SupportedCurrencyService currencyServ;
	@Autowired
	private UserService userService;
	@Autowired
	private ExchangeFormatService exchangeFormatServ;
	@Autowired
	private AddPrebookingJobFormValidator validator;
	@Autowired
	private ExcelFileReaderUtil excelFileReaderUtil;
	@Autowired
	private ExchangeFormatGeneralValidator efValidator;

	public static final String FORM = "pbjf";

	@InitBinder("pbjf")
	protected void initBinder(ServletRequestDataBinder binder) throws Exception {
		binder.setValidator(validator);
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
	}

	@ModelAttribute("pbjf")
	protected PrebookingJobForm formBackingObject(HttpServletRequest request,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@RequestParam(value = "basedonjobid", required = false, defaultValue = "0") Integer basedOnId,
			@RequestParam(value = "calloffcoid", required = false, defaultValue = "0") Integer callOffItemComp,
			@RequestParam(value = "collectedcoid", required = false, defaultValue = "0") Integer collectedComp,
			@RequestParam(value = "plantid", required = false, defaultValue = "0") Integer barcode,
			@RequestParam(value = "items", required = false) String itemStr,
			@RequestParam(value = "insts", required = false) String instStr) throws ServletRequestBindingException {
		PrebookingJobForm pbjf = new PrebookingJobForm();
		Contact currentCon = this.userService.get(username).getCon();
		Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
		// If current contact's subdiv is same, can use his/her preferences if
		// available
		if (currentCon.getSub().getId().intValue() == allocatedSubdiv.getId().intValue()) {
			if (currentCon.getDefAddress() != null) {
				pbjf.setBookedInAddrId(currentCon.getDefAddress().getAddrid());
			}
			if (currentCon.getDefLocation() != null)
				pbjf.setBookedInLocId(currentCon.getDefLocation().getLocationid());
		}
		// Otherwise default booking in address should come from subdiv
		if ((pbjf.getBookedInAddrId() == null) && (allocatedSubdiv.getDefaultAddress() != null)) {
			pbjf.setBookedInAddrId(allocatedSubdiv.getDefaultAddress().getAddrid());
		}

		pbjf.setEstCarriageOut(new BigDecimal("0.00"));

		if (currentCon.getUserPreferences() != null) {
			pbjf.setJobType(currentCon.getUserPreferences().getDefaultJobType());
		} else {
			pbjf.setJobType(JobType.STANDARD);
		}

		// set POs
		List<String> poNumberList = new ArrayList<>();
		List<String> poCommentList = new ArrayList<>();
		int i = 1;
		while (ServletRequestUtils.getStringParameter(request, "polist[" + i + "]") != null) {
			poNumberList.add(ServletRequestUtils.getStringParameter(request, "polist[" + i + "]"));
			poCommentList.add(ServletRequestUtils.getStringParameter(request, "pocomlist[" + i + "]"));
			i++;
		}
		pbjf.setPoNumberList(poNumberList);
		pbjf.setPoCommentList(poCommentList);

		return pbjf;
	}

	@RequestMapping(value = "/addprebookingjob.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username)
			throws Exception {

		HashMap<String, Object> refData = new HashMap<>();
		List<Address> addresses = new ArrayList<>();
		List<Location> locations = new ArrayList<>();
		User currentUser = this.userService.get(username);
		Set<Subdiv> userSubdivs = currentUser.getUserRoles().stream().map(Allocated::getOrganisation)
				.collect(Collectors.toSet());
		for (Subdiv subdiv : userSubdivs) {
			List<Address> subdivAddresses = subdiv.getAddresses().stream().filter(Address::getActive)
					.collect(Collectors.toList());
			addresses.addAll(subdivAddresses);
			for (Address address : subdivAddresses)
				locations.addAll(address.getLocations());
		}
		refData.put("businessAddresses", addresses);
		refData.put("businessLocations", locations);
		refData.put("useLocations", this.useLocations);
		refData.put("currencyList", this.currencyServ.getAllSupportedCurrenciesWithExRates());
		refData.put("jobTypes", JobType.getActiveJobTypes());
		return new ModelAndView("trescal/core/jobs/job/addprebookingjob", refData);
	}

	@RequestMapping(value = "/addprebookingjob.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(HttpServletRequest request,
			@RequestParam(name = "defaultpo", required = false, defaultValue = "0") Integer defaultPO,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@Valid @ModelAttribute("pbjf") PrebookingJobForm pbjf, BindingResult bindingResult,
			final RedirectAttributes redirectAttributes, Errors errors) throws Exception {
		if (bindingResult.hasErrors())
			return referenceData(username);
		else {
			// get exchange format
			ExchangeFormat ef = exchangeFormatServ.get(pbjf.getExchangeFormat());

			// get file content
			List<LinkedCaseInsensitiveMap<String>> fileContent = excelFileReaderUtil
					.readExcelFile(pbjf.getUploadFile().getInputStream(), ef.getSheetName(), ef.getLinesToSkip());

			// validate file content with exchange format
			if (efValidator.validate(ef, fileContent, bindingResult).hasErrors()) {
				return referenceData(username);
			}

			pbjf.setDefaultPO(defaultPO);

			// add BPO
			int bpoid = ServletRequestUtils.getIntParameter(request, "bpo", 0);
			if (bpoid != 0)
				pbjf.setBpo(bpoid);

			// set currency
			Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
			pbjf.setCurrencyCode(allocatedSubdiv.getComp().getCurrency().getCurrencyCode());

			redirectAttributes.addFlashAttribute("pbjf", pbjf);
			redirectAttributes.addFlashAttribute("fileContent", fileContent);

			return new ModelAndView(new RedirectView("effilesynthesis.htm"));
		}
	}
}