package org.trescal.cwms.core.logistics.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;

@Controller @IntranetController
@SessionAttributes(value = { Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class NewScanInOutItemsController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private SubdivService subdivservice;
	
	
	@RequestMapping(value="newscanout.htm")
	public String viewScanOut(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivKeyValue) {
		model.addAttribute("defaultAddressId",getdefaultAddress(username,subdivKeyValue.getKey()));
		return "trescal/core/logistics/newScanInOutItems";
	}
	
	
	@RequestMapping(value="newscanin.htm")
	public String viewScanIn(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivKeyValue) {
		model.addAttribute("defaultAddressId",getdefaultAddress(username,subdivKeyValue.getKey()));
		return "trescal/core/logistics/newScanInOutItems";
	}
	
	
	private Integer getdefaultAddress(String username,Integer subdivId){
		User user = userService.get(username);
		Subdiv subdiv = subdivservice.get(subdivId);
		Address defaultAddress = subdiv.getDefaultAddress();
		if (defaultAddress == null)
			defaultAddress = subdiv.getAddresses().stream().findFirst().orElse(null);
		if (defaultAddress == null)
			defaultAddress = user.getCon().getDefAddress();
		return defaultAddress.getAddrid();

	}
	
	

}
