package org.trescal.cwms.core.logistics.form;


import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.tools.PagedResultSet;

public class SearchPrebookingForm
{
	private Integer coid;
	private String coname;
	private Integer bsubdivid;
	private Integer plantid;
	private String plantno;
	private Boolean associatedWithJob;
	private PagedResultSet<Asn> rs;
	
	public Integer getCoid() {
		return coid;
	}
	public void setCoid(Integer coid) {
		this.coid = coid;
	}
	public Integer getPlantid() {
		return plantid;
	}
	public void setPlantid(Integer plantid) {
		this.plantid = plantid;
	}
	public PagedResultSet<Asn> getRs() {
		return rs;
	}
	public void setRs(PagedResultSet<Asn> rs) {
		this.rs = rs;
	}
	public Integer getBsubdivid() {
		return bsubdivid;
	}
	public void setBsubdivid(Integer bsubdivid) {
		this.bsubdivid = bsubdivid;
	}
	public String getPlantno() {
		return plantno;
	}
	public void setPlantno(String plantno) {
		this.plantno = plantno;
	}
	public String getConame() {
		return coname;
	}
	public void setConame(String coname) {
		this.coname = coname;
	}
	public Boolean getAssociatedWithJob() {
		return associatedWithJob;
	}
	public void setAssociatedWithJob(Boolean associatedWithJob) {
		this.associatedWithJob = associatedWithJob;
	}
}