package org.trescal.cwms.core.logistics.entity.asnitems.dao;

import java.text.ParseException;
import java.util.List;

import org.springframework.util.LinkedCaseInsensitiveMap;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.logistics.entity.asnitems.AsnItem;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;

public interface AsnitemService extends BaseService<AsnItem, Integer> {

	AsnItem getAsnItemByIndex(Integer asnId, Integer index);

	public List<AsnItemFinalSynthesisDTO> convertFileContentToJobOrAsnItemFinalSynthesisDTO(
			ExchangeFormat exchangeFormat,
			List<LinkedCaseInsensitiveMap<String>> fileContent) throws ParseException;
}
