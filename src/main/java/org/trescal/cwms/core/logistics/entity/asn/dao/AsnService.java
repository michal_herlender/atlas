package org.trescal.cwms.core.logistics.entity.asn.dao;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.logistics.dto.AsnDTO;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;
import org.trescal.cwms.core.logistics.form.PrebookingJobForm;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public interface AsnService extends BaseService<Asn, Integer> {

    List<Asn> getPrebookingByPlantId(Integer plantId);

    PagedResultSet<Asn> findPrebooking(Boolean associatedWithJob, Integer businessSubdivId,
                                       Integer clientCompanyId, Integer plantId, String plantNo, Integer resultsPerPage, Integer currentPage);

    List<AsnDTO> findAsn(Integer clientContactId, Integer plantid, String plantno);

    Asn savePreBookingASN(PrebookingJobForm synthForm, List<AsnItemFinalSynthesisDTO> list, Contact userContact)
        throws IOException;

    Asn findAsnByJobId(Integer jobid);

    List<AsnItemFinalSynthesisDTO> analyseJobOrAsnItemsList(PrebookingJobForm pbjf, Integer allcoatedSubdivId,
                                                            Locale locale, Asn asn, List<AsnItemFinalSynthesisDTO> aiFinalSynthDtoListe);

    Integer asnItemsSize(Integer asnId);

    List<Asn> getAsnByAliasGroup(AliasGroup aliasGroup);

    List<Asn> getAllAsnActiveByExchangeFormatId(Integer exchangeFormatId);

}
