package org.trescal.cwms.core.logistics.utils;

public enum SourceAPIEnum {
	
	CWMS("Cwms"),
	ADVESO("Adveso"),
	MOBILE_APP("Mobile Application");
	
private String fullName;
	
	private SourceAPIEnum(String fullName) {
		this.fullName = fullName;
	}

	public String getFullName() {
		return fullName;
	}
	
	public static SourceAPIEnum parseFullName(String fullName) {
		SourceAPIEnum result = null;
		for (SourceAPIEnum status : SourceAPIEnum.values()) {
			if (status.getFullName().equals(fullName)) {
				result = status;
				break;
			}
		}
		return result;
	}
	
	public String getValue() {
		return name();
		}

		public void setValue(String value) {}

}