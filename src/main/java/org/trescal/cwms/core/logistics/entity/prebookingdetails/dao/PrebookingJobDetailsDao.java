package org.trescal.cwms.core.logistics.entity.prebookingdetails.dao;


import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails;

public interface PrebookingJobDetailsDao extends BaseDao<PrebookingJobDetails, Integer> {

}
