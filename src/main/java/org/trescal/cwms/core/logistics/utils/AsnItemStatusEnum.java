package org.trescal.cwms.core.logistics.utils;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum AsnItemStatusEnum {

	IDENTIFIED(true, "exchangeformat.import.itemstatus.identified"),
	IDENTIFIED_ON_OTHER_COMPANY_IN_SAME_GROUP(true, "exchangeformat.import.itemstatus.identifiedonothercompanyinsamegroup"),
	ON_OTHER_COMPANY(false, "exchangeformat.import.itemstatus.onothercompany"), 
	ALREADY_ONJOB(false, "exchangeformat.import.itemstatus.alreadyactive"),
	ALREADY_ONPREBOOKING(false, "exchangeformat.import.itemstatus.alreadybooked"),
	BER(false, "exchangeformat.import.itemstatus.ber"), 
	NOT_IDENTIFIED(false,"exchangeformat.import.itemstatus.notidentified"),
	NOT_IDENTIFIED_IN_CLIENT_COMPANY(false,"exchangeformat.import.itemstatus.notidentifiedinclientcompany"),
	MULTIPLE_INSTRUMENTS_WITH_SAME_PLANTNO(false,"exchangeformat.import.itemstatus.mulitpleinstrumentswithsameplantno"),
	MULTIPLE_INSTRUMENTS_WITH_SAME_SERIALNO(false, "exchangeformat.import.itemstatus.mulitpleinstrumentswithsameserialno"),
	DUPLICATED_IN_TABLE(false, "exchangeformat.import.itemstatus.duplicatedintable"), 
	INCOMPATIBLE_SERVICE_TYPE(false, "exchangeformat.import.itemstatus.incomaptibleservicetype");

	private final boolean success;
	private final String messageCode;
	private ReloadableResourceBundleMessageSource messageSource;

	AsnItemStatusEnum(boolean isSuccess, String messageCode) {
		this.success = isSuccess;
		this.messageCode = messageCode;
	}

	@Component
	private static class CompanyRoleMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (AsnItemStatusEnum e : EnumSet.allOf(AsnItemStatusEnum.class))
				e.messageSource = messageSource;
		}
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessage() {
		return messageSource.getMessage(messageCode, null, this.toString(), LocaleContextHolder.getLocale());
	}
	
	public String getMessageCode() {
		return messageCode;
	}

	public String getName(){
		return this.name();
	}

}