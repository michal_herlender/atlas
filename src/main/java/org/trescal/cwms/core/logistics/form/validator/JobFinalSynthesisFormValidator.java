package org.trescal.cwms.core.logistics.form.validator;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemServiceImpl;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;
import org.trescal.cwms.core.logistics.form.JobFinalSynthesisDTOForm;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateServiceImpl;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;

@Service
public class JobFinalSynthesisFormValidator extends AbstractBeanValidator {

	@Autowired
	JobItemServiceImpl jobItemService;
	@Autowired
	ServiceTypeService serviceTypeService;
	@Autowired
	ItemStateServiceImpl itemStateService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(JobFinalSynthesisDTOForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		JobFinalSynthesisDTOForm jfsf = (JobFinalSynthesisDTOForm) target;
		super.validate(target, errors);

		/* General validation */
		if (StringUtils.isEmpty(jfsf.getNewanalysis()) && (jfsf.getIdentifiedJobItemsFinalSynthDTO() == null
				|| !jfsf.getIdentifiedJobItemsFinalSynthDTO().stream().anyMatch(e -> e.isChecked()))) {
			errors.reject("finalsynthesis.validation.identifiedItems.emptylist", "Please add fields !");
		} else {

			Set<Integer> compatibleServiceTypeIds = serviceTypeService.getIdsByJobType(jfsf.getPbjf().getJobType());

			for (AsnItemFinalSynthesisDTO dto : jfsf.getIdentifiedJobItemsFinalSynthDTO()) {
				int dtoIndex = jfsf.getIdentifiedJobItemsFinalSynthDTO().indexOf(dto);
				errors.pushNestedPath("identifiedJobItemsFinalSynthDTO[" + dtoIndex + "]");

				if (dto.isChecked()) {
					// next status not selected
					if (dto.getNextStatuId() == null)
						errors.rejectValue("nextStatuId", "finalsynthesis.validation.identifiedItems.nextstatus",
								"Next Status empty for checked Item");

					// empty logistics comment if an hold status is selected
					if (dto.getNextStatuId() != null) {
						boolean onHoldStatus = jobItemService.stateHasGroupOfKeyName(
								itemStateService.findItemState(dto.getNextStatuId()), StateGroup.ONHOLD);
						if (StringUtils.isBlank(dto.getLogisticsComment()) && onHoldStatus)
							errors.rejectValue("nextStatuId", "finalsynthesis.validation.identifiedItems.comment",
									"Logistics Comment empty for checked Item");

					}

					// check if the service type of the dto (if provided) is compatible with the
					// job type
					if (dto.getServicetypeid() != null && !compatibleServiceTypeIds.contains(dto.getServicetypeid())) {
						errors.rejectValue("servicetypeid", "finalsynthesis.validation.servicetype.notcompatible",
								"Service type not compatible");
					}

				}
				errors.popNestedPath();
			}
		}
	}

}
