package org.trescal.cwms.core.logistics.entity.asnitems.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.Asn_;
import org.trescal.cwms.core.logistics.entity.asnitems.AsnItem;
import org.trescal.cwms.core.logistics.entity.asnitems.AsnItem_;

@Repository
public class AsnItemDaoImpl extends BaseDaoImpl<AsnItem, Integer> implements AsnItemDao {

	@Override
	protected Class<AsnItem> getEntity() {
		return AsnItem.class;
	}

	@Override
	public AsnItem getAsnItemByIndex(Integer asnId, Integer index) {
		
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<AsnItem> query = builder.createQuery(AsnItem.class);

		Root<AsnItem> asnRoot = query.from(AsnItem.class);

		Join<AsnItem, Asn> asnJoin = asnRoot.join(AsnItem_.asn.getName());

		Predicate conjunction = builder.conjunction();
		conjunction.getExpressions().add(builder.equal(asnRoot.get(AsnItem_.index.getName()), index));
		conjunction.getExpressions().add(builder.equal(asnJoin.get(Asn_.id.getName()), asnId));

		query.select(asnRoot);
		query.where(conjunction);

		return getEntityManager().createQuery(query).getResultList().stream().findAny().orElse(null);
	}


}
