package org.trescal.cwms.core.logistics.entity.asnitems.dao;

import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.db.AliasGroupService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfieldnamedetails.ExchangeFormatFieldNameDetails;
import org.trescal.cwms.core.logistics.entity.asnitems.AsnItem;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class AsnItemServiceImpl extends BaseServiceImpl<AsnItem, Integer> implements AsnitemService {

	@Autowired
	private AsnItemDao dao;

	@Qualifier("messageSource")
	@Autowired
	private MessageSource messages;

	@Autowired
	private AliasGroupService aliasGroupService;

	@Override
	protected BaseDao<AsnItem, Integer> getBaseDao() {
		return dao;
	}

	@Override
	public AsnItem getAsnItemByIndex(Integer asnId, Integer index) {
		return dao.getAsnItemByIndex(asnId, index);
	}

	@Override
	public List<AsnItemFinalSynthesisDTO> convertFileContentToJobOrAsnItemFinalSynthesisDTO(
			ExchangeFormat exchangeFormat, List<LinkedCaseInsensitiveMap<String>> fileContent)
			throws ParseException {

		AliasGroup aliasGroup = exchangeFormat.getAliasGroup();
		val sdf = new SimpleDateFormat(exchangeFormat.getDateFormat().getValue());
		List<AsnItemFinalSynthesisDTO> myList = new ArrayList<>();
		for (LinkedCaseInsensitiveMap<String> row : fileContent) {
			AsnItemFinalSynthesisDTO dto = new AsnItemFinalSynthesisDTO();
			dto.setUndefineds(new HashMap<>());
			for (String key : row.keySet()) {
				val efFieldNameOpt = exchangeFormat.getExchangeFormatFieldNameDetails().stream()
						.filter(e -> (e.getTemplateName() != null
								&& e.getTemplateName().trim().toLowerCase().equals(key.trim().toLowerCase()))
								|| (e.getFieldName().getValue().trim().toLowerCase().equals(key.trim().toLowerCase())))
						.map(ExchangeFormatFieldNameDetails::getFieldName).findFirst();

				 if(efFieldNameOpt.filter(_unused -> StringUtils.isNotBlank(row.get(key)))
						.isPresent()) {
				 	val efFieldName = efFieldNameOpt.get();
					switch (efFieldName) {
					case ID_TRESCAL:
						dto.setIdTrescal(Integer.valueOf(row.get(key)));
						break;
					case PLANT_NO:
						dto.setPlantNo(row.get(key));
						break;
					case CUSTOMER_DESCRIPTION:
						dto.setCustomerDescription(row.get(key));
						break;
					case BRAND:
						dto.setBrand(row.get(key));
						break;
					case MODEL:
						dto.setModel(row.get(key));
						break;
					case SERIAL_NUMBER:
						dto.setSerialNo(row.get(key));
						break;
					case TECHNICAL_REQUIREMENTS:
						dto.setTechnicalRequirements(row.get(key));
						break;
					case CUSTOMER_COMMENTS:
						dto.setCustomerComments(row.get(key));
						break;
					case DELIVERY_DATE:
							dto.setDeliveryDate(sdf.parse(row.get(key)));
						break;
					case URGENCY:
						dto.setUrgency(row.get(key));
						break;
					case UNDER_GUARANTEE:
						dto.setUnderGuarantee(row.get(key));
						break;
					case PO_NUMBER:
						dto.setPoNumber(row.get(key));
						break;
					case LOCATION:
						dto.setLocation(row.get(key));
						break;
					case SERVICETYPE_ID:
						dto.setServicetypeid(Integer.valueOf(row.get(key)));
						break;
					case UNDEFINED:
						dto.getUndefineds().put(key, row.get(key));
						break;
					case CLIENT_REF:
						dto.setJobitemClientRef(row.get(key));
						break;
					case CAL_FREQUENCY:
						if (StringUtils.isNotBlank(row.get(key)))
							dto.setCalibrationFrequency(Integer.valueOf(row.get(key)));
						break;
					case INTERVAL_UNIT:
						dto.setIntervalUnit(this.aliasGroupService.determineIntervalUnitValue(aliasGroup, row.get(key)));
						break;
					case EXPECTED_SERVICE:
						dto.setExpectedService(row.get(key));
						ServiceType st = this.aliasGroupService.determineExpectedServiceTypeFromAliasOrValue(aliasGroup, row.get(key));
						if (st != null)
							dto.setServicetypeid(st.getServiceTypeId());
						break;
					default:
						break;
					}
				 }
			}
			myList.add(dto);
		}
		return myList;
	}
}
