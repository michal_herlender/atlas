package org.trescal.cwms.core.logistics.entity.analysisresult.dao;


import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.logistics.entity.analysisresult.AsnItemAnalysisResult;

public interface AsnItemAnalysisResultDao extends BaseDao<AsnItemAnalysisResult, Integer> {

}
