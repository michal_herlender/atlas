package org.trescal.cwms.core.logistics.dto;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.utils.SourceAPIEnum;

import java.util.Date;

public class AsnDTO {

	private Integer id;
	private String jobType;
	private String sourceApi;
	private Integer exchangeFormatId;
	private Date createdOn;
	private String clientRef;
	private Integer bpoid;
	//data used when we want to add items to job already exist 
	private String createdByFirstName;
	private String createdByLastName;
	private String lastModifiedByFirstName;
	private String lastModifiedByLastName;
	private int itemsSize;
		
	public AsnDTO(Asn asn) {
		this.id = asn.getId();
		this.jobType = asn.getJobType().toString();
		this.sourceApi = asn.getSourceApi().getValue();
		this.exchangeFormatId = asn.getExchangeFormat().getId();
		if(asn.getPrebookingJobDetails().getBpo() != null)
		this.bpoid = asn.getPrebookingJobDetails().getBpo().getPoId();
	}
	
	
	public AsnDTO(int id, JobType jobType, SourceAPIEnum sourceApi, ExchangeFormat exchangeFormat, Date createdOn,
			String clientRef, Integer bpoid, Contact createdBy, Contact lastModifiedBy) {
		super();
		this.id = id;
		this.jobType = jobType.toString();
		this.sourceApi = sourceApi.getValue();
		this.exchangeFormatId = exchangeFormat.getId();
		this.createdOn = createdOn;
		this.clientRef = clientRef;
		this.bpoid = bpoid;
		this.createdByFirstName=createdBy.getFirstName();
		this.createdByLastName=createdBy.getLastName();
		this.lastModifiedByFirstName=lastModifiedBy.getFirstName();
		this.lastModifiedByLastName=lastModifiedBy.getLastName();

	}
	
	public int getItemsSize() {
		return itemsSize;
	}

	public void setItemsSize(int itemsSize) {
		this.itemsSize = itemsSize;
	}

	public String getCreatedByFirstName() {
		return createdByFirstName;
	}


	public void setCreatedByFirstName(String createdByFirstName) {
		this.createdByFirstName = createdByFirstName;
	}

	public String getCreatedByLastName() {
		return createdByLastName;
	}


	public void setCreatedByLastName(String createdByLastName) {
		this.createdByLastName = createdByLastName;
	}

	public String getLastModifiedByFirstName() {
		return lastModifiedByFirstName;
	}


	public void setLastModifiedByFirstName(String lastModifiedByFirstName) {
		this.lastModifiedByFirstName = lastModifiedByFirstName;
	}


	public String getLastModifiedByLastName() {
		return lastModifiedByLastName;
	}


	public void setLastModifiedByLastName(String lastModifiedByLastName) {
		this.lastModifiedByLastName = lastModifiedByLastName;
	}


	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getClientRef() {
		return clientRef;
	}

	public void setClientRef(String clientRef) {
		this.clientRef = clientRef;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getSourceApi() {
		return sourceApi;
	}

	public void setSourceApi(String sourceApi) {
		this.sourceApi = sourceApi;
	}

	public Integer getExchangeFormatId() {
		return exchangeFormatId;
	}

	public void setExchangeFormatId(Integer exchangeFormatId) {
		this.exchangeFormatId = exchangeFormatId;
	}
	
	public Integer getBpoid() {
		return bpoid;
	}

	public void setBpoid(Integer bpoid) {
		this.bpoid = bpoid;
	}

}
