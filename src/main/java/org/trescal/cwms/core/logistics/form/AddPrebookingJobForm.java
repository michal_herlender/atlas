package org.trescal.cwms.core.logistics.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor @AllArgsConstructor
public class AddPrebookingJobForm {

	private List<AsnItemFinalSynthesisDTO> asnItemsDTOList;
	private PrebookingJobForm prebookingJobForm;


}
