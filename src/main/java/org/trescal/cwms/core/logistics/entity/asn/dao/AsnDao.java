package org.trescal.cwms.core.logistics.entity.asn.dao;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.logistics.dto.AsnDTO;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.util.List;

public interface AsnDao extends BaseDao<Asn, Integer> {

    List<Asn> getNotLinkedPrebookingsByPlantId(Integer plantId);

    PagedResultSet<Asn> findPrebooking(Boolean associatedWithJob, Integer businessSubdivId, Integer clientCompanyId, Integer plantId, String plantNo, Integer resultsPerPage,
                                       Integer currentPage);

    List<AsnDTO> findAsn(Integer clientContactId, Integer plantid, String plantno);

    Asn findAsnByJobId(Integer jobid);

    Integer asnItemsSize(Integer asnId);

    List<Asn> getAsnByAliasGroup(AliasGroup aliasGroup);

    List<Asn> getAllAsnActiveByExchangeFormatId(Integer exchangeFormatId);
}
