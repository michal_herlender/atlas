package org.trescal.cwms.core.logistics.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.dao.AsnService;
import org.trescal.cwms.core.logistics.form.SearchPrebookingForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class PrebookingJobJsonController {

	@Autowired
	private AsnService asnService;

	public static final String FORM_NAME = "form";

	@ModelAttribute(FORM_NAME)
	public SearchPrebookingForm form(
			@RequestParam(name = "pageNo", required = false, defaultValue = "1") Integer pageNo) {
		SearchPrebookingForm form = new SearchPrebookingForm();
		PagedResultSet<Asn> rs = new PagedResultSet<>(0, 0);
		rs.setCurrentPage(pageNo);
		form.setRs(rs);
		return form;
	}

	@RequestMapping(value = "/manageprebooking.json", method = RequestMethod.POST)
	@ResponseBody
	protected List<Asn> onSubmitSearchForm(
			@RequestParam(name = "pageNo", required = false, defaultValue = "1") Integer pageNo,
			@ModelAttribute(FORM_NAME) SearchPrebookingForm form) throws Exception {
		PagedResultSet<Asn> list = asnService.findPrebooking(form.getAssociatedWithJob(), form.getBsubdivid(), form.getCoid(), form.getPlantid(), form.getPlantno(), form.getRs().getStartResultsFrom(), form.getRs().getResultsPerPage());
		return (List<Asn>) list.getResults();

	}

}