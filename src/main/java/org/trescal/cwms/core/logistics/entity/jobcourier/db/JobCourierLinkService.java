package org.trescal.cwms.core.logistics.entity.jobcourier.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.logistics.entity.jobcourier.JobCourierLink;

public interface JobCourierLinkService extends BaseService<JobCourierLink, Integer> {

}
