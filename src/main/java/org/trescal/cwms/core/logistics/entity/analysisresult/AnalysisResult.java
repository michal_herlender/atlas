package org.trescal.cwms.core.logistics.entity.analysisresult;


import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.logistics.utils.NextActionEnum;

//2018-06-06 - GB - Commenting out entity declaration, as create script doesn't exist yet
//@Entity
@Table(name = "analysisresult")
public class AnalysisResult extends Versioned {

	private Integer id;
	private NextActionEnum nextAction;
	private String comment;
	private JobItem jobItem;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "nextaction")
	public NextActionEnum getNextAction() {
		return nextAction;
	}

	@Column(name = "comment")
	public String getComment() {
		return comment;
	}
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid", nullable = true, foreignKey = @ForeignKey(name = "FK_jobitem_jobitemid"))
	public JobItem getJobItem() {
		return jobItem;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNextAction(NextActionEnum nextAction) {
		this.nextAction = nextAction;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setJobItem(JobItem jobItem) {
		this.jobItem = jobItem;
	}
	
}
