package org.trescal.cwms.core.logistics.entity.asnitems.dao;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.logistics.entity.asnitems.AsnItem;

public interface AsnItemDao extends BaseDao<AsnItem, Integer> {

	AsnItem getAsnItemByIndex(Integer asnId, Integer index);

}
