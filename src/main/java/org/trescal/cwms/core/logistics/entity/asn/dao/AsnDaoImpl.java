package org.trescal.cwms.core.logistics.entity.asn.dao;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup_;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO_;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.Job_;
import org.trescal.cwms.core.logistics.dto.AsnDTO;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.Asn_;
import org.trescal.cwms.core.logistics.entity.asnitems.AsnItem;
import org.trescal.cwms.core.logistics.entity.asnitems.AsnItem_;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails_;
import org.trescal.cwms.core.tools.PagedResultSet;

import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class AsnDaoImpl extends BaseDaoImpl<Asn, Integer> implements AsnDao {

	@Override
	protected Class<Asn> getEntity() {
		return Asn.class;
	}

	@Override
	public List<Asn> getNotLinkedPrebookingsByPlantId(Integer plantId) {

		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Asn> query = builder.createQuery(Asn.class);

		Root<Asn> asnRoot = query.from(Asn.class);
		Join<Asn, AsnItem> asnItemJoin = asnRoot.join(Asn_.asnItems.getName());
		Join<AsnItem, Instrument> instrumentJoin = asnItemJoin.join(AsnItem_.instrument.getName());

		Predicate conjunction = builder.conjunction();
		conjunction.getExpressions().add(builder.isNull(asnRoot.get(Asn_.job.getName())));
		conjunction.getExpressions().add(builder.equal(instrumentJoin.get(Instrument_.plantid.getName()), plantId));

		query.select(asnRoot);
		query.where(conjunction);

		return getEntityManager().createQuery(query).getResultList();
	}
	
	@Override
	public PagedResultSet<Asn> findPrebooking(Boolean associatedWithJob, Integer businessSubdivId, Integer clientCompanyId, Integer plantId, String plantNo, Integer resultsPerPage,
			Integer currentPage) {
		PagedResultSet<Asn> prs = new PagedResultSet<>(resultsPerPage, currentPage);
		super.completePagedResultSet(prs, Asn.class, cb -> cq -> {
			Root<Asn> asnRoot = cq.from(Asn.class);
			asnRoot.alias("asnRoot");

			Join<ExchangeFormat, Asn> exchangeFormatJoin = asnRoot.join(Asn_.exchangeFormat.getName(), JoinType.LEFT);

			Join<Asn, PrebookingJobDetails> prebookingJobDetailsJoin = asnRoot.join(Asn_.prebookingJobDetails.getName(),
					JoinType.LEFT);

			Predicate conjunction = cb.conjunction();

			if (businessSubdivId != null) {
				Join<PrebookingJobDetails, Address> businessAddressJoin = prebookingJobDetailsJoin
						.join(PrebookingJobDetails_.bookedInAddr.getName());
				Join<Address, Subdiv> businessSubdivJoin = businessAddressJoin.join(Address_.sub.getName());
				conjunction.getExpressions()
						.add(cb.equal(businessSubdivJoin.get(Subdiv_.subdivid.getName()), businessSubdivId));
			}

			if (clientCompanyId != null) {
				Join<ExchangeFormat, Company> clientCompanyJoin = exchangeFormatJoin
						.join(ExchangeFormat_.clientCompany.getName());
				conjunction.getExpressions()
						.add(cb.equal(clientCompanyJoin.get(Company_.coid.getName()), clientCompanyId));
			}

			if (plantId != null || (plantNo != null && !plantNo.trim().isEmpty())) {
				Join<Asn, AsnItem> asnItemJoin = asnRoot.join(Asn_.asnItems.getName());
				Join<AsnItem, Instrument> instrumentJoin = asnItemJoin.join(AsnItem_.instrument.getName());
				if (plantId != null)
					conjunction.getExpressions()
							.add(cb.equal(instrumentJoin.get(Instrument_.plantid.getName()), plantId));
				if (plantNo != null && !plantNo.trim().isEmpty())
					conjunction.getExpressions()
							.add(cb.equal(instrumentJoin.get(Instrument_.plantno.getName()), plantNo));
			}
			
			if(associatedWithJob != null && associatedWithJob)
				conjunction.getExpressions()
				.add(cb.isNotNull(asnRoot.get(Asn_.job.getName())));
			else
				conjunction.getExpressions()
				.add(cb.isNull(asnRoot.get(Asn_.job.getName())));
			
			cq.where(conjunction);
			List<Order> order = new ArrayList<>();
			order.add(cb.desc(asnRoot.get(Asn_.createdOn)));
			cq.distinct(true);
			return Triple.of(asnRoot.get(Asn_.id), null, order);
			});
		return prs;
	}


	@Override
	public List<AsnDTO> findAsn(Integer subdivId, Integer plantid, String plantno) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<AsnDTO> query = builder.createQuery(AsnDTO.class);

		Root<Asn> asnRoot = query.from(Asn.class);

		Join<Asn, PrebookingJobDetails> prebookingJobDetailsJoin = asnRoot.join(Asn_.prebookingJobDetails.getName());
		Join<PrebookingJobDetails, BPO> bpoJoin = prebookingJobDetailsJoin.join(PrebookingJobDetails_.bpo.getName(), JoinType.LEFT);
		Join<PrebookingJobDetails, Contact> clientContactJoin = prebookingJobDetailsJoin.join(PrebookingJobDetails_.con.getName());
		Join<Contact, Subdiv> subdivJoin = clientContactJoin.join(Contact_.sub.getName());

		Predicate conjunction = builder.conjunction();
		conjunction.getExpressions()
				.add(builder.equal(subdivJoin.get(Subdiv_.subdivid.getName()), subdivId));

		conjunction.getExpressions().add(builder.isNull(asnRoot.get(Asn_.job.getName())));

		if (plantid != null || (plantno != null && !plantno.trim().isEmpty())) {
			Join<Asn, AsnItem> asnItemJoin = asnRoot.join(Asn_.asnItems.getName());
			Join<AsnItem, Instrument> instrumentJoin = asnItemJoin.join(AsnItem_.instrument.getName());
			if (plantid != null)
				conjunction.getExpressions()
						.add(builder.equal(instrumentJoin.get(Instrument_.plantid.getName()), plantid));
			if (plantno != null && !plantno.trim().isEmpty())
				conjunction.getExpressions()
						.add(builder.equal(instrumentJoin.get(Instrument_.plantno.getName()), plantno));
		}

		query.where(conjunction);
		// get results
		query.select(builder.construct(AsnDTO.class, asnRoot.get(Asn_.id.getName()),
				asnRoot.get(Asn_.jobType.getName()), asnRoot.get(Asn_.sourceApi.getName()),
				asnRoot.get(Asn_.exchangeFormat.getName()),
				asnRoot.get(Asn_.createdOn.getName()),
				prebookingJobDetailsJoin.get(PrebookingJobDetails_.clientRef.getName()),
				bpoJoin.get(BPO_.poId.getName()), 
				prebookingJobDetailsJoin.get(PrebookingJobDetails_.con.getName()),
				asnRoot.get(Asn_.lastModifiedBy)
		)).distinct(true);

		return getEntityManager().createQuery(query).getResultList();
	}

	@Override
	public Asn findAsnByJobId(Integer jobid) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Asn> query = builder.createQuery(Asn.class);

		Root<Asn> asnRoot = query.from(Asn.class);
		Join<Asn, Job> jobJoin = asnRoot.join(Asn_.job.getName());

		query.select(asnRoot);
		query.where(builder.and(builder.isNotNull(asnRoot.get(Asn_.job.getName())),
				builder.equal(jobJoin.get(Job_.jobid.getName()), jobid)));

		return getEntityManager().createQuery(query).getResultList().stream().findFirst().orElse(null);
	}

	@Override
	public Integer asnItemsSize(Integer asnId) {
		Query query = getEntityManager()
				.createNativeQuery("SELECT (SELECT count(*) FROM dbo.asnitem ai WHERE ai.asnid = a.id) AS aicount "
						+ "FROM dbo.asn a WHERE a.id=" + asnId);		
		return (Integer)query.getSingleResult();
	}
	
	@Override
	public List<Asn> getAllAsnActiveByExchangeFormatId(Integer exchangeFormatId) {
		
		List<Asn> list = getResultList(cb -> {
			CriteriaQuery<Asn> cq = cb.createQuery(Asn.class);
			Root<Asn> asn = cq.from(Asn.class);
			Join<Asn, ExchangeFormat> ef = asn.join(Asn_.exchangeFormat);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(ef.get(ExchangeFormat_.id), exchangeFormatId));
			cq.where(clauses);
			return cq;
		});
		
		return list.stream().filter(asn->asn.getJob()==null).collect(Collectors.toList());
	}

	
	@Override
	public List<Asn> getAsnByAliasGroup(AliasGroup aliasGroup) {
		
		List<Asn> list = getResultList(cb -> {
			CriteriaQuery<Asn> cq = cb.createQuery(Asn.class);
			Root<Asn> asn = cq.from(Asn.class);
			Join<Asn, ExchangeFormat> ef = asn.join(Asn_.exchangeFormat);
			Join<ExchangeFormat, AliasGroup> ag = ef.join(ExchangeFormat_.aliasGroup);
			Predicate clausses = cb.conjunction();
			clausses.getExpressions().add(cb.equal(ag.get(AliasGroup_.id), aliasGroup.getId()));
			cq.where(clausses);
			return cq;
		});
		return list.stream().filter(asn->asn.getJob()==null).collect(Collectors.toList());
	}
}
