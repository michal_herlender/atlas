package org.trescal.cwms.core.logistics.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db.ExchangeFormatFileService;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.dao.AsnService;
import org.trescal.cwms.core.logistics.form.SearchPrebookingForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Controller
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY,
	Constants.SESSION_ATTRIBUTE_SUBDIV})
public class PrebookingJobController {

	@Autowired
	private AsnService asnService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private ExchangeFormatFileService efFileService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ServletContext servletContext;

	public static final String VIEW_MANA_NAME = "trescal/core/logistics/manageprebooking";
	public static final String VIEW_VIEW_NAME = "trescal/core/logistics/viewprebooking";
	public static final String FORM_NAME = "form";

	@ModelAttribute(FORM_NAME)
	public SearchPrebookingForm form(
			@RequestParam(name = "pageNo", required = false, defaultValue = "1") Integer pageNo) {
		SearchPrebookingForm form = new SearchPrebookingForm();
		PagedResultSet<Asn> rs = new PagedResultSet<>(0, 0);
		rs.setCurrentPage(pageNo);
		form.setRs(rs);
		return form;
	}

	@RequestMapping(value = "/manageprebooking.htm", method = { RequestMethod.GET, RequestMethod.POST })
	protected String formView(@RequestParam(name = "coid", required = false) Integer coid,
			@RequestParam(name = "bsubdivid", required = false) Integer bsubdivid,
			@ModelAttribute(FORM_NAME) SearchPrebookingForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdiv, Model model)
			throws ParseException {
		if (coid != null && coid != 0) {
			Company company = companyService.get(coid);
			if (company != null) {
				form.setCoid(company.getCoid());
				form.setConame(company.getConame());
			}
		}

		if (bsubdivid != null && bsubdivid != 0) {
			form.setBsubdivid(bsubdivid);
			model.addAttribute("selectedBusinessSubdivId", bsubdivid);
		} else {
			form.setBsubdivid(subdiv.getKey());
			model.addAttribute("selectedBusinessSubdivId", subdiv.getKey());
		}

		PagedResultSet<Asn> list = asnService.findPrebooking(form.getAssociatedWithJob(), form.getBsubdivid(),
				form.getCoid(), form.getPlantid(), form.getPlantno(), form.getRs().getResultsPerPage(),
				form.getRs().getCurrentPage());
		form.setRs(list);
		model.addAttribute("asnList", list.getResults());
		model.addAttribute("rs", form.getRs());

		Subdiv businessSub = subdivService.get(subdiv.getKey());
		if (businessSub != null) {
			List<Subdiv> businessSubdivs = subdivService.getAllActiveCompanySubdivs(businessSub.getComp());
			model.addAttribute("businessSubdivs", businessSubdivs);
		}
		return VIEW_MANA_NAME;
	}

	@RequestMapping(value = "/viewprebooking.htm")
	public ModelAndView viewPrebooking(@RequestParam(name = "asnid") Integer asnid) {
		Map<String, Asn> model = new HashMap<>();
		model.put("asn", asnService.get(asnid));
		return new ModelAndView(VIEW_VIEW_NAME, model);
	}

	@RequestMapping(value = "/deleteprebooking.htm")
	public ModelAndView deletePrebooking(@RequestParam(name = "coid", required = false) Integer coid,
										 @RequestParam(name = "asnid") Integer asnid,
										 @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdiv, Model model) {

		Asn asn = asnService.get(asnid);
		if (asn != null && asn.getJob() == null)
			asnService.delete(asn);

		return new ModelAndView(new RedirectView("manageprebooking.htm"));
	}

	@RequestMapping(value = "/editprebookingjob.htm", method = RequestMethod.GET)
	protected String editPrebookingJob(@RequestParam(name = "asnid") Integer asnid, Model model) {

		return "addprebookingjob";
	}

	@RequestMapping(value = "/downloadPrebookingJobFile", method = RequestMethod.GET)
	public void downloadPrebookingJobFile(@RequestParam(value = "id") int id,
										  HttpServletResponse response, Locale locale) throws IOException {

		Asn asn = asnService.get(id);
		if (asn == null || asn.getExchangeFormatFile() == null)
			return;

		// name
		String filename = asn.getExchangeFormatFile().getFileName();

		// make the template
		File tempDir = (File) servletContext.getAttribute(ServletContext.TEMPDIR);
		File file = efFileService.getFile(asn.getExchangeFormatFile(), tempDir);

		if (file.exists()) {
			// set response headers
			response.setContentType(URLConnection.guessContentTypeFromName(filename));
			response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");

			// copy file to response
			FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());
		}

	}

}