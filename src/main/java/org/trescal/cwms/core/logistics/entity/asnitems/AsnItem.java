package org.trescal.cwms.core.logistics.entity.asnitems;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.PossibleInstrumentDTO;
import org.trescal.cwms.core.logistics.entity.analysisresult.AsnItemAnalysisResult;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;
import org.trescal.cwms.core.logistics.utils.AsnItemStatusEnum;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

@Entity
@Table(name = "asnitem")
public class AsnItem extends Versioned {

	private Integer id;
	private Integer index;
	private AsnItemStatusEnum status;
	private String comment;
	private String poNumber;
	private String clientRef;
	private Asn asn;
	private Instrument instrument;
	private PossibleInstrumentDTO possibleInstrument;
	private AsnItemAnalysisResult analysisResult;
	private String[] statusData;
	private AsnItemServiceTypeSource serviceTypeSource;
	private ServiceType servicetype;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	@Column(name = "itemindex")
	public Integer getIndex() {
		return index;
	}

	@Column(name = "comment")
	public String getComment() {
		return comment;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	public AsnItemStatusEnum getStatus() {
		return status;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "asnid", nullable = true, foreignKey = @ForeignKey(name = "FK_asnitem_asnid"))
	public Asn getAsn() {
		return asn;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "instrumentid", nullable = true, foreignKey = @ForeignKey(name = "FK_asnitem_instrumentid"))
	public Instrument getInstrument() {
		return instrument;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "analysisresultid", nullable = true, foreignKey = @ForeignKey(name = "FK_asnitem_analysisresultid"))
	public AsnItemAnalysisResult getAnalysisResult() {
		return analysisResult;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "servicetypeid", nullable = true, foreignKey = @ForeignKey(name = "FK_asnitem_servicetypeid"))
	public ServiceType getServicetype() {
		return servicetype;
	}
	
	public void setAsn(Asn asn) {
		this.asn = asn;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	@Transient
	public String[] getStatusData() {
		return statusData;
	}

	public void setStatusData(String[] statusData) {
		this.statusData = statusData;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public void setStatus(AsnItemStatusEnum Status) {
		this.status = Status;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setAnalysisResult(AsnItemAnalysisResult analysisResult) {
		this.analysisResult = analysisResult;
	}

	public void setServicetype(ServiceType servicetype) {
		this.servicetype = servicetype;
	}
	
	@Column(name = "ponumber", nullable = true)
	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

    @Transient
	public AsnItemServiceTypeSource getServiceTypeSource() {
		return serviceTypeSource;
	}

	public void setServiceTypeSource(AsnItemServiceTypeSource serviceTypeSource) {
		this.serviceTypeSource = serviceTypeSource;
	}
	
	@Transient
	public PossibleInstrumentDTO getPossibleInstrument() {
		return possibleInstrument;
	}

	public void setPossibleInstrument(PossibleInstrumentDTO possibleInstrument) {
		this.possibleInstrument = possibleInstrument;
	}

	@Column(name = "clientref", nullable = true)
	public String getClientRef() {
		return clientRef;
	}

	public void setClientRef(String clientRef) {
		this.clientRef = clientRef;
	}

	public AsnItem(AsnItemFinalSynthesisDTO asnItemDTO) {
		super();
		this.index = asnItemDTO.getIndex();
		this.status = asnItemDTO.getStatus();
		this.poNumber = asnItemDTO.getPoNumber();
		this.statusData = asnItemDTO.getStatusData();
		this.id = asnItemDTO.getAsnItemId();
	}

	public AsnItem() {
		super();
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((index == null) ? 0 : index.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AsnItem other = (AsnItem) obj;
		if (index == null) {
			if (other.index != null)
				return false;
		} else if (!index.equals(other.index))
			return false;
		return true;
	}
}
