package org.trescal.cwms.core.logistics.entity.prebookingdetails.dao;


import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails;

@Repository
public class PrebookingJobDetailsDaoImpl extends BaseDaoImpl<PrebookingJobDetails, Integer> implements PrebookingJobDetailsDao {

	@Override
	protected Class<PrebookingJobDetails> getEntity() {
		return PrebookingJobDetails.class;
	}
}
