package org.trescal.cwms.core.logistics.form.validator;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.jobs.job.form.AddJobForm;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.dao.AsnService;
import org.trescal.cwms.core.logistics.utils.RecordTypeEnum;
import org.trescal.cwms.core.logistics.utils.SourceAPIEnum;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Service
public class AddJobFormValidator extends AbstractBeanValidator {

	// 50MB
	private static final long MAX_IN_BYTES = 52428800;
	private static final String EXCEL_EXTENSION_1 = "xlsx";
	private static final String EXCEL_EXTENSION_2 = "xls";
	@Autowired
	private AsnService asnService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(AddJobForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		AddJobForm ajf = (AddJobForm) target;
		super.validate(target, errors);

		if (ajf.getAddrid() == null) {
			errors.reject("addjob.validation.deliveryaddress", "A Delivery Address must be selected for the Job.");
		}

		if (ajf.getClientref() != null && ajf.getClientref().length() > 30) {
			errors.reject("addjob.validation.clientref",
					"The Client Reference must be between 0 and 30 characters in length.");
		}

		if (ajf.getPersonid() == null) {
			errors.reject("addjob.validation.contact", "A Contact must be selected for the Job.");
		}

		if (ajf.getReceiptDate() == null) {
			errors.reject("addjob.validation.receiptdate", "A Receipt Date and time must be selected for the Job.");
		}

		if (RecordTypeEnum.PREBOOKING.equals(ajf.getRecordType())) {

			Asn asn = asnService.get(ajf.getAsnId());
			if (!SourceAPIEnum.MOBILE_APP.equals(asn.getSourceApi()) && (asn.getExchangeFormatFile() == null
					|| asn.getExchangeFormatFile().getExchangeFormatFileData() == null)) {
				errors.reject("addjob.validation.prebookingfile.notfound", "Prebooking file not found");
			}

			if (ajf.getTransportIn() == null) {
				errors.reject("addjob.validation.transportin", "A Transport In must be Selected for the Job.");
			}

			if (ajf.getAsnId() == null) {
				errors.reject("addjob.validation.asnid", "A Prebooking must be selected for the Job.");
			}
		}

		if (RecordTypeEnum.FILE.equals(ajf.getRecordType())) {
			if (ajf.getExchangeFormat() == null && ajf.getExchangeFormat() != 0)
				errors.reject("addjob.validation.exchangeformat", "An Exchange Format must be selected for the Job.");
			else {
				MultipartFile file = ajf.getUploadFile();

				// validate file
				if (file == null || file.isEmpty()) {
					errors.rejectValue("uploadFile", "error.instrument.upload.file.empty");
					return;
				} else if (!EXCEL_EXTENSION_1.equalsIgnoreCase((FilenameUtils.getExtension(file.getOriginalFilename())))
						&& !EXCEL_EXTENSION_2
								.equalsIgnoreCase((FilenameUtils.getExtension(file.getOriginalFilename())))) {
					errors.rejectValue("uploadFile", "error.instrument.upload.file.notexcel");
					return;
				}

				// check max file size
				if (file.getSize() > MAX_IN_BYTES) {
					errors.rejectValue("uploadFile", "error.instrument.upload.file.maxsizeexceeded");
				}
			}
		}

	}

}
