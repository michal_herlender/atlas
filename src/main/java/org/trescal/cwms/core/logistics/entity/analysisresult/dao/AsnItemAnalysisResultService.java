package org.trescal.cwms.core.logistics.entity.analysisresult.dao;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.logistics.entity.analysisresult.AsnItemAnalysisResult;

public interface AsnItemAnalysisResultService extends BaseService<AsnItemAnalysisResult, Integer> {

}
