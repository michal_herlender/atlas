package org.trescal.cwms.core.logistics.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.dto.AddressDTO;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.deliverynote.entity.delivery.db.DeliveryService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;

@Controller
@JsonController
@SessionAttributes(value = { Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class NewScanInOutJsonController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private DeliveryService delService;
	
		
	@RequestMapping(value = "/getbusinessAddresses.json", method = RequestMethod.GET)
	@ResponseBody
	protected List<AddressDTO> getbusinessAddresses(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivKeyValue) throws Exception {

		List<AddressDTO> addresses = new ArrayList<AddressDTO>();
		User user = userService.get(username);
		Set<Subdiv> userSubdivs = user.getUserRoles().stream().map(ur -> ur.getOrganisation())
				.collect(Collectors.toSet());
		for (Subdiv subdiv : userSubdivs) {
			List<Address> subdivAddresses = subdiv.getAddresses().stream().filter(Address::getActive)
					.collect(Collectors.toList());
			for (Address address : subdivAddresses) {
				addresses.add(new AddressDTO(address));
			}
		}
		return addresses;
	}
	
	@RequestMapping(value = "/scanOutDeliveryOrItem.json", method = RequestMethod.GET)
	@ResponseBody
	protected ResultWrapper despatchDeliveryOrItem(
			@RequestParam(name = "deliveryNoOrBarcode", required = true) String deliveryNoOrBarcode) throws Exception {
		return this.delService.scanOutDeliveryOrItem(deliveryNoOrBarcode);
	}
	
	@RequestMapping(value = "/scanInDeliveryOrItem.json", method = RequestMethod.GET)
	@ResponseBody
	protected ResultWrapper receiveDeliveryOrItem(
			@RequestParam(name = "deliveryNoOrBarcode", required = true) String deliveryNoOrBarcode,
			@RequestParam(name = "addrId", required = true) Integer addrId) throws Exception {
		return this.delService.scanInDeliveryOrItem(deliveryNoOrBarcode, addrId,null);
	}





}
