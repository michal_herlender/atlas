package org.trescal.cwms.core.logistics.entity.asnitems;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum AsnItemServiceTypeSource
{
	FILE("asnitem.servicetype.source.file", "Default From File"),				
	QUOTATION("asnitem.servicetype.source.quotation", "Default From Quotation"), 				
	JOBITEM("asnitem.servicetype.source.jobitem", "Default From JobItem"), 	 
	INSTRUMENT("asnitem.servicetype.source.instrument", "Default From Instrument"),						
	INSTRUMENT_MODEL("asnitem.servicetype.source.instrumentmodel", "Default From Instrument Model"),
	BUSINESS_COMPANY_SETTING("asnitem.servicetype.source.businesscompaynsetting","Default from business company setting");
		
	private ReloadableResourceBundleMessageSource messages;
	private final String messageCode;
	private final String description;
	
	AsnItemServiceTypeSource(String messageCode, String description) {
		this.messageCode = messageCode;
		this.description = description;
	}

	public String getDescription() 
	{
		Locale locale = LocaleContextHolder.getLocale();
		if (messages != null) {
			return messages.getMessage(this.messageCode, null, this.description, locale);
		}
		return this.toString();
	}

	public String getName() {
		return this.name();
	}

	@Component
	public static class AsnItemServiceTypeSourceMessageSourceInjector {
		@Autowired
		private ReloadableResourceBundleMessageSource messages;
		
		@PostConstruct
        public void postConstruct() {
            for (AsnItemServiceTypeSource jobType : EnumSet.allOf(AsnItemServiceTypeSource.class))
               jobType.setMessageSource(messages);
        }
	}

	public void setMessageSource (ReloadableResourceBundleMessageSource messages) {
		this.messages = messages;
	}
}
