package org.trescal.cwms.core.logistics.entity.asn.dao;

import lombok.val;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.location.db.LocationService;
import org.trescal.cwms.core.exchangeformat.aliasgroups.entity.AliasGroup;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db.ExchangeFormatFileService;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.PossibleInstrumentDTO;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.jobs.job.entity.po.db.POService;
import org.trescal.cwms.core.logistics.dto.AsnDTO;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asnitems.AsnItem;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails;
import org.trescal.cwms.core.logistics.entity.prebookingfile.PrebookingFile;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;
import org.trescal.cwms.core.logistics.form.PrebookingJobForm;
import org.trescal.cwms.core.logistics.utils.AsnItemStatusEnum;
import org.trescal.cwms.core.logistics.utils.SourceAPIEnum;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.files.exchangeformat.ExchangeFormatFileData;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AsnServiceImpl extends BaseServiceImpl<Asn, Integer> implements AsnService {

	private static final Integer BER_ID = 1;

	@Autowired
	private AsnDao dao;
	@Autowired
	private ExchangeFormatService exchangeFormatService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private LocationService locationService;
	@Autowired
	private SupportedCurrencyService supportedCurrencyService;
	@Autowired
	private ContactService contactService;
	@Autowired
	private POService poService;
	@PersistenceContext(unitName = "entityManagerFactory2")
	private EntityManager entityManager2;
	@Autowired
	@Qualifier("transactionManager2")
	private PlatformTransactionManager transactionManager2;
	@Autowired
	private BPOService bpoService;
	@Autowired
	private InstrumService instService;
	@Autowired
	private InstrumService instrumentService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private ExchangeFormatFileService exchangeFormatFileService;

	@Override
	protected BaseDao<Asn, Integer> getBaseDao() {
		return dao;
	}

	@Override
	public Asn savePreBookingASN(PrebookingJobForm prebookingJobForm, List<AsnItemFinalSynthesisDTO> list, Contact userContact) {

		List<Integer> trescalIds = list.stream().map(AsnItemFinalSynthesisDTO::getIdTrescal)
				.filter(Objects::nonNull).collect(Collectors.toList());
		List<Instrument> allInstruments = null;
		if (CollectionUtils.isNotEmpty(trescalIds))
			allInstruments = instrumentService.getInstruments(trescalIds);

		List<AsnItem> asnItemList = new ArrayList<>();
		List<Instrument> instrumentsList = allInstruments;
		list.forEach(a -> {
			AsnItem ai = new AsnItem(a);
			if (a.getServicetypeid() != null)
				ai.setServicetype(serviceTypeService.get(a.getServicetypeid()));
			if (a.getIdTrescal() != null)
				instrumentsList.stream().filter(i -> i.getPlantid() == a.getIdTrescal()).findFirst()
						.ifPresent(ai::setInstrument);
			asnItemList.add(ai);
		});

		// asn
		Asn asn;
		if (prebookingJobForm.getAsnId() != null) {
			asn = dao.find(prebookingJobForm.getAsnId());
		} else {
			asn = new Asn();
			asn.setExchangeFormat(exchangeFormatService.get(prebookingJobForm.getExchangeFormat()));
			asn.setExchangeFormatFile((PrebookingFile) exchangeFormatFileService.get(prebookingJobForm.getPrebookingFileId()));
			asn.setJobType(prebookingJobForm.getJobType());
			asn.setSourceApi(SourceAPIEnum.CWMS);
			asn.setCreatedBy(userContact);
			asn.setCreatedOn(new Timestamp(new Date().getTime()));
		}

		// PrebookingJobDetails
		if (asn.getId() == null) {
			asn.setPrebookingJobDetails(new PrebookingJobDetails());
			asn.getPrebookingJobDetails().setAsn(asn);
			if (prebookingJobForm.getBookedInAddrId() != null)
				asn.getPrebookingJobDetails().setBookedInAddr(addressService.get(prebookingJobForm.getBookedInAddrId()));
			if (prebookingJobForm.getBookedInLocId() != null)
				asn.getPrebookingJobDetails().setBookedInLoc(locationService.get(prebookingJobForm.getBookedInLocId()));
			asn.getPrebookingJobDetails().setClientRef(prebookingJobForm.getClientref());
			if (prebookingJobForm.getPersonid() != null)
				asn.getPrebookingJobDetails().setCon(contactService.get(prebookingJobForm.getPersonid()));
			if (prebookingJobForm.getCurrencyCode() != null)
				asn.getPrebookingJobDetails()
						.setCurrency(supportedCurrencyService.findCurrencyByCode(prebookingJobForm.getCurrencyCode()));
			if (prebookingJobForm.getDefaultPO() != null)
				poService.addPOsToPrebookingJob(prebookingJobForm.getPoNumberList(), prebookingJobForm.getPoCommentList(), asn,
						prebookingJobForm.getDefaultPO(), userContact.getSub().getComp());
			asn.getPrebookingJobDetails().setEstCarriageOut(prebookingJobForm.getEstCarriageOut());
		}

		if (CollectionUtils.isNotEmpty(asn.getAsnItems())) {
			Iterator<AsnItem> asnItemIterator = asnItemList.iterator();
			while (asnItemIterator.hasNext()) {
				AsnItem currentAsnItem = asnItemIterator.next();
				val asnItemToUpdateOpt =
					  asn.getAsnItems().stream().filter(i -> i.getId().equals(currentAsnItem.getId()))
							.findFirst();

				 asnItemToUpdateOpt.ifPresent(asnItemToUpdate -> {
					asnItemToUpdate.setStatus(currentAsnItem.getStatus());
					asnItemToUpdate.setStatusData(currentAsnItem.getStatusData());
					asnItemToUpdate.setServicetype(currentAsnItem.getServicetype());
					asnItemToUpdate.setServiceTypeSource(currentAsnItem.getServiceTypeSource());
					asnItemToUpdate.setInstrument(currentAsnItem.getInstrument());
					asnItemToUpdate.setPoNumber(currentAsnItem.getPoNumber());
					asnItemToUpdate.setClientRef(currentAsnItem.getClientRef());
					asnItemIterator.remove();
				});
			}
			if (CollectionUtils.isNotEmpty(asnItemList))
				asn.getAsnItems().addAll(asnItemList);
		} else {
			asn.setAsnItems(new ArrayList<>());
			asn.getAsnItems().addAll(asnItemList);
		}

		asn.getAsnItems().forEach(a -> a.setAsn(asn));

		// Add BPO
		if (prebookingJobForm.getBpo() != null)
			asn.getPrebookingJobDetails().setBpo(bpoService.get(prebookingJobForm.getBpo()));

		/* add Po */
		List<String> posNumber = asnItemList.stream().map(AsnItem::getPoNumber).distinct()
				.filter(StringUtils::isNoneBlank).collect(Collectors.toList());
		if (posNumber.size() > 0)
			poService.addPOsToPrebookingJob(posNumber, null, asn, 1, userContact.getSub().getComp());

		/* call update every method invocation to save data */
		dao.merge(asn);

		/* asn */

		return asn;

	}

	@Override
	public List<Asn> getPrebookingByPlantId(Integer plantId) {
		return dao.getNotLinkedPrebookingsByPlantId(plantId);
	}

	@Override
	public PagedResultSet<Asn> findPrebooking(Boolean associatedWithJob, Integer businessSubdivId,
			Integer clientCompanyId, Integer plantId, String plantNo, Integer resultsPerPage, Integer currentPage) {
		return dao.findPrebooking(associatedWithJob, businessSubdivId, clientCompanyId, plantId, plantNo,
				resultsPerPage, currentPage);
	}

	@Override
	public List<AsnDTO> findAsn(Integer subdivId, Integer plantid, String plantno) {
		return dao.findAsn(subdivId, plantid, plantno);
	}

	@Override
	public Integer asnItemsSize(Integer asnId) {
		return dao.asnItemsSize(asnId);
	}

	@Override
	public Asn findAsnByJobId(Integer jobid) {
		return dao.findAsnByJobId(jobid);
	}

	@Override
	public void delete(Asn asn) {
		super.delete(asn);
		if (asn.getExchangeFormatFile() != null && asn.getExchangeFormatFile().getExchangeFormatFileData() != null) {
			TransactionStatus ts = transactionManager2.getTransaction(null);
			ExchangeFormatFileData pfd = entityManager2.find(ExchangeFormatFileData.class,
					asn.getExchangeFormatFile().getExchangeFormatFileData().getEntityId());
			entityManager2.remove(pfd);
			transactionManager2.commit(ts);
		}
	}

	@Override
	public List<AsnItemFinalSynthesisDTO> analyseJobOrAsnItemsList(PrebookingJobForm prebookingJobForm, Integer allocatedSubdivId,
																   Locale locale, Asn asn, List<AsnItemFinalSynthesisDTO> aiFinalSynthDtoList) {

		// get plantid, plantno and serialno IDs list for query
		List<Integer> plantids = new ArrayList<>();
		List<String> plantnos = new ArrayList<>();
		List<String> serialnos = new ArrayList<>();
		setItemsFieldId(aiFinalSynthDtoList, plantids, plantnos, serialnos);

		// get all possible from db
		List<PossibleInstrumentDTO> allPossibleInstrumentDto = instService.lookupPossibleInstruments(prebookingJobForm.getCoid(),
				allocatedSubdivId, asn != null ? asn.getId() : 0, locale, plantids, plantnos, serialnos);

		// init asnItemIndex with first index
		int asnItemIndex = 1;

		// if prebooking get asnItems
		List<AsnItem> asnItems = null;
		if (asn != null)
			asnItems = asn.getAsnItems();

		List<AsnItemFinalSynthesisDTO> asnItemsList = new ArrayList<>();

		// compatible service types
		Set<Integer> compatibleServiceTypesIds = serviceTypeService.getIdsByJobType(prebookingJobForm.getJobType());

		// iterate fileContent to retrieve identified and non identified Items
		for (AsnItemFinalSynthesisDTO dto : aiFinalSynthDtoList) {

			// if prebooking get asnItems else create a new
			if (asnItems != null) {
				int index = dto.getAsnItemId() != null && dto.getIndex() != null ? dto.getIndex() : asnItemIndex++;
				val ai = asnItems.stream().filter(i -> i.getIndex().equals(index)).findFirst();
				ai.map(AsnItem::getId).ifPresent(dto::setAsnItemId);
				ai.map(AsnItem::getIndex).ifPresent(dto::setIndex);

			} else if (dto.getIndex() == null) {
				dto.setIndex(asnItemIndex++);
			}

			// to get plantid, plantno and serialno of every row to use it to
			// retrieve the item in all possible instruments dto list
			Integer plantid = null;
			String plantno = null;
			String serialno = null;
			if (dto.getIdTrescal() != null)
				plantid = dto.getIdTrescal();
			if (StringUtils.isNotBlank(dto.getPlantNo()))
				plantno = dto.getPlantNo();
			if (StringUtils.isNotBlank(dto.getSerialNo()))
				serialno = dto.getSerialNo();

			// get possibleInstrument
			List<PossibleInstrumentDTO> possibleInstruments = getPossibleInstrumentForFieldsId(allPossibleInstrumentDto,
				plantid, plantno, serialno);

			// filter identified items
			List<AsnItemFinalSynthesisDTO> identifiedJobItemsFinalSynthDTO = asnItemsList.stream()
				.filter(d -> d.getStatus() != null && d.getStatus().equals(AsnItemStatusEnum.IDENTIFIED))
				.collect(Collectors.toList());

			// check the possibleInstruments status
			checkCurrentItem(possibleInstruments, identifiedJobItemsFinalSynthDTO, dto, prebookingJobForm.getCoid(), plantid, plantno,
				serialno, compatibleServiceTypesIds);

			asnItemsList.add(dto);
		}

		return asnItemsList;
	}

	private void checkCurrentItem(List<PossibleInstrumentDTO> possibleInstruments,
								  List<AsnItemFinalSynthesisDTO> identifiedAsnItemsFinalSynth, AsnItemFinalSynthesisDTO dto,
								  Integer coid, Integer plantid, String plantno, String serialno,
								  Set<Integer> compatibleServiceTypesIds) {

		if (dto.getStatus() == AsnItemStatusEnum.IDENTIFIED)
			return;
		if (CollectionUtils.isEmpty(possibleInstruments)) {
			dto.setStatus(AsnItemStatusEnum.NOT_IDENTIFIED_IN_CLIENT_COMPANY);
		} else {
			if (possibleInstruments.size() == 1) {
				validateIdentifiedAsnItemAndUpdateDto(dto, identifiedAsnItemsFinalSynth, possibleInstruments.get(0),
					coid, compatibleServiceTypesIds);
			} else { // multiple instrument identified
				// try to filter
				// we might get this case, because we're using an OR while
				// fetching instrument (plantid or plantno or serialno)
				if (plantid != null) {
					possibleInstruments = possibleInstruments.stream().filter(i -> plantid.equals(i.getPlantid())).collect(Collectors.toList());

					// since it is the plantid (id), we're sure that the result
					// is unique
					if (possibleInstruments.size() == 1) {
						validateIdentifiedAsnItemAndUpdateDto(dto, identifiedAsnItemsFinalSynth,
							possibleInstruments.get(0), coid, compatibleServiceTypesIds);
					}

				} else if (StringUtils.isNoneEmpty(plantno)) {
					possibleInstruments = possibleInstruments.stream().filter(i -> plantno.equalsIgnoreCase(i.getPlantno())).collect(Collectors.toList());
					// check again if we succeeded getting one instrument
					List<PossibleInstrumentDTO> possibleInstrumentWithoutBER = possibleInstruments.stream()
							.filter(pi -> !pi.getStatus().equals(BER_ID)).collect(Collectors.toList());
					if (possibleInstrumentWithoutBER.size() == 1) {
						validateIdentifiedAsnItemAndUpdateDto(dto, identifiedAsnItemsFinalSynth,
							possibleInstruments.get(0), coid, compatibleServiceTypesIds);
					} else {
						// couldn't filter
						dto.setStatus(AsnItemStatusEnum.MULTIPLE_INSTRUMENTS_WITH_SAME_PLANTNO);
						dto.setStatusData(new String[] { plantno });
					}
				} else {
					List<PossibleInstrumentDTO> possibleInstrumentWithoutBER = possibleInstruments.stream()
							.filter(pi -> !pi.getStatus().equals(BER_ID)).collect(Collectors.toList());

					if (possibleInstrumentWithoutBER.size() == 1) {
						validateIdentifiedAsnItemAndUpdateDto(dto, identifiedAsnItemsFinalSynth,
							possibleInstruments.get(0), coid, compatibleServiceTypesIds);
					} else {
						// couldn't filter
						dto.setStatus(AsnItemStatusEnum.MULTIPLE_INSTRUMENTS_WITH_SAME_SERIALNO);
						dto.setStatusData(new String[] { serialno });
					}
				}
			}
		}
	}

	private List<PossibleInstrumentDTO> getPossibleInstrumentForFieldsId(
			List<PossibleInstrumentDTO> allPossibleInstrumentDto, Integer plantid, String plantno, String serialno) {
		List<PossibleInstrumentDTO> possibleInstruments = Collections.emptyList();
		// search the possibleInstrumentDto
		if (plantid != null) {
			return allPossibleInstrumentDto.stream()
					.filter(i -> i.getPlantid().equals(plantid)).collect(Collectors.toList());
		}
		// if no possibleInstruments found using plantId and plantNo is set,
		// get all possibleInstrumentDto with the same plantNo
		if (StringUtils.isNotBlank(plantno)
				&& !plantno.equals("/")) {
			possibleInstruments = allPossibleInstrumentDto.stream()
				.filter(i -> i.getPlantno() != null && i.getPlantno().toUpperCase(Locale.ROOT).equals(plantno.toUpperCase(Locale.ROOT)))
					.collect(Collectors.toList());
		}

		// if no possibleInstruments found using plantId or plantNo or multiple
		// instruments are found by plantno then filter
		if (possibleInstruments.size() != 1 && StringUtils.isNotBlank(serialno) && !serialno.equals("/")) {
			if (possibleInstruments.size() > 1) {
				// in this case we're sure that the plantno search resulted
				// multiple possible instruments,
				// we should try filter the possible instruments with the
				// serial no
				return possibleInstruments.stream()
					.filter(pi -> pi.getSerialno() != null && pi.getSerialno().toUpperCase(Locale.ROOT).equalsIgnoreCase(serialno.toUpperCase(Locale.ROOT))).collect(Collectors.toList());
			} else {
				return allPossibleInstrumentDto.stream()
						.filter(i -> i.getSerialno() != null && i.getSerialno().equalsIgnoreCase(serialno))
						.collect(Collectors.toList());
			}
		}
		return possibleInstruments;
	}

	private void setItemsFieldId(List<AsnItemFinalSynthesisDTO> aiFinalSynthDtoList, List<Integer> plantids,
			List<String> plantnos, List<String> serialnos) {
		for (AsnItemFinalSynthesisDTO dto : aiFinalSynthDtoList) {
			if (dto.getIdTrescal() != null)
				plantids.add(dto.getIdTrescal());
			if (StringUtils.isNotBlank(dto.getPlantNo()) && !dto.getPlantNo().equals("/"))
				plantnos.add(dto.getPlantNo());
			if (StringUtils.isNotBlank(dto.getSerialNo()) && !dto.getSerialNo().equals("/"))
				serialnos.add(dto.getSerialNo());
		}
	}

	private void validateIdentifiedAsnItemAndUpdateDto(AsnItemFinalSynthesisDTO dtoAsn,
													   List<AsnItemFinalSynthesisDTO> asnItemFinalSythesisList, PossibleInstrumentDTO possibleInstruments, int clientCompanyId,
													   Set<Integer> compatibleServiceTypesIds) {

		dtoAsn.setPossibleInstrument(possibleInstruments);

		// test if already in identified items
		AsnItemFinalSynthesisDTO existDto = asnItemFinalSythesisList.stream()
			.filter(e -> e.getPossibleInstrument().getPlantid().equals(possibleInstruments.getPlantid()))
			.findFirst().orElse(null);
		if (existDto != null) {
			dtoAsn.setStatus(AsnItemStatusEnum.DUPLICATED_IN_TABLE);
			int indexOfDuplicatedElement = existDto.getIndex();
			int indexOfDuplicatedElementInEFFile = existDto.getIndex();
			dtoAsn.setStatusData(new String[]{indexOfDuplicatedElement + "", indexOfDuplicatedElementInEFFile + ""});
			dtoAsn.setSubFamily(possibleInstruments.getModelDescription());
		} else if (possibleInstruments.getAsnid() != null) {
			// instrument identified but currently in a prebooking job
			dtoAsn.setStatus(AsnItemStatusEnum.ALREADY_ONPREBOOKING);
			dtoAsn.setStatusData(new String[] { possibleInstruments.getAsnid() + "" });
			dtoAsn.setSubFamily(possibleInstruments.getModelDescription());
		} else if (possibleInstruments.getJobitemid() != null) {
			// instrument identified but currently in a job
			dtoAsn.setStatus(AsnItemStatusEnum.ALREADY_ONJOB);
			dtoAsn.setStatusData(
					new String[] { possibleInstruments.getJobitemid() + "", possibleInstruments.getJobitemviewcode() });
			dtoAsn.setSubFamily(possibleInstruments.getModelDescription());
		} else if (possibleInstruments.getStatus().equals(InstrumentStatus.BER.ordinal())) {
			// BER
			dtoAsn.setStatus(AsnItemStatusEnum.BER);
			dtoAsn.setSubFamily(possibleInstruments.getModelDescription());
		} else if (possibleInstruments.getCoid() != clientCompanyId) {
			// instrument identified
			dtoAsn.setStatus(AsnItemStatusEnum.IDENTIFIED_ON_OTHER_COMPANY_IN_SAME_GROUP);
			dtoAsn.setSubFamily(possibleInstruments.getModelDescription());
		} else if (dtoAsn.getServicetypeid() != null
				&& !compatibleServiceTypesIds.contains(dtoAsn.getServicetypeid())) {
			// incompatible service type
			dtoAsn.setStatus(AsnItemStatusEnum.INCOMPATIBLE_SERVICE_TYPE);
		} else {
			dtoAsn.setIdTrescal(possibleInstruments.getPlantid());
			dtoAsn.setStatus(AsnItemStatusEnum.IDENTIFIED);
			dtoAsn.setSubFamily(possibleInstruments.getModelDescription());
		}
	}

	@Override
	public List<Asn> getAllAsnActiveByExchangeFormatId(Integer exchangeFormatId) {
		return this.dao.getAllAsnActiveByExchangeFormatId(exchangeFormatId);
	}

	@Override
	public List<Asn> getAsnByAliasGroup(AliasGroup aliasGroup) {
		return this.dao.getAsnByAliasGroup(aliasGroup);
	}

}
