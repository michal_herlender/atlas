package org.trescal.cwms.core.logistics.entity.analysisresult.dao;


import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.logistics.entity.analysisresult.AsnItemAnalysisResult;

@Repository
public class AsnItemAnalysisResultDaoImpl extends BaseDaoImpl<AsnItemAnalysisResult, Integer> implements AsnItemAnalysisResultDao {

	@Override
	protected Class<AsnItemAnalysisResult> getEntity() {
		return AsnItemAnalysisResult.class;
	}
}
