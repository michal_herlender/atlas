package org.trescal.cwms.core.logistics.form.validator;


import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.logistics.form.PrebookingJobForm;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Service
public class AddPrebookingJobFormValidator extends AbstractBeanValidator {
	
	// 50MB
	private static final long MAX_IN_BYTES = 52428800;
	private static final String EXCEL_EXTENSION_1 = "xlsx";
	private static final String EXCEL_EXTENSION_2 = "xls";
		
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(PrebookingJobForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		PrebookingJobForm pjf = (PrebookingJobForm) target;
		super.validate(target, errors);

		    if(pjf.getExchangeFormat() == null || pjf.getExchangeFormat() == 0)
				errors.reject("exchangeFormat", "Exchange format is mandatory.");
			else
			{
				MultipartFile file = pjf.getUploadFile();
	
				// validate file
				if (file.isEmpty()) {
					errors.rejectValue("uploadFile", "error.instrument.upload.file.empty");
				} else if (!EXCEL_EXTENSION_1.equalsIgnoreCase((FilenameUtils.getExtension(file.getOriginalFilename())))
						&& !EXCEL_EXTENSION_2.equalsIgnoreCase((FilenameUtils.getExtension(file.getOriginalFilename())))) {
					errors.rejectValue("uploadFile", "error.instrument.upload.file.notexcel");
					return;
				}
	
				// check max file size
				if (file.getSize() > MAX_IN_BYTES) {
					errors.rejectValue("uploadFile", "error.instrument.upload.file.maxsizeexceeded");
				}
			}
		
	}

}
