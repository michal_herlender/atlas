package org.trescal.cwms.core.logistics.utils;

public enum NextActionEnum {
	
	ON_HOLD_AWAITING_ITEM_REPAIR("On-Hold awaiting repair of another item"),
	ON_HOLD_AWAITING_PART("On-Hold awaiting part from supplier"),
	ON_HOLD_AWAITING_DEPARTMENT_REPAIR("On-Hold awaiting repair in another department"),
	ON_HOLD_AWAITING_EXPERT_RECOMMENDATION("On-Hold awaiting expert recommendation"),
	ON_HOLD_BATTERIES_BEING_CHARGED("On-Hold batteries being charged"),
	ON_HOLD_OTHER("On-Hold other"),
	ON_HOLD_AWAITING_ITEM_APPROVAL("On-Hold awaiting approval of another item"),
	ON_HOLD_AWAITING_INSTRUMENT_IDENTIFICATION("On-Hold awaiting identification of instrument"),
	ON_HOLD_AWAITING_PURCHASE_ORDER("On-Hold awaiting purchase order"),
	ON_HOLD_AWAITING_TECHNICAL_ISSUE("On-Hold awaiting in logistic for technical issue"),
	ON_HOLD_AWAITING_ADMINISTRATIVE_ISSUE("On-Hold awaiting in logistic for administrative issue");
	
private String fullName;
	
	private NextActionEnum(String fullName) {
		this.fullName = fullName;
	}

	public String getFullName() {
		return fullName;
	}
	
	public static NextActionEnum parseFullName(String fullName) {
		NextActionEnum result = null;
		for (NextActionEnum status : NextActionEnum.values()) {
			if (status.getFullName().equals(fullName)) {
				result = status;
				break;
			}
		}
		return result;
	}
	
	public String getValue() {
		return name();
		}

		public void setValue(String value) {}

}