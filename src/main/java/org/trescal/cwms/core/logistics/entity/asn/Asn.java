package org.trescal.cwms.core.logistics.entity.asn;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.logistics.entity.asnitems.AsnItem;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails;
import org.trescal.cwms.core.logistics.entity.prebookingfile.PrebookingFile;
import org.trescal.cwms.core.logistics.utils.SourceAPIEnum;

@Entity
@Table(name = "asn")
public class Asn extends Versioned {

	private Integer id;
	private JobType jobType;
	private SourceAPIEnum sourceApi;
	private List<AsnItem> asnItems;
	private ExchangeFormat exchangeFormat;
	private PrebookingJobDetails prebookingJobDetails;
	private Job job;
	private Timestamp createdOn;
	private Contact createdBy;
	private String itemsFromJson;
	private PrebookingFile exchangeFormatFile;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "jobtype")
	public JobType getJobType() {
		return jobType;
	}

	public void setJobType(JobType jobType) {
		this.jobType = jobType;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "sourceapi")
	public SourceAPIEnum getSourceApi() {
		return sourceApi;
	}

	@OneToOne(mappedBy = "asn", cascade = CascadeType.ALL)
	public PrebookingJobDetails getPrebookingJobDetails() {
		return prebookingJobDetails;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "exchangeformatid", nullable = true,  foreignKey = @ForeignKey(name = "FK_asn_exchangeformatid"))
	public ExchangeFormat getExchangeFormat() {
		return exchangeFormat;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "asn")
	public List<AsnItem> getAsnItems() {
		return asnItems;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobid", nullable = true, foreignKey = @ForeignKey(name = "FK_asn_jobid"))
	public Job getJob() {
		return job;
	}
	
	@Column(name="createdOn", nullable=false, columnDefinition="datetime2")
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="createdBy", foreignKey = @ForeignKey(name = "FK_asn_createdby"))
	public Contact getCreatedBy() {
		return createdBy;
	}

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="exchangeformatfileid")
	public PrebookingFile getExchangeFormatFile() {
		return exchangeFormatFile;
	}

	public void setExchangeFormatFile(PrebookingFile exchangeFormatFile) {
		this.exchangeFormatFile = exchangeFormatFile;
	}

	public void setAsnItems(List<AsnItem> asnItems) {
		this.asnItems = asnItems;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setExchangeFormat(ExchangeFormat exchangeFormat) {
		this.exchangeFormat = exchangeFormat;
	}

	public void setPrebookingJobDetails(PrebookingJobDetails prebookingJobDetails) {
		this.prebookingJobDetails = prebookingJobDetails;
	}

	public void setSourceApi(SourceAPIEnum sourceApi) {
		this.sourceApi = sourceApi;
	}
	
	public void setJob(Job job) {
		this.job = job;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public void setCreatedBy(Contact createdBy) {
		this.createdBy = createdBy;
	}

	public String getItemsFromJson() {
		return itemsFromJson;
	}

	public void setItemsFromJson(String itemsFromJson) {
		this.itemsFromJson = itemsFromJson;
	}
	
}
