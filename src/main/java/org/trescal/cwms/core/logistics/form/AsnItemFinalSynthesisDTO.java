package org.trescal.cwms.core.logistics.form;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.PossibleInstrumentDTO;
import org.trescal.cwms.core.logistics.entity.asnitems.AsnItemServiceTypeSource;
import org.trescal.cwms.core.logistics.utils.AsnItemStatusEnum;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.TranslationUtils;

import java.util.Date;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AsnItemFinalSynthesisDTO {

	private Integer idTrescal;
	private String plantNo;
	private String customerDescription;
	private String brand;
	private String model;
	private String serialNo;
	private String expectedService;
	private Integer servicetypeid;

	private String technicalRequirements;
	private String customerComments;
	private Date deliveryDate;
	private String urgency;
	private String underGuarantee;
	private String poNumber;
	private String location;
	private Map<String, String> undefineds;
	private String jobitemClientRef;

	private Integer calibrationFrequency;
	private IntervalUnit intervalUnit;

	private String subFamily;
	private int subFamilySize;
	private boolean checked;

	private String logisticsComment;
	private Integer nextStatuId;

	private AsnItemStatusEnum status;
	private String[] statusData;
	private Integer index;
	private Integer asnItemId;
	private PossibleInstrumentDTO possibleInstrument;
	private AsnItemServiceTypeSource serviceTypeSource;


	public AsnItemFinalSynthesisDTO(String subFamily, int subFamilySize) {
		super();
		this.subFamily = subFamily;
		this.subFamilySize = subFamilySize;
	}

	public void setInstrument(Instrument instrument) {
		if (instrument == null) return;
		this.plantNo = instrument.getPlantno();
		this.serialNo = instrument.getSerialno();
		this.idTrescal = instrument.getPlantid();
		this.customerDescription = instrument.getCustomerDescription();
		this.brand = instrument.getModel().getMfr().getName();
		this.model = InstModelTools.instrumentModelNameWithTypology(instrument.getModel(), LocaleContextHolder.getLocale());
		this.subFamily = TranslationUtils.getBestTranslation(instrument.getModel().getDescription().getTranslations()).orElse("");
	}

	public void setServiceType(ServiceType serviceType) {
		if (serviceType == null) return;
		this.expectedService = TranslationUtils.getBestTranslation(serviceType.getShortnameTranslation()).orElse(serviceType.getShortName());
		this.servicetypeid = serviceType.getServiceTypeId();
	}

}
