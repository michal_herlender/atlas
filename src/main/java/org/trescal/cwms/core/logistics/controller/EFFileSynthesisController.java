package org.trescal.cwms.core.logistics.controller;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.validation.Valid;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.documents.excel.utils.ExcelFileReaderUtil;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.exchangeformat.dto.form.validator.ExchangeFormatFileValidator;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformatfile.db.ExchangeFormatFileService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.dao.AsnService;
import org.trescal.cwms.core.logistics.entity.asnitems.AsnItem;
import org.trescal.cwms.core.logistics.entity.asnitems.dao.AsnitemService;
import org.trescal.cwms.core.logistics.entity.prebookingfile.PrebookingFile;
import org.trescal.cwms.core.logistics.form.AddPrebookingJobForm;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;
import org.trescal.cwms.core.logistics.form.PrebookingJobForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME,
		Constants.SESSION_ATTRIBUTE_COMPANY })
public class EFFileSynthesisController {

	public static final String FORM = "addPrebookingJobForm";

	private static final Integer AUTO_GROW_COLLECTION_LIMIT = 1024;

	@Autowired
	private ExchangeFormatService exchangeFormatServ;
	@Autowired
	private ServletContext servletContext;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private AsnService asnService;
	@Autowired
	private UserService userService;
	@Autowired
	private ExchangeFormatFileValidator validator;
	@Autowired
	private CalibrationTypeService calibrationTypeService;
	@Autowired
	protected TranslationService translationService;
	@Autowired
	private ExchangeFormatFileService efFileService;
	@Autowired
	private AsnitemService asnItemService;

	@Autowired
	private ExcelFileReaderUtil excelFileReaderUtil;

	@InitBinder("addPrebookingJobForm.prebookingJobForm")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@InitBinder(FORM)
	protected void initBinder2(WebDataBinder binder) {
		binder.setAutoGrowCollectionLimit(AUTO_GROW_COLLECTION_LIMIT);
	}

	@RequestMapping(value = "/viewefsynthesis.htm", method = { RequestMethod.GET })
	protected ModelAndView fileSynthesisFromAsnId(@RequestParam(name = "asnid") Integer asnid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Model model,
			Locale locale) throws Exception {

		Asn asn = asnService.get(asnid);
		PrebookingJobForm pbjf = new PrebookingJobForm();
		pbjf.setAsnId(asnid);
		pbjf.setJobType(asn.getJobType());
		pbjf.setExchangeFormat(asn.getExchangeFormat().getId());
		pbjf.setCoid(asn.getPrebookingJobDetails().getCon().getSub().getComp().getId());

		File tempDir = (File) servletContext.getAttribute(ServletContext.TEMPDIR);
		File file = efFileService.getFile(asn.getExchangeFormatFile(), tempDir);
		List<LinkedCaseInsensitiveMap<String>> fileContent = excelFileReaderUtil.readExcelFile(
				new FileInputStream(file), asn.getExchangeFormat().getSheetName(),
				asn.getExchangeFormat().getLinesToSkip());

		List<AsnItemFinalSynthesisDTO> aiFinalSynthDtoList = asnItemService
				.convertFileContentToJobOrAsnItemFinalSynthesisDTO(asn.getExchangeFormat(), fileContent);

		// get tableHeader
		Map<String, ExchangeFormatFieldNameEnum> columnsNameMapping = exchangeFormatServ
				.getColumnNamesMapping(asn.getExchangeFormat());

		List<AsnItemFinalSynthesisDTO> asnItemsDTOList = asnService.analyseJobOrAsnItemsList(pbjf, subdivDto.getKey(),
				locale, asn, aiFinalSynthDtoList);

		for (AsnItem i : asn.getAsnItems()) {
			val dtoOpt = asnItemsDTOList.stream()
					.filter(d -> d.getIndex().equals(i.getIndex()))
					.findFirst();
			dtoOpt.ifPresent(dto -> {
				if (i.getServicetype() != null)
					dto.setServicetypeid(i.getServicetype().getServiceTypeId());
				if (i.getId() != null)
					dto.setAsnItemId(i.getId());
				if(i.getInstrument() != null)
				{
					val inst = i.getInstrument();
					dto.setIdTrescal(inst.getPlantid());
					dto.setPlantNo(inst.getPlantno());
					dto.setSerialNo(inst.getSerialno());
					dto.setBrand(inst.getModel().getMfr().getName());
				}
			});
		}

		AddPrebookingJobForm apbjf = new AddPrebookingJobForm();
		apbjf.setPrebookingJobForm(pbjf);
		apbjf.setAsnItemsDTOList(asnItemsDTOList);
		Map<Integer, String> servicesType = calibrationTypeService
				.getCachedServiceTypesByJobType(apbjf.getPrebookingJobForm().getJobType(), locale);

		model.addAttribute("exchangeFormat", asn.getExchangeFormat());
		model.addAttribute("tableHeader", columnsNameMapping);
		model.addAttribute("clientcompany", companyService.get(apbjf.getPrebookingJobForm().getCoid()));
		model.addAttribute(FORM, apbjf);
		model.addAttribute("servicesType", servicesType);
		model.addAttribute("pbjf", pbjf);

		return new ModelAndView("trescal/core/logistics/effilesynthesis");
	}

	@RequestMapping(value = "/effilesynthesis.htm", method = { RequestMethod.GET })
	protected ModelAndView fileSynthesis(@Valid @ModelAttribute("pbjf") PrebookingJobForm pbjf, BindingResult result,
			@ModelAttribute("fileContent") ArrayList<LinkedCaseInsensitiveMap<String>> fileContent,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Model model,
			Locale locale) throws Exception {

		if (result.hasErrors()) {
			return new ModelAndView("trescal/core/logistics/effilesynthesis");
		}

		// get exchange format
		ExchangeFormat ef = exchangeFormatServ.get(pbjf.getExchangeFormat());

		// convert file content to list of AsnItemFinalSynthesisDTO
		List<AsnItemFinalSynthesisDTO> aiFinalSynthDtoListe = asnItemService
				.convertFileContentToJobOrAsnItemFinalSynthesisDTO(ef, fileContent);

		// get tableHeader
		Map<String, ExchangeFormatFieldNameEnum> columnsNameMapping = exchangeFormatServ.getColumnNamesMapping(ef);

		Contact userContact = userService.get(username).getCon();
		
		/* create prebooking file to save file in database */
		PrebookingFile pf = (PrebookingFile) this.efFileService.createEfFile(userContact,
				pbjf.getUploadFile(), new PrebookingFile());
		pbjf.setPrebookingFileId(pf.getId());
		
		Asn asn = null;
		if (pbjf.getAsnId() != null)
			asn = asnService.get(pbjf.getAsnId());

		List<AsnItemFinalSynthesisDTO> asnItemsDTOListe = asnService.analyseJobOrAsnItemsList(pbjf, subdivDto.getKey(),
				locale, asn, aiFinalSynthDtoListe);
		AddPrebookingJobForm apbjf = new AddPrebookingJobForm();
		apbjf.setPrebookingJobForm(pbjf);
		apbjf.setAsnItemsDTOList(asnItemsDTOListe);

		model.addAttribute("exchangeFormat", ef);
		model.addAttribute("tableHeader", columnsNameMapping);
		model.addAttribute("clientcompany", companyService.get(apbjf.getPrebookingJobForm().getCoid()));
		model.addAttribute(FORM, apbjf);
		Map<Integer, String> servicesType = calibrationTypeService
				.getCachedServiceTypesByJobType(apbjf.getPrebookingJobForm().getJobType(), locale);
		model.addAttribute("servicesType", servicesType);
		return new ModelAndView("trescal/core/logistics/effilesynthesis");
	}

	@RequestMapping(value = { "/effilesynthesis.htm", "/viewefsynthesis.htm" }, method = RequestMethod.POST, params = {
			"newanalysis", "!save" })
	protected ModelAndView newAnalysis(@ModelAttribute(FORM) AddPrebookingJobForm apbjf, BindingResult result,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto, Model model,
			Locale locale) {

		ExchangeFormat ef = exchangeFormatServ.get(apbjf.getPrebookingJobForm().getExchangeFormat());
		// get tableHeader
		Map<String, ExchangeFormatFieldNameEnum> columnsNameMapping = exchangeFormatServ.getColumnNamesMapping(ef);

		Asn asn = null;
		if (apbjf.getPrebookingJobForm().getAsnId() != null)
			asn = asnService.get(apbjf.getPrebookingJobForm().getAsnId());

		List<AsnItemFinalSynthesisDTO> asnItemsDTONewList = asnService.analyseJobOrAsnItemsList(apbjf.getPrebookingJobForm(),
				subdivDto.getKey(), locale, asn, apbjf.getAsnItemsDTOList());
		apbjf.setAsnItemsDTOList(asnItemsDTONewList);
		Map<Integer, String> servicesType = calibrationTypeService
				.getCachedServiceTypesByJobType(apbjf.getPrebookingJobForm().getJobType(), locale);

		model.addAttribute("exchangeFormat", ef);
		model.addAttribute("tableHeader", columnsNameMapping);
		model.addAttribute("clientcompany", companyService.get(apbjf.getPrebookingJobForm().getCoid()));
		model.addAttribute(FORM, apbjf);
		model.addAttribute("servicesType", servicesType);

		return new ModelAndView("trescal/core/logistics/effilesynthesis");
	}

	@RequestMapping(value = { "/effilesynthesis.htm", "/viewefsynthesis.htm" }, method = RequestMethod.POST, params = {
			"save", "!newanalysis" })
	protected ModelAndView save(@ModelAttribute(FORM) AddPrebookingJobForm apbjf, BindingResult result,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username, Model model) throws Exception {

		Contact userContact = userService.get(username).getCon();

		asnService.savePreBookingASN(apbjf.getPrebookingJobForm(), apbjf.getAsnItemsDTOList(), userContact);

		return new ModelAndView(new RedirectView("manageprebooking.htm"));
	}

}
