package org.trescal.cwms.core.logistics.form;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.logistics.utils.RecordTypeEnum;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class PrebookingJobForm {

	private Integer addrid;
	private Integer bookedInAddrId;
	private Integer bookedInLocId;
	private Integer bpo;
	private String clientref;
	private Integer coid;
	private String currencyCode;
	private BigDecimal estCarriageOut;
	private JobType jobType;
	private Integer locid;
	private Integer personid;
	private List<String> poCommentList;
	private String ponumber;
	private List<String> poNumberList;
	private List<Integer> poIdList;
	private Integer subdivid;
	private String comments;

	private RecordTypeEnum recordType;
    private Integer exchangeFormat;
    private MultipartFile uploadFile;

    private Integer defaultPO;
    private Integer scannedBarcode;
    private List<Integer> calTypeIds;

    private Integer prebookingFileId;
    private Integer asnId;

    private Date receiptDate;
    private Date pickupDate;
    private Boolean overrideDateInOnJobItems;
    private Integer transportIn;
    private Integer carrier;
    private String trackingNumber;
    private Integer transportOut;
    private Integer numberOfPackages;
    private String packageType;
    private String storageArea;

    public PrebookingJobForm() {
	}

	public PrebookingJobForm(Integer addrid, Integer bookedInAddrId, Integer bookedInLocId, Integer bpo,
			String clientref, String currencyCode, BigDecimal estCarriageOut, JobType jobType, Integer personid,
			List<String> poCommentList, List<String> poNumberList, Integer subdivid, Integer defaultPO,
			Date pickupDate) {
		super();
		this.addrid = addrid;
		this.bookedInAddrId = bookedInAddrId;
		this.bookedInLocId = bookedInLocId;
		this.bpo = bpo;
		this.clientref = clientref;
		this.currencyCode = currencyCode;
		this.estCarriageOut = estCarriageOut;
		this.jobType = jobType;
		this.personid = personid;
		this.poCommentList = poCommentList;
		this.poNumberList = poNumberList;
		this.subdivid = subdivid;
		this.defaultPO = defaultPO;
        if (pickupDate != null)
            this.pickupDate = pickupDate;
	}

}
