package org.trescal.cwms.core.logistics.form;

import org.trescal.cwms.core.logistics.entity.asnitems.AsnItem;

public class JobFinalSynthesis {

	private String subFamily;
	private int rowSpan;
	private boolean checked;
	private AsnItem asnItem;
	private String logisticsComment;
	private Integer nextStatuId;
	private Integer serviceTypeId;

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public AsnItem getAsnItem() {
		return asnItem;
	}

	public void setAsnItem(AsnItem asnItem) {
		this.asnItem = asnItem;
	}

	public String getLogisticsComment() {
		return logisticsComment;
	}

	public void setLogisticsComment(String logisticsComment) {
		this.logisticsComment = logisticsComment;
	}

	public Integer getNextStatuId() {
		return nextStatuId;
	}

	public void setNextStatuId(Integer nextStatuId) {
		this.nextStatuId = nextStatuId;
	}

	public Integer getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(Integer serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	public JobFinalSynthesis(AsnItem asnItem, String subFamily, int rowSpan) {
		super();
		this.asnItem = asnItem;
		this.subFamily = subFamily;
		this.rowSpan = rowSpan;
	}

	public String getSubFamily() {
		return subFamily;
	}

	public void setSubFamily(String subFamily) {
		this.subFamily = subFamily;
	}

	public int getRowSpan() {
		return rowSpan;
	}

	public void setRowSpan(int rowSpan) {
		this.rowSpan = rowSpan;
	}

	public JobFinalSynthesis() {
		super();
	}

}
