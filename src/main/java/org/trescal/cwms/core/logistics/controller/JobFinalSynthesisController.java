package org.trescal.cwms.core.logistics.controller;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.ExchangeFormat;
import org.trescal.cwms.core.exchangeformat.entity.exchangeformat.db.ExchangeFormatService;
import org.trescal.cwms.core.exchangeformat.enums.ExchangeFormatFieldNameEnum;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.logistics.entity.asn.dao.AsnService;
import org.trescal.cwms.core.logistics.entity.asnitems.dao.AsnitemService;
import org.trescal.cwms.core.logistics.form.AsnItemFinalSynthesisDTO;
import org.trescal.cwms.core.logistics.form.JobFinalSynthesisDTOForm;
import org.trescal.cwms.core.logistics.form.PrebookingJobForm;
import org.trescal.cwms.core.logistics.form.validator.JobFinalSynthesisFormValidator;
import org.trescal.cwms.core.logistics.utils.AsnItemStatusEnum;
import org.trescal.cwms.core.logistics.utils.RecordTypeEnum;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.DefaultServiceTypeForOnsiteJobs;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.DefaultServiceTypeForStandardJobs;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.dto.ItemStateDTO;
import org.trescal.cwms.core.workflow.entity.itemstate.db.ItemStateService;
import org.trescal.cwms.spring.model.KeyValue;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME,
		Constants.SESSION_ATTRIBUTE_COMPANY })
public class JobFinalSynthesisController {

	private final String JFS_FORM = "jobFinalSynthForm";
	private static final String JFS_REQUEST_URL = "/jobfinalsynthesis.htm";
	private static final String JFS_VIEW = "trescal/core/logistics/jobfinalsynthesis";
	private static final Integer AUTO_GROW_COLLECTION_LIMIT = 1024;

	@Autowired
	private ExchangeFormatService exchangeFormatServ;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private AsnService asnService;
	@Autowired
	private UserService userService;
	@Autowired
	private ItemStateService itemStateService;
	@Autowired
	private JobFinalSynthesisFormValidator JfsValidator;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private JobService jobService;
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private AsnitemService asnItemService;
	@Autowired
	protected TranslationService translationService;
	@Autowired
	private CalibrationTypeService calTypeService;
	@Autowired
	private DefaultServiceTypeForOnsiteJobs serviceTypeForOnSiteJob;
	@Autowired
	private DefaultServiceTypeForStandardJobs serviceTypeForStandardJob;

	@InitBinder(JFS_FORM)
	protected void initBinder(WebDataBinder binder) {
		binder.setAutoGrowCollectionLimit(AUTO_GROW_COLLECTION_LIMIT);
		binder.setValidator(JfsValidator);
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
	}

	@RequestMapping(value = JFS_REQUEST_URL, method = RequestMethod.GET)
	protected ModelAndView finalSynthesis(@Valid @ModelAttribute("pbjf") PrebookingJobForm pbjf,
			@ModelAttribute("fileContent") ArrayList<LinkedCaseInsensitiveMap<String>> fileContent,
			@ModelAttribute("jobId") String jobid,
			@ModelAttribute("addItemsToJobAlreadyCreated") Boolean addItemsToJobAlreadyCreated,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompany,
			BindingResult result, Model model, Locale locale) throws Exception {

		if (result.hasErrors()) {
			return new ModelAndView(JFS_VIEW);
		}

		String disabled = "";
		// get exchange format and prebooking
		ExchangeFormat ef;
		Asn asn = null;
		if (RecordTypeEnum.PREBOOKING.equals(pbjf.getRecordType())) {
			asn = asnService.get(pbjf.getAsnId());
			ef = asn.getExchangeFormat();
		} else
			ef = exchangeFormatServ.get(pbjf.getExchangeFormat());

		// convert the File Content to list of AsnItemFinalSynthesisDTO
		List<AsnItemFinalSynthesisDTO> jiFinalSynthDtoList = asnItemService
			.convertFileContentToJobOrAsnItemFinalSynthesisDTO(ef, fileContent);

		// get tableHeader
		Map<String, ExchangeFormatFieldNameEnum> columnsNameMapping = exchangeFormatServ.getColumnNamesMapping(ef);

		// create and initialize the JobFinalSynthesisDTOForm
		JobFinalSynthesisDTOForm jfsf = new JobFinalSynthesisDTOForm();
		jfsf.setPbjf(pbjf);
		List<AsnItemFinalSynthesisDTO> res = asnService.analyseJobOrAsnItemsList(pbjf, subdivDto.getKey(), locale, asn,
			jiFinalSynthDtoList);

		Map<Integer, String> servicesType = calTypeService.getCachedServiceTypesByJobType(pbjf.getJobType(), locale);

		val split = splitIdentified(res, allocatedCompany, jfsf.getPbjf(), servicesType);

		jfsf.setIdentifiedJobItemsFinalSynthDTO(split._1);
		jfsf.setNonIdentifiedJobItemsFinalSynthDTO(split._2);

		// disabled the createJob button if we already have a job (add items in
		// the job already exist)
		if (addItemsToJobAlreadyCreated) {
			disabled = "disabled";
		}
		// data to send to synthesis page
		model.addAttribute(JFS_FORM, jfsf);
		model.addAttribute("exchangeFormat", ef);
		model.addAttribute("tableHeader", columnsNameMapping);
		model.addAttribute("clientcompany", companyService.get(jfsf.getPbjf().getCoid()));
		List<ItemStateDTO> itemsState = itemStateService.getItemStatusForPrebooking(pbjf.getJobType(), locale);
		model.addAttribute("status", itemsState);
		model.addAttribute("servicetypes", servicesType);
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Integer jobDefaultCaltypeId = this.calTypeServ.getDefaultCalType(pbjf.getJobType(), allocatedSubdiv)
				.getCalTypeId();
		model.addAttribute("jobDefaultCaltypeId", jobDefaultCaltypeId);
		model.addAttribute("disabled", disabled);
		model.addAttribute("jobid", jobid);
		return new ModelAndView(JFS_VIEW);
	}

	@RequestMapping(value = JFS_REQUEST_URL, method = RequestMethod.POST, params = { "newanalysis", "!save",
			"!additems" })
	protected ModelAndView newAnalysis(
			@RequestParam(name = "disbledCreateJob", required = false, defaultValue = " ") String disbledCreateJob,
			@RequestParam(name = "jobid", required = false, defaultValue = "null") String jobid,
			@Valid @ModelAttribute(JFS_FORM) JobFinalSynthesisDTOForm form, BindingResult result,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompany,
			Model model, Locale locale) {

		// get exchange format and prebooking
		Optional<Asn> asn = Optional.ofNullable(form.getPbjf().getRecordType())
				.filter(r -> r.equals(RecordTypeEnum.PREBOOKING))
				.map(_unused -> asnService.get(form.getPbjf().getAsnId()));

		val jiFinalSynthDtoList = Stream.concat(form.getIdentifiedJobItemsFinalSynthDTO().stream(),
				form.getNonIdentifiedJobItemsFinalSynthDTO().stream()).collect(Collectors.toList());

		// analyse
		List<AsnItemFinalSynthesisDTO> res = asnService.analyseJobOrAsnItemsList(form.getPbjf(), subdivDto.getKey(),
				locale, asn.orElse(null), jiFinalSynthDtoList);

		// split the data : identified and non identified, and initialize
		// service types
		// for identified items
		Map<Integer, String> servicesType = calTypeService.getCachedServiceTypesByJobType(form.getPbjf().getJobType(),
				locale);

		val split = splitIdentified(res, allocatedCompany, form.getPbjf(), servicesType);

		form.setIdentifiedJobItemsFinalSynthDTO(split._1);
		form.setNonIdentifiedJobItemsFinalSynthDTO(split._2);

		// get tableHeader
		val ef = asn.map(Asn::getExchangeFormat)
				.orElseGet(() -> exchangeFormatServ.get(form.getPbjf().getExchangeFormat()));
		val columnsNameMapping = exchangeFormatServ.getColumnNamesMapping(ef);

		model.addAttribute(JFS_FORM, form);
		model.addAttribute("tableHeader", columnsNameMapping);
		model.addAttribute("clientcompany", companyService.get(form.getPbjf().getCoid()));
		List<ItemStateDTO> itemsState = itemStateService.getItemStatusForPrebooking(form.getPbjf().getJobType(),
				locale);
		model.addAttribute("status", itemsState);
		model.addAttribute("servicetypes", servicesType);
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Integer jobDefaultCaltypeId = this.calTypeServ.getDefaultCalType(form.getPbjf().getJobType(), allocatedSubdiv)
				.getCalTypeId();
		model.addAttribute("jobDefaultCaltypeId", jobDefaultCaltypeId);
		model.addAttribute("disabled", disbledCreateJob);
		model.addAttribute("jobid", jobid);
		return new ModelAndView(JFS_VIEW);
	}

	private Tuple2<List<AsnItemFinalSynthesisDTO>, List<AsnItemFinalSynthesisDTO>> splitIdentified(
			List<AsnItemFinalSynthesisDTO> analysisResult, KeyValue<Integer, String> allocatedCompany,
			PrebookingJobForm pbjf, Map<Integer, String> servicesType) {

		Integer serviceTypeId = calculateServiceTypeId(allocatedCompany, pbjf);

		val groupedResults = analysisResult.stream()
				.collect(Collectors.groupingBy(i -> AsnItemStatusEnum.IDENTIFIED.equals(i.getStatus())));
		val identified = groupedResults.get(true) != null ? groupedResults.get(true)
				: Collections.<AsnItemFinalSynthesisDTO>emptyList();
		val nonIdentified = groupedResults.get(false) != null ? groupedResults.get(false)
				: Collections.<AsnItemFinalSynthesisDTO>emptyList();

		identified.forEach(d -> jobService.initializeJobItemsServiceTypes(d, servicesType, serviceTypeId));
		return Tuple.of(identified, nonIdentified);
	}

	private Integer calculateServiceTypeId(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompany,
			PrebookingJobForm pbjf) {
		ServiceType serv = null;

		if (pbjf.getJobType().equals(JobType.SITE)) {
			serv = this.serviceTypeForOnSiteJob.getValueByScope(Scope.COMPANY, allocatedCompany.getKey(),
					allocatedCompany.getKey());
		} else if (pbjf.getJobType().equals(JobType.STANDARD)) {
			serv = this.serviceTypeForStandardJob.getValueByScope(Scope.COMPANY, allocatedCompany.getKey(),
					allocatedCompany.getKey());
		}
		return serv != null ? serv.getServiceTypeId() : null;
	}

	@RequestMapping(value = JFS_REQUEST_URL, method = RequestMethod.POST, params = { "!newanalysis", "save",
			"!addiems" })
	protected ModelAndView finalSynthesisPost(
			@RequestParam(name = "disbledCreateJob", required = false, defaultValue = " ") String disbledCreateJob,
			@RequestParam(name = "jobid", required = false, defaultValue = "null") String jobid,
			@Valid @ModelAttribute(JFS_FORM) JobFinalSynthesisDTOForm form, BindingResult result,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("disabled") String disabled,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompany,
			Model model, Locale locale) {

		if (result.hasErrors()) {
			return newAnalysis(disbledCreateJob, jobid, form, result, username, subdivDto, allocatedCompany, model,
					locale);
		}

		Contact contact = this.userService.get(username).getCon();
		Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());

		// remove non-checked items
		form.setNonIdentifiedJobItemsFinalSynthDTO(Collections.emptyList());
		form.setIdentifiedJobItemsFinalSynthDTO(form.getIdentifiedJobItemsFinalSynthDTO().stream()
				.filter(AsnItemFinalSynthesisDTO::isChecked).collect(Collectors.toList()));

		Job j = jobService.createJobFromPrebookingOrFile(form, contact, allocatedSubdiv);

		return new ModelAndView(new RedirectView("viewjob.htm?jobid=" + j.getJobid()));

	}

	@RequestMapping(value = JFS_REQUEST_URL, method = RequestMethod.POST, params = { "!newanalysis", "!save",
			"additems" })
	protected ModelAndView additemsToJobAlreadyCreated(
			@RequestParam(name = "disbledCreateJob", required = false, defaultValue = " ") String disabledCreateJob,
			@RequestParam(name = "jobid") String jobid,
			@Valid @ModelAttribute(JFS_FORM) JobFinalSynthesisDTOForm form, BindingResult result,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> allocatedCompany,
			Model model, Locale locale) {

		if (result.hasErrors()) {
			return newAnalysis(disabledCreateJob, jobid, form, result, username, subdivDto, allocatedCompany, model,
					locale);
		}

		Contact contact = this.userService.get(username).getCon();

		// remove non-checked items
		form.setNonIdentifiedJobItemsFinalSynthDTO(Collections.emptyList());
		form.setIdentifiedJobItemsFinalSynthDTO(form.getIdentifiedJobItemsFinalSynthDTO().stream()
				.filter(AsnItemFinalSynthesisDTO::isChecked).collect(Collectors.toList()));

		PrebookingJobForm pbjf = form.getPbjf();
		// add items to job
		Job j = jobService.addItemsToJobAlreadyExist(jobService.get(Integer.parseInt(jobid)), form, contact,
				pbjf.getAsnId());

		return new ModelAndView(new RedirectView("viewjob.htm?jobid=" + j.getJobid()));

	}

}
