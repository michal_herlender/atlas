package org.trescal.cwms.core.logistics.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;

@Controller @IntranetController
@SessionAttributes(value = { Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_SUBDIV })
public class ScanInOutController {
	
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;

	@RequestMapping(value="scanout.htm")
	public String viewScanOut(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivKeyValue) {
		User user = userService.get(username);
		Subdiv subdiv = subdivService.get(subdivKeyValue.getKey());
		Address defaultAddress = subdiv.getDefaultAddress();
		if (defaultAddress == null) defaultAddress = subdiv.getAddresses().stream().findFirst().orElse(null);
		if (defaultAddress == null) defaultAddress = user.getCon().getDefAddress();
		model.addAttribute("businessAddresses", getAddresses(user));
		model.addAttribute("defaultAddress", defaultAddress);
		model.addAttribute("focusElement", "scanout");
		return "trescal/core/logistics/scanitems";
	}

	@RequestMapping(value="scanin.htm")
	public String viewScanIn(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivKeyValue) {
		User user = userService.get(username);
		Subdiv subdiv = subdivService.get(subdivKeyValue.getKey());
		Address defaultAddress = subdiv.getDefaultAddress();
		if (defaultAddress == null) defaultAddress = subdiv.getAddresses().stream().findFirst().orElse(null);
		if (defaultAddress == null) defaultAddress = user.getCon().getDefAddress();
		model.addAttribute("businessAddresses", getAddresses(user));
		model.addAttribute("defaultAddress", defaultAddress);
		model.addAttribute("focusElement", "scanin");
		return "trescal/core/logistics/scanitems";
	}
	
	private List<Address> getAddresses(User user) {
		List<Address> addresses = new ArrayList<Address>();
		Set<Subdiv> userSubdivs = user.getUserRoles().stream().map(ur -> ur.getOrganisation())
				.collect(Collectors.toSet());
		for (Subdiv subdiv : userSubdivs) {
			List<Address> subdivAddresses = subdiv.getAddresses().stream().filter(Address::getActive)
					.collect(Collectors.toList());
			addresses.addAll(subdivAddresses);
		}
		return addresses;
	}
}
