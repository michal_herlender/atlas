package org.trescal.cwms.core.logistics.entity.prebookingdetails.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails;

@Service
public class PrebookingJobDetailsServiceImpl extends BaseServiceImpl<PrebookingJobDetails, Integer>
		implements PrebookingJobDetailsService {

	@Autowired
	private PrebookingJobDetailsDao dao;
	
	@Override
	protected BaseDao<PrebookingJobDetails, Integer> getBaseDao() {
		return dao;
	}
	
}
