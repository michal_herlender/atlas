package org.trescal.cwms.core.logistics.entity.analysisresult.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.logistics.entity.analysisresult.AsnItemAnalysisResult;

@Service
public class AsnItemAnalysisResultServiceImpl extends BaseServiceImpl<AsnItemAnalysisResult, Integer>
		implements AsnItemAnalysisResultService {

	@Autowired
	private AsnItemAnalysisResultDao dao;
	
	@Override
	protected BaseDao<AsnItemAnalysisResult, Integer> getBaseDao() {
		return dao;
	}
	
}
