package org.trescal.cwms.core.logistics.entity.jobcourier.db;


import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.logistics.entity.jobcourier.JobCourierLink;

public interface JobCourierLinkDao extends BaseDao<JobCourierLink, Integer> {

}
