package org.trescal.cwms.core.logistics.utils;

public enum JobTypeEnum {
	
	STANDARD("Standard"),
	PREBOOKING("Prebooking");
	
private String fullName;
	
	private JobTypeEnum(String fullName) {
		this.fullName = fullName;
	}

	public String getFullName() {
		return fullName;
	}
	
	public static JobTypeEnum parseFullName(String fullName) {
		JobTypeEnum result = null;
		for (JobTypeEnum status : JobTypeEnum.values()) {
			if (status.getFullName().equals(fullName)) {
				result = status;
				break;
			}
		}
		return result;
	}
	
	public String getValue() {
		return name();
		}

		public void setValue(String value) {}

}