package org.trescal.cwms.core.logistics.entity.prebookingdetails;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.deliverynote.entity.transportoption.TransportOption;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.jobs.job.entity.po.PO;
import org.trescal.cwms.core.logistics.entity.asn.Asn;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "prebookingjobdetails")
public class PrebookingJobDetails {

	private Integer id;
	private Address bookedInAddr;
	private Location bookedInLoc;
	private BPO bpo;
	private String clientRef;
	private Contact con;
	protected SupportedCurrency currency;
	private BigDecimal estCarriageOut;
	private TransportOption inOption;
	private TransportOption returnOption;
	private Address returnTo;
	private Location returnToLoc;
	private Asn asn;
	private List<PO> poList;
	private PO defaultPO;
	private Date pickupDate;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "asnid", foreignKey = @ForeignKey(name = "FK_prebookingjobdetails_asnid"))
	public Asn getAsn() {
		return asn;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bookedinaddrid", foreignKey = @ForeignKey(name = "FK_prebookingjobdetails_bookedinaddrid"))
	public Address getBookedInAddr() {
		return this.bookedInAddr;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bookedinlocid", foreignKey = @ForeignKey(name = "FK_prebookingjobdetails_bookedinlocid"))
	public Location getBookedInLoc() {
		return this.bookedInLoc;
	}

	/**
	 * @return the clientRef
	 */
	@Length(max = 30)
	@Column(name = "clientref", length = 30)
	public String getClientRef() {
		return this.clientRef;
	}

	/**
	 * @return the con
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", foreignKey = @ForeignKey(name = "FK_prebookingjobdetails_personid"))
	public Contact getCon() {
		return this.con;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currencyid", nullable = false, foreignKey = @ForeignKey(name = "FK_prebookingjobdetails_currencyid"))
	public SupportedCurrency getCurrency() {
		return this.currency;
	}

	@OneToMany(mappedBy = "prebooking", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	public List<PO> getPoList() {
		return this.poList;
	}

	@NotNull
	@Column(name = "estcarout", nullable = false, precision = 10, scale = 2)
	public BigDecimal getEstCarriageOut() {
		return this.estCarriageOut;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inoptionid", foreignKey = @ForeignKey(name = "FK_prebookingjobdetails_inoptionid"))
	public TransportOption getInOption() {
		return this.inOption;
	}

	@Transient
	public ComponentEntity getParentEntity() {
		return null;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "returnoptionid", foreignKey = @ForeignKey(name = "FK_prebookingjobdetails_returnoptionid"))
	public TransportOption getReturnOption() {
		return this.returnOption;
	}

	/**
	 * @return the returnTo
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "returnto", foreignKey = @ForeignKey(name = "FK_prebookingjobdetails_returnto"))
	public Address getReturnTo() {
		return this.returnTo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "returntoloc", foreignKey = @ForeignKey(name = "FK_prebookingjobdetails_returntoloc"))
	public Location getReturnToLoc() {
		return this.returnToLoc;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "defaultpoid", foreignKey = @ForeignKey(name = "FK_prebookingjobdetails_defaultpoid"))
	public PO getDefaultPO() {
		return this.defaultPO;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setAsn(Asn asn) {
		this.asn = asn;
	}

	public void setBookedInAddr(Address bookedInAddr) {
		this.bookedInAddr = bookedInAddr;
	}

	public void setBookedInLoc(Location bookedInLoc) {
		this.bookedInLoc = bookedInLoc;
	}

	/**
	 * @param clientRef
	 *            the clientRef to set
	 */
	public void setClientRef(String clientRef) {
		this.clientRef = clientRef;
	}

	/**
	 * @param con
	 *            the con to set
	 */
	public void setCon(Contact con) {
		this.con = con;
	}

	public void setCurrency(SupportedCurrency currency) {
		this.currency = currency;
	}

	public void setPoList(List<PO> poList) {
		this.poList = poList;
	}

	public void setEstCarriageOut(BigDecimal estCarriageOut) {
		this.estCarriageOut = estCarriageOut;
	}

	public void setInOption(TransportOption inOption) {
		this.inOption = inOption;
	}

	public void setReturnOption(TransportOption returnOption) {
		this.returnOption = returnOption;
	}

	/**
	 * @param returnTo
	 *            the returnTo to set
	 */
	public void setReturnTo(Address returnTo) {
		this.returnTo = returnTo;
	}

	public void setReturnToLoc(Location returnToLoc) {
		this.returnToLoc = returnToLoc;
	}

	public void setDefaultPO(PO defaultPO) {
		this.defaultPO = defaultPO;
	}
	
	/**
	 * @return the bpo
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bpoid")
	public BPO getBpo()
	{
		return this.bpo;
	}
	
	/**
	 * @param bpo the bpo to set
	 */
	public void setBpo(BPO bpo)
	{
		this.bpo = bpo;
	}
	
	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

}