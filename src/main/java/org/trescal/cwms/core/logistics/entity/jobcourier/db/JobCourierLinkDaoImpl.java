package org.trescal.cwms.core.logistics.entity.jobcourier.db;


import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.logistics.entity.jobcourier.JobCourierLink;

@Repository
public class JobCourierLinkDaoImpl extends BaseDaoImpl<JobCourierLink, Integer> implements JobCourierLinkDao {

	@Override
	protected Class<JobCourierLink> getEntity() {
		return JobCourierLink.class;
	}
}
