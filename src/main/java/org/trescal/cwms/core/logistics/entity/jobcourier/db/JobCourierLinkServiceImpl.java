package org.trescal.cwms.core.logistics.entity.jobcourier.db;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.logistics.entity.jobcourier.JobCourierLink;

@Service
public class JobCourierLinkServiceImpl extends BaseServiceImpl<JobCourierLink, Integer>
		implements JobCourierLinkService {

	@Autowired
	private JobCourierLinkDao dao;
	
	@Override
	protected BaseDao<JobCourierLink, Integer> getBaseDao() {
		return dao;
	}
	
}
