package org.trescal.cwms.core.logistics.entity.prebookingdetails.dao;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.logistics.entity.prebookingdetails.PrebookingJobDetails;

public interface PrebookingJobDetailsService extends BaseService<PrebookingJobDetails, Integer> {

}
