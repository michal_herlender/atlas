package org.trescal.cwms.core.logistics.utils;

public enum RecordTypeEnum {
	
	STANDARD("Standard"),
	FILE("File"),
	PREBOOKING("Prebooking");
	
private String fullName;
	
	private RecordTypeEnum(String fullName) {
		this.fullName = fullName;
	}

	public String getFullName() {
		return fullName;
	}
	
	public static RecordTypeEnum parseFullName(String fullName) {
		RecordTypeEnum result = null;
		for (RecordTypeEnum status : RecordTypeEnum.values()) {
			if (status.getFullName().equals(fullName)) {
				result = status;
				break;
			}
		}
		return result;
	}
	
	public String getValue() {
		return name();
		}

		public void setValue(String value) {}

}