package org.trescal.cwms.core.logistics.form;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class JobFinalSynthesisDTOForm {

	private List<AsnItemFinalSynthesisDTO> identifiedJobItemsFinalSynthDTO = new ArrayList<>();
	private List<AsnItemFinalSynthesisDTO> nonIdentifiedJobItemsFinalSynthDTO = new ArrayList<>();
	private PrebookingJobForm pbjf;
	private String newanalysis;
}
