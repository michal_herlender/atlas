package org.trescal.cwms.core.logistics.dto;

import org.trescal.cwms.core.jobs.job.entity.po.PO;

public class PoDTO {

	private Integer poId;
	private String poNumber;
	private String comment;
	
	public PoDTO(PO po) {
		this.poId = po.getPoId();
		this.poNumber = po.getPoNumber();
		this.comment = po.getComment();
	}

	public PoDTO(Integer poId, String poNumber, String comment) {
		super();
		this.poId = poId;
		this.poNumber = poNumber;
		this.comment = comment;
	}

	public int getPoId() {
		return poId;
	}

	public void setPoId(Integer poId) {
		this.poId = poId;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	
}
