package org.trescal.cwms.core.cdc.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.EqualsAndHashCode;
import lombok.Setter;

/**
 * MS SQL Change Data Capture CDC records have two log/sequence values,
 *  which taken together can form a unique primary key for Hibernate / JPA mapping
 *  (individually, they are not unique, so just using startLsn results in duplicates per commit)  
 * This class defines the composite ID based on these two columns
 */
@Embeddable
@Setter
@EqualsAndHashCode
@JsonIgnoreProperties({"startLsn", "seqVal"})
public class CdcCompositeId implements Serializable {
	
	private byte[] startLsn;
	private byte[] seqVal;

	private static final long serialVersionUID = 1L;

	@Column(name = "__$start_lsn")
	public byte[] getStartLsn() {
		return startLsn;
	}

	@Column(name = "__$seqval")
	public byte[] getSeqVal() {
		return seqVal;
	}
	
}
