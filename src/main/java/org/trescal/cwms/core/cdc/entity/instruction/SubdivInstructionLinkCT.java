package org.trescal.cwms.core.cdc.entity.instruction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

@Entity
@Table(name = "dbo_subdivinstructionlink_CT", schema = "cdc")
public class SubdivInstructionLinkCT extends EntityCT {
	

	private Integer id;

	private Integer instructionid;

	private Integer subdivid;

	private Integer orgid;

	private Boolean issubdivinstruction;

	private Integer businesssubdivid;

	/**
	 * @return the id
	 */
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	/**
	 * @return the instructionid
	 */
	@Column(name = "instructionid")
	public Integer getInstructionid() {
		return instructionid;
	}

	@JoinColumn(name = "subdivid")
	public Integer getSubdivid() {
		return subdivid;
	}
	
	@JoinColumn(name = "orgid")
	public Integer getOrgid() {
		return orgid;
	}
	
	@JoinColumn(name = "issubdivinstruction")
	public Boolean getIssubdivinstruction() {
		return issubdivinstruction;
	}
	
	@JoinColumn(name = "businesssubdivid")
	public Integer getBusinesssubdivid() {
		return businesssubdivid;
	}
	
	protected void setSubdivid(Integer subdivid) {
		this.subdivid = subdivid;
	}
	
	protected void setOrgid(Integer orgid) {
		this.orgid = orgid;
	}
	
	protected void setIssubdivinstruction(Boolean issubdivinstruction) {
		this.issubdivinstruction = issubdivinstruction;
	}
	
	protected void setBusinesssubdivid(Integer businesssubdivid) {
		this.businesssubdivid = businesssubdivid;
	}

	/**
	 * @param id the id to set
	 */
	protected void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param instructionid the instructionid to set
	 */
	protected void setInstructionid(Integer instructionid) {
		this.instructionid = instructionid;
	}
	
	
	
}
