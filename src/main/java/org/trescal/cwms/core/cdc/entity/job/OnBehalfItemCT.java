package org.trescal.cwms.core.cdc.entity.job;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

import lombok.Setter;

@Setter
@Entity
@Table(name = "dbo_onbehalfitem_CT", schema = "cdc")
public class OnBehalfItemCT extends EntityCT {
	
	private Integer id;
	private Integer addressid;
	private Integer coid;
	private Integer jobitemid;
	
	
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	
	@Column(name = "addressid")
	public Integer getAddressid() {
		return addressid;
	}
	
	@Column(name = "coid")
	public Integer getCoid() {
		return coid;
	}
	
	@Column(name = "jobitemid")
	public Integer getJobitemid() {
		return jobitemid;
	}


}
