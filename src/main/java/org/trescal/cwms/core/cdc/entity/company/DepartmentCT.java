package org.trescal.cwms.core.cdc.entity.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;

@Entity
@Table(name = "dbo_department_CT", schema = "cdc")
public class DepartmentCT extends EntityCT {
	

	private Integer deptid;

	private String name;

	private Integer addressid;

	private Integer subdivid;

	private DepartmentType type;

	private String shortName;
	
	@Column(name = "deptid")
	public Integer getDeptid() {
		return deptid;
	}
	
	@Column(name = "name")
	public String getName() {
		return name;
	}
	
	@Column(name = "addrid")
	public Integer getAddressid() {
		return addressid;
	}
	
	@Column(name = "subdivid")
	public Integer getSubdivid() {
		return subdivid;
	}
	
	@Column(name = "typeid")
	public DepartmentType getType() {
		return type;
	}
	
	@Column(name = "shortname")
	public String getShortName() {
		return shortName;
	}

	protected void setDeptid(Integer deptid) {
		this.deptid = deptid;
	}

	protected void setName(String name) {
		this.name = name;
	}

	protected void setAddressid(Integer addressid) {
		this.addressid = addressid;
	}

	protected void setSubdivid(Integer subdivid) {
		this.subdivid = subdivid;
	}

	protected void setType(DepartmentType type) {
		this.type = type;
	}

	protected void setShortName(String shortName) {
		this.shortName = shortName;
	}
	
}
