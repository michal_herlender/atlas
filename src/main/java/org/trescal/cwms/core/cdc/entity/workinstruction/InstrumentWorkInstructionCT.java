/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.workinstruction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

/**
 * @Author shazil.khan
 *
 */

@Entity
@Table(name = "dbo_instrumentworkinstruction_CT", schema = "cdc")
public class InstrumentWorkInstructionCT extends EntityCT {
	

	private Integer workInstructionId;

	private Integer plantId;

	/**
	 * @return the workInstructionId
	 */
	@Column(name = "workInstructionId")
	public Integer getWorkInstructionId() {
		return workInstructionId;
	}

	/**
	 * @return the plantId
	 */
	@Column(name = "plantId")
	public Integer getPlantId() {
		return plantId;
	}

	/**
	 * @param workInstructionId the workInstructionId to set
	 */
	protected void setWorkInstructionId(Integer workInstructionId) {
		this.workInstructionId = workInstructionId;
	}

	/**
	 * @param plantId the plantId to set
	 */
	protected void setPlantId(Integer plantId) {
		this.plantId = plantId;
	}
	
	
	

}
