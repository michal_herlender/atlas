package org.trescal.cwms.core.cdc.service;

public interface CdcRecoveryService {
	
	// See comments in implementation
	public String performRecovery20210802(boolean commit);
}
