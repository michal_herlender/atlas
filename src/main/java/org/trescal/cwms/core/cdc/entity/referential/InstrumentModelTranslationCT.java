package org.trescal.cwms.core.cdc.entity.referential;

import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

@Entity
@Table(name = "dbo_instmodeltranslation_CT", schema = "cdc")
public class InstrumentModelTranslationCT extends EntityCT {
	

	private Integer id;

	private Locale locale;

	private String translation;
	
	@Column(name = "modelid")
	public Integer getId() {
		return id;
	}
	
	@Column(name = "locale")
	public Locale getLocale() {
		return locale;
	}
	
	@Column(name = "translation")
	public String getTranslation() {
		return translation;
	}

	protected void setId(Integer id) {
		this.id = id;
	}

	protected void setLocale(Locale locale) {
		this.locale = locale;
	}

	protected void setTranslation(String translation) {
		this.translation = translation;
	}

}
