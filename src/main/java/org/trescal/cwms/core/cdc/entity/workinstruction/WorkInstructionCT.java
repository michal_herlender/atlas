/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.workinstruction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

/**
 * @Author shazil.khan
 *
 */
@Entity
@Table(name = "dbo_workinstruction_CT", schema = "cdc")
public class WorkInstructionCT extends EntityCT {
	
	private Integer id;
	
	private String name;

	private String title;
	
	private Integer orgid;
	
	private Integer descid;

	/**
	 * @return the id
	 */
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	@Column(name = "name")
	public String getName() {
		return name;
	}

	/**
	 * @return the title
	 */
	@Column(name = "title")
	public String getTitle() {
		return title;
	}

	/**
	 * @return the orgid
	 */
	@Column(name = "orgid")
	public Integer getOrgid() {
		return orgid;
	}

	/**
	 * @return the descid
	 */
	@Column(name = "descid")
	public Integer getDescid() {
		return descid;
	}

	/**
	 * @param id the id to set
	 */
	protected void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param name the name to set
	 */
	protected void setName(String name) {
		this.name = name;
	}

	/**
	 * @param title the title to set
	 */
	protected void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @param orgid the orgid to set
	 */
	protected void setOrgid(Integer orgid) {
		this.orgid = orgid;
	}

	/**
	 * @param descid the descid to set
	 */
	protected void setDescid(Integer descid) {
		this.descid = descid;
	}
	
	

}
