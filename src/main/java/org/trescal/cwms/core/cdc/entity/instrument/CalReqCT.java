package org.trescal.cwms.core.cdc.entity.instrument;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

@Entity
@Table(name = "dbo_calreq_CT", schema = "cdc")
public class CalReqCT extends EntityCT {
	

	private String type;

	private Integer id;

	private Boolean active;

	private String publicInstructions;

	private String privateInstructions;

	private Integer jobitemid;

	private Integer plantid;

	private Integer compid;

	private Integer modelid;

	private Integer descid;
	
	@Column(name = "type")
	public String getType() {
		return type;
	}
	
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	
	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}
	
	@Column(name = "publicinstructions")
	public String getPublicInstructions() {
		return publicInstructions;
	}
	
	@Column(name = "instructions")
	public String getPrivateInstructions() {
		return privateInstructions;
	}
	
	@Column(name = "jobitemid")
	public Integer getJobitemid() {
		return jobitemid;
	}
	
	@Column(name = "plantid")
	public Integer getPlantid() {
		return plantid;
	}
	
	@Column(name = "compid")
	public Integer getCompid() {
		return compid;
	}
	
	@Column(name = "modelid")
	public Integer getModelid() {
		return modelid;
	}
	
	@Column(name = "descid")
	public Integer getDescid() {
		return descid;
	}

	protected void setType(String type) {
		this.type = type;
	}

	protected void setId(Integer id) {
		this.id = id;
	}

	protected void setActive(Boolean active) {
		this.active = active;
	}

	protected void setPublicInstructions(String publicInstructions) {
		this.publicInstructions = publicInstructions;
	}

	protected void setPrivateInstructions(String privateInstructions) {
		this.privateInstructions = privateInstructions;
	}

	protected void setJobitemid(Integer jobitemid) {
		this.jobitemid = jobitemid;
	}

	protected void setPlantid(Integer plantid) {
		this.plantid = plantid;
	}

	protected void setCompid(Integer compid) {
		this.compid = compid;
	}

	protected void setModelid(Integer modelid) {
		this.modelid = modelid;
	}

	protected void setDescid(Integer descid) {
		this.descid = descid;
	}
	
}
