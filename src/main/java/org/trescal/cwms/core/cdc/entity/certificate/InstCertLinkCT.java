/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.certificate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

import lombok.Setter;

@Setter
@Entity
@Table(name = "dbo_instcertlink_CT", schema = "cdc")
public class InstCertLinkCT extends EntityCT {


	private Integer id;

	private Integer certid;

	private Integer plantid;

	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	@Column(name = "certid")
	public Integer getCertid() {
		return certid;
	}

	@Column(name = "plantid")
	public Integer getPlantid() {
		return plantid;
	}
}
