package org.trescal.cwms.core.cdc.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Setter;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Setter
@JsonIgnoreProperties({"compositeId", "operationCode"})
public abstract class EntityCT {

	protected CdcCompositeId compositeId;
	protected Integer operationCode;
	
	@Column(name = "__$operation")
	public Integer getOperationCode() {
		return this.operationCode;
	}

	@EmbeddedId
	public CdcCompositeId getCompositeId() {
		return compositeId;
	}

}