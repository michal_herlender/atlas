package org.trescal.cwms.core.cdc.entity.job;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Setter;

@Setter
@Entity
@Table(name = "dbo_jobitem_CT", schema = "cdc")
public class JobItemCT extends EntityCT {
	
	private Integer jobitemid;

	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS")
	private ZonedDateTime datein;

	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate duedate;

	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS")
	private Date datecomplete;

	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSS")
	private OffsetDateTime clientreceiptdate;

	private Integer itemno;

	private Integer addrid;

	private Integer plantid;

	private Integer jobid;

	private Integer stateid;

	private String clientRef;

	private Integer servicetypeid; 

	private Integer groupid; 
	
	@Column(name = "jobitemid")
	public Integer getJobitemid() {
		return jobitemid;
	}

	@Column(name = "datein")
	public ZonedDateTime getDatein() {
		return datein;
	}

	@Column(name = "duedate")
	public LocalDate getDuedate() {
		return duedate;
	}

	@Column(name = "datecomplete")
	public Date getDatecomplete() {
		return datecomplete;
	}

	@Column(name = "clientreceiptdate")
	public OffsetDateTime getClientreceiptdate() {
		return clientreceiptdate;
	}

	@Column(name = "itemno")
	public Integer getItemno() {
		return itemno;
	}

		
	/**
	 * @return the addrid
	 */
	@Column(name = "addrid")
	public Integer getAddrid() {
		return addrid;
	}

	/**
	 * @return the servicetypeid
	 */
	@Column(name = "servicetypeid")
	public Integer getServicetypeid() {
		return servicetypeid;
	}

	@Column(name = "plantid")
	public Integer getPlantid() {
		return plantid;
	}

	@Column(name = "jobid")
	public Integer getJobid() {
		return jobid;
	}

	@Column(name = "stateid")
	public Integer getStateid() {
		return stateid;
	}

	@Column(name = "groupid")
	public Integer getGroupid() {
		return groupid;
	}

	@Column(name = "clientref")
	public String getClientRef() {
		return clientRef;
	}

}