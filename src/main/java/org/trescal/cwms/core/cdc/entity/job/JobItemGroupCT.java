package org.trescal.cwms.core.cdc.entity.job;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

import lombok.Setter;

@Setter
@Entity
@Table(name = "dbo_jobitemgroup_CT", schema = "cdc")
public class JobItemGroupCT extends  EntityCT  {
	
	private Integer id;
	private Boolean calibrationGroup;
	private Boolean deliveryGroup;
	private Integer jobid;
	
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	@Column(name="calibration")
	public Boolean getCalibrationGroup() {
		return calibrationGroup;
	}
	@Column(name="delivery")
	public Boolean getDeliveryGroup() {
		return deliveryGroup;
	}
	@Column(name="jobid")
	public Integer getJobid() {
		return jobid;
	}
}
