package org.trescal.cwms.core.cdc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import org.trescal.cwms.core.events.change.enums.ChangeEntity;

import lombok.Setter;

@Entity
@Table(name = "processed_changeentity")
@Setter
public class ProcessedChangeEntity {

	private ChangeEntity changeEntity;
	private byte[] lastProcessedLsn;

	@Id
	@Column(name = "changeentity")
	@Enumerated(EnumType.ORDINAL)
	public ChangeEntity getChangeEntity() {
		return changeEntity;
	}
	
	@Column(name = "lastprocessedlsn")
	public byte[] getLastProcessedLsn() {
		return this.lastProcessedLsn;
	}

	
}