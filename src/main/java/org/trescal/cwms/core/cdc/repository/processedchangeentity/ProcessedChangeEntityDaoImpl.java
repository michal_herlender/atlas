package org.trescal.cwms.core.cdc.repository.processedchangeentity;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.cdc.entity.ProcessedChangeEntity;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;

@Repository("ProcessedChangeEntityBaseDao")
public class ProcessedChangeEntityDaoImpl extends BaseDaoImpl<ProcessedChangeEntity,ChangeEntity>
		implements ProcessedChangeEntityDao {

	@Override
	protected Class<ProcessedChangeEntity> getEntity() {
		return ProcessedChangeEntity.class;
	}

	
}