package org.trescal.cwms.core.cdc.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.cdc.service.CdcDataTransformationService;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;

import lombok.extern.slf4j.Slf4j;

/**
 * Transformation is performed in service layer to have single transaction boundary
 */
@Component("CdcDataTransformationScheduledJob")
@Slf4j
public class CdcDataTransformationScheduledJob {

	@Autowired
	private CdcDataTransformationService cdtService;
	@Autowired
	private ScheduledTaskService scheduledTaskService;
	
	public void sync() {
		
		if (this.scheduledTaskService.scheduledTaskIsActive(this.getClass().getName(), Thread.currentThread().getStackTrace())) {
			if (log.isTraceEnabled()) log.trace("START");
			String message = "";
			boolean success = true;
			try {
				message = cdtService.performTransformation();
			}
			catch (Exception e) {
				success = false;
				message = e.getMessage();
				log.error("CdcDataTransformationScheduledJob exception", e);
			}
			
			this.scheduledTaskService.reportOnScheduledTaskRun(this.getClass().getName(), Thread.currentThread().getStackTrace(), success, message);
			
			if (log.isDebugEnabled()) log.debug("COMPLETED : success = "+success+" : "+message);
		}
		else {
			if (log.isTraceEnabled()) log.debug("Scheduled job inactive, skipping job");
		}
	}
	
}
