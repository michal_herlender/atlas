/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.defaultdata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

/**
 * @Author shazil.khan
 *
 */
@Entity
@Table(name = "dbo_systemdefaultapplication_CT", schema = "cdc")
public class SystemdDefaultApplicationCT extends EntityCT {
	

	private Integer id;
	

	private Integer defaultid;
	

	private String defaultvalue;
	

	private Integer companyid;
	

	private Integer subdivid;
	

	private Integer personid;
	

	private Integer orgid;

	/**
	 * @return the id
	 */
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	/**
	 * @return the defaultid
	 */
	@Column(name = "defaultid")
	public Integer getDefaultid() {
		return defaultid;
	}

	/**
	 * @return the defaultvalue
	 */
	@Column(name = "defaultvalue")
	public String getDefaultvalue() {
		return defaultvalue;
	}

	/**
	 * @return the companyid
	 */
	@Column(name = "coid")
	public Integer getCompanyid() {
		return companyid;
	}

	/**
	 * @return the subdivid
	 */
	@Column(name = "subdivid")
	public Integer getSubdivid() {
		return subdivid;
	}

	/**
	 * @return the personid
	 */
	@Column(name = "personid")
	public Integer getPersonid() {
		return personid;
	}

	/**
	 * @return the orgid
	 */
	@Column(name = "orgid")
	public Integer getOrgid() {
		return orgid;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param defaultid the defaultid to set
	 */
	public void setDefaultid(Integer defaultid) {
		this.defaultid = defaultid;
	}

	/**
	 * @param defaultvalue the defaultvalue to set
	 */
	public void setDefaultvalue(String defaultvalue) {
		this.defaultvalue = defaultvalue;
	}

	/**
	 * @param companyid the companyid to set
	 */
	public void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}

	/**
	 * @param subdivid the subdivid to set
	 */
	public void setSubdivid(Integer subdivid) {
		this.subdivid = subdivid;
	}

	/**
	 * @param personid the personid to set
	 */
	public void setPersonid(Integer personid) {
		this.personid = personid;
	}

	/**
	 * @param orgid the orgid to set
	 */
	public void setOrgid(Integer orgid) {
		this.orgid = orgid;
	}
	
	
	
	
	
	

}
