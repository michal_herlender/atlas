package org.trescal.cwms.core.cdc.entity.referential;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

@Entity
@Table(name = "dbo_country_CT", schema = "cdc")
public class CountryCT extends EntityCT {
	

	public Integer countryid;

	public String country;

	public String countrycode;
	
	@Column(name = "countryid")
	public Integer getCountryid() {
		return countryid;
	}
	
	@Column(name = "country")
	public String getCountry() {
		return country;
	}
	
	@Column(name = "countrycode")
	public String getCountrycode() {
		return countrycode;
	}

	protected void setCountryid(Integer countryid) {
		this.countryid = countryid;
	}

	protected void setCountry(String country) {
		this.country = country;
	}

	protected void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}

}
