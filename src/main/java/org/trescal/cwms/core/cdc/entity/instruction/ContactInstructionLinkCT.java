package org.trescal.cwms.core.cdc.entity.instruction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

@Entity
@Table(name = "dbo_contactinstructionlink_CT", schema = "cdc")
public class ContactInstructionLinkCT extends EntityCT {
	
 
	private Integer id;
 
	private Integer instructionid;

	private Integer personid;

	private Integer orgid;	
	
	/**
	 * @return the id
	 */
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	/**
	 * @return the instructionid
	 */
	@Column(name = "instructionid")
	public Integer getInstructionid() {
		return instructionid;
	}

	@JoinColumn(name = "coid")
	public Integer getPersonid() {
		return personid;
	}
	
	@JoinColumn(name = "orgid")
	public Integer getOrgid() {
		return orgid;
	}
	
	

	/**
	 * @param id the id to set
	 */
	protected void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param instructionid the instructionid to set
	 */
	protected void setInstructionid(Integer instructionid) {
		this.instructionid = instructionid;
	}

	protected void setPersonid(Integer personid) {
		this.personid = personid;
	}

	protected void setOrgid(Integer orgid) {
		this.orgid = orgid;
	}
}
