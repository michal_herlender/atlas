package org.trescal.cwms.core.cdc.entity.instruction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

@Entity
@Table(name = "dbo_jobinstructionlink_CT", schema = "cdc")
public class JobInstructionLinkCT extends EntityCT {
	

	private Integer id;

	private Integer jobid;

	private Integer instructionid;
	
	/**
	 * @return the id
	 */
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	/**
	 * @return the instructionid
	 */
	@Column(name = "instructionid")
	public Integer getInstructionid() {
		return instructionid;
	}

	@JoinColumn(name = "jobid")
	public Integer getJobid() {
		return jobid;
	}

	protected void setJobid(Integer jobid) {
		this.jobid = jobid;
	}

	/**
	 * @param id the id to set
	 */
	protected void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param instructionid the instructionid to set
	 */
	protected void setInstructionid(Integer instructionid) {
		this.instructionid = instructionid;
	}
	
	
}
