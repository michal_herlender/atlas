package org.trescal.cwms.core.cdc.entity.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

@Entity
@Table(name = "dbo_location_CT", schema = "cdc")
public class LocationCT extends EntityCT {
	

	private Integer locationid;

	private Integer addressid; 

	private Boolean active;

	private String location;
	
	@Column(name = "locationid")
	public Integer getLocationid() {
		return locationid;
	}
	
	@Column(name = "addressid")
	public Integer getAddressid() {
		return addressid;
	}
	
	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}
	
	@Column(name = "location")
	public String getLocation() {
		return location;
	}

	protected void setLocationid(Integer locationid) {
		this.locationid = locationid;
	}

	protected void setAddressid(Integer addressid) {
		this.addressid = addressid;
	}

	protected void setActive(Boolean active) {
		this.active = active;
	}

	protected void setLocation(String location) {
		this.location = location;
	}

}
