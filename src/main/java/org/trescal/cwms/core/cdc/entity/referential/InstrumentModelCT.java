package org.trescal.cwms.core.cdc.entity.referential;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;

@Entity
@Table(name = "dbo_instmodel_CT", schema = "cdc")
public class InstrumentModelCT extends EntityCT {
	

	private Integer modelid;

	private Integer descriptionid;

	private Integer mfrid;

	private String model;

	private ModelMfrType modelmfrtype;

	private Integer modeltypeid;

	private Boolean quarantined;

	private Integer tmlid;
	
	@Column(name = "modelid")
	public Integer getModelid() {
		return modelid;
	}

	@Column(name = "descriptionid")
	public Integer getDescriptionid() {
		return descriptionid;
	}

	@Column(name = "mfrid")
	public Integer getMfrid() {
		return mfrid;
	}

	@Column(name = "model")
	public String getModel() {
		return model;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "modelmfrtype")
	public ModelMfrType getModelmfrtype() {
		return modelmfrtype;
	}

	@Column(name = "modeltypeid")
	public Integer getModeltypeid() {
		return modeltypeid;
	}

	@Column(name = "quarantined")
	public Boolean getQuarantined() {
		return quarantined;
	}

	@Column(name = "tmlid")
	public Integer getTmlid() {
		return tmlid;
	}

	protected void setModelid(Integer modelid) {
		this.modelid = modelid;
	}

	protected void setDescriptionid(Integer descriptionid) {
		this.descriptionid = descriptionid;
	}

	protected void setMfrid(Integer mfrid) {
		this.mfrid = mfrid;
	}

	protected void setModel(String model) {
		this.model = model;
	}

	protected void setModelmfrtype(ModelMfrType modelmfrtype) {
		this.modelmfrtype = modelmfrtype;
	}

	protected void setModeltypeid(Integer modeltypeid) {
		this.modeltypeid = modeltypeid;
	}

	protected void setQuarantined(Boolean quarantined) {
		this.quarantined = quarantined;
	}

	protected void setTmlid(Integer tmlid) {
		this.tmlid = tmlid;
	}
	
}
