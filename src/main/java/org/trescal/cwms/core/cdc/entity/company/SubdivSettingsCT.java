package org.trescal.cwms.core.cdc.entity.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

import lombok.Setter;

@Setter
@Entity
@Table(name = "dbo_subdivsettingsforallocatedsubdiv_CT", schema = "cdc")
public class SubdivSettingsCT extends EntityCT {
	private Integer id;
	private Integer orgid;
	private Integer businesscontactid;
	private Integer subdivid;
	
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	@Column(name="orgid")
	public Integer getOrgid() {
		return orgid;
	}
	@Column(name="businesscontactid")
	public Integer getBusinesscontactid() {
		return businesscontactid;
	}
	@Column(name="subdivid")
	public Integer getSubdivid() {
		return subdivid;
	}
}