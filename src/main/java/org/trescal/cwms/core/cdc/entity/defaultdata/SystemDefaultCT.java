/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.defaultdata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

/**
 * @Author shazil.khan
 *
 */
@Entity
@Table(name = "dbo_systemdefault_CT", schema = "cdc")
public class SystemDefaultCT extends EntityCT {
	

	private Integer id;
	

	private String defaultdatatype;
	

	private String defaultdescription;
	

	private String defaultname;
	

	private String defaultvalue;
	

	private Boolean groupwide;

	/**
	 * @return the id
	 */
	@Column(name = "defaultid")
	public Integer getId() {
		return id;
	}

	/**
	 * @return the defaultdatatype
	 */
	@Column(name = "defaultdatatype")
	public String getDefaultdatatype() {
		return defaultdatatype;
	}

	/**
	 * @return the defaultdescription
	 */
	@Column(name = "defaultdescription")
	public String getDefaultdescription() {
		return defaultdescription;
	}

	/**
	 * @return the defaultname
	 */
	@Column(name = "defaultname")
	public String getDefaultname() {
		return defaultname;
	}

	/**
	 * @return the defaultvalue
	 */
	@Column(name = "defaultvalue")
	public String getDefaultvalue() {
		return defaultvalue;
	}

	/**
	 * @return the groupwide
	 */
	@Column(name = "groupwide")
	public Boolean getGroupwide() {
		return groupwide;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param defaultdatatype the defaultdatatype to set
	 */
	public void setDefaultdatatype(String defaultdatatype) {
		this.defaultdatatype = defaultdatatype;
	}

	/**
	 * @param defaultdescription the defaultdescription to set
	 */
	public void setDefaultdescription(String defaultdescription) {
		this.defaultdescription = defaultdescription;
	}

	/**
	 * @param defaultname the defaultname to set
	 */
	public void setDefaultname(String defaultname) {
		this.defaultname = defaultname;
	}

	/**
	 * @param defaultvalue the defaultvalue to set
	 */
	public void setDefaultvalue(String defaultvalue) {
		this.defaultvalue = defaultvalue;
	}

	/**
	 * @param groupwide the groupwide to set
	 */
	public void setGroupwide(Boolean groupwide) {
		this.groupwide = groupwide;
	}
	
	
	
	
	

}
