package org.trescal.cwms.core.cdc.repository.processedchangeentity;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.cdc.entity.ProcessedChangeEntity;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;

public interface ProcessedChangeEntityService extends BaseService<ProcessedChangeEntity, ChangeEntity>{
	
}