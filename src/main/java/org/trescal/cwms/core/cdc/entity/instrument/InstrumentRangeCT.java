/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.instrument;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

import lombok.Setter;

@Setter
@Entity
@Table(name = "dbo_instrumentrange_CT", schema = "cdc")
public class InstrumentRangeCT extends EntityCT {
	

	private Integer id;

	private Double end;

	private Double start;

	private Integer uom;

	private Integer maxUom;

	private Integer plantid;

	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	@Column(name = "endd")
	public Double getEnd() {
		return end;
	}

	@Column(name = "start")
	public Double getStart() {
		return start;
	}

	@Column(name = "uomid")
	public Integer getUom() {
		return uom;
	}

	@Column(name = "maxuomid")
	public Integer getMaxUom() {
		return maxUom;
	}

	@Column(name = "plantid")
	public Integer getPlantid() {
		return plantid;
	}
}
