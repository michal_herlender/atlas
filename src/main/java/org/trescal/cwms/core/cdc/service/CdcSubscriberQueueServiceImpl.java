package org.trescal.cwms.core.cdc.service;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;
import org.trescal.cwms.core.events.change.queue.ChangeQueue;
import org.trescal.cwms.core.events.service.ChangeQueueService;
import org.trescal.cwms.core.events.service.EventSubscriberService;
import org.trescal.cwms.core.events.service.SubscriberQueueService;
import org.trescal.cwms.core.events.subscriber.entity.EventSubscriber;
import org.trescal.cwms.core.events.subscriber.entity.SubscriberQueue;


import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CdcSubscriberQueueServiceImpl implements CdcSubscriberQueueService {
	
	@Autowired
	private ChangeQueueService changeQueueService;
	@Autowired
	private EventSubscriberService subscriberService;
	@Autowired
	private SubscriberQueueService subscriberQueueService;
	
	// TODO consider batched inserts, max size to process, mutliple transactions 
	public static int MAX_SIZE = 1000; 

	@Override
	public String processChangeQueue() {
		long startTime = System.currentTimeMillis();
		
		MultiValuedMap<ChangeEntity, EventSubscriber> subscribersMap = createSubscribersMap();
		
		List<ChangeQueue> unprocessedQueue = this.changeQueueService.findNNotProcessedChangeQueue(MAX_SIZE);
		
		int countNew = 0;
		for (ChangeQueue changeEvent : unprocessedQueue) {
			Collection<EventSubscriber> subscribers = subscribersMap.get(changeEvent.getEntityType());
			for (EventSubscriber subscriber : subscribers) {
				SubscriberQueue subscriberEvent = new SubscriberQueue();
				subscriberEvent.setProcessed(false);
				subscriberEvent.setQueueEvent(changeEvent);
				subscriberEvent.setSubscriber(subscriber);
				
				this.subscriberQueueService.save(subscriberEvent);
				
				countNew++;
			}
			// Mark as processed
			changeEvent.setProcessed(true);
		}
		
		long finishTime = System.currentTimeMillis();
		
		StringBuffer message = new StringBuffer();
		message.append("Processed ");
		message.append(unprocessedQueue.size());
		message.append(" ChangeQueue records, added ");
		message.append(countNew);
		message.append(" SubscriberQueue records in ");
		message.append((finishTime-startTime));
		message.append(" ms");
		return message.toString();
	}
	
	private MultiValuedMap<ChangeEntity, EventSubscriber> createSubscribersMap() {
		List<EventSubscriber> subscribers = this.subscriberService.getAllSubscribersEager();
		if (log.isDebugEnabled()) log.debug("subscribers.size() : "+subscribers.size());
		MultiValuedMap<ChangeEntity, EventSubscriber> subscribersMap = new HashSetValuedHashMap<>();
		subscribers.stream()
			.forEach(subscriber -> subscriber.getEventSubscriptions().stream().
					forEach(subscription -> subscribersMap.put(subscription.getEntityType(), subscriber)));
		
		if (log.isDebugEnabled()) log.debug("subscribersMap.size() (distinct subscriptions): "+subscribersMap.size());
		return subscribersMap; 
		
		
	}
}
