/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.job;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

/**
 * @Author shazil.khan
 *
 */
@Entity
@Table(name = "dbo_upcomingworkjoblink_CT", schema = "cdc")
public class UpcomingWorkJobLinkCT extends EntityCT {
	

	private Integer id;
	
	private Boolean active;
	
	private Integer jobid;
	
	private Integer upcomingworkid;

	/**
	 * @return the id
	 */
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	/**
	 * @return the active
	 */
	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}

	/**
	 * @return the jobid
	 */
	@Column(name = "jobid")
	public Integer getJobid() {
		return jobid;
	}

	/**
	 * @return the upcomingworkid
	 */
	@Column(name = "upcomingworkid")
	public Integer getUpcomingworkid() {
		return upcomingworkid;
	}

	/**
	 * @param id the id to set
	 */
	protected void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param active the active to set
	 */
	protected void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * @param jobid the jobid to set
	 */
	protected void setJobid(Integer jobid) {
		this.jobid = jobid;
	}

	/**
	 * @param upcomingworkid the upcomingworkid to set
	 */
	protected void setUpcomingworkid(Integer upcomingworkid) {
		this.upcomingworkid = upcomingworkid;
	}
	
	
	
	
	
	

}
