package org.trescal.cwms.core.cdc.repository.base;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AbstractDaoImpl;
import org.trescal.cwms.core.cdc.entity.EntityCT;

import lombok.val;

@Repository("BaseCDCDao")
public class BaseCDCDaoImpl<Entity> extends AbstractDaoImpl
		implements BaseCDCDao<Entity> {

	public static final int CDC_OPERATION_DELETE = 1; 
	public static final int CDC_OPERATION_INSERT = 2; 
	public static final int CDC_OPERATION_UPDATE_BEFORE_CHANGE = 3; 
	public static final int CDC_OPERATION_UPDATE_AFTER_CHANGE = 4; 
/*	
	@Override
	public List<byte[]> findLsnBetweenTwoDates(Date fromDate, Date toDate, String tableName) {
		Session session = getEntityManager().unwrap(Session.class);
		ResultSet rs = null;
		List<byte[]> result = new ArrayList<byte[]>();
	      CallableStatement callableStatement =  session.doReturningWork(new ReturningWork<CallableStatement>() {
	    
				@Override
				public CallableStatement execute(Connection connection) throws SQLException {
					 CallableStatement function = connection.prepareCall(
		                        "{ ? = call dbo.getAllCdcChangeBetweenTwoDates(?, ?, ?) }");
		                function.registerOutParameter(1, Types.ARRAY);
		                function.setTimestamp(2, new Timestamp(fromDate.getTime()));
		                function.setTimestamp(3, new Timestamp(toDate.getTime()));
		                function.setString(4, tableName);

		                function.execute();
		                return function;
				}
	        });

	        try {
				rs = callableStatement.executeQuery();
				while(rs.next()){
					int columnIndex = rs.findColumn("__$start_lsn");
					System.out.println("data --- "+rs.getBytes(columnIndex));
					result.add(rs.getBytes(columnIndex));
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        return result;
	}*/

	@Override
	public List<? extends Entity> findByStartLsnIds(List<byte[]> startLsns, Class<? extends Entity> entityClass) {
		return this.getResultList(cb -> {
			val cq = cb.createQuery(entityClass);
			val root = cq.from(entityClass);
			cq.where(cb.and(
					cb.notEqual(root.get("operationCode"), 3),
					root.get("startLsn").in(startLsns)));
			cq.distinct(true);
			cq.orderBy(cb.asc(root.get("startLsn")));
			return cq;
		});
	}

	@Override
	public List<byte[]> findLsnFromLastProcessedLsn(byte[] lastProcessedLsn, String tableName) {
		Session session = getEntityManager().unwrap(Session.class);
		ResultSet rs = null;
		List<byte[]> result = new ArrayList<byte[]>();
	      CallableStatement callableStatement =  session.doReturningWork(new ReturningWork<CallableStatement>() {
	    
				@Override
				public CallableStatement execute(Connection connection) throws SQLException {
					 CallableStatement function = connection.prepareCall(
		                        "{ ? = call dbo.getAllCdcChangeFromLastProcessedLsn(?, ?) }");
		                function.registerOutParameter(1, Types.ARRAY);
		                function.setBytes(2, lastProcessedLsn);
		                function.setString(3, tableName);

		                function.execute();
		                return function;
				}
	        });

	        try {
	        	boolean resultExist = callableStatement.execute();
	        	if(resultExist) {
		        	rs = callableStatement.executeQuery();
					while(rs.next()){
						int columnIndex = rs.findColumn("__$start_lsn");
						result.add(rs.getBytes(columnIndex));
					}
	        	}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        return result;
	}

	/*
	 * NativeQuery being used, in order to pass startLsn as a parameter (binary 10 byte[] LSN) to query 
	 */
	@Override
	public List<? extends EntityCT> findGreaterThanLsn(byte[] startLsn, Class<? extends EntityCT> entityClass, String tableName) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT *");
		sql.append(" FROM [cdc].[");
		sql.append(tableName);
		sql.append("_CT] WHERE");
		sql.append(" __$operation <> :excludeOperation");
		if (startLsn != null)
		sql.append(" AND __$start_lsn > :startLsn");

		Session session = getEntityManager().unwrap(Session.class);
		NativeQuery<? extends EntityCT> query = session.createNativeQuery(sql.toString(), entityClass);
		query.setParameter("excludeOperation", CDC_OPERATION_UPDATE_BEFORE_CHANGE);
		if (startLsn != null)
			query.setParameter("startLsn", startLsn);
			
		List<? extends EntityCT> result = query.getResultList();
		return result;
	}

	
	/**
	 * Returns the lowest log sequence number (LSN) for the specified change table
	 * If no change table specified (null tableName), returns the lowest LSN for the system 
	 * 
	 */
	@Override
	public byte[] findMinLsn(String tableName) {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT sys.fn_cdc_get_min_lsn('");
		if (tableName != null) {
			sql.append(tableName);
		}
		sql.append("')");
		
		Query resultsQuery = super.getEntityManager().createNativeQuery(sql.toString());
		byte[] result = (byte[]) resultsQuery.getSingleResult();
		return result;
	}

	/**
	 * Returns the highest log sequence number (LSN) for the system
	 * 
	 */
	@Override
	public byte[] findMaxLsn() {
		String sql = "SELECT sys.fn_cdc_get_max_lsn()";
		
		Query resultsQuery = super.getEntityManager().createNativeQuery(sql.toString());
		byte[] result = (byte[]) resultsQuery.getSingleResult();
		return result;
	}
	
	
}