package org.trescal.cwms.core.cdc.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.cdc.service.CdcSubscriberQueueService;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;

import lombok.extern.slf4j.Slf4j;

@Component("CdcSubscriberQueueScheduledJob")
@Slf4j
public class CdcSubscriberQueueScheduledJob {
	
	@Autowired
	private CdcSubscriberQueueService cdcSubscriberQueueService;
	@Autowired
	private ScheduledTaskService scheduledTaskService;

	public void processChangeQueue() {
		if (this.scheduledTaskService.scheduledTaskIsActive(this.getClass().getName(), Thread.currentThread().getStackTrace())) {
			if (log.isTraceEnabled()) log.trace("START");
			String message = "";
			boolean success = true;
			try {
				message = this.cdcSubscriberQueueService.processChangeQueue();
			}
			catch (Exception e) {
				success = false;
				message = e.getMessage();
				log.error("CdcSubscriberQueueScheduledJob exception", e);
			}
			
			this.scheduledTaskService.reportOnScheduledTaskRun(this.getClass().getName(), Thread.currentThread().getStackTrace(), success, message);
			
			if (log.isDebugEnabled()) log.debug("COMPLETED : success = "+success+" : "+message);
		}
		else {
			if (log.isTraceEnabled()) log.debug("Scheduled job inactive, skipping job");
		}
	}
	
	
}
