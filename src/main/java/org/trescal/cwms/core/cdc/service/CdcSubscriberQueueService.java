package org.trescal.cwms.core.cdc.service;

public interface CdcSubscriberQueueService {
	String processChangeQueue();
}
