/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.certificate;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.instrument.CalibrationVerificationStatus;
import org.trescal.cwms.core.jobs.certificate.entity.CertificateType;
import org.trescal.cwms.core.jobs.certificate.entity.certstatus.CertStatusEnum;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Setter;

@Entity
@Setter
@Table(name = "dbo_certificate_CT", schema = "cdc")
public class CertificateCT extends EntityCT {
	

	private Integer certid;

	@JsonFormat(pattern="yyyy-MM-dd")
	private Date caldate;

	@JsonFormat(pattern="yyyy-MM-dd")
	private Date certdate;

	private String certno;

	private Integer duration;

	private String thirdcertno;

	private CertificateType type;

	private Integer servicetypeid;

	private Integer supplementaryforid;

	private Integer thirddiv;

	private IntervalUnit intervalunit;

	private CalibrationVerificationStatus calibrationverificationstatus;

	private Boolean optimization;

	private Boolean repair;

	private Boolean restriction;

	private Boolean adjustment;

	private CertStatusEnum certstatus;
	
	@Column(name = "certid")
	public Integer getCertid() {
		return certid;
	}

	@Column(name = "caldate", columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCaldate() {
		return caldate;
	}
	
	@Column(name = "certdate", columnDefinition = "datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCertdate() {
		return certdate;
	}
	
	@Column(name = "certno")
	public String getCertno() {
		return certno;
	}
	
	@Column(name = "duration")
	public Integer getDuration() {
		return duration;
	}

	@Column(name = "thirdcertno")
	public String getThirdcertno() {
		return thirdcertno;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "type")
	public CertificateType getType() {
		return type;
	}

	@Column(name = "servicetypeid")
	public Integer getServicetypeid() {
		return servicetypeid;
	}

	@Column(name = "supplementaryforid")
	public Integer getSupplementaryforid() {
		return supplementaryforid;
	}

	@Column(name = "thirddiv")
	public Integer getThirddiv() {
		return thirddiv;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "intervalunit")
	public IntervalUnit getIntervalunit() {
		return intervalunit;
	}

	@Column(name = "calibrationverificationstatus")
	@Enumerated(EnumType.STRING)
	public CalibrationVerificationStatus getCalibrationverificationstatus() {
		return calibrationverificationstatus;
	}

	@Column(name = "optimization")
	public Boolean getOptimization() {
		return optimization;
	}

	@Column(name = "repair")
	public Boolean getRepair() {
		return repair;
	}

	@Column(name = "restriction")
	public Boolean getRestriction() {
		return restriction;
	}

	@Column(name = "adjustment")
	public Boolean getAdjustment() {
		return adjustment;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "certstatus")
	public CertStatusEnum getCertstatus() {
		return certstatus;
	}

}
