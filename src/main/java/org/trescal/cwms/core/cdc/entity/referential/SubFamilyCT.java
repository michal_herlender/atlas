package org.trescal.cwms.core.cdc.entity.referential;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

@Entity
@Table(name = "dbo_description_CT", schema = "cdc")
public class SubFamilyCT extends EntityCT {
	

	private Integer descriptionid;

	private String description;

	private Integer tmlid;

	private Integer familyid;

	private Boolean active;
	
	@Column(name = "descriptionid")
	public Integer getDescriptionid() {
		return descriptionid;
	}
	
	@Column(name = "description")
	public String getDescription() {
		return description;
	}
	
	@Column(name = "tmlid")
	public Integer getTmlid() {
		return tmlid;
	}
	
	@Column(name = "familyid")
	public Integer getFamilyid() {
		return familyid;
	}
	
	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}

	protected void setDescriptionid(Integer descriptionid) {
		this.descriptionid = descriptionid;
	}

	protected void setDescription(String description) {
		this.description = description;
	}

	protected void setTmlid(Integer tmlid) {
		this.tmlid = tmlid;
	}

	protected void setFamilyid(Integer familyid) {
		this.familyid = familyid;
	}

	protected void setActive(Boolean active) {
		this.active = active;
	}
	
}
