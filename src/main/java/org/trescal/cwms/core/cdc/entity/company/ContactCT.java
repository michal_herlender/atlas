package org.trescal.cwms.core.cdc.entity.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

@Entity
@Table(name = "dbo_contact_CT", schema = "cdc")
public class ContactCT extends EntityCT {
	

	private Integer personid;

	private Boolean active;

	private String firstname;

	private String lastname;

	private String email;

	private String telephone;

	private String fax;

	private String mobile;

	private Integer subdivid;

	private Integer defaddress;
	
	@Column(name = "personid")
	public Integer getPersonid() {
		return personid;
	}
	
	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}
	
	@Column(name = "firstname")
	public String getFirstname() {
		return firstname;
	}
	
	@Column(name = "lastname")
	public String getLastname() {
		return lastname;
	}
	
	@Column(name = "email")
	public String getEmail() {
		return email;
	}
	
	@Column(name = "telephone")
	public String getTelephone() {
		return telephone;
	}
	
	@Column(name = "fax")
	public String getFax() {
		return fax;
	}
	
	@Column(name = "mobile")
	public String getMobile() {
		return mobile;
	}
	
	@Column(name = "subdivid")
	public Integer getSubdivid() {
		return subdivid;
	}
	
	@Column(name = "defaddress")
	public Integer getDefaddress() {
		return defaddress;
	}

	protected void setPersonid(Integer personid) {
		this.personid = personid;
	}

	protected void setActive(Boolean active) {
		this.active = active;
	}

	protected void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	protected void setLastname(String lastname) {
		this.lastname = lastname;
	}

	protected void setEmail(String email) {
		this.email = email;
	}

	protected void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	protected void setFax(String fax) {
		this.fax = fax;
	}

	protected void setMobile(String mobile) {
		this.mobile = mobile;
	}

	protected void setSubdivid(Integer subdivid) {
		this.subdivid = subdivid;
	}

	protected void setDefaddress(Integer defaddress) {
		this.defaddress = defaddress;
	}

}