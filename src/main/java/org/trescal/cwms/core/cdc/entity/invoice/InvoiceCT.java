package org.trescal.cwms.core.cdc.entity.invoice;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Setter;

@Setter
@Entity
@Table(name = "dbo_invoice_CT", schema = "cdc")
public class InvoiceCT extends EntityCT {
	private Integer id;
	private Boolean issued;
	private LocalDate issuedate;
	private BigDecimal finalcost;
	private BigDecimal totalcost;
	private BigDecimal vatvalue;
	private String invno;
	private Integer addressid;
	private Integer coid;
	private Integer orgid;
	private LocalDate duedate;
	
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	@Column(name="issued")
	public Boolean getIssued() {
		return issued;
	}
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(name="issuedate")
	public LocalDate getIssuedate() {
		return issuedate;
	}
	@Column(name="finalcost")
	public BigDecimal getFinalcost() {
		return finalcost;
	}
	@Column(name="totalcost")
	public BigDecimal getTotalcost() {
		return totalcost;
	}
	@Column(name="vatvalue")
	public BigDecimal getVatvalue() {
		return vatvalue;
	}
	@Column(name="invno")
	public String getInvno() {
		return invno;
	}
	@Column(name="addressid")
	public Integer getAddressid() {
		return addressid;
	}
	@Column(name="coid")
	public Integer getCoid() {
		return coid;
	}
	@Column(name="orgid")
	public Integer getOrgid() {
		return orgid;
	}
	@JsonFormat(pattern="yyyy-MM-dd")
	@Column(name="duedate")
	public LocalDate getDuedate() {
		return duedate;
	}
}
