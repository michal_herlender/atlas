package org.trescal.cwms.core.cdc.entity.referential;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

@Entity
@Table(name = "dbo_mfr_CT", schema = "cdc")
public class BrandCT extends EntityCT {


	private Integer mfrid;

	private Boolean active;

	private Boolean genericmfr;

	private String name;

	private Integer tmlid;

	@Column(name = "mfrid")
	public Integer getMfrid() {
		return mfrid;
	}

	@Column(name = "active")
	public Boolean getActive() {
		return this.active;
	}

	@Column(name = "genericmfr")
	public Boolean getGenericmfr() {
		return this.genericmfr;
	}

	@Column(name = "name")
	public String getName() {
		return this.name;
	}

	@Column(name = "tmlid")
	public Integer getTmlid() {
		return this.tmlid;
	}

	protected void setActive(Boolean active) {
		this.active = active;
	}

	protected void setMfrid(Integer mfrid) {
		this.mfrid = mfrid;
	}

	protected void setGenericmfr(Boolean genericmfr) {
		this.genericmfr = genericmfr;
	}

	protected void setName(String name) {
		this.name = name;
	}

	protected void setTmlid(Integer tmlid) {
		this.tmlid = tmlid;
	}
}