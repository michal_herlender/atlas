/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.referential;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

import lombok.Setter;

@Setter
@Entity
@Table(name = "dbo_uom_CT", schema = "cdc")
public class UomCT extends EntityCT {
	

	private Integer id;

	private String name;

	private String symbol;

	private Integer tmlid;

	private Boolean active;

	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	@Column(name = "symbol")
	public String getSymbol() {
		return symbol;
	}

	@Column(name = "tmlid")
	public Integer getTmlid() {
		return tmlid;
	}

	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}

}
