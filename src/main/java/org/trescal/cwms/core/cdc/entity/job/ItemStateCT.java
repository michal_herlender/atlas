package org.trescal.cwms.core.cdc.entity.job;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

@Entity
@Table(name = "dbo_itemstate_CT", schema = "cdc")
public class ItemStateCT extends EntityCT {
	

	private String type;

	private Integer stateid;

	private Boolean active;

	private String description;

	private Boolean retired;
	
	@Column(name = "type")
	public String getType() {
		return type;
	}
	
	@Column(name = "stateid")
	public Integer getStateid() {
		return stateid;
	}
	
	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}
	
	@Column(name = "description")
	public String getDescription() {
		return description;
	}
	
	@Column(name = "retired")
	public Boolean getRetired() {
		return retired;
	}

	protected void setType(String type) {
		this.type = type;
	}

	protected void setStateid(Integer stateid) {
		this.stateid = stateid;
	}

	protected void setActive(Boolean active) {
		this.active = active;
	}

	protected void setDescription(String description) {
		this.description = description;
	}

	protected void setRetired(Boolean retired) {
		this.retired = retired;
	}
	
}
