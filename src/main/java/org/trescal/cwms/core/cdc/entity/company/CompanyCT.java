package org.trescal.cwms.core.cdc.entity.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;

@Entity
@Table(name = "dbo_company_CT", schema = "cdc")
public class CompanyCT extends EntityCT {

	private Integer coid;

	private String coname;

	private CompanyRole companyRole;

	private Integer countryid;

	private String companyCode;

	private String legalIdentifier;

	private String fiscalIdentifier;
	
	@Column(name = "coid")
	public Integer getCoid() {
		return coid;
	}
	
	@Column(name = "coname")
	public String getConame() {
		return coname;
	}
	
	@Column(name = "corole")
	public CompanyRole getCompanyRole() {
		return companyRole;
	}
	
	@Column(name = "countryid")
	public Integer getCountryid() {
		return countryid;
	}
	
	@Column(name = "companycode")
	public String getCompanyCode() {
		return companyCode;
	}
	
	@Column(name = "legalidentifier")
	public String getLegalIdentifier() {
		return legalIdentifier;
	}
	
	@Column(name = "fiscalidentifier")
	public String getFiscalIdentifier() {
		return fiscalIdentifier;
	}

	protected void setCoid(Integer coid) {
		this.coid = coid;
	}

	protected void setConame(String coname) {
		this.coname = coname;
	}

	protected void setCompanyRole(CompanyRole companyRole) {
		this.companyRole = companyRole;
	}

	protected void setCountryid(Integer countryid) {
		this.countryid = countryid;
	}

	protected void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	protected void setLegalIdentifier(String legalIdentifier) {
		this.legalIdentifier = legalIdentifier;
	}

	protected void setFiscalIdentifier(String fiscalIdentifier) {
		this.fiscalIdentifier = fiscalIdentifier;
	}

}
