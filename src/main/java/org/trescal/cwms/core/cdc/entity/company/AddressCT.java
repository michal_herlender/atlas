package org.trescal.cwms.core.cdc.entity.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

@Entity
@Table(name = "dbo_address_CT", schema = "cdc")
public class AddressCT extends EntityCT {


	private Integer addressid;

	private Boolean active;

	private String addr1;

	private String addr2;

	private String addr3;

	private String county;

	private String postcode;

	private String town;

	private Integer countryid;

	private Integer subdivid;

	@Column(name = "addrid")
	public Integer getAddressid() {
		return addressid;
	}

	@Column(name = "active")
	public Boolean getActive() {
		return this.active;
	}

	@Column(name = "addr1")
	public String getAddr1() {
		return this.addr1;
	}

	@Column(name = "addr2")
	public String getAddr2() {
		return this.addr2;
	}

	@Column(name = "addr3")
	public String getAddr3() {
		return this.addr3;
	}
	
	@Column(name = "countryid")
	public Integer getCountryid() {
		return countryid;
	}

	@Column(name = "county")
	public String getCounty() {
		return this.county;
	}

	@Column(name = "postcode")
	public String getPostcode() {
		return this.postcode;
	}

	@Column(name = "subdivid")
	public Integer getSubdivid() {
		return this.subdivid;
	}

	@Column(name = "town")
	public String getTown() {
		return this.town;
	}

	protected void setAddressid(Integer addressid) {
		this.addressid = addressid;
	}

	protected void setActive(Boolean active) {
		this.active = active;
	}

	protected void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

	protected void setAddr2(String addr2) {
		this.addr2 = addr2;
	}

	protected void setAddr3(String addr3) {
		this.addr3 = addr3;
	}

	protected void setCountryid(Integer countryId) {
		this.countryid = countryId;
	}

	protected void setCounty(String county) {
		this.county = county;
	}
	
	protected void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	protected void setSubdivid(Integer subdivId) {
		this.subdivid = subdivId;
	}

	protected void setTown(String town) {
		this.town = town;
	}

/*	protected void setOperationCode(Integer operationCode) {
		this.operationCode = operationCode;
	}
*/
}