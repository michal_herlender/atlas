package org.trescal.cwms.core.cdc.entity.instrument;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;

import lombok.Setter;

@Setter
@Entity
@Table(name = "dbo_instrumentusagetype_CT", schema = "cdc")
public class InstrumentUsageTypeCT extends EntityCT {
	
	private Integer typeid;
	private Boolean defaultType;
	private String fullDescription;
	private String name;
	private Boolean recall; 
	private InstrumentStatus instrumentStatus;
	
	@Column(name = "typeid")
	public Integer getTypeid() {
		return typeid;
	}
	
	@Column(name = "defaultType")
	public Boolean getDefaultType() {
		return defaultType;
	}
	
	@Column(name = "fullDescription")
	public String getFullDescription() {
		return fullDescription;
	}
	
	@Column(name = "name")
	public String getName() {
		return name;
	}
	
	@Column(name = "recall")
	public Boolean getRecall() {
		return recall;
	}
	
	@Column(name = "instrumentStatus")
	public InstrumentStatus getInstrumentStatus() {
		return instrumentStatus;
	}
	
}
