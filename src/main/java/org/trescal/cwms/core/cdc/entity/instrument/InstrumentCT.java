package org.trescal.cwms.core.cdc.entity.instrument;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.instrument.entity.instrumentstatus.InstrumentStatus;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Setter;

@Entity @Setter
@Table(name = "dbo_instrument_CT", schema = "cdc")
public class InstrumentCT extends EntityCT {

	private Integer plantid;

	private Integer calInterval;

	private IntervalUnit calIntervalUnit;

	private Boolean calibrationStandard;

	@JsonFormat(pattern="YYYY-MM-dd")
	private LocalDate recallDate;

	private String plantno;

	private String serialno;

	private InstrumentStatus status;

	private String customerDescription;

	private String customerSpecification;

	private String freeTextLocation;

	private String clientBarcode;

	private String formerBarcode;

	private String modelname;

	private Integer addressid;

	private Integer coid;

	private Integer personid;

	private Integer locationid;

	private Integer mfrid;

	private Integer modelid;

	private Integer defaultservicetypeid;
	
	private Integer usageid;
	
	private Boolean customermanaged;
	
	@Column(name = "plantid")
	public Integer getPlantid() {
		return plantid;
	}
	
	@Column(name = "calfrequency")
	public Integer getCalInterval() {
		return calInterval;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "calintervalunit")
	public IntervalUnit getCalIntervalUnit() {
		return calIntervalUnit;
	}
	
	@Column(name = "calibrationStandard")
	public Boolean getCalibrationStandard() {
		return calibrationStandard;
	}
	
	@Column(name = "recalldate", columnDefinition = "date")
	public LocalDate getRecallDate() {
		return recallDate;
	}
	
	@Column(name = "plantno")
	public String getPlantno() {
		return plantno;
	}
	
	@Column(name = "serialno")
	public String getSerialno() {
		return serialno;
	}
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "statusid")
	public InstrumentStatus getStatus() {
		return status;
	}
	
	@Column(name = "customerdescription")
	public String getCustomerDescription() {
		return customerDescription;
	}
	
	@Column(name = "formerbarcode")
	public String getFormerBarcode() {
		return formerBarcode;
	}
	
	@Column(name = "modelname")
	public String getModelname() {
		return modelname;
	}
	
	@Column(name = "addressid")
	public Integer getAddressid() {
		return addressid;
	}
	
	@Column(name = "coid")
	public Integer getCoid() {
		return coid;
	}
	
	@Column(name = "personid")
	public Integer getPersonid() {
		return personid;
	}
	
	@Column(name = "locationid")
	public Integer getLocationid() {
		return locationid;
	}
	
	@Column(name = "mfrid")
	public Integer getMfrid() {
		return mfrid;
	}
	
	@Column(name = "modelid")
	public Integer getModelid() {
		return modelid;
	}
	
	@Column(name = "defaultservicetype")
	public Integer getDefaultservicetypeid() {
		return defaultservicetypeid;
	}
	

	/**
	 * @return the clientBarcode
	 */
	@Column(name = "clientbarcode")
	public String getClientBarcode() {
		return clientBarcode;
	}

	/**
	 * @return the freeTextLocation
	 */
	@Column(name = "freeTextLocation")
	public String getFreeTextLocation() {
		return freeTextLocation;
	}

	/**
	 * @return the customerSpecification
	 */
	@Column(name = "customerspecification")
	public String getCustomerSpecification() {
		return customerSpecification;
	}
	
	@Column(name = "usageid")
	public Integer getUsageid() {
		return usageid;
	}
	
	@Column(name = "customermanaged")
	public Boolean getCustomermanaged() {
		return customermanaged;
	}
}
