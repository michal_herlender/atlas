package org.trescal.cwms.core.cdc.entity.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.company.entity.companystatus.CompanyStatus;

@Entity
@Table(name = "dbo_companysettingsforallocatedcompany_CT", schema = "cdc")
public class CompanySettingsCT extends EntityCT {
	

	private Integer id;

	private Integer orgid;

	private Integer companyid;

	private Boolean active;

	private Boolean onstop;

	private CompanyStatus status;
	
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	
	@Column(name = "orgid")
	public Integer getOrgid() {
		return orgid;
	}
	
	@Column(name = "companyid")
	public Integer getCompanyid() {
		return companyid;
	}
	
	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}
	
	@Column(name = "onstop")
	public Boolean getOnstop() {
		return onstop;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	public CompanyStatus getStatus() {
		return status;
	}

	protected void setId(Integer id) {
		this.id = id;
	}

	protected void setOrgid(Integer orgid) {
		this.orgid = orgid;
	}

	protected void setCompanyid(Integer companyid) {
		this.companyid = companyid;
	}

	protected void setActive(Boolean active) {
		this.active = active;
	}

	protected void setOnstop(Boolean onstop) {
		this.onstop = onstop;
	}

	protected void setStatus(CompanyStatus status) {
		this.status = status;
	}
}
