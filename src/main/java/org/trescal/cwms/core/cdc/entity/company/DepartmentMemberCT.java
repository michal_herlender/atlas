package org.trescal.cwms.core.cdc.entity.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

@Entity
@Table(name = "dbo_departmentmember_CT", schema = "cdc")
public class DepartmentMemberCT extends EntityCT	{
	

	private Integer id;

	private Integer personid;

	private Integer deptid;
	
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	
	@Column(name = "personid")
	public Integer getPersonid() {
		return personid;
	}
	
	@Column(name = "deptid")
	public Integer getDeptid() {
		return deptid;
	}

	protected void setId(Integer id) {
		this.id = id;
	}

	protected void setPersonid(Integer personid) {
		this.personid = personid;
	}

	protected void setDeptid(Integer deptid) {
		this.deptid = deptid;
	}

}
