/**
 * 
 */
package org.trescal.cwms.core.cdc.entity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
/**
 * @Author shazil.khan
 *
 */
//@Entity
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class EntityCT_Json {

	public String toJson() {
		
		GsonBuilder builder = new GsonBuilder();
		builder = builder.excludeFieldsWithoutExposeAnnotation();
		builder = builder.serializeNulls();
	    Gson gson = builder.create();
	    return gson.toJson(this);
	}
}