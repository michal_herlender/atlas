/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.job;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Setter;

@Setter
@Entity
@Table(name = "dbo_upcomingwork_CT", schema = "cdc")
public class UpcomingWorkCT extends EntityCT {
	

	private Integer id;
	

	private Boolean active;
	

	private String description;
	

	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate dueDate;
	

	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate startDate;
	

	private String title;
	

	private Integer coid;	
	

	private Integer personid;	
	

	private Integer deptid;	
	

	private Integer respuserid;	
	

	private Integer addedbyid;	
	

	private Integer orgid;

	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	@Column(name = "duedate")
	public LocalDate getDueDate() {
		return dueDate;
	}

	@Column(name = "startdate")
	public LocalDate getStartDate() {
		return startDate;
	}

	@Column(name = "title")
	public String getTitle() {
		return title;
	}

	@Column(name = "coid")
	public Integer getCoid() {
		return coid;
	}

	@Column(name = "personid")
	public Integer getPersonid() {
		return personid;
	}

	@Column(name = "deptid")
	public Integer getDeptid() {
		return deptid;
	}

	@Column(name = "respuserid")
	public Integer getRespuserid() {
		return respuserid;
	}

	@Column(name = "addedbyid")
	public Integer getAddedbyid() {
		return addedbyid;
	}

	@Column(name = "orgid")
	public Integer getOrgid() {
		return orgid;
	}

}
