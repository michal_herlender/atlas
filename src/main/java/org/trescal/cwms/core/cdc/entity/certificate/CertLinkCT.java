/**
 * 
 */
package org.trescal.cwms.core.cdc.entity.certificate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

import lombok.Setter;

@Setter
@Entity
@Table(name = "dbo_certlink_CT", schema = "cdc")
public class CertLinkCT extends EntityCT {


	private Integer linkid;

	private Integer certid;

	private Integer jobitemid;

	@Column(name = "linkid")
	public Integer getLinkid() {
		return linkid;
	}

	@Column(name = "certid")
	public Integer getCertid() {
		return certid;
	}

	@Column(name = "jobitemid")
	public Integer getJobitemid() {
		return jobitemid;
	}	
	
}
