package org.trescal.cwms.core.cdc.entity.job;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.workflow.entity.TimeOfDay;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "dbo_engineerallocation_CT", schema = "cdc")
public class EngineerAllocationCT extends EntityCT {
	

	private Integer id;

	private Boolean active;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate allocatedFor;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate allocatedOn;

	private TimeOfDay timeofday;

	private Integer allocatedbyid;

	private Integer allocatedtoid;

	private Integer jobitemid;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate allocateduntil;

	private TimeOfDay timeofdayuntil;

	private Integer deptid;
	
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	
	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}
	
	@Column(name = "allocatedfor")
	public LocalDate getAllocatedFor() {
		return allocatedFor;
	}
	
	@Column(name = "allocatedon")
	public LocalDate getAllocatedOn() {
		return allocatedOn;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "timeofday")
	public TimeOfDay getTimeofday() {
		return timeofday;
	}
	
	@Column(name = "allocatedbyid")
	public Integer getAllocatedbyid() {
		return allocatedbyid;
	}
	
	@Column(name = "allocatedtoid")
	public Integer getAllocatedtoid() {
		return allocatedtoid;
	}
	
	@Column(name = "jobitemid")
	public Integer getJobitemid() {
		return jobitemid;
	}
	
	@Column(name = "allocateduntil")
	public LocalDate getAllocateduntil() {
		return allocateduntil;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(name = "timeofdayuntil")
	public TimeOfDay getTimeofdayuntil() {
		return timeofdayuntil;
	}
	
	@Column(name = "deptid")
	public Integer getDeptid() {
		return deptid;
	}

	protected void setId(Integer id) {
		this.id = id;
	}

	protected void setActive(Boolean active) {
		this.active = active;
	}

	protected void setAllocatedFor(LocalDate aLLOCATEDFOR2) {
		this.allocatedFor = aLLOCATEDFOR2;
	}

	protected void setAllocatedOn(LocalDate aLLOCATEDON2) {
		this.allocatedOn = aLLOCATEDON2;
	}

	protected void setTimeofday(TimeOfDay timeofday) {
		this.timeofday = timeofday;
	}

	protected void setAllocatedbyid(Integer allocatedbyid) {
		this.allocatedbyid = allocatedbyid;
	}

	protected void setAllocatedtoid(Integer allocatedtoid) {
		this.allocatedtoid = allocatedtoid;
	}

	protected void setJobitemid(Integer jobitemid) {
		this.jobitemid = jobitemid;
	}

	protected void setAllocateduntil(LocalDate aLLOCATEDUNTIL2) {
		this.allocateduntil = aLLOCATEDUNTIL2;
	}

	protected void setTimeofdayuntil(TimeOfDay timeofdayuntil) {
		this.timeofdayuntil = timeofdayuntil;
	}

	protected void setDeptid(Integer deptid) {
		this.deptid = deptid;
	}

}
