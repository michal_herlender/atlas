package org.trescal.cwms.core.cdc.repository.processedchangeentity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.cdc.entity.ProcessedChangeEntity;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;

@Service("ProcessedChangeEntityBaseService")
public class ProcessedChangeEntityServiceImpl extends BaseServiceImpl<ProcessedChangeEntity, ChangeEntity> 
implements ProcessedChangeEntityService {

	@Autowired
	private ProcessedChangeEntityDao processedChangeEntityDao;

	@Override
	protected BaseDao<ProcessedChangeEntity, ChangeEntity> getBaseDao() {
		return this.processedChangeEntityDao;
	}

}