package org.trescal.cwms.core.cdc.entity.job;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "dbo_job_CT", schema = "cdc")
public class JobCT extends EntityCT {
	

	private Integer jobid;

	private String clientref;

	private String jobno;

	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate regdate;

	@JsonFormat(pattern="yyyy-MM-dd")
	private LocalDate datecomplete;

	private Integer personid;

	private Integer statusid;

	private Integer returnto;

	private JobType jobType;

	private Integer orgid;

	@Column(name = "jobid")
	public Integer getJobid() {
		return jobid;
	}

	@Column(name = "clientref")
	public String getClientref() {
		return clientref;
	}

	@Column(name = "jobno")
	public String getJobno() {
		return jobno;
	}

	@Column(name = "regdate")
	public LocalDate getRegdate() {
		return regdate;
	}

	@Column(name = "datecomplete")
	public LocalDate getDatecomplete() {
		return datecomplete;
	}

	@Column(name = "personid")
	public Integer getPersonid() {
		return personid;
	}

	@Column(name = "statusid")
	public Integer getStatusid() {
		return statusid;
	}

	@Column(name = "returnto")
	public Integer getReturnto() {
		return returnto;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "typeid")
	public JobType getJobType() {
		return jobType;
	}

	@Column(name = "orgid")
	public Integer getOrgid() {
		return orgid;
	}

	protected void setJobid(Integer jobid) {
		this.jobid = jobid;
	}

	protected void setClientref(String clientref) {
		this.clientref = clientref;
	}

	protected void setJobno(String jobno) {
		this.jobno = jobno;
	}

	protected void setRegdate(LocalDate regdate) {
		this.regdate = regdate;
	}

	protected void setDatecomplete(LocalDate datecomplete) {
		this.datecomplete = datecomplete;
	}

	protected void setPersonid(Integer personid) {
		this.personid = personid;
	}

	protected void setStatusid(Integer statusid) {
		this.statusid = statusid;
	}

	protected void setReturnto(Integer returnto) {
		this.returnto = returnto;
	}

	protected void setJobType(JobType jobType) {
		this.jobType = jobType;
	}

	protected void setOrgid(Integer orgid) {
		this.orgid = orgid;
	}
	
}
