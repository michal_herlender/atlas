package org.trescal.cwms.core.cdc.entity.referential;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.instrumentmodel.DomainType;

@Entity
@Table(name = "dbo_servicetype_CT", schema = "cdc")
public class ServiceTypeCT extends EntityCT {
	

	public Integer servicetypeid;

	public String longname;

	public String shortname;

	public Boolean canbeperformedbyclient;

	public DomainType domaintype;

	public Integer orderby;

	public Boolean repair;
	
	@Column(name = "servicetypeid")
	public Integer getServicetypeid() {
		return servicetypeid;
	}

	@Column(name = "longname")
	public String getLongname() {
		return longname;
	}

	@Column(name = "shortname")
	public String getShortname() {
		return shortname;
	}

	@Column(name = "canbeperformedbyclient")
	public Boolean getCanbeperformedbyclient() {
		return canbeperformedbyclient;
	}

	@Column(name = "domaintype")
	public DomainType getDomaintype() {
		return domaintype;
	}

	@Column(name = "orderby")
	public Integer getOrderby() {
		return orderby;
	}

	@Column(name = "repair")
	public Boolean getRepair() {
		return repair;
	}

	protected void setServicetypeid(Integer servicetypeid) {
		this.servicetypeid = servicetypeid;
	}

	protected void setLongname(String longname) {
		this.longname = longname;
	}

	protected void setShortname(String shortname) {
		this.shortname = shortname;
	}

	protected void setCanbeperformedbyclient(Boolean canbeperformedbyclient) {
		this.canbeperformedbyclient = canbeperformedbyclient;
	}

	protected void setDomaintype(DomainType domaintype) {
		this.domaintype = domaintype;
	}

	protected void setOrderby(Integer orderby) {
		this.orderby = orderby;
	}

	protected void setRepair(Boolean repair) {
		this.repair = repair;
	}
	
}
