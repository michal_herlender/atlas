package org.trescal.cwms.core.cdc.repository.base;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;

@Service("BaseCDCService")
public class BaseCDCServiceImpl<Entity> implements BaseCDCService<Entity> {

	@Autowired
	private BaseCDCDao<Entity> baseCDCDao;
/*
	@Override
	public List<byte[]> findLsnBetweenTwoDates(Date fromDate, Date toDate, String tableName) {
		return this.baseCDCDao.findLsnBetweenTwoDates(fromDate, toDate, tableName);
	}*/

	@Override
	public List<? extends Entity> findByStartLsnIds(List<byte[]> startLsnIds, Class<? extends Entity> entityClass) {
		return this.baseCDCDao.findByStartLsnIds(startLsnIds, entityClass);
	}

	@Override
	public List<byte[]> findLsnFromLastProcessedLsn(byte[] lastProcessedLsn, String tableName) {
		return this.baseCDCDao.findLsnFromLastProcessedLsn(lastProcessedLsn, tableName);
	}
	
	@Override
	public byte[] findMinLsn(ChangeEntity changeEntity) {
		return this.baseCDCDao.findMinLsn(changeEntity != null ? changeEntity.getTableName() : null);
	}
	
	@Override
	public byte[] findMaxLsn() {
		return this.baseCDCDao.findMaxLsn();
	}

	@Override
	public List<? extends EntityCT> findGreaterThanLsn(byte[] startLsn, Class<? extends EntityCT> entityClass,
			String tableName) {
		return this.baseCDCDao.findGreaterThanLsn(startLsn, entityClass, tableName);
	}

}