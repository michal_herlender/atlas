package org.trescal.cwms.core.cdc.service;

import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.cdc.entity.ProcessedChangeEntity;
import org.trescal.cwms.core.cdc.repository.base.BaseCDCService;
import org.trescal.cwms.core.cdc.repository.processedchangeentity.ProcessedChangeEntityService;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;
import org.trescal.cwms.core.events.change.queue.ChangeQueue;
import org.trescal.cwms.core.events.service.ChangeQueueService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CdcDataTransformationServiceImpl implements CdcDataTransformationService {

	@Autowired
	private BaseCDCService<EntityCT> entityCTService;
	@Autowired
	private ChangeQueueService changeQueueService;
	@Autowired
	private ProcessedChangeEntityService processedChangeEntityBaseService;

	private ObjectMapper mapper;
	
	public CdcDataTransformationServiceImpl() {
		mapper = new ObjectMapper();
		// Required for LocalDate serialization
		mapper.registerModule(new JavaTimeModule());
	}
	
	@Override
	public String performTransformation() {
		// Processing is ordered so that records with dependencies are processed after their dependencies (see Wiki)
		// TODO : This may have to be revisited for deletions (or we query all, then sort by LSN, then process)
		long startTime = System.currentTimeMillis();
		int count = 0;
		
		// Group 1 - Referential Data
		count += performTransformation(ChangeEntity.BRAND);
		count += performTransformation(ChangeEntity.COUNTRY);
		count += performTransformation(ChangeEntity.SERVICE_TYPE);
		count += performTransformation(ChangeEntity.SUB_FAMILY);
		count += performTransformation(ChangeEntity.INSTRUMENT_MODEL);
		count += performTransformation(ChangeEntity.INSTRUMENT_MODEL_TRANSLATION);
		count += performTransformation(ChangeEntity.UOM);
		
		// Group 2 - Company Data
		count += performTransformation(ChangeEntity.COMPANY);
		count += performTransformation(ChangeEntity.COMPANY_SETTINGS);
		count += performTransformation(ChangeEntity.SUBDIV);
		count += performTransformation(ChangeEntity.SUBDIV_SETTINGS);
		count += performTransformation(ChangeEntity.ADDRESS);
		count += performTransformation(ChangeEntity.LOCATION);
		count += performTransformation(ChangeEntity.CONTACT);
		count += performTransformation(ChangeEntity.DEPARTMENT);
		count += performTransformation(ChangeEntity.DEPARTMENT_MEMBER);
		
		// Group 3 - Instrument Data
		count += performTransformation(ChangeEntity.INSTRUMENT_USAGE_TYPE);
		count += performTransformation(ChangeEntity.INSTRUMENT);
		count += performTransformation(ChangeEntity.INSTRUMENT_RANGE);
		
		// Group 4 - Job Data
		count += performTransformation(ChangeEntity.ITEM_STATE);
		count += performTransformation(ChangeEntity.JOB);
		count += performTransformation(ChangeEntity.JOB_ITEM);
		count += performTransformation(ChangeEntity.JOB_ITEM_GROUP);
		count += performTransformation(ChangeEntity.ENGINEER_ALLOCATION);
		count += performTransformation(ChangeEntity.CAL_REQ);
		count += performTransformation(ChangeEntity.UPCOMING_WORK);
		count += performTransformation(ChangeEntity.UPCOMING_WORK_JOB_LINK);
		count += performTransformation(ChangeEntity.ON_BEHALF_OF_ITEM);
		
		// Group 5 - Instruction Data
		count += performTransformation(ChangeEntity.BASE_INSTRUCTION);
		count += performTransformation(ChangeEntity.COMPANY_INSTRUCTION_LINK);
		count += performTransformation(ChangeEntity.SUBDIV_INSTRUCTION_LINK);
		count += performTransformation(ChangeEntity.CONTACT_INSTRUCTION_LINK);
		count += performTransformation(ChangeEntity.JOB_INSTRUCTION_LINK);
		
		// Group 6 - System Default Data
		count += performTransformation(ChangeEntity.SYSTEM_DEFAULT);
		count += performTransformation(ChangeEntity.SYSTEM_DEFAULT_APPLICATION);
		
		// Group 7 - Work Instruction Data
		count += performTransformation(ChangeEntity.WORK_INSTRUCTION);
		count += performTransformation(ChangeEntity.INSTRUMENT_WORK_INSTRUCTION);
		
		// Group 8 - Certificate Data
		count += performTransformation(ChangeEntity.CERTIFICATE);
		count += performTransformation(ChangeEntity.INST_CERT_LINK);
		count += performTransformation(ChangeEntity.CERT_LINK);
		
		// Group 9 - Invoice Data
		count += performTransformation(ChangeEntity.INVOICE);
		count += performTransformation(ChangeEntity.CREDIT_NOTE);
		
		long finishTime = System.currentTimeMillis();
		
		StringBuffer message = new StringBuffer();
		message.append(count);
		message.append(" change records processed in ");
		message.append((finishTime-startTime)+" ms");
		return message.toString();
	}

	private int performTransformation(ChangeEntity changeEntity) {
		int count = 0;
		byte[] newLastProcessedLsn = null;
		String className = changeEntity.getClazz().getSimpleName();
		log.trace("Begin "+className+" CDC Changes Transformation ... ");
		ProcessedChangeEntity pce = this.processedChangeEntityBaseService.get(changeEntity);
		
		byte[] startLsn = null;
		Class<? extends EntityCT> entityClass = changeEntity.getClazz(); 
		String tableName = changeEntity.getTableName();
		if (pce != null && (pce.getLastProcessedLsn() != null)) {
			startLsn = pce.getLastProcessedLsn();
			log.trace(" CDC : pce.lastProcessedLsn --> 0x"+DatatypeConverter.printHexBinary(startLsn));
		}
		else {
			log.trace(className+" CDC : No lastProcessedLsn");
		}
		List<? extends EntityCT> notProcessedEntity = this.entityCTService.findGreaterThanLsn(startLsn, entityClass, tableName);
		
		log.trace(className+" CDC : notProcessedItems size --> "+notProcessedEntity.size());
		for (EntityCT entityCT : notProcessedEntity) {
			ChangeQueue cq = new ChangeQueue();
			cq.setEntityType(changeEntity);
			cq.setProcessed(false);
			cq.setChangeType(entityCT.getOperationCode());
			String payLoad = convertJson(entityCT);
			cq.setPayload(payLoad);
			this.changeQueueService.save(cq);
			newLastProcessedLsn = entityCT.getCompositeId().getStartLsn();
			count++;
		}
		if ((newLastProcessedLsn != null) && (pce != null)) {
			// Update existing PCE 
			pce.setLastProcessedLsn(newLastProcessedLsn);
			log.trace(className+" CDC : updated PCE with new lastProcessedLSN --> "+DatatypeConverter.printHexBinary(newLastProcessedLsn));
		}
		if ((newLastProcessedLsn != null) && (pce == null)) {
			// Save new PCE
			pce = new ProcessedChangeEntity();
			pce.setChangeEntity(changeEntity);
			pce.setLastProcessedLsn(newLastProcessedLsn);
			this.processedChangeEntityBaseService.save(pce);
			log.trace(className+" CDC : stored new PCE with new lastProcessedLSN --> "+DatatypeConverter.printHexBinary(newLastProcessedLsn));
		}
		log.trace("End "+className+" CDC Changes Transformation ... ");
		return count;
	}
	
	
	private String convertJson(EntityCT entityCT) {
		// TODO revisit exception handling 
		// currently we proceed with next entity and treat payload as null
		// maybe have an "error" flag on ChangeQueue record and have some type of admin notification in job
		String result = null;
		try {
			result = mapper.writeValueAsString(entityCT);
		}
		catch (JsonProcessingException e) {
			log.error("Error converting entity to JSON payload", e);
		}
		return result;
	}	
}
