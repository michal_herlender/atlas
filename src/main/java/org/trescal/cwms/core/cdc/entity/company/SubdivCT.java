package org.trescal.cwms.core.cdc.entity.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;

@Entity
@Table(name = "dbo_subdiv_CT", schema = "cdc")
public class SubdivCT extends EntityCT {
	

	private Integer subdivid;

	private Integer coid;

	private Boolean active;

	private String subname;

	private String subdivcode;
	
	@Column(name = "subdivid")
	public Integer getSubdivid() {
		return subdivid;
	}
	
	@Column(name = "coid")
	public Integer getCoid() {
		return coid;
	}
	
	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}
	
	@Column(name = "subname")
	public String getSubname() {
		return subname;
	}
	
	@Column(name = "subdivcode")
	public String getSubdivcode() {
		return subdivcode;
	}

	protected void setSubdivid(Integer subdivid) {
		this.subdivid = subdivid;
	}

	protected void setCoid(Integer coid) {
		this.coid = coid;
	}

	protected void setActive(Boolean active) {
		this.active = active;
	}

	protected void setSubname(String subname) {
		this.subname = subname;
	}

	protected void setSubdivcode(String subdivcode) {
		this.subdivcode = subdivcode;
	}

}