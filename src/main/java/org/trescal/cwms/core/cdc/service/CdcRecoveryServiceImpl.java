package org.trescal.cwms.core.cdc.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.cdc.repository.base.BaseCDCDaoImpl;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;
import org.trescal.cwms.core.events.change.queue.ChangeQueue;
import org.trescal.cwms.core.events.service.ChangeQueueService;
import org.trescal.cwms.rest.export.service.RestExportEntityType;
import org.trescal.cwms.rest.export.service.RestExportService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * Specialized class to recover from CDC having been disabled / deleted on US production 2021-08-02
 * Not intended for reuse in its current form; is run through upgrade controller.
 * 
 * Use with commit=false first to test, and then commit=true to save changequeue records 
 * 
 * Galen Beck - 2021-08-02
 *
 */
@Service
public class CdcRecoveryServiceImpl implements CdcRecoveryService {
	
	@Autowired
	private ChangeQueueService changeQueueService; 
	@Autowired
	private RestExportService restExportService; 
	
	// This is the time that the CDC configuration was deleted, after which no changes captured (for a few hours)
	// Had to remove the 0000 at the end for Java parsing
	private static String LAST_MODIFIED_FROM_TEXT = "2021-08-02 14:04:56.385";
	// Other constants are in the performInserts section.
	private ObjectMapper mapper;
	
	public CdcRecoveryServiceImpl() {
		mapper = new ObjectMapper();
		// Required for LocalDate serialization
		mapper.registerModule(new JavaTimeModule());
	}
	
	public String performRecovery20210802(boolean commit) {
		StringBuffer result = new StringBuffer();
		result.append("COMMIT=");
		result.append(commit);
		result.append("; ");
		
		// We perform missing inserts and then missing updates 
		// No support for recovery of deletes; there don't seem to be any deleted jobs in this timeframe
		performInserts(result, commit);
		performUpdates(result, commit);
		
		return result.toString();
	}
	
	
	/** 
	 * Inserts are performed for only those entities which were found in an 
	 * external analysis to be missing inserts; the IDs here are from this analysis 
	 */
	private void performInserts(StringBuffer result, boolean commit) {
		result.append("INSERTS:; ");
		// Starting IDs below are based on the last "good" sent id before failure 
		performInsert(ChangeEntity.INSTRUMENT_MODEL,
						RestExportEntityType.INSTRUMENT_MODEL, 				80833, 80846, result, commit);
		performInsert(ChangeEntity.INSTRUMENT_MODEL_TRANSLATION,
						RestExportEntityType.INSTRUMENT_MODEL_TRANSLATION, 	80833, 80846, result, commit);
		performInsert(ChangeEntity.INSTRUMENT,
						RestExportEntityType.INSTRUMENT, 					3164283, 3164299, result, commit);
		performInsert(ChangeEntity.JOB,
						RestExportEntityType.JOB,							185856, 185862, result, commit);
		performInsert(ChangeEntity.JOB_ITEM,
						RestExportEntityType.JOB_ITEM, 						1225424, 1225450, result, commit);
		performInsert(ChangeEntity.ENGINEER_ALLOCATION,
						RestExportEntityType.ENGINEER_ALLOCATION,			66432, 66466, result, commit);
		performInsert(ChangeEntity.CERTIFICATE,
						RestExportEntityType.CERTIFICATE,					2248713, 2248730, result, commit);
		performInsert(ChangeEntity.CERT_LINK,
						RestExportEntityType.CERT_LINK,						1226373, 1226387, result, commit);
		performInsert(ChangeEntity.INSTRUMENT_RANGE,
						RestExportEntityType.INSTRUMENT_RANGE,				43678, 43696, result, commit);
		performInsert(ChangeEntity.JOB_ITEM_GROUP,
						RestExportEntityType.JOB_ITEM_GROUP,				3630, 3632, result, commit);
	}
	
	private void performInsert(ChangeEntity changeEntity, RestExportEntityType exportType, int lastSentId, int finishId, StringBuffer result, boolean commit) {
		// Same as defaults
		int resultsPerPage = 100;
		int currentPage = 1;
		
		// First insert is based on lastSentId + 1
		int currentId = lastSentId+1;
		int countInserted = 0;
		while (currentId <= finishId) {
			// Export out all locales (for translations)
			List<?> results = this.restExportService.getResults(exportType, currentId, null, resultsPerPage, currentPage).getResults();
			for (Object exportDto : results) {
				createChangeQueueRecord(changeEntity, exportDto, BaseCDCDaoImpl.CDC_OPERATION_INSERT, commit);
				countInserted++;
			}
			
			currentId++;
		}
		result.append(changeEntity.name());
		result.append("=");
		result.append(countInserted);
		result.append("; ");
	}
	
	private void createChangeQueueRecord(ChangeEntity changeEntity, Object exportDto, int operationCode, boolean commit) {
		String payload = null;
		try {
			payload = mapper.writeValueAsString(exportDto);
		}
		catch (JsonProcessingException e) {
			throw new RuntimeException("Error converting entity to JSON payload", e);
		}
		// We only save the entity if the "commit" flag is true
		ChangeQueue queueRecord = new ChangeQueue();
		queueRecord.setChangeType(operationCode);
		queueRecord.setEntityType(changeEntity);
		queueRecord.setPayload(payload);
		queueRecord.setProcessed(false);
		if (commit) {
			this.changeQueueService.save(queueRecord);
		}
	}
	
	/** 
	 * Updates are performed for only those entities which were found in an 
	 * external analysis to be missing inserts; we update all 
	 */
	private void performUpdates(StringBuffer result, boolean commit) {
		result.append("UPDATES:; ");
		performUpdate(ChangeEntity.INSTRUMENT_MODEL, 	RestExportEntityType.INSTRUMENT_MODEL, 	result, commit);
		performUpdate(ChangeEntity.COMPANY, 			RestExportEntityType.COMPANY, 			result, commit);
		performUpdate(ChangeEntity.COMPANY_SETTINGS, 	RestExportEntityType.COMPANY_SETTINGS, 	result, commit);
		performUpdate(ChangeEntity.SUBDIV, 				RestExportEntityType.SUBDIV, 			result, commit);
		performUpdate(ChangeEntity.CONTACT, 			RestExportEntityType.CONTACT, 			result, commit);
		performUpdate(ChangeEntity.INSTRUMENT, 			RestExportEntityType.INSTRUMENT, 		result, commit);
		performUpdate(ChangeEntity.JOB, 				RestExportEntityType.JOB, 				result, commit);
		performUpdate(ChangeEntity.JOB_ITEM, 			RestExportEntityType.JOB_ITEM, 			result, commit);
		performUpdate(ChangeEntity.WORK_INSTRUCTION, 	RestExportEntityType.WORK_INSTRUCTION, 	result, commit);
		performUpdate(ChangeEntity.CERTIFICATE, 		RestExportEntityType.CERTIFICATE, 		result, commit);
		performUpdate(ChangeEntity.INST_CERT_LINK, 		RestExportEntityType.INST_CERT_LINK, 	result, commit);
		performUpdate(ChangeEntity.CERT_LINK, 			RestExportEntityType.CERT_LINK, 		result, commit);
		performUpdate(ChangeEntity.INSTRUMENT_RANGE, 	RestExportEntityType.INSTRUMENT_RANGE, 	result, commit);
		performUpdate(ChangeEntity.JOB_ITEM_GROUP, 		RestExportEntityType.JOB_ITEM_GROUP, 	result, commit);
	}
	
	private void performUpdate(ChangeEntity changeEntity, RestExportEntityType exportType, StringBuffer result, boolean commit) {
		SimpleDateFormat ssmsDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date lastModifiedFrom;
		try {
			lastModifiedFrom = ssmsDateFormat.parse(LAST_MODIFIED_FROM_TEXT);
		} catch (ParseException e) {
			throw new RuntimeException("Unparseable date : "+LAST_MODIFIED_FROM_TEXT);
		}
		
		int countUpdated = 0;
		
		List<?> results = restExportService.getLastModifiedResults(exportType, lastModifiedFrom);
		for (Object exportDto : results) {
			createChangeQueueRecord(changeEntity, exportDto, BaseCDCDaoImpl.CDC_OPERATION_UPDATE_AFTER_CHANGE, commit);
			countUpdated++;
		}
		
		result.append(changeEntity.name());
		result.append("=");
		result.append(countUpdated);
		result.append("; ");
	}
}
