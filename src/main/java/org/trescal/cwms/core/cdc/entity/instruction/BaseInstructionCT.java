package org.trescal.cwms.core.cdc.entity.instruction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

@Entity
@Table(name = "dbo_baseinstruction_CT", schema = "cdc")
public class BaseInstructionCT extends EntityCT {


	private Integer instructionid;

	private String instruction;

	private InstructionType instructiontype;
	
	@Column(name = "instructionid")
	public Integer getInstructionid() {
		return instructionid;
	}
	
	@Column(name = "instruction")
	public String getInstruction() {
		return instruction;
	}
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "instructiontypeid")
	public InstructionType getInstructiontype() {
		return instructiontype;
	}

	protected void setInstructionid(Integer instructionid) {
		this.instructionid = instructionid;
	}

	protected void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	protected void setInstructiontype(InstructionType instructiontype) {
		this.instructiontype = instructiontype;
	}
}
