package org.trescal.cwms.core.cdc.repository.base;

import java.util.Date;
import java.util.List;

import org.trescal.cwms.core.cdc.entity.EntityCT;
import org.trescal.cwms.core.events.change.enums.ChangeEntity;

public interface BaseCDCService<Entity> {

/*	List<byte[]> findLsnBetweenTwoDates(Date fromDate, Date toDate, String tableName);*/
	
	List<byte[]> findLsnFromLastProcessedLsn(byte[] lastProcessedLsn, String tableName);
	
	List<? extends Entity> findByStartLsnIds(List<byte[]> startLsnIds, Class<? extends Entity> entityClass);
	
	List<? extends EntityCT> findGreaterThanLsn(byte[] startLsn, Class<? extends EntityCT> entityClass, String tableName);
	
	byte[] findMinLsn(ChangeEntity changeEntity);
	
	byte[] findMaxLsn();
	
}