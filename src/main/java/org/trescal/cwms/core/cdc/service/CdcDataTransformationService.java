package org.trescal.cwms.core.cdc.service;

public interface CdcDataTransformationService {
	String performTransformation();
}
