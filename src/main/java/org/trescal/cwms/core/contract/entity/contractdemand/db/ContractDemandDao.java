package org.trescal.cwms.core.contract.entity.contractdemand.db;

import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.contract.dtos.ContractDemandDto;
import org.trescal.cwms.core.contract.dtos.ContractDemandInput;
import org.trescal.cwms.core.contract.entity.contractdemand.ContractDemand;
import org.trescal.cwms.datatables.DatatableOutput;

public interface ContractDemandDao extends BaseDao<ContractDemand, Integer> {

	ContractDemand findBestMatch(Integer contractId, Integer plantId);

	DatatableOutput<ContractDemandDto> getDemands(ContractDemandInput input, Locale locale);
}