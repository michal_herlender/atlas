package org.trescal.cwms.core.contract.entity.contract.db;

import lombok.val;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.audit.entity.db.CriteriaQueryGenerator;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.contract.dtos.*;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.Contract_;
import org.trescal.cwms.core.contract.entity.contractdemand.ContractDemand;
import org.trescal.cwms.core.contract.entity.contractdemand.ContractDemand_;
import org.trescal.cwms.core.contract.entity.contractservicetypesettings.ContractServiceTypeSettings;
import org.trescal.cwms.core.contract.entity.contractservicetypesettings.ContractServiceTypeSettings_;
import org.trescal.cwms.core.contract.enums.ContractInstrumentListType;
import org.trescal.cwms.core.contract.form.ContractSearchForm;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily_;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem_;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation_;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.datatables.DatatableOutput;
import org.trescal.cwms.datatables.DatatableSortDirection;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.function.BiFunction;
import java.util.function.Function;

@Repository
public class ContractDaoImpl extends BaseDaoImpl<Contract, Integer> implements ContractDao {

    @Override
    protected Class<Contract> getEntity() {
        return Contract.class;
    }

    @Override
    public PagedResultSet<ContractSearchResultsDto> searchContracts(ContractSearchForm form) {

        PagedResultSet<ContractSearchResultsDto> resultSet = new PagedResultSet<>(
                form.getResultsPerPage(), form.getPageNo());

        completePagedResultSet(resultSet, ContractSearchResultsDto.class, cb -> cq -> {

            Root<Contract> contractRoot = cq.from(Contract.class);
            Join<Contract, Company> clientCompanyJoin = contractRoot.join(Contract_.clientCompany);
            Join<Contract, Company> businessCompanyJoin = contractRoot.join(Contract_.organisation.getName());
            Join<Contract, Subdiv> subdivsJoin;

            Predicate restrictions = cb.conjunction();
            restrictions.getExpressions()
                    .add(cb.equal(businessCompanyJoin.get(Company_.coid), form.getBusinessCompanyId()));
            if (form.getClientCompanyId() != null)
                restrictions.getExpressions()
                        .add(cb.equal(clientCompanyJoin.get(Company_.coid), form.getClientCompanyId()));
            if (form.getLegalContractNumber() != null && !form.getLegalContractNumber().isEmpty())
                restrictions.getExpressions()
                        .add(cb.equal(contractRoot.get(Contract_.legalContractNo), form.getLegalContractNumber()));
            if (form.getExcludeExpired() != null && form.getExcludeExpired())
                restrictions.getExpressions()
                        .add(cb.greaterThanOrEqualTo(contractRoot.get(Contract_.endDate), LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())));
            if (form.getClientSubdivId() != null && form.getClientSubdivId() > 0) {
                subdivsJoin = contractRoot.join(Contract_.subdivs, JoinType.LEFT);
                restrictions.getExpressions()
                        .add(cb.equal(subdivsJoin.get(Subdiv_.subdivid), form.getClientSubdivId()));
            }

            cq.where(restrictions);

            Selection<ContractSearchResultsDto> selection = cb.construct(ContractSearchResultsDto.class,
                    contractRoot.get(Contract_.id), contractRoot.get(Contract_.contractNumber),
                    clientCompanyJoin.get(Company_.coname), contractRoot.get(Contract_.legalContractNo),
                    contractRoot.get(Contract_.startDate), contractRoot.get(Contract_.endDate));

            List<Order> order = new ArrayList<>();
            order.add(cb.desc(contractRoot.get(Contract_.id)));

            return Triple.of(contractRoot, selection, order);

        });
        return resultSet;
    }

    @Override
    public DatatableOutput<ContractInstrumentListDto> getInstrumentList(ContractInstrumentListInput input,
                                                                        Locale locale) {
        DatatableOutput<ContractInstrumentListDto> output = new DatatableOutput<>();
        output.setDraw(input.getDraw());
        completeDataTablesOutput(output, input, ContractInstrumentListDto.class, getFilterFunction(input, locale),
                getUnfilteredTotalFunction(input));
        return output;
    }

    @Override
    public DatatableOutput<ContractInstrumentListDto> getUnlinkedInstrumentList(ContractInstrumentListInput input,
                                                                                Locale locale) {
        DatatableOutput<ContractInstrumentListDto> output = new DatatableOutput<>();
        output.setDraw(input.getDraw());
        completeDataTablesOutput(output, input, ContractInstrumentListDto.class,
                getUnlinkedInstsFunction(input, locale), getUnfilteredTotalFunction(input));
        return output;
    }

    @Override
    public List<MatchingContractDto> findContractsMatchingCompany(Company company, ServiceType serviceType) {
        return getMatchingContracts((cr, cb) -> {
            Predicate mainPredicate = cb.and(cb.equal(cr.get(Contract_.clientCompany), company),
                    cb.isEmpty(cr.get(Contract_.subdivs)),
                    cb.equal(cr.get(Contract_.instrumentListType), ContractInstrumentListType.ADDITIONAL));
            if (serviceType != null) {
                Join<Contract, ContractServiceTypeSettings> csts = cr.join(Contract_.serviceTypeSettings);
                csts.on(cb.equal(csts.get(ContractServiceTypeSettings_.serviceType), serviceType));
            }
            return mainPredicate;
        });
    }

    @Override
    public List<MatchingContractDto> findContractsMatchingSubdiv(Subdiv subdiv, Company allocatedCompany, ServiceType serviceType) {
        return getMatchingContracts((cr, cb) -> {
            Predicate mainPredicate = cb.conjunction();
            mainPredicate.getExpressions().add(cb.isMember(subdiv, cr.get(Contract_.subdivs)));
            mainPredicate.getExpressions().add(cb.equal(cr.get(Contract_.instrumentListType), ContractInstrumentListType.ADDITIONAL));
            mainPredicate.getExpressions().add(cb.equal(cr.get(Contract_.organisation), allocatedCompany));
            if (serviceType != null) {
                Join<Contract, ContractServiceTypeSettings> csts = cr.join(Contract_.serviceTypeSettings);
                csts.on(cb.equal(csts.get(ContractServiceTypeSettings_.serviceType), serviceType));
            }
            return mainPredicate;
        });
    }

    @Override
    public List<MatchingContractDto> findContractsMatchingInstrument(Instrument instrument, Company clientCompany, Company allocatedCompany, ServiceType serviceType) {
        return getMatchingContracts((cr, cb) -> {
            if (serviceType != null) {
                Join<Contract, ContractServiceTypeSettings> csts = cr.join(Contract_.serviceTypeSettings);
                csts.on(cb.equal(csts.get(ContractServiceTypeSettings_.serviceType), serviceType));
            }
            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.equal(cr.get(Contract_.clientCompany), clientCompany));
            clauses.getExpressions().add(cb.equal(cr.get(Contract_.organisation), allocatedCompany));
            clauses.getExpressions().add(cb.isMember(instrument, cr.get(Contract_.instruments)));
            return clauses;
        });
    }

    private List<MatchingContractDto> getMatchingContracts(
            BiFunction<Root<Contract>, CriteriaBuilder, Predicate> restrictions) {
        return getResultList(cb -> {
            CriteriaQuery<MatchingContractDto> cq = cb.createQuery(MatchingContractDto.class);
            Root<Contract> contract = cq.from(Contract.class);
            Predicate variableRestrictions = restrictions.apply(contract, cb);
            cq.where(cb.and(variableRestrictions,
                    cb.greaterThanOrEqualTo(contract.get(Contract_.endDate), LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()))));
            cq.select(cb.construct(MatchingContractDto.class, contract.get(Contract_.id),
                    contract.get(Contract_.contractNumber), contract.get(Contract_.description)));
            cq.orderBy(cb.asc(contract.get(Contract_.id)));
            return cq;
        });
    }

    private CriteriaQueryGenerator<ContractInstrumentListDto> getFilterFunction(ContractInstrumentListInput input,
                                                                                Locale locale) {
        return cb -> cq -> {

            Root<Contract> contractRoot = cq.from(Contract.class);
            Join<Contract, Instrument> instrumentsJoin = contractRoot.join(Contract_.instruments);
            Join<Instrument, InstrumentModel> modelJoin = instrumentsJoin.join(Instrument_.model);

            Join<InstrumentModel, Translation> modelNamesJoin = modelJoin.join(InstrumentModel_.nameTranslations,
                    javax.persistence.criteria.JoinType.LEFT);
            modelNamesJoin.on(cb.equal(modelNamesJoin.get(Translation_.locale), locale));

            Join<Instrument, Address> addressJoin = instrumentsJoin.join(Instrument_.add);
            Join<Address, Subdiv> subdivJoin = addressJoin.join(Address_.sub);

            Predicate restrictions = cb.conjunction();
            restrictions.getExpressions().add(cb.equal(contractRoot.get(Contract_.id), input.getContractId()));
            cq.where(addSearch(restrictions, cb, input, instrumentsJoin, modelNamesJoin, subdivJoin));
            return Triple.of(contractRoot, instrumentListProjection(cb, instrumentsJoin, modelNamesJoin, subdivJoin),
                    getOrder(cb, input, instrumentsJoin, modelNamesJoin, subdivJoin));

        };
    }

    private CriteriaQueryGenerator<ContractInstrumentListDto> getUnlinkedInstsFunction(
            ContractInstrumentListInput input, Locale locale) {

        Contract contract = getReference(input.getContractId());

        return cb -> cq -> {
            Root<Instrument> instrumentRoot = cq.from(Instrument.class);
            Join<Instrument, Company> companyJoin = instrumentRoot.join(Instrument_.comp);
            Join<Instrument, InstrumentModel> modelJoin = instrumentRoot.join(Instrument_.model);
            Join<InstrumentModel, Translation> modelNamesJoin = modelJoin.join(InstrumentModel_.nameTranslations,
                    javax.persistence.criteria.JoinType.LEFT);
            modelNamesJoin.on(cb.equal(modelNamesJoin.get(Translation_.locale), locale));
            Join<Instrument, Address> addressJoin = instrumentRoot.join(Instrument_.add);
            Join<Address, Subdiv> subdivJoin = addressJoin.join(Address_.sub);

            Predicate restrictions = cb.conjunction();
            restrictions.getExpressions().add(cb.equal(companyJoin.get(Company_.coid), input.getCompanyId()));
            restrictions.getExpressions().add(cb.isNotMember(contract, instrumentRoot.get(Instrument_.contracts)));
            if (input.getSubdivId() != null && input.getSubdivId() > 0) {
                restrictions.getExpressions().add(cb.equal(subdivJoin.get(Subdiv_.subdivid), input.getSubdivId()));
            }

            cq.where(addSearch(restrictions, cb, input, instrumentRoot, modelNamesJoin, subdivJoin));

            return Triple.of(instrumentRoot, instrumentListProjection(cb, instrumentRoot, modelNamesJoin, subdivJoin),
                    getOrder(cb, input, instrumentRoot, modelNamesJoin, subdivJoin));

        };

    }

    private Function<CriteriaBuilder, Function<CriteriaQuery<Long>, Expression<?>>> getUnfilteredTotalFunction(
            ContractInstrumentListInput input) {
        return cb -> cq -> {

            Root<Contract> contractRoot = cq.from(Contract.class);
            Join<Contract, Instrument> instrumentsJoin = contractRoot.join(Contract_.instruments, JoinType.LEFT);

            Predicate restrictions = cb.conjunction();
            restrictions.getExpressions().add(cb.equal(contractRoot.get(Contract_.id), input.getContractId()));
            cq.where(restrictions);

            cq.select(cb.count(instrumentsJoin));

            return contractRoot;

        };
    }

    private Selection<ContractInstrumentListDto> instrumentListProjection(CriteriaBuilder cb,
                                                                          From<?, Instrument> instrument, Join<InstrumentModel, Translation> modelName,
                                                                          Join<Address, Subdiv> subdiv) {
        return cb.construct(ContractInstrumentListDto.class, instrument.get(Instrument_.plantid),
                modelName.get(Translation_.translation), instrument.get(Instrument_.serialno),
                subdiv.get(Subdiv_.subname));
    }

    private List<Order> getOrder(CriteriaBuilder cb, ContractInstrumentListInput input, From<?, Instrument> instrument,
                                 Join<InstrumentModel, Translation> modelName, Join<Address, Subdiv> subdiv) {

        String datatableSortField = input.getColumns().get(input.getOrder().get(0).getColumn()).getData();
        Path<?> sortField;
        switch (datatableSortField) {
            case "instrumentName":
                sortField = modelName.get(Translation_.translation);
                break;
            case "serialNo":
                sortField = instrument.get(Instrument_.serialno);
                break;
            case "subdivName":
                sortField = subdiv.get(Subdiv_.subname);
                break;
            case "plantId":
            default:
                sortField = instrument.get(Instrument_.plantid);
                break;
        }

        List<Order> order = new ArrayList<>();
        if (input.getOrder().get(0).getDir().equals(DatatableSortDirection.desc)) {
            order.add(cb.desc(sortField));
        } else {
            order.add(cb.asc(sortField));
        }

        return order;
    }

    private Predicate addSearch(Predicate restrictions, CriteriaBuilder cb, ContractInstrumentListInput input,
                                From<?, Instrument> instrument, Join<InstrumentModel, Translation> modelName,
                                Join<Address, Subdiv> subdiv) {
        if (!input.getSearch().getValue().isEmpty()) {
            Predicate search = cb.or(
                    cb.like(instrument.get(Instrument_.plantid).as(String.class),
                            "%" + input.getSearch().getValue() + "%"),
                    cb.like(modelName.get(Translation_.translation), "%" + input.getSearch().getValue() + "%"),
                    cb.like(instrument.get(Instrument_.serialno), "%" + input.getSearch().getValue() + "%"),
                    cb.like(subdiv.get(Subdiv_.subname), "%" + input.getSearch().getValue() + "%"));
            restrictions.getExpressions().add(search);
        }
        return restrictions;
    }

    @Override
    public ContractInformationForJobItemDto getContractInformation(Integer contractId, Integer jobItemId) {
        return getSingleResult(cb -> {
            CriteriaQuery<ContractInformationForJobItemDto> cq = cb.createQuery(ContractInformationForJobItemDto.class);
            Root<Contract> contract = cq.from(Contract.class);
            Subquery<Description> subfamilySq = cq.subquery(Description.class);
            Root<JobItem> sfsqJobItem = subfamilySq.from(JobItem.class);
            Join<JobItem, Instrument> instrument = sfsqJobItem.join(JobItem_.inst);
            Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model);
            subfamilySq.where(cb.equal(sfsqJobItem.get(JobItem_.jobItemId), jobItemId));
            subfamilySq.select(model.get(InstrumentModel_.description));
            Join<Contract, ContractDemand> contractDemand = contract.join(Contract_.demands, JoinType.LEFT);
            contractDemand.on(cb.equal(contractDemand.get(ContractDemand_.subfamily), subfamilySq.getSelection()));
            Subquery<InstrumentModelDomain> domainSq = cq.subquery(InstrumentModelDomain.class);
            Root<JobItem> dosqJobItem = domainSq.from(JobItem.class);
            Join<JobItem, Instrument> dosqInstrument = dosqJobItem.join(JobItem_.inst);
            Join<Instrument, InstrumentModel> dosqModel = dosqInstrument.join(Instrument_.model);
            Join<InstrumentModel, Description> dosqSubfamily = dosqModel.join(InstrumentModel_.description);
            Join<Description, InstrumentModelFamily> dosqFamily = dosqSubfamily.join(Description_.family);
            domainSq.where(cb.equal(dosqJobItem.get(JobItem_.jobItemId), jobItemId));
            domainSq.select(dosqFamily.get(InstrumentModelFamily_.domain));
            Join<Contract, ContractDemand> contractDemand2 = contract.join(Contract_.demands, JoinType.LEFT);
            contractDemand2.on(cb.and(cb.isNull(contractDemand2.get(ContractDemand_.subfamily)),
                    cb.equal(contractDemand2.get(ContractDemand_.domain), domainSq.getSelection())));
            Subquery<ServiceType> serviceTypeSq = cq.subquery(ServiceType.class);
            Root<JobItem> stsqJobItem = serviceTypeSq.from(JobItem.class);
            Join<JobItem, CalibrationType> stsqCalType = stsqJobItem.join(JobItem_.calType);
            Join<CalibrationType, ServiceType> stsqServiceType = stsqCalType.join(CalibrationType_.serviceType);
            serviceTypeSq.where(cb.equal(stsqJobItem.get(JobItem_.jobItemId), jobItemId));
            serviceTypeSq.select(stsqServiceType);
            Join<Contract, ContractServiceTypeSettings> serviceTypeSettings = contract
                    .join(Contract_.serviceTypeSettings, JoinType.LEFT);
            serviceTypeSettings.on(cb.equal(serviceTypeSettings.get(ContractServiceTypeSettings_.serviceType),
                    serviceTypeSq.getSelection()));
            Join<Contract, Quotation> quotation = contract.join(Contract_.quotation, JoinType.LEFT);
            cq.where(cb.equal(contract.get(Contract_.id), contractId));
            cq.select(cb.construct(ContractInformationForJobItemDto.class, contract.get(Contract_.id),
                    serviceTypeSettings.get(ContractServiceTypeSettings_.turnaround),
                    cb.coalesce(contractDemand.get(ContractDemand_.demandSet),
                            contractDemand2.get(ContractDemand_.demandSet)),
                    quotation.get(Quotation_.id), quotation.get(Quotation_.qno), quotation.get(Quotation_.ver),
                    quotation.get(Quotation_.issued), quotation.get(Quotation_.expiryDate),
                    contract.get(Contract_.warrantyCalibration), contract.get(Contract_.warrantyRepair)));
            return cq;
        });
    }

    @Override
    public List<KeyValueIntegerString> getKeyValueList(Collection<Integer> contractIds) {
        return getResultList(cb -> {
            CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class);
            Root<Contract> root = cq.from(Contract.class);
            cq.where(root.get(Contract_.id).in(contractIds));
            cq.select(cb.construct(KeyValueIntegerString.class, root.get(Contract_.id),
                    root.get(Contract_.contractNumber)));
            return cq;
        });
    }

    @Override
    public List<Integer> getInstrumentIds(Integer contractId) {
        return getResultList(cb -> {
            CriteriaQuery<Integer> cq = cb.createQuery(Integer.class);
            Root<Contract> contract = cq.from(Contract.class);
            Join<Contract, Instrument> instrument = contract.join(Contract_.instruments);
            cq.where(cb.equal(contract.get(Contract_.id), contractId));
            cq.select(instrument.get(Instrument_.plantid));
            return cq;
        });
    }

    @Override
    public List<Contract> findContractsByClientCompany(Integer coid) {
        return getResultList(cb -> {
            CriteriaQuery<Contract> cq = cb.createQuery(Contract.class);
            Root<Contract> contract = cq.from(Contract.class);
            Join<Contract, Company> clientCompany = contract.join(Contract_.clientCompany);
            cq.where(cb.equal(clientCompany.get(Company_.coid), coid));
            cq.orderBy(cb.asc(contract.get(Contract_.id)));
            return cq;
        });
    }

    @Override
    public Contract findContractByQuotation(Quotation quotation) {
        return getFirstResult(cb -> {
            CriteriaQuery<Contract> cq = cb.createQuery(Contract.class);
            val contract = cq.from(Contract.class);
            val quotationJoin = contract.join(Contract_.quotation);
            cq.where(cb.equal(quotationJoin, quotation));
            cq.orderBy(cb.desc(contract.get(Contract_.id)));
            return cq;
        }).orElse(null);
    }
}