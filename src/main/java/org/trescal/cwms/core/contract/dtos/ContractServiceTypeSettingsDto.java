package org.trescal.cwms.core.contract.dtos;

public class ContractServiceTypeSettingsDto {

	private Integer contractId;
	private Integer serviceTypeId;
	private Integer turnaround;
	private Boolean add;

	public Integer getContractId() {
		return contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public Integer getServiceTypeId() {
		return serviceTypeId;
	}

	public void setServiceTypeId(Integer serviceTypeId) {
		this.serviceTypeId = serviceTypeId;
	}

	public Integer getTurnaround() {
		return turnaround;
	}

	public void setTurnaround(Integer turnaround) {
		this.turnaround = turnaround;
	}

	public Boolean getAdd() {
		return add;
	}

	public void setAdd(Boolean add) {
		this.add = add;
	}
}