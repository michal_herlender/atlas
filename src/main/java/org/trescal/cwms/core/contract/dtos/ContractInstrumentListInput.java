package org.trescal.cwms.core.contract.dtos;

import org.trescal.cwms.datatables.DatatableInput;

public class ContractInstrumentListInput extends DatatableInput {

    private Integer contractId;
    private Integer companyId;
    private Integer subdivId;


    public Integer getContractId() {
        return contractId;
    }
    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Integer getCompanyId() {
        return companyId;
    }
    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getSubdivId() { return subdivId; }
    public void setSubdivId(Integer subdivId) { this.subdivId = subdivId; }
}
