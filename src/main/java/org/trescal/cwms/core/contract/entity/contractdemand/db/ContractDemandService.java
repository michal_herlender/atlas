package org.trescal.cwms.core.contract.entity.contractdemand.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.contract.dtos.ContractDemandDto;
import org.trescal.cwms.core.contract.dtos.ContractDemandInput;
import org.trescal.cwms.core.contract.entity.contractdemand.ContractDemand;
import org.trescal.cwms.datatables.DatatableOutput;

public interface ContractDemandService extends BaseService<ContractDemand, Integer> {

	ContractDemand findBestMatch(Integer contractId, Integer plantId);

	DatatableOutput<ContractDemandDto> getDemands(ContractDemandInput input);

	boolean createContractDemand(Integer contractId, Integer domainId, Integer subfamilyId, Boolean cleaning,
			Boolean sealing, Boolean engraving);
}