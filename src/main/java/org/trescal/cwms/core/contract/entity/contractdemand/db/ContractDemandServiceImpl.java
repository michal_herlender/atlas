package org.trescal.cwms.core.contract.entity.contractdemand.db;

import java.util.BitSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.contract.dtos.ContractDemandDto;
import org.trescal.cwms.core.contract.dtos.ContractDemandInput;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.db.ContractService;
import org.trescal.cwms.core.contract.entity.contractdemand.ContractDemand;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.domain.db.InstrumentModelDomainService;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemand;
import org.trescal.cwms.datatables.DatatableOutput;

@Service
public class ContractDemandServiceImpl extends BaseServiceImpl<ContractDemand, Integer>
		implements ContractDemandService {

	@Autowired
	private ContractDemandDao contractDemandDao;
	@Autowired
	private ContractService contractService;
	@Autowired
	private NewDescriptionService subfamilyService;
	@Autowired
	private InstrumentModelDomainService domainService;

	@Override
	protected BaseDao<ContractDemand, Integer> getBaseDao() {
		return contractDemandDao;
	}

	@Override
	public ContractDemand findBestMatch(Integer contractId, Integer plantId) {
		return contractDemandDao.findBestMatch(contractId, plantId);
	}

	@Override
	public DatatableOutput<ContractDemandDto> getDemands(ContractDemandInput input) {
		return contractDemandDao.getDemands(input, LocaleContextHolder.getLocale());
	}

	@Override
	public boolean createContractDemand(Integer contractId, Integer domainId, Integer subfamilyId, Boolean cleaning,
			Boolean sealing, Boolean engraving) {
		Contract contract = contractService.get(contractId);
		InstrumentModelDomain domain = domainId == null ? null : domainService.getDomain(domainId);
		Description subfamily = null;
		if (subfamilyId != null) {
			subfamily = subfamilyService.findDescription(subfamilyId);
			if (subfamily != null)
				domain = subfamily.getFamily().getDomain();
		}
		ContractDemand contractDemand = new ContractDemand();
		contractDemand.setContract(contract);
		contractDemand.setDomain(domain);
		contractDemand.setSubfamily(subfamily);
		BitSet demands = new BitSet();
		if (cleaning)
			demands.set(AdditionalDemand.CLEANING.ordinal());
		if (sealing)
			demands.set(AdditionalDemand.SEALING.ordinal());
		if (engraving)
			demands.set(AdditionalDemand.ENGRAVING.ordinal());
		contractDemand.setDemandSet(demands);
		save(contractDemand);
		return true;
	}
}