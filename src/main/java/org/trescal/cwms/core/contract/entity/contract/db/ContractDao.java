package org.trescal.cwms.core.contract.entity.contract.db;

import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.contract.dtos.ContractInformationForJobItemDto;
import org.trescal.cwms.core.contract.dtos.ContractInstrumentListDto;
import org.trescal.cwms.core.contract.dtos.ContractInstrumentListInput;
import org.trescal.cwms.core.contract.dtos.ContractSearchResultsDto;
import org.trescal.cwms.core.contract.dtos.MatchingContractDto;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.form.ContractSearchForm;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.datatables.DatatableOutput;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface ContractDao extends BaseDao<Contract, Integer> {

    PagedResultSet<ContractSearchResultsDto> searchContracts(ContractSearchForm form);

    List<MatchingContractDto> findContractsMatchingCompany(Company company, ServiceType serviceType);

    List<MatchingContractDto> findContractsMatchingSubdiv(Subdiv subdiv, Company allocatedCompany, ServiceType serviceType);

    List<MatchingContractDto> findContractsMatchingInstrument(Instrument instrument, Company clientCompany, Company allocatedCompany, ServiceType serviceType);

    List<KeyValueIntegerString> getKeyValueList(Collection<Integer> contractIds);

    DatatableOutput<ContractInstrumentListDto> getInstrumentList(ContractInstrumentListInput input, Locale locale);

    DatatableOutput<ContractInstrumentListDto> getUnlinkedInstrumentList(ContractInstrumentListInput input,
                                                                         Locale locale);

    ContractInformationForJobItemDto getContractInformation(Integer contractId, Integer jobItemId);

    List<Integer> getInstrumentIds(Integer contractId);

    List<Contract> findContractsByClientCompany(Integer coid);

    Contract findContractByQuotation(Quotation quotation);
}