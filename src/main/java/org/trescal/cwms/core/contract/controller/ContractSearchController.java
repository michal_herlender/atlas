package org.trescal.cwms.core.contract.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.contract.dtos.ContractSearchResultsDto;
import org.trescal.cwms.core.contract.entity.contract.db.ContractService;
import org.trescal.cwms.core.contract.form.ContractSearchForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@RequestMapping(path = "/contractsearch.htm")
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_COMPANY })
public class ContractSearchController {

	@Autowired
	private ContractService contractService;

	@ModelAttribute("command")
	public ContractSearchForm setUpForm(
			@SessionAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyKeyValue) {
		ContractSearchForm form = new ContractSearchForm();
		form.setBusinessCompanyId(companyKeyValue.getKey());
//		form.setPageNo(1);
//		form.setResultsPerPage(Constants.RESULTS_PER_PAGE);
//		form.setResultSet(new PagedResultSet<>(Constants.RESULTS_PER_PAGE, 1));
		return form;
	}

	@GetMapping
	public String displayForm() {
		return "trescal/core/contract/contractsearch";
	}

	@PostMapping
	public String displaySearchResults(Model model, @ModelAttribute("command") ContractSearchForm form) {
		PagedResultSet<ContractSearchResultsDto> resultSet = contractService.searchContracts(form);
		model.addAttribute("resultSet", resultSet);
		
		return "trescal/core/contract/contractsearchresults";
	}
}