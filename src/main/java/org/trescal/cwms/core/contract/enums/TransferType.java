package org.trescal.cwms.core.contract.enums;


import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
public enum TransferType {


    FTP("contract.ftp"),
    REST("contract.rest"),
    WEBDAV("contract.webdav"),
    SOAP("contract.soap");


    private final String nameCode;
    private  MessageSource messageSource;

    TransferType(String nameCode){
        this.nameCode = nameCode;
    }

    @Component
    public static class TransferTypeMessageSourceInjector
    {
        @Autowired
        private MessageSource messsageSource;

        @PostConstruct
        public void postConstruct() {
            for (TransferType type : EnumSet.allOf(TransferType.class)) {
                type.setMessageSource(messsageSource);
            }
        }
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getName() {
        return messageSource.getMessage(nameCode, null, LocaleContextHolder.getLocale());
    }
}