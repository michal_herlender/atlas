package org.trescal.cwms.core.contract.enums;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.EnumSet;

public enum ContractInstrumentListType {

    ADDITIONAL("contract.instrumentlisttype.additional"),
    EXCLUSIVE("contract.instrumentlisttype.exclusive");

    private final String nameCode;
    private MessageSource messageSource;

    ContractInstrumentListType(String nameCode){
        this.nameCode = nameCode;
    }

    @Component
    public static class ContractInstrumentListTypeMessageSourceInjector
    {

        private MessageSource messageSource;

        public ContractInstrumentListTypeMessageSourceInjector(MessageSource messageSource) {
            this.messageSource = messageSource;
        }

        @PostConstruct
        public void postConstruct() {
            for (ContractInstrumentListType type : EnumSet.allOf(ContractInstrumentListType.class)) {
                type.setMessageSource(messageSource);
            }
        }
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getName() {
        return messageSource.getMessage(nameCode, null, LocaleContextHolder.getLocale());
    }

}
