package org.trescal.cwms.core.contract.dtos;

import java.time.LocalDate;

public class ContractSearchResultsDto {

    private Integer contractId;
    private String contractNumber;
    private String clientCompanyName;
    private String legalContractNumber;
    private LocalDate startDate;
    private LocalDate endDate;

    public ContractSearchResultsDto(Integer contractId, String contractNumber, String clientCompanyName,
                                    String legalContractNumber, LocalDate startDate, LocalDate endDate) {
        this.contractId = contractId;
        this.contractNumber = contractNumber;
        this.clientCompanyName = clientCompanyName;
        this.legalContractNumber = legalContractNumber;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Integer getContractId() {
        return contractId;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public String getClientCompanyName() {
        return clientCompanyName;
    }

    public String getLegalContractNumber() {
        return legalContractNumber;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

}
