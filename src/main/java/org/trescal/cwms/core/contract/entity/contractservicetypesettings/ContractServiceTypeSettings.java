package org.trescal.cwms.core.contract.entity.contractservicetypesettings;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

@Entity
@Table(name = "contractservicetypesettings", uniqueConstraints = @UniqueConstraint(name = "UK_contractservicetypesettings_contract_servicetype", columnNames = {
		"contractid", "servicetypeid" }))
public class ContractServiceTypeSettings {

	private Integer id;
	private Contract contract;
	private ServiceType serviceType;
	private Integer turnaround;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "contractid", nullable = false, foreignKey = @ForeignKey(name = "FK_contractservicetypesettings_contract"))
	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	@NotNull
	@ManyToOne
	@JoinColumn(name = "servicetypeid", nullable = false, foreignKey = @ForeignKey(name = "FK_contractservicetypesettings_servicetype"))
	public ServiceType getServiceType() {
		return serviceType;
	}

	public void setServiceType(ServiceType serviceType) {
		this.serviceType = serviceType;
	}

	@Column(name = "turnaround")
	public Integer getTurnaround() {
		return turnaround;
	}

	public void setTurnaround(Integer turnaround) {
		this.turnaround = turnaround;
	}
}