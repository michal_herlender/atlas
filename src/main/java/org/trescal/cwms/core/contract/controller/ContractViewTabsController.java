package org.trescal.cwms.core.contract.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.contract.dtos.ContractServiceTypeDto;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.db.ContractService;
import org.trescal.cwms.core.contract.entity.contractservicetypesettings.ContractServiceTypeSettings;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@IntranetController
@RequestMapping("contractviewtab")
public class ContractViewTabsController {

	@Autowired
	private ContractService contractService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private ServiceTypeService serviceTypeService;
	@Autowired
	private TranslationService translationService;

	@GetMapping("/subdivs/{contractid}")
	public String getSubdivsTab(@PathVariable("contractid") Integer contractId, Model model) {

		Contract contract = contractService.get(contractId);
		List<KeyValue<Integer, String>> unlinkedSubdivs = subdivService
				.getAllActiveCompanySubdivsKeyValue(contract.getClientCompany().getId(), false);
		List<KeyValue<Integer, String>> linkedSubdivs = contract.getSubdivs().stream()
				.map(s -> new KeyValue<>(s.getSubdivid(), s.getSubname())).collect(Collectors.toList());
		unlinkedSubdivs.removeAll(linkedSubdivs);

		model.addAttribute("listtype", contract.getInstrumentListType());
		model.addAttribute("linkedsubdivs", linkedSubdivs);
		model.addAttribute("unlinkedsubdivs", unlinkedSubdivs);

		return "trescal/core/contract/viewcontract/contractviewsubdivstab";
	}

	@GetMapping("/instruments/{contractid}")
	public String getInstrumentsTab(@PathVariable("contractid") Integer contractId, Model model) {
		Contract contract = contractService.get(contractId);
		model.addAttribute("listtype", contract.getInstrumentListType());
		model.addAttribute("companyId", contract.getClientCompany().getCoid());
		model.addAttribute("companyName", contract.getClientCompany().getConame());
		model.addAttribute("contractId", contractId);
		return "trescal/core/contract/viewcontract/contractviewinstrumentstab";
	}

	@GetMapping("/instructions/{contractid}")
	public String getInstructionsTab(@PathVariable("contractid") Integer contractId, Model model) {
		Contract contract = contractService.get(contractId);
		List<KeyValue<String, String>> instructionTypes = InstructionType.getActiveTypes().stream()
				.map(it -> new KeyValue<>(it.name(), it.getMessageCode()))
				.collect(Collectors.toList());
		model.addAttribute("instructions", contract.getInstructions());
		model.addAttribute("instructiontypes", instructionTypes);
		return "trescal/core/contract/viewcontract/contractviewinstructionstab";
	}

	@GetMapping("/technicalrequirements/{contractid}")
	public String getTechnicalRequirementsTab(@PathVariable("contractid") Integer contractId, Model model) {
		return "trescal/core/contract/viewcontract/contractviewdemandstab";
	}

	@GetMapping("/servicetypes/{contractid}")
	public String getValidServiceTypesTab(@PathVariable("contractid") Integer contractId, Model model) {
		Locale locale = LocaleContextHolder.getLocale();
		Contract contract = contractService.get(contractId);
		List<ContractServiceTypeSettings> serviceTypeSettings = contract.getServiceTypeSettings();
		List<ContractServiceTypeDto> serviceTypeDtos = serviceTypeService.getAll().stream()
				.sorted(Comparator.comparing(ServiceType::getOrder)).map(s -> {
					ContractServiceTypeDto dto = new ContractServiceTypeDto(s.getServiceTypeId(),
							translationService.getCorrectTranslation(s.getLongnameTranslation(), locale),
							translationService.getCorrectTranslation(s.getShortnameTranslation(), locale));
					Optional<ContractServiceTypeSettings> csts = serviceTypeSettings.stream()
							.filter(sts -> sts.getServiceType().equals(s)).findAny();
					if (csts.isPresent()) {
						dto.setServiceTypePermitted(true);
						dto.setTurnaround(csts.get().getTurnaround());
					} else {
						dto.setServiceTypePermitted(false);
					}
					return dto;
				}).collect(Collectors.toList());
		model.addAttribute("services", serviceTypeDtos);
		return "trescal/core/contract/viewcontract/contractviewservicestab";
	}
}