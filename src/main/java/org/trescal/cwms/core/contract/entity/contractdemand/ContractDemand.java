package org.trescal.cwms.core.contract.entity.contractdemand;

import java.util.BitSet;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.hibernateextension.BitSetJPAConverter;

@Entity
@Table(name = "contractdemand")
public class ContractDemand {

	private Integer id;
	private Contract contract;
	private BitSet demandSet;
	private InstrumentModelDomain domain;
	private Description subfamily;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "contractid", foreignKey = @ForeignKey(name = "FK_contractdemand_contract"))
	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	@Column(name = "demandset")
	@Convert(converter = BitSetJPAConverter.class)
	public BitSet getDemandSet() {
		return demandSet;
	}

	public void setDemandSet(BitSet demandSet) {
		this.demandSet = demandSet;
	}

	@ManyToOne
	@JoinColumn(name = "domainid", foreignKey = @ForeignKey(name = "FK_contractdemand_domain"))
	public InstrumentModelDomain getDomain() {
		return domain;
	}

	public void setDomain(InstrumentModelDomain domain) {
		this.domain = domain;
	}

	@ManyToOne
	@JoinColumn(name = "subfamilyid", foreignKey = @ForeignKey(name = "FK_contractdemand_subfamily"))
	public Description getSubfamily() {
		return subfamily;
	}

	public void setSubfamily(Description subfamily) {
		this.subfamily = subfamily;
	}
}