package org.trescal.cwms.core.contract.form;

import org.trescal.cwms.core.contract.dtos.ContractSearchResultsDto;
import org.trescal.cwms.core.tools.PagedResultSet;

public class ContractSearchForm {

    private Integer businessCompanyId;
    private Integer clientCompanyId;
    private Integer clientSubdivId;
    private Integer plantId;
    private String legalContractNumber;
    private Boolean excludeExpired;
    private Integer resultsPerPage;
    private Integer pageNo;
    private PagedResultSet<ContractSearchResultsDto> resultSet;

    public Integer getBusinessCompanyId() {
        return businessCompanyId;
    }

    public void setBusinessCompanyId(Integer businessCompanyId) {
        this.businessCompanyId = businessCompanyId;
    }

    public Integer getClientCompanyId() {
        return clientCompanyId;
    }

    public void setClientCompanyId(Integer clientCompanyId) {
        this.clientCompanyId = clientCompanyId;
    }

    public Integer getClientSubdivId() {
        return clientSubdivId;
    }

    public void setClientSubdivId(Integer clientSubdivId) {
        this.clientSubdivId = clientSubdivId;
    }

    public Integer getPlantId() {
        return plantId;
    }

    public void setPlantId(Integer plantId) {
        this.plantId = plantId;
    }

    public String getLegalContractNumber() {
        return legalContractNumber;
    }

    public void setLegalContractNumber(String legalContractNumber) {
        this.legalContractNumber = legalContractNumber;
    }

    public Boolean getExcludeExpired() {
        return excludeExpired;
    }

    public void setExcludeExpired(Boolean excludeExpired) {
        this.excludeExpired = excludeExpired;
    }

    public Integer getResultsPerPage() {
        return resultsPerPage;
    }

    public void setResultsPerPage(Integer resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public void setResultSet(PagedResultSet<ContractSearchResultsDto> resultSet) {
        this.resultSet = resultSet;
    }

    public PagedResultSet<ContractSearchResultsDto> getResultSet() {
        return resultSet;
    }
}
