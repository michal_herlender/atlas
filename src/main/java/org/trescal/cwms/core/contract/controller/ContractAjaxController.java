package org.trescal.cwms.core.contract.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.contract.dtos.*;
import org.trescal.cwms.core.contract.entity.contract.db.ContractService;
import org.trescal.cwms.core.contract.entity.contractdemand.ContractDemand;
import org.trescal.cwms.core.contract.entity.contractdemand.db.ContractDemandService;
import org.trescal.cwms.core.contract.entity.contractservicetypesettings.ContractServiceTypeSettings;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemIdDto;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.datatables.DatatableOutput;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.List;

@RestController
@RequestMapping("contract")
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY})
public class ContractAjaxController {

	@Autowired
	private ContractService contractService;
	@Autowired
	private ContractDemandService contractDemandService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private CalibrationTypeService calTypeService;

	@PostMapping("addsubdivs")
	public void addSubdivToContract(@RequestBody SelectedSubdivsDto input) {
		contractService.addSubdivs(input.getContractId(), input.getSubdivIds());
	}

	@PostMapping("removesubdiv")
	public void removeSubdivFromContract(@RequestParam("contractid") Integer contractId,
			@RequestParam("subdivid") Integer subdivId) {
		contractService.removeSubdiv(contractId, subdivId);
	}

	@PostMapping("addinstrument")
	public void addInstrumentToContract(@RequestParam("contractid") Integer contractId,
			@RequestParam("instrumentid") Integer instrumentId) {
		contractService.addInstrument(contractId, instrumentId);
	}

	@PostMapping("removeinstrument")
	public void removeInstrumentFromContract(@RequestParam("contractid") Integer contactId,
											 @RequestParam("instrumentid") Integer instrumentId) {
		contractService.removeInstrument(contactId, instrumentId);
	}

	@GetMapping("listinstruments")
	public DatatableOutput<ContractInstrumentListDto> postInstruments(ContractInstrumentListInput input) {
		return contractService.getInstrumentList(input);
	}

	@GetMapping("listunlinkedinstruments")
	public DatatableOutput<ContractInstrumentListDto> getUnlinkedInstruments(
		ContractInstrumentListInput input) {
		return contractService.getUnlinkedInstrumentList(input);
	}

	@GetMapping("listdemands")
	public DatatableOutput<ContractDemandDto> getDemands(ContractDemandInput input) {
		return contractDemandService.getDemands(input);
	}

	@PostMapping("adddemand")
	public boolean addContractDemand(@RequestParam(name = "contractId") Integer contractId,
									 @RequestParam(name = "domainId", required = false) Integer domainId,
									 @RequestParam(name = "subfamilyId", required = false) Integer subfamilyId,
									 @RequestParam(name = "cleaning") Boolean cleaning,
									 @RequestParam(name = "sealing") Boolean sealing,
									 @RequestParam(name = "engraving") Boolean engraving) {
		return contractDemandService.createContractDemand(contractId, domainId, subfamilyId, cleaning, sealing,
				engraving);
	}

	@PostMapping("addeditinstruction")
	public void addInstructionToContract(@RequestParam("instructionid") Integer instructionId,
			@RequestParam("contractid") Integer contractId,
			@RequestParam("instructiontype") InstructionType instructionType,
			@RequestParam("instruction") String instruction,
			@RequestParam("clientDeliveryNote") Boolean clientDeliveryNote,
			@RequestParam("supplierDeliveryNote") Boolean supplierDeliveryNote) {
		contractService.addEditInstruction(instructionId, contractId, instructionType, instruction,
				clientDeliveryNote, supplierDeliveryNote);
	}

	@PostMapping("deleteinstruction")
	public void deleteInstruction(@RequestParam("instructionid") Integer instructionId,
			@RequestParam("contractid") Integer contractId) {
		contractService.deleteInstruction(instructionId, contractId);
	}

	@GetMapping("{contractid}/instruction/{instructionid}")
	public ContractInstructionDto getContractInstruction(@PathVariable("contractid") Integer contractId,
			@PathVariable("instructionid") Integer instructionId) {
		return contractService.getInstruction(contractId, instructionId);
	}

	@PostMapping("deletedemand")
	public boolean deleteContractDemand(@RequestParam("demandId") Integer demandId) {
		try {
			ContractDemand demand = contractDemandService.get(demandId);
			contractDemandService.delete(demand);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	@GetMapping("listactivebposforcompany/{companyid}")
	public List<ContractActiveBpoDto> getPossibleBpos(@PathVariable("companyid") Integer clientCompanyId,
			@SessionAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyKeyValue) {
		return contractService.getPossibleBpos(clientCompanyId, companyKeyValue.getKey());
	}

	@PostMapping("updateservice")
	public void updateServices(@RequestBody ContractServiceTypeSettingsDto settingsDto) {
		if (settingsDto.getAdd())
			contractService.addService(settingsDto.getContractId(), settingsDto.getServiceTypeId(),
					settingsDto.getTurnaround());
		else
			contractService.removeService(settingsDto.getContractId(), settingsDto.getServiceTypeId());
	}

	@GetMapping("getsuitablecontracts")
	public List<MatchingContractDto> getSuitableContracts(@RequestParam("jobitemid") Integer jobItemId,
			@RequestParam("caltypeid") Integer calTypeId) {
		return contractService.findMatchingContractsForJobItem(jobItemId, calTypeId);
	}

	@PutMapping("getcontractinformation")
	public ContractInformationForJobItemDto getContractInformation(
			@RequestParam(name = "jobItemId") Integer jobItemId,
			@RequestParam(name = "contractId") Integer contractId,
			@RequestParam(name = "calTypeId") Integer calTypeId) {
		if (contractId == 0) {
			Integer turnaround = jobItemService.findJobItem(jobItemId).getJob().getDefaultTurn();
			return new ContractInformationForJobItemDto(turnaround);
		} else {
			int serviceTypeId = calTypeService.find(calTypeId).getServiceType().getServiceTypeId();
			
			ContractInformationForJobItemDto contractInfo = contractService
					.updateCostAndGetContractInformation(contractId, jobItemId, serviceTypeId);

			contractService.get(contractId).getServiceTypeSettings().stream()
				.filter(st -> st.getServiceType().getServiceTypeId() == serviceTypeId).findFirst()
                .map(ContractServiceTypeSettings::getTurnaround)
				.ifPresent(contractInfo::setTurnaround);

			return contractInfo;
		}
	}

	@GetMapping("joibItemIds/{jiid}")
	public Page<JobItemIdDto> getJobItemIdDto(@PathVariable(name = "jiid") String days) {
		return jobItemService.getJobItemIdDto(days);
	}
}