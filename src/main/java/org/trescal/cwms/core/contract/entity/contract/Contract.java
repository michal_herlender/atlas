package org.trescal.cwms.core.contract.entity.contract;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.paymentmode.PaymentMode;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.contract.entity.contractdemand.ContractDemand;
import org.trescal.cwms.core.contract.entity.contractservicetypesettings.ContractServiceTypeSettings;
import org.trescal.cwms.core.contract.enums.ContractInstrumentListType;
import org.trescal.cwms.core.contract.enums.InvoicingMethod;
import org.trescal.cwms.core.contract.enums.Software;
import org.trescal.cwms.core.contract.enums.TransferType;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.job.entity.bpo.BPO;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.files.ForeignDBField;
import org.trescal.cwms.files.contract.entity.ContractFile;

@Entity
@Table(name = "contract")
public class Contract extends Allocated<Company> {

	private Integer id;
	private InvoicingMethod invoicingMethod;
	private LocalDate startDate;
	private Integer duration;
	private LocalDate endDate;
	private IntervalUnit durationUnit;
	private Set<Subdiv> subdivs;
	private String contractNumber;
	private Contact trescalManager;
	private Quotation quotation;
	private ContractFile document;
	private Software software;
	private TransferType transferType;
	private Boolean dbManagement;
	private BigDecimal premium;
	private BigDecimal outstandingPremium;
	private Integer invoiceFrequency;
	private PaymentMode paymentMode;
	private Contact invoicingManager;
	private Company clientCompany;
	private Contact clientContact;
	private String legalContractNo;
	private String description;
	private SupportedCurrency currency;
	private Set<Instrument> instruments;
	private ContractInstrumentListType instrumentListType;
	private Set<Instruction> instructions;
	private List<ContractDemand> demands;
	private BPO bpo;
	private PaymentTerm paymentTerm;
	private List<ContractServiceTypeSettings> serviceTypeSettings;
	private Integer warrantyCalibration;
	private Integer warrantyRepair;

	@Column(name = "legalContractNo")
	public String getLegalContractNo() {
		return legalContractNo;
	}

	public void setLegalContractNo(String legalContractNo) {
		this.legalContractNo = legalContractNo;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "clientcontactid", nullable = false)
	public Contact getClientContact() {
		return clientContact;
	}

	public void setClientContact(Contact clientContact) {
		this.clientContact = clientContact;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "invoicingmethod")
	public InvoicingMethod getInvoicingMethod() {
		return invoicingMethod;
	}

	public void setInvoicingMethod(InvoicingMethod invoicingMethod) {
		this.invoicingMethod = invoicingMethod;
	}

	@NotNull
	@Column(name = "startdate", nullable = false)
	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	@NotNull
	@Column(name = "enddate", nullable = false)
	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	@NotNull
	@Column(name = "duration", nullable = false)
	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "durationunit", nullable = false)
	public IntervalUnit getDurationUnit() {
		return durationUnit;
	}

	public void setDurationUnit(IntervalUnit durationUnit) {
		this.durationUnit = durationUnit;
	}

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "subdiv_contract", joinColumns = @JoinColumn(name = "contractid", foreignKey = @ForeignKey(name = "FK_subdivcontractlinktable_contract")), inverseJoinColumns = @JoinColumn(name = "subdivid", foreignKey = @ForeignKey(name = "FK_subdivcontractlinktable_subdiv")))
	public Set<Subdiv> getSubdivs() {
		return subdivs;
	}

	public void setSubdivs(Set<Subdiv> subdivs) {
		this.subdivs = subdivs;
	}

	@NotNull
	@Column(name = "contractno", nullable = false)
	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "trescalmanagerid", nullable = false)
	public Contact getTrescalManager() {
		return trescalManager;
	}

	public void setTrescalManager(Contact trescalManager) {
		this.trescalManager = trescalManager;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quotationId")
	public Quotation getQuotation() {
		return quotation;
	}

	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "software")
	public Software getSoftware() {
		return software;
	}

	public void setSoftware(Software software) {
		this.software = software;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "transfertype")
	public TransferType getTransferType() {
		return transferType;
	}

	public void setTransferType(TransferType transferType) {
		this.transferType = transferType;
	}

	@Column(name = "dbmanagement")
	public Boolean getDbManagement() {
		return dbManagement;
	}

	public void setDbManagement(Boolean dbManagement) {
		this.dbManagement = dbManagement;
	}

	@Column(name = "premium")
	public BigDecimal getPremium() {
		return premium;
	}

	public void setPremium(BigDecimal premium) {
		this.premium = premium;
	}

	@Column(name = "outstandingpremium")
	public BigDecimal getOutstandingPremium() {
		return outstandingPremium;
	}

	public void setOutstandingPremium(BigDecimal outstandingPremium) {
		this.outstandingPremium = outstandingPremium;
	}

	@Column(name = "invoicefrequency")
	public Integer getInvoiceFrequency() {
		return invoiceFrequency;
	}

	public void setInvoiceFrequency(Integer invoiceFrequency) {
		this.invoiceFrequency = invoiceFrequency;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "paymentmodeid")
	public PaymentMode getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(PaymentMode paymentMode) {
		this.paymentMode = paymentMode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoicingmanagerid")
	public Contact getInvoicingManager() {
		return invoicingManager;
	}

	public void setInvoicingManager(Contact invoicingManager) {
		this.invoicingManager = invoicingManager;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "clientcompanyid", nullable = false)
	public Company getClientCompany() {
		return clientCompany;
	}

	public void setClientCompany(Company clientCompany) {
		this.clientCompany = clientCompany;
	}

	@Transient
	@ForeignDBField(fkField = "id")
	public ContractFile getDocument() {
		return document;
	}

	public void setDocument(ContractFile document) {
		this.document = document;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currencyid")
	public SupportedCurrency getCurrency() {
		return currency;
	}

	public void setCurrency(SupportedCurrency currency) {
		this.currency = currency;
	}

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "instrument_contract", joinColumns = @JoinColumn(name = "contractid"), inverseJoinColumns = @JoinColumn(name = "instrumentid"))
	public Set<Instrument> getInstruments() {
		return instruments;
	}

	public void setInstruments(Set<Instrument> instruments) {
		this.instruments = instruments;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "instrumentlisttype")
	public ContractInstrumentListType getInstrumentListType() {
		return instrumentListType;
	}

	public void setInstrumentListType(ContractInstrumentListType instrumentListType) {
		this.instrumentListType = instrumentListType;
	}

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<Instruction> getInstructions() {
		return instructions;
	}

	public void setInstructions(Set<Instruction> instructions) {
		this.instructions = instructions;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "paymentterms", length = 10)
	public PaymentTerm getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(PaymentTerm paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	@OneToMany(mappedBy = "contract", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ContractDemand> getDemands() {
		return demands;
	}

	public void setDemands(List<ContractDemand> demands) {
		this.demands = demands;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bpoid")
	public BPO getBpo() {
		return bpo;
	}

	public void setBpo(BPO bpo) {
		this.bpo = bpo;
	}

	@OneToMany(mappedBy = "contract", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ContractServiceTypeSettings> getServiceTypeSettings() {
		return serviceTypeSettings;
	}

	public void setServiceTypeSettings(List<ContractServiceTypeSettings> serviceTypeSettings) {
		this.serviceTypeSettings = serviceTypeSettings;
	}
	

	@Column(name = "warrantyCalibration")
	public Integer getWarrantyCalibration() {
		return warrantyCalibration;
	}

	public void setWarrantyCalibration(Integer warrantyCalibration) {
		this.warrantyCalibration = warrantyCalibration;
	}
	
	@Column(name = "warrantyRepaire")
	public Integer getWarrantyRepair() {
		return warrantyRepair;
	}

	public void setWarrantyRepair(Integer warrantyRepair) {
		this.warrantyRepair = warrantyRepair;
	}
	
	
}