package org.trescal.cwms.core.contract.dtos;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ContractViewDto {

    private Integer contractId;
    private String contractNumber;
    private String clientCompanyName;
    private Integer clientCompanyId;
    private String invoicingMethodName;
    private String trescalManagerName;
    private String clientContactName;
    private String quotationNumber;
    private Integer quotationId;
    private String softwareName;
    private String transferTypeName;
    private String dbManagement;
    private String description;
    private String legalContractNo;
    private String premiumWithCurrency;
    private String outstandingPremiumWithCurrency;
    private String invoiceFrequency;
    private String paymentModeDescription;
    private String invoicingManagerName;
    private Boolean hasDocument;
    private String instrumentListType;
    private String paymentTerm;
    private String bpoNumber;
    private Integer bpoId;
    private Integer warrantyCalibration;
    private Integer warrantyRepair;
    private LocalDate startDate;
    private LocalDate endDate;
}
