package org.trescal.cwms.core.contract.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.db.ContractService;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller
@IntranetController
@RequestMapping(path = "/contractinstrumentbulkupload.htm")
public class ContractInstrumentBulkUploadController {

	@Autowired
	private ContractService contractService;

	@GetMapping
	public String getForm(@RequestParam("contractid") Integer contractId, Model model) {
		Contract contract = contractService.get(contractId);
		model.addAttribute("contractNumber", contract.getContractNumber());
		model.addAttribute("contractId", contract.getId());
		model.addAttribute("clientName", contract.getClientCompany().getConame());
		return "trescal/core/contract/contractinstrumentbulkupload";
	}

	@PostMapping
	public String linkInstruments(@RequestParam("contractid") Integer contractId,
			@RequestParam("importfile") MultipartFile importFile) throws IOException {
		contractService.addInstruments(contractId, importFile);
		return "redirect:contractview.htm?id=" + contractId;
	}
}