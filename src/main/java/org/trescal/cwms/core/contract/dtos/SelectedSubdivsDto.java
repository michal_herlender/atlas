package org.trescal.cwms.core.contract.dtos;

import java.util.List;

public class SelectedSubdivsDto {
    private Integer contractId;
    private List<Integer> subdivIds;

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public List<Integer> getSubdivIds() {
        return subdivIds;
    }

    public void setSubdivIds(List<Integer> subdivIds) {
        this.subdivIds = subdivIds;
    }
}
