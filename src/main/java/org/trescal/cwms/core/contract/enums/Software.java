package org.trescal.cwms.core.contract.enums;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public enum Software {

	NONE("contract.none"),
    ADVESO("contract.adveso"),
    THEMISONLINE("contract.themis"),
    METRA("contract.metra"),
    ATLAS("contract.atlas"),
    OTHER("contract.other");

    private final String nameCode;
    private MessageSource messageSource;

    Software(String nameCode){
        this.nameCode = nameCode;
    }

    @Component
    public static class SoftwareMessageSourceInjector
    {
        @Autowired
        private MessageSource messageSource;

        @PostConstruct
        public void postConstruct() {
            for (Software type : EnumSet.allOf(Software.class)) {
                type.setMessageSource(messageSource);
            }
        }
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getName() {
        return messageSource.getMessage(nameCode, null, LocaleContextHolder.getLocale());
    }
}
