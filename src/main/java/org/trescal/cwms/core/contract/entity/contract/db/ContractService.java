package org.trescal.cwms.core.contract.entity.contract.db;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.contract.dtos.ContractActiveBpoDto;
import org.trescal.cwms.core.contract.dtos.ContractInformationForJobItemDto;
import org.trescal.cwms.core.contract.dtos.ContractInstructionDto;
import org.trescal.cwms.core.contract.dtos.ContractInstrumentListDto;
import org.trescal.cwms.core.contract.dtos.ContractInstrumentListInput;
import org.trescal.cwms.core.contract.dtos.ContractSearchResultsDto;
import org.trescal.cwms.core.contract.dtos.ContractViewDto;
import org.trescal.cwms.core.contract.dtos.MatchingContractDto;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.form.ContractAddEditForm;
import org.trescal.cwms.core.contract.form.ContractSearchForm;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.datatables.DatatableOutput;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

public interface ContractService extends BaseService<Contract, Integer> {

	ContractAddEditForm getAddEditForm(Integer contractId);

	Contract saveOrUpdateFromAddEditForm(Contract contract, ContractAddEditForm form);

	ContractViewDto getContractViewDto(Integer contractId);

	List<KeyValueIntegerString> getKeyValueList(Collection<Integer> contractIds);

	PagedResultSet<ContractSearchResultsDto> searchContracts(ContractSearchForm form);

	DatatableOutput<ContractInstrumentListDto> getInstrumentList(ContractInstrumentListInput input);

	DatatableOutput<ContractInstrumentListDto> getUnlinkedInstrumentList(ContractInstrumentListInput input);

	void addSubdivs(Integer contractId, List<Integer> subdivIds);

	void removeSubdiv(Integer contractId, Integer subdivId);

	void addInstrument(Integer contractId, Integer instrumentId);

	void removeInstrument(Integer contractId, Integer instrumentId);

	void addEditInstruction(Integer instructionId, Integer contractId, InstructionType instructionType,
			String instruction, Boolean includeOnClientDeliveryNote, Boolean includeOnSupplierDeliveryNote);

	void deleteInstruction(Integer instructionId, Integer contractId);

	ContractInstructionDto getInstruction(Integer contractId, Integer instructionId);
	

	List<ContractActiveBpoDto> getPossibleBpos(Integer clientCompanyId, Integer businessCompanyId);

	void addService(Integer contractId, Integer serviceTypeId, Integer turnaround);

	void removeService(Integer contractId, Integer serviceTypeId);

	/*
	 * Following methods find the available live contracts for an instrument,
	 * contracts that match using more specific criteria are considered a better
	 * match So a contract that matches at instrument level is a better match than a
	 * contract that matches at subdiv level which is a better match than a contract
	 * that matches at company level. If multiple contracts match at a particular
	 * level then the most recent contract is considered the best match and will be
	 * the default in the contract selection in the job item contract review page.
	 */

	List<MatchingContractDto> findMatchingContractsForJobItem(JobItem jobItem, ServiceType alternativeServiceType);

	List<MatchingContractDto> findMatchingContractsForJobItem(Integer jobItemId, Integer calTypeId);

	void addInstruments(Integer contractId, MultipartFile importFile) throws IOException;

	ContractInformationForJobItemDto updateCostAndGetContractInformation(Integer contractId, Integer jobItemId, Integer serviceTypeId);

	void updateJobItemsContract(List<JobItem> jobItems, Contract contract);

	void linkJobItemToContract(JobItem jobItem, Contract contract);
	
	List<Contract> findContractsByClientCompany(Integer coid);
	
	Contract findContractByQuotation(Quotation quotation);
}