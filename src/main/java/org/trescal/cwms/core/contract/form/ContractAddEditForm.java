package org.trescal.cwms.core.contract.form;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.contract.enums.*;
import org.trescal.cwms.core.system.enums.IntervalUnit;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;

public class ContractAddEditForm {

	private Integer contractId;
	@NotNull(message = "{error.contract.noclient}")
	private Integer clientCompanyId;
	private InvoicingMethod invoicingMethod;
	@NotNull(message = "{error.contract.noduration}")
	private Integer duration;
	private IntervalUnit durationUnit;
	@NotNull(message = "{error.value.notselected}")
	private Integer clientContactId;
	@NotNull(message = "{error.contract.nostartdate}")
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate startDate;
	private Integer trescalManagerId;
	private Integer quoteId;
	private Software software;
	private TransferType transferType;
	private Boolean dbManagement;
	private BigDecimal premium;
	private BigDecimal outstandingPremium;
	private Integer invoiceFrequency;
	private Integer paymentModeId;
	private Integer invoicingManagerId;
	private String legalContractNo;
	private String description;
	private Integer currencyId;
	private MultipartFile file;
	private ContractInstrumentListType instrumentListType;

	private Boolean editMode;
	private String contractNumber;
	private String clientCompanyName;
	private String invoicingMethodName;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate endDate;
	private String currencySymbol;
	private String quoteNumber;
	private PaymentTerm paymentTerm;
	private String bpoNumber;
	private Integer bpoId;
	private Integer warrantyCalibration;
	private Integer warrantyRepair;

	public void setPaymentTerm(PaymentTerm paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public Integer getContractId() {
		return contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}

	public Integer getClientCompanyId() {
		return clientCompanyId;
	}

	public void setClientCompanyId(Integer clientCompanyId) {
		this.clientCompanyId = clientCompanyId;
	}

	public InvoicingMethod getInvoicingMethod() {
		return invoicingMethod;
	}

	public void setInvoicingMethod(InvoicingMethod invoicingMethod) {
		this.invoicingMethod = invoicingMethod;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public IntervalUnit getDurationUnit() {
		return durationUnit;
	}

	public void setDurationUnit(IntervalUnit durationUnit) {
		this.durationUnit = durationUnit;
	}

	public Integer getClientContactId() {
		return clientContactId;
	}

	public void setClientContactId(Integer clientContactId) {
		this.clientContactId = clientContactId;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public Integer getTrescalManagerId() {
		return trescalManagerId;
	}

	public void setTrescalManagerId(Integer trescalManagerId) {
		this.trescalManagerId = trescalManagerId;
	}

	public Integer getQuoteId() {
		return quoteId;
	}

	public void setQuoteId(Integer quoteId) {
		this.quoteId = quoteId;
	}

	public Software getSoftware() {
		return software;
	}

	public void setSoftware(Software software) {
		this.software = software;
	}

	public TransferType getTransferType() {
		return transferType;
	}

	public void setTransferType(TransferType transferType) {
		this.transferType = transferType;
	}

	public Boolean getDbManagement() {
		return dbManagement;
	}

	public void setDbManagement(Boolean dbManagement) {
		this.dbManagement = dbManagement;
	}

	public BigDecimal getPremium() {
		return premium;
	}

	public void setPremium(BigDecimal premium) {
		this.premium = premium;
	}

	public BigDecimal getOutstandingPremium() {
		return outstandingPremium;
	}

	public void setOutstandingPremium(BigDecimal outstandingPremium) {
		this.outstandingPremium = outstandingPremium;
	}

	public Integer getInvoiceFrequency() {
		return invoiceFrequency;
	}

	public void setInvoiceFrequency(Integer invoiceFrequency) {
		this.invoiceFrequency = invoiceFrequency;
	}

	public Integer getPaymentModeId() {
		return paymentModeId;
	}

	public void setPaymentModeId(Integer paymentModeId) {
		this.paymentModeId = paymentModeId;
	}

	public Integer getInvoicingManagerId() {
		return invoicingManagerId;
	}

	public void setInvoicingManagerId(Integer invoicingManagerId) {
		this.invoicingManagerId = invoicingManagerId;
	}

	public String getLegalContractNo() {
		return legalContractNo;
	}

	public void setLegalContractNo(String legalContractNo) {
		this.legalContractNo = legalContractNo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getInvoicingMethodName() {
		return invoicingMethodName;
	}

	public void setInvoicingMethodName(String invoicingMethodName) {
		this.invoicingMethodName = invoicingMethodName;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}

	public Integer getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public String getQuoteNumber() {
		return quoteNumber;
	}

	public void setQuoteNumber(String quoteNumber) {
		this.quoteNumber = quoteNumber;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public ContractInstrumentListType getInstrumentListType() {
		return instrumentListType;
	}

	public void setInstrumentListType(ContractInstrumentListType instrumentListType) {
		this.instrumentListType = instrumentListType;
	}

	public PaymentTerm getPaymentTerm() {
		return paymentTerm;
	}

	public String getBpoNumber() {
		return bpoNumber;
	}

	public void setBpoNumber(String bpoNumber) {
		this.bpoNumber = bpoNumber;
	}

	public Integer getBpoId() {
		return bpoId;
	}

	public void setBpoId(Integer bpoId) {
		this.bpoId = bpoId;
	}

	public Integer getWarrantyCalibration() {
		return warrantyCalibration;
	}

	public void setWarrantyCalibration(Integer warrantyCalibration) {
		this.warrantyCalibration = warrantyCalibration;
	}

	public Integer getWarrantyRepair() {
		return warrantyRepair;
	}

	public void setWarrantyRepair(Integer warrantyRepair) {
		this.warrantyRepair = warrantyRepair;
	}
	
	
}
