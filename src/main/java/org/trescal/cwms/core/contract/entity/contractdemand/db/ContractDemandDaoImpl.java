package org.trescal.cwms.core.contract.entity.contractdemand.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.audit.entity.db.CriteriaQueryGenerator;
import org.trescal.cwms.core.contract.dtos.ContractDemandDto;
import org.trescal.cwms.core.contract.dtos.ContractDemandInput;
import org.trescal.cwms.core.contract.entity.contractdemand.ContractDemand;
import org.trescal.cwms.core.contract.entity.contractdemand.ContractDemand_;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.datatables.DatatableOutput;
import org.trescal.cwms.datatables.DatatableSortDirection;

@Repository
public class ContractDemandDaoImpl extends BaseDaoImpl<ContractDemand, Integer> implements ContractDemandDao {

	@Override
	protected Class<ContractDemand> getEntity() {
		return ContractDemand.class;
	}

	@Override
	public ContractDemand findBestMatch(Integer contractId, Integer plantId) {
		return getFirstResult(cb -> {
			CriteriaQuery<ContractDemand> cq = cb.createQuery(ContractDemand.class);
			Root<ContractDemand> contractDemand = cq.from(ContractDemand.class);
			Root<Instrument> instrument = cq.from(Instrument.class);
			Join<Instrument, InstrumentModel> model = instrument.join(Instrument_.model);
			Join<InstrumentModel, Description> subfamily = model.join(InstrumentModel_.description);
			Join<Description, InstrumentModelFamily> family = subfamily.join(Description_.family);
			Subquery<Integer> subfamilyDemandSq = cq.subquery(Integer.class);
			Root<ContractDemand> subfamilyDemand = subfamilyDemandSq.from(ContractDemand.class);
			subfamilyDemandSq.where(cb.and(cb.equal(subfamilyDemand.get(ContractDemand_.contract), contractId),
					cb.equal(subfamilyDemand.get(ContractDemand_.subfamily), subfamily)));
			subfamilyDemandSq.select(subfamilyDemand.get(ContractDemand_.id));
			Subquery<Integer> domainDemandSq = cq.subquery(Integer.class);
			Root<ContractDemand> domainDemand = domainDemandSq.from(ContractDemand.class);
			domainDemandSq.where(cb.and(cb.equal(domainDemand.get(ContractDemand_.contract), contractId),
					cb.equal(domainDemand.get(ContractDemand_.domain), family.get(InstrumentModelFamily_.domain))));
			domainDemandSq.select(domainDemand.get(ContractDemand_.id));
			cq.where(cb.and(cb.equal(instrument.get(Instrument_.plantid), plantId),
					cb.equal(contractDemand.get(ContractDemand_.id),
							cb.coalesce(subfamilyDemandSq.getSelection(), domainDemandSq.getSelection()))));
			cq.select(contractDemand);
			return cq;
		}).orElse(null);
	}

	private CriteriaQueryGenerator<ContractDemandDto>  getFilterFunction(
			ContractDemandInput input, Locale locale) {
		return cb -> cq -> {
			Root<ContractDemand> contractDemand = cq.from(ContractDemand.class);
			Join<ContractDemand, InstrumentModelDomain> domain = contractDemand.join(ContractDemand_.domain,
					JoinType.LEFT);
			Join<InstrumentModelDomain, Translation> domainTranslation = domain.join(InstrumentModelDomain_.translation,
					JoinType.LEFT);
			Join<ContractDemand, Description> subfamily = contractDemand.join(ContractDemand_.subfamily, JoinType.LEFT);
			Join<Description, Translation> subfamilyTranslation = subfamily.join(Description_.translations,
					JoinType.LEFT);
			subfamilyTranslation.on(cb.equal(subfamilyTranslation.get(Translation_.locale), locale));
			domainTranslation.on(cb.equal(domainTranslation.get(Translation_.locale), locale));
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(contractDemand.get(ContractDemand_.contract), input.getContractId()));
			String term;
			if (!(term = input.getSearch().getValue()).isEmpty()) {
				Predicate searchPredicate = cb.disjunction();
				searchPredicate.getExpressions()
						.add(cb.like(domainTranslation.get(Translation_.translation), '%' + term + '%'));
				searchPredicate.getExpressions()
						.add(cb.like(subfamilyTranslation.get(Translation_.translation), '%' + term + '%'));
				clauses.getExpressions().add(searchPredicate);
			}
			cq.where(clauses);
			String sortField = input.getColumns().get(input.getOrder().get(0).getColumn()).getData();
			Function<Path<?>, Order> orderFunction = input.getOrder().get(0).getDir()
					.equals(DatatableSortDirection.desc) ? (cb::desc) : (cb::asc);
			List<Order> order = new ArrayList<>();
			switch (sortField) {
			case "domain":
				order.add(orderFunction.apply(domainTranslation.get(Translation_.translation)));
			case "subfamily":
				order.add(orderFunction.apply(subfamilyTranslation.get(Translation_.translation)));
				break;
			default:
			}
			Selection<ContractDemandDto> selection = cb.construct(ContractDemandDto.class,
					contractDemand.get(ContractDemand_.id), domainTranslation.get(Translation_.translation),
					subfamilyTranslation.get(Translation_.translation), contractDemand.get(ContractDemand_.demandSet));
			return Triple.of(contractDemand, selection, order);
		};
	}

	private Function<CriteriaBuilder, Function<CriteriaQuery<Long>, Expression<?>>> getUnfilteredTotalFunction(
			ContractDemandInput input) {
		return cb -> cq -> {
			Root<ContractDemand> contractDemand = cq.from(ContractDemand.class);
			cq.where(cb.equal(contractDemand.get(ContractDemand_.contract), input.getContractId()));
			cq.select(cb.count(contractDemand));
			return contractDemand;
		};
	}

	@Override
	public DatatableOutput<ContractDemandDto> getDemands(ContractDemandInput input, Locale locale) {
		DatatableOutput<ContractDemandDto> output = new DatatableOutput<>();
		output.setDraw(input.getDraw());
		completeDataTablesOutput(output, input, ContractDemandDto.class, getFilterFunction(input, locale),
				getUnfilteredTotalFunction(input));
		return output;
	}
}