package org.trescal.cwms.core.contract.dtos;

public class ContractInstrumentListDto {

    private Integer plantId;
    private String instrumentName;
    private String serialNo;
    private String subdivName;

    public ContractInstrumentListDto(Integer plantId, String instrumentName, String serialNo, String subdivName) {
        this.plantId = plantId;
        this.instrumentName = instrumentName;
        this.serialNo = serialNo;
        this.subdivName = subdivName;
    }

    public Integer getPlantId() {
        return plantId;
    }

    public void setPlantId(Integer plantId) {
        this.plantId = plantId;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getSubdivName() {
        return subdivName;
    }

    public void setSubdivName(String subdivName) {
        this.subdivName = subdivName;
    }
}
