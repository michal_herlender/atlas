package org.trescal.cwms.core.contract.dtos;

import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemand;

public class ContractDemandDto {

	private Integer id;
	private String domain;
	private String subfamily;
	private Map<AdditionalDemand, Boolean> demands;

	public ContractDemandDto(Integer id, String domain, String subfamily, BitSet demandsSet) {
		super();
		this.id = id;
		this.domain = domain;
		this.subfamily = subfamily;
		demands = new HashMap<>();
		for (AdditionalDemand demand : Arrays.asList(AdditionalDemand.values()))
			demands.put(demand, demandsSet.get(demand.ordinal()));
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getSubfamily() {
		return subfamily;
	}

	public void setSubfamily(String subfamily) {
		this.subfamily = subfamily;
	}

	public Map<AdditionalDemand, Boolean> getDemands() {
		return demands;
	}

	public void setDemands(Map<AdditionalDemand, Boolean> demands) {
		this.demands = demands;
	}
}