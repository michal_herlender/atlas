package org.trescal.cwms.core.contract.dtos;

import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ContractInstructionDto {

    private String instructionText;
    private InstructionType instructionType;
    private Boolean clientDeliveryNote;
    private Boolean supplierDeliveryNote;

    public ContractInstructionDto(String instructionText, InstructionType instructionType,
    		Boolean clientDeliveryNote, Boolean supplierDeliveryNote) {
        this.instructionText = instructionText;
        this.instructionType = instructionType;
        this.clientDeliveryNote = clientDeliveryNote;
        this.supplierDeliveryNote = supplierDeliveryNote;
    }
}
