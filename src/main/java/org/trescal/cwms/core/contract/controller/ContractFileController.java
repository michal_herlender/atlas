package org.trescal.cwms.core.contract.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.db.ContractService;
import org.trescal.cwms.core.exception.controller.FileController;
import org.trescal.cwms.files.contract.db.ContractFileService;

@Controller @FileController
@RequestMapping("/contractfile")
public class ContractFileController {

    private ContractService contractService;
    private ContractFileService contractFileService;

    public ContractFileController(ContractService contractService, ContractFileService contractFileService) {
        this.contractFileService = contractFileService;
        this.contractService = contractService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<byte[]> getFile(@PathVariable("id") Integer contractId) {
        Contract contract = contractService.get(contractId);
        return contractFileService.downloadFile(contract);
    }

}
