package org.trescal.cwms.core.contract.enums;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public enum InvoicingMethod {

    PERIODIC_FIX("contract.periodicinvoicing"),
    PERIODIC_VARIABLE("contract.periodicinvoicing"),
    INDIVIDUAL("contract.individualinvoicing");
	
    private final String nameCode;
    private MessageSource messageSource;

    InvoicingMethod(String nameCode){
        this.nameCode = nameCode;
    }

    @Component
    public static class ContractTypeMessageSourceInjector
    {
        @Autowired
        private MessageSource messageSource;

        @PostConstruct
        public void postConstruct() {
            for (InvoicingMethod type : EnumSet.allOf(InvoicingMethod.class)) {
                type.setMessageSource(messageSource);
            }
        }
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getName() {
        return messageSource.getMessage(nameCode, null, LocaleContextHolder.getLocale());
    }

}