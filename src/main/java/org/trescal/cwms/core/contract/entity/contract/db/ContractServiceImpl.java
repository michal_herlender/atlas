package org.trescal.cwms.core.contract.entity.contract.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedCaseInsensitiveMap;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.paymentmode.db.PaymentModeService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.contract.dtos.*;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contractservicetypesettings.ContractServiceTypeSettings;
import org.trescal.cwms.core.contract.enums.ContractInstrumentListType;
import org.trescal.cwms.core.contract.form.ContractAddEditForm;
import org.trescal.cwms.core.contract.form.ContractSearchForm;
import org.trescal.cwms.core.documents.excel.utils.ExcelFileReaderUtil;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.jobs.contractreview.entity.costs.calcost.db.ContractReviewCalibrationCostService;
import org.trescal.cwms.core.jobs.job.dto.BPOUsageDTO;
import org.trescal.cwms.core.jobs.job.dto.BpoDTO;
import org.trescal.cwms.core.jobs.job.entity.bpo.db.BPOService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitempo.JobItemPO;
import org.trescal.cwms.core.pricing.entity.costs.CostSource;
import org.trescal.cwms.core.quotation.dto.QuotationItemCostDTO;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.instruction.Instruction;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.translation.TranslationService;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.datatables.DatatableOutput;
import org.trescal.cwms.files.contract.db.ContractFileService;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ContractServiceImpl extends BaseServiceImpl<Contract, Integer> implements ContractService {

    @Autowired
    private ContractDao dao;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private ContactService contactService;
    @Autowired
    private ContractReviewCalibrationCostService contractReviewCalibrationCostService;
    @Autowired
    private PaymentModeService paymentModeService;
    @Autowired
    private TranslationService translationService;
    @Autowired
    private SupportedCurrencyService supportedCurrencyService;
    @Autowired
    private QuotationItemService quotationItemService;
    @Autowired
    private QuotationService quotationService;
    @Autowired
    private ContractFileService contractFileService;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private InstrumService instrumentService;
    @Autowired
    private BPOService bpoService;
    @Autowired
    private ServiceTypeService serviceTypeService;
    @Autowired
    private JobItemService jobItemService;
    @Autowired
    private CalibrationTypeService calibrationTypeService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private ExcelFileReaderUtil excelFileReaderUtil;

    @Override
    protected BaseDao<Contract, Integer> getBaseDao() {
        return dao;
    }

    @Override
    public ContractAddEditForm getAddEditForm(Integer contractId) {
        ContractAddEditForm form = new ContractAddEditForm();
        Contract contract = this.get(contractId);
        form.setClientCompanyId(contract.getClientCompany().getId());
        form.setClientContactId(contract.getClientContact().getId());
        form.setContractId(contract.getId());
        form.setInvoicingMethod(contract.getInvoicingMethod());
        form.setDbManagement(contract.getDbManagement());
        form.setDuration(contract.getDuration());
        form.setDurationUnit(contract.getDurationUnit());
        form.setInvoiceFrequency(contract.getInvoiceFrequency());
        form.setInvoicingManagerId(contract.getInvoicingManager() != null ? contract.getInvoicingManager().getId() : 0);
        form.setLegalContractNo(contract.getLegalContractNo());
        form.setOutstandingPremium(contract.getOutstandingPremium());
        form.setPaymentModeId(contract.getPaymentMode() != null ? contract.getPaymentMode().getId() : 0);
        form.setPaymentTerm(contract.getPaymentTerm());
        form.setPremium(contract.getPremium());
        form.setQuoteId(contract.getQuotation() != null ? contract.getQuotation().getId() : 0);
        form.setQuoteNumber(contract.getQuotation() != null ? contract.getQuotation().getQno() : "");
        form.setSoftware(contract.getSoftware());
        form.setStartDate(contract.getStartDate());
        form.setTransferType(contract.getTransferType());
        form.setTrescalManagerId(contract.getTrescalManager() != null ? contract.getTrescalManager().getId() : 0);
        form.setDescription(contract.getDescription());
        form.setCurrencyId(contract.getCurrency().getCurrencyId());
        form.setEditMode(true);
        form.setClientCompanyName(contract.getClientCompany().getConame());
        form.setContractNumber(contract.getContractNumber());
        form.setInvoicingMethodName(contract.getInvoicingMethod().getName());
        form.setEndDate(contract.getEndDate());
        form.setCurrencySymbol(contract.getCurrency().getCurrencyERSymbol());
        form.setInstrumentListType(contract.getInstrumentListType());
        form.setBpoId(contract.getBpo() != null ? contract.getBpo().getPoId() : 0);
        form.setBpoNumber(contract.getBpo() != null ? contract.getBpo().getPoNumber() : "");
        form.setWarrantyCalibration(contract.getWarrantyCalibration());
        form.setWarrantyRepair(contract.getWarrantyRepair());
        return form;
    }

    @Override
    public Contract saveOrUpdateFromAddEditForm(Contract contract, ContractAddEditForm form) {
        contract.setStartDate(form.getStartDate());
        contract.setInvoicingMethod(form.getInvoicingMethod());
        contract.setDuration(form.getDuration());
        contract.setDurationUnit(form.getDurationUnit());
        contract.setClientCompany(companyService.get(form.getClientCompanyId()));
        contract.setClientContact(contactService.get(form.getClientContactId()));
        contract.setDbManagement(form.getDbManagement());
        contract.setDuration(form.getDuration());
        contract.setInvoiceFrequency(form.getInvoiceFrequency());
        contract.setInvoicingManager(contactService.get(form.getInvoicingManagerId()));
        contract.setOutstandingPremium(form.getOutstandingPremium());
        contract.setPaymentMode(paymentModeService.get(form.getPaymentModeId()));
        contract.setPaymentTerm(form.getPaymentTerm());
        contract.setPremium(form.getPremium() != null ? form.getPremium() : BigDecimal.ZERO);
        contract.setSoftware(form.getSoftware());
        contract.setTransferType(form.getTransferType());
        contract.setTrescalManager(contactService.get(form.getTrescalManagerId()));
        contract.setLegalContractNo(form.getLegalContractNo());
        contract.setDescription(form.getDescription());
        contract.setEndDate(
                DateTools.datePlusInterval(form.getStartDate(), form.getDuration(), form.getDurationUnit()));
        contract.setCurrency(supportedCurrencyService.findSupportedCurrency(form.getCurrencyId()));
        contract.setInstrumentListType(form.getInstrumentListType());

        if (form.getWarrantyCalibration() != null) {
            contract.setWarrantyCalibration(form.getWarrantyCalibration());
        }
        if (form.getWarrantyRepair() != null) {
            contract.setWarrantyRepair(form.getWarrantyRepair());
        }
        if (form.getQuoteId() != null && form.getQuoteId() > 0)
            contract.setQuotation(quotationService.get(form.getQuoteId()));
        else
            contract.setQuotation(null);
        if (form.getBpoId() != null && form.getBpoId() > 0)
            contract.setBpo(bpoService.get(form.getBpoId()));
        else
            contract.setBpo(null);
        if (contract.getId() == null)
            save(contract);
        else
            contract = merge(contract);

        try {
            contractFileService.addOrUpdateDocument(form.getFile().getBytes(), contract);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return contract;
    }

    @Override
    public ContractViewDto getContractViewDto(Integer contractId) {
        ContractViewDto dto = new ContractViewDto();
        Contract contract = get(contractId);
        Locale locale = LocaleContextHolder.getLocale();
        String currencySymbol = contract.getCurrency().getCurrencyERSymbol();
        dto.setStartDate(contract.getStartDate());
        dto.setEndDate(contract.getEndDate());
        dto.setContractId(contract.getId());
        dto.setClientCompanyName(contract.getClientCompany().getConame());
        dto.setClientContactName(contract.getClientContact().getName());
        dto.setContractNumber(contract.getContractNumber());
        dto.setDbManagement(contract.getDbManagement() != null && contract.getDbManagement() ? "Yes" : "No");
        dto.setDescription(contract.getDescription());
        dto.setInvoiceFrequency(
                contract.getInvoiceFrequency() != null
                        ? contract.getInvoiceFrequency().toString() + " "
                        + IntervalUnit.MONTH.getName(contract.getInvoiceFrequency())
                        : "");
        dto.setInvoicingManagerName(contract.getInvoicingManager().getName());
        dto.setLegalContractNo(contract.getLegalContractNo());
        dto.setOutstandingPremiumWithCurrency(Optional.ofNullable(contract.getOutstandingPremium())
                .orElse(BigDecimal.ZERO).setScale(2, RoundingMode.HALF_UP).toPlainString() + " " + currencySymbol);
        dto.setPaymentModeDescription(contract.getPaymentMode() != null
                ? translationService.getCorrectTranslation(contract.getPaymentMode().getTranslations(), locale)
                : "");
        dto.setPaymentTerm(contract.getPaymentTerm() != null ? (contract.getPaymentTerm().getMessage()) : "");
        dto.setPremiumWithCurrency(Optional.ofNullable(contract.getPremium()).orElse(BigDecimal.ZERO)
                .setScale(2, RoundingMode.HALF_UP).toPlainString() + " " + currencySymbol);
        dto.setQuotationNumber(contract.getQuotation() != null
                ? contract.getQuotation().getQno() + " ver " + contract.getQuotation().getVer()
                : "");
        dto.setQuotationId((contract.getQuotation() != null ? contract.getQuotation().getId() : 0));
        dto.setSoftwareName(contract.getSoftware() != null ? contract.getSoftware().getName() : "");
        dto.setTransferTypeName(contract.getTransferType() != null ? contract.getTransferType().getName() : "");
        dto.setTrescalManagerName(contract.getTrescalManager() != null ? contract.getTrescalManager().getName() : "");
        dto.setInvoicingMethodName(
                contract.getInvoicingMethod().getName());
        dto.setHasDocument(contract.getDocument() != null && contract.getDocument().getFileData() != null
                && contract.getDocument().getFileData().length > 0);
        dto.setInstrumentListType(contract.getInstrumentListType().getName());
        dto.setClientCompanyId(contract.getClientCompany().getCoid());
        dto.setBpoId(contract.getBpo() != null ? contract.getBpo().getPoId() : 0);
        dto.setBpoNumber(contract.getBpo() != null ? contract.getBpo().getPoNumber() : "");
        dto.setWarrantyCalibration(
                contract.getWarrantyCalibration() != null ? contract.getWarrantyCalibration() : null);
        dto.setWarrantyRepair(contract.getWarrantyRepair() != null ? contract.getWarrantyRepair() : null);
        return dto;
    }

    @Override
    public List<KeyValueIntegerString> getKeyValueList(Collection<Integer> contractIds) {
        List<KeyValueIntegerString> result = Collections.emptyList();
        if (!contractIds.isEmpty()) {
            result = this.dao.getKeyValueList(contractIds);
        }
        return result;
    }

    @Override
    public PagedResultSet<ContractSearchResultsDto> searchContracts(ContractSearchForm form) {
        return dao.searchContracts(form);
    }

    @Override
    public void addSubdivs(Integer contractId, List<Integer> subdivIds) {

        Contract contract = dao.find(contractId);

        for (Integer subdivId : subdivIds) {
            Subdiv subdiv = subdivService.get(subdivId);
            if (!contract.getClientCompany().equals(subdiv.getComp()))
                throw new IllegalArgumentException();
            if (contract.getSubdivs() == null)
                contract.setSubdivs(Collections.emptySet());
            contract.getSubdivs().add(subdiv);
        }
    }

    @Override
    public void removeSubdiv(Integer contractId, Integer subdivId) {

        Subdiv subdiv = subdivService.get(subdivId);
        Contract contract = dao.find(contractId);

        if (contract.getSubdivs() != null) {
            contract.getSubdivs().remove(subdiv);
        }
    }

    /*
     * Finds the available live contracts for an instrument, contracts that match
     * using more specific criteria are considered a better match So a contract that
     * matches at instrument level is a better match than a contract that matches at
     * subdiv level which is a better match than a contract that matches at company
     * level. If multiple contracts match at a particular level then the most recent
     * contract is considered the best match and will be the default in the contract
     * selection in the job item contract review page.
     */
    private List<MatchingContractDto> findMatchingContractsForInstrument(Instrument instrument, Company clientCompany,
                                                                         Company allocatedCompany, ServiceType serviceType) {
        Set<MatchingContractDto> allMatching = new TreeSet<>(
                Comparator.comparingInt(MatchingContractDto::getContractId));
        List<MatchingContractDto> matchingOnInstrument = dao.findContractsMatchingInstrument(instrument, clientCompany, allocatedCompany, serviceType);
        Subdiv instrumentOwningSubdivision = instrument.getCon().getSub();
        Company instrumentOwningCompany = instrumentOwningSubdivision.getComp();
        if (instrumentOwningCompany.equals(clientCompany)) {
            List<MatchingContractDto> matchingOnSubdiv = dao.findContractsMatchingSubdiv(instrumentOwningSubdivision, allocatedCompany, serviceType);
            List<MatchingContractDto> matchingOnCompany = dao.findContractsMatchingCompany(instrument.getComp(),
                    serviceType);
            if (matchingOnInstrument.size() > 0) {
                // there was at least 1 instrument match, it takes priority
                allMatching.addAll(prioritize(matchingOnInstrument));
                allMatching.addAll(matchingOnSubdiv);
                allMatching.addAll(matchingOnCompany);
            } else if (matchingOnSubdiv.size() > 0) {
                // there was at least 1 subdiv match, it takes priority.
                allMatching.addAll(prioritize(matchingOnSubdiv));
                allMatching.addAll(matchingOnCompany);
            } else if (matchingOnCompany.size() > 0) {
                allMatching.addAll(prioritize(matchingOnCompany));
            }
        } else if (!matchingOnInstrument.isEmpty())
            // It's not possible to have additional contracts with external subdivisions
            allMatching.addAll(prioritize(matchingOnInstrument));
        return new ArrayList<>(allMatching);
    }

    @Override
    public List<MatchingContractDto> findMatchingContractsForJobItem(JobItem jobItem,
                                                                     ServiceType alternativeServiceType) {
        return findMatchingContractsForInstrument(
                jobItem.getInst(),
                jobItem.getJob().getCon().getSub().getComp(),
                jobItem.getJob().getOrganisation().getComp(),
                Optional.ofNullable(alternativeServiceType).orElse(jobItem.getServiceType()));
    }

    @Override
    public List<MatchingContractDto> findMatchingContractsForJobItem(Integer jobItemId, Integer calTypeId) {
        JobItem jobItem = jobItemService.findJobItem(jobItemId);
        ServiceType serviceType = calibrationTypeService.get(calTypeId).getServiceType();
        return findMatchingContractsForJobItem(jobItem, serviceType);
    }

    @Override
    public void addEditInstruction(Integer instructionId, Integer contractId, InstructionType instructionType,
                                   String instructionText, Boolean includeOnClientDeliveryNote, Boolean includeOnSupplierDeliveryNote) {
        Contract contract = dao.find(contractId);
        Instruction instruction;
        if (instructionId == 0) {
            instruction = new Instruction();
            instruction.setInstructiontype(instructionType);
            if (InstructionType.CARRIAGE.equals(instructionType)) {
                instruction.setIncludeOnDelNote(includeOnClientDeliveryNote);
                instruction.setIncludeOnSupplierDelNote(includeOnSupplierDeliveryNote);
            }
            instruction.setInstruction(instructionText);
            if (contract.getInstructions() == null) {
                contract.setInstructions(Collections.emptySet());
            }
            contract.getInstructions().add(instruction);
        } else {
            instruction = contract.getInstructions().stream().filter(i -> i.getInstructionid() == instructionId)
                    .findFirst().orElseThrow(IllegalArgumentException::new);
            instruction.setInstructiontype(instructionType);
            instruction.setInstruction(instructionText);
            if (InstructionType.CARRIAGE.equals(instructionType)) {
                instruction.setIncludeOnDelNote(includeOnClientDeliveryNote);
                instruction.setIncludeOnSupplierDelNote(includeOnSupplierDeliveryNote);
            }
        }
    }

    @Override
    public void deleteInstruction(Integer instructionId, Integer contractId) {
        Contract contract = dao.find(contractId);
        Instruction instruction = contract.getInstructions().stream().filter(i -> i.getInstructionid() == instructionId)
                .findFirst().orElseThrow(IllegalArgumentException::new);
        contract.getInstructions().remove(instruction);
    }

    @Override
    public ContractInstructionDto getInstruction(Integer contractId, Integer instructionId) {
        Contract contract = dao.find(contractId);
        return contract.getInstructions().stream().filter(i -> i.getInstructionid() == instructionId)
                .map(i -> new ContractInstructionDto(i.getInstruction(), i.getInstructiontype(),
                        i.getIncludeOnDelNote(), i.getIncludeOnSupplierDelNote()))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public List<ContractActiveBpoDto> getPossibleBpos(Integer clientCompanyId, Integer BusinessCompanyId) {
        Company clientCompany = companyService.get(clientCompanyId);
        Company businessCompany = companyService.get(BusinessCompanyId);
        List<BpoDTO> bpos = bpoService.getAllByCompany(clientCompany, true, businessCompany);
        return bpos.stream().map(b -> {
            Locale locale = LocaleContextHolder.getLocale();
            List<BPOUsageDTO> bpoUsage = this.bpoService.getBPOUsage(b.getPoId(), locale);
            BigDecimal spentTotal = bpoUsage.stream().map(BPOUsageDTO::getAmount).reduce(BigDecimal.valueOf(0, 2),
                    BigDecimal::add);
            return new ContractActiveBpoDto(b, locale, spentTotal);
        }).collect(Collectors.toList());
    }

    @Override
    public void addService(Integer contractId, Integer serviceTypeId, Integer turnaround) {
        Contract contract = dao.find(contractId);
        List<ContractServiceTypeSettings> contractServiceTypeSettings = contract.getServiceTypeSettings();
        Optional<ContractServiceTypeSettings> settings = contractServiceTypeSettings.stream()
                .filter(csts -> serviceTypeId.equals(csts.getServiceType().getServiceTypeId())).findAny();
        if (settings.isPresent()) {
            settings.get().setTurnaround(turnaround);
        } else {
            ContractServiceTypeSettings newSettings = new ContractServiceTypeSettings();
            newSettings.setContract(contract);
            newSettings.setServiceType(serviceTypeService.get(serviceTypeId));
            newSettings.setTurnaround(turnaround);
            contractServiceTypeSettings.add(newSettings);
        }
    }

    @Override
    public void removeService(Integer contractId, Integer serviceTypeId) {
        Contract contract = dao.find(contractId);
        List<ContractServiceTypeSettings> contractServiceTypeSettings = contract.getServiceTypeSettings();
        Optional<ContractServiceTypeSettings> settings = contractServiceTypeSettings.stream()
                .filter(csts -> serviceTypeId.equals(csts.getServiceType().getServiceTypeId())).findAny();
        settings.ifPresent(contractServiceTypeSettings::remove);
    }

    @Override
    public void addInstrument(Integer contractId, Integer instrumentId) {

        Instrument instrument = instrumentService.get(instrumentId);
        Contract contract = get(contractId);

        if (contract.getInstruments() == null)
            contract.setInstruments(Collections.emptySet());
        contract.getInstruments().add(instrument);
    }

    @Override
    public void addInstruments(Integer contractId, MultipartFile importFile) throws IOException {

        List<LinkedCaseInsensitiveMap<String>> fileContent = excelFileReaderUtil
                .readExcelFile(importFile.getInputStream(), null, null);
        if (fileContent.size() > 0) {
            for (LinkedCaseInsensitiveMap<String> instrumentRecord : fileContent) {
                Integer instrumentId = Integer.valueOf(instrumentRecord
                        .get(this.messageSource.getMessage("barcode", null, LocaleContextHolder.getLocale())));
                addInstrument(contractId, instrumentId);
            }
        }

    }

    @Override
    public void removeInstrument(Integer contractId, Integer instrumentId) {

        Instrument instrument = instrumentService.get(instrumentId);
        Contract contract = get(contractId);

        if (contract.getInstruments() != null)
            contract.getInstruments().remove(instrument);
    }

    @Override
    public DatatableOutput<ContractInstrumentListDto> getInstrumentList(ContractInstrumentListInput input) {
        return dao.getInstrumentList(input, LocaleContextHolder.getLocale());
    }

    @Override
    public DatatableOutput<ContractInstrumentListDto> getUnlinkedInstrumentList(ContractInstrumentListInput input) {
        return dao.getUnlinkedInstrumentList(input, LocaleContextHolder.getLocale());
    }

    /*
     * Sets the last (most recent) contract in a list of contracts as the best match
     */
    private List<MatchingContractDto> prioritize(List<MatchingContractDto> matchingContractList) {
        matchingContractList.get(matchingContractList.size() - 1).setBestMatch(Boolean.TRUE);
        return matchingContractList;
    }

    @Override
    public ContractInformationForJobItemDto updateCostAndGetContractInformation(Integer contractId, Integer jobItemId, Integer serviceTypeId) {
        ContractInformationForJobItemDto contractInfo = dao.getContractInformation(contractId, jobItemId);
        Locale locale = LocaleContextHolder.getLocale();
        if (contractInfo.getQuotationId() != null) {
            contractInfo.setQuoteText(messageSource.getMessage("jicontractreview.quote", null, locale));
            if (!contractInfo.getQuotationIssued()) {
                contractInfo.setQuotationRemark("[<span style='color: red;' title='This quotation has not been issued'>"
                        + messageSource.getMessage("viewjob.unIssued", null, locale) + "</span>]");
            } else if (contractInfo.getQuotationExpired()) {
                contractInfo
                        .setQuotationRemark("[<span style='color: red;' title='This quotation has expired'>e</span>]");
            }
            QuotationItemCostDTO costDto = quotationItemService.getItemCostsByInstrument(contractInfo.getQuotationId(),
                    jobItemId, serviceTypeId);
            if (costDto != null) {
                contractInfo.setPrice(costDto);
                // update contract review cost
                contractReviewCalibrationCostService.updateCostSource(jobItemId, costDto.getCostId(),
                        CostSource.QUOTATION);
            }
            contractInfo.setQuoteItemText(messageSource.getMessage("item", null, locale));
        }
        return contractInfo;
    }

    @Override
    public void updateJobItemsContract(List<JobItem> jobItems, Contract contract) {
        if (contract.getInstrumentListType().equals(ContractInstrumentListType.ADDITIONAL))
            jobItems.forEach(jobItem -> this.linkJobItemToContract(jobItem, contract));
        else {
            List<Integer> plantIds = this.dao.getInstrumentIds(contract.getId());
            // If there are only few job items, it would be better not to sort, but sorting
            // also for large contracts shouldn't be too expensive. Hence, wouldn't like to
            // make it too complicated here.
            // Costs: m = # instruments in contract, n = # job items
            // O(m log(m)) vs O(m * n)
            SortedSet<Integer> sortedPlantIds = new TreeSet<>(plantIds);
            jobItems.forEach(jobItem -> {
                if (sortedPlantIds.contains(jobItem.getInst().getPlantid()))
                    this.linkJobItemToContract(jobItem, contract);
            });
        }
    }

    @Override
    public void linkJobItemToContract(JobItem jobItem, Contract contract) {
        jobItem.setContract(contract);
        // Reset BPO to one linked to contract if there is one linked to the
        // contract
        if (jobItem.getContract() != null && jobItem.getContract().getBpo() != null) {
            if (jobItem.getItemPOs() == null) {
                jobItem.setItemPOs(new HashSet<>());
            } else {
                Optional<JobItemPO> jibpo = jobItem.getItemPOs().stream().filter(jipo -> jipo.getBpo() != null)
                        .findFirst();
                if (jibpo.isPresent())
                    if (jibpo.get().getBpo().equals(contract.getBpo()))
                        return;
                    else
                        jobItem.getItemPOs().remove(jibpo.get());
            }
            JobItemPO jipo = new JobItemPO();
            jipo.setItem(jobItem);
            jipo.setBpo(jobItem.getContract().getBpo());
            jobItem.getItemPOs().add(jipo);
        }
    }

    @Override
    public List<Contract> findContractsByClientCompany(Integer coid) {
        return this.dao.findContractsByClientCompany(coid);
    }

    @Override
    public Contract findContractByQuotation(Quotation quotation) {
        return this.dao.findContractByQuotation(quotation);
    }

}