package org.trescal.cwms.core.contract.dtos;

import org.trescal.cwms.core.jobs.job.dto.BpoDTO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class ContractActiveBpoDto {

    private final Integer bpoId;
    private final String bpoNumber;
    private String startDate;
    private String endDate;
    private final String value;
    private final String spend;

    public ContractActiveBpoDto(BpoDTO bpo, Locale locale, BigDecimal spend) {
        String currencySymbol = bpo.getCurrencySymbol();
        this.bpoId = bpo.getPoId();
        this.bpoNumber = bpo.getPoNumber();
        if (bpo.getDurationFrom() != null) {
            LocalDate startDate = bpo.getDurationFrom().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            this.startDate = startDate.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(locale));
        }
        if (bpo.getDurationTo() != null) {
            LocalDate endDate = bpo.getDurationTo().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            this.endDate = endDate.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(locale));
        }
        this.value = bpo.getLimitAmount() != null ? currencySymbol + bpo.getLimitAmount().setScale(2, RoundingMode.CEILING).toPlainString() : "";
        this.spend = currencySymbol + spend.setScale(2, RoundingMode.CEILING).toPlainString();
    }

    public Integer getBpoId() {
        return bpoId;
    }

    public String getBpoNumber() {
        return bpoNumber;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getValue() {
        return value;
    }

    public String getSpend() {
        return spend;
    }


}
