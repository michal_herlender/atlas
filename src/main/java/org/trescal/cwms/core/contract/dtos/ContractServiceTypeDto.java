package org.trescal.cwms.core.contract.dtos;

public class ContractServiceTypeDto {

	private Integer serviceTypeId;
	private String serviceTypeName;
	private String serviceTypeCode;
	private Boolean serviceTypePermitted;
	private Integer turnaround;

	public ContractServiceTypeDto(Integer serviceTypeId, String serviceTypeName, String serviceTypeCode) {
		super();
		this.serviceTypeName = serviceTypeName;
		this.serviceTypeCode = serviceTypeCode;
		this.serviceTypeId = serviceTypeId;
	}

	public Integer getServiceTypeId() {
		return serviceTypeId;
	}

	public String getServiceTypeName() {
		return serviceTypeName;
	}

	public String getServiceTypeCode() {
		return serviceTypeCode;
	}

	public Boolean getServiceTypePermitted() {
		return serviceTypePermitted;
	}

	public void setServiceTypePermitted(Boolean serviceTypePermitted) {
		this.serviceTypePermitted = serviceTypePermitted;
	}

	public Integer getTurnaround() {
		return turnaround;
	}

	public void setTurnaround(Integer turnaround) {
		this.turnaround = turnaround;
	}
}