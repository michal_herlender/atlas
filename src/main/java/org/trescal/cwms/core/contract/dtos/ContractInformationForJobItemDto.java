package org.trescal.cwms.core.contract.dtos;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.i18n.LocaleContextHolder;
import org.trescal.cwms.core.jobs.additionaldemand.enums.AdditionalDemand;
import org.trescal.cwms.core.quotation.dto.QuotationItemCostDTO;

import java.time.LocalDate;
import java.util.BitSet;

@Getter
@Setter
public class ContractInformationForJobItemDto {

	private Integer contractId;
	private Integer turnaround;
	private boolean cleaning;
	private boolean sealing;
	private boolean engraving;
	private String quoteText;
	private Integer quotationId;
	private String quotationNumber;
	private Integer quotationVersion;
	private Boolean quotationIssued;
	private Boolean quotationExpired;
	private String quotationRemark;
	private String quoteItemText;
	private QuotationItemCostDTO price;
	private Integer warrantyCalibration;
	private Integer warrantyRepair;

	public ContractInformationForJobItemDto(Integer turnaround) {
		super();
		this.contractId = 0;
		this.turnaround = turnaround;
	}

	public ContractInformationForJobItemDto(Integer contractId, Integer turnaround, BitSet additionalDemands,
											Integer quotationId, String quotationNumber, Integer quotationVersion, Boolean quotationIssued,
											LocalDate quotationExpiryDate, Integer warrantyCalibration, Integer warrantyRepair) {
		super();
		this.contractId = contractId;
		this.turnaround = turnaround;
		if (additionalDemands != null) {
			this.setCleaning(additionalDemands.get(AdditionalDemand.CLEANING.ordinal()));
			this.setSealing(additionalDemands.get(AdditionalDemand.SEALING.ordinal()));
			this.setEngraving(additionalDemands.get(AdditionalDemand.ENGRAVING.ordinal()));
		}
		this.quotationId = quotationId;
		this.quotationNumber = quotationNumber;
		this.quotationVersion = quotationVersion;
		this.quotationIssued = quotationIssued;
		this.quotationExpired = quotationExpiryDate != null && quotationExpiryDate.isBefore(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		this.warrantyCalibration = warrantyCalibration;
		this.warrantyRepair = warrantyRepair;
	}


}