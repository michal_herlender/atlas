package org.trescal.cwms.core.contract.dtos;

import org.trescal.cwms.datatables.DatatableInput;

public class ContractDemandInput extends DatatableInput {

	private Integer contractId;

	public Integer getContractId() {
		return contractId;
	}

	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}
}