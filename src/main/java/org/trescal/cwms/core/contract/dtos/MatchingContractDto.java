package org.trescal.cwms.core.contract.dtos;

import java.util.Objects;

public class MatchingContractDto {
    private Integer contractId;
    private String contractDescription;
    private Boolean bestMatch;

    public MatchingContractDto(Integer contractId, String contractNo, String contractDescription) {
        this.contractId = contractId;
        this.contractDescription = contractNo + (contractDescription != null && !contractDescription.trim().isEmpty() ? " - " + contractDescription : "");
        this.bestMatch = false;
    }

    //Only used by tests
    public MatchingContractDto(Integer contractId, String contractDescription, Boolean bestMatch) {
        this.contractId = contractId;
        this.contractDescription = contractDescription;
        this.bestMatch = bestMatch;
    }


    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public String getContractDescription() {
        return contractDescription;
    }

    public void setContractDescription(String contractDescription) {
        this.contractDescription = contractDescription;
    }

    public Boolean getBestMatch() {
        return bestMatch;
    }

    public void setBestMatch(Boolean bestMatch) {
        this.bestMatch = bestMatch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MatchingContractDto that = (MatchingContractDto) o;
        return Objects.equals(contractId, that.contractId) &&
                Objects.equals(contractDescription, that.contractDescription) &&
                Objects.equals(bestMatch, that.bestMatch);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contractId, contractDescription, bestMatch);
    }

    @Override
    public String toString() {
        return "MatchingContractDto{" +
                "contractId=" + contractId +
                ", contractDescription='" + contractDescription + '\'' +
                ", bestMatch=" + bestMatch +
                '}';
    }
}
