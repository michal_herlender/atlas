package org.trescal.cwms.core.contract.controller;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.paymentmode.db.PaymentModeService;
import org.trescal.cwms.core.company.entity.paymentterms.PaymentTerm;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.db.ContractService;
import org.trescal.cwms.core.contract.enums.ContractInstrumentListType;
import org.trescal.cwms.core.contract.enums.InvoicingMethod;
import org.trescal.cwms.core.contract.enums.Software;
import org.trescal.cwms.core.contract.enums.TransferType;
import org.trescal.cwms.core.contract.form.ContractAddEditForm;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.enums.IntervalUnit;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@RequestMapping(path = "/contractaddedit.htm")
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV})
public class ContractAddEditController {

	@Autowired
	private ContactService contactService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private PaymentModeService paymentModeService;
	@Autowired
	private ContractService contractService;
	@Autowired
	private NumerationService numerationService;

	@ModelAttribute("command")
	protected ContractAddEditForm makeForm(
			@RequestParam(name = "id", required = false, defaultValue = "0") Integer contractId) {
		ContractAddEditForm form = null;
		if (contractId > 0) {
			form = contractService.getAddEditForm(contractId);
		}
		else {
			form = new ContractAddEditForm();
			form.setDurationUnit(IntervalUnit.MONTH);
		}
		return form;
	}

	@ModelAttribute("businesscompanycontacts")
	public List<KeyValue<Integer, String>> getBusinessCompnayContacts(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivKeyValue) {
		Subdiv subdiv = subdivService.get(subdivKeyValue.getKey());
		List<Contact> contacts = contactService.getAllCompanyContacts(subdiv.getComp().getCoid());
		return contacts.stream().map(c -> new KeyValue<>(c.getId(), c.getName())).collect(Collectors.toList());
	}


	@ModelAttribute("clientcompanycontacts")
	public List<KeyValue<Integer, String>> getClientCompanyContacts(
			@RequestParam(name = "id", required = false, defaultValue = "0") Integer contractId) {
		if (contractId > 0) {
			Contract contract = contractService.get(contractId);
			return contactService.getAllCompanyContacts(contract.getClientCompany().getCoid()).stream()
					.map(c -> new KeyValue<>(c.getId(), c.getName())).collect(Collectors.toList());
		} else {
			return Collections.singletonList(new KeyValue<>(0, "Select Company First"));
		}
	}

	@ModelAttribute("paymentmodes")
	public List<KeyValue<Integer, String>> getPaymentmode() {
		return paymentModeService.getDTOList(LocaleContextHolder.getLocale(), true);
	}

	@ModelAttribute("paymentterms")
	public Map<String, String> getPaymentterm() {

		return Arrays.stream(PaymentTerm.values())
				.collect(Collectors.toMap(PaymentTerm::name, PaymentTerm::getMessage));
	}

	@ModelAttribute("transfertypes")
	public List<KeyValue<TransferType, String>> getTransfertype() {
		return Arrays.stream(TransferType.values()).map(t -> new KeyValue<>(t, t.getName()))
				.collect(Collectors.toList());
	}

	@ModelAttribute("units")
	public List<KeyValue<String, String>> getUnits() {
		return IntervalUnit.getUnitsForSelect();
	}

	@ModelAttribute("invoicingmethods")
	public List<KeyValue<InvoicingMethod, String>> getInvoicingMethods() {
		return Arrays.stream(InvoicingMethod.values()).sorted(Comparator.comparing(im -> im.name()))
				.map(t -> new KeyValue<>(t, t.getName())).collect(Collectors.toList());
	}

	@ModelAttribute("softwaretypes")
	public List<KeyValue<Software, String>> getSoftwares() {
		return Arrays.stream(Software.values()).map(t -> new KeyValue<>(t, t.getName())).collect(Collectors.toList());
	}

	@ModelAttribute("listTypes")
	public List<KeyValue<ContractInstrumentListType, String>> getListTypes() {
		return Arrays.stream(ContractInstrumentListType.values()).map(t -> new KeyValue<>(t, t.getName()))
				.collect(Collectors.toList());
	}

	@GetMapping
	public String displayForm() {
		return "trescal/core/contract/contractaddedit";
	}

	@PostMapping
	public String saveContract(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivKeyValue,
			@Valid @ModelAttribute("command") ContractAddEditForm form, BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			return displayForm();
		}

		Subdiv subdiv = subdivService.get(subdivKeyValue.getKey());
		Company company = subdiv.getComp();

		Contract contract;

		if (form.getContractId() != null && form.getContractId() > 0) {
			contract = contractService.get(form.getContractId());
		} else {
			contract = new Contract();
			contract.setOrganisation(company);
			contract.setContractNumber(numerationService.generateNumber(NumerationType.CONTRACT, company, subdiv));
		}

		Contract savedContract = contractService.saveOrUpdateFromAddEditForm(contract, form);

		return "redirect:contractview.htm?id=" + savedContract.getId();
	}

}