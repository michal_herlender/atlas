package org.trescal.cwms.core.contract.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.contract.dtos.ContractViewDto;
import org.trescal.cwms.core.contract.entity.contract.db.ContractService;
import org.trescal.cwms.core.exception.controller.IntranetController;

@Controller
@IntranetController
@RequestMapping("/contractview.htm")
public class ContractViewController {

	private final ContractService contractService;

	public ContractViewController(ContractService contractService) {
		this.contractService = contractService;
	}

	@ModelAttribute("contractdetails")
	public ContractViewDto getContractDetails(@RequestParam(value = "id") Integer contractId) {
		return contractService.getContractViewDto(contractId);
	}

	@GetMapping
	public String display() {
		return "trescal/core/contract/viewcontract/contractview";
	}
}