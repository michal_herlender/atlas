package org.trescal.cwms.core.account.entity.nominalcode.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.account.entity.Ledgers;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode_;
import org.trescal.cwms.core.account.entity.nominalcodeapplication.NominalCodeApplication;
import org.trescal.cwms.core.account.entity.nominalcodeapplication.NominalCodeApplication_;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily_;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

import javax.persistence.criteria.*;
import java.util.Collections;
import java.util.List;

@Repository("NominalCodeDao")
public class NominalCodeDaoImpl extends BaseDaoImpl<NominalCode, Integer> implements NominalCodeDao {

	@Override
	protected Class<NominalCode> getEntity() {
		return NominalCode.class;
	}

	@Override
	public NominalCode findBestMatchingNominalCode(Ledgers ledger, CostType costType, Integer subfamilyId) {
		return getFirstResult(cb -> {
			CriteriaQuery<NominalCode> cq = cb.createQuery(NominalCode.class);
			Root<Description> subfamily = cq.from(Description.class);
			Join<Description, InstrumentModelFamily> family = subfamily.join(Description_.family);
			Join<InstrumentModelFamily, InstrumentModelDomain> domain = family.join(InstrumentModelFamily_.domain);
			Root<NominalCodeApplication> nominalCodeApplication = cq.from(NominalCodeApplication.class);
			Join<NominalCodeApplication, NominalCode> nominalCodeForSubfamily = nominalCodeApplication
					.join(NominalCodeApplication_.nominalCode, JoinType.LEFT);
			nominalCodeForSubfamily
					.on(cb.equal(nominalCodeApplication.get(NominalCodeApplication_.subfamily), subfamily));
			Join<NominalCodeApplication, NominalCode> nominalCodeForFamily = nominalCodeApplication
					.join(NominalCodeApplication_.nominalCode, JoinType.LEFT);
			nominalCodeForFamily.on(cb.equal(nominalCodeApplication.get(NominalCodeApplication_.family), family));
			Join<NominalCodeApplication, NominalCode> nominalCodeForDomain = nominalCodeApplication
					.join(NominalCodeApplication_.nominalCode, JoinType.LEFT);
			nominalCodeForDomain.on(cb.equal(nominalCodeApplication.get(NominalCodeApplication_.domain), domain));
			Predicate clauses = cb.conjunction();
			clauses.getExpressions()
				.add(cb.equal(nominalCodeApplication.get(NominalCodeApplication_.costType), costType));
			clauses.getExpressions().add(cb.equal(subfamily, subfamilyId));
			Predicate notAllNull = cb.disjunction();
			notAllNull.getExpressions().add(cb.isNotNull(nominalCodeForDomain));
			notAllNull.getExpressions().add(cb.isNotNull(nominalCodeForFamily));
			notAllNull.getExpressions().add(cb.isNotNull(nominalCodeForSubfamily));
			clauses.getExpressions().add(notAllNull);
			cq.where(clauses);
			cq.select(cb.coalesce(nominalCodeForSubfamily, cb.coalesce(nominalCodeForFamily, nominalCodeForDomain)));
			return cq;
		}).orElse(findBestMatchingNominalCode(Collections.singletonList(ledger), null, costType, null));
	}

	@Override
	public NominalCode findBestMatchingNominalCode(List<Ledgers> ledgers, CalibrationType calType, CostType costType,
			Department department) {
		return getFirstResult(cb -> {
			CriteriaQuery<NominalCode> cq = cb.createQuery(NominalCode.class);
			Root<NominalCode> root = cq.from(NominalCode.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(calType == null ? cb.isNull(root.get(NominalCode_.calType))
					: cb.equal(root.get(NominalCode_.calType), calType));
			clauses.getExpressions().add(costType == null ? cb.isNull(root.get(NominalCode_.costType))
					: cb.equal(root.get(NominalCode_.costType), costType));
			clauses.getExpressions().add(department == null ? cb.isNull(root.get(NominalCode_.department))
					: cb.equal(root.get(NominalCode_.department), department));
			clauses.getExpressions().add(root.get(NominalCode_.ledger).in(ledgers));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

	public NominalCode findNominalCodeByCode(String code) {
		return getSingleResult(cb -> {
			CriteriaQuery<NominalCode> cq = cb.createQuery(NominalCode.class);
			Root<NominalCode> root = cq.from(NominalCode.class);
			cq.where(cb.equal(root.get(NominalCode_.code), code));
			return cq;
		});
	}

	@Override
	public List<NominalCode> getNominalCodes(List<Ledgers> ledgers) {
		return getResultList(cb -> {
			CriteriaQuery<NominalCode> cq = cb.createQuery(NominalCode.class);
			Root<NominalCode> root = cq.from(NominalCode.class);
			cq.where(root.get(NominalCode_.ledger).in(ledgers));
			cq.orderBy(cb.asc(root.get(NominalCode_.ledger)), cb.asc(root.get(NominalCode_.code)));
			return cq;
		});
	}
}