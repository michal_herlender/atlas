package org.trescal.cwms.core.account.entity.creditcard;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.account.entity.creditcardtype.CreditCardType;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Entity
@Table(name = "creditcard")
public class CreditCard extends Auditable
{
	private String cardname;
	private Integer ccid;
	private Contact con;
	private String encryptedCardNo;
	private String expiryDate;
	private String houseno;
	private String issueno;
	private String postcode;
	private CreditCardType type;

	@Column(name = "cardname", length = 60)
	public String getCardname()
	{
		return this.cardname;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ccid")
	public Integer getCcid()
	{
		return this.ccid;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid")
	public Contact getCon()
	{
		return this.con;
	}

	@Length(max = 255)
	@Column(name = "encryptedcardno", length = 255)
	public String getEncryptedCardNo()
	{
		return this.encryptedCardNo;
	}

	@Column(name = "expirydate", length = 6)
	public String getExpiryDate()
	{
		return this.expiryDate;
	}

	@Column(name = "houseno", length = 4)
	public String getHouseno()
	{
		return this.houseno;
	}

	@Column(name = "issueno", length = 2)
	public String getIssueno()
	{
		return this.issueno;
	}

	@Column(name = "postcode", length = 10)
	public String getPostcode()
	{
		return this.postcode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "typeid")
	public CreditCardType getType()
	{
		return this.type;
	}

	public void setCardname(String cardname)
	{
		this.cardname = cardname;
	}

	public void setCcid(Integer ccid)
	{
		this.ccid = ccid;
	}

	public void setCon(Contact con)
	{
		this.con = con;
	}

	public void setEncryptedCardNo(String encryptedCardNo)
	{
		this.encryptedCardNo = encryptedCardNo;
	}

	public void setExpiryDate(String expiryDate)
	{
		this.expiryDate = expiryDate;
	}

	public void setHouseno(String houseno)
	{
		this.houseno = houseno;
	}

	public void setIssueno(String issueno)
	{
		this.issueno = issueno;
	}

	public void setPostcode(String postcode)
	{
		this.postcode = postcode;
	}

	public void setType(CreditCardType type)
	{
		this.type = type;
	}

	/*
	 * Implementation of the toString() method for logging purposes
	 */
	@Override
	public String toString()
	{
		return Integer.toString(this.ccid);
	}
}
