package org.trescal.cwms.core.account.entity;

public enum Ledgers
{
	SALES_LEDGER(1), PURCHASE_LEDGER(2), UTILITY(3), CAPITAL(4);

	private int sortOrder;

	Ledgers(int sortOrder)
	{
		this.sortOrder = sortOrder;
	}

	public int getSortOrder()
	{
		return this.sortOrder;
	}

	public void setSortOrder(int sortOrder)
	{
		this.sortOrder = sortOrder;
	}
}
