package org.trescal.cwms.core.account.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.account.entity.creditcard.CreditCard;
import org.trescal.cwms.core.account.entity.creditcard.db.CreditCardService;
import org.trescal.cwms.core.account.entity.creditcardtype.db.CreditCardTypeService;
import org.trescal.cwms.core.account.form.CreditCardForm;
import org.trescal.cwms.core.account.form.CreditCardFormValidator;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.tools.DigestConfigCreator;

@Controller
@IntranetController
public class CreditCardAddController {

	@Autowired
	private CreditCardService ccServ;
	@Autowired
	private CreditCardTypeService ccTypeServ;
	@Autowired
	private ContactService contactService;
	@Autowired
	private DigestConfigCreator digestCreator;
	@Autowired
	private CreditCardFormValidator validator;

	@InitBinder("formBackingObject")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("creditcardform")
	protected CreditCardForm formBackingObject(@RequestParam(value = "personid", required = false) Integer personid)
			throws Exception {
		CreditCardForm ccf = new CreditCardForm();
		CreditCard c = new CreditCard();
		ccf.setPersonid(personid);
		ccf.setCreditCard(c);
		return ccf;
	}

	@RequestMapping(value = "/addcreditcard.htm", method = RequestMethod.POST)
	protected RedirectView onSubmit(@ModelAttribute("creditcardform") CreditCardForm ccf) throws Exception {
		CreditCard cc = ccf.getCreditCard();
		Contact c = this.contactService.get(ccf.getPersonid());
		cc.setType(this.ccTypeServ.get(ccf.getTypeid()));
		cc.setCon(c);
		cc.setExpiryDate(ccf.getExpDate1() + ccf.getExpDate2());
		// encrypt card no
		String cardNo = ccf.getCardno1() + ccf.getCardno2() + ccf.getCardno3() + ccf.getCardno4();
		String encrypted = this.digestCreator.getStringEncryptor().encrypt(cardNo);
		cc.setEncryptedCardNo(encrypted);
		this.ccServ.save(cc);
		c.setCreditCard(cc);
		this.contactService.merge(c);
		String tab = "&loadtab=accounts-tab";
		return new RedirectView("viewperson.htm?personid=" + ccf.getPersonid() + tab);
	}

	@RequestMapping(value = "/addcreditcard.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData() throws Exception {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("types", this.ccTypeServ.getAll());
		return new ModelAndView("trescal/core/account/creditcard", model);
	}
}