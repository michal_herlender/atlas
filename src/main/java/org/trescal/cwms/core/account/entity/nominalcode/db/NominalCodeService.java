package org.trescal.cwms.core.account.entity.nominalcode.db;

import java.util.List;
import java.util.Set;

import org.trescal.cwms.core.account.entity.Ledgers;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;

public interface NominalCodeService extends BaseService<NominalCode, Integer> {

	/**
	 * Tries to find the best matching {@link NominalCode} based on the given
	 * criteria. This uses 'strict' criteria, i.e. it checks that *all* of the
	 * given variables exist and if a {@link NominalCode} does not match all
	 * four then it is discarded. However, if the system property
	 * {cwms.config.accounts.nominallookups.loosecaltypes} is set to true, then
	 * if a lookup fails to find a nominal using strict checking then a second
	 * pass will be made which will ignore calibration type in the nominal
	 * lookup.
	 * 
	 * @param ledger
	 *            the {@link Ledgers} to check for.
	 * @param costTypeId
	 *            the {@link CostType} id.
	 * @param jobitemid
	 *            the {@link JobItem} id.
	 * @return
	 */
	ResultWrapper findAjaxBestMatchingNominalCode(List<Ledgers> ledgers, Integer costTypeId, Integer jobitemid);

	NominalCode findBestMatchingNominalCode(Ledgers ledger, CostType costType, Integer subfamilyId);

	/**
	 * Tries to find the best matching {@link NominalCode} based on the given
	 * criteria. This uses 'strict' criteria, i.e. it checks that *all* of the
	 * given variables exist and if a {@link NominalCode} does not match all
	 * four then it is discarded. However, if the system property
	 * {cwms.config.accounts.nominallookups.loosecaltypes} is set to true, then
	 * if a lookup fails to find a nominal using strict checking then a second
	 * pass will be made which will ignore calibration type in the nominal
	 * lookup. The {@link JobItem} entity is used to try and find the matching
	 * department that the work is being done in via the jobitem's current
	 * procedure.
	 * 
	 * @param ledger
	 *            the {@link Ledgers} to check for.
	 * @param costTypeId
	 *            the {@link CostType} id.
	 * @param ji
	 *            the {@link JobItem} id.
	 * @return
	 */
	NominalCode findBestMatchingNominalCode(List<Ledgers> ledgers, Integer costTypeId, JobItem ji);

	NominalCode findNominalCodeByCode(String code);

	/**
	 * Returns all {@link NominalCode}s that belong to the given {@link Ledgers}
	 * .
	 * 
	 * @param ledgers
	 *            the {@link Ledgers}.
	 * @return {@link Set} of all matching {@link NominalCode}.
	 */
	List<NominalCode> getNominalCodes(List<Ledgers> ledgers);
}