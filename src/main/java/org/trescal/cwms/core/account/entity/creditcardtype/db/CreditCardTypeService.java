package org.trescal.cwms.core.account.entity.creditcardtype.db;

import org.trescal.cwms.core.account.entity.creditcardtype.CreditCardType;
import org.trescal.cwms.core.audit.entity.db.BaseService;

public interface CreditCardTypeService extends BaseService<CreditCardType, Integer> {
}