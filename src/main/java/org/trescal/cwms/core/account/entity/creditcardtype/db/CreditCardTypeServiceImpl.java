package org.trescal.cwms.core.account.entity.creditcardtype.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.account.entity.creditcardtype.CreditCardType;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;

@Service
public class CreditCardTypeServiceImpl extends BaseServiceImpl<CreditCardType, Integer>
		implements CreditCardTypeService {

	@Autowired
	private CreditCardTypeDao creditCardTypeDao;

	@Override
	protected BaseDao<CreditCardType, Integer> getBaseDao() {
		return creditCardTypeDao;
	}
}