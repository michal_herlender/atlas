package org.trescal.cwms.core.account.entity.creditcard.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.account.entity.creditcard.CreditCard;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;

@Repository("CreditCardDao")
public class CreditCardDaoImpl extends BaseDaoImpl<CreditCard, Integer> implements CreditCardDao {
	
	@Override
	protected Class<CreditCard> getEntity() {
		return CreditCard.class;
	}
}