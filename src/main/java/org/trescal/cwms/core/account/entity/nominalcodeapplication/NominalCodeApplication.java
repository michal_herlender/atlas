package org.trescal.cwms.core.account.entity.nominalcodeapplication;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;

import lombok.Setter;

@Entity
@Table(name = "nominalcodeapplication", uniqueConstraints = @UniqueConstraint(name = "UK_nominalcodeapplication", columnNames = {
		"domainid", "familyid", "subfamilyid" }))
@Setter
public class NominalCodeApplication {

	private Integer id;
	private NominalCode nominalCode;
	private CostType costType;
	private InstrumentModelDomain domain;
	private InstrumentModelFamily family;
	private Description subfamily;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	@ManyToOne
	@JoinColumn(name = "nominalcodeid", nullable = false, foreignKey = @ForeignKey(name = "FK_nominalcodeapplication_nominalcode"))
	public NominalCode getNominalCode() {
		return nominalCode;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "costtypeid")
	public CostType getCostType() {
		return costType;
	}

	@ManyToOne
	@JoinColumn(name = "domainid", foreignKey = @ForeignKey(name = "FK_nominalcodeapplication_domain"))
	public InstrumentModelDomain getDomain() {
		return domain;
	}

	@ManyToOne
	@JoinColumn(name = "familyid", foreignKey = @ForeignKey(name = "FK_nominalcodeapplication_family"))
	public InstrumentModelFamily getFamily() {
		return family;
	}

	@ManyToOne
	@JoinColumn(name = "subfamilyid", foreignKey = @ForeignKey(name = "FK_nominalcodeapplication_subfamily"))
	public Description getSubfamily() {
		return subfamily;
	}
}