package org.trescal.cwms.core.account.entity.companyaccount;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.account.entity.AccountStatus;
import org.trescal.cwms.core.account.entity.bankdetail.BankDetail;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;

/**
 * Entity representing the account information held on a single company - this
 * infomration is collated here typically for transfer into 3rd party accounts
 * software.
 * 
 * @author Richard
 * 
 * 2018-09-07 GB : Removed entity defintion due to performance impact (OneToOne in Customer/Contact/Address)
 * Targeted for deletion, checking with Adrian whether AccountDimensions still used
 * 
 */
//@Entity
//@Table(name = "companyaccount")
public class CompanyAccount extends Auditable
{
	private String accountNote;
	private AccountStatus accountsStatus;
	private Address address;
	private BankDetail bankDetails;
	private Company company;
	private Contact contact;
	private int id;
	private BigDecimal lineDiscount;
	private int settlementDays;
	private BigDecimal settlementDiscount;

	@Length(max = 1000)
	@Column(name = "accountnote", length = 1000)
	public String getAccountNote()
	{
		return this.accountNote;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "accountstatus", length = 1, nullable = true)
	public AccountStatus getAccountsStatus()
	{
		return this.accountsStatus;
	}

	@OneToOne(fetch=FetchType.LAZY)
	@Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	@JoinColumn(name = "addressid")
	public Address getAddress()
	{
		return this.address;
	}

	@OneToOne(mappedBy = "companyAccount", cascade = CascadeType.ALL)
	@JoinColumn(name = "bankdetid")
	public BankDetail getBankDetails()
	{
		return this.bankDetails;
	}

	@OneToOne(mappedBy = "account")
	public Company getCompany()
	{
		return this.company;
	}

	@OneToOne
	@Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
	@JoinColumn(name = "personid")
	public Contact getContact()
	{
		return this.contact;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@Column(name = "linediscount", nullable = false, precision = 5, scale = 2)
	public BigDecimal getLineDiscount()
	{
		return this.lineDiscount;
	}

	@NotNull
	@Column(name = "settlementdays", nullable = false)
	public int getSettlementDays()
	{
		return this.settlementDays;
	}

	@NotNull
	@Column(name = "settlementdiscount", nullable = false, precision = 5, scale = 2)
	public BigDecimal getSettlementDiscount()
	{
		return this.settlementDiscount;
	}

	public void setAccountNote(String accountNote)
	{
		this.accountNote = accountNote;
	}

	public void setAccountsStatus(AccountStatus accountsStatus)
	{
		this.accountsStatus = accountsStatus;
	}

	public void setAddress(Address address)
	{
		this.address = address;
	}

	public void setBankDetails(BankDetail bankDetails)
	{
		this.bankDetails = bankDetails;
	}

	public void setCompany(Company company)
	{
		this.company = company;
	}

	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setLineDiscount(BigDecimal lineDiscount)
	{
		this.lineDiscount = lineDiscount;
	}

	public void setSettlementDays(int settlementDays)
	{
		this.settlementDays = settlementDays;
	}

	public void setSettlementDiscount(BigDecimal settlementDiscount)
	{
		this.settlementDiscount = settlementDiscount;
	}
}
