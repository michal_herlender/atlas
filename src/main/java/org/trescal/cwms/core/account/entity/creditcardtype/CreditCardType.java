package org.trescal.cwms.core.account.entity.creditcardtype;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.trescal.cwms.core.account.entity.creditcard.CreditCard;

/**
 * Represents a type of Credit Card.
 * 
 * @author Richard
 */
@Entity
@Table(name = "creditcardtype")
public class CreditCardType
{
	private Set<CreditCard> creditCards;
	private String description;
	private String name;
	private Integer typeid;

	@OneToMany(mappedBy = "type")
	public Set<CreditCard> getCreditCards()
	{
		return this.creditCards;
	}

	@Column(name = "description", length = 255)
	public String getDescription()
	{
		return this.description;
	}

	@Column(name = "name", length = 10)
	public String getName()
	{
		return this.name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "typeid")
	public Integer getTypeid()
	{
		return this.typeid;
	}

	public void setCreditCards(Set<CreditCard> creditCards)
	{
		this.creditCards = creditCards;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setTypeid(Integer typeid)
	{
		this.typeid = typeid;
	}
}
