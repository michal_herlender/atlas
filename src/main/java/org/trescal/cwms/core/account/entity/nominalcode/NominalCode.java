package org.trescal.cwms.core.account.entity.nominalcode;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.trescal.cwms.core.account.NominalCodeType;
import org.trescal.cwms.core.account.entity.Ledgers;
import org.trescal.cwms.core.account.entity.nominalcodeapplication.NominalCodeApplication;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorderitem.PurchaseOrderItem;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.translation.Translation;

import lombok.Setter;

@Entity
@Table(name = "nominalcode")
@Setter
public class NominalCode extends Auditable {

	private CalibrationType calType;
	private String code;
	private CostType costType;
	private Department department;
	private int id;
	private Ledgers ledger;
	private Set<PurchaseOrderItem> poItems;
	private String title;
	private Set<Translation> titleTranslations;
	private String translatedTitle;
	private NominalCodeType nominalCodeType;
	private Set<NominalCodeApplication> nominalCodeApplications;
	private String avalaraTaxCode;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "caltypeid")
	public CalibrationType getCalType() {
		return this.calType;
	}

	@NotEmpty
	@Length(max = 100)
	@Column(name = "code", length = 100, unique = true)
	public String getCode() {
		return this.code;
	}

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "costtypeid")
	public CostType getCostType() {
		return this.costType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deptid")
	public Department getDepartment() {
		return this.department;
	}

	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	public int getId() {
		return this.id;
	}

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "ledger", nullable = false)
	public Ledgers getLedger() {
		return this.ledger;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "nominal")
	public Set<PurchaseOrderItem> getPoItems() {
		return this.poItems;
	}

	@NotEmpty
	@Length(max = 100)
	@Column(name = "title", length = 100)
	public String getTitle() {
		return this.title;
	}

	@Column(name = "typenominalcode", nullable = true)
	@Enumerated(EnumType.STRING)
	public NominalCodeType getNominalCodeType() {
		return nominalCodeType;
	}

	@OneToMany(mappedBy = "nominalCode")
	public Set<NominalCodeApplication> getNominalCodeApplications() {
		return nominalCodeApplications;
	}

	@Column(name = "avalarataxcode")
	public String getAvalaraTaxCode() {
		return avalaraTaxCode;
	}

	@Override
	public String toString() {
		return this.code + " : "
				+ (this.translatedTitle == null || this.translatedTitle.isEmpty() ? this.title : this.translatedTitle)
				+ " : " + this.ledger;
	}

	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "nominalcodetitletranslation", joinColumns = @JoinColumn(name = "id"))
	public Set<Translation> getTitleTranslations() {
		return titleTranslations;
	}

	@Transient
	public String getTranslatedTitle() {
		return this.translatedTitle;
	}

	@Transient
	public String getCodeAndTitle() {
		return this.code + " - " + this.translatedTitle;
	}
}