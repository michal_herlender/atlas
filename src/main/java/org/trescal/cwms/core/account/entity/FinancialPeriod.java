package org.trescal.cwms.core.account.entity;

public class FinancialPeriod
{
	private int realMonth;
	private String monthName;
	private int period;
	boolean currentPeriod;

	public FinancialPeriod()
	{
	}

	public FinancialPeriod(int realMonth, String monthName, int period, boolean currentPeriod)
	{
		this.realMonth = realMonth;
		this.monthName = monthName;
		this.period = period;
		this.currentPeriod = currentPeriod;
	}

	public String getMonthName()
	{
		return monthName;
	}

	public int getPeriod()
	{
		return period;
	}

	public int getRealMonth()
	{
		return realMonth;
	}

	public boolean isCurrentPeriod()
	{
		return currentPeriod;
	}

	public void setCurrentPeriod(boolean currentPeriod)
	{
		this.currentPeriod = currentPeriod;
	}

	public void setMonthName(String monthName)
	{
		this.monthName = monthName;
	}

	public void setPeriod(int period)
	{
		this.period = period;
	}

	public void setRealMonth(int realMonth)
	{
		this.realMonth = realMonth;
	}
}
