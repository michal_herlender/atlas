package org.trescal.cwms.core.account.form;

import org.trescal.cwms.core.account.entity.creditcard.CreditCard;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CreditCardForm
{
	private CreditCard creditCard;
	private int typeid;
	private int personid;
		
	private String cardno1;
	private String cardno2;
	private String cardno3;
	private String cardno4;
	
	private String expDate1;
	private String expDate2;

}
