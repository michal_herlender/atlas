package org.trescal.cwms.core.account.entity.bankaccount.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.account.entity.bankaccount.BankAccount;
import org.trescal.cwms.core.account.entity.bankaccount.BankAccount_;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;

@Repository
public class BankAccountDaoImpl extends BaseDaoImpl<BankAccount, Integer> implements BankAccountDao {

	@Override
	protected Class<BankAccount> getEntity() {
		return BankAccount.class;
	}

	@Override
	public List<BankAccount> searchByCompany(Company company) {
		return getResultList(cb -> {
			CriteriaQuery<BankAccount> cq = cb.createQuery(BankAccount.class);
			Root<BankAccount> root = cq.from(BankAccount.class);
			cq.where(cb.equal(root.get(BankAccount_.company), company));
			cq.orderBy(cb.asc(root.get(BankAccount_.iban)));
			return cq;
		});
	}

	@Override
	public Boolean isUsedInCompanySettings(BankAccount bankAccount) {
		Long count = getSingleResult(cb -> {
			CriteriaQuery<Long> cq = cb.createQuery(Long.class);
			Root<CompanySettingsForAllocatedCompany> root = cq.from(CompanySettingsForAllocatedCompany.class);
			cq.where(cb.equal(root.get(CompanySettingsForAllocatedCompany_.bankAccount), bankAccount));
			cq.select(cb.count(root));
			return cq;
		});
		return count > 0;
	}
}