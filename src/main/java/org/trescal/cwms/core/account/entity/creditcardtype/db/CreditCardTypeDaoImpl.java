package org.trescal.cwms.core.account.entity.creditcardtype.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.account.entity.creditcardtype.CreditCardType;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;

@Repository("CreditCardTypeDao")
public class CreditCardTypeDaoImpl extends BaseDaoImpl<CreditCardType, Integer> implements CreditCardTypeDao {
	
	@Override
	protected Class<CreditCardType> getEntity() {
		return CreditCardType.class;
	}
}