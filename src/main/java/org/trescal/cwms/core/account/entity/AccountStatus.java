package org.trescal.cwms.core.account.entity;

/**
 * Enumeration of the different status's an account can be set to.
 * 
 * @author Richard
 */
public enum AccountStatus
{
	P("Pending", "accountstatus.pending"), S("Synchronised", "accountstatus.synchronised"), E("Error", "accountstatus.error");

	String description;
	String messageCode;

	AccountStatus(String description, String messageCode)
	{
		this.description = description;
		this.messageCode = messageCode;
	}

	public String getDescription()
	{
		return this.description;
	}
	
	public String getMessageCode()
	{
		return this.messageCode;
	}


	public void setDescription(String description)
	{
		this.description = description;
	}
}
