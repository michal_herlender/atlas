package org.trescal.cwms.core.account.entity.nominalcode;

import java.util.Comparator;

public class NominalCodeComparator implements Comparator<NominalCode>
{
	@Override
	public int compare(NominalCode o1, NominalCode o2)
	{
		if (o1.getLedger() == o2.getLedger())
		{
			return o1.getCode().compareTo(o2.getCode());
		}
		else
		{
			return ((Integer) o1.getLedger().getSortOrder()).compareTo(o2.getLedger().getSortOrder());
		}
	}
}
