package org.trescal.cwms.core.account.entity.creditcard.db;

import org.trescal.cwms.core.account.entity.creditcard.CreditCard;
import org.trescal.cwms.core.audit.entity.db.BaseService;

public interface CreditCardService extends BaseService<CreditCard, Integer> {
}