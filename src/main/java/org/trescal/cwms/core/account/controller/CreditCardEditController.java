package org.trescal.cwms.core.account.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.account.entity.creditcard.CreditCard;
import org.trescal.cwms.core.account.entity.creditcard.db.CreditCardService;
import org.trescal.cwms.core.account.entity.creditcardtype.db.CreditCardTypeService;
import org.trescal.cwms.core.account.form.CreditCardForm;
import org.trescal.cwms.core.account.form.CreditCardFormValidator;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DigestConfigCreator;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME })
public class CreditCardEditController {

	@Autowired
	private CreditCardService ccService;
	@Autowired
	private CreditCardTypeService ccTypeService;
	@Autowired
	private ContactService contService;
	@Autowired
	private DigestConfigCreator digestCreator;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;
	@Autowired
	private CreditCardFormValidator validator;

	@InitBinder("formBackingObject")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("creditcardform")
	protected CreditCardForm formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@RequestParam(value = "ccid", required = false, defaultValue = "0") Integer ccid) throws Exception {
		CreditCardForm ccf = new CreditCardForm();
		CreditCard c = this.ccService.get(ccid);
		ccf.setPersonid(c.getCon().getPersonid());
		// decrypt card no
		String cardNo = this.digestCreator.getStringEncryptor().decrypt(c.getEncryptedCardNo());
		// if this person is authorised to see the whole card no (i.e. is an
		// admin user or in accounts dept)
		Contact contact = this.userService.get(username).getCon();
		Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
		if (this.contService.contactBelongsToDepartmentOrIsAdmin(contact, DepartmentType.ACCOUNTS,
				allocatedSubdiv.getComp())) {
			ccf.setCardno1(cardNo.substring(0, 4));
			ccf.setCardno2(cardNo.substring(4, 8));
			ccf.setCardno3(cardNo.substring(8, 12));
			ccf.setCardno4(cardNo.substring(12, 16));
		}
		// else only show last 4 digits for identification purposes
		else {
			ccf.setCardno1("XXXX");
			ccf.setCardno2("XXXX");
			ccf.setCardno3("XXXX");
			ccf.setCardno4(cardNo.substring(12, 16));
		}
		ccf.setExpDate1(c.getExpiryDate().substring(0, 2));
		ccf.setExpDate2(c.getExpiryDate().substring(2, 6));
		ccf.setTypeid(c.getType().getTypeid());
		ccf.setCreditCard(c);
		return ccf;
	}

	@RequestMapping(value = "/editcreditcard.htm", method = RequestMethod.POST)
	protected RedirectView onSubmit(@ModelAttribute("creditcardform") CreditCardForm ccf) throws Exception {
		CreditCard cc = ccf.getCreditCard();
		cc.setType(this.ccTypeService.get(ccf.getTypeid()));
		cc.setExpiryDate(ccf.getExpDate1() + ccf.getExpDate2());
		// encrypt card no
		String cardNo = ccf.getCardno1() + ccf.getCardno2() + ccf.getCardno3() + ccf.getCardno4();
		String encrypted = this.digestCreator.getStringEncryptor().encrypt(cardNo);
		cc.setEncryptedCardNo(encrypted);
		this.ccService.merge(cc);
		String tab = "&loadtab=accounts-tab";
		return new RedirectView("viewperson.htm?personid=" + ccf.getPersonid() + tab);
	}

	@RequestMapping(value = "/editcreditcard.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("types", this.ccTypeService.getAll());
		// check if this user is a member of accounts or an admin
		Contact contact = this.userService.get(username).getCon();
		Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
		model.put("accountsAuthenticated", this.contService.contactBelongsToDepartmentOrIsAdmin(contact,
				DepartmentType.ACCOUNTS, allocatedSubdiv.getComp()));
		return new ModelAndView("trescal/core/account/creditcard", model);
	}
}