package org.trescal.cwms.core.account;

public enum NominalCodeType {
	OPEX,
	GENEX,
	CAPEX
}
