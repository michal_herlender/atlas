package org.trescal.cwms.core.account.entity.bankaccount;

import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/*
 * Replacement in GroupERP for BankDetail and CompanyAccount coupled entities which are intended to be removed.
 */
@Entity
@Table(name = "bankaccount")
@Setter
public class BankAccount extends Versioned {

    private int id;
    private Company company;
    private String bankName;
	private String iban;
	private String swiftBic;
	private SupportedCurrency currency;
	private String accountNo;
	private Boolean includeOnFactoredInvoices;
	private Boolean includeOnNonFactoredInvoices;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@Type(type = "int")
	public int getId() {
		return id;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="companyid", nullable=false)
	public Company getCompany() {
		return company;
	}
	
	@NotEmpty
	@Column(name="bankname", length=100, nullable=false)
	public String getBankName() {
		return bankName;
	}

    /*
     * IBAN size can be up to 34 characters (maybe shorter)
     */
	@NotEmpty
	@Column(name="iban", length=34, nullable=false)
	public String getIban() {
		return iban;
	}
	
	/*
	 * SWIFT/BIC code can be 8 or 11 characters
	 */
	@NotEmpty
	@Column(name="swiftbic", length=11, nullable=false)
	public String getSwiftBic() {
		return swiftBic;
	}
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="currencyid", nullable=false)
	public SupportedCurrency getCurrency() {
		return currency;
	}
	
	@Length(max = 50)
	@Column(name = "accountno", length = 50)
	public String getAccountNo()
	{
		return this.accountNo;
	}

	@NotNull
	@Column(name = "includeOnFactoredInvoices", nullable = false)
	public Boolean getIncludeOnFactoredInvoices() {
		return includeOnFactoredInvoices;
	}

	@NotNull
	@Column(name = "includeOnNonFactoredInvoices", nullable = false)
	public Boolean getIncludeOnNonFactoredInvoices() {
		return includeOnNonFactoredInvoices;
	}
}