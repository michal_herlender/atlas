package org.trescal.cwms.core.account.entity.bankaccount.db;

import java.util.List;

import org.trescal.cwms.core.account.entity.bankaccount.BankAccount;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;

public interface BankAccountService extends BaseService<BankAccount, Integer> {
	List<BankAccount> searchByCompany(Company company);
	
	Boolean isUsedInCompanySettings(BankAccount bankAccount);
}
