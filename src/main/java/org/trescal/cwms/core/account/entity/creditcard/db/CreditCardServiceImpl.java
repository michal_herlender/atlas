package org.trescal.cwms.core.account.entity.creditcard.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.account.entity.creditcard.CreditCard;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;

@Service
public class CreditCardServiceImpl extends BaseServiceImpl<CreditCard, Integer> implements CreditCardService {

	@Autowired
	private CreditCardDao creditCardDao;

	@Override
	protected BaseDao<CreditCard, Integer> getBaseDao() {
		return creditCardDao;
	}
}