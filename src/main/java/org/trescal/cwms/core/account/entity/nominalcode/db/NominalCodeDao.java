package org.trescal.cwms.core.account.entity.nominalcode.db;

import java.util.List;

import org.trescal.cwms.core.account.entity.Ledgers;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

public interface NominalCodeDao extends BaseDao<NominalCode, Integer> {

	NominalCode findBestMatchingNominalCode(Ledgers ledger, CostType costType, Integer subfamilyId);

	NominalCode findBestMatchingNominalCode(List<Ledgers> ledgers, CalibrationType calType, CostType costType,
			Department department);

	NominalCode findNominalCodeByCode(String code);

	List<NominalCode> getNominalCodes(List<Ledgers> ledgers);
}