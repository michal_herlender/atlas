package org.trescal.cwms.core.account.form;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.account.entity.creditcard.CreditCard;

@Component
public class CreditCardFormValidator implements Validator
{
	protected final Log logger = LogFactory.getLog(this.getClass());
	
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(CreditCardForm.class);
	}

	public void validate(Object command, Errors errors)
	{
		CreditCardForm ccf = (CreditCardForm) command;

		if (ccf == null)
		{
			errors.reject("error.nullpointer", "Null data received");
		}
		else
		{
			CreditCard cc = ccf.getCreditCard();
			if (cc == null)
			{
				errors.rejectValue("creditCard", "error.creditcard", "Credit card not entered");
			}

			String cardno = ccf.getCardno1() + ccf.getCardno2()
					+ ccf.getCardno3() + ccf.getCardno4();
			if (cardno.length() != 16)
			{
				errors.rejectValue("cardno4", "error.creditcard.cardno.length", null, "Card name too long");
			}
			if (!cardno.matches("[0-9]*"))
			{
				errors.rejectValue("cardno4", "error.creditcard.cardno.letters", null, "The card number contains letters");
			}
			if ((cc.getCardname() == null) || cc.getCardname().equals(""))
			{
				errors.rejectValue("creditCard.cardname", "error.creditcard.cardname.null", null, "Card name not specified");
			}
			if ((cc.getCardname() != null) && (cc.getCardname().length() > 60))
			{
				errors.rejectValue("creditCard.cardname", "error.creditcard.cardname.length", null, "The card number is not the valid length");
			}
			if ((cc.getIssueno() != null) && (cc.getIssueno().length() > 2))
			{
				errors.rejectValue("creditCard.issueno", "error.creditcard.issueno.length", null, "Issue number too long");
			}
			if ((cc.getIssueno() != null) && !cc.getIssueno().matches("[0-9]*"))
			{
				errors.rejectValue("creditCard.issueno", "error.creditcard.issueno.letters", null, "The issue number contains letters");
			}
			if ((cc.getHouseno() == null) || cc.getHouseno().equals(""))
			{
				errors.rejectValue("creditCard.houseno", "error.creditcard.houseno.null", null, "House number not specified");
			}
			if ((cc.getHouseno() != null) && (cc.getHouseno().length() > 4))
			{
				errors.rejectValue("creditCard.houseno", "error.creditcard.houseno.length", null, "House number too long");
			}
			if ((cc.getPostcode() == null) || cc.getPostcode().equals(""))
			{
				errors.rejectValue("creditCard.postcode", "error.creditcard.postcode.null", null, "Postcode not specified");
			}
			if ((cc.getPostcode() != null) && (cc.getPostcode().length() > 10))
			{
				errors.rejectValue("creditCard.postcode", "error.creditcard.postcode.length", null, "Postcode too long");
			}
		}

	}
}