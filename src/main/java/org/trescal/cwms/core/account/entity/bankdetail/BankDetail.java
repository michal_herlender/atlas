package org.trescal.cwms.core.account.entity.bankdetail;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.trescal.cwms.core.account.entity.companyaccount.CompanyAccount;
import org.trescal.cwms.core.audit.Auditable;

/**
 * 2018-09-07 GB : Removed entity defintion due to performance impact (OneToOne in Customer/Contact/Address)
 * Targeted for deletion, checking with Adrian whether AccountDimensions still used
 * 
 */
//@Entity
//@Table(name = "bankdetail")
public class BankDetail extends Auditable
{
	private String accountName;
	private String accountNo;
	private String bacsRef;
	private String bankName;
	private CompanyAccount companyAccount;
	private int id;
	private String sortCode;

	@Length(max = 200)
	@Column(name = "accountname", length = 200)
	public String getAccountName()
	{
		return this.accountName;
	}

	@NotEmpty
	@Length(max = 50)
	@Column(name = "accountno", length = 50)
	public String getAccountNo()
	{
		return this.accountNo;
	}

	@Length(max = 100)
	@Column(name = "bacsref", length = 100)
	public String getBacsRef()
	{
		return this.bacsRef;
	}

	@NotEmpty
	@Length(max = 100)
	@Column(name = "bankname", length = 100)
	public String getBankName()
	{
		return this.bankName;
	}

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "accountid")
	public CompanyAccount getCompanyAccount()
	{
		return this.companyAccount;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotEmpty
	@Length(max = 6)
	@Column(name = "sortcode", length = 6)
	public String getSortCode()
	{
		return this.sortCode;
	}

	public void setAccountName(String accountName)
	{
		this.accountName = accountName;
	}

	public void setAccountNo(String accountNo)
	{
		this.accountNo = accountNo;
	}

	public void setBacsRef(String bacsRef)
	{
		this.bacsRef = bacsRef;
	}

	public void setBankName(String bankName)
	{
		this.bankName = bankName;
	}

	public void setCompanyAccount(CompanyAccount companyAccount)
	{
		this.companyAccount = companyAccount;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setSortCode(String sortCode)
	{
		this.sortCode = sortCode;
	}

}
