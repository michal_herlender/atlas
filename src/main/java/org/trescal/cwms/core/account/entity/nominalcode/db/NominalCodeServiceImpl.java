package org.trescal.cwms.core.account.entity.nominalcode.db;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.account.entity.Ledgers;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCodeComparator;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.system.entity.translation.TranslationService;

import java.util.ArrayList;
import java.util.List;

@Service("NominalCodeService")
public class NominalCodeServiceImpl extends BaseServiceImpl<NominalCode, Integer> implements NominalCodeService {
    protected final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private JobItemService jiServ;
    @Autowired
    private NominalCodeDao nominalCodeDao;
    @Autowired
    private TranslationService translationService;

    @Override
    protected BaseDao<NominalCode, Integer> getBaseDao() {
        return nominalCodeDao;
    }

    @Override
    public NominalCode findBestMatchingNominalCode(Ledgers ledger, CostType costType, Integer subfamilyId) {
        return nominalCodeDao.findBestMatchingNominalCode(ledger, costType, subfamilyId);
	}

	@Override
	public ResultWrapper findAjaxBestMatchingNominalCode(List<Ledgers> ledgers, Integer costTypeId, Integer jobitemid) {
		JobItem ji = null;
		if (jobitemid != null && jobitemid != 0) {
			ji = this.jiServ.findJobItem(jobitemid);
		}
		if (ji == null) {
			return new ResultWrapper(false, "");
		}
		else {
			NominalCode code = this.findBestMatchingNominalCode(ledgers, costTypeId, ji);
			return new ResultWrapper(code == null ? false : true, "", code, null);
		}
	}

	@Override
	public NominalCode findBestMatchingNominalCode(List<Ledgers> ledgers, Integer costTypeId, JobItem ji) {

        CostType costType = costTypeId != null ? CostType.valueOf(costTypeId) : null;
        // New design to apply nominal codes
        for (Ledgers ledger : ledgers) {
            NominalCode nominalCode = findBestMatchingNominalCode(ledger, costType, ji.getInst().getModel().getDescription().getId());
            if (nominalCode != null) return nominalCode;
        }
        Integer departmentId = null;
        Department department = null;
        // a jobitem can have one procedure but a procedure can belong to
        // multiple departments. If this procedure does belong to more than one
        // category then for now we'll try and find the category that matches to
        // a department to which the current user belongs, if not then just
        // takes the first department from the first procedure.
        // This function will probably need to change once the way we map
        // required calibrations onto jobitems is updated
        if ((ji.getCapability() != null) && (ji.getCapability().getCategories() != null)) {
            if (ji.getCapability().getCategories().size() > 1) {
                for (CategorisedCapability catProc : ji.getCapability().getCategories()) {
                    // just take the first department and use that
                    departmentId = catProc.getCategory().getDepartment().getDeptid();
                    department = catProc.getCategory().getDepartment();
                    break;
                }
            } else if (ji.getCapability().getCategories().size() == 1) {
                departmentId = ji.getCapability().getCategories().get(0).getCategory().getDepartment().getDeptid();
                department = ji.getCapability().getCategories().get(0).getCategory().getDepartment();
            }
        }
        this.logger.info("Searching for nominal with caltype:" + ji.getCalType().getCalTypeId() + ", costtype:"
            + costTypeId + ", department:" + departmentId + ", for ledgers:" + ledgers);
        NominalCode nominal = nominalCodeDao.findBestMatchingNominalCode(ledgers, ji.getCalType(), costType,
            department);
        // if no nominal found using strict checking, then have a second attempt
        // at looking up the nominal cost and do so ignoring the calibration
        // type
        if (nominal == null) {
            this.logger.info("no nominal found, searching for nominal using loose calibration checking");
            nominal = this.nominalCodeDao.findBestMatchingNominalCode(ledgers, null, costType, department);
			if (nominal != null) {
				this.logger.info("nominal " + nominal.getCode() + " found using loose calibration checking");
			} else {
				this.logger.info(
						"no nominal found, searching for nominal using loose calibration and department checking");
				// if no nominal found using strict checking and ignoring the
				// calibration type, then have a third attempt at looking up the
				// nominal cost and do so ignoring the department.
				if (departmentId != null) {
					nominal = this.nominalCodeDao.findBestMatchingNominalCode(ledgers, null, costType, null);
					if (nominal != null) {
						this.logger.info("nominal " + nominal.getCode()
								+ " found using loose calibration and department checking");
					}
				}
			}
		}
		initialiseTranslatedTitle(nominal);
		return nominal;
	}

	@Override
	public NominalCode get(Integer id) {
		NominalCode nominal = this.nominalCodeDao.find(id);
		initialiseTranslatedTitle(nominal);
		return nominal;
	}

	@Override
	public NominalCode findNominalCodeByCode(String code) {
		NominalCode nominal = this.nominalCodeDao.findNominalCodeByCode(code);
		initialiseTranslatedTitle(nominal);
		return nominal;
	}

	@Override
	public List<NominalCode> getNominalCodes(List<Ledgers> ledgers) {
		List<NominalCode> codes = new ArrayList<NominalCode>();
		if ((ledgers != null) && (ledgers.size() > 0))
			codes = this.nominalCodeDao.getNominalCodes(ledgers);
		codes.sort(new NominalCodeComparator());
		codes.forEach(this::initialiseTranslatedTitle);
		return codes;
	}

	private void initialiseTranslatedTitle(NominalCode nominalCode) {
		if (nominalCode != null) {
			String translation = translationService.getCorrectTranslation(nominalCode.getTitleTranslations(),
					LocaleContextHolder.getLocale());
			if (translation != null)
				nominalCode.setTranslatedTitle(translation);
			else
				nominalCode.setTranslatedTitle(nominalCode.getTitle());
		}
	}
}