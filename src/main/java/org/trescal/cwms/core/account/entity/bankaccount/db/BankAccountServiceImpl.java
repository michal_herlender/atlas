package org.trescal.cwms.core.account.entity.bankaccount.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.account.entity.bankaccount.BankAccount;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;

@Service
public class BankAccountServiceImpl extends BaseServiceImpl<BankAccount, Integer>
		implements BankAccountService {
	
	@Autowired
	private BankAccountDao bankAccountDao;
	
	@Override
	protected BaseDao<BankAccount, Integer> getBaseDao() {
		return bankAccountDao;
	}
	
	@Override
	public List<BankAccount> searchByCompany(Company company) {
		return bankAccountDao.searchByCompany(company);
	}
	
	@Override
	public Boolean isUsedInCompanySettings(BankAccount bankAccount) {
		return bankAccountDao.isUsedInCompanySettings(bankAccount);
	}
}