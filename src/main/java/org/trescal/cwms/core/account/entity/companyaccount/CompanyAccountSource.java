package org.trescal.cwms.core.account.entity.companyaccount;

public enum CompanyAccountSource
{
	CUSTOM, EXISTING;
}
