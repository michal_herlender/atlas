package org.trescal.cwms.core.account.entity.creditcardtype.db;

import org.trescal.cwms.core.account.entity.creditcardtype.CreditCardType;
import org.trescal.cwms.core.audit.entity.db.BaseDao;

public interface CreditCardTypeDao extends BaseDao<CreditCardType, Integer> {}