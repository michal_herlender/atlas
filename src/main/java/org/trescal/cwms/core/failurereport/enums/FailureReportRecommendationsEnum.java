package org.trescal.cwms.core.failurereport.enums;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public enum FailureReportRecommendationsEnum {
	CALIBRATION_WITH_JUDGMENT("failurereport.recom.calibrationwithjudgment", true),
	CALIBRATION_WITHOUT_JUDGMENT("failurereport.recom.calibrationwithoutjudgment",true),
	ADJUSTMENT("failurereport.recom.adjustment", true), 
	REPAIR("failurereport.recom.repair",true),
	SCRAPPING_PROPOSAL("failurereport.recom.scrappingproposal", true),
	RESTRICTION("failurereport.recom.restriction",false), 			// disabled, waiting for TLM to update his version of FR
	RETURN_TO_CLIENT("failurereport.recom.return.to.client",false),	// added back on 2020-08-14 to match existing production clientoutcome values
	DISPENSATION("failurereport.recom.dispensation", false),		// added back on 2020-08-14 to match existing production clientoutcome values
	OTHER("failurereport.recom.other",true);

	private final String nameCode;
	private final boolean active;
	private MessageSource messageSource;

	private FailureReportRecommendationsEnum(String nameCode, boolean active) {
		this.nameCode = nameCode;
		this.active = active;
	}

	@Component
	public static class FailureReportRecommendationEnumMessageSourceInjector {
		@Autowired
		private MessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (FailureReportRecommendationsEnum opt : EnumSet.allOf(FailureReportRecommendationsEnum.class)) {
				opt.setMessageSource(this.messageSource);
			}
		}
	}

	public String getName() {
		return this.messageSource.getMessage(this.nameCode, null, this.name(), LocaleContextHolder.getLocale());
	}

	public boolean isActive() {
		return active;
	}

	public static EnumSet<FailureReportRecommendationsEnum> getActiveRecommendations() {

		EnumSet<FailureReportRecommendationsEnum> activeRecommendations = EnumSet.noneOf(FailureReportRecommendationsEnum.class);

		List<FailureReportRecommendationsEnum> activeRecommendatiosEnumSets = Arrays
				.asList(FailureReportRecommendationsEnum.values()).stream().filter(en -> {
					return en.isActive();
				}).collect(Collectors.toList());

		activeRecommendations.addAll(activeRecommendatiosEnumSets);

		return activeRecommendations;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
}
