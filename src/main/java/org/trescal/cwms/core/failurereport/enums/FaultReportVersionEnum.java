package org.trescal.cwms.core.failurereport.enums;

public enum FaultReportVersionEnum {
	FAULT_REPORT, FAILURE_REPORT;

	private FaultReportVersionEnum() {
	}
}
