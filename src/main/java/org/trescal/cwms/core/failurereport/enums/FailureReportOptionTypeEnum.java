package org.trescal.cwms.core.failurereport.enums;

public enum FailureReportOptionTypeEnum {
	AS_FOUND_OPTIONS, CURRENT_SITUATION_OPTIONS, OPERATED_BY_OPTIONS, OPERATED_IN_OPTIONS;

	private FailureReportOptionTypeEnum() {
	}
}
