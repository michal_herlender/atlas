package org.trescal.cwms.core.failurereport.controller;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.failurereport.enums.*;
import org.trescal.cwms.core.failurereport.form.FailureReportForm;
import org.trescal.cwms.core.failurereport.form.FailureReportForm.FaultReportFormAction;
import org.trescal.cwms.core.failurereport.form.validator.FailureReportValidator;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitemaction.JobItemAction;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateTimeEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.workflow.entity.stategroup.StateGroup;
import org.trescal.cwms.spring.model.KeyValue;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_COMPANY,
	Constants.SESSION_ATTRIBUTE_SUBDIV})
public class FailureReportController {

	private static final String ACTIVITY_FR_SENT_TO_CLIENT = "Fault Report sent to client";
	private static final String ACTIVITY_ONSITE_FR_SENT_TO_CLIENT = "Onsite - failure report sent to client";
	private static final String ACTIVITY_FR_AVAILABLE_TO_CLIENT = "Failure Report available";
	private static final String ACTIVITY_ONSITE_FR_AVAILABLE_TO_CLIENT = "Onsite - Failure Report available";

	@Autowired
	private FaultReportService faultReportService;
	@Autowired
	private FailureReportValidator validator;
	@Autowired
	private UserService userService;
	@Autowired
	private JobItemService jobItemService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private MessageSource messages;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDateTime.class, new CustomLocalDateTimeEditor(DateTimeFormatter.ISO_DATE_TIME));
	}

	@ModelAttribute("selectTimes")
	protected List<KeyValue<Integer, String>> generateSelectTimesList() {

		Locale locale = LocaleContextHolder.getLocale();
		String minutes = this.messages.getMessage("jobcost.mins", null, "Minutes", locale);
		String hour = this.messages.getMessage("jifaultreport.hr", null, "Hour", locale);
		String hours = this.messages.getMessage("jifaultreport.hrs", null, "Hours", locale);

		List<KeyValue<Integer, String>> selectList = new ArrayList<>();
		selectList.add(new KeyValue<>(0, "0 " + minutes));
		selectList.add(new KeyValue<>(5, "5 " + minutes));
		selectList.add(new KeyValue<>(10, "10 " + minutes));
		selectList.add(new KeyValue<>(15, "15 " + minutes));
		selectList.add(new KeyValue<>(20, "20 " + minutes));
		selectList.add(new KeyValue<>(30, "30 " + minutes));
		selectList.add(new KeyValue<>(45, "45 " + minutes));
		selectList.add(new KeyValue<>(60, "1 " + hour));
		selectList.add(new KeyValue<>(75, "1 " + hour + " 15 " + minutes));
		selectList.add(new KeyValue<>(90, "1 " + hour + " 30 " + minutes));
		selectList.add(new KeyValue<>(105, "1 " + hour + " 45 " + minutes));
		selectList.add(new KeyValue<>(120, "2 " + hours));
		selectList.add(new KeyValue<>(150, "2 " + hours + " 30 " + minutes));
		selectList.add(new KeyValue<>(180, "3 " + hours));
		selectList.add(new KeyValue<>(210, "3 " + hours + " 30 " + minutes));
		selectList.add(new KeyValue<>(240, "4 " + hours));
		selectList.add(new KeyValue<>(270, "4 " + hours + " 30 " + minutes));
		selectList.add(new KeyValue<>(300, "5 " + hours));
		return selectList;
	}

	@ModelAttribute("failureReportStates")
	public List<KeyValue<FailureReportStateEnum, String>> getStates() {
		return FailureReportStateEnum.getActiveStates().stream().map(e -> new KeyValue<>(e, e.getName())).collect(Collectors.toList());
	}

	@ModelAttribute("failureReportRecommendations")
	private List<KeyValue<FailureReportRecommendationsEnum, String>> getRecommendations() {
		return FailureReportRecommendationsEnum.getActiveRecommendations().stream().map(e -> new KeyValue<>(e, e.getName())).collect(Collectors.toList());
	}

	@RequestMapping(value = "/viewfailurereport.htm", method = RequestMethod.GET)
	public String viewFailureReport(Model model,
									@RequestParam(value = "failurereportid") Integer failurereportid) {

		FaultReport faultReport = this.faultReportService.findFaultReport(failurereportid);
		if (faultReport.getVersion().equals(FaultReportVersionEnum.FAULT_REPORT)) {
			model.addAttribute("faultReport", faultReport);
			return "trescal/core/failurereport/viewoldfaultreport";
		} else {
			model.addAttribute("failureReport", faultReport);
			model.addAttribute("allowEdit", true);
			// allow generation if the failure report has been validated
			boolean allowGeneration = BooleanUtils.isTrue(faultReport.getManagerValidation());
			model.addAttribute("allowGeneration", allowGeneration);
			boolean allowValidation = faultReport.isAwaitingFinalisation();
			model.addAttribute("allowValidation", allowValidation);
			model.addAttribute("hasStateOther", faultReport.getStates().contains(FailureReportStateEnum.OTHER));
			model.addAttribute("hasDispensation", BooleanUtils.isTrue(faultReport.getDispensation()));
			model.addAttribute("businessCompanyCurrency", faultReport.getBusinessCompanyCurrency());
			boolean showClientPart = faultReport.getClientResponseNeeded() != null
					&& faultReport.getClientResponseNeeded();
			model.addAttribute("showClientPart", showClientPart);
			model.addAttribute("showFinalOutcomePart", faultReport.getManagerValidation());
			model.addAttribute("canEditFinalOutcomePart",
					jobItemService.stateHasGroupOfKeyName(faultReport.getJobItem().getState(),
							StateGroup.AWAITING_SELECTION_OF_FR_OUTCOME) || showClientPart);

			return "trescal/core/failurereport/viewfailurereport";
		}
	}

	@RequestMapping(value = "/editfailurereport.htm", method = RequestMethod.GET)
	public String editFailureReport(Model model, @ModelAttribute("form") FailureReportForm form) {
		// For creating a new failure report (jobitemid parameter), states will be uninitialized, hence null check  
		model.addAttribute("hasStateOther", (form.getStates() != null) && 
			form.getStates().contains(FailureReportStateEnum.OTHER));
		Date currentDate = new Date();
		model.addAttribute("hasDispensation", BooleanUtils.isTrue(form.getDispensation()));
		if (form.getAction().equals(FailureReportForm.FaultReportFormAction.NEW)) {
			model.addAttribute("ji", jobItemService.findJobItem(form.getJobItemId()));
			model.addAttribute("allowValidation", false);
			model.addAttribute("showClientPart", false);
			model.addAttribute("showFinalOutcomePart", false);
			model.addAttribute("canEditFinalOutcomePart", false);
			model.addAttribute("minClientApprovalDate", currentDate);
		} else {
			FaultReport faultReport = this.faultReportService.findFaultReport(form.getFaultRepId());
			model.addAttribute("ji", faultReport.getJobItem());
			model.addAttribute("failureReport", faultReport);
			model.addAttribute("allowValidation", form.isAwaitingFinalisation());
			boolean showClientPart = faultReport.getClientResponseNeeded() != null
					&& faultReport.getClientResponseNeeded();
			model.addAttribute("showClientPart", showClientPart);
			model.addAttribute("showFinalOutcomePart", faultReport.getManagerValidation());
			model.addAttribute("canEditFinalOutcomePart",
					jobItemService.stateHasGroupOfKeyName(faultReport.getJobItem().getState(),
							StateGroup.AWAITING_SELECTION_OF_FR_OUTCOME) || showClientPart);
			Date dateWhenFrWasSentToClient = getDateWhenFrWasSentToClient(faultReport.getJobItem());
			model.addAttribute("currentDate", new Date());
			model.addAttribute("minClientApprovalDate",
					dateWhenFrWasSentToClient != null ? dateWhenFrWasSentToClient : currentDate);
		}

		return "trescal/core/failurereport/editfailurereport";
	}

	private Date getDateWhenFrWasSentToClient(JobItem ji) {
		return ji.getActions().stream()
			.filter(jia -> jia.getActivityDesc().equalsIgnoreCase(ACTIVITY_FR_AVAILABLE_TO_CLIENT)
				|| jia.getActivityDesc().equalsIgnoreCase(ACTIVITY_FR_SENT_TO_CLIENT)
				|| jia.getActivityDesc().equalsIgnoreCase(ACTIVITY_ONSITE_FR_AVAILABLE_TO_CLIENT)
				|| jia.getActivityDesc().equalsIgnoreCase(ACTIVITY_ONSITE_FR_SENT_TO_CLIENT)

			).map(JobItemAction::getEndStamp).findAny().orElse(null);
	}

	@RequestMapping(value = "/editfailurereport.htm", method = RequestMethod.POST)
	protected String onSubmit(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@Valid @ModelAttribute("form") FailureReportForm form, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return editFailureReport(model, form);
		} else {
			Contact currentContact = this.userService.get(username).getCon();
			Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
			// loop on jobitemactoin and see if they were any prior calibration activities
			this.faultReportService.createOrUpdateFailureReport(form, currentContact, allocatedSubdiv, false);
			model.asMap().clear();
			if (form.getAction().equals(FaultReportFormAction.NEW))
				return "redirect:jifaultreport.htm?jobitemid=" + form.getJobItemId();
			else
				return "redirect:viewfailurereport.htm?failurereportid=" + form.getFaultRepId();
		}
	}

	@ModelAttribute("form")
	public FailureReportForm formBackingObject(Model model,
			@RequestParam(value = "jobitemid", required = false, defaultValue = "0") Integer jobitemid,
			@RequestParam(value = "failurereportid", required = false, defaultValue = "0") Integer failurereportid) {

		FailureReportForm form = new FailureReportForm();
		if (jobitemid == 0 && failurereportid == 0) {
			throw new IllegalArgumentException("Either a jobitemid or failurereportid must be specified");
		}

		if (jobitemid != 0) {
			form.setAction(FaultReportFormAction.NEW);
			form.setJobItemId(jobitemid);
			form.setFaultRepId(0);
			form.setFrSource(FailureReportSourceEnum.CWMS);
		} else {
			FaultReport fr = faultReportService.findFaultReport(failurereportid);

			form.setFaultRepId(fr.getFaultRepId());
			form.setJobItemId(fr.getJobItem().getJobItemId());
			if (fr.getClientResponseNeeded() != null && fr.getClientResponseNeeded()) {
				// Can show / edit only the client response
				if (fr.getManagerValidation() != null && fr.getManagerValidation() && (fr.getSource() != null)
						&& fr.getSource().isBlockEditAfterValidation()) {
					form.setAction(FaultReportFormAction.EDIT_CLIENT_RESPONSE);
				} else {
					// Can edit all sections of failure report
					form.setAction(FaultReportFormAction.EDIT_FULL);
				}
			} else {
				// Cam edit only the first two sections, if no client response
				// needed
				form.setAction(FaultReportFormAction.EDIT_TECHNICIAN_MANAGER);
			}

			form.setAwaitingFinalisation(fr.isAwaitingFinalisation());

			form.setBusinessCompanyCurrency(fr.getBusinessCompanyCurrency());
			form.setValueInBusinessCompanyCurrency(fr.getValueInBusinessCompanyCurrency());
			form.setFrSource(fr.getSource());

			form.setStates(new ArrayList<>());
			form.getStates().addAll(fr.getStates());

			form.setOtherState(fr.getOtherState());
			form.setTechnicianComments(fr.getTechnicianComments());

			form.setRecommendations(new ArrayList<>());
			form.getRecommendations().addAll(fr.getRecommendations());

			form.setDispensationComments(fr.getDispensationComments());
			form.setDispensation(fr.getDispensation());
			form.setManagerValidation(fr.getManagerValidation());
			form.setEstAdjustTime(fr.getEstAdjustTime());
			form.setEstRepairTime(fr.getEstRepairTime());
			form.setManagerComments(fr.getManagerComments());
			form.setEstDeliveryTime(fr.getEstDeliveryTime());
			form.setOperationByTrescal(fr.getOperationByTrescal());
			form.setOperationInLaboratory(fr.getOperationInLaboratory());
			form.setClientResponseNeeded(fr.getClientResponseNeeded());
			form.setSendToClient(fr.getSendToClient());
			form.setClientAlias(fr.getClientAlias());

			form.setClientApproval(fr.getClientApproval());
			if (fr.getClientApprovalBy() != null) {
				// Pre-fill for contact search plug-in from existing approval
				form.setPersonid(fr.getClientApprovalBy().getPersonid());
				form.setSubdivid(fr.getClientApprovalBy().getSub().getSubdivid());
				form.setCoid(fr.getClientApprovalBy().getSub().getComp().getCoid());
			} else {
				// Default contact search plug-in fields from job contact
				// Only set if actually recording approval
				form.setPersonid(fr.getJobItem().getJob().getCon().getPersonid());
				form.setSubdivid(fr.getJobItem().getJob().getCon().getSub().getSubdivid());
				form.setCoid(fr.getJobItem().getJob().getCon().getSub().getComp().getCoid());
			}
			form.setClientApprovalOn(DateTools.dateToLocalDateTime(fr.getClientApprovalOn()));
			form.setClientComments(fr.getClientComments());
			form.setClientApprovalType(fr.getApprovalType());

			form.setOutcomepreselected(fr.getOutcomePreselected());
			form.setFinalOutcome(fr.getFinalOutcome());
		}

		List<FailureReportFinalOutcome> finalOutcomes = Arrays.stream(FailureReportFinalOutcome.values())
			.filter(o -> !o.isRetired()).collect(Collectors.toList());
		model.addAttribute("finaloutcomes", finalOutcomes);

		return form;
	}

}
