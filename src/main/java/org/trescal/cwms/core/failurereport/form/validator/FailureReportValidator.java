package org.trescal.cwms.core.failurereport.form.validator;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.failurereport.enums.FailureReportStateEnum;
import org.trescal.cwms.core.failurereport.form.FailureReportForm;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import javax.validation.groups.Default;

@Component
public class FailureReportValidator extends AbstractBeanValidator {
	public boolean supports(Class<?> clazz) {
		return clazz.equals(FailureReportForm.class);
	}

	public void validate(Object obj, Errors errors) {
		FailureReportForm form = (FailureReportForm) obj;

		super.validate(form, errors, Default.class);
		if (FailureReportForm.FaultReportFormAction.NEW.equals(form.getAction())
				|| FailureReportForm.FaultReportFormAction.EDIT_FULL.equals(form.getAction())
				|| FailureReportForm.FaultReportFormAction.EDIT_TECHNICIAN_MANAGER.equals(form.getAction())
				|| FailureReportForm.FaultReportFormAction.VALIDATE.equals(form.getAction())) {
			super.validate(form, errors, FailureReportForm.TechnicianStateGroup.class);

			if (form.getStates() != null && form.getStates().contains(FailureReportStateEnum.OTHER)) {
				super.validate(form, errors, FailureReportForm.OtherStateGroup.class);
			}

			if (BooleanUtils.isTrue(form.getDispensation())) {
				super.validate(form, errors, FailureReportForm.DispensationStateGroup.class);
			}
		}
		if (FailureReportForm.FaultReportFormAction.EDIT_CLIENT_RESPONSE.equals(form.getAction())
				|| FailureReportForm.FaultReportFormAction.EDIT_FULL.equals(form.getAction())) {
			super.validate(form, errors, FailureReportForm.ClientResponseStateGroup.class);
		}

		if (FailureReportForm.FaultReportFormAction.VALIDATE.equals(form.getAction())) {
			super.validate(form, errors, FailureReportForm.ValidationStateGroup.class);
		}
	}
}