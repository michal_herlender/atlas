package org.trescal.cwms.core.failurereport.enums;

public enum FailureReportSourceEnum {
	CWMS(false), TLM(true);
	
	private boolean blockEditAfterValidation;
	
	private FailureReportSourceEnum(boolean blockEditAfterValidation) {
		this.blockEditAfterValidation = blockEditAfterValidation;
	}

	public boolean isBlockEditAfterValidation() {
		return blockEditAfterValidation;
	}
}
