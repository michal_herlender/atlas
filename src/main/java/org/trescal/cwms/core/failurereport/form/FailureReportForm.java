package org.trescal.cwms.core.failurereport.form;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.trescal.cwms.core.company.interceptor.UnescapeString;
import org.trescal.cwms.core.failurereport.enums.*;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class FailureReportForm {
    // ID of fault report being edited (0 if new)
    private Integer faultRepId;
    // ID of job item, used for new fault report creation (don't store job item
    // in form)
    private Integer jobItemId;
    private FaultReportFormAction action;
    private boolean awaitingFinalisation;
    private SupportedCurrency businessCompanyCurrency;
    private FailureReportSourceEnum frSource;

    private List<FailureReportStateEnum> states;
    private String otherState;
    private String technicianComments;
    private Integer estAdjustTime;
    private Integer estRepairTime;
    private BigDecimal valueInBusinessCompanyCurrency;

    // manager / validation selection
    private String managerComments;
    private Boolean managerValidation;
    private Integer estDeliveryTime;
    private Boolean operationByTrescal;
    private Boolean operationInLaboratory;
    private Boolean clientResponseNeeded;
    private Boolean sendToClient;
    private List<FailureReportRecommendationsEnum> recommendations;
    private Boolean dispensation;
    private String dispensationComments;

    // Client approval fields
    private Boolean clientApproval;
    private FailureReportRecommendationsEnum clientOutcome;
    private LocalDateTime clientApprovalOn;
    private String clientComments;
    private FailureReportApprovalTypeEnum clientApprovalType;
    private String clientAlias;

    // client approval id fields, prefilled into for contact search plugin
    private Integer personid;
    private Integer subdivid;
    private Integer coid;

    // outcomepreselected
    private Boolean outcomepreselected;

    private FailureReportFinalOutcome finalOutcome;

    public enum FaultReportFormAction {
        NEW, // New report, just the technician + manager section
        EDIT_TECHNICIAN_MANAGER, // Edit just the technician + manager section
        EDIT_CLIENT_RESPONSE, // Edit just the client response section
        EDIT_FULL, // Edit all sections
        VALIDATE // Edit just the technician + manager section with validation
    }

    public interface TechnicianStateGroup {
    } // When recording/updating technician information

    public interface OtherStateGroup {
    } // When state = FailureReportStateEnum.OTHER

    public interface ValidationStateGroup {
    } // When recording manager validation

    public interface ClientResponseStateGroup {
    } // When recording client response

    public interface DispensationStateGroup {
    } // When recommendations includes dispensation (aka restriction)

    // 0 indicates new fault report to be created
    @NotNull
    public Integer getFaultRepId() {
        return faultRepId;
    }

    // related job item id (used for create, but always present)
    @NotNull
    public Integer getJobItemId() {
        return jobItemId;
    }

    // Type of update being performed
    @NotNull
    public FaultReportFormAction getAction() {
        return action;
    }

    // Must always have (at least one) state
	@NotNull(message = "error.value.notselected", groups = { TechnicianStateGroup.class })
	@Size(min = 1, message = "error.value.notselected", groups = { TechnicianStateGroup.class })
	public List<FailureReportStateEnum> getStates() {
		return states;
	}

	// Comments mandatory for FailureReportStateEnum.OTHER
	@NotEmpty(groups = { OtherStateGroup.class })
	@Length(max = 100)
	public String getOtherState() {
		return otherState;
	}

	// Always optional, but should never exceed database size
	@Length(max = 2000)
	public String getTechnicianComments() {
		return technicianComments;
	}

	// Mandatory for validated failure report (default 0 minutes)
	@NotNull(groups = { ValidationStateGroup.class })
	public Integer getEstAdjustTime() {
		return estAdjustTime;
	}

	// Mandatory for validated failure report (default 0 minutes)
	@NotNull(groups = { ValidationStateGroup.class })
	public Integer getEstRepairTime() {
		return estRepairTime;
	}

	// Always optional (cost of repair parts)
	public BigDecimal getValueInBusinessCompanyCurrency() {
		return valueInBusinessCompanyCurrency;
	}

	// Always optional, but should never exceed database size
	@Length(max = 200)
	public String getManagerComments() {
		return managerComments;
	}

	// Mandatory for validated failure report
	@NotNull(message = "error.value.notselected", groups = { ValidationStateGroup.class })
	public Integer getEstDeliveryTime() {
		return estDeliveryTime;
	}

	// Mandatory for validated failure report
	@NotNull(message = "error.value.notselected", groups = { ValidationStateGroup.class })
	public Boolean getOperationByTrescal() {
		return operationByTrescal;
	}

	// Mandatory for validated failure report
	@NotNull(message = "error.value.notselected", groups = { ValidationStateGroup.class })
	public Boolean getOperationInLaboratory() {
		return operationInLaboratory;
	}

	// Mandatory for validated failure report
	@NotNull(message = "error.value.notselected", groups = { ValidationStateGroup.class })
	public Boolean getClientResponseNeeded() {
		return clientResponseNeeded;
	}

	// Mandatory for validated failure report
	@NotNull(message = "error.value.notselected", groups = { ValidationStateGroup.class })
	public Boolean getSendToClient() {
		return sendToClient;
	}

	// Mandatory (at least one) for validated failure report
	@NotNull(groups = { ValidationStateGroup.class })
	@Size(min = 1, message = "error.value.notselected", groups = { ValidationStateGroup.class })
	public List<FailureReportRecommendationsEnum> getRecommendations() {
		return recommendations;
	}

	@NotEmpty(groups = { DispensationStateGroup.class })
    @Length(max = 100)
    public String getDispensationComments() {
        return dispensationComments;
    }

    @NotNull(groups = {ClientResponseStateGroup.class})
    public Boolean getClientApproval() {
        return clientApproval;
    }

    @NotNull(groups = {ClientResponseStateGroup.class})
    public LocalDateTime getClientApprovalOn() {
        return clientApprovalOn;
    }

    // Always optional
    @Length(max = 500)
    public String getClientComments() {
        return clientComments;
    }

    public Boolean getOutcomepreselected() {
        return outcomepreselected;
    }

	public Integer getPersonid() {
		return personid;
	}

	public String getClientAlias() {
		return clientAlias;
	}

	// For contact search plug in - client approval - personid validated
	public Integer getSubdivid() {
		return subdivid;
	}

	// For contact search plug in - client approval - personid validated
	public Integer getCoid() {
		return coid;
	}

	public boolean isAwaitingFinalisation() {
		return awaitingFinalisation;
	}

	public Boolean getManagerValidation() {
		return managerValidation;
	}

	public SupportedCurrency getBusinessCompanyCurrency() {
		return businessCompanyCurrency;
	}

	public FailureReportSourceEnum getFrSource() {
		return frSource;
	}

	public FailureReportRecommendationsEnum getClientOutcome() {
		return clientOutcome;
	}

	public Boolean getDispensation() {
		return dispensation;
	}

	// Setters after here

	public void setDispensation(Boolean dispensation) {
		this.dispensation = dispensation;
	}

	public void setOutcomepreselected(Boolean outcomepreselected) {
		this.outcomepreselected = outcomepreselected;
	}

	public FailureReportApprovalTypeEnum getClientApprovalType() {
		return clientApprovalType;
	}

	public void setFaultRepId(Integer faultRepId) {
		this.faultRepId = faultRepId;
	}

	public void setAction(FaultReportFormAction action) {
		this.action = action;
	}

	public void setStates(List<FailureReportStateEnum> states) {
		this.states = states;
	}

	@UnescapeString
	public void setOtherState(String otherState) {
		this.otherState = otherState;
	}

	@UnescapeString
	public void setTechnicianComments(String technicianComments) {
		this.technicianComments = technicianComments;
	}

	public void setEstAdjustTime(Integer estAdjustTime) {
		this.estAdjustTime = estAdjustTime;
	}

	public void setEstRepairTime(Integer estRepairTime) {
		this.estRepairTime = estRepairTime;
	}

	public void setValueInBusinessCompanyCurrency(BigDecimal valueInBusinessCompanyCurrency) {
		this.valueInBusinessCompanyCurrency = valueInBusinessCompanyCurrency;
	}

	@UnescapeString
	public void setManagerComments(String managerComments) {
		this.managerComments = managerComments;
	}

	public void setEstDeliveryTime(Integer estDeliveryTime) {
		this.estDeliveryTime = estDeliveryTime;
	}

	public void setOperationByTrescal(Boolean operationByTrescal) {
		this.operationByTrescal = operationByTrescal;
	}

	public void setOperationInLaboratory(Boolean operationInLaboratory) {
		this.operationInLaboratory = operationInLaboratory;
    }

    public void setClientResponseNeeded(Boolean clientResponseNeeded) {
        this.clientResponseNeeded = clientResponseNeeded;
    }

    public void setRecommendations(List<FailureReportRecommendationsEnum> recommendations) {
        this.recommendations = recommendations;
    }

    @UnescapeString
    public void setDispensationComments(String dispensationComments) {
        this.dispensationComments = dispensationComments;
    }

    public void setClientApproval(Boolean clientApproval) {
        this.clientApproval = clientApproval;
    }

    public void setClientApprovalOn(LocalDateTime clientApprovalOn) {
        this.clientApprovalOn = clientApprovalOn;
    }

    @UnescapeString
    public void setClientComments(String clientComments) {
        this.clientComments = clientComments;
    }

    public void setSendToClient(Boolean sendToClient) {
        this.sendToClient = sendToClient;
    }

    public void setClientApprovalType(FailureReportApprovalTypeEnum clientApprovalType) {
        this.clientApprovalType = clientApprovalType;
    }

    public void setPersonid(Integer personid) {
        this.personid = personid;
    }

	public void setAwaitingFinalisation(boolean awaitingFinalisation) {
		this.awaitingFinalisation = awaitingFinalisation;
	}

	public void setManagerValidation(Boolean managerValidation) {
		this.managerValidation = managerValidation;
	}

	public void setBusinessCompanyCurrency(SupportedCurrency businessCompanyCurrency) {
		this.businessCompanyCurrency = businessCompanyCurrency;
	}

	public void setFrSource(FailureReportSourceEnum frSource) {
		this.frSource = frSource;
	}

	public void setClientOutcome(FailureReportRecommendationsEnum clientOutcome) {
		this.clientOutcome = clientOutcome;
	}

	public void setJobItemId(Integer jobItemId) {
		this.jobItemId = jobItemId;
	}

	public void setSubdivid(Integer subdivid) {
		this.subdivid = subdivid;
	}

	public void setCoid(Integer coid) {
		this.coid = coid;
	}

	public FailureReportFinalOutcome getFinalOutcome() {
		return finalOutcome;
	}

	public void setFinalOutcome(FailureReportFinalOutcome finalOutcome) {
		this.finalOutcome = finalOutcome;
	}

	@UnescapeString
	public void setClientAlias(String clientAlias) {
		this.clientAlias = clientAlias;
	}

}