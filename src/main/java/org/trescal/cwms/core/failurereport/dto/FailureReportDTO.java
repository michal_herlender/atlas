package org.trescal.cwms.core.failurereport.dto;

import java.util.Date;

import org.trescal.cwms.core.failurereport.enums.FailureReportApprovalTypeEnum;
import org.trescal.cwms.core.failurereport.enums.FailureReportFinalOutcome;

public class FailureReportDTO {

	private Integer id;
	private String number;
	private Date issueDate;
	private String issuedBy;
	private Date validatedOn;
	private String validatedBy;
	private Boolean clientReponseNeeded;
	private Date clientApprovedon;
	private FailureReportApprovalTypeEnum approvalType;
	private FailureReportFinalOutcome finalOutcome;

	public FailureReportDTO(Integer id, String number, Date issueDate, String issuedBy, Date validatedOn,
			String validatedBy, Boolean clientReponseNeeded, Date clientApprovedon,
			FailureReportApprovalTypeEnum approvalType, FailureReportFinalOutcome finalOutcome) {
		super();
		this.id = id;
		this.number = number;
		this.issueDate = issueDate;
		this.issuedBy = issuedBy;
		this.validatedOn = validatedOn;
		this.validatedBy = validatedBy;
		this.clientReponseNeeded = clientReponseNeeded;
		this.clientApprovedon = clientApprovedon;
		this.approvalType = approvalType;
		this.finalOutcome = finalOutcome;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public String getIssuedBy() {
		return issuedBy;
	}

	public void setIssuedBy(String issuedBy) {
		this.issuedBy = issuedBy;
	}

	public Date getValidatedOn() {
		return validatedOn;
	}

	public void setValidatedOn(Date validatedOn) {
		this.validatedOn = validatedOn;
	}

	public String getValidatedBy() {
		return validatedBy;
	}

	public void setValidatedBy(String validatedBy) {
		this.validatedBy = validatedBy;
	}

	public Boolean getClientReponseNeeded() {
		return clientReponseNeeded;
	}

	public void setClientReponseNeeded(Boolean clientReponseNeeded) {
		this.clientReponseNeeded = clientReponseNeeded;
	}

	public Date getClientApprovedon() {
		return clientApprovedon;
	}

	public void setClientApprovedon(Date clientApprovedon) {
		this.clientApprovedon = clientApprovedon;
	}

	public FailureReportApprovalTypeEnum getApprovalType() {
		return approvalType;
	}

	public void setApprovalType(FailureReportApprovalTypeEnum approvalType) {
		this.approvalType = approvalType;
	}

	public FailureReportFinalOutcome getFinalOutcome() {
		return finalOutcome;
	}

	public void setFinalOutcome(FailureReportFinalOutcome finalOutcome) {
		this.finalOutcome = finalOutcome;
	}

}
