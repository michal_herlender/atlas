package org.trescal.cwms.core.failurereport.enums;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public enum FailureReportFinalOutcome {

	
	// kept for backward compatibility, whenever found should be migrated to new ones
	 ERP_CALIBRATION("Calibration","actionoutcome.faultreport.erpcalibration" ,true),
	 ERP_REPAIR("Repair","actionoutcome.faultreport.repair" ,true),
	 ERP_ADJUSTMENT("Adjustment","actionoutcome.faultreport.inhouseadjustment" ,true),
	 THIRD_PARTY("Third party","actionoutcome.faultreport.thirdparty" ,true),
	 RESTRICTION("Restriction","actionoutcome.faultreport.restriction" ,true),
	 
	 // new outcomes
	INTERNAL_CALIBRATION_WITH_JUDGMENT("Internal calibration with judgment",
			"actionoutcome.faultreport.inhouse.calibration.with.judgment" ,false),
	INTERNAL_CALIBRATION_WITHOUT_JUDGMENT("Internal calibration without judgment",
			"actionoutcome.faultreport.inhouse.calibration.without.judgment" ,false),
	THIRD_PARTY_CALIBRATION_WITH_JUDGMENT("Third party calibration with judgment",
			"actionoutcome.faultreport.thirdparty.calibration.with.judgment" ,false),
	THIRD_PARTY_CALIBRATION_WITHOUT_JUDGMENT("Third party calibration without judgment",
			"actionoutcome.faultreport.thirdparty.calibration.without.judgment" ,false),
	INTERNAL_REPAIR("Internal repair", "actionoutcome.faultreport.inhouse.repair" ,false),
	THIRD_PARTY_REPAIR("Third party repair", "actionoutcome.faultreport.thirdparty.repair" ,false),
	SCRAP("Scrap", "actionoutcome.faultreport.scrap" ,false),
	RETURN_TO_CLIENT("Return to client", "actionoutcome.faultreport.returntoclient" ,false),
	SPARE_PARTS_PROCUREMENT("Spare parts procurement", "actionoutcome.faultreport.spareparts.procurement" ,false);

	private String description;
	private String messageCode;
	private boolean retired;
	private MessageSource messageSource;

	private FailureReportFinalOutcome(String description, String messageCode, boolean retired) {
		this.description = description;
		this.messageCode = messageCode;
		this.retired = retired;
	}
	
	@Component
	public static class FailureReportFinalOutcomeMessageSourceInjector {
		@Autowired
		private MessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (FailureReportFinalOutcome opt : EnumSet.allOf(FailureReportFinalOutcome.class)) {
				opt.setMessageSource(this.messageSource);
			}
		}
	}

	public String getDescription() {
		return description;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public String getTranslation() {
		return getTranslationForLocale(LocaleContextHolder.getLocale());
	}

	public String getTranslationForLocale(Locale locale) {
		return messageSource.getMessage(messageCode, null, description, locale);
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public boolean isRetired() {
		return this.retired;
	}

	public void setRetired(boolean retired) {
		this.retired = retired;
	}

}
