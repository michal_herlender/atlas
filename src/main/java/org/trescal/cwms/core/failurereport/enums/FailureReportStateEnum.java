package org.trescal.cwms.core.failurereport.enums;

import java.util.EnumSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public enum FailureReportStateEnum {

	OUT_OF_SPECIFICATION("failurereport.state.outofspecification", true, false), 
	OUT_OF_SERVICE("failurereport.state.outofservice", true, false), 
	FAILURE_NOT_COMMUNICATED("failurereport.state.failurenotcommunicated", true, false), 
	SAFETY_DEFAULT("failurereport.state.safetydefault", true, false), 
	UNKNOWN_MPES("failurereport.state.unknownmpes", true, true), 
	EQUIPMENT_QUALITY_INADEQUATE("failurereport.state.qualityinadequate", true, true), 
	MISSING_ACCESSORIES("failurereport.state.missingaccessories", false, true), 
	OTHER("failurereport.state.other", true, true);

	private final boolean active;
	private final boolean indeterminate;
	private final String nameCode;
	private MessageSource messageSource;

	private FailureReportStateEnum(String nameCode, boolean active, boolean indeterminate) {
		this.nameCode = nameCode;
		this.active = active;
		this.indeterminate = indeterminate;
	}

	@Component
	public static class FailureReportStateEnumMessageSourceInjector {
		@Autowired
		private MessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (FailureReportStateEnum opt : EnumSet.allOf(FailureReportStateEnum.class)) {
				opt.setMessageSource(messageSource);
			}
		}
	}
	
	public static Set<FailureReportStateEnum> getActiveStates() {
		Set<FailureReportStateEnum> results = 
			EnumSet.allOf(FailureReportStateEnum.class).stream()
			.filter(outcome -> outcome.isActive())
			.collect(Collectors.toCollection(TreeSet::new));
		return results;
	}

	public String getName() {
		return messageSource.getMessage(nameCode, null, this.name(), LocaleContextHolder.getLocale());
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public boolean isIndeterminate() {
		return indeterminate;
	}

	public boolean isActive() {
		return active;
	}

}
