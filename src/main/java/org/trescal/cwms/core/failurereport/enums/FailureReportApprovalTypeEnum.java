package org.trescal.cwms.core.failurereport.enums;

public enum FailureReportApprovalTypeEnum {
	NOT_NECESSARY, 
	SIGNATURE,
	EMAIL,
	ADVESO;

	private FailureReportApprovalTypeEnum() {
	}
}
