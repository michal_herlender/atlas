package org.trescal.cwms.core.failurereport.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.filenamingservice.FailureReportFileNamingService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.FaultReport;
import org.trescal.cwms.core.jobs.jobitem.entity.faultreport.db.FaultReportService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;

@Controller
@IntranetController
@SessionAttributes(Constants.HTTPSESS_NEW_FILE_LIST)
public class FailureReportDocumentController {
	@Autowired
	private ComponentDirectoryService compDirService;
	@Autowired
	private DocumentService documentService;
	@Autowired
	private FaultReportService faultReportService;
	@Autowired
	private SystemComponentService scService;

	@RequestMapping(path = "birtfailurereport.htm")
	public String generateDocument(Locale locale,
			@ModelAttribute(name = Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList,
			@RequestParam(name = "faultreportid", required = true) Integer faultReportId) throws Exception {
		FaultReport fr = this.faultReportService.findFaultReport(faultReportId);
		String jobNumber = fr.getJobItem().getJob().getJobno();
		Integer jobid = fr.getJobItem().getJob().getJobid();

		String directory = this.compDirService
				.getDirectory(this.scService.findComponent(Component.FAILURE_REPORT), jobNumber).getAbsolutePath();

		FailureReportFileNamingService fnService = new FailureReportFileNamingService(fr, directory,
				fr.getClientApproval() != null);

		Document doc = documentService.createBirtDocument(faultReportId, BaseDocumentType.FAILURE_REPORT_2014, locale,
				Component.FAILURE_REPORT, fnService);
		documentService.addFileNameToSession(doc, sessionNewFileList);

		return "redirect:viewjob.htm?jobid=" + jobid + "&loadtab=jobfiles-tab";
	}
}
