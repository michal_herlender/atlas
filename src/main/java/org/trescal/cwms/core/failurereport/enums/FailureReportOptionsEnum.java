package org.trescal.cwms.core.failurereport.enums;

import java.util.EnumSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public enum FailureReportOptionsEnum {
	TRESCAL("", FailureReportOptionTypeEnum.OPERATED_BY_OPTIONS), 
	QUALIFIED_SUPPLIER("",FailureReportOptionTypeEnum.OPERATED_BY_OPTIONS),
	LABORATORY("",FailureReportOptionTypeEnum.OPERATED_IN_OPTIONS), 
	CUSTOMER_SITE("",FailureReportOptionTypeEnum.OPERATED_IN_OPTIONS);

	private final String nameCode;
	private final FailureReportOptionTypeEnum optionType;
	private MessageSource messageSource;

	private FailureReportOptionsEnum(String nameCode, FailureReportOptionTypeEnum optionType) {
		this.nameCode = nameCode;
		this.optionType = optionType;
	}

	@Component
	public static class FailureReportOptionsEnumMessageSourceInjector {
		@Autowired
		private MessageSource messageSource;

		@PostConstruct
		public void postConstruct() {
			for (FailureReportOptionsEnum opt : EnumSet.allOf(FailureReportOptionsEnum.class)) {
				opt.setMessageSource(this.messageSource);
			}
		}
	}

	public String getName() {
		return this.messageSource.getMessage(this.nameCode, null, LocaleContextHolder.getLocale());
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
}
