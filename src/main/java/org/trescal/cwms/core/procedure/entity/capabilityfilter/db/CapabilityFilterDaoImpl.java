package org.trescal.cwms.core.procedure.entity.capabilityfilter.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter;

@Repository
public class CapabilityFilterDaoImpl extends BaseDaoImpl<CapabilityFilter, Integer> implements CapabilityFilterDao {
    @Override
    protected Class<CapabilityFilter> getEntity() {
        return CapabilityFilter.class;
    }
}
