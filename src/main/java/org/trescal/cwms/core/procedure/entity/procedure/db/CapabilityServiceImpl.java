package org.trescal.cwms.core.procedure.entity.procedure.db;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.dto.SimpleContact;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.procedure.dto.CapabilityAccreditationInfoDto;
import org.trescal.cwms.core.procedure.dto.CapabilityPopUpDto;
import org.trescal.cwms.core.procedure.dto.CapabilityProjectionDTO;
import org.trescal.cwms.core.procedure.dto.CapabilityShorDto;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilterType;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.db.CapabilityFilterService;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.CapabilityInformation;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.db.CapabilityCategoryService;
import org.trescal.cwms.core.procedure.entity.workinstruction.db.WorkInstructionService;
import org.trescal.cwms.core.procedure.form.EditCapabilityForm;
import org.trescal.cwms.core.procedure.form.RecategoriseCapabilitiesForm;
import org.trescal.cwms.core.procedure.form.SearchCapabilitiesForm;
import org.trescal.cwms.core.system.entity.accreditedlab.db.AccreditedLabService;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.calibrationtype.dto.CalibrationTypeOptionDto;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.spring.model.LabelIdDTO;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service("CapabilityService")
@Slf4j
public class CapabilityServiceImpl extends BaseServiceImpl<Capability, Integer> implements CapabilityService {

    @Autowired
    private AccreditedLabService accreditedLabService;
    @Autowired
    private AddressService addressService;
    @Autowired
    private CalibrationTypeService calibrationTypeService;
    @Autowired
    private CalibrationProcessService calibrationProcessService;
    @Autowired
    private ComponentDirectoryService componentDirectoryService;
    @Autowired
    private NewDescriptionService descriptionService;
    @Autowired
    private CapabilityDao capabilityDao;
    @Autowired
    private CapabilityCategoryService capabilityCategoryService;
    @Autowired
    private SystemComponentService systemComponentService;
    @Autowired
    private WorkInstructionService workInstructionService;
    @Autowired
    private CompanyService companyService;

    @Override
    protected BaseDao<Capability, Integer> getBaseDao() {
        return capabilityDao;
    }

    @Override
    public Capability createCapability(EditCapabilityForm form) {
        Capability capability = new Capability();
        capability.setActive(form.getActive());
        capability.setCategories(new ArrayList<>());

        updateCapabilityFields(capability, form);
        this.save(capability);
        return capability;
    }

    @Override
    public void editCapability(EditCapabilityForm form, Contact contact) {
        Capability capability = this.get(form.getCapabilityId());
        if (form.getActive() != capability.isActive()) {
            capability.setActive(form.getActive());
            // if the proc is not active then it has just been deactivated, so
            // now set the deactivated date + user
            if (!capability.isActive()) {
                capability.setDeactivatedBy(contact);
                capability.setDeactivatedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
            } else {
                // clear the deactivated on + by fields
                capability.setDeactivatedBy(null);
                capability.setDeactivatedOn(null);
            }
        }
        updateCapabilityFields(capability, form);
    }

    private void updateCapabilityFields(Capability capability, EditCapabilityForm form) {
        // This allows for a capability to move between subdivisons
        // (expected for use with supplier capabilities only for now, though may
        // use for business company merges)
        Address defaultAddress = this.addressService
            .get(form.getSupplierCapability() ? form.getAddrid() : form.getDefAddressId());
        capability.setOrganisation(defaultAddress.getSub());
        capability.setDefAddress(defaultAddress);

        // We set the "business company" capability fields only for capabilities
        // belonging to business company
        // Otherwise, they are set to empty / null as needed (clears fields, if
        // converted)
        if (capability.getOrganisation().getComp().getCompanyRole() == CompanyRole.BUSINESS) {
            updateCategories(capability, form.getCategoryids());
            capability.setCalibrationProcess(
                form.getCalProcessId() == null ? null : this.calibrationProcessService.get(form.getCalProcessId()));
            capability.setDefWorkInstruction(form.getWorkInstructionId() == null ? null
                : this.workInstructionService.get(form.getWorkInstructionId()));
            capability.setEstimatedTime(form.getEstimatedTime());
            capability.setLabAccreditation(
                form.getAccreditedLabId() == 0 ? null : this.accreditedLabService.get(form.getAccreditedLabId()));
            capability.setReqType(form.getReqType());
        } else {
            capability.getCategories().clear();
            capability.setCalibrationProcess(null);
            capability.setDefWorkInstruction(null);
            capability.setEstimatedTime(null);
            capability.setLabAccreditation(null);
            capability.setReqType(WorkRequirementType.THIRD_PARTY);
        }
        capability.setDescription(form.getDescription());
        capability.setName(form.getName());
        capability.setReference(form.getReference());
        capability.setSubfamily(
            form.getSubfamilyId() == null ? null : this.descriptionService.findDescription(form.getSubfamilyId()));
    }

    private void updateCategories(Capability capability, Set<Integer> categoryids) {
        List<CategorisedCapability> deletes = new ArrayList<>();
        // Delete categories
        for (CategorisedCapability cp : capability.getCategories()) {
            if (!categoryids.contains(cp.getCategory().getId())) {
                deletes.add(cp);
            }
        }
        capability.getCategories().removeAll(deletes);
        Set<Integer> existingIds = capability.getCategories().stream().map(pc -> pc.getCategory().getId())
            .collect(Collectors.toSet());

        // Add categories
        for (Integer categoryid : categoryids) {
            if (!existingIds.contains(categoryid)) {
                CategorisedCapability cp = new CategorisedCapability();
                cp.setCapability(capability);
                cp.setCategory(this.capabilityCategoryService.get(categoryid));
                capability.getCategories().add(cp);
            }
        }
        // Changes are saved via cascade
    }

    @Override
    public Capability findDWRProcedure(int id) {
        return capabilityDao.findCapability(id).orElse(null);
    }

    @Override
    public Capability get(Integer id) {
        Capability proc = capabilityDao.find(id);
        // 2018-12-12 GB : Prevent directory lookup on repeated gets.
        if (proc != null && proc.getDirectory() == null) {
            proc.setDirectory((this.componentDirectoryService.getDirectory(this.systemComponentService.findComponent(Component.PROCEDURE),
                proc.getReference())));
        }
        return proc;
    }

    @Override
    public CapabilityInformation findCapabilityInfo() {
        return new CapabilityInformation(this.calibrationTypeService.getActiveProcedureCalTypes()
            .stream().map(ct -> new CalibrationTypeOptionDto(ct.getCalTypeId(), ct.getServiceType().getLongName())).collect(Collectors.toList())
        );
    }

    @Override
    public List<Capability> getAll(Subdiv subdiv) {
        return capabilityDao.getAll(subdiv);
    }

    @Override
    public List<Capability> getAll(CapabilityCategory category) {
        return capabilityDao.getAll(category);
    }

    @Override
    public List<Capability> getAllForDepartment(Integer deptId) {
        return capabilityDao.getAllForDepartment(deptId);
    }

    @Override
    public List<Capability> getAllForDomainAndSubdiv(Integer domainId, Subdiv subdiv) {
        return capabilityDao.getAllForDomainAndSubdiv(domainId, subdiv);
    }

    @Override
    public List<Capability> getAllCapabilitiesWithNoDepartment(Subdiv subdiv) {
        return capabilityDao.getAllCapabilitiesWithNoDepartment(subdiv);
    }

    @Override
    public List<Subdiv> getAllSubdivsWithCapabilities() {
        List<Subdiv> subdivsWithProcedures = capabilityDao.getAllOrganisationsWith();
        subdivsWithProcedures.sort(Comparator.comparing(Subdiv::getSubname));
        return subdivsWithProcedures;
    }

    @Override
    public List<CapabilityProjectionDTO> getCapabilityProjectionDtos(Collection<Integer> capabilityIds) {
        List<CapabilityProjectionDTO> result = Collections.emptyList();
        if (capabilityIds != null && !capabilityIds.isEmpty()) {
            result = capabilityDao.getCapabilityProjectionDtos(capabilityIds);
        }
        return result;
    }

    @Override
    public java.util.List<Capability> getCapabilityByReference(String reference, Boolean active, Integer ignoreProcId) {
        return capabilityDao.getCapabilitiesByReference(reference, active, ignoreProcId);
    }

    @Override
    public PagedResultSet<Capability> searchCapabilities(PagedResultSet<Capability> rs, SearchCapabilitiesForm form) {
        return capabilityDao.searchCapabilities(rs, form);
    }

    @Override
    public List<Capability> searchCapabilityBySubFamilyIdAndSubdivId(Integer subFamilyId, boolean searchWithoutSubfamily,
                                                                     Integer businessSubdivId) {
        return capabilityDao.searchCapabilityBySubFamilyIdAndSubdivId(subFamilyId, searchWithoutSubfamily, businessSubdivId);
    }

    @Override
    public List<CapabilityShorDto> searchProceduresHQL(String searchTerm, Integer subdivId, Boolean hideDactivated) {
        return capabilityDao.searchCapabilitiesList(searchTerm, subdivId, hideDactivated);
    }

    @Override
    public List<LabelIdDTO> getLabelIdList(String reference, String name, boolean andTrueOrFalse, int subdivId,
                                           Integer modelId, int maxResults, String jobtype) {
        List<CapabilityShorDto> searchResults = this.capabilityDao.searchCapabilitiesProjection(reference, name,
            andTrueOrFalse, subdivId, modelId, maxResults, jobtype);
        return searchResults.stream()
            .map(wrapper -> new LabelIdDTO(wrapper.getReference() + " - " + wrapper.getName(), wrapper.getId()))
            .collect(Collectors.toList());
    }

    /**
     * Updates all procedures selected as a boolean true in map to the new
     * procedure category, removing the old category if present (0 can be
     * passed, if updating uncategorised)
     */
    @Override
    public void recategoriseCapabilities(int oldCategoryId, RecategoriseCapabilitiesForm form) {
        CapabilityCategory newCategory = this.capabilityCategoryService.get(form.getProcedureCategoryId());
        if (newCategory == null)
            throw new UnsupportedOperationException("New category not found");
        for (int capabalityId : form.getProcedureIds().keySet()) {
            Boolean update = form.getProcedureIds().get(capabalityId);
            if (update != null && update) {
                Capability capability = this.capabilityDao.find(capabalityId);
                // If the existing old procedure category is referenced, update
                // it
                boolean updated = false;
                for (CategorisedCapability cp : capability.getCategories()) {
                    if (cp.getCategory().getId() == oldCategoryId) {
                        cp.setCategory(newCategory);
                        updated = true;
                    }
                }
                if (!updated) {
                    CategorisedCapability cp = new CategorisedCapability();
                    cp.setCategory(newCategory);
                    cp.setCapability(capability);
                    capability.getCategories().add(cp);
                }
                capability.setDefAddress(newCategory.getDepartment().getAddress());
            }
        }
    }

    @Override
    public Optional<Capability> getActiveCapabilityUsingCapabilityFilter(Integer subFamilyId, Integer capabilityFilterId,
                                                                         Integer businessSubdivId) {
        List<CapabilityFilterType> capabilityFilterTypes = new ArrayList<>();
        capabilityFilterTypes.add(CapabilityFilterType.YES);
        capabilityFilterTypes.add(CapabilityFilterType.MAYBE);
        return this.capabilityDao.getActiveCapabilityUsingCapabilityFilter(subFamilyId, capabilityFilterId,
            businessSubdivId, capabilityFilterTypes);
    }

    @Override
    public List<Triple<Integer, Integer, Capability>> getActiveCapabilitiesUsingListOfCapabilityFilter(
        MultiValuedMap<Integer, Integer> subFamilyCapabilityFilters, Integer businessSubdivId) {

        List<Triple<Integer, Integer, Capability>> res = new ArrayList<>();

        List<CapabilityFilterType> capabilityFilterTypes = new ArrayList<>();
        capabilityFilterTypes.add(CapabilityFilterType.YES);
        capabilityFilterTypes.add(CapabilityFilterType.MAYBE);

        List<CapabilityProjectionDTO> procs = this.capabilityDao.getActiveCapabilitiesUsingListOfCapabilityFilter(subFamilyCapabilityFilters,
            businessSubdivId, capabilityFilterTypes);

        for (CapabilityProjectionDTO proc : procs) {
            res.add(new ImmutableTriple<>(proc.getSubFamilyId(), proc.getServiceTypeId(),
                capabilityDao.find(proc.getProcId())));
        }

        return res;

    }

    @Override
    public Optional<CapabilityPopUpDto> findCapabilityForPopUp(Integer id) {
        return capabilityDao.findCapabilityForPopUp(id)
            .map(c -> c.withAccreditations(this.findAccreditationForPopUp(id)))
            ;
    }

    @Override
    public List<CapabilityAccreditationInfoDto> findAccreditationForPopUp(Integer id) {
        return capabilityDao.findAccreditationsForPopUp(id);
    }

    @Override
    public List<Capability> getCapabilities(Collection<Integer> capabilityIds, Integer subdivId) {
        return capabilityDao.getCapabilities(capabilityIds,subdivId);
    }

    @Override
    public SortedMap<Integer, String> getAutorizedContactFromCapabilities(Set<Integer> procsIds,Integer subdivId) {
        SortedMap<Integer, String> resultMap = new TreeMap<>();
        for(Capability cap :this.getCapabilities(procsIds,subdivId)){
            for(CapabilityAuthorization autho : cap.getAuthorizations()){
                if(autho.getAuthorizationFor().isActive() && !autho.getAccredLevel().equals(AccreditationLevel.NONE)){
                    resultMap.put(autho.getAuthorizationFor().getPersonid(),autho.getAuthorizationFor().getName());
                }
            }
        }
        return resultMap;
    }

    @Override
    public SortedMap<String, List<SimpleContact>> getCapabilitieInSubdivsOfBc(Set<Integer> procsIds, Integer companyId) {
        SortedMap<String, List<SimpleContact>> resultMap = new TreeMap<>();
        List<Integer> subdivIds =this.companyService.get(companyId).getSubdivisions().stream().map(subdiv -> subdiv.getSubdivid()).collect(Collectors.toList());
        for(Capability cap :this.capabilityDao.getCapabilitieInSubdivsOfBc(procsIds,subdivIds)){
            List<SimpleContact> contacts = new ArrayList<SimpleContact>();
            for(CapabilityAuthorization autho : cap.getAuthorizations()){
                if(autho.getAuthorizationFor().isActive() && !autho.getAccredLevel().equals(AccreditationLevel.NONE)){
                    //resultMap.put(autho.getAuthorizationFor().getPersonid(),autho.getAuthorizationFor().getName());
                    contacts.add(new SimpleContact(autho.getAuthorizationFor().getName(),autho.getAuthorizationFor().getPersonid()));
                }
            }
            resultMap.put(cap.getOrganisation().getSubname(),contacts);
        }
        return resultMap;
    }


}

