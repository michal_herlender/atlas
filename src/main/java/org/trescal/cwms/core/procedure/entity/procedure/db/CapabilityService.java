package org.trescal.cwms.core.procedure.entity.procedure.db;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.tuple.Triple;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.dto.SimpleContact;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.procedure.dto.CapabilityAccreditationInfoDto;
import org.trescal.cwms.core.procedure.dto.CapabilityPopUpDto;
import org.trescal.cwms.core.procedure.dto.CapabilityProjectionDTO;
import org.trescal.cwms.core.procedure.dto.CapabilityShorDto;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.CapabilityInformation;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.form.EditCapabilityForm;
import org.trescal.cwms.core.procedure.form.RecategoriseCapabilitiesForm;
import org.trescal.cwms.core.procedure.form.SearchCapabilitiesForm;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.LabelIdDTO;

import java.util.*;

public interface CapabilityService extends BaseService<Capability, Integer> {

    Capability createCapability(EditCapabilityForm form);

    void editCapability(EditCapabilityForm form, Contact contact);

    /**
     * Returns a {@link Capability} entity for the dwr tooltip
     *
     * @param id is the procedure id
     * @return matching {@link Capability} entity
     */
    Capability findDWRProcedure(int id);

    CapabilityInformation findCapabilityInfo();

    /**
     * Returns a {@link List} of {@link CapabilityShorDto} whose
     * (case-insensitive) name or reference number match a {@link Capability}
     *
     * @return {@link List} of {@link CapabilityShorDto}.
     */
    List<CapabilityShorDto> searchProceduresHQL(String searchTerm, Integer subdivId, Boolean hideDactivated);

    List<Capability> getAll(Subdiv subdiv);

    List<Capability> getAll(CapabilityCategory category);

    List<Capability> getAllForDomainAndSubdiv(Integer domainId, Subdiv subdiv);

    List<Capability> getAllForDepartment(Integer deptId);

    List<CapabilityProjectionDTO> getCapabilityProjectionDtos(Collection<Integer> capabilityIds);

    /**
     * Returns a list of {@link Capability} entities that have no {@link Department}
     * or {@link CapabilityCategory}. Ordered by procedure reference, then id.
     *
     * @return list of matching {@link Capability} entities.
     */
    List<Capability> getAllCapabilitiesWithNoDepartment(Subdiv subdiv);

    List<Subdiv> getAllSubdivsWithCapabilities();

    /**
     * Returns all {@link Capability} entities that have a refernece number matching
     * the given reference, case insensitive. Optional parameters can be included to
     * ignore a given procedure using it's id or ignore active or inactive
     * procedures.
     *
     * @param reference    the exact (but case insensitive) reference number to
     *                     search for.
     * @param active       detirmines if the {@link Capability}s being searched
     *                     should be active or not, null to ignore.
     * @param ignoreProcId the id of a {@link Capability} to ignore in this search,
     *                     null or 0 to exlcude this from the search.
     */
    List<Capability> getCapabilityByReference(String reference, Boolean active, Integer ignoreProcId);

    /**
     * Returns a {@link PagedResultSet} listing all {@link Capability} entities that
     * match the criteria on the {@link SearchCapabilitiesForm}.
     *
     * @param rs   the {@link PagedResultSet}.
     * @param form the {@link SearchCapabilitiesForm}.
     * @return {@link PagedResultSet}.
     */
    PagedResultSet<Capability> searchCapabilities(PagedResultSet<Capability> rs, SearchCapabilitiesForm form);

    /**
     * Returns a List<Procedure> of procedures (aka capabilities) matching the
     * specified subfamily. Meant for use in capability research, where the caller
     * will filter the results further.
     *
     * @param subFamilyId            - may be null, restrict to specified subfamily
     * @param searchWithoutSubfamily - includes only capabilities without subfamily
     *                               if true
     * @param businessSubdivId       - may be null, restrict to specified business
     *                               subdiv
     */
    List<Capability> searchCapabilityBySubFamilyIdAndSubdivId(Integer subFamilyId, boolean searchWithoutSubfamily,
                                                              Integer businessSubdivId);

    /**
     * Returns a List<LabelIdDTO> list of tag results matching the search term
     * matchings (reference and/or name of procedure). Meant for use in jQuery
     * Autocomplete UI integration. boolean andTrueOrFalse - true - both reference
     * and name should match - false - either reference or name should match
     */
    List<LabelIdDTO> getLabelIdList(String reference, String name, boolean andTrueOrFalse, int subdivId,
                                    Integer modelId, int maxResults, String jobtype);

    /**
     * Moves procedures to a new category (inputs should have already been
     * validated)
     */
    void recategoriseCapabilities(int oldCategoryId, RecategoriseCapabilitiesForm form);

    Optional<Capability> getActiveCapabilityUsingCapabilityFilter(Integer subFamilyId, Integer capabilityFilterId,
                                                                  Integer businessSubdivId);

    List<Triple<Integer, Integer, Capability>> getActiveCapabilitiesUsingListOfCapabilityFilter(
        MultiValuedMap<Integer, Integer> subFamilyCapabilityFilters, Integer businessSubdivId);

    Optional<CapabilityPopUpDto> findCapabilityForPopUp(Integer id);

    List<CapabilityAccreditationInfoDto> findAccreditationForPopUp(Integer id);

    public List<Capability> getCapabilities(Collection<Integer> capabilityIds, Integer subdivId);

    public SortedMap<Integer, String> getAutorizedContactFromCapabilities(Set<Integer> procsIds, Integer subdivId);

    SortedMap<String, List<SimpleContact>> getCapabilitieInSubdivsOfBc(Set<Integer> procsIds, Integer companyId);
}