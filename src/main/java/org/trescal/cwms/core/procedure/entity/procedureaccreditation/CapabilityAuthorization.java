package org.trescal.cwms.core.procedure.entity.procedureaccreditation;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "capability_authorization", uniqueConstraints = {@UniqueConstraint(columnNames = {"authorizationFor", "caltypeid", "capabilityId", "accredlevel"})})
public class CapabilityAuthorization extends Auditable {
    private Contact authorizationFor;
    private AccreditationLevel accredLevel;
    private Contact awardedBy;
    private LocalDate awardedOn;
    private CalibrationType caltype;
    private int id;
    private Capability capability;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "authorizationFor")
    public Contact getAuthorizationFor() {
        return this.authorizationFor;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "accredlevel", nullable = false)
    public AccreditationLevel getAccredLevel() {
        return this.accredLevel;
    }

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "awardedby")
	public Contact getAwardedBy() {
		return this.awardedBy;
	}

	public void setAwardedBy(Contact awardedBy) {
		this.awardedBy = awardedBy;
	}

	@Column(name = "awarded", columnDefinition = "date")
	public LocalDate getAwardedOn() {
		return this.awardedOn;
	}

	public void setAwardedOn(LocalDate awardedOn) {
		this.awardedOn = awardedOn;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    public void setAuthorizationFor(Contact authorizationFor) {
        this.authorizationFor = authorizationFor;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "caltypeid")
    public CalibrationType getCaltype() {
        return this.caltype;
    }

    public void setCaltype(CalibrationType caltype) {
        this.caltype = caltype;
    }

    public void setAccredLevel(AccreditationLevel accredLevel) {
        this.accredLevel = accredLevel;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "capabilityId")
    public Capability getCapability() {
        return this.capability;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCapability(Capability capability) {
        this.capability = capability;
    }

}
