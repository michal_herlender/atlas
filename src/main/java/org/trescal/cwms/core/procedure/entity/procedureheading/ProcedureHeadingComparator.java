package org.trescal.cwms.core.procedure.entity.procedureheading;

import java.util.Comparator;

public class ProcedureHeadingComparator implements Comparator<CapabilityHeading> {

    @Override
    public int compare(CapabilityHeading heading0, CapabilityHeading heading1) {
        Integer result = null;
        if ((heading0.getHeading() != null) && (heading1.getHeading() != null)) {
            result = heading0.getHeading().compareTo(heading1.getHeading());
        }
        if (result == null || result == 0) {
            result = heading0.getId() - heading1.getId();
        }
        return result;
    }

}
