package org.trescal.cwms.core.procedure.entity.procedureheading.db;

import org.trescal.cwms.core.procedure.entity.procedureheading.CapabilityHeading;

import java.util.List;

public interface ProcedureHeadingService {
    void deleteProcedureHeading(CapabilityHeading procedureheading);

    CapabilityHeading findProcedureHeading(int id);

    /**
     * Returns a list of all {@link CapabilityHeading} entities sorted by
     * heading.
     *
     * @return {@link List} of {@link CapabilityHeading}.
     */
    List<CapabilityHeading> getAllProcedureHeadings();

    void insertProcedureHeading(CapabilityHeading procedureheading);

    void saveOrUpdateProcedureHeading(CapabilityHeading procedureheading);

    void updateProcedureHeading(CapabilityHeading procedureheading);
}