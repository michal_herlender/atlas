package org.trescal.cwms.core.procedure.entity.workinstruction.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.procedure.form.EditWorkInstructionForm;
import org.trescal.cwms.core.procedure.form.SearchWorkInstructionForm;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@Service("WorkInstructionService")
public class WorkInstructionServiceImpl extends BaseServiceImpl<WorkInstruction, Integer>
    implements WorkInstructionService {

    @Autowired
    private WorkInstructionDao workInstructionDao;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private NewDescriptionService newDescriptionService;
    @Autowired
    private UserService userService;

    @Override
    protected BaseDao<WorkInstruction, Integer> getBaseDao() {
        return workInstructionDao;
    }

    @Override
	public WorkInstruction updateWorkInstruction(EditWorkInstructionForm form, Integer coid, String username) {
		WorkInstruction workInstruction = form.getId() == 0 ? new WorkInstruction() : this.get(form.getId());
		workInstruction.setName(form.getName());
		workInstruction.setTitle(form.getTitle());
		workInstruction.setOrganisation(companyService.get(coid));
		workInstruction.setLastModified(new Timestamp(System.currentTimeMillis()));
		workInstruction.setLastModifiedBy(userService.get(username).getCon());
		if(form.getDescid() != null && form.getDescid() != 0 ){
            workInstruction.setDescription(newDescriptionService.findDescription(form.getDescid()));
        }
        // Note, updates are saved automatically via JPA
        if (form.getId() == 0) {
            this.save(workInstruction);
        }
        return workInstruction;
    }

    @Override
    public void searchWorkInstructions(PagedResultSet<WorkInstruction> pagedResults, SearchWorkInstructionForm form) {
        this.workInstructionDao.searchWorkInstructions(pagedResults, form);
    }

    @Override
    public List<KeyValueIntegerString> getAllKeyValues(String workInstructionFragment, Integer descriptionId,
                                                       Locale searchLanguage, int maxResults) {
        return this.workInstructionDao.getAllKeyValues(workInstructionFragment, descriptionId, searchLanguage, maxResults);
    }

    @Override
    public Set<KeyValue<Integer, String>> getAllAsKeyeValues() {
        return workInstructionDao.getAllAsKeyValues();
    }

}