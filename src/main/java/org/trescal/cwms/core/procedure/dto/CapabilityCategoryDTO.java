package org.trescal.cwms.core.procedure.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class CapabilityCategoryDTO {
    private int id;
    private String name;
    private String description;
}
