package org.trescal.cwms.core.procedure.entity.defaultstandard;

import java.util.Comparator;

public class DefaultStandardComparator implements Comparator<DefaultStandard>
{
	@Override
	public int compare(DefaultStandard ds1, DefaultStandard ds2)
	{
		Integer p1 = ds1.getInstrument().getPlantid();
		Integer p2 = ds2.getInstrument().getPlantid();

		if (p1.equals(p2))
		{
			Integer id1 = ds1.getId();
			Integer id2 = ds2.getId();

			return id1.compareTo(id2);
		}
		else
		{
			return p1.compareTo(p2);
		}
	}
}