package org.trescal.cwms.core.procedure.dto;

import java.util.Comparator;

public class CapabilityProjectionDTOComparator implements Comparator<CapabilityProjectionDTO> {

	@Override
	public int compare(CapabilityProjectionDTO dto0, CapabilityProjectionDTO dto1) {
		int result = dto0.getReference().compareTo(dto1.getReference());
		if (result == 0) {
			result = Integer.compare(dto0.getProcId(), dto1.getProcId());
		}
				
		return result;
	}

}
