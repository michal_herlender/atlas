package org.trescal.cwms.core.procedure.entity.capabilityfilter.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter;

public interface CapabilityFilterDao extends BaseDao<CapabilityFilter, Integer> {
}
