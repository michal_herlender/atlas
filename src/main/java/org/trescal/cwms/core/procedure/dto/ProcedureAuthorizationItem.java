package org.trescal.cwms.core.procedure.dto;

import java.time.LocalDate;

import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProcedureAuthorizationItem {

	private Integer caltypsId;
	private AccreditationLevel level;
	private LocalDate date;
}
