package org.trescal.cwms.core.procedure.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.CapabilityTrainingRecord;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.HTTPSESS_NEW_FILE_LIST})
public class ViewCapabilityController {
    protected final Log logger = LogFactory.getLog(getClass());
    @Autowired
    private CalibrationTypeService caltypeServ;
    @Autowired
    private CertificateService certServ;
    @Autowired
    private FileBrowserService fileBrowserServ;
    @Autowired
    private CapabilityService capabilityService;
    @Autowired
    private SystemComponentService systemCompServ;

    @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
    public List<String> newFiles() {
        return new ArrayList<>();
    }

    @RequestMapping(value = "/viewcapability.htm", method = RequestMethod.GET)
    protected String handleRequestInternal(Model model,
                                           @RequestParam(value = "capabilityId", required = false, defaultValue = "0") Integer capabilityId,
                                           @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                           @ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles) throws Exception {
        Capability capability = this.capabilityService.get(capabilityId);
        if ((capability == null) || (capabilityId == 0)) {
            this.logger.error("No procedure found with the procid " + capabilityId);
            throw new Exception("Procedure requested could not be found.");
        }
        // load up all procedure training record certificates
        for (CapabilityTrainingRecord ptr : capability.getCapabilityTrainingRecords()) {
            // load up certificate
            this.certServ.findCertificate(ptr.getCert().getCertid());
        }
        //get files in the root directory
        FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ.getFilesForComponentRoot(Component.PROCEDURE, capabilityId, newFiles);
        model.addAttribute("procedure", capability);
        model.addAttribute("caltypes", capability.getServiceTypes().stream().map(ServiceType::getCalibrationType).collect(Collectors.toSet()));
        model.addAttribute("accredlevels", AccreditationLevel.values());
        model.addAttribute(Constants.REFDATA_SYSTEM_COMPONENT, this.systemCompServ.findComponent(Component.PROCEDURE));
        model.addAttribute(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
        model.addAttribute(Constants.REFDATA_FILES_NAME, this.fileBrowserServ.getFilesName(fileBrowserWrapper));
        return "/trescal/core/procedure/viewcapability";
    }
}