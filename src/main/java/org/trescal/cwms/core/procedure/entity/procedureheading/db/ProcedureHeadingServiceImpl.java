package org.trescal.cwms.core.procedure.entity.procedureheading.db;

import org.springframework.stereotype.Service;
import org.trescal.cwms.core.procedure.entity.procedureheading.CapabilityHeading;

import java.util.List;

@Service
public class ProcedureHeadingServiceImpl implements ProcedureHeadingService {
    ProcedureHeadingDao ProcedureHeadingDao;

    public CapabilityHeading findProcedureHeading(int id) {
        return ProcedureHeadingDao.find(id);
    }

    public void insertProcedureHeading(CapabilityHeading CapabilityHeading) {
        ProcedureHeadingDao.persist(CapabilityHeading);
    }

    public void updateProcedureHeading(CapabilityHeading CapabilityHeading) {
        ProcedureHeadingDao.update(CapabilityHeading);
    }

    public List<CapabilityHeading> getAllProcedureHeadings() {
        return ProcedureHeadingDao.findAll();
    }

    public void setProcedureHeadingDao(ProcedureHeadingDao ProcedureHeadingDao) {
        this.ProcedureHeadingDao = ProcedureHeadingDao;
    }

    public void deleteProcedureHeading(CapabilityHeading procedureheading) {
        this.ProcedureHeadingDao.remove(procedureheading);
    }

    public void saveOrUpdateProcedureHeading(CapabilityHeading procedureheading) {
        this.ProcedureHeadingDao.saveOrUpdate(procedureheading);
    }
}