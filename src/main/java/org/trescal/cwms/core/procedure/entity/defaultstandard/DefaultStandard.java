package org.trescal.cwms.core.procedure.entity.defaultstandard;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "defaultstandard")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("Simple")
public abstract class DefaultStandard extends Auditable {
	private boolean enforceUse;
	private int id;
	private Instrument instrument;
	private Contact setBy;
	private LocalDate setOn;

	@Transient
	public abstract DefaultStandardType getDefaultStandardType();

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "plantid", nullable = false)
	public Instrument getInstrument() {
		return this.instrument;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "setby", nullable = false)
	public Contact getSetBy() {
		return this.setBy;
	}

	@NotNull
	@Column(name = "seton", nullable = false, columnDefinition = "date")
	public LocalDate getSetOn() {
		return this.setOn;
	}

	@Column(name = "enforceuse", nullable = false, columnDefinition = "bit")
	public boolean isEnforceUse() {
		return this.enforceUse;
	}

	public void setEnforceUse(boolean enforceUse) {
		this.enforceUse = enforceUse;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setSetOn(LocalDate setOn) {
		this.setOn = setOn;
	}

	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

	public void setSetBy(Contact setBy) {
		this.setBy = setBy;
	}
}
