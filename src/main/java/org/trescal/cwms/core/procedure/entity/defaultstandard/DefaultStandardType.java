package org.trescal.cwms.core.procedure.entity.defaultstandard;

import org.trescal.cwms.core.procedure.entity.procedurestandard.CapabilityStandard;
import org.trescal.cwms.core.procedure.entity.workinstructionstandard.WorkInstructionStandard;

public enum DefaultStandardType {
    PROCEDURE(CapabilityStandard.class), WORKINSTRUCTION(WorkInstructionStandard.class);

    Class<? extends DefaultStandard> clazz;

    DefaultStandardType(Class<? extends DefaultStandard> clazz) {
        this.clazz = clazz;
    }

    public Class<? extends DefaultStandard> getClazz() {
        return this.clazz;
    }

    public void setClazz(Class<? extends DefaultStandard> clazz) {
        this.clazz = clazz;
    }
}
