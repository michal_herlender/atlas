package org.trescal.cwms.core.procedure.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class ViewContactProcedureAccreditations {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private CalibrationTypeService caltypeServ;
    @Autowired
	private ContactService contactServ;
	@Autowired
	private DepartmentService deptServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/viewcontactprocedureaccreditations.htm", method=RequestMethod.GET)
	protected ModelAndView handleRequestInternal(
			@RequestParam(value="personid", required=false, defaultValue="0") Integer personId,
			@RequestParam(value="deptid", required=false, defaultValue="0") Integer deptId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String userName,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception
	{
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		Map<String, Object> model = new HashMap<>();
		Contact con;
		if(personId == 0) 
		{
			con = userService.get(userName).getCon();
		}
		else
			con = this.contactServ.get(personId);

		if ((con == null)) {
			this.logger.error("Could not find the requested personid " + personId + " whilst viewing procedure accreditations.");
			throw new Exception("Could not find the requested user.");
		}
		model.put("contact", new ContactKeyValue(con.getPersonid(), con.getName()));
		model.put("caltypes", this.caltypeServ.getActiveProcedureCalTypes());
        List<Department> departments = this.deptServ.getAllCapabilityEnabledSubdivDepartments(allocatedSubdiv);
		if (departments.isEmpty()) throw new Exception("Configuration error - no procedure enabled (laboratory) departments exist for Subdiv");
		model.put("departments", departments);
		// If department is not specified, show the first department for the subdiv
		Department department = this.deptServ.get(deptId);
		if (department == null) department = departments.get(0);
		model.put("department", department);
		model.put("deptid", department.getId());
		model.put("contacts", this.contactServ.getBusinessContacts(true));
		return new ModelAndView("trescal/core/procedure/viewcontactprocedureaccreditations", model);
	}
}