package org.trescal.cwms.core.procedure.entity.procedureheading.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.procedure.entity.procedureheading.CapabilityHeading;

public interface ProcedureHeadingDao extends BaseDao<CapabilityHeading, Integer> {
}