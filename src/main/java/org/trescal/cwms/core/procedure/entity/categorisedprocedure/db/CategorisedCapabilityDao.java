package org.trescal.cwms.core.procedure.entity.categorisedprocedure.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;

import java.util.List;

public interface CategorisedCapabilityDao extends BaseDao<CategorisedCapability, Integer> {

    List<CategorisedCapability> getAllEagerCategorisedCapabilities();

    long countForCapabilityCategory(CapabilityCategory capabilityCategory);

    List<CategorisedCapability> findByCapabilityCategory(CapabilityCategory capabilityCategory);
}