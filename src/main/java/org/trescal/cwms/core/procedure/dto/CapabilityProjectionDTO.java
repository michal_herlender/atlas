package org.trescal.cwms.core.procedure.dto;

import lombok.Getter;

@Getter
public class CapabilityProjectionDTO {
	// Mandatory fields
	private Integer procId;
	private String name;
	private String reference;
	private Integer serviceTypeId;
	private Integer subFamilyId;
	
	public CapabilityProjectionDTO(Integer procId, String name, String reference) {
		this.procId = procId;
		this.name = name;
		this.reference = reference;
	}
	
	public String getReferenceAndName() {
		StringBuffer result = new StringBuffer();
		result.append(this.reference);
		result.append(" - ");
		result.append(this.name);
		return result.toString();
	}

	public CapabilityProjectionDTO(Integer procId, Integer serviceTypeId, Integer subFamilyId) {
		super();
		this.procId = procId;
		this.serviceTypeId = serviceTypeId;
		this.subFamilyId = subFamilyId;
	}

	
}
