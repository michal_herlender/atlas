package org.trescal.cwms.core.procedure.controller;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.AutoPopulatingList;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.procedure.dto.EditCapabilityFilterDTO;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilterType;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.db.CapabilityFilterService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.db.CapabilityCategoryService;
import org.trescal.cwms.core.procedure.entity.workinstruction.db.WorkInstructionService;
import org.trescal.cwms.core.procedure.form.EditCapabilityFiltersForm;
import org.trescal.cwms.core.procedure.form.EditCapabilityServiceTypesValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.accreditedlab.db.AccreditedLabService;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;
import org.trescal.cwms.core.system.entity.servicetype.dto.ServiceTypeSimpleDto;
import org.trescal.cwms.spring.model.KeyValue;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class EditCapabilityFilterController {

    private final EditCapabilityServiceTypesValidator validator;
    private final CapabilityFilterService capabilityFilterService;
    private final CapabilityService capabilityService;
    private final ServiceTypeService serviceTypeService;
    private final CalibrationProcessService calibrationProcessService;
    private final WorkInstructionService workInstructionService;
    private final AccreditedLabService accreditedLabService;
    private final CapabilityCategoryService capabilityCategoryService;

    @Autowired
    public EditCapabilityFilterController(EditCapabilityServiceTypesValidator validator, CapabilityFilterService capabilityFilterService, CapabilityService capabilityService, ServiceTypeService serviceTypeService, CalibrationProcessService calibrationProcessService, WorkInstructionService workInstructionService, AccreditedLabService accreditedLabService, CapabilityCategoryService capabilityCategoryService) {
        this.validator = validator;
        this.capabilityFilterService = capabilityFilterService;
        this.capabilityService = capabilityService;
        this.serviceTypeService = serviceTypeService;
        this.calibrationProcessService = calibrationProcessService;
        this.workInstructionService = workInstructionService;
        this.accreditedLabService = accreditedLabService;
        this.capabilityCategoryService = capabilityCategoryService;
    }

    @InitBinder("form")
    public void initValidator(WebDataBinder binder) {
        // Enables AutoPopulatingList to grow its own list (e.g. filters)
        binder.setAutoGrowNestedPaths(false);
        binder.setValidator(validator);
    }

    @ModelAttribute("form")
    public EditCapabilityFiltersForm formBackingObject(
        @RequestParam(value = "capabilityId") Integer capabilityId) {
        EditCapabilityFiltersForm form = new EditCapabilityFiltersForm();
        Capability capability = this.capabilityService.get(capabilityId);
        val filters = capability.getCapabilityFilters().stream()
            .map(cf -> EditCapabilityFilterDTO.builder()
                    .id(cf.getId())
                    .bestLab(cf.getBestLab())
                    .active(cf.getActive())
                    .delete(false)
                    .filterType(cf.getFilterType())
                    .serviceTypeId(cf.getServicetype().getServiceTypeId())
                    .workInstruktionId(cf.getDefaultWorkInstruction() != null ? cf.getDefaultWorkInstruction().getId() : null)
                    .internalComments(cf.getInternalComments())
                    .externalComments(cf.getExternalComments())
                    .standardTime(cf.getStandardTime())
                    .build()).collect(Collectors.toCollection(() -> new AutoPopulatingList<>(EditCapabilityFilterDTO.class)));
        form.setFilters(filters);
        return form;
    }


    private List<KeyValue<Integer, String>> getCategoryDtos(Integer allocatedSubdivId) {
        List<CapabilityCategory> categories = this.capabilityCategoryService.getAllSorterdBySubdivId(allocatedSubdivId);
        return categories.stream()
            .filter(pc -> pc.getDepartment().getType().equals(DepartmentType.LABORATORY) ||
                pc.getDepartment().getType().equals(DepartmentType.SUBCONTRACTING))
            .map(pc -> new KeyValue<>(pc.getId(), pc.getExtendedName()))
            .collect(Collectors.toList());
    }

    private List<KeyValue<CapabilityFilterType, String>> getFilterTypes() {
        return Arrays.stream(CapabilityFilterType.values())
                .filter(CapabilityFilterType::getSelectable)
                .map(t -> new KeyValue<>(t, t.messageCode))
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "editcapabilityfilters.htm", method = RequestMethod.GET)
    public String referenceData(Model model,
                                @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                @RequestParam(value = "capabilityId") Integer capabilityId) {
        Capability capability = this.capabilityService.get(capabilityId);
        model.addAttribute("procedure", capability);
        model.addAttribute("filterTypes", getFilterTypes());
        model.addAttribute("requirementTypes", WorkRequirementType.values());
        model.addAttribute("accreditedLabs", accreditedLabService.getDtosBySubdivId(subdivDto.getKey()));
        model.addAttribute("workInstruktions", workInstructionService.getAllAsKeyeValues());
        model.addAttribute("calibrationProcesess", calibrationProcessService.findAllKeyValue());
        model.addAttribute("serviceTypes", serviceTypeService.getAllCalServiceTypes().stream().map(ServiceTypeSimpleDto::fromServiceTypeWithTranslatedNames).collect(Collectors.toList()));
        model.addAttribute("categories", getCategoryDtos(subdivDto.getKey()));
        return "trescal/core/procedure/editcapabilityfilters";
    }

    @RequestMapping(value = "editcapabilityfilters.htm", method = RequestMethod.POST)
    public String onSubmit(Model model,
                           @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                           @RequestParam(value = "viewcapabilities", required = false, defaultValue = "false") Boolean viewCapabilities,
                           @RequestParam(value = "capabilityId") Integer capabilityId,
                           @Valid @ModelAttribute("form") EditCapabilityFiltersForm form, BindingResult errors,
                           RedirectAttributes redirectAttributes
    ) {
        if (errors.hasErrors()) {
            return referenceData(model, subdivDto, capabilityId);
        }

        this.capabilityFilterService.editCapabilityFilters(capabilityId, form);
        if (!viewCapabilities) {
            redirectAttributes.addAttribute("capabilityId", capabilityId);
            return "redirect:viewcapability.htm";
        } else {
            Capability capability = this.capabilityService.get(capabilityId);
            // Redirects to 'NONE' domain if subfamily not defined
            int domainid = capability.getSubfamily() == null ? 0 : capability.getSubfamily().getFamily().getDomain().getDomainid();
            int subdivid = capability.getOrganisation().getSubdivid();
            redirectAttributes.addAttribute("subdivid", subdivid);
            redirectAttributes.addAttribute("domainid", domainid);
            return "redirect:viewcapabilities.htm";
        }
    }
}