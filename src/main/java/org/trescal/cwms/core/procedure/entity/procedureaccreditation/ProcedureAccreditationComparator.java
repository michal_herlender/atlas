package org.trescal.cwms.core.procedure.entity.procedureaccreditation;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationTypeComparator;

import java.util.Comparator;

/**
 * Custom {@link Comparator} used to sort collections of
 * {@link CapabilityAuthorization} entities. Sort order is by
 * {@link CalibrationType}, {@link AccreditationLevel} and then by
 * {@link Contact}.
 *
 * @author richard
 */
public class ProcedureAccreditationComparator implements Comparator<CapabilityAuthorization> {
    @Override
    public int compare(CapabilityAuthorization o1, CapabilityAuthorization o2) {
        if ((o1.getCaltype() != null) && (o2.getCaltype() == null)) {
            return 1;
        } else if ((o1.getCaltype() == null) && (o2.getCaltype() != null)) {
            return -1;
        } else if (((o1.getCaltype() != null) && (o2.getCaltype() != null))
            && (o1.getCaltype().getCalTypeId() != o2.getCaltype().getCalTypeId())) {
            return new CalibrationTypeComparator().compare(o1.getCaltype(), o2.getCaltype());
        } else {
            if (o1.getAccredLevel() != o2.getAccredLevel()) {
                return o1.getAccredLevel().compareTo(o2.getAccredLevel());
            }
			else
			{
                if (o1.getAuthorizationFor().getName().equals(o2.getAuthorizationFor().getName())) {
                    return ((Integer) o1.getId()).compareTo(o2.getId());
                } else {
                    return o1.getAuthorizationFor().getName().compareTo(o2.getAuthorizationFor().getName());
                }
			}
		}
	}
}