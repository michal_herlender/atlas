package org.trescal.cwms.core.procedure.entity.capabilityfilter.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter;
import org.trescal.cwms.core.procedure.form.EditCapabilityFiltersForm;

public interface CapabilityFilterService extends BaseService<CapabilityFilter, Integer> {
    void editCapabilityFilters(int procid, EditCapabilityFiltersForm form);
}
