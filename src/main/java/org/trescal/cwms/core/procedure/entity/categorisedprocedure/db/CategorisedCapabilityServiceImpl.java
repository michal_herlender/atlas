package org.trescal.cwms.core.procedure.entity.categorisedprocedure.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedProcedureComparator;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;

import java.util.List;
import java.util.TreeSet;

@Service
public class CategorisedCapabilityServiceImpl implements CategorisedCapabilityService {
    private final CategorisedCapabilityDao cpDao;

    @Autowired
    public CategorisedCapabilityServiceImpl(CategorisedCapabilityDao cpDao) {
        this.cpDao = cpDao;
    }

    public void deleteCategorisedCapability(CategorisedCapability categorisedprocedure) {
        this.cpDao.remove(categorisedprocedure);
    }

    public CategorisedCapability findCategorisedCapability(int id) {
        return this.cpDao.find(id);
    }

    public List<CategorisedCapability> getAllEagerCategorisedCapabilities() {
        return this.cpDao.getAllEagerCategorisedCapabilities();
    }

    @Override
    public long countForCapabilityCategory(CapabilityCategory capabilityCategory) {
        return this.cpDao.countForCapabilityCategory(capabilityCategory);
    }

    /**
     * Reassigns any CategorisedProcedure references (and thereby Procedures) using one ProcedureCategory
     * to a replacement ProcedureCategory.
     */
    @Override
    public void updateCapabilityCategories(CapabilityCategory oldCategory, CapabilityCategory newCategory) {
        List<CategorisedCapability> list = this.cpDao.findByCapabilityCategory(oldCategory);
        for (CategorisedCapability cp : list) {
            cp.setCategory(newCategory);
        }
        // Clear out procedure reference for consistency / to prevent orphan removal
        oldCategory.setProcedures(new TreeSet<>(new CategorisedProcedureComparator()));
        // Note, new category should be reloaded externally if needed
    }
}