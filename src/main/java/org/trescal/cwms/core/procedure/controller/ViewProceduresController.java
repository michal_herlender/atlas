package org.trescal.cwms.core.procedure.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.CapabilityComparator;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.ProcedureCategoryDepartmentComparator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.*;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class ViewProceduresController {
    @Autowired
    private AuthenticationService authService;
    @Autowired
    private DepartmentService deptServ;
    @Autowired
    private MessageSource messages;
    @Autowired
    private CapabilityService procServ;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private UserService userService;

    /**
     * deptid = (-1 : First department in list, 0 : no dept assigned, n : regular dept id)
     */
    @RequestMapping(value = "/viewprocedures.htm", method = RequestMethod.GET)
    protected String referenceData(Model model,
                                   @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                                   @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@RequestParam(name="deptid", required=false, defaultValue="-1") int deptid)
	{
		Subdiv subdiv = this.subdivService.get(subdivDto.getKey());
		Contact contact = this.userService.get(username).getCon();
		List<KeyValue<Integer, String>> departments = getDepartmentDtos(subdiv); 
		model.addAttribute("departments", departments);
		
		model.addAttribute("canReallocate", this.authService.hasRight(contact, Permission.REALLOCATE_MANAGEMENT , subdivDto.getKey()));
		if (deptid == 0 || (deptid == -1 && departments.size() == 1)) {
            // get a list of all procedures with no department
            model.addAttribute("nodeptprocs", this.procServ.getAllCapabilitiesWithNoDepartment(subdiv));
            model.addAttribute("selectedKey", 0);
		} else {
            Integer queryId = deptid == -1 ? departments.get(0).getKey() : deptid;
            // Get a list of procedures for specific department
            Department department = this.deptServ.get(queryId);
            List<Capability> capabilities = this.procServ.getAllForDepartment(queryId);
            model.addAttribute("department", department);
            model.addAttribute("categoryMap", getCategoryMap(department, capabilities));
            model.addAttribute("selectedKey", queryId);
        }
        return "trescal/core/procedure/viewprocedures";
    }

    public List<KeyValue<Integer, String>> getDepartmentDtos(Subdiv subdiv) {
        Locale locale = LocaleContextHolder.getLocale();

        List<Department> departments = new ArrayList<>();
        departments.addAll(this.deptServ.getAllCapabilityEnabledSubdivDepartments(subdiv));
        departments.addAll(this.deptServ.getByTypeAndSubdiv(DepartmentType.SUBCONTRACTING, subdiv.getId()));

        List<KeyValue<Integer, String>> result = new ArrayList<>();
        departments.forEach(dept -> result.add(new KeyValue<>(dept.getId(), dept.getName())));
        result.add(new KeyValue<>(0, Objects.requireNonNull(messages.getMessage("editproc.nodepartment", null, "No Department", locale))));
        return result;
    }

    public Map<CapabilityCategory, Set<Capability>> getCategoryMap(Department department, List<Capability> capabilities) {

        Map<CapabilityCategory, Set<Capability>> result = new TreeMap<>(new ProcedureCategoryDepartmentComparator());
        // Initialize so that all procedure categories shown (even if unused)
        department.getCategories().forEach(pc -> result.put(pc, new TreeSet<>(new CapabilityComparator())));
        // Note that procedure may belong to other departments' categories too (we skip those)
        capabilities.forEach(
            procedure -> procedure.getCategories().stream()
                .filter(cp -> result.containsKey(cp.getCategory()))
                .forEach(cp -> result.get(cp.getCategory()).add(procedure)));

        return result;
	}
}