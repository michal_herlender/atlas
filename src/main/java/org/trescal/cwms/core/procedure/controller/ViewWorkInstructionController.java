package org.trescal.cwms.core.procedure.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.procedure.entity.workinstruction.db.WorkInstructionService;
import org.trescal.cwms.core.procedure.form.ViewWorkInstructionForm;

@Controller
@IntranetController
public class ViewWorkInstructionController {

	private static final Logger logger = LoggerFactory.getLogger(ViewWorkInstructionController.class);

	@Autowired
	private WorkInstructionService workInstructionService;

	@ModelAttribute("form")
	private ViewWorkInstructionForm makeForm() {
		return new ViewWorkInstructionForm();
	}

	@RequestMapping(value = "/viewworkinstruction", method = RequestMethod.GET)
	protected String displayPage(Model model,
			@RequestParam(name="wid", required=true) Integer wid) {
		WorkInstruction workInstruction = this.workInstructionService.get(wid);
		if (workInstruction == null) {
			logger.error("No work instruction found with the wid " + wid);
			throw new RuntimeException("Work instruction requested could not be found.");
		}
		model.addAttribute("wi", workInstruction);
		return "trescal/core/procedure/viewworkinstruction/standards";
	}
}