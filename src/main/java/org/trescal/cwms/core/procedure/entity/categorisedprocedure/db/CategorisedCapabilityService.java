package org.trescal.cwms.core.procedure.entity.categorisedprocedure.db;

import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;

import java.util.List;

public interface CategorisedCapabilityService {
    void deleteCategorisedCapability(CategorisedCapability categorisedprocedure);

    CategorisedCapability findCategorisedCapability(int id);

    /**
     * Gets all current {@link CategorisedCapability} entities with
     * {@link Capability} and {@link CapabilityCategory} entities eagerly loaded.
     *
     * @return
     */
    List<CategorisedCapability> getAllEagerCategorisedCapabilities();

    long countForCapabilityCategory(CapabilityCategory capabilityCategory);

    void updateCapabilityCategories(CapabilityCategory oldCategory, CapabilityCategory newCategory);
}