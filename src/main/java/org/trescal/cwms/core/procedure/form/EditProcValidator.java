package org.trescal.cwms.core.procedure.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.db.CapabilityCategoryService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import javax.validation.groups.Default;

@Component
public class EditProcValidator extends AbstractBeanValidator {
    @Autowired
    private CapabilityService capabilityService;
    @Autowired
    private CapabilityCategoryService capabilityCategoryService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(EditCapabilityForm.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
		super.validate(target, errors, Default.class);
        EditCapabilityForm form = (EditCapabilityForm) target;
		if (form.getSupplierCapability()) {
            super.validate(target, errors, EditCapabilityForm.SupplierCompany.class);
		}
		else {
            super.validate(target, errors, EditCapabilityForm.BusinessCompany.class);
		}
		
		// validate that no other active procedures with the current ref number
		// already exist (only do this for not null / not "" values
		if ((form.getReference() != null)
				&& !form.getReference().trim().equals(""))
		{
			if (!errors.hasErrors()) {
                if (this.capabilityService.getCapabilityByReference(form.getReference(), true, form.getCapabilityId()).size() > 0) {
                    errors.rejectValue("", "error.duplicateprocref", null, "An active procedure with this reference number already exists.");
                }
            }
		}
		// validate that the default address of the procedure matches at least one of the procedure categories
		// and that there is at least one procedure category 
		// (except for THIRD_PARTY requirement type, or "supplier capability" where not used)
		if (form.getActive() && !WorkRequirementType.THIRD_PARTY.equals(form.getReqType()) && !form.getSupplierCapability()) {
			if (form.getCategoryids().isEmpty()) {
				errors.rejectValue("categoryids", "error.value.notselected", "A value must be selected");
			}
			else {
				boolean foundCategory = false;
				for (Integer categoryid : form.getCategoryids()) {
                    CapabilityCategory category = this.capabilityCategoryService.get(categoryid);
					if (Integer.compare(category.getDepartment().getAddress().getAddrid(), form.getDefAddressId()) == 0) {
						foundCategory = true;
						break;
					}
				}
				if (!foundCategory) {
					errors.rejectValue("categoryids", "error.procedure.addresscategory", "A procedure category belonging to the default address must be selected");
				}
			}
		}
		
	}
}