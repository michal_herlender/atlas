package org.trescal.cwms.core.procedure.entity.defaultstandard.db;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandard;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandardType;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.Capability_;
import org.trescal.cwms.core.procedure.entity.procedurestandard.CapabilityStandard;
import org.trescal.cwms.core.procedure.entity.procedurestandard.CapabilityStandard_;
import org.trescal.cwms.core.procedure.entity.workinstructionstandard.WorkInstructionStandard;

import javax.persistence.FetchType;
import javax.persistence.criteria.*;
import java.util.List;

@Repository("DefaultStandardDao")
public class DefaultStandardDaoImpl extends BaseDaoImpl<DefaultStandard, Integer> implements DefaultStandardDao {

    @Override
    protected Class<DefaultStandard> getEntity() {
        return DefaultStandard.class;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DefaultStandard> getAllDefaultStandardsForInstument(int plantid, DefaultStandardType type) {
        Class<? extends DefaultStandard> clazz = DefaultStandard.class;
        if (type != null) clazz = type.getClazz();
        Criteria crit = getSession().createCriteria(clazz);
        crit.createCriteria("instrument").add(Restrictions.idEq(plantid));
        crit.addOrder(Order.asc("id"));
        return (List<DefaultStandard>) crit.list();
    }

    @Override
    public CapabilityStandard getDefaultStandardByProcPlant(int procid, int plantid) {
        return getFirstResult(cb -> {
            CriteriaQuery<CapabilityStandard> cq = cb.createQuery(CapabilityStandard.class);
            final Root<CapabilityStandard> capabilityStandardRoot = cq.from(CapabilityStandard.class);
            final Join<CapabilityStandard, Instrument> instrumentJoin = capabilityStandardRoot.join(CapabilityStandard_.instrument, JoinType.LEFT);
            final Join<CapabilityStandard, Capability> capabilityJoin = capabilityStandardRoot.join(CapabilityStandard_.capability);
            final Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.equal(instrumentJoin.get(Instrument_.plantid), plantid));
            clauses.getExpressions().add(cb.equal(capabilityJoin.get(Capability_.id), procid));
            cq.where(clauses);
            return cq;
        }).orElse(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public WorkInstructionStandard getDefaultStandardByWIPlant(int wiid, int plantid) {
        Criteria crit = getSession().createCriteria(WorkInstructionStandard.class);
        crit.createCriteria("instrument").add(Restrictions.idEq(plantid));
        crit.createCriteria("workInstruction").add(Restrictions.idEq(wiid));
        List<WorkInstructionStandard> list = (List<WorkInstructionStandard>) crit.list();
        return list.size() > 0 ? list.get(0) : null;
    }

    @Override
    public List<CapabilityStandard> getStandardsForProcedure(Integer capabilityId, Boolean enforced) {
        return getResultList(cb -> {
            CriteriaQuery<CapabilityStandard> cq = cb.createQuery(CapabilityStandard.class);
            final Root<CapabilityStandard> capabilityStandardRoot = cq.from(CapabilityStandard.class);
            final Fetch<CapabilityStandard, Instrument> instrumentFetch = capabilityStandardRoot.fetch(CapabilityStandard_.instrument);
            final Fetch<Instrument, InstrumentModel> instrumentModelFetch = instrumentFetch.fetch(Instrument_.model);
            instrumentModelFetch.fetch(InstrumentModel_.mfr);
            instrumentModelFetch.fetch(InstrumentModel_.description);
            Predicate conjunction = cb.conjunction();
            conjunction.getExpressions().add(cb.equal(capabilityStandardRoot.get(CapabilityStandard_.capability), capabilityId));
            if (enforced != null)
               conjunction.getExpressions().add(cb.equal(capabilityStandardRoot.get(CapabilityStandard_.enforceUse), enforced));
            cq.where(conjunction);
            return cq;
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<WorkInstructionStandard> getStandardsForWorkInstruction(int wiid, Boolean enforced) {
        Criteria crit = getSession().createCriteria(WorkInstructionStandard.class);
        crit.createCriteria("workInstruction").add(Restrictions.idEq(wiid));
        if (enforced != null) crit.add(Restrictions.eq("enforceUse", enforced));
        crit.setFetchMode("instrument", FetchMode.JOIN);
        crit.setFetchMode("instrument.model", FetchMode.JOIN);
        crit.setFetchMode("instrument.model.mfr", FetchMode.JOIN);
        crit.setFetchMode("instrument.model.description", FetchMode.JOIN);
        return crit.list();
    }
}