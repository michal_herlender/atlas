package org.trescal.cwms.core.procedure.controller;

import java.util.ArrayList;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.procedure.entity.workinstruction.db.WorkInstructionService;
import org.trescal.cwms.core.procedure.form.EditWorkInstructionForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.validation.BeanValidator;

@Controller
@IntranetController
@RequestMapping(value = "/editworkinstruction.htm")
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY, Constants.SESSION_ATTRIBUTE_USERNAME})
public class EditWorkInstructionController {

	@Autowired
	private MessageSource messages;
	@Autowired
	private WorkInstructionService workInstructionService;
	@Autowired
	private BeanValidator validator;

	@InitBinder("form")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("form")
	protected EditWorkInstructionForm formBackingObject(
			@RequestParam(value = "wid", required = false, defaultValue = "0") Integer wid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto) {
		EditWorkInstructionForm form = new EditWorkInstructionForm();
		form.setId(wid);
		if (wid == 0) {
			form.setCoid(companyDto.getKey());
		} else {
			WorkInstruction workInstruction = this.workInstructionService.get(wid);
			form.setName(workInstruction.getName());
			form.setTitle(workInstruction.getTitle());
			if (workInstruction.getDescription() != null) {
				form.setDescription(workInstruction.getDescription().getDescription());
				form.setDescid(workInstruction.getDescription().getId());
			}
			form.setCoid(workInstruction.getOrganisation().getCoid());

		}
		return form;
	}

	@RequestMapping(method = RequestMethod.POST)
	protected String onSubmit(Model model, Locale locale,
			@Validated @ModelAttribute("form") EditWorkInstructionForm form, BindingResult bindingResult,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username) throws Exception {
		if (bindingResult.hasErrors())
			return referenceData(model, locale, form, companyDto, subdivDto);
		else {
			WorkInstruction wi = workInstructionService.updateWorkInstruction(form, companyDto.getKey(), username);
			model.asMap().clear();
			return "redirect:/viewworkinstruction?wid=" + wi.getId();
		}
	}

	@RequestMapping(method = RequestMethod.GET)
	protected String referenceData(Model model, Locale locale, @ModelAttribute("form") EditWorkInstructionForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto) {

		ArrayList<ContactKeyValue> prependDtos = new ArrayList<>();
		prependDtos.add(new ContactKeyValue(0, this.messages.getMessage("company.none", null, "None", locale)));
		if (form.getId() != 0) {
			// For data integrity, ensure that work instruction belongs to
			// current allocated company
			WorkInstruction wi = this.workInstructionService.get(form.getId());
			if (wi.getOrganisation().getCoid() != companyDto.getKey()) {
				throw new RuntimeException(
						"You must be logged into the company that owns the work instruction, to edit it.");
			}
		}

		model.addAttribute("newWorkInstruction", form.getId() == 0);
		model.addAttribute("allocatedCompanyName", companyDto.getValue());
		model.addAttribute("allocatedSubdivName", subdivDto.getValue());
		return "trescal/core/procedure/editworkinstruction";
	}
}