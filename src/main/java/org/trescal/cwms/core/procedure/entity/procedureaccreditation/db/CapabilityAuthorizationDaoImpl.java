package org.trescal.cwms.core.procedure.entity.procedureaccreditation.db;

import lombok.val;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

@Repository
public class CapabilityAuthorizationDaoImpl extends BaseDaoImpl<CapabilityAuthorization, Integer>
    implements CapabilityAuthorizationDao {

    @Override
    protected Class<CapabilityAuthorization> getEntity() {
        return CapabilityAuthorization.class;
    }

    @Override
    public Optional<CapabilityAuthorization> findCapabilityAuthorization(int capabilityId, Integer caltypeid, int personid) {
        return getFirstResult(cb -> {
            val cq = cb.createQuery(CapabilityAuthorization.class);
            val capabalityAccredidation = cq.from(CapabilityAuthorization.class);
            cq.where(cb.equal(capabalityAccredidation.get(CapabilityAuthorization_.capability), capabilityId),
                cb.equal(capabalityAccredidation.get(CapabilityAuthorization_.authorizationFor), personid),
                cb.equal(capabalityAccredidation.get(CapabilityAuthorization_.caltype), caltypeid)
            );
            return cq;
        });
    }

    @Override
    public List<CapabilityAuthorization> getAllEagerCapabilityAuthorizations() {
        return getResultList(cb -> {
            val cq = cb.createQuery(CapabilityAuthorization.class);
            val ca = cq.from(CapabilityAuthorization.class);
            ca.fetch(CapabilityAuthorization_.authorizationFor);
            return cq;
        });
    }

    @Override
    public List<CapabilityAuthorization> getCapabilityAuthorizations(int capabilityId, Integer caltypeid, int personid) {
        return getResultList(cb -> {
            val cq = cb.createQuery(CapabilityAuthorization.class);
            val capabalityAccredidation = cq.from(CapabilityAuthorization.class);
            val predicates =
                Stream.of(
                    Stream.of(cb.equal(capabalityAccredidation.get(CapabilityAuthorization_.capability), capabilityId)),
                    Stream.of(cb.equal(capabalityAccredidation.get(CapabilityAuthorization_.authorizationFor), personid)),
                    Stream.of(caltypeid).filter(Objects::nonNull).map(id -> cb.equal(capabalityAccredidation.get(CapabilityAuthorization_.caltype), id))).flatMap(Function.identity()).toArray(Predicate[]::new);
            cq.where(predicates);
            return cq;
        });
    }

    @Override
    public List<CapabilityAuthorization> getAllAuthorizationsForCapability(int capabilityId) {

        return getResultList(cb -> {
            CriteriaQuery<CapabilityAuthorization> cq = cb.createQuery(CapabilityAuthorization.class);
            Root<CapabilityAuthorization> pa = cq.from(CapabilityAuthorization.class);
            Predicate andClauses = cb.conjunction();
            andClauses.getExpressions().add(cb.equal(pa.get(CapabilityAuthorization_.capability), capabilityId));
            cq.where(andClauses);
            return cq;
        });
    }
}