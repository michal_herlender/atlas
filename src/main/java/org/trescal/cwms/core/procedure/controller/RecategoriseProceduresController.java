package org.trescal.cwms.core.procedure.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.db.CapabilityCategoryService;
import org.trescal.cwms.core.procedure.form.RecategoriseCapabilitiesForm;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.*;

@Controller
@IntranetController
public class RecategoriseProceduresController {
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private CapabilityCategoryService pcService;
    @Autowired
    private CapabilityService capabilityService;

    @ModelAttribute("command")
    public RecategoriseCapabilitiesForm formBackingObject(
        @RequestParam(name = "categoryid") int categoryid) {
        CapabilityCategory category = this.pcService.get(categoryid);
        Map<Integer, Boolean> procedureIds = new HashMap<>();
        List<Capability> capabilities = this.capabilityService.getAll(category);
        capabilities.forEach(proc -> procedureIds.put(proc.getId(), Boolean.FALSE));
        RecategoriseCapabilitiesForm form = new RecategoriseCapabilitiesForm();
        form.setProcedureIds(procedureIds);
        return form;
    }

    @RequestMapping(value = "recategoriseprocedures.htm", method = RequestMethod.GET)
    public String showView(Locale locale, Model model,
                           @RequestParam(name = "categoryid") int categoryid) {
        CapabilityCategory category = this.pcService.get(categoryid);
        model.addAttribute("categoryDtos", getCategoryDtos(category.getDepartment().getSubdiv(), locale));
        model.addAttribute("category", category);
        model.addAttribute("procedures", this.capabilityService.getAll(category));
        return "trescal/core/procedure/recategoriseprocedures";
    }

    private List<KeyValue<String, Integer>> getCategoryDtos(Subdiv subdiv, Locale locale) {
        List<KeyValue<String, Integer>> list = new ArrayList<>();
        list.add(new KeyValue<>(Objects.requireNonNull(messageSource.getMessage("contactadd.defaultnone", null, "-- None --", locale)), 0));
        this.pcService.getAllSorted(subdiv).stream()
            .filter(pc -> pc.getDepartment().getType().equals(DepartmentType.LABORATORY) ||
                pc.getDepartment().getType().equals(DepartmentType.SUBCONTRACTING))
            .map(pc -> new KeyValue<>(pc.getDepartment().getName() + " - " + pc.getName(), pc.getId()))
            .forEach(list::add);
        return list;
    }

    @RequestMapping(value = "recategoriseprocedures.htm", method = RequestMethod.POST)
    public String onSubmit(Locale locale, Model model,
                           @RequestParam(name = "categoryid") int categoryid,
                           @Validated @ModelAttribute("command") RecategoriseCapabilitiesForm form,
                           BindingResult bindingResult) {

        if (bindingResult.hasErrors())
            return showView(locale, model, categoryid);

        this.capabilityService.recategoriseCapabilities(categoryid, form);
        CapabilityCategory category = this.pcService.get(categoryid);
        return "redirect:viewprocedures.htm?deptid=" + category.getDepartment().getId();
    }
}
