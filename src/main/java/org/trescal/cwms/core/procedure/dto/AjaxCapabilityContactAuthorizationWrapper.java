package org.trescal.cwms.core.procedure.dto;

import lombok.Data;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;

import java.time.LocalDate;
import java.util.List;

/**
 * DTO class that is used to send updates about a single {@link Contact}'s
 * accreditations for a single {@link Capability}.
 *
 * @author Richard
 */
@Data
public class AjaxCapabilityContactAuthorizationWrapper {
    private int personid;
    private int procid;

    private List<AccreditationLevel> levels;
    private List<Integer> caltypeids;
    private List<LocalDate> dates;

}
