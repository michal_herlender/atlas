package org.trescal.cwms.core.procedure.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.db.AddressService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.departmenttype.DepartmentType;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.db.CalibrationProcessService;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.db.CapabilityCategoryService;
import org.trescal.cwms.core.procedure.entity.workinstruction.db.WorkInstructionService;
import org.trescal.cwms.core.procedure.form.EditCapabilityForm;
import org.trescal.cwms.core.procedure.form.EditProcValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.accreditedlab.db.AccreditedLabService;
import org.trescal.cwms.core.tools.TranslationUtils;
import org.trescal.cwms.spring.model.KeyValue;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class EditCapabilityController {
    @Autowired
    private AccreditedLabService accreditedLabServ;
    @Autowired
    private AddressService addressService;
    @Autowired
    private CalibrationProcessService calibrationProcessServ;
    @Autowired
    private CapabilityCategoryService catServ;
    @Autowired
    private CapabilityService capabilityService;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private UserService userService;
    @Autowired
    private WorkInstructionService wiServ;
    @Autowired
    private EditProcValidator validator;
    @Autowired
    private MessageSource messageSource;

    public static final String MESSAGE_CODE_NA = "notapplicable";

    @InitBinder("form")
    public void initValidator(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @ModelAttribute("form")
    protected EditCapabilityForm formBackingObject(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                                   @RequestParam(value = "procid", required = false, defaultValue = "0") Integer capabilityId) throws Exception {
        EditCapabilityForm form = new EditCapabilityForm();
        if (capabilityId == 0) {
            Subdiv businessSubdiv = this.subdivService.get(subdivDto.getKey());
            form.setAccreditedLabId(0);
            form.setActive(true);
            form.setCoid(0);
            form.setDefAddressId(businessSubdiv.getDefaultAddress().getAddrid());
            form.setCategoryids(new HashSet<>());
            form.setCapabilityId(0);
            form.setReqType(WorkRequirementType.CALIBRATION);
            form.setSubdivid(0);
            form.setSupplierCapability(false);
            // Fields for supplier capability (cascade search) - initialize as unselected
            form.setAddrid(0);
            form.setCoid(0);
            form.setSubdivid(0);
        } else {
            Capability capability = this.capabilityService.get(capabilityId);
            boolean supplierCapability = capability.getOrganisation().getComp().getCompanyRole() == CompanyRole.SUPPLIER;

            form.setAccreditedLabId(capability.getLabAccreditation() == null ? 0 :
                capability.getLabAccreditation().getId());
            form.setActive(capability.isActive());
            form.setCalProcessId(capability.getCalibrationProcess() == null ? null :
                capability.getCalibrationProcess().getId());
            form.setCategoryids(capability.getCategories().stream().map(pc -> pc.getCategory().getId()).collect(Collectors.toSet()));
            form.setDefAddressId(capability.getDefAddress().getAddrid());
            form.setEstimatedTime(capability.getEstimatedTime());
            form.setDescription(capability.getDescription());
            form.setName(capability.getName());
            form.setReference(capability.getReference());
            form.setCapabilityId(capability.getId());
            form.setReqType(capability.getReqType());
            if (capability.getSubfamily() != null) {
                form.setSubfamilyId(capability.getSubfamily().getId());
                form.setSubfamilyName(TranslationUtils.getBestTranslation(capability.getSubfamily().getTranslations()).orElse(""));
            }
            form.setSupplierCapability(supplierCapability);
            form.setWorkInstructionId(capability.getDefWorkInstruction() == null ? null :
                capability.getDefWorkInstruction().getId());
            // Cascade form fields used for supplier capability only are intentionally left null (chosen by user)
        }
        return form;
    }

    public Set<KeyValue<Integer, String>> getAccreditedLabs(Subdiv allocatedSubdiv, Locale locale) {
        Set<KeyValue<Integer, String>> accreditedLabs = this.accreditedLabServ.getDtosBySubdiv(allocatedSubdiv);
        accreditedLabs.add(new KeyValue<>(0, this.messageSource.getMessage(MESSAGE_CODE_NA, null, locale)));
        return accreditedLabs;
    }

    private Set<KeyValue<Integer, String>> getAddressDtos(Subdiv allocatedSubdiv) {
        List<Address> addresses = this.addressService.getAllActiveSubdivAddresses(allocatedSubdiv, null);
        return addresses.stream().map(a -> new KeyValue<>(a.getAddrid(), a.getAddressLine()))
            .collect(Collectors.toSet());
    }

    private List<KeyValue<Integer, String>> getCategoryDtos(Subdiv allocatedSubdiv) {
        List<CapabilityCategory> categories = this.catServ.getAllSorted(allocatedSubdiv);
        return categories.stream()
            .filter(pc -> pc.getDepartment().getType().equals(DepartmentType.LABORATORY) ||
                pc.getDepartment().getType().equals(DepartmentType.SUBCONTRACTING))
            .map(pc -> new KeyValue<>(pc.getId(), pc.getExtendedName()))
            .collect(Collectors.toList());
    }

    @RequestMapping(value = "/editcapability.htm", method = RequestMethod.POST)
    protected String onSubmit(Model model,
                              @Valid @ModelAttribute("form") EditCapabilityForm form, BindingResult bindingResult,
                              @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                              @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                              RedirectAttributes redirectAttributes,
                              Locale locale) {
        if (bindingResult.hasErrors()) {
            return referenceData(model, form, subdivDto, locale);
        }
        int capabilityId;
        if (form.getCapabilityId() == 0) {
            Capability capability = this.capabilityService.createCapability(form);
            capabilityId = capability.getId();
        } else {
            Contact contact = this.userService.get(username).getCon();
            this.capabilityService.editCapability(form, contact);
            capabilityId = form.getCapabilityId();
        }
        redirectAttributes.addAttribute("procid", capabilityId);
        return "redirect:viewcapability.htm?capabilityId=" + capabilityId;
    }

    @RequestMapping(value = "/editcapability.htm", method = RequestMethod.GET)
    protected String referenceData(Model model,
                                   @ModelAttribute("form") EditCapabilityForm form,
                                   @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                   Locale locale) {
        Subdiv allocatedSubdiv = this.subdivService.get(subdivDto.getKey());
        model.addAttribute("businessSubdiv", allocatedSubdiv);
        if (form.getCapabilityId() != 0) {
            Capability capability = this.capabilityService.get(form.getCapabilityId());
            // For capabilities belonging to a business company, we only allow edit of business company procedures
            // belonging to logged in subdivision, as reference data for new procedures is from this subdiv
            // (controlled also in view)
            if ((capability.getOrganisation().getComp().getCompanyRole() == CompanyRole.BUSINESS) &&
                (capability.getOrganisation().getSubdivid() != subdivDto.getKey())) {
                throw new AccessDeniedException("You must switch to the business subdivision of this capability record to edit it.");
            }
        }

        model.addAttribute("accreditedlabs", getAccreditedLabs(allocatedSubdiv, locale));
        model.addAttribute("addresses", getAddressDtos(allocatedSubdiv));
        model.addAttribute("categories", getCategoryDtos(allocatedSubdiv));
        model.addAttribute("calprocess", this.calibrationProcessServ.getAll());
        model.addAttribute("workinstructions", this.wiServ.getAll());
        model.addAttribute("reqtypes", WorkRequirementType.values());
        return "/trescal/core/procedure/editcapability";
    }
}