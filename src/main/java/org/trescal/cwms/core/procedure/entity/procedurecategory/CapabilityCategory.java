package org.trescal.cwms.core.procedure.entity.procedurecategory;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedProcedureComparator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "capabilitycategory")
public class CapabilityCategory {
    private int id;
    private String name;
    private String description;
    private Set<CategorisedCapability> procedures;
    private Department department;
    private Boolean splitTraceable;

    /**
     * Indicates if this is the default procedure for this {@link Department}.
     */
    private boolean departmentDefault;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "departmentid", nullable = false)
    public Department getDepartment() {
        return this.department;
    }

	@Length(min = 0, max = 200)
	@Column(name = "description", nullable = true, length = 200)
	public String getDescription()
	{
		return this.description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@Length(max = 100)
	@Column(name = "name", length = 100, nullable = false)
    public String getName() {
        return this.name;
    }

    @Transient
    public String getExtendedName() {
        return department.getName() + " - " + name;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "category")
    @SortComparator(CategorisedProcedureComparator.class)
    public Set<CategorisedCapability> getProcedures() {
        return this.procedures;
    }

    @Column(name = "departmentdefault", nullable = false, columnDefinition = "bit")
    public boolean isDepartmentDefault() {
        return this.departmentDefault;
    }

    public void setDepartment(Department department)
	{
		this.department = department;
	}

	public void setDepartmentDefault(boolean departmentDefault)
	{
		this.departmentDefault = departmentDefault;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProcedures(Set<CategorisedCapability> procedures) {
        this.procedures = procedures;
    }

    public Boolean getSplitTraceable() {
        return splitTraceable;
    }

    public void setSplitTraceable(Boolean splitTraceable) {
        this.splitTraceable = splitTraceable;
    }
}