package org.trescal.cwms.core.procedure.entity.procedureaccreditation.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;

import java.util.List;
import java.util.Optional;

public interface CapabilityAuthorizationDao extends BaseDao<CapabilityAuthorization, Integer> {

    Optional<CapabilityAuthorization> findCapabilityAuthorization(int capabilityId, Integer caltypeid, int personid);

    List<CapabilityAuthorization> getAllEagerCapabilityAuthorizations();

    List<CapabilityAuthorization> getCapabilityAuthorizations(int capabilityId, Integer caltypeid, int personid);


    List<CapabilityAuthorization> getAllAuthorizationsForCapability(int capabilityId);
}