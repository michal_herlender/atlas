package org.trescal.cwms.core.procedure.utils;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

/**
 * Enum is in order of preference, e.g. we want instrumentmodel matches to appear first, then subfamily
 * when they are otherwise equivalent
 */
public enum ProcedureMatchSource {
	INSTRUMENTMODEL_CAPABILITY("instrumentmodel", "Instrument Model"), 
	SUBFAMILY_PROCEDURE("sub-family", "Sub-Family"),
	GENERIC_PROCEDURE("instmod.generic", "Generic");
	
	private String messageKey;
	private String defaultName;
	private MessageSource messageSource;
	
	private ProcedureMatchSource(String messageKey, String defaultName) {
		this.messageKey = messageKey;
		this.defaultName = defaultName;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getDescription()
	{
		Locale locale = LocaleContextHolder.getLocale();
		if (messageSource != null) {
			return messageSource.getMessage(this.messageKey, null, this.defaultName, locale);
		}
		else {
			return this.defaultName;
		}
	}
	
	@Component
	public static class ProcedureMatchSourceMessageSourceInjector {
		@Autowired
		private MessageSource messages; 

		@PostConstruct
        public void postConstruct() {
            for (ProcedureMatchSource ms : EnumSet.allOf(ProcedureMatchSource.class))
               ms.setMessageSource(messages);
        }
	}
}
