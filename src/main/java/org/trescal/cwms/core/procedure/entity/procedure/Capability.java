package org.trescal.cwms.core.procedure.entity.procedure;

import lombok.Setter;
import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.location.Location;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandardComparator;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.ProcedureAccreditationComparator;
import org.trescal.cwms.core.procedure.entity.procedureheading.CapabilityHeading;
import org.trescal.cwms.core.procedure.entity.procedurestandard.CapabilityStandard;
import org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.CapabilityTrainingRecord;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.system.entity.accreditedlab.AccreditedLab;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "capability")
@Setter
public class Capability extends Allocated<Subdiv> implements ComponentEntity {
    private Set<CapabilityAuthorization> authorizations;
    private boolean active;
    /**
     * The default {@link CalibrationProcess} used to perform calibratons with
     * this
     */
    private CalibrationProcess calibrationProcess;
    private List<CategorisedCapability> categories;
    private Contact deactivatedBy;
    private LocalDate deactivatedOn;
    private Address defAddress;
    private Location defLocation;
    private WorkInstruction defWorkInstruction;
    private String description;
    @Transient
    private File directory;
    private Integer estimatedTime;
    private CapabilityHeading heading;
    private int id;
    private AccreditedLab labAccreditation;
    private Set<InstrumentModel> models;
    private String name;
    private Set<CapabilityTrainingRecord> capabilityTrainingRecords;
    private String reference;
    private WorkRequirementType reqType;
    private Set<CapabilityStandard> standards;
    private List<ServiceCapability> serviceCapabilities;
    private Description subfamily;
    private Set<CapabilityFilter> capabilityFilters;
    private Set<JobItem> jobItems;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "capability")
    @SortComparator(ProcedureAccreditationComparator.class)
    public Set<CapabilityAuthorization> getAuthorizations() {
        return this.authorizations;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "calprocesid")
    public CalibrationProcess getCalibrationProcess() {
        return this.calibrationProcess;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "capability", orphanRemoval = true)
    public List<CategorisedCapability> getCategories() {
        return this.categories;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "deactivatedby")
    public Contact getDeactivatedBy() {
        return this.deactivatedBy;
    }

    @Column(name = "deactivatedon", columnDefinition = "date")
    public LocalDate getDeactivatedOn() {
        return this.deactivatedOn;
    }

    public void setDeactivatedOn(LocalDate deactivatedOn) {
        this.deactivatedOn = deactivatedOn;
    }

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "defaddressid", nullable = false)
	public Address getDefAddress() {
		return this.defAddress;
	}

	@Override
	@Transient
	public Contact getDefaultContact() {
		return null;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "deflocationid")
	public Location getDefLocation() {
		return this.defLocation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "defworkinstructionid")
	public WorkInstruction getDefWorkInstruction() {
		return this.defWorkInstruction;
	}

	@Override
	@Transient
	public File getDirectory() {
		return this.directory;
	}

	@Length(max = 2000)
	@Column(name = "description", length = 2000, columnDefinition = "nvarchar(2000)")
	public String getDescription() {
		return this.description;
	}

	/**
	 * @return estimated time for performing this procedure in minutes
	 */
	@Min(value = 0)
	@JoinColumn(name = "estimatedTime")
	public Integer getEstimatedTime() {
		return estimatedTime;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
    public int getId() {
        return this.id;
    }

    @Override
    @Transient
    public String getIdentifier() {
        return this.reference;
    }

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "headingid")
    public CapabilityHeading getHeading() {
        return this.heading;
    }

    @Transient
    public Set<InstrumentModel> getModels() {
        return this.models;
    }

    @NotNull
    @Length(min = 1, max = 200)
    @Column(name = "name", length = 200, nullable = false)
    public String getName() {
        return this.name;
    }

    @Override
    @Transient
    public ComponentEntity getParentEntity() {
        return null;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "capability")
    public Set<CapabilityTrainingRecord> getCapabilityTrainingRecords() {
        return this.capabilityTrainingRecords;
    }

    @NotNull
    @Length(min = 1, max = 20)
    @Column(name = "reference", nullable = false, length = 20, unique = true)
    public String getReference() {
        return this.reference;
    }

    /*
     * Changed to allow nullable (e.g. no accreditation) 2018-06-18 GB as part of capability evolutions
     * Related display / sticker printing tested.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "accreditedlabid")
    public AccreditedLab getLabAccreditation() {
        return this.labAccreditation;
    }

    @Override
    @Transient
    public List<Email> getSentEmails() {
        return null;
    }

    @OneToMany(mappedBy = "capability", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @SortComparator(DefaultStandardComparator.class)
    public Set<CapabilityStandard> getStandards() {
        return this.standards;
    }

    @OneToMany(mappedBy = "capability")
    public List<ServiceCapability> getServiceCapabilities() {
        return serviceCapabilities;
    }

    @OneToMany(mappedBy = "capability", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    public Set<CapabilityFilter> getCapabilityFilters() {
        return capabilityFilters;
    }

    @Override
    @Transient
    public boolean isAccountsEmail() {
        return false;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "reqtype", nullable = false)
    public WorkRequirementType getReqType() {
        return reqType;
    }

    @Column(name = "active", nullable = false, columnDefinition = "tinyint")
    public boolean isActive() {
        return this.active;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subfamilyid", foreignKey = @ForeignKey(name = "FK_procs_description"))
    public Description getSubfamily() {
        return subfamily;
    }

    @Override
    public void setSentEmails(List<Email> emails) {

    }

    @OneToMany(mappedBy = "capability")
    public Set<JobItem> getJobItems() {
        return jobItems;
    }

    @Transient
    public Set<ServiceType> getServiceTypes() {
        return capabilityFilters.stream().map(CapabilityFilter::getServicetype).collect(Collectors.toSet());
    }
}