package org.trescal.cwms.core.procedure.entity.workinstruction.db;

import lombok.val;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction_;
import org.trescal.cwms.core.procedure.form.SearchWorkInstructionForm;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import javax.persistence.criteria.*;
import java.util.*;

import static org.trescal.cwms.core.tools.StringJpaUtils.trimAndConcatWithWhitespace;

@Repository
public class WorkInstructionDaoImpl extends BaseDaoImpl<WorkInstruction, Integer> implements WorkInstructionDao {

    @Override
    protected Class<WorkInstruction> getEntity() {
        return WorkInstruction.class;
    }

    @Override
    public void searchWorkInstructions(PagedResultSet<WorkInstruction> pagedResults, SearchWorkInstructionForm form) {
        super.completePagedResultSet(pagedResults, WorkInstruction.class, cb -> cq -> {
            Root<WorkInstruction> root = cq.from(WorkInstruction.class);

            Predicate clauses = cb.conjunction();
            if (form.getCoid() != null && form.getCoid() != 0) {
                Join<WorkInstruction, Company> comp = root.join(WorkInstruction_.organisation.getName(), JoinType.INNER);
                clauses.getExpressions().add(cb.equal(comp.get(Company_.coid), form.getCoid()));
            }
            if (form.getName() != null && form.getName().length() > 0) {
				clauses.getExpressions().add(cb.like(root.get(WorkInstruction_.name), form.getName()));
			}
			if (form.getTitle() != null && form.getTitle().length() > 0) {
				clauses.getExpressions().add(cb.like(root.get(WorkInstruction_.title), form.getTitle()));
			}
			
			if(form.getDescid() != null){
				Join<WorkInstruction, Description> subFamily = root.join(WorkInstruction_.description, JoinType.LEFT);
				clauses.getExpressions().add(cb.equal(subFamily.get(Description_.id), form.getDescid()));
			}
			
			cq.where(clauses);
			
			List<Order> orders = new ArrayList<>();
			orders.add(cb.asc(root.get(WorkInstruction_.title)));
			orders.add(cb.asc(root.get(WorkInstruction_.id)));
			return Triple.of(root.get(WorkInstruction_.id), null, orders);
		});
    }

    @Override
    public List<KeyValueIntegerString> getAllKeyValues(String workInstructionFragment, Integer descriptionId,
                                                       Locale searchLanguage, int maxResults) {
        return getResultList(cb -> {
            CriteriaQuery<KeyValueIntegerString> cq = cb.createQuery(KeyValueIntegerString.class);
            Root<WorkInstruction> wi = cq.from(WorkInstruction.class);
            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.like(wi.get(WorkInstruction_.title), '%' + workInstructionFragment + '%'));
            if (descriptionId != null) {
                Join<WorkInstruction, Description> description = wi.join(WorkInstruction_.description, JoinType.LEFT);
                clauses.getExpressions().add(cb.equal(description.get(Description_.id), descriptionId));
            }
            cq.where(clauses);
            cq.orderBy(cb.asc(wi.get(WorkInstruction_.title)));
            cq.select(cb.construct(KeyValueIntegerString.class, wi.get(WorkInstruction_.id), wi.get(WorkInstruction_.title)));
            return cq;
        }, 0, maxResults);
    }

    @Override
    public Set<KeyValue<Integer, String>> getAllAsKeyValues() {
        return new TreeSet<>(getResultList(cb -> {
            val cq = cb.createQuery(KeyValueIntegerString.class);
            val workInstruction = cq.from(WorkInstruction.class);
            cq.select(cb.construct(KeyValueIntegerString.class, workInstruction.get(WorkInstruction_.id),
                trimAndConcatWithWhitespace(workInstruction.get(WorkInstruction_.title), cb.literal(":")).append(workInstruction.get(WorkInstruction_.name)).apply(cb)
            ));
            return cq;
        }));
    }
}