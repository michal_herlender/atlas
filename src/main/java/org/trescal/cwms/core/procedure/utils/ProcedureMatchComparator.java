package org.trescal.cwms.core.procedure.utils;

import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilterType;

import java.util.Comparator;

/**
 * Currently, the business subdivision that wishes to service the instrument is used as 
 * a reference to categorize the results
 *
 * In the future, we could expand this to use the address of the instrument (e.g. normally customer address)
 * to find the nearest Trescal subdivision etc... for instrument service on a large quotation etc... 
 *
 * @author galen
 *
 */
public class ProcedureMatchComparator implements Comparator<ProcedureMatch> {

	private int compare(ServiceCapability capability0, ServiceCapability capability1) {
		Integer result = null;
		if (capability0 == null && capability1 == null) {
			result = 0;
		}
		else if (capability0 == null && capability1 != null) {
			result = -1;	// Display the result with ServiceCapability before the one without   
		}
		else if (capability0 != null && capability1 == null) {
			result = 1;	// Display the result with ServiceCapability before the one without   
		}
		else {
			result = capability0.getCalibrationType().getServiceType().getOrder() -
					capability1.getCalibrationType().getServiceType().getOrder();
			if (result == 0) {
				result = capability0.getId() - capability1.getId();
			}
		}
		return result;
	}
	
	private int compare(CapabilityFilterType type0, CapabilityFilterType type1) {
		Integer result = null;
		if (type0 == null && type1 == null) {
			result = 0;
		}
		else if (type0 == null && type1 != null) {
			result = -1;	// Display the result with filter before the one without   
		}
		else if (type0 != null && type1 == null) {
			result = 1;		// Display the result with filter before the one without   
		}
		else {
			result = type0.compareTo(type1);
		}
		return result;
	}

	@Override
	public int compare(ProcedureMatch match0, ProcedureMatch match1) {
		// Compare by match source
		int result = match0.getMatchSource().compareTo(match1.getMatchSource());
		if (result == 0) {
			// Compare by procedure identity (always set)
            result = match0.getCapability().getId() - match1.getCapability().getId();
			if (result == 0) {
				// Compare by service capability
				result = compare(match0.getServiceCapability(), match1.getServiceCapability());
				if (result == 0) {
					// Compare by capability filter
					result = compare(match0.getFilterType(), match1.getFilterType()); 
				}
			}
		}
		return result;
	}

}
