package org.trescal.cwms.core.procedure.entity.procedure;

import lombok.Data;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.system.entity.calibrationtype.dto.CalibrationTypeOptionDto;

import java.util.List;

@Data
public class CapabilityInformation {

    private List<CalibrationTypeOptionDto> calTypes;
    private AccreditationLevel[] accredLevels;

    public CapabilityInformation(List<CalibrationTypeOptionDto> calTypes) {
        this.calTypes = calTypes;
        accredLevels = AccreditationLevel.values();
    }

}
