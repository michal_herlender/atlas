package org.trescal.cwms.core.procedure.entity.capabilityfilter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "capability_filter")
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CapabilityFilter extends Versioned {

    private int id;
    private Capability capability;
    private CapabilityFilterType filterType;
    private ServiceType servicetype;
    private String internalComments;
    // Defines whether capability is flagged as being a "best lab" by regional technical management
    private Boolean bestLab;

    private Boolean active;
    private WorkInstruction defaultWorkInstruction;
    private Integer standardTime;
    private String externalComments;

    @Column(name = "externalComments", length = 2000, columnDefinition = "nvarchar(2000)")
    public String getExternalComments() {
        return this.externalComments;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "defworkinstructionid")
    public WorkInstruction getDefaultWorkInstruction() {
        return defaultWorkInstruction;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return id;
    }

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "capabilityId", foreignKey = @ForeignKey(name = "FK_capabilityservicetype_procs"), nullable = false)
    public Capability getCapability() {
        return capability;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "filtertype", nullable = false)
    public CapabilityFilterType getFilterType() {
        return filterType;
    }


    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "servicetypeid", foreignKey = @ForeignKey(name = "FK_capabilityservicetype_servicetype"), nullable = false)
    public ServiceType getServicetype() {
        return servicetype;
    }

    @NotNull
    @Length(max = 2000)
    @Column(name = "internalComments", length = 2000, nullable = false, columnDefinition = "nvarchar(2000)")
    public String getInternalComments() {
        return internalComments;
    }

    @NotNull
    @Column(name = "bestlab", nullable = false, columnDefinition = "bit default 0")
    public Boolean getBestLab() {
        return bestLab;
    }

    public Integer getStandardTime() {
        return standardTime;
    }

    @Column(name = "active", nullable = false)
    public Boolean getActive() {
        return active;
    }

}
