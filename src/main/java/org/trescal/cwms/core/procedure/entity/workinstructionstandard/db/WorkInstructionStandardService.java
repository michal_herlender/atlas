package org.trescal.cwms.core.procedure.entity.workinstructionstandard.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.procedure.entity.workinstructionstandard.WorkInstructionStandard;

public interface WorkInstructionStandardService extends BaseService<WorkInstructionStandard, Integer>
{
}