package org.trescal.cwms.core.procedure.entity.defaultstandard.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandard;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandardType;
import org.trescal.cwms.core.procedure.entity.procedurestandard.CapabilityStandard;
import org.trescal.cwms.core.procedure.entity.workinstructionstandard.WorkInstructionStandard;

import java.util.List;

public interface DefaultStandardDao extends BaseDao<DefaultStandard, Integer> {
    List<DefaultStandard> getAllDefaultStandardsForInstument(int plantid, DefaultStandardType type);

    CapabilityStandard getDefaultStandardByProcPlant(int procid, int plantid);

    WorkInstructionStandard getDefaultStandardByWIPlant(int procid, int plantid);

    List<CapabilityStandard> getStandardsForProcedure(Integer capabilityId, Boolean enforced);

    List<WorkInstructionStandard> getStandardsForWorkInstruction(int wiid, Boolean enforced);
}