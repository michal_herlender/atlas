package org.trescal.cwms.core.procedure.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.LabelIdDTO;

import java.util.List;

@Controller
@JsonController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV})
public class SearchProcedureTags {
    @Autowired
    private CapabilityService capabilityService;

    public static int MAX_RESULTS = 50;
    public static String SEPARATOR = " - ";

    @RequestMapping(value = "/searchprocedures.json", method = RequestMethod.GET)
    public @ResponseBody
    List<LabelIdDTO> getTagList(
        @RequestParam(name = "term", required = true) String searchTerm,
        @RequestParam(name = "subdivId", required = false, defaultValue = "0") int subdivId,
        @RequestParam(name = "modelId", required = false) Integer modelId,
        @RequestParam(name = "type", required = false) String type,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> allocatedSubdivDto) {
        if (subdivId == 0) {
			subdivId = allocatedSubdivDto.getKey();
		}
		String referenceTerm = searchTerm;
		String nameTerm = searchTerm;
		boolean andTrueOrFalse = false;        // By default, perform OR search
		// If in format "reference - name" then search term is aggregated - split on first occurrence, perform AND 
		if (searchTerm.contains(SEPARATOR)) {
			andTrueOrFalse = true;
			String[] tokens = searchTerm.split(SEPARATOR, 2);
			referenceTerm = tokens[0];
			if (tokens.length > 1) nameTerm = tokens[1];
			else nameTerm = "";
		}

        return capabilityService.getLabelIdList(referenceTerm, nameTerm, andTrueOrFalse, subdivId, modelId, MAX_RESULTS, type);
	}
	
	@RequestMapping(value="/proc.json", method=RequestMethod.GET)
	@ResponseBody
	public String getProcName(
			@RequestParam(value="procId", required=false) String procId) {
		if(StringUtils.isEmpty(procId)){
			return "";
		} else {
            Capability proc = this.capabilityService.get(Integer.parseInt(procId));
			return proc != null ? proc.getReference() : "";
		}
		
	}
}
