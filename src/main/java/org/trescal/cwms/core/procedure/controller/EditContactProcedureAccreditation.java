package org.trescal.cwms.core.procedure.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.procedure.form.EditContactCapabilityAccreditationForm;
import org.trescal.cwms.core.procedure.form.EditContactProcedureAccreditationValidator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class EditContactProcedureAccreditation {

    @Autowired
    private CalibrationTypeService caltypeServ;
    @Autowired
    private ContactService contactServ;
    @Autowired
    private CapabilityAuthorizationService procAccredServ;
    @Autowired
    private CapabilityService procServ;
    @Autowired
    private EditContactProcedureAccreditationValidator validator;

    protected final Log logger = LogFactory.getLog(getClass());
    public static final String FORM_NAME = "command";

    @ModelAttribute(FORM_NAME)
    protected EditContactCapabilityAccreditationForm formBackingObject(
        @RequestParam(value = "personid", required = false, defaultValue = "0") Integer personId,
        @RequestParam(value = "procid", required = false, defaultValue = "0") Integer procId,
        @RequestParam(value = "deptid", required = false, defaultValue = "0") Integer deptId,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
        Contact con = this.contactServ.get(personId);
        if ((con == null) || (personId == 0)) {
            this.logger.error("Could not find contact with id " + personId);
            throw new Exception("Could not find the requested user");
        }
        Capability proc = this.procServ.get(procId);
        if ((proc == null) || (procId == 0)) {
            this.logger.error("Could not find procedure with id " + procId);
            throw new Exception("Could not find the requested procedure");
        }
        EditContactCapabilityAccreditationForm form = new EditContactCapabilityAccreditationForm();
        form.setContact(con);
        form.setCapability(proc);
        form.setDeptid(deptId);
        List<CalibrationType> caltypes = proc.getServiceTypes().stream().map(ServiceType::getCalibrationType).collect(Collectors.toList());
        // create lists of accreditation dates + levels
        ArrayList<LocalDate> accredOns = new ArrayList<>(caltypes.size());
        ArrayList<AccreditationLevel> accredLevels = new ArrayList<>(caltypes.size());
        // find any current accreditations
        List<CapabilityAuthorization> authorizations = this.procAccredServ.getCapabilityAuthorizations(procId, null, personId);
        List<CalibrationType> cals = new ArrayList<>(caltypes.size());
        // for each caltype, add an accreditation date + level if the contact is
        // already accredited, or null values if not
        for (CalibrationType ct : caltypes) {
            boolean accredFound = false;
            cals.add(ct);
            for (CapabilityAuthorization ac : authorizations) {
                // contact already has an accreditation for this proc & caltype
                if (ac.getCaltype().getCalTypeId() == ct.getCalTypeId()) {
                    accredOns.add(ac.getAwardedOn());
                    accredLevels.add(ac.getAccredLevel());
                    accredFound = true;
                    break;
                }
            }
            // no accreditation so use null placeholders instead
            if (!accredFound) {
                accredOns.add(null);
                accredLevels.add(null);
            }
        }
        form.setCurrentCapabilityAuthorizations(authorizations);
        form.setCalTypes(cals);
		form.setAccredOns(accredOns);
		form.setAccredLevels(accredLevels);
		return form;
	}
	
	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) throws Exception
	{
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}

	@RequestMapping(value="/editcontactprocedureaccreditation.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(
        @Validated @ModelAttribute(FORM_NAME) EditContactCapabilityAccreditationForm form, BindingResult bindingResult,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username)
	{
		if (bindingResult.hasErrors()) return referenceData();
		// edit contact procedure accreditation
        this.procAccredServ.editContactCapabilityAuthorization(form, username);
		return new ModelAndView(new RedirectView("/viewcontactprocedureaccreditations.htm?deptid="
				+ form.getDeptid()
				+ "&personid="
				+ form.getContact().getPersonid(), true));
	}

	@RequestMapping(value = "/editcontactprocedureaccreditation.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData() {
		Map<String, Object> model = new HashMap<>();
		model.put("accredlevels", AccreditationLevel.values());
		return new ModelAndView("trescal/core/procedure/editcontactprocedureaccreditation", model);
	}
}
