package org.trescal.cwms.core.procedure.form;

import lombok.Data;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Data
public class EditContactCapabilityAccreditationForm {
    private Contact contact;
    private Capability capability;
    private List<CapabilityAuthorization> currentCapabilityAuthorizations;

    private Date generalAccredOn;
    private AccreditationLevel generalAccredLevel;

    private List<CalibrationType> calTypes;
    private List<LocalDate> accredOns;
    private List<AccreditationLevel> accredLevels;

    private int deptid;

}