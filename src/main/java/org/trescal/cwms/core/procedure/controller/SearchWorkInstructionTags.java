package org.trescal.cwms.core.procedure.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.trescal.cwms.core.exception.controller.JsonController;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.procedure.entity.workinstruction.db.WorkInstructionService;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

@Controller
@JsonController
public class SearchWorkInstructionTags {

	@Autowired
	private WorkInstructionService workInstructionService;
	@Autowired
	private InstrumentModelService instrumentModelService;

	public static int MAX_RESULTS = 50;

	@RequestMapping(value = "/searchmodelcodetags.json", method = RequestMethod.GET)
	public @ResponseBody List<KeyValueIntegerString> getTagList(
			@RequestParam(name = "modelid", required = false, defaultValue="0") Integer modelid,
			@RequestParam(name="term") String workInstructionFragment, Locale userLocale) {
		InstrumentModel instrumentModel = instrumentModelService.get(modelid);
		return workInstructionService.getAllKeyValues(workInstructionFragment,
				instrumentModel != null ? instrumentModel.getDescription().getId() : null, userLocale, MAX_RESULTS);
	}

}
