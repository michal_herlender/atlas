package org.trescal.cwms.core.procedure.entity.categorisedprocedure;

import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.tools.Strings;

import java.util.Comparator;

/**
 * Implementation of {@link Comparator} which sorts {@link Capability} entities
 * by reference number.
 *
 * @author richard
 */
public class CategorisedProcedureComparator implements Comparator<CategorisedCapability> {
    @Override
    public int compare(CategorisedCapability o1, CategorisedCapability o2) {
        if ((o1.getCapability().getReference() == null)
            && (o2.getCapability().getReference() == null)) {
            return ((Integer) o1.getCapability().getId()).compareTo(o2.getCapability().getId());
        } else if ((o1.getCapability().getReference() != null)
            && (o2.getCapability().getReference() == null)) {
            return 1;
        } else if ((o1.getCapability().getReference() == null)
            && (o2.getCapability().getReference() != null)) {
            return -1;
        } else {
            if (o1.getCapability().getReference().trim().equals(o2.getCapability().getReference().trim())) {
                return ((Integer) o1.getCapability().getId()).compareTo(o2.getCapability().getId());
            } else {
                return Strings.getNaturalComparator().compare(o1.getCapability().getReference(), o2.getCapability().getReference());
                // return
                // o1.getCapability().getReference().compareTo(o2.getProcedure
                // ().getReference());
            }
        }

    }
}
