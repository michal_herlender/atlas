package org.trescal.cwms.core.procedure.form;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;

import java.time.LocalDate;

@Component
public class EditContactProcedureAccreditationValidator implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(EditContactCapabilityAccreditationForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
        EditContactCapabilityAccreditationForm form = (EditContactCapabilityAccreditationForm) target;

		for (int i = 0; i < form.getCalTypes().size(); i++)
		{
			// only validate the date if an accreditation level is actually
			// being set
			if ((form.getAccredLevels().get(i) != null)
				&& !AccreditationLevel.NONE.equals(form.getAccredLevels().get(i))) {
				LocalDate d = form.getAccredOns().get(i);

				// null dates are no longer allowed
				if (d == null) {
					errors.rejectValue("accredOns[" + i + "]", "error.accreddate.blank", null, "Accreditation dates must be provided");
				} else if (d.isAfter(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()))) {
					errors.rejectValue("accredOns[" + i + "]", "error.accreddate.future", null, "Accreditation dates cannot be in the future");
				}
			}
		}
	}
}