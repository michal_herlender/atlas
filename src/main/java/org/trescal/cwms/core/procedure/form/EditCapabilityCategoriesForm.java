package org.trescal.cwms.core.procedure.form;

import org.springframework.util.AutoPopulatingList;

import javax.validation.Valid;

/**
 * Form is used for updating / deleting / adding procedure categories for a given subdivision
 * Existing and potentially new procedure categories are contained in the same DTO.
 */
public class EditCapabilityCategoriesForm {
    @Valid
    private AutoPopulatingList<EditCapabilityCategoryInput> inputDTOs;

    public EditCapabilityCategoriesForm() {
        inputDTOs = new AutoPopulatingList<EditCapabilityCategoryInput>(EditCapabilityCategoryInput.class);
    }

    public AutoPopulatingList<EditCapabilityCategoryInput> getInputDTOs() {
        return inputDTOs;
    }

    public void setInputDTOs(AutoPopulatingList<EditCapabilityCategoryInput> inputDTOs) {
        this.inputDTOs = inputDTOs;
    }
}