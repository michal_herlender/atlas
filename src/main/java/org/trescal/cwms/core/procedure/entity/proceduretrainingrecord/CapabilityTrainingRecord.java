package org.trescal.cwms.core.procedure.entity.proceduretrainingrecord;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;

import javax.persistence.*;

@Entity
@Table(name = "capability_trainingrecord")
public class CapabilityTrainingRecord {
    private Certificate cert;
    private int id;
    private Contact labManager;
    private Capability capability;
    private Contact trainee;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "certid")
    public Certificate getCert() {
        return this.cert;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "labmanager", nullable = false)
    public Contact getLabManager() {
        return this.labManager;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "capabilityId", nullable = false)
    public Capability getCapability() {
        return this.capability;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trainee", nullable = false)
    public Contact getTrainee() {
        return this.trainee;
    }

    public void setCert(Certificate cert) {
		this.cert = cert;
	}

    public void setId(int id) {
        this.id = id;
    }

    public void setLabManager(Contact labManager) {
        this.labManager = labManager;
    }

    public void setCapability(Capability capability) {
        this.capability = capability;
    }

    public void setTrainee(Contact trainee) {
        this.trainee = trainee;
    }

}
