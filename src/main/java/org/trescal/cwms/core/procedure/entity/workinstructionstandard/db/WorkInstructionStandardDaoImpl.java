package org.trescal.cwms.core.procedure.entity.workinstructionstandard.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.procedure.entity.workinstructionstandard.WorkInstructionStandard;

@Repository("WorkInstructionStandardDao")
public class WorkInstructionStandardDaoImpl extends BaseDaoImpl<WorkInstructionStandard, Integer> implements WorkInstructionStandardDao
{
	@Override
	protected Class<WorkInstructionStandard> getEntity() {
		return WorkInstructionStandard.class;
	}
}