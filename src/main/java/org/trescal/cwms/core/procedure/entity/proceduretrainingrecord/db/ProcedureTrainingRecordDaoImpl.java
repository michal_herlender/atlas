package org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.db;

import lombok.val;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.CapabilityTrainingRecord;
import org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.CapabilityTrainingRecord_;

import java.util.Optional;

@Repository("ProcedureTrainingRecordDao")
public class ProcedureTrainingRecordDaoImpl extends BaseDaoImpl<CapabilityTrainingRecord, Integer> implements ProcedureTrainingRecordDao {

    @Override
    protected Class<CapabilityTrainingRecord> getEntity() {
        return CapabilityTrainingRecord.class;
    }

    public Optional<CapabilityTrainingRecord> findCapabilityTrainingRecordViaCertificate(int certid) {
        return getFirstResult(cb -> {
            val cq = cb.createQuery(CapabilityTrainingRecord.class);
            val trainingRecord = cq.from(CapabilityTrainingRecord.class);
            cq.where(cb.equal(trainingRecord.get(CapabilityTrainingRecord_.cert), certid));
            return cq;
        });
    }
}