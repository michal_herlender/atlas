package org.trescal.cwms.core.procedure.utils;

import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilterType;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;

public class ProcedureMatch {
    private final ProcedureMatchSource matchSource;        // Mandatory
    private final Capability capability;                    // Mandatory
    private ServiceCapability serviceCapability;    // Used for INSTRUMENTMODEL MatchSource only
    private final Boolean indicateMatch;                    // Mandatory, indicates selectabilty
    private CapabilityFilter bestFilter;            // Used for SUBFAMILY_PROCEDURE only, indicates best matching filter
    private CapabilityFilterType filterType;        // Used for SUBFAMILY_PROCEDURE only, indicates best matching filter

    public ProcedureMatch(Capability capability, CapabilityFilter bestFilter) {
        this.matchSource = ProcedureMatchSource.SUBFAMILY_PROCEDURE;
        this.capability = capability;
        this.bestFilter = bestFilter;
        this.filterType = bestFilter != null ? bestFilter.getFilterType() : CapabilityFilterType.NO;
        this.indicateMatch = CapabilityFilterType.YES.equals(filterType) ||
            CapabilityFilterType.MAYBE.equals(filterType);
    }

    public ProcedureMatch(ServiceCapability serviceCapability, boolean indicateMatch) {
        this.matchSource = ProcedureMatchSource.INSTRUMENTMODEL_CAPABILITY;
        this.serviceCapability = serviceCapability;
        this.capability = serviceCapability.getCapability();
        this.indicateMatch = indicateMatch;
    }

    public ProcedureMatch(Capability capability) {
        this.matchSource = ProcedureMatchSource.GENERIC_PROCEDURE;
        this.capability = capability;
        this.indicateMatch = true;
    }

    public ProcedureMatchSource getMatchSource() {
        return matchSource;
    }

    public Capability getCapability() {
        return capability;
    }

    public ServiceCapability getServiceCapability() {
        return serviceCapability;
    }

    public Boolean getIndicateMatch() {
        return indicateMatch;
    }

    public CapabilityFilterType getFilterType() {
        return filterType;
    }

    public CapabilityFilter getBestFilter() {
        return bestFilter;
    }
}
