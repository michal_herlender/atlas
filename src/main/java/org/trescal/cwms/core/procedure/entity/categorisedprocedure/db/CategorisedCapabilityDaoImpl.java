package org.trescal.cwms.core.procedure.entity.categorisedprocedure.db;

import lombok.val;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability_;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class CategorisedCapabilityDaoImpl extends BaseDaoImpl<CategorisedCapability, Integer> implements CategorisedCapabilityDao {

    @Override
    protected Class<CategorisedCapability> getEntity() {
        return CategorisedCapability.class;
    }

    public List<CategorisedCapability> getAllEagerCategorisedCapabilities() {
        return getResultList(cb -> {
            val cq = cb.createQuery(CategorisedCapability.class);
            val categorised = cq.from(CategorisedCapability.class);
            categorised.fetch(CategorisedCapability_.capability, JoinType.INNER);
            categorised.fetch(CategorisedCapability_.category, JoinType.INNER);
            return cq;
        });
    }

    @Override
    public long countForCapabilityCategory(CapabilityCategory capabilityCategory) {
        return getSingleResult(cb -> {
            CriteriaQuery<Long> cq = cb.createQuery(Long.class);
            Root<CategorisedCapability> root = cq.from(CategorisedCapability.class);
            cq.where(cb.equal(root.get(CategorisedCapability_.category), capabilityCategory));
            cq.select(cb.count(root));
            return cq;
        });
    }

    @Override
    public List<CategorisedCapability> findByCapabilityCategory(CapabilityCategory capabilityCategory) {
        return getResultList(cb -> {
            CriteriaQuery<CategorisedCapability> cq = cb.createQuery(CategorisedCapability.class);
            Root<CategorisedCapability> root = cq.from(CategorisedCapability.class);
            cq.where(cb.equal(root.get(CategorisedCapability_.category), capabilityCategory));
            return cq;
        });
    }
}