package org.trescal.cwms.core.procedure.dto;

import org.trescal.cwms.spring.model.KeyValue;

public class ProcedureKeyValue extends KeyValue<Integer, String> {

	public ProcedureKeyValue(Integer procId, String name) {
		super(procId, name);
	}
}