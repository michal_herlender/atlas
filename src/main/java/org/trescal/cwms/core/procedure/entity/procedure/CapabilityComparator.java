package org.trescal.cwms.core.procedure.entity.procedure;

import java.util.Comparator;

/**
 * Inner comparator class for sorting {@link Capability}s when returning
 * collections
 *
 * @author jamiev
 */
public class CapabilityComparator implements Comparator<Capability> {
    public int compare(Capability proc1, Capability proc2) {
        return proc1.getReference().compareTo(proc2.getReference());
    }
}