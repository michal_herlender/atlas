package org.trescal.cwms.core.procedure.entity.procedureaccreditation.db;

import io.vavr.control.Either;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.procedure.dto.CapabilityAuthorizationForm;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;
import org.trescal.cwms.core.procedure.form.EditContactCapabilityAccreditationForm;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface CapabilityAuthorizationService {
    /**
     * Determines if the {@link Contact} identified by the given id is
     * accredited to perform the {@link Capability} identified by the given
     * procid at the given {@link CalibrationType}. Caltypeid can be left null
     * to search for generic accreditations as opposed to
     * {@link CalibrationType} specific.
     *
     * @param procid    the id of the {@link Capability}.
     * @param caltypeid the id of the {@link CalibrationType}, nullable.
     * @param personid  the id of the {@link Contact}.
     */
    Either<String, AccreditationLevel> authorizedFor(int procid, Integer caltypeid, int personid);

    void deleteCapabilityAuthorization(CapabilityAuthorization capabilityAuthorization);

    /**
     * Returns the {@link CapabilityAuthorization} for the specified
     * {@link Capability}, {@link CalibrationType} and {@link Contact}, null if
     * none matching.
     *
     * @param capabilityId the id of the {@link Capability}.
     * @param caltypeid    the id of the {@link CalibrationType}.
     * @param personid     the id of the {@link Contact}.
     * @return matching {@link CapabilityAuthorization} or null.
     */
    Optional<CapabilityAuthorization> findCapabilityAuthorization(int capabilityId, Integer caltypeid, int personid);

    /**
     * Return's a list of all {@link CapabilityAuthorization} entries in the
     * system with the accreditation {@link Contact} eagerly loaded.
     */
    List<CapabilityAuthorization> getAllEagerCapabilityAuthorizations();

    /**
     * Returns a list of all {@link CapabilityAuthorization} entities that match
     * the given {@link Capability}, {@link CalibrationType} and {@link Contact}
     * details.
     *
     * @param capabilityId the id of the {@link Capability}.
     * @param caltypeid    the id of the {@link CalibrationType}, nullable.
     * @param personid     the id of the {@link Contact}.
     * @return list of matching {@link CapabilityAuthorization} entities.
     */
    List<CapabilityAuthorization> getCapabilityAuthorizations(int capabilityId, Integer caltypeid, int personid);

    void insertCapabilityAuthorization(CapabilityAuthorization capabilityAuthorization);

    void saveOrUpdateCapabilityAuthorization(CapabilityAuthorization capabilityAuthorization);

    void updateCapabilityAuthorization(CapabilityAuthorization capabilityAuthorization);

    /**
     * Tests that the {@link Contact} editor is able to update the
     * {@link CapabilityAuthorization} for the given {@link Capability} and the
     * given contact {@link Contact}. This method is a supplement to an initial
     * layer of Acegi authentication that should be applied to each url that
     * requires protection and is called from interceptors. This function checks
     * that a manager is a department manager of the department the procedure
     * belongs to and that the manager is not attempting to update his own
     * profile.
     *
     * @param editor     the {@link Contact} performing the editing.
     * @param capability the {@link Capability} being edited.
     * @param authorized the {@link Contact} whose profile is being updated.
     * @return {@link ResultWrapper} indicating if this was a success and a
     * message indicating the reason for a failure.
     */
    ResultWrapper userCanUpdateContactCapabilityAuthorization(Contact editor, Capability capability, Contact authorized,
                                                              HttpSession session);

    /**
     * Wrapper method which prepares a call to
     * <p>
     * using a {@link HttpServletRequest} request.
     *
     * @param req {@link HttpServletRequest}.
     * @return {@link ResultWrapper} indicating if this was a success and a
     * message indicating the reason for a failure.
     */
    ResultWrapper userCanUpdateContactCapabilityAuthorization(HttpServletRequest req);

    Boolean userCanUpdateContactCapabilityAuthorization(Contact editor, Capability capability, Contact accredited,
                                                        KeyValue<Integer, String> subdivDtoId);

    List<CapabilityAuthorization> getAllAuthorizationsForCapability(int capabilityId);

    void saveOrUpdateAuthorizationsForCapability(CapabilityAuthorizationForm form, Contact editor);

    Map<Contact, List<CapabilityAuthorization>> authorizedContactsWithThereAuthorizations(Integer capabilityId, Contact editor, KeyValue<Integer, String> subdivDtoId);

    List<Contact> getNonAuthorizedContacts(Integer capabilityId, Map<Contact, List<CapabilityAuthorization>> authorizedContacts, Contact editor, KeyValue<Integer, String> subdivDtoId);

    void editContactCapabilityAuthorization(EditContactCapabilityAccreditationForm form, String username);

}