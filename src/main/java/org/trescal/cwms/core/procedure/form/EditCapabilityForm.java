package org.trescal.cwms.core.procedure.form;

import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

public class EditCapabilityForm {
    private Boolean supplierCapability;        // true = supplier capability, false = business capability
    private Integer coid;                    // Used for supplier capabilities only
    private Integer subdivid;                // Used for supplier capabilities only
    private Integer addrid;                    // Used for supplier capabilities only

    private Integer accreditedLabId;
    private Boolean active;
    private Integer calProcessId;
    private Set<Integer> categoryids;
    private Integer defAddressId;        // Used for business capabilities only
    private String description;
    private Integer estimatedTime;
    private String name;
    private Integer capabilityId;
    private String reference;
    private WorkRequirementType reqType;
    private Integer workInstructionId;
    private Integer subfamilyId;
    private String subfamilyName;

    public static final String ERROR_CODE_VALUE_NOT_SELECTED = "{error.value.notselected}";

    // Validation groups
    public interface SupplierCompany {
    }

    ;

    public interface BusinessCompany {
    }

    ;

    @NotNull
    public Boolean getSupplierCapability() {
        return supplierCapability;
    }

    @NotNull(message = ERROR_CODE_VALUE_NOT_SELECTED, groups = SupplierCompany.class)
    public Integer getCoid() {
        return coid;
    }

    @NotNull(message = ERROR_CODE_VALUE_NOT_SELECTED, groups = SupplierCompany.class)
    public Integer getSubdivid() {
        return subdivid;
    }

    @NotNull(message = ERROR_CODE_VALUE_NOT_SELECTED, groups = SupplierCompany.class)
    public Integer getAddrid() {
        return addrid;
    }
		
	@NotNull(message=ERROR_CODE_VALUE_NOT_SELECTED, groups=BusinessCompany.class)
	public Integer getAccreditedLabId()
	{
		return this.accreditedLabId;
	}

	@NotNull
	public Boolean getActive()
	{
		return this.active;
	}

	// Optional even for business company
	public Integer getCalProcessId()
	{
		return this.calProcessId;
	}

	public Set<Integer> getCategoryids() {
		return categoryids;
	}

	@Min(value=1, message=ERROR_CODE_VALUE_NOT_SELECTED, groups=BusinessCompany.class)
	public Integer getDefAddressId()
	{
		return this.defAddressId;
	}

	@NotNull
	@Size(min = 0, max = 2000)
	public String getDescription() {
		return description;
	}

	@Min(value=0)
    public Integer getEstimatedTime() {
        return estimatedTime;
    }

    @NotNull
    @Size(min = 1, max = 200)
    public String getName() {
        return name;
    }

    @NotNull
    public Integer getCapabilityId() {
        return this.capabilityId;
    }

    @NotNull
    @Size(min = 1, max = 20)
    public String getReference() {
        return reference;
    }

    @NotNull(message = ERROR_CODE_VALUE_NOT_SELECTED, groups = BusinessCompany.class)
    public WorkRequirementType getReqType() {
        return reqType;
	}

	public Integer getWorkInstructionId()
	{
		return this.workInstructionId;
	}

	public void setAccreditedLabId(Integer accreditedLabId)
	{
		this.accreditedLabId = accreditedLabId;
	}

	public void setActive(Boolean active)
	{
		this.active = active;
	}

    public void setCalProcessId(Integer calProcessId) {
        this.calProcessId = calProcessId;
    }

    public void setDefAddressId(Integer defAddressId) {
        this.defAddressId = defAddressId;
    }

    public void setCapabilityId(Integer capabilityId) {
        this.capabilityId = capabilityId;
    }

    public void setReqType(WorkRequirementType reqType) {
        this.reqType = reqType;
    }

    public void setWorkInstructionId(Integer workInstructionId) {
        this.workInstructionId = workInstructionId;
    }

	public Integer getSubfamilyId() {
		return subfamilyId;
	}

	public void setSubfamilyId(Integer subfamilyId) {
		this.subfamilyId = subfamilyId;
	}

	public String getSubfamilyName() {
		return subfamilyName;
	}

	public void setSubfamilyName(String subfamilyName) {
		this.subfamilyName = subfamilyName;
	}

	public void setCategoryids(Set<Integer> categoryids) {
		this.categoryids = categoryids;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setEstimatedTime(Integer estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public void setSupplierCapability(Boolean supplierCapability) {
		this.supplierCapability = supplierCapability;
	}

	public void setCoid(Integer coid) {
		this.coid = coid;
	}

	public void setSubdivid(Integer subdivid) {
		this.subdivid = subdivid;
	}

	public void setAddrid(Integer addressid) {
		this.addrid = addressid;
	}
}