package org.trescal.cwms.core.procedure.form;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;

/*
 * DTO for adding / editing / deleting procedure categories
 * For additions, only the name and description are used
 * For updates / deletions, all inputs are used
 */
public class EditCapabilityCategoryInput {

    private Integer id;
    private Integer deptId;
    private String name;
    private String description;
    private Boolean departmentDefault;
    private Boolean delete;
    private Boolean add;
    private Boolean splitTraceable;

    // Default constructor for Spring binding / new procedure categories
    public EditCapabilityCategoryInput() {
        departmentDefault = false;
        delete = false;
        add = false;
        splitTraceable = false;
    }

    // Constructor for editing existing procedure category
    public EditCapabilityCategoryInput(CapabilityCategory pc) {
        this.id = pc.getId();
        this.deptId = pc.getDepartment().getId();
        this.name = pc.getName();
        this.description = pc.getDescription();
        this.departmentDefault = pc.isDepartmentDefault();
        this.splitTraceable = pc.getSplitTraceable();
        this.delete = false;
        this.add = false;
    }

    public Integer getId() {
        return id;
    }

    public Integer getDeptId() {
        return deptId;
    }

    @Length(min = 1, max = 50)
	public String getName() {
		return name;
	}

	@Length(min=1, max=100)
	public String getDescription() {
		return description;
	}

	public Boolean getDepartmentDefault() {
		return departmentDefault;
	}

	public Boolean getDelete() {
		return delete;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDepartmentDefault(Boolean departmentDefault) {
		this.departmentDefault = departmentDefault;
	}

	public void setDelete(Boolean delete) {
        this.delete = delete;
    }

    public Boolean getAdd() {
        return add;
    }

    public void setAdd(Boolean add) {
        this.add = add;
    }

    public Boolean getSplitTraceable() {
        return splitTraceable;
    }

    public void setSplitTraceable(Boolean splitTraceable) {
        this.splitTraceable = splitTraceable;
    }
}
