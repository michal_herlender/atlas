package org.trescal.cwms.core.procedure.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.domain.db.InstrumentModelDomainService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db.InstrumentModelFamilyService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.spring.model.InstrumentModelDescriptionJsonDto;
import org.trescal.cwms.spring.model.InstrumentModelDomainJsonDTO;
import org.trescal.cwms.spring.model.InstrumentModelFamilyJsonDTO;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.*;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV})
public class ViewCapabilitiesController {
    @Autowired
    private NewDescriptionService descriptionService;
    @Autowired
    private InstrumentModelDomainService domainService;
    @Autowired
    private InstrumentModelFamilyService familyService;
    @Autowired
    private CapabilityService capabilityService;
    @Autowired
    private SubdivService subdivService;

    /**
     *
     */
    @RequestMapping(value = "viewcapabilities.htm")
    public String referenceData(Model model, Locale locale,
                                @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                @RequestParam(name = "subdivid", required = false, defaultValue = "0") Integer reqSubdivid,
                                @RequestParam(name = "domainid", required = false, defaultValue = "-1") Integer reqDomainid) {
        // Use user's logged in subdivision if none specified
        Subdiv subdiv = this.subdivService.get(reqSubdivid == 0 ? subdivDto.getKey() : reqSubdivid);
        List<InstrumentModelDomainJsonDTO> domainList = new ArrayList<>(this.domainService.getAllDomains(locale));
        domainList.add(getNoneDomain());
        InstrumentModelDomainJsonDTO domain = getSelectedDomain(domainList, reqDomainid);

        model.addAttribute("domainList", domainList);
        if (domain.getDomainid() > 0) {
            // Not applicable for 'None' domain with ID 0
            List<InstrumentModelFamilyJsonDTO> familyList = familyService.getAllForDomain(locale, domain.getDomainid());
            List<Capability> capabilities = this.capabilityService.getAllForDomainAndSubdiv(domain.getDomainid(), subdiv);
            Map<Integer, Map<Integer, String>> subfamilyMap = getSubfamilyMap(familyList, locale);
            Set<Integer> subfamilyIds = getSubfamilyIds(subfamilyMap);
            model.addAttribute("familyList", familyList);
            model.addAttribute("subfamilyMap", subfamilyMap);
            model.addAttribute("capabilityCountActive", getCapabilityCount(capabilities, true));
            model.addAttribute("capabilityCountInactive", getCapabilityCount(capabilities, false));
            model.addAttribute("capabilityMap", getCapabilityMap(capabilities));
            model.addAttribute("subfamilyCountWithCapability", getSubfamilyCount(capabilities, subfamilyIds, true));
            model.addAttribute("subfamilyCountWithoutCapability", getSubfamilyCount(capabilities, subfamilyIds, false));
        }
		else {
            model.addAttribute("proceduresWithoutSubfamily", this.capabilityService.getAllForDomainAndSubdiv(null, subdiv));
		}
		model.addAttribute("selectedDomain", domain);
		model.addAttribute("selectedSubdiv", subdiv);
		
		return "trescal/core/procedure/viewcapabilities";
	}
	
	private InstrumentModelDomainJsonDTO getNoneDomain() {
		return new InstrumentModelDomainJsonDTO(0, "None", false, null);
	}
	
	private InstrumentModelDomainJsonDTO getSelectedDomain(List<InstrumentModelDomainJsonDTO> listDomains, Integer domainid) {
        InstrumentModelDomainJsonDTO result;
		if (domainid == -1) {
			result = listDomains.get(0);	// No domain specified, return first domain in list
		}
		else {
            result = listDomains.stream().filter(domain -> Objects.equals(domain.getDomainid(), domainid)).findFirst().orElse(null);
		}
		return result;
	}
	
	/**
	 * Initializes a map (aka familyMap) where:
	 * map 0 - family ids to map 1
	 * map 1 - of subfamily ids to translated subfamily names
	 */
	private Map<Integer,Map<Integer,String>> getSubfamilyMap(List<InstrumentModelFamilyJsonDTO> familyList, Locale locale) {
		Map<Integer,Map<Integer,String>> map0 = new TreeMap<>();
		for (InstrumentModelFamilyJsonDTO familyDto : familyList) {
			Map<Integer,String> map1 = new TreeMap<>();
			map0.put(familyDto.getFamilyid(), map1);
			List<InstrumentModelDescriptionJsonDto> listSubfamily =
					descriptionService.findDescriptionsForFamily(familyDto.getFamilyid(), locale);
			for (InstrumentModelDescriptionJsonDto dtoSubfamily : listSubfamily) {
				map1.put(dtoSubfamily.getId(), dtoSubfamily.getBestTranslation());
				
			}
		}
		return map0;
    }

    /*
     * Returns list of unique subfamily IDs for specified map
     */
    private Set<Integer> getSubfamilyIds(Map<Integer, Map<Integer, String>> familyMap) {
        Set<Integer> result = new HashSet<>();
        familyMap.values().forEach(subfamilyMap -> result.addAll(subfamilyMap.keySet()));
        return result;
    }

    /**
     * Creates and returns count map by subfamily of active/inactive procedure
     */
    private Map<Integer, Integer> getCapabilityCount(List<Capability> capabilities, boolean activeInactive) {
        Map<Integer, Integer> result = new HashMap<>();
        for (Capability capability : capabilities) {
            if (capability.isActive() == activeInactive) {
                Integer descriptionid = capability.getSubfamily().getId();
                if (result.containsKey(descriptionid)) {
                    result.put(descriptionid, result.get(descriptionid) + 1);
                } else {
                    result.put(descriptionid, 1);
                }
            }
        }
        return result;
    }

    /**
     * Creates and returns count for subfamilies of those containing or not containing active procedures in list
     * activeCapability = true ... count subfamilies on list that contain at least one active procedure
     * activeCapability = false ... count subfamilies on list that contain no active procedures
     */
    private Integer getSubfamilyCount(List<Capability> capabilities, Set<Integer> subfamilyIds, boolean activeCapability) {
        Set<Integer> workingSet = new HashSet<>(subfamilyIds);
        Set<Integer> subfamilyIdsFromActiveProcedures = capabilities.stream()
            .filter(Capability::isActive)
            .filter(proc -> proc.getSubfamily() != null)
            .map(proc -> proc.getSubfamily().getId())
            .collect(Collectors.toSet());
        workingSet.removeAll(subfamilyIdsFromActiveProcedures);
        // Working set now has all subfamily ids that do NOT have at least one active procedure
        return activeCapability ? subfamilyIds.size() - workingSet.size() : workingSet.size();
    }

    /**
     * Creates and populates capability map
     * key - descriptionid (subfamily id), value = List of capabilities for that subfamily
     * Expects procedures to have a subfamily (result of query)
     */
    private Map<Integer, List<Capability>> getCapabilityMap(
        List<Capability> capabilities) {
        Map<Integer, List<Capability>> result = new TreeMap<>();
        for (Capability capability : capabilities) {
            Integer descriptionid = capability.getSubfamily().getId();
            List<Capability> proceduresForSubfamily = result.computeIfAbsent(descriptionid, k -> new ArrayList<>());
            proceduresForSubfamily.add(capability);
        }
        return result;
    }
}