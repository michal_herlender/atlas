package org.trescal.cwms.core.procedure.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.procedure.dto.CapabilityAuthorizationForm;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class ViewProcedureAuthorizationController {

    private final CapabilityAuthorizationService capabilityAuthorizationService;
    private final CalibrationTypeService caltypeServ;
    private final UserService userService;
    private final CapabilityService capabilityService;

    public ViewProcedureAuthorizationController(CapabilityAuthorizationService capabilityAuthorizationService, CalibrationTypeService caltypeServ, UserService userService, CapabilityService capabilityService) {
        this.capabilityAuthorizationService = capabilityAuthorizationService;
        this.caltypeServ = caltypeServ;
        this.userService = userService;
        this.capabilityService = capabilityService;
    }

    @InitBinder("form")
    protected void initBinder(WebDataBinder binder) {
        MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
        binder.registerCustomEditor(Date.class, editor);
        binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
    }

    @RequestMapping(value = "/viewProcedureAuthorization.html", method = RequestMethod.GET)
    protected String showOverlay(Model model,
                                 @RequestParam(value = "procid", defaultValue = "0") Integer procid,
                                 @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                                 @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                                 @ModelAttribute("form") CapabilityAuthorizationForm form) throws Exception {

        Map<Contact, List<CapabilityAuthorization>> authorizedContacts = this.capabilityAuthorizationService
                .authorizedContactsWithThereAuthorizations(procid, this.userService.get(username).getCon(), subdivDto);

        Capability capability = this.capabilityService.get(procid);
        model.addAttribute("procid", procid);
        model.addAttribute("form", form);
        model.addAttribute("accedLevels", this.getAccreditationLevels());
        model.addAttribute("authorizedContacts", authorizedContacts);
        model.addAttribute("nonauthorizedContacts", this.capabilityAuthorizationService.getNonAuthorizedContacts(procid,
                authorizedContacts, this.userService.get(username).getCon(), subdivDto));
        model.addAttribute("caltypes", capability.getServiceTypes().stream()
                .map(ServiceType::getCalibrationType)
                .collect(Collectors.toSet()));

        return "/trescal/core/procedure/ViewProcedureAuthorization";
    }

    @RequestMapping(value = "/viewProcedureAuthorization.html", method = RequestMethod.POST)
    protected String submit(Model model, @ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
                            @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
                            @ModelAttribute("form") CapabilityAuthorizationForm form) throws Exception {

        this.capabilityAuthorizationService.saveOrUpdateAuthorizationsForCapability(form,
            this.userService.get(username).getCon());

        return showOverlay(model, form.getProcsId(), subdivDto, username, form);
    }

    private List<AccreditationLevel> getAccreditationLevels() {
        List<AccreditationLevel> AccreditationLevels = new ArrayList<>();
        Collections.addAll(AccreditationLevels, AccreditationLevel.values());
        return AccreditationLevels;
    }

}
