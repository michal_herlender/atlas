package org.trescal.cwms.core.procedure.dto;

import lombok.*;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilterType;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@AllArgsConstructor
@Builder
@Getter
@Setter
@NoArgsConstructor
public class EditCapabilityFilterDTO {

    private Integer id;
    @NotNull
    @Builder.Default
    private Boolean delete = false;
    private CapabilityFilterType filterType;
    @NotNull
    private Integer serviceTypeId;
    @NotNull
    @Size(max = 2000)
    private String internalComments;
    @NotNull
    @Builder.Default
    private Boolean bestLab = false;

    private Boolean active;
    private Integer workInstruktionId;
    private Integer standardTime;
    private String externalComments;
}
