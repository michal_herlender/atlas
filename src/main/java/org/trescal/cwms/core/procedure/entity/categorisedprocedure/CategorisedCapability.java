package org.trescal.cwms.core.procedure.entity.categorisedprocedure;

import lombok.Setter;
import org.hibernate.annotations.Type;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;

import javax.persistence.*;

/**
 * Intersection Entity representing a joining relationship between a
 * {@link Capability} and a {@link CapabilityCategory}. Allows multiple
 * procedures to belong to multiple categories (and therefore departments).
 *
 * @author richard
 */
@Entity
@Setter
@Table(name = "categorised_capability")
public class CategorisedCapability {
    private int id;
    private Capability capability;
    private CapabilityCategory category;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryid", nullable = false)
    public CapabilityCategory getCategory() {
        return this.category;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "capabilityId", nullable = false)
    public Capability getCapability() {
        return this.capability;
    }

}
