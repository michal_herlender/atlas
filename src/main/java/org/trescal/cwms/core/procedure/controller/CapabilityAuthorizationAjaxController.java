package org.trescal.cwms.core.procedure.controller;

import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;

@RestController
@RequestMapping("capabiltiyAuthorization")
public class CapabilityAuthorizationAjaxController {

    private final CapabilityAuthorizationService capabilityAuthorizationService;

    @Autowired
    public CapabilityAuthorizationAjaxController(CapabilityAuthorizationService capabilityAuthorizationService) {
        this.capabilityAuthorizationService = capabilityAuthorizationService;
    }

    @GetMapping("isAuthorizedFor.json")
    Either<String, AccreditationLevel> isAuthorizedFor(@RequestParam Integer capabilityId,
                                                       @RequestParam Integer calibrationTypeId,
                                                       @RequestParam Integer personId) {
        return capabilityAuthorizationService.authorizedFor(capabilityId, calibrationTypeId, personId);
    }

}

