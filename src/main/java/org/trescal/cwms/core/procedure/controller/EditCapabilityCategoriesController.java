package org.trescal.cwms.core.procedure.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.db.CategorisedCapabilityService;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.db.CapabilityCategoryService;
import org.trescal.cwms.core.procedure.form.EditCapabilityCategoriesForm;
import org.trescal.cwms.core.procedure.form.EditCapabilityCategoryInput;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@IntranetController
public class EditCapabilityCategoriesController {
    @Autowired
    private CategorisedCapabilityService cpService;
    @Autowired
    private DepartmentService deptService;
    @Autowired
    private CapabilityCategoryService capabilityCategoryService;
    @Autowired
    private SubdivService subdivService;


    /*
     * Creates input DTOs to use for editing existing procedure categories
     * The display only lists / displays those procedure categories for departments that use procedure categories
     * (others really shouldn't exist / should ideally be removed in database!)
     */
    @ModelAttribute("command")
    public EditCapabilityCategoriesForm formBackingObject(@RequestParam(name = "subdivid") int subdivId) {
        Subdiv subdiv = this.subdivService.get(subdivId);
        EditCapabilityCategoriesForm form = new EditCapabilityCategoriesForm();
        form.getInputDTOs().addAll(capabilityCategoryService.getAllSorted(subdiv).stream()
                .map(EditCapabilityCategoryInput::new).collect(Collectors.toList())
        );
        return form;
    }

    @RequestMapping(value = "editcapabilitycategories.htm", method = RequestMethod.GET)
    public String referenceData(Model model, @RequestParam(name = "subdivid") int subdivId) {
        Subdiv subdiv = this.subdivService.get(subdivId);
        List<Department> depts = deptService.getAllCapabilityEnabledSubdivDepartments(subdiv);
        model.addAttribute("subdiv", subdiv);
        model.addAttribute("deptDTOs", getDeptDTOs(depts));
        model.addAttribute("procedureCounts", getCapabiltiyCounts(subdiv));
        return "trescal/core/procedure/editcapabilitycategories";
    }

    private Map<Integer, Long> getCapabiltiyCounts(Subdiv subdiv) {
        Map<Integer, Long> result = new HashMap<>();
        for (CapabilityCategory pc : this.capabilityCategoryService.getAllSorted(subdiv)) {
            result.put(pc.getId(), this.cpService.countForCapabilityCategory(pc));
        }
        return result;
    }

    private List<KeyValue<String, Integer>> getDeptDTOs(List<Department> depts) {
        return depts.stream().map(dept -> new KeyValue<>(dept.getName(), dept.getId())).collect(Collectors.toList());
    }

    @RequestMapping(value = "editcapabilitycategories.htm", method = RequestMethod.POST)
    public String onSubmit(Model model, @RequestParam(name = "subdivid") int subdivId,
                           @ModelAttribute("command") @Validated EditCapabilityCategoriesForm form, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return referenceData(model, subdivId);
        }
        this.capabilityCategoryService.editCapabilityCategories(form);
        return "redirect:viewsub.htm?loadtab=categories-tab&subdivid=" + subdivId;
    }
}
