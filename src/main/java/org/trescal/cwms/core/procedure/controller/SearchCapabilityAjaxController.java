package org.trescal.cwms.core.procedure.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.procedure.dto.CapabilityShorDto;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;

import java.util.List;

@RestController
@RequestMapping("capability")
public class SearchCapabilityAjaxController {

    @Autowired
    private CapabilityService capabilityService;

    @GetMapping("findBySubdivAndTerm.json")
    public List<CapabilityShorDto> findBySubdivAndTerm(
            @RequestParam(required = false, defaultValue = "") String term,
            @RequestParam Integer subdivId,
            @RequestParam(required = false, defaultValue = "false") Boolean hideDeactivated
    ) {
        return capabilityService.searchProceduresHQL(term, subdivId, hideDeactivated);
    }
}
