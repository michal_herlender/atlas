package org.trescal.cwms.core.procedure.utils;

import java.util.EnumSet;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

public enum ProcedureMatchType {
	BUSINESS_SAME_SUBDIV("companyrole.business", "Business", "procedurematchtype.samesubdiv", "Same Subdivision"), 
	BUSINESS_SAME_COUNTRY("companyrole.business", "Business", "procedurematchtype.samecountry", "Same Country"), 
	BUSINESS_OTHER_COUNTRY("companyrole.business", "Business", "procedurematchtype.othercountry", "Other Country"), 
	SUPPLIER_SAME_COUNTRY("companyrole.supplier", "Supplier", "procedurematchtype.samecountry", "Same Country"), 
	SUPPLIER_OTHER_COUNTRY("companyrole.supplier", "Supplier", "procedurematchtype.othercountry", "Other Country"); 
	
	private String messageKeyCorole;
	private String defaultNameCorole;
	private String messageKeyMatchType;
	private String defaultNameMatchType;
	private MessageSource messageSource;
	
	private ProcedureMatchType(String messageKeyCorole, String defaultNameCorole, String messageKeyMatchType, String defaultNameMatchType) {
		this.messageKeyCorole = messageKeyCorole;
		this.defaultNameCorole = defaultNameCorole;
		this.messageKeyMatchType = messageKeyMatchType;
		this.defaultNameMatchType = defaultNameMatchType;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public String getDescription()
	{
		Locale locale = LocaleContextHolder.getLocale();
		if (messageSource != null) {
			StringBuffer result = new StringBuffer();
			result.append(messageSource.getMessage(this.messageKeyCorole, null, this.defaultNameCorole, locale));
			result.append(" - ");
			result.append(messageSource.getMessage(this.messageKeyMatchType, null, this.defaultNameMatchType, locale));
			return result.toString();
		}
		else {
			return this.defaultNameCorole + " - " + this.defaultNameMatchType;
		}
	}

	@Component
	public static class ProcedureMatchTypeMessageSourceInjector {
		@Autowired
		private MessageSource messages; 

		@PostConstruct
        public void postConstruct() {
            for (ProcedureMatchType ms : EnumSet.allOf(ProcedureMatchType.class))
               ms.setMessageSource(messages);
        }
	}
}
