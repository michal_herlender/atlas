package org.trescal.cwms.core.procedure.entity.procedurecategory;

import org.trescal.cwms.core.company.entity.department.Department;

import java.util.Comparator;

/**
 * {@link Comparator} implementation that sorts {@link CapabilityCategory}
 * entities in an order suiting {@link Department}s. Preffered order is to show
 * default {@link CapabilityCategory} first followed by non-default order
 * alphabetically.
 *
 * @author Richard
 */
public class ProcedureCategoryDepartmentComparator implements Comparator<CapabilityCategory> {
    @Override
    public int compare(CapabilityCategory o1, CapabilityCategory o2) {
        // first try and sort by default or not
        if (o1.isDepartmentDefault() && !o2.isDepartmentDefault()) {
            return -1;
        } else if (!o1.isDepartmentDefault() && o2.isDepartmentDefault()) {
            return 1;
        } else {
            // either both default or not, so make sure names don't match
            if (o1.getName().equals(o2.getName())) {
                // names match, compare ids
                return Integer.compare(o1.getId(), o2.getId());
            } else {
				// names don't match, compare name
				return o1.getName().compareTo(o2.getName());
			}
		}
	}
}
