package org.trescal.cwms.core.procedure.entity.workinstructionstandard;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandard;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandardType;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;

/**
 * Entity defining an {@link Instrument} that can or must be used to peform
 * calibrations for a particular {@link WorkInstruction}.
 * 
 * @author Richard
 */
@Entity
@DiscriminatorValue("workinstruction")
public class WorkInstructionStandard extends DefaultStandard
{
	private WorkInstruction workInstruction;
	private final DefaultStandardType defaultStandardType = DefaultStandardType.WORKINSTRUCTION;

	@Override
	@Transient
	public DefaultStandardType getDefaultStandardType()
	{
		return this.defaultStandardType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "wiid", nullable=true)
	public WorkInstruction getWorkInstruction()
	{
		return this.workInstruction;
	}

	public void setWorkInstruction(WorkInstruction workInstruction)
	{
		this.workInstruction = workInstruction;
	}
}