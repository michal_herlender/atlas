package org.trescal.cwms.core.procedure.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CapabilityAuthorizationForm {

    private Integer personId;
    private Integer procsId;
    private List<ProcedureAuthorizationItem> items;
}
