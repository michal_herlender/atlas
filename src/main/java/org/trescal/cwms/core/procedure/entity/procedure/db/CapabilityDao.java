package org.trescal.cwms.core.procedure.entity.procedure.db;

import org.apache.commons.collections4.MultiValuedMap;
import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.procedure.dto.CapabilityAccreditationInfoDto;
import org.trescal.cwms.core.procedure.dto.CapabilityPopUpDto;
import org.trescal.cwms.core.procedure.dto.CapabilityProjectionDTO;
import org.trescal.cwms.core.procedure.dto.CapabilityShorDto;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilterType;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.form.SearchCapabilitiesForm;
import org.trescal.cwms.core.tools.PagedResultSet;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface CapabilityDao extends AllocatedDao<Subdiv, Capability, Integer> {

    Optional<Capability> findCapability(int id);

    List<CapabilityProjectionDTO> findAllForSubdiv(Integer subdivId);

    List<Capability> getAll(Subdiv subdiv);

    List<Capability> getAll(CapabilityCategory category);

    List<Capability> getAllForDepartment(Integer deptId);

    List<Capability> getAllForDomainAndSubdiv(Integer domainId, Subdiv subdiv);

    List<Capability> getAllCapabilitiesWithNoDepartment(Subdiv subdiv);

    List<CapabilityProjectionDTO> getCapabilityProjectionDtos(Collection<Integer> capabilityIds);

    List<Capability> getCapabilitiesByReference(String reference, Boolean active, Integer ignoreProcId);

    PagedResultSet<Capability> searchCapabilities(PagedResultSet<Capability> rs, SearchCapabilitiesForm form);

    List<CapabilityShorDto> searchCapabilitiesList(String searchTerm, Integer subdivId, Boolean hideDactivated);

    List<CapabilityShorDto> searchCapabilitiesProjection(String reference, String name, boolean andTrueOrFalse,
                                                         int subdivId, Integer modelId, int maxResults, String jobtype);

    List<Capability> searchCapabilityBySubFamilyIdAndSubdivId(Integer subFamilyId, boolean searchWithoutSubfamily,
                                                              Integer businessSubdivId);

    Optional<Capability> getActiveCapabilityUsingCapabilityFilter(Integer subFamilyId, Integer serviceTypeId,
                                                                  Integer businessSubdivId, List<CapabilityFilterType> capabilityFilterTypes);

    List<CapabilityProjectionDTO> getActiveCapabilitiesUsingListOfCapabilityFilter(MultiValuedMap<Integer, Integer> subFamilyServiceType, Integer businessSubdivId, List<CapabilityFilterType> capabilityFilterTypes);

    Optional<CapabilityPopUpDto> findCapabilityForPopUp(Integer id);

    List<CapabilityAccreditationInfoDto> findAccreditationsForPopUp(Integer id);

    public List<Capability> getCapabilities(Collection<Integer> capabilityIds, Integer subdivId);

    List<Capability> getCapabilitieInSubdivsOfBc(Collection<Integer> capabilityIds,List<Integer> subdivis );


}