package org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.db;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.CapabilityTrainingRecord;

import java.util.List;
import java.util.Optional;

public interface ProcedureTrainingRecordService
{
    void deleteCapabilityTrainingRecord(CapabilityTrainingRecord proceduretrainingrecord);

    CapabilityTrainingRecord findProcedureTrainingRecord(int id);

    Optional<CapabilityTrainingRecord> findCapabilityTrainingRecordViaCertificate(int certid);

    List<CapabilityTrainingRecord> getAllProcedureTrainingRecords();

    CapabilityTrainingRecord insertProcedureTrainingRecord(Certificate cert, Capability proc, Contact manager);

	ResultWrapper insertProcedureTrainingRecord(int certid, int procid, int allocatedSubdivId);

    void insertProcedureTrainingRecord(CapabilityTrainingRecord proceduretrainingrecord);

}