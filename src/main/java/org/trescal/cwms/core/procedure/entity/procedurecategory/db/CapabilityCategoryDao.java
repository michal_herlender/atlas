package org.trescal.cwms.core.procedure.entity.procedurecategory.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.procedure.dto.CapabilityCategoryDTO;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;

import java.util.List;

public interface CapabilityCategoryDao extends BaseDao<CapabilityCategory, Integer> {
    List<CapabilityCategory> getAllSorted(Integer subdivId);

    List<CapabilityCategory> getAll(Department department);

    List<CapabilityCategoryDTO> getAll(Integer departmentId);

    CapabilityCategory getDefault(Department department);

    CapabilityCategory getBySubdivAndCapability(Subdiv sub, Capability capability);
}