package org.trescal.cwms.core.procedure.entity.procedureheading.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.procedure.entity.procedureheading.CapabilityHeading;

@Repository("ProcedureHeadingDao")
public class ProcedureHeadingDaoImpl extends BaseDaoImpl<CapabilityHeading, Integer> implements ProcedureHeadingDao {

    @Override
    protected Class<CapabilityHeading> getEntity() {
        return CapabilityHeading.class;
    }
}