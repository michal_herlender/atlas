package org.trescal.cwms.core.procedure.entity.workinstruction;

import lombok.Setter;
import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.trescal.cwms.core.audit.Allocated;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandardComparator;
import org.trescal.cwms.core.procedure.entity.workinstructionstandard.WorkInstructionStandard;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Entity
@Setter
@Table(name = "workinstruction")
public class WorkInstruction extends Allocated<Company> {

	private Integer id;
	private LocalDate approvalDate;
	private Contact approver;
	private List<Instrument> instruments;
	private String name;
	private List<WorkInstructionStandard> standards;
	private String title;
	private Description description;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public Integer getId() {
		return this.id;
	}

	@Column(name = "approvaldate", nullable = true, columnDefinition = "datetime2")
	@DateTimeFormat(iso = ISO.DATE)
	public LocalDate getApprovalDate() {
		return this.approvalDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "approverid", nullable = true)
	public Contact getApprover() {
		return this.approver;
	}

	@ManyToMany
	@JoinTable(name = "instrumentworkinstruction", joinColumns = @JoinColumn(name = "workInstructionId", foreignKey = @ForeignKey(name = "FK_instrumentworkinstruction_workinstruction")), inverseJoinColumns = @JoinColumn(name = "plantId", foreignKey = @ForeignKey(name = "FK_instrumentworkinstruction_plant")))
	public List<Instrument> getInstruments() {
		return this.instruments;
	}

	/**
	 * Intended to be a descriptive name of the work instruction
	 * 
	 */
	@NotNull
	@Length(min = 1, max = 100)
	@Column(name = "name", length = 100)
	public String getName() {
		return this.name;
	}

	@Transient
	public String getExtendedName() {
		return title + " : " + name;
	}

	@OneToMany(mappedBy = "workInstruction", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@SortComparator(DefaultStandardComparator.class)
	public List<WorkInstructionStandard> getStandards() {
		return this.standards;
	}

	/**
	 * Intended to be a short reference title of the work instruction
	 * 
	 */
	@NotNull
	@Length(min = 1, max = 100)
	@Column(name = "title", length = 100)
	public String getTitle() {
		return this.title;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "descid", foreignKey = @ForeignKey(name = "FK_workinstruction_description"), nullable = true)
	public Description getDescription() {
		return description;
	}

}