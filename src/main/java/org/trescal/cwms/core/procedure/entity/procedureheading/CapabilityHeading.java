package org.trescal.cwms.core.procedure.entity.procedureheading;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * A heading that multiple entities can be associated under (independent of any
 * {@link CapabilityCategory}.
 *
 * @author richard
 */
@Entity
@Table(name = "procedureheading")
public class CapabilityHeading extends Auditable {
    private String heading;
    private int id;
    private Set<Capability> capabilities;

    @NotNull
    @Length(max = 100)
    @Column(name = "heading", length = 100, nullable = false)
    public String getHeading() {
        return this.heading;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "heading")
    public Set<Capability> getProcedures() {
        return this.capabilities;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setProcedures(Set<Capability> capabilities) {
        this.capabilities = capabilities;
    }

}
