package org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.Certificate;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.CapabilityTrainingRecord;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.userright.enums.Permission;

import java.util.List;
import java.util.Optional;

@Service("ProcedureTrainingRecordService")
public class ProcedureTrainingRecordServiceImpl implements ProcedureTrainingRecordService {
    @Autowired
    private CertificateService certServ;
    @Autowired
    private ProcedureTrainingRecordDao procedureTrainingRecordDao;
    @Autowired
    private CapabilityService procServ;
    @Autowired
    private SessionUtilsService sessionServ;
    @Autowired
    private AuthenticationService authServ;

    public void deleteCapabilityTrainingRecord(CapabilityTrainingRecord proceduretrainingrecord) {
        this.procedureTrainingRecordDao.remove(proceduretrainingrecord);
    }

    public CapabilityTrainingRecord findProcedureTrainingRecord(int id) {
        return this.procedureTrainingRecordDao.find(id);
    }

    public Optional<CapabilityTrainingRecord> findCapabilityTrainingRecordViaCertificate(int certid) {
        return this.procedureTrainingRecordDao.findCapabilityTrainingRecordViaCertificate(certid);
    }

    public List<CapabilityTrainingRecord> getAllProcedureTrainingRecords() {
        return this.procedureTrainingRecordDao.findAll();
    }

    public CapabilityTrainingRecord insertProcedureTrainingRecord(Certificate cert, Capability proc, Contact manager) {
        // create new procedure training record
        CapabilityTrainingRecord ptr = new CapabilityTrainingRecord();
        // set certificate
        ptr.setCert(cert);
        // set procedure
        ptr.setCapability(proc);
        // set lab manager
        ptr.setLabManager(manager);
        // set trainee from calibration
        ptr.setTrainee(cert.getCal().getCompletedBy());
        // insert procedure training record
        this.insertProcedureTrainingRecord(ptr);
        // return record
        return ptr;
    }

    public ResultWrapper insertProcedureTrainingRecord(int certid, int procid, int allocatedSubdivId) {
        // find certificate
		Certificate cert = this.certServ.findCertificate(certid);
		// certificate null?
		if (cert != null)
		{
			// find procedure
            Capability proc = this.procServ.get(procid);
			// procedure null
			if (proc != null)
			{

				// find contact
				Contact con = this.sessionServ.getCurrentContact();
				// contact null?
				if ((con != null) && (this.authServ.hasRight(con, Permission.PROCEDURE_TRAINING_CERTIFICATION, allocatedSubdivId)))
				{
					// insert new procedure training record
                    CapabilityTrainingRecord ptr = this.insertProcedureTrainingRecord(cert, proc, con);
					// return successful wrapper
					return new ResultWrapper(true, "", ptr, null);
				}
				else
				{
					// return un-successful wrapper
					return new ResultWrapper(false, "Logged in user could not be found or does not have Manager Role", null, null);
				}
			}
			else
			{
				// return un-successful wrapper
                return new ResultWrapper(false, "Procedure could not be found", null, null);
            }
        } else {
            // return un-successful wrapper
            return new ResultWrapper(false, "Certificate could not be found", null, null);
        }
    }

    public void insertProcedureTrainingRecord(CapabilityTrainingRecord CapabilityTrainingRecord) {
        this.procedureTrainingRecordDao.persist(CapabilityTrainingRecord);
    }

    public void setCertServ(CertificateService certServ) {
        this.certServ = certServ;
    }


}