package org.trescal.cwms.core.procedure.form;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.procedure.dto.EditCapabilityFilterDTO;

import javax.validation.Valid;
import java.util.List;

@Getter
@Setter
public class EditCapabilityFiltersForm {

    @Valid
    private List<EditCapabilityFilterDTO> filters;

}
