package org.trescal.cwms.core.procedure.entity.capabilityfilter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.EnumSet;
import java.util.Locale;

public enum CapabilityFilterType {
    UNDEFINED(false, "capabilityfiltertype.undefined", "Undefined"),
    YES(true, "capabilityfiltertype.yes", "Yes"),
    NO(false, "capabilityfiltertype.no", "No"),
    MAYBE(true, "capabilityfiltertype.maybe", "Ask Me"),
    LOCAL_ONLY(true, "capabilityfiltertype.localOnly", "Local Only");

    public final Boolean selectable;
    public final String messageCode;
    private final String defaultMessage;
    private MessageSource messageSource;

    CapabilityFilterType(Boolean selectable, String messageCode, String defaultMessage) {
        this.selectable = selectable;
        this.messageCode = messageCode;
        this.defaultMessage = defaultMessage;
    }

    @Component
    public static class CapabilityFilterTypeMessageSourceInjector {
        @Autowired
        private MessageSource messages;

        @PostConstruct
        public void postConstruct() {
            for (CapabilityFilterType cft : EnumSet.allOf(CapabilityFilterType.class))
                cft.setMessageSource(messages);
        }
    }
	
	private void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	public String getDescription() {
		Locale locale = LocaleContextHolder.getLocale();
		return this.messageSource.getMessage(messageCode, null, defaultMessage, locale);
	}

	public Boolean getSelectable() {
		return selectable;
	}
	
	
}
