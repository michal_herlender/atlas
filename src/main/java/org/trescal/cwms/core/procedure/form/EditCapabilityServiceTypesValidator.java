package org.trescal.cwms.core.procedure.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.procedure.dto.EditCapabilityFilterDTO;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class EditCapabilityServiceTypesValidator extends AbstractBeanValidator {
	
	public static String ERROR_CODE_FILTER_TYPE = "error.capabilityfilter.filtertype";
	public static String ERROR_MESSAGE_FILTER_TYPE = "At least one filter type must be selected for each filter";
	
	@Override
	public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(EditCapabilityFiltersForm.class);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
        super.validate(target, errors);
        EditCapabilityFiltersForm form = (EditCapabilityFiltersForm) target;
        for (EditCapabilityFilterDTO dto : form.getFilters()) {
            if (!dto.getDelete() && !dto.getFilterType().getSelectable()) {
                errors.reject(ERROR_CODE_FILTER_TYPE, ERROR_MESSAGE_FILTER_TYPE);
            }
        }
    }

}
