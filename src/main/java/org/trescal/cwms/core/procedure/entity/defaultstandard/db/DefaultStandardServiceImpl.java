package org.trescal.cwms.core.procedure.entity.defaultstandard.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.instrument.entity.instrument.db.InstrumService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandard;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandardType;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedurestandard.CapabilityStandard;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.procedure.entity.workinstruction.db.WorkInstructionService;
import org.trescal.cwms.core.procedure.entity.workinstructionstandard.WorkInstructionStandard;
import org.trescal.cwms.core.system.Constants;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("DefaultStandardService")
public class DefaultStandardServiceImpl implements DefaultStandardService {
    @Autowired
    private DefaultStandardDao defaultStandardDao;
    @Autowired
    private InstrumService instrumServ;
    @Autowired
    private CapabilityService procServ;
    @Autowired
    private UserService userService;
    @Autowired
    private WorkInstructionService wiServ;

    @Override
    public void delete(DefaultStandard ds) {
        this.defaultStandardDao.remove(ds);
    }

    @Override
	public ResultWrapper deleteDefaultStandard(int id)
	{
		DefaultStandard ds = this.findDefaultStandard(id);
		if (ds == null)
		{
			return new ResultWrapper(false, "Unable to find default standard to delete", null, null);
		}
		else
		{
			this.delete(ds);
			return new ResultWrapper(true, "", null, null);
		}
	}

	@Override
	public boolean enforcedAreCheckedForProc(int procid, Integer[] instIds)
	{
        List<CapabilityStandard> enforcedStds = this.getStandardsForProcedure(procid, true);
		if (enforcedStds.size() > 0) {
			if (instIds != null) {
                ArrayList<Integer> selectedStds = new ArrayList<>(Arrays.asList(instIds));
                for (CapabilityStandard std : enforcedStds) {
                    if (!selectedStds.contains(std.getInstrument().getPlantid())) {
                        return false;
                    }
                }

                return true;
            } else {
				return false;
			}
		}
		else
		{
			return true;
		}
	}

	@Override
	public boolean enforcedAreCheckedForProcAndWI(int procid, int wiid, List<Integer> selectedStdIds)
	{
		List<DefaultStandard> enforcedStds = this.getAllEnforcedStandardsForProcAndWI(procid, wiid);

		if (enforcedStds.size() > 0)
		{
			if (selectedStdIds != null)
			{
				for (DefaultStandard std : enforcedStds)
				{
					if (!selectedStdIds.contains(std.getInstrument().getPlantid()))
					{
						return false;
					}
				}

				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	}

	@Override
	public DefaultStandard findDefaultStandard(int id)
	{
		return this.defaultStandardDao.find(id);
	}

	@Override
	public List<DefaultStandard> getAllDefaultStandards()
	{
		return this.defaultStandardDao.findAll();
	}

	@Override
	public List<DefaultStandard> getAllDefaultStandardsForInstument(int plantid, DefaultStandardType type)
	{
		return this.defaultStandardDao.getAllDefaultStandardsForInstument(plantid, type);
	}

	@Override
	public List<DefaultStandard> getAllEnforcedStandardsForProcAndWI(int procid, int wiid) {
        List<CapabilityStandard> enforcedProcStds = this.getStandardsForProcedure(procid, true);
        List<WorkInstructionStandard> enforcedWiStds = this.getStandardsForWorkInstruction(wiid, true);

        List<DefaultStandard> allEnforcedStds = new ArrayList<>();
        allEnforcedStds.addAll(enforcedProcStds);
        allEnforcedStds.addAll(enforcedWiStds);

        return allEnforcedStds;
    }

    @Override
    public CapabilityStandard getDefaultStandardByProcPlant(int procid, int plantid) {
        return this.defaultStandardDao.getDefaultStandardByProcPlant(procid, plantid);
    }

    @Override
    public WorkInstructionStandard getDefaultStandardByWIPlant(int wiid, int plantid) {
        return this.defaultStandardDao.getDefaultStandardByWIPlant(wiid, plantid);
    }

    @Override
    public List<CapabilityStandard> getStandardsForProcedure(int procid, Boolean enforced) {

        // NO LONGER NECESSARY BECAUSE THIS IS NO LONGER CALLED BY AJAX

        return this.defaultStandardDao.getStandardsForProcedure(procid, enforced);
    }

    @Override
    public List<WorkInstructionStandard> getStandardsForWorkInstruction(int wiid, Boolean enforced) {
        List<WorkInstructionStandard> list = this.defaultStandardDao.getStandardsForWorkInstruction(wiid, enforced);

		for (WorkInstructionStandard wis : list)
		{
			// load eager instrument for when this is called from DWR
			wis.setInstrument(this.instrumServ.findEagerInstrum(wis.getInstrument().getPlantid()));
		}

		return list;
	}

	@Override
	public void insertDefaultStandard(DefaultStandard DefaultStandard)
	{
		this.defaultStandardDao.persist(DefaultStandard);
	}

	@Override
	public ResultWrapper insertDefaultStandard(Integer linkid, DefaultStandardType type, Integer plantid, boolean enforceUse, HttpSession session) {
        Instrument instrument = this.instrumServ.findEagerInstrum(plantid);
        Capability proc = this.procServ.get(linkid);
        WorkInstruction wi = this.wiServ.get(linkid);

        if (instrument == null) {
            return new ResultWrapper(false, "The standard requested could not be found.", null, null);
        } else if (type == null) {
            return new ResultWrapper(false, "A default standard type must be specified.", null, null);
        } else if ((proc == null) && (type == DefaultStandardType.PROCEDURE))
		{
			return new ResultWrapper(false, "The procedure requested could not be found.", null, null);
		}
		else if ((wi == null) && (type == DefaultStandardType.WORKINSTRUCTION))
		{
			return new ResultWrapper(false, "The work instruction requested could not be found.", null, null);
		}
		else if (!instrument.getCalibrationStandard())
		{
			return new ResultWrapper(false, "The instrument specified is not recorded as being a calibration standard", null, null);
		}
		else
		{
			DefaultStandard ds = null;
			String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
			Contact currentContact = this.userService.getEagerLoad(username).getCon(); 

			if (type == DefaultStandardType.PROCEDURE)
			{
				if (this.getDefaultStandardByProcPlant(linkid, plantid) != null) {
					return new ResultWrapper(false, "The instrument specified is already listed as a standard for this procedure.", null, null);
				} else {
                    CapabilityStandard ps = new CapabilityStandard();
                    ps.setCapability(proc);
                    ps.setEnforceUse(enforceUse);
                    ps.setInstrument(instrument);
                    ps.setSetOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
                    ps.setSetBy(currentContact);
                    ds = ps;
                }
			}
			else if (type == DefaultStandardType.WORKINSTRUCTION)
			{
				if (this.getDefaultStandardByWIPlant(linkid, plantid) != null) {
					return new ResultWrapper(false, "The instrument specified is already listed as a standard for this procedure.", null, null);
				} else {
					WorkInstructionStandard ws = new WorkInstructionStandard();
					ws.setWorkInstruction(wi);
					ws.setEnforceUse(enforceUse);
					ws.setInstrument(instrument);
					ws.setSetOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
					ws.setSetBy(currentContact);
					ds = ws;
				}
			}

			this.insertDefaultStandard(ds);
			return new ResultWrapper(true, "", ds, null);

		}
	}

	@Override
	public void saveOrUpdateDefaultStandard(DefaultStandard defaultstandard) {
		this.defaultStandardDao.persist(defaultstandard);
	}

	@Override
	public void updateDefaultStandard(DefaultStandard DefaultStandard) {
		this.defaultStandardDao.merge(DefaultStandard);
	}

	@Override
	public ResultWrapper updateDefaultStandardEnforceUse(int id)
	{
		DefaultStandard ds = this.findDefaultStandard(id);
		if (ds == null) {
			return new ResultWrapper(false, "Unable to find standard to update", null, null);
		} else {
			ds.setEnforceUse(!ds.isEnforceUse());
			this.updateDefaultStandard(ds);
			return new ResultWrapper(true, "", ds, null);
		}
	}
}