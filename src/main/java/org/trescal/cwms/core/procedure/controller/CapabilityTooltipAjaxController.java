package org.trescal.cwms.core.procedure.controller;

import io.vavr.Tuple;
import io.vavr.Tuple2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.trescal.cwms.core.procedure.dto.CapabilityPopUpDto;
import org.trescal.cwms.core.procedure.entity.procedure.CapabilityInformation;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;

import java.util.Optional;

@RestController
@RequestMapping("tooltip")
public class CapabilityTooltipAjaxController {

    private final CapabilityService capabilityService;

    public CapabilityTooltipAjaxController(CapabilityService capabilityService) {
        this.capabilityService = capabilityService;
    }

    @GetMapping("capability.json")
    Optional<Tuple2<CapabilityPopUpDto, CapabilityInformation>> getCapability(@RequestParam Integer id) {
        return capabilityService.findCapabilityForPopUp(id).map(cd -> Tuple.of(cd, capabilityService.findCapabilityInfo()));
    }
}
