package org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.procedure.entity.proceduretrainingrecord.CapabilityTrainingRecord;

import java.util.Optional;

public interface ProcedureTrainingRecordDao extends BaseDao<CapabilityTrainingRecord, Integer> {

    Optional<CapabilityTrainingRecord> findCapabilityTrainingRecordViaCertificate(int certid);
}