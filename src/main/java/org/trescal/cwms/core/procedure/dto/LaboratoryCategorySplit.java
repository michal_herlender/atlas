package org.trescal.cwms.core.procedure.dto;

public enum LaboratoryCategorySplit {
    ACCREDITED, TRACEABLE, NONE;
}
