package org.trescal.cwms.core.procedure.entity.workinstruction.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.procedure.form.SearchWorkInstructionForm;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import java.util.List;
import java.util.Locale;
import java.util.Set;

public interface WorkInstructionDao extends BaseDao<WorkInstruction, Integer> {

    void searchWorkInstructions(PagedResultSet<WorkInstruction> pagedResults, SearchWorkInstructionForm form);

    List<KeyValueIntegerString> getAllKeyValues(String workInstructionFragment, Integer descriptionId,
                                                Locale searchLanguage, int maxResults);

    Set<KeyValue<Integer, String>> getAllAsKeyValues();
}