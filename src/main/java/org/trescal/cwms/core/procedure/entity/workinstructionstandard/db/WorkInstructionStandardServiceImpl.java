package org.trescal.cwms.core.procedure.entity.workinstructionstandard.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.procedure.entity.workinstructionstandard.WorkInstructionStandard;

@Service("WorkInstructionStandardService")
public class WorkInstructionStandardServiceImpl extends BaseServiceImpl<WorkInstructionStandard, Integer> implements WorkInstructionStandardService
{
	@Autowired
	private WorkInstructionStandardDao workInstructionStandardDao;
	
	@Override
	protected BaseDao<WorkInstructionStandard, Integer> getBaseDao() {
		return workInstructionStandardDao;
	}
}