package org.trescal.cwms.core.procedure.entity.procedureaccreditation;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.Locale;

@JsonFormat(shape = Shape.OBJECT)
public enum AccreditationLevel implements Serializable {
    SUPERVISED(1, "viewprocedure.accreditationlevel.supervised", "Supervised calibration"),
    PERFORM(2, "viewprocedure.accreditationlevel.perform", "Calibration performed"),
    SIGN(3, "viewprocedure.accreditationlevel.sign", "Calibration signatory"),
    APPROVE(4, "viewprocedure.accreditationlevel.approve", "Calibration approved"),
    NONE(0, "viewprocedure.accreditationlevel.none", "No accreditation"),
    REMOVED(-1, "viewprocedure.accreditationlevel.removed", "Accreditation removed");

    private final int level;
    private final String messageCode;
    private final String defaultMessage;
    private MessageSource messageSource;

    AccreditationLevel(int level, String messageCode, String defaultMessage) {
        this.level = level;
        this.messageCode = messageCode;
        this.defaultMessage = defaultMessage;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public String getDefaultName() {
        return this.name();
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getDefaultMessage() {
        return defaultMessage;
    }

    @Component
    public static class AccreditationLevelMessageInjector {
        @Autowired
        private MessageSource messageSource;

        @PostConstruct
        public void postConstruct() {
            for (AccreditationLevel accreditationLevel : AccreditationLevel.values()) {
                accreditationLevel.setMessageSource(this.messageSource);
            }
        }
    }

    public int getLevel() {
        return this.level;
    }

    public String getMessage() {
        Locale locale = LocaleContextHolder.getLocale();
        return getMessageForLocale(locale);
    }

    public String getMessageForLocale(Locale locale) {
        return this.messageSource.getMessage(messageCode, null, this.defaultMessage, locale);
    }
}
