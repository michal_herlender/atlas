package org.trescal.cwms.core.procedure.dto;

import org.trescal.cwms.spring.model.KeyValue;

public class WorkInstructionKeyValue extends KeyValue<Integer, String> {

	public WorkInstructionKeyValue(Integer id, String name) {
		super(id, name);
	}
}