package org.trescal.cwms.core.procedure.form;

import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.tools.PagedResultSet;

public class SearchCapabilitiesForm {
    private boolean active;
    private String reference;
    private String name;
    private Integer departmentId;
    private String description;
    private Integer subfamilyId;
    private String subfamilyName;
    private Integer subdivid;
    private Integer coid;

    // results data
    private PagedResultSet<Capability> rs;
    private int pageNo;
    private int resultsPerPage;

    public Integer getDepartmentId() {
        return this.departmentId;
    }

    public String getDescription() {
        return this.description;
	}

	public String getName()
	{
		return this.name;
	}

	public int getPageNo()
	{
		return this.pageNo;
	}

    public String getReference() {
        return this.reference;
    }

    public int getResultsPerPage() {
        return this.resultsPerPage;
    }

    public PagedResultSet<Capability> getRs() {
        return this.rs;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

	public void setDepartmentId(Integer departmentId)
	{
		this.departmentId = departmentId;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setPageNo(int pageNo)
	{
		this.pageNo = pageNo;
	}

    public void setReference(String reference) {
        this.reference = reference;
    }

    public void setResultsPerPage(int resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }

    public void setRs(PagedResultSet<Capability> rs) {
        this.rs = rs;
    }

    public Integer getSubfamilyId() {
        return subfamilyId;
    }

    public void setSubfamilyId(Integer subfamilyId) {
        this.subfamilyId = subfamilyId;
    }

    public String getSubfamilyName() {
		return subfamilyName;
	}

	public void setSubfamilyName(String subfamilyName) {
		this.subfamilyName = subfamilyName;
	}

	public Integer getSubdivid() {
		return subdivid;
	}

	public void setSubdivid(Integer subdivid) {
		this.subdivid = subdivid;
	}

	public Integer getCoid() {
		return coid;
	}

	public void setCoid(Integer coid) {
		this.coid = coid;
	}
}
