package org.trescal.cwms.core.procedure.entity.defaultstandard.db;

import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandard;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandardType;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedurestandard.CapabilityStandard;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.procedure.entity.workinstructionstandard.WorkInstructionStandard;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface DefaultStandardService {
    void delete(DefaultStandard ds);

    /**
     * Tests if the {@link DefaultStandard} identified by the given id and type
     * exists and if so deletes it.
     *
     * @param id the id of the {@link DefaultStandard}.
     * @return {@link ResultWrapper} indicating the success of the operation.
     */
    ResultWrapper deleteDefaultStandard(int id);

    /**
     * Tests if all of the enforced {@link CapabilityStandard} {@link Instrument}
     * s for the {@link Capability} with the given ID have IDs in the given list.
     *
     * @param procid  the {@link Capability} ID.
     * @param instIds array of {@link Instrument} IDs.
     * @return true if all of the enforced {@link Instrument}s have IDs in the
     * list, otherwise false.
     */
    boolean enforcedAreCheckedForProc(int procid, Integer[] instIds);

    boolean enforcedAreCheckedForProcAndWI(int procid, int wiid, List<Integer> selectedStdIds);

    DefaultStandard findDefaultStandard(int id);

    List<DefaultStandard> getAllDefaultStandards();

    /**
     * Returns all {@link DefaultStandard} where this {@link Instrument} is the
     * standard. If type is null then both {@link CapabilityStandard} and
     * {@link WorkInstructionStandard} entities are returned.
     *
     * @param plantid the id of the {@link Instrument}.
     * @param type    the {@link DefaultStandardType} of {@link DefaultStandard}.
     * @return list of matching {@link DefaultStandard}.
     */
    List<DefaultStandard> getAllDefaultStandardsForInstument(int plantid, DefaultStandardType type);

    List<DefaultStandard> getAllEnforcedStandardsForProcAndWI(int procid, int wiid);

    /**
     * Returns the {@link CapabilityStandard} identified by the given
     * {@link Capability} and {@link Instrument} ids.
     *
     * @param procid  the {@link Capability} id.
     * @param plantid the {@link Instrument} id.
     * @return {@link CapabilityStandard} or null if none exist matching the
     * criteria.
     */
    CapabilityStandard getDefaultStandardByProcPlant(int procid, int plantid);

    /**
     * Returns the {@link WorkInstructionStandard} identified by the given
     * {@link WorkInstructionStandard} and {@link Instrument} ids.
     *
     * @param wiid    the {@link WorkInstruction} id.
     * @param plantid the {@link Instrument} id.
     * @return {@link WorkInstructionStandard} or null if none exist matching
     * the criteria.
     */
    WorkInstructionStandard getDefaultStandardByWIPlant(int wiid, int plantid);

    /**
     * Returns a {@link List} of {@link CapabilityStandard} entities for the
     * {@link Capability} with the given ID, filtering those that are enforced if
     * necessary.
     *
     * @param procid   the {@link Capability} ID.
     * @param enforced true - returns only enforced, false - returns only
     *                 optional, null - returns both enforced and optional.
     * @return the {@link List} of {@link CapabilityStandard} entities.
     */
    List<CapabilityStandard> getStandardsForProcedure(int procid, Boolean enforced);

    List<WorkInstructionStandard> getStandardsForWorkInstruction(int wiid, Boolean enforced);

    void insertDefaultStandard(DefaultStandard defaultstandard);

    /**
     * Inserts the a new {@link CapabilityStandard} entity for the given
     * {@link Capability} and {@link Instrument} combination. Validates that the
     * given {@link Capability} and {@link Instrument} exist, that the
     * {@link Instrument} is a valid calibraiton standard and that the
     * {@link Instrument} is not already part of a {@link CapabilityStandard} for
     * this {@link Capability}.
     *
     * @param linkid     the id of the {@link Capability} or {@link WorkInstruction}.
     * @param type       the {@link DefaultStandard} type indicating if we want a
     *                   {@link CapabilityStandard} or {@link WorkInstructionStandard}.
     * @param plantid    the id of the {@link Instrument}.
     * @param enforceUse true if the {@link CapabilityStandard} should be forced
     *                   to be used by the {@link Capability}.
     * @return {@link ResultWrapper} indicating the success of the operation.
     */
    ResultWrapper insertDefaultStandard(Integer linkid, DefaultStandardType type, Integer plantid, boolean enforceUse, HttpSession session);

    void saveOrUpdateDefaultStandard(DefaultStandard defaultstandard);

    void updateDefaultStandard(DefaultStandard defaultstandard);

    /**
     * Tests if the {@link DefaultStandard} identified by the given id exists
     * and if so swops the enforeUse value of the entity (to the opposite of
	 * it's current value).
	 * 
	 * @param id the id of the {@link DefaultStandard}
	 * @return {@link ResultWrapper} indicating the success of the operation.
	 */
	ResultWrapper updateDefaultStandardEnforceUse(int id);
}