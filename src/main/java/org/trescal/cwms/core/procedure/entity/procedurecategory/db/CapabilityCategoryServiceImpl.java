package org.trescal.cwms.core.procedure.entity.procedurecategory.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.db.DepartmentService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.procedure.dto.CapabilityCategoryDTO;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.db.CategorisedCapabilityService;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.form.EditCapabilityCategoriesForm;
import org.trescal.cwms.core.procedure.form.EditCapabilityCategoryInput;

import java.util.List;

@Service
public class CapabilityCategoryServiceImpl extends BaseServiceImpl<CapabilityCategory, Integer>
    implements CapabilityCategoryService {
    private final DepartmentService departmentService;
    private final CapabilityCategoryDao capabilityCategoryDao;
    private final CategorisedCapabilityService categorisedCapabilityService;

    @Autowired
    public CapabilityCategoryServiceImpl(DepartmentService departmentService, CapabilityCategoryDao capabilityCategoryDao, CategorisedCapabilityService categorisedCapabilityService) {
        this.departmentService = departmentService;
        this.capabilityCategoryDao = capabilityCategoryDao;
        this.categorisedCapabilityService = categorisedCapabilityService;
    }

    @Override
    protected BaseDao<CapabilityCategory, Integer> getBaseDao() {
        return capabilityCategoryDao;
    }

    @Override
    public List<CapabilityCategory> getAllSorted(Subdiv subdiv) {
        return capabilityCategoryDao.getAllSorted(subdiv.getSubdivid());
    }

    @Override
    public List<CapabilityCategory> getAllSorterdBySubdivId(Integer subdivId) {
        return capabilityCategoryDao.getAllSorted(subdivId);
    }

    @Override
    public List<CapabilityCategory> getAll(Department department) {
        return capabilityCategoryDao.getAll(department);
    }

    @Override
    public CapabilityCategory getDefault(Department department) {
        return capabilityCategoryDao.getDefault(department);
    }

    @Override
    public void editCapabilityCategories(EditCapabilityCategoriesForm form) {
        for (EditCapabilityCategoryInput input : form.getInputDTOs()) {
            // Elements added and deleted from list before submission may be null
            if (input != null) {
                if (input.getAdd()) {
                    CapabilityCategory pc = new CapabilityCategory();
                    pc.setDepartment(departmentService.get(input.getDeptId()));
                    pc.setDepartmentDefault(false);
                    pc.setDescription(input.getDescription());
                    pc.setName(input.getName());
                    pc.setSplitTraceable(input.getSplitTraceable());
                    capabilityCategoryDao.persist(pc);
                } else {
                    CapabilityCategory pc = capabilityCategoryDao.find(input.getId());
                    if (input.getDelete()) {
                        // Validation / UI should also prevent deletion of default procedure category
                        if (pc.isDepartmentDefault())
                            throw new UnsupportedOperationException("Cannot delete default procedure category");
                        CapabilityCategory departmentDefault = this.getDefault(pc.getDepartment());
                        if (departmentDefault == null)
                            throw new UnsupportedOperationException("No department default procedure found");
                        // reassign any procedures to default category
                        this.categorisedCapabilityService.updateCapabilityCategories(pc, departmentDefault);
                        capabilityCategoryDao.remove(pc);
                    }
					else {
						// Can modify department for non-default procedure category
						if (!pc.isDepartmentDefault()) {
                            pc.setDepartment(departmentService.get(input.getDeptId()));
                        }
                        pc.setDescription(input.getDescription());
                        pc.setName(input.getName());
                        pc.setSplitTraceable(input.getSplitTraceable());
                    }
                }
            }
        }
    }

    @Override
    public CapabilityCategory getBySubdivAndCapability(Subdiv sub, Capability capability) {
        return this.capabilityCategoryDao.getBySubdivAndCapability(sub, capability);
    }

    @Override
    public List<CapabilityCategoryDTO> getAll(Integer departmentId) {
        return this.capabilityCategoryDao.getAll(departmentId);
    }
}