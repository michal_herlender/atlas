package org.trescal.cwms.core.procedure.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.procedure.entity.workinstruction.db.WorkInstructionService;
import org.trescal.cwms.core.procedure.form.SearchWorkInstructionForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;

@RequestMapping(value="/searchwi.htm")
@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY})
public class SearchWorkInstructionController
{
	@Autowired
	private WorkInstructionService wiService;

	@ModelAttribute("command")
	protected Object formBackingObject(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) CompanyKeyValue companyDto)
	{
		SearchWorkInstructionForm form = new SearchWorkInstructionForm();
		form.setCoid(companyDto.getKey());
		form.setPageNo(1);
		form.setResultsPerPage(Constants.RESULTS_PER_PAGE);
		return form;
	}

	@RequestMapping(method=RequestMethod.GET)
	public String referenceData() {
		return "/trescal/core/procedure/searchworkinstructions";
	}

	@RequestMapping(method=RequestMethod.POST)
	protected String onSubmit(Model model, @ModelAttribute("command") SearchWorkInstructionForm form)
	{
		PagedResultSet<WorkInstruction> pagedResults = new PagedResultSet<WorkInstruction>(form.getResultsPerPage(), form.getPageNo());
		this.wiService.searchWorkInstructions(pagedResults, form);
		model.addAttribute("pagedResults", pagedResults);
		return "/trescal/core/procedure/searchworkinstructionresults";
	}
}