package org.trescal.cwms.core.procedure.form;

import javax.validation.constraints.Min;
import java.util.Map;

public class RecategoriseCapabilitiesForm {

    private Map<Integer, Boolean> procedureIds;
    @Min(value = 1, message = ERROR_CODE_VALUE_NOT_SELECTED)
    private Integer procedureCategoryId;

    public static final String ERROR_CODE_VALUE_NOT_SELECTED = "{error.value.notselected}";

    public Map<Integer, Boolean> getProcedureIds() {
        return procedureIds;
    }

    public Integer getProcedureCategoryId() {
        return procedureCategoryId;
    }

    public void setProcedureIds(Map<Integer, Boolean> procedureIds) {
        this.procedureIds = procedureIds;
    }

    public void setProcedureCategoryId(Integer procedureCategoryId) {
        this.procedureCategoryId = procedureCategoryId;
    }
}
