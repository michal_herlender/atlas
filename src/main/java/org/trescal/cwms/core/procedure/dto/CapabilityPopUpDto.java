package org.trescal.cwms.core.procedure.dto;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;

import java.util.Collections;
import java.util.List;

@Value
@RequiredArgsConstructor
@With
public class CapabilityPopUpDto {


    String name, description, reference, heading, department, category, calibrationProcess;
    Boolean active;
    List<CapabilityAccreditationInfoDto> accreditations;

    public CapabilityPopUpDto(String name, String description, String reference, String heading, CapabilityCategory category, String calibrationProcess, Boolean active) {
        this.name = name;
        this.description = description;
        this.reference = reference;
        this.heading = heading;
        this.category = (category != null) ? category.getName() : "";
        this.department = (category != null) ? category.getDepartment().getName() : "";
        this.calibrationProcess = calibrationProcess;
        this.active = active;
        this.accreditations = Collections.emptyList();
    }

}
