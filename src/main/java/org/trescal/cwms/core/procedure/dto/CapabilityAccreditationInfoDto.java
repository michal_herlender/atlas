package org.trescal.cwms.core.procedure.dto;

import lombok.Value;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;

@Value
public class CapabilityAccreditationInfoDto {
    String accreditationFor;
    Integer calibrationTypeId;
    AccreditationLevel level;
}
