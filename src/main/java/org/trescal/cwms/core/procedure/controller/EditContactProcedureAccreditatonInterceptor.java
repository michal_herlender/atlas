package org.trescal.cwms.core.procedure.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.db.CapabilityAuthorizationService;
import org.trescal.cwms.core.system.Constants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
@IntranetController
public class EditContactProcedureAccreditatonInterceptor implements HandlerInterceptor {
    @Autowired
    private CapabilityAuthorizationService procAccredServ;

    protected final Log logger = LogFactory.getLog(this.getClass());

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        ResultWrapper results = this.procAccredServ
            .userCanUpdateContactCapabilityAuthorization(request);
        if (!results.isSuccess()) {
            HttpSession session = request.getSession(true);
            session.setAttribute(Constants.SYSTEM_AUTHENTICATION_ERROR_MESSAGE,
                results.getMessage());
            response.sendRedirect("accessdenied.htm");
            return false;
        } else {
            return true;
        }
    }
	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3) throws Exception {
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3) throws Exception {
	}
}