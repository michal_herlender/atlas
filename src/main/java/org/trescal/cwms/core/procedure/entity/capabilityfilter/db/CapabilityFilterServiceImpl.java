package org.trescal.cwms.core.procedure.entity.capabilityfilter.db;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.procedure.dto.EditCapabilityFilterDTO;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.workinstruction.db.WorkInstructionService;
import org.trescal.cwms.core.procedure.form.EditCapabilityFiltersForm;
import org.trescal.cwms.core.system.entity.servicetype.db.ServiceTypeService;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CapabilityFilterServiceImpl extends BaseServiceImpl<CapabilityFilter, Integer> implements CapabilityFilterService {

    private final CapabilityFilterDao capabilityFilterDao;
    private final CapabilityService capabilityService;
    private final ServiceTypeService serviceTypeService;
    private final WorkInstructionService workInstructionService;

    @Autowired
    CapabilityFilterServiceImpl(CapabilityFilterDao capabilityFilterDao, CapabilityService capabilityService, ServiceTypeService serviceTypeService, WorkInstructionService workInstructionService) {
        this.capabilityFilterDao = capabilityFilterDao;
        this.capabilityService = capabilityService;
        this.serviceTypeService = serviceTypeService;
        this.workInstructionService = workInstructionService;
    }

    @Override
    protected BaseDao<CapabilityFilter, Integer> getBaseDao() {
        return capabilityFilterDao;
    }

    enum CapabilityFilterState {
        ADD, UPDATE, DELETE
    }

    @Override
    public void editCapabilityFilters(int procid, EditCapabilityFiltersForm form) {
        Capability capability = capabilityService.get(procid);
        val filetrMap = form.getFilters().stream().collect(Collectors.groupingBy(f -> f.getId() == null ? CapabilityFilterState.ADD : f.getDelete() ? CapabilityFilterState.DELETE : CapabilityFilterState.UPDATE));
        List<CapabilityFilter> filtersToAdd = filetrMap.getOrDefault(CapabilityFilterState.ADD, Collections.emptyList()).stream()
            .map(d -> {
                val f = new CapabilityFilter();
                f.setCapability(capability);
                updateCapabilityFilter(f, d);
                save(f);
                return f;
            }).collect(Collectors.toList());
        capability.getCapabilityFilters().addAll(filtersToAdd);

        filetrMap.getOrDefault(CapabilityFilterState.DELETE, Collections.emptyList()).stream().map(d -> getCapabilityFilter(capability, d.getId())).filter(Optional::isPresent).map(Optional::get).forEach(f -> capability.getCapabilityFilters().remove(f));

        filetrMap.getOrDefault(CapabilityFilterState.UPDATE, Collections.emptyList()).forEach(d -> {
            val filter = getCapabilityFilter(capability, d.getId());
            filter.ifPresent(f -> updateCapabilityFilter(f, d));
        });

    }

    private void updateCapabilityFilter(CapabilityFilter f, EditCapabilityFilterDTO d) {
        f.setServicetype(serviceTypeService.get(d.getServiceTypeId()));
        f.setDefaultWorkInstruction(d.getWorkInstruktionId() != null ? workInstructionService.get(d.getWorkInstruktionId()) : null);
        f.setStandardTime(d.getStandardTime());
        f.setInternalComments(d.getInternalComments());
        f.setExternalComments(d.getExternalComments());
        f.setFilterType(d.getFilterType());
        f.setActive(d.getActive());
        f.setBestLab(d.getBestLab());
    }

    private Optional<CapabilityFilter> getCapabilityFilter(Capability capability, int filterid) {
        return capability.getCapabilityFilters().stream().filter(cf -> cf.getId() == filterid).findFirst();
    }
}
