package org.trescal.cwms.core.procedure.utils;

import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Aggregates the procedures matching from various sources Presently, results
 * from ServiceCapability (by instrument model) and Capability (by subfamily)
 * are listed The matches are sorted in order of proximity / desirability
 *
 * Want to show in order: (a) matches in same business subdivision (b) matches
 * in other business subdivisions of same country (c) matches in business
 * subdivisions of other countries (d) matches in supplier subdivisions of same
 * country (e) matches in supplier subdivisions of other country ... we need a
 * concept of internal region defined (e.g. Metz area) ... or to match by
 * proximity ... ... also, we may need a "world region" defined to give
 * preference to EEA subcontractors, etc... there is a "region" desired in
 * between (a) and (b) in the specification
 * 
 * @author galen
 *
 */
public class ProcedureMatchResults {
	private Subdiv businessSubdiv;
	private ServiceType serviceType;
	private boolean performFilterMatch;
	private boolean includeAllResults;

	private Map<ProcedureMatchType, Set<ProcedureMatch>> resultMap;

	/**
	 * Constructor for use with capability results with subfamilies, when
	 * filtering is desired
	 * 
	 * @param serviceType
	 * @param includeAllResults
	 *            - if false, only "selectable" results matching the service
	 *            type will be shown
	 * @param businessSubdiv
	 *            - 'current' business subdivision for the search
	 */
	public ProcedureMatchResults(Subdiv businessSubdiv, ServiceType serviceType, boolean includeAllResults) {
		this.businessSubdiv = businessSubdiv;
		this.serviceType = serviceType;
		this.performFilterMatch = true;
		this.includeAllResults = includeAllResults;
		initResultMap();
	}

	/**
	 * Constructor for use with 'generic' capability results when no filtering
	 * is performed
     *
     * @param serviceType
     */
    public ProcedureMatchResults(Subdiv businessSubdiv, ServiceType serviceType) {
        this.businessSubdiv = businessSubdiv;
        this.serviceType = serviceType;
        this.performFilterMatch = false;
        initResultMap();
    }

    public void initResultMap() {
        this.resultMap = new TreeMap<>();
        for (ProcedureMatchType mt : ProcedureMatchType.values()) {
            resultMap.put(mt, new TreeSet<>(new ProcedureMatchComparator()));
        }
    }

    public void addMatch(Capability capability) {
        ProcedureMatchType matchType = getMatchType(capability);
        if (this.performFilterMatch) {
            CapabilityFilter bestFilter = getCapabilityServiceTypeMatch(capability);
            ProcedureMatch match = new ProcedureMatch(capability, bestFilter);
            if (includeAllResults || match.getIndicateMatch()) {
                resultMap.get(matchType).add(match);
            }
        } else {
            ProcedureMatch match = new ProcedureMatch(capability);
            resultMap.get(matchType).add(match);
        }
    }

    // Instrument model match via service capability
    public void addMatch(ServiceCapability serviceCapability) {
        ProcedureMatchType matchType = getMatchType(serviceCapability.getCapability());
        // Note, capability still implements just calibrationType
        boolean calTypeMatch = getCalibrationTypeMatch(serviceCapability);
        ProcedureMatch match = new ProcedureMatch(serviceCapability, calTypeMatch);
        if (includeAllResults || match.getIndicateMatch()) {
            resultMap.get(matchType).add(match);
        }
    }

    public ProcedureMatchType getMatchType(Capability capability) {
        ProcedureMatchType result = null;
        if (capability.getOrganisation().getComp().getCompanyRole() == CompanyRole.BUSINESS) {
            if (capability.getOrganisation().getSubdivid() == businessSubdiv.getSubdivid()) {
                result = ProcedureMatchType.BUSINESS_SAME_SUBDIV;
            } else if (capability.getOrganisation().getComp().getCountry().getCountryid() == businessSubdiv.getComp()
                .getCountry().getCountryid()) {
                result = ProcedureMatchType.BUSINESS_SAME_COUNTRY;
            } else {
                result = ProcedureMatchType.BUSINESS_OTHER_COUNTRY;
            }
        } else {
            // Supplier procedure
            if (capability.getOrganisation().getComp().getCountry().getCountryid() == businessSubdiv.getComp()
                .getCountry().getCountryid()) {
                result = ProcedureMatchType.SUPPLIER_SAME_COUNTRY;
            } else {
                result = ProcedureMatchType.SUPPLIER_OTHER_COUNTRY;
            }
        }
        return result;
    }

    public CapabilityFilter getCapabilityServiceTypeMatch(Capability capability) {
        CapabilityFilter result = null;
        // Note, no REPAIR matching added yet, add enum of category to
        // ServiceType to indicate REPAIR?
        for (CapabilityFilter filter : capability.getCapabilityFilters()) {
            if (filter.getServicetype().getServiceTypeId() == serviceType.getServiceTypeId()) {
                result = filter;
            }
        }
        return result;
    }

    public boolean getCalibrationTypeMatch(ServiceCapability serviceCapability) {
        return serviceCapability.getCalibrationType().getServiceType().getServiceTypeId() == serviceType
            .getServiceTypeId();
    }

    public Map<ProcedureMatchType, Set<ProcedureMatch>> getResultMap() {
        return resultMap;
    }
}
