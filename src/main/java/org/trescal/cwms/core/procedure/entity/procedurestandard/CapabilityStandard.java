package org.trescal.cwms.core.procedure.entity.procedurestandard;

import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandard;
import org.trescal.cwms.core.procedure.entity.defaultstandard.DefaultStandardType;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;

import javax.persistence.*;

/**
 * Entity defining an {@link Instrument} that can or must be used to peform
 * calibrations for a particular {@link Capability}.
 *
 * @author Richard
 */
@Entity
@DiscriminatorValue("procedure")
public class CapabilityStandard extends DefaultStandard {
    private Capability capability;
    private final DefaultStandardType defaultStandardType = DefaultStandardType.PROCEDURE;

    @Override
    @Transient
    public DefaultStandardType getDefaultStandardType() {
        return this.defaultStandardType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "capabilityId")
    public Capability getCapability() {
        return this.capability;
    }

    public void setCapability(Capability capability) {
        this.capability = capability;
    }
}