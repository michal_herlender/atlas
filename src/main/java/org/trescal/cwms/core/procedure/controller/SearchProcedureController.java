package org.trescal.cwms.core.procedure.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.form.SearchCapabilitiesForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_COMPANY})
public class SearchProcedureController {
    @Autowired
    private CapabilityService procServ;

    @ModelAttribute("searchprocedureform")
    protected SearchCapabilitiesForm formBackingObject(
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
        @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto
    ) throws Exception {
        SearchCapabilitiesForm form = new SearchCapabilitiesForm();
        form.setCoid(companyDto.getKey());
        form.setSubdivid(subdivDto.getKey());
        form.setActive(true);
        return form;
    }

    @RequestMapping(value = "/searchprocedure.htm", method = RequestMethod.GET)
    protected String referenceData() {
        return "/trescal/core/procedure/searchprocedureform";
    }

    @RequestMapping(value = "/searchprocedure.htm", method = RequestMethod.POST)
    protected ModelAndView onSubmit(@ModelAttribute("command") SearchCapabilitiesForm form) throws Exception {
        PagedResultSet<Capability> resultSet = new PagedResultSet<Capability>(
            form.getResultsPerPage() == 0 ? Constants.RESULTS_PER_PAGE : form.getResultsPerPage(),
            form.getPageNo() == 0 ? 1 : form.getPageNo());
        form.setRs(this.procServ.searchCapabilities(resultSet, form));
        return new ModelAndView("/trescal/core/procedure/searchprocedureresults", "command", form);
    }
}