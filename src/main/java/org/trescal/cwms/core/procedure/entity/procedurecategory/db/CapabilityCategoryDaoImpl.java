package org.trescal.cwms.core.procedure.entity.procedurecategory.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.Department_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.procedure.dto.CapabilityCategoryDTO;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability_;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory_;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;

@Repository
public class CapabilityCategoryDaoImpl extends BaseDaoImpl<CapabilityCategory, Integer> implements CapabilityCategoryDao {

    @Override
    protected Class<CapabilityCategory> getEntity() {
        return CapabilityCategory.class;
    }

    @Override
    public List<CapabilityCategory> getAllSorted(Integer subdivId) {
        return getResultList(cb -> {
            CriteriaQuery<CapabilityCategory> cq = cb.createQuery(CapabilityCategory.class);
            Root<CapabilityCategory> category = cq.from(CapabilityCategory.class);
            Join<CapabilityCategory, Department> department = category.join(CapabilityCategory_.department,
                JoinType.LEFT);
            cq.where(cb.equal(department.get(Department_.subdiv), subdivId));
            cq.orderBy(cb.asc(department.get(Department_.name)),
                cb.desc(category.get(CapabilityCategory_.departmentDefault)),
                cb.asc(category.get(CapabilityCategory_.name)));
            return cq;
        });
    }

    @Override
    public List<CapabilityCategory> getAll(Department department) {
        return getResultList(cb -> {
            CriteriaQuery<CapabilityCategory> cq = cb.createQuery(CapabilityCategory.class);
            Root<CapabilityCategory> category = cq.from(CapabilityCategory.class);
            cq.where(cb.equal(category.get(CapabilityCategory_.department), department));
            cq.orderBy(cb.desc(category.get(CapabilityCategory_.departmentDefault)),
                cb.asc(category.get(CapabilityCategory_.name)));
            return cq;
        });
    }

    @Override
    public List<CapabilityCategoryDTO> getAll(Integer departmentId) {
        return getResultList(cb->{
            CriteriaQuery<CapabilityCategoryDTO> cq = cb.createQuery(CapabilityCategoryDTO.class);
            Root<CapabilityCategory> category = cq.from(CapabilityCategory.class);
            cq.where(cb.equal(category.get(CapabilityCategory_.department).get(Department_.deptid), departmentId));

            cq.select(cb.construct(CapabilityCategoryDTO.class,category.get(CapabilityCategory_.id),category.get(CapabilityCategory_.name),
                    category.get(CapabilityCategory_.description)));
            return cq;
        });
    }

    @Override
    public CapabilityCategory getDefault(Department department) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<CapabilityCategory> cq = cb.createQuery(CapabilityCategory.class);
        Root<CapabilityCategory> pc = cq.from(CapabilityCategory.class);
        cq.where(cb.and(cb.equal(pc.get(CapabilityCategory_.department), department),
            cb.isTrue(pc.get(CapabilityCategory_.departmentDefault))));
        List<CapabilityCategory> results = getEntityManager().createQuery(cq).getResultList();
        return results.isEmpty() ? null : results.get(0);
    }

    @Override
    public CapabilityCategory getBySubdivAndCapability(Subdiv sub, Capability capability) {
        CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
        CriteriaQuery<CapabilityCategory> cq = cb.createQuery(CapabilityCategory.class);
        Root<CapabilityCategory> procCatRoot = cq.from(CapabilityCategory.class);

        Join<CapabilityCategory, Department> procCat = procCatRoot.join(CapabilityCategory_.department);
        Join<CapabilityCategory, CategorisedCapability> catPocs = procCatRoot.join(CapabilityCategory_.procedures);

        Predicate clauses = cb.conjunction();
        clauses.getExpressions().add(cb.equal(procCat.get(Department_.subdiv), sub));
        clauses.getExpressions().add(cb.equal(catPocs.get(CategorisedCapability_.capability), capability));

        cq.where(clauses);

        TypedQuery<CapabilityCategory> query = getEntityManager().createQuery(cq);
        if (query.getResultList().size() > 0)
            return query.getResultList().get(0);
        else
            return null;
    }
}