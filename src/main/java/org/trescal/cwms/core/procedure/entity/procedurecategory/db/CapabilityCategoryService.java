package org.trescal.cwms.core.procedure.entity.procedurecategory.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.procedure.dto.CapabilityCategoryDTO;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.form.EditCapabilityCategoriesForm;

import java.util.List;

public interface CapabilityCategoryService extends BaseService<CapabilityCategory, Integer> {
    /**
     * Returns a list of all {@link CapabilityCategory} entities for a Subdiv sorted by their
     * {@link Department} and then by whether or not it is the department
     * default and then by name.
     *
     * @return list of {@link CapabilityCategory}.
     */
    List<CapabilityCategory> getAllSorted(Subdiv subdiv);

    List<CapabilityCategory> getAllSorterdBySubdivId(Integer subdivId);

    /**
     * Returns all procedure categories of the department.
     *
     * @param department a {@link Department}
     * @return list of {@link CapabilityCategory}
     */
    List<CapabilityCategory> getAll(Department department);

    /**
     * Returns the default procedure category for a department
     */
    CapabilityCategory getDefault(Department department);

    /**
     * Creates/updates/deletes the selected procedure categories
     * (inputs should have already been validated)
     */
    void editCapabilityCategories(EditCapabilityCategoriesForm form);

    CapabilityCategory getBySubdivAndCapability(Subdiv sub, Capability capability);

    List<CapabilityCategoryDTO> getAll(Integer departmentId);
}