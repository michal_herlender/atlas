package org.trescal.cwms.core.procedure.entity.workinstructionstandard.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.procedure.entity.workinstructionstandard.WorkInstructionStandard;

public interface WorkInstructionStandardDao extends BaseDao<WorkInstructionStandard, Integer>
{
}