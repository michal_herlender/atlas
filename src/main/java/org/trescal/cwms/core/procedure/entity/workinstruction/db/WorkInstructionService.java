package org.trescal.cwms.core.procedure.entity.workinstruction.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.procedure.form.EditWorkInstructionForm;
import org.trescal.cwms.core.procedure.form.SearchWorkInstructionForm;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.spring.model.KeyValue;
import org.trescal.cwms.spring.model.KeyValueIntegerString;

import java.util.List;
import java.util.Locale;
import java.util.Set;

public interface WorkInstructionService extends BaseService<WorkInstruction, Integer> {

    WorkInstruction updateWorkInstruction(EditWorkInstructionForm form, Integer coid, String username);

    void searchWorkInstructions(PagedResultSet<WorkInstruction> pagedResults, SearchWorkInstructionForm form);

    List<KeyValueIntegerString> getAllKeyValues(String workInstructionFragment, Integer descriptionId,
                                                Locale searchLanguage, int maxResults);

    Set<KeyValue<Integer, String>> getAllAsKeyeValues();
}