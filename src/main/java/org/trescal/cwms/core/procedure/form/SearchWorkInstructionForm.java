package org.trescal.cwms.core.procedure.form;

import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;
import org.trescal.cwms.core.tools.PagedResultSet;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SearchWorkInstructionForm
{
	private String name;
	private String title;
	private Integer coid;
	private Integer descid;

	private PagedResultSet<WorkInstruction> rs;
	private int pageNo;
	private int resultsPerPage;
}
