package org.trescal.cwms.core.procedure.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditWorkInstructionForm {

	@NotNull
	private Integer id;
	private Integer personid;
	private Integer descid;
	private String description;
	@NotBlank
	@Size(min=1, max=100)
	private String name;
	@NotBlank
	@Size(min=1, max=100)
	private String title;
	@NotNull
	private Integer coid;

	public static final String ERROR_CODE_VALUE_NOT_SELECTED = "{error.value.notselected}";
}