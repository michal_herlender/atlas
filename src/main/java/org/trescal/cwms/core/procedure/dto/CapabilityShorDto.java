package org.trescal.cwms.core.procedure.dto;

public class CapabilityShorDto {
    private int id;
    private String reference;
    private String name;

    /*
     * Default constructor for projections use
     */
    public CapabilityShorDto() {
    }

    /*
     * Constructor for HQL use
     */
    public CapabilityShorDto(int id, String reference, String name) {
        this.id = id;
        this.reference = reference;
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

	public String getReference()
	{
		return this.reference;
	}

	public void setReference(String reference)
	{
		this.reference = reference;
	}

	public String getName()
	{
		return this.name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

}