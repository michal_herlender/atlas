package org.trescal.cwms.core.procedure.form;

import org.trescal.cwms.core.procedure.entity.workinstruction.WorkInstruction;

public class ViewWorkInstructionForm
{
	private int defaultTempalteId;
	private WorkInstruction wi;
	private String xindiceJobs;
	private String xindiceProcedures;
	private String xindiceRoot;

	public int getDefaultTempalteId()
	{
		return this.defaultTempalteId;
	}

	public WorkInstruction getWi()
	{
		return this.wi;
	}

	public String getXindiceJobs()
	{
		return this.xindiceJobs;
	}

	public String getXindiceProcedures()
	{
		return this.xindiceProcedures;
	}

	public String getXindiceRoot()
	{
		return this.xindiceRoot;
	}

	public void setDefaultTempalteId(int defaultTempalteId)
	{
		this.defaultTempalteId = defaultTempalteId;
	}

	public void setWi(WorkInstruction wi)
	{
		this.wi = wi;
	}

	public void setXindiceJobs(String xindiceJobs)
	{
		this.xindiceJobs = xindiceJobs;
	}

	public void setXindiceProcedures(String xindiceProcedures)
	{
		this.xindiceProcedures = xindiceProcedures;
	}

	public void setXindiceRoot(String xindiceRoot)
	{
		this.xindiceRoot = xindiceRoot;
	}
}