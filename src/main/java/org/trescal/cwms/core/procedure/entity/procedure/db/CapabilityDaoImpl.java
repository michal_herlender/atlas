package org.trescal.cwms.core.procedure.entity.procedure.db;

import lombok.val;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.tuple.Triple;
import org.hibernate.criterion.MatchMode;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToSubdivDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.department.Department_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily_;
import org.trescal.cwms.core.instrumentmodel.entity.servicecapability.ServiceCapability_;
import org.trescal.cwms.core.jobs.calibration.entity.calibrationprocess.CalibrationProcess_;
import org.trescal.cwms.core.jobs.contractreview.entity.WorkRequirementType;
import org.trescal.cwms.core.procedure.dto.CapabilityAccreditationInfoDto;
import org.trescal.cwms.core.procedure.dto.CapabilityPopUpDto;
import org.trescal.cwms.core.procedure.dto.CapabilityProjectionDTO;
import org.trescal.cwms.core.procedure.dto.CapabilityShorDto;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilterType;
import org.trescal.cwms.core.procedure.entity.capabilityfilter.CapabilityFilter_;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability_;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.Capability_;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization_;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory;
import org.trescal.cwms.core.procedure.entity.procedurecategory.CapabilityCategory_;
import org.trescal.cwms.core.procedure.entity.procedureheading.CapabilityHeading_;
import org.trescal.cwms.core.procedure.form.SearchCapabilitiesForm;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType_;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType;
import org.trescal.cwms.core.system.entity.servicetype.ServiceType_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.workflow.entity.itemstate.ItemState_;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import static org.trescal.cwms.core.tools.StringJpaUtils.*;

@Repository
public class CapabilityDaoImpl extends AllocatedToSubdivDaoImpl<Capability, Integer> implements CapabilityDao {
    @Override
    protected Class<Capability> getEntity() {
        return Capability.class;
    }

    @Override
    public Optional<Capability> findCapability(int id) {
        return getFirstResult(cb -> {
            val cq = cb.createQuery(Capability.class);
            val procedure = cq.from(Capability.class);
            procedure.fetch(Capability_.heading, JoinType.LEFT);
            procedure.fetch(Capability_.categories, JoinType.LEFT).fetch(CategorisedCapability_.category, JoinType.LEFT).fetch(CapabilityCategory_.department, JoinType.LEFT);
            procedure.fetch(Capability_.calibrationProcess, JoinType.LEFT);
            val acc = procedure.fetch(Capability_.authorizations, JoinType.LEFT);
            acc.fetch(CapabilityAuthorization_.caltype, JoinType.LEFT);
            acc.fetch(CapabilityAuthorization_.authorizationFor, JoinType.LEFT);
            procedure.fetch(Capability_.defWorkInstruction, JoinType.LEFT);
            cq.where(cb.equal(procedure, id));
            return cq;
        });
    }

    @Override
    public List<CapabilityProjectionDTO> findAllForSubdiv(Integer subdivId) {
        return getResultList(cb -> {
            CriteriaQuery<CapabilityProjectionDTO> cq = cb.createQuery(CapabilityProjectionDTO.class);
            Root<Capability> procedure = cq.from(Capability.class);
            Join<Capability, Subdiv> allocatedSubdiv = procedure.join("organisation");
            cq.where(cb.equal(allocatedSubdiv.get(Subdiv_.subdivid), subdivId));
            cq.orderBy(cb.asc(procedure.get(Capability_.reference)), cb.asc(procedure.get(Capability_.name)));
            cq.select(cb.construct(CapabilityProjectionDTO.class,
                procedure.get(Capability_.id),
                procedure.get(Capability_.name),
                procedure.get(Capability_.reference)
            ));
            return cq;
        });
    }

    @Override
    public List<Capability> getAll(Subdiv subdiv) {
        return getResultList(cb -> {
            val cq = cb.createQuery(Capability.class);
            val procedure = cq.from(Capability.class);
            cq.where(cb.equal(procedure.get(Capability_.organisation), subdiv));
            return cq;
        });
    }

    @Override
    public List<Capability> getAll(CapabilityCategory category) {
        return getResultList(cb -> {
            CriteriaQuery<Capability> cq = cb.createQuery(Capability.class);
            Root<Capability> root = cq.from(Capability.class);
            Join<Capability, CategorisedCapability> cp = root.join(Capability_.categories);
            cq.where(cb.equal(cp.get(CategorisedCapability_.category), category));
            return cq;
        });
    }

    @Override
    public List<Capability> getAllForDepartment(Integer deptId) {
        return getResultList(cb -> {
            CriteriaQuery<Capability> cq = cb.createQuery(Capability.class);
            Root<Capability> root = cq.from(Capability.class);
            Join<Capability, CategorisedCapability> cp = root.join(Capability_.categories);
            Join<CategorisedCapability, CapabilityCategory> pc = cp.join(CategorisedCapability_.category);
            Join<CapabilityCategory, Department> dept = pc.join(CapabilityCategory_.department);

            cq.where(cb.equal(dept.get(Department_.deptid), deptId));
            cq.distinct(true);
            return cq;
        });
    }

    @Override
    public List<Capability> getAllForDomainAndSubdiv(Integer domainId, Subdiv subdiv) {
        return getResultList(cb -> {
            CriteriaQuery<Capability> cq = cb.createQuery(Capability.class);
            Root<Capability> root = cq.from(Capability.class);

            Predicate clauses = cb.conjunction();
            if (domainId == null || domainId == 0) {
                clauses.getExpressions().add(cb.isNull(root.get(Capability_.subfamily)));
            } else {
                Join<Capability, Description> subfamily = root.join(Capability_.subfamily,
                    javax.persistence.criteria.JoinType.LEFT);
                Join<Description, InstrumentModelFamily> family = subfamily.join(Description_.family);
                Join<InstrumentModelFamily, InstrumentModelDomain> domain = family.join(InstrumentModelFamily_.domain);
                clauses.getExpressions().add(cb.equal(domain.get(InstrumentModelDomain_.domainid), domainId));
            }
            clauses.getExpressions().add(cb.equal(root.get(Capability_.organisation), subdiv));
            cq.where(clauses);
            return cq;
        });
    }

    @Override
    public List<Capability> getAllCapabilitiesWithNoDepartment(Subdiv subdiv) {
        return getResultList(cb -> {
            val cq = cb.createQuery(Capability.class);
            val procedure = cq.from(Capability.class);
            val cp = procedure.join(Capability_.categories, javax.persistence.criteria.JoinType.LEFT);
            cq.where(cb.isNull(cp), cb.equal(procedure.get(Capability_.organisation), subdiv));
            cq.orderBy(cb.asc(procedure.get(Capability_.reference)),
                cb.asc(procedure.get(Capability_.id)));
            return cq;
        });
    }
	
	@Override
	public List<CapabilityProjectionDTO> getCapabilityProjectionDtos(Collection<Integer> capabilityIds) {
		if (capabilityIds == null || capabilityIds.isEmpty())
			throw new IllegalArgumentException("At least one capabilityId must be specified");
        return getResultList(cb -> {
            CriteriaQuery<CapabilityProjectionDTO> cq = cb.createQuery(CapabilityProjectionDTO.class);
            Root<Capability> root = cq.from(Capability.class);
            cq.where(root.get(Capability_.id).in(capabilityIds));
            cq.select(cb.construct(CapabilityProjectionDTO.class,
                root.get(Capability_.id),
                root.get(Capability_.name),
                root.get(Capability_.reference)
            ));
            return cq;
        });
    }

    @Override
    public List<Capability> getCapabilitiesByReference(String reference, Boolean active, Integer ignoreProcId) {
        return getResultList(cb -> {
            val cq = cb.createQuery(Capability.class);
            val procedure = cq.from(Capability.class);
            val predicates = Stream.of(
                ilikePredicateStream(cb, procedure.get(Capability_.reference), reference, MatchMode.EXACT),
                ignoreProcId != null ? Stream.of(cb.notEqual(procedure, ignoreProcId)) : Stream.<Predicate>empty(),
                active != null ? Stream.of(cb.equal(procedure.get(Capability_.active), active)) : Stream.<Predicate>empty()
            ).flatMap(Function.identity()).toArray(Predicate[]::new);
            cq.where(predicates);
            return cq;
        });
    }

    @Override
    public PagedResultSet<Capability> searchCapabilities(PagedResultSet<Capability> rs, SearchCapabilitiesForm form) {
        this.completePagedResultSet(rs, Capability.class, cb -> cq -> {
            Root<Capability> procedure = cq.from(Capability.class);
            Predicate clauses = cb.conjunction();
            if ((form.getReference() != null) && !form.getReference().trim().equals(""))
                clauses.getExpressions()
                    .add(cb.like(procedure.get(Capability_.reference), "%" + form.getReference() + "%"));
            if ((form.getName() != null) && !form.getName().trim().equals(""))
                clauses.getExpressions().add(cb.like(procedure.get(Capability_.name), "%" + form.getName() + "%"));
            if ((form.getDescription() != null) && !form.getDescription().trim().equals(""))
                clauses.getExpressions()
                    .add(cb.like(procedure.get(Capability_.description), "%" + form.getDescription() + "%"));
            if (form.isActive())
                clauses.getExpressions().add(cb.isTrue(procedure.get(Capability_.active)));
            if (form.getSubdivid() != null && form.getSubdivid() != 0) {
                Join<Capability, Subdiv> subdiv = procedure.join(Capability_.organisation.getName());
                clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), form.getSubdivid()));
            } else if (form.getCoid() != null && form.getCoid() != 0) {
                Join<Capability, Subdiv> subdiv = procedure.join(Capability_.organisation.getName());
                Join<Subdiv, Company> company = subdiv.join(Subdiv_.comp);
                clauses.getExpressions().add(cb.equal(company.get(Company_.coid), form.getCoid()));
            }
            if (form.getSubfamilyId() != null && form.getSubfamilyId() != 0) {
                Join<Capability, Description> subfamily = procedure.join(Capability_.subfamily);
                clauses.getExpressions().add(cb.equal(subfamily.get(Description_.id), form.getSubfamilyId()));
            }
            cq.where(clauses);
            cq.distinct(true);
            List<Order> orders = new ArrayList<>();
            orders.add(cb.asc(procedure.get(Capability_.reference)));
            return Triple.of(procedure.get(Capability_.id), null, orders);
        });
        return rs;
    }

    @Override
    public List<Capability> searchCapabilityBySubFamilyIdAndSubdivId(Integer subFamilyId, boolean searchWithoutSubfamily,
                                                                     Integer businessSubdivId) {
        CriteriaBuilder cb = super.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Capability> cq = cb.createQuery(Capability.class);
        Root<Capability> root = cq.from(Capability.class);

        Predicate clauses = cb.conjunction();
        if (searchWithoutSubfamily) {
            clauses.getExpressions().add(cb.isNull(root.get(Capability_.subfamily)));
        } else if (subFamilyId != null) {
            Join<Capability, Description> subfamily = root.join(Capability_.subfamily,
                javax.persistence.criteria.JoinType.LEFT);
            clauses.getExpressions().add(cb.equal(subfamily.get(Description_.id), subFamilyId));
        }

        if (businessSubdivId != null) {
            Join<Capability, Subdiv> subdiv = root.join(Capability_.organisation.getName(),
                javax.persistence.criteria.JoinType.LEFT);
            clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), businessSubdivId));
        }
        clauses.getExpressions().add(cb.equal(root.get(Capability_.active), true));

        cq.where(clauses);

        return super.getEntityManager().createQuery(cq).getResultList();
    }

    public List<CapabilityShorDto> searchCapabilitiesProjection(String reference, String name,
                                                                boolean andTrueOrFalse, int subdivId, Integer modelId, int maxResults, String jobtype) {
        return getResultList(cb -> {
            val cq = cb.createQuery(CapabilityShorDto.class);
            val procedure = cq.from(Capability.class);
            val junctionPredicates = Stream.of(
                ilikePredicateStream(cb, procedure.get(Capability_.name), name, MatchMode.ANYWHERE),
                ilikePredicateStream(cb, procedure.get(Capability_.reference), reference, MatchMode.ANYWHERE)
            ).flatMap(Function.identity()).toArray(Predicate[]::new);
            val predicates = Stream.of(
                Stream.of(cb.isTrue(procedure.get(Capability_.active))),
                Stream.of(cb.equal(procedure.get(Capability_.organisation), subdivId)),
                modelId != null ? Stream.of(cb.equal(procedure.join(Capability_.serviceCapabilities).get(ServiceCapability_.instrumentModel), modelId)) : Stream.<Predicate>empty(),
                jobtype.equals("STANDARD") ? Stream.of(cb.notEqual(procedure.get(Capability_.reqType), WorkRequirementType.ONSITE)) : Stream.<Predicate>empty(),
                Stream.of(andTrueOrFalse ? cb.and(junctionPredicates) : cb.or(junctionPredicates))
            ).flatMap(Function.identity()).toArray(Predicate[]::new);
            cq.where(predicates);
            cq.select(cb.construct(CapabilityShorDto.class,
                procedure.get(Capability_.id),
                procedure.get(Capability_.reference),
                procedure.get(Capability_.name)
            ));
            cq.orderBy(cb.asc(procedure.get(Capability_.reference)));
            return cq;
        }, 0, maxResults);
    }

    public List<CapabilityShorDto> searchCapabilitiesList(String searchTerm, Integer subdivId, Boolean hideDactivated) {
        return getResultList(cb -> {
            val cq = cb.createQuery(CapabilityShorDto.class);
            val cabalility = cq.from(Capability.class);
            val predicates = Stream.of(
                    Stream.of(cb.or(ilike(cb, cabalility.get(Capability_.name), searchTerm, MatchMode.START),
                            ilike(cb, cabalility.get(Capability_.reference), searchTerm, MatchMode.START)
                    ), cb.equal(cabalility.get(Capability_.organisation), subdivId)),
                    hideDactivated ? Stream.of(cb.isTrue(cabalility.get(Capability_.active))) : Stream.<Predicate>empty()
            ).flatMap(Function.identity()).toArray(Predicate[]::new);
            cq.where(predicates);
            cq.select(cb.construct(CapabilityShorDto.class,
                    cabalility.get(Capability_.id), cabalility.get(Capability_.reference), cabalility.get(Capability_.name)
            ));
            return cq;
        });
    }

    @Override
    public Optional<Capability> getActiveCapabilityUsingCapabilityFilter(Integer subFamilyId, Integer serviceTypeId, Integer businessSubdivId,
                                                                         List<CapabilityFilterType> capabilityFilterTypes) {
        return getFirstResult(cb -> {
            CriteriaQuery<Capability> cq = cb.createQuery(Capability.class);
            Root<Capability> root = cq.from(Capability.class);
            Join<Capability, Description> subfamily = root.join(Capability_.subfamily,
                javax.persistence.criteria.JoinType.LEFT);
            Join<Capability, Subdiv> subdiv = root.join(Capability_.organisation.getName(),
                javax.persistence.criteria.JoinType.LEFT);
            Join<Capability, CapabilityFilter> capabilityServiceType = root.join(Capability_.capabilityFilters, javax.persistence.criteria.JoinType.LEFT);
            Join<CapabilityFilter, ServiceType> serviceType = capabilityServiceType.join(CapabilityFilter_.servicetype);

            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(cb.isTrue(root.get(Capability_.active)));
            clauses.getExpressions().add(cb.equal(subfamily.get(Description_.id), subFamilyId));
            clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), businessSubdivId));
            clauses.getExpressions().add(capabilityServiceType.get(CapabilityFilter_.filterType).in(capabilityFilterTypes));
            clauses.getExpressions().add(cb.equal(serviceType.get(ServiceType_.serviceTypeId), serviceTypeId));
            cq.where(clauses);
            cq.distinct(true);
            return cq;

        });
    }

    @Override
    public List<CapabilityProjectionDTO> getActiveCapabilitiesUsingListOfCapabilityFilter(MultiValuedMap<Integer, Integer> subFamilyServiceType,
                                                                                          Integer businessSubdivId, List<CapabilityFilterType> capabilityFilterTypes) {

        return getResultList(cb -> {
            CriteriaQuery<CapabilityProjectionDTO> cq = cb.createQuery(CapabilityProjectionDTO.class);
            Root<Capability> root = cq.from(Capability.class);
            Join<Capability, Description> subfamily = root.join(Capability_.subfamily,
                javax.persistence.criteria.JoinType.LEFT);
            Join<Capability, Subdiv> subdiv = root.join(Capability_.organisation.getName(),
                javax.persistence.criteria.JoinType.LEFT);
            Join<Capability, CapabilityFilter> capabilityServiceType = root.join(Capability_.capabilityFilters, javax.persistence.criteria.JoinType.LEFT);
            Join<CapabilityFilter, ServiceType> serviceType = capabilityServiceType.join(CapabilityFilter_.servicetype);

            Predicate orClauses = cb.disjunction();
            for (Integer key : subFamilyServiceType.keySet()) {
                for (Integer value : subFamilyServiceType.get(key)) {
                    Predicate andClauses;
                    andClauses = cb.and(cb.equal(subfamily.get(Description_.id), key),
                        cb.equal(serviceType.get(ServiceType_.serviceTypeId), value),
                        capabilityServiceType.get(CapabilityFilter_.filterType).in(capabilityFilterTypes),
                        cb.isTrue(root.get(Capability_.active)),
                        cb.equal(subdiv.get(Subdiv_.subdivid), businessSubdivId));
                    orClauses.getExpressions().add(cb.or(andClauses));
                }
            }

            cq.where(orClauses);
            cq.select(cb.construct(CapabilityProjectionDTO.class,
                root.get(Capability_.id),
                serviceType.get(ServiceType_.serviceTypeId),
                subfamily.get(Description_.id)
            ));
            cq.distinct(true);
            return cq;
        });
    }

    @Override
    public Optional<CapabilityPopUpDto> findCapabilityForPopUp(Integer id) {
        return getFirstResult(cb -> {
            val cq = cb.createQuery(CapabilityPopUpDto.class);
            val capability = cq.from(Capability.class);
            cq.where(cb.equal(capability, id));
            cq.select(cb.construct(CapabilityPopUpDto.class,
                capability.get(Capability_.name),
                capability.get(Capability_.description),
                capability.get(Capability_.reference),
                capability.join(Capability_.heading, JoinType.LEFT).get(CapabilityHeading_.heading),
                capabilityCategorySubquery(cb, cq, id),
                capability.join(Capability_.calibrationProcess, JoinType.LEFT).get(CalibrationProcess_.process),
                capability.get(Capability_.active)
            ));
            return cq;
        });
    }

    @Override
    public List<CapabilityAccreditationInfoDto> findAccreditationsForPopUp(Integer id) {
        return getResultList(cb -> {
            val cq = cb.createQuery(CapabilityAccreditationInfoDto.class);
            val authorization = cq.from(CapabilityAuthorization.class);
            cq.where(cb.equal(authorization.get(CapabilityAuthorization_.capability), id));
            cq.select(cb.construct(CapabilityAccreditationInfoDto.class,
                contactFullName(authorization.join(CapabilityAuthorization_.authorizationFor)).apply(cb),
                authorization.join(CapabilityAuthorization_.caltype).get(CalibrationType_.calTypeId),
                authorization.get(CapabilityAuthorization_.accredLevel)
            ));
            return cq;
        });
    }



    private Subquery<CapabilityCategory> capabilityCategorySubquery(CriteriaBuilder cb, CriteriaQuery<CapabilityPopUpDto> cq, Integer id) {
        val subQueryInt = cq.subquery(Integer.class);
        val categorisedInt = subQueryInt.from(CategorisedCapability.class);
        subQueryInt.select(cb.min(categorisedInt.get(CategorisedCapability_.id))).where(cb.equal(categorisedInt.get(CategorisedCapability_.capability), id));
        val subQuery = cq.subquery(CapabilityCategory.class);
        val catogorised = subQuery.from(CategorisedCapability.class);
        val category = catogorised.join(CategorisedCapability_.category);
        subQuery.select(category).where(cb.equal(catogorised.get(CategorisedCapability_.id), subQueryInt.getSelection()));
        return subQuery;
    }


    @Override
    public List<Capability> getCapabilities(Collection<Integer> capabilityIds, Integer subdivId) {
        return getResultList(cb -> {
            CriteriaQuery<Capability> cq = cb.createQuery(Capability.class);
            Root<Capability> root = cq.from(Capability.class);

            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(root.get(Capability_.id).in(capabilityIds));
            clauses.getExpressions().add(cb.equal(root.get(Capability_.organisation), subdivId));

            cq.where(clauses);

            return cq;
        });
    }

    @Override
    public List<Capability> getCapabilitieInSubdivsOfBc(Collection<Integer> capabilityIds, List<Integer> subdivis) {
        return getResultList(cb -> {
            CriteriaQuery<Capability> cq = cb.createQuery(Capability.class);
            Root<Capability> root = cq.from(Capability.class);

            Predicate clauses = cb.conjunction();
            clauses.getExpressions().add(root.get(Capability_.id).in(capabilityIds));
            clauses.getExpressions().add(root.get(Capability_.organisation).in(subdivis));

            cq.where(clauses);

            return cq;
        });
    }


}