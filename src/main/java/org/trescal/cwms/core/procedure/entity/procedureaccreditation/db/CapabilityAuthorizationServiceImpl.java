package org.trescal.cwms.core.procedure.entity.procedureaccreditation.db;

import io.vavr.control.Either;
import io.vavr.control.Option;
import lombok.val;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.ServletRequestUtils;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.department.Department;
import org.trescal.cwms.core.company.entity.departmentmember.DepartmentMember;
import org.trescal.cwms.core.company.entity.departmentmember.db.DepartmentMemberService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.entity.role.Roles;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.procedure.dto.CapabilityAuthorizationForm;
import org.trescal.cwms.core.procedure.dto.ProcedureAuthorizationItem;
import org.trescal.cwms.core.procedure.entity.categorisedprocedure.CategorisedCapability;
import org.trescal.cwms.core.procedure.entity.procedure.Capability;
import org.trescal.cwms.core.procedure.entity.procedure.db.CapabilityService;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.AccreditationLevel;
import org.trescal.cwms.core.procedure.entity.procedureaccreditation.CapabilityAuthorization;
import org.trescal.cwms.core.procedure.form.EditContactCapabilityAccreditationForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CapabilityAuthorizationServiceImpl implements CapabilityAuthorizationService {
    @Autowired
    private CalibrationTypeService calTypeServ;
    @Autowired
    private ContactService contactServ;
    @Autowired
    private DepartmentMemberService deptMemberServ;
    @Autowired
    private CapabilityAuthorizationDao CapabilityAuthorizationDao;
    @Autowired
    private CapabilityService procServ;
    @Autowired
    private UserService userService;
    @Autowired
    private AuthenticationService authServ;

    protected final Log logger = LogFactory.getLog(this.getClass());

    @Override
    public Either<String, AccreditationLevel> authorizedFor(int procid, Integer caltypeid, int personid) {
        List<CapabilityAuthorization> authorizations = this.getCapabilityAuthorizations(procid, caltypeid, personid);
        if (authorizations.size() == 0) {
            return Either.left("empty");
        } else {
            // more than one accreditation was returned, so loop through and
            // find the highest level
            val removed = authorizations.stream().filter(a -> a.getAccredLevel().getLevel() < 0).findFirst();

            val current = authorizations.stream().max(Comparator.comparing(ca -> ca.getAccredLevel().getLevel())).map(CapabilityAuthorization::getAccredLevel).map(Either::<String, AccreditationLevel>right).orElseGet(() -> Either.left("empty"));

            return removed.map(unused -> Either.<String, AccreditationLevel>left("removed"))
                    .orElse(current);
        }
    }

    public void deleteCapabilityAuthorization(CapabilityAuthorization capabilityAuthorization) {
        this.CapabilityAuthorizationDao.remove(capabilityAuthorization);
    }

    @Override
    public Optional<CapabilityAuthorization> findCapabilityAuthorization(int capabilityId, Integer caltypeid,
                                                                         int personid) {
        return this.CapabilityAuthorizationDao.findCapabilityAuthorization(capabilityId, caltypeid, personid);
    }

    public List<CapabilityAuthorization> getAllEagerCapabilityAuthorizations() {
        return this.CapabilityAuthorizationDao.getAllEagerCapabilityAuthorizations();
    }

    @Override
    public List<CapabilityAuthorization> getCapabilityAuthorizations(int capabilityId, Integer caltypeid,
                                                                     int personid) {
        return this.CapabilityAuthorizationDao.getCapabilityAuthorizations(capabilityId, caltypeid, personid);
    }

    public void insertCapabilityAuthorization(CapabilityAuthorization capabilityAuthorization) {
        this.CapabilityAuthorizationDao.persist(capabilityAuthorization);
    }

    public List<Roles> resolveRoles(String commaSeparatedRoles) {
        List<Roles> roles = new ArrayList<>();

        if ((commaSeparatedRoles != null) && !commaSeparatedRoles.trim().isEmpty()) {
            String[] roleStrings = commaSeparatedRoles.split(",");

            for (String roleStr : roleStrings) {
                try {
                    Roles r = Roles.valueOf(roleStr.trim());
                    roles.add(r);
                } catch (IllegalArgumentException iae) {
                    iae.printStackTrace();
                }
            }
        }

        return roles;
    }

    public void saveOrUpdateCapabilityAuthorization(CapabilityAuthorization capabilityAuthorization) {
        this.CapabilityAuthorizationDao.persist(capabilityAuthorization);
    }

    public void updateCapabilityAuthorization(CapabilityAuthorization capabilityAuthorization) {
        this.CapabilityAuthorizationDao.merge(capabilityAuthorization);
    }

    @Override
    public ResultWrapper userCanUpdateContactCapabilityAuthorization(Contact editor, Capability capability,
                                                                     Contact authorized, HttpSession session) {
        ResultWrapper wrapper = new ResultWrapper(false, "", null, null);
        Collection<? extends GrantedAuthority> auth = SecurityContextHolder.getContext().getAuthentication()
                .getAuthorities();

        if (auth.contains(Permission.ACCREDITATION_CONTACT_PROCEDURE_EDIT_ADMIN)) {
            wrapper.setSuccess(true);
            return wrapper;
        } else if (auth.contains(Permission.ACCREDITATION_CONTACT_PROCEDURE_EDIT)) {
            // make sure the user is not trying to edit another
            // manager, admin or director
            boolean roleAuthentication = true;
            User userToAccreditate = authorized.getUser();
            @SuppressWarnings("unchecked")
            KeyValue<Integer, String> subdivDtoId = (KeyValue<Integer, String>) session
                    .getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV);
            if (authServ.hasRight(userToAccreditate.getCon(), Permission.ACCREDITATION_CONTACT_PROCEDURE_EDIT_ADMIN,
                    subdivDtoId.getKey())) {
                roleAuthentication = false;
            }
            if (roleAuthentication) {
                // procedure does not belong to a department, but
                // the user is a manager so let them continue
                if (capability.getCategories().size() == 0) {
                    wrapper.setSuccess(true);
                } else {
                    // finally make sure that the user is a
                    // manager of one of the departments this
                    // procedure belongs to
                    for (CategorisedCapability cat : capability.getCategories()) {
                        Department d = cat.getCategory().getDepartment();
                        DepartmentMember member = this.deptMemberServ.findDepartmentMember(editor.getPersonid(),
                                d.getDeptid());
                        DepartmentMember memberTarget = this.deptMemberServ
                                .findDepartmentMember(userToAccreditate.getCon().getId(), d.getDeptid());
                        if (member != null) {
                            if (editor.getPersonid() == authorized.getPersonid()
                                    && member.getDepartmentRole().getRole().trim().equalsIgnoreCase("Director")
                                    || editor.getPersonid() == authorized.getPersonid() && member.getDepartmentRole()
                                    .getRole().trim().equalsIgnoreCase("Manager")) {
                                wrapper.setSuccess(true);
                                break;
                            } else if (member.getDepartmentRole().getRole().trim().equalsIgnoreCase("Director"))
                                if (memberTarget != null && memberTarget.getDepartmentRole().getRole().trim()
                                        .equalsIgnoreCase("Director")) {
                                    wrapper.setSuccess(false);
                                    break;
                                } else {
                                    wrapper.setSuccess(true);
                                    break;
                                }
                            else if (member.getDepartmentRole().getRole().trim().equalsIgnoreCase("Manager"))
                                if (memberTarget != null
                                        && memberTarget.getDepartmentRole().getRole().trim()
                                        .equalsIgnoreCase("Director")
                                        || memberTarget != null && memberTarget.getDepartmentRole().getRole().trim()
                                        .equalsIgnoreCase("Manager")) {
                                    wrapper.setSuccess(false);
                                    break;
                                } else {
                                    wrapper.setSuccess(true);
                                    break;
                                }
                            else {
                                wrapper.setSuccess(false);
                                break;
                            }
                        }
                    }
                }

                if (!wrapper.isSuccess()) {
                    wrapper.setMessage("The user is not a manager of this procedure's department");
                }
            } else {
                wrapper.setSuccess(false);
                wrapper.setMessage(
                        "The contact attempting to edit the procedure accreditations doesn't have access to edit accreditations of the selected person");
            }
        }
        return wrapper;

    }

    @Override
    public ResultWrapper userCanUpdateContactCapabilityAuthorization(HttpServletRequest req) {
        int personid = ServletRequestUtils.getIntParameter(req, "personid", 0);
        int procid = ServletRequestUtils.getIntParameter(req, "procid", 0);
        Contact accredited = this.contactServ.get(personid);
        Contact editor = (Contact) req.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_CONTACT);
        Capability capability = this.procServ.get(procid);
        if ((procid == 0) || (capability == null)) {
            return new ResultWrapper(false, "Unable to find the requested procedure", null, null);
        } else if ((personid == 0) || (accredited == null)) {
            return new ResultWrapper(false, "Unable to find the requested Contact", null, null);
        } else {
            return this.userCanUpdateContactCapabilityAuthorization(editor, capability, accredited, req.getSession());
        }
    }

    @Override
    public Boolean userCanUpdateContactCapabilityAuthorization(Contact editor, Capability capability,
                                                               Contact accredited, KeyValue<Integer, String> subdivDtoId) {
        boolean result = false;

        Collection<? extends GrantedAuthority> auth = SecurityContextHolder.getContext().getAuthentication()
                .getAuthorities();
        if (auth.contains(Permission.ACCREDITATION_CONTACT_PROCEDURE_EDIT_ADMIN)) {
            return true;
        } else if (auth.contains(Permission.ACCREDITATION_CONTACT_PROCEDURE_EDIT)) {
            // make sure the user is not trying to edit another
            // manager, admin or director
            boolean roleAuthentication = true;
            User userToAccreditate = accredited.getUser();
            if (authServ.hasRight(userToAccreditate.getCon(), Permission.ACCREDITATION_CONTACT_PROCEDURE_EDIT_ADMIN,
                    subdivDtoId.getKey())) {
                roleAuthentication = false;
            }
            if (roleAuthentication) {
                // procedure does not belong to a department, but
                // the user is a manager so let them continue
                if (capability.getCategories().size() == 0) {
                    result = true;
                } else {
                    // finally make sure that the user is a
                    // manager of one of the departments this
                    // procedure belongs to
                    for (CategorisedCapability cat : capability.getCategories()) {
                        Department d = cat.getCategory().getDepartment();
                        DepartmentMember member = this.deptMemberServ.findDepartmentMember(editor.getPersonid(),
                                d.getDeptid());
                        DepartmentMember memberTarget = this.deptMemberServ
                                .findDepartmentMember(userToAccreditate.getCon().getId(), d.getDeptid());
                        if (member != null) {
                            if (editor.getPersonid() == accredited.getPersonid()
                                    && member.getDepartmentRole().getRole().trim().equalsIgnoreCase("Director")
                                    || editor.getPersonid() == accredited.getPersonid() && member.getDepartmentRole()
                                    .getRole().trim().equalsIgnoreCase("Manager")) {
                                result = true;
                                break;
                            } else if (member.getDepartmentRole().getRole().trim().equalsIgnoreCase("Director"))
                                if (memberTarget != null && memberTarget.getDepartmentRole().getRole().trim()
                                        .equalsIgnoreCase("Director")) {
                                    break;
                                } else {
                                    result = true;
                                    break;
                                }
                            else if (member.getDepartmentRole().getRole().trim().equalsIgnoreCase("Manager"))
                                if (memberTarget != null
                                        && memberTarget.getDepartmentRole().getRole().trim()
                                        .equalsIgnoreCase("Director")
                                        || memberTarget != null && memberTarget.getDepartmentRole().getRole().trim()
                                        .equalsIgnoreCase("Manager")) {
                                    break;
                                } else {
                                    result = true;
                                    break;
                                }
                            else {
                                break;
                            }
                        }
                    }

                }

            }

        }

        return result;
    }

    @Override
    public List<CapabilityAuthorization> getAllAuthorizationsForCapability(int capabilityId) {
        return this.CapabilityAuthorizationDao.getAllAuthorizationsForCapability(capabilityId);
    }

    @Override
    public void saveOrUpdateAuthorizationsForCapability(CapabilityAuthorizationForm form, Contact editor) {

        for (ProcedureAuthorizationItem item : form.getItems()) {

            val acOpt = Option.ofOptional(
                    this.findCapabilityAuthorization(form.getProcsId(), item.getCaltypsId(), form.getPersonId()));
            acOpt.peek(ac -> {
                if (item.getLevel() == AccreditationLevel.NONE) {
                    this.deleteCapabilityAuthorization(ac);
                } else {
                    ac.setAccredLevel(item.getLevel());
                    ac.setAwardedOn(item.getDate());
                    this.updateCapabilityAuthorization(ac);
                }
            });
            acOpt.onEmpty(() -> {
                val ac = new CapabilityAuthorization();
                ac.setAuthorizationFor(this.contactServ.get(form.getPersonId()));
                ac.setAccredLevel(item.getLevel());
                ac.setAwardedBy(editor);
                ac.setAwardedOn(item.getDate());
                ac.setCaltype(this.calTypeServ.find(item.getCaltypsId()));
                ac.setCapability(this.procServ.get(form.getProcsId()));
                this.insertCapabilityAuthorization(ac);
            });
        }

    }

    @Override
    public Map<Contact, List<CapabilityAuthorization>> authorizedContactsWithThereAuthorizations(Integer capabilityId, Contact editor, KeyValue<Integer, String> subdivDtoId) {

        Map<Contact, List<CapabilityAuthorization>> authorizedContacts = this.getAllAuthorizationsForCapability(capabilityId).stream().
                filter(pa -> pa.getAccredLevel() != null)
                .collect(Collectors.groupingBy(CapabilityAuthorization::getAuthorizationFor));
        Capability proc = this.procServ.get(capabilityId);
        List<Contact> accreditedContacs = new ArrayList<>();
        List<Department> depts = new ArrayList<>();
        for (CategorisedCapability category : proc.getCategories()) {
            depts.add(category.getCategory().getDepartment());
        }
        List<User> users = deptMemberServ.getDepartmentUsers(depts);
        for (User user : users) {
            if (authorizedContacts.containsKey(user.getCon()) &&
                    !this.userCanUpdateContactCapabilityAuthorization(editor, proc, user.getCon(), subdivDtoId)) {
                authorizedContacts.remove(user.getCon());
            }
        }

        return authorizedContacts;


    }

    @Override
    public List<Contact> getNonAuthorizedContacts(Integer capabilityId,
                                                  Map<Contact, List<CapabilityAuthorization>> authorizedContacts, Contact editor,
                                                  KeyValue<Integer, String> subdivDtoId) {

        Capability proc = this.procServ.get(capabilityId);
        List<Contact> nonAccreditedContacs = new ArrayList<>();
        List<Department> depts = new ArrayList<>();
        for (CategorisedCapability category : proc.getCategories()) {
            depts.add(category.getCategory().getDepartment());
        }
        List<User> users = deptMemberServ.getDepartmentUsers(depts);
        for (User user : users) {
            if (!authorizedContacts.containsKey(user.getCon()) &&
                    this.userCanUpdateContactCapabilityAuthorization(editor, this.procServ.get(capabilityId), user.getCon(), subdivDtoId)) {
                nonAccreditedContacs.add(user.getCon());
            }
        }
        return nonAccreditedContacs;
    }

    @Override
    public void editContactCapabilityAuthorization(EditContactCapabilityAccreditationForm form, String username) {
        for (int i = 0; i < form.getCalTypes().size(); i++) {
            CalibrationType ct = form.getCalTypes().get(i);
            AccreditationLevel level = form.getAccredLevels().get(i);
            LocalDate setOn = form.getAccredOns().get(i);
            val accredOpt = Option.ofOptional(this.findCapabilityAuthorization(form.getCapability().getId(),
                    ct.getCalTypeId(), form.getContact().getPersonid()));
            if (level != null) {
                // test if an accreditation for this contact, proc combo exists,
                // if so get it and update it
                accredOpt.orElse(() -> Option.of(new CapabilityAuthorization())).peek(accred -> {
                    accred.setAuthorizationFor(form.getContact());
                    accred.setAccredLevel(level);
                    Contact contact = this.userService.get(username).getCon();
                    accred.setAwardedBy(contact);
                    accred.setCaltype(ct);
                    accred.setCapability(form.getCapability());
                    accred.setAwardedOn(setOn);
                    this.saveOrUpdateCapabilityAuthorization(accred);
                });
            } else {
                // test if a accreditation for this contact, proc combo exists,
                // if so get it and delete it
                accredOpt.peek(this::deleteCapabilityAuthorization);
            }
        }
    }

}