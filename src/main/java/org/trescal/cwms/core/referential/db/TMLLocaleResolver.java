package org.trescal.cwms.core.referential.db;

import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.trescal.cwms.core.tools.supportedlocale.SupportedLocales;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TMLLocaleResolver {
	private static TMLLocaleResolver instance;
	private Map<String, Locale> localeMap;
	
	private TMLLocaleResolver() {
		localeMap = new ConcurrentHashMap<String, Locale>();
		localeMap.put("en", SupportedLocales.ENGLISH_GB);
		localeMap.put("fr", SupportedLocales.FRENCH_FR);
		localeMap.put("de", SupportedLocales.GERMAN_DE);
		localeMap.put("es", SupportedLocales.SPANISH_ES);
		localeMap.put("pt", SupportedLocales.PORTUGUESE_PT);
		localeMap.put("us", SupportedLocales.ENGLISH_US);
	}
	private static TMLLocaleResolver getInstance() {
		if (instance == null)
			instance = new TMLLocaleResolver();
		return instance;
	}
	
	public static boolean isResolvable(String input) {
		return (input != null) && getInstance().localeMap.containsKey(input);
	}
	public static Locale resolveLocale(String input) {
		Locale result = null; 
		if (input == null)
			log.error("Null input locale text provided");
		else {
			result = getInstance().localeMap.get(input);
			if (result == null)
				log.error("No locale resolved for input : "+input);
		}
		return result;
	}
}
