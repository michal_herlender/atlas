package org.trescal.cwms.core.referential.dto;

import java.util.HashMap;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TMLSubFamilyDTO {
	
	private int TMLID;
	private int FamilyID;
	private boolean IsEnabled;
	private boolean IsUnderLevelRestricted;
	private boolean IsStructureDefined;
	private boolean IsSubmittedToSchedule;
	private String DefaultName;
	private String DefaultDescription;
	private int Typology;
	private String ExpertGroup;
	private HashMap<String, String> RegionalNames;
	private HashMap<String, String> RegionalDescriptions;
	
	public int getTMLID() {
		return TMLID;
	}
	public void setTMLID(int tMLID) {
		TMLID = tMLID;
	}
	public int getFamilyID() {
		return FamilyID;
	}
	public void setFamilyID(int familyID) {
		FamilyID = familyID;
	}
	public boolean isIsEnabled() {
		return IsEnabled;
	}
	public void setIsEnabled(boolean isEnabled) {
		IsEnabled = isEnabled;
	}
	public boolean isIsUnderLevelRestricted() {
		return IsUnderLevelRestricted;
	}
	public void setIsUnderLevelRestricted(boolean isUnderLevelRestricted) {
		IsUnderLevelRestricted = isUnderLevelRestricted;
	}
	public boolean isIsStructureDefined() {
		return IsStructureDefined;
	}
	public void setIsStructureDefined(boolean isStructureDefined) {
		IsStructureDefined = isStructureDefined;
	}
	public boolean isIsSubmittedToSchedule() {
		return IsSubmittedToSchedule;
	}
	public void setIsSubmittedToSchedule(boolean isSubmittedToSchedule) {
		IsSubmittedToSchedule = isSubmittedToSchedule;
	}
	public String getDefaultName() {
		return DefaultName;
	}
	public void setDefaultName(String defaultName) {
		DefaultName = defaultName;
	}
	public String getDefaultDescription() {
		return DefaultDescription;
	}
	public void setDefaultDescription(String defaultDescription) {
		DefaultDescription = defaultDescription;
	}
	public int getTypology() {
		return Typology;
	}
	public void setTypology(int typology) {
		Typology = typology;
	}
	public String getExpertGroup() {
		return ExpertGroup;
	}
	public void setExpertGroup(String expertGroup) {
		ExpertGroup = expertGroup;
	}
	public HashMap<String, String> getRegionalNames() {
		return RegionalNames;
	}
	public void setRegionalNames(HashMap<String, String> regionalNames) {
		RegionalNames = regionalNames;
	}
	public HashMap<String, String> getRegionalDescriptions() {
		return RegionalDescriptions;
	}
	public void setRegionalDescriptions(HashMap<String, String> regionalDescriptions) {
		RegionalDescriptions = regionalDescriptions;
	}   
	
	public TMLSubFamilyDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public TMLSubFamilyDTO(Node node) {
		NamedNodeMap nodeAttributes = node.getAttributes();
		this.setTMLID(Integer.parseInt(nodeAttributes.getNamedItem("ID").getNodeValue()));
		this.setFamilyID(Integer.parseInt(nodeAttributes.getNamedItem("Family").getNodeValue()));
		this.setDefaultName(nodeAttributes.getNamedItem("DefaultName").getNodeValue());
		this.setIsEnabled(nodeAttributes.getNamedItem("IsEnabled").getNodeValue().equals("1"));
		this.setIsSubmittedToSchedule(nodeAttributes.getNamedItem("IsSubmittedToSchedule").getNodeValue().equals("1"));
		this.setIsUnderLevelRestricted(nodeAttributes.getNamedItem("IsUnderLevelRestricted").getNodeValue().equals("1"));
		this.setIsStructureDefined(nodeAttributes.getNamedItem("IsStructureDefined").getNodeValue().equals("1"));
		this.setTypology(nodeAttributes.getNamedItem("Typology").getNodeValue().equals(null) ? 0 : Integer.parseInt(nodeAttributes.getNamedItem("Typology").getNodeValue()));

		HashMap<String, String> regionalNames = new HashMap<String, String>();
		HashMap<String, String> regionalDescriptions = new HashMap<String, String>();
		
		NodeList regionals = ((Element)node).getChildNodes();
		
		for (int i = 0; i < regionals.getLength(); i++) 
		{
			Node regional = regionals.item(i);
			if (regional != null && regional.getNodeType() != Node.TEXT_NODE)
			{
				NamedNodeMap regionalAttributes = regional.getAttributes();
				
				String language = regionalAttributes.getNamedItem("ID").getNodeValue();
				String name = regionalAttributes.getNamedItem("Name") != null ? regionalAttributes.getNamedItem("Name").getNodeValue() : "";
				String description = regionalAttributes.getNamedItem("Description") != null ? regionalAttributes.getNamedItem("Description").getNodeValue() : "";
				
				if (!name.equals(null) && !name.equals(""))
				{
					regionalNames.put(language, name);
				}
				if (!description.equals(null) && !description.equals(""))
				{
					regionalDescriptions.put(language, description);
				}
			}
		}
		
		this.setRegionalNames(regionalNames);
		this.setRegionalDescriptions(regionalDescriptions);
	}
}
