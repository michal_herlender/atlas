package org.trescal.cwms.core.referential.dto;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class TMLManufacturerDTO {

	private int TMLID;
	private String Name;
	private String Remarks;
	private boolean IsEnabled;
	
	public int getTMLID() {
		return TMLID;
	}
	public void setTMLID(int tMLID) {
		TMLID = tMLID;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getRemarks() {
		return Remarks;
	}
	public void setRemarks(String remarks) {
		Remarks = remarks;
	}
	public boolean isIsEnabled() {
		return IsEnabled;
	}
	public void setIsEnabled(boolean isEnabled) {
		IsEnabled = isEnabled;
	}
	
	public TMLManufacturerDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public TMLManufacturerDTO(Node node) {
		NamedNodeMap nodeAttributes = node.getAttributes();
		this.setTMLID(Integer.parseInt(nodeAttributes.getNamedItem("ID").getNodeValue()));
		this.setName(nodeAttributes.getNamedItem("Name").getNodeValue());
		this.setIsEnabled(nodeAttributes.getNamedItem("IsEnabled").getNodeValue().equals("1"));
	}
}