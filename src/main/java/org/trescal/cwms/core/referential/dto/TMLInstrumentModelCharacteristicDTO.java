package org.trescal.cwms.core.referential.dto;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.MessageFormat;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class TMLInstrumentModelCharacteristicDTO {
	private String VarChar01;
	private String VarChar02;
	private String VarChar03;
	private String VarChar04;
	private String VarChar05;
	private String VarChar06;
	private String VarChar07;
	private String VarChar08;
	private String VarChar09;
	private String VarChar10;
	private String VarChar11;
	private String VarChar12;
	private String VarChar13;
	private String VarChar14;
	private String VarChar15;
	private String VarChar16;
	private String VarChar17;
	private String VarChar18;
	private String VarChar19;
	private String VarChar20;
	private String VarChar21;
	private String VarChar22;
	private String VarChar23;
	private String VarChar24;
	private String VarChar25;
	private Double Float01;
	private Double Float02;
	private Double Float03;
	private Double Float04;
	private Double Float05;
	private Double Float06;
	private Double Float07;
	private Double Float08;
	private Double Float09;
	private Double Float10;
	private Double Float11;
	private Double Float12;
	private Double Float13;
	private Double Float14;
	private Double Float15;
	private Double Float16;
	private Double Float17;
	private Double Float18;
	private Double Float19;
	private Double Float20;
	private Double Float21;
	private Double Float22;
	private Double Float23;
	private Double Float24;
	private Double Float25;
	private Boolean Bit01;
	private Boolean Bit02;
	private Boolean Bit03;
	private Boolean Bit04;
	private Boolean Bit05;
	private Boolean Bit06;
	private Boolean Bit07;
	private Boolean Bit08;
	private Boolean Bit09;
	private Boolean Bit10;
	private String Unit01;
	private String Unit02;
	private String Unit03;
	private String Unit04;
	private String Unit05;
	private String Unit06;
	private String Unit07;
	private String Unit08;
	private String Unit09;
	private String Unit10;
	private String Unit11;
	private String Unit12;
	private String Unit13;
	private String Unit14;
	private String Unit15;
	private String Unit16;
	private String Unit17;
	private String Unit18;
	private String Unit19;
	private String Unit20;
	private String Unit21;
	private String Unit22;
	private String Unit23;
	private String Unit24;
	private String Unit25;
	
	public String getVarChar01() {
		return VarChar01;
	}
	public String getVarChar02() {
		return VarChar02;
	}
	public String getVarChar03() {
		return VarChar03;
	}
	public String getVarChar04() {
		return VarChar04;
	}
	public String getVarChar05() {
		return VarChar05;
	}
	public String getVarChar06() {
		return VarChar06;
	}
	public String getVarChar07() {
		return VarChar07;
	}
	public String getVarChar08() {
		return VarChar08;
	}
	public String getVarChar09() {
		return VarChar09;
	}
	public String getVarChar10() {
		return VarChar10;
	}
	public String getVarChar11() {
		return VarChar11;
	}
	public String getVarChar12() {
		return VarChar12;
	}
	public String getVarChar13() {
		return VarChar13;
	}
	public String getVarChar14() {
		return VarChar14;
	}
	public String getVarChar15() {
		return VarChar15;
	}
	public String getVarChar16() {
		return VarChar16;
	}
	public String getVarChar17() {
		return VarChar17;
	}
	public String getVarChar18() {
		return VarChar18;
	}
	public String getVarChar19() {
		return VarChar19;
	}
	public String getVarChar20() {
		return VarChar20;
	}
	public String getVarChar21() {
		return VarChar21;
	}
	public String getVarChar22() {
		return VarChar22;
	}
	public String getVarChar23() {
		return VarChar23;
	}
	public String getVarChar24() {
		return VarChar24;
	}
	public String getVarChar25() {
		return VarChar25;
	}
	public Double getFloat01() {
		return Float01;
	}
	public Double getFloat02() {
		return Float02;
	}
	public Double getFloat03() {
		return Float03;
	}
	public Double getFloat04() {
		return Float04;
	}
	public Double getFloat05() {
		return Float05;
	}
	public Double getFloat06() {
		return Float06;
	}
	public Double getFloat07() {
		return Float07;
	}
	public Double getFloat08() {
		return Float08;
	}
	public Double getFloat09() {
		return Float09;
	}
	public Double getFloat10() {
		return Float10;
	}
	public Double getFloat11() {
		return Float11;
	}
	public Double getFloat12() {
		return Float12;
	}
	public Double getFloat13() {
		return Float13;
	}
	public Double getFloat14() {
		return Float14;
	}
	public Double getFloat15() {
		return Float15;
	}
	public Double getFloat16() {
		return Float16;
	}
	public Double getFloat17() {
		return Float17;
	}
	public Double getFloat18() {
		return Float18;
	}
	public Double getFloat19() {
		return Float19;
	}
	public Double getFloat20() {
		return Float20;
	}
	public Double getFloat21() {
		return Float21;
	}
	public Double getFloat22() {
		return Float22;
	}
	public Double getFloat23() {
		return Float23;
	}
	public Double getFloat24() {
		return Float24;
	}
	public Double getFloat25() {
		return Float25;
	}
	public Boolean isBit01() {
		return Bit01;
	}
	public Boolean isBit02() {
		return Bit02;
	}
	public Boolean isBit03() {
		return Bit03;
	}
	public Boolean isBit04() {
		return Bit04;
	}
	public Boolean isBit05() {
		return Bit05;
	}
	public Boolean isBit06() {
		return Bit06;
	}
	public Boolean isBit07() {
		return Bit07;
	}
	public Boolean isBit08() {
		return Bit08;
	}
	public Boolean isBit09() {
		return Bit09;
	}
	public Boolean isBit10() {
		return Bit10;
	}
	public String getUnit01() {
		return Unit01;
	}
	public String getUnit02() {
		return Unit02;
	}
	public String getUnit03() {
		return Unit03;
	}
	public String getUnit04() {
		return Unit04;
	}
	public String getUnit05() {
		return Unit05;
	}
	public String getUnit06() {
		return Unit06;
	}
	public String getUnit07() {
		return Unit07;
	}
	public String getUnit08() {
		return Unit08;
	}
	public String getUnit09() {
		return Unit09;
	}
	public String getUnit10() {
		return Unit10;
	}
	public String getUnit11() {
		return Unit11;
	}
	public String getUnit12() {
		return Unit12;
	}
	public String getUnit13() {
		return Unit13;
	}
	public String getUnit14() {
		return Unit14;
	}
	public String getUnit15() {
		return Unit15;
	}
	public String getUnit16() {
		return Unit16;
	}
	public String getUnit17() {
		return Unit17;
	}
	public String getUnit18() {
		return Unit18;
	}
	public String getUnit19() {
		return Unit19;
	}
	public String getUnit20() {
		return Unit20;
	}
	public String getUnit21() {
		return Unit21;
	}
	public String getUnit22() {
		return Unit22;
	}
	public String getUnit23() {
		return Unit23;
	}
	public String getUnit24() {
		return Unit24;
	}
	public String getUnit25() {
		return Unit25;
	}
	public void setVarChar01(String varChar01) {
		VarChar01 = varChar01;
	}
	public void setVarChar02(String varChar02) {
		VarChar02 = varChar02;
	}
	public void setVarChar03(String varChar03) {
		VarChar03 = varChar03;
	}
	public void setVarChar04(String varChar04) {
		VarChar04 = varChar04;
	}
	public void setVarChar05(String varChar05) {
		VarChar05 = varChar05;
	}
	public void setVarChar06(String varChar06) {
		VarChar06 = varChar06;
	}
	public void setVarChar07(String varChar07) {
		VarChar07 = varChar07;
	}
	public void setVarChar08(String varChar08) {
		VarChar08 = varChar08;
	}
	public void setVarChar09(String varChar09) {
		VarChar09 = varChar09;
	}
	public void setVarChar10(String varChar10) {
		VarChar10 = varChar10;
	}
	public void setVarChar11(String varChar11) {
		VarChar11 = varChar11;
	}
	public void setVarChar12(String varChar12) {
		VarChar12 = varChar12;
	}
	public void setVarChar13(String varChar13) {
		VarChar13 = varChar13;
	}
	public void setVarChar14(String varChar14) {
		VarChar14 = varChar14;
	}
	public void setVarChar15(String varChar15) {
		VarChar15 = varChar15;
	}
	public void setVarChar16(String varChar16) {
		VarChar16 = varChar16;
	}
	public void setVarChar17(String varChar17) {
		VarChar17 = varChar17;
	}
	public void setVarChar18(String varChar18) {
		VarChar18 = varChar18;
	}
	public void setVarChar19(String varChar19) {
		VarChar19 = varChar19;
	}
	public void setVarChar20(String varChar20) {
		VarChar20 = varChar20;
	}
	public void setVarChar21(String varChar21) {
		VarChar21 = varChar21;
	}
	public void setVarChar22(String varChar22) {
		VarChar22 = varChar22;
	}
	public void setVarChar23(String varChar23) {
		VarChar23 = varChar23;
	}
	public void setVarChar24(String varChar24) {
		VarChar24 = varChar24;
	}
	public void setVarChar25(String varChar25) {
		VarChar25 = varChar25;
	}
	public void setFloat01(Double float01) {
		Float01 = float01;
	}
	public void setFloat02(Double float02) {
		Float02 = float02;
	}
	public void setFloat03(Double float03) {
		Float03 = float03;
	}
	public void setFloat04(Double float04) {
		Float04 = float04;
	}
	public void setFloat05(Double float05) {
		Float05 = float05;
	}
	public void setFloat06(Double float06) {
		Float06 = float06;
	}
	public void setFloat07(Double float07) {
		Float07 = float07;
	}
	public void setFloat08(Double float08) {
		Float08 = float08;
	}
	public void setFloat09(Double float09) {
		Float09 = float09;
	}
	public void setFloat10(Double float10) {
		Float10 = float10;
	}
	public void setFloat11(Double float11) {
		Float11 = float11;
	}
	public void setFloat12(Double float12) {
		Float12 = float12;
	}
	public void setFloat13(Double float13) {
		Float13 = float13;
	}
	public void setFloat14(Double float14) {
		Float14 = float14;
	}
	public void setFloat15(Double float15) {
		Float15 = float15;
	}
	public void setFloat16(Double float16) {
		Float16 = float16;
	}
	public void setFloat17(Double float17) {
		Float17 = float17;
	}
	public void setFloat18(Double float18) {
		Float18 = float18;
	}
	public void setFloat19(Double float19) {
		Float19 = float19;
	}
	public void setFloat20(Double float20) {
		Float20 = float20;
	}
	public void setFloat21(Double float21) {
		Float21 = float21;
	}
	public void setFloat22(Double float22) {
		Float22 = float22;
	}
	public void setFloat23(Double float23) {
		Float23 = float23;
	}
	public void setFloat24(Double float24) {
		Float24 = float24;
	}
	public void setFloat25(Double float25) {
		Float25 = float25;
	}
	public void setBit01(Boolean bit01) {
		Bit01 = bit01;
	}
	public void setBit02(Boolean bit02) {
		Bit02 = bit02;
	}
	public void setBit03(Boolean bit03) {
		Bit03 = bit03;
	}
	public void setBit04(Boolean bit04) {
		Bit04 = bit04;
	}
	public void setBit05(Boolean bit05) {
		Bit05 = bit05;
	}
	public void setBit06(Boolean bit06) {
		Bit06 = bit06;
	}
	public void setBit07(Boolean bit07) {
		Bit07 = bit07;
	}
	public void setBit08(Boolean bit08) {
		Bit08 = bit08;
	}
	public void setBit09(Boolean bit09) {
		Bit09 = bit09;
	}
	public void setBit10(Boolean bit10) {
		Bit10 = bit10;
	}
	public void setUnit01(String unit01) {
		Unit01 = unit01;
	}
	public void setUnit02(String unit02) {
		Unit02 = unit02;
	}
	public void setUnit03(String unit03) {
		Unit03 = unit03;
	}
	public void setUnit04(String unit04) {
		Unit04 = unit04;
	}
	public void setUnit05(String unit05) {
		Unit05 = unit05;
	}
	public void setUnit06(String unit06) {
		Unit06 = unit06;
	}
	public void setUnit07(String unit07) {
		Unit07 = unit07;
	}
	public void setUnit08(String unit08) {
		Unit08 = unit08;
	}
	public void setUnit09(String unit09) {
		Unit09 = unit09;
	}
	public void setUnit10(String unit10) {
		Unit10 = unit10;
	}
	public void setUnit11(String unit11) {
		Unit11 = unit11;
	}
	public void setUnit12(String unit12) {
		Unit12 = unit12;
	}
	public void setUnit13(String unit13) {
		Unit13 = unit13;
	}
	public void setUnit14(String unit14) {
		Unit14 = unit14;
	}
	public void setUnit15(String unit15) {
		Unit15 = unit15;
	}
	public void setUnit16(String unit16) {
		Unit16 = unit16;
	}
	public void setUnit17(String unit17) {
		Unit17 = unit17;
	}
	public void setUnit18(String unit18) {
		Unit18 = unit18;
	}
	public void setUnit19(String unit19) {
		Unit19 = unit19;
	}
	public void setUnit20(String unit20) {
		Unit20 = unit20;
	}
	public void setUnit21(String unit21) {
		Unit21 = unit21;
	}
	public void setUnit22(String unit22) {
		Unit22 = unit22;
	}
	public void setUnit23(String unit23) {
		Unit23 = unit23;
	}
	public void setUnit24(String unit24) {
		Unit24 = unit24;
	}
	public void setUnit25(String unit25) {
		Unit25 = unit25;
	}
	
	public TMLInstrumentModelCharacteristicDTO() {
		
	}

	public TMLInstrumentModelCharacteristicDTO(Node node) throws Exception {
		NamedNodeMap nodeAttributes = node.getAttributes();
		
		for (int i = 0; i < this.getClass().getDeclaredFields().length; i++) 
		{
			Field field = this.getClass().getDeclaredFields()[i];
			if (nodeAttributes.getNamedItem(field.getName()) != null)
			{
				try
				{
					if (field.getName().startsWith("VarChar"))
					{
						Method method = this.getClass().getDeclaredMethod(MessageFormat.format("set{0}", field.getName()),String.class);
						method.invoke(this, nodeAttributes.getNamedItem(field.getName()).getNodeValue());
					}
					if (field.getName().startsWith("Float"))
					{
						Method method = this.getClass().getDeclaredMethod(MessageFormat.format("set{0}", field.getName()),Double.class);
						method.invoke(this, Double.valueOf(nodeAttributes.getNamedItem(field.getName()).getNodeValue()));
					}
					if (field.getName().startsWith("Unit"))
					{
						Method method = this.getClass().getDeclaredMethod(MessageFormat.format("set{0}", field.getName()),String.class);
						method.invoke(this, nodeAttributes.getNamedItem(field.getName()).getNodeValue());
					}
					if (field.getName().startsWith("Bit"))
					{
						Method method = this.getClass().getDeclaredMethod(MessageFormat.format("set{0}", field.getName()),Boolean.class);
						method.invoke(this, nodeAttributes.getNamedItem(field.getName()).getNodeValue().equals("1"));
					}
				}
				catch (Exception ex)
				{
					throw ex;
				}
			}
		}
	}
}
