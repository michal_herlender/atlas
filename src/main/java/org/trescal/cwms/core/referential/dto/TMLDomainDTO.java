package org.trescal.cwms.core.referential.dto;

import java.util.HashMap;

import org.w3c.dom.*;

public class TMLDomainDTO {
	
	private int TMLID;
	private String DefaultName;
	private String DefaultDescription;
	private HashMap<String, String> RegionalNames;
	private HashMap<String, String> RegionalDescriptions;
	private boolean IsEnabled;
	
	public int getTMLID() {
		return TMLID;
	}
	public void setTMLID(int tMLID) {
		TMLID = tMLID;
	}
	public String getDefaultName() {
		return DefaultName;
	}
	public void setDefaultName(String defaultName) {
		DefaultName = defaultName;
	}
	public String getDefaultDescription() {
		return DefaultDescription;
	}
	public void setDefaultDescription(String defaultDescription) {
		DefaultDescription = defaultDescription;
	}
	public HashMap<String, String> getRegionalNames() {
		return RegionalNames;
	}
	public void setRegionalNames(HashMap<String, String> regionalNames) {
		RegionalNames = regionalNames;
	}
	public HashMap<String, String> getRegionalDescriptions() {
		return RegionalDescriptions;
	}
	public void setRegionalDescriptions(HashMap<String, String> regionalDescriptions) {
		RegionalDescriptions = regionalDescriptions;
	}
	public boolean isIsEnabled() {
		return IsEnabled;
	}
	public void setIsEnabled(boolean isEnabled) {
		IsEnabled = isEnabled;
	}
	
	public TMLDomainDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public TMLDomainDTO(Node node)
	{
		NamedNodeMap nodeAttributes = node.getAttributes();
		this.setTMLID(Integer.parseInt(nodeAttributes.getNamedItem("ID").getNodeValue()));
		this.setDefaultName(nodeAttributes.getNamedItem("DefaultName").getNodeValue());
		this.setIsEnabled(nodeAttributes.getNamedItem("IsEnabled").getNodeValue().equals("1"));

		HashMap<String, String> regionalNames = new HashMap<String, String>();
		HashMap<String, String> regionalDescriptions = new HashMap<String, String>();
		
		NodeList regionals = ((Element)node).getChildNodes();
		
		for (int i = 0; i < regionals.getLength(); i++) 
		{
			Node regional = regionals.item(i);
			if (regional != null && regional.getNodeType() != Node.TEXT_NODE)
			{
				NamedNodeMap regionalAttributes = regional.getAttributes();
				
				String language = regionalAttributes.getNamedItem("ID").getNodeValue();
				String name = regionalAttributes.getNamedItem("Name") != null ? regionalAttributes.getNamedItem("Name").getNodeValue() : "";
				String description = regionalAttributes.getNamedItem("Description") != null ? regionalAttributes.getNamedItem("Description").getNodeValue() : "";
				
				if (!name.equals(null) && !name.equals(""))
				{
					regionalNames.put(language, name);
				}
				if (!description.equals(null) && !description.equals(""))
				{
					regionalDescriptions.put(language, description);
				}
			}
		}
		
		this.setRegionalNames(regionalNames);
		this.setRegionalDescriptions(regionalDescriptions);
	}
}
