package org.trescal.cwms.core.referential.dto;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.trescal.cwms.core.referential.dto.TMLChoiceDTO;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TMLCharacteristicDTO {
	private int TMLID;
	private String DefaultName;
	private String DefaultDescription;
	private String InternalName;
	private int FieldType;
	private int SubFamily;
	private int OrderNo;
	private HashMap<String, String> RegionalNames;
	private HashMap<String, String> RegionalDescriptions;
	private boolean IsEnabled;
	private boolean IsRequired;
	private String MetaData;
	private String Unit;
	private String UnitList;
	private String ShortName;
	private Set<TMLChoiceDTO> choices;
	
	public int getTMLID() {
		return TMLID;
	}
	public void setTMLID(int tMLID) {
		TMLID = tMLID;
	}
	public String getDefaultName() {
		return DefaultName;
	}
	public void setDefaultName(String defaultName) {
		DefaultName = defaultName;
	}
	public String getDefaultDescription() {
		return DefaultDescription;
	}
	public void setDefaultDescription(String defaultDescription) {
		DefaultDescription = defaultDescription;
	}
	public String getInternalName() {
		return InternalName;
	}
	public void setInternalName(String internalName) {
		InternalName = internalName;
	}
	public int getFieldType() {
		return FieldType;
	}
	public void setFieldType(int fieldType) {
		FieldType = fieldType;
	}
	public int getSubFamily() {
		return SubFamily;
	}
	public void setSubFamily(int subFamily) {
		SubFamily = subFamily;
	}
	public int getOrderNo() {
		return OrderNo;
	}
	public void setOrderNo(int orderNo) {
		OrderNo = orderNo;
	}
	public HashMap<String, String> getRegionalNames() {
		return RegionalNames;
	}
	public void setRegionalNames(HashMap<String, String> regionalNames) {
		RegionalNames = regionalNames;
	}
	public HashMap<String, String> getRegionalDescriptions() {
		return RegionalDescriptions;
	}
	public void setRegionalDescriptions(HashMap<String, String> regionalDescriptions) {
		RegionalDescriptions = regionalDescriptions;
	}
	public boolean isIsEnabled() {
		return IsEnabled;
	}
	public void setIsEnabled(boolean isEnabled) {
		IsEnabled = isEnabled;
	}
	public boolean isIsRequired() {
		return IsRequired;
	}
	public String getMetaData() {
		return MetaData;
	}
	public String getUnit() {
		return Unit;
	}
	public String getUnitList() {
		return UnitList;
	}
	public void setIsRequired(boolean isRequired) {
		IsRequired = isRequired;
	}
	public void setMetaData(String metaData) {
		MetaData = metaData;
	}
	public void setUnit(String unit) {
		Unit = unit;
	}
	public void setUnitList(String unitList) {
		UnitList = unitList;
	}
	public String getShortName() {
		return ShortName;
	}
	public void setShortName(String shortName) {
		ShortName = shortName;
	}
	public Set<TMLChoiceDTO> getChoices() {
		return choices;
	}
	public void setChoices(Set<TMLChoiceDTO> choices) {
		this.choices = choices;
	}	
	public TMLCharacteristicDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public TMLCharacteristicDTO(Node node)
	{
		NamedNodeMap nodeAttributes = node.getAttributes();
		this.setTMLID(Integer.parseInt(nodeAttributes.getNamedItem("ID").getNodeValue()));
		this.setDefaultName(nodeAttributes.getNamedItem("DefaultName").getNodeValue());
		this.setInternalName(nodeAttributes.getNamedItem("InternalName").getNodeValue());
		this.setFieldType(Integer.parseInt(nodeAttributes.getNamedItem("FieldType").getNodeValue()));
		this.setSubFamily(Integer.parseInt(nodeAttributes.getNamedItem("SubFamily").getNodeValue()));
		this.setOrderNo(Integer.parseInt(nodeAttributes.getNamedItem("OrderNo").getNodeValue()));
		this.setIsEnabled(nodeAttributes.getNamedItem("IsEnabled").getNodeValue().equals("1"));
		this.setIsRequired(nodeAttributes.getNamedItem("IsRequired").getNodeValue().equals("1"));
		this.setMetaData(nodeAttributes.getNamedItem("MetaData") != null ? nodeAttributes.getNamedItem("MetaData").getNodeValue() : "");
		this.setUnit(nodeAttributes.getNamedItem("Unit") != null ? nodeAttributes.getNamedItem("Unit").getNodeValue() : "");
		this.setUnitList(nodeAttributes.getNamedItem("UnitList") != null ? nodeAttributes.getNamedItem("UnitList").getNodeValue() : "");
		this.setShortName(nodeAttributes.getNamedItem("ShortName") != null ? nodeAttributes.getNamedItem("ShortName").getNodeValue() : "");
		
		HashMap<String, String> regionalNames = new HashMap<String, String>();
		HashMap<String, String> regionalDescriptions = new HashMap<String, String>();
		Set<TMLChoiceDTO> choices = new HashSet<TMLChoiceDTO>();
		
		NodeList children = ((Element)node).getChildNodes();		
		
		for (int i = 0; i < children.getLength(); i++) 
		{
			Node child = children.item(i);	
			
			if (child != null && child.getNodeType() != Node.TEXT_NODE)
			{
				switch (child.getNodeName())
				{
					case "language":
					{
						NamedNodeMap childAttributes = child.getAttributes();
						
						String language = childAttributes.getNamedItem("ID").getNodeValue();
						String name = childAttributes.getNamedItem("Name") != null ? childAttributes.getNamedItem("Name").getNodeValue() : "";
						String description = childAttributes.getNamedItem("Description") != null ? childAttributes.getNamedItem("Description").getNodeValue() : "";
						
						if (!name.equals(null) && !name.equals(""))
						{
							regionalNames.put(language, name);
						}
						if (!description.equals(null) && !description.equals(""))
						{
							regionalDescriptions.put(language, description);
						}
						
						break;
					}
					case "MetaData":
					{
						NodeList metadata = ((Element)child).getChildNodes();
						
						if (metadata != null)
						{
							Node meta = metadata.item(0);
							if (meta != null && meta.getChildNodes() != null)
							{
//								try
//								{
//									StringWriter sw = new StringWriter(); 
//									Transformer serializer = TransformerFactory.newInstance().newTransformer(); 
//									serializer.transform(new DOMSource(meta), new StreamResult(sw)); 
//									this.setMetaData(sw != null ? sw.toString() : "");
//								}
//								catch (Exception ex)
//								{
//									
//								}
								 
								Node TMLChoices = meta.getChildNodes().item(0);
								
								if (TMLChoices != null && TMLChoices.getChildNodes() != null)
								{
									for (int j = 0; j < TMLChoices.getChildNodes().getLength(); j++) 
									{
										Node choice = TMLChoices.getChildNodes().item(j);
										
										if (choice != null && choice.getNodeType() != Node.TEXT_NODE)
										{
											NamedNodeMap choiceAttributes = choice.getAttributes();
											TMLChoiceDTO choiceDTO = new TMLChoiceDTO();
											choiceDTO.setKey(Integer.parseInt(choiceAttributes.getNamedItem("key").getNodeValue()));
											choiceDTO.setOrderNo(Integer.parseInt(choiceAttributes.getNamedItem("orderno").getNodeValue()));
											
											HashMap<String, String> Values = new HashMap<String, String>();
											
											if (choice.getChildNodes() != null)
											{
												Node values = choice.getChildNodes().item(0);
												
												if (values != null && values.getChildNodes() != null)
												{
													for (int k = 0; k < values.getChildNodes().getLength(); k++) 
													{
														Node value = values.getChildNodes().item(k);
														
														if (value != null && value.getNodeType() != Node.TEXT_NODE)
														{
															NamedNodeMap valueAttributes = value.getAttributes();
															
															String lang = valueAttributes.getNamedItem("lang") != null ? valueAttributes.getNamedItem("lang").getNodeValue() : "";
															String valueName = valueAttributes.getNamedItem("value") != null ? valueAttributes.getNamedItem("value").getNodeValue() : "";
															
															if (!lang.equals(null) && !lang.equals("") && !valueName.equals(null) && !valueName.equals(""))
															{
																Values.put(lang, valueName);
																
																if (lang.equals("en"))
																{
																	choiceDTO.setName(valueName);
																}
															}
														}
													} 
													
													choiceDTO.setValues(Values);
												}
												
												choices.add(choiceDTO);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		this.setChoices(choices);
		this.setRegionalNames(regionalNames);
		this.setRegionalDescriptions(regionalDescriptions);
	}
}
