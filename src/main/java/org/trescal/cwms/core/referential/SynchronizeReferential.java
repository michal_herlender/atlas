package org.trescal.cwms.core.referential;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.zip.GZIPInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.referential.db.TMLHandlerService;
import org.trescal.cwms.core.referential.dto.ProcessFileSummary;
import org.trescal.cwms.core.referential.dto.TMLCharacteristicDTO;
import org.trescal.cwms.core.referential.dto.TMLDomainDTO;
import org.trescal.cwms.core.referential.dto.TMLFamilyDTO;
import org.trescal.cwms.core.referential.dto.TMLInstrumentModelDTO;
import org.trescal.cwms.core.referential.dto.TMLManufacturerDTO;
import org.trescal.cwms.core.referential.dto.TMLOptionDTO;
import org.trescal.cwms.core.referential.dto.TMLSubFamilyDTO;
import org.trescal.cwms.core.referential.dto.TMLUnitDTO;
import org.trescal.cwms.core.system.entity.scheduledtask.db.ScheduledTaskService;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

@Component("SynchronizeTMLDataJob")
public class SynchronizeReferential implements SynchronizeReferentialIf
{
	private final Logger logger = LoggerFactory.getLogger(getClass());

  	@Value("${tml.ftpserver.flagdirectory}")
  	private String flagdirectory;
  	
  	@Value("${tml.ftpserver.flagfilenameok}")
  	private String flagfilenameok;
  
  	@Value("${tml.ftpserver.flagfilenamepok}")
  	private String flagfilenamepok;
  
  	@Value("${tml.ftpserver.encoding}")
  	private String encoding;
  
  	@Value("${tml.ftpserver.extension}")
  	private String extension;

  	@Value("${tml.ftpserver.dateformat}")
  	private String dateformat;
  	
  	@Value("${tml.sftpserver.address}")
  	private String sftp_address;
  	
  	@Value("${tml.sftpserver.login}")
  	private String sftp_login;
  	
  	@Value("${tml.sftpserver.pwd}")
  	private String sftp_pwd;
  	
  	@Value("${tml.sftpserver.port}")
  	private String sftp_port;  	
  	
	@Autowired
	private ScheduledTaskService schTaskServ;

	@Autowired
	private TMLHandlerService tmlHandlerServ;
	
	@Override
	public void synchronizeReferentialData()
	{
		if (this.schTaskServ.scheduledTaskIsActive(this.getClass().getName(), java.lang.Thread.currentThread().getStackTrace()))
		{
			String message = this.importDataFromSftp();

			// report results of task run
			this.schTaskServ.reportOnScheduledTaskRun(this.getClass().getName(), java.lang.Thread.currentThread().getStackTrace(), true, message);
		}
		else
		{
			logger.debug("TML Import job not running: scheduled task cannot be found or is turned off");
		}
	}
	
	@Override
	public String testConnection() {
		StringBuffer result = new StringBuffer();
		JSch jsch=new JSch();  
		
		try		
		{
			// Connect to TML sftp
			String connectionInfo = "Connecting to sftp server : "+this.sftp_address+" on port "+this.sftp_port; 
			logger.info(connectionInfo);
			result.append(connectionInfo);
			result.append("\n");
			
			Session session=jsch.getSession(this.sftp_login, this.sftp_address, Integer.parseInt(this.sftp_port));
			session.setPassword(this.sftp_pwd);
			session.setConfig("StrictHostKeyChecking", "no");
	        session.connect(5000);
			
			Channel channel=session.openChannel("sftp");
			channel.connect(5000);
			ChannelSftp c=(ChannelSftp)channel;
			
			String pwd = c.pwd();
			
			String directoryInfo = "Present working directory (remote) : "+pwd;
			logger.info(directoryInfo);
			result.append(directoryInfo);
			result.append("\n");
		}
		catch (Exception ex)
		{
			logger.error("Exception connecting ", ex);
			result.append(ex.getMessage());
		}
		
		return result.toString();
	}
	
	private String importDataFromSftp()
	{
		JSch jsch=new JSch();  
		
		try		
		{
			// Connect to TML sftp
			logger.info("Connecting to sftp server : "+this.sftp_address+" on port "+this.sftp_port);
			Session session=jsch.getSession(this.sftp_login, this.sftp_address, Integer.parseInt(this.sftp_port));
			session.setPassword(this.sftp_pwd);
			session.setConfig("StrictHostKeyChecking", "no");
	        session.connect(15000);
			
			Channel channel=session.openChannel("sftp");
			channel.connect(15000);
			ChannelSftp c=(ChannelSftp)channel;
			
			String pwd = c.pwd();
			logger.debug("Present working directory (remote) : "+pwd);
			
			// Gets the list of all directores in the TML directory
			List<String> directories = Arrays.asList(this.sftp_listDirectories(c, "."));
			logger.info(MessageFormat.format("{0} directories found",directories.equals(null) ? 0 : directories.size()));
			
			// Create the CWMS flag directory if not exists
			if (!directories.contains(this.flagdirectory))
			{
				this.sftp_createDirectory(c, this.flagdirectory);
			}

			// Gets the list of all files in the TML directory
			List<String> files = Arrays.asList(this.sftp_listFiles(c,"."));
			logger.info(MessageFormat.format("{0} files found",files.equals(null) ? 0 : files.size()));
			
			// Input files are named such that they could be sorted and then be in creation order
			List<String> sortedFiles = new ArrayList<>(files); 
			Collections.sort(sortedFiles);
			
			// Gets the list of all flag files in the CWMS flag directory
			List<String> flags = Arrays.asList(this.sftp_listFiles(c,this.flagdirectory));
			logger.info(MessageFormat.format("{0} flags found",flags.equals(null) ? 0 : flags.size()));
			
			StringBuffer message = new StringBuffer();
			boolean isSuccess = true;
			
			// For each file found
			for (String file : sortedFiles) 
			{
				if (!file.equals(null))
				{
					if (file.length() > 5)
					{
						// Only zxml extension files
						String filename = file.substring(0,file.length() - 5);
						String ext = file.replace(filename, "");
						
						if (ext.equals(extension))
						{
							logger.debug(MessageFormat.format("Checking {0}",file));
		                    
							// Build the OK flag file name
			                String MatchingFlagName = MessageFormat.format(this.flagfilenameok, filename);
			                
			                // Check if the file has already been successfully processed
			                if (!flags.contains(MatchingFlagName))
			                {
								logger.info(MessageFormat.format("Downloading and processing {0}",file));
		                        
								// Get the file from TML FTP
				                InputStream is = this.sftp_getCompressedFile(c, file);
				        		
						        try 
						        {
						        	// Parse the file into a XML DOM Document
						        	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					                
									Document doc = dBuilder.parse(is);
									doc.getDocumentElement().normalize();         
									
									// Process the file
									ProcessFileSummary ProcessSummary = processFile(doc);		
									
									if (ProcessSummary.isIsSuccess())
									{
										// Create a successed flag file
										logger.debug("Uploading the completion flag file");
										sftp_createTextFile(c, MessageFormat.format("{0}/{1}",this.flagdirectory, MatchingFlagName), ProcessSummary.ToXml(), MatchingFlagName);
									}
									else
									{
										// Create a failed flag file
										logger.debug("Uploading the failure report file");
										String failureFileName = MessageFormat.format(this.flagfilenamepok, filename,new SimpleDateFormat(dateformat).format(new Date()));
										sftp_createTextFile(c, MessageFormat.format("{0}/{1}",this.flagdirectory, failureFileName), ProcessSummary.ToXml(), failureFileName);
			                            throw new Exception(MessageFormat.format("The file {0} could not be processed. Reason : {1}", file, ProcessSummary.getException()));
									}
						        } 
						        catch (Exception ex) 
						        {
						        	isSuccess = false;
						        	logger.error("Exception processing file "+file, ex);
						        	if (message.length() > 0)
						        		message.append(" / ");
						        	message.append(ex.getMessage());
						        }
						        finally
						        {
						        	is.close();
						        }
			                }
			                else
			                {
								logger.debug(": Already processed");
			                }
						}
					}					
				}
			}
			
			// Disconnect from FTP
			c.disconnect();
			session.disconnect();
			
			logger.info("Import completed succefully");
			return isSuccess ? "Import completed succefully" : message.toString();
		}
		catch (Exception ex)
		{
			logger.error("Exception importing data ", ex);
			return ex.getMessage();
		}
	}
	
	public ProcessFileSummary processFile(Document doc)
	{
		// Instantiate the response
		ProcessFileSummary response = new ProcessFileSummary();

        String CurrentStep = "None";
        
        try
        {
        	// Loop all child nodes
			NodeList nodeList = doc.getDocumentElement().getChildNodes();
			
			if (nodeList != null)
			{
				String lastNodeName = null;
				for (int i = 0; i < nodeList.getLength(); i++) 
				{
					Node node = nodeList.item(i);
					
					if (node != null)
					{
						if (!doc.getDocumentElement().getNodeName().equals(node.getNodeName()) && node.getNodeType() != Node.TEXT_NODE)
						{
							lastNodeName = node.getNodeName().equals(lastNodeName) ? lastNodeName : node.getNodeName();
							
							// Check for domains
							if (node.getNodeName().equals("domains"))
							{
				                CurrentStep = "Domains";
								NodeList domainList = ((Element)nodeList.item(i)).getChildNodes();
								
								for (int j = 0; j < domainList.getLength(); j++) 
								{
									Node domain = domainList.item(j);
									if (domain != null && domain.getNodeType() != Node.TEXT_NODE)
									{
										// Report ADD/UPDATE/DELETE
										tmlHandlerServ.ReportDomain(new TMLDomainDTO(domain));
										response.setDomains(j+1);
									}
								}
							}

							// Check for families
							if (node.getNodeName().equals("families"))
							{
								NodeList familyList = ((Element)nodeList.item(i)).getChildNodes();
	
				                CurrentStep = "Families";
								for (int j = 0; j < familyList.getLength(); j++) 
								{
									Node family = familyList.item(j);
									if (family != null && family.getNodeType() != Node.TEXT_NODE)
									{
										// Report ADD/UPDATE/DELETE
										tmlHandlerServ.ReportFamily(new TMLFamilyDTO(family));
										response.setFamilies(j+1);
									}
								}
							}

							// Check for subfamilies
							if (node.getNodeName().equals("subfamilies"))
							{
								NodeList subfamilyList = ((Element)nodeList.item(i)).getChildNodes();
	
				                CurrentStep = "SubFamilies";
								for (int j = 0; j < subfamilyList.getLength(); j++) 
								{
									Node subfamily = subfamilyList.item(j);
									if (subfamily != null && subfamily.getNodeType() != Node.TEXT_NODE)
									{
										// Report ADD/UPDATE/DELETE
										tmlHandlerServ.ReportSubFamily(new TMLSubFamilyDTO(subfamily));
										response.setSubFamilies(j+1);
									}
								}
							}

							// Check for manufacturers
							if (node.getNodeName().equals("manufacturers"))
							{
								NodeList manufacturerList = ((Element)nodeList.item(i)).getChildNodes();
	
				                CurrentStep = "Manufacturers";
								for (int j = 0; j < manufacturerList.getLength(); j++) 
								{
									Node manufacturer = manufacturerList.item(j);
									if (manufacturer != null && manufacturer.getNodeType() != Node.TEXT_NODE)
									{
										// Report ADD/UPDATE/DELETE
										tmlHandlerServ.ReportManufacturer(new TMLManufacturerDTO(manufacturer));
										response.setManufacturers(j+1);
									}
								}
							}
							
							// Check for units
							if (node.getNodeName().equals("units"))
							{
								NodeList unitList = ((Element)nodeList.item(i)).getChildNodes();
	
				                CurrentStep = "Units";
								for (int j = 0; j < unitList.getLength(); j++) 
								{
									Node unit = unitList.item(j);
									if (unit != null && unit.getNodeType() != Node.TEXT_NODE)
									{
										// Report ADD/UPDATE/DELETE
										tmlHandlerServ.ReportUnit(new TMLUnitDTO(unit));
										response.setUnits(j+1);
									}
								}
							}
							
							// Check for characteristics
							if (node.getNodeName().equals("caracteristics"))
							{
								NodeList characteristicList = ((Element)nodeList.item(i)).getChildNodes();
	
				                CurrentStep = "Characteristics";
								for (int j = 0; j < characteristicList.getLength(); j++) 
								{
									Node characteristic = characteristicList.item(j);
									if (characteristic != null && characteristic.getNodeType() != Node.TEXT_NODE)
									{
										// Report ADD/UPDATE/DELETE
										tmlHandlerServ.ReportCharacteristic(new TMLCharacteristicDTO(characteristic));
										response.setCharacteristics(j+1);
									}
								}
							}
							
							// Check for options
							if (node.getNodeName().equals("options"))
							{
								NodeList optionList = ((Element)nodeList.item(i)).getChildNodes();
								
				                CurrentStep = "Options";
								for (int j = 0; j < optionList.getLength(); j++) 
								{
									Node option = optionList.item(j);
									if (option != null && option.getNodeType() != Node.TEXT_NODE)
									{
										// Report ADD/UPDATE/DELETE
										tmlHandlerServ.ReportOption(new TMLOptionDTO(option));
										response.setOptions(j+1);
									}
								}
							}
							
							// Check for instrument models
							if (node.getNodeName().equals("instrumentmodels"))
							{
								NodeList instrumentModelsList = ((Element)nodeList.item(i)).getChildNodes();
	
				                CurrentStep = "InstrumentModels";
								for (int j = 0; j < instrumentModelsList.getLength(); j++) 
								{
									Node instrumentModel = instrumentModelsList.item(j);
									if (instrumentModel != null && instrumentModel.getNodeType() != Node.TEXT_NODE)
									{
										// Report ADD/UPDATE/DELETE
										tmlHandlerServ.ReportInstrumentModel(new TMLInstrumentModelDTO(instrumentModel));
										response.setInstrumentModels(j+1);
									}
								}
							}
						}
					}
				}
			}
			
	        response.setIsSuccess(true);
        }
        catch (Exception ex)
        {
        	logger.error("Failure on step "+CurrentStep, ex);
        	response.setFailedStep(CurrentStep);
        	response.setException(ex.getMessage());
        	response.setIsSuccess(false);
        }
        
		return response;
	}
	
	public String[] sftp_listFiles(ChannelSftp ftp, String path) throws Exception
	{
		try
		{
			String absolutePath = ftp.realpath(path);
			@SuppressWarnings("unchecked")
			Vector<LsEntry> files = ftp.ls(absolutePath);
			Object[] fileNames = files.stream().filter(fi->!fi.getFilename().equals(".") && !fi.getFilename().equals("..")).map(fi->fi.getFilename()).toArray();
			String[] response = Arrays.copyOf(fileNames, fileNames.length, String[].class);
			return response;
		}
		catch (Exception ex)
		{
			logger.error("List files failed for path "+path, ex);
			throw ex;
		}
	}
	
	public void sftp_createDirectory(ChannelSftp ftp, String relativePath) throws Exception
	{
		try
		{
			logger.debug("Creating directory for relative path "+relativePath);
			ftp.mkdir(relativePath);
		}
		catch (Exception ex)
		{
			logger.error("Creating directory failed for path "+relativePath, ex);
			throw ex;
		}
	}
	
	public String[] sftp_listDirectories(ChannelSftp ftp, String path) throws Exception
	{
		try
		{
			String absolutePath = ftp.realpath(path);
			@SuppressWarnings("unchecked")
			Vector<LsEntry> directories = ftp.ls(absolutePath);
			Object[] directoryNames = directories.stream().filter(fi->fi.getAttrs().isDir() == true && fi.getFilename() != "." && fi.getFilename() != "..").map(fi->fi.getFilename()).toArray();
			String[] response = Arrays.copyOf(directoryNames, directoryNames.length, String[].class);
			return response;
		}
		catch (Exception ex)
		{
			logger.error("List directories failed for path "+path, ex);
			throw ex;
		}
	}
	
	public InputStream sftp_getCompressedFile(ChannelSftp ftp, String file) throws Exception
	{
		try
		{
			String absolutePath = ftp.realpath(file);
	        InputStream is = ftp.get(absolutePath);
			GZIPInputStream response = new GZIPInputStream(is);
	        
	        return response;
		}
		catch (Exception ex)
		{
			logger.error("Download failed for file "+file, ex);
			throw ex;
		}
	}
	
	public void sftp_createTextFile(ChannelSftp ftp, String path, String content, String file) throws Exception
	{
		try
		{
			File tempDir = new File(System.getProperty("java.io.tmpdir"));
			OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(MessageFormat.format("{0}/{1}",tempDir,file)));
			outputStream1.close();
			
			FileUtils.writeStringToFile(new File(MessageFormat.format("{0}/{1}",tempDir,file)), content);
			InputStream is = new FileInputStream(MessageFormat.format("{0}/{1}",tempDir,file));
			
			ftp.put(is, path);
			is.close();
			
			File fileToDelete = new File(MessageFormat.format("{0}/{1}",tempDir,file));
	        fileToDelete.delete();
		}
		catch (Exception ex)
		{
			logger.error("Upload file failed", ex);
			throw ex;
		}
	}
}