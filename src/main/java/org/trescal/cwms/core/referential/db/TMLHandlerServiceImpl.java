package org.trescal.cwms.core.referential.db;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.db.CharacteristicDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.domain.InstrumentModelDomain;
import org.trescal.cwms.core.instrumentmodel.entity.domain.db.InstrumentModelDomainService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.InstrumentModelFamily;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodelfamily.db.InstrumentModelFamilyService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.instrumentmodel.entity.option.Option;
import org.trescal.cwms.core.instrumentmodel.entity.option.db.OptionService;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.instrumentmodel.entity.uom.db.UoMService;
import org.trescal.cwms.core.referential.dto.TMLCharacteristicDTO;
import org.trescal.cwms.core.referential.dto.TMLDomainDTO;
import org.trescal.cwms.core.referential.dto.TMLFamilyDTO;
import org.trescal.cwms.core.referential.dto.TMLInstrumentModelDTO;
import org.trescal.cwms.core.referential.dto.TMLManufacturerDTO;
import org.trescal.cwms.core.referential.dto.TMLOptionDTO;
import org.trescal.cwms.core.referential.dto.TMLSubFamilyDTO;
import org.trescal.cwms.core.referential.dto.TMLUnitDTO;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

@Service
public class TMLHandlerServiceImpl implements TMLHandlerService {

	@Autowired
	private InstrumentModelDomainService instModelDomServ;
	@Autowired
	private InstrumentModelFamilyService instModelFamServ;
	@Autowired
	private NewDescriptionService descServ;
	@Autowired
	private MfrService mfrServ;
	@Autowired
	private UoMService uomServ;
	@Autowired
	private CharacteristicDescriptionService characDescServ;
	@Autowired
	private OptionService optionServ;
	@Autowired
	private InstrumentModelService instModelServ;
	@Autowired
	private SupportedLocaleService supportedLocaleServ;
	
	@Override
	public void ReportDomain(TMLDomainDTO dto) throws Exception {
		InstrumentModelDomain domain = instModelDomServ.getTMLDomain(dto.getTMLID());
		
		try
		{
			if (domain== null)
			{
				instModelDomServ.createTMLInstrumentModelDomain(dto);
			}
			else
			{
				if (!domain.getActive().equals(dto.isIsEnabled()))
				{
					instModelDomServ.deleteTMLInstrumentModelDomain(dto);
				}
				else
				{
					instModelDomServ.updateTMLInstrumentModelDomain(dto);
				}
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}

	@Override
	public void ReportFamily(TMLFamilyDTO dto) throws Exception {
		// TODO Auto-generated method stub
		InstrumentModelFamily family = instModelFamServ.getTMLFamily(dto.getTMLID()); 
		
		try
		{
			if (family == null)
			{
				instModelFamServ.createTMLInstrumentModelFamily(dto);
			}
			else
			{
				if (!family.getActive().equals(dto.isIsEnabled()))
				{
					instModelFamServ.deleteTMLInstrumentModelFamily(dto);
				}
				else
				{
					instModelFamServ.updateTMLInstrumentModelFamily(dto);
				}
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}

	@Override
	public void ReportSubFamily(TMLSubFamilyDTO dto) throws Exception {
		// TODO Auto-generated method stub
		Description description = descServ.getTMLDescription(dto.getTMLID());
		
		try
		{
			if (description == null)
			{
				descServ.insertTMLDescription(dto);
			}
			else
			{
				if (!description.getActive().equals(dto.isIsEnabled()))
				{
					descServ.deleteTMLDescription(dto);
				}
				else
				{
					descServ.updateTMLDescription(dto);
				}
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}

	@Override
	public void ReportManufacturer(TMLManufacturerDTO dto) throws Exception {
		// TODO Auto-generated method stub
		Mfr mfr = mfrServ.findTMLMfr(dto.getTMLID());
		
		try
		{
			if (mfr == null)
			{
				mfrServ.insertTMLMfr(dto);
			}
			else
			{
				if (mfr.getActive() != dto.isIsEnabled())
				{
					mfrServ.deleteTMLMfr(dto);
				}
				else
				{
					mfrServ.updateTMLMfr(dto);
				}
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}

	@Override
	public void ReportUnit(TMLUnitDTO dto)
			throws Exception {
		// TODO Auto-generated method stub
		UoM uom = uomServ.getTMLUoM(dto.getTMLID());
		
		try
		{
			if (uom == null)
			{
				uomServ.insertTMLUoM(dto);
			}
			else
			{
				if (uom.getActive() != dto.isIsEnabled())
				{
					uomServ.deleteTMLUoM(dto);
				}
				else
				{
					uomServ.updateTMLUoM(dto);
				}
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}

	@Override
	public void ReportCharacteristic(TMLCharacteristicDTO dto) throws Exception {
		// TODO Auto-generated method stub
		CharacteristicDescription characteristicDescription = characDescServ.getTMLCharacteristicDescription(dto.getTMLID());
		
		try
		{
			if (characteristicDescription == null)
			{
				characDescServ.insertTMLCharacteristicDescription(dto);
			}
			else
			{
				if (characteristicDescription.getActive() != dto.isIsEnabled())
				{
					characDescServ.deleteTMLCharacteristicDescription(dto);
				}
				else
				{
					characDescServ.updateTMLCharacteristicDescription(dto);
				}
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}

	@Override
	public void ReportOption(TMLOptionDTO dto)
			throws Exception
	{
		// TODO Auto-generated method stub
		Option option = optionServ.getTMLOption(dto.getTMLID());
		
		try
		{
			if (option == null)
			{
				optionServ.insertTMLOption(dto);
			}
			else
			{
//				if (option.getActive() != dto.isIsEnabled())
//				{
//					optionServ.deleteTMLOption(dto);
//				}
//				else
//				{
					optionServ.updateTMLOption(dto);
//				}
			}
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}
	
	@Override
	public void ReportInstrumentModel(TMLInstrumentModelDTO dto) {
		InstrumentModel instrumentModel = instModelServ.getTMLInstrumentModel(dto.getTMLID());
		InstrumentModel returnedInstrumentModel;
		if (instrumentModel == null)
		{
			returnedInstrumentModel = instModelServ.insertTMLInstrumentModel(dto);
		}
		else
		{
			returnedInstrumentModel = instModelServ.updateTMLInstrumentModel(dto);
		}
		
		// Update the instrument model names
		if (returnedInstrumentModel != null)
		{
			Set<InstrumentModel> instModels = new HashSet<>();
			instModels.add(returnedInstrumentModel);
			List<Locale> locales = this.supportedLocaleServ.getSupportedLocales();
			if (locales != null)
			{
				locales.forEach(locale -> instModelServ.updateInstrumentModelTranslations(instModels, locale));
			}
		}
	}
}
