package org.trescal.cwms.core.referential.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrumentmodel.RangeType;
import org.trescal.cwms.core.instrumentmodel.entity.ModelMfrType;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.CharacteristicDescription;
import org.trescal.cwms.core.instrumentmodel.entity.characteristicdescription.db.CharacteristicDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.CharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.characteristiclibrary.db.CharacteristicLibraryService;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.db.NewDescriptionService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.InstrumentModelType;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodeltype.db.InstrumentModelTypeService;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.db.MfrService;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.ModelRange;
import org.trescal.cwms.core.instrumentmodel.entity.modelrange.db.ModelRangeService;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.ModelRangeCharacteristicLibrary;
import org.trescal.cwms.core.instrumentmodel.entity.modelrangecharacteristiclibrary.db.ModelRangeCharacteristicLibraryService;
import org.trescal.cwms.core.instrumentmodel.entity.option.Option;
import org.trescal.cwms.core.instrumentmodel.entity.option.db.OptionService;
import org.trescal.cwms.core.instrumentmodel.entity.uom.UoM;
import org.trescal.cwms.core.instrumentmodel.entity.uom.db.UoMService;
import org.trescal.cwms.core.referential.dto.TMLInstrumentModelDTO;
import org.trescal.cwms.core.referential.dto.TMLOptionDTO;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * JPA replacement for code in InstrumentModelService/InstrumentModelDao
 * This module specifically used for synchronization import from TML
 * (Trescal Master Library)
 * <p>
 * Galen Beck - 2019-11-17
 */
@Service
public class TMLInstrumentModelServiceImpl implements TMLInstrumentModelService {
	
	@Autowired
	private CharacteristicDescriptionService characteristicDescriptionService;
	@Autowired
	private CharacteristicLibraryService characteristicLibraryService;
	@Autowired
	private InstrumentModelService instrumentModelService;
	@Autowired
	private InstrumentModelTypeService instrumentModelTypeService;
	@Autowired
	private ModelRangeCharacteristicLibraryService mrclService;
	@Autowired
	private ModelRangeService modelRangeService;
	@Autowired
	private MfrService mfrService;
	@Autowired
	private NewDescriptionService descriptionService;
	@Autowired
	private OptionService optionService;
	@Autowired
	private UoMService uomService;
	
	private final static Logger logger = LoggerFactory.getLogger(TMLInstrumentModelServiceImpl.class);

	@Override
	public void insertTMLInstrumentModel(TMLInstrumentModelDTO dto, Contact contact) {
		InstrumentModelType defaultModelType = this.instrumentModelTypeService.findDefaultType();

		InstrumentModel instModel = new InstrumentModel();
		instModel.setModel(dto.getModelName());
		instModel.setModelMfrType(ModelMfrType.MFR_SPECIFIC);
		instModel.setAddedBy(contact);
		instModel.setAddedOn(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		instModel.setModelType(defaultModelType);
		instModel.setTmlid(dto.getTMLID());
		instModel.setQuarantined(!dto.isIsEnabled());

		logger.debug("Saving: " + instModel.toFullString());
		this.instrumentModelService.save(instModel);
		logger.debug("Saved: " + instModel.toFullString());

		this.updateTMLInstrumentModel(dto, contact);
	}

	@Override
	public void updateTMLInstrumentModel(TMLInstrumentModelDTO dto, Contact contact) {
		try {
			InstrumentModel instrumentModel = this.instrumentModelService.getByTmlId(dto.getTMLID());
			logger.debug("Updating: " + instrumentModel.toFullString());
			
			Description instrumentModelSubFamily = this.descriptionService.getByTmlId(dto.getSubFamily());
			Mfr instrumentModelManufacturer = this.mfrService.findTMLMfr(dto.getManufacturer());
			
			instrumentModel.setDescription(instrumentModelSubFamily);
			instrumentModel.setMfr(instrumentModelManufacturer);
			instrumentModel.setModel(dto.getModelName());			
			instrumentModel.setQuarantined(!dto.isIsEnabled());
			
			UoM defaultUoM = this.uomService.findDefaultUoM();

			// Existing model range (CHARACTERISTIC only) and library values are cleared and re-populated 
			// Not using orphan removal yet, until old TML code is removed.
			// It would be better, to identify / update the existing values instead.
			List<ModelRange> ranges = instrumentModel.getRanges().stream()
				.filter(range -> range.getCharacteristicType() == RangeType.CHARACTERISTIC)
				.collect(Collectors.toList());
			List<ModelRangeCharacteristicLibrary> libraryValue = new ArrayList<>(instrumentModel.getLibraryValues()); 
			
			instrumentModel.getRanges().removeAll(ranges);
			this.modelRangeService.deleteAll(ranges);
			
			instrumentModel.getLibraryValues().clear();
			for (ModelRangeCharacteristicLibrary mrcl : libraryValue) {
				this.mrclService.delete(mrcl);
			}

			if (dto.getInstrumentModelCharacteristic() != null) {			
				for (int i = 0; i < dto.getInstrumentModelCharacteristic().getClass().getDeclaredFields().length; i++) {
					Field field = dto.getInstrumentModelCharacteristic().getClass().getDeclaredFields()[i];
					field.setAccessible(true);
					ModelRange modelRange = new ModelRange();
					if (field.getName().startsWith("VarChar") || field.getName().startsWith("Float"))
					{
						if (field.get(dto.getInstrumentModelCharacteristic()) != null)
						{
							CharacteristicDescription characteristicDescription  = 
								characteristicDescriptionService.getBySubfamilyAndInternalName(
									instrumentModelSubFamily.getId(), field.getName());
							
							if (characteristicDescription != null)
							{
								modelRange.setCharacteristicDescription(characteristicDescription);
								modelRange.setCharacteristicType(RangeType.CHARACTERISTIC);
								modelRange.setMaxIsInfinite(false);
								modelRange.setModel(instrumentModel);
								switch (characteristicDescription.getCharacteristicType()) {
									case FREE_FIELD: {
										modelRange.setStart((double) 0);
										modelRange.setUom(defaultUoM);
										modelRange.setMinvalue(field.get(dto.getInstrumentModelCharacteristic()).toString());

										instrumentModel.getRanges().add(modelRange);
										break;
									}
									case LIBRARY:
									{
										Integer characteristicDescriptionId = characteristicDescription.getCharacteristicDescriptionId();
										Integer tmlId = Integer.parseInt(field.get(dto.getInstrumentModelCharacteristic()).toString());
										CharacteristicLibrary characteristicLibrary = 
											this.characteristicLibraryService.getByCharacteristicDescriptionIdAndTmlId(
													characteristicDescriptionId, tmlId);
										
										if (characteristicLibrary != null)
										{
											ModelRangeCharacteristicLibrary mrcl = new ModelRangeCharacteristicLibrary();
											mrcl.setCharacteristicLibrary(characteristicLibrary);
											mrcl.setModel(instrumentModel);
											
											instrumentModel.getLibraryValues().add(mrcl);
										}
										
										break;
									}
									case VALUE_WITH_UNIT:
									{
										Field unitField = dto.getInstrumentModelCharacteristic().getClass().getDeclaredField(field.getName().replace("Float", "Unit"));
										unitField.setAccessible(true);
										
										UoM uom = defaultUoM;
										
										if (unitField.get(dto.getInstrumentModelCharacteristic()) != null && !unitField.get(dto.getInstrumentModelCharacteristic()).equals(""))
										{
											String symbol = unitField.get(dto.getInstrumentModelCharacteristic()).toString();
											uom = this.uomService.findUoMbySymbol(unitField.get(dto.getInstrumentModelCharacteristic()).toString());
											if (uom == null) {
												logger.error("No uom found for symbol/shortname "+symbol);
												// throw RuntimeException, we can't proceed further with a null / incorrect unit.
												throw new RuntimeException("No uom found for symbol/shortname "+symbol);
											}
										}
										
										modelRange.setStart(Double.valueOf(field.get(dto.getInstrumentModelCharacteristic()).toString()));
										modelRange.setUom(uom);
										
										instrumentModel.getRanges().add(modelRange);
										break;
									}
								}
							}
						}						
					}
					field.setAccessible(false);
				}
			}

			if (dto.getOptions() != null) {

				for (TMLOptionDTO tmlOptionDTO : dto.getOptions()) {
					Option option = this.optionService.getTMLOption(dto.getModel(), tmlOptionDTO.getCode());
					if (option != null) {
						boolean newModelRange = false;
						ModelRange mrOption = this.modelRangeService.getByModelIdAndOptionId(instrumentModel.getModelid(), option.getOptionid());
						if (mrOption == null) {
							mrOption = new ModelRange();
							newModelRange = true;
						}

						mrOption.setModel(instrumentModel);
						mrOption.setStart((double) 0);
						mrOption.setUom(defaultUoM);
						mrOption.setModelOption(option);
						mrOption.setCharacteristicType(RangeType.OPTION);
						mrOption.setMaxIsInfinite(false);

						if (newModelRange) {
							instrumentModel.getRanges().add(mrOption);
						}
					}
				}
			}

			logger.debug("Updated: " + instrumentModel.toFullString());

			// TODO : Update the link to the Sales Category
		} catch (IllegalAccessException | NoSuchFieldException ex) {
			throw new RuntimeException(ex);
		}
	}

}
