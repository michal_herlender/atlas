package org.trescal.cwms.core.referential.dto;

import java.util.HashMap;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TMLFamilyDTO {
	
	private int TMLID;
	private int DomainID;
	private String DefaultName;
	private String DefaultDescription;
	private boolean IsEnabled;
	private HashMap<String, String> RegionalNames;
	private HashMap<String, String> RegionalDescriptions;
	
	public int getTMLID() {
		return TMLID;
	}
	public void setTMLID(int tMLID) {
		TMLID = tMLID;
	}
	public int getDomainID() {
		return DomainID;
	}
	public void setDomainID(int domainID) {
		DomainID = domainID;
	}
	public String getDefaultName() {
		return DefaultName;
	}
	public void setDefaultName(String defaultName) {
		DefaultName = defaultName;
	}
	public String getDefaultDescription() {
		return DefaultDescription;
	}
	public void setDefaultDescription(String defaultDescription) {
		DefaultDescription = defaultDescription;
	}
	public boolean isIsEnabled() {
		return IsEnabled;
	}
	public void setIsEnabled(boolean isEnabled) {
		IsEnabled = isEnabled;
	}
	public HashMap<String, String> getRegionalNames() {
		return RegionalNames;
	}
	public void setRegionalNames(HashMap<String, String> regionalNames) {
		RegionalNames = regionalNames;
	}
	public HashMap<String, String> getRegionalDescriptions() {
		return RegionalDescriptions;
	}
	public void setRegionalDescriptions(HashMap<String, String> regionalDescriptions) {
		RegionalDescriptions = regionalDescriptions;
	}	
	
	public TMLFamilyDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public TMLFamilyDTO(Node node) {
		NamedNodeMap nodeAttributes = node.getAttributes();
		this.setTMLID(Integer.parseInt(nodeAttributes.getNamedItem("ID").getNodeValue()));
		this.setDomainID(Integer.parseInt(nodeAttributes.getNamedItem("Domain").getNodeValue()));
		this.setDefaultName(nodeAttributes.getNamedItem("DefaultName").getNodeValue());
		this.setIsEnabled(nodeAttributes.getNamedItem("IsEnabled").getNodeValue().equals("1"));

		HashMap<String, String> regionalNames = new HashMap<String, String>();
		HashMap<String, String> regionalDescriptions = new HashMap<String, String>();
		
		NodeList regionals = ((Element)node).getChildNodes();
		
		for (int i = 0; i < regionals.getLength(); i++) 
		{
			Node regional = regionals.item(i);
			if (regional != null && regional.getNodeType() != Node.TEXT_NODE)
			{
				NamedNodeMap regionalAttributes = regional.getAttributes();
				
				String language = regionalAttributes.getNamedItem("ID").getNodeValue();
				String name = regionalAttributes.getNamedItem("Name") != null ? regionalAttributes.getNamedItem("Name").getNodeValue() : "";
				String description = regionalAttributes.getNamedItem("Description") != null ? regionalAttributes.getNamedItem("Description").getNodeValue() : "";
				
				if (!name.equals(null) && !name.equals(""))
				{
					regionalNames.put(language, name);
				}
				if (!description.equals(null) && !description.equals(""))
				{
					regionalDescriptions.put(language, description);
				}
			}
		}
		
		this.setRegionalNames(regionalNames);
		this.setRegionalDescriptions(regionalDescriptions);
	}
}
