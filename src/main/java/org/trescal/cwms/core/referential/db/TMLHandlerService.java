package org.trescal.cwms.core.referential.db;

import org.trescal.cwms.core.referential.dto.TMLCharacteristicDTO;
import org.trescal.cwms.core.referential.dto.TMLDomainDTO;
import org.trescal.cwms.core.referential.dto.TMLFamilyDTO;
import org.trescal.cwms.core.referential.dto.TMLInstrumentModelDTO;
import org.trescal.cwms.core.referential.dto.TMLManufacturerDTO;
import org.trescal.cwms.core.referential.dto.TMLOptionDTO;
import org.trescal.cwms.core.referential.dto.TMLSubFamilyDTO;
import org.trescal.cwms.core.referential.dto.TMLUnitDTO;

public interface TMLHandlerService {
	void ReportDomain(TMLDomainDTO dto) throws Exception;
	void ReportFamily(TMLFamilyDTO dto) throws Exception;
	void ReportSubFamily(TMLSubFamilyDTO dto) throws Exception;
	void ReportManufacturer(TMLManufacturerDTO dto) throws Exception;
	void ReportUnit(TMLUnitDTO dto) throws Exception;
	void ReportCharacteristic(TMLCharacteristicDTO dto) throws Exception;
	void ReportOption(TMLOptionDTO dto) throws Exception;
	void ReportInstrumentModel(TMLInstrumentModelDTO dto) throws Exception;
}
