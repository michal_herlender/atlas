package org.trescal.cwms.core.referential.db;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.referential.dto.TMLInstrumentModelDTO;

public interface TMLInstrumentModelService {
	
	void insertTMLInstrumentModel(TMLInstrumentModelDTO dto, Contact contact);

	void updateTMLInstrumentModel(TMLInstrumentModelDTO dto, Contact contact);
}
