package org.trescal.cwms.core.referential.dto;

import java.util.HashSet;
import java.util.Set;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TMLInstrumentModelDTO {
	private String ModelName;
	private int TMLID;
	private int InstrumentType;
	private String CustomerID;
	private int SubFamily;
	private int Manufacturer;
	private int Model;
	private boolean IsEnabled;
	private Set<TMLOptionDTO> Options;
	private TMLInstrumentModelCharacteristicDTO InstrumentModelCharacteristic;
	
	public String getModelName() {
		return ModelName;
	}
	public int getTMLID() {
		return TMLID;
	}
	public int getInstrumentType() {
		return InstrumentType;
	}
	public String getCustomerID() {
		return CustomerID;
	}
	public int getSubFamily() {
		return SubFamily;
	}
	public int getManufacturer() {
		return Manufacturer;
	}
	public int getModel() {
		return Model;
	}
	public Set<TMLOptionDTO> getOptions() {
		return Options;
	}
	public TMLInstrumentModelCharacteristicDTO getInstrumentModelCharacteristic() {
		return InstrumentModelCharacteristic;
	}
	public void setModelName(String modelName) {
		ModelName = modelName;
	}
	public void setTMLID(int tMLID) {
		TMLID = tMLID;
	}
	public void setInstrumentType(int instrumentType) {
		InstrumentType = instrumentType;
	}
	public void setCustomerID(String customerID) {
		CustomerID = customerID;
	}
	public void setSubFamily(int subFamily) {
		SubFamily = subFamily;
	}
	public void setManufacturer(int manufacturer) {
		Manufacturer = manufacturer;
	}
	public void setModel(int model) {
		Model = model;
	}
	public boolean isIsEnabled() {
		return IsEnabled;
	}
	public void setIsEnabled(boolean isEnabled) {
		IsEnabled = isEnabled;
	}
	public void setOptions(Set<TMLOptionDTO> options) {
		Options = options;
	}
	public void setInstrumentModelCharacteristic(
			TMLInstrumentModelCharacteristicDTO instrumentModelCharacteristic) {
		InstrumentModelCharacteristic = instrumentModelCharacteristic;
	}
	
	public TMLInstrumentModelDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public TMLInstrumentModelDTO(Node node) {
		NamedNodeMap nodeAttributes = node.getAttributes();
		this.setTMLID(Integer.parseInt(nodeAttributes.getNamedItem("ID").getNodeValue()));
		this.setInstrumentType(Integer.parseInt(nodeAttributes.getNamedItem("InstrumentType").getNodeValue()));
		this.setCustomerID(nodeAttributes.getNamedItem("CustomerID").getNodeValue());
		this.setModel(Integer.parseInt(nodeAttributes.getNamedItem("Model").getNodeValue()));
		this.setModelName(nodeAttributes.getNamedItem("ModelName").getNodeValue());
		this.setSubFamily(Integer.parseInt(nodeAttributes.getNamedItem("SubFamily").getNodeValue()));
		this.setManufacturer(Integer.parseInt(nodeAttributes.getNamedItem("Manufacturer").getNodeValue()));
		this.setIsEnabled(nodeAttributes.getNamedItem("IsEnabled").getNodeValue().equals("1"));
		
		TMLInstrumentModelCharacteristicDTO instrumentModelCharacteristicDTO = new TMLInstrumentModelCharacteristicDTO();		
		Set<TMLOptionDTO> options = new HashSet<TMLOptionDTO>();
				
		NodeList children = ((Element)node).getChildNodes();
		
		for (int i = 0; i < children.getLength(); i++) 
		{
			Node child = children.item(i);	
			
			if (child != null && child.getNodeType() != Node.TEXT_NODE)
			{
				switch (child.getNodeName())
				{
					case "instrumentcharacteristics":
					{
						try
						{
							instrumentModelCharacteristicDTO = new TMLInstrumentModelCharacteristicDTO(child);
						}
						catch (Exception ex)
						{
							
						}
						break;
					}
					case "instrumentoptions":
					{
						NodeList optionList = ((Element)child).getChildNodes();
						
						if (optionList != null)
						{
							for (int j = 0; j < optionList.getLength(); j++) 
							{
								if (optionList.item(j) != null && optionList.item(j).getNodeType() != Node.TEXT_NODE)
								{
									options.add(new TMLOptionDTO(optionList.item(j), Integer.parseInt(nodeAttributes.getNamedItem("Model").getNodeValue())));
								}
							}
						}
								
						break;
					}
				}				
			}			
		}				
		
		this.setOptions(options);
		this.setInstrumentModelCharacteristic(instrumentModelCharacteristicDTO);
	}
}
