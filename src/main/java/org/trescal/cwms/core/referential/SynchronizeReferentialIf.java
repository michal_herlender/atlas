package org.trescal.cwms.core.referential;

public interface SynchronizeReferentialIf {
	void synchronizeReferentialData();
	
	String testConnection();
}
