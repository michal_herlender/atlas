package org.trescal.cwms.core.referential.dto;

import java.util.HashMap;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TMLUnitDTO {
	private int TMLID;
	private String DefaultName;
	private String Symbol;
	private HashMap<String, String> RegionalNames;
	private boolean IsEnabled;
	
	public int getTMLID() {
		return TMLID;
	}
	public void setTMLID(int tMLID) {
		TMLID = tMLID;
	}
	public String getDefaultName() {
		return DefaultName;
	}
	public void setDefaultName(String defaultName) {
		DefaultName = defaultName;
	}
	public String getSymbol() {
		return Symbol;
	}
	public void setSymbol(String symbol) {
		Symbol = symbol;
	}
	public HashMap<String, String> getRegionalNames() {
		return RegionalNames;
	}
	public void setRegionalNames(HashMap<String, String> regionalNames) {
		RegionalNames = regionalNames;
	}
	public boolean isIsEnabled() {
		return IsEnabled;
	}
	public void setIsEnabled(boolean isEnabled) {
		IsEnabled = isEnabled;
	}
	
	public TMLUnitDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public TMLUnitDTO(Node node)
	{
		NamedNodeMap nodeAttributes = node.getAttributes();
		this.setTMLID(Integer.parseInt(nodeAttributes.getNamedItem("ID").getNodeValue()));
		this.setDefaultName(nodeAttributes.getNamedItem("DefaultName").getNodeValue());
		this.setSymbol(nodeAttributes.getNamedItem("ShortName").getNodeValue());
		this.setIsEnabled(nodeAttributes.getNamedItem("IsEnabled").getNodeValue().equals("1"));

		HashMap<String, String> regionalNames = new HashMap<String, String>();
		
		NodeList regionals = ((Element)node).getChildNodes();
		
		for (int i = 0; i < regionals.getLength(); i++) 
		{
			Node regional = regionals.item(i);
			if (regional != null && regional.getNodeType() != Node.TEXT_NODE)
			{
				NamedNodeMap regionalAttributes = regional.getAttributes();
				
				String language = regionalAttributes.getNamedItem("ID").getNodeValue();
				String name = regionalAttributes.getNamedItem("Name") != null ? regionalAttributes.getNamedItem("Name").getNodeValue() : "";
				
				if (!name.equals(null) && !name.equals(""))
				{
					regionalNames.put(language, name);
				}
			}
		}
		
		this.setRegionalNames(regionalNames);
	}
}
