package org.trescal.cwms.core.referential.dto;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.Objects;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;


public class ProcessFileSummary {
	private Integer Domains;
	private Integer Families;
	private Integer SubFamilies;
	private Integer Manufacturers;
	private Integer Units;
	private Integer Characteristics;
	private Integer Options;
	private Integer InstrumentModels;
	
	private boolean IsSuccess;
	private String FailedStep;
	private String Exception;

    Date _Start;

	public Integer getDomains() {
		return Domains;
	}

	public void setDomains(int domains) {
		Domains = domains;
	}

	public Integer getFamilies() {
		return Families;
	}

	public void setFamilies(int families) {
		Families = families;
	}

	public Integer getSubFamilies() {
		return SubFamilies;
	}

	public void setSubFamilies(int subFamilies) {
		SubFamilies = subFamilies;
	}

	public Integer getManufacturers() {
		return Manufacturers;
	}

	public void setManufacturers(int manufacturers) {
		Manufacturers = manufacturers;
	}

	public Integer getUnits() {
		return Units;
	}

	public void setUnits(Integer units) {
		Units = units;
	}

	public Integer getCharacteristics() {
		return Characteristics;
	}

	public void setCharacteristics(Integer characteristics) {
		Characteristics = characteristics;
	}

	public Integer getOptions() {
		return Options;
	}

	public void setOptions(Integer options) {
		Options = options;
	}

	public Integer getInstrumentModels() {
		return InstrumentModels;
	}

	public void setInstrumentModels(Integer instrumentModels) {
		InstrumentModels = instrumentModels;
	}

	public boolean isIsSuccess() {
		return IsSuccess;
	}

	public void setIsSuccess(boolean isSuccess) {
		IsSuccess = isSuccess;
	}

	public String getFailedStep() {
		return FailedStep;
	}

	public void setFailedStep(String failedStep) {
		FailedStep = failedStep;
	}

	public String getException() {
		return Exception;
	}

	public void setException(String exception) {
		Exception = exception;
	}
	
	public Date get_Start() {
		return _Start;
	}

	public void set_Start(Date _Start) {
		this._Start = _Start;
	}

	public ProcessFileSummary() {
		this.set_Start(new Date());
	}
	
	public String ToXml() throws Exception
    {
		try
		{
			// Create the XML DOM Document
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			// Create the root
			Document doc = docBuilder.newDocument();
			Element root = doc.createElement("tml");
			
			// Create the Domains attribute
			Attr domainAttr = doc.createAttribute("Domains");
			domainAttr.setValue(this.getDomains() == null ? "0" : this.getDomains().toString());
			root.setAttributeNode(domainAttr);

			// Create the Families attribute
			Attr familyAttr = doc.createAttribute("Families");
			familyAttr.setValue(this.getFamilies() == null ? "0" : this.getFamilies().toString());
			root.setAttributeNode(familyAttr);

			// Create the SubFamilies attribute
			Attr subfamilyAttr = doc.createAttribute("SubFamilies");
			subfamilyAttr.setValue(this.getSubFamilies() == null ? "0" : this.getSubFamilies().toString());
			root.setAttributeNode(subfamilyAttr);

			// Create the Manufacturers attribute
			Attr manufacturerAttr = doc.createAttribute("Manufacturers");
			manufacturerAttr.setValue(this.getManufacturers() == null ? "0" : this.getManufacturers().toString());
			root.setAttributeNode(manufacturerAttr);

			// Create the Units attribute
			Attr unitAttr = doc.createAttribute("Units");
			unitAttr.setValue(this.getUnits() == null ? "0" : this.getUnits().toString());
			root.setAttributeNode(unitAttr);

			// Create the Characteristics attribute
			Attr characteristicAttr = doc.createAttribute("Characteristics");
			characteristicAttr.setValue(this.getCharacteristics() == null ? "0" : this.getCharacteristics().toString());
			root.setAttributeNode(characteristicAttr);

			// Create the Options attribute
			Attr optionAttr = doc.createAttribute("Options");
			optionAttr.setValue(this.getOptions() == null ? "0" : this.getOptions().toString());
			root.setAttributeNode(optionAttr);

			// Create the InstrumentModels attribute
			Attr instrumentModelsAttr = doc.createAttribute("InstrumentModels");
			instrumentModelsAttr.setValue(this.getInstrumentModels() == null ? "0" : this.getInstrumentModels().toString());
			root.setAttributeNode(instrumentModelsAttr);
			
			// Gets elapsed time
            Date now = new Date();    		
    		long elapsedTime = now.getTime() - _Start.getTime();   
			
	        if (IsSuccess)
	        {
	        	// Create the success element
	        	Element success = doc.createElement("success");  
	        	
	        	// Create the processing time attribute
	    		Attr processingTimeAttr = doc.createAttribute("processingtime"); 	    		
	    		processingTimeAttr.setValue(Objects.toString(elapsedTime, null));    		
	    		success.setAttributeNode(processingTimeAttr);    		
	    		
	    		// Append the success element to the root
	    		root.appendChild(success);
	        }
	        else
	        {
	        	// Create the failure element
	        	Element failure = doc.createElement("failure");  

	        	// Create the processing time attribute
	    		Attr processingTimeAttr = doc.createAttribute("processingtime"); 	    		
	    		processingTimeAttr.setValue(Objects.toString(elapsedTime, null));    		
	    		failure.setAttributeNode(processingTimeAttr);   

	        	// Create the failed step attribute
	    		Attr failedstepAttr = doc.createAttribute("failedstep"); 	    		
	    		failedstepAttr.setValue(FailedStep);    		
	    		failure.setAttributeNode(failedstepAttr);    

	        	// Create the exception time attribute
	    		Attr exceptionTimeAttr = doc.createAttribute("exception"); 	    		
	    		exceptionTimeAttr.setValue(Exception);    		
	    		failure.setAttributeNode(exceptionTimeAttr);     		

	    		// Append the failure element to the root
	    		root.appendChild(failure);
	        }
	        
	        // Append the root to the document
			doc.appendChild(root);
	        return getOuterXML(doc);
		}
		catch (Exception ex)
		{
			throw ex;
		}
    }

	private String getOuterXML(Document doc) throws Exception
	{
		String resultString = "";
		DOMSource source = new DOMSource(doc);
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		StreamResult result = new StreamResult(outStream);
		
		try
		{
			TransformerFactory tFactory =  TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();
			transformer.transform(source, result);
			resultString = new String( outStream.toByteArray());
		}
		catch (Exception ex)
		{
			throw ex;
		}
		
		return resultString;
	}
}
