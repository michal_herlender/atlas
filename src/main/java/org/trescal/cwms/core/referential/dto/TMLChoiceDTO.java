package org.trescal.cwms.core.referential.dto;

import java.util.HashMap;

public class TMLChoiceDTO {
	private int Key;
	private String name;
	private int OrderNo;
	private HashMap<String, String> Values;
	
	public int getKey() {
		return Key;
	}
	public String getName()	{
		return name;
	}
	public int getOrderNo() {
		return OrderNo;
	}
	public HashMap<String, String> getValues() {
		return Values;
	}
	public void setKey(int key) {
		this.Key = key;
	}
	public void setName(String name)	{
		this.name = name;
	}
	public void setOrderNo(int orderNo) {
		this.OrderNo = orderNo;
	}
	public void setValues(HashMap<String, String> values) {
		this.Values = values;
	}
}
