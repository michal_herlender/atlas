package org.trescal.cwms.core.referential.dto;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class TMLOptionDTO {
	private int TMLID;
	private int ModelTMLID;
	private String Code;
	private String DefaultName;
	private boolean IsEnabled;

	public int getTMLID() {
		return TMLID;
	}

	public int getModelTMLID() {
		return ModelTMLID;
	}

	public void setTMLID(int tMLID) {
		TMLID = tMLID;
	}

	public void setModelTMLID(int modelTMLID) {
		ModelTMLID = modelTMLID;
	}	

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}

	public String getDefaultName() {
		return DefaultName;
	}
	
	public void setDefaultName(String defaultName) {
		DefaultName = defaultName;
	}
	
	public boolean isIsEnabled() {
		return IsEnabled;
	}
	
	public void setIsEnabled(boolean isEnabled) {
		IsEnabled = isEnabled;
	}
	
	public TMLOptionDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public TMLOptionDTO(Node node) {
		NamedNodeMap nodeAttributes = node.getAttributes();
		this.setTMLID(Integer.parseInt(nodeAttributes.getNamedItem("ID").getNodeValue()));
		this.setModelTMLID(Integer.parseInt(nodeAttributes.getNamedItem("Model").getNodeValue()));
		this.setDefaultName(nodeAttributes.getNamedItem("DefaultName").getNodeValue());
		this.setCode(nodeAttributes.getNamedItem("Code").getNodeValue());
		this.setIsEnabled(nodeAttributes.getNamedItem("IsEnabled").getNodeValue().equals("1"));
	}
	
	public TMLOptionDTO(Node node, int tmlModelid) {
		NamedNodeMap nodeAttributes = node.getAttributes();
		this.setTMLID(Integer.parseInt(nodeAttributes.getNamedItem("ID").getNodeValue()));
		this.setModelTMLID(tmlModelid);
		this.setDefaultName(nodeAttributes.getNamedItem("DefaultName").getNodeValue());
		this.setCode(nodeAttributes.getNamedItem("Code").getNodeValue());
		this.setIsEnabled(nodeAttributes.getNamedItem("IsEnabled").getNodeValue().equals("1"));
	}
}
