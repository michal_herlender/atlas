package org.trescal.cwms.core.login;

import java.util.Collection;
import java.util.List;
import java.util.SortedSet;
import java.util.stream.Collectors;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CrocodileAuthenticationToken implements Authentication, CredentialsContainer {

	private static final long serialVersionUID = 6532929342470898181L;

	private Object principal;
	private Object credentials;
	private final SortedSet<? extends GrantedAuthority> authorities;
	private boolean isAuthenticated;

	public CrocodileAuthenticationToken(Object principal, Object credentials,
			SortedSet<? extends GrantedAuthority> authorities) {
		super();
		this.principal = principal;
		this.credentials = credentials;
		this.authorities = authorities;
		this.isAuthenticated = !(authorities == null || authorities.isEmpty());
		if (log.isDebugEnabled())
			log.debug("Create authentification token for " + principal.toString() + " with " + authorities.size() + " permissions");
		if (log.isTraceEnabled()) {
			List<String> firstAuthorities = this.authorities.stream().limit(5).map(a -> a.toString()).collect(Collectors.toList());
			log.trace("First 5 authorities in set : "+firstAuthorities.toString());
		}
	}

	@Override
	public String getName() {
		return principal.toString();
	}

	@Override
	public void eraseCredentials() {
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public Object getCredentials() {
		return credentials;
	}

	@Override
	public Object getDetails() {
		return null;
	}

	@Override
	public Object getPrincipal() {
		return principal;
	}

	@Override
	public boolean isAuthenticated() {
		return isAuthenticated;
	}

	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		if (isAuthenticated) {
			throw new IllegalArgumentException(
					"Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
		} else
			isAuthenticated = false;
	}
}