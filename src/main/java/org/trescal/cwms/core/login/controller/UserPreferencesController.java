package org.trescal.cwms.core.login.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;
import org.trescal.cwms.core.login.entity.userpreferences.db.UserPreferencesService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.alligatorsettings.db.AlligatorSettingsService;
import org.trescal.cwms.core.system.entity.alligatorsettingscontact.db.AlligatorSettingsContactService;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class UserPreferencesController
{
	@Autowired
	private AlligatorSettingsContactService ascService; 
	@Autowired
	private AlligatorSettingsService asService; 
	
	@Autowired
	private UserPreferencesService userPreferencesServ;

	@RequestMapping(value="/userpreferences.htm")
	protected String handleRequestInternal(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto)
	{
		List<UserPreferences> listUserPrefs = this.userPreferencesServ.findBySubdiv(subdivDto.getKey());
		List<Contact> listContacts = listUserPrefs.stream().map(up -> up.getContact()).collect(Collectors.toList());
		model.addAttribute("userPreferences", listUserPrefs);
		model.addAttribute("alligatorContactSettings", getAlligatorContactSettings(listContacts));
		model.addAttribute("alligatorProductionSettings", getAlligatorSettings(listContacts, true));
		model.addAttribute("alligatorTestSettings", getAlligatorSettings(listContacts, false));
		return "trescal/core/admin/viewuserpreferences";
	}
	
	// Note, may need to refactor for performance, but only one subdiv is shown at a time.
	private Map<Integer, Boolean> getAlligatorContactSettings(List<Contact> listContacts) {
		Map<Integer, Boolean> result = new HashMap<>();
		listContacts.stream().forEach(contact -> result.put(contact.getPersonid(), ascService.findAlligatorSettingsContact(contact) != null));
		return result;
	}
	
	// Note, may need to refactor for performance, but only one subdiv is shown at a time.
	private Map<Integer, String> getAlligatorSettings(List<Contact> listContacts, boolean production) {
		Map<Integer, String> result = new HashMap<>();
		listContacts.stream().forEach(contact -> result.put(contact.getPersonid(), asService.getSettings(contact, production).getDescription()));
		return result;
	}
}