package org.trescal.cwms.core.login.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;
import org.trescal.cwms.core.login.entity.userpreferences.db.UserPreferencesService;
import org.trescal.cwms.core.login.form.UserPreferencesForm;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettings;
import org.trescal.cwms.core.system.entity.alligatorsettings.AlligatorSettingsContact;
import org.trescal.cwms.core.system.entity.alligatorsettings.db.AlligatorSettingsService;
import org.trescal.cwms.core.system.entity.alligatorsettingscontact.db.AlligatorSettingsContactService;
import org.trescal.cwms.core.system.entity.labelprinter.LabelPrinter;
import org.trescal.cwms.core.system.entity.labelprinter.db.LabelPrinterService;
import org.trescal.cwms.core.tools.AuthenticationService;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.core.validation.BeanValidator;
import org.trescal.cwms.spring.model.KeyValue;

@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV,Constants.SESSION_ATTRIBUTE_USERNAME})
public class EditUserPreferencesController
{
	@Autowired
	private AlligatorSettingsService asService;
	@Autowired
	private AlligatorSettingsContactService ascService;	
	@Autowired
	private LabelPrinterService labelPrinterServ;
	@Autowired
	private MessageSource messages; 
	@Autowired
	private UserPreferencesService preferenceServ;
	@Autowired
	private UserService userService;
	@Autowired
	private BeanValidator validator; 
	@Autowired
	private AuthenticationService authServ;
	
	public static final String CODE_ADDRESS_DEFAULT = "edituserpreferences.nonespecifieduseaddressdefault";
	public static final String TEXT_ADDRESS_DEFAULT = "None specified (use address default)";
	
	@InitBinder("form")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }
	
	@ModelAttribute("form")
	protected UserPreferencesForm formBackingObject(@RequestParam(value="preferenceid", required=true) Integer preferenceId) throws Exception
	{
		UserPreferencesForm form = new UserPreferencesForm();
		UserPreferences up = this.preferenceServ.get(preferenceId);
		Contact contact = up.getContact();
		AlligatorSettingsContact asc = this.ascService.findAlligatorSettingsContact(contact);
		
		if (asc != null) {
			form.setAlligatorSettingsOnContact(true);
			form.setAlligatorSettingsProductionId(asc.getSettingsProduction().getId());
			form.setAlligatorSettingsTestId(asc.getSettingsTest().getId());
		}
		else {
			form.setAlligatorSettingsOnContact(false);
			form.setAlligatorSettingsProductionId(0);
			form.setAlligatorSettingsTestId(0);
		}
		form.setAutoPrintLabel(up.getAutoPrintLabel());
		form.setDefaultServiceTypeUsage(up.getDefaultServiceTypeUsage());
		form.setIncludeSelfInEmailReplies(up.isIncludeSelfInEmailReplies());
		form.setJiKeyNavigation(up.isJiKeyNavigation());
		form.setJobType(up.getDefaultJobType());
		form.setLabelPrinterId(up.getLabelPrinter() == null ? 0 : up.getLabelPrinter().getId());
		form.setMouseoverActive(up.isMouseoverActive());
		form.setMouseoverDelay(up.getMouseoverDelay());
		form.setNavBarPosition(up.isNavBarPosition());
		form.setPreferencesId(up.getId());
		
		return form;
	}
	
	@RequestMapping(value="/edituserpreferences.htm", method=RequestMethod.POST)
	public String onSubmit(Model model,
			@Valid @ModelAttribute("form") UserPreferencesForm form, BindingResult bindingResult,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto) throws Exception
	{
		if (bindingResult.hasErrors()) {
			return referenceData(model, form, username,subdivDto);
		}
		this.preferenceServ.editUserPreferences(form);
		UserPreferences up = this.preferenceServ.get(form.getPreferencesId());
		String tab = "&loadtab=userprefs-tab";
		return "redirect:viewperson.htm?personid=" + up.getContact().getPersonid() + tab;
	}
	
	public boolean isEditingOwnProfile(String username, int preferenceId) {
		Contact currentContact = userService.get(username).getCon();
		UserPreferences up = this.preferenceServ.get(preferenceId); 
		return up.getContact().getPersonid() == currentContact.getPersonid();
	}
	
	@RequestMapping(value="/edituserpreferences.htm", method=RequestMethod.GET)
	protected String referenceData(Model model, 
			@ModelAttribute("form") UserPreferencesForm form,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception
	{
		User user = this.userService.get(username);
		boolean isAdmin = this.authServ.hasRight(user.getCon(), Permission.ADMIN_EDIT_USER_PREFERENCE, subdivDto.getKey());
		Integer preferenceId = form.getPreferencesId();
		
		model.addAttribute("contact", this.preferenceServ.get(preferenceId).getContact());
		model.addAttribute("alligatorSettings", getAlligatorSettings());
		model.addAttribute("editAuthorized", isAdmin || isEditingOwnProfile(username, preferenceId));
		model.addAttribute("jobTypes", JobType.getActiveJobTypes());
		model.addAttribute("labelPrinters", getLabelPrinters());
		return "trescal/core/admin/edituserpreferences";
	}
	
	/**
	 * TODO - restrict to the subdiv(s) that the user has access to
	 */
	private List<KeyValue<Integer, String>> getLabelPrinters() {
		Locale locale = LocaleContextHolder.getLocale();
		List<LabelPrinter> printers = this.labelPrinterServ.getAllLabelPrinters();
		List<KeyValue<Integer, String>> result = new ArrayList<>();
		String notSelectedText = messages.getMessage(CODE_ADDRESS_DEFAULT, null, TEXT_ADDRESS_DEFAULT, locale);
		result.add(new KeyValue<Integer, String>(0, "--"+notSelectedText+"--"));
		for (LabelPrinter lp : printers) {
			StringBuffer lpText = new StringBuffer();
			lpText.append(lp.getAddr().getSub().getComp().getCountry().getCountryCode());
			lpText.append("-");
			lpText.append(lp.getAddr().getSub().getComp().getCompanyCode());
			lpText.append("-");
			lpText.append(lp.getAddr().getSub().getSubdivCode());
			lpText.append(" : ");
			lpText.append(lp.getDescription());
			
			result.add(new KeyValue<Integer, String>(lp.getId(), lpText.toString()));
		}
		return result;
	}
	
	/**
	 * TODO - restrict to the subdiv(s) that the user has access to
	 */
	private List<KeyValue<Integer, String>> getAlligatorSettings() {
		List<KeyValue<Integer, String>> result = new ArrayList<>();
		List<AlligatorSettings> listAs = this.asService.getAll();
		listAs.stream().forEach(as -> result.add(new KeyValue<Integer, String>(as.getId(), as.getDescription())));
		return result;
	}
}