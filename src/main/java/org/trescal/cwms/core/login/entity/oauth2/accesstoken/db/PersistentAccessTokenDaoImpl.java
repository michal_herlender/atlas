package org.trescal.cwms.core.login.entity.oauth2.accesstoken.db;

import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.login.entity.oauth2.accesstoken.PersistentAccessToken;
import org.trescal.cwms.core.login.entity.oauth2.accesstoken.PersistentAccessToken_;

@Repository
public class PersistentAccessTokenDaoImpl extends BaseDaoImpl<PersistentAccessToken, Integer> implements PersistentAccessTokenDao {

	@Override
	protected Class<PersistentAccessToken> getEntity() {
		return PersistentAccessToken.class;
	}

	@Override
	public PersistentAccessToken findSingle(String authenticationId, String tokenId, String refreshToken) {
		if ((authenticationId == null) && (tokenId == null) && (refreshToken == null)) {
			throw new IllegalArgumentException("At least one argument must be not null");
		}
		
		Optional<PersistentAccessToken> result = getFirstResult(cb -> {
			CriteriaQuery<PersistentAccessToken> cq = cb.createQuery(PersistentAccessToken.class);
			Root<PersistentAccessToken> root = cq.from(PersistentAccessToken.class);
			if (authenticationId != null)
				cq.where(cb.equal(root.get(PersistentAccessToken_.authenticationId), authenticationId));
			if (tokenId != null)
				cq.where(cb.equal(root.get(PersistentAccessToken_.tokenId), tokenId));
			if (refreshToken != null)
				cq.where(cb.equal(root.get(PersistentAccessToken_.refreshToken), refreshToken));
			return cq;
		});
		return result.orElse(null);
	}

	@Override
	public List<PersistentAccessToken> findByClientIdAndUserName(String clientId, String userName) {
		if ((clientId == null) && (userName == null)) {
			throw new IllegalArgumentException("At least one argument must be not null");
		}
		return getResultList(cb -> {
			CriteriaQuery<PersistentAccessToken> cq = cb.createQuery(PersistentAccessToken.class);
			Root<PersistentAccessToken> root = cq.from(PersistentAccessToken.class);
			if (clientId != null)
				cq.where(cb.equal(root.get(PersistentAccessToken_.clientId), clientId));
			if (userName != null)
				cq.where(cb.equal(root.get(PersistentAccessToken_.username), userName));
			return cq;
		});
	}
	
}