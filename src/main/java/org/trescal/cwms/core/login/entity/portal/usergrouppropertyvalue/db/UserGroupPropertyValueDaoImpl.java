package org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.db;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;
import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.UserGroupPropertyValue;

/**
 * Created by scottchamberlain on 31/10/2016.
 */
@Repository("UserGroupPropertyValueDao")
public class UserGroupPropertyValueDaoImpl extends BaseDaoImpl<UserGroupPropertyValue, Integer> implements UserGroupPropertyValueDao {

    @Override
    protected Class<UserGroupPropertyValue> getEntity() { return UserGroupPropertyValue.class; }


    @Override
    public UserGroupPropertyValue getValue(UserGroup userGroup, UserGroupProperty property) {
        Criteria criteria = getSession().createCriteria(UserGroupPropertyValue.class);
        criteria.add(Restrictions.eq("userGroup",userGroup));
        criteria.add(Restrictions.eq("userGroupProperty",property));
        return (UserGroupPropertyValue) criteria.uniqueResult();
    }
}
