package org.trescal.cwms.core.login.entity.oauth2.refreshtoken.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.login.entity.oauth2.refreshtoken.PersistentRefreshToken;

public interface PersistentRefreshTokenService extends BaseService<PersistentRefreshToken, Integer> {
	PersistentRefreshToken findByTokenId(String tokenId);
}
