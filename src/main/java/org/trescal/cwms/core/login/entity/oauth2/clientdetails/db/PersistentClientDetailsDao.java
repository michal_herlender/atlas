package org.trescal.cwms.core.login.entity.oauth2.clientdetails.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.login.entity.oauth2.clientdetails.PersistentClientDetails;

public interface PersistentClientDetailsDao extends BaseDao<PersistentClientDetails, String> {

}
