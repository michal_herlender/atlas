package org.trescal.cwms.core.login.entity.portal.usergroup.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup_;

@Repository("UserGroupDao")
public class UserGroupDaoImpl extends BaseDaoImpl<UserGroup, Integer> implements UserGroupDao
{
	@Override
	protected Class<UserGroup> getEntity() {
		return UserGroup.class;
	}
	
	@Override
	public List<UserGroup> getSortedUserGroups(Company company)
	{
		return getResultList(cb ->{
			CriteriaQuery<UserGroup> cq = cb.createQuery(UserGroup.class);
			Root<UserGroup> root = cq.from(UserGroup.class);
			Join<UserGroup, Company> companyJoin = root.join(UserGroup_.company, JoinType.LEFT);
			Predicate clauses = cb.disjunction();
			clauses.getExpressions().add(cb.equal(companyJoin.get(Company_.coid), company.getCoid()));
			clauses.getExpressions().add(cb.isNull(root.get(UserGroup_.company)));
			cq.where(clauses);
			cq.orderBy(cb.desc(root.get(UserGroup_.systemDefault)),
					cb.asc(root.get(UserGroup_.company)), 
					cb.asc(root.get(UserGroup_.name)));
			return cq;
		});
	}
	
	@Override
	public UserGroup getDefaultGroup()
	{
		return getFirstResult(cb ->{
			CriteriaQuery<UserGroup> cq = cb.createQuery(UserGroup.class);
			Root<UserGroup> root = cq.from(UserGroup.class);
			cq.where(cb.isTrue(root.get(UserGroup_.systemDefault)));
			return cq;
		}).orElse(null);
	}
	
	
}