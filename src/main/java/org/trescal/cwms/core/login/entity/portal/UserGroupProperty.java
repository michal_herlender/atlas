package org.trescal.cwms.core.login.entity.portal;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.EnumSet;

/**
 * Enum to replace UserGroupType entity
 * It describes valid properties for customer portal user groups
 * Created by scottchamberlain on 31/10/2016.
 */
public enum UserGroupProperty {

    VIEW_LEVEL("usergroupproperty.viewlevel","usergroupproperty.viewleveldesc",UserScope.CONTACT),
    ADD_INSTRUMENT("usergroupproperty.addinstrument","usergroupproperty.addinstrumentdesc",UserScope.NONE),
    UPDATE_INSTRUMENT("usergroupproperty.updateinstrument","usergroupproperty.updateinstrumentdesc",UserScope.NONE),
    VALIDATE_CERTIFICATE("usergroupproperty.validatecertificate","usergroupproperty.validatecertificatedesc",UserScope.NONE),
    CREATE_CALIBRATION("usergroupproperty.createcalibration","usergroupproperty.createcalibrationdesc",UserScope.NONE);

    private final String nameCode;
    private final String descriptionCode;
    private final UserScope defaultValue;
    private MessageSource messageSource;

    private UserGroupProperty(String nameCode, String descriptionCode, UserScope defaultValue){
        this.defaultValue = defaultValue;
        this.nameCode = nameCode;
        this.descriptionCode = descriptionCode;
    }

    @Component
    public static class UserGroupPropertyMessageSourceInjector
    {
        @Autowired
        private MessageSource messageSource;

        @PostConstruct
        public void postConstruct() {
            for (UserGroupProperty userGroupProperty : EnumSet.allOf(UserGroupProperty.class)) {
                userGroupProperty.setMessageSource(messageSource);
            }
        }
    }

    public String getName() {
        return messageSource.getMessage(nameCode, null, LocaleContextHolder.getLocale());
    }

    public String getDescription() {
        return messageSource.getMessage(descriptionCode, null, LocaleContextHolder.getLocale());
    }

    public UserScope getDefaultValue() {
        return defaultValue;
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
}
