package org.trescal.cwms.core.login.entity.oauth2.clientdetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.userright.entity.role.UserRightRole;

import lombok.Setter;

@Entity
@Table(name = "oauth2clientdetails")
@Setter
public class PersistentClientDetails {

	private String clientId;
	private String clientSecret;
	private String scope;
	private String resourceIds;
	private Integer accessTokenValiditySeconds;
	private Integer refreshTokenValiditySeconds;
	private String description;
	private UserRightRole role;

	@Id
	@Column(name = "clientid", unique = true, length = 256, columnDefinition = "varchar(256)")
	public String getClientId() {
		return this.clientId;
	}

	@Column(name = "clientSecret", nullable = false, length = 256, columnDefinition = "varchar(256)")
	public String getClientSecret() {
		return this.clientSecret;
	}

	@Column(name = "scope", nullable = true, length = 256, columnDefinition = "varchar(256)")
	public String getScope() {
		return scope;
	}

	@Column(name = "resourceIds", nullable = true, length = 256, columnDefinition = "varchar(256)")
	public String getResourceIds() {
		return resourceIds;
	}

	@Type(type = "int")
	@Column(name = "accesstokenvalidity")
	public Integer getAccessTokenValiditySeconds() {
		return accessTokenValiditySeconds;
	}

	@Type(type = "int")
	@Column(name = "refreshtokenvalidity")
	public Integer getRefreshTokenValiditySeconds() {
		return refreshTokenValiditySeconds;
	}

	@Column(name = "description", nullable = true, length = 2000, columnDefinition = "nvarchar(2000)")
	public String getDescription() {
		return description;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "role", nullable = false, foreignKey = @ForeignKey(name = "FK_userrole_role"))
	public UserRightRole getRole() {
		return role;
	}

}
