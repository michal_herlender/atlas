package org.trescal.cwms.core.login.entity.userpreferences.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;
import org.trescal.cwms.core.login.form.UserPreferencesForm;

public interface UserPreferencesService extends BaseService<UserPreferences, Integer>
{
	/**
	 * Creates and adds a new set of {@link UserPreferences} for the given
	 * {@link User}, using default values initially. This method must be used
	 * when adding a new business {@link Contact} to the system.
	 * 
	 * @param user the {@link User} to add {@link UserPreferences} for
	 */
	void addDefaultPreferencesForUser(User user);
	
	void editUserPreferences(UserPreferencesForm form);

	UserPreferences findUserPreferencesByContact(int contactid);
	
	UserPreferences findUserPreferencesByUsername(String username);
	
	List<UserPreferences> findBySubdiv(int subdivId);
	
	void updateDefaultServiceTypeUsage(String username, boolean usageValue);
}
