package org.trescal.cwms.core.login.controller;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("authorities")
public class AuthenticationRestController {

    @GetMapping("all.json")
    Collection<? extends GrantedAuthority> getAll() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities();
    }
}
