package org.trescal.cwms.core.login.entity.login;

public enum IPSubnet
{
	ANTECH("192.168.13.0/24"), FLOW("192.168.16.0/24"), JIM("192.168.15.0/24"), MD("192.168.100.0/24"), RON("192.168.14.0/24"), TREVOR("192.168.1.0/24");

	private String ipSubnet;

	private IPSubnet(String ipSubnet)
	{
		this.ipSubnet = ipSubnet;
	}

	public String getIpSubnet()
	{
		return this.ipSubnet;
	}
}