package org.trescal.cwms.core.login.controller;

import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.util.WebUtils;
import org.trescal.cwms.core.company.dto.CompanyKeyValue;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.corole.CompanyRole;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.login.entity.login.db.LoginService;
import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.db.UserGroupPropertyValueService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.businessdetails.db.BusinessDetailsService;
import org.trescal.cwms.core.system.entity.systemdefaulttypes.SystemDefaultTimeZoneType;
import org.trescal.cwms.core.system.enums.Scope;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.web.utilities.WebLogWriter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

/**
 * Authentication filter which sets the currently logged in {@link Contact} into
 * the session for use elsewhere and additionally updates the {@link User}'s
 * last login + login count fields.
 */
public class MyAuthenticationProcessingFilter extends UsernamePasswordAuthenticationFilter {

    @Autowired
    private BusinessDetailsService businessDetailsService;
    @Autowired
    private SystemDefaultTimeZoneType systemDefaultTimeZoneType;
    @Autowired
    private LoginService loginService;
    @Value("#{props['cwms.config.version']}")
    private String version;
    @Value("#{props['cwms.config.systemtype']}")
    private String systemType;
    @Autowired
    private SubdivService subdivService;
    @Autowired
    private SupportedLocaleService supportedLocaleService;
	@Autowired
	private UserGroupPropertyValueService userGroupPropertyValueService;
	@Autowired
	private UserService userService;
	@Autowired
	private WebLogWriter webLogWriter;

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		Authentication authResult = super.attemptAuthentication(request, response);
		String username = authResult.getName();
		User user = this.userService.get(username);
		// The authentication search is not case sensitive, this compares the
		// autenticated username against
		// the stored user name. Note that some usernames have trailing spaces in the
		// database, so trim()
		// is used before comparison, otherwise the comparison may fail.
		if (!user.getUsername().trim().equals(user.getCon().getUser().getUsername().trim()))
			throw new AuthenticationCredentialsNotFoundException("Username not found");
		if (!user.getCon().isActive() || !user.getWebAware())
			throw new AccountExpiredException("");
		if (request.getParameter("targetUrl").contains("/web/")) {
			webLogWriter.createLog(user, request);
			return authResult;
		}
		/*
		 * By default the business user will log into their own subdivision. We want to
		 * ensure that they have a user role defined for their subdivision. (Essentially
		 * checking whether the account has been configured correctly)
		 */
		boolean hasRoleAtDefaultSubdiv = authResult.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_INTERNAL") );
		if (!hasRoleAtDefaultSubdiv) {
			if(!(user.getCon().getSub().getComp().getCompanyRole()==CompanyRole.BUSINESS)) {
				logger.error("User web not have hability to log in internal application");
				throw new UsernameNotFoundException("User not found");
			}
			logger.error(user.getUsername() + " does not have proper credentials for default subdivision");
			throw new BadCredentialsException("User does not have proper credentials for default subdivision");
		}
		return authResult;
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		HttpSession s = request.getSession(true);
        String username = authResult.getName();
        User user = this.userService.get(username);
        s.setAttribute(Constants.SESSION_ATTRIBUTE_USERNAME, user.getUsername());
        s.setAttribute(Constants.SESSION_ATTRIBUTE_CONTACT, user.getCon());
        s.setAttribute(Constants.SESSION_ATTRIBUTE_SYSTEM_TYPE, this.systemType);
        s.setAttribute(Constants.SESSION_ATTRIBUTE_CONNECTION_TYPE,
            this.loginService.getConnectionTypeFromIPAddress(request));
        Subdiv subdiv = user.getCon().getSub();
        if (subdiv.getComp().getCompanyRole() != CompanyRole.BUSINESS)
            subdiv = subdiv.getComp().getDefaultBusinessContact().getSub();
        val timeZone = systemDefaultTimeZoneType.getGenericValueHierachical(Scope.SUBDIV, subdiv.getSubdivid(), subdiv.getComp())._2();
        s.setAttribute(Constants.SESSION_ATTRIBUTE_TIME_ZONE, timeZone);
        s.setAttribute(Constants.SESSION_ATTRIBUTE_COMPANY, new CompanyKeyValue(subdiv.getComp()));
        s.setAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV, new SubdivKeyValue(subdiv));
		s.setAttribute(Constants.SESSION_ATTRIBUTE_AVAILABLE_SUBDIVS, getAvailableSubdivs(user));
		s.setAttribute(Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE, supportedLocaleService.getPrimaryLocale());
		if (s.getAttribute(Constants.HTTPSESS_NEW_FILE_LIST) == null)
			s.setAttribute(Constants.HTTPSESS_NEW_FILE_LIST, new ArrayList<String>());
		if (this.version.equals("@build_version@")) {
			s.setAttribute(Constants.SESSION_ATTRIBUTE_VERSION, "Development");
		} else {
			s.setAttribute(Constants.SESSION_ATTRIBUTE_VERSION, this.version);
		}
		// update the login count + date
		user.setLastLogin(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		user.setLoginCount((user.getLoginCount() == null ? 0 : user.getLoginCount()) + 1);
		logger.info("Successful authentication for user " + user.getUsername());
		// Save user here (merge, due to lookup by Spring security) - otherwise we are
		// relying on a flush in subsequent service call
		this.userService.mergeUser(user);
		setLocaleFromPreferences(user.getCon(), request);
		s.setAttribute(Constants.SESSION_ATTRIBUTE_BUSINESS_DETAILS,
			this.businessDetailsService.getAllBusinessDetails(subdiv));
		setWebsiteAttributes(user.getCon(), s);
		super.successfulAuthentication(request, response, chain, authResult);
	}


	/*
	 * Formerly in ClientWebAuthenticationProcessingFilter; client web-specific
	 * stuff except for logging
	 */
	private void setWebsiteAttributes(Contact contact, HttpSession s) {
		s.setAttribute(Constants.SESSION_ATTRIBUTE_USER_READ_RULE,
			this.userGroupPropertyValueService.getValue(contact, UserGroupProperty.VIEW_LEVEL).name());
		s.setAttribute(Constants.SESSION_ATTRIBUTE_USER_UPDATE_RULE,
			this.userGroupPropertyValueService.getValue(contact, UserGroupProperty.UPDATE_INSTRUMENT).name());
		s.setAttribute(Constants.SESSION_ATTRIBUTE_USER_ADD_INST_RULE,
			this.userGroupPropertyValueService.getValue(contact, UserGroupProperty.ADD_INSTRUMENT).name());
		s.setAttribute(Constants.SESSION_ATTRIBUTE_USER_CERTVAL_RULE,
			this.userGroupPropertyValueService.getValue(contact, UserGroupProperty.VALIDATE_CERTIFICATE).name());
		s.setAttribute(Constants.SESSION_ATTRIBUTE_USER_ADD_CAL_RULE,
			this.userGroupPropertyValueService.getValue(contact, UserGroupProperty.CREATE_CALIBRATION).name());
	}

	/*
	 * Builds Map of {Company key-value, Set of Subdiv key-values} Generic key value
	 * pairs are used to prevent detached entities
	 */
	protected Map<CompanyKeyValue, Set<SubdivKeyValue>> getAvailableSubdivs(User user) {
		Set<Subdiv> subdivs = subdivService.getBusinessCompanySubdivsForUser(user);
		logger.debug("Found " + subdivs.size() + " subdivs for user " + user.getUsername());
		Map<CompanyKeyValue, Set<SubdivKeyValue>> result = new TreeMap<>();
		for (Subdiv subdiv : subdivs) {
			if(subdiv != null) {
				CompanyKeyValue companyKey = new CompanyKeyValue(subdiv.getComp());
				SubdivKeyValue subdivKey = new SubdivKeyValue(subdiv);
				logger.debug(
						"Adding subdiv " + subdiv.getSubname() + " id " + subdiv.getSubdivid() + " for user " + user.getUsername());
				if (result.containsKey(companyKey)) {
					result.get(companyKey).add(subdivKey);
				} else {
					Set<SubdivKeyValue> subdivSet = new TreeSet<>();
					subdivSet.add(subdivKey);
					result.put(companyKey, subdivSet);
				}
			}
		}
		return result;
	}

	public void setLocaleFromPreferences(Contact contact, HttpServletRequest request) {
		// If the contact has a default locale, then use it to set the locale in the
		// session.
		if (contact.getLocale() == null)
			return;
		WebUtils.setSessionAttribute(request, Constants.SESSION_ATTRIBUTE_LOCALE, contact.getLocale());
		logger.info("Set locale to " + contact.getLocale() + " for user " + contact.getUser().getUsername());
	}
}