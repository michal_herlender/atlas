package org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.entity.portal.UserScope;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.UserGroupPropertyValue;
import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;

/**
 * Created by scottchamberlain on 31/10/2016.
 */
@Service("UserGroupPropertyValueService")
public class UserGroupPropertyValueServiceImpl extends BaseServiceImpl<UserGroupPropertyValue, Integer> implements UserGroupPropertyValueService {

    @Autowired
    private UserGroupPropertyValueDao dao;

    @Override
    public UserScope getValue(Contact contact, UserGroupProperty userGroupProperty) {
        UserGroupPropertyValue userGroupPropertyValue = this.dao.getValue(contact.getUsergroup(),userGroupProperty);
        return (userGroupPropertyValue != null) ? userGroupPropertyValue.getScope() : userGroupProperty.getDefaultValue();
    }

	@Override
	protected BaseDao<UserGroupPropertyValue, Integer> getBaseDao() {
		return this.dao;
	}
}
