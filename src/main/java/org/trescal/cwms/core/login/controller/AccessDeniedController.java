package org.trescal.cwms.core.login.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.trescal.cwms.core.system.Constants;

@Controller //@LoginController (in future)
public class AccessDeniedController
{
	@Value("${cwms.config.systemtype}")
	private String systemType;
	
	@RequestMapping(value="/accessdenied.htm")
	public String handleRequest(HttpServletRequest request, Model model) throws Exception
	{
		HttpSession session = request.getSession(true);
		String message = (String) session.getAttribute(Constants.SYSTEM_AUTHENTICATION_ERROR_MESSAGE);
		session.removeAttribute(Constants.SYSTEM_AUTHENTICATION_ERROR_MESSAGE);
		model.addAttribute("message", message);
		model.addAttribute("systemType", this.systemType);
		
		return "trescal/core/login/accessdenied";
	}
}