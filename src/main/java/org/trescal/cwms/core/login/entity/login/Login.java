package org.trescal.cwms.core.login.entity.login;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.contact.Contact;

@Entity
@Table(name = "login")
public class Login
{
	private Contact con;
	private int id;
	private Date loginTime;
	private boolean webLogCreated;
	private String webLogPath;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", nullable=false)
	public Contact getCon()
	{
		return this.con;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@NotNull
	@Column(name = "logintime", columnDefinition="datetime")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLoginTime()
	{
		return this.loginTime;
	}

	@Length(min = 0, max = 200)
	@Column(name = "weblogpath", length = 200)
	public String getWebLogPath()
	{
		return this.webLogPath;
	}

	@Column(name = "weblogcreated", columnDefinition="tinyint")
	public boolean isWebLogCreated()
	{
		return this.webLogCreated;
	}

	public void setCon(Contact con)
	{
		this.con = con;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setLoginTime(Date loginTime)
	{
		this.loginTime = loginTime;
	}

	public void setWebLogCreated(boolean webLogCreated)
	{
		this.webLogCreated = webLogCreated;
	}

	public void setWebLogPath(String webLogPath)
	{
		this.webLogPath = webLogPath;
	}

}