package org.trescal.cwms.core.login.entity.userpreferences.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;

public interface UserPreferencesDao extends BaseDao<UserPreferences, Integer>
{
	UserPreferences findUserPreferencesByContact(int contactid);

	UserPreferences findUserPreferencesByUsername(String username);

	List<UserPreferences> findBySubdiv(int subdivId);
}