package org.trescal.cwms.core.login.entity.user;

import org.hibernate.annotations.Type;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.trescal.cwms.core.audit.Versioned;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "users")
public class User extends Versioned implements ComponentEntity, UserDetails {
	/**
	 * UserDetails interface requries serializable
	 */
	private static final long serialVersionUID = -7048499611717840933L;

	private Contact con;
	private File directory;
	private LocalDate lastLogin;
	private Integer loginCount;
	private String password;
	private Boolean passwordInvalidated;
	private Date passwordInvalidatedAt;
	private LocalDate registeredDate;
	private String securityAnswer;
	private String securityQuestion;
	private String username;
	private Boolean webAware;
	private Boolean webTermsAccepted;
	private List<org.trescal.cwms.core.userright.entity.userrole.UserRole> userRoles;
	private Boolean webUser;

	public User() {
		this.webAware = Boolean.FALSE;
		this.webTermsAccepted = Boolean.FALSE;
		this.webUser = Boolean.TRUE;
		this.passwordInvalidated = Boolean.FALSE; 
	}

	public User(String username, String password, Boolean webUser) {
		this.username = username;
		this.password = password;
		this.passwordInvalidated = Boolean.FALSE;
		this.webAware = Boolean.FALSE;
		this.webTermsAccepted = Boolean.FALSE;
		this.webUser = webUser;
		this.registeredDate = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
	}

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "personid")
	public Contact getCon() {
		return this.con;
	}

	@Override
	@Transient
	public Contact getDefaultContact() {
		return this.con;
	}

	@Transient
	@Override
	public File getDirectory() {
		return this.directory;
	}

	@Override
	@Transient
	public String getIdentifier() {
		return this.username;
	}

	@Column(name = "last_login", columnDefinition = "date")
	public LocalDate getLastLogin() {
		return this.lastLogin;
	}

	@Type(type = "int")
	@Column(name = "logincount")
	public Integer getLoginCount() {
		return this.loginCount;
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity() {
		return null;
	}

	// For UserDetails Interface
	@Override
	@Column(name = "password", columnDefinition = "nvarchar(256)", nullable = false)
	public String getPassword() {
		return this.password;
	}

	public void setLastLogin(LocalDate lastLogin) {
		this.lastLogin = lastLogin;
	}

	@Column(name = "securea", length = 20)
	public String getSecurityAnswer() {
		return this.securityAnswer;
	}

	@Column(name = "secureq", length = 200)
	public String getSecurityQuestion() {
		return this.securityQuestion;
	}

	@Override
	@Transient
	public List<Email> getSentEmails() {
		return null;
	}

	// For UserDetails Interface
	@Override
	@Id
	@Column(name = "username", length = 20)
	public String getUsername() {
		return this.username;
	}


	@OneToMany(mappedBy = "user")
	public List<org.trescal.cwms.core.userright.entity.userrole.UserRole> getUserRoles() {
		return userRoles;
	}

	@Override
	@Transient
	public boolean isAccountsEmail() {
		return false;
	}

	@NotNull
	@Column(name = "webaware", nullable = false, columnDefinition = "tinyint")
	public Boolean getWebAware() {
		return this.webAware;
	}

	@NotNull
	@Column(name = "webtermsaccepted", nullable = false, columnDefinition = "tinyint")
	public Boolean getWebTermsAccepted() {
		return this.webTermsAccepted;
	}
	
	@NotNull
	@Column(name = "webuser", nullable = false, columnDefinition = "tinyint")
	public Boolean getWebUser() {
		return this.webUser;
	}

	@Column(name ="passwordInvalidated", nullable = false)
	public Boolean getPasswordInvalidated() {
		return passwordInvalidated;
	}
	public Date getPasswordInvalidatedAt() {
		return passwordInvalidatedAt;
	}

	public void setCon(Contact con) {
		this.con = con;
	}

	@Override
	public void setDirectory(File file) {
		this.directory = file;
	}

	@Column(name = "registered_date", columnDefinition = "date")
	public LocalDate getRegisteredDate() {
		return this.registeredDate;
	}

	public void setLoginCount(Integer loginCount) {
		this.loginCount = loginCount;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setRegisteredDate(LocalDate registeredDate) {
		this.registeredDate = registeredDate;
	}

	public void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}

	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}

	@Override
	public void setSentEmails(List<Email> emails) {

	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setUserRoles(List<org.trescal.cwms.core.userright.entity.userrole.UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public void setWebAware(Boolean webAware) {
		this.webAware = webAware;
	}

	public void setWebTermsAccepted(Boolean webTermsAccepted) {
		this.webTermsAccepted = webTermsAccepted;
	}

	public void setWebUser(Boolean webUser) {
		this.webUser = webUser;
	}
	
	// For UserDetails Interface
	@Transient
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// New version 
		return this.getUserRoles().stream().flatMap(ur->ur.getRole().getGroups().stream()).flatMap(grp->grp.getPermissions().stream()).distinct().collect(Collectors.toList());
	}

	// For UserDetails Interface
	@Transient
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	// For UserDetails Interface
	@Transient
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	// For UserDetails Interface
	@Transient
	@Override
	public boolean isCredentialsNonExpired() {
		return !getPasswordInvalidated();
	}

	// For UserDetails Interface
	@Transient
	@Override
	public boolean isEnabled() {
		return true;
	}



	public void setPasswordInvalidated(Boolean passwordInvalidated) {
		this.passwordInvalidated = passwordInvalidated;
	}

	public void setPasswordInvalidatedAt(Date passwordInvalidatedAt) {
		this.passwordInvalidatedAt = passwordInvalidatedAt;
	}
}