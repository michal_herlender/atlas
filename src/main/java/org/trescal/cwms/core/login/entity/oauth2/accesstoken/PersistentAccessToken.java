package org.trescal.cwms.core.login.entity.oauth2.accesstoken;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.login.entity.oauth2.PersistentToken;

/**
 * Implementation of OAuth2 access token persisted in database 
 * 2019-09-17
 */
@Entity
@Table(name="oauth2accesstoken", uniqueConstraints={
		@UniqueConstraint(columnNames="tokenid", name="UK_oauth2accesstoken_tokenid"),
		@UniqueConstraint(columnNames="refreshtoken", name="UK_oauth2accesstoken_refreshtoken")
	})
public class PersistentAccessToken extends PersistentToken {

	private int id;
    private String username;
    private String clientId;
	private String authenticationId;
    private String refreshToken;
    
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@Type(type = "int")
	public int getId() {
		return id;
	}
	
	@NotNull
	@Size(max=255)
	@Column(name="username", nullable=false, length=255, columnDefinition="varchar(255)")
	public String getUsername() {
		return username;
	}

	@NotNull
	@Size(max=255)
	@Column(name="clientid", nullable=false, length=255, columnDefinition="varchar(255)")
	public String getClientId() {
		return clientId;
	}

	@NotNull
	@Size(max=255)
	@Column(name="authenticationid", nullable=false, length=255, columnDefinition="varchar(255)")
	public String getAuthenticationId() {
		return authenticationId;
	}
	
	@NotNull
	@Size(max=255)
	@Column(name="refreshtoken", nullable=false, length=255, columnDefinition="varchar(255)")
	public String getRefreshToken() {
		return refreshToken;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public void setAuthenticationId(String authenticationId) {
		this.authenticationId = authenticationId;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}	
}
