package org.trescal.cwms.core.login.form;

import java.util.List;

import javax.validation.Valid;

import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;

public class UserGroupPropertyForm
{
	private boolean add;
	private int coid;
	private List<UserGroupProperty> properties;
	private UserGroup ug;

	private String[] valueid;

	public int getCoid()
	{
		return this.coid;
	}

	public List<UserGroupProperty> getProperties()
	{
		return this.properties;
	}
	
	@Valid
	public UserGroup getUg()
	{
		return this.ug;
	}

	public String[] getValueid()
	{
		return this.valueid;
	}

	/**
	 * @return the add
	 */
	public boolean isAdd()
	{
		return this.add;
	}

	/**
	 * @param add the add to set
	 */
	public void setAdd(boolean add)
	{
		this.add = add;
	}

	public void setCoid(int coid)
	{
		this.coid = coid;
	}

	public void setProperties(List<UserGroupProperty> properties)
	{
		this.properties = properties;
	}

	public void setUg(UserGroup ug)
	{
		this.ug = ug;
	}

	public void setValueid(String[] valueid)
	{
		this.valueid = valueid;
	}

}
