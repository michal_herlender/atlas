package org.trescal.cwms.core.login.listener;

import java.util.Properties;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.authentication.DefaultAuthenticationEventPublisher;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.login.exception.BadCredentialsExceptionWithLimitWarning;
import org.trescal.cwms.core.login.exception.MaxLoginAttemptException;


@Component
public class CrocodileAuthenticationEventPublisher extends DefaultAuthenticationEventPublisher {
	
	public CrocodileAuthenticationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		super(applicationEventPublisher);
		Properties properties = new Properties();
		properties.setProperty(MaxLoginAttemptException.class.getName(), AuthenticationFailureBadCredentialsEvent.class.getName());
		properties.setProperty(BadCredentialsExceptionWithLimitWarning.class.getName(), AuthenticationFailureBadCredentialsEvent.class.getName());
		super.setAdditionalExceptionMappings(properties);
	}
}
