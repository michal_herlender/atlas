package org.trescal.cwms.core.login.entity.oauth2.accesstoken.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.login.entity.oauth2.accesstoken.PersistentAccessToken;

public interface PersistentAccessTokenDao extends BaseDao<PersistentAccessToken, Integer> {
	PersistentAccessToken findSingle(String authenticationId, String tokenId, String refreshToken);
	List<PersistentAccessToken> findByClientIdAndUserName(String clientId, String username);
}
