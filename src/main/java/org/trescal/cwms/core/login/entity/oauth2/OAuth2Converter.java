package org.trescal.cwms.core.login.entity.oauth2;

import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.common.util.SerializationUtils;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

/**
 * 
 * Wrapper for Spring Framework SerializationUtils for OAuth2 
 * 
 * @author galen
 *
 */
public class OAuth2Converter {
	
	private static final Logger logger = LoggerFactory.getLogger(OAuth2Converter.class); 
	
	public static String serialize(OAuth2AccessToken object) {
		return serializeObject(object);
	}
	
	public static String serialize(OAuth2RefreshToken object) {
		return serializeObject(object);
	}
	
	public static String serialize(OAuth2Authentication object) {
		return serializeObject(object);
	}
	
	public static OAuth2AccessToken deserializeOAuth2AccessToken(String encodedObject) {
		return (OAuth2AccessToken) deserialize(encodedObject);
	}
	
	public static OAuth2RefreshToken deserializeOAuth2RefreshToken(String encodedObject) {
		return (OAuth2RefreshToken) deserialize(encodedObject);
	}
	
	public static OAuth2Authentication deserializeOAuth2Authentication(String encodedObject) {
		return (OAuth2Authentication) deserialize(encodedObject);
	}
	
	private static String serializeObject(Object object) {
        try {
            byte[] bytes = SerializationUtils.serialize(object);
            return Base64.getEncoder().encodeToString(bytes);
        } catch(Exception e) {
            logger.error("Exception in serialization", e);
            throw e;
        }
    }
 
    private static Object deserialize(String encodedObject) {
        try {
            byte[] bytes = Base64.getDecoder().decode(encodedObject);
            return SerializationUtils.deserialize(bytes);
        } catch(Exception e) {
            logger.error("Exception in deserialization", e);
            throw e;
        }
    }

}
