package org.trescal.cwms.core.login.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.login.entity.login.db.LoginAttemptService;

import lombok.extern.slf4j.Slf4j;


@Component
@Slf4j
public class AuthenticationFailureListener implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {

	
	@Autowired private LoginAttemptService loginAttemptService;
	
	@Override
	public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent event) {
		String principal = (String) event.getAuthentication().getPrincipal();
		loginAttemptService.loginFailed(principal);
		log.debug("wrong credentials :"+event.getAuthentication().getCredentials()+":"+principal+":"+loginAttemptService.isWarningActive(principal));
		log.debug("is user blocked"+loginAttemptService.isBlocked(principal));
	}

}
