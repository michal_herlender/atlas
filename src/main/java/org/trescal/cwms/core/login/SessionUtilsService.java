package org.trescal.cwms.core.login;

import javax.servlet.http.HttpServletRequest;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.entity.user.User;

public interface SessionUtilsService {

	/**
	 * Returns the {@link Contact} entity of the {@link User} currently logged in
	 * using the Spring Security API.
	 * 
	 * @return the {@link Contact} or null if none can be found.
	 */
	public Contact getCurrentContact();

	/**
	 * Sets the given {@link Contact} as the currently logged in {@link Contact} .
	 * This function is only intended for use after an update of the currently
	 * logged in {@link Contact}.
	 * 
	 * @param request {@link HttpServletRequest}, if null an dwr request is used
	 *                instead.
	 * @param con     the {@link Contact} to set.
	 */
	public void setCurrentContact(HttpServletRequest request, Contact con);
}