package org.trescal.cwms.core.login.form;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class AddUserFormValidator extends AbstractBeanValidator {

	@Autowired
	private UserService userService;

	@Override
	public boolean supports(Class<?> clazz) {
		return AddUserForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		AddUserForm form = (AddUserForm) target;
		if (form.getUsername().indexOf(' ') > -1)
			errors.rejectValue("username", "error.space.character.notallowed");
		if (form.getPassword().indexOf(' ') > -1)
			errors.rejectValue("password", "error.space.character.notallowed");
		if (StringUtils.hasLength(form.getUsername())) {
			super.validate(form, errors, AddUserForm.UsernameGroup.class);
			// Check that the user name is not already taken
			User user = this.userService.get(form.getUsername());
			if (user != null)
				errors.rejectValue("username", "error.contact.username", new String[] { user.getCon().getName() },
						"The contact {0} already has this username.  Please select another.");
		}
		/* If password is empty, then a valid password will be assigned later. */
		if (StringUtils.hasLength(form.getPassword())) {
			ResultWrapper validPassword = userService.passwordValid(form.getPassword());
			if (!validPassword.isSuccess())
				errors.rejectValue("password", validPassword.getCode(), validPassword.getMessage());
		}
	}
}