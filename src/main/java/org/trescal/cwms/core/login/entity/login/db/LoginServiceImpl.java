package org.trescal.cwms.core.login.entity.login.db;

import org.apache.commons.net.util.SubnetUtils;
import org.apache.commons.net.util.SubnetUtils.SubnetInfo;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.entity.login.IPSubnet;
import org.trescal.cwms.core.login.entity.login.Login;
import org.trescal.cwms.core.tools.MySubnetUtils;

import javax.servlet.http.HttpServletRequest;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class LoginServiceImpl implements LoginService {
	private LoginDao loginDao;

	public IPSubnet getConnectionTypeFromIPAddress(HttpServletRequest request) {
		String ipAddress = request.getRemoteAddr();
		IPSubnet connectionType = null;

		try
		{
			InetAddress inetAddress = InetAddress.getByName(ipAddress);

			// Only if IP address is in IPv4 form should we try to match this to
			// our internal subnet masks. If IP address is in IPv6 form, just
			// assume the user is external for now until we know more about
			// these IPs!
			if (inetAddress instanceof Inet4Address) {
				// find which subnet the IP address is in
				for (IPSubnet ips : IPSubnet.values()) {
					SubnetInfo sub = new SubnetUtils(ips.getIpSubnet()).getInfo();
					if (MySubnetUtils.isInRange(sub, ipAddress)) {
						connectionType = ips;
						break;
					}
				}
			}
		}
		catch (UnknownHostException e)
		{
			e.printStackTrace();
		}

		return connectionType;
	}

	public void insertLogin(Login login)
	{
		this.loginDao.persist(login);
	}

	public boolean isInternalUser(HttpServletRequest request)
	{
		boolean internalUser = false;
		String ipAddress = request.getRemoteAddr();

		try
		{
			InetAddress inetAddress = InetAddress.getByName(ipAddress);

			// Only if IP address is in IPv4 form should we try to match this to
			// our internal subnet masks. If IP address is in IPv6 form, just
			// assume the user is external for now until we know more about
			// these IPs!
			if (inetAddress instanceof Inet4Address) {
				// build collection of internal subnet masks to check from enum
				Collection<SubnetInfo> subnets = new ArrayList<>();
				for (IPSubnet ips : IPSubnet.values()) {
					subnets.add(new SubnetUtils(ips.getIpSubnet()).getInfo());
				}

				// check to see if the IP is in any of the ranges
				for (SubnetInfo subnet : subnets) {
					if (MySubnetUtils.isInRange(subnet, ipAddress)) {
						internalUser = true;
						break;
					}
				}
			}
		}
		catch (UnknownHostException e)
		{
			e.printStackTrace();
		}

		return internalUser;
	}

	public void recordWebLogin(Contact con, boolean logFileCreated, String logFilePath)
	{
		Login login = new Login();

		login.setCon(con);
		login.setLoginTime(new Date());
		login.setWebLogCreated(logFileCreated);
		login.setWebLogPath(logFilePath);

		this.insertLogin(login);
	}

	public void setLoginDao(LoginDao loginDao)
	{
		this.loginDao = loginDao;
	}
}