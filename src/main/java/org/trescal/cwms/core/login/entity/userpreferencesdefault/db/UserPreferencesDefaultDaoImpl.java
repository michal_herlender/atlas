package org.trescal.cwms.core.login.entity.userpreferencesdefault.db;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.login.entity.userpreferencesdefault.UserPreferencesDefault;
import org.trescal.cwms.core.login.entity.userpreferencesdefault.UserPreferencesDefault_;

@Repository
public class UserPreferencesDefaultDaoImpl 
	extends BaseDaoImpl<UserPreferencesDefault, Integer> 
	implements UserPreferencesDefaultDao {

	@Override
	protected Class<UserPreferencesDefault> getEntity() {
		return UserPreferencesDefault.class;
	}
	
	/**
	 * This returns all @UserPreferencesDefault entries that are appropriate for use with 
	 * the specified addressid
	 */
	@Override
	public List<UserPreferencesDefault> getAllForAddress(int addressid) {
		return getResultList(cb -> {
			CriteriaQuery<UserPreferencesDefault> cq = cb.createQuery(UserPreferencesDefault.class);
			Root<UserPreferencesDefault> root = cq.from(UserPreferencesDefault.class);
			Join<UserPreferencesDefault, Address> address = root.join(UserPreferencesDefault_.address, JoinType.LEFT);
			Join<UserPreferencesDefault, Subdiv> subdiv = root.join(UserPreferencesDefault_.subdiv, JoinType.LEFT);
			Join<UserPreferencesDefault, Company> company = root.join(UserPreferencesDefault_.company, JoinType.LEFT);
			
			Subquery<Integer> sq_subdiv = cq.subquery(Integer.class);
			Root<Address> root_sq_subdiv = sq_subdiv.from(Address.class);
			Join<Address,Subdiv> join_sq_subdiv = root_sq_subdiv.join(Address_.sub, JoinType.INNER);
			sq_subdiv.where(cb.equal(root_sq_subdiv.get(Address_.addrid), addressid));
			sq_subdiv.select(join_sq_subdiv.get(Subdiv_.subdivid));
			
			Subquery<Integer> sq_company = cq.subquery(Integer.class);
			Root<Address> root_sq_company = sq_company.from(Address.class);
			Join<Address,Subdiv> join_sq_company_one = root_sq_company.join(Address_.sub, JoinType.INNER);
			Join<Subdiv,Company> join_sq_company_two = join_sq_company_one.join(Subdiv_.comp, JoinType.INNER);
			sq_company.where(cb.equal(root_sq_company.get(Address_.addrid), addressid));
			sq_company.select(join_sq_company_two.get(Company_.coid));
			
			Predicate clauses = cb.disjunction();
			clauses.getExpressions().add(cb.isTrue(root.get(UserPreferencesDefault_.globalDefault)));
			clauses.getExpressions().add(cb.equal(address.get(Address_.addrid), addressid));
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.subdivid), sq_subdiv));
			clauses.getExpressions().add(cb.equal(company.get(Company_.coid), sq_company));
			
			cq.where(clauses);
			
			return cq;
		});
	}

}
