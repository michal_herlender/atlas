package org.trescal.cwms.core.login.entity.userpreferences.db;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.User_;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences_;

@Repository("UserPreferencesDao")
public class UserPreferencesDaoImpl extends BaseDaoImpl<UserPreferences, Integer> implements UserPreferencesDao
{
	@Override
	protected Class<UserPreferences> getEntity() {
		return UserPreferences.class;
	}
	
	@Override
	public UserPreferences findUserPreferencesByContact(int contactid)
	{
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<UserPreferences> cq = cb.createQuery(UserPreferences.class);
		Root<UserPreferences> root = cq.from(UserPreferences.class);
		Join<UserPreferences, Contact> contact = root.join(UserPreferences_.contact);

		cq.where(cb.equal(contact.get(Contact_.personid), contactid));
		
		List<UserPreferences> result = getEntityManager().createQuery(cq).getResultList();
		return result.isEmpty() ? null : result.get(0);
	}

	@Override
	public UserPreferences findUserPreferencesByUsername(String username)
	{
		return getFirstResult(cb -> {
			CriteriaQuery<UserPreferences> cq = cb.createQuery(UserPreferences.class);
			Root<UserPreferences> root = cq.from(UserPreferences.class);
			Join<UserPreferences, Contact> contact = root.join(UserPreferences_.contact, JoinType.INNER);
			Join<Contact, User> user = contact.join(Contact_.user, JoinType.INNER);
			
			cq.where(cb.equal(user.get(User_.username), username));
			
			return cq;
		}).orElse(null);
	}

	@Override
	public List<UserPreferences> findBySubdiv(int subdivId) {
		CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<UserPreferences> cq = cb.createQuery(UserPreferences.class);
		Root<UserPreferences> root = cq.from(UserPreferences.class);
		Join<UserPreferences, Contact> contact = root.join(UserPreferences_.contact);
		Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
		
		cq.where(cb.equal(subdiv.get(Subdiv_.subdivid), subdivId),
			cb.isTrue(contact.get(Contact_.active)));
		cq.orderBy(cb.asc(contact.get(Contact_.firstName)), 
			cb.asc(contact.get(Contact_.lastName)), 
			cb.asc(contact.get(Contact_.personid)));
		
		return getEntityManager().createQuery(cq).getResultList();		
	}
}