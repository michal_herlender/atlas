package org.trescal.cwms.core.login.entity.userpreferencesdefault;

import java.util.Comparator;

/**
 * Compares UserPreferencesDefault based on their hierarchy, and then on identity if otherwise matching
 * 
 * Order is: 
 *  a) Address level
 *  b) Subdiv level
 *  c) Company level
 *  d) Global default level
 *  
 *  (then compare by id if at same level)
 * 
 * This is intended for obtaining the best default (among an appropriate set) and not for general display purposes (of a larger set) 
 * so is otherwise a mininal implementation (no detailed sorting, or consideration of unassigned IDs)
 * 
 * The best default will have the highest value, so if used as is, the last element in an ordered collection should be the one selected for use
 *
 */
public class UserPreferencesDefaultComparator implements Comparator<UserPreferencesDefault> {

	@Override
	public int compare(UserPreferencesDefault default0, UserPreferencesDefault default1) {
		int result = Integer.compare(getPriority(default0), getPriority(default1));
		if (result == 0)
			result = Integer.compare(default0.getId(), default1.getId());
		return result;
	}
	
	public Integer getPriority(UserPreferencesDefault instance) {
		// Fallback value
		int result = 0;
		
		// Assuming proper configuration of the database, each default should satisfy one (and only one) of the following conditions
		if (instance.getAddress() != null)
			result = 4;
		else if (instance.getSubdiv() != null)
			result = 3;
		else if (instance.getCompany() != null)
			result = 2;
		else if (instance.getGlobalDefault())
			result = 1;
		
		return result;
	}

}
