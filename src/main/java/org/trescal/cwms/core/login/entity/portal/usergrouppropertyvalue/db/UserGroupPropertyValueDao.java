package org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.UserGroupPropertyValue;
import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;

/**
 * Created by scottchamberlain on 31/10/2016.
 */
public interface UserGroupPropertyValueDao extends BaseDao<UserGroupPropertyValue, Integer> {

    UserGroupPropertyValue getValue(UserGroup userGroup, UserGroupProperty property);

}
