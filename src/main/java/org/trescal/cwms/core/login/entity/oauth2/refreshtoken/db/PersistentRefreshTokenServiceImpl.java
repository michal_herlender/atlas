package org.trescal.cwms.core.login.entity.oauth2.refreshtoken.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.login.entity.oauth2.refreshtoken.PersistentRefreshToken;

@Service
public class PersistentRefreshTokenServiceImpl extends BaseServiceImpl<PersistentRefreshToken, Integer> 
	implements PersistentRefreshTokenService {

	@Autowired
	private PersistentRefreshTokenDao baseDao;
	
	@Override
	protected BaseDao<PersistentRefreshToken, Integer> getBaseDao() {
		return baseDao;
	}

	@Override
	public PersistentRefreshToken findByTokenId(String tokenId) {
		return baseDao.findByTokenId(tokenId);
	}

}
