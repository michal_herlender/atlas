package org.trescal.cwms.core.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;

@Service("SessionUtilsService")
public class SessionUtilsServiceImpl implements SessionUtilsService, ApplicationContextAware {

	@Autowired
	private UserService userServ;

	@Override
	public Contact getCurrentContact() {
		Contact con = null;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			Object obj = auth.getPrincipal();
//			if (obj instanceof UserDetails) {
//				UserDetails details = (UserDetails) obj;
//				User user = this.userServ.get(details.getUsername());
//				if (user != null && user.getCon() != null)
//					con = user.getCon();
//			}
			if (obj instanceof String) {
				String username = (String) obj;
				User user = this.userServ.get(username);
				if (user != null && user.getCon() != null)
					con = user.getCon();
			}
		}
		return con;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
	}

	@Override
	public void setCurrentContact(HttpServletRequest request, Contact con) {
		WebContext wctx = WebContextFactory.get();
		if (request != null) {
			HttpSession session = request.getSession(true);
			if (session.getAttribute(Constants.SESSION_ATTRIBUTE_CONTACT) != null) {
				session.setAttribute(Constants.SESSION_ATTRIBUTE_CONTACT, con);
			}
		} else if (wctx != null) {
			wctx.getHttpServletRequest().getSession(true).setAttribute(Constants.SESSION_ATTRIBUTE_CONTACT, con);
		}
	}
}