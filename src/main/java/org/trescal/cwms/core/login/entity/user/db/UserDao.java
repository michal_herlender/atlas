package org.trescal.cwms.core.login.entity.user.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.exception.MultipleMatchingUsersException;
import org.trescal.cwms.core.login.dto.UserLoginReportDTO;
import org.trescal.cwms.core.login.entity.user.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface UserDao extends BaseDao<User, String> {

    /*
     * Method that does full eager load - used in DWR calls
     */
    User getEagerLoad(String userName);

    /**
     * Find the user by primary key of the corresponding contact
     *
     * @param personId primary key of the contact
     * @return user
     */
    User findByPersonId(Integer personId);

    List<User> findUsersRoleInternalForSubdiv(Subdiv subdiv);

    List<UserLoginReportDTO> getAllCompanyWebUserLoginsForPeriod(int coid, LocalDate dateFrom, LocalDate dateTo);

    List<User> webFindUserViaEmail(String email) throws MultipleMatchingUsersException;

    Optional<User> invalidatePassword(String userName);
}