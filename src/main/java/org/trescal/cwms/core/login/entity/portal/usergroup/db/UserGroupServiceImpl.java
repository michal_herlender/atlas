package org.trescal.cwms.core.login.entity.portal.usergroup.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;
import org.trescal.cwms.core.login.entity.portal.UserScope;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.db.UserGroupPropertyValueService;

@Service("UserGroupService")
public class UserGroupServiceImpl implements UserGroupService
{
	@Autowired
	private ContactService conServ;
	@Autowired
	private UserGroupDao ugDao;
	@Autowired
	private UserGroupPropertyValueService ugpvService;

	public void deleteUserGroup(UserGroup ug)
	{
		// don't allow deletion of the system default UserGroup
		if (!ug.isSystemDefault())
		{
			UserGroup defGroup = this.getDefaultGroup();

			// reassign Contacts in this group back to the system default
			List<Contact> contacts = this.conServ.getContactsInUserGroup(ug.getGroupid());
			for (Contact contact : contacts)
			{
				contact.setUsergroup(defGroup);
				// this.conServ.updateContact(contact);
			}
			this.conServ.updateAll(contacts);

			// delete the Group
			this.ugDao.remove(this.findGroup(ug.getGroupid()));
		}
	}

	public UserGroup findGroup(int groupid)
	{
		return this.ugDao.find(groupid);
	}


	public List<UserGroup> getAllUserGroups()
	{
		return this.ugDao.findAll();
	}

	public List<UserGroup> getSortedUserGroups(Company company)
	{
		return this.ugDao.getSortedUserGroups(company);
	}

	@Override
	public UserGroup getDefaultGroup()
	{
		return this.ugDao.getDefaultGroup();
	}

	public UserGroupDao getUgDao()
	{
		return this.ugDao;
	}

	public void insertUserGroup(UserGroup ug)
	{
		this.ugDao.persist(ug);
	}



	public void updateUserGroup(UserGroup ug)
	{
		this.ugDao.update(ug);
	}

	@Override 
	public Boolean canUserViewThisInstrument(Contact contact, Instrument instrument) {
		UserScope scope = ugpvService.getValue(contact, UserGroupProperty.VIEW_LEVEL);
		return this.checkScope(scope,instrument,contact);
	}

	@Override
	public Boolean canUserViewThisJobItem(Contact contact, JobItem jobitem) {
		UserScope scope = ugpvService.getValue(contact, UserGroupProperty.VIEW_LEVEL);
		Instrument instrument = jobitem.getInst();
		return this.checkScope(scope,instrument,contact);
	}

	@Override
	public Boolean canUserUpdateThisInstrument(Contact contact, Instrument instrument) {
		UserScope scope = ugpvService.getValue(contact, UserGroupProperty.UPDATE_INSTRUMENT);
		return this.checkScope(scope,instrument,contact);
	}

	@Override
	public Boolean canUserCreateCalibrationForThisInstrument(Contact contact, Instrument instrument) {
		if(instrument.getCustomerManaged()){
			UserScope scope = ugpvService.getValue(contact, UserGroupProperty.CREATE_CALIBRATION);
			return this.checkScope(scope,instrument,contact);
		} else {
			return false;
		}
	}
	
	@Override
	public Boolean canUserValidateCertificateForThisInstrument(Contact contact, Instrument instrument) {
		UserScope scope = ugpvService.getValue(contact, UserGroupProperty.VALIDATE_CERTIFICATE);
		return this.checkScope(scope,instrument,contact);
	}
	
	private Boolean checkScope(UserScope scope, Instrument instrument, Contact contact){
		switch(scope) {
		case COMPANY: return instrument.getComp().equals(contact.getSub().getComp());
		case SUBDIV: return instrument.getAdd().getSub().equals(contact.getSub());
		case ADDRESS: return instrument.getAdd().equals(contact.getDefAddress());
		case CONTACT: return instrument.getCon().equals(contact);
		default: return false;
		}
	}
}
