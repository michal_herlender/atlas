package org.trescal.cwms.core.login.entity.oauth2.refreshtoken.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.login.entity.oauth2.refreshtoken.PersistentRefreshToken;

public interface PersistentRefreshTokenDao extends BaseDao<PersistentRefreshToken, Integer> {
	PersistentRefreshToken findByTokenId(String tokenId);
}
