package org.trescal.cwms.core.login.entity.user.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.company.entity.address.Address_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.login.dto.UserLoginReportDTO;
import org.trescal.cwms.core.login.entity.login.Login;
import org.trescal.cwms.core.login.entity.login.Login_;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup_;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.User_;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences_;
import org.trescal.cwms.core.userright.entity.userrole.UserRole;
import org.trescal.cwms.core.userright.entity.userrole.UserRole_;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.trescal.cwms.core.tools.DateTools.dateFromLocalDate;

@Repository("UserDao")
public class UserDaoImpl extends BaseDaoImpl<User, String> implements UserDao {

	@Override
	protected Class<User> getEntity() {
		return User.class;
	}

	@Override
	public User findByPersonId(Integer personId) {
		return getSingleResult(cb -> {
			CriteriaQuery<User> cq = cb.createQuery(User.class);
			Root<Contact> contact = cq.from(Contact.class);
			Join<Contact, User> user = contact.join(Contact_.user);
			cq.where(cb.equal(contact, personId));
			cq.select(user);
			return cq;
		});
	}

	@Override
	public List<User> findUsersRoleInternalForSubdiv(Subdiv subdiv) {
		return getResultList(cb -> {
			CriteriaQuery<User> cq = cb.createQuery(User.class);
			Root<User> user = cq.from(User.class);
			Join<User, UserRole> userRole = user.join(User_.userRoles);
			Join<User, Contact> contact = user.join(User_.con);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(userRole.get(UserRole_.organisation), subdiv));
			clauses.getExpressions().add(cb.isTrue(contact.get(Contact_.active)));
			cq.distinct(true);
			cq.where(clauses);
			return cq.select(user);
		});
	}

	/**
	 * This method eagerly load ALL fields that are going to be accessed at ANY time
	 * by referencing the contact entity via DWR.
	 */
	@Override
	public User getEagerLoad(String username) {
		return getFirstResult(cb -> {
			CriteriaQuery<User> cq = cb.createQuery(User.class);
			Root<User> user = cq.from(User.class);
			user.fetch(User_.userRoles, JoinType.LEFT).fetch(UserRole_.role, JoinType.LEFT);
			Fetch<User, Contact> contact = user.fetch(User_.con, JoinType.LEFT);
			contact.fetch(Contact_.defAddress, JoinType.LEFT).fetch(Address_.locations, JoinType.LEFT);
			contact.fetch(Contact_.defLocation, JoinType.LEFT);
			contact.fetch(Contact_.sub).fetch(Subdiv_.comp);
			contact.fetch(Contact_.userPreferences, JoinType.LEFT).fetch(UserPreferences_.userInstructionTypes,
					JoinType.LEFT);
			cq.where(cb.equal(user.get(User_.username), username));
			return cq;
		}).orElse(null);
	}

	@Override
	public List<UserLoginReportDTO> getAllCompanyWebUserLoginsForPeriod(int coid, LocalDate fromDate, LocalDate toDate) {
		return getResultList(cb -> {
			CriteriaQuery<UserLoginReportDTO> cq = cb.createQuery(UserLoginReportDTO.class);
			Root<User> user = cq.from(User.class);
			Join<User, Contact> contact = user.join(User_.con);
			Join<Contact, UserGroup> userGroup = contact.join(Contact_.usergroup, JoinType.LEFT);
			Subquery<Long> loginCountSq = cq.subquery(Long.class);
			Root<Login> login = loginCountSq.from(Login.class);
			Predicate loginClauses = cb.conjunction();
			loginClauses.getExpressions()
				.add(cb.between(login.get(Login_.loginTime), dateFromLocalDate(fromDate), dateFromLocalDate(toDate.plusDays(1))));
			loginClauses.getExpressions().add(cb.equal(login.get(Login_.con), contact));
			loginCountSq.where(loginClauses);
			loginCountSq.select(cb.count(login));
			Expression<Long> loginCount = loginCountSq.getSelection();
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(subdiv.get(Subdiv_.comp), coid));
			clauses.getExpressions().add(cb.isTrue(contact.get(Contact_.active)));
			cq.where(clauses);
			cq.groupBy(user.get(User_.username), contact.get(Contact_.personid), userGroup.get(UserGroup_.name),
					subdiv.get(Subdiv_.subname));
			cq.select(cb.construct(UserLoginReportDTO.class, user.get(User_.username), userGroup.get(UserGroup_.name),
					subdiv.get(Subdiv_.subname), loginCount));
			return cq;
		});
	}

	@Override
	public List<User> webFindUserViaEmail(String email) {
		return getResultList(cb -> {
			CriteriaQuery<User> cq = cb.createQuery(User.class);
			Root<User> user = cq.from(User.class);
			Join<User, Contact> contact = user.join(User_.con);
			cq.where(cb.equal(cb.lower(contact.get(Contact_.email)), email.toLowerCase()));
			return cq;
		});
	}

	@Override
	public Optional<User> invalidatePassword(String userName) {
		User user = find(userName);
		if(user == null) return Optional.empty();
		user.setPasswordInvalidated(true);
		user.setPasswordInvalidatedAt(new Date());
		return Optional.of(user);
	}
	
}