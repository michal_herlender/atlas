package org.trescal.cwms.core.login.entity.portal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.EnumSet;

/**
 * enum to hold organisation levels that various
 * portal user group properties can use.
 * Created by scottchamberlain on 31/10/2016.
 */
public enum UserScope {
    NONE("usergrouppropertyorglevel.none"),
    CONTACT("usergrouppropertyorglevel.contact"),
    ADDRESS("usergrouppropertyorglevel.address"),
    SUBDIV("usergrouppropertyorglevel.subdiv"),
    COMPANY("usergrouppropertyorglevel.company");

    private final String nameCode;
    private MessageSource messageSource;

    UserScope(String nameCode){
        this.nameCode = nameCode;
    }

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Component
    public static class UserScopeMessageSourceInjector
    {
        @Autowired
        private MessageSource messageSource;

        @PostConstruct
        public void postConstruct() {
            for (UserScope orgLevel : EnumSet.allOf(UserScope.class)) {
                orgLevel.setMessageSource(messageSource);
            }
        }
    }

    public String getName() {
        return messageSource.getMessage(nameCode, null, LocaleContextHolder.getLocale());
    }

}
