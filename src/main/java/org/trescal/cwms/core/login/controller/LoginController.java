package org.trescal.cwms.core.login.controller;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.util.WebUtils;
import org.trescal.cwms.core.login.dto.ResetPasswordIn;
import org.trescal.cwms.core.login.dto.ResetPasswordOut;
import org.trescal.cwms.core.login.entity.login.db.LoginAttemptService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;

/*
 * Not annotating this at present time - didn't want to use Intranet/Extranet error page as session (+contact) likely doesn't exist 
 */
@Controller // @LoginController (in future)
@Slf4j
public class LoginController {

	/*
	 * Expected to be an instance of Spring's AcceptHeaderLocaleResolver when
	 * running, replaced with MockLocaleResolver for test context Bean id is
	 * required so that test auto-wiring to work
	 */
	@Autowired
	@Qualifier("browserLocaleResolver")
	private LocaleResolver browserLocaleResolver;

	/*
	 * Expected to be an instance of Spring's SessionLocaleResolver when running,
	 * replaced with MockLocaleResolver for test context Bean id is required so that
	 * test auto-wiring to work
	 */
	@Autowired
	@Qualifier("localeResolver")
	private LocaleResolver sessionLocaleResolver;

	@Autowired
	private SupportedLocaleService supportedLocaleService;
	
	@Autowired private LoginAttemptService loginAttemptService;

	@Value("#{props['cwms.config.systemtype']}")
	private String systemType;
	
	@Value("#{props['cwms.config.forgottenPasswordPath']}")
	private String forgottenPasswordPath;
	
	@Value("${cwms.deployed.url}")
	private URL deployedURL;
	
	@ModelAttribute("systemType")
	public String supplySystemType() {
		return this.systemType;
	}
	
	@Autowired private UserService userService;
	
//	@Autowired TokenEndpoint tokenEndpoint;
	
	@RequestMapping("/login.htm")
	public ModelAndView handleRequest(@RequestParam(value = "login_error", required = false) String error,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		Locale browserLocale = browserLocaleResolver.resolveLocale(request);
		log.debug("browser locale: " + browserLocale.toString());
		Locale supportedLocale = this.supportedLocaleService.getBestSupportedLocale(browserLocale);
		log.debug("best supported locale: " + supportedLocale.toString());
		WebUtils.setSessionAttribute(request, Constants.SESSION_ATTRIBUTE_LOCALE, supportedLocale);
		val ub = UriComponentsBuilder.fromUri(deployedURL.toURI());
		ub.path("/").path(forgottenPasswordPath);
		val model = new HashMap<String, Object>();
		model.put("error", error);
		model.put("forgottenPasswordUrl", ub.build().toUri().toURL());
		return new ModelAndView("trescal/core/login/login", model);
	}

	@RequestMapping("/forgot-my-password")
	public ModelAndView handleResetRequest(HttpServletRequest request, HttpServletResponse resonse) {
		return new ModelAndView("trescal/core/login/forgotMyPassword",
				"resetForm", new ResetPasswordIn());
	}

	@RequestMapping(value = "/forgot-my-password-form.json", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	@ResponseBody public ResetPasswordOut handleResetPost(@ModelAttribute("resetForm") ResetPasswordIn form, Locale locale) {
		User user = userService.get(form.getUsername());
		val status = userService.generateResetPasswordToken(user, locale);
		return ResetPasswordOut.builder().message(status).username(form.getUsername()).build();
	}
	
	@RequestMapping("/reset-my-password")
	public ModelAndView resetMyPassword(@RequestParam String token) {
		try {
			val jwt = userService.decodeToke(token);
		loginAttemptService.resetPassword(jwt.getBody().get("username", String.class));
		return new ModelAndView("trescal/core/login/passwordReseted");
		}
		catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("trescal/core/login/passwordReseted", "error",true);
		}
	}
	
	/**
	 * Redirects requests to the root of the application to the login page This is
	 * so that the resources returned as part of /login.htm are excluded from Spring
	 * Security to appear to the user when unauthenticated.)
	 */
	@RequestMapping("/")
	public RedirectView handleRequest() {
		return new RedirectView("/login.htm", true);
	}
	
}