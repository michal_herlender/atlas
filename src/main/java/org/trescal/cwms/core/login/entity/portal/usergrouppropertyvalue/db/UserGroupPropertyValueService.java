package org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.entity.portal.UserScope;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.UserGroupPropertyValue;
import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;

/**
 * Created by scottchamberlain on 31/10/2016.
 */
public interface UserGroupPropertyValueService extends BaseService<UserGroupPropertyValue, Integer> {

    public UserScope getValue(Contact contact, UserGroupProperty userGroupProperty);

}
