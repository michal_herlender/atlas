package org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue;

import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;
import org.trescal.cwms.core.login.entity.portal.UserScope;
import org.trescal.cwms.core.login.entity.portal.UserGroupProperty;

import javax.persistence.*;

/**
 * Entity to store the relationship between a portal user group
 * and it's properties.
 * Created by scottchamberlain on 02/11/2016.
 */
@Entity
@Table(name = "usergrouppropertyvalue", uniqueConstraints = @UniqueConstraint(columnNames = {"groupid","property"}))
public class UserGroupPropertyValue {
    private UserGroup userGroup;
    private UserGroupProperty userGroupProperty;
    private UserScope scope;
    private Integer id;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() { return id; }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "groupid")
    public UserGroup getUserGroup() { return userGroup; }

    @Enumerated(EnumType.STRING)
    @Column(name = "value")
    public UserScope getScope() {
        return scope;
    }

    @Enumerated(EnumType.STRING)
    @Column(name="property")
    public UserGroupProperty getUserGroupProperty() {
        return userGroupProperty;
    }



    public void setUserGroupProperty(UserGroupProperty userGroupProperty) {
        this.userGroupProperty = userGroupProperty;
    }

    public void setScope(UserScope value) {
        this.scope = value;
    }

    public void setUserGroup(UserGroup userGroup) {
        this.userGroup = userGroup;
    }

    public void setId(Integer id) {this.id = id;}
}
