package org.trescal.cwms.core.login.exception;

import org.springframework.security.authentication.BadCredentialsException;

public class BadCredentialsExceptionWithLimitWarning extends BadCredentialsException {

	public BadCredentialsExceptionWithLimitWarning(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2857959902220417955L;

}
