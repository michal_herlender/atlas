package org.trescal.cwms.core.login.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.ldap.userdetails.DefaultLdapAuthoritiesPopulator;
//import org.acegisecurity.GrantedAuthority;
//import org.acegisecurity.ldap.InitialDirContextFactory;
//import org.acegisecurity.providers.ldap.populator.DefaultLdapAuthoritiesPopulator;
//import org.acegisecurity.userdetails.ldap.LdapUserDetails;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.userright.enums.Permission;

public class CustomLdapAuthoritiesPopulator extends DefaultLdapAuthoritiesPopulator
{
	private UserService userServ;

	public CustomLdapAuthoritiesPopulator(ContextSource contextSource, String groupSearchBase)
	{
		super(contextSource, groupSearchBase);
	}

	@Override
	protected Set<GrantedAuthority> getAdditionalRoles(DirContextOperations userData, String username)
	{
		Set<GrantedAuthority> authorities = null;
		User user = this.userServ.get(username);
		if (user != null)
		{
			authorities = new HashSet<GrantedAuthority>();
			List<Permission> authoritiesUser = user.getUserRoles().stream()
				//.filter(ur -> ur.getOrganisation().equals(subdiv))
				.flatMap(ur -> ur.getRole().getGroups().stream().flatMap(pg -> pg.getPermissions().stream()))
				.collect(Collectors.toList());
			authorities.addAll(authoritiesUser);
		}
		return authorities;
	}

	public void setUserServ(UserService userServ)
	{
		this.userServ = userServ;
	}
}