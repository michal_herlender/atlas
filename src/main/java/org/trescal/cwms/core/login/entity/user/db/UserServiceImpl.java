package org.trescal.cwms.core.login.entity.user.db;

import java.io.File;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.MultipleMatchingUsersException;
import org.trescal.cwms.core.login.dto.UserLoginReportDTO;
import org.trescal.cwms.core.login.entity.login.db.FailedLoginAttemptRepository;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.emailcontent.EmailContentDto;
import org.trescal.cwms.core.system.entity.emailcontent.core.EmailContent_ResetPasswordToken;
import org.trescal.cwms.core.tools.LoginGenerator;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.val;
import lombok.extern.slf4j.Slf4j;

@Service("userService")
@Slf4j
public class UserServiceImpl implements UserService {

	@Autowired
	private ContactService contactService;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private LoginGenerator loginGenerator;
	@Autowired
	private MessageSource messages;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private EmailContent_ResetPasswordToken resetEmailContentService;
	@Autowired
	private FailedLoginAttemptRepository failedLoginAttemptRepository;

	@Value("#{props['cwms.users.automatedusername']}")
	private String automatedusername;
	@Value("#{props['cwms.config.user.signature_extension']}")
	private String sigFileExt;
	@Value("#{props['cwms.config.user.approval_stamp_extension']}")
	private String stampFileExt;
	@Value("#{props['cwms.config.jwt.key']}")
	private String jwtKey;
	@Value("${cwms.config.email.noreply}")
	private String noreplyAddress;
	@Autowired
	private UserDao userDao;

	@Override
	public Contact findSystemContact() {
		return this.userDao.find(automatedusername).getCon();
	}

	@Override
	public List<User> findUsersRoleInternalForSubdiv(Subdiv subdiv) {
		return userDao.findUsersRoleInternalForSubdiv(subdiv);
	}

	/*
	 * Returns the User without doing an eager load, best for normal use
	 */
	public User get(String username) {
		return this.userDao.find(username);
	}

	/*
	 * Returns the User while fetching contact, needed for DWR where contact details
	 * are to be returned
	 */
	public User getEagerLoad(String username) {
		return this.userDao.getEagerLoad(username);
	}

	public List<UserLoginReportDTO> getAllCompanyWebUserLoginsForPeriod(int coid, LocalDate dateFrom, LocalDate dateTo) {
		List<UserLoginReportDTO> results = this.userDao.getAllCompanyWebUserLoginsForPeriod(coid, dateFrom, dateTo);
		results.sort((i1, i2) -> i2.getTotal().compareTo(i1.getTotal()));
		int position = 0;
		for (UserLoginReportDTO item : results)
			item.setPos(++position);
		return results;
	}

	public List<User> getAll() {
		return this.userDao.findAll();
	}

	public File getUserApproveStampImage(String username) {
		return this.getUserImgFile(username, username.concat(this.stampFileExt));
	}

	public File getUserImgFile(String username, String fileName) {
		User user = this.get(username);
		if (user == null || user.getDirectory() == null)
			return null;
		String path = user.getDirectory().getAbsolutePath().concat(File.separator).concat(fileName);
		File file = new File(path);
		file = file.exists() ? file : null;
		return file;
	}

	public File getUserSigImage(String username) {
		return this.getUserImgFile(username, username.concat(this.sigFileExt));
	}

	public void insertUser(User u) {
		this.userDao.persist(u);
	}

	@Override
	public void mergeUser(User u) {
		this.userDao.merge(u);
	}

	private String resetPasswordByUser(User user) {
		String newPassword = loginGenerator.generatePassword();
		// Don't change log level here!!!
		log.debug("New password generated for " + user.getUsername() + ": " + newPassword);
		String result = this.contactService.sendPasswordReminder(user.getCon().getPersonid(), newPassword);
		if (result.startsWith("A confirmation e-mail has been sent to ")) {
			// Email successful, can save new password
			user.setPassword(passwordEncoder.encode(newPassword));
			user.setWebAware(true);
			user.setPasswordInvalidated(false);
			failedLoginAttemptRepository.removeByUserName(user.getUsername());
			this.userDao.merge(user);
		}
		return result;
	}

	@Override
	public String resetPassword(Integer personId) {
		User user = userDao.findByPersonId(personId);
		return resetPasswordByUser(user);
	}

	public User webFindUserViaEmail(String email) throws MultipleMatchingUsersException {
		List<User> users = this.userDao.webFindUserViaEmail(email);
		// only return the user if there is 1 result! If there is more than one
		// someone could potentially be e-mailed somebody else's login details!
		if (users.size() > 1)
			throw new MultipleMatchingUsersException();
		else if (users.size() == 1)
			return users.get(0);
		else
			return null;
	}

	public ResultWrapper webUpdateUserPassword(int personid, String oldPassword, String newPassword,
			String confirmPassword, HttpSession session) {
		// get current contact from session
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		User user = this.get(username);
		// check id's match
		if (user.getCon().getPersonid() == personid) {
			// is user null?
			if (user.getPassword() != null) {
				// does the old password supplied match the currently saved
				// password?
				if (!(user.getPassword().equals(oldPassword)
					|| passwordEncoder.matches(oldPassword, user.getPassword()))) {
					return new ResultWrapper(false,
						"The old password you supplied was not recognised, please try again");
				} else {
					// do the new password entries match?
					if (!newPassword.equals(confirmPassword) || confirmPassword.equals("")) {
						return new ResultWrapper(false, "Your new password entries did not match, please try again");
					}
					// check if a password is valid: its size is greater than 8, it have at least 
					// uppercase letter, lowercase letter, digit and a special number
					else if (!this.passwordValid(newPassword).isSuccess()) {
						return this.passwordValid(newPassword);
					} else {
						user.setPassword(passwordEncoder.encode(newPassword));
						userDao.merge(user);
						return new ResultWrapper(true, "Your password has been updated successfully");
					}
				}
			} else {
				return new ResultWrapper(false, "The user could not be found");
			}
		} else {
			return new ResultWrapper(false, "Your request for password update has not been accepted");
		}
	}

	@Override
	public Optional<User> invalidatePassword(String userName) {
		return userDao.invalidatePassword(userName);
	}

	@Override
	public String generateResetPasswordToken(User user, Locale locale) {
		if (user == null)
			return messages.getMessage("login.email.sent", null, "An  e-mail has been sent.", locale);
		val calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR, 1);
		String token = Jwts.builder().setIssuer("cwms").setIssuedAt(new Date()).setExpiration(calendar.getTime())
				.claim("username", user.getUsername()).claim("role", "ROLE_USER_CHANGE_PASSWORD")
				.signWith(Keys.hmacShaKeyFor(jwtKey.getBytes())).compact();
		log.debug("token: " + token);
		return sendResetPasswordToken(token, user, locale);
	}

	@Override
	public Jws<Claims> decodeToke(String token) {
		return Jwts.parser().setSigningKey(Keys.hmacShaKeyFor(jwtKey.getBytes()))
				.require("role", "ROLE_USER_CHANGE_PASSWORD").parseClaimsJws(token);
	}

	private String sendResetPasswordToken(String token, User user, Locale locale) {
		val con = user.getCon();
		String status;
		if ((con.getEmail() != null) && !con.getEmail().trim().equals("")) {
			EmailContentDto contentDto;
			try {
				contentDto = resetEmailContentService.getContent(con, token, con.getLocale());
			} catch (Exception e) {
				return messages.getMessage("login.email.error", null,
						"There was an error and the  e-mail could not be sent.", locale);
			}
			log.debug(contentDto.getBody());
			String from = noreplyAddress;
			String to = con.getEmail();
			// We no longer bcc the local webmaster email address 2018-07-17 - security
			// issue
			boolean success = this.emailServ.sendAdvancedEmail(contentDto.getSubject(), from,
					Collections.singletonList(to), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), contentDto.getBody(), true, false);
			if (success) {
				status = messages.getMessage("login.email.sent", null, "An  e-mail has been sent.",
						user.getCon().getLocale());
			} else {
				status = messages.getMessage("login.email.error", null,
						"There was an error and the  e-mail could not be sent.", locale);
			}
		} else {
			status = messages.getMessage("login.email.noContact", null,
					"An  e-mail cannot be sent to a contact with no e-mail address.", locale);
		}
		return status;
	}

	public User resetPasswordForBlockedUser(User user) {
		resetPasswordByUser(user);
		return user;
	}
	
	public ResultWrapper passwordValid(String password){
		
		Pattern[] passRegex = new Pattern[4];
		passRegex[0] = Pattern.compile(".*[A-Z].*");
        passRegex[1] = Pattern.compile(".*[a-z].*");
        passRegex[2] = Pattern.compile(".*\\d.*");
        passRegex[3] = Pattern.compile(".*[#?!@$%^&*-+].*");
        
        if(password.length() < 8 || password.length() > 20){
        	return new ResultWrapper(false, this.messages.getMessage("error.password.characters.size", null,
					"Your password should be 8 to 20 characters long!", null), "error.password.characters.size");
        }
        else if (!passRegex[0].matcher(password).matches()){
        	return new ResultWrapper(false, this.messages.getMessage("error.password.characters.uppercase", null,
					"Your password must contain at least one upperCase letter!", null), "error.password.characters.uppercase");
        }
        else if (!passRegex[1].matcher(password).matches()){
        	return new ResultWrapper(false, this.messages.getMessage("error.password.characters.lowercase", null,
					"Your password must contain at least one lowerCase letter!", null), "error.password.characters.lowercase" );        	
        }
        else if (!passRegex[2].matcher(password).matches()){
        	return new ResultWrapper(false, this.messages.getMessage("error.password.characters.digit", null,
					"Your password must contain at least one digit!", null), "error.password.characters.digit");        	
        }
        else if (!passRegex[3].matcher(password).matches()){
        	return new ResultWrapper(false, this.messages.getMessage("error.password.characters.special", null,
					"Your password must contain at least one special character!", null), "error.password.characters.special");
        }
        else return new ResultWrapper(true, null);
	}
}