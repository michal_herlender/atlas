package org.trescal.cwms.core.login.entity.login.db;

import javax.servlet.http.HttpServletRequest;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.entity.login.IPSubnet;
import org.trescal.cwms.core.login.entity.login.Login;

public interface LoginService
{
	/**
	 * Returns the connection type of the request using the IP address to check
	 * against known internal subnets. This method should only be used on the
	 * internal CWMS, not the client website as it will nearly always return
	 * null.
	 * 
	 * @param request the {@link HttpServletRequest} from the user
	 * @return the connection type as an {@link IPSubnet} enum type
	 */
	IPSubnet getConnectionTypeFromIPAddress(HttpServletRequest request);

	void insertLogin(Login login);

	/**
	 * Returns whether or not the user is connected from inside the local
	 * network by checking the remote IP address against a list of known
	 * subnets.
	 * 
	 * @param request the {@link HttpServletRequest} from the user
	 * @return true if the user is connected from a known subnet, false
	 *         otherwise
	 */
	boolean isInternalUser(HttpServletRequest request);

	void recordWebLogin(Contact con, boolean logFileCreated, String logFilePath);
}
