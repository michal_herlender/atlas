package org.trescal.cwms.core.login.authority;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.userright.enums.Permission;

@Component("hasJobAuthorityOnConnectedSubdiv")
public class HasJobAuthorityOnConnectedSubdiv {

	@Autowired
	private JobService jobService;
	@Autowired
	private SubdivService subdivService;

	public boolean check(Permission permission) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (request != null && authentication != null && authentication.isAuthenticated() && request.getParameter("jobid") != null && 
				request.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) != null) {
			Integer jobid = Integer.valueOf(request.getParameter("jobid"));
			Job job = this.jobService.get(jobid);
			
			Integer connectedSubdivId = ((SubdivKeyValue) request.getSession()
					.getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV)).getKey();
			Subdiv connectedSubdiv = this.subdivService.get(connectedSubdivId);

			if (job != null && job.getOrganisation().getSubdivid() != connectedSubdiv.getSubdivid())
				return false;
			else 
				return authentication.getAuthorities().contains(permission);
		} else
			return authentication.getAuthorities().contains(permission);
	}
}