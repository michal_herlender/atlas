package org.trescal.cwms.core.login.authority;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.AuthenticationService;

import java.util.Objects;

@Component("canEditUser")
@Slf4j
public class CanEditUser {

    private final UserService userService;
    private final AuthenticationService authenticationService;

    public CanEditUser(UserService userService, AuthenticationService authenticationService) {
        this.userService = userService;
        this.authenticationService = authenticationService;
    }

    public boolean check(Integer personId, String editorName) {
        val session = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest().getSession();
        val allocatedSubdiv = (SubdivKeyValue) session.getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV);
        val editor = userService.get(editorName).getCon();

        return authenticationService.allowPrivileges(editor.getPersonid(), personId, allocatedSubdiv.getKey());
    }
}
