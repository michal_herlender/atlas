package org.trescal.cwms.core.login.entity.userpreferences;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.system.entity.labelprinter.LabelPrinter;
import org.trescal.cwms.core.system.entity.userinstructiontype.UserInstructionType;

/**
 * @author samt
 */
@Entity
@Table(name = "userpreferences")
public class UserPreferences extends Auditable {
	private Contact contact;
	private JobType defaultJobType;
	private int id;
	private boolean includeSelfInEmailReplies;
	private boolean jiKeyNavigation;
	private LabelPrinter labelPrinter;
	private boolean mouseoverActive;
	private int mouseoverDelay;
	private boolean navBarPosition;
	private String scheme;
	private Set<UserInstructionType> userInstructionTypes;
	private Boolean autoPrintLabel; 		// auto print label on job item creation
	private Boolean defaultServiceTypeUsage;	// whether to use default service type (only) for all instruments added to job

	/**
	 * Intentionally nullable to allow storage of default user preferences (see UserPreferencesDefault)
	 * Contact is only stored here, when UserPreferences entity used on a specific contact
	 */
	@JoinColumn(name = "contactid")
	@OneToOne
	public Contact getContact() {
		return this.contact;
	}

	@NotNull
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "defaultJobType")
	public JobType getDefaultJobType() {
		return this.defaultJobType;
	}

	@NotNull
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "labelprinterid")
	public LabelPrinter getLabelPrinter() {
		return this.labelPrinter;
	}

	@NotNull
	@Column(name = "mouseoverdelay", nullable=false)
	public int getMouseoverDelay() {
		return this.mouseoverDelay;
	}

	/**
	 * Optional field used to describe default user preferences (see UserPreferencesDefault)
	 */
	@Length(max = 40)
	@Column(name = "scheme", columnDefinition="varchar(40)")
	public String getScheme() {
		return this.scheme;
	}

	@NotNull
	@Cascade(CascadeType.ALL)
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "userPreference")
	public Set<UserInstructionType> getUserInstructionTypes() {
		return this.userInstructionTypes;
	}

	@NotNull
	@Column(name = "includeselfinreplies", nullable = false, columnDefinition = "tinyint")
	public boolean isIncludeSelfInEmailReplies() {
		return this.includeSelfInEmailReplies;
	}

	@NotNull
	@Column(name = "jikeynavigation", nullable = false, columnDefinition = "tinyint")
	public boolean isJiKeyNavigation() {
		return this.jiKeyNavigation;
	}

	@NotNull
	@Column(name = "mouseactive", nullable = false, columnDefinition = "tinyint")
	public boolean isMouseoverActive() {
		return this.mouseoverActive;
	}

	@NotNull
	@Column(name = "navbarposition", nullable = false, columnDefinition = "tinyint")
	public boolean isNavBarPosition() {
		return this.navBarPosition;
	}

	@NotNull
	@Column(name = "autoprintlabel", nullable = false, columnDefinition = "tinyint")
	public Boolean getAutoPrintLabel() {
		return autoPrintLabel;
	}

	@NotNull
	@Column(name = "defaultservicetypeusage", nullable = false, columnDefinition = "tinyint")
	public Boolean getDefaultServiceTypeUsage() {
		return defaultServiceTypeUsage;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public void setDefaultJobType(JobType defaultJobType) {
		this.defaultJobType = defaultJobType;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setIncludeSelfInEmailReplies(boolean includeSelfInEmailReplies) {
		this.includeSelfInEmailReplies = includeSelfInEmailReplies;
	}

	public void setJiKeyNavigation(boolean jiKeyNavigation) {
		this.jiKeyNavigation = jiKeyNavigation;
	}

	public void setLabelPrinter(LabelPrinter labelPrinter) {
		this.labelPrinter = labelPrinter;
	}

	public void setMouseoverActive(boolean mouseoverActive) {
		this.mouseoverActive = mouseoverActive;
	}

	public void setMouseoverDelay(int mouseoverDelay) {
		this.mouseoverDelay = mouseoverDelay;
	}

	public void setNavBarPosition(boolean navBarPosition) {
		this.navBarPosition = navBarPosition;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public void setUserInstructionTypes(Set<UserInstructionType> userInstructionTypes) {
		this.userInstructionTypes = userInstructionTypes;
	}

	public void setAutoPrintLabel(Boolean autoPrintLabel) {
		this.autoPrintLabel = autoPrintLabel;
	}

	public void setDefaultServiceTypeUsage(Boolean defaultServiceTypeUsage) {
		this.defaultServiceTypeUsage = defaultServiceTypeUsage;
	}
	
	
}
