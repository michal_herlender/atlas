package org.trescal.cwms.core.login.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.login.CrocodileAuthenticationToken;
import org.trescal.cwms.core.login.GrantedAuthorityComparator;
import org.trescal.cwms.core.login.entity.login.db.LoginAttemptService;
import org.trescal.cwms.core.login.entity.oauth2.clientdetails.PersistentClientDetails;
import org.trescal.cwms.core.login.entity.oauth2.clientdetails.db.PersistentClientDetailsService;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.login.exception.BadCredentialsExceptionWithLimitWarning;
import org.trescal.cwms.core.login.exception.MaxLoginAttemptException;
import org.trescal.cwms.core.userright.enums.Permission;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

@Component("crocodileAuthenticationProvider")
@Slf4j
public class CrocodileAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private UserService userService;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private LoginAttemptService loginAttemptService;
	@Autowired
	private PersistentClientDetailsService persistentClientDetailsService;

	/**
	 * Notes : This handles the following general cases:
	 * 
	 * a) Session based authentication, where the username and password are provided in the Authentication
	 *    - verify the user exists
	 * b) OAuth2 access token initial authentication, where the "user" username and password are provided in the Authentication,
	 *    and the "application credentials" are provided in a header using basic authentication 
	 *    - verify the user exists
	 *    - provide just the privileges required for the "application credentials"
	 * c) Cases where an invalid user name or invalid password are provided, or the user account is locked out  
	 *    - throw an AuthenticationException with a detailed message for the user
	 *    
	 * Note that for the following cases, if security is properly configured, this method should never be called for:
	 * 
	 * d) OAuth2 access token usage, where the client is provided in a bearer token
	 *    - the OAuth2 framework provides authorization using the existing token
	 * e) OAuth2 access token refresh, where no new credentials are provided, but a refresh token is provided
	 *    - the OAuth2 framework provides a new token using the refresh token
	 *
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getName();
		log.debug("username : " + (username == null ? "(null)" : username));
		String password = authentication.getCredentials() == null ? null : authentication.getCredentials().toString();
		if (password == null) 
			log.debug("password was null"); 
		preAuthenticationChecks(username);
		User user = userService.get(username);
		if (user == null) {
			badCredentialsExceptionBuilder(username, "User not found");
			return null;
		}
		else if ((password != null) && passwordEncoder.matches(password, user.getPassword())) {
			postAuthenticationChecks(user);
			String clientId = getClientId();
			if (!StringUtils.isEmpty(clientId)) {
				/* 
				 * For authentication using OAuth2 - we want to add ONLY the authorities for the specified client
				 * to limit the permission size, and also serialize only the matching client permissions in database 
				 */
				PersistentClientDetails client = persistentClientDetailsService.findByClientId(clientId);
				if (client != null) {
					SortedSet<GrantedAuthority> authorities = new TreeSet<>(new GrantedAuthorityComparator());
					// TODO - put in one query
					Set<Permission> permissionsClient = client.getRole().getGroups().stream()
						.flatMap(pg -> pg.getPermissions().stream()).collect(Collectors.toSet());
					authorities.addAll(permissionsClient);
					return new CrocodileAuthenticationToken(username, password, authorities);
				}
				else {
					badCredentialsExceptionBuilder(clientId, "Client details not found for id "+clientId);
					return null;
				}
			}
			else {
				/*
				 * Regular session-based authentication - add authorities for specified client
				 */
				Subdiv subdiv = user.getCon().getSub();
				SortedSet<GrantedAuthority> authorities = new TreeSet<>(new GrantedAuthorityComparator());
				// TODO - we should put in one query
				Set<Permission> permissions = user.getUserRoles().stream().filter(ur -> ur.getOrganisation().equals(subdiv))
					.flatMap(ur -> ur.getRole().getGroups().stream().flatMap(pg -> pg.getPermissions().stream())).collect(Collectors.toSet());
				if (!permissions.isEmpty()) {
					// As long as the user has some authorities, we add the general role
					authorities.add(new SimpleGrantedAuthority("ROLE_INTERNAL"));
				}
				if (user.getWebUser()) {
					// Indicates access to customer portal
					authorities.add(new SimpleGrantedAuthority("ROLE_WEB"));
					authorities.add(new SimpleGrantedAuthority(Permission.WEB_ACCESS.getAuthority()));
				}
				authorities.addAll(permissions);
				return new CrocodileAuthenticationToken(username, password, authorities);
			}
		}
		else {
			badCredentialsExceptionBuilder(username, "Incorrect username or password");
			return null;
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

	public void setLoginAttemptService(LoginAttemptService loginAttemptService) {
		this.loginAttemptService = loginAttemptService;
	}

	private void badCredentialsExceptionBuilder(String userName, String message) throws AuthenticationException {
		if (loginAttemptService.isWarningActive(userName))
			throw new BadCredentialsExceptionWithLimitWarning(message);
		throw new BadCredentialsException(message);
	}

	private void postAuthenticationChecks(User user) throws AuthenticationException {
		if (user.getPasswordInvalidated())
			throw new CredentialsExpiredException("User credentials have expired");
	}

	private void preAuthenticationChecks(String username) throws AuthenticationException {
		if (loginAttemptService.isBlocked(username))
			throw new MaxLoginAttemptException("Maximal ammount of login attempt reached");
	}

	private String getClientId() {
		if (RequestContextHolder.getRequestAttributes() == null) {
			return null;
		}
		final HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
			.getRequest();

		final String authorizationHeaderValue = request.getHeader("Authorization");
		final String base64AuthorizationHeader = Optional.ofNullable(authorizationHeaderValue)
			.map(headerValue -> headerValue.substring("Basic ".length())).orElse("");

		if (StringUtils.isNotEmpty(base64AuthorizationHeader)) {
			String decodedAuthorizationHeader = new String(Base64.getDecoder().decode(base64AuthorizationHeader),
				StandardCharsets.UTF_8);
			return decodedAuthorizationHeader.split(":")[0];
		}

		return "";
	}
}