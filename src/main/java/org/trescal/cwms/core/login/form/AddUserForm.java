package org.trescal.cwms.core.login.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AddUserForm 
{
	private Integer personid;
	private String username;
	private String password;
	private Boolean sendEmailToUser;
	
	// Validation group for user name (not always entered)
	public static interface UsernameGroup {
	}
	
	public String getPassword() {
		return password;
	}
	@Size(min=3, max=20, groups=UsernameGroup.class)
	public String getUsername() {
		return username;
	}
	public Boolean getSendEmailToUser() {
		return sendEmailToUser;
	}
	@NotNull
	public Integer getPersonid() {
		return personid;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setSendEmailToUser(Boolean sendEmailToUser) {
		this.sendEmailToUser = sendEmailToUser;
	}
	public void setPersonid(Integer personid) {
		this.personid = personid;
	}
}
