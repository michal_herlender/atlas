package org.trescal.cwms.core.login.entity.oauth2.accesstoken.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.login.entity.oauth2.accesstoken.PersistentAccessToken;

public interface PersistentAccessTokenService extends BaseService<PersistentAccessToken, Integer> {
	PersistentAccessToken findByAuthenticationId(String authenticationId);
	List<PersistentAccessToken> findByClientId(String clientId);
	List<PersistentAccessToken> findByClientIdAndUserName(String clientId, String username);
	PersistentAccessToken findByRefreshTokenId(String refreshToken);
	PersistentAccessToken findByTokenId(String tokenId);
}
