package org.trescal.cwms.core.login.entity.login.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.login.entity.login.Login;

public interface LoginDao extends BaseDao<Login, Integer> {}