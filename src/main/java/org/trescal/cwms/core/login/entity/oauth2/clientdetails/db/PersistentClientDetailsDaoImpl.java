package org.trescal.cwms.core.login.entity.oauth2.clientdetails.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.login.entity.oauth2.clientdetails.PersistentClientDetails;

@Repository("PersistentClientDetailsDao")
public class PersistentClientDetailsDaoImpl extends BaseDaoImpl<PersistentClientDetails, String>
		implements PersistentClientDetailsDao {

	@Override
	protected Class<PersistentClientDetails> getEntity() {
		return PersistentClientDetails.class;
	}

}
