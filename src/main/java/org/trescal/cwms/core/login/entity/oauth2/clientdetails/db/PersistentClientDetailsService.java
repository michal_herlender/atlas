package org.trescal.cwms.core.login.entity.oauth2.clientdetails.db;

import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.trescal.cwms.core.login.entity.oauth2.clientdetails.PersistentClientDetails;

public interface PersistentClientDetailsService extends ClientDetailsService {

	PersistentClientDetails findByClientId(String clientId);
	
	String encodeClientSecrets();
}
