package org.trescal.cwms.core.login.entity.userpreferencesdefault.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.login.entity.userpreferencesdefault.UserPreferencesDefault;

public interface UserPreferencesDefaultService extends BaseService<UserPreferencesDefault, Integer> {
	
	/**
	 * Returns an unsorted List of UserPreferencesDefault records appropriate for the specified address id
	 * (may be configured at Address, Subdiv, Company, or global default level)
	 */
	List<UserPreferencesDefault> getAllForAddress(int addressid);

	/**
	 * Looks up the best @UserPreferencesDefault entry for the specified address id
	 * with preference to Address, Subdiv, Company, and global default respectively
	 * If none found (e.g. database configuration error) returns null 
	 * @param addressid
	 * @return the best @UserPreferencesDefault or null if none found 
	 */
	UserPreferencesDefault getBestDefaultForAddress(int addressid);
}
