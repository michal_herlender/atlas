package org.trescal.cwms.core.login.entity.oauth2.refreshtoken;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.login.entity.oauth2.PersistentToken;

/**
 * Implementation of OAuth2 refresh token persisted in database 
 * 2019-09-17
 */
@Entity
@Table(name="oauth2refreshtoken", uniqueConstraints={
		@UniqueConstraint(columnNames="tokenid", name="UK_oauth2refreshtoken_tokenid"),
	})
public class PersistentRefreshToken extends PersistentToken {
	private int id;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	@Type(type = "int")
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
}
