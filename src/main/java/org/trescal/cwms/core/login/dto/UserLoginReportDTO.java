package org.trescal.cwms.core.login.dto;

public class UserLoginReportDTO {

	private String name;
	private Integer pos;
	private String role;
	private String subname;
	private Integer total;

	public UserLoginReportDTO() {
		super();
	}

	public UserLoginReportDTO(String name, String role, String subname, Long total) {
		super();
		this.name = name;
		this.role = role;
		this.subname = subname;
		this.total = total.intValue();
	}

	public String getName() {
		return this.name;
	}

	public Integer getPos() {
		return this.pos;
	}

	public String getRole() {
		return this.role;
	}

	public String getSubname() {
		return this.subname;
	}

	public Integer getTotal() {
		return this.total;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPos(Integer pos) {
		this.pos = pos;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setSubname(String subname) {
		this.subname = subname;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}
}