package org.trescal.cwms.core.login.entity.userpreferencesdefault;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.company.entity.address.Address;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;

/**
 * Configuration table which allows us to have different templates for user preferences 
 * It is intended there should always be one record with "globaldefault" set to true,
 * otherwise other records can have Company, Subdiv, or Address set to true
 * The user preferences are then initialized for a Contact based on the appropriate defaults. 
 */
@Entity
@Table(name = "userpreferencesdefault", uniqueConstraints = {
		@UniqueConstraint(columnNames = {"globaldefault", "coid", "subdivid", "addressid"}, 
				name = "UK_userpreferencesdefault_definition")})
public class UserPreferencesDefault {
	private int id;
	private UserPreferences userPreferences;
	private Boolean globalDefault;
	private Company company;
	private Subdiv subdiv;
	private Address address;

	/**
	 * ID of this entry in the configuration table
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return id;
	}
	
	/**
	 * Links to the UserPreferences entity that this default setting is for
	 * @return
	 */
	@NotNull
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "userpreferencesid", nullable = false, foreignKey = 
			@ForeignKey(name = "FK_userpreferencesdefault_userpreferencesid"))
	public UserPreferences getUserPreferences() {
		return userPreferences;
	}
	
	/**
	 * Set to true for the single UserPreferences entity used as a global default
	 * @return
	 */
	@NotNull
	@Column(name="globaldefault", columnDefinition="tinyint not null")
	public Boolean getGlobalDefault() {
		return globalDefault;
	}
	
	/**
	 * Indicates that a UserPreferences entity should be used as a (business) Company default. 
	 * There should be at most one entry for any Company;
	 * The Address and Subdiv should be null if the Company is set.
	 * @return
	 */
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "coid", nullable = true, foreignKey = 
			@ForeignKey(name = "FK_userpreferencesdefault_coid"))
	public Company getCompany() {
		return company;
	}
	
	/**
	 * Indicates that a UserPreferences entity should be used as a (business) Subdiv default. 
	 * There should be at most one entry for any Subdiv;
	 * The Address and Company should be null if the Subdiv is set.
	 * @return
	 */
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "subdivid", nullable = true, foreignKey = 
			@ForeignKey(name = "FK_userpreferencesdefault_subdivid"))
	public Subdiv getSubdiv() {
		return subdiv;
	}

	/**
	 * Indicates that a UserPreferences entity should be used as a (business) Address default. 
	 * There should be at most one entry for any Address; 
	 * The Subdiv and Company should be null if the Address is set.
	 * @return
	 */
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "addressid", nullable = true, foreignKey = 
			@ForeignKey(name = "FK_userpreferencesdefault_addressid"))
	public Address getAddress() {
		return address;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public void setUserPreferences(UserPreferences userPreferences) {
		this.userPreferences = userPreferences;
	}
	public void setGlobalDefault(Boolean globalDefault) {
		this.globalDefault = globalDefault;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public void setSubdiv(Subdiv subdiv) {
		this.subdiv = subdiv;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
}
