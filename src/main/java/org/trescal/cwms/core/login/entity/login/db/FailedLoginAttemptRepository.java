package org.trescal.cwms.core.login.entity.login.db;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.trescal.cwms.core.login.entity.login.FailedLoginAttempt;

public interface FailedLoginAttemptRepository extends CrudRepository<FailedLoginAttempt, Integer>{

	Integer countByUserNameAndCreatedAtAfter(String userName, Date dateLimit);
	
	List<FailedLoginAttempt> findByUserName(String userName);
	
	void removeByUserName(String userName);
}