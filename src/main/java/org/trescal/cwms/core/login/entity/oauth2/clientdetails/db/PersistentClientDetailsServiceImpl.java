package org.trescal.cwms.core.login.entity.oauth2.clientdetails.db;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.login.entity.oauth2.clientdetails.PersistentClientDetails;

/**
 * 2021-09-17 ClientDetails is deprecated as Spring-Security-OAuth is deprecated...
 * 
 */
@Service
public class PersistentClientDetailsServiceImpl implements PersistentClientDetailsService {

	@Autowired
	private PersistentClientDetailsDao dao;
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {

		PersistentClientDetails client = this.findByClientId(clientId);
		if (client != null) {
			BaseClientDetails base = new BaseClientDetails(client.getClientId(), client.getResourceIds(),
					client.getScope(), null, client.getRole().getName());
			base.setClientSecret(client.getClientSecret());
			base.setAccessTokenValiditySeconds(client.getAccessTokenValiditySeconds());
			base.setRefreshTokenValiditySeconds(client.getRefreshTokenValiditySeconds());
			base.setAuthorizedGrantTypes(
					Arrays.asList("password", "authorization_code", "refresh_token", "client_credentials"));
			return base;
		}
		throw new ClientRegistrationException("No Client Details for client id: " + clientId);
	}

	@Override
	public String encodeClientSecrets() {
		int countAlreadyScrypt = 0;
		int countUpdatedToScrypt = 0; 
		List<PersistentClientDetails> results = this.dao.findAll();
		for (PersistentClientDetails client : results) {
			String existingSecret = client.getClientSecret();
			if (client.getClientSecret().startsWith("{scrypt}")) {
				countAlreadyScrypt++;
			}
			else {
				if (existingSecret.startsWith("{noop}")) {
					existingSecret = existingSecret.substring(6, existingSecret.length());
				}
				String encryptedSecret = this.passwordEncoder.encode(existingSecret);
				client.setClientSecret("{scrypt}"+encryptedSecret);
				countUpdatedToScrypt++;
			}
		}

		StringBuffer result = new StringBuffer(); 
		result.append("countAlreadyScrypt : ");
		result.append(countAlreadyScrypt);
		result.append(", countUpdatedToScrypt : ");
		result.append(countUpdatedToScrypt);
		
		return result.toString();
	}
	
	@Override
	public PersistentClientDetails findByClientId(String clientId) {
		return this.dao.find(clientId);
	}

}
