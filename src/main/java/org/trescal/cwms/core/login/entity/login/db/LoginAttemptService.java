package org.trescal.cwms.core.login.entity.login.db;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.login.entity.login.FailedLoginAttempt;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.user.db.UserService;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Date;

@Service
@Slf4j
@Transactional
public class LoginAttemptService {

	private final static Integer MAX_ATTEMTS = 10;
	private final static Integer WARNING_LEVEL = 5;
	
	 @Autowired FailedLoginAttemptRepository failedLoginAttemptRepository;
	 
	 @Autowired private UserService userService;
	 
	 public LoginAttemptService(UserService userService) {
		super();
		this.userService = userService;
	}
	 
	 public void loginSucceeded(String userName) {
//		 failedLoginAttemptRepository.delete(failedLoginAttemptRepository.findByUserName(userName));
		 failedLoginAttemptRepository.removeByUserName(userName);
	}
	 
	public void loginFailed(String key) {
		// Note, it's possible that there is no principal provided
		if (key != null) {
			failedLoginAttemptRepository.save(FailedLoginAttempt.builder().createdAt(new Date()).userName(key).build());
			// 2020-10-29 GB : Commented out findAll(); to discuss/verify
//			failedLoginAttemptRepository.findAll();
			if(isBlocked(key)) userService.invalidatePassword(key);
		}
	}
	
	public Boolean isBlocked(String key) {
			val calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH,-1);
        val counter = failedLoginAttemptRepository.countByUserNameAndCreatedAtAfter(key, calendar.getTime());
        log.debug("fail counter: " + counter);
        return counter >= MAX_ATTEMTS;
	}
	
	
	public Boolean isWarningActive(String key) {
		val calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH,-1);
		val countrer = failedLoginAttemptRepository.countByUserNameAndCreatedAtAfter(key, calendar.getTime());
		return countrer >= WARNING_LEVEL;
	}
	
	
	public User resetPassword(String username) {
		User user = userService.get(username);
		if(user != null) {
			userService.resetPasswordForBlockedUser(user);
		}
		return user;
	}
}
