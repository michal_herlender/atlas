package org.trescal.cwms.core.login.exception;

import org.springframework.security.authentication.BadCredentialsException;

public class MaxLoginAttemptException extends BadCredentialsException {

	public MaxLoginAttemptException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
