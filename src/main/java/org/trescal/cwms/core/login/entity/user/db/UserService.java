package org.trescal.cwms.core.login.entity.user.db;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.exception.MultipleMatchingUsersException;
import org.trescal.cwms.core.login.dto.UserLoginReportDTO;
import org.trescal.cwms.core.login.entity.user.User;

import javax.management.relation.Role;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public interface UserService {

	/**
	 * Looks for a {@link Contact} with a {@link Role} or "ROLE_CWMS_USER".
	 * 
	 * @return the associated {@link Contact}.
	 */
	Contact findSystemContact();

	/**
	 * Finds and returns a list of users with any of Employee for the given subdiv
	 *
     * @param subdiv
     * @return
     */
    List<User> findUsersRoleInternalForSubdiv(Subdiv subdiv);

    /*
     * Primarily used for DWR, does fetch join
     */
    User getEagerLoad(String username);

    /**
     * Finds the {@link User} for the given username, setting a directory in the
     * file system for the user (this will create the directory if it doesn't
     * exist).
     *
     * @param username the username
     * @return the {@link User} object if found, null otherwise
     */
    User get(String username);

    List<UserLoginReportDTO> getAllCompanyWebUserLoginsForPeriod(int coid, LocalDate dateFrom, LocalDate dateTo);

    List<User> getAll();

    /**
     * Returns the {@link User}'s approval stamp image {@link File} if one exists in
     * the {@link User}'s directory using the system naming convention. Otherwise,
     * returns null.
     *
     * @param username the username of the {@link User}
     * @return the approval stamp image {@link File} of the user, or null if none
     * exist.
     */
    File getUserApproveStampImage(String username);

    /**
     * Returns the {@link User}'s signature image {@link File} if one exists in the
     * {@link User}'s directory using the system naming convention. Otherwise,
     * returns null.
     *
     * @param username the username of the {@link User}
     * @return the signature image {@link File} of the user, or null if none exist.
     */
	File getUserSigImage(String username);

	void insertUser(User u);

	void mergeUser(User u);

	/**
	 * Reset the password of the {@link Contact} identified by the given id and
	 * sends an e-mail to them to remind them of their username and give them their
	 * new password.
	 * 
	 * @return message indicating success / fail of send.
	 */
	String resetPassword(Integer personId);

	String generateResetPasswordToken(User user, Locale locale);

	Jws<Claims> decodeToke(String token);

	User webFindUserViaEmail(String email) throws MultipleMatchingUsersException;

	/**
	 * this method updates the users password to the supplied value after checking
	 * that the old password matches.
	 * 
	 * @param personid        the id of the contact we are updating the password for
	 *                        (no longer needed!)
	 * @param oldPassword     the users old password
	 * @param newPassword     the users new password
	 * @param confirmPassword the users confirmation of new password
	 * @return {@link ResultWrapper}
	 */
	ResultWrapper webUpdateUserPassword(int personid, String oldPassword, String newPassword, String confirmPassword,
			HttpSession session);

	Optional<User> invalidatePassword(String userName);

	User resetPasswordForBlockedUser(User user);
	
	ResultWrapper passwordValid(String password);
}