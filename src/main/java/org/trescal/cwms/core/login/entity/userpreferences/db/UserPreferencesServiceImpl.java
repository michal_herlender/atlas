package org.trescal.cwms.core.login.entity.userpreferences.db;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.login.entity.user.User;
import org.trescal.cwms.core.login.entity.userpreferences.UserPreferences;
import org.trescal.cwms.core.login.entity.userpreferencesdefault.db.UserPreferencesDefaultService;
import org.trescal.cwms.core.login.form.UserPreferencesForm;
import org.trescal.cwms.core.system.entity.alligatorsettingscontact.db.AlligatorSettingsContactService;
import org.trescal.cwms.core.system.entity.instructiontype.InstructionType;
import org.trescal.cwms.core.system.entity.labelprinter.LabelPrinter;
import org.trescal.cwms.core.system.entity.labelprinter.db.LabelPrinterService;
import org.trescal.cwms.core.system.entity.userinstructiontype.UserInstructionType;

@Service("UserPreferencesService")
public class UserPreferencesServiceImpl extends BaseServiceImpl<UserPreferences, Integer> implements UserPreferencesService
{
	@Autowired
	private AlligatorSettingsContactService ascService;
	@Autowired
	private LabelPrinterService labelPrinterService;
	@Autowired
	private UserPreferencesDefaultService userPreferencesDefaultService;
	@Autowired
	private UserPreferencesDao userPreferencesDao;
	
	@Override
	public void addDefaultPreferencesForUser(User user)
	{
		// only add new preferences object if not already set on contact, otherwise no action needed
		if (user.getCon().getUserPreferences() == null) {
			// create new preferences object
			UserPreferences prefs = new UserPreferences();

			if (user.getCon().getDefAddress() == null)
				throw new UnsupportedOperationException("Default address must be set on contact, before adding user preferences");
			int addressid = user.getCon().getDefAddress().getAddrid();
			// get best set of defaults
			UserPreferences defaultPrefs = this.userPreferencesDefaultService.getBestDefaultForAddress(addressid).getUserPreferences();
			
			// set defaults
			prefs.setAutoPrintLabel(defaultPrefs.getAutoPrintLabel());
			prefs.setContact(user.getCon());
			prefs.setDefaultJobType(defaultPrefs.getDefaultJobType());
			prefs.setDefaultServiceTypeUsage(defaultPrefs.getDefaultServiceTypeUsage());
			prefs.setIncludeSelfInEmailReplies(defaultPrefs.isIncludeSelfInEmailReplies());
			prefs.setJiKeyNavigation(defaultPrefs.isJiKeyNavigation());
			prefs.setLabelPrinter(defaultPrefs.getLabelPrinter());
			prefs.setMouseoverActive(defaultPrefs.isMouseoverActive());
			prefs.setMouseoverDelay(defaultPrefs.getMouseoverDelay());
			prefs.setNavBarPosition(defaultPrefs.isNavBarPosition());
			
			Set<UserInstructionType> uitypes = new HashSet<UserInstructionType>();
			for (InstructionType it :  InstructionType.values())
			{
				UserInstructionType uitype = new UserInstructionType();
				uitype.setInstructionType(it);
				uitype.setUserPreference(prefs);

				uitypes.add(uitype);
			}
			prefs.setUserInstructionTypes(uitypes);

			// set the prefs onto the contact entity (persisted through cascade)
			user.getCon().setUserPreferences(prefs);
		}
	}

	@Override
	public void editUserPreferences(UserPreferencesForm form) {
		UserPreferences up = super.get(form.getPreferencesId());
		LabelPrinter labelPrinter = null;
		if (form.getLabelPrinterId() != 0) {
			labelPrinter = this.labelPrinterService.findLabelPrinter(form.getLabelPrinterId()); 
		}
		
		up.setAutoPrintLabel(form.getAutoPrintLabel());
		up.setDefaultServiceTypeUsage(form.getDefaultServiceTypeUsage());
		up.setDefaultJobType(form.getJobType());
		up.setIncludeSelfInEmailReplies(form.getIncludeSelfInEmailReplies());
		up.setJiKeyNavigation(form.getJiKeyNavigation());
		up.setLabelPrinter(labelPrinter);
		up.setMouseoverActive(form.getMouseoverActive());
		up.setMouseoverDelay(form.getMouseoverDelay());
		up.setNavBarPosition(form.getNavBarPosition());
		
		ascService.editAlligatorSettingsContact(up.getContact(), 
				form.getAlligatorSettingsOnContact(), form.getAlligatorSettingsProductionId(), form.getAlligatorSettingsTestId());
	}

	@Override
	public UserPreferences findUserPreferencesByContact(int contactid)
	{
		return this.userPreferencesDao.findUserPreferencesByContact(contactid);
	}

	@Override
	public UserPreferences findUserPreferencesByUsername(String username)
	{
		return this.userPreferencesDao.findUserPreferencesByUsername(username);
	}

	@Override
	public List<UserPreferences> findBySubdiv(int subdivId) {
		return this.userPreferencesDao.findBySubdiv(subdivId);
	}
	
	@Override
	public void updateDefaultServiceTypeUsage(String username, boolean usageValue) {
		UserPreferences up = this.findUserPreferencesByUsername(username);
		if (up == null) throw new RuntimeException("UserPreferences not found");
		up.setDefaultServiceTypeUsage(usageValue);
	}

	@Override
	protected BaseDao<UserPreferences, Integer> getBaseDao() {
		return userPreferencesDao;
	}
}