package org.trescal.cwms.core.login.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.trescal.cwms.core.login.entity.login.db.LoginAttemptService;

@Component
public class AuthenticationSucceedListener implements ApplicationListener<AuthenticationSuccessEvent> {
	
	@Autowired private LoginAttemptService loginAttemptService;
	

	@Override
	public void onApplicationEvent(AuthenticationSuccessEvent event) {
		Object principal = event.getAuthentication().getPrincipal();
		switch (principal.getClass().getCanonicalName()) {
		case "java.lang.String":
			loginAttemptService.loginSucceeded((String)principal);
			break;
		case "org.springframework.security.core.userdetails.User":
			loginAttemptService.loginSucceeded(((User)principal).getUsername());
			break;
		default:
			break;
		}
		

	}
}
