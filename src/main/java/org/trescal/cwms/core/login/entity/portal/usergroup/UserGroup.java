package org.trescal.cwms.core.login.entity.portal.usergroup;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.login.entity.portal.usergrouppropertyvalue.UserGroupPropertyValue;
import org.trescal.cwms.core.system.entity.translation.Translation;

/**
 * Represents an instance of a UserGroup made up of
 * an allowed {@link UserGroupPropertyValue}. {@link UserGroup}s are used to set
 * properties for {@link Company} and their {@link Contact}s.
 * 
 * @author Richard
 */
@Entity
@Table(name = "usergroup")
public class UserGroup
{
	private Company company;
	private boolean compDefault;
	private Set<Contact> contacts;
	private String description;
	private int groupid;
	private String name;
	private boolean systemDefault;
	private Set<UserGroupPropertyValue> properties;
	private Set<Translation> descriptionTranslation;
	private Set<Translation> nameTranslation;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coid")
	public Company getCompany()
	{
		return this.company;
	}

	@OneToMany(mappedBy = "usergroup")
	public Set<Contact> getContacts()
	{
		return this.contacts;
	}

	@Length(min = 1, max = 255)
	@Column(name = "description", length = 255)
	public String getDescription()
	{
		return this.description;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "groupid")
	public int getGroupid()
	{
		return this.groupid;
	}

	@NotNull
	@Length(min = 1, max = 50)
	@Column(name = "name", length = 30)
	public String getName()
	{
		return this.name;
	}

	@NotNull
	@Column(name = "compdefault", nullable = false, columnDefinition="tinyint")
	public boolean isCompDefault()
	{
		return this.compDefault;
	}

	@NotNull
	@Column(name = "systemdefault", nullable = false, columnDefinition="tinyint")
	public boolean isSystemDefault()
	{
		return this.systemDefault;
	}

    @OneToMany(mappedBy = "userGroup", cascade = CascadeType.ALL)
	@OrderBy("property")
    public Set<UserGroupPropertyValue> getProperties() {
        return properties;
    }

    @ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="usergroupdescriptiontranslation", joinColumns=@JoinColumn(name="groupid"))
	public Set<Translation> getDescriptionTranslation() {
		return descriptionTranslation;
	}

	public void setDescriptionTranslation(Set<Translation> descriptionTranslation) {
		this.descriptionTranslation = descriptionTranslation;
	}

	@ElementCollection(fetch=FetchType.LAZY)
	@CollectionTable(name="usergroupnametranslation", joinColumns=@JoinColumn(name="groupid"))
	public Set<Translation> getNameTranslation() {
		return nameTranslation;
	}



	public void setNameTranslation(Set<Translation> nameTranslation) {
		this.nameTranslation = nameTranslation;
	}

    public void setProperties(Set<UserGroupPropertyValue> properties) { this.properties = properties; }

    public void setCompany(Company company)
    {
        this.company = company;
    }

    public void setCompDefault(boolean compDefault)
    {
        this.compDefault = compDefault;
    }

    public void setContacts(Set<Contact> contacts)
    {
        this.contacts = contacts;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setGroupid(int groupid)
    {
        this.groupid = groupid;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @param systemDefault the systemDefault to set
     */
    public void setSystemDefault(boolean systemDefault)
    {
        this.systemDefault = systemDefault;
    }

    /*
     * Implementation of the toString() method for logging purposes
     */
    @Override
    public String toString()
    {
        return Integer.toString(this.groupid);
    }

}
