package org.trescal.cwms.core.login;

import java.io.Serializable;
import java.util.Comparator;

import org.springframework.security.core.GrantedAuthority;

public class GrantedAuthorityComparator implements Comparator<GrantedAuthority>, Serializable {

	/**
	 * Serialized as part of OAuth2 token persistence
	 */
	private static final long serialVersionUID = -4743960103116495630L;

	@Override
	public int compare(GrantedAuthority o1, GrantedAuthority o2) {
		return o1.getAuthority().compareTo(o2.getAuthority());
	}
}