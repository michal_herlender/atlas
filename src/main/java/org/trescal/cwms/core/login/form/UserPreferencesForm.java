package org.trescal.cwms.core.login.form;

import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;

import lombok.Setter;

/**
 * Validation matches requirements of UserPreferences
 *
 */
@Setter
public class UserPreferencesForm {
	private Integer preferencesId;
	private JobType jobType;
	private Integer labelPrinterId;
	private Boolean alligatorSettingsOnContact;
	private Integer alligatorSettingsProductionId;
	private Integer alligatorSettingsTestId;
	private Boolean jiKeyNavigation;
	private Boolean mouseoverActive;
	private Boolean includeSelfInEmailReplies;
	private Integer mouseoverDelay;
	private Boolean navBarPosition;
	private Boolean autoPrintLabel;
	private Boolean defaultServiceTypeUsage;

	@NotNull
	public Integer getPreferencesId() {
		return preferencesId;
	}

	// Optional
	public JobType getJobType() {
		return jobType;
	}

	// 0 for not selected
	@NotNull
	public Integer getLabelPrinterId() {
		return labelPrinterId;
	}

	@NotNull
	public Boolean getAlligatorSettingsOnContact() {
		return alligatorSettingsOnContact;
	}

	// 0 for not selected
	@NotNull
	public Integer getAlligatorSettingsProductionId() {
		return alligatorSettingsProductionId;
	}

	// 0 for not selected
	@NotNull
	public Integer getAlligatorSettingsTestId() {
		return alligatorSettingsTestId;
	}

	@NotNull
	public Boolean getJiKeyNavigation() {
		return jiKeyNavigation;
	}

	@NotNull
	public Boolean getMouseoverActive() {
		return mouseoverActive;
	}

	@NotNull
	public Boolean getIncludeSelfInEmailReplies() {
		return includeSelfInEmailReplies;
	}

	@NotNull
	public Integer getMouseoverDelay() {
		return mouseoverDelay;
	}

	@NotNull
	public Boolean getNavBarPosition() {
		return navBarPosition;
	}

	@NotNull
	public Boolean getAutoPrintLabel() {
		return autoPrintLabel;
	}

	@NotNull
	public Boolean getDefaultServiceTypeUsage() {
		return defaultServiceTypeUsage;
	}

}