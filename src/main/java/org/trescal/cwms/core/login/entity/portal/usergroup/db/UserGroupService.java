package org.trescal.cwms.core.login.entity.portal.usergroup.db;

import java.util.List;

import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.instrument.entity.instrument.Instrument;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;

public interface UserGroupService
{
	/**
	 * Deletes the given {@link UserGroup}.
	 * 
	 * @param ug the {@link UserGroup} to delete.
	 */
	void deleteUserGroup(UserGroup ug);

	UserGroup findGroup(int groupid);

	List<UserGroup> getAllUserGroups();

	/**
	 * Returns a list of {@link UserGroup}s that are defined for the
	 * {@link Company} including the default group used for all companies.
	 * 
	 * coid the company id.
	 * @return {@link List} of {@link UserGroup}s.
	 */
	List<UserGroup> getSortedUserGroups(Company company);

	/**
	 * Returns the current system default {@link UserGroup}.
	 * 
	 * @return the {@link UserGroup}
	 */
	UserGroup getDefaultGroup();

	void insertUserGroup(UserGroup ug);

	void updateUserGroup(UserGroup ug);
	
	Boolean canUserViewThisInstrument(Contact contact, Instrument instrument);
	
	Boolean canUserViewThisJobItem(Contact contact, JobItem job);
	
	Boolean canUserUpdateThisInstrument(Contact contact, Instrument instrument);
	
	Boolean canUserCreateCalibrationForThisInstrument(Contact contact, Instrument instrument);

	Boolean canUserValidateCertificateForThisInstrument(Contact contact, Instrument instrument);
	
}
