package org.trescal.cwms.core.login.entity.oauth2.refreshtoken.db;

import java.util.Optional;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.login.entity.oauth2.refreshtoken.PersistentRefreshToken;
import org.trescal.cwms.core.login.entity.oauth2.refreshtoken.PersistentRefreshToken_;

@Repository
public class PersistentRefreshTokenDaoImpl extends BaseDaoImpl<PersistentRefreshToken, Integer>
		implements PersistentRefreshTokenDao {

	@Override
	protected Class<PersistentRefreshToken> getEntity() {
		return PersistentRefreshToken.class;
	}
	
	@Override
	public PersistentRefreshToken findByTokenId(String tokenId) {
		if (tokenId == null) {
			throw new IllegalArgumentException("Supplied tokenId must be not null");
		}
		
		Optional<PersistentRefreshToken> result = getFirstResult(cb -> {
			CriteriaQuery<PersistentRefreshToken> cq = cb.createQuery(PersistentRefreshToken.class);
			Root<PersistentRefreshToken> root = cq.from(PersistentRefreshToken.class);
			cq.where(cb.equal(root.get(PersistentRefreshToken_.tokenId), tokenId));
			return cq;
		});
		return result.orElse(null);
	}

}
