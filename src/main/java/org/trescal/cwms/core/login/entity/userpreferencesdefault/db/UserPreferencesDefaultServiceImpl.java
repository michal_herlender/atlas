package org.trescal.cwms.core.login.entity.userpreferencesdefault.db;

import java.util.List;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.login.entity.userpreferencesdefault.UserPreferencesDefault;
import org.trescal.cwms.core.login.entity.userpreferencesdefault.UserPreferencesDefaultComparator;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserPreferencesDefaultServiceImpl 
	extends BaseServiceImpl<UserPreferencesDefault, Integer> 
	implements UserPreferencesDefaultService {

	@Autowired
	private UserPreferencesDefaultDao baseDao;
	
	@Override
	protected BaseDao<UserPreferencesDefault, Integer> getBaseDao() {
		return baseDao;
	}
	
	@Override
	public List<UserPreferencesDefault> getAllForAddress(int addressid) {
		return baseDao.getAllForAddress(addressid);
	}
	
	@Override
	public UserPreferencesDefault getBestDefaultForAddress(int addressid) {
		List<UserPreferencesDefault> unsorted = this.baseDao.getAllForAddress(addressid);
		log.debug("Found "+unsorted.size()+" potential user preference defaults for address "+addressid);
		if (unsorted.isEmpty()) 
			throw new RuntimeException("Configuration error : No defaults appear to be configured");
		TreeSet<UserPreferencesDefault> sorted = new TreeSet<>(new UserPreferencesDefaultComparator());
		sorted.addAll(unsorted);
		UserPreferencesDefault bestDefault = sorted.last(); 
		log.debug("Returning user preference default with id "+bestDefault.getId()+" for address "+addressid);
		return bestDefault;
	}

}
