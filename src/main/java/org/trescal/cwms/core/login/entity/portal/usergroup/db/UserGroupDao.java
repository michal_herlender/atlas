package org.trescal.cwms.core.login.entity.portal.usergroup.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.login.entity.portal.usergroup.UserGroup;

public interface UserGroupDao extends BaseDao<UserGroup, Integer>
{
	List<UserGroup> getSortedUserGroups(Company company);

	UserGroup getDefaultGroup();

	
}