package org.trescal.cwms.core.login.entity.login.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.login.entity.login.Login;

@Repository("LoginDao")
public class LoginDaoImpl extends BaseDaoImpl<Login, Integer> implements LoginDao {
	
	@Override
	protected Class<Login> getEntity() {
		return Login.class;
	}
}