package org.trescal.cwms.core.login.entity.userpreferencesdefault.db;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.login.entity.userpreferencesdefault.UserPreferencesDefault;

@Repository
public interface UserPreferencesDefaultDao extends BaseDao<UserPreferencesDefault, Integer> {
	/**
	 * Returns an unsorted List of UserPreferencesDefault records appropriate for the specified address id
	 * (may be configured at Address, Subdiv, Company, or global default level)
	 */
	List<UserPreferencesDefault> getAllForAddress(int addressid);
}
