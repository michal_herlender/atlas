package org.trescal.cwms.core.login.entity.oauth2;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Base class for different token types (general and refresh)
 *
 */
@MappedSuperclass
public abstract class PersistentToken {
	private String tokenId;
	private String serializedToken;
    private String serializedAuthentication;
	
	@NotNull
	@Size(max=255)
	@Column(name="tokenid", nullable=false, length=255, columnDefinition="varchar(255)")
	public String getTokenId() {
		return tokenId;
	}
	
	/**
	 * Actual serialized token size seems to be 1268
	 * @return
	 */
	@NotNull
	@Size(max=8000)
	@Column(name="serializedtoken", nullable=false, length=8000, columnDefinition="varchar(8000)")
	public String getSerializedToken() {
		return serializedToken;
	}
	
	/**
	 * Serialization size is 16848 bytes for an internal user with 325 permissions; setting JPA limit to 64K
	 * Web users are much smaller, 2944 bytes 
	 * @return
	 */
	@NotNull
	@Size(max=65536)
	@Column(name="serializedauthentication", nullable=false, length=8000, columnDefinition="varchar(max)")
	public String getSerializedAuthentication() {
		return serializedAuthentication;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	public void setSerializedToken(String serializedToken) {
		this.serializedToken = serializedToken;
	}
	public void setSerializedAuthentication(String serializedAuthentication) {
		this.serializedAuthentication = serializedAuthentication;
	}
}
