package org.trescal.cwms.core.login.authority;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.trescal.cwms.core.jobs.certificate.entity.certificate.db.CertificateService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.userright.enums.Permission;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Component to check authorization for activities to be performed on a specific certificate
 * (e.g. create replacement) 
 * 
 * Currently, this is intended for internal certificates, with operations performed after calibration, 
 * (e.g. sign / reset / create supplement) where the certificate already has a calibration attached.
 * 
 * If a certificate has no calibration attached (e.g. reserved certificate, third party certificate, client certificate)
 * we use the logged in subdiv.
 *   
 * @author galen
 *
 */
@Component("hasCertificateAuthorityOnConnectedSubdiv")
public class HasCertificateAuthorityOnConnectedSubdiv {

	@Autowired
	private CertificateService certificateService;
	
	public boolean check(Permission permission) {
		boolean result = false;
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String paramCertid = request.getParameter("certid");
		if (authentication != null && 
				authentication.isAuthenticated() 
				&& paramCertid != null
				&& NumberTools.isAnInteger(paramCertid)
				&& request.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) != null) {
			
			// Retrieve the subdivision associated with the certificate (via its calibration if it exists) 
			Integer certid = Integer.valueOf(paramCertid);
			Integer certSubdivId = this.certificateService.getSubdivIdFromCalibration(certid);

			@SuppressWarnings("unchecked")
			KeyValue<Integer,String> subdivDto = (KeyValue<Integer,String>) request.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV);
			
			// If the certificate has a calibration, user must be logged into same subdiv to perform activity
			// (see comments in class heading)
			if ((certSubdivId != null) && subdivDto.getKey().equals(certSubdivId) ||
					(certSubdivId == null)) {
				result = authentication.getAuthorities().contains(permission);
			}
		}
		return result;
	}
}
