package org.trescal.cwms.core.login.entity.oauth2.accesstoken.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.login.entity.oauth2.accesstoken.PersistentAccessToken;

@Service
public class PersistentAccessTokenServiceImpl extends BaseServiceImpl<PersistentAccessToken, Integer> implements PersistentAccessTokenService {

	@Autowired
	private PersistentAccessTokenDao accessTokenDao;

	@Override
	protected BaseDao<PersistentAccessToken, Integer> getBaseDao() {
		return accessTokenDao;
	}
	
	@Override
	public PersistentAccessToken findByAuthenticationId(String authenticationId) {
		return accessTokenDao.findSingle(authenticationId, null, null);
	}

	@Override
	public List<PersistentAccessToken> findByClientId(String clientId) {
		return accessTokenDao.findByClientIdAndUserName(clientId, null);
	}

	@Override
	public List<PersistentAccessToken> findByClientIdAndUserName(String clientId, String username) {
		return accessTokenDao.findByClientIdAndUserName(clientId, username);
	}
	
	@Override
	public PersistentAccessToken findByRefreshTokenId(String refreshToken) {
		return accessTokenDao.findSingle(null, null, refreshToken);
	}

	@Override
	public PersistentAccessToken findByTokenId(String tokenId) {
		return accessTokenDao.findSingle(null, tokenId, null);
	}
}
