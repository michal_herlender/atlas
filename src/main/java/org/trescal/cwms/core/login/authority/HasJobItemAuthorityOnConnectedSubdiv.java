package org.trescal.cwms.core.login.authority;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.jobs.job.entity.jobtype.JobType;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.userright.enums.Permission;

@Component("hasJobItemAuthorityOnConnectedSubdiv")
public class HasJobItemAuthorityOnConnectedSubdiv {

	@Autowired
	private JobItemService jiService;

	public boolean check(Permission permission) {

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null && authentication.isAuthenticated() && request.getParameter("jobitemid") != null 
				&& request.getSession().getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) != null) {
			Integer jobitemid = request.getParameter("jobitemid") != null ? Integer.valueOf(request.getParameter("jobitemid")):null;
			JobItem ji = this.jiService.findJobItem(jobitemid);
				
			Integer connectedSubdivId = ((SubdivKeyValue) request.getSession()
					.getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV)).getKey();

			if (ji == null ||
					// for site job, must be connected to same subdiv as job ownership
					(JobType.SITE.equals(ji.getJob().getType()) && 
							(ji.getJob().getOrganisation().getSubdivid() != connectedSubdivId)) ||
					// for regular job with no current address (in transit), must connected to same subdiv as job ownership
					(!JobType.SITE.equals(ji.getJob().getType()) && (ji.getCurrentAddr() == null) && 
							(ji.getJob().getOrganisation().getSubdivid() != connectedSubdivId)) ||
					// for regular job with current address set, must either be connected to same address of job or have job ownership  
					(!JobType.SITE.equals(ji.getJob().getType()) && (ji.getCurrentAddr() != null) && 
							(ji.getJob().getOrganisation().getSubdivid() != connectedSubdivId) &&
							(ji.getCurrentAddr().getSub().getSubdivid() != connectedSubdivId)))
				return false;
			else 
				return authentication.getAuthorities().contains(permission);
		} else
			return authentication.getAuthorities().contains(permission);
	}

}