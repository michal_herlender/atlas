package org.trescal.cwms.core.login.controller;


import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Order(value = 1)
@ControllerAdvice
public class CustomAccessDeniedHandler {

    public static final Logger LOG
      = LoggerFactory.getLogger(CustomAccessDeniedHandler.class);

    @ExceptionHandler(value = { AccessDeniedException.class })
	public ModelAndView handleEmployeeNotFoundException(HttpServletRequest request, Exception ex){
		return new ModelAndView(new RedirectView("accessdenied.htm"));
	}
}