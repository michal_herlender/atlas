package org.trescal.cwms.core.exception.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.tools.labelprinting.alligator.xml.XmlAlligatorPrintJob;

/**
 * Currently, only the XmlPrintJob is returned as XML so if an error occurs, the same entity type is marshalled back. 
 * If we need other XML types this should be made more generic,
 * though Alligator will also need to be changed / tested if this strategy is altered.
 * @author Galen
 * 2017-12-21
 */
@ControllerAdvice(annotations={RestXmlController.class})
public class RestXmlExceptionHandlingController {

	private static final Logger logger = LoggerFactory.getLogger(RestXmlExceptionHandlingController.class);
	
	@ResponseStatus(value=HttpStatus.OK)
	@ExceptionHandler(Exception.class)
	public @ResponseBody XmlAlligatorPrintJob handleException(Exception e) {
		logger.error(e.getMessage(), e);
		StringBuffer result = new StringBuffer();
		result.append(e.getClass().getName());
		if (e.getLocalizedMessage() != null) {
			result.append(" : ");
			result.append(e.getLocalizedMessage());
		}
		
		return new XmlAlligatorPrintJob(false, result.toString());
	}	
}
