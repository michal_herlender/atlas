package org.trescal.cwms.core.exception.controller;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to indicate that a controller returns JSON responses (non REST web service)
 * Exception handling will return a string with exception message and 404 
 * @author Galen
 * 2016-06-10
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface JsonController {

}
