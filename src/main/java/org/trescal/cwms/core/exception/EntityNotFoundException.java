package org.trescal.cwms.core.exception;

/**
 * Exception thrown when an entity cannot be located.
 * 
 * TODO we should route to a general error page for this
 * (stack trace probably not needed - user may be accessing old data)
 * 
 * @author richard
 */
public class EntityNotFoundException extends RuntimeException
{
	private static final long serialVersionUID = -5325664878373722663L;
	
	private String entity;

	public EntityNotFoundException()
	{
		super();
	}

	public EntityNotFoundException(String entity)
	{
		super();
		this.entity = entity;
	}

	public String getEntity()
	{
		return this.entity;
	}
}
