package org.trescal.cwms.core.exception.controller;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to indicate that a controller returns JSON responses (REST web service only - not regular web app Json)
 * Exception Handling will return a RestResultDTO object for compatibility with the "calibration interface"
 * @author Galen Beck
 * 2016-10-21
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RestJsonController {

}
