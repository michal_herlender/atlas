package org.trescal.cwms.core.exception.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.core.exception.dto.JsonExceptionDto;

@ControllerAdvice(annotations={JsonController.class})
public class JsonExceptionHandlingController {

	@ResponseStatus(value=HttpStatus.BAD_REQUEST)
	@ExceptionHandler(Exception.class)
	public @ResponseBody JsonExceptionDto handleException(Exception e) {
		e.printStackTrace();
		return new JsonExceptionDto(e.getLocalizedMessage());
	}
}
