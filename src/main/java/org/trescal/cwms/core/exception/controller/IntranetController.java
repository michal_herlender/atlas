package org.trescal.cwms.core.exception.controller;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to indicate that a controller is an intranet application controller for exception handling purposes
 * returning a traditional web response (i.e. user should get well formed Intranet error page)
 * @author Galen
 * 2016-06-10
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface IntranetController {

}
