package org.trescal.cwms.core.exception;

/**
 * Exception thrown on the website when a user tries to access/update
 * information belonging to another company! e.g. a user trying to view a job
 * for a company other than the one they work for!
 * 
 * @author jamiev
 */
@SuppressWarnings("serial")
public class WebExternalPermissionException extends WebPermissionException
{
	public WebExternalPermissionException(int personid, String name, Class<?> clazz, String identifier)
	{
		super(personid, name, clazz, identifier);
	}
}