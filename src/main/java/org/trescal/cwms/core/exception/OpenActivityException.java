package org.trescal.cwms.core.exception;

/**
 * Exception thrown when an activity is attempted to be logged against an item
 * already having another activity performed.
 * 
 * @author jamiev
 */
@SuppressWarnings("serial")
public class OpenActivityException extends RuntimeException
{
	private String attemptedActivity;
	private String currentActivity;
	private String itemDetails;

	public OpenActivityException(String itemDetails, String currentActivity, String attemptedActivity)
	{
		super();
		this.itemDetails = itemDetails;
		this.currentActivity = currentActivity;
		this.attemptedActivity = attemptedActivity;
	}

	public String getAttemptedActivity()
	{
		return this.attemptedActivity;
	}

	public String getCurrentActivity()
	{
		return this.currentActivity;
	}

	public String getItemDetails()
	{
		return this.itemDetails;
	}
	
	@Override
	public String getMessage() {
		return "Activity already in progress for item "+getItemDetails()+". Current activity "+getCurrentActivity()+". Attempted activity "+getAttemptedActivity();
	}
}