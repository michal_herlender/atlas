package org.trescal.cwms.core.exception;

/**
 * Exception thrown on the website when a user tries to access/update
 * information belonging to their own company but without the correct company
 * permissions to do so. e.g. a user trying to view a job for another subdiv
 * within their company when they only have location access
 * 
 * @author jamiev
 */
@SuppressWarnings("serial")
public class WebInternalPermissionException extends WebPermissionException
{
	public WebInternalPermissionException(int personid, String name, Class<?> clazz, String identifier)
	{
		super(personid, name, clazz, identifier);
	}
}