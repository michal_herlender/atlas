package org.trescal.cwms.core.exception.dto;

/*
 * Proper entity that can be serialized to a JSON response
 * (just a message String can't be used, it becomes text/html as it is not JSON format)
 */
public class JsonExceptionDto {
	private String message;

	public JsonExceptionDto(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
