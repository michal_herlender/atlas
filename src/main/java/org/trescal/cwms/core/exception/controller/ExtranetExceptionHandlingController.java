package org.trescal.cwms.core.exception.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.trescal.cwms.core.exception.WebPermissionException;

@ControllerAdvice(annotations={ExtranetController.class})
public class ExtranetExceptionHandlingController {
	
	@ExceptionHandler(Exception.class)
	public String exceptionPage(Exception ex, Model model) {
		ex.printStackTrace();
		model.addAttribute("error", ex);
		if (WebPermissionException.class.isAssignableFrom(ex.getClass())) {
			// Already running in 'web' context so this will return web/login/nopermission
			return "login/nopermission";
		}
		else {
			// Already running in 'web' context so this will return web/login/error
			return "login/error";
		}
	}
}
