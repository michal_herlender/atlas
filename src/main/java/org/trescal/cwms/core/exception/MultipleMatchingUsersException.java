package org.trescal.cwms.core.exception;

/**
 * Exception thrown on the website when a user tries to request login details
 * with their e-mail address, but the e-mail address is used for many contacts
 * 
 * @author jamiev
 */
@SuppressWarnings("serial")
public class MultipleMatchingUsersException extends Exception
{
	public MultipleMatchingUsersException()
	{
	}
}
