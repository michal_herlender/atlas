package org.trescal.cwms.core.exception.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice(annotations = { IntranetController.class })
@Slf4j
public class IntranetExceptionHandlingController {

	@ExceptionHandler(Exception.class)
	public String exceptionPage(Exception ex, Model model, HttpServletResponse response) {
		String stackTrace = ExceptionUtils.getStackTrace(ex);
		String message = ex.getMessage();
		log.error(message);
		log.error(stackTrace);
		
		model.addAttribute("error", ex);
		model.addAttribute("message", message);
		model.addAttribute("stackTrace", stackTrace);
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		return "trescal/core/login/error";
	}
}