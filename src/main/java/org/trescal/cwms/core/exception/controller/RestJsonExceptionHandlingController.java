package org.trescal.cwms.core.exception.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.trescal.cwms.rest.common.dto.output.RestResultDTO;

@ControllerAdvice(annotations={RestJsonController.class})
public class RestJsonExceptionHandlingController {

	private static final Logger logger = LoggerFactory.getLogger(RestJsonExceptionHandlingController.class); 
	
	@ResponseStatus(value=HttpStatus.OK)
	@ExceptionHandler(Exception.class)
	public @ResponseBody RestResultDTO<Object> handleException(Exception e) {
		logger.error(e.getMessage(), e);
		StringBuffer result = new StringBuffer();
		result.append(e.getClass().getName());
		if (e.getLocalizedMessage() != null) {
			result.append(" : ");
			result.append(e.getLocalizedMessage());
		}
		
		return new RestResultDTO<Object>(false, result.toString());
	}
}
