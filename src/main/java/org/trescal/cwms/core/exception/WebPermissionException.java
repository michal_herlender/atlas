package org.trescal.cwms.core.exception;

/**
 * Exception thrown on the website when a user tries to access/update
 * information without appropriate permissions to do so
 * 
 * @author jamiev
 */
@SuppressWarnings("serial")
public abstract class WebPermissionException extends Exception
{
	private Class<?> objectClass;
	private String objectIdentifier;
	private int personId;
	private String personName;

	public WebPermissionException()
	{
		super();
	}

	public WebPermissionException(int personId, String personName, Class<?> objectClass, String objectIdentifier)
	{
		super();
		this.objectClass = objectClass;
		this.objectIdentifier = objectIdentifier;
		this.personId = personId;
		this.personName = personName;
	}

	public Class<?> getObjectClass()
	{
		return this.objectClass;
	}

	public String getObjectIdentifier()
	{
		return this.objectIdentifier;
	}

	public int getPersonId()
	{
		return this.personId;
	}

	public String getPersonName()
	{
		return this.personName;
	}
}