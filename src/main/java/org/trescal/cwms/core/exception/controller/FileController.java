package org.trescal.cwms.core.exception.controller;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used to indicate that a controller provides file management (download/upload)
 * exception handling TBD
 * @author Galen
 * 2016-06-10
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface FileController {

}
