package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault.TPQuotationCaltypeDefault;

public interface TPQuotationCaltypeDefaultDao extends BaseDao<TPQuotationCaltypeDefault, Integer> {}