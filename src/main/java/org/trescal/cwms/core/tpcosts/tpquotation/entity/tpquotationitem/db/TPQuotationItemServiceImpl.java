package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.db;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.directwebremoting.util.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.pricing.jobcost.entity.jobcostingitem.JobCostingItem;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.db.NoteService;
import org.trescal.cwms.core.tools.MathTools;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tpcosts.tpquotation.dto.TPQuotationItemConvertedCurrency;
import org.trescal.cwms.core.tpcosts.tpquotation.dto.TPQuotationItemCostDTO;
import org.trescal.cwms.core.tpcosts.tpquotation.dto.TPQuotationItemDTO;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.adjcost.TPQuotationAdjustmentCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost.TPQuotationCalibrationCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.purchasecost.TPQuotationPurchaseCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.repcost.TPQuotationRepairCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote.TPQuoteItemNote;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.TPQuoteLink;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.TPQuoteLinkComparator;
import org.trescal.cwms.core.tpcosts.tpquotation.form.EditBulkTPQuotationItemForm;
import org.trescal.cwms.core.tpcosts.tpquotation.form.EditTPQuotationItemForm;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitemnote.TPQuoteRequestItemNote;

import lombok.val;

@Service("TPQuotationItemService")
public class TPQuotationItemServiceImpl extends BaseServiceImpl<TPQuotationItem, Integer>
		implements TPQuotationItemService {
	public static final Logger logger = Logger.getLogger(TPQuotationItemServiceImpl.class);

	@Autowired
	private CalibrationTypeService calTypeService;
	@Autowired
	private QuotationItemService qiServ;
	@Autowired
	private TPQuotationItemDao tpQuotationItemDao;
	@Autowired
	private TPQuotationService tpQuoteServ;
	@Autowired
	private NoteService noteService;
	@Autowired
	private MessageSource messageSource;

	@Override
	protected BaseDao<TPQuotationItem, Integer> getBaseDao() {
		return tpQuotationItemDao;
	}

	@Override
	public ResultWrapper ajaxDeleteTPQuotationItems(String listOfIds) {
		String[] qiStringIds = listOfIds.split(",");
		Integer[] qiIds = new Integer[qiStringIds.length];
		int i = 0;
		while (i < qiStringIds.length) {
			qiIds[i] = Integer.parseInt(qiStringIds[i]);
			i++;
		}

		TPQuotation tpquote = null;
		List<TPQuotationItem> tpqis = new ArrayList<>();

		for (Integer id : qiIds) {
			// get quote item
			TPQuotationItem tpqitem = this.get(id);
			tpqis.add(tpqitem);
		}

		for (TPQuotationItem tpqitem : tpqis) {
			// quotation yet to be loaded?
			if (tpquote == null) {
				tpquote = this.tpQuoteServ.get(tpqitem.getTpquotation().getId());
			}

			Set<TPQuotationItem> modules = tpqitem.getModules();
			tpqitem.setModules(null);

			// first delete any modules if this is a base unit
			for (TPQuotationItem mod : modules) {
				this.deleteTPQuoteItem(tpquote, mod);
			}

			// remove quote item from references and delete
			this.deleteTPQuoteItem(tpquote, tpqitem);

			// update the total cost of the quotation
			CostCalculator.updatePricingFinalCost(tpquote, tpquote.getItems());
		}
		this.tpQuoteServ.merge(tpquote);
		return new ResultWrapper(true, "Third party quotation items successfully deleted");
	}

	public BigDecimal calculateRunningTotal(TPQuotationItem tpquotationitem) {
		BigDecimal runningTotal = new BigDecimal("0.00");
		if (tpquotationitem.getAdjustmentCost() != null) {
			if (tpquotationitem.getAdjustmentCost().isActive()) {
				runningTotal = runningTotal.add(tpquotationitem.getAdjustmentCost().getFinalCost());
			}
		}
		if (tpquotationitem.getCalibrationCost() != null) {
			if (tpquotationitem.getCalibrationCost().isActive()) {
				runningTotal = runningTotal.add(tpquotationitem.getCalibrationCost().getFinalCost());
			}
		}
		if (tpquotationitem.getRepairCost() != null) {
			if (tpquotationitem.getRepairCost().isActive()) {
				runningTotal = runningTotal.add(tpquotationitem.getRepairCost().getFinalCost());
			}
		}
		if (tpquotationitem.getPurchaseCost() != null) {
			if (tpquotationitem.getPurchaseCost().isActive()) {
				runningTotal = runningTotal.add(tpquotationitem.getPurchaseCost().getFinalCost());
			}
		}

		runningTotal = runningTotal.add(tpquotationitem.getCarriageIn()).add(tpquotationitem.getCarriageOut());

		return runningTotal;
	}

	@Override
	public ResultWrapper checkQuotationItemIsSafeToDelete(TPQuotationItem tpqi) {
		int i = tpqi.getCalibrationCost().getLinkedJobCalCosts().size();
		i += tpqi.getCalibrationCost().getLinkedQuoteCalCosts().size();
		i += tpqi.getPurchaseCost().getLinkedJobCalCosts().size();
		i += tpqi.getPurchaseCost().getLinkedQuoteCalCosts().size();
		i += tpqi.getRepairCost().getLinkedJobRepCosts().size();
		i += tpqi.getAdjustmentCost().getLinkedJobAdjCosts().size();

		if (i > 0) {
			String reason = "Item " + tpqi.getItemno()
					+ " cannot be deleted because it has been used as a cost source on the following:<br/>";

			for (JobCostingCalibrationCost jccc : tpqi.getCalibrationCost().getLinkedJobCalCosts()) {
				if ((jccc != null) && (jccc.getJobCostingItem() != null)) {
					JobCostingItem jci = jccc.getJobCostingItem();
					reason = reason.concat("Job costing calibration cost for item " + jci.getItemno() + " on Job "
							+ jci.getJobCosting().getJob().getJobno() + ".<br/>");
				}
			}

			return new ResultWrapper(false, reason);
		} else {
			return new ResultWrapper(true, null);
		}
	}

	private BigDecimal convertCurrency(BigDecimal fromRate, BigDecimal toRate, BigDecimal value) {
		if ((fromRate != null) && (toRate != null) && (value != null)) {
			BigDecimal divisor = fromRate.divide(toRate, MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
			return value.divide(divisor, MathTools.SCALE_FIN_CALC, MathTools.ROUND_MODE_FIN);
		}

		return null;
	}

	@Override
	public TPQuotationItem createFromDTO(TPQuotationItemDTO dto) {
		// create the TPQuotationItem and initialize it's costs
		TPQuotationItem tpQuoteItem = new TPQuotationItem();
		tpQuoteItem.setCaltype(calTypeService.find(dto.getCalTypeId()));
		tpQuoteItem.setCarriageIn(dto.getCarriageIn());
		tpQuoteItem.setCarriageOut(dto.getCarriageOut());
		tpQuoteItem.setModel(dto.getModel());
		tpQuoteItem.setGeneralDiscountRate(dto.getDiscountRate());
		tpQuoteItem.setQuantity(dto.getQuantity());
		TPQuotationItemCostDTO adjCostDTO = dto.getCosts().stream()
				.filter(cost -> cost.getCostType() == CostType.ADJUSTMENT).findAny()
				.orElse(new TPQuotationItemCostDTO(CostType.ADJUSTMENT));
		TPQuotationAdjustmentCost adjCost = new TPQuotationAdjustmentCost();
		CostCalculator.populateCostType(adjCostDTO.getCostType(), adjCostDTO.getCost(), adjCostDTO.getDiscountRate(),
				true, adjCost);
		tpQuoteItem.setAdjustmentCost(adjCost);
		TPQuotationItemCostDTO calCostDTO = dto.getCosts().stream()
				.filter(cost -> cost.getCostType() == CostType.CALIBRATION).findAny()
				.orElse(new TPQuotationItemCostDTO(CostType.CALIBRATION));
		TPQuotationCalibrationCost calCost = new TPQuotationCalibrationCost();
		CostCalculator.populateCostType(calCostDTO.getCostType(), calCostDTO.getCost(), calCostDTO.getDiscountRate(),
				true, calCost);
		tpQuoteItem.setCalibrationCost(calCost);
		TPQuotationItemCostDTO inspectionCostDTO = dto.getCosts().stream()
				.filter(cost -> cost.getCostType() == CostType.INSPECTION).findAny()
				.orElse(new TPQuotationItemCostDTO(CostType.INSPECTION));
		tpQuoteItem.setInspection(inspectionCostDTO.getCost());
		TPQuotationItemCostDTO purchaseCostDTO = dto.getCosts().stream()
				.filter(cost -> cost.getCostType() == CostType.PURCHASE).findAny()
				.orElse(new TPQuotationItemCostDTO(CostType.PURCHASE));
		TPQuotationPurchaseCost purchaseCost = new TPQuotationPurchaseCost();
		CostCalculator.populateCostType(purchaseCostDTO.getCostType(), purchaseCostDTO.getCost(),
				purchaseCostDTO.getDiscountRate(), true, purchaseCost);
		tpQuoteItem.setPurchaseCost(purchaseCost);
		TPQuotationItemCostDTO repairCostDTO = dto.getCosts().stream()
				.filter(cost -> cost.getCostType() == CostType.REPAIR).findAny()
				.orElse(new TPQuotationItemCostDTO(CostType.REPAIR));
		TPQuotationRepairCost repairCost = new TPQuotationRepairCost();
		CostCalculator.populateCostType(repairCostDTO.getCostType(), repairCostDTO.getCost(),
				repairCostDTO.getDiscountRate(), true, repairCost);
		tpQuoteItem.setRepairCost(repairCost);
		if (dto.getModules() != null) {
			tpQuoteItem.setModules(new TreeSet<>());
			for (TPQuotationItemDTO module : dto.getModules())
				tpQuoteItem.getModules().add(this.createFromDTO(module));
		}
		this.updateTPQuotationItemCosts(tpQuoteItem);
		return tpQuoteItem;
	}

	private void deleteTPQuoteItem(TPQuotation tpquote, TPQuotationItem tpqitem) {
		// remove the module from it's base unit
		if (tpqitem.isPartOfBaseUnit()) {
			TPQuotationItem base = tpqitem.getBaseUnit();

			if ((base.getModules() != null) && (base.getModules().size() > 0)) {
				base.getModules().remove(tpqitem);
				this.merge(base);
			}
		}
		// explicity remove this item from the Quotation set of QuotationItems
		// (otherwise hibernate chokes)
		tpquote.getItems().remove(tpqitem);
		// delete the item
		this.delete(tpqitem);
	}

	@Override
	public List<TPQuotationItem> findTPQuotationItemsWithCosts(List<Integer> itemIds) {
		return this.tpQuotationItemDao.getWithCosts(itemIds);
	}

	@Override
	public TPQuotationItem findTPQuotationItemWithCosts(int id) {
		return this.tpQuotationItemDao.getWithCosts(id);
	}

	@Override
	public List<TPQuotationItem> getMatchingItems(String costType, int modelid) {
		CostType ct = CostType.valueOf(costType.toUpperCase());
		return this.tpQuotationItemDao.getMatchingItems(ct, modelid);
	}

	@Override
	public Integer getMaxItemno(int tpQuoteId) {
		return this.tpQuotationItemDao.getMaxItemno(tpQuoteId);
	}

	public TPQuotationItemConvertedCurrency getTPQuoteItemInConvertedCurrency(int tpQuoteItemId, int quoteItemId,
			String costType) {
		TPQuotationItem tpqi = this.get(tpQuoteItemId);
		Quotationitem qi = this.qiServ.findQuotationItem(quoteItemId);

		TPQuotationItemConvertedCurrency temp = new TPQuotationItemConvertedCurrency();

		if (costType.trim().equalsIgnoreCase("calibration")) {
			temp.setCost(
					NumberTools.displayBigDecimalTo2DecimalPlaces(this.convertCurrency(tpqi.getTpquotation().getRate(),
							qi.getQuotation().getRate(), tpqi.getCalibrationCost().getFinalCost())));
		} else if (costType.trim().equalsIgnoreCase("purchase")) {
			temp.setCost(
					NumberTools.displayBigDecimalTo2DecimalPlaces(this.convertCurrency(tpqi.getTpquotation().getRate(),
							qi.getQuotation().getRate(), tpqi.getPurchaseCost().getFinalCost())));
		}

		temp.setCarriageIn(NumberTools.displayBigDecimalTo2DecimalPlaces(this
				.convertCurrency(tpqi.getTpquotation().getRate(), qi.getQuotation().getRate(), tpqi.getCarriageIn())));
		temp.setCarriageOut(NumberTools.displayBigDecimalTo2DecimalPlaces(this
				.convertCurrency(tpqi.getTpquotation().getRate(), qi.getQuotation().getRate(), tpqi.getCarriageOut())));

		return temp;
	}

	@Override
	public void saveOrUpdateAllTPQuotationItems(List<TPQuotationItem> tpquotationitems) {
		tpquotationitems.forEach(this::merge);
	}

	@Override
	public PagedResultSet<TPQuotationItem> searchAjaxTPQuotationItem(Integer modelid, Integer years, boolean expired,
			Integer coid, int currentPage, int resultsPerPage) {
		PagedResultSet<TPQuotationItem> ps = new PagedResultSet<>(resultsPerPage, currentPage);
		return this.searchTPQuotationItem(ps, modelid, years, expired, coid);
	}

	@Override
	public PagedResultSet<TPQuotationItem> searchTPQuotationItem(PagedResultSet<TPQuotationItem> ps, Integer modelid,
			Integer years, boolean expired, Integer coid) {
		return this.tpQuotationItemDao.searchTPQuotationItem(ps, modelid, years, expired, coid);
	}

	@Override
	/*
	 * Aggregates the costs for the given TPQuotationItem.
	 * 
	 * @param tpquotationitem the TPQuotationItem to aggregate the costs for
	 */
	public void updateTPQuotationItemCosts(TPQuotationItem tpquotationitem) {
		// add up the various Costs included for this item
		BigDecimal runningTotal = this.calculateRunningTotal(tpquotationitem);
		tpquotationitem.setTotalCost(runningTotal);

		// re-assess the situation with carriage and items with multiple
		// quantities: cwms1 used to multiply the carriage by the quantity so we
		// will continue with this for the time being - to change this uncomment
		// the following in place of the cost calculator call below and remove
		// carriage costs from update above. bare in mind the legacy data will
		// all have to be updated to do this!

		// now update the discount + final cost details
		CostCalculator.updateItemCost(tpquotationitem);
	}

	@Override
	public void createTPQuotationItems(List<TPQuotationItemDTO> items, TPQuotation tpQuote,
			TPQuoteRequest tpQuoteRequest, Contact currentContact) {
		val itemNo = new AtomicInteger(0);
		tpQuote.setItems(new TreeSet<>());

		for (TPQuotationItemDTO item : items.stream().filter(TPQuotationItemDTO::isSetOnQuotation)
				.collect(Collectors.toList())) {
			// create the TPQuotationItem and initialize it's costs
			TPQuotationItem tpQuoteItem = this.createFromDTO(item);
			tpQuoteItem.setTpquotation(tpQuote);
			tpQuoteItem.setItemno(itemNo.getAndIncrement());
			tpQuoteItem.setNotes(new TreeSet<TPQuoteItemNote>(new NoteComparator()));
			tpQuoteRequest.getItems().stream()
					.filter(qri -> qri.getId() == item.getRequestItemId() && qri.getRequestFromJobItem() != null)
					.forEach(qri -> {
						TPQuoteLink link = new TPQuoteLink();
						link.setComment(messageSource.getMessage("message.tpquotation.autolinked", null,
								"Autolinked on receipt of quotation", null));
						link.setContact(currentContact);
						link.setDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
						link.setJobItem(qri.getRequestFromJobItem());
						link.setTpQuoteItem(tpQuoteItem);
						tpQuoteItem.setLinkedTo(new TreeSet<>(new TPQuoteLinkComparator()));
						tpQuoteItem.getLinkedTo().add(link);
					});
//			this.save(tpQuoteItem);
			for (Integer noteid : item.getNoteIds()) {
				TPQuoteItemNote note = new TPQuoteItemNote();
				Note requestNote = this.noteService.findNote(noteid, TPQuoteRequestItemNote.class);
				if (requestNote != null && requestNote.isActive()) {
					note.setNote(requestNote.getNote());
					note.setPublish(requestNote.getPublish());
					note.setSetBy(requestNote.getSetBy());
					note.setLabel(requestNote.getLabel());
					note.setSetOn(new Date());
					note.setActive(true);
					note.setTpquotationitem(tpQuoteItem);
					this.noteService.saveOrUpdate(note);
					tpQuoteItem.getNotes().add(note);
				}
			}
			tpQuote.getItems().add(tpQuoteItem);
		}
		;
	}

	@Override
	public void copyFileIntoTPQuotationDirectory(TPQuotation tpQuote, MultipartFile file) throws Exception {
		if (file != null) {
			String name = file.getOriginalFilename();
			if (!name.equals("")) {
				if (name.lastIndexOf('\\') > -1)
					name = name.substring(name.lastIndexOf('\\'));
				file.transferTo(new File(tpQuote.getDirectory().getAbsolutePath().concat(File.separator).concat(name)));
			}
		}
	}

	@Override
	public TPQuotation editTpQuoteBulkItem(EditBulkTPQuotationItemForm form) {


		int i = 0;
		for (TPQuotationItem item : form.getTpQitems()) {

			int calTypeId = form.getCalTypeIds().get(i);
			
			if (calTypeId == 0) {
				item.setCaltype(null);
			} else if ((item.getCaltype() == null) || (item.getCaltype().getCalTypeId() != calTypeId)) {
				item.setCaltype(this.calTypeService.find(calTypeId));
			}
			
			item.setAdjustmentCost((TPQuotationAdjustmentCost) CostCalculator.updateSingleCost(item.getAdjustmentCost(), false));
			item.setCalibrationCost((TPQuotationCalibrationCost) CostCalculator.updateSingleCost(item.getCalibrationCost(), false));
			item.setRepairCost((TPQuotationRepairCost) CostCalculator.updateSingleCost(item.getRepairCost(), false));
			item.setPurchaseCost((TPQuotationPurchaseCost) CostCalculator.updateSingleCost(item.getPurchaseCost(), false));
			
			this.updateTPQuotationItemCosts(item);
			
			i++;
		}

		this.saveOrUpdateAllTPQuotationItems(form.getTpQitems());

		// update the total costs of the TPQuotation
		TPQuotation tpQuote = this.tpQuoteServ.get(form.getTpQuote().getId());
		CostCalculator.updatePricingFinalCost(tpQuote, tpQuote.getItems());
		this.tpQuoteServ.update(tpQuote);

		return tpQuote;
	}

	@Override
	public TPQuotationItem editTpquoteItem(EditTPQuotationItemForm form) {
		TPQuotationItem item = form.getTpQitem();

		// only search for a new CalType if the user is actually changing it
		if ((item.getCaltype() == null) || (item.getCaltype().getCalTypeId() != form.getCaltypeid())) {
			item.setCaltype(this.calTypeService.find(form.getCaltypeid()));
		}

		item.setAdjustmentCost(
				(TPQuotationAdjustmentCost) CostCalculator.updateSingleCost(item.getAdjustmentCost(), false));
		item.setCalibrationCost(
				(TPQuotationCalibrationCost) CostCalculator.updateSingleCost(item.getCalibrationCost(), false));
		item.setRepairCost((TPQuotationRepairCost) CostCalculator.updateSingleCost(item.getRepairCost(), false));
		item.setPurchaseCost((TPQuotationPurchaseCost) CostCalculator.updateSingleCost(item.getPurchaseCost(), false));

		this.updateTPQuotationItemCosts(item);

		this.update(item);

		// update the total costs of the TPQuotation
		TPQuotation tpQuote = this.tpQuoteServ.get(item.getTpquotation().getId());
		CostCalculator.updatePricingFinalCost(tpQuote, tpQuote.getItems());
		this.tpQuoteServ.update(tpQuote);
		return item;
	}

}