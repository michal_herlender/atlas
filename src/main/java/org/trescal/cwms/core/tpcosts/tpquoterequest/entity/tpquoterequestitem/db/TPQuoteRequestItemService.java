package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.db;

import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface TPQuoteRequestItemService {
    /**
     * Function looks at the TPQuoteRequestDefaultCostType list belonging to the
     * TPQuoteRequest and converts this into a list of TPQuoteRequestedCostTypes
     * for the given TPQuoteRequestItem.
     *
     * @param tpQuoteReqItem TPQuoteRequestItem to append the CostTypes to.
     * @param tpQuoteReq     TPQuoteRequest to obtain the default costs from.
     * @return the TPQuoteRequestItem with the populated
     * TPQuoteRequestedCostTYpe list.
     */
	void addItemCostTypesFromDefaults(TPQuoteRequestItem tpQuoteReqItem, TPQuoteRequest tpQuoteReq);

	void copyItemsToNewTPQuoteRequest(List<Integer> oldTPQRItemIds, TPQuoteRequest tpqr, Contact currentContact);

	/**
	 * Creates a new TPQuoteRequestItem based on the {@link JobItem} passed as
	 * the first argument. If the {@link JobItem} is a base unit then function
	 * will recursively call itself to add each module for the base unit.
	 * 
	 * @param ji a {@link JobItem}
	 * @param tpQuoteRequest the TPQuoteRequest that the item will belong to
	 * @param qitems the entire list of {@link JobItem} (used for module
	 *        checking)
	 * @param costTypes list of all CostTypes that the TPQuoteRequestItem should
	 *        be assigned
	 * @return TPQuoteRequestItem
	 */
	TPQuoteRequestItem createTPQuoteItem(JobItem ji, TPQuoteRequest tpQuoteRequest, List<JobItem> qitems, List<CostType> costTypes);

	/**
	 * Creates a new TPQuoteRequestItem based on the QuotationItem passed as the
	 * first argument. If the QuotationItem is a base unit then function will
	 * recursively call itself to add each module for the base unit.
	 * 
	 * @param qi a QuotationItem
	 * @param tpQuoteRequest the TPQuoteRequest that the item will belong to
	 * @param qitems the entire list of QuotationItems (used for module
	 *        checking)
	 * @param costTypes list of all CostTypes that the TPQuoteRequestItem should
	 *        be assigned
	 * @return TPQuoteRequestItem
	 */
	TPQuoteRequestItem createTPQuoteItem(Quotationitem qi, TPQuoteRequest tpQuoteRequest, List<Quotationitem> qitems, List<CostType> costTypes);

	void deleteTPQuoteRequestItem(int id);

	TPQuoteRequestItem findTPQuoteRequestItem(int id);

	/**
	 * Returns a {@link List} of {@link TPQuoteRequestItem} entities that have
	 * ids matching those in the list parameter.
	 * 
	 * @param ids list of {@link TPQuoteRequestItem} ids.
	 * @return list of {@link TPQuoteRequestItem} entities.
	 */
	List<TPQuoteRequestItem> getAllTPQRItems(List<Integer> ids);

	List<TPQuoteRequestItem> getAllTPQuoteRequestItems();

	void insertTPQuoteRequestItem(TPQuoteRequestItem tpquoterequestitem);

	/**
	 * Creates a Set and adds a collection of CostTypes which
	 * then become the set of cost types for a TPQuoteRequestItem
	 * 
	 * @param tpItem The TPQuoteRequestItem
	 * @param costTypeList a List of CostTypes
	 */
	void loadCostTypes(TPQuoteRequestItem tpItem, List<CostType> costTypeList);

    /**
     * @param tpItemId The TPQuoteRequestItem
     * @return List<KeyValue < Integer, String>> a List of CostTypes
	 */
	List<KeyValue<Integer, String>> getAvailableTPQuoteRequestCostTypeDtos(int tpItemId, HttpServletRequest request);

	void addCostType(int costTypeId, int tpQuoteRequestId);

	void saveOrUpdateAll(List<TPQuoteRequestItem> tpItems);

	void updateTPQuoteRequestItem(TPQuoteRequestItem tpquoterequestitem);
}