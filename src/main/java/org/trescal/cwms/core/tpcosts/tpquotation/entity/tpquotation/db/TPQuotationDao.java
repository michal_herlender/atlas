package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.AllocatedDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.form.TPQuoteSearchForm;

public interface TPQuotationDao extends AllocatedDao<Company, TPQuotation, Integer>
{
	TPQuotation findEagerTPQuotation(int id);
	
	/**
	 * Loads a TPQuotation item and all of it's associated costs eagerly
	 * 
	 * @param id the id of the TPQuotation
	 * @return TPQuotation the third party quotation requested
	 */
	TPQuotation findTPQuotationWithItems(int id);
	
	PagedResultSet<TPQuotation> getTPQuotations(PagedResultSet<TPQuotation> rs, TPQuoteSearchForm tpsf);
	
	TPQuotation getTPQuoteByExactTPQNo(String tpqNo);
	
	List<TPQuotation> getTPQuotesFromTPRequest(int tpqrId);
}