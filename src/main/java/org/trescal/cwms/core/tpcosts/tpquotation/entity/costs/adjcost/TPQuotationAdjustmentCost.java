/**
 * 
 */
package org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.adjcost;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.trescal.cwms.core.pricing.entity.costs.base.AdjCost;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.adjcost.JobCostingAdjustmentCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

@Entity
@DiscriminatorValue("tpquotation")
public class TPQuotationAdjustmentCost extends AdjCost
{
	/**
	 * All Job Costing Adjustment costs ( {@link JobCostingAdjustmentCost})
	 * derived from this {@link TPQuotationAdjustmentCost}.
	 */
	private Set<JobCostingAdjustmentCost> linkedJobAdjCosts;

	private TPQuotationItem tpQuoteItem;

	@Override
	@Transient
	public Cost getLinkedCostSrc()
	{
		return null;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "linkedCostSrc")
	public Set<JobCostingAdjustmentCost> getLinkedJobAdjCosts()
	{
		return this.linkedJobAdjCosts;
	}

	/**
	 * @return the tpQuoteItem
	 */
	@OneToOne(mappedBy = "adjustmentCost")
	public TPQuotationItem getTpQuoteItem()
	{
		return this.tpQuoteItem;
	}

	/**
	 * @param linkedJobAdjCosts the linkedJobAdjCosts to set
	 */
	public void setLinkedJobAdjCosts(Set<JobCostingAdjustmentCost> linkedJobAdjCosts)
	{
		this.linkedJobAdjCosts = linkedJobAdjCosts;
	}

	/**
	 * @param tpQuoteItem the tpQuoteItem to set
	 */
	public void setTpQuoteItem(TPQuotationItem tpQuoteItem)
	{
		this.tpQuoteItem = tpQuoteItem;
	}
}
