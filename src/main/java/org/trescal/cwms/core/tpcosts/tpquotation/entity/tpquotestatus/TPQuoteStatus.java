package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.trescal.cwms.core.system.entity.status.Status;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;

@Entity
@DiscriminatorValue("tpquotation")
public class TPQuoteStatus extends Status
{
	private Set<TPQuotation> tpquotations = new HashSet<TPQuotation>(0);
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "status")
	public Set<TPQuotation> getTpquotations()
	{
		return tpquotations;
	}

	public void setTpquotations(Set<TPQuotation> tpquotations)
	{
		this.tpquotations = tpquotations;
	}
}
