package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem;

import java.util.Set;
import java.util.SortedSet;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import lombok.Setter;
import org.hibernate.annotations.SortComparator;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.costtype.CostTypeComparator;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitemnote.TPQuoteRequestItemNote;

@Entity
@Table(name = "tpquoterequestitem")
@Setter
public class TPQuoteRequestItem extends Auditable implements NoteAwareEntity {

    private TPQuoteRequestItem baseUnit;
    private CalibrationType caltype;
    private SortedSet<CostType> costTypes;
    private int id;
    private InstrumentModel model;
    private Set<TPQuoteRequestItem> modules;
    private Set<TPQuoteRequestItemNote> notes;
    private boolean partOfBaseUnit;
    private int qty;
    private String referenceNo;
    private JobItem requestFromJobItem;
    private Quotationitem requestFromQuoteItem;
    private TPQuoteRequest tpQuoteRequest;

    @ManyToOne(cascade = {}, fetch = FetchType.EAGER)
    @JoinColumn(name = "baseid", nullable = true)
    public TPQuoteRequestItem getBaseUnit() {
        return this.baseUnit;
    }

    @ManyToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name = "caltypeid", nullable = true)
    public CalibrationType getCaltype() {
        return this.caltype;
    }

    /* Was being stored via join table, can now be an ElementCollection of enums GB 2015-10-06
    @Entity
    @Table(	name="tpquoterequestedcosttype",
            uniqueConstraints = { @UniqueConstraint(columnNames = { "itemid", "costtypeid" }) })
    @OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "tpQuoteRequestItem")
    @SortComparator(TPQuoteRequestedCostType.TPQuoteRequestCostTypeComparator.class) */
    @ElementCollection
    @CollectionTable(name = "tpquoterequestedcosttype", joinColumns = @JoinColumn(name = "itemid"))
    @Column(name = "costtypeid")
    @SortComparator(CostTypeComparator.class)
    public SortedSet<CostType> getCostTypes() {
        return this.costTypes;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @Type(type = "int")
    public int getId() {
        return this.id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modelid", nullable = true)
    public InstrumentModel getModel() {
        return this.model;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER, mappedBy = "baseUnit")
    public Set<TPQuoteRequestItem> getModules() {
        return this.modules;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "tpQuoteRequestItem")
    @SortComparator(NoteComparator.class)
    public Set<TPQuoteRequestItemNote> getNotes() {
        return this.notes;
    }

    @Transient
    public Integer getPrivateActiveNoteCount() {
        return NoteCountCalculator.getPrivateActiveNoteCount(this);
    }

    @Transient
    public Integer getPrivateDeactivatedNoteCount() {
        return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
    }

    @Transient
    public Integer getPublicActiveNoteCount() {
        return NoteCountCalculator.getPublicActiveNoteCount(this);
    }

    @Transient
    public Integer getPublicDeactivatedNoteCount() {
        return NoteCountCalculator.getPublicDeactivedNoteCount(this);
    }

    @NotNull
    @Column(name = "qty", nullable = false)
    @Type(type = "int")
    public int getQty() {
        return this.qty;
    }

    @Length(max = 50, min = 0)
    @Column(name = "referenceno", length = 50, nullable = true)
    public String getReferenceNo() {
        return this.referenceNo;
    }

    @ManyToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name = "jobitemid", nullable = true)
    public JobItem getRequestFromJobItem() {
        return this.requestFromJobItem;
    }

    @ManyToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name = "quoteitemid", nullable = true, foreignKey = @ForeignKey(name = "FK_tpquoterequestitem_quoteitemid"))
    public Quotationitem getRequestFromQuoteItem() {
        return this.requestFromQuoteItem;
    }

    @ManyToOne(cascade = {}, fetch = FetchType.LAZY)
    @JoinColumn(name = "tpquotereqid", nullable = false)
    public TPQuoteRequest getTpQuoteRequest() {
        return this.tpQuoteRequest;
    }

    @NotNull
    @Column(name = "partofbaseunit", nullable = false, columnDefinition = "tinyint")
    public boolean isPartOfBaseUnit() {
        return this.partOfBaseUnit;
    }
}