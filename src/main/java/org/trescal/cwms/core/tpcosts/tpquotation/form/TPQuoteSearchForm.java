package org.trescal.cwms.core.tpcosts.tpquotation.form;

import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TPQuoteSearchForm
{
	private Integer allocatedSubdivId;
	private String clientref;
	private Integer coid;
	private String coname;
	private String contact;
	private Integer descid;
	private String description;
	private String mfr;
	private Integer mfrid;
	private String model;
	private Integer modelid;
	private int pageNo;
	private Integer personid;
	private String qno;
	private int resultsPerPage;
	private PagedResultSet<TPQuotation> rs;
	private Integer statusid;
	private String tpqno;

}
