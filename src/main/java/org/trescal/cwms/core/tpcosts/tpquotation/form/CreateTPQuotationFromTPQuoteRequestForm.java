package org.trescal.cwms.core.tpcosts.tpquotation.form;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.tpcosts.tpquotation.dto.TPQuotationItemDTO;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class CreateTPQuotationFromTPQuoteRequestForm {
    private String tpQuoteNumber;
    @NotNull
    private Integer contactId;
    private LocalDate regDate;
    private LocalDate issueDate;
    @NotNull
    @Min(1)
    private Integer duration;
    private Integer statusId;
    private MultipartFile file;
    private List<TPQuotationItemDTO> items;
    private List<Integer> noteIds = new ArrayList<>();


}