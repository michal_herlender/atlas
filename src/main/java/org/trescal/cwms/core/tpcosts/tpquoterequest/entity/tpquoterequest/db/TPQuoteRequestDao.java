package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tpcosts.tpquoterequest.dto.TPQuoteRequestDTO;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.TPQuoteRequestSearchForm;

public interface TPQuoteRequestDao extends BaseDao<TPQuoteRequest, Integer> {
	TPQuoteRequest getTPQuoteReqByExactTPQRNo(String tpqrNo, Company allocatedCompany);

	public PagedResultSet<TPQuoteRequestDTO> searchTPQuoteRequest(TPQuoteRequestSearchForm form,
			PagedResultSet<TPQuoteRequestDTO> rs, Company allocatedCompany);
	
	List<ContactKeyValue> getClientContacts(TPQuoteRequest tpQuoteRequest);
}