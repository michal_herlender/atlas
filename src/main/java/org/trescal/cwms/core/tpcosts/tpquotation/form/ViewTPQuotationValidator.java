package org.trescal.cwms.core.tpcosts.tpquotation.form;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import java.time.LocalDate;

@Component
public class ViewTPQuotationValidator extends AbstractBeanValidator {
	public boolean supports(Class<?> clazz) {
		return clazz.equals(CreateTPQuotationForm.class);
	}

	public void validate(Object target, Errors errors)
	{
		CreateTPQuotationForm command = (CreateTPQuotationForm) target;
		TPQuotation tpquote = command.getTpQuotation();
		
		super.validate(command, errors);

		// perform some custom date validation
		if ((tpquote.getIssuedate() != null)
			&& tpquote.getIssuedate().isAfter(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()))) {
			errors.rejectValue("tpQuotation.issuedate", "error.tpquotation.issuedate", null, "Issue date cannot be in the future");
		}
		// perform tp qno validation
		if ((tpquote.getTpqno() != null) && (!tpquote.getTpqno().equals(""))
				&& (tpquote.getTpqno().length() > 30))
		{
			errors.rejectValue("tpQuotation.tpqno", "error.tpquotation.tpqno", null, "Third party quote number should be 30 characters or less");
		}
		
	}
}