package org.trescal.cwms.core.tpcosts.tpquoterequest.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.filenamingservice.FileNamingService;
import org.trescal.cwms.core.documents.filenamingservice.TPQRFileNamingService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;

@Controller @IntranetController
@SessionAttributes({Constants.HTTPSESS_NEW_FILE_LIST})
public class TPQuoteRequestBirtDocumentController {
	@Autowired
	private DocumentService documentService;
	@Autowired
	private TPQuoteRequestService tpqrService;
	
	@RequestMapping(value="/tpqrbirtdocs.htm")
	public String handleRequest(Locale locale,
			@RequestParam(value="id", required=true) Integer tpqrId,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList) throws Exception
	{
		TPQuoteRequest tpqr = this.tpqrService.get(tpqrId);
		FileNamingService fnService = new TPQRFileNamingService(tpqr);
		Document doc = documentService.createBirtDocument(tpqrId, BaseDocumentType.TP_QUOTE_REQUEST, locale, Component.THIRD_PARTY_QUOTATION_REQUEST, fnService);
		documentService.addFileNameToSession(doc, sessionNewFileList);
		return "redirect:viewtpquoterequest.htm?id="+tpqrId+"&loadtab=quotedocs-tab";
	}
	
}
