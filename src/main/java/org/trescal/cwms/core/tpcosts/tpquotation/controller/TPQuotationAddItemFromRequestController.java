package org.trescal.cwms.core.tpcosts.tpquotation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.tpcosts.tpquotation.dto.TPQuotationItemDTO;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.db.TPQuotationItemService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.TPQuoteLink;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.TPQuoteLinkComparator;
import org.trescal.cwms.core.tpcosts.tpquotation.form.CreateTPQuotationFromTPQuoteRequestForm;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_USERNAME)
public class TPQuotationAddItemFromRequestController {
	private static final Logger logger = LoggerFactory.getLogger(TPQuotationAddItemFromRequestController.class);

	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private MessageSource messageSource;
	@Autowired
	private TPQuoteRequestService tpqrServ;
	@Autowired
	private TPQuotationService tpQuotationService;
	@Autowired
	private TPQuotationItemService tpQuoteItemServ;
	@Autowired
	private UserService userService;
	
	@ModelAttribute("tpQuoteRequest")
	public TPQuoteRequest getTPQuoteRequest(
			@RequestParam(name="tpqrid", required=false, defaultValue="0") Integer tpQuoteRequestId) throws Exception {
		TPQuoteRequest tpQuoteRequest = this.tpqrServ.get(tpQuoteRequestId);
		if (tpQuoteRequest == null) {
			logger.error("Third party quote request was not found");
			throw new Exception("Third party quote request was not found");
		}
		else return tpQuoteRequest;
	}
	
	@ModelAttribute("tpQuote")
	public TPQuotation getTPQuote(
			@RequestParam(name="tpqid", required=false, defaultValue="0") Integer tpQuotationId) throws Exception {
		TPQuotation tpQuotation = this.tpQuotationService.get(tpQuotationId);
		if (tpQuotation == null) {
			logger.error("Third party quote was not found");
			throw new Exception("Third party quote was not found");
		}
		else return tpQuotation;
	}
	
	@ModelAttribute("tpquoteform")
	protected CreateTPQuotationFromTPQuoteRequestForm formBackingObject(
			@ModelAttribute("tpQuoteRequest") TPQuoteRequest tpQuoteRequest) {
		return tpQuotationService.initialiseTPQuotationForm(tpQuoteRequest);
	}
	
	@ModelAttribute("bgColors")
	public Map<Integer, String> getBackgroundColors(
			@ModelAttribute("tpQuoteRequest") TPQuoteRequest tpQuoteRequest) {
		return tpQuoteRequest.getItems().stream().collect(Collectors.toMap(
			TPQuoteRequestItem::getId, this::getDisplayColor));
	}
	
	private String getDisplayColor(TPQuoteRequestItem item) {
		String result = "#DBDBB7";
		if (item.getCaltype() != null) {
			result = item.getCaltype().getServiceType().getDisplayColour();
		}
		return result;
	}
	
	@ModelAttribute("calTypes")
	public List<CalibrationType> getCalTypes() {
		return calTypeServ.getCalTypes();
	}
	
	@ModelAttribute("MAXQUANTITY")
	public Integer getMaxQuantity() {
		return 50;
	}
	
	@RequestMapping(value="/addtpquoteitemsfromrequest.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("tpQuote") TPQuotation tpQuotation,
			@ModelAttribute("tpQuoteRequest") TPQuoteRequest tpQuoteRequest,
			@Validated @ModelAttribute("tpquoteform") CreateTPQuotationFromTPQuoteRequestForm tpQuoteForm, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) return referenceData();
		else {
			Contact currentContact = userService.get(username).getCon();
			int itemNo = this.tpQuoteItemServ.getMaxItemno(tpQuotation.getId()) + 1;
			// iterate over the selected TPQuoteRequestItems
			for(TPQuotationItemDTO item: tpQuoteForm.getItems().stream()
					.filter(TPQuotationItemDTO::isSetOnQuotation).collect(Collectors.toList())) {
				// create the TPQuotationItem and initialize it's costs
				TPQuotationItem tpQuoteItem = tpQuoteItemServ.createFromDTO(item);
				tpQuoteItem.setTpquotation(tpQuotation);
				tpQuoteItem.setItemno(itemNo++);
				tpQuoteRequest.getItems().stream()
					.filter(qri -> qri.getId() == item.getRequestItemId() && qri.getRequestFromJobItem() != null)
					.forEach(qri -> {
						TPQuoteLink link = new TPQuoteLink();
						link.setComment(messageSource.getMessage("message.tpquotation.autolinked", null, "Autolinked on receipt of quotation", null));
						link.setContact(currentContact);
						link.setDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
						link.setJobItem(qri.getRequestFromJobItem());
						link.setTpQuoteItem(tpQuoteItem);
						tpQuoteItem.setLinkedTo(new TreeSet<>(new TPQuoteLinkComparator()));
						tpQuoteItem.getLinkedTo().add(link);
					});
				tpQuotation.getItems().add(tpQuoteItem);
			}
			CostCalculator.updatePricingFinalCost(tpQuotation, tpQuotation.getItems());
			this.tpQuotationService.merge(tpQuotation);
			return new ModelAndView(new RedirectView("viewtpquote.htm?id=" + tpQuotation.getId(), true));
		}
	}
	
	@RequestMapping(value="/addtpquoteitemsfromrequest.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData() throws Exception {
		return new ModelAndView("trescal/core/thirdparty/additemfromrequest");
	}
}