package org.trescal.cwms.core.tpcosts.tpquoterequest.dto;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;

import lombok.Getter;

/**
 * Formatted result DTO intended for use with creation of TPQuoteRequest from JIThirdParty screen
 * Change as necessary if/when we replace DWR call with controller
 * Only the fields which are displayed in the row are included 
 */
@Getter
public class TPQuoteRequestRowDTO {
	private Integer id;
	private String requestNo;
	private String companyName;
	private String contactName;
	private String formattedReqDate;
	private String formattedDueDate;
	private String status;
	
	public TPQuoteRequestRowDTO(TPQuoteRequest tpqr, Locale locale) {
		DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(locale);
		
		this.id = tpqr.getId();
		this.requestNo = tpqr.getRequestNo();
		this.companyName = tpqr.getContact().getSub().getComp().getConame();
		this.contactName = tpqr.getContact().getName();
		this.formattedReqDate = tpqr.getReqdate() != null ? tpqr.getReqdate().format(formatter) : "";
		this.formattedDueDate = tpqr.getDueDate() != null ? tpqr.getDueDate().format(formatter) : "";
		// TODO : Existing status not translated, revisit when fully replacing with DWR 
		this.status = tpqr.getStatus().getName();
	}
}
