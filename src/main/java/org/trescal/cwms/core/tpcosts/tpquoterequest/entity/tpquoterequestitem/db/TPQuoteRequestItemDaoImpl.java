package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.db;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;

@Repository("TPQuoteRequestItemDao")
public class TPQuoteRequestItemDaoImpl extends BaseDaoImpl<TPQuoteRequestItem, Integer> implements TPQuoteRequestItemDao
{
	@Override
	protected Class<TPQuoteRequestItem> getEntity() {
		return TPQuoteRequestItem.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<TPQuoteRequestItem> getAllTPQRItems(List<Integer> ids) {
		if ((ids != null) && (ids.size() > 0)) {
			Criteria crit = getSession().createCriteria(TPQuoteRequestItem.class);
			crit.add(Restrictions.in("id", ids));
			return (List<TPQuoteRequestItem>) crit.list();
		}
		else return new ArrayList<TPQuoteRequestItem>();
	}
	
	public void saveOrUpdateAll(List<TPQuoteRequestItem> tpItems)
	{
		for (TPQuoteRequestItem tpQuoteRequestItem : tpItems)
			saveOrUpdate(tpQuoteRequestItem);
	}
}