package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote.TPQuoteRequestNote;

@Repository("TPQuoteRequestNoteDao")
public class TPQuoteRequestNoteDaoImpl extends BaseDaoImpl<TPQuoteRequestNote, Integer> implements TPQuoteRequestNoteDao
{
	@Override
	protected Class<TPQuoteRequestNote> getEntity() {
		return TPQuoteRequestNote.class;
	}
}