package org.trescal.cwms.core.tpcosts.tpquoterequest.form;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class TPQuoteRequestAddModelFormValidator extends AbstractBeanValidator {

    @Override
    public boolean supports(Class<?> clazz) {
        return TPQuoteRequestAddModelForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        super.validate(target, errors);
        TPQuoteRequestAddModelForm form = (TPQuoteRequestAddModelForm) target;
        // Form may be used for searches, in which case no selected ids yet
        if (form.getDefaultCostTypes() != null && form.getDefaultCostTypes().contains(CostType.CALIBRATION) && form.getSelectedIds() != null &&
                form.getSelectedIds().stream().map(id -> form.getCalTypeIds().get(id)).anyMatch(ct -> ct == null || ct == 0))
            errors.reject("error.tpquoterequest.servicetype",
                    "A service type must be specified for each selected model when requesting calibration");
    }
}