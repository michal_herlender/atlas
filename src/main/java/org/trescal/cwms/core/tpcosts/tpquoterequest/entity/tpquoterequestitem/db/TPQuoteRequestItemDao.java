package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;

public interface TPQuoteRequestItemDao extends BaseDao<TPQuoteRequestItem, Integer>
{
	List<TPQuoteRequestItem> getAllTPQRItems(List<Integer> ids);
	
	void saveOrUpdateAll(List<TPQuoteRequestItem> tpItems);
}