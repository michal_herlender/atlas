package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db;

import lombok.val;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description;
import org.trescal.cwms.core.instrumentmodel.entity.description.Description_;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel_;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr;
import org.trescal.cwms.core.instrumentmodel.entity.mfr.Mfr_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation_;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem_;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.TPQuoteStatus;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.TPQuoteStatus_;
import org.trescal.cwms.core.tpcosts.tpquotation.form.TPQuoteSearchForm;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest_;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

import static org.trescal.cwms.core.tools.StringJpaUtils.ilike;

@Repository("TPQuotationDao")
public class TPQuotationDaoImpl extends AllocatedToCompanyDaoImpl<TPQuotation, Integer> implements TPQuotationDao {
	
	@Override
	protected Class<TPQuotation> getEntity() {
		return TPQuotation.class;
	}

	@SuppressWarnings("unchecked")
	public TPQuotation findEagerTPQuotation(int id) {
		Criteria crit = this.getSession().createCriteria(TPQuotation.class);
		crit.setFetchMode("contact", FetchMode.JOIN);
		crit.setFetchMode("contact.sub", FetchMode.JOIN);
		crit.setFetchMode("contact.sub.comp", FetchMode.JOIN);
		crit.setFetchMode("status", FetchMode.JOIN);
		crit.setFetchMode("createdBy", FetchMode.JOIN);
		crit.setFetchMode("currency", FetchMode.JOIN);
		crit.setFetchMode("items", FetchMode.JOIN);
		crit.setFetchMode("items.caltype", FetchMode.JOIN);
		crit.setFetchMode("items.model", FetchMode.JOIN);
		crit.setFetchMode("items.model.mfr", FetchMode.JOIN);
		crit.setFetchMode("items.model.description", FetchMode.JOIN);
		crit.setFetchMode("items.model.modelMfrType", FetchMode.JOIN);
		crit.setFetchMode("items.calibrationCost", FetchMode.JOIN);
		crit.setFetchMode("items.salesCost", FetchMode.JOIN);
		crit.setFetchMode("items.repairCost", FetchMode.JOIN);
		crit.setFetchMode("items.adjustmentCost", FetchMode.JOIN);
		crit.setFetchMode("items.calibrationCost.costType", FetchMode.JOIN);
		crit.setFetchMode("items.salesCost.costType", FetchMode.JOIN);
		crit.setFetchMode("items.repairCost.costType", FetchMode.JOIN);
		crit.setFetchMode("items.adjustmentCost.costType", FetchMode.JOIN);
		crit.add(Restrictions.idEq(id));
		List<TPQuotation> list = crit.list();
		return list.size() > 0 ? list.get(0) : null;
	}

	/**
	 * Loads a TPQuotation item and all of it's associated costs eagerly. We
	 * explicity eagerly load the costs and costtypes as this function is being
	 * called through dwr which is no good at lazily loading and then
	 * manipulating entities.
	 * 
	 * @param id
	 *            the id of the TPQuotation
	 * @return TPQuotation the third party quotation requested
	 */
	@SuppressWarnings("unchecked")
	public TPQuotation findTPQuotationWithItems(int id) {
		DetachedCriteria crit = DetachedCriteria.forClass(TPQuotation.class);
		crit.setFetchMode("items", FetchMode.JOIN);
		crit.setFetchMode("items.calibrationCost", FetchMode.JOIN);
		crit.setFetchMode("items.salesCost", FetchMode.JOIN);
		crit.setFetchMode("items.repairCost", FetchMode.JOIN);
		crit.setFetchMode("items.adjustmentCost", FetchMode.JOIN);
		crit.setFetchMode("items.calibrationCost.costType", FetchMode.JOIN);
		crit.setFetchMode("items.salesCost.costType", FetchMode.JOIN);
		crit.setFetchMode("items.repairCost.costType", FetchMode.JOIN);
		crit.setFetchMode("items.adjustmentCost.costType", FetchMode.JOIN);
		List<TPQuotation> list = (List<TPQuotation>) crit.getExecutableCriteria(getSession()).list();
		return list.size() > 0 ? list.get(0) : null;
	}

	public PagedResultSet<TPQuotation> getTPQuotations(PagedResultSet<TPQuotation> rs, TPQuoteSearchForm tpsf) {

		List<TPQuotation> tpQuotationList = getResultList(cb -> {
			CriteriaQuery<TPQuotation> cq = cb.createQuery(TPQuotation.class);
			Root<TPQuotation> root = cq.from(TPQuotation.class);

			Join<TPQuotation, Company> compJoin = root.join("organisation");
			Join<Company, Subdiv> allocatedSubdivJoin = compJoin.join(Company_.subdivisions);
			Predicate clauses = cb.conjunction();
			if (tpsf.getAllocatedSubdivId() != null) {
				clauses.getExpressions()
						.add(cb.equal(allocatedSubdivJoin.get(Subdiv_.subdivid), tpsf.getAllocatedSubdivId()));
			}
			Join<TPQuotation, Contact> contactJoin = root.join(TPQuotation_.contact);
			Join<Contact, Subdiv> subdivJoin = contactJoin.join(Contact_.sub);
			Join<Subdiv, Company> companyJoin = subdivJoin.join(Subdiv_.comp);
			if (tpsf.getCoid() != null) {
				clauses.getExpressions().add(cb.equal(companyJoin.get(Company_.coid), tpsf.getCoid()));
			} else if ((tpsf.getConame() != null) && !tpsf.getConame().trim().equals("")) {
				clauses.getExpressions()
						.add(ilike(cb, companyJoin.get(Company_.coname), tpsf.getConame(), MatchMode.START));
			}

			if (tpsf.getPersonid() != null) {
				clauses.getExpressions().add(cb.equal(contactJoin.get(Contact_.personid), tpsf.getPersonid()));
			} else if ((tpsf.getContact() != null) && !tpsf.getContact().trim().equals("")) {
				clauses.getExpressions()
						.add(ilike(cb, contactJoin.get(Contact_.firstName), tpsf.getContact(), MatchMode.START));
			}

			if ((tpsf.getQno() != null) && !tpsf.getQno().trim().equals("")) {
				clauses.getExpressions().add(ilike(cb, root.get(TPQuotation_.qno), tpsf.getQno(), MatchMode.ANYWHERE));
			}

			if ((tpsf.getTpqno() != null) && !tpsf.getTpqno().trim().equals("")) {
				clauses.getExpressions()
						.add(ilike(cb, root.get(TPQuotation_.tpqno), tpsf.getTpqno(), MatchMode.ANYWHERE));
			}

			if (tpsf.getStatusid() != null) {
				Join<TPQuotation, TPQuoteStatus> statusJoin = root.join(TPQuotation_.status);
				clauses.getExpressions().add(cb.equal(statusJoin.get(TPQuoteStatus_.statusid), tpsf.getStatusid()));
			}

			Join<TPQuotation, TPQuotationItem> quoteItemJoin = null;
			Join<TPQuotationItem, InstrumentModel> modelJoin = null;
			if (((tpsf.getMfr() != null) && !tpsf.getMfr().trim().equals(""))
					|| ((tpsf.getMfrid() != null) && (tpsf.getMfrid() != 0))
					|| ((tpsf.getModel() != null) && !tpsf.getModel().trim().equals(""))
					|| ((tpsf.getDescid() != null) && (tpsf.getDescid() != 0))
					|| ((tpsf.getDescription() != null) && !tpsf.getDescription().trim().equals(""))
					|| ((tpsf.getModelid() != null) && (tpsf.getModelid() != 0))) {

				quoteItemJoin = root.join(TPQuotation_.items);
				modelJoin = quoteItemJoin.join(TPQuotationItem_.model);
			}

			// search mfr
			if ((tpsf.getMfr() != null) && !tpsf.getMfr().trim().equals("")) {
				Join<InstrumentModel, Mfr> mfrJoin = modelJoin.join(InstrumentModel_.mfr);
				clauses.getExpressions().add(ilike(cb, mfrJoin.get(Mfr_.name), tpsf.getMfr(), MatchMode.START));
			} else if (tpsf.getMfrid() != null) {
				Join<InstrumentModel, Mfr> mfrJoin = modelJoin.join(InstrumentModel_.mfr);
				clauses.getExpressions().add(cb.equal(mfrJoin.get(Mfr_.mfrid), tpsf.getMfrid()));
			}

			// search description
			if ((tpsf.getDescription() != null) && !tpsf.getDescription().trim().equals("")) {
				Join<InstrumentModel, Description> descriptionJoin = modelJoin.join(InstrumentModel_.description);
				clauses.getExpressions().add(ilike(cb, descriptionJoin.get(Description_.description),
						tpsf.getDescription(), MatchMode.START));
			} else if (tpsf.getDescid() != null) {
				Join<InstrumentModel, Description> descriptionJoin = modelJoin.join(InstrumentModel_.description);
				clauses.getExpressions().add(cb.equal(descriptionJoin.get(Description_.id), tpsf.getDescid()));
			}

			if ((tpsf.getModel() != null) && !tpsf.getModel().trim().equals("")) {
				clauses.getExpressions()
						.add(ilike(cb, modelJoin.get(InstrumentModel_.model), tpsf.getModel(), MatchMode.START));
			}
			if ((tpsf.getModelid() != null) && !tpsf.getModelid().equals(0)) {
				clauses.getExpressions().add(cb.equal(modelJoin.get(InstrumentModel_.modelid), tpsf.getModelid()));
			}

			cq.where(clauses);
			cq.distinct(true);
			cq.orderBy(cb.desc(root.get(TPQuotation_.regdate)), cb.desc(root.get(TPQuotation_.qno)));
			return cq;
		}	);

		rs.setResultsCount(tpQuotationList.size());
		// update the query so it will return the correct number of rows
		// from the correct start point
		if ((rs.getResultsPerPage() + rs.getStartResultsFrom()) >= tpQuotationList.size()) {
			tpQuotationList = tpQuotationList.subList(rs.getStartResultsFrom(), tpQuotationList.size());
		} else {
			tpQuotationList = tpQuotationList.subList(rs.getStartResultsFrom(),
					(rs.getStartResultsFrom() + rs.getResultsPerPage()));
		}
		// get results
		rs.setResults(tpQuotationList);

		return rs;
	}

	@Override
	public TPQuotation getTPQuoteByExactTPQNo(String tpqNo) {
		return getFirstResult(cb ->{
			CriteriaQuery<TPQuotation> cq = cb.createQuery(TPQuotation.class);
			Root<TPQuotation> tpq = cq.from(TPQuotation.class);
			cq.where(cb.equal(tpq.get(TPQuotation_.qno), tpqNo));
			return cq;
		}).orElse(null);
	}

	public List<TPQuotation> getTPQuotesFromTPRequest(int tpqrId) {
		return getResultList(cb -> {
			CriteriaQuery<TPQuotation> cq = cb.createQuery(TPQuotation.class);
			Root<TPQuotation> root = cq.from(TPQuotation.class);
			val tpQuoteRequest = root.join(TPQuotation_.fromRequest);
			cq.where(cb.equal(tpQuoteRequest.get(TPQuoteRequest_.id), tpqrId));
			cq.orderBy(cb.desc(root.get(TPQuotation_.regdate)));
			return cq;
		});
	}
}