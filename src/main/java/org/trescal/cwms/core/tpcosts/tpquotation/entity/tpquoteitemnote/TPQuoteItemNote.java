package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

@Entity
@Table(name = "tpquoteitemnote")
public class TPQuoteItemNote extends Note
{
	private TPQuotationItem tpquotationitem;
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.TPQUOTEITEMNOTE;
	}
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "id", unique = false, nullable = false, insertable = true, updatable = true)
	public TPQuotationItem getTpquotationitem() {
		return tpquotationitem;
	}
	
	public void setTpquotationitem(TPQuotationItem tpquotationitem) {
		this.tpquotationitem = tpquotationitem;
	}
}