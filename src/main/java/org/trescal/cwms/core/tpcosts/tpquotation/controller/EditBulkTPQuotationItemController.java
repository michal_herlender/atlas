package org.trescal.cwms.core.tpcosts.tpquotation.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.pricing.entity.costtype.db.CostTypeService;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.tools.GenericTools;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.db.TPQuotationItemService;
import org.trescal.cwms.core.tpcosts.tpquotation.form.EditBulkTPQuotationItemForm;
import org.trescal.cwms.core.tpcosts.tpquotation.form.EditBulkTPQuotationItemValidator;

/**
 * Controller which allows bulk editing of TPQuotationItems. Form allows
 * addition / removal or CostTypes, updating of CostTypes + prices and addition
 * / removal of Notes relating to CostTypes.
 * 
 * @author Richard
 */
@Controller @IntranetController
public class EditBulkTPQuotationItemController
{
	@Autowired
	private CalibrationTypeService caltypeServ;
	@Autowired
	private CostTypeService costTypeService;
	@Autowired
	private TPQuotationItemService tpQuoteItemServ;
	@Autowired
	private TPQuotationService tpQuoteServ;
	@Autowired
	private EditBulkTPQuotationItemValidator validator;

	public static final String FORM_NAME = "command";
	
	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
	@ModelAttribute(FORM_NAME)
	protected EditBulkTPQuotationItemForm formBackingObject(HttpServletRequest request) throws Exception
	{
		List<Integer> itemIds = GenericTools.stringToIntegerList(ServletRequestUtils.getStringParameter(request, "ids"));
		int tpQuoteId = ServletRequestUtils.getIntParameter(request, "id", 0);
		EditBulkTPQuotationItemForm form = new EditBulkTPQuotationItemForm();
		form.setTpQitems(this.tpQuoteItemServ.findTPQuotationItemsWithCosts(itemIds));
		form.setTpQuote(this.tpQuoteServ.get(tpQuoteId));
		return form;
	}

	@RequestMapping(value="/tpquotebulkitemedit.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(HttpSession session, @ModelAttribute(FORM_NAME) @Validated EditBulkTPQuotationItemForm form, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) {
			return referenceData(session);
		}
		
		TPQuotation tpQuote = this.tpQuoteItemServ.editTpQuoteBulkItem(form);
		return new ModelAndView(new RedirectView("/viewtpquote.htm?id="+tpQuote.getId(), true));
	}

	@RequestMapping(value="/tpquotebulkitemedit.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(HttpSession session) throws Exception
	{

		Map<String, Object> refData = new HashMap<String, Object>();

		// get all available CalTypes
		refData.put("caltypes", this.caltypeServ.getCalTypes());
		refData.put("costtypes", this.costTypeService.getAllActiveCostTypes());

		return new ModelAndView("trescal/core/tpcosts/tpquote/editbulktpquotationitem", refData);
	}
}
