package org.trescal.cwms.core.tpcosts.tpquoterequest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.pricing.entity.costtype.db.CostTypeService;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.TPQuoteRequestExternalForm;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.TPQuoteRequestForm;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.TPQuoteRequestFormValidator;
import org.trescal.cwms.spring.model.KeyValue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Set;

/**
 * Form Controller which allows users to create a TPQuoteRequest (third party
 * quotation request). If the form has been navigated to via a Quotation then
 * the form also displays options to create the TPQuoteRequest pre-populated
 * with item(s) from the Quotation. The Quotation will also be explicitly linked
 * to in the TPQuoteRequest. In addition, part of the form lists all available
 * CostTypes. On submission these CostTypes are saved as the TPQuoteRequest
 * default CostTypes.
 */
@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class TPQuoteRequestFormController
{
	@Value("${cwms.config.quotation.size.restricttpquoterequest}")
	private long sizeRestrictTPQuoteRequest;
	
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private CostTypeService costTypeServ;
	@Autowired
	private JobService jobServ;
	@Autowired
	private QuotationItemService quoteItemServ;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private TPQuoteRequestService tpQuoteRequestService;
	@Autowired
	private UserService userService;
	@Autowired
	private TPQuoteRequestFormValidator validator;
	
	@ModelAttribute("tpQuoteRequestForm")
	protected TPQuoteRequestExternalForm formBackingObject(
			@RequestParam(value="quoteid", required=false, defaultValue="0") Integer quoteId,
			@RequestParam(value="jobid", required=false, defaultValue="0") Integer jobId,
			@RequestParam(value="basedontpqrid", required=false, defaultValue="0") Integer basedOnTpQrId,
			@RequestParam(value="itemid", required=false, defaultValue="0") Integer itemId
			) throws Exception
	{
		TPQuoteRequestForm tpQuoteReqForm = new TPQuoteRequestForm();
		if (basedOnTpQrId != 0) {
			tpQuoteReqForm.setBasedOnTPQuoteRequestId(basedOnTpQrId);
		}
		else if (quoteId != 0) {
			long totalItemCount = quoteItemServ.getItemCount(quoteId);
			// 2016-02-22 - Added check of number of quotation items until performance issues fully resolved 
			if (totalItemCount >= sizeRestrictTPQuoteRequest) {
				throw new RuntimeException("This quotation has "+totalItemCount+" items and due to temporary performance restrictions, quotations with "+sizeRestrictTPQuoteRequest+" items or more cannot be used to create third party quotation requests.");
			}
			tpQuoteReqForm.setRequestFromQuoteId(quoteId);
			
		}
		else if (jobId != 0) {
			tpQuoteReqForm.setRequestFromJobId(jobId);			
		}
		tpQuoteReqForm.setItemid(itemId);
		return tpQuoteReqForm;
	}
	
	@InitBinder("tpQuoteRequestForm")
	protected void initBinder(WebDataBinder binder) throws Exception {
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}
	
	@RequestMapping(value="/tpquoterequestform.htm", method=RequestMethod.POST)
	protected String onSubmit(Model model,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute("tpQuoteRequestForm") @Validated TPQuoteRequestForm tpForm, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) return referenceData(model, tpForm, subdivDto);
		Contact contact = userService.get(username).getCon();
		TPQuoteRequest tpqr = this.tpQuoteRequestService.createTPQuoteRequestFromForm(tpForm, subdivDto.getKey(), contact);
		// Remove any existing request parameters bound in map
		model.asMap().clear();
		return "redirect:viewtpquoterequest.htm?id=" + tpqr.getId();
	}
	
	@RequestMapping(value="/tpquoterequestform.htm", method=RequestMethod.GET)
	protected String referenceData(Model model,
			@ModelAttribute("tpQuoteRequestForm") TPQuoteRequestForm tpForm,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception
	{
		// TODO this could probably be restrcted to just those for external / intercompany services?
		model.addAttribute("caltypes", this.calTypeServ.getActiveCalTypes());
		model.addAttribute("costTypes", this.costTypeServ.getAllActiveCostTypes());
		if (tpForm.getBasedOnTPQuoteRequestId() != null) {
			model.addAttribute("basedOnTPQuoteRequest", this.tpQuoteRequestService.get(tpForm.getBasedOnTPQuoteRequestId())); 
		}
		if (tpForm.getRequestFromJobId() != null) {
			Job job = this.jobServ.get(tpForm.getRequestFromJobId());
			model.addAttribute("requestFromJob", job); 
			
			if (job.getItems() != null) {
				// get a distinct list of models to display
				Set<InstrumentModel> mods = this.jobServ.getDistinctModels(job);
				// convert this into a HashMap
				HashMap<Integer, InstrumentModel> jobModels = new HashMap<>();
				int id = 0;
				for (InstrumentModel m : mods) {
					jobModels.put(id, m);
					id++;
				}
				model.addAttribute("models", jobModels);
			}
		}
		if (tpForm.getRequestFromQuoteId() != null) {
			Quotation quote = this.quoteServ.get(tpForm.getRequestFromQuoteId());
			model.addAttribute("requestFromQuote", quote);
			if (quote.getQuotationitems() != null) {
				// get a distinct list of models / instrumentmodels to display
				Set<InstrumentModel> mods = this.quoteServ.getDistinctModels(quote);
				// convert this into a HashMap
				HashMap<Integer, InstrumentModel> quoteModels = new HashMap<>();
				int id = 0;
				for (InstrumentModel m : mods) {
					quoteModels.put(id, m);
					id++;
				}
				model.addAttribute("models", quoteModels);
			}
		}
		
		return "trescal/core/tpcosts/tpquoterequest/tpquoterequestform";
	}
}