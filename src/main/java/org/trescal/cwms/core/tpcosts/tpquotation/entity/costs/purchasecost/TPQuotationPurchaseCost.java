/**
 * 
 */
package org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.purchasecost;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costs.base.PurchaseCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.purchasecost.JobCostingPurchaseCost;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.purchasecost.QuotationPurchaseCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost.TPQuotationCalibrationCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

@Entity
@DiscriminatorValue("tpquotation")
public class TPQuotationPurchaseCost extends PurchaseCost
{
	/**
	 * All Job Costing Purchase costs ( {@link JobCostingPurchaseCost}) derived
	 * from this {@link TPQuotationPurchaseCost}.
	 */
	private Set<JobCostingPurchaseCost> linkedJobCalCosts;

	/**
	 * All Quotation Purchase costs ({@link QuotationCalibrationCost}) derived
	 * from this {@link TPQuotationCalibrationCost}.
	 */
	private Set<QuotationPurchaseCost> linkedQuoteCalCosts;

	private TPQuotationItem tpQuoteItem;

	@Override
	@Transient
	public Cost getLinkedCostSrc()
	{
		return null;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "linkedCostSrc")
	public Set<JobCostingPurchaseCost> getLinkedJobCalCosts()
	{
		return this.linkedJobCalCosts;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "linkedCostSrc")
	public Set<QuotationPurchaseCost> getLinkedQuoteCalCosts()
	{
		return this.linkedQuoteCalCosts;
	}

	/**
	 * @return the tpQuoteItem
	 */
	@OneToOne(mappedBy = "purchaseCost")
	public TPQuotationItem getTpQuoteItem()
	{
		return this.tpQuoteItem;
	}

	/**
	 * @param linkedJobCalCosts the linkedJobCalCosts to set
	 */
	public void setLinkedJobCalCosts(Set<JobCostingPurchaseCost> linkedJobCalCosts)
	{
		this.linkedJobCalCosts = linkedJobCalCosts;
	}

	/**
	 * @param linkedQuoteCalCosts the linkedQuoteCalCosts to set
	 */
	public void setLinkedQuoteCalCosts(Set<QuotationPurchaseCost> linkedQuoteCalCosts)
	{
		this.linkedQuoteCalCosts = linkedQuoteCalCosts;
	}

	/**
	 * @param tpQuoteItem the tpQuoteItem to set
	 */
	public void setTpQuoteItem(TPQuotationItem tpQuoteItem)
	{
		this.tpQuoteItem = tpQuoteItem;
	}
}
