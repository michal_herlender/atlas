package org.trescal.cwms.core.tpcosts.tpquotation.form;

import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;

@Deprecated
public class ViewTPQuotationForm
{
	private TPQuotation tpQuote;
	
	public TPQuotation getTpQuote()
	{
		return tpQuote;
	}
	
	public void setTpQuote(TPQuotation tpQuote)
	{
		this.tpQuote = tpQuote;
	}
}
