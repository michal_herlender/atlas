package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.audit.Auditable;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "tpquotelink", uniqueConstraints = {@UniqueConstraint(columnNames = {"jobitemid", "quoteitemid"})})
public class TPQuoteLink extends Auditable {
	private String comment;
	private Contact contact;
	private LocalDate date;
	private int id;
	private JobItem jobItem;
	private TPQuotationItem tpQuoteItem;

	@Length(max = 200)
	@Column(name = "comment", length = 200)
	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personid", nullable = false)
	public Contact getContact() {
		return this.contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	@Column(name = "date", nullable = false, columnDefinition = "date")
	public LocalDate getDate() {
		return this.date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobitemid", nullable = false)
	public JobItem getJobItem() {
		return this.jobItem;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quoteitemid", nullable = false)
	public TPQuotationItem getTpQuoteItem() {
		return this.tpQuoteItem;
	}

	public void setJobItem(JobItem jobItem)
	{
		this.jobItem = jobItem;
	}

	public void setTpQuoteItem(TPQuotationItem tpQuoteItem)
	{
		this.tpQuoteItem = tpQuoteItem;
	}
}