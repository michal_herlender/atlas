package org.trescal.cwms.core.tpcosts.tpquotation.form;

import java.beans.PropertyEditorSupport;

import org.springframework.util.StringUtils;

/**
 * This is not used, can be deleted
 * @author richard
 *
 */
public class TPQuotationAddModelCostTypeCustomEditor extends PropertyEditorSupport
{
	@Override
	public void setAsText(String text) throws IllegalArgumentException
	{
		if(!StringUtils.hasText(text))
		{
			throw new IllegalArgumentException("text must not be empty or null");
		}
		if(text.indexOf('-') < 0)
		{
			throw new IllegalArgumentException("invalid CostType reference");
		}
	}
	
	@Override
	public String getAsText()
	{
		return super.getAsText();
	}
}
