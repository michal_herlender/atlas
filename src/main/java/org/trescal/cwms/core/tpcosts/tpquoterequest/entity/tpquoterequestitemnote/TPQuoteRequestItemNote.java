package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitemnote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;

@Entity
@Table(name = "tpquoterequestitemnote")
public class TPQuoteRequestItemNote extends Note
{
	private TPQuoteRequestItem tpQuoteRequestItem;
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.TPQUOTEREQUESTITEMNOTE;
	}
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "itemid", unique = false, nullable = false, insertable = true, updatable = true)
	public TPQuoteRequestItem getTpQuoteRequestItem() {
		return tpQuoteRequestItem;
	}
	
	public void setTpQuoteRequestItem(TPQuoteRequestItem tpQuoteRequestItem) {
		this.tpQuoteRequestItem = tpQuoteRequestItem;
	}
}