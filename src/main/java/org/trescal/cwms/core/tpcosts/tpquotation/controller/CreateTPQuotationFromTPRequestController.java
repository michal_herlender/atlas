package org.trescal.cwms.core.tpcosts.tpquotation.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.db.TPQuotationItemService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.TPQuoteStatus;
import org.trescal.cwms.core.tpcosts.tpquotation.form.CreateTPQuotationFromTPQuoteRequestForm;
import org.trescal.cwms.core.tpcosts.tpquotation.form.CreateTPQuotationFromTPQuoteRequestValidator;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Form allowing user to create a TPQuotation record from a TPQuoteRequest.
 * Offers user the ability to select one-to-many different TPQuoteRequestItems
 * (and their associated CostTypes) and and add them as TPQuoteItems
 */
@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_USERNAME})
public class CreateTPQuotationFromTPRequestController {
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private TPQuoteRequestService tpqrServ;
	@Autowired
	private TPQuotationService tpQuotationService;
	@Autowired
	private CreateTPQuotationFromTPQuoteRequestValidator validator;
	@Autowired
	private TPQuotationItemService tpQuotationItemService;
	
	public static final String FORM_NAME = "tpquoteform";

	private TPQuoteRequest getTPQuoteRequest(Integer tpQuoteRequestId) {
		TPQuoteRequest tpQuoteRequest = this.tpqrServ.get(tpQuoteRequestId);
		if (tpQuoteRequest == null)
			throw new RuntimeException("Third party quote request was not found");
		else
			return tpQuoteRequest;
	}

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) throws Exception {
		// register the document upload binder
		binder.registerCustomEditor(byte[].class, new ByteArrayMultipartFileEditor());
		binder.registerCustomEditor(Date.class, new MyCustomDateEditor(DateTools.df_ISO8601, true));
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}

	@ModelAttribute(FORM_NAME)
	protected CreateTPQuotationFromTPQuoteRequestForm formBackingObject(
			@RequestParam(name = "tpqrid", required = true) Integer tpQuoteRequestId) {
		TPQuoteRequest tpQuoteRequest = getTPQuoteRequest(tpQuoteRequestId);
		return tpQuotationService.initialiseTPQuotationForm(tpQuoteRequest);
	}

	private Map<Integer, String> getBackgroundColors(TPQuoteRequest tpQuoteRequest) {
		return tpQuoteRequest.getItems().stream()
				.collect(Collectors.toMap(TPQuoteRequestItem::getId, this::getDisplayColor));
	}

	private String getDisplayColor(TPQuoteRequestItem item) {
		String result = "#DBDBB7";
		if (item.getCaltype() != null) {
			result = item.getCaltype().getServiceType().getDisplayColour();
		}
		return result;
	}

	@RequestMapping(value = "/tpquotefromrequest.htm", method = RequestMethod.POST)
	protected String onSubmit(Model model,
			@RequestParam(name = "tpqrid", required = true) Integer tpQuoteRequestId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto,
			@ModelAttribute(FORM_NAME) @Validated CreateTPQuotationFromTPQuoteRequestForm tpQuoteForm,
			BindingResult bindingResult, RedirectAttributes redirectAttributes) throws Exception {
		if (bindingResult.hasErrors())
			return referenceData(model, tpQuoteRequestId);
		else {
			// create TP quotation
			TPQuotation tpQuote = this.tpQuotationService.createTPQuotation(username, subdivDto.getKey(), tpQuoteForm, tpQuoteRequestId);
			// if a file has been uploaded then copy it into the tpquotation directory
			this.tpQuotationItemService.copyFileIntoTPQuotationDirectory(tpQuote, tpQuoteForm.getFile());
			Integer tpQuoteId = tpQuote.getId();
			return "redirect:viewtpquote.htm?id="+tpQuoteId;
		}
	}

	@RequestMapping(value = "/tpquotefromrequest.htm", method = RequestMethod.GET)
	protected String referenceData(Model model,
			@RequestParam(name = "tpqrid", required = true) Integer tpQuoteRequestId) throws Exception {
		
		TPQuoteRequest tpQuoteRequest = getTPQuoteRequest(tpQuoteRequestId);
		model.addAttribute("statusList", this.statusServ.getAllStatuss(TPQuoteStatus.class));
		model.addAttribute("MAXQUANTITY", 50);
		model.addAttribute("tpQuoteRequest", tpQuoteRequest);
		model.addAttribute("bgColors", getBackgroundColors(tpQuoteRequest));
		model.addAttribute("clientConList", this.tpqrServ.getClientContacts(tpQuoteRequest));
		model.addAttribute("calTypes", calTypeServ.getCalTypes());
		model.addAttribute("defaultCurrency", tpQuoteRequest.getCurrency());
		
		return "trescal/core/thirdparty/tpquotefromrequest";
	}
}