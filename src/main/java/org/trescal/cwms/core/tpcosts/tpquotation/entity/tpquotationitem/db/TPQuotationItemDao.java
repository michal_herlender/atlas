package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

public interface TPQuotationItemDao extends BaseDao<TPQuotationItem, Integer>
{
	List<TPQuotationItem> getWithCosts(List<Integer> itemIds);
	
	TPQuotationItem getWithCosts(int id);
	
	List<TPQuotationItem> getMatchingItems(CostType ct, int modelid);
	
	Integer getMaxItemno(int tpQuoteId);
	
	PagedResultSet<TPQuotationItem> searchTPQuotationItem(PagedResultSet<TPQuotationItem> ps, Integer modelid, Integer years, boolean expired, Integer coid);
}