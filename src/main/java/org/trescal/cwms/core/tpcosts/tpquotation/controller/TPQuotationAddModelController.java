package org.trescal.cwms.core.tpcosts.tpquotation.controller;

import static java.util.Comparator.comparing;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.costtype.db.CostTypeService;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.tools.GenericTools;
import org.trescal.cwms.core.tools.InstModelTools;
import org.trescal.cwms.core.tools.supportedlocale.SupportedLocaleService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.adjcost.TPQuotationAdjustmentCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost.TPQuotationCalibrationCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.purchasecost.TPQuotationPurchaseCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.repcost.TPQuotationRepairCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault.TPQuotationCaltypeDefault;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault.db.TPQuotationCaltypeDefaultService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.db.TPQuotationItemService;
import org.trescal.cwms.core.tpcosts.tpquotation.form.TPQuotationAddModelForm;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Form allowing user to search the instrumentmodel database for items to add to
 * a TPQuotation.
 */
@Controller
@IntranetController
public class TPQuotationAddModelController {
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private CostTypeService costTypeServ;
	@Autowired
	private InstrumentModelService modelServ;
	@Autowired
	private TPQuotationCaltypeDefaultService tpQuoteDefCalServ;
	@Autowired
	private TPQuotationItemService tpQuoteItemServ;
	@Autowired
	private TPQuotationService tpQuoteServ;
	@Autowired
	private SupportedLocaleService supportedLocaleService;
	@Autowired
	private MessageSource messageSource;

	@ModelAttribute("modelTypeSelections")
	private List<KeyValue<Integer, String>> createModelTypeSelections() {
		List<KeyValue<Integer, String>> selections = new ArrayList<>();
		selections.add(new KeyValue<Integer, String>(0,
				messageSource.getMessage("instmod.all", null, LocaleContextHolder.getLocale())));
		selections.add(new KeyValue<Integer, String>(1,
				messageSource.getMessage("instmod.salescatonly", null, LocaleContextHolder.getLocale())));
		selections.add(new KeyValue<Integer, String>(2,
				messageSource.getMessage("instmod.excludesalescat", null, LocaleContextHolder.getLocale())));
		return selections;
	}

	@ModelAttribute("form")
	protected TPQuotationAddModelForm formBackingObject(HttpServletRequest request) throws Exception {

		TPQuotationAddModelForm form = new TPQuotationAddModelForm();
		int tpQuoteId = ServletRequestUtils.getIntParameter(request, "id", 0);

		TPQuotation tpQuote = this.tpQuoteServ.get(tpQuoteId);
		form.setTpQuote(tpQuote);

		List<CostType> costTypes = this.costTypeServ.getAllActiveCostTypes();
		HashMap<Integer, CostType> costTypeHash = new HashMap<Integer, CostType>(0);
		for (CostType ct : costTypes) {
			costTypeHash.put(ct.getTypeid(), ct);
		}
		form.setAvailableCostTypes(costTypeHash);

		form.setMaxQuantity(50);

		List<CalibrationType> calTypes = this.calTypeServ.getCalTypes();
		calTypes.sort((c1, c2) -> c1.getOrderBy().compareTo(c2.getOrderBy()));
		LinkedHashMap<Integer, CalibrationType> calTypeHash = new LinkedHashMap<Integer, CalibrationType>(0);

		for (CalibrationType ct : calTypes) {
			calTypeHash.put(ct.getCalTypeId(), ct);
		}
		form.setCaltypes(calTypeHash);

		form.setNextId(this.tpQuoteItemServ.getMaxItemno(tpQuote.getId()));

		form.setSearch(new InstrumentAndModelSearchForm<InstrumentModel>());

		return form;
	}

	@RequestMapping(value = "/tpquoteaddmodel.htm", method = RequestMethod.GET)
	protected String referenceData() {
		return "trescal/core/tpcosts/tpquote/tpquotationaddmodel";
	}

	@RequestMapping(value = "/tpquoteaddmodel.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(@ModelAttribute("form") TPQuotationAddModelForm tpForm, BindingResult bindingResult)
			throws Exception {
		if (bindingResult.hasErrors()) {
			return new ModelAndView(referenceData());
		}
		this.updateCalTypeDefaults(tpForm);

		if (!tpForm.getSubmitted().equals("")) {
			if (tpForm.getModelIds() != null) {
				if (tpForm.getModelIds().size() > 0) {
					int nextId = this.tpQuoteItemServ.getMaxItemno(tpForm.getTpQuote().getId()) + 1;
					ArrayList<Integer> newItemIds = new ArrayList<Integer>();

					for (int i = 0; i < tpForm.getModelIds().size(); i++) {
						TPQuotationItem item = new TPQuotationItem();
						item.setTpquotation(tpForm.getTpQuote());
						item.setModel(this.modelServ.findInstrumentModel(tpForm.getModelIds().get(i)));
						item.setQuantity(tpForm.getQtys().get(i));
						item.setCaltype(tpForm.getCalTypeIds().get(i) == 0 ? null
								: this.calTypeServ.find(tpForm.getCalTypeIds().get(i)));
						item.setItemno(nextId);
						nextId++;

						item.setGeneralDiscountRate(new BigDecimal(0.00));
						item.setGeneralDiscountValue(new BigDecimal(0.00));
						item.setInspection(new BigDecimal(0.00));
						item.setCarriageIn(new BigDecimal(0.00));
						item.setCarriageOut(new BigDecimal(0.00));

						for (Integer key : tpForm.getAvailableCostTypes().keySet()) {
							this.createDefaultCost(item, tpForm.getAvailableCostTypes().get(key), 0.00, 0.00, false);
						}
						CostCalculator.updateItemCostWithRunningTotal(item);

						this.tpQuoteItemServ.save(item);

						newItemIds.add(item.getId());
					}

					// we don't need to update the quotation costs here because
					// we've only added 0 value costs

					if (tpForm.getSubmitted().equals("add")) {
						return new ModelAndView(
								new RedirectView("/viewtpquote.htm?id=" + tpForm.getTpQuote().getId(), true));
					} else {
						String newItems = GenericTools.intListToCSV(newItemIds);
						if (!newItems.trim().equals("")) {
							return new ModelAndView(new RedirectView(
									"/tpquotebulkitemedit.htm?ids=" + newItems + "&id=" + tpForm.getTpQuote().getId(),
									true));
						}
					}
				}
			}
		} else {
			tpForm.getSearch().setMfrNm(tpForm.getMfrtext());
			// search for models to display

			// Set required instrument model types
			switch (tpForm.getModelTypeSelectorChoice()) {
			case 1:
				tpForm.getSearch().setExcludeStndardModelTypes(true);
				break;
			case 2:
				tpForm.getSearch().setExcludeSalesCategoryModelTypes(true);
				break;
			default:
				break;
			}

			tpForm.getSearch().setExcludeCapabilityModelTypes(true);
			tpForm.getSearch().setExcludeQuarantined(true);

			List<InstrumentModel> searchMods = this.modelServ.searchInstrumentModels(tpForm.getSearch());

			final Locale locale = LocaleContextHolder.getLocale();
			final Locale defaultLocale = supportedLocaleService.getPrimaryLocale();
			final Function<InstrumentModel, String> byFullName = model -> InstModelTools.modelNameViaTranslations(model,
					locale, defaultLocale);

			List<InstrumentModel> searchModelsSorted = searchMods.stream().sorted(comparing(byFullName))
					.collect(Collectors.toList());

			// convert the results into a HashMap

			Map<Integer, InstrumentModel> searchModels = new HashMap<Integer, InstrumentModel>();
			int id = 0;
			for (InstrumentModel m : searchModelsSorted) {
				searchModels.put(id, m);
				id++;
			}

			tpForm.setSearchModels(searchModels);
		}

		tpForm.setSubmitted("");
		return new ModelAndView(referenceData());
	}

	private void createDefaultCost(TPQuotationItem tpQuoteItem, CostType ct, double totalCost, double discountRate,
			boolean activeCost) {
		switch (ct) {
		case ADJUSTMENT:
			TPQuotationAdjustmentCost costAdjustment = new TPQuotationAdjustmentCost();
			CostCalculator.populateCostType(ct, totalCost, discountRate, activeCost, costAdjustment);
			tpQuoteItem.setAdjustmentCost(costAdjustment);
			break;
		case CALIBRATION:
			TPQuotationCalibrationCost costCalibration = new TPQuotationCalibrationCost();
			CostCalculator.populateCostType(ct, totalCost, discountRate, activeCost, costCalibration);
			tpQuoteItem.setCalibrationCost(costCalibration);
			break;
		case REPAIR:
			TPQuotationRepairCost costRepair = new TPQuotationRepairCost();
			CostCalculator.populateCostType(ct, totalCost, discountRate, activeCost, costRepair);
			tpQuoteItem.setRepairCost(costRepair);
			break;
		case PURCHASE:
			TPQuotationPurchaseCost costPurchase = new TPQuotationPurchaseCost();
			CostCalculator.populateCostType(ct, totalCost, discountRate, activeCost, costPurchase);
			tpQuoteItem.setPurchaseCost(costPurchase);
			break;
		default:
			// No action needed for other cost types
			break;
		}
	}

	private void updateCalTypeDefaults(TPQuotationAddModelForm form) {
		// convert the list of current TPQuotationDefaultCostTypes into a
		// HashMap
		List<TPQuotationCaltypeDefault> cts = this.tpQuoteDefCalServ
				.findTPQuotationCaltypeDefaults(form.getTpQuote().getId());
		HashMap<Integer, TPQuotationCaltypeDefault> currentCalTypes = new HashMap<Integer, TPQuotationCaltypeDefault>(
				0);
		for (TPQuotationCaltypeDefault ct : cts) {
			currentCalTypes.put(ct.getCaltype().getCalTypeId(), ct);
		}

		TPQuotation tpQuote = form.getTpQuote();

		// user has selected one or more TPQuotationDefaultCostTypes
		if (form.getDefcaltypeids() != null) {
			// loop through the current list of costtypes and add any new
			// CostTypes not already selected
			for (Integer id : form.getDefcaltypeids()) {
				if (!currentCalTypes.containsKey(id)) {
					TPQuotationCaltypeDefault ct = new TPQuotationCaltypeDefault();
					ct.setTpquotation(tpQuote);
					ct.setCaltype(form.getCaltypes().get(id));
					this.tpQuoteDefCalServ.insertTPQuotationCaltypeDefault(ct);
					currentCalTypes.put(id, ct);
				}
			}

			// now remove any TPQuotationDefaultCostTypes that were selected but
			// have now been removed
			ArrayList<Integer> idsToRemove = new ArrayList<Integer>();
			for (Integer key : currentCalTypes.keySet()) {
				if (!form.getDefcaltypeids().contains(key)) {
					idsToRemove.add(key);
				}
			}

			for (Integer id : idsToRemove) {
				TPQuotationCaltypeDefault ct = currentCalTypes.get(id);
				this.tpQuoteDefCalServ.deleteTPQuotationCaltypeDefault(ct);
				currentCalTypes.remove(id);
			}

			// convert the hashmap back to a list and save it to the quotation
			cts.clear();
			Set<TPQuotationCaltypeDefault> defs = new HashSet<TPQuotationCaltypeDefault>(0);
			for (Integer id : currentCalTypes.keySet()) {
				// cts.add(currentCalTypes.get(id));
				defs.add(currentCalTypes.get(id));
			}
			form.getTpQuote().setDefaultCalTypes(defs);
		} else {
			// no TPQuotationDefaultCostTypes were selected, make sure none
			// exist for this TPQuotation
			List<TPQuotationCaltypeDefault> list = this.tpQuoteDefCalServ
					.findTPQuotationCaltypeDefaults(tpQuote.getId());
			for (TPQuotationCaltypeDefault def : list) {
				this.tpQuoteDefCalServ.deleteTPQuotationCaltypeDefault(def);
			}
			tpQuote.setDefaultCalTypes(null);
		}
	}
}
