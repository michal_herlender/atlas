package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.TPQuoteLink;

public interface TPQuoteLinkService extends BaseService<TPQuoteLink, Integer>
{
	void deleteTPQuoteLinkById(int id);
	
	/**
	 * Inserts a {@link TPQuoteLink} for the given {@link JobItem} and
	 * {@link TPQuotationItem}.
	 * 
	 * @param jobitemid the id of the {@link JobItem}.
	 * @param tpquoteitemid the id of the {@link TPQuotationItem}.
	 * @return a {@link ResultWrapper} with an eagerly loaded instance of the
	 *         new {@link TPQuoteLink}.
	 */
	ResultWrapper insertAjaxTPQuoteLink(int jobitemid, int tpquoteitemid);
}