package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.SortNatural;
import org.hibernate.validator.constraints.Length;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.poorigin.OriginKind;
import org.trescal.cwms.core.pricing.entity.pricings.pricing.Pricing;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault.TPQuotationCaltypeDefault;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotenote.TPQuoteNote;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.TPQuoteStatus;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

@Entity
@Table(name = "tpquotation")
public class TPQuotation extends Pricing<Company> implements NoteAwareEntity, ComponentEntity, OriginKind {
	private Set<TPQuotationCaltypeDefault> defaultCalTypes;
	private int defaultQty;
	private File directory;
	private TPQuoteRequest fromRequest;
	private SortedSet<TPQuotationItem> items;
	private Set<TPQuoteNote> notes;
	private String qno;
	private List<Email> sentEmails;
	private TPQuoteStatus status;
	private String tpqno;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tpquotation")
	public Set<TPQuotationCaltypeDefault> getDefaultCalTypes() {
		return this.defaultCalTypes;
	}
	
	@Override
	@Transient
	public Contact getDefaultContact() {
		return this.contact;
	}
	
	@Column(name = "defqty", nullable = false)
	public int getDefaultQty() {
		return this.defaultQty;
	}

	@Override
	@Transient
	public File getDirectory() {
		return this.directory;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tpqrid")
	public TPQuoteRequest getFromRequest() {
		return this.fromRequest;
	}

	@Override
	@Transient
	public String getIdentifier() {
		return this.qno;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tpquotation")
	@SortNatural
	public SortedSet<TPQuotationItem> getItems() {
		return this.items;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tpquotation")
	@SortComparator(NoteComparator.class)
	public Set<TPQuoteNote> getNotes() {
		return this.notes;
	}
	
	@Override
	@Transient
	public ComponentEntity getParentEntity() {
		return null;
	}
	
	@Transient
	public Integer getPrivateActiveNoteCount()
	{
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount()
	{
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount() {
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount() {
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}

	@Length(max = 30)
	@Column(name = "qno", nullable = false, length = 30)
	public String getQno() {
		return this.qno;
	}

	@Override
	@Transient
	public List<Email> getSentEmails() {
		return this.sentEmails;
	}

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "status", nullable = false)
	public TPQuoteStatus getStatus() {
		return this.status;
	}

	@Length(max = 30)
	@Column(name = "tqno", nullable = false, length = 30)
	public String getTpqno() {
		return this.tpqno;
	}

	@Override
	@Transient
	public boolean isAccountsEmail() {
		return false;
	}
	
	public void setDefaultCalTypes(Set<TPQuotationCaltypeDefault> defaultCalTypes)
	{
		this.defaultCalTypes = defaultCalTypes;
	}

	public void setDefaultQty(int defaultQty)
	{
		this.defaultQty = defaultQty;
	}

	@Override
	public void setDirectory(File file)
	{
		this.directory = file;
	}

	public void setFromRequest(TPQuoteRequest fromRequest)
	{
		this.fromRequest = fromRequest;
	}

	public void setItems(SortedSet<TPQuotationItem> items)
	{
		this.items = items;
	}

	public void setNotes(Set<TPQuoteNote> quotenotes)
	{
		this.notes = quotenotes;
	}
	
	public void setQno(String qno)
	{
		this.qno = qno;
	}

	@Override
	public void setSentEmails(List<Email> emails)
	{
		this.sentEmails = emails;
	}

	public void setStatus(TPQuoteStatus status)
	{
		this.status = status;
	}

	public void setTpqno(String tpqno)
	{
		this.tpqno = tpqno;
	}
}