package org.trescal.cwms.core.tpcosts.tpquotation.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.TPQuoteStatus;
import org.trescal.cwms.core.tpcosts.tpquotation.form.CreateTPQuotationForm;
import org.trescal.cwms.core.tpcosts.tpquotation.form.ViewTPQuotationValidator;

@Controller @IntranetController
@SessionAttributes(Constants.HTTPSESS_NEW_FILE_LIST)
public class ViewTPQuotationController
{
	@Autowired
	private ContactService conServ;
	@Autowired
	private EmailService emailService;
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private SystemComponentService scService;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private TPQuotationService tpQServ;
	@Autowired
	private SupportedCurrencyService currencyServ;
	@Autowired
	private ViewTPQuotationValidator validator;
	
	public static final String FORM_NAME = "command";
	
	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
		return new ArrayList<>();
	}
	
	@ModelAttribute(FORM_NAME)
	protected CreateTPQuotationForm formBackingObject(
			@RequestParam(value="id", required=false, defaultValue="0") Integer id) throws Exception
	{
		TPQuotation tpQuote = this.tpQServ.get(id);
		tpQuote.setSentEmails(this.emailService.getAllComponentEmails(Component.THIRD_PARTY_QUOTATION, id));

		CreateTPQuotationForm form = new CreateTPQuotationForm();
		form.setCurrencyCode(tpQuote.getCurrency().getCurrencyCode());
		form.setPersonId(tpQuote.getContact().getPersonid());
		form.setStatusId(tpQuote.getStatus().getStatusid());
		
		form.setTpQuotation(tpQuote);
		return form;
	}

	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}

	@RequestMapping(value = "/viewtpquote.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
									@ModelAttribute(FORM_NAME) @Validated CreateTPQuotationForm form, BindingResult bindingResult, RedirectAttributes ra) throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData(newFiles, form, bindingResult);
		}
		TPQuotation tpQuote = form.getTpQuotation();
		// update the currency
		this.currencyServ.setCurrencyFromForm(tpQuote, form.getCurrencyCode(), tpQuote.getContact().getSub().getComp());
		tpQuote.setContact(conServ.get(form.getPersonId()));
		tpQuote.setStatus(statusServ.findStatus(form.getStatusId(), TPQuoteStatus.class));
		this.tpQServ.merge(tpQuote);
		ra.addAttribute("id", tpQuote.getId());
		return new ModelAndView(new RedirectView("viewtpquote.htm", true));
	}

	@RequestMapping(value="/viewtpquote.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@ModelAttribute(FORM_NAME) CreateTPQuotationForm form, BindingResult bindingResult) throws Exception {
		TPQuotation tpQuote = form.getTpQuotation();
		// get files in the root directory
		FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ.getFilesForComponentRoot(Component.THIRD_PARTY_QUOTATION, tpQuote.getId(), newFiles);
		Map<String, Object> refData = new HashMap<>();
		// get a list of all available contacts for the subdivision
		refData.put("clientConList", this.conServ.getAllActiveSubdivContacts(tpQuote.getContact().getSub().getSubdivid()));
		// get a list of available statuss for this Quotation type
		refData.put("statusList", this.statusServ.getAllStatuss(TPQuoteStatus.class));
		refData.put("privateOnlyNotes", NoteType.TPQUOTENOTE.isPrivateOnly());
		refData.put(Constants.REFDATA_SYSTEM_COMPONENT, this.scService.findComponent(Component.THIRD_PARTY_QUOTATION));
		refData.put(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
		refData.put(Constants.REFDATA_FILES_NAME, fileBrowserServ.getFilesName(fileBrowserWrapper));
		// add a list of currency options for this company
		refData.put("currencyopts", this.currencyServ.getCompanyCurrencyOptions(tpQuote.getContact().getSub().getComp().getCoid()));
		return new ModelAndView("trescal/core/thirdparty/viewtpquote", refData);
	}
}