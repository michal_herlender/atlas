package org.trescal.cwms.core.tpcosts.tpquotation.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.costtype.db.CostTypeService;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.db.TPQuotationItemService;
import org.trescal.cwms.core.tpcosts.tpquotation.form.EditTPQuotationItemForm;

/**
 * Controller which allows editing of a TPQuotationItem. Form allows addition /
 * removal or CostTypes, updating of CostTypes + prices and addition / removal
 * of Notes relating to CostTypes.
 * 
 * @author Richard
 */
@Controller
@IntranetController
public class EditTPQuotationItemController {
	@Autowired
	private CalibrationTypeService caltypeServ;
	@Autowired
	private CostTypeService costTypeServ;
	@Autowired
	private TPQuotationItemService tpQuoteItemServ;

	public static final String FORM_NAME = "command";

	@ModelAttribute(FORM_NAME)
	protected EditTPQuotationItemForm formBackingObject(@RequestParam(name = "id", required = true) int id)
			throws Exception {
		EditTPQuotationItemForm form = new EditTPQuotationItemForm();
		form.setTpQitem(this.tpQuoteItemServ.findTPQuotationItemWithCosts(id));
		return form;
	}

	@RequestMapping(value = "/edittpquoteitem.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(@ModelAttribute(FORM_NAME) @Validated EditTPQuotationItemForm form,
			BindingResult bindingResult) throws Exception {
		
		if (bindingResult.hasErrors()) {
			return referenceData(form);
		}
		
		TPQuotationItem item = this.tpQuoteItemServ.editTpquoteItem(form);
		// return new ModelAndView(new
		// RedirectView("/edittpquoteitem.htm?id="+item.getId(), true));
		return new ModelAndView(new RedirectView("/viewtpquote.htm?id=" + item.getTpquotation().getId(), true));
	}

	@RequestMapping(value = "/edittpquoteitem.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute(FORM_NAME) EditTPQuotationItemForm form) throws Exception {
		Map<String, Object> refData = new HashMap<String, Object>();
		// get a lookup list of all CostTypes *not* currently assigned to this
		// TPQuotationItem
		Set<CostType> existingCostTypes = this.getCurrentCostTypes(form.getTpQitem().getCosts());
		List<CostType> remainingCostTypes = this.costTypeServ.getRemainingCostTypes(existingCostTypes);
		remainingCostTypes.remove(CostType.INSPECTION);
		refData.put("remCostTypes", remainingCostTypes);
		// get all available CalTypes
		refData.put("caltypes", this.caltypeServ.getCalTypes());

		return new ModelAndView("trescal/core/tpcosts/tpquote/edittpquotationitem", refData);
	}

	/**
	 * Takes a Set<Cost> and extracts a List<CostType> representing the
	 * CostType.typeids that are currently assigned to this quotation item
	 * 
	 * @param costs
	 * @return
	 */
	private Set<CostType> getCurrentCostTypes(Set<Cost> costs) {
		Set<CostType> types = new HashSet<CostType>();
		for (Cost c : costs) {
			if (c.isActive()) {
				types.add(c.getCostType());
			}
		}
		return types;
	}
}
