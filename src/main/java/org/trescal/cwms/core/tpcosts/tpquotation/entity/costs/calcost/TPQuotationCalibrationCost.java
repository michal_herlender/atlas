/**
 * 
 */
package org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.trescal.cwms.core.pricing.entity.costs.base.CalCost;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.calcost.JobCostingCalibrationCost;
import org.trescal.cwms.core.quotation.entity.costs.calcost.QuotationCalibrationCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

@Entity
@DiscriminatorValue("tpquotation")
public class TPQuotationCalibrationCost extends CalCost
{
	/**
	 * All Job Costing Calibration costs ( {@link JobCostingCalibrationCost})
	 * derived from this {@link TPQuotationCalibrationCost}.
	 */
	private Set<JobCostingCalibrationCost> linkedJobCalCosts;

	/**
	 * All Quotation Calibration costs ({@link QuotationCalibrationCost})
	 * derived from this {@link TPQuotationCalibrationCost}.
	 */
	private Set<QuotationCalibrationCost> linkedQuoteCalCosts;

	private TPQuotationItem tpQuoteItem;

	@Transient
	@Override
	/**
	 * Method inherited from {@link Cost}, {@link TPQuotationCalibrationCost}
	 * can have no cost source other than itself, so always returns null.
	 */
	public Cost getLinkedCostSrc()
	{
		return null;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "linkedCostSrc")
	public Set<JobCostingCalibrationCost> getLinkedJobCalCosts()
	{
		return this.linkedJobCalCosts;
	}

	/**
	 * @return the linkedQuoteCalCosts
	 */
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "linkedCostSrc")
	public Set<QuotationCalibrationCost> getLinkedQuoteCalCosts()
	{
		return this.linkedQuoteCalCosts;
	}

	/**
	 * @return the tpQuoteItem
	 */
	@OneToOne(mappedBy = "calibrationCost")
	public TPQuotationItem getTpQuoteItem()
	{
		return this.tpQuoteItem;
	}

	public void setLinkedJobCalCosts(Set<JobCostingCalibrationCost> linkedJobCalCosts)
	{
		this.linkedJobCalCosts = linkedJobCalCosts;
	}

	/**
	 * @param linkedQuoteCalCosts the linkedQuoteCalCosts to set
	 */
	public void setLinkedQuoteCalCosts(Set<QuotationCalibrationCost> linkedQuoteCalCosts)
	{
		this.linkedQuoteCalCosts = linkedQuoteCalCosts;
	}

	/**
	 * @param tpQuoteItem the tpQuoteItem to set
	 */
	public void setTpQuoteItem(TPQuotationItem tpQuoteItem)
	{
		this.tpQuoteItem = tpQuoteItem;
	}

	@Override
	public String toString() {
		return "parent class type: " + getClass().getSuperclass().toString();
	}
}
