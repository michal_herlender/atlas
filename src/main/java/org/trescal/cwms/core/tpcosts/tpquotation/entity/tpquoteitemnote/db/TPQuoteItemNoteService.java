package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote.TPQuoteItemNote;

public interface TPQuoteItemNoteService extends BaseService<TPQuoteItemNote, Integer> {}