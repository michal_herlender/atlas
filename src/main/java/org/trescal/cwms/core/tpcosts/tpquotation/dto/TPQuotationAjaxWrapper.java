package org.trescal.cwms.core.tpcosts.tpquotation.dto;

import java.util.List;

import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.TPQuoteStatus;

import lombok.Getter;
import lombok.Setter;

/**
 * Wrapper used to bundle useful reference data to be displayed on an ajax
 * {@link TPQuotation} form.
 * 
 * @author Richard
 */
@Getter @Setter
public class TPQuotationAjaxWrapper
{
	private String defaultCurrencyCode;
	private List<SupportedCurrency> currencyList;
	private List<TPQuoteStatus> statusList;
}
