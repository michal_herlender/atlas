package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;

@Entity
@Table(name = "tpquoterequestnote")
public class TPQuoteRequestNote extends Note
{
	private TPQuoteRequest tpQuoteRequest;
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.TPQUOTEREQUESTNOTE;
	}
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "tpquotereqid", unique = false, nullable = false, insertable = true, updatable = true)
	public TPQuoteRequest getTpQuoteRequest() {
		return tpQuoteRequest;
	}
	
	public void setTpQuoteRequest(TPQuoteRequest tpQuoteRequest) {
		this.tpQuoteRequest = tpQuoteRequest;
	}
}