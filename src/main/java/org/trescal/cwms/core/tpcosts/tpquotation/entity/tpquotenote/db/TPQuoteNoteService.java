package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotenote.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotenote.TPQuoteNote;

public interface TPQuoteNoteService extends BaseService<TPQuoteNote, Integer> {}