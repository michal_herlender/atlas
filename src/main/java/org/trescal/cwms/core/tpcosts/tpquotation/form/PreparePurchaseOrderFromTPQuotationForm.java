package org.trescal.cwms.core.tpcosts.tpquotation.form;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PreparePurchaseOrderFromTPQuotationForm {

	private Map<Integer, Boolean> selection;
	private Map<Integer, Integer> nominals;
	private Map<Integer, Boolean> disabled;
	
	public PreparePurchaseOrderFromTPQuotationForm() {
		selection = new HashMap<Integer, Boolean>();
		nominals = new HashMap<Integer, Integer>();
		disabled = new HashMap<Integer, Boolean>();
	}

}