package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault.TPQuotationCaltypeDefault;

@Repository("TPQuotationCaltypeDefaultDao")
public class TPQuotationCaltypeDefaultDaoImpl extends BaseDaoImpl<TPQuotationCaltypeDefault, Integer> implements TPQuotationCaltypeDefaultDao {

	@Override
	protected Class<TPQuotationCaltypeDefault> getEntity() {
		return TPQuotationCaltypeDefault.class;
	}
}