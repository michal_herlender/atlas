package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem;

import java.math.BigDecimal;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.SortComparator;
import org.hibernate.annotations.SortNatural;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.pricing.entity.costs.CostOrderComparator;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.pricingitems.pricingitem.PricingItem;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.adjcost.TPQuotationAdjustmentCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost.TPQuotationCalibrationCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.purchasecost.TPQuotationPurchaseCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.repcost.TPQuotationRepairCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote.TPQuoteItemNote;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.TPQuoteLink;

import lombok.Setter;

@Entity
@Table(name = "tpquotationitem", uniqueConstraints = { @UniqueConstraint(columnNames = { "itemno", "tpquoteid" }) })
@Setter
public class TPQuotationItem extends PricingItem implements NoteAwareEntity, Comparable<TPQuotationItem> {

	private Integer id;
	protected TPQuotationAdjustmentCost adjustmentCost;
	/**
	 * Defines this module's parent base unit
	 */
	private TPQuotationItem baseUnit;
	protected TPQuotationCalibrationCost calibrationCost;
	private CalibrationType caltype;

	private BigDecimal carriageIn;
	private BigDecimal carriageOut;
	private BigDecimal inspection;

	private Set<TPQuoteLink> linkedTo;

	private InstrumentModel model;

	/**
	 * Defines a list of all modules belonging to this quotationitem.
	 */
	private Set<TPQuotationItem> modules;
	private Set<TPQuoteItemNote> notes;
	protected TPQuotationPurchaseCost purchaseCost;
	protected TPQuotationRepairCost repairCost;

	private TPQuotation tpquotation;

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	public Integer getId() {
		return id;
	}

	@Override
	@Transient
	public SortedSet<Cost> getCosts() {
		SortedSet<Cost> costs = new TreeSet<Cost>(new CostOrderComparator());
		if (adjustmentCost != null)
			costs.add(adjustmentCost);
		if (calibrationCost != null)
			costs.add(calibrationCost);
		if (purchaseCost != null)
			costs.add(purchaseCost);
		if (repairCost != null)
			costs.add(repairCost);
		return costs;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "adjcost_id")
	public TPQuotationAdjustmentCost getAdjustmentCost() {
		return this.adjustmentCost;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.EAGER)
	@JoinColumn(name = "baseid", unique = false, nullable = true, insertable = true, updatable = true)
	@Override
	public TPQuotationItem getBaseUnit() {
		return this.baseUnit;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "calcost_id")
	public TPQuotationCalibrationCost getCalibrationCost() {
		return this.calibrationCost;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "caltype_caltypeid")
	public CalibrationType getCaltype() {
		return this.caltype;
	}

	@NotNull
	@Column(name = "carriagein", nullable = false, precision = 10, scale = 2)
	public BigDecimal getCarriageIn() {
		return this.carriageIn;
	}

	@NotNull
	@Column(name = "carriageout", nullable = false, precision = 10, scale = 2)
	public BigDecimal getCarriageOut() {
		return this.carriageOut;
	}

	@NotNull
	@Column(name = "inspection", nullable = false, precision = 10, scale = 2)
	public BigDecimal getInspection() {
		return this.inspection;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tpQuoteItem")
	public Set<TPQuoteLink> getLinkedTo() {
		return this.linkedTo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modelid")
	public InstrumentModel getModel() {
		return this.model;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "baseUnit")
	@SortNatural
	@Override
	public Set<TPQuotationItem> getModules() {
		return this.modules;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tpquotationitem")
	@SortComparator(NoteComparator.class)
	public Set<TPQuoteItemNote> getNotes() {
		return this.notes;
	}

	@Transient
	public Integer getPrivateActiveNoteCount() {
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount() {
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount() {
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount() {
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "purchasecost_id")
	public TPQuotationPurchaseCost getPurchaseCost() {
		return this.purchaseCost;
	}

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "repcost_id")
	public TPQuotationRepairCost getRepairCost() {
		return this.repairCost;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "tpquoteid", unique = false, nullable = false, insertable = true, updatable = true)
	public TPQuotation getTpquotation() {
		return this.tpquotation;
	}

	@Override
	public int compareTo(TPQuotationItem other) {
		return getItemno().compareTo(other.getItemno());
	}
}