package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.db;

import java.util.List;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.TPQuoteLink;

public interface TPQuoteLinkDao extends BaseDao<TPQuoteLink, Integer>
{
	TPQuoteLink getEager(int id);
	
	List<TPQuoteLink> get(int jobitemid, int tpquoteitemid);
}