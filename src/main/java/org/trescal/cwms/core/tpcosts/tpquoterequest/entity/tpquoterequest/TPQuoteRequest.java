package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest;

import org.hibernate.annotations.SortComparator;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.job.entity.job.Job;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.costtype.CostTypeComparator;
import org.trescal.cwms.core.pricing.entity.pricings.genericpricingentity.GenericPricingEntity;
import org.trescal.cwms.core.quotation.entity.quotation.Quotation;
import org.trescal.cwms.core.system.entity.email.Email;
import org.trescal.cwms.core.system.entity.note.NoteAwareEntity;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.note.NoteCountCalculator;
import org.trescal.cwms.core.system.entity.systemcomponent.ComponentEntity;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItemComparator;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote.TPQuoteRequestNote;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequeststatus.TPQuoteRequestStatus;

import javax.persistence.*;
import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

@Entity
@Table(name = "tpquoterequest")
public class TPQuoteRequest extends GenericPricingEntity<Company> implements NoteAwareEntity, ComponentEntity {
    protected Contact contact;
    private SortedSet<CostType> defaultCostTypes;    // Was TPQuoteRequestDefaultCostType
    private File directory;
    private LocalDate dueDate;
    private Set<TPQuoteRequestItem> items;
    private Set<TPQuoteRequestNote> notes;
    private LocalDate receiptDate;
    private Job requestFromJob;
    // this should be one-to-many
    private Quotation requestFromQuote;
    private String requestNo;
    private List<Email> sentEmails;
    private TPQuoteRequestStatus status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "personid", nullable = false)
    public Contact getContact() {
        return this.contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @Transient
    public String getBaseFinalCostFormatted() {
        return null;
    }

    @Override
    @Transient
    public BigDecimal getBaseFinalCost() {
        return null;
    }

    @Column(name = "duedate", columnDefinition = "date")
    public LocalDate getDueDate() {
        return this.dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    @Override
    @Transient
    public Contact getDefaultContact() {
        return this.contact;
    }

    @Transient
    @Override
    public File getDirectory() {
        return this.directory;
    }

    @Override
    public void setDirectory(File directory) {
        this.directory = directory;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "tpQuoteRequest")
    @SortComparator(TPQuoteRequestItemComparator.class)
    public Set<TPQuoteRequestItem> getItems() {
        return this.items;
    }

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "tpQuoteRequest")
    @SortComparator(NoteComparator.class)
	public Set<TPQuoteRequestNote> getNotes()
	{
		return this.notes;
	}

	@Override
	@Transient
	public ComponentEntity getParentEntity()
	{
		return null;
	}

	@Transient
	public Integer getPrivateActiveNoteCount()
	{
		return NoteCountCalculator.getPrivateActiveNoteCount(this);
	}

	@Transient
	public Integer getPrivateDeactivatedNoteCount()
	{
		return NoteCountCalculator.getPrivateDeactivedNoteCount(this);
	}

	@Transient
	public Integer getPublicActiveNoteCount() {
		return NoteCountCalculator.getPublicActiveNoteCount(this);
	}

	@Transient
	public Integer getPublicDeactivatedNoteCount() {
		return NoteCountCalculator.getPublicDeactivedNoteCount(this);
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobid")
	public Job getRequestFromJob() {
		return this.requestFromJob;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "quoteid")
	public Quotation getRequestFromQuote() {
		return this.requestFromQuote;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "statusid", nullable = false)
	public TPQuoteRequestStatus getStatus() {
		return this.status;
	}

	@Column(name = "requestno", length = 30, nullable = false, unique = true)
	public String getRequestNo() {
		return this.requestNo;
	}

	@Override
	@Transient
    public List<Email> getSentEmails() {
        return this.sentEmails;
    }

    public static class TPQuoteRequestComparator implements Comparator<TPQuoteRequest> {
        public int compare(TPQuoteRequest tp1, TPQuoteRequest tp2) {
            return Integer.compare(tp2.getId(), tp1.getId());
        }
    }

    @ElementCollection
    @CollectionTable(name = "tpquoterequestdefaultcosttype", joinColumns = @JoinColumn(name = "tpquotereqid"))
    @Column(name = "costtypeid")
    @SortComparator(CostTypeComparator.class)
    public SortedSet<CostType> getDefaultCostTypes() {
        return this.defaultCostTypes;
    }

    public void setDefaultCostTypes(SortedSet<CostType> defaultCostTypes) {
        this.defaultCostTypes = defaultCostTypes;
    }

    @Override
    @Transient
    public String getIdentifier() {
        return this.requestNo;
    }

    @Override
    @Transient
    public boolean isAccountsEmail() {
        return false;
    }

    @Column(name = "receiptdate", columnDefinition = "date")
    public LocalDate getReceiptDate() {
        return this.receiptDate;
    }

    public void setReceiptDate(LocalDate receiptDate) {
        this.receiptDate = receiptDate;
    }

    public void setItems(Set<TPQuoteRequestItem> items) {
        this.items = items;
    }

    public void setNotes(Set<TPQuoteRequestNote> notes) {
        this.notes = notes;
    }

    public void setRequestFromJob(Job requestFromJob) {
        this.requestFromJob = requestFromJob;
    }

    public void setRequestFromQuote(Quotation requestFromQuote) {
        this.requestFromQuote = requestFromQuote;
    }

    public void setRequestNo(String requestNo) {
        this.requestNo = requestNo;
    }

    @Override
    public void setSentEmails(List<Email> emails) {
        this.sentEmails = emails;
    }

	public void setStatus(TPQuoteRequestStatus status)
	{
		this.status = status;
	}
}