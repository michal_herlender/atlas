package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.db.TPQuotationItemService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.TPQuoteLink;

import java.time.LocalDate;
import java.util.List;

@Service("TPQuoteLinkService")
public class TPQuoteLinkServiceImpl extends BaseServiceImpl<TPQuoteLink, Integer> implements TPQuoteLinkService {
    @Autowired
    private TPQuoteLinkDao tPQuoteLinkDao;
    @Autowired
    private JobItemService jobitemServ;
    @Autowired
    private TPQuotationItemService tpquoteItemServ;
    @Autowired
    private MessageSource messages;
    @Autowired
    private SessionUtilsService sessionServ;

    @Override
    protected BaseDao<TPQuoteLink, Integer> getBaseDao() {
        return tPQuoteLinkDao;
    }

    public void deleteTPQuoteLinkById(int id) {
        TPQuoteLink link = this.get(id);
        this.delete(link);
	}
	
	@Override
	public ResultWrapper insertAjaxTPQuoteLink(int jobitemid, int tpquoteitemid)
	{
		JobItem ji = this.jobitemServ.findJobItem(jobitemid);
		TPQuotationItem tpQuoteItem = this.tpquoteItemServ.get(tpquoteitemid);
		// validate jobitem
		if (ji == null)
			return new ResultWrapper(false, this.messages.getMessage("error.tpquotelink.nojobitem", null, "A valid jobitem must be selected", null));
		// validate tpquotationitem
		else if (tpQuoteItem == null)
			return new ResultWrapper(false, this.messages.getMessage("error.tpquotelink.noquoteitem", null, "A valid tp quotation item must be selected", null));
		else {
			// make sure the two are not already linked
			List<TPQuoteLink> dupList = tPQuoteLinkDao.get(jobitemid, tpquoteitemid);
			if (dupList.size() > 0)
				return new ResultWrapper(false, this.messages.getMessage("error.tpquotelink.duplicate", null, "This jobitem and tp quotation are already linked", null));
			else {
                // create and insert the link
                TPQuoteLink link = new TPQuoteLink();
                link.setJobItem(ji);
                link.setTpQuoteItem(tpQuoteItem);
                link.setContact(this.sessionServ.getCurrentContact());
                link.setDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
                this.save(link);
                // eagerly reload the link and return as a wrapper
                link = tPQuoteLinkDao.getEager(link.getId());
                return new ResultWrapper(true, "", link, null);
            }
		}
	}
}