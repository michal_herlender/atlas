package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tpcosts.tpquoterequest.dto.TPQuoteRequestDTO;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.TPQuoteRequestExternalForm;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.TPQuoteRequestForm;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.TPQuoteRequestSearchForm;

public interface TPQuoteRequestService extends BaseService<TPQuoteRequest, Integer> {
    void addItemsFromExternalSource(int tpqrid, TPQuoteRequestExternalForm tpForm);

    ResultWrapper createAjaxTPQuotationRequest(int jobitemid, int personid, String duedateText, int[] costtypeids, HttpSession session);

    TPQuoteRequest createTPQuoteRequestFromForm(TPQuoteRequestForm form, int subdivid, Contact contact);

    /**
     * Returns the {@link TPQuoteRequest} with the given tpqr No or null if
     * there are no third party quote requests with the given tpqr No.
     *
     * @param tpqrNo the tpqr No to match.
     * @return the {@link TPQuoteRequest}
     */
    TPQuoteRequest getTPQuoteReqByExactTPQRNo(String tpqrNo, int allocatedSubdivId);

    PagedResultSet<TPQuoteRequestDTO> searchTPQuoteRequest(TPQuoteRequestSearchForm form, PagedResultSet<TPQuoteRequestDTO> rs,
                                                           Company allocatedCompany);

    /**
     * @param req       TPQuoteRequest.
     * @param costTypes List of CostType enums requested to add as defaults for this TPQuoteRequest.
     * @return
     */
    void updateCostTypeDefaults(TPQuoteRequest req, List<CostType> costTypes);
	
	List<ContactKeyValue> getClientContacts(TPQuoteRequest tpQuoteRequest);
}