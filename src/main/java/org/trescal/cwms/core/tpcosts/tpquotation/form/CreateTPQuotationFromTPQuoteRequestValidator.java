package org.trescal.cwms.core.tpcosts.tpquotation.form;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import java.time.LocalDate;

@Component
public class CreateTPQuotationFromTPQuoteRequestValidator extends AbstractBeanValidator {

	public boolean supports(Class<?> clazz) {
		return clazz.equals(CreateTPQuotationFromTPQuoteRequestForm.class);
	}

	public void validate(Object target, Errors errors) {
		CreateTPQuotationFromTPQuoteRequestForm form = (CreateTPQuotationFromTPQuoteRequestForm) target;
		super.validate(form, errors);
		// perform some custom date validation
		if (form.getIssueDate() != null && form.getIssueDate().isAfter(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())))
			errors.rejectValue("issueDate", "error.tpquotation.issuesate", null, "Issue date cannot be in the future");
		if (form.getRegDate() != null && form.getRegDate().isAfter(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId())))
			errors.rejectValue("regDate", "error.tpquotation.regdate", null, "Receipt date cannot be in the future");
	}
}