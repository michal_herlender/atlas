package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.db;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;
import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tpcosts.tpquotation.dto.TPQuotationItemConvertedCurrency;
import org.trescal.cwms.core.tpcosts.tpquotation.dto.TPQuotationItemDTO;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.form.EditBulkTPQuotationItemForm;
import org.trescal.cwms.core.tpcosts.tpquotation.form.EditTPQuotationItemForm;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;

public interface TPQuotationItemService extends BaseService<TPQuotationItem, Integer> {
    ResultWrapper ajaxDeleteTPQuotationItems(String listOfIds);

    ResultWrapper checkQuotationItemIsSafeToDelete(TPQuotationItem tpqi);

    TPQuotationItem createFromDTO(TPQuotationItemDTO dto);

    List<TPQuotationItem> findTPQuotationItemsWithCosts(List<Integer> itemIds);

    TPQuotationItem findTPQuotationItemWithCosts(int id);
	
	/**
	 * Returns a list of {@link TPQuotationItem}s that have a costs of the given
	 * {@link CostType} id that is active for the given {@link InstModel}.
	 * 
	 * @param costTypeId the id of the {@link CostType}.
	 * @param modelid the id of the {@link InstModel}.
	 * @return list of {@link Cost}.
	 */
	List<TPQuotationItem> getMatchingItems(String costType, int modelid);
	
	Integer getMaxItemno(int tpQuoteId);
	
	/**
	 * QUICK AND DIRTY METHOD TO CONVERT CURRENCY OF A TP QUOTE ITEM WHEN
	 * LINKING TO A QUOTE ITEM. REQUIRES TESTING BEFORE USED MORE GENERALLY
	 * 
	 * @param tpQuoteItemId
	 * @param quoteItemId
	 * @param costType
	 * @return
	 */
	TPQuotationItemConvertedCurrency getTPQuoteItemInConvertedCurrency(int tpQuoteItemId, int quoteItemId, String costType);
	
	void saveOrUpdateAllTPQuotationItems(List<TPQuotationItem> tpquotationitems);

	/**
	 * Returns a list of {@link TPQuotationItem} matching the given criteria.
	 * 
	 * @param modelid the id of the model to return.
	 * @param years the number of years to search back over.
	 * @param expired whether or not to show expired quotes.
	 * @param coid {@link Company} restriction.
	 * @return {@link PagedResultSet}.
	 */
	PagedResultSet<TPQuotationItem> searchAjaxTPQuotationItem(Integer modelid, Integer years, boolean expired, Integer coid, int currentPage, int resultsPerPage);

	/**
	 * Returns a list of {@link TPQuotationItem} matching the given criteria.
	 * 
	 * @param ps
	 * @param modelid
	 * @param years
	 * @param expired
	 * @param coid
     * @return
     */
    PagedResultSet<TPQuotationItem> searchTPQuotationItem(PagedResultSet<TPQuotationItem> ps, Integer modelid, Integer years, boolean expired, Integer coid);

    /**
     * Aggregates the costs for the given TPQuotationItem.
     *
     * @param tpquotationitem the TPQuotationItem to aggregate the costs for
     */
    void updateTPQuotationItemCosts(TPQuotationItem tpquotationitem);

    void createTPQuotationItems(List<TPQuotationItemDTO> items, TPQuotation tpQuote,
                                                   TPQuoteRequest tpQuoteRequest, Contact currentContact);

    void copyFileIntoTPQuotationDirectory(TPQuotation tpQuote, MultipartFile file) throws Exception;
    
    TPQuotation editTpQuoteBulkItem(EditBulkTPQuotationItemForm form);
    
    TPQuotationItem editTpquoteItem(EditTPQuotationItemForm form);
}