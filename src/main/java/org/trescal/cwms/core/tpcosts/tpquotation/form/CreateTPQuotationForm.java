package org.trescal.cwms.core.tpcosts.tpquotation.form;

import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CreateTPQuotationForm
{
	private TPQuotation tpQuotation;
	private String currencyCode;
	private Integer personId;
	private Integer statusId;

}
