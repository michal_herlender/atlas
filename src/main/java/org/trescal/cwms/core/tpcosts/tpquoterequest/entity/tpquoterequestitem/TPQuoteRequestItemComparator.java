package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem;

import java.util.Comparator;

public class TPQuoteRequestItemComparator implements Comparator<TPQuoteRequestItem>
{
	public int compare(TPQuoteRequestItem t1, TPQuoteRequestItem t2)
	{
		if ((t1.getId() == 0) && (t2.getId() == 0))
		{
			return 1;
		}
		else
		{
			return Integer.compare(t1.getId(), t2.getId());
		}
	}
}
