package org.trescal.cwms.core.tpcosts.tpquoterequest.form;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;

public class TPQuoteRequestAddModelForm
{
	private List<Integer> calTypeIds;

	private List<CalibrationType> calTypes;
	private List<CostType> defaultCostTypes;

	private String description;
	private String instmodel;

	private String mfr;

	private List<Integer> modelids;

	private List<Integer> modelQtys;
	private InstrumentAndModelSearchForm<InstrumentModel> searchForm;
	// list of models returned as search results
	private Map<Integer, InstrumentModel> searchModels;
	private List<Integer> selectedIds;
	private String submitted;

	private TPQuoteRequest tpQuoteRequest;
	
	private String mfrtext;
	
	private Integer modelTypeSelectorChoice;

	public List<Integer> getCalTypeIds()
	{
		return this.calTypeIds;
	}

	public List<CalibrationType> getCalTypes()
	{
		return this.calTypes;
	}
	
	@NotNull
	public List<CostType> getDefaultCostTypes()
	{
		return this.defaultCostTypes;
	}

	public String getDescription()
	{
		return this.description;
	}

	public String getInstmodel()
	{
		return this.instmodel;
	}

	public String getMfr()
	{
		return this.mfr;
	}

	public List<Integer> getModelids()
	{
		return this.modelids;
	}

	public List<Integer> getModelQtys()
	{
		return this.modelQtys;
	}

	public InstrumentAndModelSearchForm<InstrumentModel> getSearchForm()
	{
		return this.searchForm;
	}

	public Map<Integer, InstrumentModel> getSearchModels()
	{
		return this.searchModels;
	}

	public List<Integer> getSelectedIds()
	{
		return this.selectedIds;
	}

	public String getSubmitted()
	{
		return this.submitted;
	}

	public TPQuoteRequest getTpQuoteRequest()
	{
		return this.tpQuoteRequest;
	}
	
	public void setCalTypeIds(List<Integer> calTypeIds)
	{
		this.calTypeIds = calTypeIds;
	}

	public void setCalTypes(List<CalibrationType> calTypes)
	{
		this.calTypes = calTypes;
	}

	public void setDefaultCostTypes(List<CostType> defaultCostTypes)
	{
		this.defaultCostTypes = defaultCostTypes;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public void setInstmodel(String instmodel)
	{
		this.instmodel = instmodel;
	}

	public void setMfr(String mfr)
	{
		this.mfr = mfr;
	}

	public void setModelids(List<Integer> modelids)
	{
		this.modelids = modelids;
	}

	public void setModelQtys(List<Integer> modelQtys)
	{
		this.modelQtys = modelQtys;
	}

	public void setSearchForm(InstrumentAndModelSearchForm<InstrumentModel> searchForm)
	{
		this.searchForm = searchForm;
	}

	public void setSearchModels(Map<Integer, InstrumentModel> searchModels)
	{
		this.searchModels = searchModels;
	}

	public void setSelectedIds(List<Integer> selectedIds)
	{
		this.selectedIds = selectedIds;
	}

	public void setSubmitted(String submitted)
	{
		this.submitted = submitted;
	}

	public void setTpQuoteRequest(TPQuoteRequest tpQuoteRequest)
	{
		this.tpQuoteRequest = tpQuoteRequest;
	}

	public String getMfrtext() {
		return mfrtext;
	}

	public void setMfrtext(String mfrtext) {
		this.mfrtext = mfrtext;
	}

	public Integer getModelTypeSelectorChoice() {
		return modelTypeSelectorChoice;
	}

	public void setModelTypeSelectorChoice(Integer modelTypeSelectorChoice) {
		this.modelTypeSelectorChoice = modelTypeSelectorChoice;
	}
}
