package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.SortComparator;

import org.hibernate.annotations.Type;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationTypeComparator;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;

@Entity
@Table(name = "tpquotationcaltypedefault", uniqueConstraints = { @UniqueConstraint(columnNames = { "tpquoteid", "caltypeid" }) })
public class TPQuotationCaltypeDefault
{
	private CalibrationType caltype;
	private int id;
	private TPQuotation tpquotation;

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "caltypeid", unique = false, nullable = false, insertable = true, updatable = true)
	@SortComparator(CalibrationTypeComparator.class)
	public CalibrationType getCaltype()
	{
		return this.caltype;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Type(type = "int")
	public int getId()
	{
		return this.id;
	}

	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "tpquoteid", unique = false, nullable = false, insertable = true, updatable = true)
	public TPQuotation getTpquotation()
	{
		return this.tpquotation;
	}

	public void setCaltype(CalibrationType caltype)
	{
		this.caltype = caltype;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setTpquotation(TPQuotation tpquotation)
	{
		this.tpquotation = tpquotation;
	}
}