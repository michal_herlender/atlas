package org.trescal.cwms.core.tpcosts.tpquoterequest.form;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;

import java.time.LocalDate;

@Data
public class ViewTPQuoteRequestForm {

    public ViewTPQuoteRequestForm(TPQuoteRequest tpqr) {
        this.contactId = tpqr.getContact().getPersonid();
        this.statusId = tpqr.getStatus().getStatusid();
        this.dueDate = tpqr.getDueDate();
    }

    private Integer contactId;
    private Integer statusId;
    @DateTimeFormat(iso = ISO.DATE)
    private LocalDate dueDate;
}