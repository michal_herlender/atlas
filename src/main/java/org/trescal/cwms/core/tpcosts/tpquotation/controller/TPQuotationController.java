package org.trescal.cwms.core.tpcosts.tpquotation.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.account.entity.Ledgers;
import org.trescal.cwms.core.account.entity.nominalcode.NominalCode;
import org.trescal.cwms.core.account.entity.nominalcode.db.NominalCodeService;
import org.trescal.cwms.core.company.dto.SubdivKeyValue;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.PurchaseOrder;
import org.trescal.cwms.core.pricing.purchaseorder.entity.purchaseorder.db.PurchaseOrderService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.form.PreparePurchaseOrderFromTPQuotationForm;

@Controller @IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_USERNAME, Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE,Constants.SESSION_ATTRIBUTE_SUBDIV})
public class TPQuotationController {
	
	@Autowired
	private NominalCodeService nominalCodeService;
	@Autowired
	private PurchaseOrderService purchaseOrderService;
	@Autowired
	private TPQuotationService tpQuotationService;
	@Autowired
	private UserService userService;
	@Autowired
	private SubdivService subService;
	
	private static final List<Ledgers> LEDGERS = Arrays.asList(Ledgers.PURCHASE_LEDGER, Ledgers.CAPITAL, Ledgers.UTILITY);
	
	@ModelAttribute("tpQuotationItems")
	public PreparePurchaseOrderFromTPQuotationForm model(
			@RequestParam(name="tpQuotationId", required=true) Integer tpQuotationId) {
		TPQuotation tpQuotation = tpQuotationService.get(tpQuotationId);
		PreparePurchaseOrderFromTPQuotationForm form = new PreparePurchaseOrderFromTPQuotationForm();
		for(TPQuotationItem tpqItem: tpQuotation.getItems()) {
			boolean disabled = tpqItem.getQuantity() != 1;
			form.getDisabled().put(tpqItem.getId(), disabled);
			// Only select by default if not disabled
			form.getSelection().put(tpqItem.getId(), !disabled);
			if (!disabled && !tpqItem.getCosts().isEmpty() && !tpqItem.getLinkedTo().isEmpty()) {
				Cost firstCost = tpqItem.getCosts().first();
				JobItem ji = tpqItem.getLinkedTo().iterator().next().getJobItem();
				NominalCode nominalCode = this.nominalCodeService.findBestMatchingNominalCode(LEDGERS, firstCost.getCostType().getTypeid(), ji);
				form.getNominals().put(tpqItem.getId(), nominalCode != null ? nominalCode.getId() : 0);
			}
			else {
				form.getNominals().put(tpqItem.getId(), 0);
			}
		}
		return form;
	}
	
	@RequestMapping(value="preparePurchaseOrderFromTPQuotation.htm", method=RequestMethod.POST)
	public String onSubmit(
			@RequestParam(name="tpQuotationId", required=true) Integer tpQuotationId,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_USERNAME) String username,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_DEFAULT_LOCALE) Locale defaultLocale,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) SubdivKeyValue subdivDto,
			@ModelAttribute("tpQuotationItems") PreparePurchaseOrderFromTPQuotationForm form) {
		TPQuotation tpQuotation = tpQuotationService.get(tpQuotationId);
		Contact currentContact = userService.get(username).getCon();
		Subdiv subdiv= subService.get(subdivDto.getKey());
		PurchaseOrder purchaseOrder = purchaseOrderService.createPurchaseOrderFromTPQuotation(tpQuotation, form.getSelection(), form.getNominals(), currentContact, LocaleContextHolder.getLocale(), defaultLocale,subdiv);
		return "redirect:viewpurchaseorder.htm?id=" + purchaseOrder.getId();
	}
	
	@RequestMapping(value="preparePurchaseOrderFromTPQuotation.htm", method=RequestMethod.GET)
	public String preparePurchaseOrderFromTPQuotation(Model model,
			@RequestParam(name="tpQuotationId", required=true) Integer tpQuotationId) {
		TPQuotation tpQuotation = tpQuotationService.get(tpQuotationId);
		
		model.addAttribute("tpQuotation", tpQuotation);
		model.addAttribute("nominals", nominalCodeService.getNominalCodes(LEDGERS));
		return "trescal/core/thirdparty/preparePurchaseOrderFromTPQuotation";
	}
}