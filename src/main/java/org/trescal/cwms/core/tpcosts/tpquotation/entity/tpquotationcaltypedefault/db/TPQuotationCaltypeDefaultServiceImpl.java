package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault.db;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault.TPQuotationCaltypeDefault;

@Service
public class TPQuotationCaltypeDefaultServiceImpl implements TPQuotationCaltypeDefaultService
{
	@Autowired
	private TPQuotationCaltypeDefaultDao TPQuotationCaltypeDefaultDao;
	@Autowired
	private TPQuotationService tpQuotationService;
	
	public TPQuotationCaltypeDefault findTPQuotationCaltypeDefault(int id)
	{
		return TPQuotationCaltypeDefaultDao.find(id);
	}

	public void insertTPQuotationCaltypeDefault(TPQuotationCaltypeDefault TPQuotationCaltypeDefault)
	{
		TPQuotationCaltypeDefaultDao.persist(TPQuotationCaltypeDefault);
	}

	public void updateTPQuotationCaltypeDefault(TPQuotationCaltypeDefault TPQuotationCaltypeDefault)
	{
		TPQuotationCaltypeDefaultDao.update(TPQuotationCaltypeDefault);
	}

	@Override
	public void deleteTPQuotationCaltypeDefault(TPQuotationCaltypeDefault tpquotationcaltypedefault)
	{
		TPQuotationCaltypeDefaultDao.remove(tpquotationcaltypedefault);
	}
	
	public List<TPQuotationCaltypeDefault> getAllTPQuotationCaltypeDefaults()
	{
		return TPQuotationCaltypeDefaultDao.findAll();
	}

	@Override
	public List<TPQuotationCaltypeDefault> findTPQuotationCaltypeDefaults(int tpQuoteId)
	{
		TPQuotation tpQuotation = tpQuotationService.get(tpQuoteId);
		return tpQuotation.getDefaultCalTypes().stream().collect(Collectors.toList());
	}
	
	public void setTPQuotationCaltypeDefaultDao(TPQuotationCaltypeDefaultDao TPQuotationCaltypeDefaultDao)
	{
		this.TPQuotationCaltypeDefaultDao = TPQuotationCaltypeDefaultDao;
	}
}