package org.trescal.cwms.core.tpcosts.tpquoterequest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.db.CompanyService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tpcosts.tpquoterequest.dto.TPQuoteRequestDTO;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequeststatus.TPQuoteRequestStatus;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.TPQuoteRequestSearchForm;
import org.trescal.cwms.spring.model.KeyValue;

import java.util.HashMap;
import java.util.Map;

@Controller
@IntranetController
@SessionAttributes({Constants.SESSION_ATTRIBUTE_COMPANY})
public class TPQuoteRequestSearchController {
	@Autowired
	private CompanyService companyService;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private TPQuoteRequestService tpqrServ;

	public static final String FORM_NAME = "tpQuoteRequestSearchForm";
	public static final String REQUEST_URL = "/tpquoterequestsearch.htm";
	public static final String VIEW_NAME = "trescal/core/tpcosts/tpquoterequest/tpquoterequestsearch";
	public static final String VIEW_RESULT_NAME = "trescal/core/tpcosts/tpquoterequest/tpquoterequestsearchresults";

	@ModelAttribute(FORM_NAME)
	protected TPQuoteRequestSearchForm formBackingObject() {
		return new TPQuoteRequestSearchForm();
	}

	@RequestMapping(value = REQUEST_URL, method = RequestMethod.POST)
	protected ModelAndView onSubmit(
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_COMPANY) KeyValue<Integer, String> companyDto,
			@ModelAttribute(FORM_NAME) TPQuoteRequestSearchForm form, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData();
		}
		Company allocatedCompany = this.companyService.get(companyDto.getKey());
		PagedResultSet<TPQuoteRequestDTO> resultSet = this.tpqrServ.searchTPQuoteRequest(form,
			new PagedResultSet<>(form.getResultsPerPage(), form.getPageNo()), allocatedCompany);
		form.setRs(resultSet);
		return new ModelAndView(VIEW_RESULT_NAME);
	}

	@RequestMapping(value = REQUEST_URL, method = RequestMethod.GET)
	protected ModelAndView referenceData() throws Exception {
		Map<String, Object> refData = new HashMap<String, Object>();
		refData.put("statusList", this.statusServ.getAllStatuss(TPQuoteRequestStatus.class));
		return new ModelAndView(VIEW_NAME, refData);
	}
}