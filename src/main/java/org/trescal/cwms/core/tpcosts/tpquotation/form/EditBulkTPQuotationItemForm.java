package org.trescal.cwms.core.tpcosts.tpquotation.form;

import java.util.List;

import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditBulkTPQuotationItemForm 
{
	private TPQuotation tpQuote;
	private List<TPQuotationItem> tpQitems;
	private List<Integer> calTypeIds;

}