package org.trescal.cwms.core.tpcosts.tpquoterequest.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.pricing.entity.costtype.db.CostTypeService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.db.TPQuoteRequestItemService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.TPQuoteRequestAddModelForm;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.TPQuoteRequestAddModelFormValidator;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Form Controller which enables users to add items to a TPQuoteRequest by
 * searching for models / instrumentmodels. Search results are displayed on the
 * same form page and can be re-searched at any given time. <br/> In addition,
 * the top of the form lists all available CostTypes. On submission the form
 * checks if any of these costs have been added/removed the the TPQuoteRequest
 * default CostTypes list and saves this back to the TPQuoteRequest
 * automatically.
 */
@Controller
@IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class TPQuoteRequestAddModelController {

    @Autowired
    private CalibrationTypeService calTypeServ;
    @Autowired
    private CostTypeService costTypeServ;
    @Autowired
    private InstrumentModelService modelServ;
    @Autowired
    private TPQuoteRequestItemService tpQuoteRequestItemService;
    @Autowired
    private TPQuoteRequestService tpQuoteRequestService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    private TPQuoteRequestAddModelFormValidator validator;

    public static final String FORM_NAME = "tpQuoteRequestItemForm";

    @ModelAttribute("modelTypeSelections")
    private List<KeyValue<Integer, String>> createModelTypeSelections() {
        List<KeyValue<Integer, String>> selections = new ArrayList<>();
        selections.add(new KeyValue<Integer, String>(0, messageSource.getMessage("instmod.all", null, LocaleContextHolder.getLocale())));
        selections.add(new KeyValue<Integer, String>(1, messageSource.getMessage("instmod.salescatonly", null, LocaleContextHolder.getLocale())));
        selections.add(new KeyValue<Integer, String>(2, messageSource.getMessage("instmod.excludesalescat", null, LocaleContextHolder.getLocale())));
        return selections;
    }

    @InitBinder(FORM_NAME)
    protected void initBinder(WebDataBinder binder) throws Exception {
        binder.setValidator(validator);
    }

    @ModelAttribute(FORM_NAME)
    protected TPQuoteRequestAddModelForm formBackingObject(
            @RequestParam(name = "id", required = false, defaultValue = "0") Integer reqid,
            @ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdivDto) throws Exception {
        TPQuoteRequest tpqr = this.tpQuoteRequestService.get(reqid);
        if ((reqid == 0) || (tpqr == null))
            throw new Exception("Third party quote request was not found");
        else {
            TPQuoteRequestAddModelForm tpForm = new TPQuoteRequestAddModelForm();
            tpForm.setTpQuoteRequest(tpqr);
            // set a list of all available costtypes into the fbo. We have to do
            // this here as
            // referenceData() data is lost after the first search submit.
            tpForm.setCalTypes(this.calTypeServ.getActiveCalTypes());
            tpForm.setSearchForm(new InstrumentAndModelSearchForm<InstrumentModel>());
            return tpForm;
        }
    }

    @RequestMapping(value = "/tpquotereqadditemmodel.htm", method = RequestMethod.GET)
    protected String referenceData(Model model) {
        model.addAttribute("costTypes", this.costTypeServ.getAllActiveCostTypes());
        return "trescal/core/tpcosts/tpquoterequest/tpquoterequestaddmodel";
    }

    @RequestMapping(value = "/tpquotereqadditemmodel.htm", method = RequestMethod.POST)
    protected String onSubmit(Model model,
                              @Validated @ModelAttribute(FORM_NAME) TPQuoteRequestAddModelForm tpForm,
                              BindingResult bindingResult) throws Exception {
        if (bindingResult.hasErrors()) {
            return referenceData(model);
        }
        // get the TPQuoteRequest and update the default CostTypes assigned to
        // it - this may have been altered by the user on the form
        TPQuoteRequest tpQuoteRequest = tpForm.getTpQuoteRequest();
        this.tpQuoteRequestService.updateCostTypeDefaults(tpQuoteRequest, tpForm.getDefaultCostTypes());

        // add the items to the tpquoterequest + make sure user is not trying to
        // add no items
        if (tpForm.getSubmitted().trim().equals("add") && (tpForm.getSelectedIds() != null)) {
            for (Integer id : tpForm.getSelectedIds()) {
                TPQuoteRequestItem tpItem = new TPQuoteRequestItem();
                tpItem.setModel(this.modelServ.findInstrumentModel(tpForm.getModelids().get(id)));
                // add each of the TPQuoteRequest's default CostTypes as a new
                // CostType for this item
                this.tpQuoteRequestItemService.addItemCostTypesFromDefaults(tpItem, tpForm.getTpQuoteRequest());
                tpItem.setTpQuoteRequest(tpForm.getTpQuoteRequest());
                tpItem.setQty(tpForm.getModelQtys().get(id));
                tpItem.setCaltype(tpForm.getCalTypeIds().get(id) == null ? null : this.calTypeServ.find(tpForm.getCalTypeIds().get(id)));
                this.tpQuoteRequestItemService.insertTPQuoteRequestItem(tpItem);
            }
            return "redirect:viewtpquoterequest.htm?id=" + tpForm.getTpQuoteRequest().getId();
        }
        // user has submitted a search request, perform the search and send the
        // form back to itself
        else {
            //Set required instrument model types
            switch (tpForm.getModelTypeSelectorChoice()) {
                case 0:
                    tpForm.getSearchForm().setExcludeCapabilityModelTypes(true);
                    break;
                case 1:
                    tpForm.getSearchForm().setExcludeCapabilityModelTypes(true);
                    tpForm.getSearchForm().setExcludeStndardModelTypes(true);
                    break;
                case 2:
                    tpForm.getSearchForm().setExcludeCapabilityModelTypes(true);
                    tpForm.getSearchForm().setExcludeSalesCategoryModelTypes(true);
                    break;
                default:
                    tpForm.getSearchForm().setExcludeCapabilityModelTypes(true);
                    break;
            }
            // search for models to display
            List<InstrumentModel> searchMods = this.modelServ.searchInstrumentModels(tpForm.getSearchForm());
            // convert the results into a HashMap
            Map<Integer, InstrumentModel> searchModels = new HashMap<Integer, InstrumentModel>();
            int id = 0;
            for (InstrumentModel m : searchMods) {
                searchModels.put(id, m);
                id++;
            }
            tpForm.setSearchModels(searchModels);
            return referenceData(model);
        }
    }
}