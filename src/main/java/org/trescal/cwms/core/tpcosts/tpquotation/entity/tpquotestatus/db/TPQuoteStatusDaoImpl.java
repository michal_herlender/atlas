package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.TPQuoteStatus;

@Repository("TPQuoteStatusDao")
public class TPQuoteStatusDaoImpl extends BaseDaoImpl<TPQuoteStatus, Integer> implements TPQuoteStatusDao
{
	@Override
	protected Class<TPQuoteStatus> getEntity() {
		return TPQuoteStatus.class;
	}
}