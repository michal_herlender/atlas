package org.trescal.cwms.core.tpcosts.tpquotation.dto;

import java.math.BigDecimal;

import org.trescal.cwms.core.pricing.entity.costtype.CostType;

public class TPQuotationItemCostDTO {
	
	private boolean setOnQuotation;
	private CostType costType;
	private BigDecimal cost;
	private BigDecimal discountRate;
	
	public TPQuotationItemCostDTO(CostType costType) {
		setOnQuotation = true;
		this.costType = costType;
		cost = new BigDecimal("0.00");
		discountRate = new BigDecimal("0.0");
	}
	
	public boolean isSetOnQuotation() {
		return setOnQuotation;
	}
	
	public void setSetOnQuotation(boolean setOnQuotation) {
		this.setOnQuotation = setOnQuotation;
	}
	
	public CostType getCostType() {
		return costType;
	}
	
	public void setCostType(CostType costType) {
		this.costType = costType;
	}
	
	public BigDecimal getCost() {
		return cost;
	}
	
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	
	public BigDecimal getDiscountRate() {
		return discountRate;
	}
	
	public void setDiscountRate(BigDecimal discountRate) {
		this.discountRate = discountRate;
	}
}