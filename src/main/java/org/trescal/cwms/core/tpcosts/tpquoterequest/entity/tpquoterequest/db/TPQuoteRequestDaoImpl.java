package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db;

import static org.trescal.cwms.core.tools.StringJpaUtils.ilike;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.criteria.CompoundSelection;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.tuple.Triple;
import org.hibernate.criterion.MatchMode;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.AllocatedToCompanyDaoImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.company.Company_;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany;
import org.trescal.cwms.core.company.entity.companysettings.CompanySettingsForAllocatedCompany_;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.contact.Contact_;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv_;
import org.trescal.cwms.core.system.entity.translation.Translation;
import org.trescal.cwms.core.system.entity.translation.Translation_;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tpcosts.tpquoterequest.dto.TPQuoteRequestDTO;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest_;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequeststatus.TPQuoteRequestStatus;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequeststatus.TPQuoteRequestStatus_;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.TPQuoteRequestSearchForm;

@Repository("TPQuoteRequestDao")
public class TPQuoteRequestDaoImpl extends AllocatedToCompanyDaoImpl<TPQuoteRequest, Integer>
		implements TPQuoteRequestDao {
	@Override
	protected Class<TPQuoteRequest> getEntity() {
		return TPQuoteRequest.class;
	}

	@Override
	public TPQuoteRequest getTPQuoteReqByExactTPQRNo(String tpqrNo, Company allocatedCompany) {
		return getFirstResult(cb -> {
			CriteriaQuery<TPQuoteRequest> cq = cb.createQuery(TPQuoteRequest.class);
			Root<TPQuoteRequest> root = cq.from(TPQuoteRequest.class);
			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(TPQuoteRequest_.organisation), allocatedCompany));
			clauses.getExpressions().add(cb.equal(root.get(TPQuoteRequest_.requestNo), tpqrNo));
			cq.where(clauses);
			return cq;
		}).orElse(null);
	}

	@Override
	public PagedResultSet<TPQuoteRequestDTO> searchTPQuoteRequest(TPQuoteRequestSearchForm form,
			PagedResultSet<TPQuoteRequestDTO> rs, Company allocatedCompany) {
		Locale locale = LocaleContextHolder.getLocale();

		super.completePagedResultSet(rs, TPQuoteRequestDTO.class, cb -> cq -> {
			Root<TPQuoteRequest> root = cq.from(TPQuoteRequest.class);
			Join<TPQuoteRequest, Contact> contactJoin = root.join(TPQuoteRequest_.contact);
			Join<Contact, Subdiv> subJoin = contactJoin.join(Contact_.sub);
			Join<Subdiv, Company> companyJoin = subJoin.join(Subdiv_.comp);
			Join<Company, CompanySettingsForAllocatedCompany> settings = companyJoin
					.join(Company_.settingsForAllocatedCompanies, JoinType.LEFT);
			Join<TPQuoteRequest, TPQuoteRequestStatus> statusJoin = root.join(TPQuoteRequest_.status);
			Join<TPQuoteRequestStatus, Translation> statusTranslation = statusJoin
					.join(TPQuoteRequestStatus_.nametranslations, JoinType.LEFT);
			settings.on(cb.equal(settings.get(CompanySettingsForAllocatedCompany_.organisation), allocatedCompany));
			statusTranslation.on(cb.equal(statusTranslation.get(Translation_.locale), locale));

			Predicate clauses = cb.conjunction();
			clauses.getExpressions().add(cb.equal(root.get(TPQuoteRequest_.organisation), allocatedCompany));

			// company restriction
			if (form.getCoid() != null) {
				clauses.getExpressions().add(cb.equal(companyJoin.get(Company_.coid), form.getCoid()));
			} else if ((form.getConame() != null) && !form.getConame().trim().equals("")) {
				clauses.getExpressions()
						.add(ilike(cb, companyJoin.get(Company_.coname), form.getConame(), MatchMode.START));
			}
			// contact restriction
			if (form.getPersonid() != null) {
				clauses.getExpressions().add(cb.equal(contactJoin.get(Contact_.personid), form.getPersonid()));
			} else if ((form.getContact() != null) && !form.getContact().trim().equals("")) {
				clauses.getExpressions()
						.add(ilike(cb, contactJoin.get(Contact_.firstName), form.getContact(), MatchMode.START));
			}
			// requestNo restriction
			if ((form.getRequestno() != null) && !form.getRequestno().trim().equals("")) {
				clauses.getExpressions()
						.add(ilike(cb, root.get(TPQuoteRequest_.requestNo), form.getRequestno(), MatchMode.ANYWHERE));
			}

			// status restriction
			if (form.getStatusid() != null) {
				clauses.getExpressions()
						.add(cb.equal(statusJoin.get(TPQuoteRequestStatus_.statusid), form.getStatusid()));
			}
			cq.where(clauses);
			cq.distinct(true);

			CompoundSelection<TPQuoteRequestDTO> selection = cb.construct(TPQuoteRequestDTO.class,
					root.get(TPQuoteRequest_.id), root.get(TPQuoteRequest_.requestNo),
					contactJoin.get(Contact_.personid), contactJoin.get(Contact_.firstName),
					contactJoin.get(Contact_.lastName), contactJoin.get(Contact_.active),
					companyJoin.get(Company_.coid), companyJoin.get(Company_.coname),
					settings.get(CompanySettingsForAllocatedCompany_.active),
					settings.get(CompanySettingsForAllocatedCompany_.onStop),
					statusTranslation.get(Translation_.translation), root.get(TPQuoteRequest_.issuedate),
					root.get(TPQuoteRequest_.regdate));

			// Most recent tp quote request to appear first
			List<javax.persistence.criteria.Order> order = new ArrayList<>();
			order.add(cb.desc(root.get(TPQuoteRequest_.regdate)));
			order.add(cb.desc(root.get(TPQuoteRequest_.requestNo)));

			return Triple.of(root, selection, order);
		});

		return rs;
	}
	
	@Override
	public List<ContactKeyValue> getClientContacts(TPQuoteRequest tpQuoteRequest){
		return getResultList(cb ->{
			CriteriaQuery<ContactKeyValue> cq = cb.createQuery(ContactKeyValue.class);
			Root<TPQuoteRequest> tpQuoteRequestRoot = cq.from(TPQuoteRequest.class);
			Join<TPQuoteRequest, Contact> contact = tpQuoteRequestRoot.join(TPQuoteRequest_.contact);
			Join<Contact, Subdiv> subdiv = contact.join(Contact_.sub);
			Join<Subdiv, Contact> contacts = subdiv.join(Subdiv_.contacts);
			cq.where(cb.equal(tpQuoteRequestRoot.get(TPQuoteRequest_.id), tpQuoteRequest.getId()));
			cq.select(cb.construct(ContactKeyValue.class, 
					contacts.get(Contact_.personid),
					contacts.get(Contact_.firstName),
					contacts.get(Contact_.lastName)));
			return cq;
		});
	}
	
	
	
	
	
	
}