package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.db;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem_;

@Repository
public class TPQuotationItemDaoImpl extends BaseDaoImpl<TPQuotationItem, Integer> implements TPQuotationItemDao
{
	@Override
	protected Class<TPQuotationItem> getEntity() {
		return TPQuotationItem.class;
	}
	
	public List<TPQuotationItem> getWithCosts(List<Integer> itemIds) {
		List<TPQuotationItem> result = Collections.emptyList();
		if (!itemIds.isEmpty()) { 
			result = getResultList(cb -> {
				CriteriaQuery<TPQuotationItem> cq = cb.createQuery(TPQuotationItem.class);
				Root<TPQuotationItem> item = cq.from(TPQuotationItem.class);
				item.fetch(TPQuotationItem_.adjustmentCost, JoinType.LEFT);
				item.fetch(TPQuotationItem_.calibrationCost, JoinType.LEFT);
				item.fetch(TPQuotationItem_.repairCost, JoinType.LEFT);
				item.fetch(TPQuotationItem_.purchaseCost, JoinType.LEFT);
				cq.where(item.get(TPQuotationItem_.id).in(itemIds));
				return cq;
			});
		}
		return result;
/*		Criteria crit = getSession().createCriteria(TPQuotationItem.class);
		crit.add(Restrictions.in("id", itemIds));
		crit.setFetchMode("adjustmentCost", FetchMode.JOIN);
		crit.setFetchMode("calibrationCost", FetchMode.JOIN);
		crit.setFetchMode("repairCost", FetchMode.JOIN);
		crit.setFetchMode("purchaseCost", FetchMode.JOIN);
		return (List<TPQuotationItem>) crit.list(); */
	}
	
	@SuppressWarnings("unchecked")
	public TPQuotationItem getWithCosts(int id) {
		
		DetachedCriteria crit = DetachedCriteria.forClass(TPQuotationItem.class);
		crit.add(Restrictions.idEq(id));
		crit.setFetchMode("adjustmentCost", FetchMode.JOIN);
		crit.setFetchMode("calibrationCost", FetchMode.JOIN);
		crit.setFetchMode("repairCost", FetchMode.JOIN);
		crit.setFetchMode("purchaseCost", FetchMode.JOIN);
		
		crit.setFetchMode("caltype", FetchMode.JOIN);

		crit.setFetchMode("model", FetchMode.JOIN);
		crit.setFetchMode("model.mfr", FetchMode.JOIN);
		crit.setFetchMode("model.description", FetchMode.JOIN);

		crit.setFetchMode("tpquotation", FetchMode.JOIN);
		crit.setFetchMode("tpquotation.currency", FetchMode.JOIN);
		crit.setFetchMode("tpquotation.contact", FetchMode.JOIN);
		crit.setFetchMode("tpquotation.contact.sub", FetchMode.JOIN);
		crit.setFetchMode("tpquotation.contact.sub.comp", FetchMode.JOIN);
		List<TPQuotationItem> list = (List<TPQuotationItem>) crit.getExecutableCriteria(getSession()).list();
		return list.size() == 0 ? null : list.get(0);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TPQuotationItem> getMatchingItems(CostType ct, int modelid)
	{
		DetachedCriteria crit = DetachedCriteria.forClass(TPQuotationItem.class);
		DetachedCriteria costCrit = null;

		if (ct == CostType.ADJUSTMENT)
			costCrit = crit.createCriteria("adjustmentCost");
		else if (ct == CostType.CALIBRATION)
			costCrit = crit.createCriteria("calibrationCost");
		else if (ct == CostType.REPAIR)
			costCrit = crit.createCriteria("repairCost");
		else if (ct == CostType.PURCHASE)
			costCrit = crit.createCriteria("purchaseCost");
		costCrit.add(Restrictions.eq("active", true));
		// this function is primarily called by dwr calls so we have to eagerly
		// load everything we want to display
		crit.createCriteria("model").add(Restrictions.idEq(modelid));
		crit.setFetchMode("adjustmentCost", FetchMode.JOIN);
		crit.setFetchMode("adjustmentCost.costType", FetchMode.JOIN);
		crit.setFetchMode("calibrationCost", FetchMode.JOIN);
		crit.setFetchMode("calibrationCost.costType", FetchMode.JOIN);
		crit.setFetchMode("repairCost", FetchMode.JOIN);
		crit.setFetchMode("repairCost.costType", FetchMode.JOIN);
		crit.setFetchMode("purchaseCost", FetchMode.JOIN);
		crit.setFetchMode("purchaseCost.costType", FetchMode.JOIN);

		crit.setFetchMode("caltype", FetchMode.JOIN);
		crit.setFetchMode("caltype.serviceType", FetchMode.JOIN);
		crit.setFetchMode("model", FetchMode.JOIN);
		crit.setFetchMode("model.mfr", FetchMode.JOIN);
		crit.setFetchMode("model.description", FetchMode.JOIN);

		crit.setFetchMode("tpquotation", FetchMode.JOIN);
		crit.setFetchMode("tpquotation.currency", FetchMode.JOIN);
		crit.setFetchMode("tpquotation.contact", FetchMode.JOIN);
		crit.setFetchMode("tpquotation.contact.sub", FetchMode.JOIN);
		crit.setFetchMode("tpquotation.contact.sub.comp", FetchMode.JOIN);
		
		DetachedCriteria critTp = crit.createCriteria("tpquotation");
		critTp.addOrder(Order.desc("regdate"));
		return (List<TPQuotationItem>) crit.getExecutableCriteria(getSession()).list();
	}
	
	public Integer getMaxItemno(int tpQuoteId)
	{
		Criteria criteria = getSession().createCriteria(TPQuotationItem.class)
				.add(Restrictions.eq("tpquotation.id", tpQuoteId))
				.setProjection(Projections.max("itemno"));
		return  criteria.uniqueResult() == null ? 0 : (Integer) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public PagedResultSet<TPQuotationItem> searchTPQuotationItem(PagedResultSet<TPQuotationItem> ps, Integer modelid, Integer years, boolean expired, Integer coid)
	{
		Criteria crit = getSession().createCriteria(TPQuotationItem.class);
		Criteria tpquoteCrit = crit.createCriteria("tpquotation");
		crit.setFetchMode("model", FetchMode.JOIN);
		crit.setFetchMode("model.mfr", FetchMode.JOIN);
		crit.setFetchMode("model.description", FetchMode.JOIN);
		crit.setFetchMode("caltype", FetchMode.JOIN);
		crit.setFetchMode("calibrationCost", FetchMode.JOIN);
		crit.setFetchMode("repairCost", FetchMode.JOIN);
		crit.setFetchMode("purchaseCost", FetchMode.JOIN);
		crit.setFetchMode("adjustmentCost", FetchMode.JOIN);
		crit.setFetchMode("tpquotation", FetchMode.JOIN);
		crit.setFetchMode("tpquotation.contact", FetchMode.JOIN);
		crit.setFetchMode("tpquotation.contact.sub", FetchMode.JOIN);
		crit.setFetchMode("tpquotation.contact.sub.comp", FetchMode.JOIN);
		crit.setFetchMode("linkedTo", FetchMode.JOIN);
		// add model restrictions
		if ((modelid != null) && (modelid > 0))
		{
			crit.createCriteria("model").add(Restrictions.idEq(modelid));
		}
		// add company restrictions
		if ((coid != null) && (coid > 0))
		{
			tpquoteCrit.createCriteria("contact").createCriteria("sub").createCriteria("comp").add(Restrictions.idEq(coid));
		}
		// this is purposefully not currently implemented - too complex for
		// criteria - needs hql (see quotationdao.searchCompanyQuotations
		if (!expired)
		{

		}
		if ((years != null) && (years > 0))
		{
			LocalDate fromRegdate = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()).minusYears(years);
			tpquoteCrit.add(Restrictions.gt("regdate", fromRegdate));
		}
		crit.setProjection(Projections.rowCount());
		Long count = (Long) crit.uniqueResult();
		int itemCount = count.intValue();
		ps.setResultsCount(itemCount);

		// remove the projection from the query so it will return a normal
		// resultset
		crit.setProjection(null);
		crit.setResultTransformer(Criteria.ROOT_ENTITY);

		// update the query so it will return the correct number of rows
		// from the correct start point
		crit.setFirstResult(ps.getStartResultsFrom());
		crit.setMaxResults(ps.getResultsPerPage());

		tpquoteCrit.addOrder(Order.desc("regdate"));
		tpquoteCrit.addOrder(Order.desc("tpqno"));
		crit.addOrder(Order.asc("itemno"));

		// get results
		ps.setResults(crit.list());

		return ps;
	}
}