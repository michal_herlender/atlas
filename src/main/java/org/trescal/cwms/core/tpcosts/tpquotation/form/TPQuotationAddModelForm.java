package org.trescal.cwms.core.tpcosts.tpquotation.form;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.trescal.cwms.core.instrument.form.InstrumentAndModelSearchForm;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.system.entity.calibrationtype.CalibrationType;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;

public class TPQuotationAddModelForm
{
	private HashMap<Integer, CostType> availableCostTypes;
	private List<Integer> calTypeIds;
	// reference fields
	private HashMap<Integer, CalibrationType> caltypes;

	private List<Integer> defcaltypeids;
	private String desc;

	private String instmodel;
	private List<Integer> itemNos;
	private Integer maxQuantity;
	// form fields
	private String mfr;
	private List<Integer> modelIds;

	private List<String> modelNames;
	private int nextId;
	private List<Integer> qtys;
	private InstrumentAndModelSearchForm<InstrumentModel> search;
	/**
	 * Map of models returned by the search.
	 */
	private Map<Integer, InstrumentModel> searchModels;

	private String submitted;

	// backing fields
	private TPQuotation tpQuote;
	
	private String mfrtext;
	
	private Integer modelTypeSelectorChoice;

	public HashMap<Integer, CostType> getAvailableCostTypes()
	{
		return this.availableCostTypes;
	}

	public List<Integer> getCalTypeIds()
	{
		return this.calTypeIds;
	}

	public HashMap<Integer, CalibrationType> getCaltypes()
	{
		return this.caltypes;
	}

	public List<Integer> getDefcaltypeids()
	{
		return this.defcaltypeids;
	}

	public String getDesc()
	{
		return this.desc;
	}

	public String getInstmodel()
	{
		return this.instmodel;
	}

	public List<Integer> getItemNos()
	{
		return this.itemNos;
	}

	public Integer getMaxQuantity()
	{
		return this.maxQuantity;
	}

	public String getMfr()
	{
		return this.mfr;
	}

	public List<Integer> getModelIds()
	{
		return this.modelIds;
	}

	public List<String> getModelNames()
	{
		return this.modelNames;
	}

	public int getNextId()
	{
		return this.nextId;
	}

	public List<Integer> getQtys()
	{
		return this.qtys;
	}

	public InstrumentAndModelSearchForm<InstrumentModel> getSearch()
	{
		return this.search;
	}

	public Map<Integer, InstrumentModel> getSearchModels()
	{
		return this.searchModels;
	}

	public String getSubmitted()
	{
		return this.submitted;
	}

	public TPQuotation getTpQuote()
	{
		return this.tpQuote;
	}

	public void setAvailableCostTypes(HashMap<Integer, CostType> availableCostTypes)
	{
		this.availableCostTypes = availableCostTypes;
	}

	public void setCalTypeIds(List<Integer> calTypeIds)
	{
		this.calTypeIds = calTypeIds;
	}

	public void setCaltypes(HashMap<Integer, CalibrationType> caltypes)
	{
		this.caltypes = caltypes;
	}

	public void setDefcaltypeids(List<Integer> caltypeids)
	{
		this.defcaltypeids = caltypeids;
	}

	public void setDesc(String desc)
	{
		this.desc = desc;
	}

	public void setInstmodel(String instmodel)
	{
		this.instmodel = instmodel;
	}

	public void setItemNos(List<Integer> itemNos)
	{
		this.itemNos = itemNos;
	}

	public void setMaxQuantity(Integer maxQuantity)
	{
		this.maxQuantity = maxQuantity;
	}

	public void setMfr(String mfr)
	{
		this.mfr = mfr;
	}

	public void setModelIds(List<Integer> modelidIds)
	{
		this.modelIds = modelidIds;
	}

	public void setModelNames(List<String> modelNames)
	{
		this.modelNames = modelNames;
	}

	public void setNextId(int nextId)
	{
		this.nextId = nextId;
	}

	public void setQtys(List<Integer> qtys)
	{
		this.qtys = qtys;
	}

	public void setSearch(InstrumentAndModelSearchForm<InstrumentModel> search)
	{
		this.search = search;
	}

	public void setSearchModels(Map<Integer, InstrumentModel> searchModels)
	{
		this.searchModels = searchModels;
	}

	public void setSubmitted(String submitted)
	{
		this.submitted = submitted;
	}

	public void setTpQuote(TPQuotation tpQuote)
	{
		this.tpQuote = tpQuote;
	}

	public String getMfrtext() {
		return mfrtext;
	}

	public void setMfrtext(String mfrtext) {
		this.mfrtext = mfrtext;
	}

	public Integer getModelTypeSelectorChoice() {
		return modelTypeSelectorChoice;
	}

	public void setModelTypeSelectorChoice(Integer modelTypeSelectorChoice) {
		this.modelTypeSelectorChoice = modelTypeSelectorChoice;
	}
}