package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.entity.user.db.UserService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.pricing.TotalDiscountCalculator;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.NumberTools;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tpcosts.tpquotation.dto.TPQuotationAjaxWrapper;
import org.trescal.cwms.core.tpcosts.tpquotation.dto.TPQuotationItemCostDTO;
import org.trescal.cwms.core.tpcosts.tpquotation.dto.TPQuotationItemDTO;
import org.trescal.cwms.core.tpcosts.tpquotation.dto.TPQuotationRowDTO;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.adjcost.TPQuotationAdjustmentCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.calcost.TPQuotationCalibrationCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.purchasecost.TPQuotationPurchaseCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.repcost.TPQuotationRepairCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.db.TPQuotationItemService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.TPQuoteLink;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.TPQuoteStatus;
import org.trescal.cwms.core.tpcosts.tpquotation.form.CreateTPQuotationFromTPQuoteRequestForm;
import org.trescal.cwms.core.tpcosts.tpquotation.form.TPQuoteSearchForm;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequeststatus.TPQuoteRequestStatus;
import org.trescal.cwms.spring.model.KeyValue;

@Service("TPQuotationService")
public class TPQuotationServiceImpl extends BaseServiceImpl<TPQuotation, Integer> implements TPQuotationService {
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private ContactService contactServ;
	@Autowired
	private JobItemService jobitemServ;
	@Autowired
	private MessageSource messages;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private SupportedCurrencyService currencyServ;
	@Autowired
	private SystemComponentService scServ;
	@Autowired
	private UserService userService;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private TPQuotationItemService tpqiServ;
	@Autowired
	private TPQuotationDao tpquotationDao;
	@Autowired
	private TPQuoteRequestService tpqrServ;

	@Override
	protected BaseDao<TPQuotation, Integer> getBaseDao() {
		return tpquotationDao;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResultWrapper createAjaxTPQuotation(Integer personid, String tpqno, String quotedateText, String calcost,
											   String calcostdisc, String repaircost, String repaircostdisc, String adjustcost, String adjustcostdisc,
											   String salescost, String salescostdisc, String inspectioncost, String itemdisc, String carin, String carout,
											   Integer jobitemid, Integer duration, String currencyCode, Integer statusid, HttpSession session) {
		String username = (String) session.getAttribute(Constants.SESSION_ATTRIBUTE_USERNAME);
		Contact userContact = this.userService.getEagerLoad(username).getCon();
		KeyValue<Integer, String> subdivDto = (KeyValue<Integer, String>) session
			.getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV);
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		personid = personid == null ? 0 : personid;
		jobitemid = jobitemid == null ? 0 : jobitemid;
		duration = duration == null ? 30 : duration;
		calcost = NumberTools.isADouble(calcost) ? calcost : "0.00";
		calcostdisc = NumberTools.isADouble(calcostdisc) ? calcostdisc : "0.00";
		repaircost = NumberTools.isADouble(repaircost) ? repaircost : "0.00";
		repaircostdisc = NumberTools.isADouble(repaircostdisc) ? repaircostdisc : "0.00";
		salescost = NumberTools.isADouble(salescost) ? salescost : "0.00";
		salescostdisc = NumberTools.isADouble(salescostdisc) ? salescostdisc : "0.00";
		adjustcost = NumberTools.isADouble(adjustcost) ? adjustcost : "0.00";
		adjustcostdisc = NumberTools.isADouble(adjustcostdisc) ? adjustcostdisc : "0.00";
		inspectioncost = NumberTools.isADouble(inspectioncost) ? inspectioncost : "0.00";
		itemdisc = NumberTools.isADouble(itemdisc) ? itemdisc : "0.00";
		// test contact
		Contact contact = this.contactServ.findEagerContact(personid);
		if ((contact == null) || (personid == 0))
			return new ResultWrapper(false, this.messages.getMessage("error.tpquotation.nocontact", null,
					"A contact must be selected for the quotation.", null));
		else {
			JobItem jobitem = this.jobitemServ.findJobItem(jobitemid);
			if ((jobitem == null) || (jobitemid == 0)) {
				return new ResultWrapper(false, this.messages.getMessage("error.tpquotation.nojobitem", null,
						"A jobitem must be selected for the quotation.", null));
			} else {
				// we have all the required fields, now create the quote and
				// an item.
				TPQuotation tpquote = new TPQuotation();
				tpquote.setOrganisation(allocatedSubdiv.getComp());
				tpquote.setContact(contact);
				tpquote.setCreatedBy(userContact);
				tpquote.setDuration(duration);
				LocalDate now = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
				tpquote.setExpiryDate(now.plusDays(duration));
				tpquote.setRegdate(now);
				tpquote.setReqdate(now);
				tpquote.setTpqno(tpqno);
				// currency + date
				// check the date
				LocalDate quotedate = LocalDate.parse(quotedateText);
				tpquote.setIssuedate(quotedate);
				// get the currency
				tpquote.setCurrency(this.currencyServ.findCurrencyByCode(currencyCode));
				tpquote.setRate(tpquote.getCurrency().getDefaultRate());
				// work out the status
				tpquote.setStatus(this.statusServ.findStatus(statusid, TPQuoteStatus.class));
				// add item + costs
				TPQuotationItem item = new TPQuotationItem();
				item.setCaltype(jobitem.getServiceType().getCalibrationType());
				item.setItemno(1);
				item.setModel(jobitem.getInst().getModel());
				item.setQuantity(1);
				item.setTpquotation(tpquote);
				TPQuotationCalibrationCost cc = new TPQuotationCalibrationCost();
				cc.setActive(true);
				cc.setAutoSet(false);
				cc.setDiscountRate(new BigDecimal(calcostdisc));
				cc.setDiscountValue(new BigDecimal("0.00"));
				cc.setTotalCost(new BigDecimal(calcost));
				cc.setFinalCost(new BigDecimal(calcost));
				cc.setCostType(CostType.CALIBRATION);
				item.setCalibrationCost((TPQuotationCalibrationCost) CostCalculator.updateSingleCost(cc, false));
				TPQuotationRepairCost rc = new TPQuotationRepairCost();
				rc.setActive(true);
				rc.setAutoSet(false);
				rc.setDiscountRate(new BigDecimal(repaircostdisc));
				rc.setDiscountValue(new BigDecimal("0.00"));
				rc.setTotalCost(new BigDecimal(repaircost));
				rc.setFinalCost(new BigDecimal(repaircost));
				rc.setCostType(CostType.REPAIR);
				item.setRepairCost((TPQuotationRepairCost) CostCalculator.updateSingleCost(rc, false));
				TPQuotationAdjustmentCost ac = new TPQuotationAdjustmentCost();
				ac.setActive(true);
				ac.setAutoSet(false);
				ac.setDiscountRate(new BigDecimal(adjustcostdisc));
				ac.setDiscountValue(new BigDecimal("0.00"));
				ac.setTotalCost(new BigDecimal(adjustcost));
				ac.setFinalCost(new BigDecimal(adjustcost));
				ac.setCostType(CostType.ADJUSTMENT);
				item.setAdjustmentCost((TPQuotationAdjustmentCost) CostCalculator.updateSingleCost(ac, false));
				TPQuotationPurchaseCost sc = new TPQuotationPurchaseCost();
				sc.setActive(true);
				sc.setAutoSet(false);
				sc.setDiscountRate(new BigDecimal(salescostdisc));
				sc.setDiscountValue(new BigDecimal("0.00"));
				sc.setTotalCost(new BigDecimal(salescost));
				sc.setFinalCost(new BigDecimal(salescost));
				sc.setCostType(CostType.PURCHASE);
				item.setPurchaseCost((TPQuotationPurchaseCost) CostCalculator.updateSingleCost(sc, false));
				item.setInspection(new BigDecimal(inspectioncost));
				item.setCarriageIn(new BigDecimal(carin));
				item.setCarriageOut(new BigDecimal(carout));
				item.setGeneralDiscountRate(new BigDecimal(itemdisc));
				item.setGeneralDiscountValue(new BigDecimal("0.00"));
				// link tpquotationitem to jobitem
				TPQuoteLink link = new TPQuoteLink();
				link.setComment("");
				link.setContact(userContact);
				link.setDate(now);
				link.setJobItem(jobitem);
				link.setTpQuoteItem(item);
				item.setLinkedTo(new HashSet<>());
				item.getLinkedTo().add(link);
				SortedSet<TPQuotationItem> items = new TreeSet<>();
				items.add(item);
				tpquote.setItems(items);
				// calculate costs
				this.tpqiServ.updateTPQuotationItemCosts(item);
				CostCalculator.updatePricingFinalCost(tpquote, tpquote.getItems());
				// run validation
				// work out and set the id last - don't want to issue an id and
				// then have the item fail validation
				String qno = numerationService.generateNumber(NumerationType.TPQUOTATION, allocatedSubdiv.getComp(),
						allocatedSubdiv);
				tpquote.setQno(qno);
				this.save(tpquote);
				// No LocalDate support in DWR; use DTO until full replacement occurs
				Locale locale = LocaleContextHolder.getLocale();
				TPQuotationRowDTO dto = new TPQuotationRowDTO(link, locale);
				return new ResultWrapper(true, "", dto, null);
			}
		}
	}

	@Override
	public TPQuotation findEagerTPQuotation(int id) {
		return this.tpquotationDao.findEagerTPQuotation(id);
	}

	@Override
	public TPQuotation get(Integer id) {
		TPQuotation tpquotation = this.tpquotationDao.find(id);
		// 2018-12-12 GB : Prevent directory lookup on repeated gets.
		if (tpquotation != null && tpquotation.getDirectory() == null)
			tpquotation.setDirectory(this.compDirServ
					.getDirectory(this.scServ.findComponent(Component.THIRD_PARTY_QUOTATION), tpquotation.getQno()));
		return tpquotation;
	}

	@Override
	public TPQuotationAjaxWrapper getAjaxTPQuotationReferenceData(Integer jobItemId) {
		JobItem jobItem = this.jobitemServ.get(jobItemId);
		
		TPQuotationAjaxWrapper wrapper = new TPQuotationAjaxWrapper();
		wrapper.setDefaultCurrencyCode(jobItem.getJob().getCurrency().getCurrencyCode());
		wrapper.setCurrencyList(this.currencyServ.getAllSupportedCurrencys());
		wrapper.setStatusList(this.statusServ.getAllStatuss(TPQuoteStatus.class));
		return wrapper;
	}

	@Override
	public TotalDiscountCalculator getQuotationTotalDiscount(Set<TPQuotationItem> items) {
		return new TotalDiscountCalculator(items);
	}

	@Override
	public PagedResultSet<TPQuotation> getTPQuotations(PagedResultSet<TPQuotation> rs, TPQuoteSearchForm tpsf) {
		return this.tpquotationDao.getTPQuotations(rs, tpsf);
	}

	@Override
	public TPQuotation getTPQuoteByExactTPQNo(String tpqNo) {
		return this.tpquotationDao.getTPQuoteByExactTPQNo(tpqNo);
	}

	@Override
	public List<TPQuotation> getTPQuotesFromTPRequest(int tpqrId) {
		return this.tpquotationDao.getTPQuotesFromTPRequest(tpqrId);
	}

	@Override
	public CreateTPQuotationFromTPQuoteRequestForm initialiseTPQuotationForm(TPQuoteRequest tpQuoteRequest) {
		CreateTPQuotationFromTPQuoteRequestForm tpQuoteForm = new CreateTPQuotationFromTPQuoteRequestForm();
		tpQuoteForm.setContactId(tpQuoteRequest.getContact().getPersonid());
		tpQuoteForm.setDuration(30);
		tpQuoteForm.setRegDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		tpQuoteForm.setIssueDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		tpQuoteForm.setStatusId(0);
		// create items
		List<TPQuotationItemDTO> items = new ArrayList<>();
		for (TPQuoteRequestItem item : tpQuoteRequest.getItems()) {
			Integer calTypeId = item.getCaltype() == null ? 0 : item.getCaltype().getCalTypeId();
			TPQuotationItemDTO newItem = new TPQuotationItemDTO(item.getId(), calTypeId, item.getModel());
			if (!item.getNotes().isEmpty()) {
				newItem.setNotes(item.getNotes());
			}
			for (CostType costType : item.getCostTypes())
				newItem.getCosts().add(new TPQuotationItemCostDTO(costType));
			newItem.setModules(new ArrayList<>());
			for (TPQuoteRequestItem module : item.getModules()) {
				Integer moduleCalTypeId = module.getCaltype() == null ? 0 : module.getCaltype().getCalTypeId();
				TPQuotationItemDTO newModule = new TPQuotationItemDTO(module.getId(), moduleCalTypeId,
						module.getModel());
				for (CostType costType : module.getCostTypes())
					newModule.getCosts().add(new TPQuotationItemCostDTO(costType));
				newItem.getModules().add(newModule);
			}
			items.add(newItem);
		}
		tpQuoteForm.setItems(items);
		return tpQuoteForm;
	}

	@Override
	public TPQuotation merge(TPQuotation tpquotation) {
		tpquotation = this.tpquotationDao.merge(tpquotation);
		tpquotation.setDirectory(this.compDirServ
				.getDirectory(this.scServ.findComponent(Component.THIRD_PARTY_QUOTATION), tpquotation.getQno()));
		return tpquotation;
	}

	@Override
	public TPQuotation updateItemCosts(int tpQuoteId) {
		TPQuotation tpQuote = this.tpquotationDao.findTPQuotationWithItems(tpQuoteId);
		return this.updateItemCosts(tpQuote);
	}

	@Override
	public TPQuotation updateItemCosts(TPQuotation tpquotation) {
		tpquotation = (TPQuotation) CostCalculator.updatePricingFinalCost(tpquotation, tpquotation.getItems());
		this.merge(tpquotation);
		return tpquotation;
	}

	@Override
	public TPQuotation createTPQuotation(String username, Integer allocatedSubdivId, 
			CreateTPQuotationFromTPQuoteRequestForm tpQuoteForm, Integer tpQuoteRequestId) {
		Contact currentContact = userService.get(username).getCon();
		Subdiv allocatedSubdiv = subdivService.get(allocatedSubdivId);
		TPQuoteRequest tpQuoteRequest = this.tpqrServ.get(tpQuoteRequestId);
		
		TPQuotation tpQuote = new TPQuotation();
		tpQuote.setOrganisation(allocatedSubdiv.getComp());
		tpQuote.setCreatedBy(currentContact);
		tpQuote.setContact(this.contactServ.get(tpQuoteForm.getContactId()));
		String qno = numerationService.generateNumber(NumerationType.TPQUOTATION, allocatedSubdiv.getComp(),
				allocatedSubdiv);
		tpQuote.setQno(qno);
		tpQuote.setTpqno(tpQuoteForm.getTpQuoteNumber());
		tpQuote.setRegdate(tpQuoteForm.getRegDate());
		if (tpQuoteForm.getIssueDate() != null) {
			tpQuote.setIssuedate(tpQuoteForm.getIssueDate());
			tpQuote.setIssueby(currentContact);
		}
		tpQuote.setIssued(tpQuoteForm.getIssueDate() != null);
		tpQuote.setDuration(tpQuoteForm.getDuration());
		tpQuote.setCurrency(tpQuoteRequest.getCurrency());
		tpQuote.setRate(tpQuoteRequest.getRate());
		tpQuote.setStatus(statusServ.findStatus(tpQuoteForm.getStatusId(), TPQuoteStatus.class));
		tpQuote.setFromRequest(tpQuoteRequest);

		// iterate over the selected items
		this.tpqiServ.createTPQuotationItems(tpQuoteForm.getItems(), tpQuote, tpQuoteRequest, currentContact);
		// update the third party quotation status to be 'received'
		tpQuoteRequest.setStatus(this.statusServ.findFollowingReceiptStatus(TPQuoteRequestStatus.class));
		tpQuoteRequest.setReceiptDate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
		
		tpQuote = (TPQuotation) CostCalculator.updatePricingFinalCost(tpQuote, tpQuote.getItems());
		
		this.save(tpQuote);
		tpQuote.setDirectory(this.compDirServ.getDirectory(this.scServ.findComponent(Component.THIRD_PARTY_QUOTATION),
				tpQuote.getQno()));
		return tpQuote;
	}
}