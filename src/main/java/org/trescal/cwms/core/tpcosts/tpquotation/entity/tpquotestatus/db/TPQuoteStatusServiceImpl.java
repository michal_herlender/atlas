package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.TPQuoteStatus;

@Service("TPQuoteStatusService")
public class TPQuoteStatusServiceImpl extends BaseServiceImpl<TPQuoteStatus, Integer> implements TPQuoteStatusService
{
	@Autowired
	private TPQuoteStatusDao tpquotestatusDao;

	@Override
	protected BaseDao<TPQuoteStatus, Integer> getBaseDao() {
		return tpquotestatusDao;
	}
}