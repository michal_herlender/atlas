package org.trescal.cwms.core.tpcosts.tpquotation.form;

import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EditTPQuotationItemForm {
	private TPQuotationItem tpQitem;
	private int caltypeid;
}