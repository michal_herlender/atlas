package org.trescal.cwms.core.tpcosts.tpquotation.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.documents.Document;
import org.trescal.cwms.core.documents.DocumentService;
import org.trescal.cwms.core.documents.birt.common.BaseDocumentType;
import org.trescal.cwms.core.documents.filenamingservice.DefaultFileNamingService;
import org.trescal.cwms.core.documents.filenamingservice.FileNamingService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;

@Controller @IntranetController
@SessionAttributes({Constants.HTTPSESS_NEW_FILE_LIST})
public class TPQuotationBirtDocumentController {
	@Autowired
	private DocumentService documentService;
	@Autowired
	private TPQuotationService tpQuoteService;

	@RequestMapping(value="/tpquotebirtdocs.htm")
	public String handleRequest(Locale locale,
			@RequestParam(value="id", required=true) Integer tpQuoteId,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> sessionNewFileList) throws Exception {
		TPQuotation tpQuote = this.tpQuoteService.get(tpQuoteId);
		String rootDirectory = tpQuote.getDirectory().getAbsolutePath();
		// There is no specific file naming service for TP Quote
		FileNamingService fnService = new DefaultFileNamingService(Component.THIRD_PARTY_QUOTATION, rootDirectory);
		Document doc = documentService.createBirtDocument(tpQuoteId, BaseDocumentType.TP_QUOTATION, locale, Component.THIRD_PARTY_QUOTATION, fnService);
		documentService.addFileNameToSession(doc, sessionNewFileList);
		return "redirect:viewtpquote.htm?id=" + tpQuoteId + "&loadtab=docs-tab";
	}
}
