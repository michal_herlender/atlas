package org.trescal.cwms.core.tpcosts.tpquoterequest.form;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.time.LocalDate;

@Component
public class ViewTPQuoteRequestValidator implements Validator {

	public boolean supports(Class<?> clazz) {
		return clazz.equals(ViewTPQuoteRequestForm.class);
	}

	public void validate(Object target, Errors errors) {
		ViewTPQuoteRequestForm form = (ViewTPQuoteRequestForm) target;
		if (form.getDueDate() != null) {
			// if due date is an earlier date and it is NOT today
			LocalDate today = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
			if (!form.getDueDate().isAfter(today)) {
				errors.rejectValue("dueDate", "error.tpquoterequest.duedate", null,
					"Due-Date cannot be in the past");
			}
		}
	}
}