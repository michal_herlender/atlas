package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote.TPQuoteItemNote;

@Service
public class TPQuoteItemNoteServiceImpl extends BaseServiceImpl<TPQuoteItemNote, Integer> implements TPQuoteItemNoteService
{
	@Autowired
	private TPQuoteItemNoteDao tpQuoteItemNoteDao;
	
	@Override
	protected BaseDao<TPQuoteItemNote, Integer> getBaseDao() {
		return tpQuoteItemNoteDao;
	}
}