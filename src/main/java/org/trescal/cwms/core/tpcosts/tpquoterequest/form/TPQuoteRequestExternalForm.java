package org.trescal.cwms.core.tpcosts.tpquoterequest.form;

import java.util.List;

import org.trescal.cwms.core.pricing.entity.costtype.CostType;

import lombok.Data;

@Data
public class TPQuoteRequestExternalForm {

	private List<CostType> defaultCostTypes;
	private List<Integer> jobitemids;
	private List<Integer> modelCalTypeIds;
	private List<Integer> modelidQtys;
	private List<Integer> modelids;
	private List<Integer> quoteids;
	private List<Integer> noteids;
	private int itemid;
}