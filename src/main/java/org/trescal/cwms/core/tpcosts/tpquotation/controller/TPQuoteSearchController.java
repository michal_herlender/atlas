package org.trescal.cwms.core.tpcosts.tpquotation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.TPQuoteStatus;
import org.trescal.cwms.core.tpcosts.tpquotation.form.TPQuoteSearchForm;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class TPQuoteSearchController
{
	@Autowired
	private StatusService statusServ;
	@Autowired
	private TPQuotationService tpqServ;
	
	@ModelAttribute("command")
	protected TPQuoteSearchForm formBackingObject(HttpServletRequest request) throws Exception
	{
		return new TPQuoteSearchForm();
	}
	
	@RequestMapping(value="/tpquotesearch.htm", method=RequestMethod.POST)
	protected ModelAndView onSubmit(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdiv,
			@ModelAttribute("command") TPQuoteSearchForm form, BindingResult bindingResult) throws Exception {
		if (bindingResult.hasErrors()) return referenceData(form);
		form.setAllocatedSubdivId(subdiv.getKey());
		PagedResultSet<TPQuotation> rs = this.tpqServ.getTPQuotations(new PagedResultSet<>(form.getResultsPerPage() == 0 ? Constants.RESULTS_PER_PAGE : form.getResultsPerPage(), form.getPageNo() == 0 ? 1 : form.getPageNo()), form);
		form.setRs(rs);
		return new ModelAndView("trescal/core/tpcosts/tpquote/tpquotesearchresults");
	}
	
	@RequestMapping(value="/tpquotesearch.htm", params="forced=forced", method=RequestMethod.GET)
	public ModelAndView forceSubmit(@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdiv,
			@ModelAttribute("command") TPQuoteSearchForm form, BindingResult bindingResult,
			@RequestParam(value="mid", required=false, defaultValue="-1") Integer modelId) throws Exception
	{
		if (modelId >= 0) form.setModelid(modelId);
		return onSubmit(subdiv, form, bindingResult);
	}
	
	@RequestMapping(value="/tpquotesearch.htm", method=RequestMethod.GET)
	protected ModelAndView referenceData(@ModelAttribute("command") TPQuoteSearchForm form) throws Exception {
		Map<String, Object> refData = new HashMap<>();
		refData.put("statusList", this.statusServ.getAllStatuss(TPQuoteStatus.class));
		return new ModelAndView("trescal/core/thirdparty/tpquotesearch", refData);
	}
}