package org.trescal.cwms.core.tpcosts.tpquotation.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;

@Controller @IntranetController
public class DeleteTPQuotationController
{
	@Autowired
	private TPQuotationService tpQuoteServ;
	
	@RequestMapping(value="/deletetpquote.htm")
	public ModelAndView handleRequest(HttpServletRequest request) throws Exception
	{
		Integer id = Integer.parseInt(request.getParameter("id"));

		TPQuotation tpQuote = tpQuoteServ.get(id);
		tpQuoteServ.delete(tpQuote);
		
		return new ModelAndView(new RedirectView("tpquotesearch.htm", true));
	}
}
