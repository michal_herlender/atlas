package org.trescal.cwms.core.tpcosts.tpquoterequest.form;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;
import java.util.List;

/**
 * Form for creating a new TPQuoteRequest (may be blank, from a Job, Quote, or
 * other TPQuoteRequest)
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TPQuoteRequestForm extends TPQuoteRequestExternalForm {

    private LocalDate dueDate;
    private List<Integer> oldTPQRItemIds;
    // create request form fields
    private Integer personid;
    // Only valid when creating request from another source
    private Integer basedOnTPQuoteRequestId;
    private Integer requestFromJobId;
    private Integer requestFromQuoteId;
}