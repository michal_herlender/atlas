package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotenote.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotenote.TPQuoteNote;

@Repository
public class TPQuoteNoteDaoImpl extends BaseDaoImpl<TPQuoteNote, Integer> implements TPQuoteNoteDao
{
	@Override
	protected Class<TPQuoteNote> getEntity() {
		return TPQuoteNote.class;
	}
}