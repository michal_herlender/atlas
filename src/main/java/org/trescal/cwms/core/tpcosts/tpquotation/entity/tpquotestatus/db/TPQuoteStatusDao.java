package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.TPQuoteStatus;

public interface TPQuoteStatusDao extends BaseDao<TPQuoteStatus, Integer>
{
}