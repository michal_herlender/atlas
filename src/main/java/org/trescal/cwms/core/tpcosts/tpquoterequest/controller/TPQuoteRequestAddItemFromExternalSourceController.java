package org.trescal.cwms.core.tpcosts.tpquoterequest.controller;

import java.util.HashMap;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.entity.costtype.db.CostTypeService;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.TPQuoteRequestExternalForm;
import org.trescal.cwms.spring.model.KeyValue;

/**
 * Form Controller which enables users to add items to a TPQuoteRequest from a
 * list of {@link QuotationItemService} or {@link JobItem} that the
 * TPQuoteRequest was created from originally. Form allows users to select from
 * a list of actual {@link QuotationItem} or {@link JobItem} or alternately a
 * list of distinct modelTypes that appear on the Quotation that the
 * TPQuoteRequest was created from originally. <br/>
 * In addition, the top of the form lists all available CostTypes. On submission
 * the form checks if any of these costs have been added/removed the the
 * TPQuoteRequest default CostTypes list and saves this back to the
 * TPQuoteRequest automatically.
 */
@Controller @IntranetController
@SessionAttributes(Constants.SESSION_ATTRIBUTE_SUBDIV)
public class TPQuoteRequestAddItemFromExternalSourceController
{
	@Autowired
	private CalibrationTypeService calTypeService;
	@Autowired
	private CostTypeService costTypeService;
	@Autowired
	private JobService jobService;
	@Autowired
	private QuotationService quoteService;
	@Autowired
	private TPQuoteRequestService tpQuoteRequestService;

	@ModelAttribute("tpQuoteRequestForm")
	protected TPQuoteRequestExternalForm formBackingObject(
			) throws Exception
	{
		TPQuoteRequestExternalForm tpQuoteReqForm = new TPQuoteRequestExternalForm();
		return tpQuoteReqForm;
	}
	
	@RequestMapping(value="/tpquotereqadditemexternal.htm", method=RequestMethod.POST)
	protected String onSubmit(Model model,
			@RequestParam(value="id", required=true) Integer tpqrid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto,
			@ModelAttribute("tpQuoteRequestForm") TPQuoteRequestExternalForm tpForm, BindingResult bindingResult) throws Exception
	{
		if (bindingResult.hasErrors()) return referenceData(model, tpqrid, subdivDto);
		this.tpQuoteRequestService.addItemsFromExternalSource(tpqrid, tpForm);
		return "redirect:viewtpquoterequest.htm?id="+tpqrid;
	}
	
	@RequestMapping(value="/tpquotereqadditemexternal.htm", method=RequestMethod.GET)
	protected String referenceData(Model model,
			@RequestParam(value="id", required=true) Integer tpqrid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer,String> subdivDto) throws Exception
	{
		model.addAttribute("caltypes", this.calTypeService.getActiveCalTypes());
		model.addAttribute("costTypes", this.costTypeService.getAllActiveCostTypes());
		TPQuoteRequest tpqr = this.tpQuoteRequestService.get(tpqrid);
		model.addAttribute("req",tpqr);
		if (tpqr == null) {
			throw new Exception("The third party quote request could not be found");
		}
		else {
			if ((tpqr.getRequestFromQuote() != null) && 
				(tpqr.getRequestFromQuote().getQuotationitems() != null)) {
				// get a distinct list of models / instrumentmodels to display
				Set<InstrumentModel> mods = this.quoteService.getDistinctModels(tpqr.getRequestFromQuote());

				// convert this into a HashMap
				HashMap<Integer, InstrumentModel> quoteMods = new HashMap<Integer, InstrumentModel>();
				int id = 0;
				for (InstrumentModel m : mods) {
					quoteMods.put(id, m);
					id++;
				}
				model.addAttribute("models",quoteMods);
			}
			else if ((tpqr.getRequestFromJob() != null)
					&& (tpqr.getRequestFromJob().getItems() != null)) {
				// get a distinct list of models to display
				Set<InstrumentModel> mods = this.jobService.getDistinctModels(tpqr.getRequestFromJob());
				// convert this into a HashMap
				HashMap<Integer, InstrumentModel> quoteMods = new HashMap<Integer, InstrumentModel>();
				int id = 0;
				for (InstrumentModel m : mods) {
					quoteMods.put(id, m);
					id++;
				}
				model.addAttribute("models",quoteMods);
			}
			// set a list of all available costtypes into the fbo. We have to do
			// this here as
			// referenceData() data is lost after the first search submit.
		}
		return "trescal/core/tpcosts/tpquoterequest/tpquoterequestadditemfromquote";
	}
}