package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequeststatus;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.trescal.cwms.core.system.entity.status.Status;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;

@Entity
@DiscriminatorValue("tpquoterequest")
public class TPQuoteRequestStatus extends Status
{
	private Set<TPQuoteRequest> tpQuoteRequests;
	
	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "status")
	public Set<TPQuoteRequest> getTpQuoteRequests()
	{
		return tpQuoteRequests;
	}
	
	public void setTpQuoteRequests(Set<TPQuoteRequest> tpQuoteRequests)
	{
		this.tpQuoteRequests = tpQuoteRequests;
	}
}
