package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.TPQuoteStatus;

public interface TPQuoteStatusService extends BaseService<TPQuoteStatus, Integer>
{
}