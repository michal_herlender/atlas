package org.trescal.cwms.core.tpcosts.tpquoterequest.form;

import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tpcosts.tpquoterequest.dto.TPQuoteRequestDTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TPQuoteRequestSearchForm
{
	private Integer personid;
	private String contact;
	private Integer coid;
	private String coname;
	private String requestno;
	private String issued;
	private Integer statusid;
	// results data
	private PagedResultSet<TPQuoteRequestDTO> rs;
	private int pageNo;
	private int resultsPerPage;

}
