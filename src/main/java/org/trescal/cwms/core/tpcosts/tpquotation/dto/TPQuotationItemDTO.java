package org.trescal.cwms.core.tpcosts.tpquotation.dto;

import lombok.Getter;
import lombok.Setter;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitemnote.TPQuoteRequestItemNote;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter @Setter
public class TPQuotationItemDTO {

	private boolean setOnQuotation;
	private Integer requestItemId;
	private Integer quantity;
	private Integer calTypeId;
	private BigDecimal carriageIn;
	private BigDecimal carriageOut;
	private BigDecimal discountRate;
	private List<TPQuotationItemCostDTO> costs;
	private List<TPQuotationItemDTO> modules;
	private InstrumentModel model;
	private Set<TPQuoteRequestItemNote> notes;
	private List<Integer> noteIds;

	public TPQuotationItemDTO(Integer requestItemId, Integer calTypeId, InstrumentModel model) {
        setOnQuotation = true;
        this.requestItemId = requestItemId;
        quantity = 1;
        this.calTypeId = calTypeId;
        carriageIn = new BigDecimal("0.00");
        carriageOut = new BigDecimal("0.00");
        discountRate = new BigDecimal("0.0");
        costs = new ArrayList<>();
        noteIds = new ArrayList<>();
        this.setModel(model);
    }
}