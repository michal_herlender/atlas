package org.trescal.cwms.core.tpcosts.tpquotation.form;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

import java.time.LocalDate;

@Component
public class CreateTPQuotationValidator extends AbstractBeanValidator {
	public boolean supports(Class<?> clazz) {
		return clazz.equals(CreateTPQuotationForm.class);
	}

	public void validate(Object target, Errors errors)
	{
		CreateTPQuotationForm command = (CreateTPQuotationForm) target;
		TPQuotation tpquote = command.getTpQuotation();

		super.validate(command, errors);

		// perform some custom date validation
		if ((tpquote.getIssuedate() != null)
			&& tpquote.getIssuedate().isAfter(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()))) {
			errors.rejectValue("tpQuotation.issuedate", "error.tpquotation.issuesate", null, "Issue date cannot be in the future");
		}
		if (tpquote.getRegdate().isAfter(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()))) {
			errors.rejectValue("tpQuotation.regdate", "error.tpquotation.regdate", null, "Receipt date cannot be in the future");
		}

	}
}