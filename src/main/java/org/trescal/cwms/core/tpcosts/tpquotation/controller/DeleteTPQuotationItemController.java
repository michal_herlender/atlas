package org.trescal.cwms.core.tpcosts.tpquotation.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.pricing.entity.costs.CostCalculator;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.db.TPQuotationItemService;

@Controller @IntranetController
public class DeleteTPQuotationItemController
{
	@Autowired
	private TPQuotationItemService tpQuoteItemServ;
	@Autowired
	private TPQuotationService tpQuoteServ;
	
	@RequestMapping(value="/deletetpqitem.htm")
	protected ModelAndView handleRequestInternal(HttpServletRequest request) throws Exception 
	{
		int tpQuoteItemId = ServletRequestUtils.getIntParameter(request, "id", 0);
		TPQuotationItem toDel = tpQuoteItemServ.get(tpQuoteItemId);
		TPQuotation tpQuote = tpQuoteServ.get(toDel.getTpquotation().getId());
		
		// explicitly remove the item entity from the TPQuotation or hibernate will attempt to resave it after it's been deleted
		tpQuote.getItems().remove(toDel);
		
		tpQuoteItemServ.delete(toDel);
		
		CostCalculator.updatePricingFinalCost(tpQuote, tpQuote.getItems());
		tpQuoteServ.update(tpQuote);
		
		return new ModelAndView(new RedirectView("/viewtpquote.htm?id="+tpQuote.getId(), true));
	}
}
