/**
 * 
 */
package org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.repcost;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.trescal.cwms.core.pricing.entity.costs.base.Cost;
import org.trescal.cwms.core.pricing.entity.costs.base.RepCost;
import org.trescal.cwms.core.pricing.jobcost.entity.costs.repcost.JobCostingRepairCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;

@Entity
@DiscriminatorValue("tpquotation")
public class TPQuotationRepairCost extends RepCost
{
	/**
	 * All Job Costing Repair costs ( {@link JobCostingRepairCost}) derived
	 * from this {@link TPQuotationRepaorCost}.
	 */
	private Set<JobCostingRepairCost> linkedJobRepCosts;

	private TPQuotationItem tpQuoteItem;

	@Override
	@Transient
	public Cost getLinkedCostSrc()
	{
		return null;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, mappedBy = "linkedCostSrc")
	public Set<JobCostingRepairCost> getLinkedJobRepCosts()
	{
		return this.linkedJobRepCosts;
	}

	/**
	 * @return the tpQuoteItem
	 */
	@OneToOne(mappedBy = "repairCost")
	public TPQuotationItem getTpQuoteItem()
	{
		return this.tpQuoteItem;
	}

	/**
	 * @param linkedJobCalCosts the linkedJobCalCosts to set
	 */
	public void setLinkedJobRepCosts(Set<JobCostingRepairCost> linkedJobRepCosts)
	{
		this.linkedJobRepCosts = linkedJobRepCosts;
	}

	/**
	 * @param tpQuoteItem the tpQuoteItem to set
	 */
	public void setTpQuoteItem(TPQuotationItem tpQuoteItem)
	{
		this.tpQuoteItem = tpQuoteItem;
	}
}
