package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotenote;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.trescal.cwms.core.system.entity.note.Note;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;

@Entity
@Table(name = "tpquotenote")
public class TPQuoteNote extends Note
{
	private TPQuotation tpquotation;
	
	@Override
	@Transient
	public NoteType getNoteType() {
		return NoteType.TPQUOTENOTE;
	}
	
	@ManyToOne(cascade = {}, fetch = FetchType.LAZY)
	@JoinColumn(name = "id", unique = false, nullable = false, insertable = true, updatable = true)
	public TPQuotation getTpquotation() {
		return tpquotation;
	}
	
	public void setTpquotation(TPQuotation quotation) {
		tpquotation = quotation;
	}
}