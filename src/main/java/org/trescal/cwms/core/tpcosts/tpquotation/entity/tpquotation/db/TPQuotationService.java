package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.pricing.TotalDiscountCalculator;
import org.trescal.cwms.core.system.entity.supportedcurrency.SupportedCurrency;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tpcosts.tpquotation.dto.TPQuotationAjaxWrapper;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.TPQuoteLink;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.TPQuoteStatus;
import org.trescal.cwms.core.tpcosts.tpquotation.form.CreateTPQuotationFromTPQuoteRequestForm;
import org.trescal.cwms.core.tpcosts.tpquotation.form.TPQuoteSearchForm;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;

public interface TPQuotationService extends BaseService<TPQuotation, Integer> {
	/**
	 * Createa a new {@link TPQuotation} from an ajax call. Validates the data
	 * passed over and removes any 'bad' values. Automatically creates a
	 * {@link TPQuoteLink} using the given jobitemid.
	 *
	 * @param currencyCode.
	 * @param personid       the personid of the contact at third party.
	 * @param tpqno          the tp quote number.
	 * @param quotedate
	 * @param calcost
	 * @param repaircost
	 * @param adjustcost
	 * @param salescost
	 * @param inspectioncost
	 * @param carin
	 * @param carout
	 * @param jobitemid      the id of the {@link JobItem} being linked from.
	 * @param duration       in days.
	 * @param statusid
	 * @return {@link ResultWrapper} indicating success of operation.
	 */
	ResultWrapper createAjaxTPQuotation(Integer personid, String tpqno, String quotedateText, String calcost, String calcostdisc, String repaircost, String repaircostdisc, String adjustcost, String adjustcostdisc, String salescost, String salescostdisc, String inspectioncost, String itemdisc, String carin, String carout, Integer jobitemid, Integer duration, String currencyCode, Integer statusid, HttpSession session);
	
	/**
	 * Loads a TPQuotation eagerly so we can retrieve selected information for
	 * display in pop up.
	 * 
	 * @param id the id of the TPQuotation
	 * @return TPQuotation the third party quotation requested
	 */
	TPQuotation findEagerTPQuotation(int id);
	
	/**
	 * Populates a {@link TPQuotationAjaxWrapper} with {@link SupportedCurrency}
	 * and {@link TPQuoteStatus} data.
	 * 
	 * @return populated {@link TPQuotationAjaxWrapper}.
	 */
	TPQuotationAjaxWrapper getAjaxTPQuotationReferenceData(Integer jobItemId);
	
	TotalDiscountCalculator getQuotationTotalDiscount(Set<TPQuotationItem> items);
	
	PagedResultSet<TPQuotation> getTPQuotations(PagedResultSet<TPQuotation> rs, TPQuoteSearchForm tpsf);
	
	/**
	 * Returns the {@link TPQuotation} with the given tpq No or null if there
	 * are no third party quote with the given tpq No.
	 * 
	 * @param tpqNo the tpq No to match.
	 * @return the {@link TPQuotation}
	 */
	TPQuotation getTPQuoteByExactTPQNo(String tpqNo);
	
	/**
	 * this method retrieves all {@link TPQuotation} created from a
	 * {@link TPQuoteRequest}
	 * 
	 * @param tpqrId id of the third party quotation request
	 * @return {@link List} {@link TPQuotation}
	 */
	List<TPQuotation> getTPQuotesFromTPRequest(int tpqrId);
	
	CreateTPQuotationFromTPQuoteRequestForm initialiseTPQuotationForm(TPQuoteRequest tpQuoteRequest);
	
	TPQuotation updateItemCosts(int tpQuoteId);
	
	TPQuotation updateItemCosts(TPQuotation tpquotation);
	
	TPQuotation createTPQuotation(String username, Integer allocatedSubdivId, 
			CreateTPQuotationFromTPQuoteRequestForm tpQuoteForm, Integer tpQuoteRequestId);
}