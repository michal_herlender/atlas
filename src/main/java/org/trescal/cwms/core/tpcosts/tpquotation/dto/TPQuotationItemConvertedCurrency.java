package org.trescal.cwms.core.tpcosts.tpquotation.dto;

public class TPQuotationItemConvertedCurrency
{
	private String carriageIn;
	private String carriageOut;
	private String cost;

	public String getCarriageIn()
	{
		return this.carriageIn;
	}

	public String getCarriageOut()
	{
		return this.carriageOut;
	}

	public String getCost()
	{
		return this.cost;
	}

	public void setCarriageIn(String carriageIn)
	{
		this.carriageIn = carriageIn;
	}

	public void setCarriageOut(String carriageOut)
	{
		this.carriageOut = carriageOut;
	}

	public void setCost(String cost)
	{
		this.cost = cost;
	}
}