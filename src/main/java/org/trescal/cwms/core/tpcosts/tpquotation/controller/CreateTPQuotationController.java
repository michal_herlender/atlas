package org.trescal.cwms.core.tpcosts.tpquotation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.misc.tool.CustomLocalDateEditor;
import org.trescal.cwms.core.misc.tool.MyCustomDateEditor;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.supportedcurrency.db.SupportedCurrencyService;
import org.trescal.cwms.core.tools.DateTools;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotestatus.TPQuoteStatus;
import org.trescal.cwms.core.tpcosts.tpquotation.form.CreateTPQuotationForm;
import org.trescal.cwms.core.tpcosts.tpquotation.form.CreateTPQuotationValidator;
import org.trescal.cwms.spring.model.KeyValue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.trescal.cwms.core.system.Constants.COMPANY_DEFAULT_CURRENCY;

@Controller
@IntranetController
@SessionAttributes({ Constants.SESSION_ATTRIBUTE_SUBDIV, Constants.SESSION_ATTRIBUTE_CONTACT })
public class CreateTPQuotationController {
	@Autowired
	private ContactService conServ;
	@Autowired
	private SupportedCurrencyService currencyServ;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private TPQuotationService tpQuoteServ;
	@Autowired
	private CreateTPQuotationValidator validator;
	
	public static final String FORM_NAME = "tpquoteform";

	@ModelAttribute(FORM_NAME)
	protected CreateTPQuotationForm formBackingObject(
		@RequestParam(name = "personid") Integer personid) throws Exception {
        CreateTPQuotationForm form = new CreateTPQuotationForm();
        TPQuotation tpQuote = new TPQuotation();
        Contact contact = this.conServ.get(personid);
        tpQuote.setContact(contact);
        tpQuote.setRegdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
        tpQuote.setIssuedate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
        tpQuote.setDuration(30);
        tpQuote.setStatus(new TPQuoteStatus());
        tpQuote.setDefaultQty(1);
        tpQuote.setCurrency(contact.getSub().getComp().getCurrency());
        tpQuote.setRate(tpQuote.getCurrency().getDefaultRate());
        tpQuote.setTotalCost(new BigDecimal("0.00"));
        tpQuote.setFinalCost(new BigDecimal("0.00"));
        tpQuote.setVatRate(new BigDecimal("0.00"));
        tpQuote.setVatValue(new BigDecimal("0.00"));
        form.setTpQuotation(tpQuote);
		return form;
	}
	
	@InitBinder(FORM_NAME)
	protected void initBinder(WebDataBinder binder) throws Exception {
		MyCustomDateEditor editor = new MyCustomDateEditor(DateTools.df_ISO8601, true);
		binder.registerCustomEditor(Date.class, editor);
		binder.registerCustomEditor(LocalDate.class, new CustomLocalDateEditor(DateTimeFormatter.ISO_DATE));
		binder.setValidator(validator);
	}

	@RequestMapping(value = "/tpquoteform.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(@RequestParam(name = "personid") Integer personid,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_CONTACT) Contact contact,
			@ModelAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV) KeyValue<Integer, String> subdiv,
			@ModelAttribute(FORM_NAME) @Validated CreateTPQuotationForm form, BindingResult bindingResult)
			throws Exception {
		if (bindingResult.hasErrors()) {
			return referenceData(personid, form);
		}
		TPQuotation tpQuote = form.getTpQuotation();
		tpQuote.setCreatedBy(contact);
		Subdiv sub = this.subdivService.get(subdiv.getKey());
		tpQuote.setOrganisation(sub.getComp());
		String qno = this.numerationService.generateNumber(NumerationType.TPQUOTATION, sub.getComp(), sub);
		tpQuote.setQno(qno);
		// check if the company default currency has been selected
		if (form.getCurrencyCode().equals(COMPANY_DEFAULT_CURRENCY)) {
			// use the company defaults to set currency and rate
			tpQuote.setCurrency(tpQuote.getContact().getSub().getComp().getCurrency());
			tpQuote.setRate(tpQuote.getContact().getSub().getComp().getRate());
		} else {
			// otherwise use the select option
			tpQuote.setCurrency(this.currencyServ.findSupportedCurrencyOrDefault(form.getCurrencyCode()));
			tpQuote.setRate(tpQuote.getCurrency().getDefaultRate());
		}
		tpQuote = this.tpQuoteServ.merge(tpQuote);
		return new ModelAndView(new RedirectView("viewtpquote.htm?id=" + tpQuote.getId(), true));
	}

	@RequestMapping(value = "/tpquoteform.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@RequestParam(name = "personid") Integer personid,
			@ModelAttribute(FORM_NAME) CreateTPQuotationForm form) throws Exception {
		Map<String, Object> refData = new HashMap<>();
		Contact contact = this.conServ.get(personid);
		
		refData.put("statusList", this.statusServ.getAllStatuss(TPQuoteStatus.class));
		refData.put("currencyopts", this.currencyServ.getCompanyCurrencyOptions(contact.getSub().getComp().getId()));
		return new ModelAndView("trescal/core/tpcosts/tpquote/tpquoteform", refData);
	}
}