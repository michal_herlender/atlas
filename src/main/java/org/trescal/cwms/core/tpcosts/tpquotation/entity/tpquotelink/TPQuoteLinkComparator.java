package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink;

import java.util.Comparator;

/**
 * Sorts {@link TPQuoteLink} according to date.
 * 
 * @author Richard
 */
public class TPQuoteLinkComparator implements Comparator<TPQuoteLink>
{
	@Override
	public int compare(TPQuoteLink o1, TPQuoteLink o2)
	{
        if (o1.getDate().isEqual(o2.getDate())) {
            return Integer.compare(o1.getId(), o2.getId());
        } else {
            return o1.getDate().compareTo(o2.getDate());
        }
    }
}
