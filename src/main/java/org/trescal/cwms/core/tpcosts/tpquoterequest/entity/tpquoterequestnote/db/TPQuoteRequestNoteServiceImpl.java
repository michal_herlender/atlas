package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote.TPQuoteRequestNote;

@Service("TPQuoteRequestNoteService")
public class TPQuoteRequestNoteServiceImpl extends BaseServiceImpl<TPQuoteRequestNote, Integer> implements TPQuoteRequestNoteService
{
	@Autowired
	private TPQuoteRequestNoteDao tpQuoteRequestNoteDao;
	
	@Override
	protected BaseDao<TPQuoteRequestNote, Integer> getBaseDao() {
		return tpQuoteRequestNoteDao;
	}
}