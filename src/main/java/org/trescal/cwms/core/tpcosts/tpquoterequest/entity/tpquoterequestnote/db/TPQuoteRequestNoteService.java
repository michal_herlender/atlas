package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote.db;

import org.trescal.cwms.core.audit.entity.db.BaseService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote.TPQuoteRequestNote;

public interface TPQuoteRequestNoteService extends BaseService<TPQuoteRequestNote, Integer>
{
}