package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.db;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.TPQuoteLink;

@Repository("TPQuoteLinkDao")
public class TPQuoteLinkDaoImpl extends BaseDaoImpl<TPQuoteLink, Integer> implements TPQuoteLinkDao
{
	@Override
	protected Class<TPQuoteLink> getEntity() {
		return TPQuoteLink.class;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public TPQuoteLink getEager(int id)
	{
		Criteria criteria = getSession().createCriteria(TPQuoteLink.class);
		criteria.add(Restrictions.idEq(id));
		criteria.setFetchMode("jobItem", FetchMode.JOIN);
		criteria.setFetchMode("tpQuoteItem", FetchMode.JOIN);
		criteria.setFetchMode("tpQuoteItem.tpquotation", FetchMode.JOIN);
		criteria.setFetchMode("tpQuoteItem.tpquotation.contact", FetchMode.JOIN);
		criteria.setFetchMode("tpQuoteItem.tpquotation.contact", FetchMode.JOIN);
		criteria.setFetchMode("tpQuoteItem.tpquotation.contact.sub", FetchMode.JOIN);
		criteria.setFetchMode("tpQuoteItem.tpquotation.contact.sub.comp", FetchMode.JOIN);
		criteria.setFetchMode("contact", FetchMode.JOIN);
		List<TPQuoteLink> links = (List<TPQuoteLink>) criteria.list();
		return links.size() < 1 ? null : links.get(0);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TPQuoteLink> get(int jobitemid, int tpquoteitemid)
	{
		Criteria criteria = getSession().createCriteria(TPQuoteLink.class);
		criteria.createCriteria("jobItem").add(Restrictions.idEq(jobitemid));
		criteria.createCriteria("tpQuoteItem").add(Restrictions.idEq(tpquoteitemid));
		return (List<TPQuoteLink>) criteria.list();
	}
}