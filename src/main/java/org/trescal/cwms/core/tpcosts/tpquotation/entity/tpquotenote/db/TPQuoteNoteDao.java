package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotenote.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotenote.TPQuoteNote;

public interface TPQuoteNoteDao extends BaseDao<TPQuoteNote, Integer> {}