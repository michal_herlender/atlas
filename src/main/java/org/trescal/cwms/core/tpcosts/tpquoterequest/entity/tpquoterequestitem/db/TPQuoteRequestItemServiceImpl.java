package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.LocaleResolver;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.CalReq;
import org.trescal.cwms.core.jobs.calibration.entity.calreq.db.CalReqService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.costtype.CostTypeComparator;
import org.trescal.cwms.core.pricing.entity.costtype.db.CostTypeService;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quoteitemnote.QuoteItemNote;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.tools.StringTools;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItemComparator;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitemnote.TPQuoteRequestItemNote;
import org.trescal.cwms.spring.model.KeyValue;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service("TPQuoteRequestItemService")
public class TPQuoteRequestItemServiceImpl implements TPQuoteRequestItemService {
	@Autowired
	private CalReqService calReqServ;
	@Autowired
	private SessionUtilsService sessionServ;
	@Autowired
	private TPQuoteRequestItemDao dao;
	@Autowired
	private CostTypeService costTypeService;
	@Autowired
	private LocaleResolver localeResolver; 

	/**
	 * Function looks at the TPQuoteRequestDefaultCostType list belonging to the
	 * TPQuoteRequest and converts this into a list of TPQuoteRequestedCostTypes
	 * for the given TPQuoteRequestItem.
	 * 
	 * @param tpQuoteReqItem TPQuoteRequestItem to append the CostTypes to.
	 * @param tpQuoteReq TPQuoteRequest to obtain the default costs from.
	 */
	@Override
	public void addItemCostTypesFromDefaults(TPQuoteRequestItem tpQuoteReqItem, TPQuoteRequest tpQuoteReq)
	{
		TreeSet<CostType> costTypes = new TreeSet<>(new CostTypeComparator());
		costTypes.addAll(tpQuoteReq.getDefaultCostTypes());
		tpQuoteReqItem.setCostTypes(costTypes);
	}

	@Override
	public void copyItemsToNewTPQuoteRequest(List<Integer> oldTPQRItemIds, TPQuoteRequest tpqr, Contact currentContact) {
		List<TPQuoteRequestItem> oldItems = this.getAllTPQRItems(oldTPQRItemIds);

		if (tpqr.getItems() == null) {
			tpqr.setItems(new TreeSet<>(new TPQuoteRequestItemComparator()));
		}

		for (TPQuoteRequestItem oldItem : oldItems) {
			TPQuoteRequestItem qi = new TPQuoteRequestItem();

			qi.setBaseUnit(oldItem.getBaseUnit());
			qi.setCaltype(oldItem.getCaltype());

			// create new list of cost types
			SortedSet<CostType> costtypes = new TreeSet<>(new CostTypeComparator());
			costtypes.addAll(oldItem.getCostTypes());
			qi.setCostTypes(costtypes);
			qi.setModel(oldItem.getModel());
			qi.setModules(new TreeSet<>(new TPQuoteRequestItemComparator()));
			qi.getModules().addAll(oldItem.getModules());
			qi.setPartOfBaseUnit(oldItem.isPartOfBaseUnit());
			qi.setQty(oldItem.getQty());
			qi.setReferenceNo(oldItem.getReferenceNo());
			qi.setRequestFromJobItem(oldItem.getRequestFromJobItem());
			qi.setRequestFromQuoteItem(oldItem.getRequestFromQuoteItem());
			qi.setTpQuoteRequest(tpqr);

			// notes null
			if (qi.getNotes() == null) {
				qi.setNotes(new TreeSet<>(new NoteComparator()));
			}
			// add each note from old quote item
			for (TPQuoteRequestItemNote oldnote : oldItem.getNotes()) {
				// create third party quote request item note from old
				TPQuoteRequestItemNote newnote = new TPQuoteRequestItemNote();
				newnote.setActive(oldnote.isActive());
				newnote.setDeactivatedBy(oldnote.getDeactivatedBy());
				newnote.setDeactivatedOn(oldnote.getDeactivatedOn());
				newnote.setLabel(oldnote.getLabel());
				newnote.setNote(oldnote.getNote());
				newnote.setPublish(oldnote.getPublish());
				newnote.setSetBy(currentContact);
				newnote.setSetOn(new Date());
				newnote.setTpQuoteRequestItem(qi);
				// add new note to third party quote request
				qi.getNotes().add(newnote);
			}
			// add item to tpqr
			tpqr.getItems().add(qi);
		}
	}

	@Override
	public TPQuoteRequestItem createTPQuoteItem(JobItem ji, TPQuoteRequest tpQuoteRequest, List<JobItem> jitems, List<CostType> costTypes)
	{
		TPQuoteRequestItem tpItem = new TPQuoteRequestItem();
		tpItem.setTpQuoteRequest(tpQuoteRequest);
		tpItem.setModel(ji.getInst().getModel());
		// instrument has plant or serial no
		if ((ji.getInst().getPlantno() != null)
			&& (!ji.getInst().getPlantno().equalsIgnoreCase(""))) {
			tpItem.setReferenceNo("(P) " + ji.getInst().getPlantno());
		} else {
			tpItem.setReferenceNo("(S) " + ji.getInst().getSerialno());
		}
		tpItem.setRequestFromJobItem(ji);
		tpItem.setCaltype(ji.getServiceType().getCalibrationType());

		// load the TPQuoteRequestItem's CostTypes
		this.loadCostTypes(tpItem, costTypes);
		tpItem.setQty(1);

		return tpItem;
	}

	/**
	 * Creates a new TPQuoteRequestItem based on the QuotationItem passed as the
	 * first argument. If the QuotationItem is a base unit then function will
	 * recursively call itself to add each module for the base unit.
	 * 
	 * @param tpQuoteRequest the {@link TPQuoteRequest} that the item will
	 *        belong to
	 *        checking)
	 * @param costTypes {@link List} of {@link CostType} that the
	 *        {@link TPQuoteRequestItem} should be assigned
	 * @return {@link TPQuoteRequestItem}
	 */
	@Override
	public TPQuoteRequestItem createTPQuoteItem(Quotationitem qi, TPQuoteRequest tpQuoteRequest, List<Quotationitem> qitems, List<CostType> costTypes)
	{
		TPQuoteRequestItem tpItem = new TPQuoteRequestItem();
		tpItem.setTpQuoteRequest(tpQuoteRequest);
		// is quote item instrument?
		if (qi.getInst() != null)
		{
			// get model from quote item instrument
			tpItem.setModel(qi.getInst().getModel());
			// instrument has plant or serial no
			if ((qi.getInst().getPlantno() != null)
					&& (!qi.getInst().getPlantno().equalsIgnoreCase("")))
			{
				tpItem.setReferenceNo(qi.getInst().getPlantno());
			}
			else
			{
				tpItem.setReferenceNo(qi.getInst().getSerialno());
			}
		}
		else
		{
			tpItem.setModel(qi.getModel());
		}
		// has reference number been assigned?
		// could not find on instrument, maybe still use quote item?
		if ((tpItem.getReferenceNo() == null)
				|| (tpItem.getReferenceNo().equalsIgnoreCase("")))
		{
			if ((qi.getPlantno() != null)
				&& (!qi.getPlantno().equalsIgnoreCase(""))) {
				tpItem.setReferenceNo(qi.getPlantno());
			}
		}
		tpItem.setRequestFromQuoteItem(qi);

		// load the TPQuoteRequestItem's CostTypes
		this.loadCostTypes(tpItem, costTypes);
		tpItem.setQty(qi.getQuantity());
		tpItem.setCaltype(qi.getServiceType().getCalibrationType());

		// look at this item, if it is a base unit and has modules see if they
		// have also been selected
		// to add to the request and if so include them as modules of this base
		// unit on the request.
		if ((qi.getModules() != null) && (qi.getModules().size() > 0)) {
			for (Quotationitem qiMod : qitems) {
				if (qiMod.isPartOfBaseUnit()) {
					if (qiMod.getBaseUnit().getId() == qi.getId()) {
						TPQuoteRequestItem modItem = this.createTPQuoteItem(qiMod, tpQuoteRequest, qitems, costTypes);
						modItem.setBaseUnit(tpItem);
						modItem.setPartOfBaseUnit(true);
						if (tpItem.getModules() == null) {
							tpItem.setModules(new HashSet<>(0));
						}
						tpItem.getModules().add(modItem);
					}
				}
			}
		}
		// quote item has notes?
		if ((qi.getNotes() != null) && (qi.getNotes().size() > 0)) {
			// initialise note list
			if (tpItem.getNotes() == null) {
				tpItem.setNotes(new TreeSet<>(new NoteComparator()));
			}
			// add each note on quote item to the new tp quote item
			for (QuoteItemNote qn : qi.getNotes()) {
				// is this note active and public?
				if (qn.isActive()) {
					// get current contact
					Contact con = this.sessionServ.getCurrentContact();
					// create new tp quote request item note
					TPQuoteRequestItemNote tpqrnote = new TPQuoteRequestItemNote();
					tpqrnote.setActive(qn.isActive());
					tpqrnote.setLabel(qn.getLabel());
					tpqrnote.setNote(qn.getNote());
					tpqrnote.setPublish(qn.getPublish());
					tpqrnote.setSetBy(con);
					tpqrnote.setSetOn(new Date());
					tpqrnote.setTpQuoteRequestItem(tpItem);
					// add note to third party quote request item
					tpItem.getNotes().add(tpqrnote);
				}
			}
		}

		// get calbration requirement for quote item
		CalReq cr = this.calReqServ.findCalReqsForQuotationItem(qi);
		// calibration requirement found?
		if (cr != null) {
			// get current contact
			Contact con = this.sessionServ.getCurrentContact();
			// initialise note list
			if (tpItem.getNotes() == null) {
				tpItem.setNotes(new TreeSet<>(new NoteComparator()));
			}
			// calreq has points or range?
			if ((cr.getPointSet() != null) || (cr.getRange() != null)) {
				// initialise note string
				String note = "";

				if (cr.getPointSet() != null) {
					// add points to note
					note = "Points: "
							+ StringTools.convertCalibrationPointSetToString(cr.getPointSet())
							+ ", ";
				}
				else if (cr.getRange() != null)
				{
					// add range to note
					note = "Range: "
							+ StringTools.convertCalibrationRangeToString(cr.getRange())
							+ ", ";
				}
				// calreq has public instructions?
				if ((cr.getPublicInstructions() != null)
						&& !cr.getPublicInstructions().trim().isEmpty())
				{
					// add public instructions to note
					note += "Calibration Instruction: "
							+ cr.getPublicInstructions().trim() + "";
				}
				// create note for points or range
				TPQuoteRequestItemNote tpqrnote = new TPQuoteRequestItemNote();
				tpqrnote.setActive(true);
				tpqrnote.setLabel("Calibration Instructions");
				tpqrnote.setNote(note.trim());
				tpqrnote.setPublish(true);
				tpqrnote.setSetBy(con);
				tpqrnote.setSetOn(new Date());
				tpqrnote.setTpQuoteRequestItem(tpItem);
				tpItem.getNotes().add(tpqrnote);
			}
		}

		return tpItem;
	}

	@Override
	public void deleteTPQuoteRequestItem(int id)
	{
		TPQuoteRequestItem tp = this.findTPQuoteRequestItem(id);
		if (tp != null)
		{
			this.dao.remove(tp);
		}
	}

	@Override
	public TPQuoteRequestItem findTPQuoteRequestItem(int id)
	{
		return this.dao.find(id);
	}

	@Override
	public List<TPQuoteRequestItem> getAllTPQRItems(List<Integer> ids)
	{
		return this.dao.getAllTPQRItems(ids);
	}

	@Override
	public List<TPQuoteRequestItem> getAllTPQuoteRequestItems()
	{
		return this.dao.findAll();
	}

	@Override
	public void insertTPQuoteRequestItem(TPQuoteRequestItem tpquoterequestitem)
	{
		this.dao.persist(tpquoterequestitem);
	}

	/**
	 * Creates and adds a collection of TPQuoteRequestedCostTypes to a Set which
	 * will become the set of cost types for a TPQuoteRequestItems
	 * 
	 * @param tpItem The TPQuoteRequestItem
	 * @param costTypeList a List of CostTypes
	 */
	@Override
	public void loadCostTypes(TPQuoteRequestItem tpItem, List<CostType> costTypeList)
	{
		TreeSet<CostType> costTypes = new TreeSet<>(new CostTypeComparator());
		costTypes.addAll(costTypeList);
		tpItem.setCostTypes(costTypes);
	}

	/*
	 * Called by DWR
	 */
	@Override
	public List<KeyValue<Integer, String>> getAvailableTPQuoteRequestCostTypeDtos(int tpItemId, HttpServletRequest request) {
		Locale locale = this.localeResolver.resolveLocale(request);
		LocaleContextHolder.setLocale(locale);
		
		TPQuoteRequestItem item = this.dao.find(tpItemId);
		return this.costTypeService.getRemainingCostTypeDtos(item.getCostTypes());
	}
	
	/*
	 * Called by DWR
	 */
	@Override
	public void addCostType(int costTypeId, int tpQuoteRequestId) {
		TPQuoteRequestItem tpQuoteReqItem = this.dao.find(tpQuoteRequestId);
		CostType ct = CostType.valueOf(costTypeId);
		tpQuoteReqItem.getCostTypes().add(ct);
		dao.merge(tpQuoteReqItem);
	}

	@Override
	public void saveOrUpdateAll(List<TPQuoteRequestItem> tpItems)
	{
		this.dao.saveOrUpdateAll(tpItems);
	}

	@Override
	public void updateTPQuoteRequestItem(TPQuoteRequestItem tpquoterequestitem) {
		this.dao.merge(tpquoterequestitem);
	}
}