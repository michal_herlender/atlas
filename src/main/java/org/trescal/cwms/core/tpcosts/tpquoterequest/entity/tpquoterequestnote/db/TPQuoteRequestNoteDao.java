package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote.TPQuoteRequestNote;

public interface TPQuoteRequestNoteDao extends BaseDao<TPQuoteRequestNote, Integer>
{
}