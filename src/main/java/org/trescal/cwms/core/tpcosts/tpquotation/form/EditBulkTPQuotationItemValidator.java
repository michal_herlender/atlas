package org.trescal.cwms.core.tpcosts.tpquotation.form;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.costs.adjcost.TPQuotationAdjustmentCost;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.validation.AbstractBeanValidator;

@Component
public class EditBulkTPQuotationItemValidator extends AbstractBeanValidator
{
	public boolean supports(Class<?> clazz)
	{
		return clazz.equals(EditBulkTPQuotationItemForm.class);
	}

	public void validate(Object target, Errors errors)
	{
		EditBulkTPQuotationItemForm command = (EditBulkTPQuotationItemForm) target;
		
		super.validate(command, errors);
		

		for (int i = 0; i < command.getTpQitems().size(); i++)
		{
			TPQuotationItem item = command.getTpQitems().get(i);

			// validate the TPQuotationItem
			Set<ConstraintViolation<TPQuotationItem>> violations = super.getConstraintViolations(item);
			for (ConstraintViolation<TPQuotationItem> violation : violations)
			{
				errors.rejectValue("tpQitems[" + i + "]." + violation.getPropertyPath(), null, violation.getMessage());
			}

			// now validate each of the costs
			Set<ConstraintViolation<TPQuotationAdjustmentCost>> violations1 = super.getConstraintViolations(item.getAdjustmentCost());
			for (ConstraintViolation<TPQuotationAdjustmentCost> violation : violations1)
			{
				errors.rejectValue("tpQitems[" + i + "].adjustmentCost." + violation.getPropertyPath(), null, violation.getMessage());
			}
		}
	}
}
