package org.trescal.cwms.core.tpcosts.tpquoterequest.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class TPQuoteRequestDTO {

    private Integer tpquoterequestId;
    private String requestNo;
    private Integer personId;
    private String contactName;
    private Boolean contactActive;
    private Integer coid;
    private String coname;
    private Boolean companyActive;
    private Boolean companyOnstop;
    private String status;
    private LocalDate regDate;
    private LocalDate issueDate;

    public TPQuoteRequestDTO(Integer tpquoterequestId, String requestNo, Integer personId, String firstName, String lastName,
                             Boolean contactActive, Integer coid, String coname, Boolean companyActive, Boolean companyOnstop, String status,
                             LocalDate issueDate, LocalDate regDate) {

        this.tpquoterequestId = tpquoterequestId;
        this.requestNo = requestNo;
        this.personId = personId;
        this.contactName = firstName + " " + lastName;
        this.contactActive = contactActive;
        this.coid = coid;
        this.coname = coname;
        this.companyActive = companyActive;
        this.companyOnstop = companyOnstop;
        this.status = status;
        this.regDate = regDate;
        this.issueDate = issueDate;
    }

}
