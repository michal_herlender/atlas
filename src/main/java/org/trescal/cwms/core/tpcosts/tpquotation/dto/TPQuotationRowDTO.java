package org.trescal.cwms.core.tpcosts.tpquotation.dto;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationitem.TPQuotationItem;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotelink.TPQuoteLink;

import lombok.Getter;

/**
 * Formatted result DTO intended for use with creation of TPQuoteRequest from JIThirdParty screen
 * Change as necessary if/when we replace DWR call with controller
 * Only the fields which are displayed in the row are included 
 */
@Getter
public class TPQuotationRowDTO {
	private Integer tpQuoteId;
	private Integer tpQuoteItemLinkId;
	private Integer tpQuoteItemNo;
	private String tpQuoteQno;
	private String tpQuoteCompanyName;
	private String tpQuoteContactName;
	private String tpQuoteItemLinkContactName;
	private String tpQuoteItemLinkDate;
	
	public TPQuotationRowDTO(TPQuoteLink quoteLink, Locale locale) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(locale);
		TPQuotationItem quoteItem = quoteLink.getTpQuoteItem();
		TPQuotation quote = quoteItem.getTpquotation();
		
		this.tpQuoteId = quote.getId();
		this.tpQuoteItemLinkId = quoteLink.getId();
		this.tpQuoteItemNo = quoteItem.getItemno();
		this.tpQuoteQno = quote.getQno();
		this.tpQuoteCompanyName = quote.getContact().getSub().getComp().getConame();
		this.tpQuoteContactName = quote.getContact().getName();
		this.tpQuoteItemLinkContactName = quoteLink.getContact().getName();
		this.tpQuoteItemLinkDate = quoteLink.getDate() != null ? quoteLink.getDate().format(formatter) : "";
	}
}
