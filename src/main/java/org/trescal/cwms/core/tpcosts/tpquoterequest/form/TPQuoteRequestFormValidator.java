package org.trescal.cwms.core.tpcosts.tpquoterequest.form;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.time.LocalDate;

@Component
public class TPQuoteRequestFormValidator implements Validator {

	public boolean supports(Class<?> clazz) {
		return clazz.equals(TPQuoteRequestForm.class);
	}

	public void validate(Object target, Errors errors) {
		TPQuoteRequestForm form = (TPQuoteRequestForm) target;

		if (form.getPersonid() == null) {
			errors.rejectValue("personid", "error.tpquoterequest.contact", null, "You must select a contact");
		}
		// check due date
		if (form.getDueDate() != null) {
			// new todays date
			LocalDate dateCheck = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
			// is due date before todays date?
			if (form.getDueDate().isBefore(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()))) {
				// check that it's not the same day? (i.e. today)
				if (form.getDueDate().isEqual(dateCheck)) {
					errors.rejectValue("dueDate", "error.tpquoterequest.duedate", null, "Due-Date cannot be in the past");
				}
			}
		}
		// check that the user is not creating a new quote from old
		if (form.getBasedOnTPQuoteRequestId() == null) {
			// no cost type has been selected
			if ((form.getDefaultCostTypes() == null) || form.getDefaultCostTypes().isEmpty()) {
				errors.rejectValue("defaultCostTypes", "error.tpquoterequest.costtypeids", null, "At least one cost type should be selected");
			}
		}
	}
}
