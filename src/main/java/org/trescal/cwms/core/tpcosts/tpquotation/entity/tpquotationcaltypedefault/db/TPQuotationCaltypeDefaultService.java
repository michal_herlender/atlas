package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault.db;

import java.util.List;

import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotationcaltypedefault.TPQuotationCaltypeDefault;

public interface TPQuotationCaltypeDefaultService
{
	TPQuotationCaltypeDefault findTPQuotationCaltypeDefault(int id);
	void insertTPQuotationCaltypeDefault(TPQuotationCaltypeDefault tpquotationcaltypedefault);
	void updateTPQuotationCaltypeDefault(TPQuotationCaltypeDefault tpquotationcaltypedefault);
	void deleteTPQuotationCaltypeDefault(TPQuotationCaltypeDefault tpquotationcaltypedefault);
	List<TPQuotationCaltypeDefault> getAllTPQuotationCaltypeDefaults();
	List<TPQuotationCaltypeDefault> findTPQuotationCaltypeDefaults(int tpQuoteId);
}