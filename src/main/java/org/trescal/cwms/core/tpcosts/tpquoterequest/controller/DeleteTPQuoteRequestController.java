package org.trescal.cwms.core.tpcosts.tpquoterequest.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.TPQuotation;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;

@Controller @IntranetController
public class DeleteTPQuoteRequestController
{
	@Autowired
	private TPQuoteRequestService tpqrServ;
	@Autowired
	private TPQuotationService tpqServ;

	@RequestMapping(value="/deletetpquoterequest.htm")
	public ModelAndView handleRequest(HttpServletRequest req) throws Exception
	{
		Integer id = Integer.parseInt(req.getParameter("id"));

		TPQuoteRequest tpRequest = this.tpqrServ.get(id);

		List<TPQuotation> tpqs = this.tpqServ.getTPQuotesFromTPRequest(tpRequest.getId());

		for (TPQuotation tpq : tpqs)
		{
			// null tpquote request
			tpq.setFromRequest(null);
			// update tp quote
			this.tpqServ.update(tpq);
		}

		this.tpqrServ.delete(tpRequest);

		return new ModelAndView(new RedirectView("tpquoterequestform.htm", true));
	}
}
