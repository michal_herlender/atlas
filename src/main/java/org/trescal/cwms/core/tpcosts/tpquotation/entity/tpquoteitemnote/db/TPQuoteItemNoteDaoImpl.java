package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote.db;

import org.springframework.stereotype.Repository;
import org.trescal.cwms.core.audit.entity.db.BaseDaoImpl;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote.TPQuoteItemNote;

@Repository
public class TPQuoteItemNoteDaoImpl extends BaseDaoImpl<TPQuoteItemNote, Integer> implements TPQuoteItemNoteDao
{
	@Override
	protected Class<TPQuoteItemNote> getEntity() {
		return TPQuoteItemNote.class;
	}
}