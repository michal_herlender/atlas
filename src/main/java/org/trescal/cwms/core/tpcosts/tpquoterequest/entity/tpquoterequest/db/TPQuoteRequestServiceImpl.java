package org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.company.entity.company.Company;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.ContactKeyValue;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.company.entity.subdiv.Subdiv;
import org.trescal.cwms.core.company.entity.subdiv.db.SubdivService;
import org.trescal.cwms.core.dwr.ResultWrapper;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.InstrumentModel;
import org.trescal.cwms.core.instrumentmodel.entity.instrumentmodel.db.InstrumentModelService;
import org.trescal.cwms.core.jobs.job.entity.job.db.JobService;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.JobItem;
import org.trescal.cwms.core.jobs.jobitem.entity.jobitem.db.JobItemService;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.misc.entity.numeration.NumerationType;
import org.trescal.cwms.core.misc.entity.numeration.db.NumerationService;
import org.trescal.cwms.core.pricing.entity.costtype.CostType;
import org.trescal.cwms.core.pricing.entity.costtype.CostTypeComparator;
import org.trescal.cwms.core.quotation.entity.quotation.db.QuotationService;
import org.trescal.cwms.core.quotation.entity.quotationitem.Quotationitem;
import org.trescal.cwms.core.quotation.entity.quotationitem.db.QuotationItemService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.calibrationtype.db.CalibrationTypeService;
import org.trescal.cwms.core.system.entity.note.NoteComparator;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.PagedResultSet;
import org.trescal.cwms.core.tools.componentdirectory.ComponentDirectoryService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.dto.TPQuoteRequestDTO;
import org.trescal.cwms.core.tpcosts.tpquoterequest.dto.TPQuoteRequestRowDTO;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItem;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.TPQuoteRequestItemComparator;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestitem.db.TPQuoteRequestItemService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequestnote.TPQuoteRequestNote;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequeststatus.TPQuoteRequestStatus;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.TPQuoteRequestExternalForm;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.TPQuoteRequestForm;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.TPQuoteRequestSearchForm;
import org.trescal.cwms.spring.model.KeyValue;

import lombok.extern.slf4j.Slf4j;

@Service("TPQuoteRequestService")
@Slf4j
public class TPQuoteRequestServiceImpl extends BaseServiceImpl<TPQuoteRequest, Integer> implements TPQuoteRequestService {
	@Autowired
	private CalibrationTypeService calTypeServ;
	@Autowired
	private ComponentDirectoryService compDirServ;
	@Autowired
	private ContactService conServ;
	@Autowired
	private NumerationService numerationService;
	@Autowired
	private JobService jobServ;
	@Autowired
	private JobItemService jiServ;
	@Autowired
	private InstrumentModelService modelServ;
	@Autowired
	private QuotationService quoteServ;
	@Autowired
	private QuotationItemService quotationItemServ;
	@Autowired
	private SystemComponentService scServ;
	@Autowired
	private SessionUtilsService sessionServ;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private SubdivService subdivService;
	@Autowired
	private TPQuoteRequestDao tpquoterequestDao;
	@Autowired
	private TPQuoteRequestItemService tpQuoteRequestItemServ;
	
	@Override
	protected BaseDao<TPQuoteRequest, Integer> getBaseDao() {
		return tpquoterequestDao;
	}

	
	@Override
	public void addItemsFromExternalSource(int tpqrid, TPQuoteRequestExternalForm tpForm) {
		// get the TPQuoteRequest and update the default CostTypes assigned to
		// it - this may have been altered by the user on the form
		TPQuoteRequest tpQuoteRequest = this.get(tpqrid);
		List<CostType> costTypes = tpForm.getDefaultCostTypes();
		this.updateCostTypeDefaults(tpQuoteRequest, costTypes);
		// get all CostTypes that the user wants to include on this
		// TPQuoteRequest
		// user is adding items from the quotationitem list
		if (tpForm.getQuoteids() != null) {
			// get all quotation items selected from the form
			List<Quotationitem> qitems = this.quotationItemServ.findQuotationItems(tpForm.getQuoteids());
			// create a list of third party quote request items
			ArrayList<TPQuoteRequestItem> tpReqItems = new ArrayList<>(0);
			for (Quotationitem qi : qitems) {
				if (!qi.isPartOfBaseUnit()) {
					// create and add a TPQuoteRequestItem to the list of
					// tpReqItems
					tpReqItems.add(this.tpQuoteRequestItemServ.createTPQuoteItem(qi, tpQuoteRequest, qitems, costTypes));
				}
			}
			// persist all new TPQuoteRequestItems
			this.tpQuoteRequestItemServ.saveOrUpdateAll(tpReqItems);
		}
		else if (tpForm.getJobitemids() != null) {
			List<JobItem> jItems = this.jiServ.getAllItems(tpForm.getJobitemids());
			List<TPQuoteRequestItem> items = new ArrayList<>();
			for (JobItem ji : jItems)
				items.add(this.tpQuoteRequestItemServ.createTPQuoteItem(ji, tpQuoteRequest, jItems, costTypes));
			// persist all new TPQuoteRequestItems
			this.tpQuoteRequestItemServ.saveOrUpdateAll(items);
		}
		// user is adding items from the models list
		else if (tpForm.getModelids() != null) {
			// iterate over the list of models and create a list of
			// TPQuoteRequestItems from them
			List<TPQuoteRequestItem> tpReqItems = new ArrayList<>();
			for (int key : tpForm.getModelids()) {
				InstrumentModel mod = this.modelServ.get(key);

				TPQuoteRequestItem tpItem = new TPQuoteRequestItem();
				tpItem.setTpQuoteRequest(tpQuoteRequest);
				tpItem.setModel(mod);

				// load a list of TPQuoteRequestedCostTypes for the item
				this.tpQuoteRequestItemServ.loadCostTypes(tpItem, costTypes);
				tpItem.setQty(tpForm.getModelidQtys().get(key));
				tpItem.setCaltype(this.calTypeServ.find(tpForm.getModelCalTypeIds().get(key)));
				tpReqItems.add(tpItem);
			}
			this.tpQuoteRequestItemServ.saveOrUpdateAll(tpReqItems);
		}
	}
		
	@Override
	public ResultWrapper createAjaxTPQuotationRequest(int jobitemid, int personid, String duedateText, int[] costtypeids, HttpSession session) {

		@SuppressWarnings("unchecked")
		KeyValue<Integer, String> subdivDto = (KeyValue<Integer, String>) session.getAttribute(Constants.SESSION_ATTRIBUTE_SUBDIV);
		Subdiv allocatedSubdiv = subdivService.get(subdivDto.getKey());
		// check cost type supplied
		if ((costtypeids != null) && (costtypeids.length > 0)) {
			
			// check due date
			LocalDate duedate = LocalDate.parse(duedateText);
			if (duedate != null) {
				// find the job item
				JobItem ji = this.jiServ.findJobItem(jobitemid);
				// job item null?
				if (ji != null) {
					// find contact for tp quote
					Contact tpcon = this.conServ.findEagerContact(personid);
					// contact null?
					if (tpcon != null) {
						// create date variable
						// create new tp quote request number
						TPQuoteRequest tpqReq = new TPQuoteRequest();
						// get next request number
						String reqno = numerationService.generateNumber(NumerationType.TP_QUOTATION_REQUEST, allocatedSubdiv.getComp(), allocatedSubdiv);
						tpqReq.setRequestNo(reqno);
						tpqReq.setContact(tpcon);
						tpqReq.setClientref("");
						tpqReq.setCreatedBy(this.sessionServ.getCurrentContact());
						tpqReq.setIssued(false);
						tpqReq.setOrganisation(allocatedSubdiv.getComp());
						tpqReq.setRegdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
						tpqReq.setReqdate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
						tpqReq.setDueDate(duedate);
						tpqReq.setStatus(this.statusServ.findDefaultStatus(TPQuoteRequestStatus.class));
						tpqReq.setRequestFromJob(ji.getJob());
						tpqReq.setDuration(1);
						// Use the currency of the job 
						tpqReq.setCurrency(ji.getJob().getCurrency());
						tpqReq.setRate(new BigDecimal("1.0"));
						// create new list of cost types
						List<CostType> costTypes = new ArrayList<>();
						// find all cost types selected
						for (int costtypeid : costtypeids) costTypes.add(CostType.valueOf(costtypeid));
						// insert the TPQuoteRequest
						this.save(tpqReq);
						// create new list of items
						TreeSet<TPQuoteRequestItem> items = new TreeSet<>(new TPQuoteRequestItemComparator());
						// add our only item from job item
						items.add(this.tpQuoteRequestItemServ.createTPQuoteItem(ji, tpqReq, null, costTypes));
						// set item onto request
						tpqReq.setItems(items);
						// update the third party
						tpqReq = this.merge(tpqReq);
						// Use DTO in wrapper (no LocalDate in DWR)
						Locale locale = LocaleContextHolder.getLocale();
						TPQuoteRequestRowDTO dto = new TPQuoteRequestRowDTO(tpqReq, locale);
						// return successful wrapper
						return new ResultWrapper(true, "", dto, null);
					}
					else {
						// return un-successful wrapper
						return new ResultWrapper(false, "Contact for third party quote request could not be found", null, null);
					}
				}
				else {
					// return un-successful wrapper
					return new ResultWrapper(false, "Job item for third party quote request could not be found", null, null);
				}
			}
			else {
				// return un-successful wrapper
				return new ResultWrapper(false, "Due date for third party quote request not supplied", null, null);
			}
		}
		else {
			// return un-successful wrapper
			return new ResultWrapper(false, "Cost type for third party quote request not supplied", null, null);
		}
	}
	
	@Override
	public TPQuoteRequest createTPQuoteRequestFromForm(TPQuoteRequestForm tpForm, int subdivid, Contact contact) {
		Subdiv allocatedSubdiv = subdivService.get(subdivid);
		TPQuoteRequest tpQuoteRequest = new TPQuoteRequest();
		// get the next requestid
		String reqno = numerationService.generateNumber(NumerationType.TP_QUOTATION_REQUEST, allocatedSubdiv.getComp(), allocatedSubdiv);
		tpQuoteRequest.setRequestNo(reqno);
		// set contact
		Contact clientcontact = this.conServ.get(tpForm.getPersonid());
		tpQuoteRequest.setContact(clientcontact);
		tpQuoteRequest.setDueDate(tpForm.getDueDate());
		tpQuoteRequest.setClientref("");
		tpQuoteRequest.setCreatedBy(contact);
		tpQuoteRequest.setIssued(false);
		tpQuoteRequest.setOrganisation(allocatedSubdiv.getComp());
		LocalDate now = LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId());
		tpQuoteRequest.setRegdate(now);
		tpQuoteRequest.setReqdate(now);
		tpQuoteRequest.setStatus(this.statusServ.findDefaultStatus(TPQuoteRequestStatus.class));
		tpQuoteRequest.setDuration(1);
		if (tpForm.getBasedOnTPQuoteRequestId() != null) {
			TPQuoteRequest sourceTPQR = this.get(tpForm.getBasedOnTPQuoteRequestId());
			tpQuoteRequest.setCurrency(sourceTPQR.getCurrency());
			tpQuoteRequest.setRate(sourceTPQR.getRate());
			tpQuoteRequest.setRequestFromQuote(sourceTPQR.getRequestFromQuote());
			tpQuoteRequest.setRequestFromJob(sourceTPQR.getRequestFromJob());
			if (tpQuoteRequest.getNotes() == null)
				tpQuoteRequest.setNotes(new TreeSet<>(new NoteComparator()));
			// add each note from old quote
			for (TPQuoteRequestNote oldnote : sourceTPQR.getNotes()) {
				// create third party quote request note from old
				TPQuoteRequestNote newnote = new TPQuoteRequestNote();
				newnote.setActive(oldnote.isActive());
				newnote.setDeactivatedBy(oldnote.getDeactivatedBy());
				newnote.setDeactivatedOn(oldnote.getDeactivatedOn());
				newnote.setLabel(oldnote.getLabel());
				newnote.setNote(oldnote.getNote());
				newnote.setPublish(oldnote.getPublish());
				newnote.setSetBy(contact);
				newnote.setSetOn(new Date());
				newnote.setTpQuoteRequest(tpQuoteRequest);
				// add new note to third party quote request
				tpQuoteRequest.getNotes().add(newnote);
			}
		}
		else {
			tpQuoteRequest.setCurrency(clientcontact.getSub().getComp().getCurrency());
			tpQuoteRequest.setRate(new BigDecimal("1.0"));
			if (tpForm.getRequestFromQuoteId() != null) {
				tpQuoteRequest.setRequestFromQuote(quoteServ.get(tpForm.getRequestFromQuoteId()));
			}
			if (tpForm.getRequestFromJobId() != null) {
				tpQuoteRequest.setRequestFromJob(jobServ.get(tpForm.getRequestFromJobId()));
			}
		}
		// get all CostTypes that the user wants to include on this
		// TPQuoteRequest
		List<CostType> costTypes = tpForm.getDefaultCostTypes();
		if (tpForm.getBasedOnTPQuoteRequestId() == null)
			// update it with a set Default CostTypes
			this.updateCostTypeDefaults(tpQuoteRequest, costTypes);
		if (tpForm.getOldTPQRItemIds() != null)
			// add items from old third party quote request
			this.tpQuoteRequestItemServ.copyItemsToNewTPQuoteRequest(tpForm.getOldTPQRItemIds(), tpQuoteRequest, contact);
		else if (tpForm.getQuoteids() != null) {
			// get all quotation items selected from the form
			List<Quotationitem> qitems = this.quotationItemServ.findQuotationItems(tpForm.getQuoteids());
			// create a list of third party quote request items
			List<TPQuoteRequestItem> tpReqItems = new ArrayList<>();
			for (Quotationitem qi : qitems)
				if (!qi.isPartOfBaseUnit())
					tpReqItems.add(this.tpQuoteRequestItemServ.createTPQuoteItem(qi, tpQuoteRequest, qitems, costTypes));
			TreeSet<TPQuoteRequestItem> items = new TreeSet<>(new TPQuoteRequestItemComparator());
			items.addAll(tpReqItems);
			tpQuoteRequest.setItems(items);
		}
		else if (tpForm.getJobitemids() != null) {
			List<JobItem> jItems = this.jiServ.getAllItems(tpForm.getJobitemids());
			TreeSet<TPQuoteRequestItem> items = new TreeSet<>(new TPQuoteRequestItemComparator());
			for (JobItem ji : jItems)
				items.add(this.tpQuoteRequestItemServ.createTPQuoteItem(ji, tpQuoteRequest, jItems, costTypes));
			tpQuoteRequest.setItems(items);
		}
		// user has selected one or more modeltypes to add TPQuoteRequestItems
		// from
		else if (tpForm.getModelids() != null) {
			List<TPQuoteRequestItem> tpReqItems = new ArrayList<>();
			for (int key : tpForm.getModelids()) {
				InstrumentModel mod = modelServ.get(key);
				TPQuoteRequestItem tpItem = new TPQuoteRequestItem();
				tpItem.setTpQuoteRequest(tpQuoteRequest);
				tpItem.setModel(mod);
				this.tpQuoteRequestItemServ.loadCostTypes(tpItem, costTypes);
				tpItem.setQty(tpForm.getModelidQtys().get(key));
				log.debug(tpForm.getModelCalTypeIds().get(key).toString());
				tpItem.setCaltype(tpForm.getModelCalTypeIds().get(key) == null ? null : this.calTypeServ.find(tpForm.getModelCalTypeIds().get(key)));
				tpReqItems.add(tpItem);
			}
			TreeSet<TPQuoteRequestItem> items = new TreeSet<>(new TPQuoteRequestItemComparator());
			items.addAll(tpReqItems);
			tpQuoteRequest.setItems(items);
		}
		// insert the TPQuoteRequest
		this.save(tpQuoteRequest);
		return tpQuoteRequest;
	}
	
	@Override
	public TPQuoteRequest get(Integer id)
	{
		TPQuoteRequest tp = tpquoterequestDao.find(id);
		// 2018-12-12 GB : Prevent directory lookup on repeated gets.
		if (tp != null && tp.getDirectory() == null) 
			tp.setDirectory(this.compDirServ.getDirectory(this.scServ.findComponent(Component.THIRD_PARTY_QUOTATION_REQUEST), tp.getRequestNo()));
		return tp;
	}
	
	@Override
	public TPQuoteRequest getTPQuoteReqByExactTPQRNo(String tpqrNo, int allocatedSubdivId)
	{
		Subdiv allocatedSubdiv = this.subdivService.get(allocatedSubdivId);
		return tpquoterequestDao.getTPQuoteReqByExactTPQRNo(tpqrNo, allocatedSubdiv.getComp());
	}
	
	@Override
	public void save(TPQuoteRequest tp)
	{
		tpquoterequestDao.persist(tp);
		// create a new tpquoterequest directory
		tp.setDirectory(this.compDirServ.getDirectory(this.scServ.findComponent(Component.THIRD_PARTY_QUOTATION_REQUEST), tp.getRequestNo()));
	}
	
	@Override
	public TPQuoteRequest merge(TPQuoteRequest tp)
	{
		// create a new tpquoterequest directory
		tp.setDirectory(this.compDirServ.getDirectory(this.scServ.findComponent(Component.THIRD_PARTY_QUOTATION_REQUEST), tp.getRequestNo()));
		tp = tpquoterequestDao.merge(tp);
		return tp;
	}
	
	@Override
	public PagedResultSet<TPQuoteRequestDTO> searchTPQuoteRequest(TPQuoteRequestSearchForm form, PagedResultSet<TPQuoteRequestDTO> rs, 
			Company allocatedCompany)
	{
		return this.tpquoterequestDao.searchTPQuoteRequest(form, rs, allocatedCompany);
	}
	
	/**
	 * @param costTypes List of CostType enums requested to add as defaults for this TPQuoteRequest.
	 */
	@Override
	public void updateCostTypeDefaults(TPQuoteRequest tpQuoteReq, List<CostType> costTypes)
	{
		if (costTypes == null) throw new IllegalArgumentException("costTypes was null");
		SortedSet<CostType> defaultCostTypes = new TreeSet<>(new CostTypeComparator());
		defaultCostTypes.addAll(costTypes);
		tpQuoteReq.setDefaultCostTypes(defaultCostTypes);
	}
	
	@Override
	public List<ContactKeyValue> getClientContacts(TPQuoteRequest tpQuoteRequest){
		return this.tpquoterequestDao.getClientContacts(tpQuoteRequest);
	}
	
}