package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote.db;

import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquoteitemnote.TPQuoteItemNote;

public interface TPQuoteItemNoteDao extends BaseDao<TPQuoteItemNote, Integer> {}