package org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotenote.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.trescal.cwms.core.audit.entity.db.BaseDao;
import org.trescal.cwms.core.audit.entity.db.BaseServiceImpl;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotenote.TPQuoteNote;

@Service
public class TPQuoteNoteServiceImpl extends BaseServiceImpl<TPQuoteNote, Integer> implements TPQuoteNoteService
{
	@Autowired
	private TPQuoteNoteDao tpquotenoteDao;
	
	@Override
	protected BaseDao<TPQuoteNote, Integer> getBaseDao() {
		return tpquotenoteDao;
	}
}