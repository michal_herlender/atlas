package org.trescal.cwms.core.tpcosts.tpquoterequest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.trescal.cwms.core.company.entity.contact.Contact;
import org.trescal.cwms.core.company.entity.contact.db.ContactService;
import org.trescal.cwms.core.contract.entity.contract.Contract;
import org.trescal.cwms.core.contract.entity.contract.db.ContractService;
import org.trescal.cwms.core.exception.controller.IntranetController;
import org.trescal.cwms.core.login.SessionUtilsService;
import org.trescal.cwms.core.pricing.entity.costtype.db.CostTypeService;
import org.trescal.cwms.core.system.Constants;
import org.trescal.cwms.core.system.entity.email.db.EmailService;
import org.trescal.cwms.core.system.entity.note.NoteType;
import org.trescal.cwms.core.system.entity.status.db.StatusService;
import org.trescal.cwms.core.system.entity.systemcomponent.Component;
import org.trescal.cwms.core.system.entity.systemcomponent.db.SystemComponentService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserService;
import org.trescal.cwms.core.tools.filebrowser.FileBrowserWrapper;
import org.trescal.cwms.core.tpcosts.tpquotation.entity.tpquotation.db.TPQuotationService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.TPQuoteRequest;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequest.db.TPQuoteRequestService;
import org.trescal.cwms.core.tpcosts.tpquoterequest.entity.tpquoterequeststatus.TPQuoteRequestStatus;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.ViewTPQuoteRequestForm;
import org.trescal.cwms.core.tpcosts.tpquoterequest.form.ViewTPQuoteRequestValidator;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@IntranetController
@SessionAttributes({Constants.HTTPSESS_NEW_FILE_LIST})
public class ViewTPQuoteRequestController {

	private static final String TP_QUOTE_REQUEST_FORM = "form";

	@Autowired
	private ContactService conServ;
	@Autowired
	private CostTypeService costTypeServ;
	@Autowired
	private EmailService emailServ;
	@Autowired
	private FileBrowserService fileBrowserServ;
	@Autowired
	private SystemComponentService scService;
	@Autowired
	private SessionUtilsService sessionServ;
	@Autowired
	private StatusService statusServ;
	@Autowired
	private TPQuoteRequestService tpQuoteReqServ;
	@Autowired
	private TPQuotationService tpQuoteServ;
	@Autowired
	private ViewTPQuoteRequestValidator validator;
	@Autowired
	private ContractService contractService;

	@InitBinder(TP_QUOTE_REQUEST_FORM)
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST)
	public List<String> newFiles() {
		return new ArrayList<>();
	}

	@ModelAttribute(TP_QUOTE_REQUEST_FORM)
	protected ViewTPQuoteRequestForm formBackingObject(
			@RequestParam(value = "id", required = false, defaultValue = "0") Integer id) {
		TPQuoteRequest tpqr = tpQuoteReqServ.get(id);
		return new ViewTPQuoteRequestForm(tpqr);
	}

	@RequestMapping(value = "/viewtpquoterequest.htm", method = RequestMethod.POST)
	protected ModelAndView onSubmit(@RequestParam(value = "id", required = false, defaultValue = "0") Integer id,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@ModelAttribute(TP_QUOTE_REQUEST_FORM) @Validated ViewTPQuoteRequestForm form, BindingResult bindingResult)
			throws Exception {
		if (bindingResult.hasErrors())
			return referenceData(id, newFiles);
		else {
			// check if the status has been changed to an 'issued' status and if so
			// update the issue date + contact etc
			TPQuoteRequest tpqr = tpQuoteReqServ.get(id);
			TPQuoteRequestStatus status = this.statusServ.findStatus(form.getStatusId(), TPQuoteRequestStatus.class);
			if (!tpqr.isIssued() && status.getIssued()) {
				tpqr.setIssueby(this.sessionServ.getCurrentContact());
				tpqr.setIssued(true);
				tpqr.setIssuedate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			}
			// save the quote request
			Contact contact = conServ.get(form.getContactId());
			tpqr.setContact(contact);
			tpqr.setStatus(status);
			tpqr.setDueDate(form.getDueDate());
			tpqr = this.tpQuoteReqServ.merge(tpqr);
			return new ModelAndView(new RedirectView("viewtpquoterequest.htm?id=" + tpqr.getId(), true));
		}
	}

	@RequestMapping(value = "/viewtpquoterequest.htm", params = "submission=submit", method = RequestMethod.GET)
	protected ModelAndView forceSubmit(@RequestParam(value = "id", required = false, defaultValue = "0") Integer id,
			@RequestParam(value = "issuing", required = false, defaultValue = "false") Boolean issuing,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles,
			@ModelAttribute(TP_QUOTE_REQUEST_FORM) @Validated ViewTPQuoteRequestForm form, BindingResult bindingResult)
			throws Exception {
		TPQuoteRequest tpqr = tpQuoteReqServ.get(id);
		if (issuing && !tpqr.isIssued()) {
			// issue the quotation from a hyperlink
			tpqr.setIssueby(this.sessionServ.getCurrentContact());
			tpqr.setIssued(true);
			tpqr.setIssuedate(LocalDate.now(LocaleContextHolder.getTimeZone().toZoneId()));
			// find the default 'issued' status for TPQuoteRequests
			tpqr.setStatus((TPQuoteRequestStatus) this.statusServ.findIssuedStatus(TPQuoteRequestStatus.class));
		}
		return onSubmit(id, newFiles, form, bindingResult);
	}

	@RequestMapping(value = "/viewtpquoterequest.htm", method = RequestMethod.GET)
	protected ModelAndView referenceData(@RequestParam(value = "id", required = false, defaultValue = "0") Integer id,
			@ModelAttribute(Constants.HTTPSESS_NEW_FILE_LIST) List<String> newFiles) throws Exception {
		TPQuoteRequest tpqr = this.tpQuoteReqServ.get(id);
		if ((id == 0) || (tpqr == null))
			throw new Exception("Third party quote request was not found");
		else
			tpqr.setSentEmails(this.emailServ.getAllComponentEmails(Component.THIRD_PARTY_QUOTATION_REQUEST, id));
		Map<String, Object> refData = new HashMap<>();
		// get files in the root directory
		FileBrowserWrapper fileBrowserWrapper = this.fileBrowserServ
			.getFilesForComponentRoot(Component.THIRD_PARTY_QUOTATION_REQUEST, tpqr.getId(), newFiles);
		refData.put("tpquoterequest", tpqr);
		refData.put("clientConList", this.conServ.getAllActiveSubdivContacts(tpqr.getContact().getSub().getSubdivid()));
		refData.put("tpQuotes", this.tpQuoteServ.getTPQuotesFromTPRequest(tpqr.getId()));
		refData.put("statusList", this.statusServ.getAllStatuss(TPQuoteRequestStatus.class));
		refData.put("costtypes", this.costTypeServ.getAllActiveCostTypes());
		refData.put(Constants.REFDATA_SYSTEM_COMPONENT,
			this.scService.findComponent(Component.THIRD_PARTY_QUOTATION_REQUEST));
		refData.put(Constants.REFDATA_SC_ROOT_FILES, fileBrowserWrapper);
		refData.put(Constants.REFDATA_FILES_NAME, this.fileBrowserServ.getFilesName(fileBrowserWrapper));
		refData.put("privateOnlyNotes", NoteType.TPQUOTEREQUESTNOTE.isPrivateOnly());
		// set tp quotation instructions links
		String instructionLinkCoids = String.valueOf(tpqr.getContact().getSub().getComp().getCoid());
		String instructionLinkSubdivids = String.valueOf(tpqr.getContact().getSub().getSubdivid());
		String instructionLinkContactids = String.valueOf(tpqr.getContact().getPersonid());
		String instructionLinkContractid = null;
		if(tpqr.getRequestFromQuote() != null) {
			instructionLinkCoids += ",".concat(String.valueOf(tpqr.getRequestFromQuote().getContact().getSub().getComp().getCoid()));
			instructionLinkSubdivids += ",".concat(String.valueOf(tpqr.getRequestFromQuote().getContact().getSub().getSubdivid()));
			instructionLinkContactids += ",".concat(String.valueOf(tpqr.getRequestFromQuote().getContact().getPersonid()));
			Contract quotationContrat = this.contractService.findContractByQuotation(tpqr.getRequestFromQuote());
			if (quotationContrat != null)
				instructionLinkContractid = String.valueOf(quotationContrat.getId());
		}
		refData.put("instructionLinkCoids", instructionLinkCoids);
		refData.put("instructionLinkSubdivids", instructionLinkSubdivids);
		refData.put("instructionLinkContactids", instructionLinkContactids);
		refData.put("instructionLinkContractid", instructionLinkContractid);
		return new ModelAndView("trescal/core/tpcosts/tpquoterequest/viewtpquoterequest", refData);
	}
}